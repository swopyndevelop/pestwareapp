<?php

/*
|--------------------------------------------------------------------------
| Web Routes PestWareApp
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Business\CommonCompany;
use Illuminate\Support\Facades\DB;
Route::get('/blog/post/{id}','Blog\PostController@index');
Route::get('/blog','Blog\BlogController@index');
Route::get('/','Landing\LandingController@index');
Route::name('login_new')->get('/instance/{form}/{other?}', function () {
    $countries = DB::table('countries')
        ->orderBy('name')
        ->get();
    if (auth()->check()) return redirect()->route('index_register');
    return view('vendor.adminlte.auth.login_new')->with(['countries' => $countries]);
});
Route::name('login_new_internal')->get('/register-internal', function () {
    $countries = DB::table('countries')
        ->orderBy('name')
        ->get();
    if (auth()->check()) return redirect()->route('index_register');
    return view('vendor.adminlte.auth.register_internal')->with(['countries' => $countries]);
});
Route::get('/register', function () {
    return redirect('/instance/register');
});
Route::get('/register/verify/{token}', 'Auth\RegisterController@verify');
Route::get('/recover/password/{email}', 'Auth\RegisterController@recoverPasswordMail');
Route::get('/recover/password/show/form/{token}', 'Auth\RegisterController@recoverPasswordShowForm');
Route::post('/account/email/verify', 'Auth\RegisterController@reSendEmailConfirm');
Route::post('/reset/password/token', 'Auth\RegisterController@recoverPasswordByToken');
Route::get('/login', function () {
    return redirect('/instance/login');
});
Route::name('terms_conditions')->get('/terms-conditions', function () {
    return view('vendor.adminlte.auth.terms_and_conditions');
});

Route::name('signature_service_get')->get('signature-service/{id}','Services\CalendarController@indexSignature');
Route::name('signature_service_post')->post('signature/service/mip','Services\CalendarController@signatureMip');

Route::name('pdf_quotation')->get('quotation/{quotation}/PDF','Quotations\RegisterController@PDF');
Route::name('pdf_service')->get('order/{order}/PDF','Services\ServiceController@PDF');
Route::name('pdf_order_service')->get('order/{order}/PDF/service','Services\ServiceController@orderServicePDF');
Route::name('pdf_order_service_template')->get('order/{order}/PDF/service/template','Services\ServiceController@orderServiceTemplate');
Route::name('pdf_order_service_template_os')->get('order/{order}/PDF/service/template/os','Services\ServiceController@orderServiceTemplateOS');
Route::name('pdf_custom')->get('quotation/{quotation}/custom/PDF','Quotations\CustomQuotationController@PDFCustom');
Route::name('station_monitoring_pdf')->get('station/monitoring/pdf/{idOrder}', 'StationMonitoring\InspectionsMonitoringController@stationMonitoringPdf');
Route::name('area_inspection_pdf')->get('area/inspections/pdf/{idOrder}', 'AreaMonitoring\InspectionsAreasController@areasInspectionPdf');
Route::name('view_pdf_sales')->get('pdf/sales/download/{id}','Sales\SaleController@viewPdfSales');
Route::name('view_pdf_purchase_order')->get('acounting/view/pdf/purchase/{id}','Accounting\AccountingController@viewPdfPurchaseOrder');
Route::name('whatsapp_pdf_purchase_order')->get('accounting/whatsapp/pdf/purchase/{id}','Accounting\AccountingController@whatsappPdfPurchaseOrder');

Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('register/instance', 'Auth\RegisterController@registerInstance')->name('register_instance');
Route::post('register/internal', 'Auth\RegisterInternalController@registerInstance')->name('register_internal_instance');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('demo', 'Auth\LoginController@demo')->name('demo');

Route::get('/phpinfo', function () {
    //phpinfo();
});

Route::post('conekta/webhook','Conekta\ConektaController@webhook');
Route::post('stripe/webhook', 'Stripe\StripeController@webhook');

//Giveaway
Route::name('giveaway')->get('giveaway','Giveaway\GiveawayController@index');

Route::group(['middleware' => ['auth']], function () {

    // START QUOTATION
    Route::name('index_register')->get('index_register/{jobcenter?}', 'Quotations\RegisterController@index');
    Route::name('index_inspection')->get('index_inspection/{jobcenter?}', 'Quotations\InspectionController@index');
    Route::get('/index_register/', function () {
        return redirect('/index_register');
    });
    Route::name('autocomplete_cellphone')->get('autocomplete/cellphone', 'Quotations\RegisterController@fetchCellphone');
    Route::name('autocomplete_customer_name')->get('autocomplete/customer/name', 'Quotations\RegisterController@fetchCustomerName');
    Route::name('plagues_by_service')->get('plagues/service/{serviceId}', 'Quotations\RegisterController@getPlaguesByService');
    Route::name('load_client')->get('autocomplete/client', 'Quotations\RegisterController@loadClient');
    Route::name('load_client_name')->get('autocomplete/client/name', 'Quotations\RegisterController@loadClientName');
    Route::name('load_client_by_quotation')->get('autocomplete/client/quotation', 'Quotations\RegisterController@loadClientByQuotation');
    Route::name('add_quotation')->post('quotation/create','Quotations\RegisterController@store');
    Route::name('massiveQuote')->get('quotation/massiveQuote','Quotations\RegisterController@massiveQuote');
    Route::name('edit_quotation')->get('quotation/edit/{idQuotation}', 'Quotations\RegisterController@editQuotation');
    Route::name('update_quotation')->put('quotation/update','Quotations\RegisterController@update');
    Route::name('delete_quotation')->delete('quotation/delete/{quotation}','Quotations\RegisterController@delete');
    Route::name('approved_quotation')->post('approved/quotation','Quotations\RegisterController@approvedQuotation');
    Route::name('quotation_price')->get('quotation/price', 'Quotations\RegisterController@getPriceQuotation');
    Route::name('last_quotation')->get('quotation/last','Quotations\RegisterController@LastQuotation');
    Route::name('mail_quotation')->post('quotation/send/mail','Quotations\RegisterController@Messamail_quotationge');
    Route::name('view_mail')->get('quotation/{id}/send','Quotations\RegisterController@viewMessage');
    Route::name('mail_generate')->post('quotation/mail/send','Quotations\RegisterController@MailGenerate');
    Route::name('deny_quotation')->post('quotation/deny','Quotations\RegisterController@Deny_Quotation');
    Route::name('cancel_view')->get('quotation/cancel/{quotation}','Quotations\RegisterController@CancelView');
    Route::name('cancel_quotation')->post('quotation/cancel','Quotations\RegisterController@Cancel_Quotation');
    Route::name('tracing_view')->get('quotation/tracing/{quotation}','Quotations\RegisterController@tracingView');
    Route::name('tracing_quotation')->post('quotation/tracing','Quotations\RegisterController@tracingQuotation');
    Route::name('recover')->put('quotation/recover/{quotation}','Quotations\RegisterController@Recover_Quotation');
    Route::name('whatsapp_quotation')->put('quotation/sendwhatsapp/pdf', 'Quotations\RegisterController@whatsapp');
    Route::name('descriptions_custom_quote')->get('descriptions/custom/quote/{id}', 'Quotations\DescriptionsCustomQuoteController@getDescriptionById');
    Route::name('descriptions_custom_quote_save')->post('descriptions/custom/save', 'Quotations\DescriptionsCustomQuoteController@saveDescription');
    Route::name('store_inspection')->post('inspection/create','Quotations\InspectionController@store');
    // END QUOTATION

    // START CUSTOM QUOTATION
    Route::name('add_quotation_custom')->post('quotation/custom/create','Quotations\CustomQuotationController@store');
    Route::name('update_quotation_custom')->put('quotation/custom/update', 'Quotations\CustomQuotationController@update');
    Route::name('delete_quotation_custom')->delete('quotation/custom/delete/{id}/{idCustom}', 'Quotations\CustomQuotationController@delete');
    Route::name('mail_custom')->get('quotation/custom/{quotation}/mail','Quotations\CustomQuotationController@customMessage');
    Route::name('generate_custom')->get('quotation/custom/mail/send','Quotations\CustomQuotationController@customGenerate');
    Route::name('get_concepts_custom_quot')->get('quotation/custom/concepts/{quotationId}', 'CustomServices\CustomServicesController@getAllConceptsByIdQuot');
    Route::name('get_concepts_by_quote')->get('quote/custom/concepts/{quotationId}', 'Quotations\CustomQuotationController@getConceptsByQuote');
    // END CUSTOM QUOTATION

    // START REINFORCEMENTS
    Route::name('index_reinforcements')->get('index/reinforcements', 'Reinforcements\ReinforcementsController@getReinforcements');
    // END REINFORCEMENTS

    // START RECURRENT
    Route::name('index_recurrent')->get('index/recurrent', 'Recurrent\RecurrentController@getRecurrent');
    Route::name('filters_quotation')->get('filters/recurrent/{idQuotation}', 'Recurrent\RecurrentController@getDataSelects');
    // END RECURRENT

    //START ORDER SERVICES
    Route::name('order_service')->get('order/services/{quotation}','Services\ServiceController@orderView');
    Route::name('order_service_custom')->get('order/services/custom/{quotation}','Services\ServiceController@orderViewCustom');
    Route::name('order_store')->post('order/create/service','Services\ServiceController@orderStore');
    Route::name('delete_view')->get('order/cancel/{service}','Services\ServiceController@deleteview');
    Route::name('delete_order')->post('order/delete','Services\ServiceController@delete');
    Route::name('mail_service')->get('order/sendmail','Services\ServiceController@mail');
    Route::name('order_view')->get('order/view/{idOrder}','Services\ServiceController@orderViewModal');
    Route::name('whatsapp_service')->put('order/sendwhatsapp/indications', 'Services\ServiceController@whatsapp');
    Route::name('reminder_service')->put('order/sendwhatsapp/reminder', 'Services\ServiceController@reminder');
    Route::name('confirmed_service')->put('order/confirmed/event', 'Services\ServiceController@confirmed');
    Route::name('order_recover')->put('order/recover/{order}','Services\ServiceController@orderRecover');
    //END ORDER SERVICES

    //START WARRANTY
    Route::name('warranty_store')->post('warranty/store/{idOrder}','Services\ServiceController@createWarranty');
    //END WARRANTY

    //START REINFORCEMENT
    Route::name('reinforcement_store')->post('reinforcement/store/{idOrder}','Services\ServiceController@createReinforcement');
    //END REINFORCEMENT

    //START TRACING
    Route::name('tracing_store')->post('tracing/store/{idOrder}','Services\ServiceController@createTracing');
    //END TRACING

    //START RATING
    Route::name('store_raiting')->post('rating/store','Services\ServiceController@ratingService');
    //END RATING

    //START CALENDAR
    Route::name('calendario')->get('service/schedule/{id?}/{idCustomer?}/{idEmployee?}/{idJobCenter?}', 'Services\CalendarController@index');
    Route::name('event_store')->post('service/schedule/store', 'Services\CalendarController@store');
    Route::name('edit_service')->get('order/edit/{service}','Services\CalendarController@edit');
    Route::name('update_order')->put('order/update','Services\CalendarController@update');
    Route::name('edit_event')->get('service/edit/{id}/schedule','Services\CalendarController@edit_event');
    Route::name('info_event')->get('schedule/event/{id}','Services\CalendarController@getInfoEvent');
    Route::name('load_technicians_available')->post('technicians/available','Services\CalendarController@getTechniciansAvailable');
    Route::name('update_event')->post('service/update','Services\CalendarController@update_event');
    Route::name('update_event_drop')->post('event/drop/update','Services\CalendarController@updateEventWithDrop');
    //END CALENDAR

    //START JOBTITLE PROFILE
    Route::name('jobtitleprofile')->get('jobtitleprofile/create/{jobprofile_id}/{name}', 'Jobtitleprofile\JobtitleprofileController@createPermission');
    Route::name('store_permission')->post('job_title_profile_permission/store','Jobtitleprofile\JobtitleprofileController@storePermission');
    Route::name('jobtitleprofile_add')->post('job_title_profile/add','Jobtitleprofile\JobtitleprofileController@addprofile');
    Route::name('jobtitleprofile_interview_add')->post('job_title_profile_interview/add','Jobtitleprofile\JobtitleprofileController@addInterview');
    Route::name('jobtitleprofile_contract_add')->post('job_title_profile_contract/add','Jobtitleprofile\JobtitleprofileController@addContract');
    Route::name('jobtitleprofile_demographic_add')->post('job_title_profile_demographic/add','Jobtitleprofile\JobtitleprofileController@addDemographic');
    Route::name('joblanguage_add')->post('job_language/add','Jobtitleprofile\JobtitlelanguageController@add');
    Route::name('joblanguage_delete')->post('job_language/delete','Jobtitleprofile\JobtitlelanguageController@delete');
    Route::name('jobtitle_office_add')->post('jobtitle_office/add','Jobtitleprofile\JobofficefunctionController@add');
    Route::name('jobtitle_office_delete')->post('jobtitle_office/delete','Jobtitleprofile\JobofficefunctionController@delete');
    Route::name('jobtitle_machine_add')->post('jobtitle_machine/add','Jobtitleprofile\JobtitlemachineController@add');
    Route::name('jobtitle_machine_delete')->post('jobtitle_machine/delete','Jobtitleprofile\JobtitlemachineController@delete');
    Route::name('jobtitle_function_add')->post('jobtitle_function/add','Jobtitleprofile\JobtitlefunctionController@add');
    Route::name('jobtitle_function_delete')->post('jobtitle_function/delete','Jobtitleprofile\JobtitlefunctionController@delete');
    Route::name('jobtitle_attitude_add')->post('jobtitle_attitude/add','Jobtitleprofile\JobtitleattitudeController@add');
    Route::name('jobtitle_attitude_delete')->post('jobtitle_attitude/delete','Jobtitleprofile\JobtitleattitudeController@delete');
    Route::name('jobtitle_software_add')->post('jobtitle_software/add','Jobtitleprofile\JobtitlesoftwareController@add');
    Route::name('jobtitle_software_delete')->post('jobtitle_software/delete','Jobtitleprofile\JobtitlesoftwareController@delete');
    Route::name('jobtitle_responsability_add')->post('jobtitle_responsability/add','Jobtitleprofile\JobtitleresponsabilityController@add');
    Route::name('jobtitle_responsability_delete')->post('jobtitle_responsability/delete','Jobtitleprofile\JobtitleresponsabilityController@delete');
    Route::name('jobtitleprofile_scholar')->post('job_title_profile_scholar/add','Jobtitleprofile\JobtitleprofileController@addscholar');
    Route::name('jobtitleprofile_scholar')->post('job_title_profile_scholar/delete','Jobtitleprofile\JobtitleprofileController@deletescholar');
    Route::name('jobtitle_experience_add')->post('job_title_experience/add','Jobtitleprofile\JobtitleexperienceController@add');
    Route::name('jobtitle_experience_delete')->post('job_title_experience/delete','Jobtitleprofile\JobtitleexperienceController@delete');
    Route::name('jobtitle_documents_add')->post('job_title_documents/add','Jobtitleprofile\JobtitledocumentController@add');
     //Route::name('show_image')->get('documents/{document}', 'Jobapplication\PersonaldataController@showDocument');
    Route::post('search_education_profile', 'Jobtitleprofile\JobtitleprofileController@select_education_profile');
    //END JOB TITLE

    //START JOB CENTER
     //Route::name('jobcenter')->get('jobcenter/create',function(){return view('adminlte::jobcenter.create');});
    Route::name('jobcenter')->get('profile_job_centers/create/{jobcenter_id}/{jobcenter_name}', 'JobCenter\ProfilejobcenterController@create');
    Route::name('jobcenter_address')->get('profile_job_centers/address/{jobcenter_id}/{jobcenter_name}', 'JobCenter\ProfilejobcenterController@createAddress');
    Route::name('jobcenter_address_store')->post('profile_job_centers/address/store', 'JobCenter\ProfilejobcenterController@storeAddress');
    Route::name('profile_job_centers_add')->post('profile_job_centers/add','JobCenter\ProfilejobcenterController@add');
    Route::name('address_job_centers_add')->post('address_job_centers/add','JobCenter\AddressjobcenterController@add');
    Route::name('workcapacity_job_centers_add')->post('workcapacity_job_centers/add','JobCenter\WorkcapacityController@add');
    Route::name('workcapacity_job_centers_delete')->post('workcapacity_job_centers/delete','JobCenter\WorkcapacityController@delete');
    Route::name('general_information_job_centers_add')->post('general_information_job_centers/add','JobCenter\GeneralinformationController@add');
    Route::name('goal_job_centers_add')->post('goal_job_centers/add','JobCenter\GoalJobCenterController@add');
    Route::name('sumaPersonal')->get('workcapacity_job_centers/sum/{jobcenter_id}','JobCenter\WorkcapacityController@totPersonal');
    //END JOB CENTER

    //START JOBCENTER TREE
    Route::name('tree_jobcenter')->get('tree_jobcenter/create','Tree\TreeController@create');
    Route::name('tree_jobcenter_read')->get('tree_jobcenter/read','Tree\TreeController@read');
    Route::name('tree_jobcenter_write')->post('tree_jobcenter/write','Tree\TreeController@write');
    //END JOBCENTER TREE

    //START JOBCENTER SUMMARY
    Route::name('tree_jobcenter_summary')->get('tree_jobcenter/createsummary','JobCenter\ProfilejobcenterController@create2');
    //START JOBCENTER SUMMARY

    //START JOBPROFILE TREE
    Route::name('tree_jobprofile')->get('tree_jobprofile/create','Treejobprofile\TreejobprofileController@create');
    Route::name('tree_jobprofile_read')->get('tree_jobprofile/read','Treejobprofile\TreejobprofileController@read');
    Route::name('tree_jobprofile_write')->post('tree_jobprofile/write','Treejobprofile\TreejobprofileController@write');
    Route::name('data_job_center')->get('data/job/center/{jobcenter_id}', 'JobCenter\DataJobCenterController@index');
    Route::name('data_job_center_company_update')->post('data/job/center/company', 'JobCenter\DataJobCenterController@updateCompany');
    Route::name('add_sanitary_license')->post('branch/sanitaryLicense', 'JobCenter\DataJobCenterController@addSanitaryLicense');
    Route::name('data_job_center_address_update')->post('data/job/center/address', 'JobCenter\DataJobCenterController@updateAddress');
    Route::name('data_job_center_tax_update')->post('data/job/center/tax', 'JobCenter\DataJobCenterController@updateTax');
    Route::name('data_job_center_ssid_update')->post('data/job/center/ssid', 'JobCenter\DataJobCenterController@updateSsid');
    Route::name('data_job_center_label_update')->post('data/job/center/label', 'JobCenter\DataJobCenterController@updateLabel');
    Route::name('data_job_center_billing')->post('data/job/center/billing', 'JobCenter\DataJobCenterController@updateBilling');
    Route::name('add_file_certificate_billing')->post('file/certificate/billing', 'JobCenter\DataJobCenterController@addFileCertificate');
    Route::name('add_file_key_billing')->post('file/key/billing', 'JobCenter\DataJobCenterController@addFileKey');
    //END JOBPROFILE TREE

    //START EMPLOYEES
    Route::name('kardex_employees')->get('employees/kardex/{jobcenter?}','Employees\EmployeeController@kardex');
    Route::name('kardexEmployeeCourses')->get('employees/evaluations/kardex/{jobcenter}/{jobTitle}','Employees\EmployeeController@kardexEvaluations');
    Route::name('SearchKardex')->get('employees/evaluations/kardex/{search}','Employees\EmployeeController@kardexSucursales');
    Route::name('index_employees')->get('employees/index/{id}','Employees\EmployeeController@index');
    Route::name('update_employees')->post('employees/update','Employees\EmployeeController@update');
    Route::name('searchEmployee')->post('employees/search','Employees\EmployeeController@searchEmployee');
    Route::name('update_status')->put('employees/status/{employee_status}','Employees\EmployeeController@updateStatus');
    Route::name('employees_change_password')->post('employees/change/password', 'Employees\EmployeeController@changePassword');
    //END EMPLOYEES

    //START DISCOUNTS
    Route::name('discount_admin')->get('discount/admin','Pays\PayController@adminDiscount');
    Route::name('search_discount')->get('discount/admin/search','Pays\PayController@adminDiscount');
    Route::name('create_discount')->get('discount/create','Pays\PayController@create_discount');
    Route::name('add_discount')->post('discount/add','Pays\PayController@add_discount');
    Route::name('edit_discount')->get('discount/{discount}/edit','Pays\PayController@edit_discount');
    Route::name('update_discount')->put('discount/update/{discount}','Pays\PayController@update_discount');
    Route::name('delete_discount')->delete('discount/delete/{discount}','Pays\PayController@delete_discount');
    //END DISCOUNTS

    //START EXTRA
    Route::name('extra_admin')->get('extra/admin','Pays\ExtraController@adminExtra');
    Route::name('search_extra')->get('extra/admin/search','Pays\ExtraController@adminExtra');
    Route::name('add_extra')->post('extra/add','Pays\ExtraController@add_extra');
    Route::name('edit_extra')->get('extra/{extra}/edit','Pays\ExtraController@edit_extra');
    Route::name('update_extra')->put('extra/update/{extra}','Pays\ExtraController@update_extra');
    Route::name('delete_extra')->delete('extra/delete/{extra}','Pays\ExtraController@delete_extra');
    //END EXTRA

    //START PRODUCT
    Route::name('index_product')->get('products/admin/{jobcenter?}','Products\ProductController@index');
    Route::name('product_search')->get('products/admin/search','Products\ProductController@index');
    Route::name('save_product')->post('products/save','Products\ProductController@save');
    Route::name('download_product')->get('products/{doc}/download','Products\ProductController@download');
    Route::name('edit_product')->get('products/{id}/edit','Products\ProductController@edit');
    Route::name('update_product')->post('products/update','Products\ProductController@update');
    Route::name('vdelete_product')->get('products/{id}/delete','Products\ProductController@view_delete');
    Route::name('delete_product')->post('products/delete','Products\ProductController@delete');
    Route::name('product_list_price')->get('product/list/price/{idJobCenter}','Products\ProductController@listPrice');
    //END PRODUCT

    //START TRAINING COURSES
    Route::name('training_study')->get('training/study',
        'Training\TrainingController@study');

    Route::name('training_personal_study')->get('training/personal/study/{id}',
        'Training\TrainingController@personalStudy');

    Route::name('module_personal_study')->get('module/personal/study/{id}',
        'Training\TrainingController@moduleStudy');

    Route::name('training_add')->post('training/add',
        'Training\TrainingController@add');
    //END TRAINING COURSES

    //START COURSE EVALUATION
    Route::name('intro')->get('courses/intro/{course}',
        'Courseevaluation\CourseevaluationController@intro');

    Route::name('course_view')->get('courses/index/{course}',
        'Courseevaluation\CourseevaluationController@index');

    Route::name('question_save')->post('courses/save',
        'Courseevaluation\CourseevaluationController@save');

    Route::name('resultsUser')->get('courses/resultsUser/{course}',
        'Courseevaluation\CourseevaluationController@resultsUser');
    //END COURSE EVALUATION

    //START TRAINING COURSES
    Route::name('training_index')->get('training/index',
        'Training\TrainingController@index');

    Route::name('training_edit')->get('training/{training}/edit',
        'Training\TrainingController@edit');

    Route::name('training_update')->put('training/{training}',
        'Training\TrainingController@update');

    Route::name('training_delete')->delete('training/{training}',
        'Training\TrainingController@delete');

    Route::name('training_course')->get('training/{training}/course',
        'Training\TrainingController@play');

    Route::name('training_progress')->post('training/progress',
        'Training\TrainingController@progress');

    Route::name('training_kardex')->get('training/kardex/{training}',
        'Training\TrainingController@kardexUser');

    Route::name('training_index_assign')->get('training/indexassign',
        'Training\TrainingController@indexassign');

    Route::name('training_create_assign')->get('training/createassign',
        'Training\TrainingController@createassign');

    Route::name('training_create_assign_plan')->get('training/createassign/plan',
        'Training\TrainingController@createassignplan');

    Route::name('training_store_assign')->post('training/storeassign',
        'Training\TrainingController@storeassign');

    Route::name('training_store_assign_plan')->post('training/storeassign/plan',
        'Training\TrainingController@storeassignplan');

    Route::name('training_edit_assign')->get('training/{trainingplan}/editassign',
        'Training\TrainingController@editassign');

    Route::name('training_update_assign')->put('trainingplan/{trainingplan}/updateassign',
        'Training\TrainingController@updateassign');

    Route::name('training_delete_assign')->delete('trainingplan/{trainingplan}',
        'Training\TrainingController@deleteassign');

    Route::name('training_create')->get('training/create', 'Training\TrainingController@create');

    Route::name('training_add')->post('training/add', 'Training\TrainingController@add');

    Route::name('new_module')->get('module/new', 'Training\TrainingController@createModule');

    Route::name('create_module')->post('module/create', 'Training\TrainingController@addModule');

    Route::name('delete_module')->delete('module/{module}/delete', 'Training\TrainingController@deleteModule');

    Route::name('module_assign_update')->put('module/{module}/update', 'Training\TrainingController@moduleUpdate');

    Route::name('module_edit')->get('module/edit/{module}', 'Training\TrainingController@editModule');
    //END TRAINING COURSES

    //START CREATE EVALUATION
    Route::name('kardex_question_evaluation')->get('evaluation/courses/{course}','Courseevaluation\EvaluationcreateController@kardex');
    Route::name('evaluation_course_create')->get('evaluationcourse/create/{course}/{question}','Courseevaluation\EvaluationcreateController@create');
    Route::name('evaluation_question_edit')->get('evaluationcourse/edit/{question}','Courseevaluation\EvaluationcreateController@editView');
    Route::name('evaluation_question_delete')->get('evaluationcourse/delete/{question}','Courseevaluation\EvaluationcreateController@delete');
    Route::name('evaluation_question_save')->post('evaluationcourse/save','Courseevaluation\EvaluationcreateController@save');
    Route::name('evaluation_question_edit_it')->post('evaluationcourse/editquestion','Courseevaluation\EvaluationcreateController@edit');
    Route::name('companyEvaluation')->get('courses/companyEvaluation','Courseevaluation\CourseevaluationController@CompanyEval');
    //END CREATE EVALUATION

    //START CORTES DE CAJA
    Route::name('index_cut')->get('pays/cut','Pays\CutController@index');
    Route::name('cut_cash')->get('pays/cut/cash/{user}','Pays\CutController@arqueo');
    Route::name('sn_arqueo')->get('pays/cut/arq/{id}/cash','Pays\CutController@sn_arqueo');
    Route::name('cuts_tech')->get('pays/cut/tech','Pays\CutController@Cuts_Tec');
    Route::name('detail_cons')->get('pays/consum/{id}/cut','Pays\CutController@Cuts_Consum');
    Route::name('detail_cons2')->get('pays/consum/{id}/cutadmin','Pays\CutController@Cuts_Consum2');
    Route::name('cuts_admin')->get('pays/cut/admin/{jobcenter?}','Pays\CutController@adminCuts');
    Route::name('report_cut')->get('pays/{id}/cut/report','Pays\CutController@report');
    Route::name('desfase_pay')->get('pays/{id}/desfase','Pays\CutController@desfase');
    Route::name('deposito')->get('pays/{id}/cut/desp','Pays\CutController@deposito');
    Route::name('add_dep')->post('pays/deposito','Pays\CutController@add_deposito');
    Route::name('detail_dep')->get('pays/deposito/{id}/detail','Pays\CutController@Dep_detail');
    Route::name('adeudo')->get('pays/delivered/{id}','Pays\CutController@Quit');
    Route::name('view_cut')->get('pays/cut/{id}/view','Pays\CutController@View');
    Route::name('cut_store')->get('pays/cut/cash/last','Pays\CutController@cut_store');
    //END CORTES DE CAJA

    //START ENTRADAS
    Route::name('index_entry')->get('entry/index/{jobcenter?}','Inventory\EntryController@index');
    Route::name('create_entry')->get('entry/create', 'Inventory\EntryController@createEntry');
    Route::name('add_entry')->post('entry/add','Inventory\EntryController@add_entry');
    Route::name('add_pdf_comprobant')->post('entry/store/comprobant/pdf','Inventory\EntryController@addComprobant');
    Route::name('view_entry')->get('entry/{id}/view','Inventory\EntryController@View_Detail');
    Route::name('data_product')->get('transfer/product/data', 'Inventory\EntryController@getData');
    Route::name('get_products_entry')->get('entry/products/get', 'Inventory\EntryController@getProducts');
    Route::name('data_product_entry')->get('entry/product/data', 'Inventory\EntryController@getDataProductEntry');
    Route::name('download_facture_entry')->get('factureEntry/{id}/download','Inventory\EntryController@download');
    Route::name('get_products_purchase_order')->get('purchase/products/get', 'Inventory\EntryController@getProductsForPurchaseOrder');
    Route::name('data_product_purchase_order')->get('purchase/product/data', 'Inventory\EntryController@getDataProductForPurchaseOrder');
    Route::name('get_data_purchase_order')->get('purchase/order/data', 'Inventory\EntryController@getDataPurchase');
    Route::name('delete_entry')->post('entry/delete','Inventory\EntryController@deleteEntry');
    Route::name('inventory_report_datastudio')->get('inventory/report/datastudio','Inventory\StoreHouseController@reportDataStudio');
    //END ENTRADAS

    //START TRASPASOS
    Route::name('index_transfer')->get('transfers/index', 'Inventory\TransferController@indexTransfer');
    Route::name('search_transfer')->get('transfer/home/search','Inventory\TransferController@indexTransfer');
    Route::name('get_products_available')->post('products/available', 'Inventory\TransferController@getProducts');
    Route::name('get_products_available_for_cc')->post('products/available/transfer/cc', 'Inventory\TransferController@getProductsForTransferCC');
    Route::name('create_transfer_cc')->get('transfer/cc/create', 'Inventory\TransferController@createTransferCC');
    Route::name('create_transfer_ce')->get('transfer/ce/create', 'Inventory\TransferController@createTransferCE');
    Route::name('create_transfer_ee')->get('transfer/ee/create', 'Inventory\TransferController@createTransferEE');
    Route::name('create_transfer_ec')->get('transfer/ec/create', 'Inventory\TransferController@createTransferEC');
    Route::name('create_transfer_cs')->get('transfer/cs/create', 'Inventory\TransferController@createTransferCS');
    Route::name('create_transfer_cem')->get('transfer/cem/create', 'Inventory\TransferController@createTransferCEM');

    Route::name('get_transfer_cc')->get('transfers/data/cc', 'Inventory\TransferController@getTransfersCC');
    Route::name('get_transfer_ce')->get('transfers/data/ce', 'Inventory\TransferController@getTransfersCE');
    Route::name('get_transfer_ee')->get('transfers/data/ee', 'Inventory\TransferController@getTransfersEE');
    Route::name('get_transfer_ec')->get('transfers/data/ec', 'Inventory\TransferController@getTransfersEC');
    Route::name('get_transfer_cs')->get('transfers/data/cs', 'Inventory\TransferController@getTransfersCS');
    Route::name('get_transfer_cemp')->get('transfers/data/cemp', 'Inventory\TransferController@getTransfersCEMP');
    Route::name('get_transfer_tac')->get('transfers/data/tac', 'Inventory\TransferController@getTransfersTAC');

    Route::name('add_transfer_cc')->post('transfer/add/cc/store','Inventory\TransferController@add_transfer_ss');
    Route::name('add_transfer_ce')->post('transfer/add/ce/store','Inventory\TransferController@add_transfer_se');
    Route::name('add_transfer_ee')->post('transfer/add/ee/store','Inventory\TransferController@add_transfer_ee');
    Route::name('add_transfer_ec')->post('transfer/add/ec/store','Inventory\TransferController@add_transfer_ec');
    Route::name('add_transfer_cs')->post('transfer/addstore/cs','Inventory\TransferController@addTransferCustomerToJobCenter');
    Route::name('add_transfer_cem')->post('transfer/addstore/cem','Inventory\TransferController@addTransferCustomerToEmployee');
    //END TRASPASOS

    //START INVENTORY
    Route::name('index_inventory')->get('inventory/index', 'Inventory\StoreHouseController@index');
    Route::name('get_data_job_centers')->get('get/data/job/centers/{jobcenter?}', 'Inventory\StoreHouseController@getDataJobCenters');
    Route::name('download_excel_detail_store_house')->post('download/excel/detail/store/house', 'Inventory\StoreHouseController@index');
    Route::name('index_movements')->get('inventory/movements/{id}', 'Inventory\StoreHouseController@showMovementsByStorehouse');
    Route::name('index_mov_emp')->get('inventory/movements/emp/{id}', 'Inventory\StoreHouseController@showMovementsByEmployee');
    Route::name('store_adjustment')->post('inventory/store/adjustment', 'Inventory\AdjustmentController@storeAdjustment');
    Route::name('get_data_view_detail_store_house')->get('get/data/view/detail/store/house/{idJobCenter}', 'Inventory\StoreHouseController@getDataViewDetailStoreHouse');
    Route::name('get_data_view_detail_store_house_employee')->get('get/data/view/detail/store/house/employee/{idEmployee}', 'Inventory\StoreHouseController@getDataViewDetailStoreHouseEmployee');
    Route::name('get_data_store_house')->get('get/data/store/house/{idStoreHouse}', 'Inventory\StoreHouseController@getDataStoreHouse');
    Route::name('get_data_store_house_employee')->get('get/data/store/house/employee/{idStoreHouseEmployee}', 'Inventory\StoreHouseController@getDataStoreHouseEmployee');
    Route::name('get_data_view_historical_inventory_job_center')->get('get/data/view/historical/inventory/job/center/{idJobCenter}', 'Inventory\StoreHouseController@getDataHistoricalInventoryJobCenter');
    Route::name('get_data_view_historical_inventory_product_center')->get('get/data/view/historical/inventory/product/center/{idProduct}/{idJobCenter}', 'Inventory\StoreHouseController@getDataHistoricalInventoryProductCenter');
    Route::name('get_data_view_historical_inventory_employee')->get('get/data/view/historical/inventory/employee/{idEmployee}', 'Inventory\StoreHouseController@getDataHistoricalInventoryEmployee');
    Route::name('get_data_view_historical_inventory_product')->get('get/data/view/historical/inventory/product/{idProduct}/{idEmployee}', 'Inventory\StoreHouseController@getDataHistoricalInventoryProduct');
    Route::name('get_data_view_historical_inventory_company')->get('get/data/view/historical/inventory/company/{idCompany}', 'Inventory\StoreHouseController@getDataHistoricalInventoryCompany');
    Route::name('get_data_detail_movements')->get('get/data/detail/movements/{id}', 'Inventory\StoreHouseController@getDataDetailMovements');
    //END INVENTORY

    //START CAJA
    Route::name('cash_index')->get('cash/index','Services\ServiceController@cash_index');
    Route::name('cash_add')->post('cash/add','Services\ServiceController@cash_add');
    Route::name('cash_payment_methods')->get('cash/payment/methods','Services\ServiceController@getPaymentMethods');
    Route::name('download_image')->get('cash/{doc}/download','Services\ServiceController@download');
    Route::name('search_cashes')->get('cash/index/search','Services\ServiceController@cash_index');
    Route::name('payments_selected_data')->post('payments/selected/data', 'Services\ServiceController@getSelectedPayments');
    //END CAJA

    //START INVOICE
    Route::name('invoices_selected_data')->post('invoices/selected/data', 'Services\ServiceController@getSelectedInvoices');
    Route::name('add_invoices')->post('invoices/add','Services\ServiceController@invoicesAdd');
    //END INVOICE

    //START CONTABILIDAD
    Route::name('index_accounting')->get('accounting/index/{jobcenter?}', 'Accounting\AccountingController@index');
    Route::name('load_company')->get('autocomplete/company/purchase', 'Accounting\AccountingController@loadCompany');
    Route::name('autocomplete_company')->get('autocomplete/company', 'Accounting\AccountingController@fetchCompany');

    Route::name('add_purchase')->post('acounting/add/purchase','Accounting\AccountingController@storePurchase');
    Route::name('edit_purchase_order')->get('acounting/edit/purchase/{id}','Accounting\AccountingController@editPurchase');
    Route::name('edit_purchase')->post('acounting/update/purchase','Accounting\AccountingController@updatePurchase');
    Route::name('send_purchase_order')->post('acounting/send/purchase','Accounting\AccountingController@sendEmailPurchaseOrder');
    Route::name('delete_purchase_order')->delete('purchase/order/delete/','Accounting\AccountingController@deletePurchaseOrder');

    Route::name('add_expense')->post('acounting/add/expense','Accounting\AccountingController@storeExpense');
    Route::name('edit_expense')->get('acounting/edit/expense/{id}','Accounting\AccountingController@editExpense');
    Route::name('update_expense')->post('acounting/update/expense','Accounting\AccountingController@updateExpense');
    Route::name('download_archive_accounting')->get('archiveExpense/{id}/{type}/download','Accounting\AccountingController@download');

    Route::name('update_status_account')->post('account/status/update','Accounting\AccountingController@accountStatus');
    Route::name('add_payment')->post('account/payment/status','Accounting\AccountingController@accountPayment');
    Route::name('add_refund')->post('account/refund','Accounting\AccountingController@accountRefund');
    Route::name('my_expense')->get('account/expense/index','Accounting\AccountingController@Expenses');
    //END CONTABILIDAD

    //START CATALOGOS
    Route::name('index_catalogs')->get('catalogos/show/all', 'Catalogs\CatalogController@index');

    //Module Quotations
    Route::name('store_sourceorigin')->post('catalogos/store/source', 'Catalogs\CatalogController@storeSourceOrigin');
    Route::name('store_typeservice')->post('catalogos/store/typeservice', 'Catalogs\CatalogController@storeTypeService');
    Route::name('store_discount')->post('catalogos/store/discount', 'Catalogs\CatalogController@storeDiscount');
    Route::name('store_extra')->post('catalogos/store/extra', 'Catalogs\CatalogController@storeExtra');
    Route::name('store_indications')->post('catalogos/store/indication', 'Catalogs\CatalogController@storeIndication');
    Route::name('store_custom_descriptions')->post('catalogos/store/custom/descriptions', 'Catalogs\CatalogController@storeCustomDescriptions');
    Route::name('store_types_stations')->post('catalogos/store/stations', 'Catalogs\CatalogController@storeStation');
    Route::name('store_list_prices')->get('catalogos/store/list/prices', 'PriceLists\PriceListsController@store');
    Route::name('view_pdf_price')->get('catalogos/store/list/prices/{id}/pdf','PriceLists\PriceListsController@view_pdf');
    Route::name('view_pdf_portada')->get('catalogos/store/list/prices/{id}/portada','PriceLists\PriceListsController@view_portada');
    Route::name('add_pdf_price')->post('catalogos/store/price/portada/pdf','PriceLists\PriceListsController@pdf');
    Route::name('add_pdf_portada')->post('catalogos/store/price/pdf','PriceLists\PriceListsController@add_pdf');
    Route::name('download_pdf_price')->get('catalogos/store/price/pdf/{id}/download','PriceLists\PriceListsController@download');
    Route::name('download_pdf_mip')->get('catalogos/store/mip/pdf/{id}/download','PriceLists\PriceListsController@downloadMip');
    Route::name('export_excel_price')->get('catalogos/store/price/{id}/excel/export','PriceLists\ExcelController@export');
    Route::name('format_excel_price')->get('catalogos/store/price/excel/format','PriceLists\ExcelController@exportFormat');
    Route::name('import_excel_price')->post('catalogos/store/price/excel/import','PriceLists\ExcelController@importUpdate');
    Route::name('import_create_excel_price')->post('catalogos/store/price/excel/import/create','PriceLists\ExcelController@importCreate');
    Route::name('delete_quote_cover_list_price')->post('catalogos/delete/quote/cover/list/price','PriceLists\PriceListsController@deleteQuoteCoverListPrice');
    Route::name('delete_folder_mip_list_price')->post('catalogos/delete/folder/mip/list/price','PriceLists\PriceListsController@deleteFolderMipListPrice');

    Route::name('update_sourceorigin')->post('catalogos/update/source', 'Catalogs\CatalogController@updateSourceOrigin');
    Route::name('update_typeservice')->post('catalogos/update/typeservice', 'Catalogs\CatalogController@updateTypeService');
    Route::name('update_discount')->post('catalogos/update/discount', 'Catalogs\CatalogController@updateDiscount');
    Route::name('update_extra')->post('catalogos/update/extra', 'Catalogs\CatalogController@updateExtra');
    Route::name('update_indications')->post('catalogos/update/indication', 'Catalogs\CatalogController@updateIndication');
    Route::name('update_custom_descriptions')->post('catalogos/update/custom/descriptions', 'Catalogs\CatalogController@updateCustomDescription');
    Route::name('update_stations')->post('catalogos/update/station', 'Catalogs\CatalogController@updateStation');
    Route::name('update_prices')->post('catalogos/update/prices', 'Catalogs\CatalogController@updatePrices');
    Route::name('update_list_prices')->post('catalogos/update/listprice/', 'PriceLists\PriceListsController@update');
    Route::name('change_status_list_prices')->put('listprice/change/status/{id}', 'PriceLists\PriceListsController@changeStatus');

    Route::name('delete_sourceorigin')->delete('catalogos/delete/source/{id}', 'Catalogs\CatalogController@deleteSourceOrigin');
    Route::name('delete_typeservice')->delete('catalogos/delete/typeservice/{id}', 'Catalogs\CatalogController@deleteTypeService');
    Route::name('delete_discount')->delete('catalogos/delete/discount/{id}', 'Catalogs\CatalogController@deleteDiscount');
    Route::name('delete_extra')->delete('catalogos/delete/extra/{id}', 'Catalogs\CatalogController@deleteExtra');
    Route::name('delete_indications')->delete('catalogos/delete/indication/{id}', 'Catalogs\CatalogController@deleteIndication');
    Route::name('delete_custom_descriptions')->delete('catalogos/delete/custom/descriptions/{id}', 'Catalogs\CatalogController@deleteCustomDescription');
    Route::name('delete_stations')->delete('catalogos/delete/station/{id}', 'Catalogs\CatalogController@deleteStation');
    Route::name('delete_list_prices')->delete('catalogos/delete/list/prices/{id}', 'PriceLists\PriceListsController@delete');

    Route::name('list_sourceorigin')->get('catalogos/list/source/{jobcenter?}', 'Catalogs\CatalogController@listSourceOrigin');
    Route::name('list_typeservice')->get('catalogos/list/typeservice/{jobcenter?}', 'Catalogs\CatalogController@listTypeService');
    Route::name('list_discount')->get('catalogos/list/discount/{jobcenter?}', 'Catalogs\CatalogController@listDiscount');
    Route::name('list_extra')->get('catalogos/list/extra/{jobcenter?}', 'Catalogs\CatalogController@listExtra');
    Route::name('list_indications')->get('catalogos/list/indications/{jobcenter?}', 'Catalogs\CatalogController@listIndications');
    Route::name('list_custom_descriptions')->get('catalogos/list/custom/descriptions/{jobcenter?}', 'Catalogs\CatalogController@listCustomDescriptions');
    Route::name('list_prices')->get('catalogos/list/prices/{jobcenter?}', 'PriceLists\PriceListsController@create');
    Route::name('edit_prices')->get('catalogos/edit/listprices/{id}', 'PriceLists\PriceListsController@edit');

    //Module Services
    Route::name('store_methodapp')->post('catalogos/store/methodapp', 'Catalogs\CatalogController@storeMethodApplication');
    Route::name('store_grade')->post('catalogos/store/grade', 'Catalogs\CatalogController@storeGradeInfestation');
    Route::name('store_cleaning')->post('catalogos/store/clenaing', 'Catalogs\CatalogController@storeClenaing');
    Route::name('store_plague')->post('catalogos/store/plague', 'Catalogs\CatalogController@storePlague');
    Route::name('store_plague_category')->post('catalogos/store/plague/category', 'Catalogs\CatalogController@storePlagueCategory');

    Route::name('update_methodapp')->post('catalogos/update/methodapp', 'Catalogs\CatalogController@updateMethodApplication');
    Route::name('update_grade')->post('catalogos/update/grade', 'Catalogs\CatalogController@updateGradeInfestation');
    Route::name('update_cleaning')->post('catalogos/update/clenaing', 'Catalogs\CatalogController@updateClenaing');
    Route::name('update_plague')->post('catalogos/update/plague', 'Catalogs\CatalogController@updatePlague');
    Route::name('update_plague_category')->post('catalogos/update/plague/category', 'Catalogs\CatalogController@updatePlagueCategory');

    Route::name('delete_methodapp')->delete('catalogos/delete/methodapp/{id}', 'Catalogs\CatalogController@deleteMethodApplication');
    Route::name('delete_grade')->delete('catalogos/delete/grade/{id}', 'Catalogs\CatalogController@deleteGradeInfestation');
    Route::name('delete_cleaning')->delete('catalogos/delete/clenaing/{id}', 'Catalogs\CatalogController@deleteClenaing');
    Route::name('delete_plague')->delete('catalogos/delete/plague/{id}', 'Catalogs\CatalogController@deletePlague');
    Route::name('delete_plague_category')->delete('catalogos/delete/plague/category/{id}', 'Catalogs\CatalogController@deletePlagueCategory');

    Route::name('list_methodapp')->get('catalogos/list/methodapp/{jobcenter?}', 'Catalogs\CatalogController@listMethodApplication');
    Route::name('list_grade')->get('catalogos/list/grade/{jobcenter?}', 'Catalogs\CatalogController@listGradeInfestation');
    Route::name('list_cleaning')->get('catalogos/list/clenaing/{jobcenter?}', 'Catalogs\CatalogController@listClenaing');
    Route::name('list_plague')->get('catalogos/list/plagues/{jobcenter?}', 'Catalogs\CatalogController@listPlague');
    Route::name('list_type_stations')->get('catalogos/list/stations/{jobcenter?}', 'Catalogs\CatalogController@listStations');
    Route::name('list_plague_categories')->get('catalogos/list/plague/categories/{jobcenter?}', 'Catalogs\CatalogController@listPlagueCategories');

    //Module Products
    Route::name('store_presentation')->post('catalogos/store/presentation', 'Catalogs\CatalogController@storePresentation');
    Route::name('store_type')->post('catalogos/store/type', 'Catalogs\CatalogController@storeType');
    Route::name('store_units')->post('catalogos/store/units', 'Catalogs\CatalogController@storeUnits');

    Route::name('update_presentation')->post('catalogos/update/presentation', 'Catalogs\CatalogController@updatePresentation');
    Route::name('update_type')->post('catalogos/update/type', 'Catalogs\CatalogController@updateType');
    Route::name('update_units')->post('catalogos/update/units', 'Catalogs\CatalogController@updateUnits');

    Route::name('delete_presentation')->delete('catalogos/update/presentation/{id}', 'Catalogs\CatalogController@deletePresentation');
    Route::name('delete_type')->delete('catalogos/update/type/{id}', 'Catalogs\CatalogController@deleteType');
    Route::name('delete_units')->delete('catalogos/update/units/{id}', 'Catalogs\CatalogController@deleteUnits');

    Route::name('list_presentation')->get('catalogos/list/presentation/{jobcenter?}', 'Catalogs\CatalogController@listPresentation');
    Route::name('list_type')->get('catalogos/list/type/{jobcenter?}', 'Catalogs\CatalogController@listType');
    Route::name('list_units')->get('catalogos/list/units/{jobcenter?}', 'Catalogs\CatalogController@listUnits');

    //Module Accounting
    Route::name('store_paymentmethod')->post('catalogos/store/paymentmethod', 'Catalogs\CatalogController@storePaymentMethod');
    Route::name('store_paymentway')->post('catalogos/store/paymentway', 'Catalogs\CatalogController@storePaymentWay');
    Route::name('store_comprobant')->post('catalogos/store/comprobant', 'Catalogs\CatalogController@storeComprobant');
    Route::name('store_concepts')->post('catalogos/store/concepts', 'Catalogs\CatalogController@storeConcepts');
    Route::name('store_taxes')->post('catalogos/store/taxes', 'Catalogs\CatalogController@storeTaxes');

    Route::name('update_paymentmethod')->post('catalogos/update/paymentmethod', 'Catalogs\CatalogController@updatePaymentMethod');
    Route::name('update_paymentway')->post('catalogos/update/paymentway', 'Catalogs\CatalogController@updatePaymentWay');
    Route::name('update_comprobant')->post('catalogos/update/comprobant', 'Catalogs\CatalogController@updateComprobant');
    Route::name('update_concepts')->post('catalogos/update/concepts', 'Catalogs\CatalogController@updateConcepts');
    Route::name('update_taxes')->post('catalogos/update/taxes', 'Catalogs\CatalogController@updateTaxes');

    Route::name('delete_paymentmethod')->delete('catalogos/delete/paymentmethod/{id}', 'Catalogs\CatalogController@deletePaymentMethod');
    Route::name('delete_paymentway')->delete('catalogos/delete/paymentway/{id}', 'Catalogs\CatalogController@deletePaymentWay');
    Route::name('delete_comprobant')->delete('catalogos/delete/comprobant/{id}', 'Catalogs\CatalogController@deleteComprobant');
    Route::name('delete_concepts')->delete('catalogos/delete/concepts/{id}', 'Catalogs\CatalogController@deleteConcepts');
    Route::name('delete_taxes')->delete('catalogos/delete/taxes/{id}', 'Catalogs\CatalogController@deleteTaxes');

    Route::name('list_paymentmethod')->get('catalogos/list/paymentmethod/{jobcenter?}', 'Catalogs\CatalogController@listPaymentMethod');
    Route::name('list_paymentway')->get('catalogos/list/paymentway/{jobcenter?}', 'Catalogs\CatalogController@listPaymentWay');
    Route::name('list_comprobant')->get('catalogos/list/comprobant/{jobcenter?}', 'Catalogs\CatalogController@listComprobant');
    Route::name('list_concepts')->get('catalogos/list/concepts/{jobcenter?}', 'Catalogs\CatalogController@listConcepts');
    Route::name('list_taxes')->get('catalogos/list/taxes/{jobcenter?}', 'Catalogs\CatalogController@listTaxes');
    // END CATALOGOS

    // PROFILE
    Route::name('edit_user_profile')->get('edit/profile/', 'Profile\UserProfileController@getProfileByUser');
    Route::name('update_user_profile')->post('update/profile/', 'Profile\UserProfileController@updateProfile');
    //

    //START CONFIG NEW INSTANCE
    Route::name('index_config_instance')->get('register/companie/instance', 'Instance\InstanceController@viewConfigStart');
    Route::name('edit_config_instance')->get('edit/companie/instance/', 'Instance\InstanceController@editConfigStart');
    Route::name('add_picture_logo')->post('companie/addpicture/logo', 'Instance\InstanceController@addPictureLogo');
    Route::name('add_pdf_logo')->post('companie/addpdf/logo', 'Instance\InstanceController@addPDFLogo');
    Route::name('add_pdf_sello')->post('companie/addpdf/sello', 'Instance\InstanceController@addPDFSello');
    Route::name('add_pdf_sanitary_license')->post('companie/addpdf/sanitaryLicense', 'Instance\InstanceController@addPDFSanitaryLicense');
    Route::name('add_picture_mini_logo')->post('companie/addpicture/logo/mini', 'Instance\InstanceController@addPictureMiniLogo');
    Route::name('save_config_start')->get('companie/store/data', 'Instance\InstanceController@storeConfigStart');
    Route::name('update_config_start')->post('companie/update/data', 'Instance\InstanceController@updateConfigStart');
    Route::name('update_config_start_tax')->post('companie/update/data/tax', 'Instance\InstanceController@updateConfigStartTax');
    //END CONFIG NEW INSTANCE

    // START CONEKTA
    Route::post('conekta/validate-data','Conekta\ConektaController@validateData');
    Route::name('billing_payment')->get('billing/payment', 'Conekta\ConektaController@index');
    // END CONEKTA

    // START STRIPE
    Route::post('stripe/create/checkout/session','Stripe\StripeController@createCheckoutSession');
    Route::get('stripe/success/session/{session_id}','Stripe\StripeController@successCheckoutSession');
    Route::get('stripe/canceled/session/{session_id}','Stripe\StripeController@canceledCheckoutSession');
    Route::get('stripe/index','Stripe\StripeController@index');
    Route::post('stripe/current/extras/plan','Stripe\StripeController@getCurrentExtrasPlan');
    Route::post('stripe/customer/portal','Stripe\StripeController@customerPortal');
    Route::post('stripe/calculate/price/plan','Stripe\StripeController@getPricePlan');
    // END STRIPE

    //START PERSONAL MAIL
    Route::name('config_mail')->get('config/mail','PersonalmailController@index');
    Route::name('save_config_mail')->post('config/mail/save','PersonalmailController@save');
    //END START PERSONAL MAIL

    //REPORTING MODULE
    Route::name('expenses_total')->get('expenses/total','Reporting\GrossProfitReportingController@getExpensesByFilters');
    Route::name('cost_total')->get('cost/total','Reporting\TotalCostReportingController@total');
    Route::get('filter/jobCenters', 'Reporting\FiltersReportingController@getAllJobCenters');
    Route::get('filter/employees', 'Reporting\FiltersReportingController@getAllEmployees');
    //END REPORTING MODULE


    //START MONITORING
    Route::name('monitoring')->get('monitoring/index', function () {
        $symbolCountry = CommonCompany::getSymbolByCountry();
        return view('vendor.adminlte.monitoring.index')->with(['symbolCountry' => $symbolCountry]);
    });
    //END MONITORING

    //START IMPORT QUOTATION CUSTOM
    Route::name('format_excel_customer_branches')->get('customer/branches/format/excel','TestImportsController@format');
    Route::name('import_excel_customer_branches')->get('customer/branches/import/excel','TestImportsController@import');
    //END IMPORT QUOTATION CUSTOM

    //START CUSTOMER BRANCHES
    Route::name('export_format_branches')->get('customer/branches/export/format/{quotationId}', 'Customers\CustomerBranchesController@exportFormat');
    Route::name('import_excel_branches')->post('customer/branches/import/excel/{customerId}', 'Customers\CustomerBranchesController@importFromExcel');
    Route::name('store_customer_branches')->post('customer/branches/store', 'Customers\CustomerBranchesController@store');
    Route::name('store_order_services')->post('custom/order/services/store', 'Customers\CustomerBranchesController@createServiceOrders');
    Route::name('update_order_service')->post('custom/order/service/update', 'Customers\CustomerBranchesController@updateServiceOrder');
    Route::name('delete_order_service')->post('custom/order/service/delete', 'Customers\CustomerBranchesController@deleteServiceOrders');
    Route::name('update_status_quotation_custom')->post('custom/update/status/quotation', 'Customers\CustomerBranchesController@scheduleCustomQuotation');
    //END CUSTOMER BRANCHES

    //START SCHEDULE INTELLIGENT
    Route::name('schedule_intelligent_branches')->get('schedule/intelligent/branches', 'CustomServices\CustomServicesController@getBranchesByConcept');
    Route::name('schedule_intelligent_first_day')->post('schedule/intelligent/first/day', 'CustomServices\CustomServicesController@scheduleServicesByConcept');
    Route::name('schedule_intelligent_second_day')->post('schedule/intelligent/second/day', 'CustomServices\CustomServicesController@scheduleTwoServicesByConcept');
    Route::name('schedule_intelligent_third_day')->post('schedule/intelligent/third/day', 'CustomServices\CustomServicesController@scheduleServicesByConceptTest');
    Route::name('schedule_intelligent_fourth_day')->post('schedule/intelligent/fourth/day', 'CustomServices\CustomServicesController@scheduleFourthServicesByConcept');
    Route::name('schedule_intelligent_daily_services')->post('schedule/intelligent/daily/services', 'CustomServices\CustomServicesController@scheduleDailyServices');
    Route::name('schedule_intelligent_custom_services')->post('schedule/intelligent/custom/services', 'CustomServices\CustomServicesController@scheduleCustomServices');
    Route::name('schedule_excel_template_export')->get('schedule/excel/template/export', 'CustomServices\CustomServicesController@exportTemplateScheduleServices');
    Route::name('schedule_excel_template_import')->post('schedule/excel/template/import', 'CustomServices\CustomServicesController@importFromExcel');
    Route::name('smart_proposal')->get('schedule/smart/proposal', 'CustomServices\CustomServicesController@getSmartProposal');
    //END SCHEDULE INTELLIGENT

    //START CUSTOMERS
    Route::name('index_customers')->get('customers/view/{jobcenter?}', 'Customers\CustomerController@view');
    Route::name('index_gps')->get('employees/tracking/gps', 'Employees\EmployeeController@showTrackingMap');
    Route::name('gps_last_positions')->get('gps/last/position', 'Employees\EmployeeController@getLastPosition');
    Route::name('gps_tracking')->post('gps/tracking/employee', 'Employees\EmployeeController@getTracking');
    Route::name('customers_change_password')->post('customers/change/password', 'Customers\CustomerController@changePassword');
    Route::name('customers_add_user')->post('customers/add/user', 'Customers\CustomerController@CustomerUser');
    Route::name('customers_edit_customer_branch')->post('customers/edit/customer/branch', 'Customers\CustomerController@editCustomerBranch');
    Route::name('customers_create_customer_main')->post('customers/create/customer/main', 'Customers\CustomerController@createCustomerMain');
    Route::name('customers_edit_customer_main')->post('customers/edit/customer/main', 'Customers\CustomerController@editCustomerMain');
    Route::name('customer_calendar')->get('customer/calendar/{customerId}/{date}', 'Customers\CustomerCalendarController@index');
    Route::name('customer_documents_get')->get('customer/documents/mip/{customerId}', 'Customers\CustomerDocumentationController@getAllDocumentsByCustomerId');
    Route::name('customer_documents_post')->post('customer/documents/create/new', 'Customers\CustomerDocumentationController@store');
    Route::name('customer_delete')->delete('customer/delete','Customers\CustomerController@customerDelete');
    Route::name('customer_branch_delete')->delete('customer/branch/delete','Customers\CustomerController@customerBranchDelete');
    Route::name('services_by_branch')->get('customer/services/by/branch/{branchId}', 'Customers\CustomerController@getServicesByBranchId');
    Route::name('services_customer_by_branch')->get('customer/services/orders/by/branch/{customerId}', 'Customers\CustomerController@getServicesOrdersByCustomerId');
    Route::name('customer_branch_edit')->get('customer/branch/edit/{idCustomerBranch}', 'Customers\CustomerController@getDataCustomerBranch');
    Route::name('update_by_branch')->post('update/services/by/branch', 'Customers\CustomerController@updateServicesByBranch');
    Route::name('get_mail_accounts_by_customer')->get('get/mail/accounts/customer/{customerId}', 'Customers\MailAccountController@getMailAccountsByCustomerId');
    Route::name('create_mail_accounts_by_customer')->post('create/mail/accounts/customer', 'Customers\MailAccountController@addNewMailAccount');
    Route::name('update_mail_accounts_by_customer')->post('update/mail/accounts/customer', 'Customers\MailAccountController@updateMailAccount');
    Route::name('delete_mail_accounts_by_customer')->delete('delete/mail/accounts/customer/{id}', 'Customers\MailAccountController@deleteMailAccount');
    Route::name('customer_documents_separate_mip')->post('customer/documents/separate/mip', 'Customers\CustomerDocumentationController@getSeparateDocumentsByCustomerId');
    //END CUSTOMERS

    //START CUSTOMERS HISTORICAL
    Route::name('index_historical_customer')->get('index/historical/customer/{id}', 'Customers\HistoricalCustomerController@index');
    Route::name('add_commentary_historical_customer')->post('add/commentary/historical/customer', 'Customers\HistoricalCustomerController@addCommentary');
    //END CUSTOMERS HISTORICAL

    //START THEME
    Route::name('change_sidebar_collapse')->get('theme/change/sidebar/collapse', 'Theme\ThemeController@changeSidebarCollapse');
    Route::name('change_introjs_user')->post('theme/change/introjs/user', 'Theme\ThemeController@updateintroJs');
    Route::name('change_intro_upgrade_user')->post('theme/change/intro/upgrade/user', 'Theme\ThemeController@updateintroUpgrade');
    Route::name('get_notifications_nav')->get('get/notifications/nav', 'Theme\ThemeController@getNotifications');
    Route::name('mark_all_read_notifications')->post('mark/all/read/notifications/nav', 'Theme\ThemeController@markAllReadNotifications');
    Route::name('mark_read_notification')->post('mark/read/notification/{notificationId}', 'Theme\ThemeController@markReadNotification');
    Route::name('get_more_notification')->get('get/more/notification/{notificationId}', 'Theme\ThemeController@getNotificationById');
    //END THEME

    //START STATION MONITORING
    Route::name('index_station_monitoring')->get('station/monitoring/index/{jobcenter?}', 'StationMonitoring\StationMonitoringController@show');
    Route::name('station_monitoring_tree_read')->get('station/monitoring/tree/read/{id}','StationMonitoring\StationMonitoringController@read');
    Route::name('station_monitoring_tree_new')->post('station/monitoring/tree/new', 'StationMonitoring\StationMonitoringController@saveTree');
    Route::name('station_monitoring_tree_edit')->post('station/monitoring/tree/edit', 'StationMonitoring\StationMonitoringController@editTree');
    Route::name('station_monitoring_node_area')->get('station/monitoring/area/node/{id}','StationMonitoring\StationMonitoringController@getTypeAreaByNodeId');
    Route::name('station_monitoring_tree_delete')->post('station/monitoring/tree/delete/{id}', 'StationMonitoring\StationMonitoringController@deleteMonitoring');
    Route::name('custom_qr')->post('custom/qr', 'StationMonitoring\CustomQrController@storeCustomQr');
    Route::name('monitoring_print_qr')->get('monitoring/print/qr/{idMonitoring}', 'StationMonitoring\CustomQrController@printQr');
    Route::name('index_inspections_monitoring')->get('inspection/monitoring/index/{jobcenter?}', 'StationMonitoring\InspectionsMonitoringController@show');
    Route::name('reporting_station_monitoring')->get('station/monitoring/reporting', 'StationMonitoring\StationMonitoringController@reporting');
    Route::name('reporting_areas_monitoring')->get('areas/monitoring/reporting', 'StationMonitoring\StationMonitoringController@reportingAreas');
    Route::name('customers_monitoring')->get('station/monitoring/reporting/customers', 'StationMonitoring\StationMonitoringController@getCustomersWithMonitoring');
    Route::name('station_monitoring_tree_create_zone')->post('station/monitoring/tree/create/zone', 'StationMonitoring\StationMonitoringController@createZone');
    Route::name('station_monitoring_tree_create_perimeter')->post('station/monitoring/tree/create/perimeter', 'StationMonitoring\StationMonitoringController@createPerimeter');
    Route::name('station_monitoring_tree_create_station')->post('station/monitoring/tree/create/station', 'StationMonitoring\StationMonitoringController@createStation');
    Route::name('station_monitoring_tree_delete_zone')->post('station/monitoring/tree/delete/zone/{id}', 'StationMonitoring\StationMonitoringController@deleteZoneById');
    Route::name('station_monitoring_tree_delete_perimeter')->post('station/monitoring/tree/delete/perimeter/{id}', 'StationMonitoring\StationMonitoringController@deletePerimeterById');
    Route::name('get_perimeters_all_monitoring')->get('get/perimeters/all/monitoring', 'StationMonitoring\StationMonitoringController@getPerimetersMonitoring');
    Route::name('station_monitoring_tree_relocate_stations_by_perimeter')->post('station/monitoring/tree/relocate/stations/by/perimeter', 'StationMonitoring\StationMonitoringController@relocateStationsByPerimeter');
    Route::name('station_monitoring_tree_cancel_stations_by_perimeter')->post('station/monitoring/tree/cancel/stations/by/perimeter', 'StationMonitoring\StationMonitoringController@cancelStationsByPerimeter');
    Route::name('station_monitoring_tree_loan_stations_by_perimeter')->post('station/monitoring/tree/loan/stations/by/perimeter', 'StationMonitoring\StationMonitoringController@loanStationsByPerimeter');
    Route::name('station_monitoring_tree_delete_stations_by_perimeter_singular')->post('station/monitoring/tree/delete/stations/by/perimeter/singular', 'StationMonitoring\StationMonitoringController@deleteStationsByPerimeterSingular');
    Route::name('station_monitoring_tree_delete_stations_by_perimeter')->post('station/monitoring/tree/delete/stations/by/perimeter', 'StationMonitoring\StationMonitoringController@deleteStationsByPerimeter');
    Route::name('station_monitoring_tree_update_stations_by_perimeter')->post('station/monitoring/tree/update/stations/by/perimeter', 'StationMonitoring\StationMonitoringController@updateStationsByPerimeter');
    Route::name('station_monitoring_tree_update_zone')->post('station/monitoring/tree/update/zone', 'StationMonitoring\StationMonitoringController@updateZoneById');
    Route::name('station_monitoring_tree_update_perimeter')->post('station/monitoring/tree/update/perimeter', 'StationMonitoring\StationMonitoringController@updatePerimeterById');
    Route::name('get_all_tree_by_customer')->get('all/tree/by/customer/{id}','StationMonitoring\StationMonitoringController@getAllTreeByCustomer');
    Route::name('get_all_customer_branch_by_customer')->get('all/customer/branch/by/customer/{id}','StationMonitoring\StationMonitoringController@getAllCustomerBranchByCustomer');
    Route::name('download_report_pdf_qrs')->get('station/monitoring/{id}/download','StationMonitoring\StationMonitoringController@download');
    Route::name('merge_report_pdf_qrs')->get('station/monitoring/{id}/merge','StationMonitoring\StationMonitoringController@mergePartsQrPDF');
    Route::name('station_monitoring_tree_update_date')->post('station/monitoring/tree/update/date/{monitoringId}', 'StationMonitoring\StationMonitoringController@updateDateMonitoring');
    Route::name('station_monitoring_tree_update_customer_branch')->post('station/monitoring/tree/update/customer/branch/{monitoringId}', 'StationMonitoring\StationMonitoringController@updateBranchIdMonitoring');
    Route::name('mail_inspection')->post('mail/inspection','StationMonitoring\InspectionsMonitoringController@mailInspection');
    //END STATION MONITORING

    //START AREA MONITORING
    Route::name('index_area_station_monitoring')->get('area/monitoring/index/{jobcenter?}', 'AreaMonitoring\AreaMonitoringController@index');
    Route::name('area_monitoring_tree_read')->get('area/monitoring/tree/read/{id}','AreaMonitoring\AreaMonitoringController@read');
    Route::name('area_monitoring_tree_edit')->post('area/monitoring/tree/edit', 'AreaMonitoring\AreaMonitoringController@editTree');
    Route::name('area_monitoring_tree_delete')->post('area/monitoring/tree/delete/{id}', 'AreaMonitoring\AreaMonitoringController@deleteTree');
    Route::name('area_monitoring_node_area')->get('area/monitoring/area/node/{id}','AreaMonitoring\AreaMonitoringController@getTypeAreaByNodeId');
    Route::name('station_monitoring_client_tree_new')->post('station/monitoring/client/tree/new', 'AreaMonitoring\AreaMonitoringController@saveTree');
    Route::name('area_print_card_qr')->get('area/print/card/qr/{idArea}', 'AreaMonitoring\AreaMonitoringController@printCardQr');
    Route::name('download_area_pdf_qrs')->get('area/monitoring/{id}/download','AreaMonitoring\AreaMonitoringController@download');
    Route::name('index_inspections_area')->get('inspection/area/index/{jobcenter?}', 'AreaMonitoring\InspectionsAreasController@show');
    Route::name('get_inspections_area')->get('inspections/area/get', 'AreaMonitoring\InspectionsAreasController@getInspections');
    Route::name('get_inspections_detail_area')->get('inspections/detail/area/get/{orderId}', 'AreaMonitoring\InspectionsAreasController@getDetailInspectionByOrderId');
    Route::name('mail_inspection_area')->post('mail/inspection/area','AreaMonitoring\InspectionsAreasController@mailInspectionArea');

    Route::name('area_monitoring_tree_create_zone')->post('area/monitoring/tree/create/zone', 'AreaMonitoring\AreaMonitoringController@createZone');
    Route::name('area_monitoring_tree_create_perimeter')->post('area/monitoring/tree/create/perimeter', 'AreaMonitoring\AreaMonitoringController@createPerimeter');
    Route::name('area_monitoring_tree_create_area')->post('area/monitoring/tree/create/area', 'AreaMonitoring\AreaMonitoringController@createStation');
    Route::name('area_monitoring_tree_delete_zone')->post('area/monitoring/tree/delete/zone/{id}', 'AreaMonitoring\AreaMonitoringController@deleteZoneById');
    Route::name('area_get_all_tree_by_customer')->get('area/all/tree/by/customer/{id}','AreaMonitoring\AreaMonitoringController@getAllTreeByCustomer');
    Route::name('area_monitoring_tree_update_zone')->post('area/monitoring/tree/update/zone', 'AreaMonitoring\AreaMonitoringController@updateZoneById');
    Route::name('area_monitoring_tree_delete_perimeter')->post('area/monitoring/tree/delete/perimeter/{id}', 'AreaMonitoring\AreaMonitoringController@deletePerimeterById');
    Route::name('area_monitoring_tree_update_perimeter')->post('area/monitoring/tree/update/perimeter', 'AreaMonitoring\AreaMonitoringController@updatePerimeterById');
    Route::name('area_monitoring_tree_update_date')->post('area/monitoring/tree/update/date/{monitoringId}', 'AreaMonitoring\AreaMonitoringController@updateDateMonitoring');
    Route::name('get_perimeters_all_monitoring_area')->get('get/perimeters/all/monitoring/area','AreaMonitoring\AreaMonitoringController@getPerimetersArea');
    Route::name('station_area_tree_relocate_stations_by_perimeter')->post('station/area/tree/relocate/stations/by/perimeter', 'AreaMonitoring\AreaMonitoringController@relocateAreasByPerimeter');
    Route::name('station_area_tree_cancel_stations_by_perimeter')->post('station/area/tree/cancel/stations/by/perimeter', 'AreaMonitoring\AreaMonitoringController@cancelAreasByPerimeter');
    Route::name('station_area_tree_delete_stations_by_perimeter_singular')->post('station/area/tree/delete/stations/by/perimeter/singular', 'AreaMonitoring\AreaMonitoringController@deleteAreasByPerimeterSingular');
    //END AREA MONITORING

    //START PLAN INSTANCE
    Route::name('get_plan_user')->get('plan/current/user', 'Instance\InstanceUserController@getCurrentPlan');
    Route::name('purchase_plans')->get('plans/purchase', 'Instance\InstanceUserController@showPlans');
    //END PLAN INSTANCE

    //START PESTWARE ADMIN
    Route::get('pwa/reports/companies/all/{token}', 'PestWareAdmin\ReportsController@getReportAllCompanies');
    Route::get('pwa/import/data/csv', 'PestWareAdmin\ReportsController@importDataTest');
    //END PESTWARE ADMIN

    //START SALES
    Route::name('index_sales')->get('sales/index','Sales\SaleController@index');
    Route::name('save_sale')->post('sales/save', 'Sales\SaleController@store');
    Route::name('get_products_sales')->get('sales/products/get/{storehouseId}', 'Sales\SaleController@getProductsForSale');
    Route::name('data_product_sales')->get('sales/product/data', 'Sales\SaleController@getDataProductForPurchaseOrder');
    Route::name('data_discount_sales')->get('sales/discount/data/{id}', 'Sales\SaleController@getDiscountById');
    Route::name('send_email_sale')->post('send/email/sale','Sales\SaleController@sendEmailSale');
    Route::name('payment_status_sale')->post('payment/status/sale','Sales\SaleController@paymentStatus');
    Route::name('download_voucher_sale')->get('download/voucher/sale/{id}','Sales\SaleController@downloadVoucher');
    Route::name('cancel_sale')->post('cancel/sale','Sales\SaleController@cancelSale');
    //END SALES

    //START COMMON
    Route::name('convert_to_money')->get('convert/to/money/{quantity}', 'Quotations\CustomQuotationController@convertToMoney');
    //END COMMON

    // INVOICES
    Route::name('validate_documents')->post('invoice/validate/documents','Invoices\InvoiceController@loadDocumentsJobCenter');
    Route::name('delete_csd_by_rfc')->delete('invoice/delete/csd/bt/rfc', 'Invoices\InvoiceController@deleteCSDbyRFC');
    Route::name('catalogs_sat_regimens')->get('invoice/catalogs/regimens', 'Invoices\CatalogsSATController@getCatalogsFiscalRegimens');
    Route::name('catalogs_sat_payment_forms')->get('invoice/catalogs/payment/forms', 'Invoices\CatalogsSATController@getCatalogsPaymentForms');
    Route::name('catalogs_sat_payment_methods')->get('invoice/catalogs/payment/methods', 'Invoices\CatalogsSATController@getCatalogsPaymentMethods');
    Route::name('catalogs_sat_type_cfdi')->get('invoice/catalogs/type/cfdi', 'Invoices\CatalogsSATController@getCatalogsTypeCfdi');
    Route::name('catalogs_sat_products_services')->get('invoice/catalogs/products/services/{keyword}', 'Invoices\CatalogsSATController@getProductsOfServicesByKeyword');
    Route::name('catalogs_sat_products_units')->get('invoice/catalogs/products/units/{keyword}', 'Invoices\CatalogsSATController@getUnitsByKeyword');
    Route::name('catalogs_sat_billing_modes')->get('invoice/catalogs/billing/modes', 'Invoices\CatalogsSATController@getBillingModes');
    Route::name('catalogs_bank_accounts')->get('invoice/catalogs/billing/bank/accounts/{customerId}', 'Invoices\CatalogsSATController@getBankAccounts');
    Route::name('add_bank_accounts')->post('invoice/add/billing/bank/accounts', 'Invoices\CatalogsSATController@saveBankAccount');
    Route::name('get_job_centers_folios')->get('invoice/billing/job/centers/folios', 'Invoices\CatalogsSATController@getJobCentersFolios');
    Route::name('get_job_centers_folios_by_id')->get('invoice/billing/job/centers/folios/by/{id}', 'Invoices\CatalogsSATController@getJobCentersFoliosById');
    Route::name('transfer_folios_invoice')->post('invoice/billing/transfer/folios', 'Invoices\CatalogsSATController@transferFolios');
    Route::name('update_is_folios_consumed')->put('invoice/billing/updated/config/folios', 'Invoices\CatalogsSATController@updateIsFoliosConsumed');
    Route::name('update_is_buy_folios')->put('invoice/billing/updated/buy/folios', 'Invoices\CatalogsSATController@updateIsBuyFolios');
    Route::name('purchase_folios_billing')->post('invoice/billing/purchase/folios', 'Invoices\CatalogsSATController@purchaseFolios');

    Route::name('index_billing')->get('billing/invoices/{orderId?}', 'Invoices\BillingController@index');
    Route::name('download_document_invoice')->get('billing/download/invoice/{invoiceId}/{type}', 'Invoices\BillingController@downloadInvoiceById');
    Route::name('cancel_invoice_sat')->delete('billing/cancel/sat/invoice/{invoiceId}', 'Invoices\BillingController@cancelInvoiceById');
    Route::name('send_invoice_mail')->post('billing/send/invoice/mail', 'Invoices\BillingController@sendInvoiceForMail');
    Route::name('complement_invoice')->post('billing/complement/invoice', 'Invoices\BillingController@paymentInvoice');

    Route::name('create_product_billing')->post('billing/custom/create/product', 'Invoices\CustomInvoiceController@createProductService');
    Route::name('create_customer_billing')->post('billing/custom/create/customer', 'Invoices\CustomInvoiceController@createCustomerInvoice');
    Route::name('get_product_billing')->get('billing/custom/get/product/{productId}', 'Invoices\CustomInvoiceController@getProductById');
    Route::name('create_custom_invoice')->post('billing/custom/create/invoice', 'Invoices\CustomInvoiceController@createCustomInvoice');
    Route::name('create_service_invoice')->post('billing/create/invoice/service', 'Invoices\InvoiceController@createInvoiceByOrderId');
    Route::name('calculate_amounts_order')->get('billing/calculate/amounts/service', 'Invoices\InvoiceController@calculateAmountsInvoiceByOrder');
    Route::name('get_data_customer_invoice')->get('billing/get/data/customer/{customerId}', 'Invoices\InvoiceController@getDataCustomerInvoiceByOrder');
    // END INVOICES

    // START MY ACCOUNT
    Route::name('index_invoices_my_account')->get('my/account/invoices', 'MyAccount\MyInvoicesController@index');
    Route::name('download_document_invoice_company')->get('my/account/download/invoice/{invoiceId}/{type}', 'MyAccount\MyInvoicesController@downloadInvoiceById');
    Route::name('store_company_data_billing')->post('my/account/company/data/billing', 'MyAccount\MyInvoicesController@storeDataBilling');
    // END MY ACCOUNT

    // Management
    Route::name('management_general')->get('management/general','Management\ManagementController@index');
    Route::name('add_image_banner')->post('add/image/banner','Management\ManagementController@importBanner');

    // Send Email
    Route::name('import_excel_email')->post('email/massive/excel/import','PriceLists\ExcelController@importEmailMassive');
    Route::name('export_excel_email')->get('email/massive/excel/export','PriceLists\ExcelController@exportEmailMassive');

    // START ATTENDANCES
    Route::name('index_attendances')->get('attendances', 'Attendance\AttendanceController@index');
    Route::name('generate_code_attendance')->post('attendances/generate/code', 'Attendance\AttendanceController@generateCode');
    // END ATTENDANCES


});
