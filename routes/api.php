<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API REST Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::resource('users', 'AdminAPI\UserControllerAPI');
    Route::apiResource('companies', 'AdminAPI\CompanyControllerAPI');
});

#region JSON

Route::group(['prefix' => 'js'], function () {

    //Prefijo para el modulo de servicios.
    Route::group(['prefix' => 'services'], function () {

        /**
         * Ruta para crear un nuevo refuerzo por el id so.
         *
         * @url api/js/services/reinforcement/create/{idOrder}/{date}/{initialHour}/{finalHour}/{user}/{company}
         */
        Route::post('/reinforcement/create/{idOrder}/{date}/{initialHour}/{finalHour}/{user}/{company}', 'Api\ServicesApiController@createReinforcement');

        /**
         * Ruta para realizar el corte por id del técnico.
         *
         * @url api/js/services/cut/create/{user}
         */
        Route::post('/cut/create/{user}', 'Pays\CutController@arqueo');

        /**
         * Ruta para obtener todos los servicios por el Id del técnico y rango de fechas.
         *
         * @url api/js/services/all/employee/{idEmployee}/{serviceStatus}/{serviceType}/{paymentStatus}/{startDate}/{endDate}
         */
        Route::get('/all/employee/{idEmployee}/{serviceStatus}/{serviceType}/{paymentStatus}/{startDate}/{endDate}', 'Api\ServicesApiController@getAllServicesByEmployee');

        /**
         * Ruta para obtener todos los servicios por el Id del técnico y del día actual.
         *
         * @url api/js/services/today/employee/{id}
         */
        Route::get('/today/employee/{id}', 'Api\ServicesApiController@getTodayServicesByEmployee');

        /**
         * Ruta para obtener todos los servicios por el centro de trabajo y una fecha.
         *
         * @url api/js/services/all/day/{date}/{jobCenter}
         */
        Route::get('/all/day/{date}/{jobCenter}/{userId?}', 'Api\ServicesApiController@getAllServicesByDate');

        /**
         * Ruta para obtener todos los servicios por el Id del técnico del día de mañana.
         *
         * @url api/js/services/tomorrow/employee/{id}
         */
        Route::get('/tomorrow/employee/{id}', 'Api\ServicesApiController@getTomorrowServicesByEmployee');

        /**
         * Ruta para obtener todos los servicios por el Id del técnico del día de ayer.
         *
         * @url api/js/services/yesterday/employee/{id}
         */
        Route::get('/yesterday/employee/{id}', 'Api\ServicesApiController@getYesterdayServicesByEmployee');

        /**
         * Ruta para obtener todos los servicios cancelados por el Id del técnico y rango de fechas.
         *
         * @url api/js/services/canceled/employee/{id}/{startDate}/{endDate}
         */
        Route::get('/canceled/employee/{id}/{startDate}/{endDate}', 'Api\ServicesApiController@getCanceledServicesByEmployee');

        /**
         * Ruta para obtener todos los servicios comenzados por el Id del técnico.
         *
         * @url api/js/services/started/employee/{id}
         */
        Route::get('/started/employee/{id}', 'Api\ServicesApiController@getStartedServicesByEmployee');

        /**
         * Ruta para obtener todos los servicios finalizados por el Id del técnico y rango de fechas.
         *
         * @url api/js/services/finished/employee/{id}/{startDate}/{endDate}
         */
        Route::get('/finished/employee/{id}/{startDate}/{endDate}', 'Api\ServicesApiController@getFinishedServicesByEmployee');

        /**
         *  Ruta para obtener todos los métodos de pago
         *
         * @url api/js/services/payment/methods/{idCompany}
         */
        Route::get('payment/methods/{idCompany}', 'Services\AppServicesController@getPaymentMethods');

        /**
         *  Ruta para obtener todos los métodos de pago
         *
         * @url api/js/services/payment/methods
         */
        Route::get('payment/types/{idCompany}', 'Services\AppServicesController@getPaymentTypes');

        /**
         *  Ruta para obtener todos los métodos de aplicación
         *
         * @url api/js/services/application/methods/{idCompany}
         */
        Route::get('application/methods/{idCompany}', 'Services\AppServicesController@getApplicationMethods');

        /**
         *  Ruta para obtener todos los vouchers
         *
         * @url api/js/services/expense/vouchers/{idCompany}
         */
        Route::get('expense/vouchers/{idCompany}', 'Services\AppServicesController@getVouchers');

        /**
         *  Ruta para obtener todos los conceptos para gastos
         *
         * @url api/js/services/expense/concepts/{idCompany}
         */
        Route::get('expense/concepts/{idCompany}', 'Services\AppServicesController@getConceptsForExpense');

        /**
         *  Ruta para obtener todos los orders cleanings
         *
         * @url api/js/services/cleanings/order/{idCompany}
         */
        Route::get('cleanings/order/{idCompany}', 'Services\AppServicesController@getOrderCleanings');

        /**
         *  Ruta para obtener todos los tipos de plaga
         *
         * @url api/js/services/plagues/all/{idCompany}/{type}
         */
        Route::get('plagues/all/{idCompany}/{type}', 'Services\AppServicesController@getPlagues');

        /**
         *  Ruta para obtener todos los grados de infestación.
         *
         * @url api/js/services/grades/infestation/all/{idCompany}
         */
        Route::get('grades/infestation/all/{idCompany}', 'Services\AppServicesController@getGradesInfestation');

        /**
         *  Ruta para obtener un producto por su Id.
         *
         * @url api/js/services/product/{idProduct}
         */
        Route::post('product', 'Services\AppServicesController@getProductById');

        /**
         *  Ruta para obtener todos los productos
         *
         * @url api/js/services/plagues/all/{idCompany}
         */
        Route::get('products/all/{idCompany}/{idEmployee}', 'Services\AppServicesController@getProducts');

        /**
         *  Ruta para actualizar es estatus del service event por su Id.
         *
         * @url api/js/services/event/status/id
         */
        Route::put('event/status/{id}', 'Services\AppServicesController@startServiceWithEvent');
        Route::put('event/checkin/{id}', 'Services\AppServicesController@checkInManual');

        /**
         *  Ruta para guardar la cancelación de un servicio.
         *
         * @url api/js/services/event/cancel
         */
        Route::post('event/cancel', 'Services\AppServicesController@saveCancelService');

        /**
         *  Ruta para guardar imagen en inspection pictures.
         *
         * @url api/js/services/inspection/picture
         */
        Route::post('inspection/picture', 'Services\AppServicesController@saveInspectionPicture');

        /**
         *  Ruta para guardar datos en inspection place.
         *
         * @url api/js/services/inspection/place
         */
        Route::post('inspection/place', 'Services\AppServicesController@savePlaceInspection');

        /**
         *  Ruta para guardar imagen en inspection pictures.
         *
         * @url api/js/services/inspection/picture
         */
        Route::post('condition/picture', 'Services\AppServicesController@saveConditionPicture');

        /**
         *  Ruta para guardar imagen en control plague pictures.
         *
         * @url api/js/services/control/picture
         */
        Route::post('control/picture', 'Services\AppServicesController@saveControlPlaguePicture');

        /**
         *  Ruta para guardar imagen en firmas.
         *
         * @url api/js/services/firm/picture
         */
        Route::post('firm/picture', 'Services\AppServicesController@saveFirmsPicture');

        /**
         *  Ruta para guardar imagen en cash.
         *
         * @url api/js/services/cash/picture
         */
        Route::post('cash/picture', 'Services\AppServicesController@saveCashPicture');

        /**
         *  Ruta para guardar datos en inspection place.
         *
         * @url api/js/services/inspection/place
         */
        Route::post('condition/place', 'Services\AppServicesController@savePlaceCondition');

        /**
         *  Ruta para guardar datos en plague control place.
         *
         * @url api/js/services/control/plgue
         */
        Route::post('control/plague', 'Services\AppServicesController@savePlagueControl');

        /**
         *  Ruta para guardar datos en plague control place.
         *
         * @url api/js/services/control/plgue
         */
        Route::post('save/box', 'Services\AppServicesController@saveBox');

        /**
         *  Ruta para guardar datos en gastos.
         *
         * @url api/js/services/expense/save
         */
        Route::post('expense/save', 'Services\AppServicesController@saveExpense');

        /**
         *  Ruta para guardar datos en inspection place.
         *
         * @url api/js/services/inspection/place/pictures/{idServiceOrder}
         */
        Route::get('inspection/place/pictures/{idServiceOrder}', 'Services\AppServicesController@getPicturesPlaceInspection');

        /**
         *  Ruta para guardar datos en conditins place.
         *
         * @url api/js/services/condition/place/pictures/{idServiceOrder}
         */
        Route::get('condition/place/pictures/{idServiceOrder}', 'Services\AppServicesController@getPicturesPlaceCondition');

        /**
         *  Ruta para guardar datos en conditins plague.
         *
         * @url api/js/services/condition/place/pictures/{idServiceOrder}
         */
        Route::get('control/place/pictures/{idServiceOrder}', 'Services\AppServicesController@getPicturesPlagueControl');

        /**
         *  Ruta para guardar datos en conditins plague.
         *
         * @url api/js/services/condition/place/pictures/{idServiceOrder}
         */
        Route::get('cash/pictures/{idServiceOrder}', 'Services\AppServicesController@getPicturesCash');

        /**
         *  Ruta para guardar mostrar los datos de inspection service.
         *
         * @url api/js/services/inspection/place/show
         */
        Route::get('place/inspection/show/{id}', 'Services\AppServicesController@getPlaceInspection');

        /**
         *  Ruta para guardar mostrar los datos de plague control.
         *
         * @url api/js/services/plague/control/show
         */
        Route::get('plague/control/show/{id}', 'Services\AppServicesController@getControlPlagues');

        /**
         *  Ruta para guardar mostrar los datos de cashes.
         *
         * @url api/js/services/cashes/show
         */
        Route::get('cashes/show/{id}', 'Services\AppServicesController@getBoxPayment');

        /**
         *  Ruta para guardar mostrar la picture sign.
         *
         * @url api/js/services/sign/show
         */
        Route::get('sign/show/{id}', 'Services\AppServicesController@getSignPicture');

        /**
         *  Ruta para guardar mostrar la picture sign.
         *
         * @url api/js/services/sign/show
         */
        Route::get('initial/hour/{id}', 'Services\AppServicesController@getInitialHour');

        /**
         *  Ruta para guardar mostrar la picture sign.
         *
         * @url api/js/services/sign/show
         */
        Route::get('final/hour/{id}', 'Services\AppServicesController@getFinalHour');

        /**
         *  Ruta para mostrar las areas de control por id de orden.
         *
         * @url api/js/services/areascontrol/all/id
         */
        Route::get('areascontrol/all/{id}', 'Services\AppServicesController@getAreasControlById');

        /**
         *  Ruta para mostrar las areas de control por id de orden.
         *
         * @url api/js/services/areascontrol/all/id
         */
        Route::get('areascontrol/pictures/{id}', 'Services\AppServicesController@getPicturesPlagueControlById');

        /**
         *  Ruta para descargar la app apk android
         *
         * @url api/js/services/condition/place/pictures/{idServiceOrder}
         */
        Route::name('download_apk')->get('app/android/download', 'Services\AppServicesController@downloadApp');

        /**
         *  Ruta para enviar reporte pdf por email
         *
         * @url api/js/services/order/sendmail/app
         */
        Route::name('sendmail_customer_service')->get('order/sendmail/app/{email}/{emailInput}/{id}','Services\AppServicesController@mail');

        /**
         *  Ruta para subir foto de evidencias.
         *
         * @url api/js/services/photo/upload
         */
        Route::post('photo/upload', 'Services\AppServicesController@uploadPhoto');

        Route::post('photo/station/upload', 'Services\AppServicesController@uploadPhotoStation');

        Route::post('package/json', 'Api\ServiceJsonController@packageJsonService');

        Route::post('package/json/canceled', 'Api\ServiceJsonController@packageJsonCanceledService');

        Route::post('photo/upload/inspection', 'Api\UploadPhotoController@uploadPhotosInspection');

        Route::post('photo/upload/place/inspection/{id}/{name}', 'Api\UploadPhotoController@uploadPhotoInspection');

        Route::get('photos/all/order/section/{orderId}/{section}', 'Api\UploadPhotoController@getAllPhotosBySectionAndOrderId');

        Route::post('monitoring/station/store/only', 'Api\ServiceJsonController@saveStationInspection');

    });

    //Prefijo para el modulo de refuerzos.
    Route::group(['prefix' => 'reinforcement'], function () {

        /**
         * Ruta para obtener todos los refuerzos por el Id del técnico.
         *
         * @url api/js/reinforcement/all/employee/{id}
         */
        Route::get('/all/employee/{id}/', 'Api\ReinforcementApiController@getAllReinforcementByEmployee');

        /**
         * Ruta para obtener todos los refuerzos por el Id del técnico y del día actual.
         *
         * @url api/js/reinforcement/today/employee/{id}
         */
        Route::get('/today/employee/{id}', 'Api\ReinforcementApiController@getTodayReinforcementByEmployee');

        /**
         * Ruta para obtener todos los refuerzos por el Id del técnico del día de mañana.
         *
         * @url api/js/reinforcement/tomorrow/employee/{id}
         */
        Route::get('/tomorrow/employee/{id}', 'Api\ReinforcementApiController@getTomorrowReinforcementByEmployee');

        /**
         * Ruta para obtener todos los refuerzos cancelados por el Id del técnico.
         *
         * @url api/js/reinforcement/canceled/employee/{id}
         */
        Route::get('/canceled/employee/{id}', 'Api\ReinforcementApiController@getCanceledReinforcementByEmployee');

        /**
         * Ruta para obtener todos los refuerzos comenzados por el Id del técnico.
         *
         * @url api/js/reinforcement/started/employee/{id}
         */
        Route::get('/started/employee/{id}', 'Api\ReinforcementApiController@getStartedReinforcementByEmployee');

        /**
         * Ruta para obtener todos los refuerzos finalizados por el Id del técnico.
         *
         * @url api/js/reinforcement/finished/employee/{id}
         */
        Route::get('/finished/employee/{id}', 'Api\ReinforcementApiController@getFinishedReinforcementByEmployee');

        /**
         * Ruta para obtener la información de los refuerzos.
         *
         * @url api/js/reinforcement/order/id
         */
        Route::get('/order/{id}', 'Services\CalendarController@getInformationByReinforcement');

    });

    //Prefijo para el modulo de garantías.
    Route::group(['prefix' => 'warranty'], function () {

        /**
         * Ruta para obtener todos los garantías por el Id del técnico.
         *
         * @url api/js/warranty/all/employee/{id}
         */
        Route::get('/all/employee/{id}/', 'Api\WarrantyApiController@getAllWarrantyByEmployee');

        /**
         * Ruta para obtener todos los garantías por el Id del técnico y del día actual.
         *
         * @url api/js/warranty/today/employee/{id}
         */
        Route::get('/today/employee/{id}', 'Api\WarrantyApiController@getTodayWarrantyByEmployee');

        /**
         * Ruta para obtener todos los garantías por el Id del técnico del día de mañana.
         *
         * @url api/js/warranty/tomorrow/employee/{id}
         */
        Route::get('/tomorrow/employee/{id}', 'Api\WarrantyApiController@getTomorrowWarrantyByEmployee');

        /**
         * Ruta para obtener todos los garantías cancelados por el Id del técnico.
         *
         * @url api/js/warranty/canceled/employee/{id}
         */
        Route::get('/canceled/employee/{id}', 'Api\WarrantyApiController@getCanceledWarrantyByEmployee');

        /**
         * Ruta para obtener todos los garantías comenzados por el Id del técnico.
         *
         * @url api/js/warranty/started/employee/{id}
         */
        Route::get('/started/employee/{id}', 'Api\WarrantyApiController@getStartedWarrantyByEmployee');

        /**
         * Ruta para obtener todos los garantías finalizados por el Id del técnico.
         *
         * @url api/js/warranty/finished/employee/{id}
         */
        Route::get('/finished/employee/{id}', 'Api\WarrantyApiController@getFinishedWarrantyByEmployee');

        /**
         * Ruta para obtener la información de los garantías.
         *
         * @url api/js/warranty/order/id
         */
        Route::get('/order/{id}', 'Services\CalendarController@getInformationByWarranty');

    });

    //Prefijo para el modulo de seguimientos.
    Route::group(['prefix' => 'tracing'], function () {

        /**
         * Ruta para obtener todos los seguimientos por el Id del técnico.
         *
         * @url api/js/tracing/all/employee/{id}
         */
        Route::get('/all/employee/{id}/', 'Api\TracingApiController@getAllTracingByEmployee');

        /**
         * Ruta para obtener todos los seguimientos por el Id del técnico y del día actual.
         *
         * @url api/js/tracing/today/employee/{id}
         */
        Route::get('/today/employee/{id}', 'Api\TracingApiController@getTodayTracingByEmployee');

        /**
         * Ruta para obtener todos los seguimientos por el Id del técnico del día de mañana.
         *
         * @url api/js/tracing/tomorrow/employee/{id}
         */
        Route::get('/tomorrow/employee/{id}', 'Api\TracingApiController@getTomorrowTracingByEmployee');

        /**
         * Ruta para obtener todos los seguimientos cancelados por el Id del técnico.
         *
         * @url api/js/tracing/canceled/employee/{id}
         */
        Route::get('/canceled/employee/{id}', 'Api\TracingApiController@getCanceledTracingByEmployee');

        /**
         * Ruta para obtener todos los seguimientos comenzados por el Id del técnico.
         *
         * @url api/js/tracing/started/employee/{id}
         */
        Route::get('/started/employee/{id}', 'Api\TracingApiController@getStartedTracingByEmployee');

        /**
         * Ruta para obtener todos los seguimientos finalizados por el Id del técnico.
         *
         * @url api/js/tracing/finished/employee/{id}
         */
        Route::get('/finished/employee/{id}', 'Api\TracingApiController@getFinishedTracingByEmployee');

        /**
         * Ruta para obtener la información de los seguimientos.
         *
         * @url api/js/tracing/order/id
         */
        Route::get('/order/{id}', 'Services\CalendarController@getInformationByTracing');

    });

    //Prefijo para login y session
    Route::group(['prefix' => 'session'], function () {

        /**
         * Ruta para login
         *
         * @url api/js/session/login
         */
        Route::post('/login', 'Auth\LoginController@authenticate');

        /**
         * Ruta para register
         *
         * @url api/js/session/register
         */
        Route::post('/register', 'Auth\RegisterController@registerInstanceApi');

        /**
         * Ruta para obtener el perfil del usuario
         *
         * @url api/js/session/user/profile/{idUser}
         */
        Route::get('/user/profile/{idUser}', 'Auth\LoginController@getProfile');

        /**
         * Ruta para guardar la ruta de la foto del perfil
         *
         * @url api/js/session/user/photo/{idUser}
         */
        Route::post('/user/photo/{idUser}', 'Services\AppServicesController@saveDataPhotoProfile');

        /**
         * Ruta para guardar la ruta de la firma
         *
         * @url api/js/session/user/photo/{idUser}
         */
        Route::post('/user/firm/{idUser}', 'Services\AppServicesController@saveDataPhotoFirmProfile');

        /**
         * Ruta para actualizar el token device firebase FCM
         * @url api/js/session/fcm/token
         */
        Route::post('/fcm/token', 'Api\FirebaseController@postToken');

        /**
         * Ruta para actualizar el token device firebase FCM
         * @url api/js/session/fcm/token
         */
        Route::post('/fcm/notification', 'Api\FirebaseController@sendNotification');

        /**
         * Ruta para cambiar la contraseña del usuario.
         *
         * @url api/js/session/user/change/password
         */
        Route::post('/user/change/password', 'Auth\LoginController@changePasswordEmployee');

        /**
         * Ruta para validar que la app del usuario este actualizada.
         *
         * @url api/js/session/app/version/{code}
         */
        Route::get('/app/version/{code}', 'Services\AppServicesController@validateVersionApp');

    });

    //Prefijo para asistencia
    Route::group(['prefix' => 'attendance'], function () {

        /**
         * Ruta para registrar asistencia.
         *
         * @url api/js/attendance/register
         */
        Route::post('/register', 'Attendance\AttendanceController@store');

    });

    //Prefix for expenses
    Route::group(['prefix' => 'expenses'], function () {

        Route::get('/byUser/{userId}', 'Api\ExpensesApiController@getExpensesByUser');
        Route::post('/create', 'Api\ExpensesApiController@packageJsonExpense');

    });

    //Prefix for inventory
    Route::group(['prefix' => 'inventory'], function () {

        Route::get('/byEmployee/{employeeId}', 'Api\InventoryApiController@getInventoryByEmployee');

    });

    //Prefix for Tools
    Route::group(['prefix' => 'common'], function () {

        Route::get('tools/encode/{id}', 'Api\CommonToolsController@encodeId');
        Route::get('tools/events/massive', 'Api\CommonToolsController@eventsMassive');

    });

    //Prefix for quotations
    Route::group(['prefix' => 'quotation'], function () {

        Route::get('/establishments/{id}', 'Services\AppServicesController@getTypesOfService');
        Route::get('/sources/{id}', 'Services\AppServicesController@getSourcesOrigin');
        Route::get('/technicians/{jobCenterId}', 'Services\AppServicesController@getTechniciansByJobCenter');
        Route::post('/create', 'Api\ExpressQuoteApiController@store');

    });

    //Prefix for monitoring
    Route::group(['prefix' => 'monitoring'], function () {

        Route::get('/conditions/{idCompany}', 'Api\MonitoringApiController@getConditions');
        Route::get('/tree/{idMonitoring}', 'Api\MonitoringApiController@getMonitoringTree');
        Route::get('/inspections/customers/{technicianId}', 'Api\InspectionsApiController@getCustomersInspections');
        Route::get('/inspections/customer/{customerId}', 'Api\InspectionsApiController@getInspectionsByCustomer');
        Route::get('/inspection/{inspectionId}', 'Api\InspectionsApiController@getInspectionById');

    });

    //Prefix for area inspections
    Route::group(['prefix' => 'area'], function () {

        Route::post('/inspections', 'Api\AreaInspectionsController@store');
        Route::get('/validate/qr/{nodeId}/{employeeId}', 'Api\AreaInspectionsController@validateQrArea');
        Route::post('/inspections/photo', 'Services\AppServicesController@uploadPhotoArea');
        Route::get('/inspections/customers/by/{jobCenterId}', 'Api\AreaInspectionsController@getCustomersWithAreasByJobCenterId');
        Route::get('/inspections/areas/tree/{areaId}', 'Api\AreaInspectionsController@getTreeAreasById');
        Route::get('/inspections/areas/orders/{customerId}', 'Api\AreaInspectionsController@getOrdersByCustomerNow');

    });

    //Prefix for reporting
    Route::group(['prefix' => 'reporting'], function () {

        Route::post('/quotation/total', 'Reporting\QuotationReportingController@getAllQuotationsByFilter');
        Route::post('/grossSales/total', 'Reporting\GrossSalesReportingController@getAllEventsByFilter');
        Route::post('/grossIncome/total', 'Reporting\GrossIncomeReportingController@getAllPaymentsByFilter');
        Route::post('/global/total', 'Reporting\FiltersReportingController@globalTotal');
        Route::post('/rating/services', 'Reporting\SatisfactionIndexController@getAllRatingsByFilter');
        Route::post('/stations/monitoring', 'Reporting\MonitoringReportingController@getAllInspectionsByFilter');

        Route::post('upload/chart/image', 'StationMonitoring\StationMonitoringController@uploadImageChart');
        Route::get('report/stations/baits/{customerId}/{nameImage}/{period}', 'StationMonitoring\ChartsReportController@reportStationBaits');
        Route::get('report/stations/all/{customerId}/{nameImage}/{period}', 'StationMonitoring\ChartsReportController@reportAllStations');

    });

    //Prefix for reporting
    Route::group(['prefix' => 'portal'], function () {

        Route::get('/customer/{customerId}/{year}', 'Api\CustomerPortalApiController@getFolderMIP');
        Route::get('/mips/{customerId}', 'Api\CustomerPortalApiController@getYearsMIP');
        // test
        Route::get('/customer/mip/license/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipLicense');
        Route::get('/customer/mip/analisis/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipAnalisis');
        Route::get('/customer/mip/policy/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadInsurancePolicy');
        Route::get('/customer/mip/plan/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipPlanWork');
        Route::get('/customer/mip/process/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipProcess');
        Route::get('/customer/mip/normas/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipNormas');
        Route::get('/customer/mip/capacitation/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipCapacition');
        Route::get('/customer/mip/orders/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipOrders');
        Route::get('/customer/mip/services/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipCertificates');
        Route::get('/customer/mip/inspections/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipInspections');
        Route::get('/customer/mip/products/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipProducts');
        Route::get('/customer/mip/calendar/{customerId}/{year}', 'Api\CustomerPortalApiController@downloadMipCalendar');
    });

    //Prefix for mails
    Route::group(['prefix' => 'mail'], function () {

        Route::get('/notification/instances', 'Api\ServiceApiMailController@notification');

    });

    Route::name('index_catalogs')->put('update/orders/paymentWay/byCompany/{companyId}/{old}/{new}', 'Catalogs\CatalogController@updatePaymentWayMatchChange');
    Route::name('index_catalogs')->put('update/customers/match/data', 'Customers\CustomerController@matchCustomersData');
    Route::name('update_customers_mip')->put('update/customers/match/docs/mip', 'Customers\CustomerController@matchCustomerDocumentation');
    Route::name('update_historical_customers')->post('update/historical/customers/match/data', 'Customers\CustomerController@matchHistoricalCustomersData');
    Route::name('match_url_photos')->post('match/url/photos', 'Customers\CustomerController@matchUrlPhotos');
    Route::name('legend_price_list')->post('legend/price/list', 'Customers\CustomerController@legendPriceList');
    Route::name('subtotal_service_order')->post('subtotal/service/order/{id}', 'TestImportsController@subtotalServiceOrder');
    Route::name('instance_sicom_monitoring')->post('instance/sicom/monitoring/{id}', 'TestImportsController@instanceSicom');
    Route::name('send_notification_app_update')->post('notification/fcm/update/app', 'Customers\CustomerController@sendNotificationUpdateApp');
    Route::post('update/stations/types/by/companie', 'StationMonitoring\StationMonitoringController@createBaseStations');
    Route::put('reset/storehouse/all', 'StationMonitoring\StationMonitoringController@updateStorehouse');

    // Invoices
    Route::group(['prefix' => 'invoices'], function () {
        Route::post('/create', 'Invoices\InvoiceController@createInvoiceForEachOrder');
        Route::delete('/cancel', 'Invoices\InvoiceController@cancelInvoice');
        Route::post('/catalogs/test', 'Invoices\InvoiceController@testCatalogsFacturama');
        Route::get('/download/{invoiceId}', 'Invoices\InvoiceController@downloadInvoice');
    });

});

#endregion
