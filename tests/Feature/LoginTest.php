<?php

namespace Tests\Feature;

use Tests\TestCase;

class LoginTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     *
     * @test
     */
    public function an_user_can_loading_login() {
        // Url
        $this->get('login')
            ->assertStatus(200)
            ->assertSee('Bienvenido');
    }
}
