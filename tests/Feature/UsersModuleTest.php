<?php

namespace Tests\Feature;

use Tests\TestCase;

class UsersModuleTest extends TestCase
{

    /**
     * A login module test.
     *
     * @test
     */
    public function itLoadsTheUsersPage()
    {
        $this->get('login')
            ->assertStatus(200)
            ->assertSee('Bienvenido');
    }

    /**
     *  A user can be created
     *
     * @test
     */
    public function a_user_can_be_created()
    {
        $this->withExceptionHandling();

        $this->assertDatabaseHas('users', [
            'name' => 'Administrador'
        ]);
    }

}
