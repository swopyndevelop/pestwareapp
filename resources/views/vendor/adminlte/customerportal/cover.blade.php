<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carpeta </title>
</head>
<style>
    body{
        font-family:'sans-serif';
    }
    @page  {
        margin-right: 0;
        margin-bottom: 0;
        margin-top: 0;
    }
    .column {
        float: left;
        width: 50%;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
</style>
<body>
<div class="row">
    <div class="column">
        @if($isHeader)
        <p><span style="font-weight: bold">Nombre:</span> {{$cliente->name}}</p>
        <p><span style="font-weight: bold">Celular:</span> {{$cliente->cellphone}}</p>
        <p><span style="font-weight: bold">Dirección:</span> {{$cliente->address}} {{$cliente->address_number}}, {{$cliente->colony}}, {{$cliente->municipality}}, {{$cliente->state}}</p>
        <p><span style="font-weight: bold">E-mail:</span> {{$cliente->email}}</p>
        @endif
        <br><br><br><br><br><br><br>
        <img src="{{$pdf_logo}}" alt="" width="97%">
    </div>
    <div class="column" align="center" style="background-color: {{ $cover_color }}">
        <div style="height: 98%;">
            <br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br>
            <br>
            <h1 style="font-size: 30pt; font-weight:bold; color:white">{{ $title }}</h1>
            <p style="font-size: 25pt; color:white">{{ $subtitle }}</p>
            <br><br><br><br><br><br><br>
            <p style="font-size: 15pt; color:white">{{ $description }}</p>
            <br><br><br><br><br><br><br><br><br>
            @if($isHeader)
            <p style="font-size: 12pt; color:white">Fecha de Elaboración: {{$fecha}}</p>
            <p style="font-size: 12pt; color:white">Elaborado por: {{$elaborado->responsable}}</p>
            @endif
            <br>
        </div>
    </div>
</div>
</body>
</html>