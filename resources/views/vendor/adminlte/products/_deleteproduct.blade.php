@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="deleteP" role="dialog" aria-labelledby="cancelModal" tabindex="-1" >
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
              <form action="{{ Route('delete_product') }}" method="POST" id="form" enctype="multipart/form-data">
                {{ csrf_field() }}
              <div class="box-body">
                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Eliminar Producto</h4>
                <div class="row">
                    <div class="col-md-12 text-center">
                       <h4 style="color: black">¿Estas seguro de eliminar esta producto?</h4>
                        <input type="hidden" name="product" id="product" value="{{$product}}">
                    </div>
                </div>
              <div class="modal-footer">
                <div class="text-center">
                  <div class="row">
                    <a href="{{Route('index_product')}}" class="btn btn-primary btn-sm">Cancelar</a>
                    <button class="btn btn-danger btn-sm" type="submit" id="DeleteBtn" name="DeleteBtn">Elminar</button>
                </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
  <script>
    //$(".plagueList-single").select2();
    $("#deleteP").modal();
    $("#deleteP").on('hidden.bs.modal', function() {
      window.location.href = '{{route("index_product")}}'; //using a named route
    });
  </script>
@endsection
