@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="content container-fluid spark-screen" id="courseevaluation-CompanyEvaluation">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="titleProducts">Productos</h3>
                    <input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary-dark" data-toggle="modal" href="#newP" id="newProduct"> <span class="fa fa-plus"></span> Agregar Producto</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalListPrice"> <span class="fa fa-list"></span> Lista de Precios</button>
                                </div>
                                <div class="col-md-4 margin-mobile">
                                    <select name="selectJobCenterProducts" id="selectJobCenterProducts" class="form-control" style="font-weight: bold;">
                                        @foreach($jobCenters as $jobCenter)
                                            @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                                <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                                    {{ $jobCenter->name }}
                                                </option>
                                            @else
                                                <option value="{{ $jobCenter->id }}">
                                                    {{ $jobCenter->name }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 margin-mobile">
                                    {!!Form::open(['method' => 'GET','route'=>'product_search'])!!}
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar por: (Nombre)" name="search" id="search">
                                        <span class="input-group-btn">
											<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
										</span>
                                    </div>
                                    {!!Form::close()!!}
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table tablesorter" id="tableProduct">
                                        <thead class="table-general" id="thTableProducts">
                                            <tr>
                                                <td style="font-weight:bold">Nombre</td>
                                                <td style="font-weight:bold">Tipo</td>
                                                <td style="font-weight:bold">Descripción</td>
                                                <td style="font-weight:bold">Características</td>
                                                <td style="font-weight:bold">Uso Sugerida</td>
                                                <td style="font-weight:bold">Presentación</td>
                                                <td style="font-weight:bold">Cantidad<br>/envase</td>
                                                <td style="font-weight:bold">Precio Base</td>
                                                <td style="font-weight:bold">Unidad<br>/envase</td>
                                                <td style="font-weight:bold">Documentación</td>
                                                <td style="font-weight:bold">Fecha<br>De Revisión</td>
                                                <td style="font-weight:bold">Venta</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $a = 0; ?>
                                        @foreach ($products as $product)
                                        <?php $a = $product->id; ?>
                                            <tr>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                            {{$product->producto}} <span class="caret"></span>
                                                        </a>
                                                        @include('vendor.adminlte.products._menu')
                                                    </div>
                                                </td>
                                                <td>{{$product->tipo}}</td>
                                                <td>
                                                    @php
                                                        $resultado = $product->description;
                                                        $r1 = substr($resultado, 0,85);
                                                        echo $r1."...";
                                                        
                                                    @endphp
                                                </td>
                                                <td>
                                                    @php
                                                        $resultado2 = $product->characteristics;
                                                        $r2 = substr($resultado2, 0,85);
                                                        echo $r2."...";
                                                    @endphp
                                                </td>
                                                <td>{{$product->suggested_use}}</td>
                                                <td>{{$product->presentation}}</td>
                                                <td>{{$product->quantity}}</td>
                                                <td>{{ $symbol_country }}{{$product->base_price}} <br>
                                                    @if($product->sale_price)
                                                    {{ $symbol_country }}{{ $product->sale_price }}
                                                    @endif
                                                </td>
                                                <td>{{$product->unit}}</td>
                                                
                                                <td>
                                                    @foreach ($product->productsFile as $productFile)
                                                        <a href="{{Route('download_product',$productFile->id)}}" title="{{$productFile->original_name}}">
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 1.4em;color:red"></i>
                                                        </a>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    {{ \Carbon\Carbon::parse($product->productsFileDate)->format('d/m/Y') }} <br>
                                                    {{ \Carbon\Carbon::parse($product->productsFileDate)->format('H:i') }}
                                                </td>
                                                <td>
                                                    @if($product->is_available == 1)
                                                    <span style="color: green">Si</span>
                                                    @else <span style="color: red">No</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{$products->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('vendor.adminlte.products._newproduct')
@include('vendor.adminlte.products._listPrices')
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/products.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/inventory.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $('#selectJobCenterProducts').on('change', function() {
            let idJobCenterProduct = $(this).val();
            window.location.href = route('index_product', idJobCenterProduct);
        });
        $("#taxes").select2();
    </script>
@endsection