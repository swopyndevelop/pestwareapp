<!--START REGISTER PRODUCTS-->
<div class="container-fluid spark-screen">
    <div class="modal fade" aria-hidden="true" id="newP" role="dialog" aria-labelledby="newProductModal">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document" style="width: 90%;">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">

                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Nuevo Producto</h4>
                            </div>

                            <form action="{{ Route('save_product') }}" method="POST" id="formNewProduct" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="box-body">

                                    <div class="row container-fluid">

                                        <div class="col-md-4" id="leftProductDiv">

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__tipo">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Tipo de producto*: </label>
                                                    <select name="tipo" id="tipo" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                        <option value="0" disabled selected>Seleccione un tipo de producto</option>
                                                        @foreach($types as $type)
                                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__nombre">
                                                    <label style="font-weight: bold;">Nombre del Producto*:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="nombre" id="nombre" placeholder="Ejemplo: Cirano">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">El nombre debe tener de 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__description">
                                                    <label style="font-weight: bold;">Descripción:</label>
                                                    <div class="">
                                                        <textarea name="description" id="description" cols="10" rows="5" class="formulario__input form-control" placeholder="Ejemplo: Insecticida de uso urbano para uso exclusivo de aplicadores..."></textarea>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">La descripción debe ser de 3 a 5000 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__adjetive">
                                                    <label style="font-weight: bold;">Características:</label>
                                                    <div class="">
                                                        <textarea name="adjetive" id="adjetive" cols="10" rows="8" class="formulario__input form-control"
                                                                  placeholder="Ejemplo: Este insecticida afecta los canales de sodio (Na+) en la membrana nerviosa provocando una...">
                                                        </textarea>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Las características deben ser de 3 a 5000 caracteres.</p>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__uso">
                                                    <label style="font-weight: bold;">Uso Sugerido:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="uso" id="uso" placeholder="Ejemplo: Mantenimiento Residencial - Jardines - Exteriores.">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">La descripción del messenger debe ser de 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__presentacion">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Presentación:</label>
                                                    <select name="presentacion" id="presentacion" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                        <option value="0" disabled selected>Seleccione la unidad correspondiente</option>
                                                        @foreach($presentation as $p)
                                                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__unidad" id="grupo__unidad">
                                                    <label class="control-label" for="unidad" style="font-weight: bold;">Unidad por Envase:</label>
                                                    <select name="unidad" id="unidad" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                        <option value="0" disabled selected>Seleccione un tipo de producto</option>
                                                        @foreach($unit as $u)
                                                            <option value="{{ $u->id }}">{{ $u->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__cantidad">
                                                    <label style="font-weight: bold;">Cantidad por envase:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="number" step="any" name="cantidad" id="cantidad" placeholder="Ejemplo: 15.00">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo números con decimal</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__ingredient">
                                                    <label style="font-weight: bold;">Ingrediente Activo:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="ingredient" id="ingredient" placeholder="Ejemplo: Cipermetrina.">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">La descripción debe ser de 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__ingredient">
                                                    <label for="" class="control-label" style="font-weight: bold;">Documentación:</label>
                                                    <input type="file" name="information[]" id="information" multiple="">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__ingredient">
                                                    <label for="" class="control-label" style="font-weight: bold;">Imágen del Producto:</label>
                                                    <input type="file" name="image" id="image" accept="image/png, image/jpeg, image/jpg">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4" id="rightProductDiv">

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__register">
                                                    <label style="font-weight: bold;">Registro Plaguicida:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="register" id="register" placeholder="Ejemplo: Registro 2013V-0005062.">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">La descripción debe ser de 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__precio">
                                                    <label style="font-weight: bold;">Precio Base:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="number" step="any" name="precio" id="precio" placeholder="Ejemplo: $100.00">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo números con decimal</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__precio">
                                                    <label style="font-weight: bold;">Precio Venta (sin impuestos):</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="number" step="any" name="sale_price" id="sale_price" placeholder="Ejemplo: $100.00">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo números con decimal</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__taxes">
                                                    <label class="control-label" style="font-weight: bold;" for="taxes">Impuestos:</label>
                                                    <select name="taxes[]" id="taxes"
                                                            class="form-control select__input"
                                                            style="width: 100%; font-weight: bold;" multiple="multiple">
                                                        @foreach($taxes as $tax)
                                                            <option value="{{ $tax->id }}">{{ $tax->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group" @if($company->id_code_country != 2 || Auth::user()->companie != 37) style="display: none;" @endif>
                                                <div class="col-sm-8 formulario__grupo" id="grupo__unit_code_billing">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Asocia tu producto al cátalogo del SAT:</label>
                                                    <input type="text" class="form-control" name="productCodeSat" id="productCodeSat">
                                                    <input type="hidden" name="productCodeSatId" id="productCodeSatId">
                                                    <div id="productCodeSatList"></div>
                                                </div>
                                                <div class="col-sm-4 formulario__grupo" id="grupo__unit_code_billing">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                    <a href="http://200.57.3.89/PyS/catPyS.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Clave</a>
                                                </div>
                                            </div>

                                            <div class="row form-group" @if($company->id_code_country != 2 || Auth::user()->companie != 37) style="display: none;" @endif>
                                                <div class="col-sm-8 formulario__grupo" id="grupo__unit_code_billing">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Define la unidad de tu producto</label>
                                                    <input type="text" class="form-control" name="productUnitSat" id="productUnitSat">
                                                    <input type="hidden" name="productUnitSatId" id="productUnitSatId">
                                                    <div id="productUnitSatList"></div>
                                                </div>

                                                <div class="col-sm-4 formulario__grupo" id="grupo__unit_code_billing">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                    <a href="http://200.57.3.89/PyS/catUnidades.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Unidad</a>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__unit_code_billing">
                                                    <input type="checkbox" class="form-check-input" name="is_available" id="is_available">
                                                    <span style="font-weight: bold;">Producto Disponible (Venta)</span>
                                                </div>
                                            </div>

                                        </div>
                                </div>
                            </div><hr>
                            <div class="text-center">

                                <div class="formulario__mensaje" id="formulario__mensaje">
                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                </div>
                                <br>
                                <div class="text-center formulario__grupo formulario__grupo-btn-enviar">
                                   <button type="submit" class="btn btn-primary btn" id="btnSaveProduct">Guardar Producto</button>
                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se guardaron los datos correctamente.</p>
                                </div>

                            </div>
                                <br>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
