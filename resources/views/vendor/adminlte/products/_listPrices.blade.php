<!-- INICIA MODAL PARA MOSTRAR DETALLE DE LISTAS DE PRECIO -->
<div class="modal fade" id="modalListPrice" tabindex="-1">
  <div class="modal-body modal-dialog" style="width: 70% !important; text-align: center">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="col-md-10">
          <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Lista de Precios</h4>
        </div>
        <div class="col-md-2">
          <button type="button" class="btn btn-secondary" id="exportListPrice">Exportar Excel</button>
        </div>
        <hr>
        <div class="row" style="margin: 10px;">
          <div class="col-md-12 table-responsive">
            <table class="table table-sorter table-hover text-center" id="tableListPrices">
              <thead>
              <tr class="small">
                <th>Nombre</th>
                <th>Precio Base</th>
                <th>Impuestos</th>
                <th>Precio Venta <br> Sin impuestos</th>
                <th>Precio Venta <br> Con impuestos</th>
              </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL LISTAS DE PRECIO-->