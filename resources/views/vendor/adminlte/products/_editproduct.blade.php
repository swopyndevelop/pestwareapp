@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!--START EDIT PRODUCTS-->
<div class="container-fluid spark-screen">
        <div class="modal fade" id="EditP" role="dialog" aria-labelledby="newProductModal">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal-dialog modal-lg" role="document" style="width: 90%;">
                        <div class="modal-context">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Actualizar Producto</h4>
                                </div>
                                <br>
                                <form action="{{ Route('update_product') }}" method="POST" id="form" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                <div class="box-body">

                                    <div class="row container-fluid">

                                        <div class="col-md-4">

                                            <input type="hidden" name="id_product" id="id_product" value="{{$product->id_product}}">

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Tipo de Producto*:</label>
                                                    <select name="tipo" id="tipo" class="form-control" style="width:100%; font-weight:bold" required>
                                                        @foreach($types as $type)
                                                            <option value="{{ $type->id }}"
                                                                    @if ($product->id_type_product == $type->id)
                                                                    selected
                                                                    @endif
                                                                    {{ (in_array($type, old('types', []))) }} > {{$type->name}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Nombre del Producto*:</label>
                                                    <input type="text" name="nombre" id="nombre" class="form-control" required value="{{$product->producto}}">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Descripción:</label>
                                                    <textarea name="description" id="description" cols="10" rows="5" class="form-control" required>{{$product->description}}</textarea>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="adjetive" class="control-label">Características:</label>
                                                    <textarea name="adjetive" id="adjetive" cols="10" rows="7" class="form-control" required>{{$product->characteristics}}</textarea>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Uso Sugerida:</label>
                                                    <input type="text" name="uso" id="uso" class="form-control" required value="{{$product->suggested_use}}">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Presentación:</label>
                                                    <select name="presentacion" id="presentacion" class="form-control" style="width:100%; font-weight:bold" required>
                                                        @foreach($presentation as $p)
                                                            <option value="{{ $p->id }}"
                                                                    @if ($product->id_presentation == $p->id)
                                                                    selected
                                                                    @endif
                                                                    {{ (in_array($p, old('presentation', []))) }}> {{ $p->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Unidad por Envase:</label>
                                                    <select name="unidad" id="unidad" class="form-control" style="width:100%; font-weight:bold" required>
                                                        @foreach($unit as $u)
                                                            <option value="{{ $u->id }}"
                                                                    @if ($product->unidad == $u->id)
                                                                    selected
                                                                    @endif
                                                                    {{ (in_array($u, old('unit', []))) }} >{{ $u->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Cantidad por envase:</label>
                                                    <input type="number" name="cantidad" id="cantidad" class="form-control" required step="any"  value="{{$product->quantity}}">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Ingrediente Activo:</label>
                                                    <input type="text" name="ingredient" id="ingredient" class="form-control" value="{{$product->active_ingredient}}">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Documentación:</label>
                                                    <input type="file" name="information[]" id="information[]" multiple="">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Imágen del Producto:</label>
                                                    <input type="file" name="image" id="image" accept="image/png, image/jpeg, image/jpg">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Registro Plaguicida:</label>
                                                    <input type="text" name="register" id="register" class="form-control" value="{{$product->register}}">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Precio Base:</label>
                                                    <input type="number" name="precio" id="precio" class="form-control" required step="any" value="{{$product->base_price}}">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <label for="" class="control-label">Precio Venta (sin impuestos):</label>
                                                    <input type="number" name="sale_price" id="sale_price" class="form-control" step="any" value="{{$product->sale_price}}">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__taxes">
                                                    <label class="control-label" style="font-weight: bold;" for="taxes">Impuestos:</label>
                                                    <select name="taxes[]" id="taxes"
                                                            class="form-control select__input"
                                                            style="width: 100%; font-weight: bold;" multiple="multiple">
                                                        @foreach($taxes as $tax)
                                                            <option value="{{ $tax->id }}"
                                                                    @foreach($taxProducts as $taxProduct)
                                                                    @if($taxProduct->id_tax == $tax->id)
                                                                    selected='selected'
                                                                    @endif
                                                                    @endforeach
                                                                    {{ (in_array($tax, old('taxes', []))) }} >{{ $tax->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group" @if($company->id_code_country != 2 || Auth::user()->companie != 37) style="display: none;" @endif>
                                                <div class="col-sm-8 formulario__grupo" id="grupo__unit_code_billing">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Asocia tu producto al cátalogo del SAT:</label>
                                                    <input type="text" class="form-control" name="productCodeSat" id="productCodeSat" @if($productCodeSat != null) value="{{ $productCodeSat->Value }} - {{ $productCodeSat->Name }}" @endif>
                                                    <input type="hidden" name="productCodeSatId" id="productCodeSatId" @if($productCodeSat != null) value="{{ $productCodeSat->Value }}" @endif>
                                                    <div id="productCodeSatList"></div>
                                                </div>
                                                <div class="col-sm-4 formulario__grupo" id="grupo__unit_code_billing">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                    <a href="http://200.57.3.89/PyS/catPyS.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Clave</a>
                                                </div>
                                            </div>

                                            <div class="row form-group" @if($company->id_code_country != 2 || Auth::user()->companie != 37) style="display: none;" @endif>
                                                <div class="col-sm-8 formulario__grupo" id="grupo__unit_code_billing">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Define la unidad de tu producto</label>
                                                    <input type="text" class="form-control" name="productUnitSat" id="productUnitSat" @if($productUnitSat != null) value="{{ $productUnitSat->key }} - {{ $productUnitSat->name }}" @endif>
                                                    <input type="hidden" name="productUnitSatId" id="productUnitSatId" @if($productUnitSat != null) value="{{ $productUnitSat->id }}" @endif>
                                                    <div id="productUnitSatList"></div>
                                                </div>

                                                <div class="col-sm-4 formulario__grupo" id="grupo__unit_code_billing">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                    <a href="http://200.57.3.89/PyS/catUnidades.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Unidad</a>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    @if($product->is_available == 0)
                                                        <input type="checkbox" class="form-check-input" name="is_available" id="is_available">
                                                    @else
                                                        <input type="checkbox" class="form-check-input" name="is_available" id="is_available" checked>
                                                    @endif
                                                    <span style="font-weight: bold;">Producto Disponible (Venta)</span>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <br>
                                    <hr>
                                    <button type="submit" class="btn btn-primary" name="EditProduct" id="EditProduct" style="margin-bottom: 5px">Actualizar Producto</button>
                                    <br>
                                    <br>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script>
        $(".plagueList-single").select2();
        $("#EditP").modal();
        $("#EditP").on('hidden.bs.modal', function() {
            window.location.href = '{{route("index_product")}}'; //using a named route
        });
        $("#taxes").select2();
    </script>
    <script>
        const productCodeSat = document.getElementById('productCodeSat');
        const productUnitSat = document.getElementById('productUnitSat');
        let token = $("meta[name=csrf-token]").attr("content");

        productCodeSat.onkeyup = function (ev) {
            let keyword = document.getElementById('productCodeSat').value;
            findKeysProductsSat(keyword);
        }

        productUnitSat.onkeyup = function (ev) {
            let keyword = document.getElementById('productUnitSat').value;
            findUnitsProductsSat(keyword);
        }

        function findKeysProductsSat(keyword) {
            if (keyword !== '') {
                $.ajax({
                    type: 'GET',
                    url: route('catalogs_sat_products_services', keyword),
                    data: {
                        _token: token
                    },
                    success: function(data) {
                        let productCodeSatList = $('#productCodeSatList');
                        productCodeSatList.fadeIn();
                        productCodeSatList.html(data);
                    }
                });
            }
        }

        function findUnitsProductsSat(keyword) {
            if (keyword !== '') {
                $.ajax({
                    type: 'GET',
                    url: route('catalogs_sat_products_units', keyword),
                    data: {
                        _token: token
                    },
                    success: function(data) {
                        let productUnitSatList = $('#productUnitSatList');
                        productUnitSatList.fadeIn();
                        productUnitSatList.html(data);
                    }
                });
            }
        }

        $(document).on('click', 'li#getdataProductKey', function() {
            let all = $(this);
            document.getElementById('productCodeSatId').value = all.attr('data-id');
            let productCodeSat = $('#productCodeSat');
            let productCodeSatList =  $('#productCodeSatList');
            productCodeSat.val(all.text());
            productCodeSatList.fadeOut();
        });

        $(document).on('click', 'li#getdataProductUnit', function() {
            let all = $(this);
            document.getElementById('productUnitSatId').value = all.attr('data-id');
            let productUnitSat = $('#productUnitSat');
            let productUnitSatList =  $('#productUnitSatList');
            productUnitSat.val(all.text());
            productUnitSatList.fadeOut();
        });
    </script>
@endsection
