<!-- INICIA MODAL PARA MOSTRAR FICHA DEL PRODUCTO -->
<div class="modal fade" id="ficha<?php echo $a; ?>" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Ficha de Producto</h4>
				<div class="row">
					<div class="col-md-6">
						<p style="font-weight: bold; color: black; font-size: 18px">{{ $product->producto }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $product->tipo }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px; white-space: normal;">{{ $product->active_ingredient }}</p>
					</div>
					<div class="col-md-6 text-right">
						<label style="font-weight: bold; color: black;">Presentación:</label> {{ $product->presentation }}
						<br>
						<label style="font-weight: bold; color: black;">Cantidad por Envase:</label> {{ $product->quantity }} {{ $product->unit }}
						<br>
						<label style="font-weight: bold; color: black;">Precio Base:</label> {{ $symbol_country }}{{ $product->base_price }}
						<br>
						<label style="font-weight: bold; color: black;">Fecha de Alta:</label> {{ $product->fecha }}
					</div>
				</div>
				<div class="row" style="margin: 2px;">
					<hr>
					<label style="font-weight: bold; color: black; font-size: 18px">Descripción</label>
					<p style="color: black; font-size: 14px; white-space: normal;">{{ $product->description }}</p>
					<br>
					<label style="font-weight: bold; color: black; font-size: 18px">Características</label>
					<p style="color: black; font-size: 14px; white-space: normal;">{{ $product->characteristics }}</p>
					<br>
					<label style="font-weight: bold; color: black; font-size: 18px">Uso Sugerido</label>
					<p style="color: black; font-size: 14px; white-space: normal;">{{ $product->suggested_use }}</p>
					<label style="font-weight: bold; color: black; font-size: 18px">Impuestos</label>
					@foreach($product->taxes as $tax)
					<p style="color: black; font-size: 14px; white-space: normal;">{{ $tax->name }}: {{ $tax->value }}%</p>
					@endforeach
					<div class="row">
						<div class="col-md-6">
							<h3 style="font-weight: bold; color: black; font-size: 18px">Documentación</h3>
							<div class="row" style="margin: 10px;">
								@foreach ($product->productsFile as $productFile)
										<a href="{{Route('download_product',$productFile->id)}}" title="{{$productFile->original_name}}">
											<i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 2em;color:red"></i>
										</a>
								@endforeach
							</div>
						</div>
						<div class="col-md-6 text-center">
							<h3 style="font-weight: bold; color: black; font-size: 18px">Producto</h3>
							<img src="{{ env('URL_STORAGE_FTP').$product->image_product }}" alt="{{ $product->producto }}" width="300">
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR FICHA DEL PRODUCTO-->