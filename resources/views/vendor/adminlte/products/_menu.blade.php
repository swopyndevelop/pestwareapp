<ul class="dropdown-menu">
	<li><a data-toggle="modal" style="cursor: pointer" data-target="#ficha<?php echo $a; ?>"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a></li>
    <li><a href="{{ Route('edit_product', \Vinkla\Hashids\Facades\Hashids::encode($product->id_product)) }}"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i>Editar</a></li>
    <li><a href="{{ Route('vdelete_product', \Vinkla\Hashids\Facades\Hashids::encode($product->id_product)) }}"><i class="fa fa-trash" aria-hidden="true" style="color: red;"></i>Eliminar</a></li>
</ul>

@include('vendor.adminlte.products._modalFicha')