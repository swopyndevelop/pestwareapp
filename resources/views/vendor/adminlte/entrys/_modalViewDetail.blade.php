<!-- INICIA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA -->
<div class="modal fade" id="entryDetail<?php echo $a; ?>" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<input type="hidden" id="idEntryModal" name="idEntryModal" value="<?php echo $a; ?>">
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle de la Entrada</h4>
				<div class="row container-fluid">
					<div class="col-md-6" id="leftModalEntryDiv">
						<p style="font-weight: bold; color: black; font-size: 18px">{{ $ent->id_entry }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $ent->date }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $ent->name }}</p>
					</div>
					<div class="col-md-6" id="rightModalEntryDiv">
						<label style="font-weight: bold; color: black;">Origen:</label> {{ $ent->origin }}
						<br>
						<label style="font-weight: bold; color: black;">Destino:</label> {{ $ent->destinity }}
						<br>
						<label style="font-weight: bold; color: black;">Total:</label> {{ $symbol_country }}{{ $ent->total }}
					</div>
				</div>
				<div class="row" style="margin: 10px;" id="tableDetailEntryDiv">
					<div class="col-md-12 table-responsive">
                        <table class="table tablesorter" id="tableProduct">
                            <thead id="thModalProductsDiv">
                                <tr>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Cantidad Envase</th>
                                    <th class="text-center">Unidades Recibidas</th>
                                    <th class="text-center">Precio Base</th>
                                    <th class="text-center">Precio Factura</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                               	@foreach($productsEntry as $p)
                               		@if($p->id_entry == $ent->id)
	                                    <tr>
	                                        <td class="text-center">{{ $p->name }}</td>
	                                        <td class="text-center">{{ $p->quantity }} {{ $p->unit }}</td>
	                                        <td class="text-center">{{ $p->units }}</td>
	                                        <td class="text-center">{{ $symbol_country }}{{ $p->base_price }}</td>
	                                        <td class="text-center">{{ $symbol_country }}{{ $p->price }}</td>
	                                        <td class="text-center">{{ $symbol_country }}{{ $p->price * $p->units }}</td>
	                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					@foreach ($factura as $f)
                        @if ($f->id_entry == $ent->id)
                            <a href="{{Route('download_facture_entry', $f->id)}}">
                                <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 3.5em;color:red"></i>
                            </a>
                        @endif
                    @endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA-->