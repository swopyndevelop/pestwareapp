@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen" id="entry">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title" id="titleEntries">Entradas</h3>
                        <input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">
                        <input type="hidden" id="idEntry" name="idEntry" value="{{1}}">
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2">
                                        <a type="submit" class="btn btn-primary-dark" href="{{ route('create_entry') }}" id="EntryInventoryButton"> <span class="fa fa-plus"></span> Agregar Entrada</a>
                                    </div>
                                    <div class="col-lg-5">
                                        <select name="selectJobCenterInventoryEntries" id="selectJobCenterInventoryEntries" class="form-control" style="font-weight: bold;">
                                            @foreach($jobCenters as $jobCenter)
                                                @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                                    <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                                        {{ $jobCenter->name }}
                                                    </option>
                                                @else
                                                    <option value="{{ $jobCenter->id }}">
                                                        {{ $jobCenter->name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-5 margin-mobile">
                                        {!!Form::open(['method' => 'GET','route'=>'index_entry'])!!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Buscar (Origen)" name="search" id="search">
                                            <span class="input-group-btn">
											<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
										</span>
                                        </div>
                                        {!!Form::close()!!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table tablesorter table-hover" id="tableProduct">
                                            <thead class="table-general" id="tableEntriesDiv">
                                            <tr>
                                                <th># Entrada</th>
                                                <th>Fecha/Usuario</th>
                                                <th>Origen</th>
                                                <th>Destino</th>
                                                <th>Unidades<br>Recibidas</th>
                                                <th>Total Factura</th>
                                                <th>Factura PDF</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php $a = 0; ?>
                                                @foreach($entry as $ent)
                                                <?php $a = $ent->id; ?>
                                                    <tr>
                                                        <td>
                                                            <div class="btn-group" id="ulEntryDiv">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                                    {{ $ent->id_entry }} <span class="caret"></span>
                                                                </a>
                                                                @include('vendor.adminlte.entrys._menu')
                                                            </div>
                                                        </td>
                                                        <td>{{ $ent->date }} <br> {{ $ent->name }}</td>
                                                        <td>
                                                            {{ $ent->origin }}
                                                            @if($ent->id_purchase != 0)
                                                                <br>
                                                                <a href="{{ route('index_accounting') }}" target="_blank"><strong style="color: #103195;">{{ $ent->purchase->id_purchase_order }}</strong></a>
                                                            @endif
                                                        </td>
                                                        <td>{{ $ent->destinity }}</td>
                                                        <td>{{ $ent->tot_unit }}</td>
                                                        <td>{{ $symbol_country }}{{ $ent->total }}</td>
                                                        <td>
                                                            @foreach ($factura as $f)
                                                                @if ($f->id_entry == $ent->id)
                                                                    <a href="{{Route('download_facture_entry',$f->id)}}" target="_blank">
                                                                        <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 1.5em;color:red"></i>
                                                                    </a>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    {{ $entry->links() }}
                                    <!-- TODO: Add link paginate -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/inventory.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
@endsection
