@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen" id="entry">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title" id="titleCreateEntry">Nueva Entrada</h3>
                        <input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method="post" id="insert_form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <label for="origin">Orden de compra</label>
                                <input type="number" hidden id="purchaseOrderId" name="purchaseOrderId" value="0">
                                <select name="purchasesOrder" id="purchasesOrder" class="form-control">
                                    <option value="0" selected>Entrada sin orden de compra</option>
                                    @foreach($purchasesOrder as $purchase)
                                        <option value="{{ $purchase->id }}">
                                            {{ $purchase->id_purchase_order }} ({{ $purchase->provider }})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3" id="sourceCreateEntry">
                                <label for="origin">Origen</label>
                                <input class="form-control" name="origin" id="origin">
                            </div>

                            <div class="col-md-3" id="destinyCreateEntry">
                                <label for="origin">Destino</label>
                                <select name="selectJobCenters" id="selectJobCenters" class="jobCentersList-single form-control" style="width: 100%; font-size: small !important;">
                                    <option value="0" disabled="" selected="">Seleccione una opción</option>
                                    @foreach($jobCenters as $jobCenter)
                                        <option value="{{ $jobCenter->id }}">
                                            {{ $jobCenter->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2" id="billCreateEntry">
                                <label for="total">Total Factura</label>
                                <p style="font-weight: bold; font-size: large;" id="total">00.00</p>
                            </div>
                        </div>
                        <h5 class="text-danger">NOTA: LAS ENTRADAS SE DEBEN REALIZAR EN UNIDADES.</h5>
                        <div class="row col-md-12">
                            <div class="col-md-12 table-responsive">
                                <span id="error"></span>
                                <table class="table table-striped table-hover" id="item_table">
                                    <thead class="table-general">
                                        <tr>
                                            <th>Producto <button type="button" name="addProduct" data-toggle="modal" data-target="#newP" class="btn btn-secondary btn-sm addProduct" id="addNewProduct"><span class="glyphicon glyphicon-plus"></span></button></th>
                                            <th>Cantidad Envase</th>
                                            <th>Unidades Recibidas</th>
                                            <th>Precio</th>
                                            <th>Precio Factura</th>
                                            <th><button type="button" name="add" class="btn btn-primary-dark btn-sm add" id="addProduct"><span class="glyphicon glyphicon-plus"></span></button></th>
                                        </tr>
                                    </thead>
                                </table>
                                <br><br>
                                <label class="control-label">Adjuntar Factura:</label>
                                <input type="file" name="facture[]" id="facture[]" multiple="">
                                <hr>
                                <button type="button" class="btn btn-primary" style="float:right" id="btnSaveEntry">Guardar</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('vendor.adminlte.products._newproduct')
@endsection
@section('personal-js')

    <script type="text/javascript" src="{{ URL::asset('js/build/entry.js') }}"></script>

    <script>
        $(".productsList-single").select2();
        $(".jobCentersList-single").select2();
        $("#taxes").select2();
    </script>

@endsection