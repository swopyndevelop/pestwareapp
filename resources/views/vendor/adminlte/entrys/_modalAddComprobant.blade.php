<!-- INICIA MODAL PARA AGREGAR COMPROBANTE -->
<div class="modal fade" id="entryAddComprobant<?php echo $a; ?>" tabindex="-1">
	<div class="modal-dialog modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<form id="add_pdf" method="POST" enctype="multipart/form-data" action="{{Route('add_pdf_comprobant')}}">
					{{ csrf_field() }}
				<div class="modal-body text-center">
					<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Factura</h4>
					<input type="hidden" id="idEntryAddComprobant" name="idEntryAddComprobant" value="<?php echo $a; ?>">
					<div class="container-fluid text-center">
						<label for="comprobantInput" class="h5">Adjuntar Factura</label>
						<input type="file" class="form-control" name="comprobantInput" id="comprobantInput" accept="application/pdf" required>
					</div>
				</div>
				<div class="footer text-center">
					<button type="submit" class="btn btn-primary" style="margin-bottom: 5px">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA AGREGAR COMPROBANTE-->