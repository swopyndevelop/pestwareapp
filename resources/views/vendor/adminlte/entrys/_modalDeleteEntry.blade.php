<!-- INICIA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA -->
<div class="modal fade" id="entryDelete" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="col-md-6">
					<h4 class="modal-title text-center text-info" id="modalTitle">Eliminar Entrada</h4>
				</div>
				<div class="col-md-6">
					<h4 class="modal-title text-center text-info" id="labelEntry"></h4>
				</div>
				<div class="row container-fluid text-center">
					<p>Al eliminar la entrada no se descontará el producto del Inventario Teórico para ello necesitarás ir a hacer el ajuste por salida.</p>
				</div>

				<div class="footer">
					<div class="text-center">
						<button class="btn btn-primary" id="btnDeleteEntry">Eliminar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA-->