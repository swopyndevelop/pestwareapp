<ul class="dropdown-menu">
	<li><a data-toggle="modal" data-target="#entryDetail<?php echo $a; ?>" style="cursor:pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a></li>
	<li><a data-toggle="modal" data-target="#entryAddComprobant<?php echo $a; ?>" style="cursor:pointer"><i class="fa fa-file" aria-hidden="true" style="color: #222d32;"></i>Agregar Factura</a></li>
	@if(Entrust::can('Eliminar Datos') || Entrust::hasRole('Cuenta Maestra'))
	<li><a data-toggle="modal" data-target="#entryDelete"
		   data-id="{{\Vinkla\Hashids\Facades\Hashids::encode($a)}}"
		   data-entry="{{$ent->id_entry}}"
		   style="cursor:pointer"><i class="fa fa-trash" aria-hidden="true" style="color: #222d32;">
			</i>Eliminar Entrada</a></li>
	@endif
</ul>
@include('vendor.adminlte.entrys._modalViewDetail')
@include('vendor.adminlte.entrys._modalAddComprobant')
@include('vendor.adminlte.entrys._modalDeleteEntry')