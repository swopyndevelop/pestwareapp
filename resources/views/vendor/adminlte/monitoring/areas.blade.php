@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <section class="content">
        @if(Auth::user()->companie == 338)
            <iframe width="100%" height="850"
                    src="https://datastudio.google.com/embed/reporting/549e703e-625c-4a5f-aee1-389222cb702f/page/0A0fC"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
        @endif
    </section>
@endsection
