@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<input type="number" hidden value="{{ auth()->user()->companie }}" id="companieMonitoring">
<section class="content">
	<div id="InitApp">
	</div>
	<input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">
	<input type="hidden" id="symbolCountry" value="{{ $symbolCountry }}">
</section>

@endsection

@section('personal-js')
	<script type="text/javascript" src="{{ URL::asset('js/build/inventory.js') }}"></script>
@endsection
