@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <section class="content">
        @if(Auth::user()->companie == 338)
            <iframe width="100%" height="850"
                    src="https://datastudio.google.com/embed/reporting/add856ba-ad2c-4a74-9f79-07e08b4f3559/page/LUYgC"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
        @elseif(Auth::user()->companie == 371)
            <iframe width="100%" height="850"
                    src="https://datastudio.google.com/embed/reporting/90f4377f-9bf7-4095-ae70-e814ff014920/page/LUYgC"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
        @elseif(Auth::user()->companie == 187)
            <iframe width="100%" height="850"
                    src="https://datastudio.google.com/embed/reporting/7184a02e-aa35-4eaa-9c18-7c97ad5415e4/page/LUYgC"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
        @elseif(Auth::user()->companie == 503)
            <<iframe width="100%" height="850"
                     src="https://lookerstudio.google.com/embed/reporting/fe171517-89f3-48c8-a3d4-94e57a82abfb/page/LUYgC"
                     frameborder="0" style="border:0" allowfullscreen></iframe>
        @else
            <input type="number" hidden value="{{ Auth::user()->companie }}" id="companyMonitoring">
            <div id="StationsCharts">
            </div>
        @endif
    </section>
@endsection
