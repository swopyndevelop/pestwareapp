<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Corte de Caja</title>
</head>
<style>
    @page {
        margin: 0;
    }
    body{
        font-family: 'Montserrat'
    }
    table {
        width: 100%;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
    }
    tbody {
    }
    th{
        text-align: center;
    }
    td{
        padding: 0.3em;
    }
</style>
<body>

<div style="margin-left:25px;margin-right:25px; margin-top:100px;">
    <h4 style="color:black;font-weight:bold">DESGLOSE DE VENTA TOTAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GASTOS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONSUMO</h4>
    <div style="width:300px;line-height:0.5em;float:left;font-size:9pt">
        <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL EFECTIVO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->total_efec}}</span></p>
        <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL TRANSFERENCIA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            @if($cortes->sub_transf == null)
                <span style="color:black;font-weight:unset">{{ $symbol_country }}0</span>
            @else
                <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->sub_transf}}</span>
            @endif
        </p>
        <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL DEPÓSITO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            @if($cortes->sub_depos == null)
                <span style="color:black;font-weight:unset">{{ $symbol_country }}0</span>
            @else
                <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->sub_depos}}</span>
            @endif
        </p>
        <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL CHEQUE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            @if($cortes->sub_cheq == null)
                <span style="color:black;font-weight:unset">{{ $symbol_country }}0</span>
            @else
                <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->sub_cheq}}</span>
            @endif
        </p>
        <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL TARJETA DÉBITO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            @if($cortes->sub_deb == null)
                <span style="color:black;font-weight:unset">{{ $symbol_country }}0</span>
            @else
                <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->sub_deb}}</span>
            @endif
        </p>
        <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL TARJETA CRÉDITO:&nbsp;&nbsp;&nbsp;</span>
            @if($cortes->sub_cred == null)
                <span style="color:black;font-weight:unset">{{ $symbol_country }}0</span>
            @else
                <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->sub_cred}}</span>
            @endif
        </p>
        <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL ADEUDOS:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            @if($cortes->total_adeudo == null)
                <span style="color:black;font-weight:unset">{{ $symbol_country }}0</span>
            @else
                <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->total_adeudo}}</span>
            @endif
        </p>
        <p style="color:black;font-weight:bold">VENTA TOTAL:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="font-weight:bold;color:black">{{ $symbol_country }}{{$cortes->total}}</span></p>
        <table>
        </table>
    </div>

    <div style="margin-left:400px;margin-right:400px;float:center;font-size:9pt;line-height:0.5em;">
        <table>
            <thead>
            <tr>
                <th>Gasto</th>
                <th>Metodo</th>
                <th>Importe</th>
            </tr>
            </thead>
            <tbody>
            @foreach($expenses as $expense)
                <tr>
                    <td style="text-align: center">{{ $expense->expense_name }}</td>
                    <td style="text-align: center">{{ $expense->method }}</td>
                    <td style="text-align: center;">{{ $symbol_country }}{{ $expense->total }}</td>
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td style="border-bottom: 1px solid black"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <th style="text-align: center">{{ $symbol_country }}{{ $expenseTotal }}</th>
            </tr>
            </tbody>
        </table>
    </div>
    <div style="float:right;width:400px;line-height:0.5em;font-size:9pt">
        <table>
            <thead>
            <tr>
                <th>Producto/Equipo</th>
                <th>Consumo</th>
                <th>Inventario</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sum as $p2)
                @if($p2->usuario == $cortes->user_id)
                    <tr>
                        <td style="text-align: center">{{$p2->name}}</td>
                        <td style="text-align: center">{{$p2->sumx}} {{ $p2->unit }}s</td>
                        <td style="text-align: center">{{ $p2->stock_other_units }} {{ $p2->unit }}s</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<br><br><br><br><br><br><br>

<div style="margin-left:25px;margin-right:25px; margin-top:100px;">
    <h4 style="color:black;font-weight:bold">BALANCE DE EFECTIVO</h4>
    <div style="width:400px;line-height:0.5em;float:left;font-size:9pt">
        <p style="color:black;font-weight:bold;border-bottom: 3px solid black;">TOTAL DE VENTAS EN EFECTIVO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->total_efec}}</span>&nbsp;&nbsp;&nbsp;&nbsp; <b>DIFERENCIA</b>
        </p>
        <p style="color:black;font-weight:bold;">TOTAL DE EFECTIVO RECIBIDO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->sub_efec}}</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>{{ $symbol_country }}{{ $effectiveDiff }}</b>
        </p>
        <p style="color:black;font-weight:bold;border-bottom: 1px solid black; width: 300px;">TOTAL DE GASTOS EN EFECTIVO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span style="color:red;font-weight:unset">- {{ $symbol_country }}{{$expenseTotal}}</span>
        </p>
        <p style="color:black;font-weight:bold;">SALDO DE EFECTIVO AL CORTE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span style="font-weight:bold;color:black">{{ $symbol_country }}{{$effectiveTotal}}</span>
        </p>
        <table>
        </table>
    </div>
</div>

<br><br><br><br><br>

<div style="margin-left:25px;margin-right:25px; margin-top:100px;">
    <h4 style="color:black;font-weight:bold"></h4>
    <div style="width:400px;line-height:0.5em;float:left;font-size:9pt">
        <p style="color:black;font-weight:bold;border-bottom: 3px solid black;">TOTAL DE VENTAS EN EFECTIVO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span style="color:black;font-weight:unset">{{ $symbol_country }}{{$cortes->total_efec}}</span>&nbsp;&nbsp;&nbsp;&nbsp; <b>DIFERENCIA</b>
        </p>
    </div>
</div>

</body>
</html>