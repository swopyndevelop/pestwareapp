<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Entrega</title>
</head>
<style>
    @page {
        margin: 0;
    }
    body{
        font-family: 'Montserrat'
    }
</style>
<body>
    <div>
        <div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
            <img src="img/Biofin_logo.png" alt="logo" width="15%">
        </div>
    </div>
    <br><br><br><br><br><br><br><br>
    <div style="padding-left:40px;padding-right:40px">
        <p style="text-align:justify">
            A través de este documento yo <span style="font-weight:bold">{{$tecnico->name}}</span>, entrego la cantidad de <span style="font-weight:bold">${{$cortes->total_arqueo}} MXN </span>(<span>{{$moneda}} MEXICANOS</span>) 
        a la orden de <span style="font-weight:bold">Saúl Esparza Romo</span>. Dicha cantidad es entregada el día 
        <span style="font-weight:bold">{{$fecha1}}</span>.
        </p>
        <p style="text-align:center">
            Firmado en: <span style="font-weight:bold">Aguascalientes</span>, el {{$fecha1}}
        </p>
        <br><br>
    </div>
    <div style="float:left;padding-left:-400px">
            <div style="line-height:0.7em;font-size:11pt">
                    <p style="text-align:center">
                            <span style="font-weight:bold">FIRMA DIRECTOR:</span>
                            <p style="text-align: center">_________________________</p>
                            <p style="text-align: center">Saúl Esparza Romo</p>
                        </p>  
        
            </div>
    </div>
    <div style="padding-left:350px">
            <div style="font-size:11pt;line-height:0.7em;">
                    <p style="text-align:center">
                        <span style="font-weight:bold">FIRMA TÉCNICO:</span>
                        <p style="text-align: center">_________________________</p>
                        <p style="text-align: center">{{$tecnico->name}}</p>
                    </p>  
                </div>
    </div>
    
    
</body>
</html>