@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen content" id="courseevaluation-CompanyEvaluation">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Cortes</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <select name="selectJobCenterCutsGeneral" id="selectJobCenterCutsGeneral" class="form-control" style="font-weight: bold;">
                                @foreach($jobCenters as $jobCenter)
                                    @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                        <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                            {{ $jobCenter->name }}
                                        </option>
                                    @else
                                        <option value="{{ $jobCenter->id }}">
                                            {{ $jobCenter->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-6 margin-mobile">
                            {!!Form::open(['method' => 'GET','route'=>'cuts_admin'])!!}
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar por: (Técnico)" name="search" id="search" value="">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                </span>
                            </div>
                            {!!Form::close()!!}
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 style="text-align:center;color:black">Cortes Realizados</h3>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table tablesorter" id="tableProduct">
                                        <thead class="table-general">
                                            <tr style="font-weight: bold">
                                                <td># Corte</td>
                                                <td>Fecha/Hora</td>
                                                <td>Técnico</td>
                                                <td>Servicios <br> Programados</td>
                                                <td>Servicios <br> Realizados</td>
                                                <td>Venta Total</td>
                                                <td>Total Efectivo</td>
                                                <td>Total Otras Formas</td>
                                                <td>Total CxC</td>
                                                <td>Total Consumo</td>
                                                <td>Estatus</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($cortes as $c)
                                                <tr>
                                                    <td style="color:red;font-weight:bold;font-size:10pt">
                                                        <div class="btn-group">
                                                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                                CD-{{ $c->id}} <span class="caret"></span>
                                                            </a>
                                                            @include('vendor.adminlte.cuts._menu2')
                                                        </div>
                                                    </td>
                                                    <td><?php echo date("d-m-Y",strtotime($c->date))?><br><?php echo date("H:i",strtotime($c->time))?></td>
                                                    <td>{{$c->tecnico}}</td>
                                                    <td class="text-center">{{$c->programs}}</td>
                                                    <td class="text-center">{{$c->realize}}</td>
                                                    <td class="text-center">{{ $symbol_country }}{{$c->total}}</td>
                                                    @if($c->total_efec == null)
                                                        <td class="text-center">{{ $symbol_country }}0</td>
                                                    @else
                                                        <td class="text-center"> {{ $symbol_country }} {{$c->total_efec}}</td>
                                                    @endif
                                                    <td class="text-center">{{ $symbol_country }}{{$c->tot_other}}</td>
								                    <td class="text-center">{{ $symbol_country }}{{$c->tot_credit}}</td>
                                                    <td>
                                                        <a href="{{Route('detail_cons2',$c->id)}}">Ver Consumo</a>
                                                    </td>
                                                    <td class="text-center">
                                                        @if($c->depositado == 1)
                                                            <span style="color:red;font-weight:bold">Depositado</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <br><br><br><br>
                                </div>
                                {{ $cortes->links() }}
                                <br><br><br>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('personal-js')
    <script>
        $('#selectJobCenterCutsGeneral').on('change', function() {
            let idJobCenterCutsGeneral = $(this).val();
            window.location.href = route('cuts_admin', idJobCenterCutsGeneral);
        });
    </script>
@endsection