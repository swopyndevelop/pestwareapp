<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <title>Corte de Caja</title>
</head>
<style>
    @page {
        margin: 0;
    }
    body{
        font-family: 'Montserrat'
    }
    table {
        width: 100%;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
    }
    tbody {
    }
    th{
        text-align: center;
    }
    td{
        padding: 0.3em;
    }
</style>
<body>
    <div>
        <div style="float:left;padding-left:25px;padding-right:40px;padding-top:15px">
            <img src="{{ env('URL_STORAGE_FTP').$imagen->logo }}" alt="logo" width="150px" height="150px">
        </div>
        <div style="float:right;text-align:justify;padding-left:10px;padding-right:15px;margin-right:10px">
            <p><span style="color:black;font-weight:bold;font-size:11pt">NO.CORTE: </span>  
                <span style="color:red;font-weight:bold;font-size:11pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CD-{{$cortes->id}}</span>
                <br><span style="color:black;font-weight:bold;font-size:11pt">FECHA: </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp; <?php echo date("d/m/y",strtotime($cortes->date))?> | <?php echo date("H:i",strtotime($cortes->time))?>
                <br><span style="color:black;font-weight:bold;font-size:11pt">T脡CNICO: </span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$tecnico->name}}</span>
                <br><span style="color:black;font-weight:bold;font-size:11pt">VENTA TOTAL: </span>
                <span style="font-weight:bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${{$cortes->total}}</span>
            </p>
        </div>	
    </div>
    <br><br><br><br><br>
    <div style="font-weight:bold;color:red;text-align:center">
        <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CORTE DIARIO</h1>
    </div>
    <div style="margin-left:25px; margin-right:25px;margin-top:-30px"> 
        <h3 style="color:black;font-weight:bold">SERVICIOS PROGRAMADOS</h3>
        <table>
            <thead>
                <tr>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">SERVICIO</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">FECHA</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">HORA</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">CLIENTE</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">PLAGA</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">MONTO</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">ESTATUS</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">PRODUCTO</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">DOSIS</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;">CANTIDAD</th>
                </tr>
            </thead>
            <tbody style="border-top: 1px solid black;border-bottom:1 px solid black">
            @foreach($order as $o)
                <tr>
                    <td style="font-weight:bold;border-right:1px solid black;font-size:10pt;text-align:center">{{$o->id_service_order}}</td>
                    <td style="border-right:1px solid black;font-size:10pt;text-align:center">{{ $o->initial_date }}</td>
                    <td style="border-right:1px solid black;font-size:10pt;text-align:center"><?php echo date("H:i",strtotime($o->initial_hour))?></td>
                    <td style="border-right:1px solid black">
                        <span style="font-size:10pt;">{{$o->cliente}}</span>
                        @if($o->empresa != null)
                        <br>
                            <span style="font-weight:bold;font-size:8pt">
                                {{$o->empresa}}
                            </span>
                        @endif
                        <br>
                        <span style="font-size:6pt;">{{$o->address}} #{{$o->address_number}}, {{$o->colony}}</span>
                    </td>
                    
                        <td style="text-align:center;border-right:1px solid black;font-size:10pt">
                            @if($o->plague_key == "MTTO_organic")
                                MTTO
                            @elseif($o->plague_key == "CA_1_application")
                                CA
                            @elseif($o->plague_key == "CH_1_application")
                                CH
                            @elseif($o->plague_key == "TER_treatment")
                                TER
                            @else
                                RAT
                            @endif
                        </td>
                    
                        <td style="color:gray;border-right:1px solid black;text-align:center;font-size:10pt">
                            @if($o->id_status == 3)
                                ${{$o->total}}
                            @elseif($o->id_status == 4)
                                @if($o->payment == 2)
                                    <span style="color:black">${{$o->total}}</span><br>
                                    <span style="font-weight:bold;font-size:7pt;color:black">{{$o->metodo}}</span>
                                @elseif($o->payment == 1)
                                    <span style="color:red;">${{$o->total}}</span><br>
                                    <span style="font-weight:bold;font-size:7pt;color:red;">Adeudo</span>
                                @endif
                            @endif 
                        </td>
                    
                        <td style="border-right:1px solid black;text-align:center;font-size:10pt">
                            @if($o->id_status == 3)
                                <strong style="color: #b71c1c;">Cancelado</strong>
					        @elseif($o->id_status == 4)
						        <strong>Finalizado</strong>
                            @endif
                        </td>
                    <td style="border-right:1px solid black;font-size:10pt">
                        @foreach($product as $p)
                            @if($p->id_service_order == $o->order)
                                {{$p->name}} <br>
                            @endif
                        @endforeach
                    </td>
                    <td style="border-right:1px solid black;font-size:10pt">
                        @foreach($product as $p)
                            @if($p->id_service_order == $o->order)
                                {{$p->dose}} mlxlt <br>
                            @endif
                        @endforeach
                    </td>
                    <td style="font-size:10pt;">
                        @foreach($product as $p)
                            @if($p->id_service_order == $o->order)
                                {{$p->quantity}} ml <br>
                            @endif
                        @endforeach
                    </td>    
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br>
    <div style="margin-top:80px;margin-left:25px;margin-right:25px">
        <div style="width:300px;line-height:0.5em;float:left;font-size:10pt">
                    <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL EFECTIVO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span style="color:black;font-weight:unset">${{$cortes->total_efec}}</span></p>
                    <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL TRANSFERENCIA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        @if($cortes->total_transf == null)
                            <span style="color:black;font-weight:unset">$0</span>
                        @else
                            <span style="color:black;font-weight:unset">${{$cortes->total_transf}}</span>
                        @endif
                    </p>
                    <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL DEP脫SITO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        @if($cortes->total_depos == null)
                            <span style="color:black;font-weight:unset">$0</span>
                        @else
                            <span style="color:black;font-weight:unset">${{$cortes->total_depos}}</span>
                        @endif
                    </p>
                    <p style="color:black;font-weight:bold;border-bottom: 1px solid black">TOTAL CHEQUE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        @if($cortes->total_cheq == null)
                            <span style="color:black;font-weight:unset">$0</span>
                        @else
                            <span style="color:black;font-weight:unset">${{$cortes->total_cheq}}</span>
                        @endif
                    </p>
                    <p style="color:black;font-weight:bold">VENTA TOTAL:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span style="font-weight:bold;color:black">${{$cortes->total}}</span></p>
        </div>
        <div style="float:right;width:300px;line-height:0.5em;font-size:10pt">
           <p style="color:black;font-weight:bold">CONSUMO</p>
            <table>
                <tbody>
                    @foreach($sum as $p2)
                    @if($p2->usuario == $cortes->user_id)
                            <tr>
                                    <td>{{$p2->name}}</td>
                                    <td>{{$p2->sumx}} ml</td>
                            </tr>
                    @endif
                    @endforeach
                    </tbody>
           </table>
        </div>
    </div>
    
</body>
</html>