@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen content" id="courseevaluation-CompanyEvaluation">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Cortes Diarios</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 style="text-align:center;color:black">Cortes Diarios Realizados</h3>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table tablesorter" id="tableProduct">
                                        <thead class="table-general">
                                            <tr>
                                                <td style="font-weight:bold">#</td>
                                                <td style="font-weight:bold">Fecha/Hora</td>
                                                <td style="font-weight:bold">Servicios <br> Programados</td>
                                                <td style="font-weight:bold">Servicios <br> Realizados</td>
                                                <td style="font-weight:bold">Total</td>
                                                <td style="font-weight:bold">Total Efectivo</td>
                                                <td style="font-weight:bold">Total Otras Formas</td>
                                                <td style="font-weight:bold">Total CxC</td>
                                                <td style="font-weight:bold">Total Consumo</td>
                                                <td style="font-weight:bold">Estatus</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($cortes as $c)
                                                <tr>
                                                    <td style="color:red;font-weight:bold">
                                                        <div class="btn-group">
                                                            <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                CD-{{ $c->id}} <span class="caret"></span>
                                                            </a>
                                                            @include('vendor.adminlte.cuts._menu')
                                                        </div>
                                                    </td>
                                                    <td><?php echo date("d-m-Y",strtotime($c->date))?>/<?php echo date("H:i",strtotime($c->time))?></td>
                                                    <td>{{$c->programs}}</td>
                                                    <td>{{$c->realize}}</td>
                                                    <td>${{$c->total}}</td>
                                                    @if($c->total_efec == null)
                                                        <td>$0</td>
                                                    @else
                                                        <td>${{$c->total_efec}}</td>
                                                    @endif
                                                    @if($c->tot_other == null)
                                                        <td>$0</td>
                                                    @else
                                                    <td>${{$c->tot_other}}</td>
                                                    @endif
                                                    @if($c->tot_credit == null)
                                                        <td>$0</td>
                                                    @else
                                                    <td>${{$c->tot_credit}}</td>
                                                    @endif
                                                    <td>
                                                    <a href="{{Route('detail_cons',$c->id)}}">Ver Consumo</a>
                                                    </td>
                                                    <td class="text-center">
                                                        @if($c->depositado == 1)
                                                            <span style="color:red;font-weight:bold">Depositado</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <br><br><br><br>
                                </div>
                                <br><br><br>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection