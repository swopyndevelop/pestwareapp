@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen content" id="courseevaluation-CompanyEvaluation">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Corte Diario</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 style="text-align:center;color:black">Servicios al día de hoy</h3>
                                </div>
                            </div>

                            <br><br>
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table tablesorter" id="tableProduct">
                                        <thead class="table-general">
                                            <tr>
                                                <td>Servicio</td>
                                                <td>Hora</td>
                                                <td>Cliente</td>
                                                <td>Plaga</td>
                                                <td>Monto</td>
                                                <td>Status</td>
                                                <td>Producto</td>
                                                <td>Dosis</td>
                                                <td>Cantidad</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order as $o)
                                        <input type="hidden" value="{{$o->order}}" name="event" id="event">
                                            <tr>
                                                <td style="font-weight:bold">{{$o->id_service_order}}</td>
                                                <td><?php echo date("H:i",strtotime($o->initial_hour))?></td>
                                                <td>{{$o->cliente}}
                                                    @if($o->empresa != null) 
                                                    <span style="font-weight:bold;font-size:9pt">
                                                        {{$o->empresa}}
                                                    </span>
                                                     @endif
                                                     <br>
                                                    <span style="font-size:7pt">{{$o->address}} #{{$o->address_number}}, {{$o->colony}}, {{$o->municipality}}, {{$o->state}}</span>
                                                </td>
                                                
                          							<td>{{$o->key}}</td>
                        						
                                                @if($o->id_status == 3)
                                                    <td style="color:gray">{{ $symbol_country }}{{$o->total}}</td>
                                                @elseif($o->id_status == 4)
                                                @foreach($cashes as $c)
                                                @if($c->ser == $o->order)
                                                    @if($c->payment == 2)
                                                        <td style="color:black">{{ $symbol_country }}{{$o->total}} <br>
                                                        <span style="font-weight:bold;font-size:9pt">{{$o->metodo}}</span>
                                                        </td>
                                                    @endif
                                                @endif
                                                @endforeach
                                                    
                                                    @foreach($cashes as $c)
                                                    @if($c->ser == $o->order)
                                                    @if($c->payment == 1)
                                                        <td style="color:#b71c1c">{{ $symbol_country }}{{$o->total}} <br>
                                                            <span style="font-weight:bold;font-size:9pt">Adeudo</span>
                                                        </td>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                @endif
												@if($o->id_status == 3)
													<td><strong style="color: #b71c1c">Cancelado</strong></td>
												@elseif($o->id_status == 4)
													<td><strong style="color: #00a157">Finalizado</strong></td>
                                                @endif
                                                <td>
                                                    @foreach($product as $p)
                                                        @if($p->id_service_order == $o->order)
                                                            {{$p->name}} <br>
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach($product as $p)
                                                        @if($p->id_service_order == $o->order)
                                                            {{$p->dose}} <br>
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach($product as $p)
                                                        @if($p->id_service_order == $o->order)
                                                            {{$p->quantity}} ml <br>
                                                         @endif
                                                    @endforeach
                                                </td>    
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                                <br><br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 style="text-align:center;color:black">Gastos al día de hoy</h3>
                                </div>
                            </div>
                                <div class="col-md-12" style="text-align:center">
                                    @if($cuts == null)
                                        <button type="submit" class="btn btn-primary" id="click_b" name="click_b">Realizar Corte</button>
                                    @endif
                                    <a href="{{route('cuts_tech',$employee->employee_id)}}" class="btn btn-secondary" style="font-weight:bold">
                                        <i class="fa fa-toggle-right" aria-hidden="true" style=" font-size: 1em;"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/cut.js') }}"></script>
@endsection
