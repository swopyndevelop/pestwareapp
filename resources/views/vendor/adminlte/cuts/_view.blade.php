@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="ViewC" role="dialog" aria-labelledby="View_CutModal" tabindex="-1" >
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" style="width: 90% !important;">

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
              <div class="box-body">
                  <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle del Corte</h4>
                  <br><br>
                    <div class="row">
                            <div class="col-md-12 table-responsive">
                                    <table class="table tablesorter" id="tableProduct">
                                        <thead>
                                            <tr>
                                                <td>Servicio</td>
                                                <td>Fecha</td>
                                                <td>Hora</td>
                                                <td>Cliente</td>
                                                <td>Plaga</td>
                                                <td>Monto</td>
                                                <td>Status</td>
                                                <td>Producto</td>
                                                <td>Dosis</td>
                                                <td>Cantidad</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order as $o)
                                            <tr>
                                                <td style="font-weight:bold">{{$o->id_service_order}}</td>
                                                <td>{{ $o->initial_date }}</td>
                                                <td><?php echo date("H:i",strtotime($o->initial_hour))?></td>
                                                <td>{{$o->cliente}}
                                                    @if($o->empresa != null)
                                                    <span style="font-weight:bold;font-size:9pt">
                                                        {{$o->empresa}}
                                                    </span>
                                                     @endif
                                                     <br>
                                                    <span style="font-size:7pt">{{$o->address}} #{{$o->address_number}}, {{$o->colony}}, {{$o->municipality}}, {{$o->state}}</span>
                                                </td>
                          							<td>{{$o->key}}</td>
                                        <td style="color:gray;text-align:center;">
                                          @if($o->id_status == 3)
                                              ${{$o->total}}
                                          @elseif($o->id_status == 4)
                                          @foreach($cashes as $c)
                                              @if($c->ser == $o->order)
                                              @if($c->payment == 2)
                                                  <span style="color:black">{{ $symbol_country }}{{$o->total}}</span><br>
                                                  <span style="font-weight:bold;font-size:7pt;color:black">{{$o->metodo}}</span>
                                              @endif
                                              @endif
                                          @endforeach
                                          @foreach($cashes as $c)
                                          @if($c->ser == $o->order)
                                              @if($c->payment == 1)
                                                  <span style="color:red;">{{ $symbol_country }}{{$o->total}}</span><br>
                                                  <span style="font-weight:bold;font-size:7pt;color:red;">Adeudo</span>
                                              @endif
                                          @endif
                                          @endforeach
                                          @endif
                                      </td>
												@if($o->id_status == 3)
													<td><strong style="color: #b71c1c">Cancelado</strong></td>
												@elseif($o->id_status == 4)
													<td><strong style="color: #00a157">Finalizado</strong></td>
                                                @endif
                                                <td>
                                                    @foreach($product as $p)
                                                        @if($p->id_service_order == $o->order)
                                                            {{$p->name}} <br>
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach($product as $p)
                                                        @if($p->id_service_order == $o->order)
                                                            {{$p->dose}} <br>
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach($product as $p)
                                                        @if($p->id_service_order == $o->order)
                                                            {{$p->quantity}} ml <br>
                                                         @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
  <script>
    //$(".plagueList-single").select2();
    $("#ViewC").modal();
    $("#ViewC").on('hidden.bs.modal', function() {
      window.location.href = '{{route("cuts_admin")}}'; //using a named route
    });
  </script>
@endsection
