@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="Deposit" role="dialog" aria-labelledby="DeposModal" tabindex="-1" >
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
            <form id="add_deposito" method="POST" enctype="multipart/form-data" action="{{Route('add_dep')}}">
                {{ csrf_field() }}
              <div class="box-body">
                  <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Realizar Depósito</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">

                            <input type="hidden" name="corte" id="corte" value="{{$cortes->id}}">
                                <label class="control-label" for="dateStart" style="font-weight: bold;">Fecha de Depósito: </label>
                                <br>
                                <input type="date" name="dateDep" id="dateDep" class="form-control" required>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label" for="hourEnd" style="font-weight: bold;">Hora de Depósito: </label>
                                <br>
                                <input type="time" name="hourDep" id="hourDep" class="form-control" required>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label" for="hourEnd" style="font-weight: bold;">Total Depositado: </label>
                                <br>
                                <input type="number" name="totalDep" id="totalDep" class="form-control" required step="any">
                            </div>
                            <div class="col-md-3">
                                <label for="techniciansEvent" style="font-weight: bold;">Evidencia:</label>
                                <input type="file" name="imagen" id="imagen" accept="image/*" capture class="form-control" required>
                            </div>
                        </div>

                    </div>

                </div>
              <div class="modal-footer">
                <div class="text-center">
                  <div class="row">
                      <button class="btn btn-primary btn-sm" type="submit" name="SaveDepos">Guardar</button>
                </div>
                </div>
              </div>
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
  <script>
    //$(".plagueList-single").select2();
    $("#Deposit").modal();
    $("#Deposit").on('hidden.bs.modal', function() {
      window.location.href = '{{route("cuts_tech",$cortes->user_id)}}'; //using a named route
    });
  </script>
@endsection
