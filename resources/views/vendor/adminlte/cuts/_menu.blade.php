<ul class="dropdown-menu">
    <li><a href="{{Route('report_cut', $c->id)}}" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar Reporte</a></li>
    @if($c->depositado == 0)
        <li><a href="{{Route('deposito', $c->id)}}"><i class="fa fa-ticket" aria-hidden="true" style="color: black;"></i>Depósito</a></li>
    @endif
</ul>