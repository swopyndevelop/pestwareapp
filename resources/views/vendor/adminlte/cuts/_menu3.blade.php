@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="_menu3" role="dialog" aria-labelledby="_menu3" tabindex="-1" >
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
              <div class="box-body">
                  <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalles de Consumo</h4>
                    <div class="row">
                        <div class="col-md-12">
                          <div class="col-md-12" style="color:black;float:left">
                            <table class="table tablesorter" id="tableProduct">
                                     <thead>
                                             <tr>
                                                     <th>PRODUCTO</th>
                                                     <th>CANTIDAD</th>
                                             </tr>
                                     </thead>
                                     <tbody>
                                     @foreach($sum as $p2)
                                     @if($p2->usuario == $cortes->user_id)
                                             <tr>
                                                     <td>{{$p2->name}}</td>
                                                     <td>{{$p2->sumx}}</td>
                                             </tr>
                                     @endif
                                     @endforeach
                                     </tbody>
                            </table>
                         </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
  <script>
    //$(".plagueList-single").select2();
    $("#_menu3").modal();
    $("#_menu3").on('hidden.bs.modal', function() {
      window.location.href = '{{route("cuts_tech",$cortes->user_id)}}'; //using a named route
    });
  </script>
@endsection
