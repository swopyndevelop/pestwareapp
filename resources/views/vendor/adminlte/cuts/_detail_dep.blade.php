@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="Det_Deposit" role="dialog" aria-labelledby="Det_DeposModal" tabindex="-1" >
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
              <div class="box-body">
                  <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalles del Depósito</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12" style="color:black;float:left">
                                <h4 style="color:black;text-align:center">Depósito del Corte de Caja: CD-{{$dep->id_cash_cut}}</h3>
                                <p><span style="font-weight:bold">Fecha de Depósito: </span><?php echo date("d/m/y",strtotime($dep->date))?></p>
                                <p><span style="font-weight:bold">Hora de Depósito: </span><?php echo date("H:i",strtotime($dep->hour))?></p>
                                <p><span style="font-weight:bold">Persona que realizó el depósito: </span>{{$dep->name}}</p>
                                <p><span style="font-weight:bold">Total Depósitado: </span>${{$dep->total}}</p>
                            <div class="col-md-12 text-center">
                                @php
                                    echo '<img src="data:image/jpeg;base64,'.base64_encode( $dep->image ).'"  alt="" style="width:100%" class="img-responsive"/>';
                                @endphp
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
  <script>
    //$(".plagueList-single").select2();
    $("#Det_Deposit").modal();
    $("#Det_Deposit").on('hidden.bs.modal', function() {
      window.location.href = '{{route("cuts_admin")}}'; //using a named route
    });
  </script>
@endsection
