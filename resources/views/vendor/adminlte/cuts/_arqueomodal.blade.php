<!-- Ventana para el arqueo-->
<div class="modal fade" id="CutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="text-align:center">
                <br>
                <h4 style="color:black" class="modal-title" id="exampleModalLongTitle">Arqueo de Corte de Caja: <?php echo date("d/m/y",strtotime($date))?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <h4 class="modal-title" style="color:black;text-align:center">Introducir las cantidades correspondientes</h4>
            <br>
            <br>
            <div class="col-md-4">
                <label for="mil"><img src="{{ asset( 'img/1000.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
                <input type="number" name="mil" id="mil" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="quinientos"><img src="{{ asset( 'img/500.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
                <input type="number" name="quinientos" id="quinientos" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="doscientos"><img src="{{ asset( 'img/200.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
                <input type="number" name="doscientos" id="doscientos" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <br>
            <br>
            <div class="col-md-4">
                <label for="cien"><img src="{{ asset( 'img/100.png' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
                <input type="number" name="cien" id="cien" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="cincuenta"><img src="{{ asset( 'img/50.jpg' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
                <input type="number" name="cincuenta" id="cincuenta" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="veinte"><img src="{{ asset( 'img/20.jpg' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
                <input type="number" name="veinte" id="veinte" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="diez"><img src="{{ asset( 'img/10.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                <br>
                <input type="number" name="diez" id="diez" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="cinco"><img src="{{ asset( 'img/5.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                <br>
                <input type="number" name="cinco" id="cinco" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="dos"><img src="{{ asset( 'img/2.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                <br>
                <input type="number" name="dos" id="dos" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="uno"><img src="{{ asset( 'img/1.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                <br>
                <input type="number" name="uno" id="uno" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="cincuenta_centavos"><img src="{{ asset( 'img/50-Centavos.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                <br>
                <input type="number" name="cincuenta_centavos" id="cincuenta_centavos" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="veinte_centavos"><img src="{{ asset( 'img/20-c.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                <br>
                <input type="number" name="veinte_centavos" id="veinte_centavos" style="width:80px;height:30px" value="" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="diez_centavos"><img src="{{asset('img/10centavos.jpg')}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                <br>
                <input type="number" name="diez_centavos" id="diez_centavos"  style="width:80px;height:30px" value="" class="form-control">
            </div>
        </div>
        <br>
        <div class="modal-footer" style="margin-top: 500px">
            <button type="button" class="btn btn-primary" id="Arqueo" name="Arqueo">Guardar</button>
        </div>
    </div>
</div>