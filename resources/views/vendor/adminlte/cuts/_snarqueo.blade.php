@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- Ventana para el arqueo-->
<div class="modal fade" id="CutModalX" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="text-align:center">
                    <br>
                    <h4 style="color:black" class="modal-title" id="exampleModalLongTitle">Arqueo de Corte de Caja</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <h4 class="modal-title" style="color:black;text-align:center">Introducir las cantidades correspondientes</h4>
                <br>
                <br>
                <input type="hidden" name="corter" id="corter" value="{{$x}}">
                <div class="col-md-4">
                    <label for="mil"><img src="{{ asset( 'img/1000.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
                    <input type="number" name="milx" id="milx" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="quinientos"><img src="{{ asset( 'img/500.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
                    <input type="number" name="quinientosx" id="quinientosx" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="doscientos"><img src="{{ asset( 'img/200.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
                    <input type="number" name="doscientosx" id="doscientosx" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <br>
                <br>
                <div class="col-md-4">
                    <label for="cien"><img src="{{ asset( 'img/100.png' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
                    <input type="number" name="cienx" id="cienx" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="cincuenta"><img src="{{ asset( 'img/50.jpg' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
                    <input type="number" name="cincuentax" id="cincuentax" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="veinte"><img src="{{ asset( 'img/20.jpg' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
                    <input type="number" name="veintex" id="veintex" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="diez"><img src="{{ asset( 'img/10.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                    <br>
                    <input type="number" name="diezx" id="diezx" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="cinco"><img src="{{ asset( 'img/5.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                    <br>
                    <input type="number" name="cincox" id="cincox" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="dos"><img src="{{ asset( 'img/2.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                    <br>
                    <input type="number" name="dosx" id="dosx" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="uno"><img src="{{ asset( 'img/1.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                    <br>
                    <input type="number" name="unox" id="unox" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="cincuenta_centavos"><img src="{{ asset( 'img/50-Centavos.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                    <br>
                    <input type="number" name="cincuenta_centavosx" id="cincuenta_centavosx" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="veinte_centavos"><img src="{{ asset( 'img/20-c.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                    <br>
                    <input type="number" name="veinte_centavosx" id="veinte_centavosx" style="width:80px;height:30px" value="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="diez_centavos"><img src="{{asset('img/10centavos.jpg')}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
                    <br>
                    <input type="number" name="diez_centavosx" id="diez_centavosx"  style="width:80px;height:30px" value="" class="form-control">
                </div>
            </div>
            <br>
            <div class="modal-footer" style="margin-top: 500px">
                <button type="button" class="btn btn-primary" id="ArqueoX" name="ArqueoX">Guardar</button>
            </div>
        </div>
    </div>

    @endsection

    @section('personal-js')
      <script>
        //$(".plagueList-single").select2();
        $("#CutModalX").modal();
        $("#CutModalX").on('hidden.bs.modal', function() {
          window.location.href = '{{route("cuts_admin")}}'; //using a named route
        });
      </script>
    @endsection