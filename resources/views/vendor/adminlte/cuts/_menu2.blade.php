<ul class="dropdown-menu">
    <li><a href="{{Route('view_cut', \Vinkla\Hashids\Facades\Hashids::encode($c->id)) }}"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a></li>
    <li><a href="{{Route('report_cut', \Vinkla\Hashids\Facades\Hashids::encode($c->id)) }}" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar Reporte</a></li>
    <li><a href="{{Route('deposito', $c->id)}}"><i class="fa fa-ticket" aria-hidden="true" style="color: black;"></i>Depósito</a></li>
    @if($c->depositado == 1)
        <li><a href="{{Route('detail_dep', \Vinkla\Hashids\Facades\Hashids::encode($c->id)) }}"><i class="fa fa-info" aria-hidden="true" style="color: black;"></i> Ver Depósito</a></li>
    @endif
</ul>