@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="_adeudo" role="dialog" aria-labelledby="_adeudo" tabindex="-1" >
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
              <div class="box-body">
                  <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalles de Consumo</h4>
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                               <table class="table tablesorter" id="tableProduct">
                                        <thead>
                                                <tr>
                                                        <th>PRODUCTO</th>
                                                        <th>CANTIDAD</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($sum as $p2)
                                        @if($p2->usuario == $cortes->user_id)
                                                <tr>
                                                        <td>{{$p2->name}}</td>
                                                        <td>{{$p2->sumx}}</td>
                                                </tr>
                                        @endif
                                        @endforeach
                                        </tbody>
                               </table>
                            </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
  <script>
    //$(".plagueList-single").select2();
    $("#_adeudo").modal();
    $("#_adeudo").on('hidden.bs.modal', function() {
      window.location.href = '{{route("cuts_admin")}}'; //using a named route
    });
  </script>
@endsection
