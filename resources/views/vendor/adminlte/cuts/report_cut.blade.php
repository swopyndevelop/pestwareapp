<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>Corte de Caja</title>
</head>
<style>
    @page {
        margin-top: 25px;
        margin-bottom: 25px;
        margin-left: 0;
        margin-right: 0;
    }
    body{
        font-family: 'Montserrat'
    }
    table {
        width: 100%;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
    }
    tbody {
    }
    th{
        text-align: center;
    }
    td{
        padding: 0.3em;
    }
</style>
<body>
    <div>
        <div style="float:left;padding-left:25px;padding-right:40px;padding-top:15px">
            <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="100px" height="100px">
        </div>
        <div style="float:right;text-align:justify;padding-left:10px;padding-right:15px;margin-right:10px">
            <p><span style="color:black;font-weight:bold;font-size:10pt">NO.CORTE: </span>  
                <span style="color:red;font-weight:bold;font-size:10pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;CD-{{$cortes->id}}</span>
                <br><span style="color:black;font-weight:bold;font-size:10pt">FECHA: </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp; <?php echo date("d/m/y",strtotime($cortes->date))?> | <?php echo date("H:i",strtotime($cortes->time))?>
                <br><span style="color:black;font-weight:bold;font-size:10pt">TÉCNICO: </span>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$tecnico->name}}</span>
                <br><span style="color:black;font-weight:bold;font-size:10pt">VENTA TOTAL: </span>
                <span style="font-weight:bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $symbol_country }}{{$cortes->total}}</span>
            </p>
        </div>	
    </div>
    <br><br><br><br><br>
    <div style="font-weight:bold;color:black;text-align:center">
        <h2 style="text-align:center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CORTE DIARIO</h2>
    </div>
    <div style="margin-left:25px; margin-right:25px;margin-top:-30px"> 
        <h4 style="color:black;font-weight:bold">SERVICIOS PROGRAMADOS</h4>
        <table>
            <thead>
                <tr>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black">SERVICIO</th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black">FECHA</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black">HORA</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black">CLIENTE</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black">PLAGA</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black">MONTO</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black">ESTATUS</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black">PRODUCTO</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black">DOSIS</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;">CANTIDAD</th>
                </tr>
                <tr>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black"></th>
                    <th style="color:black;font-weight:bold;font-size:11pt;border-right:1px solid black"></th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black"></th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black"></th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black"></th>
                    <th style="color:black;font-weight:bold;font-size:6pt;border-right:1px solid black">TOTAL / RECIBIDO</th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black"></th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black"></th>
                    <th style="color:black;font-weight:bold;font-size:10pt;border-right:1px solid black"></th>
                    <th style="color:black;font-weight:bold;font-size:10pt;"></th>
                </tr>
            </thead>
            <tbody style="border-top: 1px solid black; border-bottom:1px solid black">
            @foreach($order as $o)
                <tr>
                    <td style="font-weight:bold;border-right:1px solid black;font-size:9pt;text-align:center">{{$o->id_service_order}}</td>
                    <td style="border-right:1px solid black;font-size:10pt;text-align:center">{{ $o->initial_date }}</td>
                    <td style="border-right:1px solid black;font-size:9pt;text-align:center"><?php echo date("H:i",strtotime($o->initial_hour))?></td>
                    <td style="border-right:1px solid black">
                        <span style="font-size:9pt;">{{$o->cliente}}</span>
                        @if($o->empresa != null)
                        <br>
                            <span style="font-weight:bold;font-size:7pt">
                                {{$o->empresa}}
                            </span>
                        @endif
                        <br>
                        <span style="font-size:6pt;">{{$o->address}} #{{$o->address_number}}, {{$o->colony}}</span>
                    </td>
                    
                        <td style="text-align:center;border-right:1px solid black;font-size:9pt">
                            {{$o->key}}
                        </td>
                    
                        <td style="color:gray;border-right:1px solid black;text-align:center;font-size:9pt">
                            @if($o->id_status == 3)
                                {{ $symbol_country }}{{$o->total}}
                            @elseif($o->id_status == 4)
                            @foreach($cashes as $c)
                                @if($c->ser == $o->order)

                                    <span style="color:black">{{ $symbol_country }}{{$o->total}} / <span style="@if($c->amount_received < $o->total) color:red; @elseif($c->amount_received > $o->total) color:darkgreen; @endif">{{ $symbol_country }}{{ $c->amount_received }}</span></span><br>
                                    <span style="font-weight:bold;font-size:6pt;color:black">{{$o->metodo}}</span>

                                        @if($c->payment == 1)
                                            <br><span style="font-weight:bold;font-size:6pt;color:red;">Adeudo</span>
                                        @endif
                                @endif
                            @endforeach
                         {{--   @foreach($cashes as $c)
                            @if($c->ser == $o->order)
                                @if($c->payment == 1)
                                    <span style="color:red;">{{ $symbol_country }}{{$o->total}}</span><br>

                                @endif
                            @endif
                            @endforeach--}}
                            @endif 
                        </td>
                    
                        <td style="border-right:1px solid black;text-align:center;font-size:9pt">
                            @if($o->id_status == 3)
                                <strong style="color: #b71c1c;">Cancelado</strong>
					        @elseif($o->id_status == 4)
						        <strong>Finalizado</strong>
                            @endif
                        </td>
                    <td style="border-right:1px solid black;font-size:9pt">
                        @foreach($product as $p)
                            @if($p->id_service_order == $o->order)
                                {{$p->name}} <br>
                            @endif
                        @endforeach
                    </td>
                    <td style="border-right:1px solid black;font-size:9pt">
                        @foreach($product as $p)
                            @if($p->id_service_order == $o->order)
                                {{$p->dose}} <br>
                            @endif
                        @endforeach
                    </td>
                    <td style="font-size:9pt;">
                        @foreach($product as $p)
                            @if($p->id_service_order == $o->order)
                                {{$p->quantity}} {{ $p->unit }}s<br>
                            @endif
                        @endforeach
                    </td>    
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>