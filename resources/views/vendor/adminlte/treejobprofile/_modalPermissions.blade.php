@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection
@section('css-personal')
    @include('adminlte::treejobprofile.PersonalSelect2')
@stop

@section('main-content')
<!-- START MODAL DE CONFIG START -->
<div class="container-fluid spark-screen">
        <div class="modal fade" id="createPermission" role="dialog" aria-labelledby="configStart" style="overflow-y: scroll;" data-backdrop="static" data-keyboard="false">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <!--Default box-->
                    <div class="modal-dialog" role="document" style="width: 90% !important;">

                        <div class="modal-content" >
                            <div class="box">
                                <div class="modal-header" >
                                    <div class="box-header" >
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <input type="text" id="namejobtitleprofile" value="{{ $name }}" hidden>
                                        <input type="number" id="idjobtitleprofile" value="{{ $job_profile_id }}" hidden>
                                    </div>
                                </div>

                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Asignación de Permisos</h4>
                                        <div class="col-md-11">
                                            <label for="address" class="form-control-label">Selecciona las funciones del puesto de trabajo:</label>
                                            <br>
                                            @if($permissionsRole != null)
                                                <select name="permissions[]" id="permissions" class="form-control multiple" multiple="multiple">
                                                    @foreach($permissions as $permission)
                                                        <option value="{{ $permission->id }} }}"
                                                            @foreach($permissionsRole as $permissionRo)
                                                                @if($permissionRo->permission_id == $permission->id)
                                                                    selected = 'selected'
                                                                @endif
                                                            @endforeach
                                                            {{ (in_array($permission, old('permissions', []))) }} >{{ $permission->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <select name="permissions[]" id="permissions" class="form-control multiple" multiple="multiple">
                                                    @foreach($permissions as $permission)
                                                        <option value="{{ $permission->id }} }}">{{ $permission->name }}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                </div>
    
                                <div class="modal-footer">
                                    <br>
                                    <div class="text-center">
                                        <button class="btn btn-primary" type="submit" id="savePermissions">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- END MODAL DE CONFIG START -->
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/jobtitleprofile.js') }}"></script>
    <script>
        $("#createPermission").modal();
        $("#createPermission").on('hidden.bs.modal', function() {
            window.location.href = '{{route("tree_jobprofile")}}';
        });
    </script>
    <script>
        $("#permissions").select2();
    </script>
@endsection