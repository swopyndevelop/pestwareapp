<style>
    .select2-container--default .select2-selection--multiple .select2-selection__rendered li {
        background-color: white;
        color: black;
        font-weight: bold;
        border: none;
        font-size: 1em;
        margin: .5em;
    }
    span.select2-selection.select2-selection--multiple{border: 2px solid #0199bf;}
    input:focus {
        outline: none !important;
        border-color: #719ECE;
        box-shadow: 0 0 30px #719ECE;
    }
</style>