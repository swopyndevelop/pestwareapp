@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="col-lg-6">
                    <input type="hidden" id="symbolCountry" value="{{ $symbolCountry }}">
                    <!-- Single button -->
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="transferAddButton">
                            <span class="fa fa-plus"></span> Agregar Traspaso <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            @if($treeJobCenterMain->parent == '#')
                            <li><a href="{{ route('create_transfer_cc') }}">Sucursal a Sucursal</a></li>
                            @endif
                            <li><a href="{{ route('create_transfer_ce') }}">Sucursal a Empleado</a></li>
                            <li><a href="{{ route('create_transfer_ee') }}">Empleado a Empleado</a></li>
                            <li><a href="{{ route('create_transfer_ec') }}">Empleado a Sucursal</a></li>
                            <li><a href="{{ route('create_transfer_cs') }}">Cliente a Sucursal</a></li>
                            <li><a href="{{ route('create_transfer_cem') }}">Cliente a Empleado</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 margin-mobile">
                    {!!Form::open(['method' => 'GET','route'=>'search_transfer'])!!}
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="container-fluid spark-screen" id="entry">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title" id="titleTransferDiv">Traspasos Sucursal a Sucursal</h3>
                        <input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="#transfers_cc" aria-controls="transfers_cc" role="tab" data-toggle="tab" id="tab_transfers_cc">Sucursal a Sucursal (TCC)</a></li>
                                <li role="presentation"><a href="#transfers_ce" aria-controls="transfers_ce" role="tab" data-toggle="tab" id="tab_transfers_ce">Sucursal a Empleado (TCE)</a></li>
                                <li role="presentation"><a href="#transfers_ee" aria-controls="transfers_ee" role="tab" data-toggle="tab" id="tab_transfers_ee">Empleado a Empleado (TEE)</a></li>
                                <li role="presentation"><a href="#transfers_ec" aria-controls="transfers_ec" role="tab" data-toggle="tab" id="tab_transfers_ec">Empleado a Sucursal (TEC)</a></li>
                                <li role="presentation"><a href="#transfers_cs" aria-controls="transfers_cs" role="tab" data-toggle="tab" id="tab_transfers_cs">Cliente a Sucursal (TCLC)</a></li>
                                <li role="presentation"><a href="#transfers_cemp" aria-controls="transfers_cemp" role="tab" data-toggle="tab" id="tab_transfers_cemp">Cliente a Empleado (TCLE)</a></li>
                                <li role="presentation"><a href="#transfers_tac" aria-controls="transfers_tac" role="tab" data-toggle="tab" id="tab_transfers_tac">Técnico a Cliente (TAC)</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                
                                <br>

                                <div role="tabpanel" class="tab-pane active" id="transfers_cc">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive" id="masterContainer">
                                            <table class="table tablesorter table-hover" id="tableProductCC">
                                                <thead>
                                                    <tr>
                                                        <th># Traspaso</th>
                                                        <th>Fecha/Usuario</th>
                                                        <th>Origen</th>
                                                        <th>Destino</th>
                                                        <th>Unidades<br>Traspasadas</th>
                                                        <th>Total Inventario</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tableBodyTransfersCC">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" id="pagination-wrapper">
                                                    
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="transfers_ce">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive" id="masterContainerCE">
                                            <table class="table tablesorter table-hover" id="tableProductCE">
                                                <thead>
                                                <tr>
                                                    <th># Traspaso</th>
                                                    <th>Fecha/Usuario</th>
                                                    <th>Origen</th>
                                                    <th>Destino</th>
                                                    <th>Unidades<br>Traspasadas</th>
                                                    <th>Total Inventario</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tableBodyTransfersCE">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" id="pagination-wrapperCE">
                                                    
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="transfers_ee">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive" id="masterContainerEE">
                                            <table class="table tablesorter table-hover" id="tableProductEE">
                                                <thead>
                                                <tr>
                                                    <th># Traspaso</th>
                                                    <th>Fecha/Usuario</th>
                                                    <th>Origen</th>
                                                    <th>Destino</th>
                                                    <th>Unidades<br>Traspasadas</th>
                                                    <th>Total Inventario</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tableBodyTransfersEE">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" id="pagination-wrapperEE">
                                                    
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="transfers_ec">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive" id="masterContainerEC">
                                            <table class="table tablesorter table-hover" id="tableProductEC">
                                                <thead>
                                                <tr>
                                                    <th># Traspaso</th>
                                                    <th>Fecha/Usuario</th>
                                                    <th>Origen</th>
                                                    <th>Destino</th>
                                                    <th>Unidades<br>Traspasadas</th>
                                                    <th>Total Inventario</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tableBodyTransfersEC">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" id="pagination-wrapperEC">
                                                    
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="transfers_cs">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive" id="masterContainerCS">
                                            <table class="table tablesorter table-hover" id="tableProductCS">
                                                <thead>
                                                <tr>
                                                    <th># Traspaso</th>
                                                    <th>Fecha/Usuario</th>
                                                    <th>Origen</th>
                                                    <th>Destino</th>
                                                    <th>Unidades<br>Traspasadas</th>
                                                    <th>Total Inventario</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tableBodyTransfersCS">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" id="pagination-wrapperCS">
                                                    
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="transfers_cemp">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive" id="masterContainerCEMP">
                                            <table class="table tablesorter table-hover" id="tableProductCEMP">
                                                <thead>
                                                <tr>
                                                    <th># Traspaso</th>
                                                    <th>Fecha/Usuario</th>
                                                    <th>Origen</th>
                                                    <th>Destino</th>
                                                    <th>Unidades<br>Traspasadas</th>
                                                    <th>Total Inventario</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tableBodyTransfersCEMP">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" id="pagination-wrapperCEMP">
                                                    
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="transfers_tac">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive" id="masterContainerTAC">
                                            <table class="table tablesorter table-hover" id="tableProductTAC">
                                                <thead>
                                                <tr>
                                                    <th># Traspaso</th>
                                                    <th>Fecha/Usuario</th>
                                                    <th>Origen</th>
                                                    <th>Destino</th>
                                                    <th>Unidades<br>Traspasadas</th>
                                                    <th>Total Inventario</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tableBodyTransfersTAC">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" id="pagination-wrapperTAC">
                                                    
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                            </div>  

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('vendor.adminlte.transfers._modalViewDetailCC')
    @include('vendor.adminlte.transfers._modalViewDetailCE')
    @include('vendor.adminlte.transfers._modalViewDetailEE')
    @include('vendor.adminlte.transfers._modalViewDetailEC')
    @include('vendor.adminlte.transfers._modalViewDetailCS')
    @include('vendor.adminlte.transfers._modalViewDetailCEMP')
    @include('vendor.adminlte.transfers._modalViewDetailTAC')
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/inventory.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/transferTabs.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
@endsection