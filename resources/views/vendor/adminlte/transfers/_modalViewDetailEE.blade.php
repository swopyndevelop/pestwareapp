<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL TRASPASO -->
<div class="modal fade" id="transferDetailEE" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle del Traspaso</h4>
				<div class="row container">
					<div class="col-md-6">
						<p style="font-weight: bold; color: black; font-size: 18px" id="id_transfer_ee"></p>
						<p style="font-weight: bold; color: black; font-size: 15px" id="dateEE"></p>
						<p style="font-weight: bold; color: black; font-size: 15px" id="usuarioEE"></p>
					</div>
					<div class="col-md-6">
						<label style="font-weight: bold; color: black;" id="origenEE">Origen:</label>
						<br>
						<label style="font-weight: bold; color: black;" id="destinyEE">Destino:</label>
						<br>
						<label style="font-weight: bold; color: black;" id="totalEE">Total:</label>
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive" id="masterDetailEE">
                        <table class="table tablesorter" id="tableProductDetailEE">
                            <thead class="table-general">
                                <tr>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Cantidad Envase</th>
                                    <th class="text-center">Unidades Traspasadas</th>
                                    <th class="text-center">Precio Base</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody id="tableBodyDetailEE">
                               	 
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL TRASPASO-->