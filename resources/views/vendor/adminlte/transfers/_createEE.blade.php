@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen" id="entry">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nuevo Traspaso Empleado a Empleado</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method="post" id="insert_formEE">
                                {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-5">
                                <label for="origin">Origen</label>
                                <select name="selectEmployeesOrigin" id="selectEmployeesOrigin" class="employeesListOrigin-single form-control" style="width: 100%; font-size: small !important;">
                                    @foreach($employees as $employee)
                                        <option value="{{ $employee->id }}">
                                            {{ $employee->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-5">
                                <label for="origin">Destino</label>
                                <select name="selectEmployeesDestiny" id="selectEmployeesDestiny" class="employeesListDestiny-single form-control" style="width: 100%; font-size: small !important;">
                                    <option value="0" disabled="" selected="">Seleccione una opción</option>
                                    @foreach($employeesDestiny as $employee)
                                        <option value="{{ $employee->id }}">
                                            {{ $employee->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="total">Total Inventario</label>
                                <p style="font-weight: bold; font-size: large;" id="totalCS">$00.00</p>
                            </div>
                        </div>
                        <br>
                        <div class="row col-md-12">
                                <div class="col-md-12 table-repsonsive">
                                    <span id="error"></span>
                                    <table class="table" id="item_table">
                                        <thead class="table-general">
                                            <tr>
                                                <th>Producto</th>
                                                <th>Cantidad Envase</th>
                                                <th>Existencia (Unidades)</th>
                                                <th>Existencia (Cantidad)</th>
                                                <th>Tipo Traspaso</th>
                                                <th>Cantidad a Traspasar</th>
                                                <th>Precio Base</th>
                                                <th><button type="button" name="add" class="btn btn-primary-dark btn-sm addEE"><span class="glyphicon glyphicon-plus"></span></button></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <hr>
                                <button type="submit" id="btnSaveTransferEE" class="btn btn-primary" style="float:right">Guardar Traspaso</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('personal-js')

    <script type="text/javascript" src="{{ URL::asset('js/build/transferCreateEE.js') }}"></script>

    <script>
        $(".productsList-single").select2();
        $(".employeesListDestiny-single").select2();
        $(".employeesListOrigin-single").select2();
    </script>

@endsection