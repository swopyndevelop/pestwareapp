@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Actualizar mi plan</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-primary" @if($company->id_plan == 3) style="pointer-events: none; opacity: 0.5; background: #CCC;" @endif>
                                    <div class="panel-heading">
                                        <i class="fa fa-university" aria-hidden="true"></i>
                                        <span class="text-bold">Emprendedor</span>
                                    </div>
                                    <div class="panel-body text-center">
                                        <span class="text-primary" style="font-size: 3em;">$499</span><span class="text-secondary"> MXN/29 USD </span><span class="label label-default">mensual</span>
                                        <div class="list-group">
                                            <a href="#" class="list-group-item padding-panel-plans">Cursos</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Cotizador</a>
                                            <a href="#" class="list-group-item padding-panel-plans">App Móvil</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Clientes</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Registro de Gastos <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Reportes Inteligentes <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Cortes Administrativos <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Control de Inventarios <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Monitoreo de Estaciones <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Programación de Servicios</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Envío de mensajes por Whatsapp <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Recordatorio Refuerzos y Recurrentes <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Registro de Gastos</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Soporte ilimitado</a>
                                            <a href="#" class="list-group-item padding-panel-plans">--</a>
                                            <a href="#" class="list-group-item padding-panel-plans">--</a>
                                            <a href="#" class="list-group-item padding-panel-plans">--</a>
                                            <a href="#" class="list-group-item padding-panel-plans">--</a>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-secondary text-bold text-left">Usuarios Extras:</p>
                                                    <button id="addUser" class="btn btn-sm btn-primary-outline pull-left">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <button id="minusUser" class="btn btn-sm btn-primary-outline pull-left">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button> <b><span id="labelUsers" class="badge">3 Usuarios</span></b>
                                                </div>
                                                <div class="col-md-6 pull-right text-right">
                                                    <span id="totalUsers" class="text-secondary" style="font-size: 1.1em;">Usuarios: $0.00</span><br>
                                                    <span id="totalCenters" class="text-secondary" style="font-size: 1.1em;">Sucursales: $0.00</span><br>
                                                    <span id="subtotal" class="text-secondary" style="font-size: 1.1em;">Subtotal: $499</span><br>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-secondary text-bold text-left">Sucursales Extras:</p>
                                                    <button id="addCenter" class="btn btn-sm btn-primary-outline pull-left">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <button id="minusCenter" class="btn btn-sm btn-primary-outline pull-left">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button> <b><span id="labelCenters" class="badge">0 Sucursal</span></b>
                                                </div>
                                                <div class="col-md-6 pull-right text-right">
                                                    <span id="totalIVA" class="text-secondary" style="font-size: 1.1em;">IVA: $79.84</span><br>
                                                    <span id="totalPlan1" class="text-primary" style="font-size: 2em;">Total: $578.84</span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row text-center">
                                            @if($company->id_stripe_customer == null || $company->id_plan == 3)
                                                <button id="btnActionA" class="btn btn-primary">Seleccionar Plan</button>
                                                <br>
                                            @else
                                                <button id="btnActionA" class="btn btn-primary">
                                                    <i class="fa fa-credit-card-alt" aria-hidden="true"></i> Método de Pago / Descargar Recibos
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-primary" @if($company->id_plan == 2) style="pointer-events: none; opacity: 0.5; background: #CCC;" @endif>
                                    <div class="panel-heading">
                                        <i class="fa fa-building-o" aria-hidden="true"></i>
                                        <span class="text-bold">Empresarial</span>
                                    </div>
                                    <div class="panel-body text-center">
                                        <span class="text-primary" style="font-size: 3em;">$899</span><span> MXN/49 USD </span><span class="label label-default">mensual</span>
                                        <div class="list-group">
                                            <a href="#" class="list-group-item padding-panel-plans">Cursos</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Cotizador</a>
                                            <a href="#" class="list-group-item padding-panel-plans">App Móvil</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Clientes</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Portal de Clientes <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Registro de Gastos</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Reportes Inteligentes</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Cortes Administrativos</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Control de Inventarios</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Monitoreo de Áreas <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Monitoreo de Estaciones</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Programación de Servicios</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Envío de mensajes por Whatsapp</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Recordatorio Refuerzos y Recurrentes</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Programación Inteligente Multisucursal <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Optimización de Rutas por Google Maps <i class="fa fa-check text-primary" aria-hidden="true"></i></a>
                                            <a href="#" class="list-group-item padding-panel-plans">Registro de Gastos</a>
                                            <a href="#" class="list-group-item padding-panel-plans">Soporte ilimitado</a>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-secondary text-bold text-left">Usuarios Extras:</p>
                                                    <button id="addUserB" class="btn btn-sm btn-primary-outline pull-left">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <button id="minusUserB" class="btn btn-sm btn-primary-outline pull-left">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button> <b><span id="labelUsersB" class="badge">3 Usuarios</span></b>
                                                </div>
                                                <div class="col-md-6 pull-right text-right">
                                                    <span id="totalUsersB" class="text-secondary" style="font-size: 1.1em;">Usuarios: $0.00</span><br>
                                                    <span id="totalCentersB" class="text-secondary" style="font-size: 1.1em;">Sucursales: $0.00</span><br>
                                                    <span id="subtotalB" class="text-secondary" style="font-size: 1.1em;">Subtotal: $899</span><br>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-secondary text-bold text-left">Sucursales Extras:</p>
                                                    <button id="addCenterB" class="btn btn-sm btn-primary-outline pull-left">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <button id="minusCenterB" class="btn btn-sm btn-primary-outline pull-left">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button> <b><span id="labelCentersB" class="badge">0 Sucursal</span></b>
                                                </div>
                                                <div class="col-md-6 pull-right text-right">
                                                    <span id="totalIVAB" class="text-secondary" style="font-size: 1.1em;">IVA: $143.84</span><br>
                                                    <span id="totalPlan1B" class="text-primary" style="font-size: 2em;">Total: $1042.84</span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row text-center">
                                            @if($company->id_stripe_customer == null || $company->id_plan == 2)
                                                <button id="btnActionB" class="btn btn-primary">Seleccionar Plan</button>
                                                <br>
                                            @else
                                                <button id="btnActionB" class="btn btn-primary">
                                                    <i class="fa fa-credit-card-alt" aria-hidden="true"></i> Método de Pago / Descargar Recibos
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/billingCheckout.js') }}"></script>
@endsection