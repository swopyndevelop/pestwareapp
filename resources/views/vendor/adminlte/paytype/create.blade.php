@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Tipos de pago</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						
						@include('adminlte::layouts.partials._errors')

						<form action="{{ Route('paytype_add') }}" method="POST" role="form">

							{{ csrf_field() }}

							<legend>Nuevo tipo de pago</legend>

							<div class="form-group">
								<label for="Nombre">Nombre</label>
								<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Ej. Efectivo .." value="{{ old('Nombre')}}">
							</div>
							<button type="submit" class="btn btn-primary">Crear Caso</button>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
