<div class="form-horizontal panel-body" id="_general_inf">
	<div class="principal-container">
		<div class="col-lg-6">
			<label>
				Horario Inicial
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_horarioinicial"></span>
				<input type="time" class="form-control text-center" name="HorarioInicial" id="HorarioInicial" style="border-radius: 8px" value="@if(isset($general)){{$general->hour_init or old('hour_init')}}@endif">
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Horario Final
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_horariofinal"></span>
				<input type="time" class="form-control text-center" name="HorarioFinal" id="HorarioFinal" style="border-radius: 8px" value="@if(isset($general)){{$general->hour_final or old('hour_final')}}@endif">
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Inicio de días laborales
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_inicioDias"></span>
				<select name="DiasLaboralesInicio" id="DiasLaboralesInicio" class="form-control">
					<option value="">Seleccione</option>
					<option value="Lunes"@if (isset($general)) ? ('Lunes' == $general->start_workdays) selected = 'selected' : ' ' @endif>Lunes</option>
					<option value="Martes"@if (isset($general)) ? ('Martes' == $general->start_workdays) selected = 'selected' : ' ' @endif>Martes</option>
					<option value="Miercoles"@if (isset($general)) ? ('Miercoles' == $general->start_workdays) selected = 'selected' : ' ' @endif>Miércoles</option>
					<option value="Jueves"@if (isset($general)) ? ('Jueves' == $general->start_workdays) selected = 'selected' : ' ' @endif>Jueves</option>
					<option value="Viernes"@if (isset($general)) ? ('Viernes' == $general->start_workdays) selected = 'selected' : ' ' @endif>Viernes</option>
					<option value="Sabado"@if (isset($general)) ? ('Sabado' == $general->start_workdays) selected = 'selected' : ' ' @endif>Sábado</option>
					<option value="Domingo"@if (isset($general)) ? ('Domingo' == $general->start_workdays) selected = 'selected' : ' ' @endif>Domingo</option>
				</select>
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Final de días laborales
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_finalDias"></span>
				<select name="DiasLaboralesFinal" id="DiasLaboralesFinal" class="form-control">
					<option value="">Seleccione</option>
					<option value="Lunes"@if (isset($general)) ? ('Lunes' == $general->final_workdays) selected = 'selected' : ' ' @endif>Lunes</option>
					<option value="Martes"@if (isset($general)) ? ('Martes' == $general->final_workdays) selected = 'selected' : ' ' @endif>Martes</option>
					<option value="Miercoles"@if (isset($general)) ? ('Miercoles' == $general->final_workdays) selected = 'selected' : ' ' @endif>Miércoles</option>
					<option value="Jueves"@if (isset($general)) ? ('Jueves' == $general->final_workdays) selected = 'selected' : ' ' @endif>Jueves</option>
					<option value="Viernes"@if (isset($general)) ? ('Viernes' == $general->final_workdays) selected = 'selected' : ' ' @endif>Viernes</option>
					<option value="Sabado"@if (isset($general)) ? ('Sabado' == $general->final_workdays) selected = 'selected' : ' ' @endif>Sábado</option>
					<option value="Domingo"@if (isset($general)) ? ('Domingo' == $general->final_workdays) selected = 'selected' : ' ' @endif>Domingo</option>
				</select>
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Fondo fijo o caja chica
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_fondofijo"></span>
				<input type="number" class="form-control text-center" name="Fondo" id="Fondo" style="border-radius: 8px" max="999999" min="100" placeholder="2" step=".01" value="@if(isset($general)){{ $general->small_box or old('small_box') }}@endif">
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Archivo Fondo fijo
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_archivofondo"></span>
				<input type="file" class="form-control text-center" name="ArchivoFondo" id="ArchivoFondo" style="border-radius: 8px">
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Punto de equilibrio
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_equilibrio"></span>
				<input type="number" class="form-control text-center" name="Equilibrio" id="Equilibrio" style="border-radius: 8px" max="999999" min="100" placeholder="00000" step=".01" value="{{ $general->breakeven or old('breakeven') }}">
			</label>
		</div>
		<div class="col-lg-6">
			<label >
				Contrato de renta
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_contratorenta"></span>
				<input type="file" class="form-control text-center" name="ContratoRenta" id="ContratoRenta" style="border-radius: 8px">
			</label>
		</div>
		<div class="col-lg-6">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px;">Vigencia</label>
			<input type="checkbox" id="chk_1" name="chk_1" onclick="enable()" style="margin-bottom: 0; margin-top: 10px; padding-bottom: 0;">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px; margin-left: 200px;">Sin vigencia</label>
			<label>
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_vigenciarenta"></span>
				<input type="date" class="form-control text-center" name="VigenciaRenta" id="VigenciaRenta" min="2017-01-01" max="2050-12-31" style="border-radius: 8px; margin-top: 0; padding-top: 0;" value="@if(isset($general)){{ $general->validity_rent_contract or old('validity_rent_contract') }}@endif">
			</label>
			<script type="text/javascript">
				function enable(){
					check = document.getElementById("chk_1");
					fecha = document.getElementById("VigenciaRenta");
					try{
						if (check.checked) {
    						fecha.disabled = true;
  						} else {
    						fecha.disabled = false;
  						}
					}catch(e){
					}
				}
			</script>
		</div>
		<div class="col-lg-6">
			<label>
				R-1 Hacienda
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_hacienda"></span>
				<input type="file" class="form-control text-center" name="Hacienda" id="Hacienda" style="border-radius: 8px">
			</label>
		</div>
		<div class="col-lg-6">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px;">Vigencia</label>
			<input type="checkbox" id="chk_2" name="chk_2" onclick="enable2()" style="margin-bottom: 0; margin-top: 10px; padding-bottom: 0;">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px; margin-left: 200px;">Sin vigencia</label>
			<label>
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_vigenciahacienda"></span>
				<input type="date" class="form-control text-center" name="VigenciaHacienda" id="VigenciaHacienda" min="2017-01-01" max="2050-12-31" style="border-radius: 8px; margin-top: 0; padding-top: 0;" value="@if(isset($general)){{ $general->validity_rent_contract or old('validity_hacienda') }}@endif">
			</label>
			<script type="text/javascript">
				function enable2(){
					check = document.getElementById("chk_2");
					fecha = document.getElementById("VigenciaHacienda");
					try{
						if (check.checked) {
    						fecha.disabled = true;
  						} else {
    						fecha.disabled = false;
  						}
					}catch(e){
					}
				}
			</script>
		</div>
		<div class="col-lg-6">
			<label>
				Permiso comercial
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_permisocomercial"></span>
				<input type="file" class="form-control text-center" name="PermisoComercial" id="PermisoComercial" style="border-radius: 8px">
			</label>
		</div>
		<div class="col-lg-6">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px;">Vigencia</label>
			<input type="checkbox" id="chk_3" name="chk_3" onclick="enable3()" style="margin-bottom: 0; margin-top: 10px; padding-bottom: 0;">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px; margin-left: 200px;">Sin vigencia</label>
			<label>
			<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_vigenciapermiso"></span>
			<input type="date" class="form-control text-center" name="VigenciaPermiso" id="VigenciaPermiso" min="2017-01-01" max="2050-12-31" style="border-radius: 8px; margin-top: 0; padding-top: 0;" value="@if(isset($general)){{ $general->validity_commercial_permission or old('validity_commercial_permission') }}@endif">
			</label>
			<script type="text/javascript">
				function enable3(){
					check = document.getElementById("chk_3");
					fecha = document.getElementById("VigenciaPermiso");
					try{
						if (check.checked) {
    						fecha.disabled = true;
  						} else {
    						fecha.disabled = false;
  						}
					}catch(e){
					}
				}
			</script>
		</div>
		<div class="col-lg-6">
			<label>
				Última fumigación
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_fumigacion"></span>
				<input type="file" class="form-control text-center" name="Fumigacion" id="Fumigacion" style="border-radius: 8px">
			</label>
		</div>
		<div class="col-lg-6">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px;">Vigencia</label>
			<input type="checkbox" id="chk_4" name="chk_4" onclick="enable4()" style="margin-bottom: 0; margin-top: 10px; padding-bottom: 0;">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px; margin-left: 200px;">Sin vigencia</label>
			<label>
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_vigenciafumigacion"></span>
				<input type="date" class="form-control text-center" name="VigenciaFumigacion" id="VigenciaFumigacion" min="2017-01-01" max="2050-12-31" style="border-radius: 8px; margin-top: 0; padding-top: 0;" value="@if(isset($general)){{ $general->validity_fumigation or old('validity_fumigation') }}@endif">
			</label>
			<script type="text/javascript">
				function enable4(){
					check = document.getElementById("chk_4");
					fecha = document.getElementById("VigenciaFumigacion");
					try{
						if (check.checked) {
    						fecha.disabled = true;
  						} else {
    						fecha.disabled = false;
  						}
					}catch(e){
					}
				}
			</script>
		</div>
		<div class="col-lg-6">
			<label>
				Carga extintor
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_extintor"></span>
				<input type="file" class="form-control text-center" name="Extintor" id="Extintor" style="border-radius: 8px">
			</label>
		</div>
		<div class="col-lg-6">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px;">Vigencia</label>
			<input type="checkbox" id="chk_5" name="chk_5" onclick="enable5()" style="margin-bottom: 0; margin-top: 10px; padding-bottom: 0;">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px; margin-left: 200px;">Sin vigencia</label>
			<label>
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_vigenciaextintor"></span>
				<input type="date" class="form-control text-center" name="VigenciaExtintor" id="VigenciaExtintor" min="2017-01-01" max="2050-12-31" style="border-radius: 8px; margin-top: 0; padding-top: 0;" value="@if(isset($general)){{ $general->validity_extinguisher or old('validity_extinguisher') }}@endif">
			</label>
			<script type="text/javascript">
				function enable5(){
					check = document.getElementById("chk_5");
					fecha = document.getElementById("VigenciaExtintor");
					try{
						if (check.checked) {
    						fecha.disabled = true;
  						} else {
    						fecha.disabled = false;
  						}
					}catch(e){
					}
				}
			</script>
		</div>
		<div class="col-lg-6">
			<label>
				Permiso salubridad
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_salubridad"></span>
				<input type="file" class="form-control text-center" name="Salubridad" id="Salubridad" style="border-radius: 8px">
			</label>
		</div>
		<div class="col-lg-6">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px;">Vigencia</label>
			<input type="checkbox" id="chk_6" name="chk_6" onclick="enable6()" style="margin-bottom: 0; margin-top: 10px; padding-bottom: 0;">
			<label class="col-lg-3" style="margin-bottom: 0; padding-bottom: 0; margin-top: 0; padding-top: 8px; margin-left: 200px;">Sin vigencia</label>
			<label>
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_vigenciasalubridad"></span>
				<input type="date" class="form-control text-center" name="VigenciaSalubridad" id="VigenciaSalubridad" min="2017-01-01" max="2050-12-31" style="border-radius: 8px; margin-top: 0; padding-top: 0;" value="@if(isset($general)){{ $general->validity_salubrity or old('validity_salubrity') }}@endif">
			</label>
			<script type="text/javascript">
				function enable6(){
					check = document.getElementById("chk_6");
					fecha = document.getElementById("VigenciaSalubridad");
					try{
						if (check.checked) {
    						fecha.disabled = true;
  						} else {
    						fecha.disabled = false;
  						}
					}catch(e){
					}
				}
			</script>
		</div>
		<div class="form-group text-center col-xs-12" align="center">
			<br>
			<button type="button" class="btn btn-primary next-step btn-lg" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="guardar_general_info">Siguiente
			</button>
		</div>
	</div>
</div>
