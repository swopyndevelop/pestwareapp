<div class="form-horizontal panel-body">
	<div class="principal-container">
		<div class="col-lg-10 col-lg-offset-1" style="height: unset;overflow-x:auto;">
			<table class="table table-striped col-md-10" id="tablaMetas" style="margin-bottom: 0px;">
				<thead style="height: unset;">
					<tr style="color: black">
						<th class="col-md-4 text-center col-md-pull-2"><label>MES</label></th>
						<th class="col-md-4 text-center" id="indicador"><label>INDICADOR</label></th>
					</tr>
				</thead>
				<tbody  id="contenedor" >
					<tr >
						<td class="text-center" style="color: #f5216e"><label>Enero <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_enero"></span></label></td>
						<td class="text-center"><label>
							<input id="numero1" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo1" value="{{ $goals->value_january or old('value_january') }}" ></label>
						</td>
					</tr>
					<tr>
						<td class="text-center" style="color: #f5216e"><label>Febrero<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_febrero"></span></label></td>
						<td class="text-center"><label>
							<input id="numero2" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo2" value="{{ $goals->value_february or old('value_february') }}" ></label>
						</td>
					</tr>
					<tr>
						<td class="text-center" style="color: #f5216e"><label> Marzo <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_marzo"></span></label></td>
						<td class="text-center"><label>
							<input id="numero3" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo3" value="{{ $goals->value_march or old('value_march') }}"></label>
						</td>
					</tr>
					<tr>
						<td class="text-center" style="color: #f5216e"><label>Abril<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_abril"></span></label></td>
						<td class="text-center"><label>
							<input id="numero4" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo4" value="{{ $goals->value_april or old('value_april') }}">
						</label>
					</td>
				</tr>
				<tr>
					<td class="text-center" style="color: #f5216e"><label> Mayo <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_mayo"></span> </label></td>
					<td class="text-center"><label>	
						<input id="numero5" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo5" value="{{ $goals->value_may or old('value_may') }}"></label>
					</td>
				</tr>
				<tr>
					<td class="text-center" style="color: #f5216e"><label> Junio <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_junio"></span> </label> </td>
					<td class="text-center"><label>
						<input id="numero6" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo6" value="{{ $goals->value_june or old('value_june') }}"></label>
					</td>
				</tr>
				<tr>
					<td class="text-center" style="color: #f5216e"><label> Julio <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_julio"></span> </label></td>
					<td class="text-center"><label>
						<input id="numero7" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo7" value="{{ $goals->value_july or old('value_july') }}">
					</label>
				</td>
			</tr>
			<tr>
				<td class="text-center" style="color: #f5216e"><label> Agosto 	<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_agosto"></span> </label></td>
				<td class="text-center"><label>
					<input id="numero8" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo8" value="{{ $goals->value_august or old('value_august') }}"></label>
				</td>
			</tr>
			<tr>
				<td class="text-center" style="color: #f5216e"><label> Septiembre <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_septiembre"></span></label></td>
				<td class="text-center"><label>
					<input id="numero9" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo9" value="{{ $goals->value_september or old('value_september') }}"></label>
				</td>
			</tr>
			<tr>
				<td class="text-center" style="color: #f5216e"><label> Octubre <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_octubre"></span></label></td>
				<td class="text-center"><label>
					<input id="numero10" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo10" value="{{ $goals->value_october or old('value_october') }}"></label>
				</td>
			</tr>
			<tr>
				<td class="text-center" style="color: #f5216e"><label> Noviembre 						<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_noviembre"></span></label></td>
				<td class="text-center"><label>
					<input id="numero11" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo11" value="{{ $goals->value_november or old('value_november') }}"></label>
				</td>
			</tr>
			<tr>
				<td class="text-center" style="color: #f5216e"><label> Diciembre 						<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_diciembre"></span> </label></td>
				<td class="text-center"><label>
					<input id="numero12" size="20" type="number" min="0" style="border-radius: 8px" class="col-md-8 col-md-push-3 sumas" step=".1" id="txt_campo12" value="{{ $goals->value_december or old('value_december') }}"></label>
				</td>
			</tr>
			<tr>
				<td class="text-center">
					<strong><span style="font-size: 2em;color: black">TOTAL</span></strong>
				</td>
				<td class="text-center">
					<strong><span style="font-size: 2em;color: black">$</span></strong>
					<span id="spTotal" style="font-size: 2em">{{ $goals->value_spTotal or old('value_spTotal')}}</span>
				</td>
			</tr>
		</tbody>
	</label>
</table>
<div class="form-group" align="center">
	<br>
	<button type="button" class="btn btn-primary btn-lg" id="guardar_goals" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em">Finalizar</button>
</div>
</div>
</div>
</div>
