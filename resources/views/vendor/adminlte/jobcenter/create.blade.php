@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('personal_style')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
<style> 
.principal-container 
{ 
    width: 100%; 
} 
.error{
    font-size: .8em;
}

.principal-container > div > label 
{ 
    text-align: left; 
    display: block; 
    padding: 1.1em 1.1em .6em; 
    font-size: 1em; 
    color: #f5216e; 
    cursor: pointer; 
} 
.principal-container > [class^='col-'] {  
    border: 1px solid #E4E4E4;  
    height: 6em; 
}   
.ok, .principal-container > div > label > input, .principal-container > div > label > select, .principal-container > div > label > center > input 
{ 
    display: inline-block; 
    position: relative; 
    width: 100%; 
    margin: 5px -5px 0; 
    padding: 7px 5px 3px; 
    border: none; 
    outline: none; 
    background: transparent; 
    font-size: 1.3em; 
    color: #000; 
} 
.principal-container > div > label > center > input{ 
    width: 3.25em; 
} 
@media screen and (max-width: 480px) { 
    .principal-container > div > label{ 
      font-size: .8em; 
  } 
  .ok, .principal-container > div > label > input, .principal-container > div > label > select, .principal-container > div > label > center > input{ 
      font-size: 1.1em; 
  }   
}
#map {
    height: 470px ;
    width: 100% ;
} 
</style>
@stop
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
                <div class="box-header with-border">
                    <h4 class="titleCenter" style="">ESTRUCTURA DE CENTROS DE TRABAJO</h4>
                </div>
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="active" style="margin-left: 10%; width: 16%" data-toggle="tooltip" title="Perfil">
                            <a data-toggle="tab" href="#profileTab" id="TabProfile">
                                <div class="row text-center">
                                    <i style="color: black" class="fa fa-user-o fa-4x" aria-hidden="true"></i>
                                </div>
                            </a>
                        </li>
                        <li style="width: 16%" data-toggle="tooltip" title="Domicilio">
                            <a data-toggle="tab" href="#domicilioTab" id="Tabdomicilio">
                                <div class="row text-center">
                                   <i style="color: black" class="fa fa-address-book fa-4x" aria-hidden="true"></i>
                               </div>
                           </a>
                       </li>
                       <li style="width: 16%" data-toggle="tooltip" title="Capacidad Laboral">
                        <a data-toggle="tab" href="#CapacidadLaboraltab" id="TabCapacidadLaboral">
                            <div class="row text-center">
                                <i style="color: black" class="fa fa-users fa-4x" aria-hidden="true"></i>
                            </div>
                        </a>
                    </li>
                    <li style="width: 16%" data-toggle="tooltip" title="Información General">
                        <a data-toggle="tab" href="#InformaciongeneralTab" id="TabInformaciongeneral">
                            <div class="row text-center">
                                <i style="color: black" class="fa fa-info fa-4x" aria-hidden="true"></i>
                            </div>
                        </a>
                    </li>
                    <li style="width: 16%" data-toggle="tooltip" title="Metas">
                        <a data-toggle="tab" href="#MetasTab" id="TabMetas">
                            <div class="row text-center">
                                <i style="color: black" class="fa fa-flag-checkered fa-4x" aria-hidden="true"></i>
                            </div>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <input type="hidden" id="jobcenter_id" name="jobcenter_id" value="{{ $jobcenter->profile_job_centers_id}}">
                    <div class="tab-pane fade in active" id="profileTab">
                        @include('adminlte::jobcenter._profile')
                    </div>
                    <div class="tab-pane fade " id="domicilioTab">
                        @include('adminlte::jobcenter._address')
                    </div>
                    <div class="tab-pane fade " id="CapacidadLaboraltab">
                        @include('adminlte::jobcenter._workcapacity')
                    </div>
                    <div class="tab-pane fade " id="InformaciongeneralTab">
                        @include('adminlte::jobcenter._generalinformation')
                    </div>
                    <div class="tab-pane fade " id="MetasTab">
                     @include('adminlte::jobcenter._goals')
                 </div>    
             </div>
             <!--box-body-->
         </div>
     </div>
     <!--.box-->
 </div>
</div>
</div>
@endsection
@section('personal-js')
<script >
    var marker;          //variable del marcador
    var coords = {};    //coordenadas obtenidas con la geolocalización
    var map;

    function getLocation() {
        if ($('#Ubicacion').val() == ""){
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lng: position.coords.longitude,
                        lat: position.coords.latitude
                    };
                    setMapa(pos);
                });
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
    }

    function address (){
        var address = $('#address').val();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address}, geocodeResult);  
    }

    function geocodeResult(results, status) {
        // Verificamos el estatus
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);
            marker.setPosition(results[0].geometry.location);
        } else {
        	alert("Geocoding no tuvo éxito debido a: " + status);
        }
    }
    
    function setMapa (coords){   
        //Se crea una nueva instancia del objeto mapa
        map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 13,
            center:new google.maps.LatLng(coords.lat,coords.lng),
        });
        //Creamos el marcador en el mapa con sus propiedades
        //para nuestro obetivo tenemos que poner el atributo draggable en true
        //position pondremos las mismas coordenas que obtuvimos en la geolocalización
        marker = new google.maps.Marker({
        	map: map,
        	draggable: true,
        	animation: google.maps.Animation.DROP,
        	position: new google.maps.LatLng(coords.lat,coords.lng),
        });
        //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica 
        //cuando el usuario a soltado el marcador
        listeners(marker);
    }
    //callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
    function listeners(marcador)
    {
        google.maps.event.addListenerOnce(map, 'idle', function(){
            document.getElementById("Ubicacion").value = marcador.getPosition().lat()+","+ marcador.getPosition().lng();
        });
        marcador.addListener( 'position_changed', function (event)
        {
          //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
          document.getElementById("Ubicacion").value = this.getPosition().lat()+","+ this.getPosition().lng();
      });
    }

    function toggleBounce()     
    {
    	document.getElementById("coords").value = this.getPosition().lat()+","+ this.getPosition().lng();
    	if (marker.getAnimation() !== null) {
    		marker.setAnimation(null);
    	} else {
    		marker.setAnimation(google.maps.Animation.BOUNCE);
    	}
    }
// Carga de la libreria de google maps 
</script>
<script>
    $('.delete_Puesto').click(function() {
        // deleteLan();
        var Puesto = $(this).parent('td').parent('tr');

        $.ajax({
            type: "POST",
            url: "{{ route('workcapacity_job_centers_delete') }}",
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                Id : $(this).attr("data-info")
            },
            success: function(data){
                Puesto.remove();
            }
        });
    });
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDldpjKxaPoyGu2NLMSH1tfvqIfsZg0eqA" type="text/javascript"></script>
@stop