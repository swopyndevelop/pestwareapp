<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="{{ asset('/js/jquery.min.js') }}"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <title>Ubicación Modal</title>
  <style>
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
  #map {
    height: 450px ;
    width: 100% ;
  }
</style>
</head>
<body>
  <!-- Button trigger modal -->
  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" id="btn-activo">
    <span class="glyphicon glyphicon-map-marker"></span>
  </button>
  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ubicacion</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Aguascalientes" id="address">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="address_search">Search</button>
              </span>
            </div><!-- /input-group -->
            <br>
          </div>
          <div class="row">
           <div id="map"></div>
         </div>
         <input  class="form-control text-center" type="text" id="coords" readonly/>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" style="background-color: black; border-color: black;" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</body>
<script>
    var marker;          //variable del marcador
    var coords = {};    //coordenadas obtenidas con la geolocalización
    var map;
    
    $('#address_search').click(function(){
      var address = $('#address').val();
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': address}, geocodeResult);  
    });
    function geocodeResult(results, status) {
    // Verificamos el estatus
    if (status == 'OK') {
      var mapOptions = {
        center: results[0].geometry.location,
        zoom: 13,
      };
      map = new google.maps.Map($("#map").get(0), mapOptions);
      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: results[0].geometry.location,
      });
      listeners(marker);
    } else {
      alert("Geocoding no tuvo éxito debido a: " + status);
    }
  }

  $('#btn-activo').click(function(){
    setTimeout(
      function(){
        $.get("https://ipinfo.io", function(response) {
          var loc = response.loc.split(',');
          coords = {
            lng: loc[1],
            lat: loc[0]
          };
          setMapa(coords);
        }, "jsonp");
      },0);
  });
  function setMapa (coords)
  {   
        //Se crea una nueva instancia del objeto mapa
        map = new google.maps.Map(document.getElementById('map'),
        {
          zoom: 13,
          center:new google.maps.LatLng(coords.lat,coords.lng),

        });

        //Creamos el marcador en el mapa con sus propiedades
        //para nuestro obetivo tenemos que poner el atributo draggable en true
        //position pondremos las mismas coordenas que obtuvimos en la geolocalización
        marker = new google.maps.Marker({
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(coords.lat,coords.lng),
        });
        //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica 
        //cuando el usuario a soltado el marcador
        listeners(marker);
      }
    //callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE

    function listeners(marcador){
      marcador.addListener('click', toggleBounce);

      marcador.addListener( 'dragend', function (event)
      {
          //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
          document.getElementById("coords").value = this.getPosition().lat()+","+ this.getPosition().lng();
        });
    }
    function toggleBounce() 
    {
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }
// Carga de la libreria de google maps 
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDldpjKxaPoyGu2NLMSH1tfvqIfsZg0eqA" type="text/javascript"></script>
</html>
