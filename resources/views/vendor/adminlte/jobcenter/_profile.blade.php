<div class="form-horizontal panel-body" id="_profile_center">
	<div class="principal-container">
		<div class="col-lg-6">
			<label>
				Nombre <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
				<input type="text" class="form-control" placeholder="Nombre Centro de trabajo" name="NombreJobCenter" id="NombreJobCenter" value="{{ $jobcenter->name or old('NombreJobCenter') }}" disabled="disabled">
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Código del centro de trabajo <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_codigojobcenter"></span>
				<input type="text" class="form-control" id="CodigoJobCenter" placeholder="Código" name="CodigoJobCenter" value="{{ $jobcenter->code or old('CodigoJobCenter') }}">
			</label>
		</div>
		</span>
		<div class="col-lg-6">
			<label>
			Imagen del Centro de trabajo:
			<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_imagen"></span>
			<input type="file" class="form-control" id="Imagen" name="Imagen" value="{{ $jobcenter->image or old('Imagen') }}">
			</label>
		</div>
		<div class="col-lg-6">
			<label>Tipo de centro:
				<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_tipopuesto"></span>
				<select name="TipoPuesto" id="TipoPuesto" class="form-control">
					<option value="">Selecccione una opción...</option>
					<option value="Almacen" @if ('Almacen' == $jobcenter->type) selected='selected' @endif>Almacén</option>
					<option value="Centro_de_distribucion"@if ('Centro_de_distribucion' == $jobcenter->type) selected='selected' @endif>Centro de distribución</option>
					<option value="Sucursal"@if ('Sucursal' == $jobcenter->type) selected='selected' @endif>Sucursal</option>
					<option value="Oficina"@if ('Oficina' == $jobcenter->type) selected='selected' @endif>Oficina</option>
					<!-- <option value="CEDIS"@if ('Cedis' == $jobcenter->type) selected='selected' @endif>CEDIS</option> -->
				</select>
			</label>
		</div>
		<div class="form-group text-center col-xs-12">
			<br>
			<button type="button" class="btn btn-primary btn-lg" onclick="" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="guardar_perfil_jobcenter">Siguiente </button>
		</div>
	</div>
</div>
