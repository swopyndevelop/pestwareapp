<div class="form-horizontal panel-body" id="_profile_address">
	<div class="principal-container">
		<div class="col-lg-4">
			<label>
				Calle<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_calle"></span>
				<input type="text" class="form-control" placeholder="Calle" name="Calle" id="Calle" value="@if(isset($jobaddress)) {{ $jobaddress->street or old('street') }} @endif">
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Número exterior
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_exterior"></span>
				<input type="number" class="form-control" placeholder="Número exterior" name="NumExt" id="NumExt" min="1" max="9999" maxlength="5" value="@if(isset($jobaddress)){{$jobaddress->num_ext or old('num_ext')}}@endif">
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Número interior
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_interior"></span>
				<input type="text" class="form-control" placeholder="Número interior" name="NumInt" id="NumInt" maxlength="50" value="@if(isset($jobaddress)){{$jobaddress->num_int or old('num_int')}}@endif">
			</label>		
		</div>
		<div class="col-lg-5">
			<label>
				Colonia
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_colonia"></span>
				<input type="text" class="form-control" placeholder="Colonia" name="Colonia" id="Colonia" value="@if(isset($jobaddress)) {{ $jobaddress->district or old('colonia') }} @endif">
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Código postal
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_cp"></span>
				<input type="text" class="form-control" placeholder="Código postal" name="CodigoPostal" id="CodigoPostal" maxlength="5" value="@if(isset($jobaddress)) {{ $jobaddress->zip_code or old('zip_code') }} @endif">
			</label>
		</div>	
		<div class="col-lg-3">
			<label>
				Localidad
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_localidad"></span>
				<input type="text" class="form-control" placeholder="Localidad" name="Localidad" id="Localidad" value="@if(isset($jobaddress)) {{ $jobaddress->location or old('location') }} @endif">
			</label>
		</div>	
		<div class="col-lg-3">
			<label>
				Municipio
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_municipio"></span>
				<input type="text" class="form-control" placeholder="Municipio" name="Municipio" id="Municipio" value="@if(isset($jobaddress)) {{ $jobaddress->municipality or old('municipality') }} @endif">
			</label>
		</div>
		<div class="col-lg-3">
			<label>
				Estado
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_estado"></span>
				<select id="Estado" name="Estado" class="form-control" data-live-search="true" required="true" value="{{ $jobaddress->state or old('state') }}">
					<option value="">Seleccione una opción</option>
					<option value="Aguascalientes" @if (isset($jobaddress)) @if('Aguascalientes' == $jobaddress->state) selected ='selected' @endif @endif>Aguascalientes</option>
					<option value="Baja California"@if (isset($jobaddress)) @if('Baja California' === $jobaddress->state) selected ='selected' @endif @endif>Baja California</option>
					<option value="Baja California Sur"@if (isset($jobaddress)) @if('Baja California Sur' === $jobaddress->state) selected = 'selected' @endif @endif>Baja California Sur</option>
					<option value="Campeche"@if (isset($jobaddress)) @if('Campeche' == $jobaddress->state) selected = 'selected' @endif @endif>Campeche</option>
					<option value="Coahuila de Zaragoza"@if (isset($jobaddress)) @if('Coahuila de Zaragoza' === $jobaddress->state) selected = 'selected' @endif @endif>Coahuila de Zaragoza</option>
					<option value="Colima"@if (isset($jobaddress)) @if('Colima' == $jobaddress->state) selected = 'selected' @endif @endif>Colima</option>
					<option value="Chiapas"@if (isset($jobaddress)) @if('Chiapas' == $jobaddress->state) selected = 'selected' @endif @endif>Chiapas</option>
					<option value="Chihuahua"@if (isset($jobaddress)) @if('Chihuahua' == $jobaddress->state) selected = 'selected' @endif @endif>Chihuahua</option>
					<option value="Estado de México"@if (isset($jobaddress)) @if('Estado de México' === $jobaddress->state) selected = 'selected' @endif @endif>Estado de México</option>
					<option value="Durango"@if (isset($jobaddress)) @if('Durango' == $jobaddress->state) selected = 'selected' @endif @endif>Durango</option>
					<option value="Guanajuato"@if (isset($jobaddress)) @if('Guanajuato' == $jobaddress->state) selected = 'selected' @endif @endif>Guanajuato</option>
					<option value="Guerrero"@if (isset($jobaddress)) @if('Guerrero' == $jobaddress->state) selected = 'selected' @endif @endif>Guerrero</option>
					<option value="Hidalgo"@if (isset($jobaddress)) @if('Hidalgo' == $jobaddress->state) selected = 'selected' @endif @endif>Hidalgo</option>
					<option value="Jalisco"@if (isset($jobaddress)) @if('Jalisco' == $jobaddress->state) selected = 'selected' @endif @endif>Jalisco</option>
					<option value="México"@if (isset($jobaddress)) @if('México' == $jobaddress->state) selected = 'selected' @endif @endif>México</option>
					<option value="Michoacán de Ocampo"@if (isset($jobaddress)) @if('Michoacán de Ocampo' === $jobaddress->state) selected = 'selected' @endif @endif>Michoacán de Ocampo</option>
					<option value="Morelos"@if (isset($jobaddress)) @if('Morelos' == $jobaddress->state) selected = 'selected' @endif @endif>Morelos</option>
					<option value="Nayarit"@if (isset($jobaddress)) @if('Nayarit' == $jobaddress->state) selected = 'selected' @endif @endif>Nayarit</option>
					<option value="Nuevo León"@if (isset($jobaddress)) @if('Nuevo León' === $jobaddress->state) selected = 'selected' @endif @endif>Nuevo León</option>
					<option value="Oaxaca"@if (isset($jobaddress)) @if('Oaxaca' == $jobaddress->state) selected = 'selected' @endif @endif>Oaxaca</option>
					<option value="Puebla"@if (isset($jobaddress)) @if('Puebla' == $jobaddress->state) selected = 'selected' @endif @endif>Puebla</option>
					<option value="Querétaro"@if (isset($jobaddress)) @if('Querétaro' == $jobaddress->state) selected = 'selected' @endif @endif>Querétaro</option>
					<option value="Quintana Roo"@if (isset($jobaddress)) @if('Quintana Roo' === $jobaddress->state) selected = 'selected' @endif @endif>Quintana Roo</option>
					<option value="San Luis Potosí"@if (isset($jobaddress)) @if('San Luis Potosí' === $jobaddress->state) selected = 'selected' @endif @endif>San Luis Potosí</option>
					<option value="Sinaloa"@if (isset($jobaddress)) @if('Sinaloa' == $jobaddress->state) selected = 'selected' @endif @endif>Sinaloa</option>
					<option value="Sonora"@if (isset($jobaddress)) @if('Sonora' == $jobaddress->state) selected = 'selected' @endif @endif>Sonora</option>
					<option value="Tabasco"@if (isset($jobaddress)) @if('Tabasco' == $jobaddress->state) selected = 'selected' @endif @endif>Tabasco</option>
					<option value="Tamaulipas"@if (isset($jobaddress)) @if('Tamaulipas' == $jobaddress->state) selected = 'selected' @endif @endif>Tamaulipas</option>
					<option value="Tlaxcala"@if (isset($jobaddress)) @if('Tlaxcala' == $jobaddress->state) selected = 'selected' @endif @endif>Tlaxcala</option>
					<option value="Veracruz de Ignacio de la Llave"@if (isset($jobaddress)) @if('Veracruz de Ignacio de la Llave' === $jobaddress->state) selected = 'selected' @endif @endif>Veracruz de Ignacio de la Llave</option>
					<option value="Yucatán"@if (isset($jobaddress)) @if('Yucatán' == $jobaddress->state) selected = 'selected' @endif @endif>Yucatán</option>
					<option value="Zacatecas"@if (isset($jobaddress)) @if('Zacatecas' == $jobaddress->state) selected = 'selected' @endif @endif>Zacatecas</option>
				</select>
			</label>
		</div>	
		<div class="col-lg-3">
			<label>
				País
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_pais"></span>
				<input type="text" class="form-control" placeholder="País" name="Pais" id="Pais" value="@if(isset($jobaddress)) {{ $jobaddress->country or old('country') }} @endif">
			</label>	
		</div>
		
		<div class="col-lg-3">
			<label>
				Teléfono
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_telefono"></span>
				<input type="tel" class="form-control" placeholder="10 dígitos" name="Telefono" id="Telefono" maxlength="10" value="@if(isset($jobaddress)) {{ $jobaddress->phone or old('phone') }} @endif">
			</label>
		</div>
		<div class="col-lg-6">
			<label >
				Ubicación<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_ubicacion"></span>
				<div class="input-group">
					<input  class="form-control text-center" type="text" id="Ubicacion" name="Ubicacion" readonly placeholder="Latitud y Longitud" value="@if(isset($jobaddress)) {{ $jobaddress->ubication or old('ubication') }} @endif" width="90%" >
					<span class="input-group-btn ">
						<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal" onclick="getLocation();" id="btn-activo" >
							<span class="glyphicon glyphicon-map-marker"></span>
						</button>
					</span>
				</div>
			</label>		
		</div>
		<div class="col-lg-6">
			<label>
				Correo Electrónico
				<span class="text-center text-danger hidden" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_correo"></span>
				<input type="email" class="form-control" placeholder="example@dominio.com" name="Correo" id="Correo" value="@if(isset($jobaddress)) {{ $jobaddress->email or old('email') }} @endif">
			</label>
		</div>
		<div class="form-group text-center col-xs-12" align="right">
			<br>
			<button type="button" class="btn btn-primary btn-lg" id="guardar_address" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em">Siguiente 
			</button>
		</div>
	</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Ubicación</h4>
			</div>
			<div class="modal-body">
				<div class="row col-md-10 col-md-offset-1">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Aguascalientes" id="address">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button" id="address_search" onclick="address()" style="height: auto;">Buscar</button>
						</span>
					</div><!-- /input-group -->
					<br>
				</div>
				<div class="row">
					<div id="map"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>