<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-addinterview">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Asignación de Entrevista</h4>
				<input type="hidden" id='Applicant_id' name="Applicant_id">
			</div>
			<div class="modal-body">
				<div class="principal-container">
					<div style="border: 1px solid #E4E4E4; height: 6em;">
						<label style="text-align: left; display: block; padding: 1.1em 1.1em .6em; font-size: 1em; color: #f5216e; cursor: pointer;">
							Solicitante <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_entrevistapuesto"></span>
							<input type="text" id="interviewApplicant" disabled="true" class="form-control" style="display: inline-block; position: relative; width: 100%; margin: 5px -5px 0; padding: 7px 5px 3px; border: none; outline: none; font-size: 1.3em; color: #000;">
						</label>
					</div>
					<div style="border: 1px solid #E4E4E4; height: 6em;">
						<label style="text-align: left; display: block; padding: 1.1em 1.1em .6em; font-size: 1em; color: #f5216e; cursor: pointer;">
							Fecha
							<input type="date" id="dateInterview" name="dateInterview" class="form-control" style="display: inline-block; position: relative; width: 100%; margin: 5px -5px 0; padding: 7px 5px 3px; border: none; outline: none; font-size: 1.3em; color: #000;">
						</label>
					</div>
					<div style="border: 1px solid #E4E4E4; height: 6em;">
						<label style="text-align: left; display: block; padding: 1.1em 1.1em .6em; font-size: 1em; color: #f5216e; cursor: pointer;">
							Hora de Entrevista
							<input type="time" id="timeInterview" name="timeInterview" class="form-control" style="display: inline-block; position: relative; width: 100%; margin: 5px -5px 0; padding: 7px 5px 3px; border: none; outline: none; font-size: 1.3em; color: #000;">
						</label>
					</div>
					<div style="border: 1px solid #E4E4E4; height: 6em;">
						<label style="text-align: left; display: block; padding: 1.1em 1.1em .6em; font-size: 1em; color: #f5216e; cursor: pointer;">
							Entrevistador
							<select name="interviewerAdd" id="interviewerAdd" class="form-control" style="display: inline-block; position: relative; width: 100%; margin: 5px -5px 0; padding: 7px 5px 3px; border: none; outline: none; font-size: 1.3em; color: #000;">
								<option value="Entrevistador 1">Entrevistador 1</option>
								<option value="Entrevistador 2">Entrevistador 2</option>
								<option value="Entrevistador 3">Entrevistador 3</option>
							</select>
						</label>
					</div>
					<div style="border: 1px solid #E4E4E4; height: 6em;">
						<label style="text-align: left; display: block; padding: 1.1em 1.1em .6em; font-size: 1em; color: #f5216e; cursor: pointer;">
							Sucursal
							<select id="JobcenterInterview" name="JobcenterInterview" data-live-search="true" class="form-control" style="display: inline-block; position: relative; width: 100%; margin: 5px -5px 0; padding: 7px 5px 3px; border: none; outline: none; font-size: 1.3em; color: #000;">
								<option value=""> Seleccione...</option>
								@foreach ($jobcenter as $j)
								<option value="{{ $j->id }}">{{ $j->name }}</option>
								@endforeach
							</select>
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" style="background-color: black; border-color: black;" id="agenda_entrevista" name="agenda_entrevista" data-dismiss="modal">Guardar
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>