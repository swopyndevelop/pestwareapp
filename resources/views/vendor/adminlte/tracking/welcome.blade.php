@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Bienvenido !!</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						{{ trans('adminlte_lang::message.logged') }}. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis officia aspernatur dicta molestias praesentium voluptatem qui quam fugit alias officiis dolor, nobis architecto ad, iusto hic quaerat, doloremque corporis omnis!
						<hr><center>	
						<form action="{{  route('candidate_start') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
							<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Continuar</button>
						</form>
						<center>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

	
@endsection
