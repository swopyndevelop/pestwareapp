@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
<!-- aqui  -->
@section('main-content')

<div class="container-fluid spark-screen" id="tracking_index">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border text-center">
					<h3 class="box-title" style="font-size: 2em; margin: .5em">Aspirantes</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>

				<div class="box-body">
					<div class="col-md-offset-4 col-md-8 col-sm-4 col-xs-4" style="text-align:right; margin-bottom: 1em;">
						<i class="glyphicon glyphicon-search"></i>
						<input type="text" value="" data-table="tableAspirants" class="search_employees" placeholder="Buscar"/>
					</div>

					<div class="table-responsive col-xs-12">
						<table class="table table-striped tablesorter" id="tableAspirants">
							<thead style="background: black; color: white;">
								<tr>
									<th class="text-center">Usuario</th>
									<th class="text-center">Video</th>
									<th class="text-center">Solicitud</th>
									<th class="text-center">Psicométrico</th>
									<th class="text-center">Entrevista</th>
									<th class="text-center">Documentos</th>
									<th class="text-center">Contrato</th>
									<th class="text-center">Curso</th>
									<th class="text-center">Fecha ingreso</th>
								</tr></thead>

								<tbody>
									@foreach($trackings as $tracking)
									<tr>
										<td>
											@if($tracking->contract == 1)
											<a href="{{ route('tracking_contract', [ '$user' => $tracking->user_id ]) }}">
												<i class="fa fa-thumbs-o-up" aria-hidden="true" style="font-size:1.5em;color:green"></i>
											</a>
											@endif
											<strong>{{ $tracking->user->name }}</strong>&nbsp;
											@if (empty($tracking->job_title_profile->id ))
											<button class="btn btn-sm btn-circle matchModal" data-info="{{ $tracking->user_id }}"  style="background: none; height: auto; width: 15px; padding: 0" >
												<i style="color: #f39c12" class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" title="Sin registro"></i>
											</button>
											@else

											@if($tracking->percentage_match >= 70)
											<button class="btn btn-sm btn-circle matchModal" data-info="{{ $tracking->user_id }}"  style="background: none; height: auto; width: 15px; padding: 0" >

												<i style="color:green;" class="fa fa-circle" aria-hidden="true" data-toggle="tooltip" title="Compatible {{ $tracking->percentage_match }} %"></i>
											</button>
											@else
											<button class="btn btn-sm btn-circle matchModal" data-info="{{ $tracking->user_id }}"  style="background: none; height: auto; width: 15px; padding: 0" >
												<i class="fa fa-circle" aria-hidden="true" style="color: #d73925;" data-toggle="tooltip" title="Incompatible {{ $tracking->percentage_match }} %"></i>
											</button>
											@endif
											@endif
											<p class="text-default">&nbsp;&nbsp;
												<strong>
													<i class="fa fa-envelope-o" aria-hidden="true"></i>
												</strong> &nbsp; <a href="mailto:{{ $tracking->user->email }}">{{ $tracking->user->email }} </a>
											</p>
										</td>

										<td class="text-center">@if ( $tracking->video ==1 )
											<button type="button" class="btn" style="background: transparent;"><i class="fa fa-youtube-play fa-3x" aria-hidden="true" style="color: black; background: none;"></i>
											</button>
											@endif
										</td>

										<td class="text-center">@if (empty($tracking->job_title_profile->id ))
											<p class="text-danger"><strong>Sin Solicitud</strong></p>
											@else
											<a href="{{ route('job_application_pdf', [ '$id_user' => $tracking->user_id ]) }}">{{ $tracking->job_title_profile->job_title_name }}</a>
											@endif
										</td>

										<!--César-->
										<td class="text-center">
											@foreach($types as $ty)
											@if($tracking->user_id == $ty->user_id)
                        					@if ($ty->function_type == 1)
											@if ( $tracking->psychometric != null)
											@if ($tracking->psychometric <=25)
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-default" data-whatever="{{ $tracking->psychometric }}" style="background-color: #1bb49a; border-color:  #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> RECHAZADO </button>
											@elseif($tracking->psychometric >= 26 and $tracking->psychometric <= 31 )
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-defaultAA" data-whatever="{{ $tracking->psychometric }}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> APROBADO </button>
											@elseif($tracking->psychometric >=32)
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-defaultAAA" data-whatever="{{ $tracking->psychometric }}" style="background-color: #1bb49a; border-color:  #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> APROBADO </button>
											@endif
											@endif
											@endif
											@if ($ty->function_type == 2)
											@if ( $tracking->psychometric != null)
											@if ($tracking->psychometric <=31)
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-default2A" data-whatever="{{ $tracking->psychometric }}" style="background-color: #1bb49a; border-color:  #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> RECHAZADO </button>
											@elseif($tracking->psychometric >= 32 and $tracking->psychometric <= 36 )
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-default2AA" data-whatever="{{ $tracking->psychometric }}" style="background-color:#1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> APROBADO </button>
											@elseif($tracking->psychometric >=37)
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-default2AAA" data-whatever="{{ $tracking->psychometric }}" style="background-color: #1bb49a; border-color:  #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> APROBADO </button>
											@endif
											@endif
											@endif
											@if ($ty->function_type == 3)
											@if ( $tracking->psychometric != null)
											@if ($tracking->psychometric <=37)
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-default3A" data-whatever="{{ $tracking->psychometric }}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> RECHAZADO </button>
											@elseif($tracking->psychometric >= 38 and $tracking->psychometric <= 40 )
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-default3AA" data-whatever="{{ $tracking->psychometric }}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> APROBADO </button>
											@elseif($tracking->psychometric >=41)
											<button type="button" class="btn btn-info" aria-label="Left Align"  data-toggle="modal" data-target="#modal-default3AAA" data-whatever="{{ $tracking->psychometric }}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> APROBADO </button>
											@endif
											@endif
											@endif
											@endif
											@endforeach
										</td>

										<td class="text-center">
											@if ( $tracking->interview != null )
											@if ($tracking->interview <=25)
											<button type="button" class="btn btn-danger openResultInterview" aria-label="Left Align" data-toggle="modal" data-target="#modal-interview" data-whatresult="{{ $tracking->user_id }}" data-jobTitle="{{$tracking->job_title_profile_id}}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> NO CONTRATABLE
											</button>
										@elseif ($tracking->interview <=50)
											<button type="button" class="btn btn-warning openResultInterview" aria-label="Left Align" data-toggle="modal" data-target="#modal-interview" data-whatresult="{{ $tracking->user_id }}" data-jobTitle="{{$tracking->job_title_profile_id}}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> POCO SATISFACTORIO
											</button>
										@elseif ($tracking->interview <=75)
											<button type="button" class="btn btn-info openResultInterview" aria-label="Left Align" data-toggle="modal" data-target="#modal-interview" data-whatresult="{{ $tracking->user_id }}" data-jobTitle="{{$tracking->job_title_profile_id}}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> MEDIANAMENTE SATISFACTORIO
											</button>
											@else
											<button type="button" class="btn btn-primary openResultInterview" aria-label="Left Align" data-toggle="modal" data-target="#modal-interview" data-whatresult="{{ $tracking->user_id }}" data-jobTitle="{{$tracking->job_title_profile_id}}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square-o" aria-hidden="true"></i> SATISFACTORIO
											</button>
											@endif
											@else
											@foreach($allInterviews as $key => $aI)
											@if($aI->general_personal_data_id == $tracking->user_id)
											<a type="button" class="btn btn-sm btn-info openInterview" aria-label="Left Align" href="{{ route('interview_create', ['$user' => $tracking->user_id]) }}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-external-link-square" aria-hidden="true"></i>
											</a>
											<button type="button" class="btn btn-info btn-sm openInterview" aria-label="Left Align" data-toggle="modal" data-target="#modal-addinterview" data-id="{{$tracking->user_id}}" data-name="{{$tracking->user->name}}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square" aria-hidden="true"></i>
											</button>
											@break
											@else
											@if ($loop->last)
											<button type="button" class="btn btn-info openInterview" aria-label="Left Align" data-toggle="modal" data-target="#modal-addinterview" data-id="{{$tracking->user_id}}" data-name="{{$tracking->user->name}}" style="background-color: #1bb49a; border-color: #1bb49a;">
												<i class="fa fa-pencil-square" aria-hidden="true"></i> Asignar
											</button>
											@endif
											@endif
											@endforeach
											@endif
										</td>
										<td class="text-center">
											@if (empty($tracking->job_title_profile->id ))
											<button type="button" class="btn btn-danger btn-circle" disabled="true">
												<i class="fa fa-folder-open-o" aria-hidden="true"></i>
											</button>
											@elseif ($tracking->document != 0)
											@if (($tracking->doc_upload*100/$tracking->document) == 0)
											<a class="btn btn-danger" disabled = "disabled">
												<i class="fa fa-folder-open-o" aria-hidden="true"></i>
												@if ( $tracking->doc_upload == null ) 0 @else {{ $tracking->doc_upload }} @endif
												/
												@if ($tracking->document == null ) 0 
												@else {{ $tracking->document }} @endif
											</a>
											@elseif (($tracking->doc_upload*100/$tracking->document) <=49)
											<a class="btn btn-danger" href="{{ route('documents_show', ['document'=> $tracking->user_id ]) }}">
												<i class="fa fa-folder-open-o" aria-hidden="true"></i>
												@if ( $tracking->doc_upload == null ) 0 @else {{ $tracking->doc_upload }} @endif
												/
												@if ($tracking->document == null ) 0 
												@else {{ $tracking->document }} @endif
											</a>
											@elseif (( $tracking->doc_upload*100/$tracking->document) >=50 and ($tracking->doc_upload*100/$tracking->document)<=69)
											<a class="btn btn-warning" href="{{ route('documents_show', ['document'=> $tracking->user_id ]) }}">
												<i class="fa fa-folder-open-o" aria-hidden="true"></i>
												@if ( $tracking->doc_upload == null ) 0 @else {{ $tracking->doc_upload }} @endif
												/
												@if ($tracking->document == null ) 0 @else {{ $tracking->document }} @endif
											</a>
											@elseif (($tracking->doc_upload*100/$tracking->document) >=70 and ($tracking->doc_upload*100/$tracking->document)<=79)
											<a class="btn btn-info" href="{{ route('documents_show', ['document'=> $tracking->user_id ]) }}">
												<i class="fa fa-folder-open-o" aria-hidden="true"></i>
												@if ( $tracking->doc_upload == null ) 0 @else {{ $tracking->doc_upload }} @endif
												/
												@if ($tracking->document == null ) 0 @else {{ $tracking->document }} @endif
											</a>
											@elseif (($tracking->doc_upload*100/$tracking->document) >=80 and ($tracking->doc_upload*100/$tracking->document)<=89)
											<a class="btn btn-primary" href="{{ route('documents_show', ['document'=> $tracking->user_id ]) }}">
												<i class="fa fa-folder-open-o" aria-hidden="true"></i>
												@if ( $tracking->doc_upload == null ) 0 @else {{ $tracking->doc_upload }} @endif
												/
												@if ($tracking->document == null ) 0 @else {{ $tracking->document }} @endif
											</a>
											@elseif (($tracking->doc_upload*100/$tracking->document)  >=90 and ($tracking->doc_upload*100/$tracking->document)<=100)
											<a class="btn btn-success" href="{{ route('documents_show', ['document'=> $tracking->user_id ]) }}">
												<i class="fa fa-folder-open-o" aria-hidden="true"></i>
												@if ( $tracking->doc_upload == null ) 0 @else {{ $tracking->doc_upload }} @endif
												/
												@if ($tracking->document == null ) 0 @else {{ $tracking->document }} @endif
											</a>
											@endif

											<div class="progress" style="height:4px">
												<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{ ($tracking->doc_verified *100) / $tracking->document }}%" >
													<span class="sr-only">40% Complete (success)</span>
												</div>
											</div>
											@endif
										</td>

										<td class="text-center">
											@if (isset($tracking->contract_upload))
											<a href="{{ route('tracking_contract', [ '$user' => $tracking->user_id ]) }}">
												<i class="fa fa-file-text-o" aria-hidden="true" style="font-size:1.5em;color:green"></i>
											</a>
											@elseif (isset($tracking->contract_download))
											<button type="button" class="btn btn-info" aria-label="Left Align" style="background-color: #1bb49a; border-color: #1bb49a; margin-right: .2em">
												<a href="{{ route('contracts_index', [ '$usuario' => $tracking->user_id ]) }}"><i style="font-size: 1.3em; color: white;" class="fa fa-upload" aria-hidden="true"></i>
												</a>
											</button>
											@elseif (isset($tracking->doc_verified))
											<button type="button" class="btn btn-info" aria-label="Left Align" style="background-color: #1bb49a; border-color: #1bb49a; margin-right: .2em">
												<a href="{{ route('contracts_index', [ '$usuario' => $tracking->user_id ]) }}"><i style="font-size: 1.3em; color: white;" class="fa fa-download" aria-hidden="true"></i>
												</a>
											</button>
											@endif
										</td>

										<td class="text-center">
											@foreach($progress as $p)
											@if($tracking->user_id == $p->user_id)
											@if ( $p->promedio != null ) 
											@if ($p->promedio <=49)
											<a href="{{ route('training_kardex', ['$user' =>  $tracking->user_id ]) }}" class="btn btn-danger" aria-label="Left Align" data-toggle="tooltip" data-placement="right" title="">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i>
												{{ round($p->promedio) }}%
											</a>
											@elseif ($p->promedio >=50 and $p->promedio <=69)
											<a href="{{ route('training_kardex', ['$training' =>  $tracking->user_id ]) }}" class="btn btn-warning" aria-label="Left Align" data-toggle="tooltip" data-placement="right" title="">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i> {{ round($p->promedio) }}%
											</a>
											@elseif ($p->promedio >=70 and $p->promedio <=79)
											<a href="{{ route('training_kardex', ['$training' =>  $tracking->user_id ]) }}" class="btn btn-info" aria-label="Left Align" data-toggle="tooltip" data-placement="right" title="" style="background-color: #48099d; border-color: #48099d;">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i> {{ round($p->promedio)}}%
											</a>
											@elseif ($p->promedio >=80 and $p->promedio <=89)
											<a href="{{ route('training_kardex', ['$training' =>  $tracking->user_id ]) }}" class="btn btn-primary" aria-label="Left Align" data-toggle="tooltip" data-placement="right" title="">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i> {{ round($p->promedio)}}%
											</a>
											@else ($p->promedio >=90 and $p->promedio <=100)
											<a href="{{ route('training_kardex', ['$training' =>  $tracking->user_id ]) }}" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" data-placement="right" title="">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i> {{ round($p->promedio)}}%
											</a>
											@endif
											@endif
											@endif
											@endforeach
										</td>
										<td class="text-center">
											@php
											echo date("d-m-Y", strtotime($tracking->created_at));
											@endphp
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							{{ $trackings->links() }}
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<!-- modalOperativoA -->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-default">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultados de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">

						<p id="showResultLevel"></p>

						<table class="table">
							<thead> Nivel Operativo
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">32 a 48</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">152</td>
								</tr>
								<tr>
								<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">26 a 31</td>
									<td class="text-center">25</td>
									<td class="text-center">62</td>
									<td class="text-center">87</td>
								</tr>
								<tr>
								<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">5 a 25</td>
									<td class="text-center">5</td>
									<td class="text-center">62</td>
									<td class="text-center">72</td>
								</tr>
							</tbody>
						</table>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">A</td>
									<td class="text-center">Poco satisfactorio</td>
									<td class="text-center">La ejecución con respecto a la característica es baja y sin responsabilidades de mejorar.</td>
									<td class="text-center">Rechazado</td>
								</tr>
							</tbody>
						</table>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalOperativoA -->

		<!-- modalOperativoAA-->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-defaultAA">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultados de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">
						 <p id="showResultLevelAA"></p>
						 <table class="table">
							<thead> Nivel Operativo
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">32 a 48</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">152</td>
								</tr>
								<tr>
								<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">26 a 31</td>
									<td class="text-center">25</td>
									<td class="text-center">62</td>
									<td class="text-center">87</td>
								</tr>
								<tr>
								<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">5 a 25</td>
									<td class="text-center">5</td>
									<td class="text-center">62</td>
									<td class="text-center">72</td>
								</tr>
							</tbody>
						</table>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AA</td>
									<td class="text-center">Satisfactorio</td>
									<td class="text-center">La ejecución con respecto a la característica llena los requisitos del puesto.</td>
									<td class="text-center">Aceptado</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalOperativoAA-->

		<!-- modalOperativoAAA-->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-defaultAAA">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultados de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">
						 <p id="showResultLevelAAA"></p>
						 <table class="table">
							<thead> Nivel Operativo
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">32 a 48</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">152</td>
								</tr>
								<tr>
								<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">26 a 31</td>
									<td class="text-center">25</td>
									<td class="text-center">62</td>
									<td class="text-center">87</td>
								</tr>
								<tr>
								<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">5 a 25</td>
									<td class="text-center">5</td>
									<td class="text-center">62</td>
									<td class="text-center">72</td>
								</tr>
							</tbody>
						</table>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AAA</td>
									<td class="text-center">Altamente satisfactorio</td>
									<td class="text-center">La ejecución es superior a los requerimientos necesarios para el buen desempeño del puesto.</td>
									<td class="text-center">Aceptado</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalOperativoAAA-->

		<!-- modalMediosA-->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-default2A">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultado de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">
						 <p id="showResultLevel2A"></p>
						 <table class="table">
							<thead> Nivel Mandos Medios
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">37 a 48</td>
									<td class="text-center">90</td>
									<td class="text-center">62</td>
									<td class="text-center">157</td>
								</tr>
								<tr>
								<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">32 a 36</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">112</td>
								</tr>
								<tr>
								<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">26 a 31</td>
									<td class="text-center">25</td>
									<td class="text-center">62</td>
									<td class="text-center">87</td>
								</tr>
							</tbody>
						</table>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">A</td>
									<td class="text-center">Poco satisfactorio</td>
									<td class="text-center">La ejecución con respecto a la característica es baja y sin responsabilidades de mejorar.</td>
									<td class="text-center">Rechazado</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalMediosA-->

		<!-- modalMediosAA -->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-default2AA">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultado de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">
						 <p id="showResultLevel2AA"></p>
						 <table class="table">
							<thead> Nivel Mandos Medios
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">37 a 48</td>
									<td class="text-center">90</td>
									<td class="text-center">62</td>
									<td class="text-center">157</td>
								</tr>
								<tr>
								<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">32 a 36</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">112</td>
								</tr>
								<tr>
								<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">26 a 31</td>
									<td class="text-center">25</td>
									<td class="text-center">62</td>
									<td class="text-center">87</td>
								</tr>
							</tbody>
						</table>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AA</td>
									<td class="text-center">Satisfactorio</td>
									<td class="text-center">La ejecución con respecto a la característica llena los requisitos del puesto.</td>
									<td class="text-center">Aceptado</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalMediosAA-->

		<!-- modalMediosAAA-->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-default2AAA">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultado de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">
						 <p id="showResultLevel2AAA"></p>
						 <table class="table">
							<thead> Nivel Mandos Medios
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">37 a 48</td>
									<td class="text-center">90</td>
									<td class="text-center">62</td>
									<td class="text-center">157</td>
								</tr>
								<tr>
								<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">32 a 36</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">112</td>
								</tr>
								<tr>
								<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">26 a 31</td>
									<td class="text-center">25</td>
									<td class="text-center">62</td>
									<td class="text-center">87</td>
								</tr>
							</tbody>
						</table>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AAA</td>
									<td class="text-center">Altamente satisfactorio</td>
									<td class="text-center">La ejecución es superior a los requerimientos necesarios para el buen desempeño del puesto.</td>
									<td class="text-center">Aceptado</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalMediosAAA-->

		<!-- modalGerencialA-->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-default3A">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultado de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">
						 <p id="showResultLevel3A">
						 <table class="table">
							<thead class="text-center"> Nivel Gerencias
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">41 a 48</td>
									<td class="text-center">95</td>
									<td class="text-center">62</td>
									<td class="text-center">157</td>
								</tr>
								<tr>
									<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">37 a 40</td>
									<td class="text-center">90</td>
									<td class="text-center">62</td>
									<td class="text-center">152</td>
								</tr>
								<tr>
									<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">32 a 37</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">112</td>
								</tr>
							</tbody>
						</table>
						<br>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">A</td>
									<td class="text-center">Poco satisfactorio</td>
									<td class="text-center">La ejecución con respecto a la característica es baja y sin responsabilidades de mejorar.</td>
									<td class="text-center">Rechazado</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalGerencialA-->

		<!-- modalGerencialAA-->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-default3AA">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultado de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">
						 <p id="showResultLevel3AA">
						 <table class="table">
							<thead class="text-center"> Nivel Gerencias
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">41 a 48</td>
									<td class="text-center">95</td>
									<td class="text-center">62</td>
									<td class="text-center">157</td>
								</tr>
								<tr>
									<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">37 a 40</td>
									<td class="text-center">90</td>
									<td class="text-center">62</td>
									<td class="text-center">152</td>
								</tr>
								<tr>
									<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">32 a 37</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">112</td>
								</tr>
							</tbody>
						</table>
						<br>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AA</td>
									<td class="text-center">Satisfactorio</td>
									<td class="text-center">La ejecución con respecto a la característica llena los requisitos del puesto.</td>
									<td class="text-center">Aceptado</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalGerencialAA-->

		<!-- modalGerencialAAA-->

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-default3AAA">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Resultado de la Prueba Dominos</h4>
					</div>
					<div class="modal-body">
						 <p id="showResultLevel3AAA">
						 <table class="table">
							<thead class="text-center"> Nivel Gerencias
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Resultado</th>
									<th style="width: 20%" class="text-center">Aciertos</th>
									<th style="width: 20%" class="text-center">Rank</th>
									<th style="width: 20%" class="text-center">EM</th>
									<th style="width: 20%" class="text-center">CIEM</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AAA</td>
									<td class="text-center">Brillante</td>
									<td class="text-center">41 a 48</td>
									<td class="text-center">95</td>
									<td class="text-center">62</td>
									<td class="text-center">157</td>
								</tr>
								<tr>
									<td class="text-center">AA</td>
									<td class="text-center">Normal</td>
									<td class="text-center">37 a 40</td>
									<td class="text-center">90</td>
									<td class="text-center">62</td>
									<td class="text-center">152</td>
								</tr>
								<tr>
									<td class="text-center">A</td>
									<td class="text-center">Deficiente</td>
									<td class="text-center">32 a 37</td>
									<td class="text-center">50</td>
									<td class="text-center">62</td>
									<td class="text-center">112</td>
								</tr>
							</tbody>
						</table>
						<br>
						<table class="table">
							<thead>
								<tr>
									<th style="width: 20%" class="text-center">Nivel</th>
									<th style="width: 20%" class="text-center">Valoración</th>
									<th style="width: 20%" class="text-center">Comentario</th>
									<th style="width: 20%" class="text-center">Observación</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">AAA</td>
									<td class="text-center">Altamente satisfactorio</td>
									<td class="text-center">La ejecución es superior a los requerimientos necesarios para el buen desempeño del puesto.</td>
									<td class="text-center">Aceptado</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modalGerencialAAA-->



	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-interview">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Resultado de la Entrevista</h4>
				</div>
				<div class="modal-body" id="modal-body">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
					</button>

				</div>
			</div>
			<!-- /.modal -->
		</div>
	</div>
</div>
</div>
@include('adminlte::tracking.interview')
<!-- Modal -->
<div id="matchModal" class="modal fade" role="dialog" style="background-color: rgba(0,0,0,0.01); ">
	<div class="modal-dialog " >
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="text-center">
					<h4>Compatibilidad con el puesto</h4>
				</div>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<h3>Nombre:
						<span id="name_applicant"></span>
					</h3>
					<h4>Aspira:
						<span id="work_applicant"></span>
					</h4>
					<!-- valor para el porcentaje -->
					<span id="porcentaje_applicant"></span>
				</div>
				<div class="row">
					<div class="col-md-offset-3 col-md-6 text-center">
						<img class="img-responsive" style="margin: 0 auto" src="{{ asset('img/avatar.png') }}"></img>
					</div>
				</div>
				<br>
				<div class="row text-center">
					<div id="test-circle" name="test-circle" class="col-md-offset-3 col-md-6" ></div>
				</div>
				<input type="hidden" id="id_applicant">
				<button id="VerDetalles" type="button" class="btn btn-link" data-toggle="VerDetalles" data-target="#Modaldetalles" disabled="true">
					<b>Ver detalles</b>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- END MATCH MODAL -->

<!-- Modal details-->
<div id="Modaldetalles" class="modal fade" role="dialog" style="background-color: transparent; top: 5em;">
	<div class="modal-dialog modal-lg" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
		<!-- Modal content-->
		<div class="modal-content">
			<!-- Modal header-->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="text-center">
					<h3 class="modal-title"> Detalles </h3>
				</div>
			</div>
			<!-- Modal body-->
			<div class="modal-body">
				<div class="text-right">
					<span style="font-size: 1.2em">Correcto</span> <i class="fa fa-circle" aria-hidden="true" style="color: #d0f1c2; font-size: 1.5em"></i>
					&nbsp;<span style="font-size: 1.2em">Revisar</span> <i class="fa fa-circle" aria-hidden="true" style="color: #e4e1e1; font-size: 1.5em"></i>
				</div>
				<div class="text-center row">
					<div class="col-md-4">
						<h4>Género: <br>
							<span id="gender_applicant"></span>
						</h4>
					</div>
					<div class="col-md-4">
						<h4>Edad: <br>
							<span id="edad_applicant"></span>
						</h4>
					</div>
					<div class="col-md-4">
						<h4> Estado civil: <br>
							<span  id="civilstatus_applicant"></span>
						</h4>
					</div>
					<div class="col-md-6">
						<table class="table table-hover text-center">
							<thead>
								<tr>
									<th>Lenguaje</th>
									<th>Nivel</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="success" style="background: #d0f1c2">
										<span id="lan_applicant"></span>
									</td>
									<td class="success" style="background: #d0f1c2">
										<span id="level_applicant"></span>
									</td>
								</tr>
								<tr>
									<td class="warning" style="background: #e4e1e1;">
										<span id="lannot_applicant"></span>
									</td>
									<td class="warning" style="background: #e4e1e1;">
										<span id="levelnot_applicant"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table table-hover text-center">
							<thead>
								<tr>
									<th>Grado acádemico</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="success" style="background: #d0f1c2">
										<span id="aca_grade_applicant"></span>
									</td>
								</tr>
								<tr>
									<td class="warning" style="background: #e4e1e1;">
										<span id="aca_grade_not_applicant"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-10 col-md-offset-1">
						<table class="table table-hover text-center">
							<thead>
								<tr>
									<th>Carrera</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="success" style="background: #d0f1c2">
										<span id="carrer_applicant"></span>
									</td>
								</tr>
								<tr>
									<td class="warning" style="background: #e4e1e1;">
										<span id="carrer_not_applicant"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- Modal body-->
		</div>
		<!-- Modal content-->
	</div>
</div>
@endsection

@section('personal-js')
<style>
table.tablesorter thead tr .header {
	background-image: url(../img/bg.gif);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter thead tr .headerSortUp {
	background-image: url(../img/asc.gif);
}
table.tablesorter thead tr .headerSortDown {
	background-image: url(../img/desc.gif);
}
</style>
<!-- <link rel="stylesheet" href="{{ asset('css/style_sorterTable.css') }}"> -->
<script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
<script>
    // call the tablesorter plugin
    $("#tableAspirants").tablesorter({
        // pass the headers argument and assing a object
        headers: {
            // assign the secound column (we start counting zero)
            1: {
                // disable it by setting the property sorter to false
                sorter: false
            },
            // assign the third column (we start counting zero)
            6: {
                // disable it by setting the property sorter to false
                sorter: false
            }
        }
    });
</script>
@stop
