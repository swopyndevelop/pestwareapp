@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')

<style type="text/css">
.user{
  height: 300px;
  width: 300px;
}
h3.user{
  text-align: center;
}
p.title{
  text-align: center;
  color: grey;
  font-size: 18px;
  margin-top: -.5em;
}
</style>

<!-- Trigger the modal with a button -->
<div>

  <select name="References_applications" id="References_applications" class=" form-control">
    <option value="">Selecciona un aplicante</option>
    @foreach($applicants as $a)
    <option value="{{ $a->general_personal_data_id }}">{{ $a->first_name }} {{ $a->last_name}}</option>
    @endforeach
  </select>
</div>
<div>
  <button id="Modal" name="Modal" type="button" class="btn btn-primary " data-toggle="Modal" data-target="#myModal">Open Modal</button>
</div>
<!-- Modal -->
<div id="myModal" name="myModal" class="modal fade" role="dialog" style="background-color: rgba(0,0,0,0.01); ">
  <div class="modal-dialog " >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-md-2">
          <button type="button" class="btn btn-default" data-dismiss="modal">X</button>
        </div>
        <div class="col-md-8">
         <h4>Compatibilidad con el puesto</h4>
       </div>
     </div>
     <div class="modal-body">
      <div class="text-center">
        <h3>Nombre: <span id="name_applicant"></span></h3>
        <h4>Aspira: <span id="work_applicant"> </span></h4>
        <!-- valor para el porcentaje -->
        <span id="porcentaje_applicant"> </span>
      </div>
      <div class="row">
        <div class="col-md-offset-3 col-md-6 text-center">
          <img class="img-responsive" style="margin: 0 auto" src="{{ asset('img/avatar.png') }}"></img>
        </div>
      </div>
      <br>
      <div class="row text-center">
        <div  id="test-circle" name="test-circle" class="col-md-offset-3 col-md-6" ></div>
      </div>
      <input type="hidden" id="id_applicant">
      <button id="VerDetalles" type="button" class="btn btn-link" data-toggle="VerDetalles" data-target="#Modaldetalles" disabled="true"><b>Ver detalles</b></button>
    </div>
  </div>
</div>
</div>

<!-- Modal details-->
<div id="Modaldetalles" class="modal fade" role="dialog" style="background-color: rgba(0,0,0,0.01); top: 35em ">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <!-- Modal header-->
      <div class="modal-header">
        <div class="col-md-2">
         <button type="button" class="btn btn-default" data-dismiss="modal">X</button>
       </div>
       <div class="col-md-8">
        <h4 class="modal-title"> Detalles </h4>
      </div>
    </div>
    <!-- Modal header-->
    <!-- Modal body-->
    <div class="modal-body">
      <div class="row">
        <div class="col-md-6  text-center ">
          <h5>Genero: <span id="gender_applicant"></span></h5>
          <h5>Edad: <span id="edad_applicant"></span></h5>
          <h5> Estado civil: <span  id="civilstatus_applicant"></span></h5>
        </div>
        <div class="col-md-6 text-center " >
          <div class="col-md-6 text-center">
            <h5>Lenguaje:</h5>
            <h5><span id="lan_applicant"></span></h5>
            <h5><span id="lannot_applicant"></span></h5>
          </div>
          <div class="col-md-6 text-center">
            <h5>Nivel:</h5>
            <h5><span id="level_applicant"></span></h5>
            <h5><span id="levelnot_applicant"></span></h5>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-5 text-center ">
            <h5> Grado academico:</h5>
            <h5><span id="aca_grade_applicant"></span></h5>
            <h5><span id="aca_grade_not_applicant"></span></h5>
          </div>
          <div class="col-md-7 text-center ">
            <h5>Carrera:</h5>
            <h5><span id="carrer_applicant"></span></h5>
            <h5><span id="carrer_not_applicant"></span></h5>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal body-->
    <!-- Modal footer-->
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
      <!-- Modal footer-->
    </div>
    <!-- Modal content-->
  </div>
</div>
<!-- Modal details-->
@endsection