@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Aspirantes</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						  
							<div class="embed-responsive embed-responsive-16by9">
						    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/9GSsaPtYMS8"></iframe>
						</div>					
					<hr><center>	
						<form action="{{  route('candidate_video') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
							<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Continuar</button>
						</form>
						<center>												
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>


		</div>
	</div>
@endsection
