@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Cursos</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">

							@include('adminlte::layouts.partials._errors')

							<form action="{{ Route('training_update', ['training' => $training->id]) }}" method="POST" role="form" enctype="multipart/form-data">

								{{ csrf_field() }}
								{{ method_field('PUT') }}
								<legend>Actualizar</legend>

								<div class="form-group">
									<label for="Titulo">Titulo</label>
									<input type="text" class="form-control" id="Titulo" name="Titulo" required="required" 
									placeholder="Atención al Cliente" value="{{ $training->title or old('Titulo') }}">
								</div>

								<div class="form-group">
									<label for="Duracion">Duración</label>
									<input type="text" class="form-control" id="Duracion" name="Duracion" required="required" placeholder="14 minutos" value="{{ $training->duration or old('Duracion') }}">
								</div>

								<div class="form-group">
									<label for="Descripcion">Descripción</label>
									<input type="text" class="form-control" id="Descripcion" name="Descripcion" required="required" placeholder="Un saludo y una sonrisa es lo que todos nos nerecemos ..." value="{{ $training->description or old('Descripcion') }}">
								</div>

								<div class="form-group">
									<label for="Inicio">Fecha de inicio</label>
									<input type="date" class="form-control" id="Inicio" name="Inicio" required="required" placeholder="2017/01/01" value="{{ $training->start or old('Inicio') }}">
								</div>
								<div class="form-group">
									<label for="Fin">Fecha de fin</label>
									<input type="date" class="form-control" id="Fin" name="Fin" required="required" value="{{ $training->end or old('Fin') }}">
								</div>
								<div class="form-group">
									<label for="Curso">Archivo Zip del Curso</label>
									<input type="file" class="form-control" id="Curso" name="Curso" value="{{ $training->course or old('Course') }}">
								</div>

								<button type="submit" class="btn btn-primary">Guardar</button>

							</form>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
			</div>
		</div>
		@endsection
