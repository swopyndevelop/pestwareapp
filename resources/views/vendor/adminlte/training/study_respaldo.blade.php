<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Cursos</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <br>
                    <div class="table-responsive">
                        @foreach($plans as $plan)
                            <table class="table table-hover table table-striped" @if($loop->first) @else style="margin-top: 4em;" @endif>
                                <tr>
                                    <td class="text-center" colspan="7" style="background-color: black;">
                                        <span style="color: white; font-weight: bold; font-size: 1.5em;">{{$plan->name}}</span>
                                    </td>
                                </tr>
                                @foreach($modules as $module)
                                    @if($module->training_plan_id == $plan->id)
                                        <tr>
                                            <td class="text-center" colspan="7" style="background-color: #00AB7F;">
                                                <span style="color: white; font-weight: bold; font-size: 1em;"> {{$module->name}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">Título</th>
                                            <th class="text-center">Duración</th>
                                            <th class="text-center">Descripción</th>
                                            <th class="text-center">Curso</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Resultado</th>
                                        </tr>
                                        @foreach($trainings as $training)
                                            @if($module->module_id == $training->module_id)
                                                <tr>
                                                    <td>{{ $training->title }}</td>
                                                    <td class="text-center">{{ $training->duration }}</td>
                                                    <td class="text-center">{{ $training->description }}</td>
                                                    <td class="text-center">
                                                        <a href="{{ asset($training->course) }}" target="_blank" class="btn"><i class="fa fa-youtube-play fa-3x" aria-hidden="true" style="color: #3400AB"></i></a>
                                                    </td>
                                                    @foreach($progress as $p)
                                                        @if($training->id == $p->training_id)
                                                            @if($p->progress>=5 and $p->progress<=99)
                                                                <td class="text-center">
                                                                    <button type="button" class="btn btn-warning" aria-label="Left Align" disabled="true" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title=" Porcentaje cursado: {{ $p->progress }}%">
                                                                        <i class="fa fa-spinner" aria-hidden="true"></i>
                                                                        EN CURSO </button>
                                                                </td>
                                                                <td class="text-center" width="15%">
                                                                </td>
                                                            @elseif($p->progress == 100)
                                                                <td class="text-center">
                                                                    <button type="button" class="btn btn-success" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title=" Porcentaje cursado: {{ $p->progress }}%" disabled="true">
                                                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                                                        FINALIZADO
                                                                    </button>
                                                                </td>
                                                                <td class="text-center" width="15%">
                                                                    <?php $pass = 'noEvaluado'  ?>
                                                                    @if($p->course_grades >= 80)
                                                                        <button type="button" class="btn btn-success" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title="Calificación {{$p->course_grades}}" disabled="disabled">
                                                                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                                                            APROBADO
                                                                        </button>
                                                                        @php $pass = 'aprobado' @endphp
                                                                    @elseif($p->course_grades != null)
                                                                        @php $pass = 'reprobado' @endphp
                                                                    @endif
                                                                    @if($pass != 'aprobado')
                                                                        <a href="{{ route('intro', ['course' => $training->id]) }}" class="btn @if($pass == 'reprobado') btn-danger @else btn-info @endif " aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;">
                                                                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                                                            @if($pass == 'reprobado') Aplicar nuevamente @else EVALUACION @endif
                                                                        </a>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </table>
                        @endforeach
                    </div>
                    {{ $plans->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div> <!-- col-md-12 -->
    </div>
</div>