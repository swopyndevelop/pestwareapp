@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('personal_style')
    @include('adminlte::training.PersonalSelect2')
@stop

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h2 class="box-title">Actualizar Modulos</h2>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        @include('adminlte::layouts.partials._errors')
                        <form action="{{ Route('module_assign_update', ['module' => $module->id]) }}" method="POST" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <!-- <span style="font-size: 2em">Actualizar</span> -->
                            <div class="col-md-12">
                                <div class="panel panel-info">
                                    <table class="table table-hover">
                                        <thead class="table-general">
                                        <tr>
                                            <th class="text-center">Actualizar</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <br>
                                    <div class="panel-body">
                                        <div class="form-group col-md-12">
                                            <div class="col-md-12 text-center">
                                                <label for="Nombre" style="font-size: 1.5em">Nombre del módulo</label>
                                            </div>
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="text" class="form-control" id="moduleName" name="moduleName" value="{{ $module->name or old('moduleName') }}" placeholder="Módulo..." style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
                                            </div>
                                        </div>


                                        <div class="form-group col-md-12">
                                            <div class="col-md-12 text-center">
                                                <label for="Nombre" style="font-size: 1.5em">Asignación de Cursos</label>
                                            </div>
                                            <div class="col-md-8 col-md-offset-2">
                                                <select required="required" name="course[]" multiple="multiple" class="form-control multiple" id="course">
                                                    @foreach($trainings as $training)
                                                        <option value="{{ $training->id}}"
                                                                @foreach( $modulesTraining as $moduleTraining)
                                                                @if($moduleTraining->id == $training->id)
                                                                selected = 'selected'
                                                                @endif
                                                                @endforeach
                                                                {{ (in_array($training, old('course', []))) }} >{{ $training->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <br>
                                                <button type="submit" class="btn btn-primary">Actualizar</button>
                                            </div>
                                            <br>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script>
        $(".multiple").select2();
    </script>
@endsection