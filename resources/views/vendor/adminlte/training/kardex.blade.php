@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">{{$usuario->name}}</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12 text-left">
							<button onclick="goBack()" class="btn btn-default" data-toggle="tooltip" title="Regresar"><i class="fa fa-arrow-left"></i></button>
						</div>
					</div>
					<br>
					<div class="table-responsive">
						@foreach($plans as $plan)
						<table class="table table-hover table table-striped" @if($loop->first) @else style="margin-top: 4em;" @endif>
							<thead>
								<tr>
									<td class="text-center" colspan="7" style="background-color: black;">
										<span style="color: white; font-weight: bold; font-size: 1.5em;">Plan {{$plan->name}}</span>
									</td>
								</tr>
								<tr>
									<th class="text-center">Título</th>
									<th class="text-center">Duración</th>
									<th class="text-center">Descripción</th>
									<th class="text-center">Status</th>
									<th class="text-center">Resultado</th>
								</tr>
							</thead>
							<tbody>
								@foreach($trainings as $training)
								@if($plan->id == $training->TrainingPlan)
								<tr>
									<td>{{ $training->title }}</td>
									<td class="text-center">{{ $training->duration }}</td>
									<td class="text-center">{{ $training->description }}</td>
									@foreach($progress as $p)
									@if($training->id == $p->training_id)
									@if($p->progress>=5 and $p->progress<=99)
									<td class="text-center">
										<button type="button" class="btn btn-warning" aria-label="Left Align" disabled="true" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title=" Porcentaje cursado: {{ $p->progress }}%">
											<i class="fa fa-spinner" aria-hidden="true"></i>
										EN CURSO </button>
									</td>
									<td class="text-center" width="15%">
									</td>
									@elseif($p->progress == 100)
									<td class="text-center">
										<button type="button" class="btn btn-success" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title=" Porcentaje cursado: {{ $p->progress }}%" disabled="true">
											<i class="fa fa-check-square-o" aria-hidden="true"></i>
											FINALIZADO
										</button>
									</td>
									<td class="text-center" width="15%">
										<?php $pass = 'noEvaluado'  ?>
										@if($p->course_grades >= 80)
										<button type="button" class="btn btn-success" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title="Calificación {{$p->course_grades}}" disabled="disabled">
											<i class="fa fa-graduation-cap" aria-hidden="true"></i>
											APROBADO
										</button>
										@php $pass = 'aprobado' @endphp
										@elseif($p->course_grades != null)
										@php $pass = 'reprobado' @endphp
										@endif
										@if($pass != 'aprobado')
										<button class="btn @if($pass == 'reprobado') btn-danger @else btn-info @endif " aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;" disabled="disabled">
											<i class="fa fa-graduation-cap" aria-hidden="true"></i>
											@if($pass == 'reprobado') NO APROBADO @else SIN EVALUACION @endif
										</button>
										@endif
									</td>
									@endif
									@endif
									@endforeach
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
						@endforeach
					</div>
					{{ $plans->links() }}				
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->

		</div>

	</div>
</div>
@endsection
