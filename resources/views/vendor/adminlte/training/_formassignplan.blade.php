<div class="panel panel-primary" style="border-color: #bce8f1;">
    <table class="table table-hover">
        <thead class="header-title">
            <tr>
                <th class="text-center">
                    Asignación de Planes
                </th>
            </tr>
        </thead>
    </table>
    <div class="pull-right">
        <a data-perform="panel-collapse" href="javascript:void(0)">
            <i class="ti-minus">
            </i>
        </a>
        <a data-perform="panel-dismiss" href="javascript:void(0)">
            <i class="ti-close">
            </i>
        </a>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <span style="font-size: 1.5em">
                        Nombres
                    </span>
                <div class="form-group">
                    <div class="input-group">
                            <span class="input-group-addon" style="border: none;">
                                <i aria-hidden="true" class="fa fa-user" style="font-size: 2em;">
                                </i>
                            </span>
                        <select class="form-control multiple" id="users" multiple="multiple" name="users[]" style="width: 100%;">
                            @foreach($users as $user)
                                <option value="{{ $user->id }}" {{ (in_array($user, old('users', []))) }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <span style="font-size: 1.5em">Planes</span>
                <div class="form-group">
                    <div class="input-group">
                            <span class="input-group-addon" style="border: none;">
                                <i aria-hidden="true" class="fa fa-book" style="font-size: 2em;">
                                </i>
                            </span>
                        <select class="form-control multiple" id="plans" multiple="multiple" name="plans[]">
                            @foreach($plans as $plan)
                                <option value="{{ $plan->id }}" {{ (in_array($plan, old('plans', []))) }} >
                                    {{ $plan->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button class="btn btn-primary" type="submit">
                    Asignar
                </button>
            </div>
        </div>
    </div>
</div>
