@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">PestWare Academy</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
								<i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<br>
						<div class="container">
							<div class="row">
								<div class="col-md-2">
									<img src="{{ asset('img/logoPestwareAcademy.jpeg') }}" alt="logo-pwa" height="100">
								</div>
								<div class="col-md-10">
									<h2></h2>
								</div>
							</div>
						<hr>
						<section>
							<div class="row container-fluid">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h3 class="panel-title"><span class="h3">Más Vistos</span><a href="#" class=""></a></h3>
									</div>
									<div class="panel-body">
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499418668" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Introducción al módulo de Atención a Cliente</span>
														<br>
														Introducción a uno de los módulos más importantes dentro de PestWareApp - Atención al cliente.
													</p>
												</div>
												<div class="card-footer text-center">

													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 700 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 80%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>

<!--													<div class="text-center">
														<span class="badge card-badge-danger text-capitalize">Rebajas</span>
													</div>
													<div class="text-center">
														<span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"></span>
													</div>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499435097" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Funcionamiento del cotizador</span><br>
														Qué se toma en cuenta al momento de cotizar.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 540 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 76%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
<!--													<div class="text-center">
														<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
													</div>
													<div class="text-center">
														<span class="badge card-badge-danger text-capitalize">Rebajas</span>
													</div>
													<div class="text-center">
														<span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"></span>
													</div>
													<a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499443408" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Crear Cotización </span><br>
														Procedimiento para crear una cotización.
													</p><br><br>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 710 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 50%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
<!--													<div class="text-center">
														<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
													</div>
													<div class="text-center">
														<span class="badge card-badge-danger text-capitalize">Rebajas</span>
													</div>
													<div class="text-center">
														<span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
													</div>
													<a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
										<div class="card-deck">
											<div class="card">
												<iframe
														width=93%" height="" src="https://player.vimeo.com/video/499418668" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Opciones de una Cotización</span>
														<br>
														Breve descripción de las opciones que presenta cada cotización.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 633 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 81%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499444048" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Editar Cotización</span><br>
														Procedimiento para modificar/actualizar/editar cualquiera de los campos capturados en una cotización
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 109 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 90%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499444830" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title"> Rechazar cotización </span><br>
														Procedimiento para rechazar una cotización.
													</p>
													<br>
												</div>
													<div class="card-footer text-center">
														<div class="row">
															<div class="col-6 card-div-padding-custom">
																<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 806 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 69%</span>
															</div>
															<div class="col-6 card-div-padding-custom">
																<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
															</div>
														</div>
														<!--													<div class="text-center">
                                                                                                                <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                            </div>
                                                                                                            <div class="text-center">
                                                                                                                <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                            </div>
                                                                                                            <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
													</div>
											</div>
										</div>
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499448162" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title"> Enviar cotización WhatsApp </span><br>
														Procedimiento para enviar la cotización a través de WhatsApp.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 189 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 91%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499449601" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title"> Enviar cotización correo electrónico</span>
														<br>
														Procedimiento para enviar la cotización a través del correo electrónico.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 371 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 77%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=93%" height="" src="https://player.vimeo.com/video/499450676" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title"> Seguimiento de cotización </span><br>
														Procedimiento para programar seguimientos a cotizaciones cuando el cliente no se ha decidido a contratar, pero tampoco ha cancelado.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 628 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 79%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499457333" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title"> Catálogo descuentos</span>
														<br>
														Descripción detallada del catálogo de descuentos.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 918 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 90%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499458091" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Catálogo extras</span><br>
														Descripción detallada del catálogo de extras.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 630 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 67%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499459183" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Catálogo fuentes de origen</span><br>
														Descripción detallada del catálogo de fuentes de origen.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 409 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 88%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499459922" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title"> Catálogo plagas</span>
														<br>
														Descripción detallada del catálogo de plagas.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 901 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 80%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499460774" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Catálogo tipos de servicio</span><br>
														Descripción detallada del catálogo de tipos de servicio.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 707 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 82%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/499460774" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Catálogo tipos de servicio</span><br>
														Descripción detallada del catálogo de tipos de servicio.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 570 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 75%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section>
							<div class="row container-fluid">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h3 class="panel-title"><span class="h3">Nuevos</span><a href="#" class=""></a></h3>
									</div>
									<div class="panel-body">
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515482985" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Introducción al módulo agenda</span>
														<br>
														En éste video se da una pequeña introducción al módulo de agenda, uno de los módulos más importantes, ya que en éste módulo es dónde
														se agendan todas las cotizaciones que procederán a la contratación del servicio.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 718 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 87%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515486770" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Agendar OS</span><br>
														Explicación detallada del proceso paso a paso para agendar una orden de servicio una vez que el cliente esté decidido en la contratación.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 807 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 71%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515489442" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Introducción opciones - OS</span><br>
														Breve explicación de las diferentes opciones que presentan las órdenes de servicio (OS, R, S ó G) en las distintas etapas que se presentan.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 577 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 93%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515492447" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Editar OS</span>
														<br>
														Explicación detallada y paso a paso del como modificar los datos capturados de las órdenes de servicio dentro de Pestware App.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 911 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 90%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515494446" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Cancelar OS</span><br>
														Explicación detallada y paso a paso del cómo se cancela una orden de servicio dentro de Pestware App.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 787 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 92%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515496029" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Cobrar OS</span><br>
														Explicación detallada y paso a paso del cómo se debe cobrar una orden de servicio dentro de Pestware App.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 770 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 95%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515497601" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Enviar indicaciones</span>
														<br>
														Explicación detallada y paso a paso del cómo se envían las indicaciones que los clientes deberán seguir al momento de la realización de un servicio
														para garantizar el bienestar y además cumplir con las condiciones óptimas para que el servicio sea efectivo.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 931 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 92%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515502219" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Enviar recordatorio</span><br>
														Explicación detallada y paso a paso del cómo se envían los recordatorios para que los clientes estén al tanto con la fecha y hora en la que se llevarán a cabo los servicios
														y así puedan confirmar que estarán disponibles o de lo contrario proceder con la cancelación del servicio.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 887 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 89%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515504696" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Confirmar OS</span><br>
														Explicación detallada de cómo confirmar la orden de servicio una vez que el cliente se haya puesto en contacto para asegurar la disponibilidad de la recepción del servicio (previo envío de recordatorio).
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 900 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 85%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515507167" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Ver OS</span>
														<br>
														Explicación detallada del contenido de una orden de servicio que ya ha sido finalizada (datos del cliente, de la empresa, del técnico,
														condiciones del lugar, características generales del servicio, etc.).
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 711 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 84%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515509599" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Enviar OS</span><br>
														Explicación detallada y paso a paso del cómo se envían las órdenes de servicio finalizadas a los correos de contacto de los clientes.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 917 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 79%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515511535" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Evaluar servicio</span><br>
														Explicación del proceso de evaluación de un servicio finalizado emitida por el cliente.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 703 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 95%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
										<div class="card-deck">
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515525132" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Programar Refuerzo-Garantía-Seguimiento</span>
														<br>
														Explicación detallada y paso a paso del cómo se programan los refuerzos, garantías y seguimientos de los servicios finalizados (siempre y cuándo éstos apliquen).
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 821 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 94%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515528642" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Descargar OS</span><br>
														Explicación del proceso para la descarga en formato PDF de las órdenes de servicio.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 857 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 87%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
											<div class="card">
												<iframe
														width=100%" height="" src="https://player.vimeo.com/video/515529951" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="card-video-margin">
												</iframe>
												<div class="card-block">
													<p class="card-text-custom">
														<span class="badge badge-pill card-badge-warning text-capitalize">Principiante</span>
														<span class="card-p-title">Editar pago</span><br>
														Explicación detallada y paso a paso del cómo poder editar el pago efectuado por el cliente en caso de que requiera modificación.
													</p>
												</div>
												<div class="card-footer text-center">
													<div class="row">
														<div class="col-6 card-div-padding-custom">
															<i class="fa fa-user text-blue" aria-hidden="true"></i><span> 783 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 88%</span>
														</div>
														<div class="col-6 card-div-padding-custom">
															<a class="btn btn-primary btn-xl text-capitalize" style="margin: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>
														</div>
													</div>
													<!--													<div class="text-center">
                                                                                                            <i class="fa fa-user text-blue" aria-hidden="true"></i><span> 3000 </span><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i><span> 100%</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="badge card-badge-danger text-capitalize">Rebajas</span>
                                                                                                        </div>
                                                                                                        <div class="text-center">
                                                                                                            <span class="text-capitalize text-danger">100%</span><span class= text-danger> Descuento </span><span class="text-capitalize text-danger bold-span-price"> </span>
                                                                                                        </div>
                                                                                                        <a class="btn btn-primary btn-xl text-capitalize" style="margin-bottom: 10px"><i class="fa fa-cart-plus" aria-hidden="true"></i> $0 MXN</a>-->
												</div>
											</div>
										</div>
									</div>
								</div>


							</div>
						</section>
						<div class="table-responsive">
							@foreach($plans as $plan)
								<div class="container-fluid spark-screen">
									<div class="row">
										<div class="col-md-12">
											<!-- Default box -->
											<div class="box box-solid box-default">
												<div class="box-header with-border">
													<h3 class="box-title">{{ $plan->name }}</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
															<i class="fa fa-minus"></i>
														</button>
														<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
															<i class="fa fa-times"></i>
														</button>
													</div>
												</div>
												<div class="box-body">
														@foreach($modules as $module)
															<div class="container-fluid spark-screen">
																<div class="row">
																	<div class="col-md-12">
																		<!-- Default box -->
																		<div class="box box-warning collapsed-box">
																			<div class="box-header with-border">
																				<h3 class="box-title">{{ $module->name }}</h3>

																				<div class="box-tools pull-right">
																					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
																						<i class="fa fa-minus"></i>
																					</button>
																					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
																						<i class="fa fa-times"></i>
																					</button>
																				</div>
																			</div>
																			<div class="box-body">
																				<table class="table table-hover">
																					@if($module->training_plan_id == $plan->id)
																					<tr>
																						<th class="text-center">Título</th>
																						<th class="text-center">Duración</th>
																						<th class="text-center">Descripción</th>
																						<th class="text-center">Curso</th>
																						<th class="text-center">Status</th>
																						<th class="text-center">Resultado</th>
																					</tr>
																					@foreach($trainings as $training)
																						@if($module->module_id == $training->module_id)
																							<tr>
																								<td>{{ $training->title }}</td>
																								<td class="text-center">{{ $training->duration }}</td>
																								<td class="text-center">{{ $training->description }}</td>
																								<td class="text-center">
																									<a href="{{ route('training_course', $training->id) }}" class="btn"><i class="fa fa-youtube-play fa-3x" aria-hidden="true" style="color: #3400AB"></i></a>
																								</td>
																								@foreach($progress as $p)
																									@if($training->id == $p->training_id)
																										@if($p->progress>=5 and $p->progress<=99)
																											<td class="text-center">
																												<button type="button" class="btn btn-warning" aria-label="Left Align" disabled="true" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title=" Porcentaje cursado: {{ $p->progress }}%">
																													<i class="fa fa-spinner" aria-hidden="true"></i>
																													EN CURSO </button>
																											</td>
																											<td class="text-center" width="15%">
																											</td>
																										@elseif($p->progress == 100)
																											<td class="text-center">
																												<button type="button" class="btn btn-success" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title=" Porcentaje cursado: {{ $p->progress }}%" disabled="true">
																													<i class="fa fa-check-square-o" aria-hidden="true"></i>
																													FINALIZADO
																												</button>
																											</td>
																											<td class="text-center" width="15%">
																												<?php $pass = 'noEvaluado'  ?>
																												@if($p->course_grades >= 80)
																													<button type="button" class="btn btn-success" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;" data-toggle="tooltip" title="Calificación {{$p->course_grades}}" disabled="disabled">
																														<i class="fa fa-graduation-cap" aria-hidden="true"></i>
																														APROBADO
																													</button>
																													@php $pass = 'aprobado' @endphp
																												@elseif($p->course_grades != null)
																													@php $pass = 'reprobado' @endphp
																												@endif
																												@if($pass != 'aprobado')
																													<a href="{{ route('intro', ['course' => $training->id]) }}" class="btn @if($pass == 'reprobado') btn-danger @else btn-info @endif " aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;">
																														<i class="fa fa-graduation-cap" aria-hidden="true"></i>
																														@if($pass == 'reprobado') Aplicar nuevamente @else EVALUACION @endif
																													</a>
																												@endif
																											</td>
																										@endif
																									@endif
																								@endforeach
																							</tr>
																						@endif
																					@endforeach
																				@endif
																				</table>
																			</div>
																			<!-- /.box-body -->
																		</div>
																		<!-- /.box -->
																	</div> <!-- col-md-12 -->
																</div>
															</div>
														@endforeach
												</div>
												<!-- /.box-body -->
											</div>
											<!-- /.box -->
										</div> <!-- col-md-12 -->
									</div>
								</div>
							@endforeach
						</div>
						{{ $plans->links() }}
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div> <!-- col-md-12 -->
		</div>
	</div>
@endsection
