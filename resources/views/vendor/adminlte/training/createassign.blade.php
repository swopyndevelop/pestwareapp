@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop
@section('main-content') 
<div class="content container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">CAPACITACI&Oacute;N</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                          <form action="{{ Route('training_store_assign') }}" method="POST" role="form" enctype="multipart/form-data">
                           {{ csrf_field() }}
                           @include('adminlte::training._formassign')
                       </form>
                   </div>
                   <!-- /.box-body -->
               </div>
               <!-- /.box -->
           </div>
       </div>
   </div>
   @endsection
   @section('personal-js')
   <script>
    $(".multiple").select2();
</script>
@endsection