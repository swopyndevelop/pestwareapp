@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="content container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h2 class="titleCenter">Plan de capacitaci&oacute;n</h2>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						@if (session()->has('message_update'))

						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>{{ session()->get('message_update')}}</strong>
						</div>

						@endif
						<div class="box-body">
							<div class="row">
								<div class="col-md-4 col-md-offset-8 col-sm-12 col-xs-12">	
									<a href="{{ route('training_create_assign') }}" class="btn btn-primary-dark btn-md">
										<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Agregar Plan
									</a>
									<a href="{{ route('training_create_assign_plan') }}" class="btn btn-primary-dark btn-md">
										<span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> Asignar Plan
									</a>
								</div>
							</div>
							<br>
							<div class="table-responsive">
								<table class="table table-hover">
									<thead class="table-general">
										<tr>
											<th class="tableCourse">Titulo</th>
											<th class="tableCourse">Descripción</th>
											<th>Detalle</th>
											<th>Actualizar</th>
											<th>Eliminar</th>
										</tr>
									</thead>
									<tbody>
										@foreach($trainingplans as $trainingplan)
										<tr>
											<td>{{ $trainingplan->name }}</td>
											<td>{{ $trainingplan->description }}</td>
											<td>
												<a href="{{ route('training_personal_study', ['id'=> $trainingplan->id]) }}" class="btn">
													<i class="fa fa-eye fa-2x" aria-hidden="true" style="color: #00AB7F;"></i>
												</a>
											</td>
											<td>
												<a href="{{ route('training_edit_assign', ['trainingplan'=> $trainingplan->id]) }}" class="btn">
													<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
												</a>
											</td>
											<td>
												<form action="{{ route('training_delete_assign', ['trainingplan' => $trainingplan->id]) }}" method="POST">
													{{ csrf_field() }}
													{{ method_field('DELETE') }}
													<button type="submit" class="btn" style="background-color: transparent;">
														<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
													</button>
												</form>	
											</td>
										</tr>
										@endforeach
									</tbody>


								</table>
							</div>
							{{ $trainingplans->links() }}

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
		@endsection
