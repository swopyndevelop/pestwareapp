<!-- INICIA MODAL PARA ENVIAR EMAIL SERVICIO -->
<div class="modal fade" id="sendEmailInspectionArea" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="box-body">
        <h4 class="modal-title text-center col-lg-12 text-info">Enviar Inspección Área</h4>
        <label for="email" style="font-weight: bold;">Correo Electrónico:</label>
        <input type="text" id="emailText" value="" class="form-control">
        <br>
        <label for="email" style="font-weight: bold;">Otro Correo Electrónico:</label>
        <br>
        <input type="text" name="email" id="email" class="form-control">
      </div>
      <div class="modal-footer">
        <div class="text-center">
          <button class="btn btn-primary btn-sm" type="button" id="sendEmailInspectionAreaButton" name="sendEmailInspectionAreaButton">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL SERVICIO -->