@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Inspecciones de Áreas</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-lg-6 margin-mobile">
                            <select name="selectInspectionAreaCustomer" id="selectInspectionAreaCustomer" class="form-control" style="font-weight: bold;">
                                @foreach($jobCenters as $jobCenter)
                                    @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                        <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                            {{ $jobCenter->name }}
                                        </option>
                                    @else
                                        <option value="{{ $jobCenter->id }}">
                                            {{ $jobCenter->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 margin-mobile">

                        </div>
                        <div class="row">

                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Área</th>
                                        <th># Orden Servicio</th>
                                        <th>Fecha / Hora de Inspección</th>
                                        <th>Cliente / Empresa</th>
                                        <th>Domicilio</th>
                                        <th># Áreas Inspeccionadas</th>
                                    </tr>
                                    <tr>
                                        <th class="">
                                        <span @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
                                            <button id="btnFilterInspection" class="btn btn-default btn-md">
                                                <i class="glyphicon glyphicon-search" @if(Auth::user()->id_plan == 1) disabled @endif></i> Filtrar
                                            </button>
                                        </span>
                                        </th>
                                        <th scope="col">
                                            <input type="text" name="idServiceOrderInspection" id="idServiceOrderInspection" class="form-control">
                                        </th>
                                        <th scope="col">
                                            <input type="text" name="filterDateInspection" id="filterDateInspection" class="form-control" style="font-size: .7em">
                                        </th>
                                        <th scope="col">
                                            <select id="customersFilter" class="form-control applicantsList-single">
                                                <option value="0" selected>Todos</option>
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id }}">
                                                        {{ $customer->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th scope="col">
                                            <select id="addressFilter" class="form-control applicantsList-single">
                                                <option value="0" selected>Todos</option>
                                                @foreach($addressCustomers as $addressCustomer)
                                                    <option value="{{ $addressCustomer->id }}">
                                                        {{ $addressCustomer->address }}, {{ $addressCustomer->address_number }} {{ $addressCustomer->colony }}
                                                        {{ $addressCustomer->municipality }} {{ $addressCustomer->state }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th class="text-center">
                                            #
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody id="tableBodyInspections">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@include('vendor.adminlte.areamonitoring._modalDetailInspection')
@include('vendor.adminlte.areamonitoring._modalSendEmailInspectionArea')
@include('vendor.adminlte.areamonitoring._modalSendEmailWhatsapp')
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/areaInspections.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>//Select Job Center
        $('#selectInspectionAreaCustomer').on('change', function() {
            let InspectionAreaCustomer = $(this).val();
            window.location.href = route('index_inspections_area', InspectionAreaCustomer);
        });
        $(".applicantsList-single").select2();
    </script>
@endsection
