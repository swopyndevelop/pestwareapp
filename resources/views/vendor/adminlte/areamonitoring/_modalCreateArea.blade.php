<!--START CREATE NEW AREA-->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="newAreaMonitoringClientModal" role="dialog" aria-labelledby="newAreaMonitoringClientModal">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="modal-title col-md-12">
                                    <div class="col-md-7">
                                        <h4 class="text-right text-info" id="modalTitle">Nueva Área</h4>
                                    </div>
                                    <div class="col-md-5">
                                        <h4 class="text-right text-info" id="modalTitleFolio"><b class="text-right folioMonitoring">A-###</b></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="radio-inline"><input type="radio" name="optradio" checked id="inspection">Inspección</label>
                                        <label class="radio-inline"><input type="radio" name="optradio" id="order">Orden de Servicio</label>
                                    </div>
                                </div>
                                <div class="row" style="display: none" id="configMultiple">
                                    <div class="col-md-3">
                                        <label for="zone">Zona:</label>
                                        <select class="form-control" name="zone" id="zone">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="perimeter">Perímetro:</label>
                                        <select class="form-control" name="zone" id="perimeter">
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="commonArea">Área en común:</label>
                                        <input class="form-control" type="text" id="commonArea">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="commonArea">Rango inicial:</label>
                                        <input class="form-control" type="number" id="commonArea">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="commonArea">Rango final:</label>
                                        <input class="form-control" type="number" id="commonArea">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-8">
                                        <select id="selectFilterCustomerName" class="applicantsList-single form-control header-table" style="width: 100%;">
                                            <option value="0" selected disabled>Buscar cliente...</option>
                                            @foreach($customers as $customer)
                                                <option value="{{ $customer->id }}" data-name="{{ $customer->name }}" data-establishment="{{ $customer->establishment_name }}">
                                                    {{ $customer->name }} / {{ $customer->establishment_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-secondary btn-sm" id="loadNodesMultiple"><i class="fa fa-upload"></i> Carga Masiva</button>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-primary btn-sm" id="saveTreeStationMonitoringClient"><i class="glyphicon glyphicon-floppy-save"></i> Guardar Área</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="jsTreeAreaMonitoring" class="demo" style="margin-top:1em; min-height:200px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END CREATE NEW AREA-->
