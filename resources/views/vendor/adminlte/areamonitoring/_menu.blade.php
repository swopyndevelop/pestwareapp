<ul class="dropdown-menu">
    <li><a class="openModalShowAreaMonitoring" data-toggle="modal" data-target="#showAreaMonitoringModal" style="cursor: pointer"
           data-id="{{ $area->id }}"
           data-inspection="{{ $area->is_inspection }}"
           data-monitoring="{{ $area->id_area }}">
            <i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>
            Ver Diagrama
        </a>
    </li>
    @if($area->status == 0 || $area->status == 2)
        <li><a class="openModalUpdateAreaMonitoring" style="cursor: pointer"
               data-id="{{ $area->id }}"
               data-customerId="{{ $area->customer_id }}"
               data-customerName="{{ $area->customer . " / " . $area->establishment_name }}"
               data-inspection="{{ $area->is_inspection }}"
               data-monitoring="{{ $area->id_area }}"
                data-amount="{{ $area->amount }}">
                <i class="fa fa-pencil-square-o" aria-hidden="true" style="color: darkgreen;"></i>
                Editar
            </a>
        </li>
        @if(Entrust::can('Eliminar Datos')  || Entrust::hasRole('Cuenta Maestra'))
            @if($area->lastInspection == null)
            <li><a data-toggle="modal" data-target="#deleteArea{{ $area->id }}" style="cursor: pointer"><i class="fa fa-trash" aria-hidden="true" style="color: red;"></i>Eliminar</a></li>
            @endif
        @endif
    @endif
    @if($area->status == 0)
        <li><a href="{{ route('area_print_card_qr',\Vinkla\Hashids\Facades\Hashids::encode($area->id)) }}"><i class="fa fa-qrcode" aria-hidden="true" style="color: dodgerblue;"></i>Imprimir QRs</a></li>
    @endif
    @if($area->status == 2)
        @if($area->diffInSeconds == true)
            <li><a href="{{ route('area_print_card_qr',\Vinkla\Hashids\Facades\Hashids::encode($area->id)) }}"><i class="fa fa-qrcode" aria-hidden="true" style="color: dodgerblue;"></i>Imprimir QRs</a></li>
        @endif
    @endif
</ul>
@include('vendor.adminlte.areamonitoring._modalCustomQr')
@include('vendor.adminlte.areamonitoring._modalConfirmDelete')
@include('vendor.adminlte.areamonitoring._modalShowTree')