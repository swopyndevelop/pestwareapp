<!-- INICIA MODAL PARA ELIMINAR MONITOREO -->
<div class="modal fade" id="deleteArea<?php echo $area->id; ?>" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <form action="{{ route('area_monitoring_tree_delete', ['id' => $area->id]) }}" method="POST" id="form">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-body" style="text-align: center;">
                    <h4 class="modal-title">¿Está seguro de eliminar el Área?</h4>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <div class="text-center">
                        <button type="submit" title="Eliminar Área" class="btn btn-primary" data-toggle="modal" data-target="#tracing<?php echo $area->id; ?>"><strong>Aceptar</strong></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA ELIMINAR MONITOREO -->