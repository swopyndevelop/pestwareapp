@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Áreas de Clientes</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                <a type="submit" class="btn btn-primary-dark openModalNewAreaV2">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    Nueva Área
                                </a>
                            </div>
                        <div class="col-lg-5 margin-mobile">
                            <select name="selectJobCenterAreaMonitoring" id="selectJobCenterAreaMonitoring" class="form-control" style="font-weight: bold;">
                                @foreach($jobCenters as $jobCenter)
                                    @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                        <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                            {{ $jobCenter->name }}
                                        </option>
                                    @else
                                        <option value="{{ $jobCenter->id }}">
                                            {{ $jobCenter->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-5 margin-mobile">
                            {!!Form::open(['method' => 'GET','route'=>'index_area_station_monitoring'])!!}
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar por: (Cliente)" name="search" id="search" value="">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                </span>
                            </div>
                            {!!Form::close()!!}
                        </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Área</th>
                                        <th>Fecha / Hora de Creación</th>
                                        <th>Cliente / Empresa</th>
                                        <th>Domicilio</th>
                                        <th># Áreas</th>
                                        <th>Última Inspección</th>
                                        <th class="text-center">QRs PDF</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($areas as $area)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                                        {{ $area->id_area }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.areamonitoring._menu')
                                                </div>
                                            </td>
                                            <td>{{ $area->created_at }}<br>{{ $area->user }}</td>
                                            <td>{{ $area->customer }}<br>{{ $area->establishment_name }}</td>
                                            <td>{{ $area->address }} #{{ $area->address_number }},<br>{{ $area->colony }},<br>{{ $area->municipality }}, {{ $area->state }}</td>
                                            <td>
                                                @foreach($area->nodes as $node)
                                                    {{ $node->count }} {{ $node->type_node }}<br>
                                                @endforeach
                                            </td>
                                            <td class="text-center">
                                                @if($area->lastInspection != null)
                                                    <a href="{{ route('index_inspections_area') }}">{{ $area->id_area }}</a><br>{{ $area->lastInspection->created_at }}
                                                @else
                                                    Sin Inspecciones
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($area->status == 1)
                                                    <span class="label label-warning" data-toggle="tooltip"
                                                          data-placement="top"
                                                          title="Tu documento se esta generando y estará listo para descargar en unos minutos.">Generando PDF...</span>
                                                @endif
                                                @if($area->url_pdf != null && $area->diffInSeconds == false)
                                                    <a href="{{ Route('download_area_pdf_qrs', \Vinkla\Hashids\Facades\Hashids::encode($area->id)) }}" target="_blank">
                                                        <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 2.5em;color:red"></i>
                                                    </a>
                                                    <br>
                                                    <span data-toggle="tooltip"
                                                          data-placement="top"
                                                          title="Tu documento esta actualizado.">{{ $area->date_updated_file }}</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $areas->links() }}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
    @include('vendor.adminlte.areamonitoring._modalCreateArea')
    @include('vendor.adminlte.areamonitoring._modalCreateAreaV2')
    @include('vendor.adminlte.areamonitoring._modalUpdateArea')
    @include('vendor.adminlte.areamonitoring._modalRelocateStation')
@endsection

@section('personal-js')
    <script src="{{ asset('/js/jstree-monitoring.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/areaStationMonitoring.js') }}"></script>
    <script>
        $('#selectJobCenterAreaMonitoring').on('change', function() {
            let idJobCenterAreaMonitoring = $(this).val();
            window.location.href = route('index_area_station_monitoring', idJobCenterAreaMonitoring);
        });
        $(".applicantsList-single").select2();
    </script>
@endsection
