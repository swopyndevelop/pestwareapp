<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Tarjetas Qrs</title>
</head>
<style>
    @page {
        margin: 0px;
    }
    body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
    }
    th{
        width: 25%;
        border: dotted;
        font-size: 8pt;
    }
</style>
<body>
{{--//Tables Qrs--}}
<div style="margin: 30px;">
    <table style="width: 62%; text-align: left;">
        <tbody>
        @foreach($rows as $i => $row)
            <tr>
                @foreach($row as $j => $station)
                    <th>

                            <div style="margin: 5px; text-align: left;">

                                <div style="width: 80px; display: inline-block; vertical-align: top; margin-top: 0px;">
                                    <img src="{{ env('URL_STORAGE_FTP').$company->pdf_logo }}" alt="" width="40px" height="25px" style="margin-top: 3px">
<!--                                    <img src="https://storage.pestwareapp.com/logos/0MoEUC9cPPByHCiMujJBZLXiAhA7ygHmC2Ecu2yC.png" alt="" width="35px" height="25px" style="margin-top: 3px">-->
                                    <p style="color:black;font-size:7pt; font-weight: bold; margin-top: 0px;margin-bottom: 0px;">{{ $station['instance'] }}</p>
                                    <p style="font-size:5pt; margin-top: 0px;margin-bottom: 0px">Zona: {{ $station['zone'] }}</p>
                                    <p style="font-size:5pt; margin-top: 0px;margin-bottom: 0px">Perímetro: {{ $station['perimeter'] }}</p>
                                </div>
                                <div style="width: 65px; display: inline-block; margin-top: 0px; vertical-align: top; text-align: center; margin-top: 15px">
                                    <span style="color:black;font-size:10pt; margin-bottom: 0px">No. Área</span>
                                    <p style="color:black;font-size:10pt; font-weight: bold; margin-top: 0px; margin-bottom: 0px">{{ $station['area'] }}</p>
                                </div>
                                <div style="width: 65px; display: inline-block; text-align: center; margin-top: 0px; vertical-align: top;">
                                    <img style="margin-top: 0px;" src="data:image/png;base64, {!! $station['qr'] !!}"><br>
<!--                                    <img src="{{ asset( 'img/pestware_large.png') }}" height="15px" style="margin-top: 15px;">-->
                                    <img src="https://pestwareapp.com/img/pestware_large.png" height="15px" width="40px" style="margin-top: 5px">
                                </div>

                            </div>
                        </th>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
