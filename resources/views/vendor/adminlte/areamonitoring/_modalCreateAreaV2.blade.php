<!--START CREATE NEW AREA-->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="newAreaModalV2" role="dialog" aria-labelledby="newAreaModalV2" style="overflow-y: scroll;">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document" style="width: 90% !important;">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="modal-title col-md-12">
                                    <div class="col-md-7">
                                        <h4 class="text-right text-info" id="modalTitleModalAreaV2">Nueva Área</h4>
                                    </div>
                                    <div class="col-md-5">
                                        <h4 class="text-right text-info" id="modalTitleFolio"><b class="text-right folioMonitoring">A-###</b></h4>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                                                <b>Seleccionar Cliente</b>
                                                <span class="badge pull-right">PASO 1</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row container-fluid">
                                                    <div class="col-md-12">
                                                        <div class="col-md-5">
                                                            <label class="radio-inline"><input type="radio" name="optRadio" id="inspectionCheck" checked>Inspección</label>
                                                            <label class="radio-inline"><input type="radio" name="optRadio" id="orderCheck">Orden de Servicio</label>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input class="form-control" type="number" id="amount" style="display: none; width: 50%" placeholder="Monto">
                                                        </div>
                                                        <div class="col-md-3">
                                                        </div>
                                                        <div class="col-md-12">
                                                            <hr>
                                                            <select id="selectFilterCustomerNameV2" class="applicantsList-single form-control header-table" style="width: 100%;">
                                                                <option value="0" selected disabled>Buscar cliente...</option>
                                                                @foreach($customers as $customer)
                                                                    <option value="{{ $customer->id }}" data-name="{{ $customer->name }}" data-establishment="{{ $customer->establishment_name }}">
                                                                        {{ $customer->name }} / {{ $customer->establishment_name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-primary" style="display: none;" id="panelStep2">
                                            <div class="panel-heading">
                                                <span class="fa fa-square-o" aria-hidden="true"></span>
                                                <b>Crear Zonas</b>
                                                <span class="badge pull-right">PASO 2</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <input type="text" id="nameZone" class="form-control" placeholder="Nombre de la zona Ejemplo: Exterior">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button id="saveZone" class="btn btn-block btn-secondary">Guardar Zona</button>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 container-fluid" style="display: none;" id="tableZones">
                                                        <br>
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center"># Zona</th>
                                                                <th class="text-center">Zona</th>
                                                                <th class="text-center">Acciones</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tableBodyDetailZones">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-primary" style="display: none;" id="panelStep3">
                                            <div class="panel-heading">
                                                <span class="fa fa-circle-o-notch" aria-hidden="true"></span>
                                                <b>Crear Perímetros</b>
                                                <span class="badge pull-right">PASO 3</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <select name="" id="zoneSelect" class="form-control"></select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" id="namePerimeter" class="form-control" placeholder="Nombre del perímetro: Ejemplo: Lobby">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button id="savePerimeter" class="btn btn-block btn-secondary">Guardar Perímetro</button>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 container-fluid" style="display: none;" id="tablePerimeters">
                                                        <br>
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center"># Perímetro</th>
                                                                <th class="text-center">Perímetro</th>
                                                                <th class="text-center">Acciones</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tableBodyDetailPerimeters">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-primary" style="display: none;" id="panelStep4">
                                            <div class="panel-heading">
                                                <span class="fa fa-cubes" aria-hidden="true"></span>
                                                <b>Crear Áreas</b>
                                                <span class="badge pull-right">PASO 4</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <select id="perimeterSelect" class="form-control"></select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" id="nameArea" placeholder="Nombre del área" class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <input class="form-control" type="number" id="initialRange" placeholder="Rango inicial">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <input class="form-control" type="number" id="finalRange" placeholder="Rango Final">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button id="saveStation" class="btn btn-block btn-secondary">Guardar Áreas</button>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                    </div>
                                                    <div class="col-md-3">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label for="initialRange"><input id="checkboxOnlyArea" type="checkbox"> Única Área</label>
                                                    </div>
                                                    <div class="col-md-2">
                                                    </div>
                                                    <div class="col-md-3">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 container-fluid" style="display: none;" id="tableStations">
                                                        <br>
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center">Zona</th>
                                                                <th class="text-center">Perímetro</th>
                                                                <th class="text-center">Estación</th>
                                                                <th class="text-center">Cantidad</th>
                                                                <th class="text-center">Acciones</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tableBodyDetail">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <span class="fa fa-code-fork" aria-hidden="true"></span>
                                                <b>Diagrama de Áreas</b>
                                                <span class="badge pull-right">Previsualización</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12" id="container-tree-pv">
                                                        <div id="jsShowTreeAreaMonitoringPv" class="demo" style="margin-top:1em; min-height:200px;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <span class="fa fa-code-fork" aria-hidden="true"></span>
                                                <b>Lista de Estaciones</b>
                                                <span class="badge pull-right">Ajustes</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12 container-fluid" id="tableStationsMap">
                                                        <br>
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center">Estación</th>
                                                                <th class="text-center">Reubicar</th>
                                                                <th class="text-center">Estatus</th>
                                                                <th class="text-center">Eliminar</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tableDetailStationMap">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="text-center">
                                        <button id="finishedMonitoring" class="btn btn-primary-dark" style="visibility: hidden;">Guardar Área</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END CREATE NEW AREA-->
