<!--START CREATE NEW AREA-->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="relocateStation" role="dialog" aria-labelledby="relocateStation">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="modal-title col-md-12">
                                    <div class="col-md-12">
                                        <h4 class="text-center text-info"><b class="text-center" id="labelTextStation"></b></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="container-tree">
                                        <h5 class="text-center text-info">Reubicar Estación</h5>
                                        <select id="perimeterSelectSingular" class="form-control"></select>
                                        <h5 class="text-center text-info">Nombre Estación</h5>
                                        <input type="text" name="nameStationArea" id="nameStationArea" class="form-control">
                                    </div>
                                </div>
                                <div class="footer">
                                    <div class="text-center">
                                        <button id="btnRelocateStation" class="btn btn-primary" style="margin: 5px">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END CREATE NEW AREA-->
