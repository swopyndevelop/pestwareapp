    @extends('adminlte::layouts.app')

    @section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
    @endsection
    @section('main-content')

    <link rel="stylesheet" type="text/css" href="{{asset('css/grafica.css')}}">
    <div class="container-fluid spark-screen" id="courseevaluation-CompanyEvaluation">
    	<div class="row">
    		<div class="col-md-12">
    			<!-- Default box -->
    			<div class="box">
    				<div class="box-header with-border">
    					<h2 class="text-center">EVALUACIÓN POR EMPRESAS</h2>
    					<div class="box-tools pull-right">
    						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
    							<i class="fa fa-minus"></i>
    						</button>
    						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
    							<i class="fa fa-times"></i>
    						</button>
    					</div>
    				</div>
    				<div class="box-body">

                        <div class="row" style="padding: 2em;">
                          <div class="col-lg-4 col-sm-6 col-xs-12">
                             <div class="white-box">
                                <h3 class="box-title text-center">Ocupación</h3>
                                <div class="graphOcupation"></div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="white-box">
                      <h3 class="box-title text-center">Vacantes</h3>
                     
                    
                      <div class="graphVacant"></div>

                    
                      <div>
                        <div class="cuadrado1 left"></div>
                        <div class="left">&nbsp; Solicitantes &nbsp;</div>
                        <div id="Solicitantes" class="center">{{ number_format($porcentajesoli,2 )}}</div>
                      </div>
                      <br>
                      <div>
                        <div class="cuadrado2 left"> </div>
                        <div class="left">&nbsp; Examen psycométrico &nbsp;</div>
                        <div id="Psy" class="center">{{ number_format($porcentajepsy,2 )}}</div>
                      </div>
                      <br>
                      <div>
                        <div class="cuadrado3 left"> </div>
                        <div class="left">&nbsp; Contratados &nbsp;</div>
                        <div id="Con" class="center">{{ number_format($porcentajecontra,2 )}}</div>
                      </div>

                    </div>
                  </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                     <div class="white-box">
                        <h3 class="box-title text-center">Capacitación</h3>
                        <div class="graphTraining"></div>
                    </div>
                </div>
            </div>

            <div class="container col-md-8 col-md-offset-2">

                @php $sumContratado = 0; $sumTotal = 0 @endphp
                @php $sumContradado = $work_capacities @endphp
                 @if(empty($work_capacities->total)) 
                    <input type="hidden" id="Work_capacities" value="0">
                @else
                    <input type="hidden" id="Work_capacities" value="{{$work_capacities->total}}">
                @endif
                 @if(empty($employeestotal)) 
                    <input type="hidden" id="EmployeesTotal" value="0">
                @else
                    <input type="hidden" id="EmployeesTotal" value="{{$employeestotal}}">
                @endif
                 @if(empty($progress_total)) 
                    <input type="hidden" id="ProgressTotal" value="0">
                @else
                    <input type="hidden" id="ProgressTotal" value="{{$progress_total->promedio}}">
                @endif
                

                               <div id="jstree_kardex" class="demo">
                                    <ul class="list-group">
                                    @foreach($column as $raw)
                                    @if($raw['promedio'] >= 0 && $raw['promedio'] <= 24)
                                        <li class="list-group-item d-flex justify-content-between align-items-center"> {{$raw['text']}}
                                            
                                                &nbsp;<span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$raw['promedio'], 2, '.', '')}} %</span>
                                    @elseif($raw['promedio'] >=25 && $raw['promedio'] <= 74)
                                    <li class="list-group-item d-flex justify-content-between align-items-center"> {{$raw['text']}}
                                            
                                                &nbsp;<span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$raw['promedio'], 2, '.', '')}} %</span>
                                    @elseif($raw['promedio'] >=75 && $raw['promedio'] <=99)
                                    <li class="list-group-item d-flex justify-content-between align-items-center"> {{$raw['text']}}
                                            
                                                &nbsp;<span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$raw['promedio'], 2, '.', '')}} %</span>
                                    @elseif($raw['promedio'] == 100)
                                    <li class="list-group-item d-flex justify-content-between align-items-center"> {{$raw['text']}}
                                            
                                                &nbsp;<span style="font-size: 0.5em;" class="btn btn-success btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$raw['promedio'], 2, '.', '')}} %</span>
                                    @endif
                                                <ul class="list-group">

                                                        @foreach($zonas as $zone)
                                                            @if($zone['parent_id'] == $raw['id'])
                                                            @if($zone['promedio'] >= 0 && $zone['promedio'] <=24)
                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                
                                                                {{$zone['text']}}
                                                            
                                                                <span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="btn btn-danger btn-xs" class="border border-primary badge badge-primary badge-pill">{{number_format((float)$zone['promedio'], 2, '.', '')}} %</span>
                                                        
                                                        
                                                        @elseif($zone['promedio'] >=25 && $zone['promedio'] <=74)
                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                
                                                                {{$zone['text']}}
                                                            
                                                                <span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="btn btn-danger btn-xs" class="border border-primary badge badge-primary badge-pill">{{number_format((float)$zone['promedio'], 2, '.', '')}} %</span>
                                                        
                                                        
                                                        @elseif($zone['promedio'] >=75 && $zone['promedio'] <=99)
                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                
                                                                {{$zone['text']}}
                                                            
                                                                <span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="btn btn-danger btn-xs" class="border border-primary badge badge-primary badge-pill">{{number_format((float)$zone['promedio'], 2, '.', '')}} %</span>
                                                        
                                                        
                                                        @elseif($zone['promedio'] == 100)
                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                
                                                                {{$zone['text']}}
                                                            
                                                                <span style="font-size: 0.5em;" class="btn btn-success btn-xs" class="btn btn-danger btn-xs" class="border border-primary badge badge-primary badge-pill">{{number_format((float)$zone['promedio'], 2, '.', '')}} %</span>

                                                            @endif
                                                        

                                                                <ul class="list-group">
                                                                    @foreach($determinantes as $det)
                                                                    @if($det['parent_id'] == $zone['id'])
                                                                    @if($det['promedio'] >= 0 && $det['promedio'] <=24)
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                        {{$det['text']}}

                                                                        <span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$det['promedio'], 2, '.', '')}} %</span>

                                                                    @elseif($det['promedio'] >= 25 && $det['promedio'] <=74)
                                                                    
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                        {{$det['text']}}

                                                                        <span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$det['promedio'], 2, '.', '')}} %</span>

                                                                    @elseif($zone['promedio'] >=75 && $zone['promedio'] <=99)
                                                                    
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                        {{$det['text']}}

                                                                        <span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$det['promedio'], 2, '.', '')}} %</span>

                                                                    @elseif($zone['promedio'] == 100)
                                                                    
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                        {{$det['text']}}

                                                                        <span style="font-size: 0.5em;" class="btn btn-success btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$det['promedio'], 2, '.', '')}} %</span>
                                                                    @endif
                                                                
                                                                        
                                                                        <ul class="list-group">
                                                                            @foreach($sucursal as $suc)
                                                                            @if($suc['parent_id'] == $det['id'])
                                                                            @if($suc['promedio'] >= 0 && $suc['promedio'] <= 24)
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                                {{$suc['text']}}

                                                                                <span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$suc['promedio'], 2, '.', '')}} %</span>

                                                                            @elseif($suc['promedio'] >=25 && $suc['promedio'] <= 74)
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                                {{$suc['text']}}

                                                                                <span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$suc['promedio'], 2, '.', '')}} %</span>

                                                                            @elseif($suc['promedio'] >=75 && $suc['promedio'] <= 99)
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                                {{$suc['text']}}

                                                                                <span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$suc['promedio'], 2, '.', '')}} %</span>

                                                                            @elseif($suc['promedio'] == 100)
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                                {{$suc['text']}}

                                                                                <span class="btn btn-success btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$suc['promedio'], 2, '.', '')}} %</span>
                                                                            @endif
                                                                            

                                                                                <ul class="list-group">
                                                                                    @foreach($empleado as $emp)
                                                                                    @if($emp['parent_id'] == $suc['id'])
                                                                                    @if($emp['promedio'] >= 0 && $emp['promedio'] <= 24)
                                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                                        {{$emp['text']}}        
    
                                                                                    <span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$emp['promedio'], 2, '.', '')}} %</span>

                                                                            @elseif($emp['promedio'] >= 25 && $emp['promedio'] <= 74)
                                                                            
                                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                                        {{$emp['text']}}        
    
                                                                                    <span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$emp['promedio'], 2, '.', '')}} %</span>

                                                                            @elseif($emp['promedio'] >= 75 && $emp['promedio'] <= 99)
                                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                                        {{$emp['text']}}        
    
                                                                                    <span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$emp['promedio'], 2, '.', '')}} %</span>

                                                                            @elseif($emp['promedio'] == 100)
                                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">                  
                                                                                        {{$emp['text']}}        
    
                                                                                    <span style="font-size: 0.5em;" class="btn btn-success btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$emp['promedio'], 2, '.', '')}} %</span>
                                                                                    
                                                                                    </li>
                                                                                    
                                                                                    @endif
                                                                                    @endif
                                                                                    @endforeach
                                                                                </ul>

                                                                            </li>
                                                                            @endif
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>
                                                                @endif
                                                                @endforeach
                                                                </ul>
                                                                
                                                             </li>
                                                            @endif

                                                        
                                                        @endforeach
                                                    
                                                </ul>
                                        </li>

                                    @endforeach
                                    </ul>
                                </div>








                </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
    </div>
    </div>
    @endsection
    @section('circliful')
    <script src="{{ asset('js/jquery.circliful.js')}}"></script>
    <link href="{{ asset('/css/jquery.circliful.css') }}" rel="stylesheet"/>
    @stop