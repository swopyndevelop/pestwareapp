<div class="row" style="margin-top: 2em" id="progress_bar">
	<div class="col-md-1 col-md-offset-1 col-xs-3">
		<h6><b><p class="text-right" style="font-size: 1.5em">Avance</p></b><h6>
		</div>

		<div class="col-md-1 col-xs-1 text-right">
			<h6>
				<b>
					<p>{{ str_limit($psychometric->id,$limit=3, $end='%') }}</p>
				</b>
			</h6>
		</div>

		<div class="progress col-xs-6 col-xs-offset-0 col-md-6 col-md-offset-0" style="border-radius: 9px;height: 2.5em">				<div class="progress-bar progress-bar-striped active col-xs-6" role="progressbar" id="progressBarText" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="border-radius: 9px; width: {{ str_limit($psychometric->id*100/52,$limit=3, $end='') }}%">
		</div>
	</div>

	<div class="col-md-1 col-xs-1 text-left">
		<h6>
			<b>
				<p id="letters_prueba">52</p>
			</b>
		</h6>
	</div>

	<!-- TEMPORIZADOR -->
	<div style="text-align:center; " class="col-md-1 col-xs-12 " id="muestraReloj">
		<div>
			<h6>
				<p style="font-size: 1.5em;">Tiempo Restante 
					<br>
					<span id="displayReloj"></span>
				</p>
			</h6>
		</div>
	</div>

</div>