@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
<style>
input.hidden-inputs.answer{
	width: 1.3em;
	height: 1.3em;
}
span.optionsEvaluation{
	font-size: 1.3em;
	text-align: justify;
	color: black;
	font-weight: normal;
	font-family: Gotham;
}
label.input-group.input-group-radio.row{
	margin-left: .5em;
}
span.text-muted{
	font-weight: bold;
}
</style>
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h2 class="box-title" style="font-size: 2em; margin: .5em">Resultados de Evaluación</h2> <br>
					<h3>
						Aspirante: {{ $usuario->name }} <br>
						Puesto: {{ $usuario->job_title_name }} 
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="col-md-8">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							@for ($i = 0; $i < count($question); $i++)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h2 class="panel-title text-left" style="font-size: 1.5em">
										Pregunta {{ $i+1 }}.  
										<a data-toggle="collapse" data-parent="#accordion" href="#{{ $i+1 }}" aria-expanded="true" aria-controls="{{ $i+1 }}">
											<i class="glyphicon glyphicon-chevron-down pull-right"></i>
										</a>
									</h2>
								</div>
								<div id="{{ $i+1 }}" class="panel-collapse collapse">
									<div class="form-horizontal panel-body">
										<div class="text-center">
											<span style="font-size: 1.5em;">{{ $question[$i]->question }}</span>
										</div>
										<div class="well col-md-12" style="font-size: 1.2em; margin-top: 2em;">
											<table class="table" width="">
												<thead>
													<tr>
														<th>Pregunta</th>
													</tr>
												</thead>
												<tbody>    
													<tr>
														<td>
															<span class="text-muted">
																@if($question[$i]->type == 'order')
																<ul style="list-style: none; text-align: justify; margin-left: -2em;">
																	@for ($j = 0; $j < count($option[$i]); $j++)
																	<div class="row">
																		<div class="col-md-10" style="margin-top: 1.3em">
																			<li data-id="{{ $option[$i][$j]->id }}">
																				<span class="optionsEvaluation">
																					{{ $j+1 }}. {!! $option[$i][$j]->option !!}
																				</span>
																			</li>
																		</div>
																		<div class="col-md-2 text-center"  style="margin-top: 1.3em">
																			@for ($z = 0; $z < count($answers[$i]); $z++) 
																			@if ($answers[$i][$z]->option_evaluations_id ==$option[$i][$j]->id) @if($z+1 == $option[$i][$j]->validation)
																			<i style="font-size: 1.5em; color: #f5216e" class="fa fa-check" aria-hidden="true"></i>
																			@else
																			<i style="font-size: 1em; color: red" class="fa fa-times" aria-hidden="true"></i>
																			@endif
																			@endif
																			@endfor
																		</div>
																	</div>
																	@endfor
																</ul>
																@else
																@for ($j = 0; $j < count($option[$i]); $j++)
																<div class="row">
																	<div class="col-md-1 text-center" style="margin-top: 1.57em;">
																		@if ($option[$i][$j]->validation == 'si')
																		<i style="font-size: 1.5em; color: green" class="fa fa-chevron-right" aria-hidden="true"></i>
																		@endif
																	</div>
																	<div class="col-md-10" style="margin-top: 1.3em;">
																		<label class="input-group input-group-radio row">
																			@if($question[$i]->type == 'only')
																			<input type="radio" disabled="true" class="hidden-inputs answer" name="answer" value="checked" @foreach($answers[$i] as $a) @if ($a->option_evaluations_id == $option[$i][$j]->id) checked = 'checked' @endif @endforeach />&#160;
																			<span class="optionsEvaluation"> {!! $option[$i][$j]->option !!}</span>
																			@elseif($question[$i]->type == 'multioption')

																			<input type="checkbox" disabled="true" class="hidden-inputs answer" name="answer" value="checked" @foreach($answers[$i] as $a) @if ($a->option_evaluations_id == $option[$i][$j]->id) checked = 'checked' @endif @endforeach />&#160;
																			<span class="optionsEvaluation"> {!! $option[$i][$j]->option !!}</span>
																			@endif
																		</label>
																	</div>
																	<div class="col-md-1 text-center"  style="margin-top: 1.3em">
																		@foreach($answers[$i] as $a) 
																		@if ($a->option_evaluations_id == $option[$i][$j]->id) 
																		@if ($option[$i][$j]->validation == 'si') <i style="font-size: 1.5em; color: #f5216e" class="fa fa-check" aria-hidden="true"></i>
																		@else 
																		<i style="font-size: 1.5em; color: red" class="fa fa-times" aria-hidden="true"></i>@endif
																		@endif 
																		@endforeach

																	</div>
																</div>
																@endfor
																@endif
															</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							@endfor
						</div>
					</div>
					<div class="col-md-4">
						<ul class="list-group">
							<li class="list-group-item lead text-center" style="background: #f5216e; color: white;">Resultados</li>
							<li class="list-group-item">
								<span style="font-size: 1.5em">Respuestas Correctas:</span>
								<span class="text-muted" style="font-size: 1.5em">{{ $tot }}/{{ $count }}</span>
							</li>
							<li class="list-group-item">
								<span style="font-size: 1.5em">Puntaje Total:</span>
								<span class="text-muted" style="font-size: 1.5em">{{ number_format(($tot/$count)*100) }}%</span>
								<input type="hidden" id="grade" value="{{ number_format(($tot/$count)*100) }}">
							</li>
						</ul>  
							<div id="test-circle4"></div>
						<div class="text-center">
							<a href="{{ route('training_kardex', [ 'training' => $usuario->id]) }}">
							<button class="btn btn-primary" id="closeResults" style="background: black; border-color: black; width: 10em;">CERRAR</button>
							</a>
						</div>
					</div>
					<!--box-body-->
				</div>
				<!--.box-->
			</div>
		</div>
		@endsection

		@section('circliful')
		<script src="{{ asset('js/jquery.circliful.js')}}"></script>
		<script>

    $( document ).ready(function() { // 6,32 5,38 2,34
    	$("#test-circle4").circliful({
    		animation: 1,
    		animationStep: 1,
    		foregroundColor: ($('#grade').val() > 69) ? 'green' : 'red',
    		foregroundBorderWidth: 20,
    		backgroundBorderWidth: 15,
    		percent: $('#grade').val(),
    		textSize: 30,
    		textStyle: 'font-size: 12px;',
    		textColor: '#666',
    		// halfCircle: 1,
    	});
    });
</script>
@stop