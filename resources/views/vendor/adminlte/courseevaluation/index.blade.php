@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@include('adminlte::courseevaluation.Personal_style')
<script type="text/javascript" src="{{ asset('/js/modernizr.custom.29258.js')}}"></script>

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h2 class="box-title" style="font-size: 2em; margin: .5em">Evaluación Encargado de Tienda</h2>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="col-md-offset-10">
						<h4>Tiempo restante</h4>
						<h4 id="displayRelojEvaluation"></h4>
					</div>
					<div class="col-md-1 col-md-offset-1">
						<h4 style="margin-top: -0.5em;margin-right: -2em">0</h4>
					</div>
					<div class="container text-center col-md-8">
						<div class="progress">
							<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style=" width: {{ str_limit(count($time)*100/$question->total(),$limit=3, $end='') }}%; font-size: 1.3em; font-weight: bold;">{{ number_format(count($time)*100/$question->total()) }}
							</div>
						</div>
					</div>
					<div class="col-md-1">
						<h4 style="margin-top: -0.5em;margin-left: -0em">100</h4>
					</div>
					<div class="col-md-12 text-center">
						<h3>¡Hola!<br>
						Por favor, invierte unos pocos minutos de tu tiempo para contestar el siguiente cuestionario.</h3>
						<input type="hidden" id="courseId" value="{{$course}}">
					</div>
					@foreach ($question as $q)
					<section class="last">
						<fieldset class="required">
							<h2>
								<div class="title-part">
									<input type="hidden" id="question_id" value="{{ $q -> id }}">
									{{ $question->currentPage()  }}.- {{ $q -> question }}
								</div>
							</h2>
							<div class="special-padding-row">
								<div class="label-cont col-md-10 col-md-offset-1">
									@if($q->type == 'order')
									<ul class="drag ui-sortable" data-drag="ot18158746">
										@if (count($answers) > 0)
										@foreach ($answers as $a)
										@foreach($option as $o)
										@if ($o->id == $a->option_evaluations_id)
										<li data-id="{{ $o->id }}">
											<input type="hidden" name="options" value="{{ $o->id }}" class="ord answer">
											<span>
												<strong class="ord"></strong>{!! $o->option !!}
											</span>
										</li>
										@endif
										@endforeach
										@endforeach
										@else
										@for ($i = 0; $i < count($option); $i++)
										<li data-id="{{ $option[$i]->id }}">
											<input type="hidden" name="options" value="{{ $option[$i]->id }}" class="ord answer">
											<span>
												<strong class="ord">{{ $i+1 }}. </strong>{!! $option[$i]->option !!}
											</span>
										</li>
										@endfor
										@endif
									</ul>
									@else
									@for ($i = 0; $i < count($option); $i++)
									<label class="input-group input-group-radio row">
										@if($q->type == 'only')
										<input type="radio" class="hidden-inputs answer" name="answer" value="{{ $option[$i]->id }}" @foreach($answers as $ans) @if (isset($ans) && $ans->option_evaluations_id == $option[$i]->id) checked = 'checked' @endif @endforeach/>
										<span class="input-group-addon"></span>
										<span class="input-group-title">{!! $option[$i]->option !!}</span>
										@elseif($q->type == 'multioption')
										<input type="checkbox" class="hidden-inputs answer" name="answer" value="{{ $option[$i]->id }}" @foreach($answers as $ans) @if (isset($ans) && $ans->option_evaluations_id == $option[$i]->id) checked = 'checked' @endif @endforeach  />
										<span class="input-group-addon"></span>
										<span class="input-group-title">{!! $option[$i]->option !!}</span>
										@endif
									</label>
									@endfor
									@endif
								</div>
							</div>
						</fieldset>
					</section>
					@endforeach
					<div class="text-center" style="margin-top: 2em;">
						<input type="hidden" id="next_page" value="{{ $question->nextPageUrl() }}">
						<input type="hidden" id="previous_page" value="{{ $question->previousPageUrl() }}">
						@if (null !== ($question->previousPageUrl()) )
						<button class="btn btn-primary btn-save-encuesta" data-info="Previous">Anterior</button>
						@endif
						@for ($i=1; $i <= $question->total(); $i++)
						<a href="{{ $question->url($i) }}"><button class="btn btn-primary">{{ $i }}</button></a>
						@endfor
						@if (null !== $question->nextPageUrl())
						<button class="btn btn-primary btn-save-encuesta" data-info="Next">Siguiente</button>
						@elseif ($question->currentPage() == ($question->lastPage()))
						<button class="btn btn-primary btn-save-encuesta" data-info="End">Finalizar</button>
						@endif
					</div>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ asset('/js/jquery-ui-1.10.4.custom.min.js') }}"></script>
<script type="text/javascript">
	function supportsDateInput(){
		var i = document.createElement("input");
		i.setAttribute("type", "date");
		return i.type !== "text";
	}
	var datepickerlang = {
		lang:"es",
		closeText:"Cerrar",
		prevText:"Anterior",
		nextText:"Siguiente",
		currentText:"Hoy",
		monthNames:["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
		monthNamesShort:["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
		dayNames:["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
		dayNamesShort:["Do","Lun","Mar","Mie","Jue","Vie","Sab"],
		dayNamesMin:["Do","Lun","Mar","Mie","Jue","Vie","Sab"],
		dateFormat:supportsDateInput()?'yy-mm-dd':"dd\/mm\/yy",
		firstDay:1,isRTL:false
	};
</script>

<script type="text/javascript" src="{{ asset('/js/main.js')}}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
	var lang = {
		"code":"es",
		"incomplete" :"Esta pregunta no ha sido respondida completamente. Por favor, revísala y completa tu respuesta. "
	}

</script>
@endsection                        