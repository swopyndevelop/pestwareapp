@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h2 class="box-title" style="font-size: 2em; margin: .5em">{{ $course->title }}</h2>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-2 col-md-offset-10">	
							<a href="{{ route('evaluation_course_create', ['$course' => $course->id, '$question' => 0]) }}" class="btn btn-success" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span> Pregunta</a>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Pregunta</th>
								</tr>
							</thead>
							<tbody>
								@isset($questions)
								@foreach ($questions as $q)
								<tr>
									<td width="5%">{{ $loop->index + 1 }}</td>
									<td width="80%">{{ $q->question }}</td>
									<td width="15%">
										<a href=" {{ route('evaluation_course_create', ['$course' => $course->id, '$question' => $q->id]) }}" class="btn btn-success" style="background-color: #1bb49a; border-color: #1bb49a;"><span class="glyphicon glyphicon-pencil"></span></a>
										<a href=" {{ route('evaluation_question_delete', ['$question' => $q->id]) }}" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
									</td>
								</tr>
								@endforeach
								@endisset
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-md-2">	
							<a href="{{ route('training_index') }}" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span> Regresar</a>
						</div>
					</div>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>
@endsection