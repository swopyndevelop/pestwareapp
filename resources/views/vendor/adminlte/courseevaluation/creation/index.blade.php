@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h2 class="box-title" style="font-size: 2em; margin: .5em">Creación de Evaluaciones</h2>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="inputPregunta" class="col-sm-2 control-label">Curso: </label>
							<div class="col-sm-7">
								<input type="text" class="form-control" placeholder="Pregunta" value="{{ $course->title }}" disabled="true">
								<input type="hidden" value="{{$course->id}}" id="courseId">
							</div>
						</div>
						<div class="form-group">
							<label for="inputPregunta" class="col-sm-2 control-label">Pregunta:</label>
							<div class="col-sm-7">
								<input type="hidden" value="@if (isset($question)) {{$question->id}} @else @endif" id="questionId">
								<input type="text" class="form-control" id="inputAsk" placeholder="Pregunta" value=" @if (isset($question->question)) {{ $question->question }} @endif">
							</div>
							<div class="col-sm-2">
								<select name="typeQuestion" id="typeQuestion" class="form-control">
									<option value="only">Única</option>
									<option value="multioption">Multiopción</option>
									<option value="order">Ordenamiento</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-2">
								<h5 class="form-control-static"> A continuación agregue las respuestas correspondientes. Seleccione y/o ordene las correctas.</h5>
							</div>
						</div>
						<div class="col-md-8 col-md-offset-2">
							<table class="table">
								<thead>
									<tr>
										<th colspan="3">
											Respuesta
										</th>
									</tr>
									<tr>
										<th style="height: 1.5em; vertical-align: middle; text-align: center" width="1em">
											<input type="checkbox" id="inputCorrect">
										</th>
										<th style="height: 1.5em; vertical-align: middle; text-align: center">
											<input type="text" class="form-control" style="margin-left:auto; margin-right:auto;" id="optionCreate">
										</th>
										<th style="height: 1.5em; vertical-align: middle; text-align: center" width="1em">
											<button type="button" class="btn btn-success" style="background-color: #1bb49a; border-color: #1bb49a;" id="add_answer"><i class="fa fa-plus" aria-hidden="true"></i></button>
										</th>
									</tr>
								</thead>
								<tbody id="tableOptionsCreate">
									@if (!empty($options))
									@foreach ($options as $o)
									<tr>
										<td>
											<span>{{ $o->validation }}</span>
										</td>
										<td>
											<span>{{ $o->option }}</span>
										</td>
										<td hidden="true">
											<span>{{ $o->id }}</span>
										</td>
										<td><button type='button' class='btn btn-danger removeOption'><i class='fa fa-minus' aria-hidden='true'></i></button></td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
						<div class="col-md-8 col-md-offset-2 text-center">
							<div class="col-md-6">
								<a href="{{ route('kardex_question_evaluation', [ '$course' => $course->id ]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span> Regresar</a>
							</div>
							<div class="col-md-6">
								<button type="button" class="btn btn-success" style="background-color: black; border-color: black;" id="saveQuestion" data-info="@if (isset($question)) {{ route('evaluation_question_edit_it') }} @else {{ route('evaluation_question_save') }} @endif "><span class="glyphicon glyphicon-ok" aria-hidden="true" ></span> Guardar</button>
							</div>
						</div>
					</form>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>
@endsection