@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h2 class="box-title" style="font-size: 2em; margin: .5em">Resultados de Evaluación</h2> <br>
					<h3>
						Aspirante: {{ auth()->user()->name }} <br>
						Puesto: {{ auth()->user()->job_title_name }} 
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="col-md-8">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							@for ($i = 0; $i < count($question); $i++)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h2 class="panel-title text-left" style="font-size: 1.5em">
										Pregunta {{ $i+1 }}.  
										<a data-toggle="collapse" data-parent="#accordion" href="#{{ $i+1 }}" aria-expanded="true" aria-controls="{{ $i+1 }}">
											<i class="glyphicon glyphicon-chevron-down pull-right"></i>
										</a>
									</h2>
								</div>
								<div id="{{ $i+1 }}" class="panel-collapse collapse">
									<div class="form-horizontal panel-body">
										<div class="text-center">
											<span style="font-size: 1.5em;">{{ $question[$i]->question }}</span>
										</div>
										<div class="well col-md-12" style="font-size: 1.2em; margin-top: 2em;">
											<span class="text-muted">
												
												@if($question[$i]->type == 'order')
												<ul class="drag ui-sortable" data-drag="ot18158746">
													@for ($j = 0; $j < count($option[$i]); $j++)
													<li data-id="{{ $option[$i][$j]->id }}">
														<input type="hidden" name="options" value="{{ $option[$i][$j]->id }}" class="ord answer">
														<span>
															<strong class="ord">{{ $j+1 }}. </strong>{!! $option[$i][$j]->option !!}
														</span>
													</li>
													@endfor

												</ul>
												@else
												@for ($j = 0; $j < count($option[$i]); $j++)
												<label class="input-group input-group-radio row">
													@if($question[$i]->type == 'only')
													<input type="radio" class="hidden-inputs answer" name="answer" value="checked" />
													<span class=""></span>
													<span class="" style="color: black">{!! $option[$i][$j]->option !!}</span>
													@elseif($question[$i]->type == 'multioption')
													<input type="checkbox" class="hidden-inputs answer" name="answer" value="{{ $option[$i][$j]->id }}"/>
													<span class=""></span>
													<span class="" style="color: black">{!! $option[$i][$j]->option !!}</span>
													@endif
												</label>
												@endfor
												@endif
											</span>
										</div>
									</div>
								</div>
							</div>
							@endfor
						</div>
					</div>
					<div class="col-md-4">
						<ul class="list-group">
							<li class="list-group-item lead text-center" style="background: #f5216e; color: white;">Resultados</li>
							<li class="list-group-item">
								<span style="font-size: 1.5em">Respuestas Correctas:</span>
								<span class="text-muted" style="font-size: 1.5em">0/{{ count($question) }}</span>
							</li>
							<li class="list-group-item">
								<span style="font-size: 1.5em">Puntos Obtenidos:</span>
								<span class="text-muted" style="font-size: 1.5em">0</span>
							</li>
							<li class="list-group-item">
								<span style="font-size: 1.5em">Puntaje Total:</span>
								<span class="text-muted" style="font-size: 1.5em">0</span>
							</li>
						</ul>  
						<div class="text-center">
							<button class="btn btn-primary" id="closeResults" style="background: black; border-color: black; width: 10em;">CERRAR</button>
						</div>
					</div>
					<!--box-body-->
				</div>
				<!--.box-->
			</div>
		</div>
		@endsection