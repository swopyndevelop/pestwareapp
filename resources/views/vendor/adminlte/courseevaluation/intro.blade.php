@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h3 class="box-title" style="font-size: 2em; margin: .5em">Evaluación Encargado de Tienda</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="text-center">
						<p style="font-size: 2em;">@if($pass > 0)La siguiente evaluación nos ayudará a saber si el curso que acabas de tomar te sirvió para poder ofrecer al cliente una mejor atención. <br> Tendrás 30 minutos para terminar la siguiente evaluación, cuando estés listo presiona el siguiente botón para iniciar con tu evaluación.@else Actualmente el curso en el que te encuentras no tiene registrada alguna prueba. @endif </p>
						

						<form action="{{ route('course_view', ['course' => $course]) }}" method="GET">
							{{ csrf_field() }}
							<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
							<div align="center" style="margin-top: 3em">
								<button type="submit" class="btn-lg btn btn-primary" id="StartEvaluation" style="background-color: black; border-color: black;">Iniciar evaluación</button>
							</div>
						</form>	
					</div>
				</div>
			</div>
			<!--box-body-->
		</div>
		<!--.box-->
	</div>
</div>

@endsection