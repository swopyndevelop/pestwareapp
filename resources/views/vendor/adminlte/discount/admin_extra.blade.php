@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@section('personal_style')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Extras</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<form method="POST" enctype="multipart/form-data" action="{{ Route('add_extra') }}" role="form">
									{{ csrf_field() }}
									<div class="pull-right">
										<a href="javascript:void(0)" data-perform="panel-collapse">
											<i class="ti-minus"></i></a>
											<a href="javascript:void(0)" data-perform="panel-dismiss">
												<i class="ti-close"></i></a>
											</div>
											<div class="panel-body">

												<h3 style="text-align: center;" ><strong>Creación de Extras</strong></h3>
												<br>

												<div class="row">
													<div class="col-md-4">
														<label for="Nombre" style="font-size: 1.5em">Nombre</label>
														<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="" required  style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px">
													</div>

													<div class="col-md-4">
														<label for="Descripcion" style="font-size: 1.5em">Descripción</label>
														<textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Descripcion" name="Descripcion" placeholder="" required></textarea>													
													</div>

													<div class="col-md-4">
														<label for="Cantidad" style="font-size: 1.5em">Monto</label>
														<input type="text" class="form-control" id="Cantidad" name="Cantidad" placeholder="" min="0" required style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px">
													</div>
													
												</div>
												<br><br>
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
												</div>

												</form>

												<div class="row">
													<div class="col-md-12">
														<h3 class="titleCenter" style="text-align: center;">Extras</h3>
														<div class="table-responsive">
															<table class="table table-hover table-striped sortable" name= "tableEmployees" id="tableEmployees">
																<thead style="background: black; color: white;">
																	<tr>
																		<th class="text-center sorttable_nosort">Nombre</th>
																		<th class="text-center sorttable_nosort">Descripción</th>
																		<th class="text-center">Monto</th>
																		<th colspan="3" class="text-center sorttable_nosort">Opciones</th>
																	</tr>
																</thead>

																<tbody>
																@foreach($extras as $extra)
																	<tr>
																		<td class="text-center">{{ $extra->name }}</td>
																		<td class="text-center">{{ $extra->description }}</td>
																		<td class="text-center">{{ $extra->amount }}</td>
																		<td>
																			<a href="{{ route('edit_extra', $extra->id) }}" class="btn">
																				<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
																			</a>
																		</td>
																		<td>

																			<form action="{{ route('delete_extra', $extra->id) }}" method="POST">
																				{{ csrf_field() }}
																				{{ method_field('DELETE') }}
																				<button type="submit" class="btn" style="background-color: transparent;">
																					<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
																				</button>
																			</form>
																		</td>
																	</tr>
																@endforeach
																</tbody>

															</table>
															
														</div>
													</div>	
												</div>
											</div>
											<br>
										</div>
									
								</div>
							</div>
							@endsection