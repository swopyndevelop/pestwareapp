@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Catálogo de Descuentos</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
					<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'search_discount','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<div class="col-md-4 col-md-offset-8">
						<a href="{{ route('create_discount') }}" class="btn btn-success" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span>Crear Descuento</a>
					</div>
					</div>

					<h3 class="titleCenter" style="text-align: center;">Descuentos Creados</h3>
					<div class="table-responsive">
						<table class="table table-hover table-striped sortable" name= "tableEmployees" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center sorttable_nosort">Título</th>
								<th class="text-center sorttable_nosort">Descripción</th>
								<th class="text-center">% a descontar</th>
								<th colspan="3" class="text-center sorttable_nosort">Opciones</th>
							</tr>
						</thead>
						@foreach($discount as $discounts)
						<tbody>
							
								<tr>
									<td class="text-center">{{$discounts->title}}</td>
									<td class="text-center">{{$discounts->description}}</td>
									<td class="text-center">{{$discounts->percentage}}</td>
									<td>
										<a href="{{ route('edit_discount', $discounts->id) }}" class="btn">
											<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
										</a>
									</td>
									<td>
										<form action="{{ route('delete_discount', $discounts->id) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn" style="background-color: transparent;">
												<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
											</button>
										</form>
									</td>
								</tr>
							</tbody>
							@endforeach
						</table>
						<center>{{$discount->links()}}</center>
					</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>