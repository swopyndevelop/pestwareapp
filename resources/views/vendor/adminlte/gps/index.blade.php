@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Seguimiento de Unidades (GPS)</h3>

                        <ol class="breadcrumb" style="margin-top: 5px;">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Mis Facturas</li>
                        </ol>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="col-md-6 margin-mobile">
                        <select name="technician" id="technician" class="form-control">
                            @foreach($technicians as $technician)
                                @if($technician->gps != null)
                                    <option value="{{ $technician->id }}">{{ $technician->name }} (GPS: {{$technician->gps}})</option>
                                @else
                                    <option value="{{ $technician->id }}" disabled>{{ $technician->name }} (SIN GPS)</option>
                                @endif
                            @endforeach
                        </select>
                        <br>
                    </div>

                    <div class="col-md-3 margin-mobile">
                        <input class="text-input" type="text" name="filterDateEvent" id="filterDateEvent" value="" style="width: 100%">
                    </div>

                    <div class="col-md-3 margin-mobile">
                        <button type="button" class="btn btn-primary-dark" id="btnRoute">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            Filtrar Ruta
                        </button>
                        <br>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="container-fluid col-md-12">
                               <div class="col-md-12">
                                   <div id="map-gps" style="height: 600px; width: 100%;"></div>
                               </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTay1aJA56eD1BRc4m87_2YSSDvPtEIKg"
            type="text/javascript">
    </script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script type="text/javascript" src="{{ URL::asset('js/build/gps.js') }}"></script>
@endsection