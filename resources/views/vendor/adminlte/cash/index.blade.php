@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen content" id="courseevaluation-CompanyEvaluation">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Historial de Pagos</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    
                                </div>
                                <div class="col-lg-6 margin-mobile">
                                    {!!Form::open(['method' => 'GET','route'=>'search_cashes'])!!}
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                        </span>
                                    </div>
                                    {!!Form::close()!!}
                                </div>
                            </div>
                            <br><br>
                            <!-- Table Data  -->
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table tablesorter table-hover text-center" id="tableProduct">
                                        <thead class="table-general">
                                            <tr>
                                                <th>#</th>
                                                <th>Orden de Servicio</th>
                                                <th>Cliente</th>
                                                <th>Fecha</th>
                                                <th>Método/Forma</th>
                                                <th>Monto</th>
                                                <th>Comentarios</th>
                                                <th>Estatus</th>
                                                <th>Anexos</th>
                                            </tr>

                                            <tr>
                                                <th scope="col">
                                                    <button id="btnFilterSpend" name="btnFilterSpend" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-search"></i> Filtrar</button>
                                                </th>
                                                <th scope="col">
                                                    <select id="selectFilterServiceOrderSpend" class="applicantsList-single form-control-pwa">
                                                        <option value="0" selected>Todos</option>
                                                        @foreach($cash as $c)
                                                            <option value="{{ $c->cobro }}">
                                                                    {{ $c->servicio }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </th>
                                                <th scope="col">
                                                    <select id="selectFilterCustomerNameSpend" class="applicantsList-single form-control-pwa">
                                                    <option value="0" selected>Todos</option>
                                                            @foreach($customers as $customer)
                                                                <option value="{{ $customer->id }}">
                                                                    {{ $customer->name }}
                                                                </option>
                                                            @endforeach
                                                    </select>
                                                </th>
                                                <th scope="col">
                                                    <input id="filterDateSpend" name="filterDateSpend" class="text-input" type="text">
                                                </th>
                                                <th scope="col">
                                                    <select id="filterPaySpend" name="filterPaySpend" class="applicantsList-single form-control-pwa">
                                                        <option value="0" selected>Todos</option>
                                                        <option value="1">Efectivo</option>
                                                        <option value="2">Tarjeta de Créditi</option>
                                                        <option value="3">Tarjeta de Débito</option>
                                                        <option value="4">Transferencia</option>
                                                        <option value="5">Contado</option>
                                                        <option value="6">Crédito</option>
                                                    </select>
                                                </th>
                                                <th scope="col">
                                                <input name="filterAmountSpend" id="filterAmountSpend" class="text-input" type="number" >
                                                </th>
                                                <th scope="col">
                                                    <input name="filterDescriptionSpend" id="filterDescriptionSpend" type="text" class="applicantsList-single form-control-pwa" >
                                                </th>
                                                <th scope="col">
                                                    <select name="filterStatusSpend" id="filterStatusSpend" class="applicantsList-single form-control-pwa">
                                                        <option value="0" selected>Todos</option>
                                                        <option value="4">Programado</option>
                                                        <option value="10">Sin Programar</option>
                                                        <option value="1">Seguimiento</option>
                                                        <option value="3">Rechazado</option>
                                                    </select>
                                                </th>
                                                <th scope="col">
                                                </th>
                                            </tr>
                                          </thead>
                                        <tbody>
                                            @foreach($cash as $c)
                                            <tr>
                                                <td>
                                                    CC-{{$c->cobro}}
                                                </td>
                                                <td style="font-weight:bold" class="text-center">{{$c->servicio}}</td>
                                                <td class="text-center">{{$c->cliente}}</td>
                                                <td class="text-center"><?php echo date("d/m/y",strtotime($c->final_date))?> | <?php echo date("H:i",strtotime($c->initial_hour))?></td>
                                                <td class="text-center">{{$c->metodo}}/<br>{{$c->forma}}</td>
                                                @if($c->amount_received != null)
                                                    <td class="text-center">${{$c->amount_received}}</td>
                                                @else
                                                    <td class="text-center">$0</td>
                                                @endif
                                                <td class="text-center" style="width:15%">{{$c->commentary}}</td>
                                                <td>@if($c->payment == 1)
                                                        <span style="color: #b71c1c;font-weight:bold">Adeudo</span>
                                                    @else
                                                        <span style="color: #00a157;font-weight:bold">Pagado</span>
                                                    @endif
                                                </td>  
                                                <td class="text-center">
                                                    @foreach ($info as $i)
                                                        @if ($i->cash_id == $c->cobro)
                                                            @if($i->route == null)
                                                                <!--<a href="{{$i->file_route}}" target="_blank">
                                                                    <i class="fa fa-image" aria-hidden="true" style=" font-size: 3.5em;color:#oeb0e3"></i>
                                                                </a>-->
                                                                <span></span>
                                                            @else
                                                                <a href="{{Route('download_image',$i->id)}}" target="_blank">
                                                                    <i class="fa fa-image" aria-hidden="true" style=" font-size: 3.5em;color:#oeb0e3"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$cash->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('vendor.adminlte.cash._cash')
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/payment.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
        $(".applicantsList-single").select2();
        $(".applicantsList-single2").select2();
        $(".applicantsList-single3").select2();
        $(".applicantsList-single").change(function(event) {
            $('#Applicant_id').val($(this).val());
        });
        $(".jobTitleList-single").select2();
        $(".jobCenterList-single").select2();
    </script>
@stop