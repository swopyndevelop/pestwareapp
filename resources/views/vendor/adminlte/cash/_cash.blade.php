<!--START REGISTER PRODUCTS-->
<div class="container-fluid spark-screen">
        <div class="modal fade" id="newCs" role="dialog" aria-labelledby="newCashModal">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-context">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>                                    
                                </div>
                                <br>
                                <form action="{{ Route('cash_add') }}" method="POST" id="form" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Cobrar Servicio</h4>
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="name">Servicio: </label>
                                        <div class="col-sm-9">
                                            <select name="service" id="service" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;" required>
                                                @foreach($order as $o)
                                                    <option value="{{ $o->event }}">
                                                        {{ $o->id_service_order }} -- {{$o->cliente}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="company">Método de Pago:</label>
                                            <div class="col-sm-9"> 
                                                <select name="metodo" id="metodo" class="applicantsList-single2 form-control" style="width: 100%; font-weight: bold;" required>
                                                    @foreach($metodo as $m)
                                                        <option value="{{ $m->id }}">
                                                            {{ $m->name }}
                                                        </option>
                                                    @endforeach
                                                </select>                                            
                                            </div>
                                    </div>
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="company">Forma de Pago:</label>
                                        <div class="col-sm-9"> 
                                            <select name="forma" id="forma" class="applicantsList-single3 form-control" style="width: 100%; font-weight: bold;" required>
                                                @foreach($forma as $f)
                                                    <option value="{{ $f->id }}">
                                                        {{ $f->name }}
                                                    </option>
                                                @endforeach
                                            </select>                                            
                                        </div>
                                    </div>
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="company">Monto Recibido: </label>
                                        <div class="col-sm-9"> 
                                            <input type="number" class="form-control" id="monto" name="monto" required>
                                        </div>
                                    </div>
                                   <!-- <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="company">Estatus: </label>
                                        <div class="col-sm-9"> 
                                            <input type="checkbox" id="pagado" name="pagado"> Pagado &nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="npagado" name="npagado"> Aduedo
                                        </div>
                                    </div>-->
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="company">Comentarios: </label>
                                        <div class="col-sm-9"> 
                                            <textarea class="form-control" id="commentary" name="commentary"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="company">Evidencia: </label>
                                        <div class="col-sm-9"> 
                                                <input type="file" name="information[]" id="information[]" multiple="">
                                            </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary btn-sm" name="SaveCash" id="SaveCash">Guardar</button>
                                </form>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>