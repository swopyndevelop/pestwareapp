@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')


<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Edición de Empleados</h4>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<form action="{{ route('update_employees',['id' => $employee->id]) }}" class="form-horizontal" method="POST">
								{{ csrf_field() }}
								<div class="form-group row">
									<label class="control-label col-sm-3" for="name">Nombre: </label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="nameEmployee" name="nameEmployee" placeholder="Juan Esparza" value="@if(isset($employee)) {{$employee->name}} @else @endif">
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-sm-3" for="company">Empresa: </label>
									<div class="col-sm-9"> 
										<input type="text" class="form-control" id="inputCompany" @if(isset($employee)) value="{{ $employee->company }}" disabled="true" @endif>
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-sm-3" for="jobTitle">Puesto: </label>
									<div class="col-sm-9"> 
										<select name="inputjobTitle" id="inputjobTitle" class="form-control">
											@foreach($jobTitle as $jt)
											<option @if(isset($employee) && $employee->jobTitle == $jt->id) selected @endif value="{{ $jt->id }}">{{ $jt->job_title_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-sm-3" for="jobTitle">Centro de Trabajo: </label>
									<div class="col-sm-9"> 
										<select name="inputJobCenter" id="inputJobCenter" class="form-control">
											@foreach($jobCenter as $jc)
											<option @if(isset($employee) && $employee->jobCenter == $jc->id) selected @endif value="{{ $jc->id }}">{{ $jc->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group text-center">
									<button type="submit" class="btn btn-success btn-lg">Guardar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>
@endsection