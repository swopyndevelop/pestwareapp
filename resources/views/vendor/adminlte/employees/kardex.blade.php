@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')


    <div class="content container-fluid spark-screen" id="kardex_Employee">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box-->
                <div class="box">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title" style="font-size: 2em; margin: .5em">Empleados</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="col-md-2" style="margin-bottom: 1em">
                            @if($isCreated == true)
                                <a href="#newEmployees" id="newEmployee" class="btn btn-primary-dark" data-toggle="modal"><i class="fa fa-user-plus" aria-hidden="true"></i> Crear Usuario</a>
                            @else
                                <a class="btn btn-primary-dark" disabled data-toggle="tooltip" data-placement="bottom" title="Plan Empresarial"><i class="fa fa-user-plus" aria-hidden="true"></i> Crear Usuario</a>
                            @endif
                        </div>
                        <div class="col-md-5">
                            <select name="selectEmployeeProfile" id="selectEmployeeProfile" class="form-control" style="font-weight: bold;">
                                @foreach($jobCenters as $jobCenterSelect)
                                    @if($jobCenterSession->id_profile_job_center == $jobCenterSelect->id)
                                        <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                            {{ $jobCenterSelect->name }}
                                        </option>
                                    @else
                                        <option value="{{ $jobCenterSelect->id }}">
                                            {{ $jobCenterSelect->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
                            {!!Form::open(['method' => 'GET','route'=>'kardex_employees'])!!}
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar por: (Empleado)" name="search" id="search" value="">
                                <span class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                            </div>
                            {!!Form::close()!!}
                        </div>

                        <div class="row">
                            <div class="table-responsive col-xs-12">
                                <table class="table table-secondary" id="tableEmployees">
                                    <thead class="table-general">
                                    <tr>
                                        <th class="text-center">Empleado</th>
                                        <th class="text-center">Puesto</th>
                                        <th class="text-center">Zona</th>
                                        <th class="text-center">Centro de trabajo</th>
                                        <th class="text-center">Color</th>
                                        <th class="text-center" colspan="2">Fecha de ingreso</th>
                                        <th class="text-center">Acceso</th>
                                        <th class="text-center">Resetear</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($employee as $e)
                                        <tr>
                                            <td class="text-center">{{ $e->name }}
                                                <p class="text-default">
                                                    <strong>
                                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                    </strong>
                                                    <a href="mailto:{{ $e->email }}">{{ $e->email }} </a>
                                                </p>
                                            </td>
                                        <!-- <td class="text-center" style="font-weight: bold">{{ $e->company }}</td> -->
                                            <td class="text-center">{{ $e-> jobTitleName}}</td>
                                            <td class="text-center">
                                                @foreach($zones as $z)
                                                    @if($z->id == $e->Zone)
                                                        {{ $z->text }}
                                                    @endif
                                                @endforeach
                                            </td>

                                            <td class="text-center">{{ $e-> jobCenter }}</td>
                                            <td class="text-center">
                                                <input type="color" disabled="disabled" value="{{ $e->color }}" style="border: none;">
                                            </td>
                                            <td class="text-center" class="text-center">
                                                @php
                                                    echo date("d-m-Y", strtotime($e->created_at));
                                                @endphp
                                            </td>
                                            <td>
                                            <!-- <a href="{{ route('index_employees', ['id' => $e->id]) }}" class="btn"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> -->
                                                <a href="#updateEmployees" class="btnUpdateEmployee" data-toggle="modal" data-name="{{ $e->name }}" data-id="{{ $e->employee_id }}">
                                                    <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                @if($e->status == 1)
                                                    <a href="#" data-toggle="modal" data-target="#status<?php echo $e->id; ?>" title="Permitido"><i class="fa fa-unlock fa-2x" aria-hidden="true"></i></a>
                                                @else
                                                    <a href="#" data-toggle="modal" data-target="#status<?php echo $e->id; ?>" title="Bloqueado"><i class="fa fa-lock fa-2x" aria-hidden="true"></i></a></li>
                                                @endif
                                                @include('vendor.adminlte.employees._modalAccessEmployee')
                                            </td>
                                            <td class="text-center">
                                                <a class="openModalChangePassword" data-toggle="modal" data-target="#changePassword" style="cursor: pointer" data-id="{{ $e->id }}">
                                                    <i class="fa fa-repeat fa-2x" aria-hidden="true"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="Resetear contraseña">
                                                    </i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{$employee->links()}}
                        </div>
                    </div>
                </div>
                <!--box-body-->
            </div>
            <!--.box-->
            </div>
        </div>
    </div>

    @include('vendor.adminlte.employees._modalNewEmployee')
    @include('vendor.adminlte.employees._modalUpdateEmployee')
    @include('vendor.adminlte.employees._modalChangePassword')

@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/employee.js') }}"></script>
    <style>

        /*	table.tablesorter thead tr .header {
                background-image: url(../img/bg.gif);
                background-repeat: no-repeat;
                background-position: center right;
                cursor: pointer;
            }
            table.tablesorter thead tr .headerSortUp {
                background-image: url(../img/asc.gif);
            }
            table.tablesorter thead tr .headerSortDown {
                background-image: url(../img/desc.gif);
            }*/
    </style>
    <!-- <link rel="stylesheet" href="{{ asset('css/style_sorterTable.css') }}"> -->
    <script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
    <script>
        // call the tablesorter plugin
        $("#tableEmployees").tablesorter( {sortList: [[0,0], [1,0]]} );
    </script>
    <script>
        $(".applicantsList-single").select2();
        $(".applicantsList-single").change(function(event) {
            $('#Applicant_id').val($(this).val());
        });
        $(".jobTitleList-single").select2();
        $(".jobCenterList-single").select2();
    </script>
    <script>
        //Job Center Select
        $('#selectEmployeeProfile').on('change', function() {
            let idEmployeeProfile = $(this).val();
            window.location.href = route('kardex_employees', idEmployeeProfile);
        });
    </script>
@stop
