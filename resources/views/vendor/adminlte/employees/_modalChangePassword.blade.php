<!-- INICIA MODAL PARA CAMBIAR CONTRASEÑA -->
<div class="modal fade" id="changePassword" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body text-center">
                <h4>¿Está seguro que desea resetear la contraseña?</h4>
                <h5>La contraseña por defecto es: PestWareApp2021@</h5>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-primary" type="button" id="sendChangePasswordSave"
                    name="sendChangePasswordSave">Confirmar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA CAMBIAR CONTRASEÑA -->