<!-- INICIA MODAL PARA ACCESO -->
<div class="modal fade" id="status<?php echo $e->id; ?>" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<form action="{{ route('update_status', ['employee_status' => $e->id]) }}" method="POST" id="form" class="text-center">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<div class="modal-body">
					<h5 class="modal-title">¿Está seguro de que desea modificar el acceso?</h5>
					<div class="col-md-12">
						<select name="StatusEdit<?php echo $e->id; ?>" class="form-control" id="StatusEdit<?php echo $e->id; ?>" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.1em; border-radius: 7px; visibility:hidden">
							@if($e->status==1)
								<option value="1">Vigente</option>
								<option value="0" selected="true">No vigente</option>
							@else
								<option value="1" selected="true">Vigente</option>
								<option value="0">No vigente</option>
							@endif
						</select>
					</div>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<div class="text-center">
						<button type="submit" title="Aceptar" class="btn btn-primary" data-toggle="modal" data-target="#status<?php echo $e->id; ?>">
							Aceptar
						</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA ACCESO -->