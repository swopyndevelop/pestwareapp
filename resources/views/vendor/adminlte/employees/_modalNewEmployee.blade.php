<!-- START NEW EMPLOYEE MODAL -->
<div class="container-fluid spark-screen">
	<div class="modal fade" id="newEmployees" role="dialog" aria-labelledby="newEmployeesModal">
		<div class="row">
			<div class="col-md-12">
				<!--Default box-->
				<div class="modal-dialog modal-lg" role="document">

					<div class="modal-content">
						<div class="box">
							<div class="modal-header">
								<div class="box-header">
									<button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
							<div class="box-body">
								<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle"></h4>

								<div class="form-group row col-lg-push-1 col-lg-11">
									<input type="hidden" id="employeeIdNew" value="0">
									<label class="control-label col-sm-3" for="company">Empresa: </label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="newCompany" @if(isset($companie)) value="{{ $companie->name }}" disabled="true" @endif>
									</div>
								</div>

								<div class="form-group row col-lg-push-1 col-lg-11 formulario__grupo" id="grupo__nameEmployee">
									<label class="control-label col-sm-3" for="name">Nombre Empleado: </label>
									<div class="col-sm-9">
										<input type="text" class="form-control formulario__input" id="nameEmployee" name="nameEmployee">
										<i class="formulario__validacion-estado-label fa fa-times-circle"></i>
									</div>
									<p class="formulario__input-error text-center">Este campo sólo admite letras, espacios y acentos de 3 a 250 caracteres.</p>
								</div>

								<div class="form-group row col-lg-push-1 col-lg-11 formulario__grupo" id="grupo__email">
									<label class="control-label col-sm-3" for="email">Correo electrónico: </label>
									<div class="col-sm-9">
										<input type="email" class="form-control formulario__input" id="email" name="email">
										<i class="formulario__validacion-estado-label fa fa-times-circle"></i>
									</div>
									<p class="formulario__input-error text-center">El correo electrónico tiene que ser válido por ejemplo: email@gmail.com</p>
								</div>

								<div class="form-group row col-lg-push-1 col-lg-11 formulario__grupo" id="grupo__inputjobTitle">
									<label class="control-label col-sm-3" for="jobTitle">Puesto: </label>
									<div class="col-sm-9">
										<select name="inputjobTitle" id="inputjobTitle" class="form-control select__input" style="width: 100%; font-weight: bold;">
											<option value="" disabled selected>Selecciona puesto</option>
											@foreach($jobTitle as $jt)
												<option value="{{ $jt->id }}">{{ $jt->job_title_name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group row col-lg-push-1 col-lg-11 formulario__grupo" id="grupo__inputJobCenter">
									<label class="control-label col-sm-3" for="jobTitle">Centro de Trabajo: </label>
									<div class="col-sm-9">
										<select name="inputJobCenter" id="inputJobCenter" class="form-control select__input" style="width: 100%; font-weight: bold;">
											<option value="" disabled selected>Selecciona centro de trabajo</option>
											@if($jobcenter != 0)
												@foreach($jobCentersUnique as $jobCentersU)
													<option value="{{ $jobCentersU->id }}">{{ $jobCentersU->name }}</option>
												@endforeach
											@else
												@foreach($jobCenters as $jc)
													<option value="{{ $jc->id }}">{{ $jc->name }}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>

								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="color">Color para Calendario: </label>
									<div class="col-sm-9">
										<input type="color" id="color" name="color" style="width: 100%;">
									</div>
								</div>
							</div>

							<div class="modal-footer">

								<div class="formulario__mensaje" id="formulario__mensaje">
									<p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
								</div>
								<br>
								<div class="text-center formulario__grupo formulario__grupo-btn-enviar">
									<button class="btn btn-primary btn-lg" type="button" id="EmployeeNew">Guardar</button>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END NEW EMPLOYEE MODAL -->