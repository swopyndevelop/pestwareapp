@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registro de Asistencias</h3>

                        <ol class="breadcrumb" style="margin-top: 5px;">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Mis Asistencias</li>
                        </ol>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="col-md-3 margin-mobile">
                        <select name="technician" id="technician" class="form-control">
                            <option value="0">Todos</option>
                            @foreach($employees as $technician)
                                <option value="{{ $technician->id }}">{{ $technician->name }}</option>
                            @endforeach
                        </select>
                        <br>
                    </div>

                    <div class="col-md-3 margin-mobile">
                        <input class="text-input" type="text" name="filterDateEvent" id="filterDateEvent" value="" style="width: 100%">
                    </div>

                    <div class="col-md-3 margin-mobile">
                        <button type="button" class="btn btn-primary-dark" id="btnRoute">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            Filtrar
                        </button>
                        <br>
                    </div>

                    <div class="col-md-3 margin-mobile">
                        <button type="button" class="btn btn-secondary" id="btnGetCode" style="display: none">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Generar Código
                        </button>
                        <br>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <br><br>
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># ID</th>
                                        <th>Empleado</th>
                                        <th>Fecha</th>
                                        <th>Hora Entrada</th>
                                        <th>Hora Salida</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($attendances as $attendance)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                        AS-{{ $attendance->id }} <span class="caret"></span>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>{{ $attendance->employee_name }}</td>
                                            <td>{{ $attendance->date }}</td>
                                            <td>{{ $attendance->start_hour }}</td>
                                            <td>{{ $attendance->end_hour }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $attendances->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script type="text/javascript" src="{{ URL::asset('js/build/attendance.js') }}"></script>
@endsection