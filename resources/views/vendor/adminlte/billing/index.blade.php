@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Facturación</h3>

                        <ol class="breadcrumb" style="margin-top: 5px;">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Mi Suscripción</li>
                        </ol>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                                        <span class="text-bold">Detalle de Suscripción</span>
                                    </div>
                                    <div class="panel-body">
                                        @if($companyPayment == null)
                                        <div class="col-md-6">
                                            <form action="" id="formulario-tarjeta" class="formulario-tarjeta">
                                                <div style="background-color: #d32f2f; color: white;" width="100%"><span class="card-errors"></span></div>
                                                <div class="form-container">
                                                    <div class="field-container">
                                                        <label for="inputNombre">Nombre del titular</label>
                                                        <input id="inputNombre" name="card_name" maxlength="20" type="text" autocomplete="off" data-conekta="card[name]" class="conekta form-control">
                                                    </div>
                                                    <div class="field-container">
                                                        <label for="inputNumero">Número de tarjeta</label>
                                                        <input id="inputNumero" name="card_number" type="text" pattern="[0-9]*" inputmode="numeric" autocomplete="off" data-conekta="card[number]" class="conekta form-control">
                                                        <svg id="ccicon" class="ccicon" width="750" height="250" viewBox="0 0 750 250" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink">
                                                        </svg>
                                                    </div>
                                                    <div class="field-container">
                                                        <label for="selectMes">Fecha de vencimiento</label>
                                                        <select id="selectMes" class="conekta form-control" name="card_month" data-conekta="card[exp_month]">
                                                            <option disabled selected>Mes</option>
                                                            <option value="1">Enero</option>
                                                            <option value="2">Febrero</option>
                                                            <option value="3">Marzo</option>
                                                            <option value="4">Abril</option>
                                                            <option value="5">Mayo</option>
                                                            <option value="6">Junio</option>
                                                            <option value="7">Julio</option>
                                                            <option value="8">Agosto</option>
                                                            <option value="9">Septiembre</option>
                                                            <option value="10">Octubre</option>
                                                            <option value="11">Noviembre</option>
                                                            <option value="12">Diciembre</option>
                                                        </select>
                                                    </div>
                                                    <div class="field-container">
                                                        <label for="selectYear">.</label>
                                                        <select id="selectYear" class="conekta form-control" name="card_year" data-conekta="card[exp_year]">
                                                            <option disabled selected>Año</option>
                                                        </select>
                                                    </div>
                                                    <div class="field-container">
                                                        <label for="inputCCV">Código de Seguridad</label>
                                                        <input id="inputCCV" type="text" pattern="[0-9]*" inputmode="numeric" data-conekta="card[cvc]" class="form-control">
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="form-container">
                                                <div class="field-container">
                                                    <button class="btn btn-primary-dark btn-block" id="saveCardPayment">Realizar Pago de Suscripción</button>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-md-6">
                                                <span class="text-primary-dark">Plan: <b>{{ $planName }}</b></span><br>
                                                <span class="text-primary-dark">Fecha de corte: El día <b>{{ $createdAt }} </b>de cada mes.</span><br>
                                                <span class="text-primary-dark" style="font-size: 2em;">${{ $planPrice }}</span><span class="text-secondary"> {{ $currency }} </span><span class="label label-default">mensual</span>
                                                @if($subscriptionPaymentDate):
                                                <h5>{{ $subscriptionPaymentDate }}</h5>
                                                @endif
                                                <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h5 id="plan-subscription-main" data-extras="{{ sizeof($planExtras) }}">Actualmente cuenta con el <b>Plan {{ $planName }}</b></h5>
                                                            @foreach($planExtras as $extras)
                                                                <h6>- {{ $extras }} {{ $currency }}</h6>
                                                            @endforeach
                                                        </div>
                                                        <div class="col-md-6 text-right">
                                                            <br><br><br>
                                                            <span class="text-primary" style="font-size: 2em;">${{ $total }}</span><span class="text-secondary"> {{ $currency }} </span><span class="label label-default">mensual</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="col-md-6">
                                            <div class="container preload">
                                                <div class="creditcard">
                                                    <div class="front">
                                                        <div id="ccsingle"></div>
                                                        <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                    <g id="Front">
                        <g id="CardBackground">
                            <g id="Page-1_1_">
                                <g id="amex_1_">
                                    <path id="Rectangle-1_1_" class="lightcolor grey" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                            C0,17.9,17.9,0,40,0z" />
                                </g>
                            </g>
                            <path class="darkcolor greydark" d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z" />
                        </g>
                        <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber" class="st2 st3 st4"><?= $companyPayment ? '**** **** **** '.$companyPayment->card_number : "#### #### #### ####" ?></text>
                        <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname" class="st2 st5 st6"><?= $companyPayment ? $companyPayment->card_name : "" ?></text>
                        <text transform="matrix(1 0 0 1 54.1074 389.8793)" class="st7 st5 st8">Nombre del titular</text>
                        <text transform="matrix(1 0 0 1 479.7754 388.8793)" class="st7 st5 st8">Vencimiento</text>
                        <text transform="matrix(1 0 0 1 65.1054 241.5)" class="st7 st5 st8">Número de tarjeta</text>
                        <g>
                            <text transform="matrix(1 0 0 1 574.4219 433.8095)" id="svgexpire" class="st2 st5 st9"><?= $companyPayment ? $companyPayment->card_month : "MM" ?>/<?= $companyPayment ? $companyPayment->card_year : "AA" ?></text>
                            <text transform="matrix(1 0 0 1 479.3848 417.0097)" class="st2 st10 st11">VALIDA</text>
                            <text transform="matrix(1 0 0 1 479.3848 435.6762)" class="st2 st10 st11">HASTA</text>
                            <polygon class="st2" points="554.5,421 540.4,414.2 540.4,427.9 		" />
                        </g>
                        <g id="cchip">
                            <g>
                                <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                        c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z" />
                            </g>
                            <g>
                                <g>
                                    <rect x="82" y="70" class="st12" width="1.5" height="60" />
                                </g>
                                <g>
                                    <rect x="167.4" y="70" class="st12" width="1.5" height="60" />
                                </g>
                                <g>
                                    <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                            c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                            C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                            c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                            c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z" />
                                </g>
                                <g>
                                    <rect x="82.8" y="82.1" class="st12" width="25.8" height="1.5" />
                                </g>
                                <g>
                                    <rect x="82.8" y="117.9" class="st12" width="26.1" height="1.5" />
                                </g>
                                <g>
                                    <rect x="142.4" y="82.1" class="st12" width="25.8" height="1.5" />
                                </g>
                                <g>
                                    <rect x="142" y="117.9" class="st12" width="26.2" height="1.5" />
                                </g>
                            </g>
                        </g>
                    </g>
                                                            <g id="Back">
                                                            </g>
                </svg>
                                                    </div>
                                                    <div class="back">
                                                        <svg version="1.1" id="cardback" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                    <g id="Front">
                        <line class="st0" x1="35.3" y1="10.4" x2="36.7" y2="11" />
                    </g>
                                                            <g id="Back">
                                                                <g id="Page-1_2_">
                                                                    <g id="amex_2_">
                                                                        <path id="Rectangle-1_2_" class="darkcolor greydark" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                        C0,17.9,17.9,0,40,0z" />
                                                                    </g>
                                                                </g>
                                                                <rect y="61.6" class="st2" width="750" height="78" />
                                                                <g>
                                                                    <path class="st3" d="M701.1,249.1H48.9c-3.3,0-6-2.7-6-6v-52.5c0-3.3,2.7-6,6-6h652.1c3.3,0,6,2.7,6,6v52.5
                    C707.1,246.4,704.4,249.1,701.1,249.1z" />
                                                                    <rect x="42.9" y="198.6" class="st4" width="664.1" height="10.5" />
                                                                    <rect x="42.9" y="224.5" class="st4" width="664.1" height="10.5" />
                                                                    <path class="st5" d="M701.1,184.6H618h-8h-10v64.5h10h8h83.1c3.3,0,6-2.7,6-6v-52.5C707.1,187.3,704.4,184.6,701.1,184.6z" />
                                                                </g>
                                                                <text transform="matrix(1 0 0 1 621.999 227.2734)" id="svgsecurity" class="st6 st7"></text>
                                                                <g class="st8">
                                                                    <text transform="matrix(1 0 0 1 518.083 280.0879)" class="st9 st6 st10">security code</text>
                                                                </g>
                                                                <rect x="58.1" y="378.6" class="st11" width="375.5" height="13.5" />
                                                                <rect x="58.1" y="405.6" class="st11" width="421.7" height="13.5" />
                                                                <text transform="matrix(1 0 0 1 59.5073 228.6099)" id="svgnameback" class="st12 st13">John Doe</text>
                                                            </g>
                </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/billing.js') }}"></script>
    <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>
    <script type="text/javascript">
        Conekta.setPublicKey("{{ env('CONEKTA_PUBLIC_KEY') }}");
        Conekta.setLanguage("es");
    </script>
@endsection