
<!-- INICIA MODAL PARA EDITAR PERFIL -->
<div class="modal fade" id="myModalProfileEdit" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title-profile text-center" id="h4EditProfile">Editar Perfil</h4>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
            <form action="{{ Route('edit_user_profile') }}" method="POST" id="formEditProfile" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div>
                        <div class="row">
                            <div class="col-md-6" id="nameEditProfileDiv">
                                <label for="name">Nombre</label>
                                <input type="text" id="name" class="form-control" name="name" required="required" value="{{ Auth::user()->name }}">
                            </div>
                            <div class="col-md-6">
                                <label for="memberFrom">Miembro desde: </label>
                                <input type="text" id="memberFrom" class="form-control" name="memberFrom" required="required" value="{{ Auth::user()->created_at }}" disabled="disabled">  
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="centerJob">Centro de trabajo</label>
                                <input type="text" id="centerJob" class="form-control" name="centerJob" required="required" value="" disabled="disabled">
                            </div>
                            <div class="col-md-6">
                                <label for="email">Correo electrónico</label>
                                <input type="text" id="email" class="form-control" name="email" required="required" value="{{ Auth::user()->email }}" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="row">
                            <div class="col-md-6" id="passwordEditProfileDiv">
                                <label for="password">
                                    Cambiar Contraseña
                                    <input type="checkbox" id="changePassword" name="changePassword">
                                </label>
                                <input type="password" id="password" class="form-control" name="password" value="">
                            </div>
                            <div class="col-md-6">
                            <label for="workstation">Puesto</label>
                    <input type="text" id="workstation" class="form-control" name="workstation" required="required" value="" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    
                    <label for="">Imagen de perfil</label>
                    <br>
                    <img class="rounded mx-auto d-block" alt="Responsive image" width="150px" height="150px" id="imgProfile" @if(Auth::user()->profile_photo != null) src="{{ env('AWS_STORAGE_PUBLIC') . Auth::user()->profile_photo }}" @else src="{{ Gravatar::get($user->email) }}" @endif>
                    <input class="form-control" type="file" name="achievementImgProfile" id="achievementImgProfile">
                        <!-- Modal footer -->
                    <div class="modal-footer">
                        <div class="text-center col-sm-12">
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelEditProfileDiv">Cancelar</button>
                            </div>
                            <div class="col-sm-6" id="btnUpdateButtonDiv">
                                <button type="submit" class="btn btn-primary" id="updateEditProfileDiv">Actualizar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
<!-- TERMINA MODAL-->
