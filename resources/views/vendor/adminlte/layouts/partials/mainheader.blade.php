<!-- Main Header -->
<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo" style="padding: 0 !important;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            @if(Session::get('mini_logo') == null)
                <img src="{{ env('URL_STORAGE_FTP')."mini_logos/vOetciTB7TcFXBbNLwBVIhN6ZqPWFsBJHGwaxOz3.jpeg" }}" alt="logo">
            @else
                <img src="{{ env('URL_STORAGE_FTP').\App\companie::find(auth()->user()->companie)->mini_logo }}" alt="logo">
            @endif
        </span>
        <!-- logo for regular state and mobile devices 208x79 pixels -->
        <span class="logo-lg">
            @if(Session::get('logo') == null)
                <img src="{{ env('URL_STORAGE_FTP')."logos/Ne1f6LZKM2EXX6bFzwMDHQOrUXKnojdngSOTJfDO.png" }}" alt="logo" width="100%" height="53px">
            @else
                <img src="{{ env('URL_STORAGE_FTP').\App\companie::find(auth()->user()->companie)->logo }}" alt="logo" width="100%" height="53px">
            @endif
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="sidebarToggle">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <span class="badge card-badge-danger text-capitalize">#Cliente: PWA-{{ \App\companie::find(auth()->user()->companie)->id }} | Soporte: +52 449 413 9091</span>
        <div class="navbar-custom-menu" style="background-color: darkred;">
            <ul class="nav navbar-nav">

            </ul>
        </div>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <!-- Menu toggle button -->
<!--                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">4</span>
                    </a>-->
                    <ul class="dropdown-menu">
                        <li class="header">Tienes 1 mensaje.</li>
                        <li>
                            <!-- inner menu: contains the messages -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            <!-- User Image -->
                                            <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/>
                                        </div>
                                        <!-- Message title and timestamp -->
                                        <h4>
                                            {{ trans('adminlte_lang::message.supteam') }}
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <!-- The message -->
                                        <p>{{ trans('adminlte_lang::message.awesometheme') }}</p>
                                    </a>
                                </li><!-- end message -->
                            </ul><!-- /.menu -->
                        </li>
                        <li class="footer"><a href="#">c</a></li>
                    </ul>
                </li><!-- /.messages-menu -->

                <!-- Notifications Menu -->
                <li class="dropdown messages-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-danger" id="countNotifications"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header" id="headerNotifications"></li>
                        <li>
                            <!-- Inner Menu: contains the notifications -->
                            <ul class="menu" id="ulNotifications">

                            </ul>
                        </li>
                        <li class="footer"><a class="markAllReadNotifications" style="cursor: pointer;">Marcar todas como leídas.</a></li>
                    </ul>
                </li>
                <!-- Tasks Menu -->
                <li class="dropdown tasks-menu">
                    <!-- Menu Toggle Button -->
                    <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">9</span>
                    </a> -->
                    <ul class="dropdown-menu">
                        <li class="header">{{ trans('adminlte_lang::message.tasks') }}</li>
                        <li>
                            <!-- Inner menu: contains the tasks -->
                            <ul class="menu">
                                <li><!-- Task item -->
                                    <a href="#">
                                        <!-- Task title and progress text -->
                                        <h3>
                                            {{ trans('adminlte_lang::message.tasks') }}
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        <!-- The progress bar -->
                                        <div class="progress xs">
                                            <!-- Change the css width attribute to simulate progress -->
                                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">20% {{ trans('adminlte_lang::message.complete') }}</span>
                                            </div>
                                        </div>
                                    </a>
                                </li><!-- end task item -->
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">{{ trans('adminlte_lang::message.alltasks') }}</a>
                        </li>
                    </ul>
                </li>
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu" id="user_menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img @if(Auth::user()->profile_photo != null) src="{{ env('AWS_STORAGE_PUBLIC') . Auth::user()->profile_photo }}" @else src="{{ Gravatar::get($user->email) }}" @endif class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu" id="ulEditProfileDiv">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img @if(Auth::user()->profile_photo != null) src="{{ env('AWS_STORAGE_PUBLIC') . Auth::user()->profile_photo }}" @else src="{{ Gravatar::get($user->email) }}" @endif class="img-circle" alt="User Image" />
                                <p>
                                    {{ Auth::user()->name }}
                                    <small>Miembro desde: {{ $user->created_at }}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!-- <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.followers') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.sales') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.friends') }}</a>
                                </div>
                            </li> -->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <!-- <div class="pull-left">
                                    <a href="{{ url('/settings') }}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                                </div> -->
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i> Salir
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>
                                </div>
                                <div id="editProfileDiv" class="pull-left">
                                    <a data-toggle="modal" data-target="#myModalProfileEdit" class="btn btn-default btn-flat">
                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Editar perfil</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                @endif
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-question"></i> Tutorial</a>
                </li>
<!--                <li>
                    <a href="#"><i class="fa fa-shopping-cart"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>
