<!-- Main Footer -->
<footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} | <a href="{{ route('terms_conditions') }}" target="_blank">Términos y Condiciones</a> | Developed By </strong> | <img src="{{ asset( 'img/swopyn_mini.png') }}" alt="logo" height="20">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <img src="{{ asset( 'img/pestware_large.png') }}" alt="logo" height="30">
    </div>

</footer>
