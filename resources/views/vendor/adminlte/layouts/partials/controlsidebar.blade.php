<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-question"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
{{--            <h3 class="control-sidebar-heading">Actividades Recientes</h3>--}}
        <!-- <ul class='control-sidebar-menu'>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">{{ trans('adminlte_lang::message.birthday') }}</h4>
                            <p>{{ trans('adminlte_lang::message.birthdaydate') }}</p>
                        </div>
                    </a>
                </li>
            </ul> --><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading"> Índice Tutorial </h3>
            <ul class='control-sidebar-menu'>
                <li id="introQuoatationIndex">
                    <a class="label-a-custom">Crear Cotización</a>
                </li>
                <li id="introScheduleIndex">
                    <a  class="label-a-custom">Agendar Cotización</a>
                </li>
                <li id="introManagementIndex">
                    <a  class="label-a-custom">Administrativo</a>
                </li>
                <li id="introBusinessIndex">
                    <a  class="label-a-custom">Inteligencia de Negocios</a>
                </li>
                <li id="introManelManagementIndex">
                    <a  class="label-a-custom">Panel Configuración</a>
                </li>
            </ul><!-- /.control-sidebar-menu -->

        </div><!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">{{ trans('adminlte_lang::message.statstab') }}</div><!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <h4 class="control-sidebar-heading"> ¿Continuar tutorial donde te quedaste? </h4>
            <ul class='control-sidebar-menu'>
                <li id="questionIntroJs">
                    <a class="label-a-custom"> Continuar Tutorial</a>
                </li>
            </ul>
        </div><!-- /.tab-pane -->
    </div>
</aside><!-- /.control-sidebar

<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>