<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
        <div class="user-panel">
            <div class="pull-left">
                <img @if(Auth::user()->profile_photo != null) src="{{ env('AWS_STORAGE_PUBLIC') . Auth::user()->profile_photo }}" @else src="{{ Gravatar::get($user->email) }}" @endif class="img-circle" alt="User Image" height="30" width="30">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
            </div>
        </div>
        @endif

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" style="margin-top: 10px;" id="ulMenuSideBarDiv">

            @role(['Cuenta Maestra'])
                <li class="header text-center" style="font-weight: bold;">Administrador</li>
            @endrole

            @role(['Administrador'])
                <li class="header text-center" style="font-weight: bold;">Super Usuario (Dev)</li>
            @endrole

            @if(Entrust::can('Mis Cursos'))
                <li><a href="{{ route('training_study') }}" id="universityMenu"><i class="fa fa-graduation-cap" aria-hidden="true"></i><span> Pestware Academy</span></a></li>
            @endif

            <li class="treeview">
                <ul class="treeview-menu">
                    <li><a href="#">{{ trans('adminlte_lang::message.linklevel2') }}</a></li>
                    <li><a href="#">{{ trans('adminlte_lang::message.linklevel2') }}</a></li>
                </ul>
            </li>
                    

            @if(Entrust::can('Cotizaciones'))
                <li><a href="{{ route('index_register') }}" id="attentionClientMenu"><i class="fa fa-address-book" aria-hidden="true"></i><span> Atención al Cliente</span></a></li>
            @endif

            @if(Auth::user()->companie == 338 || Auth::user()->companie == 220)
            @if(Entrust::can('Cotizaciones'))
                <li><a href="{{ route('index_inspection') }}" id="attentionClientMenu"><i class="fa fa-address-book" aria-hidden="true"></i><span> Inspecciones y Varios</span></a></li>
            @endif
            @endif

            @if(Entrust::can('Agenda'))
                <li><a href="{{ route('calendario') }}" id="scheduleMenu"><i class="fa fa-calendar" aria-hidden="true"></i><span> Agenda</span></a></li>
            @endif

            @if(Entrust::can('Clientes') || Entrust::hasRole('Cuenta Maestra'))
                <li><a href="{{ route('index_customers') }}" id="clientMenu"><i class="fa fa-users" aria-hidden="true"></i><span> Clientes</span></a></li>
            @endif

            @if(Entrust::can('Control de Plagas') || Entrust::hasRole('Cuenta Maestra'))
            <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif class="treeview" id="monitorinStationsSide">
                <a href=""><i class="fa fa-bug" aria-hidden="true"></i> <span>Control de Plagas</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="treeview" id="stationMonitoringLiOpen">
                        <a href=""><i class="fa fa-qrcode" aria-hidden="true"></i> <span>Monitoreo de Estaciones</span>
                            <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('index_station_monitoring') }}" @endif id="areasMenu"><i class="fa fa-map-o" aria-hidden="true"></i><span> Áreas</span></a></li>
                            <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('index_inspections_monitoring') }}"@endif id="inspectionsMonMenu" ><i class="fa fa-list-alt" aria-hidden="true"></i><span> Inspecciones</span></a></li>
                        </ul>
                    </li>
                    <li @if(Auth::user()->id_plan == 1 || Auth::user()->id_plan == 2) data-toggle="tooltip" data-placement="right" title="Plan Empresarial" class="treeview" id="stationMonitoringLiOpen" @endif>
                        <a href=""><i class="fa fa-qrcode" aria-hidden="true"></i> <span>Monitoreo de Areás</span>
                            <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li @if(Auth::user()->id_plan == 1 || Auth::user()->id_plan == 2) data-toggle="tooltip"
                                data-placement="right" title="Plan Empresarial" @endif><a @if(Auth::user()->id_plan == 3) href="{{ route('index_area_station_monitoring') }}" @endif id="areasMenu">
                                    <i class="fa fa-map-o" aria-hidden="true"></i><span> Áreas Cliente</span></a>
                            </li>
                            <li @if(Auth::user()->id_plan == 1 || Auth::user()->id_plan == 2) data-toggle="tooltip"
                                data-placement="right" title="Plan Empresarial" @endif><a @if(Auth::user()->id_plan == 3) href="{{ route('index_inspections_area') }}" @endif id="inspectionsMonMenu" >
                                    <i class="fa fa-list-alt" aria-hidden="true"></i><span> Inspecciones</span></a>
                            </li>
                        </ul>
                    </li>
                    <li data-toggle="tooltip" data-placement="right"><a href="{{ route('reporting_station_monitoring') }}"><i class="fa fa-area-chart" aria-hidden="true"></i><span> Reportes de Estaciones</span></a></li>
                    @if(Auth::user()->companie == 338)
                        <li data-toggle="tooltip" data-placement="right"><a href="{{ route('reporting_areas_monitoring') }}"><i class="fa fa-area-chart" aria-hidden="true"></i><span> Reportes de Áreas</span></a></li>
                    @endif
                </ul>
            </li>
            @endif

            {{--@if(Entrust::can('Caja') || Entrust::can('Corte Diario') || Entrust::can('Cortes'))
                <li class="treeview">
                    <a href=""><i class='fa fa-usd'></i><span>Pagos</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        @if(Entrust::can('Caja'))
                            <li><a href="{{route('cash_index')}}">Caja</a></li>
                        @endif

                    </ul>
                </li>
            @endif--}}

            @if(Entrust::can('Inventario') || Entrust::can('Corte Diario'))
                <li class="treeview" id="liMangmentSide">
                    <a href="" id="managmentMenu"><i class='fa fa-briefcase'></i> <span>Administrativo</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu" id="liMangmentSideOpen">
                        @if(Entrust::can('Inventario'))
                            <li class="treeview" id="liInventorySide">
                                <a href=""><i class="fa fa-list-alt" aria-hidden="true"></i> <span>Inventario</span>
                                    <i class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="{{ route('index_product') }}" id="aProductsDiv">Productos</a></li>
                                    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('index_entry') }}" @endif id="aEntryDiv">Entrada de Producto</a></li>
                                    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('index_transfer') }}" @endif id="aTransferDiv" >Traspaso de Producto</a></li>
                                    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('index_inventory') }}" @endif id="aInventoryTheoricDiv">Inventario Teórico</a></li>
                                    @if(Auth::user()->companie == 338)
                                        <li data-toggle="tooltip" data-placement="right"><a href="{{ route('inventory_report_datastudio') }}"><i class="fa fa-area-chart" aria-hidden="true"></i><span> Reportes de Producto</span></a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if(Entrust::can('Mis Gastos'))
                            <li class="treeview">
                                <a href=""><i class="fa fa-credit-card" aria-hidden="true"></i> <span>Gastos</span>
                                    <i class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                    @if(Entrust::can('Contabilidad'))
                                        <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{route('index_accounting')}}" @endif id="accountingMenu" ><i class="fa fa-balance-scale" aria-hidden="true"></i><span> Gastos Generales</span></a></li>
                                    @endif
                                    @if(Entrust::can('Mis Gastos'))
                                        <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{route('my_expense')}}" @endif id="expenseMenu"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span> Mis Gastos</span></a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if(Entrust::can('Corte Diario') || Entrust::can('Cortes'))
                            <li class="treeview">
                                <a href=""><i class="fa fa-scissors" aria-hidden="true"></i> <span>Cortes</span>
                                    <i class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                    @if(Entrust::can('Corte Diario'))
                                        <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('cuts_admin') }}" @endif>Cortes Generales</a></li>
                                    @endif
                                    @if(Entrust::can('Cortes'))
                                        <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('index_cut') }}" @endif>Mis Cortes</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if(Entrust::can('Ventas') || Entrust::hasRole('Cuenta Maestra'))
                            <li><a href="{{ route('index_sales') }}" id="attentionClientMenu"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>Ventas</span></a></li>
                            <li><a href="{{ route('index_billing') }}" id="attentionClientMenu"><i class="fa fa-file-text" aria-hidden="true"></i><span>Facturación</span></a></li>
                        @endif
                        @if(Auth::user()->companie == 37 || Auth::user()->companie == 220 || Auth::user()->companie == 503)
                            <li><a href="{{ route('index_attendances') }}" id="attentionClientMenu"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Asistencias</span></a></li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(Auth::user()->companie == 37 || Auth::user()->companie == 220 || Auth::user()->companie == 629)
                <li>
                    <a href="{{ route('index_gps') }}">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <span>Tracking GPS</span>
                    </a>
                </li>
            @endif

            @if(Entrust::can('Inteligencia Empresarial (BI)'))
                <li>
                    <a href="{{ route('monitoring') }}" id="businessIntellinge">
                        <i class="fa fa-area-chart" aria-hidden="true"></i>
                        <span>Inteligencia de Negocios</span>
                    </a>
                </li>
            @endif

            @if(Entrust::can('Estructura de Centros de Trabajo') || Entrust::can('Estructura de Puestos') || Entrust::can('Empleados') || Entrust::hasRole('Cuenta Maestra'))
                <li class="treeview" id="liPanelSettingPanel">
                    <a href="#" id="settingPanelMenu"><i class="fa fa-cubes" aria-hidden="true"></i> <span>Panel de Configuración</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        
                        <li class="treeview">
                            <a href=""><i class='fa fa-users'></i> <span>Estructura Organizacional</span>
                                <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                @if(Entrust::can('Estructura de Centros de Trabajo') || Entrust::hasRole('Cuenta Maestra'))
                                    <li><a href="{{ route('tree_jobcenter') }}">Estructura de Centros <br>de Trabajo</a></li>
                                @endif
                                @if(Entrust::can('Estructura de Puestos') || Entrust::hasRole('Cuenta Maestra'))
                                    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('tree_jobprofile') }}" @endif>Estructura de Puestos</a></li>
                                @endif
                                @if(Entrust::can('Empleados') || Entrust::hasRole('Cuenta Maestra'))
                                    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('kardex_employees') }}" @endif>Empleados</a></li>
                                @endif
                            </ul>
                        </li>

<!--                        <li class="treeview">
                            <a href=""><i class='fa fa-book'></i> <span>Capacitación</span>
                                <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                @if(Entrust::can('Plan de Capacitación'))
                                    <li><a href="{{ route('training_index_assign') }}">Plan de capacitación</a></li>
                                @endif
                                @if(Entrust::can('Cursos'))
                                    <li><a href="{{ route('training_index') }}">Cursos</a></li>
                                @endif
                            </ul>
                        </li>-->

                        @role(['Cuenta Maestra'])
                        <li class="treeview" id="liPanelSetting">
                            <a href="" id="magnagmentConfig"><i class='fa fa-credit-card'></i> <span>Mi Cuenta</span>
                                <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu" id="ulMagngmentConfigSide">
<!--                                <li><a href="{{ route('purchase_plans') }}">Pago Suscripción</a></li>-->
                                <li><a href="{{ route('index_invoices_my_account') }}">Mis Facturas</a></li>
                            </ul>
                        </li>
                        @endrole

                        @if(Entrust::can('Personalizar (Logos y Tema)')  || Entrust::hasRole('Cuenta Maestra') || Entrust::can('Catalogos del Sistema'))
                        <li class="treeview" id="liPanelSetting">
                            <a href="" id="magnagmentConfig"><i class='fa fa-cogs'></i> <span>Configuración</span>
                                <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu" id="ulMagngmentConfigSide">
                                @if(Entrust::can('Personalizar (Logos y Tema)')  || Entrust::hasRole('Cuenta Maestra'))
                                    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('edit_config_instance') }}" @endif id="customConfigSideDiv">Personalizar</a></li>
                                @endif
                                @if(Entrust::can('Catalogos del Sistema')  || Entrust::hasRole('Cuenta Maestra'))
                                    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="right" title="Plan Emprendor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ route('index_catalogs') }}" @endif>Catálogos del Sistema</a></li>
                                @endif
                            </ul>
                        </li>
                        @endif
                        
                    </ul>
                </li>
            @endif

            @if(Auth::user()->companie == 220 || Auth::user()->companie == 37)
                <li>
                    <a href="{{ route('management_general') }}"><i class="fa fa-folder" aria-hidden="true"></i>
                        <span> Administración General</span>
                    </a>
                </li>
            @endif

        </ul><!-- /.sidebar-menu -->
    </section>
     <!-- /.sidebar -->
</aside>
