@if(session('success'))
    <script>toastr.success("{{ session('message') }}", "{{ session('success') }}")</script>
@elseif(session('info'))
    <script>toastr.info("{{ session('message') }}", "{{ session('info') }}")</script>
@elseif(session('warning'))
    <script>toastr.warning("{{ session('message') }}", "{{ session('warning') }}")</script>
@elseif(session('error'))
    <script>toastr.error("{{ session('message') }}", "{{ session('error') }}")</script>
@endif