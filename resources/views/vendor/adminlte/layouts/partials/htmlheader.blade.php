<head>
    <meta charset="UTF-8">
    <title>PestWareApp</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="icon" type="image/png" href="{{ asset('/img/pestware-cuadre.png') }}" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-EV5N873YG9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-EV5N873YG9');
    </script>

    <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/select2.css')}}" rel="stylesheet" />
    @if(Route::currentRouteName() == "billing_payment")
    <link href="{{ asset('/css/form-payment.css')}}" rel="stylesheet" />
    @endif
    <script src="{{ asset('/js/dropzone.js') }}"></script>
    <link href="{{ asset('/css/dropzone.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos/biofin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos/calendar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos/quotation.css') }}">
    <!-- Instances Pest Wera App -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos/pestwareapp.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/estilos/loader-pestware.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/3.1.0/introjs.css" integrity="sha512-i+WzzATeaDcwcfi5CfLn63qBxrKqiQvDLC+IChU1zVlaPguPgJlddOR07nU28XOoIOno9WPmJ+3ccUInpmHxBg==" crossorigin="anonymous" />
    <!-- Iconos fontawesome -->
    <script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.all.min.js"></script>
    <script src="{{ asset('/js/jquery.circliful.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery.circliful.css') }}">
    <link href="{{ asset('/plugins/fxss-rate/rate.css')}}" rel='stylesheet' />
    <script type="text/javascript" src="{{ asset('/plugins/fxss-rate/rate.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('/css/style.min.css') }}" /> <!-- css de tree js -->
   <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script> <!-- React js -->
    <script type="text/javascript" src="{{ URL::asset('js/build/profile.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/menu.js') }}"></script>
{{--    <script type="text/javascript" src="{{ URL::asset('js/build/build.js') }}"></script> <!-- modules js -->--}}

    <link href="{{ asset('/plugins/fullcalendar/packages/core/main.css')}}" rel='stylesheet' />
    <link href="{{ asset('/plugins/fullcalendar/packages/daygrid/main.css')}}" rel='stylesheet' />
    <link href="{{ asset('/plugins/fullcalendar/packages/timegrid/main.css')}}" rel='stylesheet' />
    <link href="{{ asset('/plugins/fullcalendar/packages/list/main.css')}}" rel='stylesheet' />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/css/lightgallery.min.css">
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js"></script>

    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/packages/core/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/packages/interaction/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/packages/daygrid/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/packages/timegrid/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/packages/list/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/packages/core/locales/es.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/3.1.0/intro.js" integrity="sha512-xKOYlDSfO2wrZECZHmyB4NNNuwQg+Jvs0ZFDAyMnKeOv3hDKs2G4HU+W8inPBxcOt9OBLjX/+Qe0ajWQCT/YxQ==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/imask/3.4.0/imask.min.js"></script>
    <script>
        //See https://laracasts.com/discuss/channels/vue/use-trans-in-vuejs
        window.trans = @php
            // copy all translations from /resources/lang/CURRENT_LOCALE/* to global JS variable
            $lang_files = File::files(resource_path() . '/lang/' . App::getLocale());
            $trans = [];
            foreach ($lang_files as $f) {
                $filename = pathinfo($f)['filename'];
                $trans[$filename] = trans($filename);
            }
            $trans['adminlte_lang_message'] = trans('adminlte_lang::message');
            echo json_encode($trans);
        @endphp
    </script>
    <!--     Smartsupp Live Chat script -->
<!--    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '3a846447921143867cd57012b906e928e6b08e45';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
    </script>-->

    @yield('css-personal')
</head>
