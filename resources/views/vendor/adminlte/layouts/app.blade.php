<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @routes
    @include('adminlte::layouts.partials.htmlheader')
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
@if (\App\companie::find(auth()->user()->companie)->id_theme == 1)
    <body id="body" class="skin-blue sidebar-mini @if(Session::get('sidebar-collapse')) sidebar-collapse @endif">
@elseif(\App\companie::find(auth()->user()->companie)->id_theme == 2)
    <body id="body" class="skin-black sidebar-mini @if(Session::get('sidebar-collapse')) sidebar-collapse @endif">
@elseif(\App\companie::find(auth()->user()->companie)->id_theme == 3)
    <body id="body" class="skin-purple sidebar-mini @if(Session::get('sidebar-collapse')) sidebar-collapse @endif">
@elseif(\App\companie::find(auth()->user()->companie)->id_theme == 4)
    <body id="body" class="skin-yellow sidebar-mini @if(Session::get('sidebar-collapse')) sidebar-collapse @endif">
@elseif(\App\companie::find(auth()->user()->companie)->id_theme == 5)
    <body id="body" class="skin-red sidebar-mini @if(Session::get('sidebar-collapse')) sidebar-collapse @endif">
@else
    <body id="body" class="skin-green sidebar-mini @if(Session::get('sidebar-collapse')) sidebar-collapse @endif">
@endif

<div id="app" v-cloak>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

    <!-- ContentHeader -->

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Modal para editar perfil -->
    @include('vendor.adminlte.profile._modalEditProfile')

    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show
@yield('personal-js')
</body>
</html>
