<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="PestWare App">
    <meta name="description" content="Software para Empresas de Control de Plagas">
    <meta name="keywords" content="Software,Plagas,Fumigaciones,Monitoreo de Estaciones">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>PestWare App | Software para Empresas de Control de Plagas </title>

    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{ asset('landing/images/pestware-cuadre.png') }}">
    <link rel="shortcut icon" type="image/ico" href="{{asset('landing/images/pestware-cuadre.png')}}">
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{ asset('landing/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href=" {{ asset('landing/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/css/linearicons.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/css/magnific-popup.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/css/animate.css') }}">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href=" {{ asset('landing/css/normalize.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/style.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/css/responsive.css') }} ">
    <script src=" {{ asset('landing/js/vendor/modernizr-2.8.3.min.js') }}"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
<!--     Smartsupp Live Chat script -->
    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '3a846447921143867cd57012b906e928e6b08e45';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
    </script>
</head>

<body data-spy="scroll" data-target=".mainmenu-area">

<div class="preloader">
    <span><i class="fas fa-bug"></i></span>
</div>
<!-- MainMenu-Area -->
<nav class="mainmenu-area" data-spy="affix" data-offset-top="200">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary_menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    Llámanos al <span><a href="tel:+52 4494139091">+52 4494139091</a></span> - <span><a href="mailto:contacto@pestwareapp.com">contacto@pestwareapp.com</a></span>
                </div>
                <div class="col text-center">
                    <a target="_blank" href="{{ route('login_new','register') }}">
                        <button class="btn btn-md btn-outline-info">Demostración</button>
                    </a>
                </div>
                <div class="col text-center">
                    <a href=" {{ route('login_new','login') }}">
                        <img src="landing/images/pestware-cuadre.png" alt="" width="24"> <span class="h5"> Inicia Sesión</span>
                    </a>
                </div>
            </div>
        </div>
        <hr>
        <div class="collapse navbar-collapse" id="primary_menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <ul class="nav navbar-nav mainmenu">
                            <li class="active"><a href="#home_page">Conócenos</a></li>
                            <li><a href="#progress_page">Funcionalidades</a></li>
<!--                            <li><a href="#price_page">Precios</a></li>-->
                            <li><a href="#contact_page">Contacto</a></li>
                            <li><a href="/blog">Blog</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- MainMenu-Area-End -->
<!-- Home-Area -->
<center>
    <div class="prueba_svg backgronund-circle"></div>
    <header id="home_page">
        <div class="container">
            <div class="row" style="text-align: center; display: block">
                <div>
                    <div class="space-100"></div>
                    <br><br><br>
                    <h1 class="wow fadeInUp text-center" data-wow-delay="0.4s">Software para Empresas de Control de Plagas</h1>
                    <h4 class="wow fadeInUp text-center" data-wow-delay="0.4s">Tú administración, logística y operación en un mismo lugar</h4>
                    <div class="space-40"></div>
<!--                    <a target="_blank" href="https://play.google.com/store/apps/details?id=com.swopyn.pestwareapp" class="bttn-default wow fadeInUp" data-wow-delay="0.8s"><i class="fab fa-google-play"></i> Descargalo Gratis</a>-->
                    <a target="_blank" href="https://pestwareapp.com/instance/register" class="bttn-default wow fadeInUp" data-wow-delay="0.8s"><i class="fab fa-google-play"></i> Solicitar Demostración</a>
                </div>
            </div>
        </div>
    </header>
</center>
<!-- Home-Area-End -->
<!-- About-Area -->
<section class="home-about video-area" id="about_page">
    <div class="space-40"></div>
    <div class="container">
        <div class="col-xs-12 col-md-12">
            <div class="video-photo">
                <img src="landing/images/backgroundVideo.jpeg" alt="">
                <a href="https://vimeo.com/498196508" class="popup video-button">
                    <img src="landing/images/play-button.png" alt="">
                </a>
            </div>
        </div>
    </div>
</section>
<!-- About-Area-End -->

<!--Area-new-Features -->
<section class="progress-area section-padding" id="new_features">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="page-title text-center">
                    <h3 class="title mt-5">Nuevas funcionalidades</h3>
                    <div class="space-10"></div>
                    <h3>PestWare App Web</h3>
                    <div class="space-60"></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                <div class="page-title">
                    <nav>
                        <div class="nav nav-pills" id="nav-tab-managment" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab-Customers" data-toggle="tab" href="#nav-home-customers" role="tab" aria-controls="nav-home" aria-selected="true">Clientes</a>
                            <a class="nav-item nav-link" id="nav-profile-tab-Area" data-toggle="tab" href="#nav-Area" role="tab" aria-controls="nav-profile" aria-selected="false">Áreas</a>
                            <a class="nav-item nav-link" id="nav-profile-tab-quotations" data-toggle="tab" href="#nav-quotation" role="tab" aria-controls="nav-profile" aria-selected="false">Cotizaciones Personalizadas</a>
                            <a class="nav-item nav-link" id="nav-profile-tab-quotations" data-toggle="tab" href="#nav-excel" role="tab" aria-controls="nav-profile" aria-selected="false">Exportar a Excel</a>
                        </div>
                    </nav>
                    <br>
                    <div class="tab-content" id="nav-tabContent-managment">
                        <div class="tab-pane fade show active" id="nav-home-customers" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="col-md-10 mt-5">
                                <img src="landing/images/landing_clientes_mod.png" alt="Feature Photo" width="100%">
                            </div>
                            <div class="col-md-2 mt-5">
                                <h5 class="title">Clientes</h5>
                                <p class="wow fadeInUp text-left mt-3" data-wow-delay="0.4s">El nuevo módulo "Clientes" permite llevar un control de los clientes registrados en la plataforma: .</p>
                                <br>
                                <ul style="list-style: none; padding-left: 0px">
                                    <li class="wow fadeInUp text-left" data-wow-delay="0.4s">*Editar sucursales de un cliente.</li>
                                    <li class="wow fadeInUp text-left" data-wow-delay="0.4s">*Crear cuenta para portal de cliente.</li>
                                    <li class="wow fadeInUp text-left" data-wow-delay="0.4s">*Reseteo de contraseña del cliente.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-Area" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="col-md-2 mt-5">
                                <h5 class="title" data-wow-delay="0.4s">Monitoreo de Áreas</h5>
                                <p class="wow text-left mt-3" data-wow-delay="0.4s"> Pódras realizar servícios de forma masiva en lugares como: Hoteles y Condominios:</p>
                                <br>
                                <ul class="text-left" style="list-style: none; padding-left: 0px">
                                    <li class="wow" data-wow-delay="0.4s">*Generación de codigos QR.</li>
                                    <li class="wow" data-wow-delay="0.4s">*App móvil para escaneo de áreas.</li>
                                    <li class="wow" data-wow-delay="0.4s">*Reportes y Gráficas.</li>
                                </ul>
                            </div>
                            <div class="col-md-10 mt-5">
                                <img src="landing/images/landing_areas_mod.png" alt="Feature Photo" width="100%">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-quotation" role="tabpanel" aria-labelledby="nav-profile-tab">

                            <div class="col-md-2 mt-5">
                                <h5 class="title mt-5" data-wow-delay="0.4s">Cotizaciones Personalizadas</h5>
                                <p class="wow text-left mt-3" data-wow-delay="0.4s">Algunas mejoras son:</p>
                                <br>
                                <ul class="text-left" style="list-style: none; padding-left: 0px">
                                    <li class="wow" data-wow-delay="0.4s">*Tipos de conceptos: Servicio, Contable y Sucursal.</li>
                                    <li class="wow" data-wow-delay="0.4s">*Historial de descripciones.</li>
                                </ul>
                            </div>
                            <div class="col-md-10 mt-5">
                                <img src="landing/images/landing_custom_quotations.png" alt="Feature Photo" width="100%">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-excel" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="text-center">
                                <h5 class="title" class="col-md-5 mt-5" data-wow-delay="0.4s">Exportar Ordenes de Servicio</h5>
                                <p data-wow-delay="0.4s">Ahora podras exportar datos en específico de las ordenes de servicio a Excel.</p>

                            </div>
                            <div class="text-center">
                                <img src="landing/images/landing_export_excel.png" alt="Feature Photo" width="40%" >
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<!-- New Features Area End-->

<!-- Progress-Area -->
<section class="progress-area section-padding" id="progress_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="page-title text-center">
                    <h3 class="title mt-5">Funcionalidades Web</h3>
                    <div class="space-10"></div>
                    <h3>PestWare App Web</h3>
                    <div class="space-60"></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                <div class="page-title">
                    <nav>
                        <div class="nav nav-pills" id="nav-tab-managment" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab-panel" data-toggle="tab" href="#nav-home-panel" role="tab" aria-controls="nav-home" aria-selected="true">Portal Administrativo</a>
                            <a class="nav-item nav-link" id="nav-profile-tab-portal" data-toggle="tab" href="#nav-portal" role="tab" aria-controls="nav-profile" aria-selected="false">Portal Cliente</a>
                            <a class="nav-item nav-link" id="nav-contact-tab-app" data-toggle="tab" href="#nav-app" role="tab" aria-controls="nav-contact" aria-selected="false">App móvil</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent-managment">
                        <div class="tab-pane fade show active" id="nav-home-panel" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="col-md-10 mt-5">
                                <img src="landing/images/diap-2.png" alt="Feature Photo" width="100%">
                            </div>
                            <div class="col-md-2 mt-5">
                                <h5 class="title">Portal Administrativo</h5>
                                <p class="wow fadeInUp text-left mt-3" data-wow-delay="0.4s">Con Pestware App el control diario de tu empresa es sencillo, ya que todas nuestras herramientas son fáciles e intuitivas de utilizar.</p>
                                <br>
                                <ul style="list-style: none; padding-left: 0px">
                                    <li class="wow fadeInUp text-left" data-wow-delay="0.4s">Cotizador</li>
                                    <li class="wow fadeInUp text-left" data-wow-delay="0.4s">Inventarios</li>
                                    <li class="wow fadeInUp text-left" data-wow-delay="0.4s">Control de Gastos</li>
                                    <li class="wow fadeInUp text-left" data-wow-delay="0.4s">Agenda de Trabajo</li>
                                    <li class="wow fadeInUp text-left" data-wow-delay="0.4s">Reportes Inteligentes</li>
                                </ul>
                                <p class="wow fadeInUp text-left" data-wow-delay="0.8s">Tu Administración será simple y completa.</p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-portal" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="col-md-2 mt-5">
                                <h5 class="title" data-wow-delay="0.4s">Portal Clientes</h5>
                                <p class="wow text-left mt-3" data-wow-delay="0.4s">Si tus Cliente te solicitan:</p>
                                <br>
                                <ul class="text-left" style="list-style: none; padding-left: 0px">
                                    <li class="wow" data-wow-delay="0.4s">Carpetas Operativas</li>
                                    <li class="wow" data-wow-delay="0.4s">Informes de Servicio</li>
                                    <li class="wow" data-wow-delay="0.4s">Certificados y Constancias</li>
                                    <li class="wow" data-wow-delay="0.4s">Reportes y Gráficas de Tendencia</li>
                                    <li class="wow" data-wow-delay="0.4s">Diplomas de Capacitación de Técnicos</li>
                                    <li class="wow" data-wow-delay="0.4s">Documentación para Auditorias Internas</li>
                                    <li class="wow" data-wow-delay="0.4s">Hojas de Seguridad  y Fichas Técnicas de Plaguicidas</li>
                                </ul>
                                <p class="wow fadeInUp text-center" data-wow-delay="0.4s">Genera esta y mucha más documentación en tiempo real en portales personalizados para cada uno de tus Clientes.</p>
                            </div>
                            <div class="col-md-10 mt-5">
                                <img src="landing/images/atencion-a-cliente2.png" alt="Feature Photo" width="100%">
                            </div>
                        </div>
                        <div class="fade" id="nav-app" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="col-md-6 mt-5">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="gallery-slide">
                                                <div class="item"><img src="landing/images/introCel/intro1.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro2.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro3.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro4.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro5.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro6.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro7.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro8.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro9.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro10.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro11.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro12.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro13.jpg" alt=""></div>
                                                <div class="item"><img src="landing/images/introCel/intro14.jpg" alt=""></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 mt-5">
                                <h5 class="wow title" data-wow-delay="0.4s">App Móvil</h5>
                                <p class="wow fadeInUp text-left mt-3" data-wow-delay="0.4s">Facilita el trabajo de tu personal de campo y obtén la siguiente información de cada servicio al momento:</p>
                                <br>
                                <ul class="wow fadeInUp text-left" data-wow-delay="0.4s" style="list-style: none; padding-left: 0px">
                                    <li class="wow fadeInUp " data-wow-delay="0.4s">Agenda de Trabajo</li>
                                    <li class="wow fadeInUp " data-wow-delay="0.4s">Detalle de Servicio</li>
                                    <li class="wow fadeInUp " data-wow-delay="0.4s">Informe de Servicio</li>
                                    <li class="wow fadeInUp " data-wow-delay="0.4s">Cortes de Servicio</li>
                                    <li class="wow fadeInUp " data-wow-delay="0.4s">Monitoreo de Estaciones</li>
                                </ul>
                                <p class="wow fadeInUp text-left" data-wow-delay="0.4s">Supervisa el trabajo de campo y agiliza el envío de información entre oficina y técnicos.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-60"></div>
            <div class="col-xs-12 col-md-12">
                <div class="page-title">
                    <nav>
                        <div class="nav nav-pills" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab-customer" data-toggle="tab" href="#nav-customer" role="tab" aria-controls="nav-customer" aria-selected="true">Atención al Cliente</a>
                            <a class="nav-item nav-link" id="nav-schedule-tab" data-toggle="tab" href="#nav-schedule" role="tab" aria-controls="nav-schedule" aria-selected="false">Agenda de Trabajo</a>
                            <a class="nav-item nav-link" id="nav-inventory-tab" data-toggle="tab" href="#nav-inventory" role="tab" aria-controls="nav-inventory" aria-selected="false">Inventarios</a>
                            <a class="nav-item nav-link" id="nav-expense-tab" data-toggle="tab" href="#nav-expense" role="tab" aria-controls="nav-expense" aria-selected="false">Control de Gastos</a>
                            <a class="nav-item nav-link" id="nav-bussiness-tab" data-toggle="tab" href="#nav-bussiness" role="tab" aria-controls="nav-bussiness" aria-selected="false">Reportes Inteligentes</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-customer" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="col-md-2 mt-5">
                                <h5 class="wow fadeInUp title" data-wow-delay="0.4s">Atención al Cliente</h5>
                                <p class="wow fadeInUp text-left mt-3" data-wow-delay="0.4s">
                                    Contamos con todo un módulo lleno de herramientas para brindar la mejor atención y experiencia a tu
                                    cliente, podrás generar cotizaciones estandarizadas o personalizadas, descargarlas en PDF, enviarlas por Email o Whatsapp y muchas más.
                                </p>
                                <br>
                                <div class="text-center">
<!--                                    <a href="https://www.youtube.com/watch?v=xpGsvZ8-zN4" class="popup video-button">
                                        <button class="wow fadeInUp btn btn-md btn-outline-info">Conoce más</button>
                                    </a>-->
                                </div>
                            </div>
                            <div class="col-md-10 mt-5">
                                <img src="landing/images/diap-5-atencion%20al%20cliente.png" alt="Feature Photo" width="100%">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-schedule" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="col-md-10 mt-5">
                                <img src="landing/images/diap-5-agenda.png" alt="Feature Photo" width="100%">
                            </div>
                            <div class="col-md-2 mt-5">
                                <h5 class="wow fadeInUp title" data-wow-delay="0.4s">Agenda de Trabajo</h5>
                                <p class="wow fadeInUp text-left mt-3" data-wow-delay="0.4s">
                                    Controla todas tus órdenes de servicio en un solo lugar, asígnalo a tus técnicos y notifícalos en tiempo real, envía recordatorios e indicaciones
                                    de preparación a tus clientes por Whatsapp, supervisa las actividades concluidas y visualiza la información que registro tu Técnico en Campo y mucho más.
                                </p>
                                <br>
                                <div class="text-center">
<!--                                    <button class="wow fadeInUp btn btn-md btn-outline-info">Conoce más</button>-->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-inventory" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="col-md-2 mt-5">
                                <h5 class="wow fadeInUp title" data-wow-delay="0.4s">Inventarios</h5>
                                <p class="wow fadeInUp text-left mt-3" data-wow-delay="0.4s">
                                    Controla tus insumos, equipos y herramientas de trabajo en tiempo real. A través de Pestware App Móvil tu Técnico registrará el producto
                                    utilizado para contabilizar automáticamente el costo de cada servicio disminuyen las mermas de tus  insumos.
                                </p>
                                <br>
                                <div class="text-center">
<!--                                    <button class="wow fadeInUp btn btn-md btn-outline-info">Conoce más</button>-->
                                </div>
                            </div>

                            <div class="col-md-10 mt-5">
                                <img src="landing/images/diap-6-inventories.png" alt="Feature Photo" width="100%">
                            </div>

                        </div>
                        <div class="tab-pane fade" id="nav-expense" role="tabpanel" aria-labelledby="nav-contact-tab">

                            <div class="col-md-10 text-center mt-5">
                                <img src="landing/images/control-gastos.png" alt="Feature Photo" width="100%">
                            </div>

                            <div class="col-md-2 mt-5">
                                <h5 class="wow fadeInUp title" data-wow-delay="0.4s">Control de Gastos</h5>
                                <p class="wow fadeInUp text-left mt-3" data-wow-delay="0.4s">
                                    Llevar la contabilidad de tu negocio es sencillo con las herramientas que te brindamos para el registro y administración de los gastos, conocerás
                                    al momento cuanto y en que gastas para saber cuanto estas ganando o perdiendo todo sin tener que esperar a que tu contador te lo diga.
                                </p>
                                <br>
                                <div class="text-center">
<!--                                    <button class="wow fadeInUp btn btn-md btn-outline-info">Conoce más</button>-->
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="nav-bussiness" role="tabpanel" aria-labelledby="nav-contact-tab">

                            <div class="col-md-6 mt-5">
                                <h5 class="wow fadeInUp title" data-wow-delay="0.4s">Resportes Inteligentes</h5>
                                <p class="wow fadeInUp text-left mt-3" data-wow-delay="0.4s">
                                    Indicadores claros que te ayudarán a tomar las mejores decisiones en tu negocio como:
                                </p>
                                <br>
                                <ul class="wow fadeInUp text-center" data-wow-delay="0.4s" style="list-style: none; padding-left: 0px">
                                    <li class="wow fadeInUp text-center" data-wow-delay="0.4s">Gasto Total</li>
                                    <li class="wow fadeInUp text-center" data-wow-delay="0.4s">Ventas Brutas</li>
                                    <li class="wow fadeInUp text-center" data-wow-delay="0.4s">Ingreso Bruto</li>
                                    <li class="wow fadeInUp text-center" data-wow-delay="0.4s">Beneficio Bruto</li>
                                    <li class="wow fadeInUp text-center" data-wow-delay="0.4s">Cotizaciones Totales</li>
                                    <li class="wow fadeInUp text-center" data-wow-delay="0.4s">Índice de Satisfacción</li>
                                    <li class="wow fadeInUp text-center" data-wow-delay="0.4s">Gráficos de Tendencia</li>
                                    <li class="wow fadeInUp text-center" data-wow-delay="0.4s">Segmentación por Concepto
                                    </li>
                                </ul>
                                <p class="wow fadeInUp text-center" data-wow-delay="0.4s">Reportes automáticos en tiempo Real para conocer la salud financiera de tu negocio.</p>
                                <br>
                                <div class="text-center">
<!--                                    <button class="wow fadeInUp btn btn-md btn-outline-info">Conoce más</button>-->
                                </div>
                            </div>

                            <div class="col-md-6 text-center mt-5">
                                <img src="landing/images/report.png" alt="Feature Photo" width="30%" height="50%">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Progress-Area-End -->

<!-- Feature-Area -->
<section class="feature-area" id="features_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="page-title text-center">
                    <h3 class="title mt-5">Funcionalidades App</h3>
                    <div class="space-10"></div>
                    <h3>PestWare App Móvil</h3>
                    <div class="space-60"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a id="smartphonePicture1">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                        <div class="box-icon">
                            <i class="fas fa-camera-retro"></i>
                        </div>
                        <h4>Evidencias Fotográficas</h4>
                        <p>Registra con fotografías las diferentes situaciones de cada servicio.</p>
                    </div>
                    <div class="text-center mt-4">
                        <figure class="hidden-md hidden-lg hidden-xl hidden-xxl">
                            <img src="landing/images/phone1.png" alt="Feature Photo" width="60%" height="2000">
                        </figure>
                    </div>
                </a>
                <div class="space-60"></div>
                <a id="smartphonePicture2">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.4s">
                        <div class="box-icon">
                            <i class="fas fa-wine-bottle"></i>
                        </div>
                        <h4>Control de Insumos</h4>
                        <p>Lleva la administración de cuanto producto tienes y utilizas en cada servicio</p>
                    </div>
                    <div class="text-center mt-4">
                        <figure class="hidden-md hidden-lg hidden-xl hidden-xxl">
                            <img src="landing/images/phone2.png" alt="Feature Photo" width="60%" height="2000">
                        </figure>
                    </div>
                </a>
                <div class="space-60"></div>
                <a id="smartphonePicture3">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.6s">
                        <div class="box-icon">
                            <i class="fas fa-money-check-alt"></i>
                        </div>
                        <h4>Registro de Pagos</h4>
                        <p>Controla los ingresos de los servicios que realizan tus Técnicos en campo para mantener una carta de cobranza sana.</p>
                    </div>
                    <div class="text-center mt-4">
                        <figure class="hidden-md hidden-lg hidden-xl hidden-xxl">
                            <img src="landing/images/phone3.png" alt="Feature Photo" width="60%" height="2000">
                        </figure>
                    </div>
                </a>
                <div class="space-60"></div>
            </div>
            <div class="col-md-4 text-center">
                <figure class="hidden-xs hidden-sm">
                    <img id="smartphoneThemImg" src="landing/images/phone1.png" alt="Feature Photo" width="60%" height="2000">
                </figure>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a id="smartphonePicture4">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                        <div class="box-icon">
                            <i class="fas fa-file-signature"></i>
                        </div>
                        <h4>Firmas Digitales</h4>
                        <p>Registra la firma de conformidad de tus clientes en cada servicio y brinda formalidad en cada trabajo que realices.</p>
                    </div>
                    <div class="text-center mt-4">
                        <figure class="hidden-md hidden-lg hidden-xl hidden-xxl">
                            <img src="landing/images/phone4.png" alt="Feature Photo" width="60%" height="2000">
                        </figure>
                    </div>
                </a>
                <div class="space-60"></div>
                <a id="smartphonePicture5">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.4s">
                        <div class="box-icon">
                            <i class="fas fa-receipt"></i>
                        </div>
                        <h4>Certificados Digitales</h4>
                        <p>Entrega Certificados Digitales de manera automática de cada servicio para dar cumplimiento a la normatividad.</p>
                    </div>
                    <div class="text-center mt-4">
                        <figure class="hidden-md hidden-lg hidden-xl hidden-xxl">
                            <img src="landing/images/phone5.png" alt="Feature Photo" width="60%" height="2000">
                        </figure>
                    </div>
                </a>
                <div class="space-60"></div>
                <a id="smartphonePicture6">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.6s">
                        <div class="box-icon">
                            <i class="fas fa-project-diagram"></i>
                        </div>
                        <h4>Muchas más funciones</h4>
                        <p>Marcación Directa al cliente Indicaciones de  ruta por GPS Programación de Refuerzos
                            Envío de Certificados por Whatsapp Descarga tu prueba Gratis y descubre más Aquí
                        </p>
                    </div>
                    <div class="text-center mt-4">
                        <figure class="hidden-md hidden-lg hidden-xl hidden-xxl">
                            <img src="landing/images/phone6.png" alt="Feature Photo" width="60%" height="2000">
                        </figure>
                    </div>
                </a>
                <div class="space-60"></div>
            </div>
        </div>
    </div>
</section>

<!-- Feature-Area-End -->

<!--Start Price-Area -->
<!--<section class="price-area" id="price_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title text-center">
                    <h3 class="title">Precios y Planes</h3>
                    <h4 class="dark-color">
                        Nuestros Planes son flexibles y se adaptan al tamaño de tu negocio para que pagues solo por lo que necesitas.
                    </h4>
                    <div class="space-60"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="price-box">
                    <div class="price-header">
                        <div class="price-icon">
                            <span class=""></span>
                            <i class="lnr lnr-rocket"></i>
                        </div>
                        <h4 class="upper">15 días gratis</h4>
                    </div>
                    <div class="price-body" style="padding-top: 58px; padding-bottom: 205px">
                        <ul>
                            <li>15 días Acceso Completo</li>
                            <br>
                            <li>App Móvil</li>
                            <li>Cotizador</li>
                            <li>1 Usuario</li>
                            <li>1 Sucursal</li>
                            <li>Programación de Servicios</li>
                        </ul>
                        <br>
                        <p style="font-size: 10px">*1 demo por empresa.</p>
                    </div>
&lt;!&ndash;                    <div class="price-rate">
                        <sup>&#36;</sup><span class="rate">0</span><small>Gratis</small>
                    </div>&ndash;&gt;
                    <div class="price-footer">
                        <a href="{{ route('login_new','register') }}" class="bttn-white">Pruébalo Gratis</a>
                    </div>
                </div>
                <div class="space-30 hidden visible-xs"></div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="price-box">
                    <div class="price-header">
                        <div class="price-icon">
                            <i class="lnr lnr-store"></i>
                        </div>
                        <h4 class="upper">Emprendedor</h4>
                    </div>
                    <div class="price-body" style="padding: 30px">
                        <ul>
                            <li class="font-weight-bold">3 Usuarios***</li>
                            <li class="font-weight-bold">1 Sucursal***</li>
                            <li class="font-weight-bold">Cursos</li>
                            <li>Cotizador</li>
                            <li>App Móvil</li>
                            <li class="font-weight-bold">Clientes</li>
                            <li class="font-weight-bold">Registro de Gastos</li>
                            <li class="font-weight-bold">Reportes Inteligentes</li>
                            <li class="font-weight-bold">Cortes Administrativos</li>
                            <li class="font-weight-bold">Control de Inventarios</li>
                            <li class="font-weight-bold">Monitoreo de Estaciones</li>
                            <li>Programación de Servicios</li>
                            <li class="font-weight-bold">Envío de mensajes por Whatsapp</li>
                            <li class="font-weight-bold">Recordatorio Refuerzos y Recurrentes</li>
                        </ul>
                        <br>
&lt;!&ndash;                        <ul>
                            <li>*Costo fuera de México</li>
                            <li>**Usuario extra $99 MXN/9 USD</li>
                            <li>***Sucursal extra $499 MXN/29 USD</li>
                            <li>(El costo por sucursal es por sede de operación)</li>
                        </ul>&ndash;&gt;
                        <br>
                        <p style="font-size: 10px">Precios más impuestos. <br>
                            Los impuestos aplican para nacionales y extranjeros.</p>
                        <p style="font-size: 10px">Solicita tu Cotización</p>
                    </div>
                    <br>
&lt;!&ndash;                    <div class="price-rate">
                        <sup>&#36;</sup> <span class="rate">499</span><small>MXN/ 29 USD *Mensual</small>
                    </div>&ndash;&gt;
                    <div class="price-footer">
                        <a href="{{ route('login_new','register') }}" class="bttn-white">Suscríbete</a>
                    </div>
                </div>
                <div class="space-30 hidden visible-xs"></div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="price-box">
                    <div class="price-header">
                        <div class="price-icon">
                            <i class="lnr lnr-apartment"></i>
                        </div>
                        <h4 class="upper">Empresarial</h4>
                    </div>
                    <div class="price-body" style="padding: 5px">
                        <ul>
                            <li class="font-weight-bold">6 Usuarios***</li>
                            <li class="font-weight-bold">1 Sucursal***</li>
                            <li>Cursos</li>
                            <li>Cotizador</li>
                            <li>App Móvil</li>
                            <li class="font-weight-bold">Portal de Clientes</li>
                            <li>Registro de Gastos</li>
                            <li>Reportes Inteligentes</li>
                            <li>Cortes Administrativos</li>
                            <li>Control de Inventarios</li>
                            <li>de Clientes</li>
                            <li class="font-weight-bold">Monitoreo de Áreas</li>
                            <li>Monitoreo de Estaciones</li>
                            <li>Programación de Servicios</li>
                            <li>Envío de mensajes por Whatsapp</li>
                            <li>Recordatorio Refuerzos y Recurrentes</li>
                            <li class="font-weight-bold">Programación Inteligente Multisucursal</li>
                            <li class="font-weight-bold">Optimización de Rutas por Google Maps</li>
                        </ul>
                        <br>
&lt;!&ndash;                        <ul>
                            <li>*Costo fuera de México</li>
                            <li>**Usuario extra $99 MXN/9 USD</li>
                            <li>***Sucursal extra con 3 usuarios $699 MXN/34 USD</li>
                            <li>(El costo por sucursal es por sede de operación)</li>
                        </ul>&ndash;&gt;
                        <br>
                        <p style="font-size: 10px">Precios más impuestos. <br>
                            Los impuestos aplican para nacionales y extranjeros.</p>
                        <p style="font-size: 10px">Solicita tu Cotización</p>
                    </div>
&lt;!&ndash;                    <div class="price-rate">
                        <sup>&#36;</sup> <span class="rate">899</span><small>MXN/ 49 USD *Mensual</small>
                    </div>&ndash;&gt;
                    <div class="price-footer">
                        <a href="{{ route('login_new','register') }}" class="bttn-white">Suscríbete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--End Price-Area -->

<!-- Footer-Area -->
<footer class="footer-area" id="contact_page">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title text-center">
                        <h3 class="title">Contáctanos</h3>
                        <h3 class="dark-color">Por medio de...</h3>
                        <div class="space-60"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <div class="box-icon">
                            <span class="lnr lnr-map-marker"></span>
                        </div>
                        <p>México</p>
                    </div>
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <a target="_blank" href="https://api.whatsapp.com/send/?phone=524494139091&text=Hola,%20quiero%20m%C3%A1s%20informaci%C3%B3n">
                            <div class="box-icon">
                                <span class="fab fa-whatsapp"></span>
                            </div>
                            <p>+52 4494139091</p>
                        </a>
                    </div>
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <a target="_blank" href="mailto:contacto@pestwareapp.com">
                            <div class="box-icon">
                                <span class="lnr lnr-envelope"></span>
                            </div>
                            <p>contacto@pestwareapp.com</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Gallery-Area
   <section class="gallery-area section-padding" id="gallery_page">
       <div class="container-fluid">
           <div class="row">
               <div class="col-xs-12 col-sm-6 gallery-slider">
                   <div class="gallery-slide">
                       <div class="item"><img src="images/gallery-1.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-2.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-3.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-4.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-1.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-2.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-3.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-1.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-2.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-3.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-4.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-1.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-2.jpg" alt=""></div>
                       <div class="item"><img src="images/gallery-3.jpg" alt=""></div>
                   </div>
               </div>
               <div class="col-xs-12 col-sm-5 col-lg-3">
                   <div class="page-title">
                       <h5 class="white-color title wow fadeInUp" data-wow-delay="0.2s">Screenshots</h5>
                       <div class="space-10"></div>
                       <h3 class="white-color wow fadeInUp" data-wow-delay="0.4s">Screenshot 01</h3>
                   </div>
                   <div class="space-20"></div>
                   <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                       <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut labore et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                   </div>
                   <div class="space-50"></div>
                   <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s">Learn More</a>
               </div>
           </div>
       </div>
   </section>
    Gallery-Area-End

    Video-Area
   <section class="video-area section-padding">
       <div class="container">
           <div class="row">
               <div class="col-xs-12 col-md-6">
                   <div class="video-photo">
                       <img src="images/video-image.jpg" alt="">
                       <a href="https://www.youtube.com/watch?v=ScrDhTsX0EQ" class="popup video-button">
                           <img src="images/play-button.png" alt="">
                       </a>
                   </div>
               </div>
               <div class="col-xs-12 col-md-5 col-md-offset-1">
                   <div class="space-60 hidden visible-xs"></div>
                   <div class="page-title">
                       <h5 class="title wow fadeInUp" data-wow-delay="0.2s">VIDEO FEATURES</h5>
                       <div class="space-10"></div>
                       <h3 class="dark-color wow fadeInUp" data-wow-delay="0.4s">Grat Application Ever</h3>
                       <div class="space-20"></div>
                       <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                           <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut labore et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                       </div>
                       <div class="space-50"></div>
                       <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s">Learn More</a>
                   </div>
               </div>
           </div>
       </div>
   </section>
    Video-Area-End

    Testimonial-Area
   <section class="testimonial-area" id="testimonial_page">
       <div class="container">
           <div class="row">
               <div class="col-xs-12">
                   <div class="page-title text-center">
                       <h5 class="title">Testimonials</h5>
                       <h3 class="dark-color">Our Client Loves US</h3>
                       <div class="space-60"></div>
                   </div>
               </div>
           </div>
           <div class="row">
               <div class="col-xs-12">
                   <div class="team-slide">
                       <div class="team-box">
                           <div class="team-image">
                               <img src="images/team-1.png" alt="">
                           </div>
                           <h4>Ashekur Rahman</h4>
                           <h6 class="position">Art Dirrector</h6>
                           <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                       </div>
                       <div class="team-box">
                           <div class="team-image">
                               <img src="images/team-2.jpg" alt="">
                           </div>
                           <h4>Ashekur Rahman</h4>
                           <h6 class="position">Art Dirrector</h6>
                           <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                       </div>
                       <div class="team-box">
                           <div class="team-image">
                               <img src="images/team-3.jpg" alt="">
                           </div>
                           <h4>Ashekur Rahman</h4>
                           <h6 class="position">Art Dirrector</h6>
                           <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                       </div>
                       <div class="team-box">
                           <div class="team-image">
                               <img src="images/team-1.png" alt="">
                           </div>
                           <h4>Ashekur Rahman</h4>
                           <h6 class="position">Art Dirrector</h6>
                           <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                       </div>
                       <div class="team-box">
                           <div class="team-image">
                               <img src="images/team-2.jpg" alt="">
                           </div>
                           <h4>Ashekur Rahman</h4>
                           <h6 class="position">Art Dirrector</h6>
                           <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                       </div>
                       <div class="team-box">
                           <div class="team-image">
                               <img src="images/team-3.jpg" alt="">
                           </div>
                           <h4>Ashekur Rahman</h4>
                           <h6 class="position">Art Dirrector</h6>
                           <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>
    Testimonial-Area-End
    Gallery-Area-End
    How-To-Use
   <section class="section-padding">
       <div class="container">
           <div class="row">
               <div class="col-xs-12 col-sm-6">
                   <div class="page-title">
                       <h5 class="title wow fadeInUp" data-wow-delay="0.2s">Our features</h5>
                       <div class="space-10"></div>
                       <h3 class="dark-color wow fadeInUp" data-wow-delay="0.4s">Aour Approach of Design is Prety Simple and Clear</h3>
                   </div>
                   <div class="space-20"></div>
                   <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                       <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut labore et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                   </div>
                   <div class="space-50"></div>
                   <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s">Learn More</a>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                   <div class="space-60 hidden visible-xs"></div>
                   <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                       <div class="box-icon">
                           <i class="lnr lnr-clock"></i>
                       </div>
                       <h4>Easy Notifications</h4>
                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                   </div>
                   <div class="space-50"></div>
                   <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                       <div class="box-icon">
                           <i class="lnr lnr-laptop-phone"></i>
                       </div>
                       <h4>Fully Responsive</h4>
                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                   </div>
                   <div class="space-50"></div>
                   <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                       <div class="box-icon">
                           <i class="lnr lnr-cog"></i>
                       </div>
                       <h4>Editable Layout</h4>
                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                   </div>
               </div>
           </div>
       </div>
   </section>
    How-To-Use-End
    Download-Area
   <div class="download-area overlay">
       <div class="container">
           <div class="row">
               <div class="col-xs-12 col-sm-6 hidden-sm">
                   <figure class="mobile-image">
                       <img src="images/download-image.png" alt="">
                   </figure>
               </div>
               <div class="col-xs-12 col-md-6 section-padding">
                   <h3 class="white-color">Download The App</h3>
                   <div class="space-20"></div>
                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam possimus eaque magnam eum praesentium unde.</p>
                   <div class="space-60"></div>
                   <a href="#" class="bttn-white sq"><img src="images/apple-icon.png" alt="apple icon"> Apple Store</a>
                   <a href="#" class="bttn-white sq"><img src="images/play-store-icon.png" alt="Play Store Icon"> Play Store</a>
               </div>
           </div>
       </div>
   </div>
    Download-Area-End
   Questions-Area-End
    Subscribe-Form
   <div class="subscribe-area section-padding">
       <div class="container">
           <div class="row">
               <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                   <div class="subscribe-form text-center">
                       <h3 class="blue-color">Subscribe for More Features</h3>
                       <div class="space-20"></div>
                       <form id="mc-form">
                           <input type="email" class="control" placeholder="Enter your email" required="required" id="mc-email">
                           <button class="bttn-white active" type="submit"><span class="lnr lnr-location"></span> Subscribe</button>
                           <label class="mt10" for="mc-email"></label>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
    Subscribe-Form-Area -->
    <!--Price-Area-End -->
    <!--&lt;!&ndash;Questions-Area &ndash;&gt;
    <section id="questions_page" class="questions-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title text-center">
                        <h5 class="title">FAQ</h5>
                        <h3 class="dark-color">Frequently Asked Questions</h3>
                        <div class="space-60"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="toggole-boxs">
                        <h3>Faq first question goes here? </h3>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                        <h3>About freewuent question goes here? </h3>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                        <h3>Why more question goes here? </h3>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                        <h3>What question goes here? </h3>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="space-20 hidden visible-xs"></div>
                    <div class="toggole-boxs">
                        <h3>Faq second question goes here? </h3>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                        <h3>Third faq question goes here? </h3>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                        <h3>Why more question goes here? </h3>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                        <h3>Seventh frequent question here? </h3>
                        <div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->

    <!--Footer-Bootom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-5">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <a href="https://pestwareapp.com" target="_blank"> <span>Copyright &copy;<script>document.write(new Date().getFullYear());</script> | powered by PestwareApp | <span class="font-weight-bold">Developed Swopyn</span></span> </a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-md-7">
                    <div class="footer-menu">
                        <ul>
                            <li><a href="#about_page">Acerca De</a></li>
                            <li><a href="#progress_page">Funcionalidades</a></li>
                            <!--                                <li><a href="#">Features</a></li>-->
                            <li><a href="#price_page">Precios</a></li>
                            <!--                                <li><a href="#">Testimonial</a></li>-->
                            <li><a href="#contact_page">Contacto</a></li>
                            <li><a href="{{ route('terms_conditions') }}" target="_blank">Términos y Condiciones</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Footer-Bootom-End -->
</footer>
<!-- Footer-Area-End -->
<!--Vendor-JS-->
<script src=" {{ asset('landing/js/vendor/jquery-1.12.4.min.js') }}"></script>
<script src=" {{ asset('landing/js/vendor/jquery-ui.js') }}"></script>
<script src=" {{ asset('landing/js/vendor/bootstrap.min.js') }}"></script>
<!--Plugin-JS-->
<script src=" {{ asset('landing/js/owl.carousel.min.js')}}"></script>
<script src=" {{ asset('landing/js/contact-form.js')}}"></script>
<script src=" {{ asset('landing/js/ajaxchimp.js')}}"></script>
<script src=" {{ asset('landing/js/scrollUp.min.js')}}"></script>
<script src=" {{ asset('landing/js/magnific-popup.min.js')}}"></script>
<script src=" {{ asset('landing/js/wow.min.js')}}"></script>
<!--Main-active-JS-->
<script src=" {{ asset('landing/js/main.js')}}"></script>
<script src=" {{ asset('landing/js/custom-smart-phone.js')}}"></script>
<script src=" {{ asset('landing/js/slider.js')}}"></script>
</body>

</html>

<style scoped>

.h1{
    color: #0f74a8;
    text-align: center;

}
</style>