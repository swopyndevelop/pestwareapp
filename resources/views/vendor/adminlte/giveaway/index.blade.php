<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="{{ asset('/img/pestware-cuadre.png') }}">

    <title>Giveaway</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/giveaway/bootstrap.min.css') }}">
    <!-- Icon -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
           integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/giveaway/slicknav.css') }}">
    <!-- Nivo Lightbox -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/giveaway/nivo-lightbox.css') }}">
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/giveaway/animate.css') }}">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/giveaway/main.css') }}">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/giveaway/responsive.css') }}">

</head>
<body>

<!-- Header Area wrapper Starts -->
<header id="header-wrap">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="icon-menu"></span>
                    <span class="icon-menu"></span>
                    <span class="icon-menu"></span>
                </button>
                <a href="#" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>
            </div>
            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="navbar-nav mr-auto w-100 justify-content-end">
                    <li class="nav-item active">
                        <a class="nav-link" href="#header-wrap">
                            Home
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#about">
                            About
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#faq">
                            Faq
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#contact-map">
                            Contact
                        </a>
                    </li>

                </ul>
            </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">
            <li>
                <a class="page-scrool" href="#header-wrap">Home</a>
            </li>
            <li>
                <a class="page-scrool" href="#about">About</a>
            </li>

            <li>
                <a class="page-scroll" href="#faq">Faq</a>
            </li>

            <li>
                <a class="page-scrool" href="#contact-map">Contact</a>
            </li>
        </ul>
        <!-- Mobile Menu End -->

    </nav>
    <!-- Navbar End -->

    <!-- Main Carousel Section Start -->
    <div id="main-slide" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#main-slide" data-slide-to="0" class="active"></li>
            <li data-target="#main-slide" data-slide-to="1"></li>
            <li data-target="#main-slide" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{ $bannerOne }}" alt="First slide">
                <div class="carousel-caption d-md-block">
                    <p class="fadeInUp wow" data-wow-delay=".6s">Global Grand Event on Digital Design</p>
                    <h1 class="wow fadeInDown heading" data-wow-delay=".4s">Design Thinking Conference</h1>
                    <a href="#" class="fadeInLeft wow btn btn-common btn-lg" data-wow-delay=".6s">Get Ticket</a>
                    <a href="#" class="fadeInRight wow btn btn-border btn-lg" data-wow-delay=".6s">Explore More</a>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ $bannerTwo }}" alt="Second slide">
                <div class="carousel-caption d-md-block">
                    <p class="fadeInUp wow" data-wow-delay=".6s">Global Grand Event on Digital Design</p>
                    <h1 class="wow bounceIn heading" data-wow-delay=".7s">22 Amazing Speakers</h1>
                    <a href="#" class="fadeInUp wow btn btn-border btn-lg" data-wow-delay=".8s">Learn More</a>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('img/item-02.png') }}" alt="Third slide">
                <div class="carousel-caption d-md-block">
                    <p class="fadeInUp wow" data-wow-delay=".6s">Global Grand Event on Digital Design</p>
                    <h1 class="wow fadeInUp heading" data-wow-delay=".6s">Book Your Seat Now!</h1>
                    <a href="#" class="fadeInUp wow btn btn-common btn-lg" data-wow-delay=".8s">Explore</a>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#main-slide" role="button" data-slide="prev">
            <span class="carousel-control" aria-hidden="true"><i class="fa fa-arrow-left"></i></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#main-slide" role="button" data-slide="next">
            <span class="carousel-control" aria-hidden="true"><i class="fa fa-arrow-right"></i></span>
            <span class="sr-only">Siguiente</span>
        </a>
    </div>
    <!-- Main Carousel Section End -->

</header>
<!-- Header Area wrapper End -->

<!-- About Section Start -->
<section id="about" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title-header text-center">
                    <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">About This Events</h1>
                    <p class="wow fadeInDown" data-wow-delay="0.2s">Global Grand Event on Digital Design</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="about-item">
                    <img class="img-fluid" src="assets/img/about/img1.jpg" alt="">
                    <div class="about-text">
                        <h3><a href="#">Wanna Know Our Mission?</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer commodo ligula eget dolor.</p>
                        <a class="btn btn-common btn-rm" href="#">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="about-item">
                    <img class="img-fluid" src="assets/img/about/img2.jpg" alt="">
                    <div class="about-text">
                        <h3><a href="#">What you will learn?</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer commodo ligula eget dolor.</p>
                        <a class="btn btn-common btn-rm" href="#">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="about-item">
                    <img class="img-fluid" src="assets/img/about/img3.jpg" alt="">
                    <div class="about-text">
                        <h3><a href="#">What are the benifits?</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer commodo ligula eget dolor.</p>
                        <a class="btn btn-common btn-rm" href="#">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Section End -->


<!-- Ask Question Section Start -->
<section id="faq" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title-header text-center">
                    <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Ask Question?</h1>
                    <p class="wow fadeInDown" data-wow-delay="0.2s">Global Grand Event on Digital Design</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                <div class="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <div class="header-title" data-toggle="collapse" data-target="#questionOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="lni-pencil"></i> How to make a new event?
                            </div>
                        </div>
                        <div id="questionOne" class="collapse" aria-labelledby="headingOne" data-parent="#question">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <div class="header-title" data-toggle="collapse" data-target="#questionTwo" aria-expanded="false" aria-controls="questionTwo">
                                <i class="lni-pencil"></i>  Which payment methods do you accept?
                            </div>
                        </div>
                        <div id="questionTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#question">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <div class="header-title" data-toggle="collapse" data-target="#questionThree" aria-expanded="false" aria-controls="questionThree">
                                <i class="lni-pencil"></i>  Which document can i bring to meeting?
                            </div>
                        </div>
                        <div id="questionThree" class="collapse" aria-labelledby="headingThree" data-parent="#question">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <div class="header-title" data-toggle="collapse" data-target="#questionFour" aria-expanded="false" aria-controls="questionFour">
                                <i class="lni-pencil"></i> Who can join at the live event venue?
                            </div>
                        </div>
                        <div id="questionFour" class="collapse" aria-labelledby="headingThree" data-parent="#question">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                <div class="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne2">
                            <div class="header-title" data-toggle="collapse" data-target="#questionOne2" aria-expanded="true" aria-controls="collapseOne">
                                <i class="lni-pencil"></i> How to make a new event?
                            </div>
                        </div>
                        <div id="questionOne2" class="collapse" aria-labelledby="headingOne" data-parent="#question">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo2">
                            <div class="header-title" data-toggle="collapse" data-target="#questionTwo2" aria-expanded="false" aria-controls="questionTwo">
                                <i class="lni-pencil"></i>  Which payment methods do you accept?
                            </div>
                        </div>
                        <div id="questionTwo2" class="collapse" aria-labelledby="headingTwo" data-parent="#question">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <div class="header-title" data-toggle="collapse" data-target="#questionFive" aria-expanded="false" aria-controls="questionFive">
                                <i class="lni-pencil"></i>How to set price?
                            </div>
                        </div>
                        <div id="questionFive" class="collapse" aria-labelledby="headingThree" data-parent="#question">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <div class="header-title" data-toggle="collapse" data-target="#questionSix" aria-expanded="false" aria-controls="questionSix">
                                <i class="lni-pencil"></i>  What our price list?
                            </div>
                        </div>
                        <div id="questionSix" class="collapse" aria-labelledby="headingThree" data-parent="#question">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Ask Question Section End -->

<!-- Blog Section Start -->
<section id="blog" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title-header text-center">
                    <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Our Latest News & Articles</h1>
                    <p class="wow fadeInDown" data-wow-delay="0.2s">Global Grand Event on Digital Design</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="blog-item">
                    <div class="blog-image">
                        <a href="#">
                            <img class="img-fluid" src="assets/img/blog/img-1.jpg" alt="">
                        </a>
                    </div>
                    <div class="descr">
                        <div class="tag">Design</div>
                        <h3 class="title">
                            <a href="single-blog.html">
                                The 9 Design Trends You Need to Know
                            </a>
                        </h3>
                        <div class="meta-tags">
                            <span class="date">Jan 20, 2018</span>
                            <span class="comments">| <a href="#"> by Cindy Jefferson</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="blog-item">
                    <div class="blog-image">
                        <a href="#">
                            <img class="img-fluid" src="assets/img/blog/img-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="descr">
                        <div class="tag">Design</div>
                        <h3 class="title">
                            <a href="single-blog.html">
                                The 9 Design Trends You Need to Know
                            </a>
                        </h3>
                        <div class="meta-tags">
                            <span class="date">Jan 20, 2018 </span>
                            <span class="comments">| <a href="#"> by Cindy Jefferson</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="blog-item">
                    <div class="blog-image">
                        <a href="#">
                            <img class="img-fluid" src="assets/img/blog/img-3.jpg" alt="">
                        </a>
                    </div>
                    <div class="descr">
                        <div class="tag">Design</div>
                        <h3 class="title">
                            <a href="single-blog.html">
                                The 9 Design Trends You Need to Know
                            </a>
                        </h3>
                        <div class="meta-tags">
                            <span class="date">Jan 20, 2018</span>
                            <span class="comments">| <a href="#"> by Cindy Jefferson</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center">
                <a href="#" class="btn btn-common">Read More News</a>
            </div>
        </div>
    </div>
</section>
<!-- Blog Section End -->

<!-- Subscribe Area Start -->
<div id="subscribe" class="section-padding">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-10 col-lg-7">
                <div class="subscribe-inner wow fadeInDown" data-wow-delay="0.3s">
                    <h2 class="subscribe-title">Sign Up For Our Newsletter</h2>
                    <form class="text-center form-inline">
                        <input class="mb-20 form-control" name="email" placeholder="Enter Your Email Here">
                        <button type="submit" class="btn btn-common sub-btn" data-style="zoom-in" data-spinner-size="30" name="submit" id="submit">
                            <span class="ladda-label"><i class="lni-check-box"></i> Submit</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Subscribe Area End -->

<!-- Contact Us Section -->
<section id="contact-map" class="section-padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title-header text-center">
                    <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Drop A Message</h1>
                    <p class="wow fadeInDown" data-wow-delay="0.2s">Global Grand Event on Digital Design</p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="container-form wow fadeInLeft" data-wow-delay="0.2s">
                    <div class="form-wrapper">
                        <form role="form" method="post" id="contactForm" name="contact-form" data-toggle="validator">
                            <div class="row">
                                <div class="col-md-6 form-line">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="email" placeholder="First Name" required data-error="Please enter your name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 form-line">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required data-error="Please enter your Email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-line">
                                    <div class="form-group">
                                        <input type="tel" class="form-control" id="msg_subject" name="subject" placeholder="Subject" required data-error="Please enter your message subject">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" id="message" name="message" required data-error="Write your message"></textarea>
                                    </div>
                                    <div class="form-submit">
                                        <button type="submit" class="btn btn-common" id="form-submit"><i class="fa fa-paper-plane" aria-hidden="true"></i>  Send Us Now</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Us Section End -->

<!-- Footer Section Start -->
<footer class="footer-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
                <h3><img src="assets/img/logo.png" alt=""></h3>
                <p>
                    Aorem ipsum dolor sit amet elit sed lum tempor incididunt ut labore el dolore alg minim veniam quis nostrud ncididunt.
                </p>
            </div>
            <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <h3>QUICK LINKS</h3>
                <ul>
                    <li><a href="#">About Conference</a></li>
                    <li><a href="#">Our Speakers</a></li>
                    <li><a href="#">Event Shedule</a></li>
                    <li><a href="#">Latest News</a></li>
                    <li><a href="#">Event Photo Gallery</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                <h3>RECENT POSTS</h3>
                <ul class="image-list">
                    <li>
                        <figure class="overlay">
                            <img class="img-fluid" src="assets/img/art/a1.jpg" alt="">
                        </figure>
                        <div class="post-content">
                            <h6 class="post-title"> <a href="blog-single.html">Lorem ipsm dolor sumit.</a> </h6>
                            <div class="meta"><span class="date">October 12, 2018</span></div>
                        </div>
                    </li>
                    <li>
                        <figure class="overlay">
                            <img class="img-fluid" src="assets/img/art/a2.jpg" alt="">
                        </figure>
                        <div class="post-content">
                            <h6 class="post-title"><a href="blog-single.html">Lorem ipsm dolor sumit.</a></h6>
                            <div class="meta"><span class="date">October 12, 2018</span></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
                <h3>SUBSCRIBE US</h3>
                <div class="widget">
                    <div class="newsletter-wrapper">
                        <form method="post" id="subscribe-form" name="subscribe-form" class="validate">
                            <div class="form-group is-empty">
                                <input type="email" value="" name="Email" class="form-control" id="EMAIL" placeholder="Your email" required="">
                                <button type="submit" name="subscribe" id="subscribes" class="btn btn-common sub-btn"><i class="fa fa-send"></i></button>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.widget -->
                <div class="widget">
                    <h5 class="widget-title">FOLLOW US ON</h5>
                    <ul class="footer-social">
                        <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->

<div id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="site-info">
                    <p>© Diseñada y Desarrollada <a href="#" rel="nofollow">Swopyn</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Go to Top Link -->
<a href="#" class="back-to-top">
    <i class="lni-chevron-up"></i>
</a>

<!--<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>-->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="{{ URL::asset('js/build/giveaway.js') }}"></script>

</body>
</html>
