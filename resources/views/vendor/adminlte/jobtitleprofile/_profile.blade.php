<div class="form-horizontal panel-body" id="_profile">
	<div class="principal-container">
		<div class="col-lg-6">
			<label>
				Puesto <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_puestonombre"></span>
				<input type="text" class="form-control" placeholder="Supervisor" name="Nombre_Puesto" id="Nombre_Puesto"  value="{{ $jobprofile->job_title_name or old('Nombre_Puesto') }}" disabled="disabled">
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Tipo de Función <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_funcionpuesto"></span>
				<select name="Funcion_Puesto" id="Funcion_Puesto" class="form-control" data-live-search="true">
					<option value="">Seleccione uno</option>
					@foreach($interviews as $interviews)
					<option value="{{$interviews->id}}" @if ($interviews->id == $jobprofile->function_type) selected='selected' @endif>{{$interviews->name}}</option>
					@endforeach
				</select>
			</label>
		</div>
		<div class="col-lg-6" style="height: 7.5em;">
			<label>
				Objetivo del Puesto <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_objetivopuesto"></span>
				<textarea name="Objetivo_Puesto" id="Objetivo_Puesto" class="form-control">{{ $jobprofile->job_title_objetive or old('Objetivo_Puesto') }}</textarea>
			</label>
		</div>
		<div class="col-lg-6" style="height: 7.5em;">
			<label>
				Rangos de salario<span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_salariomaximo"></span><span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_salariominimo"></span><br>
				<div class="form-group" style=" padding-top: 1em">
					<label class="control-label col-md-2" style="color: #c1003d">Mínimo</label>
					<div class="col-md-4">
						<input type="number" class="form-control" name="SalarioMin" id="SalarioMin" min="100" step="1" value="{{ $jobprofile->min_salary or old('SalarioMin') }}" style="border: none; outline: none; background: transparent; font-size: 1.3em; color: #000;">
					</div>
					<label class="control-label col-md-2" style="color: #c1003d">Máximo</label>
					<div class="col-md-4">
						<input type="number" class="form-control" name="SalarioMax" id="SalarioMax" min="100" step="1" value="{{ $jobprofile->max_salary or old('SalarioMax') }}" style="border: none; outline: none; background: transparent; font-size: 1.3em; color: #000;">
					</div>
				</div>
			</label>
		</div>
		<div class="form-group text-center col-xs-12" style="margin-top: 2em">
		<button type="button" class="btn btn-primary" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="guardar_perfil">Siguiente</button>
	</div>
	</div>
</div>