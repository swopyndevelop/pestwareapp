@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('css-personal')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
	.principal-container
	{
		width: 100%;
	}

	.principal-container > div > label
	{
		text-align: left;
		display: block;
		padding: 1.1em 1.1em .6em;
		font-size: 1em;
		color: #f5216e;
		cursor: pointer;
	}
	.principal-container > [class^='col-'] { 
		border: 1px solid #E4E4E4; 
		height: 6em;
	}  
	.ok, .principal-container > div > label > input, .principal-container > div > label > select, .principal-container > div > label > center > input
	{
		display: inline-block;
		position: relative;
		width: 100%;
		margin: 5px -5px 0;
		padding: 7px 5px 3px;
		border: none;
		outline: none;
		background: transparent;
		font-size: 1.3em;
		color: #000;
	}
	.principal-container > div > label > center > input{
		width: 3.25em;
	}
	@media screen and (max-width: 480px) {
		.principal-container > div > label{
			font-size: .8em;
		}
		.ok, .principal-container > div > label > input, .principal-container > div > label > select, .principal-container > div > label > center > input{
			font-size: 1.1em;
		}	
	}
</style>
@stop

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border">
					<h2 class="titleCenter" style="color:;" >PERFIL DE PUESTO</h2>
<!-- 					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div> -->
				</div>
				<div class="box-body">
					<ul class="nav nav-tabs">
						<li data-toggle="tooltip" title="Perfil" class="active" style="width: 15%">
							<a data-toggle="tab" href="#profileTab" id="tabProfile">
								<div class="row text-center">
									<i style="color: black" class="fa fa-user-o fa-4x" aria-hidden="true"></i>
								</div>
							</a>
						</li>
						<li data-toggle="tooltip" title="Responsabilidades" style="width: 14%">
							<a data-toggle="tab" href="#responsabilitiesTab" id="tabResponsabilities">
								<div class="row text-center">
									<i style="color: black" class="fa fa-folder-open fa-4x" aria-hidden="true"></i>
								</div>
							</a>
						</li>
						<li data-toggle="tooltip" title="Demograficos" style="width: 14%">
							<a data-toggle="tab" href="#demograficosTab" id="Tabdemograficos">
								<div class="row text-center">
									<i style="color: black" class="fa fa-address-card fa-4x" aria-hidden="true"></i>
								</div>
							</a>
						</li>
						<li data-toggle="tooltip" title="Conocimientos" style="width: 14%">
							<a data-toggle="tab" href="#conocimientosTab" id="Tabconocimientos">
								<div class="row text-center">
									<i style="color: black" class="fa fa-briefcase fa-4x" aria-hidden="true"></i>
								</div>
							</a>
						</li>
						<li data-toggle="tooltip" title="Entrevista" style="width: 14%">
							<a data-toggle="tab" href="#interviewTab" id="Tabinterview">
								<div class="row text-center">
									<i style="color: black" class="fa fa-microphone fa-4x" aria-hidden="true"></i>
								</div>
							</a>
						</li>
						<li data-toggle="tooltip" title="Documentos" style="width: 14%">
							<a data-toggle="tab" href="#documentosTab" id="Tabdocuments">
								<div class="row text-center">
									<i style="color: black" class="fa fa-file-text fa-4x" aria-hidden="true"></i>
								</div>
							</a>
						</li>
						<li data-toggle="tooltip" title="Contrato" style="width: 15%">
							<a data-toggle="tab" href="#contratoTab" id="Tabcontract">
								<div class="row text-center">
									<i style="color: black" class="fa fa-pencil-square-o fa-4x" aria-hidden="true"></i>
								</div>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<input type="hidden" id="id_profile" name="id_profile" value="{{ $jobprofile->job_title_profiles_id}}">
						<div class="tab-pane fade in active" id="profileTab">
							@include('adminlte::jobtitleprofile._profile')
						</div>
						<div class="tab-pane fade" id="responsabilitiesTab">
							@include('adminlte::jobtitleprofile._responsabilities')
						</div>
						<div class="tab-pane fade" id="demograficosTab">
							@include('adminlte::jobtitleprofile._demographic')
						</div>
						<div class="tab-pane fade" id="conocimientosTab">
							@include('adminlte::jobtitleprofile._knowledges')
						</div>
						<div class="tab-pane fade" id="interviewTab">
							@include('adminlte::jobtitleprofile._interview')
						</div>
						<div class="tab-pane fade" id="documentosTab">
							@include('adminlte::jobtitleprofile._documents')
						</div>
						<div class="tab-pane fade" id="contratoTab">
							@include('adminlte::jobtitleprofile._contract')
						</div>
					</div>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>
@endsection