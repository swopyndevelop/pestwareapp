<div class="form-horizontal panel-body" id="tab_interview">
	<div class="principal-container">
		<div class="col-md-8 col-md-offset-2">
			<label>
				Entrevista <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_entrevistapuesto"></span>
				<select name="EntrevistaPuesto" id="EntrevistaPuesto" class="form-control">
					<option value="">Seleccionar el modulo de entrevistas.</option>
					@foreach($interviews as $interviews)
					<option value="{{$interviews->id}}" @if ($interviews->id == $jobprofile->interview_type) selected='selected' @endif>Entrevista {{$interviews->name}}</option>
					@endforeach
				</select>
			</label>
		</div>
		<div class="form-group text-right col-xs-12" style="margin-top: 2em">
			<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="guardar_entrevista">Siguiente</button>
		</div>
	</div>
</div>