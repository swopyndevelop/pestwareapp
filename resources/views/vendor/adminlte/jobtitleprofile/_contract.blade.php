<div class="form-horizontal panel-body">
	<div class="principal-container">
		<b><p class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_contratopuesto"></p></b>
		<div class="form-group">
			<div class="col-md-8 col-xs-12 col-md-offset-2">
				<table class="table table-striped" style="margin-bottom: 0px;">
					<thead>
						<tr>
							<th class="text-center" style="color: #f5216e;">TIPO DE CONTRATO</th>
							<th class="text-center" style="color: #f5216e;">¿ES REQUERIDO?</th>
						</tr>
					</thead>
					<tbody class="text-center" id="tablaDocumentos">
						@foreach($contracts as $contract)
						<tr>
							<td class="textoContrato">{{ $contract -> contract_name }}</td>
							<td class="text-center">
								<input type="checkbox" class="contratosjobs" name="contratosjobs" value="{{ $contract -> id }}" @foreach($con as $cont) @if($cont->contract_types_id == $contract->id) checked="checked" @endif @endforeach>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<br>
		<div class="row" aling="center">
			<button type="button" class="btn btn-success" style="background-color: black; border-color: black;" id="guardar_contrato_jobtitle" style="margin-right: 2%; width:12em; height:3em">Finalizar Solicitud</button>
		</div>
	</div>
</div>
