<div class="form-horizontal panel-body" id="tab_responsabilities">
	<div class="principal-container">
		<div class="col-md-10 col-md-offset-1">
			<label>
				RESPONSABILIDADES <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_responsabilidadespuesto"></span>
				<div class="form-group">
					<div class="col-md-10">
						<input type="text" class="form-control" id="inputResponsabilidades" name="inputResponsabilidades" style="outline: none; background: transparent; font-size: 1.3em; color: #000;">
					</div>
					<div class="col-md-2">
						<button type="button" class="btn btn-warning form-control" id="add_Responsabilidades" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;">
							<span class="glyphicon glyphicon-plus-sign"></span>
						</button>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto;">
			<table class="table table-striped" style="margin-top: 1em;">
				<tbody id="tablaResponsabilidades">
					@foreach ($res as $res)
					<tr>
						<td>{{ $res->responsability_name }}</td>
						<td><button class="btn btn-sm btn-circle delete_res" data-info="{{ $res->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-10 col-md-offset-1" style=" margin-top: 3em;">
			<label>
				FUNCIONES <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_funcionespuesto"></span>
				<div class="form-group">
					<div class="col-md-10">
						<input type="text" class="form-control" id="inputFunciones" name="inputFunciones" style="outline: none; background: transparent; font-size: 1.3em; color: #000;">
					</div>
					<div class="col-md-2">
						<button type="button" class="btn btn-warning form-control" id="add_function" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;">
							<span class="glyphicon glyphicon-plus-sign"></span>
						</button>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto;">
			<table class="table table-striped" style="margin-top: 1em;">
				<tbody id="tablaFunciones">
					@foreach ($fun as $fun)
					<tr>
						<td>{{ $fun->function_name }}</td>
						<td><button class="btn btn-sm btn-circle delete_fun" data-info="{{ $fun->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-10 col-md-offset-1" style=" margin-top: 3em;">
			<label>
				ACTITUDES <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_actitudespuesto"></span>
				<div class="form-group">
					<div class="col-md-10">
						<input type="text" class="form-control" id="inputActitudes" name="inputActitudes" style="outline: none; background: transparent; font-size: 1.3em; color: #000;">
					</div>
					<div class="col-md-2">
						<button type="button" class="btn btn-warning form-control" id="add_Actitudes" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;">
							<span class="glyphicon glyphicon-plus-sign"></span>
						</button>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto;">
			<table class="table table-striped" style="margin-top: 1em;">
				<tbody id="tablaActitudes">
					@foreach ($att as $att)
					<tr>
						<td>{{ $att->attitude_name }}</td>
						<td><button class="btn btn-sm btn-circle delete_attitude" data-info="{{ $att->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash " aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="form-group text-right col-xs-12" style="margin-top: 2em">
			<button type="button" class="btn btn-primary" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="step_responsabilities">Siguiente</button>
		</div>
	</div>
</div>
@section('personal-js')
	<script>
		$('.delete_res').click(function() {
		// deleteLan();
		var Responsabilidades = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('jobtitle_responsability_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				Responsabilidades.remove();
			}
		});
	});
		$('.delete_fun').click(function() {
		// deleteLan();
		var funciones = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('jobtitle_function_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				funciones.remove();
			}
		});
	});
		$('.delete_attitude').click(function() {
		// deleteLan();
		var Actitudes = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('jobtitle_attitude_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				Actitudes.remove();
			}
		});
	});
	</script>
	@stop