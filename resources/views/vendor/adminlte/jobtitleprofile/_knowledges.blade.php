<div class="form-horizontal panel-body" id="tab_knowledges">
	<div class="principal-container">
		<div class="col-md-10 col-md-offset-1">
			<label>
				Idioma <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_idiomapuesto"></span>
				<select id="languageknow_profile" name="languageknow_profile" class="form-control" data-live-search="true">
					<option value=""> Seleccione...</option>
					@foreach ($arr as $l)
					<option value="{{ $l->language }}">{{ $l->language }}</option>
					@endforeach
				</select>
			</label>
		</div>
		<div class="col-md-5 col-md-offset-1">
			<label>
				Leer <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_languageread_profile"></span>
				<select id="languageread_profile" name="languageread_profile" data-live-search="true" class="form-control" style="color: black;">
					<option value=""> Seleccione...</option>
					@foreach ($lanlevel as $l)
					<option value="{{ $l->value }}">{{ $l->name }}</option>
					@endforeach
				</select>
			</label>
		</div>
		<div class="col-md-5">
			<label>
				Escibir <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_languagewrite_profile"></span>
				<select id="languagewrite_profile" name="languagewrite_profile" class="form-control" data-live-search="true" style="color: black;">
					<option value=""> Seleccione...</option>
					@foreach ($lanlevel as $l)
					<option value="{{ $l->value }}">{{ $l->name }}</option>
					@endforeach
				</select>
			</label>
		</div>
		<div class="col-md-5 col-md-offset-1">
			<label>
				Escuchar <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_languagelisten_profile"></span>
				<select id="languagelisten_profile" name="languagelisten_profile" class="form-control" data-live-search="true" style="color: black;">
					<option value=""> Seleccione...</option>
					@foreach ($lanlevel as $l)
					<option value="{{ $l->value }}">{{ $l->name }}</option>
					@endforeach
				</select>	
			</label>
		</div>
		<div class="col-md-5">
			<label>
				Hablar <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_languagespeak_profile"></span>
				<select id="languagespeak_profile" name="languagespeak_profile" class="form-control" data-live-search="true" style="color: black;">
					<option value=""> Seleccione...</option>
					@foreach ($lanlevel as $l)
					<option value="{{ $l->value }}">{{ $l->name }}</option>
					@endforeach
				</select>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto; border: none;">
			<label>
				<div class="text-center">
					<button type="button" class="btn btn-warning form-control" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;" id="add_language_profile">
						<span class="glyphicon glyphicon-plus-sign"></span>
					</button>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto;">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>
							Idioma
						</th>
						<th colspan="2" class="text-center">
							Nivel
						</th>
					</tr>
				</thead>
				<tbody id="tablaLenguajes_profile" style="width: 100%" >
					@foreach ($lan as $lan)
					<tr>
						<td class="col-md-6">{{ $lan->language }}</td>
						<td class="col-md-6 text-center">{{ $lan->level }}</td>
						<td class="col-md-6 text-center">
							<button  class="btn btn-danger form-control text-center delete_language_profile" data-info="{{ $lan->id }}"><span class="fa fa-trash"></span></button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-10 col-md-offset-1" style="margin-top: 2em">
			<label>
				Funciones de Oficina <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_funciones"></span>
				<div class="form-group">
					<div class="col-md-10">
						<input type="text" placeholder="Funciones Específicas" class="form-control" id="Oficina" name="Oficina" style="border: none; outline: none; background: transparent; font-size: 1.3em; color: #000; margin: 5px -5px 0; padding: 7px 5px 3px;">
					</div>
					<div class="col-md-2">
						<button type="button" class="btn btn-warning form-control" id="add_Oficina" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;">
							<span class="glyphicon glyphicon-plus-sign"></span>
						</button>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto; margin-top: .5em">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>
							Función
						</th>
					</tr>
				</thead>
				<tbody id="tablaOficina">
					@foreach ($funoff as $funoff)
					<tr>
						<td>{{ $funoff->function }}</td>
						<td class="text-center"><button class="btn btn-sm btn-circle delete_function" data-info="{{ $funoff->id }}" style="background: none; height: auto; width: 15px; padding-left: 0.8em" >
								<i class="fa fa-trash " aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-10 col-md-offset-1" style="margin-top: 2em">
			<label>
				Máquinas <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_maquinaspuesto"></span>
				<div class="form-group">
					<div class="col-md-10">
						<input type="text" placeholder="Maquinaría que se domina" class="form-control" id="MaquinasPuesto" name="MaquinasPuesto" style="border: none; outline: none; background: transparent; font-size: 1.3em; color: #000; margin: 5px -5px 0; padding: 7px 5px 3px;">
					</div>
					<div class="col-md-2">
						<button type="button" class="btn btn-warning form-control" id="add_Maquinas" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;">
							<span class="glyphicon glyphicon-plus-sign"></span>
						</button>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto; margin-top: .5em">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>
							Máquinas
						</th>
					</tr>
				</thead>
				<tbody id="tablaMaquinas">
					@foreach ($mac as $mac)
					<tr>
						<td>{{ $mac->machine_name }}</td>
						<td class="text-center"><button class="btn btn-sm btn-circle delete_mac" data-info="{{ $mac->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash " aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-10 col-md-offset-1" style="margin-top: 2em">
			<label>
				Software <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_softwarePuesto"></span>
				<div class="form-group">
					<div class="col-md-10">
						<input type="text" placeholder="Software que se domina" class="form-control" id="SoftwarePuesto" name="SoftwarePuesto" style="border: none; outline: none; background: transparent; font-size: 1.3em; color: #000; margin: 5px -5px 0; padding: 7px 5px 3px;">
					</div>
					<div class="col-md-2">
						<button type="button" class="btn btn-warning form-control" id="add_Software" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;">
							<span class="glyphicon glyphicon-plus-sign"></span>
						</button>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto; margin-top: .5em">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>
							Software
						</th>
					</tr>
				</thead>
				<tbody id="tablaSoftware">
					@foreach ($soft as $soft)
					<tr>
						<td>{{ $soft->software_name }}</td>
						<td class="text-center"><button class="btn btn-sm btn-circle delete_soft" data-info="{{ $soft->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash " aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<div class="form-group text-right col-xs-12" style="margin-top: 2em">
			<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="step_knowledges">Siguiente</button>
		</div>
	</div>
</div>