<div class="form-horizontal panel-body" id="tab_documents">
	<div class="form-group">
		<div class="col-md-8 col-md-offset-2">
			<table class="table table-striped" style="margin-bottom: 0px;">
				<thead>
					<tr>
						<th class="text-center" style="color: #f5216e;">DOCUMENTOS</th>
						<th class="text-center" style="color: #f5216e;">¿ES REQUERIDO?</th>
					</tr>
				</thead>
				<tbody class="text-left" id="tablaDocumentos">
					@foreach ($docs as $docs)
					<tr>
						<td class="textoDocumento">{{ $docs->text }}</td>
						<td class="text-center" name="acta_de_nacimiento">
							<input type="checkbox" class="verificadorDocumentos" value="{{ $docs->text }}" @foreach($doc as $d) @if($docs->text == $d->document_name) checked = "checked" @endif @endforeach>						
						</td>
					</tr>
					@endforeach

					@foreach ($extraDoc as $eD)
						<tr>
						<td class="textoDocumento">{{ $eD }}</td>
						<td class="text-center" name="acta_de_nacimiento">
							<input type="checkbox" class="verificadorDocumentos" value="{{ $eD }}" checked>						
						</td>
					</tr>
					@endforeach
					<tr>
						<td class="textoDocumento">  
							<input type="text" class="form-control" name="OtroDocumento" id="OtroDocumento" placeholder="Otro documento" style="border: none; outline: none; background: transparent; font-size: 1.3em; color: #000; margin: 5px -5px 0; padding: 7px 5px 3px;"> 
						</td> 
						<td class="text-center"> 
							<button type="button" class="btn btn-warning form-control" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;" id="add_documento"> 
								<span class="glyphicon glyphicon-plus-sign"></span> 
							</button> 
						</td> 
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="form-group" align="right">
		<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="guardarDocumentos">Siguiente</button>
	</div>
</div>