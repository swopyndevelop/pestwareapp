<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="PestWare App">
    <meta name="description" content="Software para Empresas de Control de Plagas">
    <meta name="keywords" content="Software,Plagas,Fumigaciones,Monitoreo de Estaciones">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>PestWare App | Software para Empresas de Control de Plagas </title>

    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{ asset('landing/images/pestware-cuadre.png') }}">
    <link rel="shortcut icon" type="image/ico" href="{{asset('landing/images/pestware-cuadre.png')}}">
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{ asset('landing/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href=" {{ asset('landing/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/css/linearicons.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/css/magnific-popup.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/css/animate.css') }}">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href=" {{ asset('landing/css/normalize.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/style.css') }}">
    <link rel="stylesheet" href=" {{ asset('landing/css/responsive.css') }} ">
    <script src=" {{ asset('landing/js/vendor/modernizr-2.8.3.min.js') }}"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!--     Smartsupp Live Chat script -->
    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '3a846447921143867cd57012b906e928e6b08e45';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
    </script>
</head>

<body data-spy="scroll" data-target=".mainmenu-area">

<div class="preloader">
    <span><i class="fas fa-bug"></i></span>
</div>
<!-- MainMenu-Area -->
<nav class="mainmenu-area" data-spy="affix" data-offset-top="200">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary_menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    Llámanos al <span><a href="tel:+52 4494139091">+52 4494139091</a></span> - <span><a href="mailto:contacto@pestwareapp.com">contacto@pestwareapp.com</a></span>
                </div>
                <div class="col text-center">
                    <a target="_blank" href="{{ route('login_new','register') }}">
                        <button class="btn btn-md btn-outline-info">Pruebalo Gratis</button>
                    </a>
                </div>
                <div class="col text-center">
                    <a href=" {{ route('login_new','login') }}">
                        <img src="landing/images/pestware-cuadre.png" alt="" width="24"> <span class="h5"> Inicia Sesión</span>
                    </a>
                </div>
            </div>
        </div>
        <hr>
    </div>
</nav>
<!-- MainMenu-Area-End -->
<br><br><br><br><br>
<!-- Blog-Area-->

<div id="blog">
    <center>
        <header class="header--type2">
            <div class="inner" style="-webkit-user-select: auto;">
                <div class="container" style="-webkit-user-select: auto;">
                    <a href="/"><i class="fa fa-home fa-2x btn-info" aria-hidden="true"></i></a>
                    <section class="ct-page_title" style="-webkit-user-select: auto;">
                        <div class="page-title "><h2 class="title">Blog</h2></div>
                    </section>
                </div>
            </div>
        </header>
    </center>
    <br>
    <div class="container-fluid">

        @if($varpost == true)
            <div class="row">
                <div class="offset-md-1 offset-xs-1 col-xs-11 col-md-10">
                        <div class="price-box card mb-3">
                            <div class="price-header">
                                <h3 class="uppercase">No hay Artículos</h3>
                        </div>
                </div>
            </div>
        @else

        @foreach($posts['items'] as $post)
        <div class="row">

                <div class="offset-md-1 offset-xs-1 col-xs-11 col-md-10">
                    <a href="blog/post/{{$post['id']}}" target="_blank">
                        <div class="price-box card mb-3">
                            <div class="price-header text-left">
                                <h3 class="">{{$post['title']}}</h3>
                            </div>
                            <div class="price-body">
                                <div class="col-md-12 text-left">
                                    <span class="btn-outline-info badge" style="font-size: small">Autor: </span>
                                    <span>{{ $author }}</span>
                                    <span class="btn-outline-info badge" style="font-size: small">Publicado: </span>
                                    <span> {{ \Carbon\Carbon::parse($post['published'])->format('d/m/Y') }} </span>
                                    <span class="btn-outline-info badge" style="font-size: small">Actualizado: </span>
                                    <span>{{ \Carbon\Carbon::parse($post['updated'])->format('d/m/Y') }} </span>
                                </div>
                                <div class="col-md-6 text-right">
                                </div>
                            </div>
                            <div class="price-footer">
                            </div>
                        </div>
                    </a>
                </div>

        </div>
        @endforeach
        @endif
        </div>
    </div>
<!--Blog-Area-End-->

<!-- Footer-Area -->
<footer class="footer-area" id="contact_page">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title text-center">
                        <h3 class="title">Contáctanos</h3>
                        <h3 class="dark-color">Por medio de...</h3>
                        <div class="space-60"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <div class="box-icon">
                            <span class="lnr lnr-map-marker"></span>
                        </div>
                        <p>México</p>
                    </div>
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <a target="_blank" href="https://api.whatsapp.com/send/?phone=524494139091&text=Hola,%20quiero%20m%C3%A1s%20informaci%C3%B3n">
                            <div class="box-icon">
                                <span class="fab fa-whatsapp"></span>
                            </div>
                            <p>+52 4494139091</p>
                        </a>
                    </div>
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <a target="_blank" href="mailto:contacto@pestwareapp.com">
                            <div class="box-icon">
                                <span class="lnr lnr-envelope"></span>
                            </div>
                            <p>contacto@pestwareapp.com</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Footer-Bootom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-5">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <a href="https://pestwareapp.com" target="_blank"> <span>Copyright &copy;<script>document.write(new Date().getFullYear());</script> | powered by PestwareApp | <span class="font-weight-bold">Developed Swopyn</span></span> </a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-md-7">
                    <div class="footer-menu">
                        <ul>
                            <li><a href="#about_page">Acerca De</a></li>
                            <li><a href="#progress_page">Funcionalidades</a></li>
                            <!--                                <li><a href="#">Features</a></li>-->
                            <li><a href="#price_page">Precios</a></li>
                            <!--                                <li><a href="#">Testimonial</a></li>-->
                            <li><a href="#contact_page">Contacto</a></li>
                            <li><a href="{{ route('terms_conditions') }}" target="_blank">Términos y Condiciones</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Footer-Bootom-End -->
</footer>
<!-- Footer-Area-End -->
<!--Vendor-JS-->
<script src=" {{ asset('landing/js/vendor/jquery-1.12.4.min.js') }}"></script>
<script src=" {{ asset('landing/js/vendor/jquery-ui.js') }}"></script>
<script src=" {{ asset('landing/js/vendor/bootstrap.min.js') }}"></script>
<!--Plugin-JS-->
<script src=" {{ asset('landing/js/owl.carousel.min.js')}}"></script>
<script src=" {{ asset('landing/js/contact-form.js')}}"></script>
<script src=" {{ asset('landing/js/ajaxchimp.js')}}"></script>
<script src=" {{ asset('landing/js/scrollUp.min.js')}}"></script>
<script src=" {{ asset('landing/js/magnific-popup.min.js')}}"></script>
<script src=" {{ asset('landing/js/wow.min.js')}}"></script>
<!--Main-active-JS-->
<script src=" {{ asset('landing/js/main.js')}}"></script>
<script src=" {{ asset('landing/js/custom-smart-phone.js')}}"></script>
<script src=" {{ asset('landing/js/slider.js')}}"></script>
</body>

</html>
