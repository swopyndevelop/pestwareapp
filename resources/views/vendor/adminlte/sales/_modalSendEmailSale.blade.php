<!-- INICIA MODAL PARA ENVIAR EMAIL VENTA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="sendMailSale" role="dialog" aria-labelledby="sendMailSale">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Enviar Venta</h4>
                            </div>

                            <div class="box-body text-center">

                                <div class="formulario__grupo col-xs-6 col-xs-offset-3" id="grupo__emailCustomerQuot">
                                    <div>
                                        <span style="font-weight: bold;">Correo del cliente:</span>
                                        <input type="email" class="formulario__input form-control" name="emailSale"
                                               id="emailSale">
                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                    </div>
                                    <p class="formulario__input-error">Los datos ingresados deben ser un correo electrónico.</p>
                                </div>

                                <div class="formulario__grupo col-xs-6 col-xs-offset-3" id="grupo__emailQuot">
                                    <div>
                                        <br>
                                        <span style="font-weight: bold;">Otro correo electrónico:</span>
                                        <input type="email" class="formulario__input form-control" name="emailSaleOther"
                                               id="emailSaleOther">
                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                    </div>
                                    <p class="formulario__input-error">Los datos ingresados deben ser un correo electrónico.</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary" name="sendEmailSaleButton" id="sendEmailSaleButton">Envíar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL VENTA -->