<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Nota de Venta</title>
</head>
<style>
    @page {
        margin: 0;
    }

    body {
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
    }

    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }

    caption, td, th {
        padding: 0.3em;
    }

    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }

    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
{{--//headers--}}
<div style="margin: 0px;">
    <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

        <div style="text-align: center; margin-bottom: 0px; margin-top: 20px;">
            <h2 style="font-weight: bold; margin-bottom: 0px">NOTA DE VENTA</h2>
        </div>

        <div style="width: 130px; display: inline-block; vertical-align: top;">
            @if($sale->pdf_logo == null)
                <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
            @else
                <img src="{{ env('URL_STORAGE_FTP').$sale->pdf_logo }}" alt="logo" width="130px">
            @endif
        </div>

        <div style="width: 400px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top;">
            <p style="margin-top: 0px; margin-bottom: 0">
                <span style="color:black;font-size:9.5pt">
                    Razón Social: {{ $sale->business_name }} | Tel: {{$sale->whatsapp_personal}} |RFC: {{ $sale->rfc }} | {{ $addressProfile->street }} #{{ $addressProfile->num_ext }}, {{ $addressProfile->location }},
                        {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. | Email: {{ $sale->email_personal }} | Facebook: {{$sale->facebook_personal}}.
                </span>
            </p>
        </div>

        <div style="width: 200px; display: inline-block; margin-top: 0px; font-size: 8pt; margin-left: 10px; vertical-align: top; text-align: right;">
            <p style="margin-bottom: 2px; margin-top: 0px">
                <span style="font-size:8pt; font-weight: bold">NO. VENTA: </span>
                <span style="color:red;font-weight:bold;font-size:8pt;">{{$sale->folio}}</span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">FECHA: </span>
                <span style="font-size:8pt;">{{ \Carbon\Carbon::parse($sale->date)->format('d/m/Y') }}</span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">HORA: </span>
                <span style="font-size:8pt;">{{ \Carbon\Carbon::parse($sale->created_at)->format('H:i') }}</span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">Vendedor: </span>
                <span style="font-size:8pt;">@if($sale->user_name == null)<span></span>@else<span>{{$sale->user_name}}</span>@endif</span>
            </p>
        </div>

    </div>
</div>
<div style="text-align: left; ; margin: 0 20px 0 20px;">
    <p style="margin-top: 0px; margin-bottom: 7px">
        <span style="font-size:11pt; font-weight: bold"> Atención a {{$sale->customer_name }} </span><br>
        <span style="font-size:10pt"> <br>
            @if($sale->establishment_name != null) {{$sale->contact_address}} | @endif
            @if($sale->colony != null) Dirección: {{$sale->colony}} | @endif @if($sale->municipality != null) {{$sale->municipality}} | @endif
            @if($sale->cellphone != null) Tel: {{$sale->cellphone}} | @endif
            @if($sale->email != null ) Email: {{$sale->email}} | @endif
            @if($sale->rfc != null ) Email: {{$sale->rfc}} @endif
        </span>
    </p>
</div>
{{--//Body--}}
<div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; border: solid 1px">
    <p style="margin: 0px 0px 0px 5px">
        <span style="font-size:8pt; font-weight: bold">Tipo de Venta: </span>
        <span style="font-weight:bold;font-size:8pt;">{{$sale->type_sale}}</span>
    </p>
    <p style="margin: 0px 0px 0px 5px">
        <span style="font-size:8pt; font-weight: bold">Forma de Pago: </span>
        <span style="font-weight:bold;font-size:8pt;">{{$sale->payment_way_name}}</span>
    </p>
    <p style="margin: 0px 0px 0px 5px">
        <span style="font-size:8pt; font-weight: bold">Método de Pago: </span>
        <span style="font-weight:bold;font-size:8pt;">{{$sale->payment_method_name}}</span>
    </p>
    <p style="margin: 0px 0px 0px 5px">
        <span style="font-size:8pt; font-weight: bold">Tipo de Comprobante: </span>
        <span style="font-weight:bold;font-size:8pt;">{{$sale->voucher_name}}</span>
    </p>

    <p style="margin: 0px 0px 0px 5px">
        <span style="font-size:8pt; font-weight: bold">Descripción: </span>
        <span style="font-weight:bold;font-size:8pt;">{{$sale->description}}</span>
    </p>
</div>
{{--Table Concepts--}}
<div style="margin-left: 20px; margin-right: 20px">

    <div style="width: 750px; display: inline-block;border: 1px solid black; margin-top: 10px">

        <table style="width: 100%; text-align: center">
            <thead>
            <tr style="font-size: 9.5pt">
                <th>Concepto/ Artículo</th>
                <th>Cantidad</th>
                <th>Precio unitario</th>
                <th>Subtotal</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody style="font-size: 8pt">
            @foreach($products_sale as $product_sale)
                <tr>
                    <td>{{ $product_sale->product_name }}</td>
                    <td>{{ $product_sale->quantity }}</td>
                    <td>{{ $symbol_country }}@convert($product_sale->sale_price)</td>
                    <td>{{ $symbol_country }}@convert($product_sale->subtotal)</td>
                    <td>{{ $symbol_country }}@convert($product_sale->total)</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
{{--//Footer--}}
<footer style="position: absolute; bottom: 15px;">

    <div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; margin-top: 0px; border-bottom: solid 1px;">
        <div style="width: 200px; display: inline-block; vertical-align: middle;">
            <img src="{{ env('URL_STORAGE_FTP').$sale->pdf_sello }}" alt="" width="85px" height="85px">
        </div>
        <div style="width: 200px; display: inline-block; vertical-align: top;">

        </div>
        <div style="width: 345px; display: inline-block; text-align: right">
            <p style="margin: 0px 0px 0px 0px">
                <span style="font-size:16pt; font-weight: bold">Subtotal: </span>
                <span style="font-weight:bold;font-size:16pt; color: grey">{{ $symbol_country }}@convert($sale->subtotal)</span><br>
                <span style="font-size:16pt; font-weight: bold">Descuento: </span>
                <span style="font-weight:bold;font-size:16pt; color: grey">{{ $symbol_country }}@convert($totalDiscounts) ({{$discounts_sale->percentage}}%)</span><br>
                <span style="font-size:16pt; font-weight: bold">Total: </span>
                <span style="font-weight:bold;font-size:16pt; color: red">{{ $symbol_country }}@convert($sale->total)</span>
            </p>
        </div>
    </div>

</footer>
</body>
</html>