<ul class="dropdown-menu">
    <li>
        <a class="openModalShowAreaMonitoring" data-toggle="modal" data-target="#showAreaMonitoringModal" data-id="{{ $sale->id }}" data-sale="{{ $sale->id_sale }}" style="cursor:pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver Detalle</a>
    </li>
    <li>
        <a href="{{ Route('view_pdf_sales', \Vinkla\Hashids\Facades\Hashids::encode($sale->id)) }}" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Nota de Venta</a>
    </li>
    <li>
        <a style="cursor: pointer;" data-toggle="modal" data-target="#sendMailSale"
           data-id="{{ Vinkla\Hashids\Facades\Hashids::encode($sale->id) }}"
           data-email="{{ $sale->email }}">
            <i class="fa fa-envelope-o" aria-hidden="true">
            </i>Enviar por Correo
        </a>
    </li>
    <li><a style="cursor: pointer;" data-toggle="modal" data-target="#whatsappSale"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($sale->id) }}"
           data-cellphone="{{ $sale->cellphone }}"
           data-codecounty="{{ $code_country }}">
            <i class="fa fa-whatsapp" aria-hidden="true" style="color: green">
            </i>Compartir por  Whatsapp</a>
    </li>
    <li>
        <a data-toggle="modal" data-target="#ModalPaymentSale" style="cursor:pointer"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($sale->id) }}">
            <i class="fa fa-money" aria-hidden="true" style="color: dodgerblue;"></i>
            Ingresar Pago
        </a>
    </li>
    @if(Entrust::can('Eliminar Datos')  || Entrust::hasRole('Cuenta Maestra'))
    <li>
        <a style="cursor: pointer;" data-toggle="modal" data-target="#cancelSale" data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($sale->id) }}">
            <i class="fa fa-window-close-o" aria-hidden="true" style="color: red;"></i>Cancelar</a>
    </li>
    @endif
</ul>