<!-- INICIA MODAL PARA CANCELAR VENTA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="cancelSale" role="dialog" aria-labelledby="cancelSale">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Cancelar Venta</h4>
                            </div>

                            <div class="box-body text-center">
                                <span style="font-weight: bold;">¿Estás Seguro que deseas Cancelar?</span>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <button type="button" class="btn btn-primary" id="btnCancelSale">Cancelar</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA CANCELAR VENTA -->