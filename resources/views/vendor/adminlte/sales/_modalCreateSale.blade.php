<!-- START MODAL DE VENTA-->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalCreateSale" role="dialog" aria-labelledby="modalCreateSale" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitleOrder">Nueva Venta</h4>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <span style="color:black;"># Venta </span><span id="folioPurchase">1</span>
                                    </div>
                                </div>

                                    <div class="col-md-12">

                                        <div class="col-md-4">
                                            <label for="">Seleccionar Cliente:</label>
                                            <select name="selectCustomer" id="selectCustomer" class="applicantsList-single form-control" style="font-weight: bold; width: 100%">
                                                <option value="0">Cliente Mostrador</option>
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id }}">
                                                        {{ $customer->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <label for="">Nombre Cliente (Mostrador):</label>
                                            <input type="text" class="form-control" id="customerNameOnly" placeholder="Nombre del cliente">
                                            <label for="">Teléfono Cliente (Mostrador):</label>
                                            <input type="number" class="form-control" id="customerCellphoneOnly" placeholder="Número de teléfono">
                                        </div>

                                        <div class="col-md-4">
                                            <label for="">Tipo de Venta:</label>
                                            <select name="selectTypeSale" id="selectTypeSale" class="form-control" style="font-weight: bold;width: 100%">
                                                @foreach($storehouses as $storehouse)
                                                    <option value="{{ $storehouse->id }}">
                                                        {{ $storehouse->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <label for="">Método de Pago</label>
                                            <select name="selectPaymentMethod" id="selectPaymentMethod" class="form-control" style="font-weight: bold;width: 100%">
                                                @foreach($payment_methods as $payment_method)
                                                    <option value="{{ $payment_method->id }}">
                                                        {{ $payment_method->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <label for="">Forma de Pago:</label>
                                            <select name="selectPaymentWay" id="selectPaymentWay" class="form-control" style="font-weight: bold; width: 100%">
                                                @foreach($payment_ways as $payment_way)
                                                    <option value="{{ $payment_way->id }}">
                                                        {{ $payment_way->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label for="">Fecha de Venta:</label>
                                            <input type="date" class="form-control" id="date" name="date" value="{{ $now }}">
                                            <label for="">Tipo de Comprobante</label>
                                            <select name="selectVoucher" id="selectVoucher" class="form-control" style="font-weight: bold;width: 100%">
                                                @foreach($vouchers as $voucher)
                                                    <option value="{{ $voucher->id }}">
                                                        {{ $voucher->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <label for="">Descuento:</label>
                                            <select name="selectDiscount" id="selectDiscount" class="form-control" style="font-weight: bold;width: 100%">
                                                @foreach($discounts as $discount)
                                                    <option value="{{ $discount->id }}"
                                                            @if($discount->title == "Sin Descuento") selected @endif>{{ $discount->title }}
                                                        / {{ $discount->percentage }}%
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <label style="font-weight: bold;">Descripción:</label>
                                            <div class="">
                                                <textarea name="description" id="description" cols="10" rows="3" class="form-control" placeholder="Ejemplo: Descripción Producto..."></textarea>
                                            </div>
                                        </div>

                                    </div>

                                <!-- Start New version -->
                                <div class="col-md-12">
                                    <br>
                                    <div class="text-center">
                                        <label class="control-label text-primary" style="font-weight: bold;">Detalle de la Venta</label>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="control-label" style="font-weight: bold;">Producto</label>
                                            <select id="products" class="form-control" style="width: 100%"></select>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label" style="font-weight: bold;">Cantidad Envase</label>
                                            <input type="text" class="form-control" id="quantityProduct" placeholder="Cantidad Envase" disabled>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label" style="font-weight: bold;">Impuestos Aplicados</label>
                                            <input type="text" class="form-control" id="taxes" placeholder="Impuestos Aplicados" disabled>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label" style="font-weight: bold;">Precio de Venta</label>
                                            <input type="text" class="form-control" id="lastPrice" placeholder="Precio de Venta">
                                        </div>
                                        <div class="col-md-1">
                                            <label class="control-label" style="font-weight: bold;">Existencia</label>
                                            <input type="number" class="form-control" id="stock" placeholder="Existencia" disabled>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="control-label" style="font-weight: bold;">Cantidad</label>
                                            <input type="number" class="form-control" id="quantity" placeholder="Cantidad" min="1">
                                        </div>
                                        <div class="col-md-1" id="actionsHeader">
                                            <label class="control-label" style="font-weight: bold;">Acciones</label>
                                            <button class="btn btn-primary" id="addTest">Agregar</button>
                                        </div>
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-12 table-responsive">
                                    <table class="table table-responsive-md table-sm table-bordered table-hover" id="customTable">
                                        <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Cantidad Envase</th>
                                            <th>Impuestos Aplicados</th>
                                            <th>Último Precio</th>
                                            <th>Cantidad</th>
                                            <th>Subtotal</th>
                                            <th>Total</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- End New version -->

                            </div>

                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">Subtotal: <b id="subtotalTest"></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <label class="control-label text-primary" for="total" style="font-size: 1.5em;">Total: <b id="totalTest"></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelEditProfileDiv">Cancelar</button>
                                        <button class="btn btn-primary" type="button" id="saveSale" name="saveSale">Generar Venta</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE VENTA -->
