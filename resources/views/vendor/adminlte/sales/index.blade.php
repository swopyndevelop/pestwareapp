@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mis Ventas</h3>

                        <ol class="breadcrumb" style="margin-top: 5px;">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Mis Ventas</li>
                        </ol>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a type="submit" class="btn btn-primary-dark" data-toggle="modal" href="#modalCreateSale">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Nueva Venta
                        </a>
                    </div>

                    <div class="col-md-5 margin-mobile">
                        <select name="selectJobCenterCustomer" id="selectJobCenterCustomer" class="form-control" style="font-weight: bold;">
                        </select>
                    </div>

                    <div class="col-md-5 margin-mobile">
                        {!!Form::open(['method' => 'GET','route'=>'index_customers'])!!}
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Buscar por: (Nombre)" name="search" id="search" value="">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                        {!!Form::close()!!}
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <br><br>
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Venta</th>
                                        <th>Vendedor</th>
                                        <th>Fecha / Hora</th>
                                        <th>Cliente</th>
                                        <th>Forma de Pago</th>
                                        <th>Método de Pago</th>
                                        <th>Tipo de Comprobante</th>
                                        <th>Tipo de Venta</th>
                                        <th>Subtotal</th>
                                        <th>Total</th>
                                        <th>Estatus</th>
                                        <th>Comprobante</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sales as $sale)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                        {{ $sale->id_sale }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.sales._menu')
                                                </div>
                                            </td>
                                            <td>{{ $sale->username }}</td>
                                            <td>{{ $sale->date }}<br>{{ \Carbon\Carbon::parse($sale->created_at)->format('H:i') }}</td>
                                            <td>{{ $sale->customer }}</td>
                                            <td>{{ $sale->payment_way }}</td>
                                            <td>{{ $sale->payment_method }}</td>
                                            <td>{{ $sale->voucher }}</td>
                                            <td>{{ $sale->storehouse }}</td>
                                            <td>$@convert($sale->subtotal)</td>
                                            <td>$@convert($sale->total)</td>
                                            <td>
                                                <b
                                                @if($sale->id_status == 11) style="color: #3C5898" @endif
                                                @if($sale->id_status == 12) style="color: red" @endif
                                                @if($sale->id_status == 13) style="color: green" @endif
                                                >{{ $sale->status }}
                                                </b>
                                            </td>
                                            <td class="text-center">
                                                @if($sale->url_file_voucher != null)
                                                    <a href="{{ Route('download_voucher_sale', \Vinkla\Hashids\Facades\Hashids::encode($sale->id)) }}" target="_blank">
                                                        <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 2.5em;color:red"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $sales->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@include('vendor.adminlte.sales._modalCreateSale')
@include('vendor.adminlte.sales._modalSendEmailSale')
@include('vendor.adminlte.sales._modalSendWhatsappSale')
@include('vendor.adminlte.sales._modalPaymentStatus')
@include('vendor.adminlte.sales._modalCancelSale')
@endsection
@section('personal-js')
    <script>$(".applicantsList-single").select2();</script>
    <script type="text/javascript" src="{{ URL::asset('js/build/sales.js') }}"></script>
@endsection