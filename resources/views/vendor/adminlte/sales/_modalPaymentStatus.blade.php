<!-- START MODAL DE VENTA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalPaymentSale" role="dialog" aria-labelledby="ModalPaymentSale" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document">
                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <form action="{{ Route('payment_status_sale') }}" method="POST" id="formPaymentStatusSale" enctype="multipart/form-data">
                                {{ csrf_field() }}
                            <div class="box-body text-center">
                                <h4 class="modal-title text-center text-info" id="modalTitle">Pagos</h4>
                                <h5>Anexe aquí su factura y comprobante de pago:</h5>
                                <div class="col-sm- text-center">
                                    <input class="form-control" type="file" name="fileVoucher" id="fileVoucher" required>
                                    <input type="hidden" name="idSale" id="idSale">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <br>
                                <div class="text-center">
                                    <button class="btn btn-primary" type="submit" id="payStatusButton">Pagar</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE VENTA -->
