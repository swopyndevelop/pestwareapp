<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL INVENTARIO HISTÓRICO -->
<div class="modal fade" id="storehouseHistorical" tabindex="-1">
	<div class="modal-body modal-lg" style="width: 100% !important;">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Histórico Inventario Teórico</h4>
				<input type="hidden" id="inventoryIdDetailIdJobCenter">
				<input type="hidden" id="inventoryIdDetailIdIdEmployee">
				<input type="hidden" id="inventoryIdDetailIdIdProduct">
				<input type="hidden" id="inventoryIdDetailIdIdCompany">
				<div class="row container-fluid">
					<div class="col-md-6">
						<label style="font-weight: bold; color: black;">Almacén:</label> <span id="nameHistoricalInventory"></span>
						<br>
						<label style="font-weight: bold; color: black;">Total de Unidades:</label> <span id="totalUnitsInventory"></span>
						<br>
						<label style="font-weight: bold; color: black;">Total de Inventario:</label> <span id="valueTotalInventory"></span>
					</div>
					<div class="col-md-3">

					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-secondary" id="exportExcelDetailHistoricalInventory"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar a Excel</button>
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table table-sorter table-hover" id="tableHistoricalInventory">
                            <thead>
                                <tr class="small">
                                    <th>Folio</th>
                                    <th>Fecha/Hora</th>
                                    <th>Usuario</th>
                                    <th>Movimiento</th>
									<th>Producto</th>
									<th>Origen</th>
									<th>Destino</th>
									<th class="text-center">Unidad (U) <br> <b id="quantityTotal" class="text-primary"></b></th>
									<th class="text-center">Cantidad (C) <br> <b id="fractionQuantityTotal" class="text-primary"></b></th>
									<th>Existencia Antes del (U)</th>
									<th>Existencia Después del (U)</th>
									<th>Existencia Antes del (C)</th>
									<th>Existencia Después del (C)</th>
									<th class="text-center">Precio Unitario <br> <b id="unitPriceTotal" class="text-primary"></b></th>
									<th class="text-center">Valor Movimiento <br> <b id="valueMovementTotal" class="text-primary"></b></th>
									<th class="text-center">Valor Inventario <br> <b id="valueInventoryTotal" class="text-primary"></b></th>
                                </tr>
								<tr class="small">
									<th>
										<input type="text" name="folio" id="folio" class="form-control">
									</th>
									<th class="">
										<input type="text" name="filterDateHistoricalInventory" id="filterDateHistoricalInventory" class="form-control" style="font-size: .7em">
									</th>
									<th class="">
										<select id="usersFilter" data-live-search="true" data-width="100%">
											<option value="0" selected>Todos</option>
										</select>
									</th>
									<th class="">
										<select id="typeMovementsFilter" class="selectpicker" data-live-search="true" data-width="100%">
											<option value="0" selected>Todos</option>
											@foreach($typeMovements as $typeMovement)
												<option value="{{ $typeMovement->id }}">
													{{ $typeMovement->name }}
												</option>
											@endforeach
										</select>
									</th>
									<th class="">
										<select id="productsFilter" data-live-search="true" data-width="100%">
											<option value="0" selected>Todos</option>
										</select>
									</th>
									<th class="">
										<input type="text" name="nameSource" id="nameSource" class="form-control">
									</th>
									<th class="">
										<input type="text" name="nameDestiny" id="nameDestiny" class="form-control">
									</th>
									<th class="">
										<button id="btnFilterHistoricalInventory" class="btn btn-primary btn-md">
											<i class="glyphicon glyphicon-search"></i> Filtrar
										</button>
									</th>
									<th class=""></th>
									<th class=""></th>
									<th class=""></th>
									<th class=""></th>
									<th class=""></th>
									<th class=""></th>
									<th class=""></th>
									<th class=""></th>
								</tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL DEL INVENTARIO HISTÓRICO-->