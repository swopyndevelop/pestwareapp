<!-- INICIA MODAL PARA CREAR AJUSTE POR ENTRADA -->
<div class="modal fade" id="adjustmentExit<?php echo $adj; ?>" tabindex="-1">
	<div class="modal-dialog modal-lg" style="width: 90% !important;">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
			</div>
			<!-- Modal body -->
			<form action="{{ route('store_adjustment') }}" method="POST" id="form">
				{{ csrf_field() }}
				{{ method_field('POST') }}
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Salida por ajuste: <b>{{ $shd->product }}</b></h4>
				<div class="row container">
					<div class="col-md-12">
						<br>
						<label style="font-weight: bold; color: black;">Almacen:</label> {{ $store['name'] }}
						<br>
					</div>
				</div>
				<input type="hidden" value="{{ $shd->id }}" name="id_storehouse">
				<input type="hidden" value="APS-" name="adjustment_type">
				<input type="hidden" value="{{ $shd->stock }}" name="units_before">
				<input type="hidden" value="{{ $shd->stock_other_units }}" name="quantity_before">
				<input type="hidden" value="{{ $shd->total }}" name="total_before">
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table tablesorter">
						<thead>
                                <tr>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Existencia <br> Unidades</th>
                                    <th class="text-center">Existencia <br> Cantidad</th>
                                    <th class="text-center">Total</th>
									<th class="text-center">Tipo Entrada</th>
									<th class="text-center">Unidades/Cantidad Entrada</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">{{ $shd->product }}</td>
                                    <td class="text-center">
										<input type="number" readonly="readonly" name="units_adjustment" value="{{ $shd->stock }}">
									</td>
                                    <td class="text-center">
										<input type="number" readonly="readonly" name="quantity_adjustment" value="{{ $shd->stock_other_units }}"> {{ $shd->unit }}s
									</td>
                                    <td class="text-center">
										{{ $symbol_country }} <input type="number" readonly="readonly" name="total_adjustment" value="{{ $shd->total }}">
									</td>
									<td class="text-center">
										<select name="type_entry">
											<option value="0">Unidades</option>
											<option value="1">Fracción</option>
										</select>
									</td>
									<td class="text-center">
										<input type="number" name="units_entry" required>
									</td>
                                </tr>
                            </tbody>
                        </table>
						<br>
						<textarea required style="font-style: normal; font-size: 18px; width: 100%" name="comments" rows="5" placeholder="Comentarios del motivo de ajuste..."></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-sm">Guardar Ajuste</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA AJUSTE POR ENTRADA-->