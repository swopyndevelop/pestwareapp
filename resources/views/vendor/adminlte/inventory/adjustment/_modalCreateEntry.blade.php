<!-- INICIA MODAL PARA CREAR AJUSTE POR ENTRADA -->
<div class="modal fade" id="adjustmentEntry" tabindex="-1">
	<div class="modal-body" style="width: 70%; margin-left: 15%">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body text-center">
				<span class="modal-title text-center text-info" style="font-size: large; font-weight: bold" id="modalTitleEntryExit"></span>
				<span class="modal-title text-center text-info" style="font-size: large; font-weight: bold"> </span><span id="nameStoreHouseProduct" style="font-weight: bold"></span>
				<div class="row container-fluid">
					<div class="col-md-12">
						<br>
						<label style="font-weight: bold; color: black;">Almacen:</label> <span id="nameStoreHouse"></span>
					</div>
				</div>
				<input type="hidden" name="id_storehouse" id="id_storehouse">
				<input type="hidden" name="adjustment_type" id="adjustment_type">
				<input type="hidden" name="units_before" id="units_before">
				<input type="hidden" name="quantity_before" id="quantity_before">
				<input type="hidden" name="total_before" id="total_before">
				<div class="row container-fluid">
					<div class="col-md-12 table-responsive">
                        <table class="table tablesorter">
                            <thead>
                                <tr>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Existencia/Unidades</th>
                                    <th class="text-center">Existencia/Cantidad</th>
                                    <th class="text-center">Total <span id="symbolCode"></span></th>
									<th class="text-center">Tipo</th>
									<th class="text-center">Unidades/Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"><span id="nameProductTable"></span></td>
                                    <td class="text-center">
										<input class="form-control" type="number" readonly="readonly" name="units_adjustment" id="units_adjustment">
									</td>
                                    <td class="text-center">
										<input class="form-control" type="number" readonly="readonly" name="quantity_adjustment" id="quantity_adjustment">
										<span id="quantityUnits">s</span>
									</td>
                                    <td class="text-center">
										<div class="input-group">
											<input class="form-control" type="number" readonly="readonly" name="total_adjustment" id="total_adjustment">
										</div>
									</td>
									<td class="text-center">
										<select name="type_entry" id="type_entry" class="form-control">
											<option value="0">Unidades</option>
											<option value="1">Fracción</option>
										</select>
									</td>
									<td class="text-center">
										<input type="number" id="units_entry" class="form-control">
									</td>
                                </tr>
                            </tbody>
                        </table>
						<textarea class="form-control" style="font-style: normal; font-size: 18px;" name="comments" id="comments" rows="5" placeholder="Comentarios del motivo de ajuste..."></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<button type="button" id="btnSaveSetting" class="btn btn-primary btn-sm">Guardar Ajuste</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA AJUSTE POR ENTRADA-->