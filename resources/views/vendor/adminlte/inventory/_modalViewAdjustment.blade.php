<!-- INICIA MODAL PARA MOSTRAR AJUSTE DE INVENTARIO -->
<div class="modal fade" id="storehouseAdjustment" tabindex="-1">
	<div class="modal-content">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Ajuste del Inventario Teórico</h4>
				<div class="row container">
					<div class="col-md-12">
						<label style="font-weight: bold; color: black;">Almacen:</label>
						<br>
						<label style="font-weight: bold; color: black;">Total de Unidades:</label>
						<br>
						<label style="font-weight: bold; color: black;">Total de Inventario:</label>
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table tablesorter" id="tableProduct">
                            <thead>
                                <tr>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Existencia <br> Unidades</th>
                                    <th class="text-center">Existencia <br> Cantidad</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
						</table>
						<br><br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR AJUSTE DE INVENTARIO-->