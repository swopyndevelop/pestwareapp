<!-- INICIA MODAL PARA MOSTRAR AJUSTE DE INVENTARIO -->
<div class="modal fade" id="movementStorehouse" tabindex="-1" style="overflow-y: scroll">
	<div class="modal-body">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Movimientos -> <b id="labelMovementStoreHose"></b></h4>

				<div class="row container-fluid" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table table-sorter" id="tableMovementStoreHouse">
                            <thead>
                                <tr>
                                    <th class="text-center">#Movimiento</th>
                                    <th class="text-center">Fecha/Usuario</th>
                                    <th class="text-center">Origen</th>
                                    <th class="text-center">Destino</th>
									<th class="text-center">Unidades Traspasadas</th>
									<th class="text-center">Total Inventario</th>
                                </tr>
								<tr class="small">
									<th>
										<input type="text" name="folioMovementStoreHouse" id="folioMovementStoreHouse" class="form-control">
									</th>
									<th class="">
										<input type="text" name="filterDateMovementStoreHouse" id="filterDateMovementStoreHouse" class="form-control" style="font-size: .7em">
									</th>
									<th class="">
										<input type="text" name="nameSourceMovementStoreHouse" id="nameSourceMovementStoreHouse" class="form-control">
									</th>
									<th class="">
										<input type="text" name="nameDestinyMovementStoreHouse" id="nameDestinyMovementStoreHouse" class="form-control">
									</th>
									<th class="">
										<button id="btnFilterMovementStoreHouse" class="btn btn-primary btn-md">
											<i class="glyphicon glyphicon-search"></i> Filtrar
										</button>
									</th>
								</tr>
                            </thead>
                            <tbody>
                            </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR AJUSTE DE INVENTARIO-->