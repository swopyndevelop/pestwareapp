<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL MOVIMIENTO-->
<div class="modal fade" id="detailMovementAdjustment" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle del Ajuste</h4>
				<div class="row container-fluid">
					<div class="col-md-6">
						<label>Movimiento: </label> <label id="labelAdjustment"></label> <br>
						<label>Fecha: </label> <label id="labelDateAdjustment"></label> <br>
						<label>Usuario: </label> <label id="labelUserNameAdjustment"></label>
					</div>
					<div class="col-md-6">
						<label>Origen: </label> <label id="labelSourceAdjustment"></label> <br>
						<label>Destino: </label> <label id="labelDestinyAdjustment"></label> <br>
						<label>Total: </label> <label id="labelTotalAdjustment"></label>
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table table-hover" id="tableDetailMovementAdjustment">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad Envase</th>
                                    <th>Ajuste</th>
                                    <th>Precio Base</th>
                                    <th>Total</th>
									<th>Comentarios</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA-->