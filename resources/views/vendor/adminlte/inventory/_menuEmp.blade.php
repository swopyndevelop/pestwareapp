<ul class="dropdown-menu">
	<li><a data-toggle="modal" data-target="#storehouseEmpDetail<?php echo $b; ?>" style="cursor: pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a></li>
	<li><a data-toggle="modal" data-target="#storehouseAdjustmentEmp<?php echo $b; ?>" style="cursor: pointer"><i class="fa fa-wrench" aria-hidden="true" style="color: #138D75;"></i>Ajuste</a></li>
	<li><a id="menuMovementsDiv" href="{{ route('index_mov_emp', \Vinkla\Hashids\Facades\Hashids::encode($storeEmp['id_employee'])) }}"><i class="fa fa-arrows" aria-hidden="true" style="color: #D35400;"></i>Movimientos</a></li>
	<li><a data-toggle="modal" data-target="#entryDetail" style="cursor: pointer"><i class="fa fa-shield" aria-hidden="true" style="color: #922B21;"></i>Auditoría</a></li>
</ul>
@include('vendor.adminlte.inventory._modalViewDetailEmp')
@include('vendor.adminlte.inventory._modalViewAdjustmentEmp')