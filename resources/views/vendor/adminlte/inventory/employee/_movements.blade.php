@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen" id="entry">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title" id="titleMevementsEmployee">Movimientos de {{ $nameStorehouse->name }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table tablesorter table-hover" id="tableProduct">
                                            <thead id="thMovementEmployee">
                                            <tr>
                                                <th># Movimiento</th>
                                                <th>Fecha/Usuario</th>
                                                <th>Origen</th>
                                                <th>Destino</th>
                                                <th>Cantidad<br>Traspasada</th>
                                                <th>Total Inventario</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $a = 0; ?>
                                             @foreach($movements as $mov)
                                             <?php $a = $mov->id; ?>
                                                    <tr id="rowMovementEmployee">
                                                        <td>
                                                            <div class="btn-group" id="ulMovementButton">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    @if($mov->type == "tee")
                                                                        {{ $mov->id_transfer_ee }} <span class="caret"></span>
                                                                        </a>
                                                                        @include('vendor.adminlte.inventory.employee._menuEE')
                                                                    @elseif($mov->type == "tce")
                                                                        {{ $mov->id_transfer_ce }} <span class="caret"></span>
                                                                        </a>
                                                                        @include('vendor.adminlte.inventory.employee._menuCE')
                                                                    @elseif($mov->type == "tec")
                                                                        {{ $mov->id_transfer_ec }} <span class="caret"></span>
                                                                        </a>
                                                                        @include('vendor.adminlte.inventory.employee._menuEC')
                                                                    @else
                                                                        {{ $mov->id_adjustment }} <span class="caret"></span>
                                                                        </a>
                                                                        @include('vendor.adminlte.inventory.employee._menuAdj')
                                                                    @endif
                                                            </div>
                                                        </td>
                                                        <td>{{ $mov->date }} <br> {{ $mov->username }}</td>
                                                        <td>{{ $mov->origin }}</td>
                                                        <td>{{ $mov->destiny }}</td>
                                                        <td>{{ $mov->tot_unit }}</td>
                                                        <td>${{ $mov->total }}</td>
                                                    </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/inventory.js') }}"></script>
@endsection