<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL AJUSTE -->
<div class="modal fade" id="adjustmentDetail<?php echo $a; ?>" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle del Ajuste</h4>
				<div class="row col-md-12">
					<div class="col-md-6">
						<p style="font-weight: bold; color: black; font-size: 18px">{{ $mov->id_adjustment }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $mov->date }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $mov->username }}</p>
					</div>
					<div class="col-md-6 text-right">
						<label style="font-weight: bold; color: black;">Origen:</label> {{ $mov->origin }}
						<br>
						<label style="font-weight: bold; color: black;">Destino:</label> {{ $mov->destiny }}
						<br>
						<label style="font-weight: bold; color: black;">Total:</label> ${{ $mov->total }}
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table tablesorter" id="tableProduct">
                            <thead>
                                <tr>
								<th class="text-center">Producto</th>
                                    <th class="text-center">Cantidad Envase</th>
                                    <th class="text-center">Ajuste</th>
                                    <th class="text-center">Precio Base</th>
									<th class="text-center">Total</th>
									<th class="text-center">Comentarios</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">{{ $mov->product }}</td>
									<td class="text-center">{{ $mov->quantity }} {{ $mov->unit }}s</td>
									<td class="text-center">{{ $mov->units_adjustment }}</td>
									<td class="text-center">${{ $mov->base_price }}</td>
									<td class="text-center">${{ $mov->total }}</td>
                                    <td class="text-center">{{ $mov->type }}</td>
                                </tr>
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL AJUSTE-->