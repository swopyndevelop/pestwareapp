<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL TRASPASO -->
<div class="modal fade" id="transferDetailCE<?php echo $a; ?>" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle del Traspaso</h4>
				<div class="row container">
					<div class="col-md-6">
						<p style="font-weight: bold; color: black; font-size: 18px">{{ $mov->id_transfer_ce }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $mov->date }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $mov->username }}</p>
					</div>
					<div class="col-md-6">
						<label style="font-weight: bold; color: black;">Origen:</label> {{ $mov->origin }}
						<br>
						<label style="font-weight: bold; color: black;">Destino:</label> {{ $mov->destiny }}
						<br>
						<label style="font-weight: bold; color: black;">Total:</label> ${{ $mov->total }}
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table tablesorter" id="tableProduct">
                            <thead class="table-general">
                                <tr>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Cantidad Envase</th>
                                    <th class="text-center">Unidades Traspasadas</th>
                                    <th class="text-center">Precio Base</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                               	@foreach($productsTransferCE as $p)
                               		@if($p->id_transfer_ce == $mov->id)
	                                    <tr>
	                                        <td class="text-center">{{ $p->name }}</td>
	                                        <td class="text-center">{{ $p->quantity }} {{ $p->unit }}</td>
	                                        <td class="text-center">{{ $p->units }} @if($p->is_units == 0){{ $p->unit }}@endif</td>
	                                        <td class="text-center">${{ $p->base_price }}</td>
	                                        <td class="text-center">$@if($p->is_units == 0){{ ($p->base_price / $p->quantity) * $p->units }} @else {{ $p->base_price * $p->units }} @endif</td>
	                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL TRASPASO-->