<!-- INICIA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA -->
<div class="modal fade" id="entryDetail<?php echo $a; ?>" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle de la Entrada</h4>
				<div class="row container-fluid">
					<div class="col-md-6" id="divLefMovementDetailChage">
						<p style="font-weight: bold; color: black; font-size: 18px">{{ $mov->id_entry }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $mov->date }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $mov->username }}</p>
					</div>
					<div class="col-md-6" id="divRightMovementDetailChage">
						<label style="font-weight: bold; color: black;">Origen:</label> {{ $mov->origin }}
						<br>
						<label style="font-weight: bold; color: black;">Destino:</label> {{ $mov->destiny }}
						<br>
						<label style="font-weight: bold; color: black;">Total:</label> ${{ $mov->total }}
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table tablesorter" id="tableProductChangeDiv">
                            <thead>
                                <tr>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Cantidad Envase</th>
                                    <th class="text-center">Unidades Recibidas</th>
                                    <th class="text-center">Precio Base</th>
                                    <th class="text-center">Precio Factura</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                               	@foreach($productsEntry as $p)
                               		@if($p->id_entry == $mov->id)
	                                    <tr>
	                                        <td class="text-center">{{ $p->name }}</td>
	                                        <td class="text-center">{{ $p->quantity }} {{ $p->unit }}</td>
	                                        <td class="text-center">{{ $p->units }}</td>
	                                        <td class="text-center">${{ $p->base_price }}</td>
	                                        <td class="text-center">${{ $p->price }}</td>
	                                        <td class="text-center">${{ $p->price * $p->units }}</td>
	                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					@foreach ($factura as $f)
                        @if ($f->id_entry == $mov->id)
                            <a href="{{Route('download_facture_entry', $f->id)}}">
                                <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 3.5em;color:red"></i>
                            </a>
                        @endif
                    @endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA-->