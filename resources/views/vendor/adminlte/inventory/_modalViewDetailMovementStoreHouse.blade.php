<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL MOVIMIENTO-->
<div class="modal fade" id="detailMovementSoreHouse" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle del Movimiento</h4>
				<div class="row container-fluid">
					<div class="col-md-6">
						<label>Movimiento: </label> <label id="labelMovement"></label> <br>
						<label>Fecha: </label> <label id="labelDate"></label> <br>
						<label>Usuario: </label> <label id="labelUserName"></label>
					</div>
					<div class="col-md-6">
						<label>Origen: </label> <label id="labelSourceMovement"></label> <br>
						<label>Destino: </label> <label id="labelDestinyMovement"></label> <br>
						<label>Total: </label> <label id="labelTotalMovement"></label>
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table table-hover" id="tableDetailMovementStoreHouse">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad Envase</th>
                                    <th>Unidades Recibidas</th>
                                    <th>Precio Base</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
					</div>
				</div>
<!--				<div class="row" style="margin: 10px;">
					<a href="">
						<i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 3.5em;color:red"></i>
					</a>
				</div>-->
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA-->