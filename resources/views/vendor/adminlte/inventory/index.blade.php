@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen" id="entry">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title" id="titleInventoryTheoricDiv">Inventario Teórico</h3>
<!--                        <input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">-->
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <select id="selectJobCenterInventory" class="form-control" style="font-weight: bold;">
                            @foreach($jobCenters as $jobCenter)
                                @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                    <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                        {{ $jobCenter->name }}
                                    </option>
                                @else
                                    <option value="{{ $jobCenter->id }}">
                                        {{ $jobCenter->name }}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                        <div class="input-group">
                            <input class="form-control" type="text" name="filterNameInventory" id="filterNameInventory" placeholder="Buscar...">
                            <span class="input-group-btn">
                                <button id="btnFilterInventory" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <a data-toggle="modal" data-target="#storehouseHistorical"
                           data-idJobCenter="0"
                           data-idEmployee="0"
                           data-idCompany="{{ auth()->user()->companie }}">
                        <button type="button" class="btn btn-primary"><i class="fa fa-arrows-v" aria-hidden="true"></i> Histórico Inventario</button></a>
                        <br><br>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-secondary pull-right" id="exportExcelInventory"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar Inventario a Excel</button>
                        <br><br>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12 table-responsive">

                            <table class="table table-sorter table-hover" id="storeHouseJobCenters">
                                <thead class="table-general">
                                <tr>
                                    <th>Almacén</th>
                                    <th>Fecha Último Movimiento</th>
                                    <th>Unidades Totales</th>
                                    <th>Valor de Inventario</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                        <!-- TODO: Add link paginate -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('vendor.adminlte.inventory._modalViewDetail')
@include('vendor.adminlte.inventory._modalViewAdjustment')
@include('vendor.adminlte.inventory._modalViewMovementStorehouse')
@include('vendor.adminlte.inventory._modalViewDetailMovementStoreHouse')
@include('vendor.adminlte.inventory._modalViewDetailAdjustmentMovement')
@include('vendor.adminlte.inventory.adjustment._modalCreateEntry')
@include('vendor.adminlte.inventory._modalViewHistorical')
@endsection

@section('personal-js')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/inventory.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection