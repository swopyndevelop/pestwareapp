<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL TRASPASO -->
<div class="modal fade" id="storehouseAdjustmentEmp<?php echo $b; ?>" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Ajuste del Inventario Teórico</h4>
				<div class="row container">
					<div class="col-md-12">
						<label style="font-weight: bold; color: black;">Almacen:</label> {{ $storeEmp['name'] }}
						<br>
						<label style="font-weight: bold; color: black;">Total de Unidades:</label> {{ $storeEmp['total_units'] }}
						<br>
						<label style="font-weight: bold; color: black;">Total de Inventario:</label> {{ $symbol_country }}{{ $storeEmp['total'] }}
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table tablesorter" id="tableProduct">
                            <thead>
                                <tr>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Existencia <br> Unidades</th>
                                    <th class="text-center">Existencia <br> Cantidad</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php $adj = 0; ?>
                               	@foreach($storehousesDetail as $shd)
									   @if($shd->id_employee == $storeEmp['id_employee'])
									   <?php $adj = $shd->id_employee . $shd->id_product; ?>
	                                    <tr>
	                                        <td class="text-center">
												<div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        {{ $shd->product }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.inventory._menuAdjustment')
                                                </div>
											</td>
	                                        <td class="text-center">{{ $shd->stock }}</td>
	                                        <td class="text-center">{{ $shd->stock_other_units }} {{ $shd->unit }}</td>
	                                        <td class="text-center">{{ $symbol_country }}{{ $shd->total }}</td>
	                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
						</table>
						<br><br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL TRASPASO-->