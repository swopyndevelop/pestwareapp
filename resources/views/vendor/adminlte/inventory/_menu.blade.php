<ul class="dropdown-menu">
	<li><a data-toggle="modal" data-target="#storehouseDetail{{$a}}" style="cursor:pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a></li>
	<li><a data-toggle="modal" data-target="#storehouseAdjustment{{$a}}" style="cursor:pointer"><i class="fa fa-wrench" aria-hidden="true" style="color: #138D75;"></i>Ajuste</a></li>
	<li><a href="{{ route('index_movements', \Vinkla\Hashids\Facades\Hashids::encode($store['id_job_center'])) }}"><i class="fa fa-arrows" aria-hidden="true" style="color: #D35400;"></i>Movimientos</a></li>
	<li><a data-toggle="modal" data-target="#entryDetail" style="cursor:pointer"><i class="fa fa-shield" aria-hidden="true" style="color: #922B21;"></i>Auditoría</a></li>
</ul>
@include('vendor.adminlte.inventory._modalViewDetail')
@include('vendor.adminlte.inventory._modalViewAdjustment')