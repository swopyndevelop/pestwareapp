@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <section class="content">
        @if(Auth::user()->companie == 338)
            <iframe width="100%" height="850"
                    src="https://datastudio.google.com/embed/reporting/030f336d-7e1a-4e88-a2ce-86b4ff46a520/page/riZuC"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
        @endif
    </section>
@endsection
