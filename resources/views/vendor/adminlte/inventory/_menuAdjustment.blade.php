<ul class="dropdown-menu">
	<li><a data-toggle="modal" href="#adjustmentEntry<?php echo $adj; ?>"><i class="fa fa-chevron-right" aria-hidden="true" style="color: #138D75;"></i>Entrada por Ajuste</a></li>
	<li><a data-toggle="modal" href="#adjustmentExit<?php echo $adj; ?>"><i class="fa fa-chevron-left" aria-hidden="true" style="color: #922B21;"></i>Salida por Ajuste</a></li>
</ul>
@include('vendor.adminlte.inventory.adjustment._modalCreateEntry')
@include('vendor.adminlte.inventory.adjustment._modalCreateExit')