<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL TRASPASO -->
<div class="modal fade" id="storehouseDetail" tabindex="-1" style="overflow-y: scroll;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle del Inventario Teórico</h4>
				<input type="hidden" id="storeIdDetailIdJobCenter" value="">
				<input type="hidden" id="storeIdDetailIdEmployee" value="">
				<div class="row container-fluid">
					<div class="col-md-9">
						<label style="font-weight: bold; color: black;">Almacen:</label> <span id="storeLabel"></span>
						<br>
						<label style="font-weight: bold; color: black;">Total de Unidades:</label> <span id="totalUnitsLabel"></span>
						<br>
						<label style="font-weight: bold; color: black;">Total de Inventario:</label> <span id="totalGeneralLabel"></span>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary btn-sm" id="exportExcelDetailInventory"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar</button>
					</div>
				</div>
				<div class="row" style="margin: 10px;">
					<div class="col-md-12 table-responsive">
                        <table class="table table-sorter table-hover" id="tableStoreHouseDetail">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Existencia <br> Unidades</th>
                                    <th>Existencia <br> Cantidad</th>
									<th>Precio <br> Base</th>
									<th>Precio <br> Costeo</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL TRASPASO-->