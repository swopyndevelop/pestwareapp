@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 id="titleInstance" class="box-title">Editar Configuración Inicial y Personalización</h3>
                        <input type="hidden" id="introJsInput" value="{{ auth()->user()->introjs }}">

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#instances" aria-controls="instances" role="tab" data-toggle="tab">Instancia</a></li>
                                <li role="presentation"><a href="#mips" aria-controls="mips" role="tab" data-toggle="tab">MIP</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <br>

                                <div role="tabpanel" class="tab-pane active" id="instances">
                                    <div class="row container-fluid">
                                        <div class="col-md-8" id="ConfigurationInstanceDivLeft">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    @if ($companie->logo != null)
                                                        <h5 for="" class="form-control-label">Logo actual:</h5>
                                                        <div style="width: 230px; height: 54px">
                                                            <img class="border border-primary" src="{{ env('URL_STORAGE_FTP').$companie->logo }}" width="100%" height="53" style="border: 3px solid rgba(0, 0, 0, 0.3); border-radius: 20px">
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="col-md-6">
                                                    <h5 for="form" class="form-control-label">Seleccionar Logo (208x79 pixels):</h5>
                                                    <form action="{{ route('add_picture_logo') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="logo">
                                                        {{ csrf_field() }}
                                                        <input type="number" id="idCompanieLogo" name="idCompanieLogo" hidden>
                                                        <div class="dz-message" data-dz-message>
                                                            <i class="glyphicon glyphicon-cloud-upload" style="font-size:20px"></i>
                                                            <br/>
                                                            <span>Arrastra y suelta tu logo aquí.</span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    @if ($companie->mini_logo != null)
                                                        <h5 for="" class="form-control-label">Mini Logo actual:</h5>
                                                        <div style="width: 52px; height: 52px">
                                                            <img src="{{ env('URL_STORAGE_FTP').$companie->mini_logo }}" alt="" width="100%" height="50px" style="border: 3px solid rgba(0, 0, 0, 0.3); border-radius: 5px">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <h5 for="form" class="form-control-label">Seleccionar Mini Logo(50x50 pixel):</h5>
                                                    <form action="{{ route('add_picture_mini_logo') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="minilogo">
                                                        {{ csrf_field() }}
                                                        <input type="number" id="idCompanieMiniLogo" name="idCompanieMiniLogo" hidden>
                                                        <div class="dz-message" data-dz-message>
                                                            <i class="glyphicon glyphicon-cloud-upload" style="font-size:20px"></i>
                                                            <br/>
                                                            <span>Arrastra y suelta tu mini logo aquí.</span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    @if ($companie->pdf_logo != null)
                                                        <h5 for="" class="form-control-label">PDF Logo actual:</h5>
                                                        <div style="width: 202px; height: 202px">
                                                            <img src="{{ env('URL_STORAGE_FTP').$companie->pdf_logo }}" alt="" width="100%" height="200px" style="border: 3px solid rgba(0, 0, 0, 0.3); border-radius: 20px">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <h5 for="form" class="form-control-label">Seleccionar Logo para PDF(200x200 pixel):</h5>
                                                    <form action="{{ route('add_pdf_logo') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="minilogo">
                                                        {{ csrf_field() }}
                                                        <input type="number" id="idCompaniePDFlogo" name="idCompaniePDFlogo" hidden>
                                                        <div class="dz-message" data-dz-message>
                                                            <i class="glyphicon glyphicon-cloud-upload" style="font-size:30px"></i>
                                                            <br/>
                                                            <span>Arrastra y suelta tu logo para PDF.</span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    @if ($companie->pdf_sello != null)
                                                        <h5 class="form-control-label">Sello de PDF actual:</h5>
                                                        <div style="width: 87px; height: 87px">
                                                            <img src="{{ env('URL_STORAGE_FTP').$companie->pdf_sello }}" alt="" width="100%" height="85px" style="border: 3px solid rgba(0, 0, 0, 0.3); border-radius: 20px">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <h5 for="form" class="form-control-label">Seleccionar Sello para PDF(85x85 pixel):</h5>
                                                    <form action="{{ route('add_pdf_sello') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="minilogo">
                                                        {{ csrf_field() }}
                                                        <input type="number" id="idCompaniePDFsello" name="idCompaniePDFsello" hidden>
                                                        <div class="dz-message" data-dz-message>
                                                            <i class="glyphicon glyphicon-cloud-upload" style="font-size:30px"></i>
                                                            <br/>
                                                            <span>Arrastra y suelta tu sello para PDF.</span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4" id="ConfigurationInstanceDivRight">

                                            <h5 for="address" class="form-control-label">Selecciona un tema:</h5>
                                            <select name="theme" id="theme" class="form-control" required>
                                                @if($companie->id_theme == 1)
                                                    <option value="1" selected="true">skin-blue</option>
                                                    <option value="2">skin-white</option>
                                                    <option value="3">skin-purple</option>
                                                    <option value="4">skin-yellow</option>
                                                    <option value="5">skin-red</option>
                                                    <option value="6">skin-green</option>
                                                @elseif($companie->id_theme == 2)
                                                    <option value="1">skin-blue</option>
                                                    <option value="2" selected="true">skin-white</option>
                                                    <option value="3">skin-purple</option>
                                                    <option value="4">skin-yellow</option>
                                                    <option value="5">skin-red</option>
                                                    <option value="6">skin-green</option>
                                                @elseif($companie->id_theme == 3)
                                                    <option value="1">skin-blue</option>
                                                    <option value="2">skin-white</option>
                                                    <option value="3" selected="true">skin-purple</option>
                                                    <option value="4">skin-yellow</option>
                                                    <option value="5">skin-red</option>
                                                    <option value="6">skin-green</option>
                                                @elseif($companie->id_theme == 4)
                                                    <option value="1">skin-blue</option>
                                                    <option value="2">skin-white</option>
                                                    <option value="3">skin-purple</option>
                                                    <option value="4" selected="true">skin-yellow</option>
                                                    <option value="5">skin-red</option>
                                                    <option value="6">skin-green</option>
                                                @elseif($companie->id_theme == 5)
                                                    <option value="1">skin-blue</option>
                                                    <option value="2">skin-white</option>
                                                    <option value="3">skin-purple</option>
                                                    <option value="4">skin-yellow</option>
                                                    <option value="5" selected="true">skin-red</option>
                                                    <option value="6">skin-green</option>
                                                @elseif($companie->id_theme == 6)
                                                    <option value="1">skin-blue</option>
                                                    <option value="2">skin-white</option>
                                                    <option value="3">skin-purple</option>
                                                    <option value="4">skin-yellow</option>
                                                    <option value="5">skin-red</option>
                                                    <option value="6" selected="true">skin-green</option>
                                                @endif

                                            </select>
                                            <br>
                                            @if($companie->id_theme == 1)
                                                <img id="themeImage" src="https://pestwareapp.com/img/themes/skin-blue.png" height="170">
                                            @elseif($companie->id_theme == 2)
                                                <img id="themeImage" src="https://pestwareapp.com/img/themes/skin-white.png" height="170">
                                            @elseif($companie->id_theme == 3)
                                                <img id="themeImage" src="https://pestwareapp.com/img/themes/skin-purple.png" height="170">
                                            @elseif($companie->id_theme == 4)
                                                <img id="themeImage" src="https://pestwareapp.com/img/themes/skin-yellow.png" height="170">
                                            @elseif($companie->id_theme == 5)
                                                <img id="themeImage" src="https://pestwareapp.com/img/themes/skin-red.png" height="170">
                                            @elseif($companie->id_theme == 6)
                                                <img id="themeImage" src="https://pestwareapp.com/img/themes/skin-green.png" height="170">
                                            @endif

                                            <div class="modal-footer">
                                                <hr>
                                                <div class="text-center">
                                                    <button class="btn btn-primary" type="submit" id="updateConfigStart">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="mips">
                                    <div class="col-md-6">
                                        <h5 for="address" class="form-control-label">Color para portada MIP:</h5>
                                        <input type="color" id="updateColorMIP" name="updateColorMIP" style="width: 100%;" class="form-control" value="{{ $companie->cover_mip_color }}">

                                        <div class="modal-footer">
                                            <hr>
                                            <div class="text-center">
                                                <button class="btn btn-primary" type="submit" id="updateConfigStartMip">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/instance.js') }}"></script>
    <script>
        Dropzone.options.myDropzone = {
            maxFilesize: 5, //mb- Image files not above this size
            uploadMultiple: false, // set to true to allow multiple image uploads
            parallelUploads: 2, //all images should upload same time
            maxFiles: 1, //number of images a user should upload at an instance
            acceptedFiles: ".jpg,.jpeg", //allowed file types, .pdf or anyother would throw error
            addRemoveLinks: true, // add a remove link underneath each image to
            autoProcessQueue: true // Prevents Dropzone from uploading dropped files immediately
        };
    </script>
@endsection