<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="{{ asset('/img/pestware-cuadre.png') }}" />
    <link rel="stylesheet" href="{{ asset('/loginew/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/estilos/loader-pestware-login.css') }}" />
    <title>PestWare App | Ingresar</title>
</head>

<body>
<div class="container">
    <div class="forms-container">
        <div class="signin-signup">
            <form action="{{ route('login') }}" method="POST" class="sign-in-form" id="formLoginNew">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <img src="{{ asset('/loginew/img/logo.svg') }}" class="image" alt="logo" height="200px" style="margin-bottom: 50px;" />
                <h2 class="title">Inicia sesión</h2>
                <div class="input-field" id="inputEmail">
                    <i class="fas fa-user" id="iconEmail"></i>
                    <input name="{{ config('auth.providers.users.field','email') }}" id="email" type="text" placeholder="Correo electrónico" />
                </div>
                <p class="text-danger" id="errorsMessage"></p>
                <div class="input-field" id="inputPassword">
                    <i class="fas fa-lock" id="iconPassword"></i>
                    <input id="password" name="password" autocomplete="off" type="password" placeholder="Contraseña" />
                    <span style="position: absolute; right: 15px; transform: translate(0,-50%); top: 50%; cursor: pointer;">
			            <i class="fas fa-eye" aria-hidden="true" id="eye" onclick="toggle()"></i>
		            </span>
                </div>
                <label for="terms"><a style="color: #0a568c; cursor: pointer;" id="recoverPassword">¿Olvidaste tu contraseña?</a></label>
                <label for="terms"><a style="color: #c44f4f; cursor: pointer; display: none;" id="sendEmailBackup">¿No recibiste un correo? Reenviar correo</a></label>
                <div id="errors">

                </div>
                <input type="submit" value="Entrar" class="btn solid" />
            </form>
            <form action="{{ url('/register/instance') }}" method="post" class="sign-up-form" id="formRegisterNewInstance">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h2 class="title">Ingresa tus datos</h2>
                <div class="input-field" id="inputCompany">
                    <i class="fas fa-building" id="iconCompany"></i>
                    <input type="text" placeholder="Empresa" name="company" id="company" />
                </div>
                <div class="input-field" id="inputContact">
                    <i class="fas fa-user" id="iconContact"></i>
                    <input type="text" placeholder="Nombre del contacto" name="contact" id="contact" />
                </div>
                <div class="input-field" id="inputPhone">
                    <i class="fas fa-phone-alt" id="iconPhone"></i>
                    <input type="text" placeholder="Teléfono" name="phone" id="phone" />
                </div>
                <div class="input-field" id="inputEmailRegister">
                    <i class="fas fa-envelope" id="iconEmailRegister"></i>
                    <input type="email" placeholder="Correo electrónico" name="emailRegister" id="emailRegister" />
                </div>
<!--                <div class="input-field" id="inputPasswordRegister">
                    <i class="fas fa-lock" id="iconPasswordRegister"></i>
                    <input type="password" placeholder="Contraseña" name="passwordRegister" id="passwordRegister" />
                </div>-->
                <div class="select-field" id="selectCodeRegister">
                    <i class="fas fa-flag" id="iconCodeRegister"></i>
                    <select name="codeRegister" id="codeRegister">
                        <option value="0" disabled selected>Seleccione su País de residencia</option>
                        @foreach($countries as $country)
                        <option value="{{ $country->id }}">{{ $country->name }} (+{{ $country->code_country }})</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <input type="checkbox" placeholder="Terminos y Condiciones" name="terms" id="terms" />
                    <label for="terms">He leído y acepto los <a style="color: #0a568c;" target="_blank" href="{{ route('terms_conditions') }}">términos y condiciones.</a></label>
                </div>
                <div id="errors-register">

                </div>
                <input type="submit" class="btn" value="Solicitar Demo" />

                <br><br>
                <p class="text-center" style="font-size: 10pt">Llámanos al +52 449 413 9091  -  contacto@pestwareapp.com</p>
            </form>
        </div>
    </div>

    <div class="panels-container">
        <div class="panel left-panel">
            <div class="content">
                <h3>¿Eres nuevo en PestWare App?</h3>
                <p>
                    Regístra tu solicitud para tener tu demostración gratis.
                </p>
                <button class="btn transparent" id="sign-up-btn">
                    Registrarme
                </button>
            </div>
            <img src="{{ asset('/loginew/img/log.svg') }}" class="image" alt="" />
        </div>
        <div class="panel right-panel">
            <div class="content">
                <h3>¿Ya tienes cuenta?</h3>
                <p></p>
                <button class="btn transparent" id="sign-in-btn">
                    Inicia sesión
                </button>
            </div>
            <img src="{{ asset('/loginew/img/register.svg') }}" class="image" alt="" />
        </div>
    </div>
</div>

<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.all.min.js"></script>
<script src="{{ asset('/loginew/app.js') }}"></script>
<script src="{{ asset('/js/build/authentication.js') }}"></script>
<script>
    let state= false;
    function toggle(){
        if(state){
            document.getElementById("password").setAttribute("type","password");
            state = false;
        }
        else{
            document.getElementById("password").setAttribute("type","text");
            state = true;
        }
    }
</script>
</body>

</html>