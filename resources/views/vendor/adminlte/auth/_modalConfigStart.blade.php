@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Configuración Inicial y Personalización</h3>
                        <input type="hidden" id="introJsInput" value="{{ auth()->user()->introjs }}">

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row container-fluid">
                            <div class="col-md-6">
                                <label for="form" class="form-control-label">Seleccionar Logo (208x79 pixels):</label>
                                <form action="{{ route('add_picture_logo') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="logo">
                                    {{ csrf_field() }}
                                    <input type="number" id="idCompanieLogo" name="idCompanieLogo" hidden>
                                    <div class="dz-message" data-dz-message>
                                        <i class="glyphicon glyphicon-cloud-upload" style="font-size:30px"></i>
                                        <br/>
                                        <span>Arrastra y suelta tu logo aquí.</span>
                                    </div>
                                </form>
                                <br>
                                <label for="form" class="form-control-label">Seleccionar Mini Logo(50x50 pixel):</label>
                                <form action="{{ route('add_picture_mini_logo') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="minilogo">
                                    {{ csrf_field() }}
                                    <input type="number" id="idCompanieMiniLogo" name="idCompanieMiniLogo" hidden>
                                    <div class="dz-message" data-dz-message>
                                        <i class="glyphicon glyphicon-cloud-upload" style="font-size:30px"></i>
                                        <br/>
                                        <span>Arrastra y suelta tu mini logo aquí.</span>
                                    </div>
                                </form>
                                <br>
                                <label for="form" class="form-control-label">Seleccionar Logo para PDF(200x200 pixel):</label>
                                <form action="{{ route('add_pdf_logo') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="minilogo">
                                    {{ csrf_field() }}
                                    <input type="number" id="idCompaniePDFlogo" name="idCompaniePDFlogo" hidden>
                                    <div class="dz-message" data-dz-message>
                                        <i class="glyphicon glyphicon-cloud-upload" style="font-size:30px"></i>
                                        <br/>
                                        <span>Arrastra y suelta tu logo para PDF.</span>
                                    </div>
                                </form>
                                <br>
                                <label for="form" class="form-control-label">Seleccionar Sello para PDF(85x85 pixel):</label>
                                <form action="{{ route('add_pdf_sello') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="minilogo">
                                    {{ csrf_field() }}
                                    <input type="number" id="idCompaniePDFsello" name="idCompaniePDFsello" hidden>
                                    <div class="dz-message" data-dz-message>
                                        <i class="glyphicon glyphicon-cloud-upload" style="font-size:30px"></i>
                                        <br/>
                                        <span>Arrastra y suelta tu sello para PDF.</span>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <br>
                                <label for="address" class="form-control-label">Licencia No.:</label>
                                <input type="text" name="licencia" id="licencia" class="form-control" placeholder="Ejemplo: 20AP0102030405">
                                <br>
                                <label for="address" class="form-control-label">Facebook:</label>
                                <input type="text" name="facebook" id="facebook" class="form-control">
                                <br>
                                <label for="address" class="form-control-label">Selecciona un tema:</label>
                                <br>
                                <select name="theme" id="theme" class="form-control" required>
                                    <option value="1">skin-blue</option>
                                    <option value="2">skin-white</option>
                                    <option value="3">skin-purple</option>
                                    <option value="4">skin-yellow</option>
                                    <option value="5">skin-red</option>
                                    <option value="6">skin-green</option>
                                </select>
                                <br>
                                <img id="themeImage" src="https://pestwareapp.com/img/themes/skin-blue.png " height="170">

                                <div class="modal-footer">
                                    <br>
                                    <div class="text-center">
                                        <button class="btn btn-primary" type="submit" id="saveConfigStart">Guardar y Continuar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/instance.js') }}"></script>
@endsection