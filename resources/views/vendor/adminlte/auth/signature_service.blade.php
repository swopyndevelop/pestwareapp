<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="{{ asset('/img/pestware-cuadre.png') }}" />
    <link rel="stylesheet" href="{{ asset('/loginew/style.css') }}" />
    <script type="text/javascript" src="https://unpkg.com/paper"></script>
    <title>PestWare App | Firma Cliente</title>
</head>

<body>
<div class="containerCenter">
    <img src="{{ asset('/loginew/img/logo.svg') }}" class="image" alt="logo" height="100px" />
    <h4>FIRMA CLIENTE => Servicio: {{$event->id_service_order}}</h4>
    <h5>Fecha: {{$event->initial_date}} | Hora: {{$event->initial_hour}} - {{$event->final_hour}}</h5>
    <h5>Técnico: {{$event->name}}</h5>

    <img src="{{ $imageSignature }}" alt="{{ $serviceFirm->other_name }}" width="350" height="250">
    <canvas id="myCanvas" width="350" height="250" style="border: 1px solid #ccc; background: transparent"></canvas>
    <br>
    <input class="input-field" type="text" name="other_name_firm" id="other_name_firm" placeholder="Nombre Cliente"
           value="{{ $serviceFirm->other_name }}">

    <button class="btn btn-primary btn-lg" type="button" id="sendSignatureCustomer"
                name="sendSignatureCustomer">Enviar</button>

</div>

<script src="{{ asset('/js/paper.js') }}"></script>
<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.all.min.js"></script>
<script src="{{ asset('/loginew/app-signature.js') }}"></script>
</body>

</html>