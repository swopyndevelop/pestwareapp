@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Register
@endsection

@section('content')

<link rel="stylesheet" href="{{ asset('/css/style_auth.css')}}">
<script src="{{ asset('/js/ProgressDialogModal.js') }}" ></script>
<body class="hold-transition register-page text-center" style="background-color: white;">
  <div id="app">
    <div class="col-md-12">
        
        <div class="col-md-6 resize_col" style="padding-right: 0px; padding-top: 6em;">
            <img src="{{ asset('/img/pestware.jpeg') }}" alt="" class="center-block img-responsive" id="imgcontainer" style="height: 500px; width: 500px;">
        </div>

        <div class="col-md-6">
            <div class="register-box">
          
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                    <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
                @endif

          <div class="row">
            <p class="login-box-msg"><h4>Queremos que seas parte de nuestro equipo.<br> <label style="font-size: 1.1em"><small><strong>Regístrate para comenzar</strong></small></h4></label></p>
            <hr>
            <form action="{{ url('/register') }}" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="group">      
                <input type="text" required name="name" class="cssinput">
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input" style="font-size: 1.3em">Nombre de Empresa</label>
              </div>
              @if (config('auth.providers.users.field','email') === 'username')
              <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Nombre de Empresa" name="username"/>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              @endif
                <div class="group">
                    <input type="text" required name="contact" class="cssinput">
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="title_input" style="font-size: 1.3em">Nombre del Contacto</label>
                </div>
              <div class="group">      
                <input type="number" required name="phone" class="cssinput">
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input" style="font-size: 1.3em">Teléfono</label>
              </div>
              <div class="group">      
                <input type="text" required name="email" class="cssinput">
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input" style="font-size: 1.3em">{{ trans('adminlte_lang::message.email') }}</label>
              </div>
              <div class="group"> 
              <div class="password">     
                <input required id="password_register" name="password" type="password"  class="cssinput" autocomplete="off">
                <span class="glyphicon glyphicon-eye-open" id="glyphicon-eye-open1"></span>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input" style="font-size: 1.3em">{{ trans('adminlte_lang::message.password') }}</label>
              </div>
            </div>
              <div class="group"> 
              <div class="password">     
                <input  required id="password_confirmation" name="password_confirmation" type="password" class="cssinput">
                <span class="glyphicon glyphicon-eye-open" id="glyphicon-eye-open2"></span>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input" style="font-size: 1.3em">{{ trans('adminlte_lang::message.retypepassword') }}</label>
              </div>
              </div>
              <div class="group">
                <div class="col-xs-12">
                <br>
                  <button type="submit" class="btn btn-primary btn-block" style="font-size: 1.5em; background-color: #1e8cc7; border-color: #1e8cc7;">Comenzar Demo</button>
                </div><!-- /.col -->
              </div>
            </form>
         </div><!-- /.form-box -->
       </div><!-- /.register-box -->
     </div>
   </div>
 </div>

@include('adminlte::layouts.partials.scripts_auth')
@include('adminlte::auth.terms')

<style type="text/css">
    .password  {
      position: relative;
    }
    .password .glyphicon {
      display:none;
      right: 15px;
      position: absolute;
      top: 12px;
      cursor:pointer;
    }
  
  </style>
   <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
    increaseArea: '20%' // optional
  });
    });
  </script>
  
  <script>
      ProgressDialogModal.InitModal($("#app"));
  </script>
  
  </body>

@endsection
