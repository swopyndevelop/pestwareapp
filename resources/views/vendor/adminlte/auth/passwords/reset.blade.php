<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="{{ asset('/img/pestware-cuadre.png') }}" />
    <link rel="stylesheet" href="{{ asset('/loginew/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/estilos/loader-pestware-login.css') }}" />
    <title>PestWare App | Ingresar</title>
</head>

<body>
<div class="container">
    <div class="forms-container">
        <div class="signin-signup">
            <form action="{{ route('login') }}" method="POST" class="sign-in-form" id="formResetPassword">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <img src="{{ asset('/loginew/img/logo.svg') }}" class="image" alt="logo" height="200px" style="margin-bottom: 50px;" />
                <h2 class="title">Recuperar contraseña</h2>
                <div class="input-field" id="inputPasswordNew">
                    <i class="fas fa-lock" id="iconPasswordNew"></i>
                    <input id="passwordNew" name="passwordNew" autocomplete="off" type="password" placeholder="Nueva contraseña" />
                    <span style="position: absolute; right: 15px; transform: translate(0,-50%); top: 50%; cursor: pointer;">
			            <i class="fas fa-eye" aria-hidden="true" id="eye" onclick="toggle()"></i>
		            </span>
                </div>
                <div class="input-field" id="inputPasswordConfirm">
                    <i class="fas fa-lock" id="iconPasswordConfirm"></i>
                    <input id="passwordConfirm" name="passwordConfirm" autocomplete="off" type="password" placeholder="Confirmar contraseña" />
                    <span style="position: absolute; right: 15px; transform: translate(0,-50%); top: 50%; cursor: pointer;">
			            <i class="fas fa-eye" aria-hidden="true" id="eye" onclick="toggleConfirm()"></i>
		            </span>
                </div>
                <div id="errors">

                </div>
                <input type="submit" value="Cambiar" class="btn solid" />
            </form>
        </div>
    </div>

    <div class="panels-container">
        <div class="panel left-panel">
            <div class="content">
                <h3>PestWare App</h3>
                <p>
                    El mejor aliado en la administración de tu negocio de control de plagas
                </p>
                <p>¡Mide, Controla y Mejora tu Negocio!</p>
            </div>
            <img src="{{ asset('/loginew/img/log.svg') }}" class="image" alt="" />
        </div>
        <div class="panel right-panel">
            <div class="content">
                <h3>¿Ya tienes cuenta?</h3>
                <p></p>
                <button class="btn transparent" id="sign-in-btn">
                    Inicia sesión
                </button>
            </div>
            <img src="{{ asset('/loginew/img/register.svg') }}" class="image" alt="" />
        </div>
    </div>
</div>

<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.all.min.js"></script>
<script src="{{ asset('/js/build/reset.js') }}"></script>
<script>
    let state= false;
    function toggle(){
        if(state){
            document.getElementById("passwordNew").setAttribute("type","password");
            state = false;
        }
        else{
            document.getElementById("passwordNew").setAttribute("type","text");
            state = true;
        }
    }
</script>
<script>
    let stateConfirm= false;
    function toggleConfirm(){
        if(stateConfirm){
            document.getElementById("passwordConfirm").setAttribute("type","password");
            stateConfirm = false;
        }
        else{
            document.getElementById("passwordConfirm").setAttribute("type","text");
            stateConfirm = true;
        }
    }
</script>
</body>

</html>