@extends('adminlte::layouts.auth')  
@section('htmlheader_title')
Log in
@endsection

@section('content')

<link rel="stylesheet" href="{{ asset('/css/style_auth.css')}}">


<script src="{{ asset('/js/ProgressDialogModal.js') }}" ></script>

<body class="hold-transition login-page text-center" style="background-color: white;">
  <div id="app">
    <div class="col-md-12">

      <div class="col-md-6 resize_col" style="padding-right: 0px; padding-top: 6em;">
        <img src="{{ asset('/img/pestware.jpeg') }}" alt="" class="center-block img-responsive" id="imgcontainer" style="height: 500px; width: 500px;">
      </div> 

      <div class="clearfix visible-xs">
          
      </div>

      <div class="col-md-6 col-xs-12 resize_col">
        <div class="login-box">
          <div class="login-logo">

          </div>
          <div class="login-logo" style="margin-top: 0.2em">
            <p id="welcome_text" style="width: 100%">Bienvenido</p>
          </div>

          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Parece que algo salió mal</strong><br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          @if (session()->has('message_register'))
          
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ session()->get('message_register')}}</strong>
          </div>
          
          @endif
          <div class="login-box-body">
            <form action="{{ route('login') }}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="group">      
                <input type="text" required name="{{ config('auth.providers.users.field','email') }}" id="email" value="{{ old('email') }}" class="cssinput">
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input">{{ trans('adminlte_lang::message.email') }}</label>
              </div>
              <div class="group"> 
                <div class="password">  
                  <input type="password" required id="password" name="password" class="cssinput" autocomplete="off">
                  <span class="glyphicon glyphicon-eye-open"></span>
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label class="title_input">{{ trans('adminlte_lang::message.password') }}</label>
                </div>
              </div>
              <div class="group">
                <div class="col-xs-12 text-left" style="margin-bottom: 1em">
                  <input type="checkbox" name="terms">
                  <label>{{ trans('adminlte_lang::message.remember') }}</label> 
                </div>
              </div>
              <br>
              <div class="group">
                <div class="col-xs-12">
                  <button type="submit" class="btn-primary-login btn btn-block" style="font-size: 1.5em; background-color: #1e8cc7; border-color: #1e8cc7;">{{ trans('adminlte_lang::message.buttonsign') }}</button>
                </div><!-- /.col -->
              </div>
            </form>
            <label style="font-size: .9em ">¿Has olvidado tu contraseña? <a style="color: #385fa4" data-toggle="modal" href="#RecoverPassword">{{ trans('adminlte_lang::message.forgotpassword') }}</a></label><br>
            <a href="{{ url('/register') }}"class="text-center"><h4 style="color: #55b7a4;"><strong>Registro de Empresa</strong></h4></a>
            <div class="col-md-12">             
             <br>
           </div>
         </div><!-- /.form-box -->
       </div><!-- /.register-box -->
     </div>
   </div>
 </div>
 @include('adminlte::layouts.partials.scripts_auth')
 @include('adminlte::auth.terms')
 @include('adminlte::auth.recoverpassword')

 <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
  increaseArea: '20%' // optional
});
  });
</script>

<script>
    ProgressDialogModal.InitModal($("#app"));
</script>

<style type="text/css">

  .password{
    position: relative;
  }
  .password .glyphicon,#password2 .glyphicon {
    display:none;
    right: 15px;
    position: absolute;
    top: 12px;
    cursor:pointer;
  }
</style>

</body>

@endsection