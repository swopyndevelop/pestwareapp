@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <!-- START MODAL INDEX JOB CENTER -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="indexJobCenter" role="dialog" aria-labelledby="indexJobCenter" tabindex="-1" style="overflow-y: scroll;">
            <div class="row">
                <div class="col-md-12">
                    <!--Default box-->
                    <div class="modal-dialog" role="document" style="width: 90%">

                        <div class="content modal-content">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal"
                                                aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <h4 class="modal-title text-center col-lg-12 text-primary" id="modalTitle">
                                        Sucursal ->{{ $profileJobCenter->name }}</h4>
                                    <input type="number" id="jobCenterId" value="{{ $jobCenterId }}" hidden>

                                </div>

                                <div class="box-body">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#company" aria-controls="company" role="tab" data-toggle="tab">Empresa</a></li>
                                        <li role="presentation"><a href="#address" aria-controls="address" role="tab" data-toggle="tab">Domicilio</a></li>
                                        <li role="presentation"><a href="#mails" aria-controls="mails" role="tab" data-toggle="tab">Emails y Whatsapp</a></li>
                                        <li role="presentation"><a href="#taxes" aria-controls="taxes" role="tab" data-toggle="tab">Impuestos</a></li>
                                        @if($company->id_code_country != 2)
                                        <li role="presentation"><a href="#labels" aria-controls="labels" role="tab" data-toggle="tab">Etiquetas Personalizadas</a></li>
                                        @endif
                                        @if($company->id_code_country == 2)
                                            <li role="presentation"><a href="#billing" aria-controls="billing" role="tab" data-toggle="tab">Facturación</a></li>
                                        @endif
                                        @if($company->id == 220 || $company->id == 503  || $company->id == 37)
                                            <li role="presentation"><a href="#attendance" aria-controls="billing" role="tab" data-toggle="tab">Reloj Checador</a></li>
                                        @endif
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <br>
                                        <div role="tabpanel" class="tab-pane active" id="company">
                                            <div class="row container-fluid">
                                                <div class="col-md-4">
                                                    <h5 for="health_manager" class="form-control-label">Responsable Sanitario:</h5>
                                                    <input type="text" name="health_manager" id="health_manager" class="form-control" value="{{ $profileJobCenter->health_manager }}">
                                                    <h5 for="business_name" class="form-control-label">Razón Social:</h5>
                                                    <input type="text" name="business_name" id="business_name" class="form-control" value="{{ $profileJobCenter->business_name }}">
                                                    <h5 for="email_personal" class="form-control-label">Correo:</h5>
                                                    <input type="text" name="email_personal" id="email_personal" class="form-control" value="{{ $profileJobCenter->email_personal }}">
                                                    <h5 for="rfc" class="form-control-label">{{$profileJobCenter->rfc_country}}:</h5>
                                                    @if($profileJobCenter->rfc === '')
                                                        <input type="text" name="rfc" id="rfc" class="form-control" value="XXX-XXX">
                                                    @else
                                                        <input type="text" name="rfc" id="rfc" class="form-control" value="{{ $profileJobCenter->rfc }}">
                                                    @endif
                                                    <h5 for="license" class="form-control-label">Licencia No:</h5>
                                                    <input type="text" name="license" id="license" class="form-control" value="{{ $profileJobCenter->license }}">
                                                    <h5 for="facebook_personal" class="form-control-label">Facebook</h5>
                                                    <input type="text" name="facebook_personal" id="facebook_personal" class="form-control" value="{{ $profileJobCenter->facebook_personal }}">
                                                </div>
                                                <div class="col-md-4">
                                                    <h5 for="web_page" class="form-control-label">Página Web</h5>
                                                    <input type="text" name="web_page" id="web_page" class="form-control" value="{{ $profileJobCenter->web_page }}">
                                                    <h5 for="messenger_personal" class="form-control-label">Messenger:</h5>
                                                    <input type="text" name="messenger_personal" id="messenger_personal" class="form-control" value="{{ $profileJobCenter->messenger_personal }}">
                                                    <h5 for="whatsapp_personal" class="form-control-label">Teléfono Whatsapp:</h5>
                                                    <input type="text" name="whatsapp_personal" id="whatsapp_personal" class="form-control" value="{{ $profileJobCenter->whatsapp_personal }}">
                                                    <h5 for="cellphoneGeneral" class="form-control-label">Teléfono:</h5>
                                                    <input type="text" name="cellphoneGeneral" id="cellphoneGeneral" class="form-control" value="{{ $profileJobCenter->cellphone }}">
                                                    @if ($pdfOrImageExplode != null)
                                                        <h5 for="" class="form-control-label">Licencia Sanitaria Actual</h5>
                                                        @if ($pdfOrImageExplode == 'pdf')
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 3.5em;color:red"></i>
                                                        @else
                                                            <div style="max-height: 700px">
                                                                <img src="{{ env('URL_STORAGE_FTP').$profileJobCenter->sanitary_license }}"
                                                                     alt="" width="100%"
                                                                     style="border: 3px solid rgba(0, 0, 0, 0.3); border-radius: 3px; max-height: 700px">
                                                            </div>
                                                        @endif
                                                    @endif
                                                    <h5 for="form" class="form-control-label">Licencia Sanitaria</h5>
                                                    <form action="{{ route('add_sanitary_license') }}" method="POST"
                                                          role="form" enctype="multipart/form-data" class="dropzone"
                                                          id="my-dropzone" name="minilogo">
                                                        {{ csrf_field() }}
                                                        <input type="number" id="jobCenterIdSanitaryLicense" name="jobCenterIdSanitaryLicense" value="{{ $jobCenterId }}" hidden>
                                                        <input type="number" id="sanitaryLicense" name="sanitaryLicense" hidden>
                                                        <div class="dz-message" data-dz-message>
                                                            <i class="glyphicon glyphicon-cloud-upload" style="font-size:10px"></i>
                                                            <br/>
                                                            <span>Arrastra y suelta tu licencia sanitaria.</span>
                                                        </div>
                                                    </form>

                                                </div>
                                                <div class="col-md-4">

                                                    <h5 for="warning_service" class="form-control-label">Precauciones y
                                                        Recomendaciones</h5>
                                                    <div>
                                                        <textarea class="form-control" cols="50" rows="7" id="warning_service"
                                                          name="warning_service" value=""
                                                          placeholder="">{{ $profileJobCenter->warning_service }}</textarea>
                                                    </div>
                                                    <h5 for="contracts_service" class="form-control-label">Contrato de
                                                        Prestación de Servicio</h5>
                                                    <div>
                                                        <textarea class="form-control" cols="50" rows="7" id="contract_service"
                                                          name="contract_service" value=""
                                                          placeholder="">{{ $profileJobCenter->contract_service }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="text-center">
                                                    <button class="btn btn-primary" type="button"
                                                            id="buttonCompanySave">Guardar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="address">
                                            <div class="row container-fluid">
                                                <div class="col-md-6">
                                                    <label for="street">Calle</label>
                                                    <input type="text" class="form-control" id="street"
                                                           placeholder="Ejemplo: Sierra de la canela"
                                                           value="@if($isAddress){{ $address->street }}@endif">
                                                    <label for="num_ext">Número</label>
                                                    <input type="text" class="form-control" id="num_ext"
                                                           placeholder="Ejemplo: 152"
                                                           value="@if($isAddress){{ $address->num_ext }}@endif">
                                                    <label for="location">Colonia</label>
                                                    <input type="text" class="form-control" id="location"
                                                           placeholder="Ejemplo: Bosques del Prado Norte"
                                                           value="@if($isAddress){{ $address->location }}@endif">

                                                </div>
                                                <div class="col-md-6">
                                                    <label for="municipality">Municipio</label>
                                                    <input type="text" class="form-control" id="municipality"
                                                           placeholder="Ejemplo: Calvillo"
                                                           value="@if($isAddress){{ $address->municipality }}@endif">
                                                    <label for="state">Estado</label>
                                                    <input type="text" class="form-control" id="state"
                                                           placeholder="Ejemplo: Aguascalientes"
                                                           value="@if($isAddress){{ $address->state }}@endif">
                                                    <label for="zip_code">Código Postal</label>
                                                    <input type="text" class="form-control" id="zip_code"
                                                           placeholder="Ejemplo: 20157"
                                                           value="@if($isAddress){{ $address->zip_code }}@endif" required>
                                                </div>

                                            </div>

                                            <div class="modal-footer">
                                                <div class="text-center">
                                                    <button class="btn btn-primary" type="submit" id="saveAddressDataJobCenter">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mails">
                                            <div class="col-md-12">
                                                @include('adminlte::layouts.partials.session-messages')

                                                <form action="{{route('save_config_mail')}}" method="POST" id="form" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="number" id="jobCenterMail" name="jobCenterMail" value="{{ $profileJobCenter->id }}" hidden>
                                                    <input type="number" id="jobCenterRoute" name="jobCenterRoute" value="{{ $jobCenterId }}" hidden>
                                                    <div class="col-md-6">
                                                        <h5 for="Banner" class="form-control-label">Banner</h5>
                                                        <div>
                                                            @if ($email)
                                                                @if($email->banner)
                                                                    <input type="file" name="banner" id="banner" accept="image/*" class="form-control" value="{{ $email->banner }}">
                                                                @else
                                                                    <input type="file" name="banner" id="banner" accept="image/*" class="form-control" required>
                                                                @endif
                                                            @else
                                                                <input type="file" name="banner" id="banner" accept="image/*" class="form-control" required>
                                                            @endif
                                                        </div>
                                                        <h5 class="form-control-label">Seleccionar Banner para Correo de(1200x400 pixel):</h5>
                                                            <div>
                                                                @if ($email)
                                                                    <img class="rounded mx-auto d-block" alt="Responsive image" id="imgBanner" src="{{ env('URL_STORAGE_FTP').$email->banner }}" width="100%" height="100%" style="border: 3px solid rgba(0, 0, 0, 0.3); border-radius: 3px">
                                                                @else
                                                                    <img class="rounded mx-auto d-block" alt="Responsive image" id="imgBanner" src="" width="100%" height="100%" style="border: 3px solid rgba(0, 0, 0, 0.3); border-radius: 3px">
                                                                @endif
                                                            </div>
                                                        <h5 for="description" class="form-control-label">Texto del e-mail</h5>
                                                        <div>
                                                            @if ($email)
                                                            <textarea class="form-control" cols="50" rows="10" id="description" name="description" value="" required
                                                            placeholder="Queremos encontrar las mejores soluciones para ti en tu hogar o negocio. Gracias por confiar en nosotros, tenemos como objetivo cuidar y mejorar tu calidad de vida. Nuestros programas de control de plagas buscan optimizar el medio ambiente donde vives a través de soluciones que no pongan en riesgo tu salud.continuación encontrarás adjunta la cotización del servicio solicitado.">{{ $email->description }}</textarea>
                                                            @else
                                                                <textarea class="form-control" cols="50" rows="10" id="description" name="description" value="" required
                                                                          placeholder="Queremos encontrar las mejores soluciones para ti en tu hogar o negocio. Gracias por confiar en nosotros, tenemos como objetivo cuidar y mejorar tu calidad de vida. Nuestros programas de control de plagas buscan optimizar el medio ambiente donde vives a través de soluciones que no pongan en riesgo tu salud.continuación encontrarás adjunta la cotización del servicio solicitado."></textarea>
                                                            @endif
                                                        </div>

                                                        <h5 for="Banner" class="form-control-label">Correo Seguimiento</h5>
                                                        <div>
                                                            @if ($email)
                                                                @if($email->email_tracing)
                                                                    <input class="formulario__input form-control" type="email" name="emailTracing" id="emailTracing" value="{{ $email->email_tracing }}">
                                                                @else
                                                                    <input class="formulario__input form-control" type="email" name="emailTracing" id="emailTracing" value="">
                                                                @endif
                                                            @else
                                                                <input class="formulario__input form-control" type="text" name="emailTracing" id="emailTracing" value="">
                                                            @endif
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">
                                                        <h5 for="reminderWhatsapp" class="form-control-label">Recordatorio Whatsapp</h5>
                                                        <div class="panel panel-info">
                                                            <div class="panel-heading"><b>Instrucciones de uso</b></div>
                                                            <div class="panel-body">
                                                                Aquí puedes construir una plantilla personalizada para enviar recordatorios por WhatsApp;
                                                                Te puedes apoyar usando nuestras variables para unir tus frases junto con datos dinámicos como por ejemplo:
                                                                <br><br>
                                                                Hola <b>[customer_name]</b>, Te escribo de <b>[company_name]</b>, te recordamos que el día <b>[service_date]</b> a las <b>[service_hour]</b> tenemos programado tu servicio
                                                                de fumigación contra: <b>[service_plagues]</b> el cual tiene un costo de <b>[service_amount]</b> Si no estas en condiciones de recibirnos o deseas cambiar la
                                                                cita por favor avisános, tienes 24 horas para hacer cambios o cancelación ya que de lo contrario se tendrá que pagar el servicio completo para
                                                                posteriormente reprogramarte. RECUERDA: Al solicitar un servicio con fecha y hora es un contrato de palabra y tenemos el compromiso de reservar
                                                                este horario solo para ti. En caso de no contestar se considera el servicio confirmado. <b>[service_folio]</b> Agente: <b>[service_user]</b>.
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <table class="table table-hover">
                                                                <thead class="bg-primary">
                                                                    <tr>
                                                                        <th>Campo</th>
                                                                        <th>Valor a copiar</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Nombre del cliente</td>
                                                                        <td>[customer_name]</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Nombre de tu empresa</td>
                                                                        <td>[company_name]</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Fecha del servicio</td>
                                                                        <td>[service_date]</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Hora del servicio</td>
                                                                        <td>[service_hour]</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Plagas del servicio</td>
                                                                        <td>[service_plagues]</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Costo del servicio</td>
                                                                        <td>[service_amount]</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Folio del servicio</td>
                                                                        <td>[service_folio]</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Agente / Usuario</td>
                                                                        <td>[service_user]</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div>
                                                            @if($email)
                                                            <textarea class="form-control" cols="50" rows="10" id="reminderWhatsapp" name="reminderWhatsapp" value="" required
                                                                placeholder="Recordatorio antes de las 24 horas.">{{ $email->reminder_whatsapp }}</textarea>
                                                            @else
                                                                <textarea class="form-control" cols="50" rows="10" id="reminderWhatsapp" name="reminderWhatsapp" value="" required
                                                                          placeholder="Recordatorio antes de las 24 horas."></textarea>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="text-center">
                                                        <button type="submit" class="btn btn-primary btn-md" name="SaveMail" id="SaveMail" style="margin: 10px">Guardar</button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="taxes">
                                            <div class="container">
                                                <div class="col-md-3 col-lg-3">
                                                    <div class="formulario__grupo" id="grupo__iva_country">
                                                        <h5 for="ivaProfileJobCenter" class="form-control-label">
                                                            @if($labels['iva'] != null) {{ $labels['iva']->label_spanish }}
                                                            @else IVA
                                                            @endif
                                                        </h5>
                                                        <input class="formulario__input form-control" type="text" name="ivaProfileJobCenter" id="ivaProfileJobCenter" value="{{$profileJobCenter->iva}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                    </div>
                                                </div>
<!--                                                <div class="col-md-3 col-lg-3">
                                                    <div class="formulario__grupo" id="grupo__isr_country">
                                                        <h5 for="isr_country" class="form-control-label">ISR</h5>
                                                        <input class="formulario__input form-control" type="text" name="isr_country" id="isr_country" value="0" disabled>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="modal-footer">
                                                <div class="formulario__mensaje" id="formulario__mensaje">
                                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                                </div>
                                                <br>
                                                <div class="text-center formulario__grupo formulario__grupo-btn-enviar">
                                                    <button class="btn btn-primary" type="submit" id="buttonTaxSave">Guardar</button>
                                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="labels">
                                            <div class="container">
                                                <div class="col-md-3 col-lg-3">
                                                    <div class="formulario__grupo" id="grupo__iva_country">
                                                        <h5 for="streetLabel" class="form-control-label">Calle</h5>
                                                        @if($labels['street'] != null)
                                                            <input class="formulario__input form-control" type="text" name="streetLabel" id="streetLabel" value="{{$labels['street']->label_spanish}}">
                                                            <input type="hidden"  id="streetId" value="{{$labels['street']->idLabel}}">
                                                        @else
                                                            <input class="formulario__input form-control" type="text" name="streetLabel" id="streetLabel" value="Calle">
                                                            <input type="hidden"  id="streetId" value="1">
                                                        @endif
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                    </div>

                                                    <div class="formulario__grupo" id="grupo__iva_country">
                                                        <h6 for="colonyLabel" class="form-control-label">Colonia</h6>
                                                        @if($labels['colony'] != null)
                                                        <input class="formulario__input form-control" type="text" name="colonyLabel" id="colonyLabel" value="{{$labels['colony']->label_spanish}}">
                                                        <input type="hidden"  id="colonyId" value="{{$labels['colony']->idLabel}}">
                                                        @else
                                                            <input class="formulario__input form-control" type="text" name="colonyLabel" id="colonyLabel" value="Colonia">
                                                            <input type="hidden"  id="colonyId" value="2">
                                                        @endif
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                    </div>

                                                    <div class="formulario__grupo" id="grupo__iva_country">
                                                        <h6 for="stateLabel" class="form-control-label">Estado</h6>
                                                        @if($labels['state'] != null)
                                                        <input class="formulario__input form-control" type="text" name="stateLabel" id="stateLabel" value="{{$labels['state']->label_spanish}}">
                                                        <input type="hidden"  id="stateId" value="{{$labels['state']->idLabel}}">
                                                        @else
                                                            <input class="formulario__input form-control" type="text" name="stateLabel" id="stateLabel" value="Estado">
                                                            <input type="hidden"  id="stateId" value="3">
                                                        @endif
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-lg-3">
                                                    <div class="formulario__grupo" id="grupo__iva_country">
                                                        <h6 for="rfcCountryProfileJobCenter" class="form-control-label">Registro Federal Contribuyente (RFC) México</h6>
                                                        <input class="formulario__input form-control" type="text" name="rfcCountryProfileJobCenter" id="rfcCountryProfileJobCenter" value="{{$profileJobCenter->rfc_country}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                    </div>

                                                    <div class="formulario__grupo" id="grupo__iva_country">
                                                        <h5 for="streetLabel" class="form-control-label">Impuesto Sobre la Renta (IVA) México</h5>
                                                        @if($labels['iva'] != null)
                                                            <input class="formulario__input form-control" type="text" name="ivaLabel" id="ivaLabel" value="{{$labels['iva']->label_spanish}}">
                                                            <input type="hidden"  id="ivaId" value="{{$labels['iva']->idLabel}}">
                                                        @else
                                                            <input class="formulario__input form-control" type="text" name="ivaLabel" id="ivaLabel" value="">
                                                            <input type="hidden"  id="ivaId" value="4">
                                                        @endif
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="formulario__mensaje" id="formulario__mensaje">
                                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                                </div>
                                                <br>
                                                <div class="text-center formulario__grupo formulario__grupo-btn-enviar">
                                                    <button class="btn btn-primary" type="submit" id="buttonLabelsSave">Guardar</button>
                                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="billing">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <span class="fa fa-address-card-o" aria-hidden="true"></span>
                                                            <b>Datos Generales del Emisor</b>
                                                            <span class="badge pull-right">PASO 1</span>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-6 col-lg-6">
                                                                        <div class="formulario__grupo" id="">
                                                                            <h5 for="ivaBilling" class="form-control-label">IVA*</h5>
                                                                            @if($profileJobCenterBilling != null)
                                                                                <input class="formulario__input form-control" type="text" name="ivaBilling" id="ivaBilling" value="{{ $profileJobCenterBilling->iva }}">
                                                                            @else <input class="formulario__input form-control" type="text" name="ivaBilling" id="ivaBilling" value="">
                                                                            @endif
                                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                            <p class="formulario__input-error">Números con decimal.</p>
                                                                        </div>
                                                                        <div class="formulario__grupo" id="">
                                                                            <h5 for="emailBilling" class="form-control-label">Correo*</h5>
                                                                            @if($profileJobCenterBilling != null)
                                                                                <input class="formulario__input form-control" type="text" name="emailBilling" id="emailBilling" value="{{ $profileJobCenterBilling->email }}">
                                                                            @else <input class="formulario__input form-control" type="text" name="emailBilling" id="emailBilling" value="">
                                                                            @endif
                                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                            <p class="formulario__input-error">Números con decimal.</p>
                                                                        </div>
                                                                        <div class="formulario__grupo" id="">
                                                                            <h5 for="addressBilling" class="form-control-label">Calle*</h5>
                                                                            @if($profileJobCenterBilling != null)
                                                                                <input class="formulario__input form-control" type="text" name="addressBilling" id="addressBilling" value="{{ $profileJobCenterBilling->address }}">
                                                                            @else <input class="formulario__input form-control" type="text" name="addressBilling" id="addressBilling" value="">
                                                                            @endif
                                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                            <p class="formulario__input-error">Números con decimal.</p>
                                                                        </div>
                                                                        <div class="formulario__grupo" id="">
                                                                            <h5 for="addressNumberBilling" class="form-control-label">Número*</h5>
                                                                            @if($profileJobCenterBilling != null)
                                                                                <input class="formulario__input form-control" type="text" name="addressNumberBilling" id="addressNumberBilling" value="{{ $profileJobCenterBilling->address_number }}">
                                                                            @else <input class="formulario__input form-control" type="text" name="addressNumberBilling" id="addressNumberBilling" value="">
                                                                            @endif
                                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                            <p class="formulario__input-error">Números con decimal.</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-6">
                                                                        <div class="formulario__grupo" id="">
                                                                            <h5 for="colonyBilling" class="form-control-label">Colonia*</h5>
                                                                            @if($profileJobCenterBilling != null)
                                                                                <input class="formulario__input form-control" type="text" name="colonyBilling" id="colonyBilling" value="{{ $profileJobCenterBilling->colony }}">
                                                                            @else <input class="formulario__input form-control" type="text" name="colonyBilling" id="colonyBilling" value="">
                                                                            @endif
                                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                            <p class="formulario__input-error">Números con decimal.</p>
                                                                        </div>
                                                                        <div class="formulario__grupo" id="">
                                                                            <h5 for="municipalityBilling" class="form-control-label">Municipio*</h5>
                                                                            @if($profileJobCenterBilling != null)
                                                                                <input class="formulario__input form-control" type="text" name="municipalityBilling" id="municipalityBilling" value="{{ $profileJobCenterBilling->municipality }}">
                                                                            @else <input class="formulario__input form-control" type="text" name="municipalityBilling" id="municipalityBilling" value="">
                                                                            @endif
                                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                            <p class="formulario__input-error">Números con decimal.</p>
                                                                        </div>
                                                                        <div class="formulario__grupo" id="">
                                                                            <h5 for="stateBilling" class="form-control-label">Estado*</h5>
                                                                            @if($profileJobCenterBilling != null)
                                                                                <input class="formulario__input form-control" type="text" name="stateBilling" id="stateBilling" value="{{ $profileJobCenterBilling->state }}">
                                                                            @else <input class="formulario__input form-control" type="text" name="stateBilling" id="stateBilling" value="">
                                                                            @endif
                                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                            <p class="formulario__input-error">Números con decimal.</p>
                                                                        </div>
                                                                        <div class="formulario__grupo" id="">
                                                                            <h5 for="postalCodeBilling" class="form-control-label">Código Postal*</h5>
                                                                            @if($profileJobCenterBilling != null)
                                                                                <input class="formulario__input form-control" type="text" name="postalCodeBilling" id="postalCodeBilling" value="{{ $profileJobCenterBilling->code_postal }}">
                                                                            @else <input class="formulario__input form-control" type="text" name="postalCodeBilling" id="postalCodeBilling" value="">
                                                                            @endif
                                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                            <p class="formulario__input-error">Números con decimal.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-12 col-lg-12">
                                                                        <h5>Régimen Fiscal*</h5>
                                                                        <select name="regimeFiscal" id="regimeFiscal" class="form-control">
                                                                        </select>
                                                                        <h5>Texto Correo (factura por correo a cliente)</h5>
                                                                        <textarea class="form-control" rows="4" id="descriptionBillingEmail" name="descriptionBillingEmail">@if($profileJobCenterBilling != null){{ $profileJobCenterBilling->text_mail }}@endif</textarea>
                                                                        <br>
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <br>
                                                                        <button class="btn btn-primary" type="button" id="buttonBillingSave">Guardar Datos Emisor</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="panel panel-primary" @if($profileJobCenterBilling == null) style="display: none;" @elseif($profileJobCenterBilling != null) @if($profileJobCenterBilling->rfc == "") style="display: none;" @endif @endif id="panelStep2">
                                                        <div class="panel-heading">
                                                            <span class="fa fa-plug" aria-hidden="true"></span>
                                                            <b>Autenticación con el SAT</b>
                                                            <span class="badge pull-right">PASO 2</span>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="formulario__grupo" id="">
                                                                        <h5 for="rfcBilling" class="form-control-label">RFC*</h5>
                                                                        @if($profileJobCenterBilling != null)
                                                                            <input class="formulario__input form-control" type="text" name="rfcBilling" id="rfcBilling" value="{{ $profileJobCenterBilling->rfc }}">
                                                                        @else <input class="formulario__input form-control" type="text" name="rfcBilling" id="rfcBilling" value="">
                                                                        @endif
                                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                                    </div>
                                                                    <div class="formulario__grupo" id="">
                                                                        <h5 for="passwordBilling" class="form-control-label">Contraseña llave privada*</h5>
                                                                        <input class="formulario__input form-control" type="password" name="passwordBilling" id="passwordBilling" value="">
                                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                                        <p class="formulario__input-error">Números con decimal.</p>
                                                                    </div>
                                                                    <h5 for="form" class="form-control-label">Certificado (.Cer)</h5>
                                                                    <form action="{{ route('add_file_certificate_billing') }}" method="POST"
                                                                          role="form" enctype="multipart/form-data" class="dropzone"
                                                                          id="my-dropzone" name="minilogo">
                                                                        {{ csrf_field() }}
                                                                        <input type="number" id="jobCenterIdCertificate" name="jobCenterIdCertificate" value="{{ $jobCenterId }}" hidden>
                                                                        <input type="number" id="fileCertificate" name="fileCertificate" hidden>
                                                                        <div class="dz-message" data-dz-message>
                                                                            <i class="fa fa-file-text-o fa-rotate-90" style="font-size:75px; color: #3097d1;"></i>
                                                                            <br>
                                                                            <span>Arrastra y suelta tu Certificado.</span>
                                                                        </div>
                                                                    </form>
                                                                    <h5 for="form" class="form-control-label">Llave Privada (.Key)</h5>
                                                                    <form action="{{ route('add_file_key_billing') }}" method="POST"
                                                                          role="form" enctype="multipart/form-data" class="dropzone"
                                                                          id="my-dropzone" name="minilogo">
                                                                        {{ csrf_field() }}
                                                                        <input type="number" id="jobCenterIdKey" name="jobCenterIdKey" value="{{ $jobCenterId }}" hidden>
                                                                        <input type="number" id="fileKey" name="fileKey" hidden>
                                                                        <div class="dz-message" data-dz-message>
                                                                            <i class="fa fa-file-archive-o fa-rotate-90" style="font-size:75px; color: #3097d1;"></i>
                                                                            <br>
                                                                            <span>Arrastra y suelta tu Llave privada.</span>
                                                                        </div>
                                                                    </form>
                                                                    <div class="text-center">
                                                                        <br>
                                                                        <div style="background-color: #16A085; @if($profileJobCenterBilling != null) @if($profileJobCenterBilling->status_connection_sat == 0) display: none; @endif @endif" id="divStatusConnectionSat">
                                                                            <p class="text-bold" style="color: white;" id="text-message-response">Conexión con el SAT establecida.</p>
                                                                        </div>
                                                                        @if($profileJobCenterBilling != null)
                                                                            @if($profileJobCenterBilling->status_connection_sat == 1)
                                                                                <button class="btn btn-danger text-center" type="button" id="deleteSat" style="margin-top: 50px;">Desconectarme con el SAT</button>
                                                                                <button class="btn btn-primary text-center" type="button" id="validateSat" style="margin-top: 20px; display: none;">Autenticarme con el SAT</button>
                                                                            @else
                                                                                <button class="btn btn-danger text-center" type="button" id="deleteSat" style="margin-top: 20px; display: none;">Desconectarme con el SAT</button>
                                                                                <button class="btn btn-primary text-center" type="button" id="validateSat" style="margin-top: 20px;">Autenticarme con el SAT</button>
                                                                            @endif
                                                                        @else
                                                                            <button class="btn btn-danger text-center" type="button" id="deleteSat" style="margin-top: 50px; display: none;">Desconectarme con el SAT</button>
                                                                            <button class="btn btn-primary text-center" type="button" id="validateSat" style="margin-top: 20px;">Autenticarme con el SAT</button>
                                                                        @endif
                                                                        <br>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel panel-primary" @if($profileJobCenterBilling == null) style="display: none" @endif>
                                                        <div class="panel-heading">
                                                            <span class="fa fa-certificate" aria-hidden="true"></span>
                                                            <b>Configuración de Folios</b>
                                                            <span class="badge pull-right">PASO 3</span>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    @if($tree->parent == '#')
                                                                    <div class="col-md-6 col-lg-6">
                                                                        <div class="formulario__grupo" id="">
                                                                            <a class="btn btn-primary-outline btn-sm pull-right" data-toggle="modal" href="#modalPurchaseFolios"><i class="fa fa-cart-plus" aria-hidden="true"></i> Comprar Folios</a>
                                                                            <h5 for="ivaBilling" class="form-control-label text-primary">Distribución de Folios:</h5>
                                                                            <label class="radio-inline"><input type="radio" name="optRadio" id="optConsume" @if($company->is_folios_consumed == 1) checked @endif>Consumo Automático</label>
                                                                            <label class="radio-inline"><input type="radio" name="optRadio" id="optManual" @if($company->is_folios_consumed == 0) checked @endif>Administrar por Centro de Trabajo</label>
                                                                            <div class="col-md-12" id="divFolios" @if($company->is_folios_consumed == 0) style="display: none;" @endif>
                                                                                <div class="col-md-12">
                                                                                    <label for="folios">Folios Disponibles:</label>
                                                                                    @if($profileJobCenterBilling != null)
                                                                                        <input class="formulario__input form-control" type="number" name="folios" id="folios" value="{{ $profileJobCenterBilling->folios }}" disabled>
                                                                                    @else <input class="formulario__input form-control" type="number" name="folios" id="folios" value="0" disabled>
                                                                                    @endif
                                                                                    <br>
                                                                                </div>
                                                                            </div>
                                                                            <div id="divSelects" class="col-md-12" @if($company->is_folios_consumed == 1) style="display: none;" @endif>
                                                                                <div class="col-md-6">
                                                                                    <label for="origin">Origen</label>
                                                                                    <select id="origin" class="form-control"></select>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <label for="source">Destino</label>
                                                                                    <select id="source" class="form-control"></select>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <label for="origin">Folios disponibles</label>
                                                                                    <input type="number" class="form-control" disabled id="quantityFoliosAvailable">
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <label for="source">Folios a traspasar</label>
                                                                                    <input type="number" class="form-control" id="quantityFolios">
                                                                                </div>
                                                                                <div class="col-md-12 text-center">
                                                                                    <br>
                                                                                    <button class="btn btn-primary" type="button" id="transferFolios">Traspasar Folios</button>
                                                                                    <br>
                                                                                </div>
                                                                            </div>
                                                                            <h5 class="form-control-label text-primary">Compra automática de folios:</h5>
                                                                            <label class="checkbox-inline"><input type="checkbox" name="purchaseFolios" id="purchaseFolios" @if($company->is_buy_folios == 1) checked @endif>Autorizo a PestWare App realizar compra de folios de manera automática.</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-6">
                                                                        <table class="table table-hover" id="tableMailAccounts">
                                                                            <thead>
                                                                            <tr>
                                                                                <th class="text-center">Centro de Trabajo</th>
                                                                                <th class="text-center">Folios Disponibles</th>
                                                                                <th class="text-center">Folios Consumidos</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody id="tableBodyMailAccounts">

                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    @else
                                                                        <div class="col-md-12 col-lg-12">
                                                                            <div class="formulario__grupo" id="">
                                                                                @if($profileJobCenterBilling != null)
                                                                                <span class="badge">Folios Disponibles: {{ $profileJobCenterBilling->folios }}</span>
                                                                                <span class="badge">Folios Consumidos: {{ $profileJobCenterBilling->folios_consumed }}</span>
                                                                                @endif
                                                                                <hr>
                                                                                <h5 class="form-control-label text-primary">*La compra y administración de folios es gestionada por tu administrador de la instancia padre.</h5>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <div class="formulario__mensaje" id="formulario__mensaje">
                                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                                </div>
                                                <br>
                                                <div class="text-center formulario__grupo formulario__grupo-btn-enviar">
                                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="attendance">
                                            <div class="container">
                                                <div class="col-md-3 col-lg-3">
                                                    <div class="formulario__grupo" id="grupo__iva_country">
                                                        <h5 for="ssid" class="form-control-label">Nombre de la RED WiFi
                                                        </h5>
                                                        <input class="formulario__input form-control" type="text" name="ssid" id="ssid" value="{{$profileJobCenter->ssid}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="text-center formulario__grupo formulario__grupo-btn-enviar">
                                                    <button class="btn btn-primary" type="submit" id="buttonSsidSave">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL INDEX JOB CENTER -->
    @include('vendor.adminlte.tree._modalPurchaseFolios')
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/jobcenter.js') }}"></script>
    <script>
        $("#indexJobCenter").modal();
        $("#indexJobCenter").on('hidden.bs.modal', function () {
            window.location.href = '{{route("tree_jobcenter")}}';
        });
        //
        Dropzone.options.myDropzone = {
            maxFilesize: 5, //mb- Image files not above this size
            uploadMultiple: false, // set to true to allow multiple image uploads
            parallelUploads: 2, //all images should upload same time
            maxFiles: 1, //number of images a user should upload at an instance
            acceptedFiles: ".pdf,.cer,.key", //allowed file types, .pdf or anyother would throw error
            addRemoveLinks: true, // add a remove link underneath each image to
            autoProcessQueue: true, // Prevents Dropzone from uploading dropped files immediately
            success: function (file, response) {
                if (response.code == 301){
                   showToast('warning','Archivo inválido', response.message)
                }
                if (response.code == 201){
                    showToast('success','Archivo Guardado', response.message)
                }
                if (response.code == 500){
                    showToast('error','Error',response.message)
                }
            }
        };

        function showToast(type, title, message, path) {
            if (type === 'success-redirect') {
                toastr.success(message, title);
                setTimeout(() => {
                    window.location.href = route(path);
                }, 5000);
            } else if (type === 'success') toastr.success(message, title)
            else if (type === 'info') toastr.info(message, title)
            else if (type === 'warning') toastr.warning(message, title)
            else if (type === 'error') toastr.error(message, title)
        }
    </script>
@endsection