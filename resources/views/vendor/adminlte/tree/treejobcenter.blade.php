@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="content container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h4 class="titleCenter margin-mobile">ESTRUCTURA DE CENTROS DE TRABAJO</h4>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-lg-8">
							@if($treeJobCenterMain->parent == '#')
								<button type="button" class="btn btn-primary" id="saveTree"><i class="glyphicon glyphicon-floppy-save"></i> Guardar</button>
							@else
								<a href="{{ route('data_job_center',\Vinkla\Hashids\Facades\Hashids::encode($treeJobCenterMain->id_inc)) }}" class="btn btn-primary-dark">Datos Sucursal</a>
							@endif

						</div>
						<div class="col-lg-4 margin-mobile">
							<div class="input-group">
								<input type="text" value=""  id="demo_q" placeholder="Buscar" class="form-control" />
								<span class="input-group-btn">
									<button class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
								</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							@if($treeJobCenterMain->parent == '#')
								<div id="jstree_jobcenter" class="demo" style="margin-top:1em; min-height:200px;"></div>
							@endif
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>
@endsection

@section('personal-js')
	<script src="{{ asset('/js/jstree.js') }}"></script>
	<script src="{{ asset('/js/swopyn.js') }}"></script>
@endsection