@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <!-- START MODAL ADDRESS JOB CENTER -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="addressJobCenter" role="dialog" aria-labelledby="scheduleManual"
             tabindex="-1">
            <div class="row">
                <div class="col-md-12">
                    <!--Default box-->
                    <div class="modal-dialog modal-lg" role="document">

                        <div class="content modal-content">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <h4 class="modal-title text-center col-lg-12 text-primary" id="modalTitle">
                                        Domicilio de {{ $jobCenterName }}</h4>
                                    <input type="number" id="jobCenterId" value="{{ $jobCenterId }}" hidden>
                                    <input type="text" id="jobCenterName" value="{{ $jobCenterName }}" hidden>
                                </div>

                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label for="street">Calle</label>
                                                <input type="text" class="form-control" id="street" placeholder="Ejemplo: Sierra de la canela" value="@if($isAddress){{ $address->street }}@endif">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="zip_code">Código Postal</label>
                                                <input type="text" class="form-control" id="zip_code" placeholder="Ejemplo: 20157" value="@if($isAddress){{ $address->zip_code }}@endif">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label for="num_ext">Número</label>
                                                <input type="text" class="form-control" id="num_ext" placeholder="Ejemplo: 152" value="@if($isAddress){{ $address->num_ext }}@endif">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="municipality">Municipio</label>
                                                <input type="text" class="form-control" id="municipality"
                                                       placeholder="Ejemplo: Calvillo"
                                                       value="@if($isAddress){{ $address->municipality }}@endif">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label for="location">Colonia</label>
                                                <input type="text" class="form-control" id="location" placeholder="Ejemplo: Bosques del Prado Norte" value="@if($isAddress){{ $address->location }}@endif">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="state">Estado</label>
                                                <input type="text" class="form-control" id="state" placeholder="Ejemplo: Aguascalientes" value="@if($isAddress){{ $address->state }}@endif">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="text-center">
                                        <div class="row">
                                            <button class="btn btn-primary" type="button" id="saveAddressJobCenter">Guardar Domicilio</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL ADDRESS JOB CENTER -->
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/jobcenter.js') }}"></script>
    <script>
        $("#addressJobCenter").modal();
        $("#addressJobCenter").on('hidden.bs.modal', function() {
            window.location.href = '{{route("tree_jobcenter")}}';
        });
    </script>
@endsection
