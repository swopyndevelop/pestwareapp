@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h2 class="titleCenter">CENTROS DE TRABAJO-RESUMEN VIGENCIAS</h2>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						<i class="glyphicon glyphicon-search"></i>
						<input type="text" value="" data-table="tableSummary" class="search_employees" placeholder="Ingrese el número del mes. Ejemplo: -01-" size="40"/>
					</div>

				</div>
				<div class="table-responsive">
					<table class="table table-hover table table-striped" id="tableSummary">
						<thead>
							<tr>
								<td class="text-center" colspan="7" style="background-color: black;">
									<span style="color: white; font-weight: bold; font-size: 1.5em;">Sucursales - Vigencias</span>
											</td>
										</tr>
										<tr>
											<th class="text-center">Sucursal</th>
											<th class="text-center">Concepto</th>
											<th class="text-center">Vigencia</th>
										</tr>
									</thead>
									@foreach($jobcenters as $jobcenter)
									<tbody>
										<tr>
											<th colspan="3" class="text-center" style="background-color: #ff6aa0;">{{$jobcenter-> name}}</th>
										</tr>
										<tr>
											<td class="text-center">{{$jobcenter-> name}}</td>
											<td class="text-center">Contrato de renta</td>
											<td class="text-center">{{$jobcenter->validity_rent_contract }}</td>
										</tr>
										<tr>
											<td class="text-center">{{$jobcenter-> name}}</td>
											<td class="text-center">R-1 Hacienda</td>
											<td class="text-center">{{ $jobcenter->validity_hacienda }}</td>
										</tr>
										<tr>
											<td class="text-center">{{$jobcenter-> name}}</td>
											<td class="text-center">Permiso comercial</td>
											<td class="text-center">{{ $jobcenter->validity_commercial_permission }}</td>
										</tr>
										<tr>
											<td class="text-center">{{$jobcenter-> name}}</td>
											<td class="text-center">Última fumigación</td>
											<td class="text-center">{{ $jobcenter->validity_fumigation}}</td>
										</tr>
										<tr>
											<td class="text-center">{{$jobcenter-> name}}</td>
											<td class="text-center">Carga extintor</td>
											<td class="text-center">{{ $jobcenter->validity_extinguisher}}</td>
										</tr>
										<tr>
											<td class="text-center">{{$jobcenter-> name}}</td>
											<td class="text-center">Permiso salubridad</td>
											<td class="text-center">{{ $jobcenter->validity_salubrity}}</td>
										</tr>
									</tbody>
									@endforeach
								</table>

 						</div>
    				</div>
  				</div>

			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>

@endsection
