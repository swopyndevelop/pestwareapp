<!-- START MODAL CUSTOM INVOICE -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalPurchaseFolios" role="dialog" aria-labelledby="modalPurchaseFolios" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 95% !important; height: 95% !important;">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Adquirir Nuevos Folios</h4>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="pricing-table">
                                            <div class="container">
                                                <div class="block-heading">
                                                </div>
                                                <div class="row justify-content-md-center">
                                                    @foreach($plansBilling as $plan)
                                                        <div class="col-md-3 col-lg-3">
                                                            <div class="panel panel-primary text-center">
                                                                <div class="panel-heading">
                                                                    <h3 class="panel-title">{{ $plan->name }}</h3>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <br>
                                                                    <span class="text-primary text-bold" style="font-size: x-large;">$@convert($plan->price)</span> MXN
                                                                    <br><br>
                                                                    <span class="text-secondary text-bold"><span class="text-primary" style="font-size: large;">{{ $plan->quantity }}</span> @if($plan->quantity == 1)folio con única vigencia de 1 mes* @else folios con vigencia de 6 meses*@endif</span>
                                                                    <br><br><br>
                                                                    <span class="text-dark text-bold">$@convert($plan->price_folio)</span> / folio
                                                                    <br>
                                                                    <hr>
                                                                    <button class="btn btn-primary btn-block" id="purchaseFolios-{{ $plan->id }}"><i class="fa fa-cart-plus" aria-hidden="true"></i> Comprar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Comprar Después</button>
                                        <button class="btn btn-primary btn-sm" type="button" data-dismiss="modal">Finalizar Compra</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CUSTOM INVOICE -->
