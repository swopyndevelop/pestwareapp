<!-- START MODAL CUSTOM INVOICE -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalCustomInvoice" role="dialog" aria-labelledby="modalCustomInvoice" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 95% !important; height: 95% !important;">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Factura Personalizada</h4>
                            </div>
                            <div class="box-body">

                                <h5>Datos de la Factura:</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="control-label" for="customerNameCustom">Seleccionar Cliente:</label>
                                                    <div class="input-group">
                                                        <select class="form-control applicantsList-single" id="customer" style="font-weight: bold; width: 100%">
                                                            <option value="0" selected>Público en general</option>
                                                            @foreach($customers as $customer)
                                                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="input-group-btn">
                                                        <button data-toggle="modal" href="#modalCreateCustomerInvoice" class="btn btn-primary">
                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                        </button>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="paymentFormSat">Forma de Pago:</label>
                                                <select class="form-control" id="paymentFormSat"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="typeInvoice">Tipo de Factura:</label>
                                                <select class="form-control" id="typeInvoice">
                                                    <option value="1" selected>Factura</option>
                                                    <option value="2">Nota de Crédito</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="paymentMethodSat">Método de Pago:</label>
                                                <select class="form-control" id="paymentMethodSat"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="checkbox-inline text-primary"><input type="checkbox" id="checkboxMoreInputs">Ver opciones adicionales</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="divMoreData" style="display: none;">
                                    <div class="col-md-3">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="control-label" for="customerNameCustom">Número de pedido:</label>
                                                    <input type="text" class="form-control" id="orderNumber">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="accountNumber">Número de cuenta:</label>
                                                <input type="number" class="form-control" id="accountNumber" placeholder="Número de cuenta de pago (4 digitos)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="control-label" for="currency">Moneda:</label>
                                                    <select class="form-control" id="currency">
                                                        <option data-decimals="2" value="AFN">AFN - Afghani</option>
                                                        <option data-decimals="2" value="MGA">MGA - Ariary malgache</option>
                                                        <option data-decimals="2" value="AWG">AWG - Aruba Florin</option>
                                                        <option data-decimals="2" value="AZN">AZN - Azerbaijanian Manat</option>
                                                        <option data-decimals="2" value="THB">THB - Baht</option>
                                                        <option data-decimals="2" value="PAB">PAB - Balboa</option>
                                                        <option data-decimals="2" value="ETB">ETB - Birr etíope</option>
                                                        <option data-decimals="2" value="VEF">VEF - Bolívar</option>
                                                        <option data-decimals="2" value="BOB">BOB - Boliviano</option>
                                                        <option data-decimals="0" value="BIF">BIF - Burundi Franc</option>
                                                        <option data-decimals="2" value="CVE">CVE - Cabo Verde Escudo</option>
                                                        <option data-decimals="2" value="GHS">GHS - Cedi de Ghana</option>
                                                        <option data-decimals="2" value="KES">KES - Chelín keniano</option>
                                                        <option data-decimals="2" value="SOS">SOS - Chelín somalí</option>
                                                        <option data-decimals="0" value="XTS">XTS - Códigos reservados específicamente para propósitos de prueba</option>
                                                        <option data-decimals="2" value="CRC">CRC - Colón costarricense</option>
                                                        <option data-decimals="2" value="SVC">SVC - Colon El Salvador</option>
                                                        <option data-decimals="2" value="BAM">BAM - Convertibles marca</option>
                                                        <option data-decimals="2" value="NIO">NIO - Córdoba Oro</option>
                                                        <option data-decimals="2" value="KPW">KPW - Corea del Norte ganó</option>
                                                        <option data-decimals="2" value="CZK">CZK - Corona checa</option>
                                                        <option data-decimals="2" value="DKK">DKK - Corona danesa</option>
                                                        <option data-decimals="0" value="ISK">ISK - Corona islandesa</option>
                                                        <option data-decimals="2" value="NOK">NOK - Corona noruega</option>
                                                        <option data-decimals="2" value="SEK">SEK - Corona sueca</option>
                                                        <option data-decimals="2" value="GMD">GMD - Dalasi</option>
                                                        <option data-decimals="0" value="XDR">XDR - DEG (Derechos Especiales de Giro)</option>
                                                        <option data-decimals="2" value="MKD">MKD - Denar</option>
                                                        <option data-decimals="2" value="DZD">DZD - Dinar argelino</option>
                                                        <option data-decimals="3" value="BHD">BHD - Dinar de Bahrein</option>
                                                        <option data-decimals="3" value="IQD">IQD - Dinar iraquí</option>
                                                        <option data-decimals="3" value="JOD">JOD - Dinar jordano</option>
                                                        <option data-decimals="3" value="KWD">KWD - Dinar kuwaití</option>
                                                        <option data-decimals="3" value="LYD">LYD - Dinar libio</option>
                                                        <option data-decimals="2" value="RSD">RSD - Dinar serbio</option>
                                                        <option data-decimals="3" value="TND">TND - Dinar tunecino</option>
                                                        <option data-decimals="2" value="AED">AED - Dirham de EAU</option>
                                                        <option data-decimals="2" value="MAD">MAD - Dirham marroquí</option>
                                                        <option data-decimals="2" value="STD">STD - Dobra</option>
                                                        <option data-decimals="2" value="USD">USD - Dolar americano</option>
                                                        <option data-decimals="2" value="AUD">AUD - Dólar Australiano</option>
                                                        <option data-decimals="2" value="CAD">CAD - Dolar Canadiense</option>
                                                        <option data-decimals="2" value="BBD">BBD - Dólar de Barbados</option>
                                                        <option data-decimals="2" value="BZD">BZD - Dólar de Belice</option>
                                                        <option data-decimals="2" value="BMD">BMD - Dólar de Bermudas</option>
                                                        <option data-decimals="2" value="BND">BND - Dólar de Brunei</option>
                                                        <option data-decimals="2" value="FJD">FJD - Dólar de Fiji</option>
                                                        <option data-decimals="2" value="HKD">HKD - Dolar De Hong Kong</option>
                                                        <option data-decimals="2" value="BSD">BSD - Dólar de las Bahamas</option>
                                                        <option data-decimals="2" value="KYD">KYD - Dólar de las Islas Caimán</option>
                                                        <option data-decimals="2" value="SBD">SBD - Dólar de las Islas Salomón</option>
                                                        <option data-decimals="2" value="NAD">NAD - Dólar de Namibia</option>
                                                        <option data-decimals="2" value="NZD">NZD - Dólar de Nueva Zelanda</option>
                                                        <option data-decimals="2" value="SGD">SGD - Dolar De Singapur</option>
                                                        <option data-decimals="2" value="SRD">SRD - Dólar de Suriname</option>
                                                        <option data-decimals="2" value="TTD">TTD - Dólar de Trinidad y Tobago</option>
                                                        <option data-decimals="2" value="XCD">XCD - Dólar del Caribe Oriental</option>
                                                        <option data-decimals="2" value="USN">USN - Dólar estadounidense (día siguiente)</option>
                                                        <option data-decimals="2" value="GYD">GYD - Dólar guyanés</option>
                                                        <option data-decimals="2" value="JMD">JMD - Dólar Jamaiquino</option>
                                                        <option data-decimals="2" value="LRD">LRD - Dólar liberiano</option>
                                                        <option data-decimals="0" value="VND">VND - Dong</option>
                                                        <option data-decimals="2" value="AMD">AMD - Dram armenio</option>
                                                        <option data-decimals="2" value="EUR">EUR - Euro</option>
                                                        <option data-decimals="2" value="HUF">HUF - Florín</option>
                                                        <option data-decimals="2" value="ANG">ANG - Florín antillano neerlandés</option>
                                                        <option data-decimals="2" value="CHW">CHW - Franc WIR</option>
                                                        <option data-decimals="0" value="XOF">XOF - Franco CFA BCEAO</option>
                                                        <option data-decimals="0" value="XAF">XAF - Franco CFA BEAC</option>
                                                        <option data-decimals="0" value="XPF">XPF - Franco CFP</option>
                                                        <option data-decimals="0" value="KMF">KMF - Franco Comoro</option>
                                                        <option data-decimals="2" value="CDF">CDF - Franco congoleño</option>
                                                        <option data-decimals="0" value="DJF">DJF - Franco de Djibouti</option>
                                                        <option data-decimals="0" value="GNF">GNF - Franco guineano</option>
                                                        <option data-decimals="0" value="RWF">RWF - Franco ruandés</option>
                                                        <option data-decimals="2" value="CHF">CHF - Franco Suizo</option>
                                                        <option data-decimals="2" value="HTG">HTG - Gourde</option>
                                                        <option data-decimals="0" value="PYG">PYG - Guaraní</option>
                                                        <option data-decimals="2" value="UAH">UAH - Hryvnia</option>
                                                        <option data-decimals="2" value="PGK">PGK - Kina</option>
                                                        <option data-decimals="2" value="LAK">LAK - Kip</option>
                                                        <option data-decimals="2" value="HRK">HRK - Kuna</option>
                                                        <option data-decimals="2" value="MWK">MWK - Kwacha</option>
                                                        <option data-decimals="2" value="ZMW">ZMW - Kwacha zambiano</option>
                                                        <option data-decimals="2" value="AOA">AOA - Kwanza</option>
                                                        <option data-decimals="2" value="MMK">MMK - Kyat</option>
                                                        <option data-decimals="2" value="GEL">GEL - Lari</option>
                                                        <option data-decimals="2" value="ALL">ALL - Lek</option>
                                                        <option data-decimals="2" value="HNL">HNL - Lempira</option>
                                                        <option data-decimals="2" value="SLL">SLL - Leona</option>
                                                        <option data-decimals="2" value="MDL">MDL - Leu moldavo</option>
                                                        <option data-decimals="2" value="RON">RON - Leu rumano</option>
                                                        <option data-decimals="2" value="BGN">BGN - Lev búlgaro</option>
                                                        <option data-decimals="2" value="GIP">GIP - Libra de Gibraltar</option>
                                                        <option data-decimals="2" value="SHP">SHP - Libra de Santa Helena</option>
                                                        <option data-decimals="2" value="EGP">EGP - Libra egipcia</option>
                                                        <option data-decimals="2" value="GBP">GBP - Libra Esterlina</option>
                                                        <option data-decimals="2" value="LBP">LBP - Libra libanesa</option>
                                                        <option data-decimals="2" value="FKP">FKP - Libra malvinense</option>
                                                        <option data-decimals="2" value="SYP">SYP - Libra Siria</option>
                                                        <option data-decimals="2" value="SDG">SDG - Libra sudanesa</option>
                                                        <option data-decimals="2" value="SSP">SSP - Libra sudanesa Sur</option>
                                                        <option data-decimals="2" value="SZL">SZL - Lilangeni</option>
                                                        <option data-decimals="2" value="TRY">TRY - Lira turca</option>
                                                        <option data-decimals="0" value="XXX">XXX - Los códigos asignados para las transacciones en que intervenga ninguna moneda</option>
                                                        <option data-decimals="2" value="LSL">LSL - Loti</option>
                                                        <option data-decimals="0" value="XBD">XBD - Mercados de Bonos Unidad Europea unidad de cuenta a 17 (UCE-17)</option>
                                                        <option data-decimals="0" value="XBC">XBC - Mercados de Bonos Unidad Europea unidad de cuenta a 9 (UCE-9)</option>
                                                        <option data-decimals="2" value="MXV">MXV - México Unidad de Inversión (UDI)</option>
                                                        <option data-decimals="2" value="MZN">MZN - Mozambique Metical</option>
                                                        <option data-decimals="2" value="BOV">BOV - Mvdol</option>
                                                        <option data-decimals="2" value="NGN">NGN - Naira</option>
                                                        <option data-decimals="2" value="ERN">ERN - Nakfa</option>
                                                        <option data-decimals="2" value="BTN">BTN - Ngultrum</option>
                                                        <option data-decimals="2" value="TWD">TWD - Nuevo dólar de Taiwán</option>
                                                        <option data-decimals="2" value="ILS">ILS - Nuevo Shekel Israelí</option>
                                                        <option data-decimals="2" value="PEN">PEN - Nuevo Sol</option>
                                                        <option data-decimals="0" value="XAU">XAU - Oro</option>
                                                        <option data-decimals="2" value="MRO">MRO - Ouguiya</option>
                                                        <option data-decimals="2" value="TOP">TOP - Pa anga</option>
                                                        <option data-decimals="0" value="XPD">XPD - Paladio</option>
                                                        <option data-decimals="2" value="MOP">MOP - Pataca</option>
                                                        <option data-decimals="2" value="ARS">ARS - Peso Argentino</option>
                                                        <option data-decimals="0" value="CLP">CLP - Peso chileno</option>
                                                        <option data-decimals="2" value="COP">COP - Peso Colombiano</option>
                                                        <option data-decimals="2" value="CUC">CUC - Peso Convertible</option>
                                                        <option data-decimals="2" value="CUP">CUP - Peso Cubano</option>
                                                        <option data-decimals="2" value="DOP">DOP - Peso Dominicano</option>
                                                        <option data-decimals="2" value="PHP">PHP - Peso filipino</option>
                                                        <option data-decimals="2" value="MXN" selected="selected">MXN - Peso Mexicano</option>
                                                        <option data-decimals="0" value="UYI">UYI - Peso Uruguay en Unidades Indexadas (URUIURUI)</option>
                                                        <option data-decimals="2" value="UYU">UYU - Peso Uruguayo</option>
                                                        <option data-decimals="0" value="XAG">XAG - Plata</option>
                                                        <option data-decimals="0" value="XPT">XPT - Platino</option>
                                                        <option data-decimals="2" value="BWP">BWP - Pula</option>
                                                        <option data-decimals="2" value="QAR">QAR - Qatar Rial</option>
                                                        <option data-decimals="2" value="GTQ">GTQ - Quetzal</option>
                                                        <option data-decimals="2" value="ZAR">ZAR - Rand</option>
                                                        <option data-decimals="2" value="BRL">BRL - Real brasileño</option>
                                                        <option data-decimals="2" value="IRR">IRR - Rial iraní</option>
                                                        <option data-decimals="3" value="OMR">OMR - Rial omaní</option>
                                                        <option data-decimals="2" value="YER">YER - Rial yemení</option>
                                                        <option data-decimals="2" value="KHR">KHR - Riel</option>
                                                        <option data-decimals="2" value="MYR">MYR - Ringgit malayo</option>
                                                        <option data-decimals="2" value="SAR">SAR - Riyal saudí</option>
                                                        <option data-decimals="0" value="BYR">BYR - Rublo bielorruso</option>
                                                        <option data-decimals="2" value="RUB">RUB - Rublo ruso</option>
                                                        <option data-decimals="2" value="MVR">MVR - Rupia</option>
                                                        <option data-decimals="2" value="IDR">IDR - Rupia</option>
                                                        <option data-decimals="2" value="MUR">MUR - Rupia de Mauricio</option>
                                                        <option data-decimals="2" value="PKR">PKR - Rupia de Pakistán</option>
                                                        <option data-decimals="2" value="SCR">SCR - Rupia de Seychelles</option>
                                                        <option data-decimals="2" value="LKR">LKR - Rupia de Sri Lanka</option>
                                                        <option data-decimals="2" value="INR">INR - Rupia india</option>
                                                        <option data-decimals="2" value="NPR">NPR - Rupia nepalí</option>
                                                        <option data-decimals="0" value="UGX">UGX - Shilling de Uganda</option>
                                                        <option data-decimals="2" value="TZS">TZS - Shilling tanzano</option>
                                                        <option data-decimals="2" value="KGS">KGS - Som</option>
                                                        <option data-decimals="2" value="TJS">TJS - Somoni</option>
                                                        <option data-decimals="0" value="XSU">XSU - Sucre</option>
                                                        <option data-decimals="2" value="BDT">BDT - Taka</option>
                                                        <option data-decimals="2" value="WST">WST - Tala</option>
                                                        <option data-decimals="2" value="KZT">KZT - Tenge</option>
                                                        <option data-decimals="2" value="MNT">MNT - Tugrik</option>
                                                        <option data-decimals="2" value="TMT">TMT - Turkmenistán nuevo manat</option>
                                                        <option data-decimals="0" value="XUA">XUA - Unidad ADB de Cuenta</option>
                                                        <option data-decimals="4" value="CLF">CLF - Unidad de Fomento</option>
                                                        <option data-decimals="0" value="XBA">XBA - Unidad de Mercados de Bonos Unidad Europea Composite (EURCO)</option>
                                                        <option data-decimals="2" value="COU">COU - Unidad de Valor real</option>
                                                        <option data-decimals="0" value="XBB">XBB - Unidad Monetaria de Bonos de Mercados Unidad Europea (UEM-6)</option>
                                                        <option data-decimals="2" value="UZS">UZS - Uzbekistán Sum</option>
                                                        <option data-decimals="0" value="VUV">VUV - Vatu</option>
                                                        <option data-decimals="2" value="CHE">CHE - WIR Euro</option>
                                                        <option data-decimals="0" value="KRW">KRW - Won</option>
                                                        <option data-decimals="0" value="JPY">JPY - Yen</option>
                                                        <option data-decimals="2" value="CNY">CNY - Yuan Renminbi</option>
                                                        <option data-decimals="2" value="ZWL">ZWL - Zimbabwe Dólar</option>
                                                        <option data-decimals="2" value="PLN">PLN - Zloty</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="bankName">Nombre del Banco:</label>
                                                <input type="text" class="form-control" id="bankName">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="control-label" for="exchangeRate">Tipo de cambio:</label>
                                                    <input type="text" class="form-control" id="exchangeRate" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="conditionsPayment">Condiciones de pago:</label>
                                                <input type="text" class="form-control" id="conditionsPayment">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="control-label" for="dateIssue">Fecha de Emisión:</label>
                                                    <select class="form-control" id="dateIssue">
                                                        <option value="0" selected>Fecha actual</option>
                                                        <option value="1">1 día antes</option>
                                                        <option value="2">2 días antes</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="comments">Observaciones:</label>
                                                <input type="text" class="form-control" id="comments">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="productsBilling">Seleccionar producto o servicio:</label>
                                                <div class="input-group">
                                                    <select id="productsBilling" class="form-control">
                                                        <option value="0" selected="selected" disabled="disabled">Selecciona un producto</option>
                                                        @foreach($products as $product)
                                                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="input-group-btn">
                                                        <button id="btnShowModalCreateProduct" class="btn btn-primary">
                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                        </button>
                                                        <button class="btn btn-secondary" id="btnShowModalEditProduct">
                                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="quantityProductInput">Cantidad:</label>
                                                <input type="text" class="form-control" id="quantityProductInput" value="1" placeholder="Cantidad">
                                            </div>
                                            <div class="col-md-2">
                                                <label for="priceInput">Precio:</label>
                                                <input type="number" class="form-control" id="priceInput" placeholder="Precio">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="selectDiscount">Descuento:</label>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <select class="form-control" id="selectDiscount">
                                                            <option value="0">-</option>
                                                            <option value="1">$</option>
                                                            <option value="2">%</option>
                                                        </select>
                                                        <span class="input-group-addon">-</span>
                                                        <input class="form-control" type="number" value="0" disabled id="inputDiscount">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" id="actionsHeader">
                                                <label for="addTest">Agregar</label>
                                                <button class="btn btn-primary" id="addTest">Agregar</button>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-responsive-md table-sm table-bordered table-hover" id="customTable">
                                            <thead>
                                            <tr>
                                                <th>Cantidad</th>
                                                <th>Claves</th>
                                                <th>Producto</th>
                                                <th>Precio</th>
                                                <th>Subtotal</th>
                                                <th>Descuento</th>
                                                <th>Impuestos</th>
                                                <th>Total</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-md-12 pull-right">
                                            <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">Subtotal: <b id="subtotalTest"></b></label>
                                        </div>
                                    </div>
                                    <div class="row" style="display: none" id="divDiscountTest">
                                        <div class="col-md-12 pull-right">
                                            <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">Descuento: <b id="discountTest"></b></label>
                                        </div>
                                    </div>
                                    <div class="row" style="display: none" id="divIvaTest">
                                        <div class="col-md-12 pull-right">
                                            <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">IVA: <b id="ivaTest"></b></label>
                                        </div>
                                    </div>
                                    <div class="row" style="display: none" id="divIepsTest">
                                        <div class="col-md-12 pull-right">
                                            <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">IEPS: <b id="iepsTest"></b></label>
                                        </div>
                                    </div>
                                    <div class="row" style="display: none" id="divIsrTest">
                                        <div class="col-md-12 pull-right">
                                            <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">ISR: <b id="isrTest"></b></label>
                                        </div>
                                    </div>
                                    <div class="row" style="display: none" id="divIvaRetTest">
                                        <div class="col-md-12 pull-right">
                                            <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">IVA RET: <b id="ivaRetTest"></b></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 pull-right">
                                            <label class="control-label text-primary" for="total" style="font-size: 1.5em;">Total: <b id="totalTest"></b></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="text-center">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelCustomInvoice">Cancelar</button>
                                            <button class="btn btn-primary" type="button" id="btnCreateCustomInvoice">Crear Factura</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CUSTOM INVOICE -->
