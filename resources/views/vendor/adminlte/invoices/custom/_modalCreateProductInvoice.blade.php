<!-- START MODAL CUSTOM INVOICE -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalCreateProductInvoice" role="dialog" aria-labelledby="modalCreateProductInvoice" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 70% !important;">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitleProduct">Nuevo Producto</h4>
                                <input type="number" id="idProductInvoice" value="0" hidden>
                            </div>
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 style="font-weight: bold;">Datos del producto:</h5>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="control-label" for="customerNameCustom">Nombre Interno:</label>
                                                    <input type="text" class="form-control" id="internalName" placeholder="Nombre del producto o servicio">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="numberIdentification">No Identificación (Opcional):</label>
                                                <input type="text" class="form-control" id="numberIdentification" placeholder="Código interno (opcional)">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="descriptionProduct">Descripción:</label>
                                                <input type="text" class="form-control" id="descriptionProduct" placeholder="Descripción...">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="priceUnitProduct">Precio Unitario:</label>
                                                <input type="number" class="form-control" id="priceUnitProduct" placeholder="Precio">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="accountPredial">Cuenta Predial:</label>
                                                <input type="number" class="form-control" id="accountPredial" placeholder="Cuenta Predial (Solo para arrendamiento)">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <h5 style="font-weight: bold;">Cambios obligatorios CFDI 3.3 SAT:</h5>
                                        <div class="row form-group">
                                            <div class="col-sm-8">
                                                <label class="control-label" for="tipo">Asocia tu producto al cátalogo del SAT:</label>
                                                <input type="text" class="form-control" name="productCodeSat" id="productCodeSat">
                                                <input type="hidden" name="productCodeSatId" id="productCodeSatId">
                                                <div id="productCodeSatList"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                <a href="http://200.57.3.89/PyS/catPyS.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Clave</a>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-8">
                                                <label class="control-label" for="tipo">Define la unidad de tu producto</label>
                                                <input type="text" class="form-control" name="productUnitSat" id="productUnitSat">
                                                <input type="hidden" name="productUnitSatId" id="productUnitSatId">
                                                <div id="productUnitSatList"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                <a href="http://200.57.3.89/PyS/catUnidades.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Unidad</a>
                                            </div>
                                        </div>
                                        <h5 style="font-weight: bold; margin-top: 45px;">Impuestos Federales:</h5>
                                        <span>Selecciona únicamente los impuestos que necesite tu producto</span>
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label" for="iva">IVA:</label>
                                                <select id="iva" class="form-control">
                                                    <option value="0.16">IVA 16%</option>
                                                    <option value="0.08">IVA 8%</option>
                                                    <option value="0.00">IVA 0%</option>
                                                    <option value="0" data-type="exento">Exento</option>
                                                    <option value="-" selected>-</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label" for="ivaRet">IVA RET:</label>
                                                <select id="ivaRet" class="form-control">
                                                    <option value="0.16">IVA Ret 16%</option>
                                                    <option value="0.106668">IVA Ret 10.6668%</option>
                                                    <option value="0.106667">IVA Ret 10.6667%</option>
                                                    <option value="0.106666">IVA Ret 10.6666%</option>
                                                    <option value="0.1067">IVA Ret 10.67%</option>
                                                    <option value="0.1066">IVA Ret 10.66%</option>
                                                    <option value="0.106">IVA Ret 10.6%</option>
                                                    <option value="0.1">IVA Ret 10%</option>
                                                    <option value="0.0919">IVA Ret 9.19%</option>
                                                    <option value="0.08">IVA Ret 8%</option>
                                                    <option value="0.06">IVA Ret 6%</option>
                                                    <option value="0.054">IVA Ret 5.4%</option>
                                                    <option value="0.053333">IVA Ret 5.3333%</option>
                                                    <option value="0.05">IVA Ret 5%</option>
                                                    <option value="0.04">IVA Ret 4%</option>
                                                    <option value="0.03">IVA Ret 3%</option>
                                                    <option value="0.025">IVA Ret 2.5%</option>
                                                    <option value="0.02">IVA Ret 2%</option>
                                                    <option value="0.007">IVA Ret 0.7%</option>
                                                    <option value="0.005">IVA Ret 0.5%</option>
                                                    <option value="0.002">IVA Ret 0.2%</option>
                                                    <option value="0">IVA Ret 0%</option>
                                                    <option value="-" selected="selected">-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label" for="ieps">IEPS:</label>
                                                <label class="radio-inline"><input type="radio" name="optRadio" id="tasaCheck" checked>TASA</label>
                                                <label class="radio-inline"><input type="radio" name="optRadio" id="cuotaCheck">CUOTA</label>
                                                <select id="ieps" class="form-control">
                                                    <option value="3.000000">IEPS 300%</option>
                                                    <option value="1.600000">IEPS 160%</option>
                                                    <option value="0.530000">IEPS 53%</option>
                                                    <option value="0.500000">IEPS 50%</option>
                                                    <option value="0.350000">IEPS 35%</option>
                                                    <option value="0.304000">IEPS 30.4%</option>
                                                    <option value="0.300000">IEPS 30%</option>
                                                    <option value="0.298800">IEPS 29.88%</option>
                                                    <option value="0.265000">IEPS 26.5%</option>
                                                    <option value="0.250000">IEPS 25%</option>
                                                    <option value="0.090000">IEPS 9%</option>
                                                    <option value="0.080000">IEPS 8%</option>
                                                    <option value="0.070000">IEPS 7%</option>
                                                    <option value="0.060000">IEPS 6%</option>
                                                    <option value="0.059100">IEPS 5.91%</option>
                                                    <option value="0.040000">IEPS 4%</option>
                                                    <option value="0.030000">IEPS 3%</option>
                                                    <option value="-" selected="selected">-</option>
                                                </select>
                                                <input class="form-control" type="number" placeholder="Ej. 0.005, 0.03" id="cuota" style="display: none;">
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label" for="isr">ISR:</label>
                                                <select id="isr" class="form-control">
                                                    <option value="0.200000">ISR 20%</option>
                                                    <option value="0.106660">ISR 10.666%</option>
                                                    <option value="0.100000">ISR 10%</option>
                                                    <option value="0.054000">ISR 5.4%</option>
                                                    <option value="0.040000">ISR 4%</option>
                                                    <option value="0.030000">ISR 3%</option>
                                                    <option value="0.021000">ISR 2.10%</option>
                                                    <option value="0.020000">ISR 2%</option>
                                                    <option value="0.011000">ISR 1.1%</option>
                                                    <option value="0.010000">ISR 1%</option>
                                                    <option value="0.009000">ISR 0.9%</option>
                                                    <option value="0.005000">ISR 0.5%</option>
                                                    <option value="0.004000">ISR 0.4%</option>
                                                    <option value="0.001000">ISR 0.1%</option>
                                                    <option value="0.000000">ISR 0%</option>
                                                    <option value="-" selected="selected">-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>


                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="text-center">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelProductBilling">Cancelar</button>
                                            <button class="btn btn-primary" type="button" id="saveProductBilling">Guardar</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CUSTOM INVOICE -->
