<!-- START MODAL DE COMPLEMENTO DE FACTURA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="complementModalInvoice" role="dialog" aria-labelledby="complementModalInvoice" tabindex="-1">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-dialog" role="document" >

                    <div class="content modal-content" >
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-primary" id="modalTitle">Cobrar Factura <b class="text-danger" id="invoiceFolioComplement"></b></h4>
                            </div>

                            <div class="box-body">
                                <div class="row container-fluid">
                                    <div class="col-md-12">
                                        <div class="form-group text-center">
                                            <span id="amountInvoice" class="text-bold text-success" style="font-size: x-large;"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="amountPaymentInvoice">Monto recibido:</label>
                                            <input id="amountPaymentInvoice" type="number" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="amountPaymentInvoice">Forma de Pago:</label>
                                            <select class="form-control" id="paymentWay">
                                                <option value="0" selected>Seleccione una forma de pago</option>
                                                @foreach($paymentWays as $paymentWay)
                                                    <option value="{{ $paymentWay->id }}">{{ $paymentWay->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="amountPaymentInvoice">Método de Pago:</label>
                                            <select class="form-control" id="paymentForm">
                                                <option value="0" selected>Seleccione un método de pago</option>
                                                @foreach($paymentMethods as $paymentMethod)
                                                    <option value="{{ $paymentMethod->id }}">{{ $paymentMethod->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="dateInvoicePayment">Fecha y Hora de Pago:</label>
                                            <input type="datetime-local" class="form-control" id="dateInvoicePayment">
                                        </div>
                                        <input type="checkbox" id="checkboxComplement"><span class="text-primary text-bold"> Generar Complemento de Pago.</span>
                                        <div id="divComplementInvoice" class="form-group" style="display: none;">
                                            <div class="col-md-6">
                                                <label for="amountPaymentInvoice">Método de Pago SAT:</label>
                                                <select class="form-control" id="paymentFormInvoice">
                                                </select>
                                                <label for="partiality">Parcialidad:</label>
                                                <input id="partiality" type="number" value="1" min="1" max="10" class="form-control">
                                                <label for="payerAccount">Cuenta ordenante: (opcional)</label>
                                                <input id="payerAccount" type="number" class="form-control">
                                                <label for="BeneficiaryAccount">Cuenta beneficiario: (opcional)</label>
                                                <input id="BeneficiaryAccount" type="number" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="ForeignAccountNamePayer">Nombre de banco: (opcional)</label>
                                                <input id="ForeignAccountNamePayer" type="text" class="form-control">
                                                <label for="OperationNumber">Número de operación: (opcional)</label>
                                                <input id="OperationNumber" type="text" class="form-control">
                                                <label for="RfcIssuerPayerAccount">RFC cuenta ordenante: (opcional)</label>
                                                <input id="RfcIssuerPayerAccount" type="text" class="form-control">
                                                <label for="RfcReceiverBeneficiaryAccount">RFC cuenta beneficiario: (opcional)</label>
                                                <input id="RfcReceiverBeneficiaryAccount" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="row">
                                        <button class="btn btn-primary" type="button" id="complementInvoice">Pagar Factura</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE COMPLEMENTO DE PAGO FACTURA -->
