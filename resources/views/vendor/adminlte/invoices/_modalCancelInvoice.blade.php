<!-- START MODAL DE CANCELAR FACTURA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="cancelInvoice" role="dialog" aria-labelledby="denyModal" tabindex="-1">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-dialog" role="document" >

                    <div class="content modal-content" >
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="row container-fluid">
                                    <div class="col-md-12">
                                        <h4 class="modal-title text-center col-lg-12 text-danger" id="modalTitle">¿Estás seguro de cancelar la factura <b class="text-danger" id="invoiceFolio"></b>?</h4>
                                        <hr>
                                        <h5 class="text-primary text-bold text-center">Proceso de cancelación bajo el nuevo esquema:</h5>
                                        <ul>
                                            <li>Los emisores deberán enviar la solicitud de cancelación de la factura a través del módulo de Facturación de PestWare App.</li>
                                            <li>
                                                Cuando se requiera la aceptación para la cancelación, el receptor de la factura, recibirá una notificación mediante su buzon tributario informando que existe una solicitud de cancelación.
                                            </li>
                                            <li>
                                                El receptor deberá manifestar la aceptación o rechazo de la cancelación a través de las herramientas que el SAT ofrece, dentro de los tres días hábiles siguientes contados a partir de la solicitud. De no emitir respuesta, se considera como una positiva ficta y la factura será cancelada.
                                            </li>
                                            <li>
                                                En caso de que la solicitud de cancelación no requiera aceptación por parte del receptor, la factura se cancelará de manera inmediata.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="row">
                                        <button class="btn btn-primary" type="button" id="cancelInvoiceSat">Cancelar Factura</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE CANCELAR FACTURA -->
