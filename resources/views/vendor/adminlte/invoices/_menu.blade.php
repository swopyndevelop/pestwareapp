<ul class="dropdown-menu">
    <li>
        <a style="cursor: pointer;" class="openModalSenMailInvoice" data-toggle="modal" data-target="#sendMailInvoice"
           data-id="{{ Vinkla\Hashids\Facades\Hashids::encode($invoice->id) }}"
           data-folio="{{ $invoice->folio_invoice }}"
           data-email="{{ $invoice->email_billing }}">
            <i class="fa fa-envelope-o" aria-hidden="true">
            </i>Enviar por Correo
        </a>
    </li>
    <li><a style="cursor: pointer;" class="openModalSendWhatsAppInvoice" data-toggle="modal" data-target="#whatsappModalInvoice"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($invoice->id) }}"
           data-folio="{{ $invoice->folio_invoice }}"
           data-cellphone="{{ $invoice->phone_number }}">
            <i class="fa fa-whatsapp" aria-hidden="true" style="color: green">
            </i>Compartir por WhatsApp</a>
    </li>
    @if($invoice->invoice_complement == null)
    <li>
        <a class="openModalComplementInvoice" data-toggle="modal" data-target="#complementModalInvoice" style="cursor:pointer"
           data-folio="{{ $invoice->folio_invoice }}"
           data-amount="$@convert($invoice->amount)"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($invoice->id) }}">
            <i class="fa fa-money" aria-hidden="true" style="color: dodgerblue;"></i>
            Ingresar Pago
        </a>
    </li>
    @endif
    <li>
        <a style="cursor: pointer;" class="openModalCancelInvoice" data-toggle="modal" data-target="#cancelInvoice" data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($invoice->id) }}" data-folio="{{ $invoice->folio_invoice }}"><i class="fa fa-window-close-o" aria-hidden="true" style="color: red;"></i>Cancelar</a>
    </li>
</ul>