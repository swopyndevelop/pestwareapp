@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Facturación de Clientes</h3>

                        <ol class="breadcrumb" style="margin-top: 5px;">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Mis Facturas</li>
                        </ol>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a type="submit" class="btn btn-primary-dark" data-toggle="modal" href="#modalCustomInvoice">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Nueva Factura
                        </a>
                    </div>

                    <div class="col-md-5 margin-mobile">
                        <select name="selectJobCenterCustomer" id="selectJobCenterCustomer" class="form-control" style="font-weight: bold;">
                        </select>
                    </div>

                    <div class="col-md-5 margin-mobile">
                        {!!Form::open(['method' => 'GET','route'=>'index_customers'])!!}
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Buscar por: (Nombre)" name="search" id="search" value="">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                        {!!Form::close()!!}
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <br><br>
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Factura</th>
                                        <th>Fecha / Hora</th>
                                        <th>Cliente</th>
                                        <th>Domicilio</th>
                                        <th>Descripción</th>
                                        <th>Total</th>
                                        <th>Estatus</th>
                                        <th class="text-center">PDF / XML</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($invoices as $invoice)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                        {{ $invoice->folio_invoice }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.invoices._menu')
                                                </div>
                                            </td>
                                            <td>{{ $invoice->date }}<br>{{ \Carbon\Carbon::parse($invoice->created_at)->format('H:i') }}</td>
                                            <td>{{ $invoice->customer_name }}<br>{{ $invoice->establishment_name }}</td>
                                            <td>{{ $invoice->address }} #{{ $invoice->address_number }} {{ $invoice->address_number_int }},<br>{{ $invoice->colony }},<br>{{ $invoice->municipality }}, {{ $invoice->state }}</td>
                                            <td>{{ $invoice->description }}</td>
                                            <td>$@convert($invoice->amount)</td>
                                            <td>
                                                <b
                                                    @if($invoice->id_status == 14) style="color: #3C5898" @endif
                                                    @if($invoice->id_status == 17) style="color: red" @endif
                                                    @if($invoice->id_status == 15) style="color: green" @endif
                                                >{{ $invoice->status }}
                                                </b>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ Route('download_document_invoice', ['invoiceId' => \Vinkla\Hashids\Facades\Hashids::encode($invoice->id), 'type' => 'pdf']) }}" target="_blank">
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 2em;color:red"></i>
                                                </a>
                                                <a href="{{ Route('download_document_invoice', ['invoiceId' => \Vinkla\Hashids\Facades\Hashids::encode($invoice->id), 'type' => 'Xml']) }}" target="_blank">
                                                    <i class="fa fa-file-excel-o" aria-hidden="true" style=" font-size: 2em;color:dimgrey"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $invoices->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('adminlte::invoices._modalCancelInvoice')
    @include('adminlte::invoices._modalSendInvoiceEmail')
    @include('adminlte::invoices._modalSendInvoiceWhatsApp')
    @include('adminlte::invoices._modalComplementInvoice')
    @include('vendor.adminlte.invoices.custom._modalCreateInvoice')
    @include('vendor.adminlte.invoices.custom._modalCreateProductInvoice')
    @include('vendor.adminlte.invoices.custom._modalCreateCustomerInvoice')
    @include('vendor.adminlte.register.billing._modalPurchaseFoliosCalendar')
@endsection

@section('personal-js')
    <script>$(".applicantsList-single").select2();</script>
    <script type="text/javascript" src="{{ URL::asset('js/build/invoices.js') }}"></script>
@endsection