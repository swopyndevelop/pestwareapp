<!-- INICIA MODAL PARA ENVIAR EMAIL ORDEN DE COMPRA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="sendMailInvoice" role="dialog" aria-labelledby="sendMailInvoice">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Enviar Factura <b class="text-danger" id="invoiceFolioSendEmail"></b></h4>
                            </div>

                            <div class="box-body text-center">
                                <input type="hidden" name="idPurchaseOrderEmail" id="idPurchaseOrderEmail">

                                <div class="formulario__grupo col-xs-6 col-xs-offset-3" id="grupo__emailCustomerQuot">
                                    <div>
                                        <span style="font-weight: bold;">Correo del cliente:</span>
                                        <input type="email" class="formulario__input form-control" name="emailCustomerInvoice"
                                               id="emailCustomerInvoice">
                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                    </div>
                                    <p class="formulario__input-error">Los datos ingresados deben ser un correo electrónico.</p>
                                </div>

                                <div class="formulario__grupo col-xs-6 col-xs-offset-3" id="grupo__emailQuot">
                                    <div>
                                        <br>
                                        <span style="font-weight: bold;">Otro correo electrónico:</span>
                                        <input type="email" class="formulario__input form-control" name="emailOtherInvoice"
                                               id="emailOtherInvoice">
                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                    </div>
                                    <p class="formulario__input-error">Los datos ingresados deben ser un correo electrónico.</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary" name="sendEmailInvoiceButton" id="sendEmailInvoiceButton">Envíar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL ORDEN DE COMPRA -->