<!-- START MODAL DE RECHAZAR COTIZACIÓN -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="deleteCatalogs" role="dialog" aria-labelledby="denyModal"
         style="background-color:transparent;" tabindex="-1">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document">

                    <div class="content modal-content">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="col-md-12 text-left">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitleDeleteCatalogs"></h4>
                                </div>
                                <div style="visibility: hidden" class="col-md-3">
                                </div>
                                <div class="text-center col-md-7">
                                    <p class="text-danger font-weight-bold">
                                        Se eliminará toda la información asociada a los seguimientos elementos:
                                    </p>
                                    <ul class="text-left">
                                        <li>Cotizaciones</li>
                                        <li>Ordenes de Servicio</li>
                                        <li>Monitoreo de Estaciones</li>
                                    </ul>
                                </div>
                                <div style="visibility: hidden" class="col-md-2">
                                </div>
                                <div style="visibility: hidden" class="col-md-4">
                                </div>
                                <div class="text-center col-md-4">
                                    <h4 class="text-center">Contraseña Administrador</h4>
                                    <input id="passwordManagerCatalogs" type="password" class="form-control">
                                </div>

                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <button class="btn btn-primary" type="button" id="deleteCatalogButtonModal">
                                        Eliminar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE RECHAZAR COTIZACIÓN -->
