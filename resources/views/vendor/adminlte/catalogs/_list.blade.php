@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content') 
<div class="content container-fluid spark-screen">
    <div class="row">
        @include('adminlte::layouts.partials.session-messages')
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Listado</h3>
                    <br><br>
                    <ol class="breadcrumb">
                        <li><a href="{{ route('index_catalogs') }}">Cátalogos del Sistema</a></li>
                        <li class="active">{{ $title }}</li>
                        <input type="hidden" value="{{ $indexRoute }}" id="indexRoute">
                    </ol>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            @if($type == 1)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 1 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 2)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 2 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 3)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 3 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 4)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 4 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 5)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 5 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 6)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 6 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 7)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 7 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 8)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 8 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 9)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 9 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 10)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 10 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 11)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 11 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 12)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 12 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 13)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 13 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 14)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 14 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 15)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 15 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 16)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 16}}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 17)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 17 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 18)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 18 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 19)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 19 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @elseif($type == 20)
                                <div class="col-md-1">
                                    <button type="button" data-toggle="modal" data-target="#ModalCreate{{ $c = 20 }}" class="btn btn-primary btn-sm">Agregar Nuevo</button>
                                </div>
                            @endif
                            <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <select name="selectJobCenterCatalogsList" id="selectJobCenterCatalogsList" class="form-control" style="font-weight: bold; width: 50%">
                                        @foreach($jobCenters as $jobCenter)
                                            @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                                <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                                    {{ $jobCenter->name }}
                                                </option>
                                            @else
                                                <option value="{{ $jobCenter->id }}">
                                                    {{ $jobCenter->name }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    {!!Form::open(['method' => 'GET','route'=> $indexRoute])!!}
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar por: (Nombre)" name="search" id="search" value="">
                                        <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                    </span>
                                    </div>
                                    {!!Form::close()!!}
                                </div>

                            <br><br>
                            <div class="panel panel-primary" style="border-color: #bce8f1;">

                                <h3 class="text-center">{{ $title }}</h3>
                            
                                <div class="pull-right">
                                    <a href="javascript:void(0)" data-perform="panel-collapse">
                                        <i class="ti-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" data-perform="panel-dismiss">
                                        <i class="ti-close"></i>
                                    </a>
                                </div>
                            
                                <div class="panel-body">
                            
                                    <div class="table-responsive">
                                        <table class="table table-hover table table-striped">
                                            <thead><tr>
                                                <th class="text-center">Nombre</th>
                                                @if($type == 3)
                                                    <th class="text-center">Descripción</th>
                                                    <th class="text-center">Porcentaje</th>
                                                @endif
                                                @if($type == 4)
                                                    <th class="text-center">Descripción</th>
                                                    <th class="text-center">Cantidad</th>
                                                @endif
                                                @if($type == 8)
                                                    <th class="text-center">Categoría</th>
                                                    <th class="text-center">Trampas Captura</th>
                                                    <th class="text-center">Lamparas UV</th>
                                                @endif
                                                @if($type == 13)
                                                    <th class="text-center">Días de Crédito</th>
                                                @endif
                                                @if($type == 15)
                                                    <th class="text-center">Tipo</th>
                                                @endif
                                                @if($type == 16)
                                                    <th class="text-center">Clave</th>
                                                    <th class="text-center">Descripción</th>
                                                @endif
                                                @if($type == 17)
                                                    <th class="text-center">Clave</th>
                                                    <th class="text-center">Tipo</th>
                                                @endif
                                                @if($type == 19)
                                                    <th class="text-center">Valor</th>
                                                @endif
                                                @if($type == 20)
                                                    <th class="text-center">Descripción</th>
                                                @endif
                                                <th>Actualizar</th>
                                                <th>Eliminar</th>
                                            </tr></thead>
                                            <tbody>
                                            @php($a = 0)
                                            @php($ind = 0)
                                            @foreach($data as $dat)
                                                @php($a = $dat->id)
                                                @php($ind = $dat)
                                                <tr>
                                                    @if($type != 3 && $type != 4)
                                                        <td class="text-center">{{ $dat->name }}</td>
                                                    @endif
                                                    @if($type == 3)
                                                        <td class="text-center">{{ $dat->title }}</td>
                                                        <td class="text-center">{{ $dat->description }}</td>
                                                        <td class="text-center">{{ $dat->percentage }}%</td>
                                                    @endif
                                                    @if($type == 4)
                                                        <td class="text-center">{{ $dat->name }}</td>
                                                        <td class="text-center">{{ $dat->description }}</td>
                                                        <td class="text-center">{{ $symbol_country }}{{ $dat->amount }}</td>
                                                    @endif
                                                    @if($type == 8)
                                                        <td class="text-center">{{ $dat->category }}</td>
                                                        <td class="text-center">
                                                            @if($dat->is_monitoring == 1 || $dat->is_monitoring == 3)
                                                                <i class="fa fa-check text-success" aria-hidden="true"></i>
                                                            @else
                                                                <i class="fa fa-times text-danger" aria-hidden="true"></i>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($dat->is_monitoring == 2 || $dat->is_monitoring == 3)
                                                                <i class="fa fa-check text-success" aria-hidden="true"></i>
                                                            @else
                                                                <i class="fa fa-times text-danger" aria-hidden="true"></i>
                                                            @endif
                                                        </td>
                                                    @endif
                                                    @if($type == 13)
                                                        <td class="text-center">{{ $dat->credit_days }}</td>
                                                    @endif
                                                    @if($type == 15)
                                                        @if($dat->tipo == 1)
                                                            <td class="text-center">Orden de Compra</td>
                                                        @else
                                                            <td class="text-center">Captura de Gasto</td>
                                                        @endif
                                                    @endif
                                                    @if($type == 16)
                                                        <td class="text-center">{{ $dat->key }}</td>
                                                        <td class="text-center">
                                                            <?php
                                                            $resultado = $dat->description;
                                                            $r1 = substr($resultado, 0,85);
                                                            echo $r1."...";
                                                            ?>
                                                        </td>
                                                    @endif
                                                    @if($type == 17)
                                                        <td class="text-center">{{ $dat->key }}</td>
                                                        <td class="text-center">@if($dat->id_type_area == 4)Estación @else Trampa @endif</td>
                                                    @endif
                                                    @if($type == 19)
                                                        <td class="text-center">{{ $dat->value }}</td>
                                                    @endif
                                                    @if($type == 20)
                                                            <td class="text-center">{{ \Illuminate\Support\Str::limit($dat->description, 100) }}</td>
                                                    @endif
                                                    <td>
                                                        @include('vendor.adminlte.catalogs._modalEdit')
                                                        <a data-toggle="modal" data-target="#ModalEdit{{ $a }}" class="btn">
                                                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                    @if($type == 1)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_sourceorigin"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 2)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_typeservice"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 3)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_discount"
                                                                    data-title="{{ $dat->title }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 4)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_extra"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 5)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_methodapp"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 6)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_grade"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 7)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_cleaning"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 8)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_plague"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 9)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_presentation"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 10)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_type"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 11)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_units"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 12)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_paymentmethod"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 13)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_paymentway"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 14)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_comprobant"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 15)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_concepts"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 16)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_indications"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 17)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_stations"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 18)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_plague_category"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 19)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_taxes"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @elseif($type == 20)
                                                        <td>
                                                            <button type="button" class="openModalDeleteCatalogs btn" data-toggle="modal" data-target="#deleteCatalogs" id="openModalDeleteCatalogs" style="background-color: transparent;"
                                                                    data-id="{{ $dat->id }}"
                                                                    data-route="delete_custom_descriptions"
                                                                    data-title="{{ $dat->name }}">
                                                                <i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
                                                            </button>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                            
                                        </table>
                                    </div>
                                    {{ $data->links() }}
                            
                                </div>
                            </div>
                        </div>
                   <!-- /.box-body -->
               </div>
               <!-- /.box -->
           </div>
       </div>
   </div>
@include('vendor.adminlte.catalogs._modalCreate')
@include('adminlte::catalogs._deleteCatalogs')
@endsection

@section('personal-js')
    <script>//select job center
        $('#selectJobCenterCatalogsList').on('change', function() {
            let indexRoute = document.getElementById('indexRoute').value;
            let idJobCenterCatalogsList = $(this).val();
            window.location.href = route(indexRoute, idJobCenterCatalogsList);
        });
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/build/catalog.js') }}"></script>
@endsection