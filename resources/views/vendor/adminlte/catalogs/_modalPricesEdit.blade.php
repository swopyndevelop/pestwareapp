<!-- START MODAL EDIT ITEM PRICE -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalEditPrices{{ $a }}" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <form action="{{ Route('update_prices') }}" method="POST" id="form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Actualizar Precios</h4>
                                    <input type="number" name="id" value="{{ $price->id }}" hidden="hidden">

                                    <div class="table-responsive">
                                        <table class="table table-hover table-condensed table-bordered">
                                            <thead>
                                                <tr class="active">
                                                    <th class="text-center">Categoría</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th>Mantenimiento</th>
                                                    <th></th>
                                                    <th class="text-right">Cucaracha</th>
                                                    <th>Alemana</th>
                                                    <th class="text-right">Hematófagos</th>
                                                    <th></th>
                                                    <th class="text-center">Termitas</th>
                                                    <th></th>
                                                    <th class="text-center">Roedores</th>
                                                    <th></th>
                                                </tr>
                                                <tr class="active">
                                                    <th class="text-center">Jerarquía</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="text-center">3</th>
                                                    <th></th>
                                                    <th class="text-right">2</th>
                                                    <th></th>
                                                    <th class="text-center">1</th>
                                                    <th></th>
                                                    <th class="text-center">0</th>
                                                    <th></th>
                                                    <th class="text-center">0</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <th class="active text-center">Dimensiones en m2</th>
                                                    <th class="info text-center">BIODEGRADABLE</th>
                                                    <th class="success text-center">ORGÁNICO</th>
                                                    <th class="success text-center">CUALQUIER SERVICICIO <br> + JARDIN EXTERIOR</th>
                                                    <th class="success text-center">SOLO JARDIN EXTERIOR</th>
                                                    <th class="warning text-center">1ª APLICACIÓN</th>
                                                    <th class="warning text-center">REFUERZO</th>
                                                    <th class="danger text-center">1ª APLICACIÓN</th>
                                                    <th class="danger text-center">REFUERZO</th>
                                                    <th style="background: #EDBB99;" class="text-center">TRATAMIENTO</th>
                                                    <th style="background: #D7DBDD;" class="text-center">CANTIDAD DE TRAMPAS</th>
                                                    <th style="background: #D7DBDD;" class="text-center">ESTACIONES CEBO</th>
                                                    <th style="background: #D7DBDD;" class="text-center">TRAMPAS PEGAMENTO</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center"><b>{{ $price->area }}</b></td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="MTTO_biodegradable" value="{{ $price->MTTO_biodegradable }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="MTTO_organic" value="{{ $price->MTTO_organic }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="MTTO_service_plus_garden" value="{{ $price->MTTO_service_plus_garden }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="MTTO_garden" value="{{ $price->MTTO_garden }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="CA_1_application" value="{{ $price->CA_1_application }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="CA_reinforcement" value="{{ $price->CA_reinforcement }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="CH_1_application" value="{{ $price->CH_1_application }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="CH_reinforcement" value="{{ $price->CH_reinforcement }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="TER_treatment" value="{{ $price->TER_treatment }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="RAT_amount_traps" value="{{ $price->RAT_amount_traps }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="RAT_bait_stations" value="{{ $price->RAT_bait_stations }}" required>
                                                    </td>
                                                    <td class="text-center">
                                                        <input class="form-control text-center" type="number" name="RAT_glue_traps" value="{{ $price->RAT_glue_traps }}" required>
                                                    </td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                 </div>

                                <div class="modal-footer">
                                    <br>
                                    <div class="text-center">
                                        <button class="btn btn-success" type="submit">Actualizar Precios</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL EDIT ITEM PRICE -->
