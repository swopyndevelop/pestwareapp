@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen" id="entry">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Catálogos del Sistema</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="box-body">
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#quotations" aria-controls="quotations" role="tab" data-toggle="tab">Cotizaciones</a></li>
                                <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Servicios</a></li>
                                <li role="presentation"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Productos</a></li>
                                <li role="presentation"><a href="#accounting" aria-controls="accounting" role="tab" data-toggle="tab">Contabilidad</a></li>
                            </ul>
                                  
                            <!-- Tab panes -->
                            <div class="tab-content">
                                
                                <br>

                                <div role="tabpanel" class="tab-pane active" id="quotations">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Fuente de Origen</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-globe fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_sourceorigin') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Tipos de Servicio</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-list-ol fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_typeservice') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Descuentos</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-percent fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_discount') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Extras</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-plus fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_extra') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Indicaciones</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-commenting-o fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_indications') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Listas de Precio</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-usd fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_prices') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Descripciones Personalizadas</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-sticky-note-o fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_custom_descriptions') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="services">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Métodos de Aplicación</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-snowflake-o fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_methodapp') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Grado de Infestación</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-line-chart fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_grade') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Orden y Limpieza</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-trash fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_cleaning') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Plagas</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-bug fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_plague') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Categorías de Plagas</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-list fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_plague_categories') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Tipos de Estaciones</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-th fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_type_stations') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="products">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Presentaciones</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-shopping-bag fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_presentation') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Tipos</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-list-ul fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_type') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Unidades</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-tachometer fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_units') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div role="tabpanel" class="tab-pane" id="accounting">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Métodos de Pago</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-credit-card-alt fa-4x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_paymentmethod') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Formas de Pago</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-money fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_paymentway') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Tipo de Comprobantes</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-file-o fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_comprobant') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Conceptos</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-th-list fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_concepts') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Impuestos</h3>
                                                </div>
                                                <div class="panel-body text-center">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <i class="fa fa-th-list fa-5x" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <br>
                                                            <p>
                                                                <a type="button" href="{{ route('list_taxes') }}" class="btn btn-primary btn-sm">Agregar / Listar</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
