<!-- START MODAL CREATE ITEM CATALOG -->
<div class="container-fluid spark-screen">
<div class="modal fade" id="ModalCreate{{ $c }}" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            @if ($c == 1)
                                <input type="text" id="urlItemCatalog" value="store_sourceorigin" hidden="hidden">
                            @elseif($c == 2)
                                <input type="text" id="urlItemCatalog" value="store_typeservice" hidden="hidden">
                            @elseif($c == 3)
                                <input type="text" id="urlItemCatalog" value="store_discount" hidden="hidden">
                            @elseif($c == 4)
                                <input type="text" id="urlItemCatalog" value="store_extra" hidden="hidden">
                            @elseif($c == 5)
                                <input type="text" id="urlItemCatalog" value="store_methodapp" hidden="hidden">
                            @elseif($c == 6)
                                <input type="text" id="urlItemCatalog" value="store_grade" hidden="hidden">
                            @elseif($c == 7)
                                <input type="text" id="urlItemCatalog" value="store_cleaning" hidden="hidden">
                            @elseif($c == 8)
                                <input type="text" id="urlItemCatalog" value="store_plague" hidden="hidden">
                            @elseif($c == 9)
                                <input type="text" id="urlItemCatalog" value="store_presentation" hidden="hidden">
                            @elseif($c == 10)
                                <input type="text" id="urlItemCatalog" value="store_type" hidden="hidden">
                            @elseif($c == 11)
                                <input type="text" id="urlItemCatalog" value="store_units" hidden="hidden">
                            @elseif($c == 12)
                                <input type="text" id="urlItemCatalog" value="store_paymentmethod" hidden="hidden">
                            @elseif($c == 13)
                                <input type="text" id="urlItemCatalog" value="store_paymentway" hidden="hidden">
                            @elseif($c == 14)
                                <input type="text" id="urlItemCatalog" value="store_comprobant" hidden="hidden">
                            @elseif($c == 15)
                                <input type="text" id="urlItemCatalog" value="store_concepts" hidden="hidden">
                            @elseif($c == 16)
                                <input type="text" id="urlItemCatalog" value="store_indications" hidden="hidden">
                            @elseif($c == 17)
                                <input type="text" id="urlItemCatalog" value="store_types_stations" hidden="hidden">
                            @elseif($c == 18)
                                <input type="text" id="urlItemCatalog" value="store_plague_category" hidden="hidden">
                            @elseif($c == 19)
                                <input type="text" id="urlItemCatalog" value="store_taxes" hidden="hidden">
                            @elseif($c == 20)
                                <input type="text" id="urlItemCatalog" value="store_custom_descriptions" hidden="hidden">
                            @endif
                                {{ csrf_field() }}
                                <div class="box-body text-center">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Nuevo Registro</h4>

                                    <input type="number" id="typeCatalog" value="{{ $type }}" hidden="hidden">
                                    <input class="form-control" type="text" id="nameItemCatalog" placeholder="Nombre..." required>

                                    @if($c == 3)
                                        <br>
                                        <input class="form-control" type="text" id="description" placeholder="Descripción" required>
                                        <br>
                                        <input class="form-control" type="number" id="percentage" placeholder="Porcentaje %" required>
                                    @endif

                                    @if($c == 4)
                                        <br>
                                        <input class="form-control" type="text" id="description" placeholder="Descripción" required>
                                        <br>
                                        <input class="form-control" type="number" id="amount" placeholder="Cantidad $" required>
                                    @endif

                                    @if($c == 8)
                                        <br>
                                        <select class="form-control" name="plagueCategoryNew" required id="plagueCategoryNew">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                        <br>
                                        <span style="font-weight: bold;">Mostrar para Estaciones:</span>
                                        <br>
                                        <input type="checkbox" class="form-check-input" name="isMonitoringUVNew" id="isMonitoringUVNew">
                                        <span style="font-weight: bold;">Lamparas UV</span>
                                        <br>
                                        <input type="checkbox" class="form-check-input" name="isMonitoringCaptureNew" id="isMonitoringCaptureNew">
                                        <span style="font-weight: bold;">Trampas Captura</span>
                                    @endif

                                    @if($c == 13)
                                        <br>
                                        <input class="form-control" type="text" id="creditDays" placeholder="Días de Crédito">
                                    @endif

                                @if($c == 15)
                                        <br>
                                        <select class="form-control" id="categorie" required>
                                            <option disabled="" selected="" value="1">Selecciona el Tipo</option>
                                            <option value="1">Orden de Compra</option>
                                            <option value="2">Captura de Gasto</option>
                                        </select>
                                    @endif

                                    @if($c == 16)
                                        <br>
                                        <input class="form-control" type="text" id="key" placeholder="Clave" required>
                                        <br>
                                        <textarea class="form-control" rows="5" id="description" name="description" placeholder="Indicaciones..."></textarea>
                                    @endif
                                    @if($c == 17)
                                        <br>
                                        <input class="form-control" type="text" id="key" placeholder="Clave" required>
                                        <br>
                                        <select class="form-control" id="typeArea" required>
                                            <option disabled="" selected="" value="1">Selecciona el Tipo</option>
                                            <option value="4">Estación</option>
                                            <option value="5">Trampa</option>
                                        </select>
                                    @endif
                                    @if($c == 19)
                                        <br>
                                        <input class="form-control" type="text" id="value" placeholder="Valor" required>
                                    @endif
                                    @if($c == 20)
                                        <br>
                                        <textarea class="form-control" rows="15" id="description" name="description" placeholder="Descripción del servicio..."></textarea>
                                    @endif

                                </div>

                                <div class="modal-footer">
                                    <br>
                                    <div class="text-center">
                                        <button class="btn btn-primary" id="updateItemCatalog" name="updateItemCatalog">Guardar</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CREATE ITEM CATALOG -->
