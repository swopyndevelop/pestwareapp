@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <!-- START MODAL PORTADA COTIZACIÓN -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="Adjunto" role="dialog" aria-labelledby="AdjuntoModal" tabindex="-1" >
            <div class="row">
                <div class="col-md-12">
                    <!--Default box-->
                    <div class="modal-dialog modal-md" role="document" >

                        <div class="modal-content" >
                            <div class="box">
                                <div class="modal-header" >
                                    <div class="box-header" >
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                                <form id="add_pdf" method="POST" enctype="multipart/form-data" action="{{Route('add_pdf_price')}}">
                                    {{ csrf_field() }}
                                    <div class="box-body">
                                        <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Subir Platilla PDF -> {{$priceLists->name}}</h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="col-md-9">

                                                    <input type="hidden" name="priceList" id="priceList" value="{{$priceLists->id}}">
                                                </div>
                                                <br>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <label for="techniciansEvent" style="font-weight: bold;">Plantilla:</label>
                                                <input type="file" name="plantilla" id="plantilla" accept="application/pdf" class="form-control" required>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="text-center">
                                            <div class="row">
                                                <button class="btn btn-primary btn-sm" type="submit" name="SaveDepos">Guardar</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- END MODAL DE PORTADA COTIZACIÓN -->
@endsection

@section('personal-js')
    <script>
        //$(".plagueList-single").select2();
        $("#Adjunto").modal();
        $("#Adjunto").on('hidden.bs.modal', function() {
            window.location.href = '{{route("list_prices")}}'; //using a named route
        });
    </script>
@endsection