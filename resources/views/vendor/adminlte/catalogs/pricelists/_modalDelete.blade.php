<!-- INICIA MODAL PARA ELIMINAR LISTA -->
<div class="modal fade" id="delete<?php echo $a->id; ?>" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<form action="{{ route('delete_list_prices', ['id' => $a->id]) }}" method="POST" id="form">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				<div class="modal-body" style="text-align: center;">
					<h5 class="modal-title">¿Está seguro de eliminar la lista de precios?</h5>
				<div class="col-md-8 col-md-offset-2">

				</div>
		</div>
		<!-- Modal footer -->
				<hr>
		<div class="text-center">
			<button type="submit" title="Aceptar" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#delete<?php echo $a->id; ?>"><strong>Aceptar</strong></button>
		</form>
		</div>
		<br>
	</div>
	</div>
</div>
<!-- TERMINA MODAL PARA ELIMINAR LISTA -->
