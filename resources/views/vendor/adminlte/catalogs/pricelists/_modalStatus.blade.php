<!-- INICIA MODAL PARA EDITAR STATUS -->
<div class="modal fade" id="status<?php echo $a->id; ?>" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<form action="{{ route('change_status_list_prices', ['id' => $a->id]) }}" method="POST" id="form">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<div class="modal-body">
					<h5 class="modal-title">¿Está seguro de que desea @if($a->status==1)<b class="text-danger">inactivar</b> @else <b class="text-success">activar</b> @endif la lista?</h5>
					<div class="col-md-8 col-md-offset-2">	
					@if($a->status==1)
						<input type="number" name="status" hidden="hidden" value="0">
		            @else
		            	<input type="number" name="status" hidden="hidden" value="1">
					@endif
				</div>
				</div>
			<!-- Modal footer -->
			<div class="modal-footer">										
				<button type="submit" title="Aceptar" style="background-color: green; border-color: green;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#status<?php echo $a->id; ?>"><strong>Aceptar</strong></button>
			</form>	
		</div>
	</div>
	</div>
</div>
<!-- TERMINA MODAL PARA EDITAR STTAUS -->