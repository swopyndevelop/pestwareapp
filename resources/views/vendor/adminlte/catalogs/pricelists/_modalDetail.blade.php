<!-- START MODAL DE LISTA DE PRECIO -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalPriceListDetail<?php echo $a->id; ?>" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Lista de Precio {{ $a->name }}</h4>
                                <div class="row form-group">

                                    <div class="col-md-6">
                                        <span style="font-weight: bold;">Nombre de Lista</span>
                                        <input class="form-control" type="text" value="{{ $a->name }}" disabled="true" style="font-size: larger;">
                                    </div>

                                    <div class="col-md-3">
                                        <span style="font-weight: bold;">Clave</span>
                                        <input class="form-control" type="text" value="{{ $a->key }}" disabled="true" style="font-size: larger;">
                                    </div>

                                    <div class="col-md-3">
                                        <span style="font-weight: bold;">Tipo de Servicio</span>
                                        <input class="form-control" type="text" value="{{ $a->establishment }}" disabled="true" style="font-size: larger;">
                                    </div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-md-9">
                                        <span style="font-weight: bold;">Plagas</span>
                                        <input class="form-control" type="text" value="{{ $a->plagues }}" disabled="true" style="font-size: larger;">
                                    </div>

                                    <div class="col-md-1">
                                        <span style="font-weight: bold;">Jerarquía</span>
                                        <input type="number" class="form-control" value="{{ $a->hierarchy }}" disabled="true" style="font-size: larger;">
                                    </div>

                                    <div class="col-md-1">
                                        <span style="font-weight: bold;">Refuerzo</span>
                                        <input type="text" class="form-control" disabled="true" @if($a->reinforcement_days == null) value="No" @else value="{{ $a->reinforcement_days }} días" @endif>
                                    </div>

                                    <div class="col-md-1">
                                        <span style="font-weight: bold;">Portal</span><br>
                                        <input type="checkbox" class="form-check-input" name="customerPortal" id="customerPortal" disabled @if($a->customer_portal == 1) checked @endif>
                                    </div>

                                </div>

                                <div class="row form-group">

                                    <div class="col-md-6">
                                        <span style="font-weight: bold;">Descripción</span>
                                        <textarea class="form-control" rows="10" disabled="true" style="font-size: medium;">{{ $a->description }}</textarea>
                                    </div>

                                    <div class="col-md-6">
                                        <span style="font-weight: bold;">Indicaciones</span>
                                        <input type="text" class="form-control" value="{{ $a->indications }}" disabled="true" style="font-size: larger;">
                                        <textarea class="form-control" rows="9" disabled="true" style="font-size: medium;">{{ $a->indications_description }}</textarea>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">

                                            <table class="table table-hover table-condensed table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="info text-center">Escala</th>
                                                        <th class="success text-center">Cantidad</th>
                                                        <th class="warning text-center">Precio 1</th>
                                                        <th class="danger text-center">Precio 2</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($prices as $price)
                                                    @if($price->price_list_id == $a->id)
                                                        <tr>
                                                            <td class="text-center"><b>{{ $price->area }}</b></td>
                                                            <td class="text-center">{{ $price->quantity }}</td>
                                                            <td class="text-center">{{ $symbol_country }}{{ $price->price_one }}</td>
                                                            <td class="text-center">{{ $symbol_country }}{{ $price->price_two }}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE LISTA DE PRECIO -->
