@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('css-personal')
    @include('adminlte::catalogs.pricelists.PersonalSelect2')
@stop

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            @if(session('success'))
                <script>toastr.success("{{ session('success') }}");</script>
            @endif
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado</h3>
                        <br><br>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index_catalogs') }}">Cátalogos del Sistema</a></li>
                            <li class="active">Listas de Precio</li>
                        </ol>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-1">
                            <button type="button" data-toggle="modal" data-target="#ModalPriceList" class="btn btn-primary btn-sm">
                                Nueva Lista
                            </button>
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-5">
                            <select name="selectJobCenterPriceList" id="selectJobCenterPriceList" class="form-control" style="font-weight: bold; width: 50%">
                                @foreach($jobCenters as $jobCenter)
                                    @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                        <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                            {{ $jobCenter->name }}
                                        </option>
                                    @else
                                        <option value="{{ $jobCenter->id }}">
                                            {{ $jobCenter->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
                            {!!Form::open(['method' => 'GET','route'=> 'list_prices'])!!}
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar por: (Nombre)" name="search" id="search" value="">
                                <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                    </span>
                            </div>
                            {!!Form::close()!!}
                        </div>
                        <br><br>
                        <div class="panel panel-primary" style="border-color: #bce8f1;">

                            <h3 class="text-center">Listas de Precio</h3>

                            <div class="pull-right">
                                <a href="javascript:void(0)" data-perform="panel-collapse">
                                    <i class="ti-minus"></i>
                                </a>
                                <a href="javascript:void(0)" data-perform="panel-dismiss">
                                    <i class="ti-close"></i>
                                </a>
                            </div>

                            <div class="panel-body">

                                <div class="table-responsive" style="min-height: 500px;">
                                    <table class="table table-hover table-condensed table-bordered text-center">
                                        <thead class="small">
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Clave</th>
                                                <th>Fecha de Creación</th>
                                                <th>Plagas</th>
                                                <th>Tipo de Servicio</th>
                                                <th>Jerarquía</th>
                                                <th>Indicaciones</th>
                                                <th>Portada Cotización</th>
                                                <th>Carpeta MIP</th>
                                                <th>Estatus</th>
                                            </tr>
                                        </thead>
                                        <tbody class="small">
                                        @php($a = 0)
                                        @foreach($priceLists as $pl)
                                            @php($a = $pl)
                                            <tr>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                                            {{ $pl->name }} <span class="caret"></span>
                                                        </a>
                                                        @include('vendor.adminlte.catalogs.pricelists._menu')
                                                    </div>
                                                </td>
                                                <td class="text-center">{{ $pl->key }}</td>
                                                <td class="text-center">{{ $pl->created_at }} <br> {{ $pl->username }}</td>
                                                <td class="text-center">{{ $pl->plagues }}</td>
                                                <td class="text-center">{{ $pl->establishment }}</td>
                                                <td class="text-center">{{ $pl->hierarchy }}</td>
                                                <td class="text-center">{{ $pl->indications }}</td>
                                                <td class="text-center">
                                                @if($pl->pdf != null)
                                                    <div>
                                                        <a href="{{Route('download_pdf_price',$pl->id)}}">
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 2.5em;color:red"></i>
                                                        </a>
                                                    </div>
                                                   <div style="margin-top: 10px">
                                                       <a class="openModalDeleteQuoteCover text-danger" style="cursor: pointer;"
                                                          data-toggle="modal"
                                                          data-target="#deleteQuoteCover"
                                                          data-id="{{ $pl->id }}"><i class="fa fa-trash text-danger" aria-hidden="true"></i> Eliminar
                                                       </a>
                                                   </div>
                                                @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($pl->portada != null)
                                                        <div>
                                                            <a href="{{Route('download_pdf_mip',$pl->id)}}">
                                                                <i class="fa fa-folder-o" aria-hidden="true" style=" font-size: 3.0em;color:orange"></i>
                                                            </a>
                                                        </div>
                                                        <div style="margin-top: 10px">
                                                            <a class="openModalDeleteFolderMip text-danger" style="cursor: pointer;"
                                                               data-toggle="modal"
                                                               data-target="#deleteFolderMip"
                                                               data-id="{{ $pl->id }}"><i class="fa fa-trash text-danger" aria-hidden="true"></i> Eliminar
                                                            </a>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($pl->status == 1)
                                                        <a href="#" data-toggle="modal" data-target="#status<?php echo $a->id; ?>" title="Activo"><i class="fa fa-unlock fa-2x" aria-hidden="true"></i></a>
                                                    @else
                                                        <a href="#" data-toggle="modal" data-target="#status<?php echo $a->id; ?>" title="Inactivo"><i class="fa fa-lock fa-2x" aria-hidden="true"></i></a></li>
                                                    @endif
                                                    @include('vendor.adminlte.catalogs.pricelists._modalStatus')
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                                {{ $priceLists->links() }}

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('vendor.adminlte.catalogs.pricelists._modalCreate')
    @include('vendor.adminlte.catalogs.pricelists._deleteQuoteCover')
    @include('vendor.adminlte.catalogs.pricelists._deleteFolderMIp')
@endsection

@section('personal-js')
    <script>
        $("#plagues").select2();
        //select job center
        $('#selectJobCenterPriceList').on('change', function() {
            let idJobCenterPriceList = $(this).val();
            window.location.href = route('list_prices', idJobCenterPriceList);
        });
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/build/priceLists.js') }}"></script>
    <script src="{{ asset('js/bootstable.js') }}" ></script>
    <script src="https://use.fontawesome.com/92e776c0ac.js"></script>
    <script>
        $(function() {

            $('#makeEditablePrices').SetEditable({ $addButton: $('#but_add_price')});

            $('#saveListPrice').on('click',function() {
                saveListPrice();
            });

            let priceValue, isIndividualPrice, tablePrice;
            isIndividualPrice = $("#is_individual_price");
            isIndividualPrice.change(function() {
                priceValue = document.getElementById("value_individual_price");
                tablePrice = document.getElementById("divTablePrice");
                if (this.checked) {
                    priceValue.disabled = false
                    tablePrice.style.display = "none"
                }else{
                    priceValue.disabled = true
                    priceValue.value = 0;
                    tablePrice.style.display = "block"
                }
            });

            function saveListPrice() {

                //get values input
                let name = $('#name').val();
                let key = $('#key').val();
                let establishment = $('#establishment option:selected').val();
                let selectJobCenterPriceList = $('#selectJobCenterPriceList option:selected').val();
                let description = $('#description').val();
                let hierarchy = $('#hierarchy').val();
                let indications = $('#indications option:selected').val();
                let plagues = $('#plagues').val();
                let reinforcementDays = $('#reinforcementDays').val();
                let warrantyQuotation = $('#warrantyQuotation').val();
                let descriptionBilling = $('#descriptionBilling').val();
                let legend = $('#legend').val();
                let customerPortal = document.getElementById('customerPortal').checked;
                let customerShowPrice = document.getElementById('customerShowPrice').checked;
                let isDisinfection = document.getElementById('is_disinfection').checked;
                let dateExpirationCertificate = $('#dateExpirationCertificate').val();
                let serviceCodeSatId = document.getElementById('productCodeSatId').value;
                let serviceUnitSatId = document.getElementById('productUnitSatId').value;
                let priceValue = document.getElementById("value_individual_price").value;
                let isIndividualPrice = document.getElementById("is_individual_price").checked;

                //get data table prices
                var arrayConcepts = [];
                var td = TableToCSV('makeEditablePrices', ',');
                var ar_lines = td.split("\n");
                var each_data_value = [];
                arrayConcepts = [];
                ar_lines.pop();
                for(i=0;i<ar_lines.length;i++)
                {
                    each_data_value[i] = ar_lines[i].split(",");
                }

                for(i=0;i<each_data_value.length;i++)
                {
                    // object json
                    arrayConcepts.push({
                        'area': each_data_value[i][0],
                        'quantity': each_data_value[i][1],
                        'price_one': each_data_value[i][2],
                        'price_two': each_data_value[i][3]
                    });

                }

                //validations
                if (name.length === 0){
                    showToast('warning','Nombre Lista','Ingresa el Nombre de Lista');
                }else if (key.length === 0){
                    showToast('warning','Clave','Ingresa la Clave');
                }else if (key.length > 10){
                    showToast('warning','Clave','La Clave debe ser de máximo 10 caracteres');
                }else if(establishment == 0) {
                    showToast('warning','Tipo de Servicio','Selecciona el Tipo de Servicio');
                }else if(plagues.length == 0){
                    showToast('warning','Plagas','Selecciona por lo menos una Plaga');
                }else if(description.length === 0){
                    showToast('warning','Descripción','Ingresa la Descripción');
                }else if(hierarchy.length === 0){
                    showToast('warning','Jerarquía','Ingresa la Jerarquía');
                }else if(indications == 0){
                    showToast('warning','Indicaciones','Selecciona las Indicaciones');
                }else{
                    loaderPestWare('');
                    $.ajax({
                        type: 'GET',
                        url: route('store_list_prices'),
                        data: {
                            _token: $("meta[name=csrf-token]").attr("content"),
                            name: name,
                            key: key,
                            establishment: establishment,
                            selectJobCenterPriceList: selectJobCenterPriceList,
                            plagues: plagues,
                            hierarchy: hierarchy,
                            description: description,
                            legend: legend,
                            indications: indications,
                            reinforcementDays: reinforcementDays,
                            customerPortal: customerPortal,
                            customerShowPrice: customerShowPrice,
                            dateExpirationCertificate: dateExpirationCertificate,
                            warrantyQuotation: warrantyQuotation,
                            isDisinfection: isDisinfection,
                            descriptionBilling: descriptionBilling,
                            serviceCodeSatId: serviceCodeSatId,
                            serviceUnitSatId: serviceUnitSatId,
                            priceValue: priceValue,
                            isIndividualPrice: isIndividualPrice,
                            arrayConcepts: JSON.stringify(arrayConcepts)
                        },
                        success: function (data) {
                            if (data.errors){
                                Swal.close();
                                showToast('error','Lista de Precio', 'Error al Guardar la Lista de Precio');
                            }
                            else{
                                Swal.close();
                                showToast('success-redirect','Lista de Precio','Guardando','list_prices');
                            }
                        }
                    });
                }

            }

            $('#importNewList').on('click',function() {
                importNewList();
            });

            function importNewList() {

                //get values input
                let name = $('#name').val();
                let key = $('#key').val();
                let establishment = $('#establishment').val();
                let description = $('#description').val();
                let hierarchy = $('#hierarchy').val();
                let indications = $('#indications').val();
                let plagues = $('#plagues').val();

                //validations
                if (name.length === 0){
                    showToast('warning','Nombre Lista','Ingresa el Nombre de Lista');
                }else if (key.length === 0){
                    showToast('warning','Clave','Ingresa la Clave');
                }else if (key.length > 10){
                    showToast('warning','Clave','La Clave debe ser de máximo 10 caracteres');
                }else if(establishment == 0) {
                    showToast('warning','Tipo de Servicio','Selecciona el Tipo de Servicio');
                }else if(plagues.length == 0){
                    showToast('warning','Plagas','Selecciona por lo menos una Plaga');
                }else if(description.length === 0){
                    showToast('warning','Descripción','Ingresa la Descripción');
                }else if(hierarchy.length === 0){
                    showToast('warning','Jerarquía','Ingresa la Jerarquía');
                }else if(indications == 0){
                    showToast('warning','Indicaciones','Selecciona las Indicaciones');
                }else{

                    $('#nameModal').val(name);
                    $('#keyModal').val(key);
                    $('#establishmentModal').val(establishment);
                    $('#descriptionModal').val(description);
                    $('#hierarchyModal').val(hierarchy);
                    $('#indicationsModal').val(indications);
                    $('#plaguesModal').val(plagues);
                    $("#importExcel").modal();
                }
            }

            $('#importFilePricesCreate').on('click', function () {
                let file = $('#fileImport').val();
                if (file)
                    loaderPestWare('Importando lista de precios...');
                else showToast('error','Lista Excel','Debes seleccionar un archivo de excel');
            });

            //alerts messages

            function loaderPestWare(message) {
                Swal.fire({
                    html: `<div class="content-loader">
                    <div class="loader">
                        <span style="--i:1;"></span>
                        <span style="--i:2;"></span>
                        <span style="--i:3;"></span>
                        <span style="--i:4;"></span>
                        <span style="--i:5;"></span>
                        <span style="--i:6;"></span>
                        <span style="--i:7;"></span>
                        <span style="--i:8;"></span>
                        <span style="--i:9;"></span>
                        <span style="--i:10;"></span>
                        <span style="--i:11;"></span>
                        <span style="--i:12;"></span>
                        <span style="--i:13;"></span>
                        <span style="--i:14;"></span>
                        <span style="--i:15;"></span>
                        <span style="--i:16;"></span>
                        <span style="--i:17;"></span>
                        <span style="--i:18;"></span>
                        <span style="--i:19;"></span>
                        <span style="--i:20;"></span>
                        <div class="insect"></div>
                    </div>
                </div>
                <p class="text-center" style="color: #ffffff"><b>${message}</b></p>`,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false
                });
                $(".swal2-modal").css('background-color', 'transparent');
                $('.swal2-container.swal2-shown').css('background', 'rgb(11 12 12 / 55%)');
            }

            function showToast(type, title, message, path) {
               if (type === 'success-redirect') {
                    toastr.success(message, title);
                    setTimeout(() => {
                        window.location.href = route(path);
                    }, 5000);
                }
               else if (type === 'success') toastr.success(message, title)
               else if (type === 'info') toastr.info(message, title)
               else if (type === 'warning') toastr.warning(message, title)
               else if (type === 'error') toastr.error(message, title)
            }

        });
    </script>
@endsection