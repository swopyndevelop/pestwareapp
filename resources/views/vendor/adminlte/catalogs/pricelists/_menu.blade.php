
<ul class="dropdown-menu">
	<li><a data-toggle="modal" data-target="#ModalPriceListDetail<?php echo $a->id; ?>" style="cursor: pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a></li>
	<li><a href="{{ route('view_pdf_price',['id' => \Vinkla\Hashids\Facades\Hashids::encode($a->id)])}}"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color:darkred"></i>Portada Cotización</a></li>
	<li><a href="{{ route('view_pdf_portada',['id' => \Vinkla\Hashids\Facades\Hashids::encode($a->id)])}}"><i class="fa fa-file-text-o" aria-hidden="true" style="color:dodgerblue"></i>Carpeta MIP</a></li>
	<li><a href="{{ route('edit_prices', ['id' => \Vinkla\Hashids\Facades\Hashids::encode($a->id)]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i>Editar</a></li>
	<li><a data-toggle="modal" data-target="#delete<?php echo $a->id; ?>" style="cursor: pointer"><i class="fa fa-trash" aria-hidden="true" style="color: red;"></i>Eliminar</a></li>
</ul>
@include('vendor.adminlte.catalogs.pricelists._modalDelete')
@include('vendor.adminlte.catalogs.pricelists._modalDetail')