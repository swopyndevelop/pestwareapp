<!-- START MODAL DE  -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalPriceList" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Lista de Precio</h4>
                                <div class="row form-group">

                                    <div class="col-md-6">
                                        <span style="font-weight: bold;">Nombre de Lista</span>
                                        <input class="form-control" type="text" name="name" id="name" placeholder="Mantenimiento">
                                    </div>

                                    <div class="col-md-3">
                                        <span style="font-weight: bold;">Clave</span>
                                        <input class="form-control" type="text" name="key" id="key" placeholder="Mtto">
                                    </div>

                                    <div class="col-md-3">
                                        <span style="font-weight: bold;">Tipo de Servicio</span>
                                        <select class="form-control" name="establishment" id="establishment">
                                            <option value="0" disabled selected>Seleccione el tipo de servicio</option>
                                            @foreach($establishments as $establishment)
                                                <option value="{{ $establishment->id }}">{{ $establishment->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-md-9">
                                        <span style="font-weight: bold;">Plagas</span>
                                        <select name="plague[]" id="plagues" class="form-control multiple" style="width: 100%; font-weight: bold;" multiple="multiple">
                                            @foreach($plagues as $plague)
                                                <option value="{{ $plague->id }}">{{ $plague->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <span style="font-weight: bold;">Jerarquía</span>
                                        <input type="number" min="0" class="form-control" name="hierarchy" id="hierarchy" value="1">
                                    </div>

                                </div>

                                <div class="row form-group">

                                    <div class="col-md-9">
                                        <span style="font-weight: bold;">Descripción</span>
                                        <textarea class="form-control" rows="6" id="description" name="description" placeholder="Este servicio aplica unicamente para fumigación de interiores y exteriores de tu domicilio, tiene una garantía de 30 días sujeta a condiciones."></textarea>
                                    </div>

                                    <div class="col-md-3">
                                        <span style="font-weight: bold;">Indicaciones</span>
                                        <select class="form-control" name="indications" id="indications">
                                            <option value="0" disabled selected>Seleccione el tipo de indicaciones</option>
                                            @foreach($indications as $indication)
                                                <option value="{{ $indication->id }}">{{ $indication->name }}</option>
                                            @endforeach
                                        </select>
                                        <span style="font-weight: bold;">Frecuencia para Refuerzo (Días)</span>
                                        <input type="number" class="form-control" name="reinforcementDays" id="reinforcementDays" placeholder="21">
                                        <span style="font-weight: bold;">Expiración Certificado (Días)</span>
                                        <input type="number" class="form-control" name="dateExpirationCertificate" id="dateExpirationCertificate" placeholder="30" value="0">
                                    </div>

                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3" @if($company->id_code_country != 2) style="display: none;" @endif>
                                        <span style="font-weight: bold;">Descripción Facturación</span>
                                        <textarea class="form-control" rows="4" id="descriptionBilling" name="descriptionBilling" placeholder="Descripción."></textarea>
                                    </div>
                                    <div class="col-md-6" @if($company->id_code_country != 2) style="display: none;" @endif>
                                        <div class="col-sm-8 formulario__grupo" id="grupo__unit_code_billing">
                                            <label class="control-label" for="tipo" style="font-weight: bold;">Asocia tu servicio al cátalogo del SAT:</label>
                                            <input type="text" class="form-control" name="productCodeSat" id="productCodeSat" value="70141605 - Servicios de manejo integrado de plagas">
                                            <input type="hidden" name="productCodeSatId" id="productCodeSatId" value="70141605">
                                            <div id="productCodeSatList"></div>
                                        </div>
                                        <div class="col-sm-4 formulario__grupo" id="grupo__unit_code_billing">
                                            <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                            <a href="http://200.57.3.89/PyS/catPyS.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Clave</a>
                                        </div>
                                        <div class="col-sm-8 formulario__grupo" id="grupo__unit_code_billing">
                                            <label class="control-label" for="tipo" style="font-weight: bold;">Define la unidad de tu servicio</label>
                                            <input type="text" class="form-control" name="productUnitSat" id="productUnitSat" value="E48 - Unidad de Servicio">
                                            <input type="hidden" name="productUnitSatId" id="productUnitSatId" value="E48">
                                            <div id="productUnitSatList"></div>
                                        </div>

                                        <div class="col-sm-4 formulario__grupo" id="grupo__unit_code_billing">
                                            <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                            <a href="http://200.57.3.89/PyS/catUnidades.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Unidad</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" class="form-check-input" name="customerPortal" id="customerPortal" checked @if(Auth::user()->id_plan != 3) disabled @endif>
                                        <span style="font-weight: bold; margin-right: 10px;">Portal de Clientes</span><br>
                                        <input type="checkbox" class="form-check-input" name="customerShowPrice" id="customerShowPrice" checked>
                                        <span style="font-weight: bold; margin-right: 10px;">Mostrar Precio Certificado</span><br>
                                        <input type="checkbox" class="form-check-input" name="is_disinfection" id="is_disinfection">
                                        <span style="font-weight: bold;">¿Es desinfección?</span>
                                    </div>
                                    <div class="col-md-6">
                                        <span style="font-weight: bold;">Leyenda Cotización</span>
                                        <textarea class="form-control" rows="4" id="legend" name="legend" placeholder="*1.-Cotización válida por 30 días naturales." value=""></textarea>
                                    </div>
                                </div>

                                <div class="row" style="display: block;" id="divTablePrice">
                                    <div class="col-md-12">
                                        <table class="table table-responsive-md table-sm table-bordered" id="makeEditablePrices">
                                            <thead>
                                            <tr>
                                                <th>Escala</th>
                                                <th>Cantidad</th>
                                                <th>Precio 1</th>
                                                <th>Precio 2</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="row container-fluid">
                                <div class="col-md-4 align-self-start">
                                    <a class="btn btn-secondary btn-sm" type="button" id="importNewList">
                                        <i class="fa fa-file-excel-o"></i> Importar lista desde Excel
                                    </a>
                                    <a href="{{ route('format_excel_price') }}" class="btn btn-secondary btn-sm" type="button">
                                        <i class="fa fa-file-excel-o"></i> Descargar formato
                                    </a>

                                </div>
                                <div class="col-md-4">
                                    <input type="checkbox" class="form-check-input" name="is_individual_price" id="is_individual_price">
                                    <span style="font-weight: bold;">Costo Individual</span>
                                    <input class="form-control" type="number" name="value_individual_price" id="value_individual_price" value="0" style="width: 30%" disabled>
                                </div>

                                <div class="col-md-4 align-self-end">
                                    <span style="float:right"><button id="but_add_price" class="btn btn-danger">+</button></span>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <button class="btn btn-primary" type="button" id="saveListPrice" name="saveListPrice">Guardar Lista</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('vendor.adminlte.catalogs.pricelists._modalImportCreateExcel')
<!-- END MODAL DE LISTA DE PRECIO -->
