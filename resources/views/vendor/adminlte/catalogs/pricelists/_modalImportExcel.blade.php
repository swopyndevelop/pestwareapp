<!-- INICIA MODAL PARA IMPORTAR LISTA DESDE EXCEL -->
<div class="modal fade" id="importExcel" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
                <form action="{{ route('import_excel_price') }}" method="POST" id="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="modal-body text-center">
                        <h5 class="modal-title">Seleccione la lista de precios en formato excel</h5>
                        <div class="col-md-8 col-md-offset-2">
                            <input type="file" class="form-control" id="fileImport" name="fileImport" required="required"
                                   accept=".xls">
                            <input type="number" name="idPriceList" hidden value="{{ $pricesList->id }}">
                        </div>
                    </div>
                    <br><br>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <div class="text-center">
                            <button type="submit" id="importFilePrices" title="Importar" class="btn btn-primary btn-sm"><strong>Importar</strong></button>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA IMPORTAR LISTA DESDE EXCEL -->