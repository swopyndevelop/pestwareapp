@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('css-personal')
    @include('adminlte::catalogs.pricelists.PersonalSelect2')
@stop

@section('main-content')
    <!-- START MODAL DE LISTA DE PRECIO -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="ModalPriceList" role="dialog" aria-labelledby="newEmployeesModal"
             style="overflow-y: scroll;">
            <div class="row">
                @if(session('message'))
                    <div class="col-md-4 col-md-offset-4">
                        <div class="alert alert-success alert-dismissible text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ session('message') }}</strong>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <!--Default box-->
                    <div class="modal-dialog" role="document" style="width: 90% !important;">

                        <div class="modal-content">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal"
                                                aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <input type="number" name="id" id="id" hidden value="{{ $pricesList->id }}">
                                    </div>
                                </div>

                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Lista
                                        de Precio: <b>{{ $pricesList->name }}</b></h4>
                                    <div class="row form-group">

                                        <div class="col-md-6">
                                            <span style="font-weight: bold;">Nombre de Lista</span>
                                            <input class="form-control" type="text" name="name" id="name"
                                                   placeholder="Mantenimiento" value="{{ $pricesList->name }}">
                                        </div>

                                        <div class="col-md-3">
                                            <span style="font-weight: bold;">Clave</span>
                                            <input class="form-control" type="text" name="key" id="key"
                                                   placeholder="Mtto" value="{{ $pricesList->key }}">
                                        </div>

                                        <div class="col-md-3">
                                            <span style="font-weight: bold;">Tipo de Servicio</span>
                                            <select class="form-control" name="establishment" id="establishment">
                                                <option value="{{ $pricesList->establishment_id }}" selected="true">
                                                    {{ $pricesList->establishment }}
                                                </option>
                                                @foreach($establishments as $establishment)
                                                    @if($pricesList->establishment_id != $establishment->id)
                                                        <option value="{{ $establishment->id}}"
                                                                {{ (in_array($establishment, old('establishments', []))) }} >{{ $establishment->name }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row form-group">

                                        <div class="col-md-9">
                                            <span style="font-weight: bold;">Plagas</span>
                                            <select name="plague[]" id="plagues" class="form-control multiple"
                                                    style="width: 100%; font-weight: bold;" multiple="multiple">
                                                @foreach($plagues as $plague)
                                                    <option value="{{ $plague->id }} }}"
                                                            @foreach($plaguesList as $plagueList)
                                                            @if($plagueList->plague_id == $plague->id)
                                                            selected='selected'
                                                            @endif
                                                            @endforeach
                                                            {{ (in_array($plague, old('plagues', []))) }} >{{ $plague->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <span style="font-weight: bold;">Jerarquía</span>
                                            <input type="number" min="0" class="form-control" name="hierarchy"
                                                   id="hierarchy" placeholder="3" value="{{ $pricesList->hierarchy }}">
                                        </div>

                                    </div>

                                    <div class="row form-group">

                                        <div class="col-md-9">
                                            <span style="font-weight: bold;">Descripción</span>
                                            <textarea class="form-control" rows="6" id="description" name="description"
                                                      placeholder="Este servicio aplica unicamente para fumigación de interiores y exteriores de tu domicilio, tiene una garantía de 30 días sujeta a condiciones.">{{ $pricesList->description }}</textarea>
                                        </div>

                                        <div class="col-md-3">
                                            <span style="font-weight: bold;">Indicaciones</span>
                                            <select class="form-control" name="indications" id="indications">
                                                <option value="{{ $pricesList->indications_id }}" selected="true">
                                                    {{ $pricesList->indications }}
                                                </option>
                                                @foreach($indications as $indication)
                                                    @if($pricesList->indications_id != $indication->id)
                                                        <option value="{{ $indication->id}}"
                                                                {{ (in_array($indication, old('indications', []))) }} >{{ $indication->name }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <span style="font-weight: bold;">Frecuencia para Refuerzo (Días)</span>
                                            <input type="number" class="form-control" name="reinforcementDays" id="reinforcementDays" placeholder="21" value="{{ $pricesList->reinforcement_days }}">
                                            <span style="font-weight: bold;">Expiración Certificado (Días)</span>
                                            <input type="number" class="form-control" name="dateExpirationCertificate" id="dateExpirationCertificate" placeholder="30" value="{{ $pricesList->days_expiration_certificate }}">
                                        </div>

                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-3" @if($company->id_code_country != 2) style="display: none;" @endif>
                                            <span style="font-weight: bold;">Descripción Facturación</span>
                                            <textarea class="form-control" rows="4" id="descriptionBilling" name="descriptionBilling" placeholder="Descripción para factura del servicio.">{{ $pricesList->description_billing }}</textarea>
                                        </div>
                                        <div class="col-md-6" @if($company->id_code_country != 2) style="display: none;" @endif>
                                            <div class="col-sm-8 formulario__grupo" id="grupo__unit_code_billing">
                                                <label class="control-label" for="tipo" style="font-weight: bold;">Asocia tu servicio al cátalogo del SAT:</label>
                                                <input type="text" class="form-control" name="productCodeSat" id="productCodeSat" @if($productCodeSat != null) value="{{ $productCodeSat->Value }} - {{ $productCodeSat->Name }}" @else value="70141605 - Servicios de manejo integrado de plagas" @endif>
                                                <input type="hidden" name="productCodeSatId" id="productCodeSatId" @if($productCodeSat != null) value="{{ $productCodeSat->Value }}" @else value="70141605" @endif>
                                                <div id="productCodeSatList"></div>
                                            </div>
                                            <div class="col-sm-4 formulario__grupo" id="grupo__unit_code_billing">
                                                <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                <a href="http://200.57.3.89/PyS/catPyS.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Clave</a>
                                            </div>
                                            <div class="col-sm-8 formulario__grupo" id="grupo__unit_code_billing">
                                                <label class="control-label" for="tipo" style="font-weight: bold;">Define la unidad de tu servicio</label>
                                                <input type="text" class="form-control" name="productUnitSat" id="productUnitSat" @if($productUnitSat != null) value="{{ $productUnitSat->key }} - {{ $productUnitSat->name }}" @else value="E48 - Unidad de Servicio" @endif>
                                                <input type="hidden" name="productUnitSatId" id="productUnitSatId" @if($productUnitSat != null) value="{{ $productUnitSat->id }}" @else value="E48" @endif>
                                                <div id="productUnitSatList"></div>
                                            </div>

                                            <div class="col-sm-4 formulario__grupo" id="grupo__unit_code_billing">
                                                <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                <a href="http://200.57.3.89/PyS/catUnidades.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Unidad</a>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="checkbox" class="form-check-input" name="customerPortal" id="customerPortal" @if($pricesList->customer_portal != 0) checked @endif @if(Auth::user()->id_plan != 3) disabled @endif>
                                            <span style="font-weight: bold; margin-right: 10px;">Portal de Clientes</span><br>
                                            <input type="checkbox" class="form-check-input" name="customerShowPrice" id="customerShowPrice" @if($pricesList->show_price != 0) checked @endif>
                                            <span style="font-weight: bold; margin-right: 10px;">Mostrar Precio Certificado</span><br>
                                            <input type="checkbox" class="form-check-input" name="is_disinfection" id="is_disinfection" @if($pricesList->is_disinfection != 0) checked @endif>
                                            <span style="font-weight: bold;">¿Es desinfección?</span>
                                        </div>
                                        <div class="col-md-6">
                                            <span style="font-weight: bold;">Leyenda Cotización</span>
                                            <textarea class="form-control" rows="4" id="legend" name="legend" placeholder="*1.-Cotización válida por 30 días naturales." value="">{{ $pricesList->legend }}</textarea>
                                        </div>
                                    </div>

                                    <div class="row" @if($pricesList->individual_price == 0) style="display: block;" @else style="display: none;" @endif id="divTablePrice">
                                        <div class="col-md-12">
                                            <table class="table table-responsive-md table-sm table-bordered"
                                                   id="makeEditablePricesEdit">
                                                <thead>
                                                <tr>
                                                    <th>Escala</th>
                                                    <th>Cantidad</th>
                                                    <th>Precio 1</th>
                                                    <th>Precio 2</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($prices as $price)
                                                    <tr>
                                                        <td>{{ $price->area }}</td>
                                                        <td>{{ $price->quantity }}</td>
                                                        <td>{{ $price->price_one }}</td>
                                                        <td>{{ $price->price_two }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row container-fluid">
                                    <div class="col-md-4 align-self-start">
                                        <a data-toggle="modal" data-target="#importExcel" class="btn btn-secondary btn-sm" type="button">
                                            <i class="fa fa-file-excel-o"></i> Importar lista desde Excel
                                        </a>
                                        <a href="{{Route('export_excel_price', $pricesList->id)}}" class="btn btn-secondary btn-sm" type="button">
                                            <i class="fa fa-file-excel-o"></i> Exportar lista a Excel
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="checkbox" class="form-check-input" name="is_individual_price" id="is_individual_price" @if($pricesList->individual_price != 0) checked @endif>
                                        <span style="font-weight: bold;">Costo Individual</span>
                                        <input class="form-control" type="number" name="value_individual_price" id="value_individual_price" value="{{ $pricesList->value_individual_price }}" @if($pricesList->individual_price == 0) disabled @endif style="width: 30%">
                                    </div>
                                    <div class="col-md-4 align-self-end">
                                        <span style="float:right"><button id="but_add_price_Edit" class="btn btn-primary">+</button></span>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <div class="text-center">
                                        <button class="btn btn-primary btn-sm" type="button" id="editListPrice" name="editListPrice">Editar Lista</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('vendor.adminlte.catalogs.pricelists._modalImportExcel')
    <!-- END MODAL DE LISTA DE PRECIO -->
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/priceLists.js') }}"></script>
    <script>
        $("#plagues").select2();
        $("#ModalPriceList").modal();
        $("#ModalPriceList").on('hidden.bs.modal', function () {
            window.location.href = '{{route("list_prices")}}'; //using a named route
        });
    </script>

    <script>
        $(function () {

            //select payment way
            $('#typePayment').on('change', function () {
                if ($(this).val() === "1") {
                    $("#days").prop("disabled", false);
                } else {
                    $("#days").prop("disabled", true);
                }
            });

        });
    </script>
    <script src="{{ asset('js/bootstable.js') }}"></script>
    <script src="https://use.fontawesome.com/92e776c0ac.js"></script>
    <script>
        $(function () {

            //edit
            $('#makeEditablePricesEdit').SetEditable({$addButton: $('#but_add_price_Edit')});

            let priceValue, isIndividualPrice, tablePrice;
            isIndividualPrice = $("#is_individual_price");
            isIndividualPrice.change(function() {
                priceValue = document.getElementById("value_individual_price");
                tablePrice = document.getElementById("divTablePrice");
                if (this.checked) {
                    priceValue.disabled = false
                    tablePrice.style.display = "none"
                }else{
                    priceValue.disabled = true
                    priceValue.value = 0;
                    tablePrice.style.display = "block"
                }
            });

            $('#editListPrice').on('click', function () {
                //validations
                //get values input
                let name = $('#name').val();
                let key = $('#key').val();
                let establishment = $('#establishment').val();
                let description = $('#description').val();
                let hierarchy = $('#hierarchy').val();
                let indications = $('#indications').val();
                let plagues = $('#plagues').val();
                let id = $('#id').val();
                let reinforcementDays = $('#reinforcementDays').val();
                let customerPortal = document.getElementById('customerPortal').checked;
                let customerShowPrice = document.getElementById('customerShowPrice').checked;
                let isDisinfection = document.getElementById('is_disinfection').checked;
                let warrantyQuotation = $('#warrantyQuotation').val();
                let descriptionBilling = $('#descriptionBilling').val();
                let legend = $('#legend').val();
                let dateExpirationCertificate = $('#dateExpirationCertificate').val();
                let serviceCodeSatId = document.getElementById('productCodeSatId').value;
                let serviceUnitSatId = document.getElementById('productUnitSatId').value;
                let priceValue = document.getElementById("value_individual_price").value;
                let isIndividualPrice = document.getElementById("is_individual_price").checked;

                //get data table prices
                var arrayConceptsEdit = [];
                var td = TableToCSV('makeEditablePricesEdit', ',');
                var ar_lines = td.split("\n");
                var each_data_value = [];
                arrayConceptsEdit = [];
                ar_lines.pop();
                for (i = 0; i < ar_lines.length; i++) {
                    each_data_value[i] = ar_lines[i].split(",");
                }

                for (i = 0; i < each_data_value.length; i++) {
                    // object json
                    arrayConceptsEdit.push({
                        'area': each_data_value[i][0],
                        'quantity': each_data_value[i][1],
                        'price_one': each_data_value[i][2],
                        'price_two': each_data_value[i][3]
                    });

                }

                //validations
                if (name.length === 0){
                    showToast('warning','Nombre Lista','Ingresa el Nombre de Lista');
                }else if (key.length === 0){
                    showToast('warning','Clave','Ingresa la Clave');
                }else if (key.length > 10){
                    showToast('warning','Clave','La Clave debe ser de máximo 10 caracteres');
                }else if(establishment == 0) {
                    showToast('warning','Tipo de Servicio','Selecciona el Tipo de Servicio');
                }else if(plagues.length == 0){
                    showToast('warning','Plagas','Selecciona por lo menos una Plaga');
                }else if(description.length === 0){
                    showToast('warning','Descripción','Ingresa la Descripción');
                }else if(hierarchy.length === 0){
                    showToast('warning','Jerarquía','Ingresa la Jerarquía');
                }else if(indications == 0){
                    showToast('warning','Indicaciones','Selecciona las Indicaciones');
                }else{
                    //loaderPestWare('Lista de Precio');
                    $.ajax({
                        type: 'POST',
                        url: route('update_list_prices'),
                        data: {
                            _token: $("meta[name=csrf-token]").attr("content"),
                            id: id,
                            name: name,
                            key: key,
                            establishment: establishment,
                            plagues: plagues,
                            hierarchy: hierarchy,
                            description: description,
                            indications: indications,
                            reinforcementDays: reinforcementDays,
                            customerPortal: customerPortal,
                            customerShowPrice: customerShowPrice,
                            dateExpirationCertificate: dateExpirationCertificate,
                            warrantyQuotation: warrantyQuotation,
                            isDisinfection: isDisinfection,
                            descriptionBilling: descriptionBilling,
                            legend: legend,
                            serviceCodeSatId: serviceCodeSatId,
                            serviceUnitSatId: serviceUnitSatId,
                            priceValue: priceValue,
                            isIndividualPrice: isIndividualPrice,
                            arrayConcepts: JSON.stringify(arrayConceptsEdit)
                        },
                        success: function (data) {
                            if (data.errors) {
                                Swal.close();
                                showToast('error','Lista de Precio', 'Error al Guardar la Lista de Precio');
                            } else {
                                Swal.close();
                                showToast('success-redirect','Lista de Precio','Guardando','list_prices');
                            }
                        }
                    });
                }
            });

            $('#importFilePrices').on('click', function () {
                let file = $('#fileImport').val();
                if (file)
                    loaderPestWare('Importando lista de precios...');
                else showToast('error','Lista Excel','Debes seleccionar un archivo de excel');
            });

            //alerts messages
            function loaderPestWare(message) {
                Swal.fire({
                    html: `<div class="content-loader">
                    <div class="loader">
                        <span style="--i:1;"></span>
                        <span style="--i:2;"></span>
                        <span style="--i:3;"></span>
                        <span style="--i:4;"></span>
                        <span style="--i:5;"></span>
                        <span style="--i:6;"></span>
                        <span style="--i:7;"></span>
                        <span style="--i:8;"></span>
                        <span style="--i:9;"></span>
                        <span style="--i:10;"></span>
                        <span style="--i:11;"></span>
                        <span style="--i:12;"></span>
                        <span style="--i:13;"></span>
                        <span style="--i:14;"></span>
                        <span style="--i:15;"></span>
                        <span style="--i:16;"></span>
                        <span style="--i:17;"></span>
                        <span style="--i:18;"></span>
                        <span style="--i:19;"></span>
                        <span style="--i:20;"></span>
                        <div class="insect"></div>
                    </div>
                </div>
                <p class="text-center" style="color: #ffffff"><b>${message}</b></p>`,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false
                });
                $(".swal2-modal").css('background-color', 'transparent');
                $('.swal2-container.swal2-shown').css('background', 'rgb(11 12 12 / 55%)');
            }

            function showToast(type, title, message, path) {
                if (type === 'success-redirect') {
                    toastr.success(message, title);
                    setTimeout(() => {
                        window.location.href = route(path);
                    }, 5000);
                }
                else if (type === 'success') toastr.success(message, title)
                else if (type === 'info') toastr.info(message, title)
                else if (type === 'warning') toastr.warning(message, title)
                else if (type === 'error') toastr.error(message, title)
            }

        });
    </script>
@endsection
