<!-- START MODAL DE ELIMINAR CARPETA MIP -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="deleteFolderMip" role="dialog" aria-labelledby="deleteFolderMip" tabindex="-1">
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="content modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>

              <div class="box-body">
                <div class="row container-fluid">
                    <div class="col-md-12 text-left">
                      <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">¿Estás seguro de eliminar la Carpeta MIP?<b id="quotationKey"></b></h4>
                    </div>
                </div>
              </div>

              <div class="modal-footer">
                <div class="text-center">
                  <div class="row">
                      <button class="btn btn-primary" type="button" id="deleteFolderMipButton">Eliminar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- END MODAL DE ELIMINAR CARPETA MIP -->
