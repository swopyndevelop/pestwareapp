<!-- INICIA MODAL PARA IMPORTAR LISTA DESDE EXCEL -->
<div class="modal fade" id="importExcel" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <form action="{{ route('import_create_excel_price') }}" method="POST" id="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-body">
                    <h5 class="modal-title">Seleccione la lista de precios en formato excel</h5>
                    <div class="col-md-8 col-md-offset-2">
                        <input type="file" class="form-control" id="fileImport" name="fileImport" required="required"
                               accept=".xls">
                        <input type="text" name="name" id="nameModal" hidden>
                        <input type="text" name="key" id="keyModal" hidden>
                        <input type="number" name="hierarchy" id="hierarchyModal" hidden>
                        <input type="text" name="description" id="descriptionModal" hidden>
                        <input type="number" name="establishment" id="establishmentModal" hidden>
                        <input type="number" name="indications" id="indicationsModal" hidden>
                        <input type="text" name="plagues" id="plaguesModal" hidden>
                    </div>
                </div>
                <br><br>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" id="importFilePricesCreate" title="Importar" style="background-color: green; border-color: green;" class="btn btn-info btn-sm"><strong>Importar</strong></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA IMPORTAR LISTA DESDE EXCEL -->