<!-- START MODAL CREATE ITEM CATALOG -->
<div class="container-fluid spark-screen">
<div class="modal fade" id="ModalEdit{{ $a }}" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            @if ($dat->type == 1)
                                <form action="{{ Route('update_sourceorigin') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 2)
                                <form action="{{ Route('update_typeservice') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 3)
                                <form action="{{ Route('update_discount') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 4)
                                <form action="{{ Route('update_extra') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 5)
                                <form action="{{ Route('update_methodapp') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 6)
                                <form action="{{ Route('update_grade') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 7)
                                <form action="{{ Route('update_cleaning') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 8)
                                <form action="{{ Route('update_plague') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 9)
                                <form action="{{ Route('update_presentation') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 10)
                                <form action="{{ Route('update_type') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 11)
                                <form action="{{ Route('update_units') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 12)
                                <form action="{{ Route('update_paymentmethod') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 13)
                                <form action="{{ Route('update_paymentway') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 14)
                                <form action="{{ Route('update_comprobant') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 15)
                                <form action="{{ Route('update_concepts') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 16)
                                <form action="{{ Route('update_indications') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 17)
                                <form action="{{ Route('update_stations') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 18)
                                <form action="{{ Route('update_plague_category') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 19)
                                <form action="{{ Route('update_taxes') }}" method="POST" id="form" enctype="multipart/form-data">
                            @elseif($dat->type == 20)
                                <form action="{{ Route('update_custom_descriptions') }}" method="POST" id="form" enctype="multipart/form-data">
                            @endif
                                {{ csrf_field() }}
                                <div class="box-body text-center">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Registro</h4>

                                    <input type="number" name="id" value="{{ $dat->id }}" hidden="hidden">
                                    <input class="form-control" type="text" name="name" @if($dat->type == 3) value="{{ $dat->title }}" @else value="{{ $dat->name }}" @endif placeholder="Nombre"  required>

                                    @if($dat->type == 3)
                                        <br>
                                        <input class="form-control" type="text" name="description" value="{{ $dat->description }}" placeholder="Descripción" required>
                                        <br>
                                        <input class="form-control" type="number" name="percentage" value="{{ $dat->percentage }}" placeholder="Porcentaje %" required>
                                    @endif

                                    @if($dat->type == 4)
                                        <br>
                                        <input class="form-control" type="text" name="description" value="{{ $dat->description }}" placeholder="Descripción" required>
                                        <br>
                                        <input class="form-control" type="number" name="amount" value="{{ $dat->amount }}" placeholder="Cantidad $" required>
                                    @endif

                                    @if($dat->type == 8)
                                        <br>
                                        <select class="form-control" name="plagueCategory" required id="plagueCategory">
                                            @foreach($categories as $category)
                                                @if($dat->id_categorie == $category->id)
                                                    <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                                @else
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <br>
                                        <span style="font-weight: bold;">Mostrar para Estaciones:</span>
                                        <br>
                                        <input type="checkbox" class="form-check-input" name="isMonitoringUV" id="isMonitoringUV" @if($dat->is_monitoring == 2 || $dat->is_monitoring == 3) checked @endif>
                                        <span style="font-weight: bold;">Lamparas UV</span>
                                        <br>
                                        <input type="checkbox" class="form-check-input" name="isMonitoringCapture" id="isMonitoringCapture" @if($dat->is_monitoring == 1 || $dat->is_monitoring == 3) checked @endif>
                                        <span style="font-weight: bold;">Trampas Captura</span>
                                    @endif

                                    @if($dat->type == 13)
                                        <br>
                                        <input class="form-control" type="text" name="creditDays" id="creditDays" placeholder="Días de Crédito" value="{{ $dat->credit_days }}">
                                    @endif

                                    @if($dat->type == 15)
                                        <br>
                                        <select class="form-control" name="type" required>
                                            @if($dat->tipo == 1)
                                                <option value="1" selected="true">Orden de Compra</option>
                                                <option value="2">Captura de Gasto</option>
                                            @else
                                                <option value="1">Orden de Compra</option>
                                                <option value="2" selected="true">Captura de Gasto</option>
                                            @endif
                                        </select>
                                    @endif

                                    @if($dat->type == 16)
                                        <br>
                                        <input class="form-control" type="text" name="key" value="{{ $dat->key }}" placeholder="Clave" required>
                                        <br>
                                        <textarea class="form-control" rows="5" name="description" placeholder="Indicaciones...">{{ $dat->description }}</textarea>
                                    @endif
                                    @if($dat->type == 17)
                                        <br>
                                        <input class="form-control" type="text" name="key" value="{{ $dat->key }}" placeholder="Clave" required>
                                        <br>
                                        <select class="form-control" name="typeArea" required>
                                            @if($dat->id_type_area == 4)
                                                <option value="4" selected="true">Estación</option>
                                                <option value="5">Trampa</option>
                                            @else
                                                <option value="4">Estación</option>
                                                <option value="5" selected="true">Trampa</option>
                                            @endif
                                        </select>
                                    @endif
                                    @if($dat->type == 19)
                                        <br>
                                        <input class="form-control" type="number" name="value" value="{{ $dat->value }}" placeholder="Value" required>
                                    @endif
                                    @if($dat->type == 20)
                                        <br>
                                        <textarea class="form-control" rows="15" name="description" placeholder="Descripción del Servicio..." required>{{ $dat->description }}</textarea>
                                    @endif

                                </div>

                                <div class="modal-footer">
                                    <br>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">Actualizar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CREATE ITEM CATALOG -->
