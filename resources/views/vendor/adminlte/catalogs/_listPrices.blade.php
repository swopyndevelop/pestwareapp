@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content') 
<div class="content container-fluid spark-screen">
    <div class="row">
        @if(session('success'))
            <script>toastr.success("{{ session('success') }}")</script>
        @endif
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Listado</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            
                            <div class="panel panel-primary" style="border-color: #bce8f1;">

                                <h3 class="text-center">Lista de Precios</h3>
                            
                                <div class="pull-right">
                                    <a href="javascript:void(0)" data-perform="panel-collapse">
                                        <i class="ti-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" data-perform="panel-dismiss">
                                        <i class="ti-close"></i>
                                    </a>
                                </div>
                            
                                <div class="panel-body">

                                    <div class="table-responsive">

                                        <table class="table table-hover table-condensed table-bordered" id="tablePrices">
                                            <thead>
                                                <tr class="active">
                                                    <th class="text-center">Categoría</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th>Mantenimiento</th>
                                                    <th></th>
                                                    <th class="text-right">Cucaracha</th>
                                                    <th>Alemana</th>
                                                    <th class="text-right">Hematófagos</th>
                                                    <th></th>
                                                    <th class="text-center">Termitas</th>
                                                    <th></th>
                                                    <th class="text-center">Roedores</th>
                                                    <th></th>
                                                </tr>
                                                <tr class="active">
                                                    <th class="text-center">Jerarquía</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="text-center">3</th>
                                                    <th></th>
                                                    <th class="text-right">2</th>
                                                    <th></th>
                                                    <th class="text-center">1</th>
                                                    <th></th>
                                                    <th class="text-center">0</th>
                                                    <th></th>
                                                    <th class="text-center">0</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <th class="active text-center">Dimensiones en m2</th>
                                                    <th class="info text-center">BIODEGRADABLE</th>
                                                    <th class="success text-center">ORGÁNICO</th>
                                                    <th class="success text-center">CUALQUIER SERVICICIO <br> + JARDIN EXTERIOR</th>
                                                    <th class="success text-center">SOLO JARDIN EXTERIOR</th>
                                                    <th class="warning text-center">1ª APLICACIÓN</th>
                                                    <th class="warning text-center">REFUERZO</th>
                                                    <th class="danger text-center">1ª APLICACIÓN</th>
                                                    <th class="danger text-center">REFUERZO</th>
                                                    <th style="background: #EDBB99;" class="text-center">TRATAMIENTO</th>
                                                    <th style="background: #D7DBDD;" class="text-center">CANTIDAD DE TRAMPAS</th>
                                                    <th style="background: #D7DBDD;" class="text-center">ESTACIONES CEBO</th>
                                                    <th style="background: #D7DBDD;" class="text-center">TRAMPAS PEGAMENTO</th>
                                                    <th class="active text-center">Editar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php($a = 0)
                                            @foreach($prices as $price)
                                                @php($a = $price->id)
                                                <tr>
                                                    <td class="text-center"><b>{{ $price->area }}</b></td>
                                                    <td class="text-center">${{ $price->MTTO_biodegradable }}</td>
                                                    <td class="text-center">${{ $price->MTTO_organic }}</td>
                                                    <td class="text-center">${{ $price->MTTO_service_plus_garden }}</td>
                                                    <td class="text-center">${{ $price->MTTO_garden }}</td>
                                                    <td class="text-center">${{ $price->CA_1_application }}</td>
                                                    <td class="text-center">${{ $price->CA_reinforcement }}</td>
                                                    <td class="text-center">${{ $price->CH_1_application }}</td>
                                                    <td class="text-center">${{ $price->CH_reinforcement }}</td>
                                                    <td class="text-center">${{ $price->TER_treatment }}</td>
                                                    <td class="text-center">{{ $price->RAT_amount_traps }}</td>
                                                    <td class="text-center">${{ $price->RAT_bait_stations }}</td>
                                                    <td class="text-center">${{ $price->RAT_glue_traps }}</td>
                                                    <td class="text-center">
                                                        @include('vendor.adminlte.catalogs._modalPricesEdit')
                                                        <a data-toggle="modal" data-target="#ModalEditPrices{{ $a }}" class="btn" style="margin: none; padding: 0px;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                   <!-- /.box-body -->
               </div>
               <!-- /.box -->
           </div>
       </div>
   </div>
@endsection





