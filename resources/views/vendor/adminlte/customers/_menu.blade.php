<ul class="dropdown-menu">
    <li>
        <a data-toggle="modal" data-target="#editCustomerMain" class="openModalEditCustomerMain" style="cursor:pointer"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($a) }}"
           data-customer-main-name="{{ $customer->name }}"
           data-customer-main-establishment-name="{{ $customer->establishment_name }}"
           data-customer-main-cellphone="{{ $customer->cellphone }}"
           data-customer-main-cellphone-main=" {{ $customer->cellphone_main }}"
           data-customer-main-email="{{ $customer->email }}"
           data-customer-main-address="{{ $customer->address }}"
           data-customer-main-address-number="{{ $customer->addressNumber }}"
           data-customer-main-colony="{{ $customer->colony }}"
           data-customer-main-municipality="{{ $customer->municipality }}"
           data-customer-main-state="{{ $customer->state }}"
           data-customer-main-billing="{{ $customer->billing }}"
           data-customer-main-rfc="{{ $customer->rfc }}"
           data-customer-main-tax-residence="{{ $customer->tax_residence }}"
           data-customer-main-contact-name="{{ $customer->contact_two_name }}"
           data-customer-main-contact-phone="{{ $customer->contact_two_phone }}"
           data-customer-main-show-price="{{ $customer->show_price }}"
           data-customer-main-days-expiration="{{ $customer->days_expiration_certificate}}"
           data-customer-main-user-id="{{ $customer->user_id}}"
           data-customer-main-rfc-country="{{ $profileJobCenter->rfc_country }}"
           data-customer-main-email-billing="{{ $customer->email_billing }}"
           data-customer-main-cfdi-type="{{ $customer->cfdi_type }}"
           data-customer-main-payment-method="{{ $customer->payment_method }}"
           data-customer-main-payment-way="{{ $customer->payment_way }}"
           data-customer-main-billing-mode="{{ $customer->billing_mode }}"
           data-customer-main-service_code_billing="{{ $customer->service_code_billing }}"
           data-customer-main-service_code_billing-name="{{ $customer->service_code_billing_name }}"
           data-customer-main-unit_code_billing="{{ $customer->unit_code_billing }}"
           data-customer-main-unit_code_billing_name="{{ $customer->unit_code_billing_name }}"
           data-customer-main-description_billing="{{ $customer->description_billing }}"
           data-customer-main-amount_billing="{{ $customer->amount_billing }}"
           data-customer-main-date_billing="{{ $customer->date_billing }}"
           data-customer-main-billing_street="{{ $customer->billing_street }}"
           data-customer-main-billing_interior_number="{{ $customer->billing_interior_number }}"
           data-customer-main-billing_exterior_number="{{ $customer->billing_exterior_number }}"
           data-customer-main-billing_postal_code="{{ $customer->billing_postal_code }}"
           data-customer-main-billing_colony="{{ $customer->billing_colony }}"
           data-customer-main-billing_municipality="{{ $customer->billing_municipality }}"
           data-customer-main-billing_state="{{ $customer->billing_state }}"
           data-customer-main-is-main-check="{{ $customer->is_main }}"
           data-customer-main-select-id="{{ $customer->customer_main_id }}"
           data-customer-main-regime-fiscal="{{ $customer->fiscal_regime }}"
        >
            <i class="fa fa-user" aria-hidden="true" style="color: dodgerblue;"></i>Editar Cliente</a>
    </li>
    <li>
        <a data-toggle="modal" data-target="#dateYearCalendar"  target="_blank" style="cursor: pointer"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($a) }}">
            <i class="fa fa-calendar text-success" aria-hidden="true"></i>Calendario
        </a>
    </li>
    @if($customer->user_id !== null)
    <li @if(Auth::user()->id_plan == 1 || Auth::user()->id_plan == 2) data-toggle="tooltip" data-placement="right" title="Plan Empresarial" @endif>
        <a @if(Auth::user()->id_plan == 3) data-toggle="modal" data-target="#documentationCustomer" class="openModalDocumentationCustomer" @endif style="cursor:pointer"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($a) }}"
           data-customer-main-name="{{ $customer->name }}">
            <i class="fa fa-folder-open text-orange" aria-hidden="true"></i>Documentación</a>
    </li>
    @endif
    <li>
        <a href="{{ Route('index_historical_customer', \Vinkla\Hashids\Facades\Hashids::encode($a)) }}">
            <i class="fa fa-line-chart text-danger" aria-hidden="true"></i>Histórico
        </a>
    </li>
    @if(Entrust::can('Eliminar Datos')  || Entrust::hasRole('Cuenta Maestra'))
        <li data-toggle="tooltip" data-placement="bottom" title="">
            <a style="cursor: pointer" data-toggle="modal" data-target="#deleteClient"
               data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($a) }}"
               data-client="{{ $customer->name }}"
               data-quotationscount="{{ $customer->quotationsCount }}"
               data-customerbranchcount="{{ $customer->customerBranchCount }}"
               data-servicesordercount="{{ $customer->servicesOrderCount }}">
                <i class="fa fa-trash-o" aria-hidden="true" style="color: red;"></i>
                Eliminar
            </a>
        </li>
    @endif
    <li data-toggle="tooltip">
        <a data-toggle="modal" data-target="#serviceOrderByBranch" style="cursor:pointer"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($a) }}">
            <i class="fa fa-list text-" aria-hidden="true"></i>Sucursales/Servicios</a>
    </li>
    <li data-toggle="tooltip">
        <a data-toggle="modal" data-target="#modalMailAccounts" style="cursor:pointer"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($a) }}"
           data-customer="{{ $customer->name }}">
            <i class="fa fa-envelope-o text-yellow" aria-hidden="true"></i>Envío de Correos</a>
    </li>
</ul>