<!-- START MODAL DE ELIMINAR CUENTA CORREO -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="deleteModalMailAccount" role="dialog" aria-labelledby="deleteModalMailAccount" tabindex="-1">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-dialog" role="document" >
                    <div class="content modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="row container-fluid">
                                    <div class="col-md-12 text-center">
                                        <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">¿Estás seguro de eliminar la cuenta de correo?
                                            <br>
                                            <b class="text-danger" id="nameMailAccount"></b>
                                        </h4>
                                        <p class="text-bold">
                                            * Al borrar la cuenta, se dejará de enviar el documento asociado al correo de forma automática.<br>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="row">
                                        <button class="btn btn-primary" type="button" id="deleteMailAccount">Eliminar Cuenta</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE ELIMINAR CUENTA CORREO -->
