<!-- INICIA MODAL PARA EDITAR SUCURSALES DE CLIENTES -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="editMailAccount" role="dialog" aria-labelledby="editMailAccount">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" >

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info">Editar Cuentas de <span class="text-bold" id="modalEditTitleMailAccount"></span></h4>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <div class="col-md-12">
                                            <label for="documentUpd">Documento*:</label>
                                            <select class="form-control" id="documentUpd">
                                                <option value="Certificado de Servicio (pdf)">Certificado de Servicio (pdf)</option>
                                                <option value="Orden de Servicio (pdf)">Orden de Servicio (pdf)</option>
                                                <option value="Inspección de Estaciones (pdf)">Inspección de Estaciones (pdf)</option>
                                                <option value="Inspección de Áreas (pdf)">Inspección de Áreas (pdf)</option>
                                                <option value="Factura electrónica (pdf y xml)">Factura electrónica (pdf y xml)</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="namePeopleUpd">Nombre de la persona:</label>
                                            <input type="text" id="namePeopleUpd" class="form-control" placeholder="Ej: Rafael Nuñez">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="jobUpd">Puesto:</label>
                                            <input type="text" id="jobUpd" class="form-control" placeholder="Ej: Supervisor">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="emailAccountUpd">Correo electrónico*:</label>
                                            <input type="text" id="emailAccountUpd" class="form-control" placeholder="Ej: contacto@pestwareapp.com">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-primary" type="button" id="btnUpdMailAccount">Actualizar Cuenta</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA EDITAR SUCURSALES DE CLIENTES -->
