<!-- START MODAL MAIL ACCOUNTS -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalMailAccounts" role="dialog" aria-labelledby="modalMailAccounts">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document" style="width: 90% !important;">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Configuración de Envío de Correos <span class="text-bold" id="labelTitleCustomerModalMails"></span></h4>
                            </div>
                            <br>
                            <div class="box-body">
                                <div class="col-md-12">
                                    <span class="text-bold">*La siguiente configuración de correo aplica para el envío automático de documentos una vez finalizado el servicio.</span>
                                </div>
                                <div class="col-md-12 container-fluid">
                                    <div class="col-md-3">
                                        <label for="document">Documento*:</label>
                                        <select class="form-control" id="document">
                                            <option value="0" selected>Seleccione un documento</option>
                                            <option value="Certificado de Servicio (pdf)">Certificado de Servicio (pdf)</option>
                                            <option value="Orden de Servicio (pdf)">Orden de Servicio (pdf)</option>
                                            <option value="Inspección de Estaciones (pdf)">Inspección de Estaciones (pdf)</option>
                                            <option value="Inspección de Áreas (pdf)">Inspección de Áreas (pdf)</option>
                                            <option value="Factura electrónica (pdf y xml)">Factura electrónica (pdf y xml)</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="namePeople">Nombre de la persona:</label>
                                        <input type="text" id="namePeople" class="form-control" placeholder="Ej: Rafael Nuñez">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="job">Puesto:</label>
                                        <input type="text" id="job" class="form-control" placeholder="Ej: Supervisor">
                                    </div>
                                    <div class="col-md-3">
                                        <label for="emailAccount">Correo electrónico*:</label>
                                        <input type="text" id="emailAccount" class="form-control" placeholder="Ej: contacto@pestwareapp.com">
                                    </div>
                                    <div class="col-md-1">
                                        <label for="job">Acciones:</label>
                                        <button class="btn btn-secondary btn-block" id="btnAddNewMailAccount">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                        <br>
                                    </div>

                                    <table class="table table-hover" id="tableMailAccounts">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Documento</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Puesto</th>
                                            <th class="text-center">Correo Electrónico</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tableBodyMailAccounts">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="text-center">
                                <hr>
                                <button class="btn btn-primary btn-md" id="saveConfigMailAccounts">Guardar Configuración</button>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL EDIT SERVICES CUSTOMER MAIL ACCOUNTS -->