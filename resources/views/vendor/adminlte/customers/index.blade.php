@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mis Clientes</h3>

                        <ol class="breadcrumb" style="margin-top: 5px;">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Mis Clientes</li>
                        </ol>

                            <!--<nav aria-label="...">
                            <ul class="pager">
                                <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Regresar</a></li>
                                <li class="next"><a href="#">Clientes <span aria-hidden="true">&rarr;</span></a></li>
                            </ul>
                        </nav>-->

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <button type="button" title="Nuevo Cliente" class="btn btn-primary" data-toggle="modal" data-target="#createCustomerMain">
                            <i class="fa fa-plus" aria-hidden="true"></i> Nuevo Cliente
                        </button>
                    </div>

                    <div class="col-md-4 margin-mobile">
                        <select name="selectJobCenterCustomer" id="selectJobCenterCustomer" class="form-control" style="font-weight: bold;">
                            @foreach($jobCenters as $jobCenter)
                                @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                    <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                        {{ $jobCenter->name }}
                                    </option>
                                @else
                                    <option value="{{ $jobCenter->id }}">
                                        {{ $jobCenter->name }}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-2 margin-mobile">
                        <select id="filterPaginate" name="filterPaginate" class="form-control">
                            <option value="10" selected>10 por página</option>
                            <option value="30">30 por página</option>
                            <option value="50">50 por página</option>
                            <option value="70">70 por página</option>
                            <option value="100">100 por página</option>
                        </select>
                    </div>

                    <div class="col-md-4 margin-mobile">

                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Buscar por: (Nombre)" name="search" id="search" value="">
                            <span class="input-group-btn">
                                <button type="button" id="btnFilter" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>

                    </div>

                    <div class="box-body">
                        <br><br>
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Cliente</th>
                                        <th>Cliente/Empresa</th>
                                        <th>Contacto</th>
                                        <th>Sucursal</th>
                                        <th># C</th>
                                        <th># OS</th>
                                        <th @if(Auth::user()->id_plan == 1 || Auth::user()->id_plan == 2) data-toggle="tooltip" data-placement="bottom" title="Plan Empresarial" @endif>
                                            <a data-toggle="tooltip" data-placement="top" title="Portal Clientes" href="https://portal.pestwareapp.com" target="_blank">Portal</a></th>
                                        <th>Creación</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($a = 0)
                                    @foreach($customers as $customer)
                                        @php($a = $customer->id)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                                      CL-{{ $customer->id }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.customers._menu')
                                                </div>
                                            </td>
                                            <td> <span class="text-primary">
                                                    {{ $customer->name }}
                                                </span>
                                            </td>
                                            <td>{{ $customer->establishment_name }}</td>
                                            <td>
                                                @if($customer->is_main == 0)
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>
                                                @else
                                                    <i class="fa fa-times text-danger" aria-hidden="true"></i>
                                                @endif
                                            </td>
                                            <td>{{ $customer->quotationsCount }}</td>
                                            <td>{{ $customer->servicesOrderCount }}</td>
                                            <td @if(Auth::user()->id_plan == 1 || Auth::user()->id_plan == 2) style="pointer-events: none; opacity: 0.5;background: #CCC;" @endif>
                                                @if($customer->user_id !== null)
                                                    <i class="fa fa-eye fa-2x text-info" aria-hidden="true"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="Usuario: {{ $customer->cellphone }}"
                                                       style="margin-right: 5px; cursor: pointer"
                                                       id="buttonShow-{{$customer->id}}">
                                                    </i> <span id="showCellphone-{{$customer->id}}" style="display: none; font-size: 10pt">{{ $customer->cellphone }}</span>
                                                    <a class="openModalChangePassword" data-toggle="modal" data-target="#changePassword" style="cursor: pointer" data-id="{{ $customer->id }}">
                                                        <i class="fa fa-lock fa-2x" aria-hidden="true"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="Resetear contraseña">
                                                        </i>
                                                    </a>
                                                @else
                                                    <a class="openModalCustomerUser" data-toggle="modal" data-target="#customerUser" style="cursor: pointer"
                                                       data-id="{{ $customer->id }}"
                                                       data-cellphone="{{ $customer->cellphone }}">
                                                        <i class="fa fa-user-plus fa-2x text-info" aria-hidden="true"
                                                            data-toggle="tooltip"
                                                            data-placement="top"
                                                            title="Crear cuenta">
                                                        </i>
                                                    </a>
                                                @endif
                                            </td>
                                            <td>{{ $customer->created_at->format('d-m-Y') }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $customers->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

        </div>
    </div>
@include('vendor.adminlte.customers._modalChangePassword')
@include('vendor.adminlte.customers._modalCustomerUser')
@include('vendor.adminlte.customers._modalEditCustomerMain')
@include('vendor.adminlte.customers._modalCreateCustomerMain')
@include('vendor.adminlte.customers._modalDocumentation')
@include('vendor.adminlte.customers._modalServiceOrderByBranch')
@include('vendor.adminlte.customers._modalEditServicesBranch')
@include('vendor.adminlte.customers._modalInvoiceByAmount')
@include('vendor.adminlte.customers._modalCreateBankAccount')
@include('vendor.adminlte.customers._modalEditCustomerBranch')
@include('vendor.adminlte.customers._deleteClient')
@include('vendor.adminlte.customers._deleteBranchClient')
@include('vendor.adminlte.customers.mailaccount._modalMailAccounts')
@include('vendor.adminlte.customers.mailaccount._modalEditMailAccount')
@include('vendor.adminlte.customers.mailaccount._modalDeleteMailAccount')
@include('vendor.adminlte.customers._modalYearCalendar')

@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/customer.js') }}"></script>
    <script>
        $(".applicantsList-single").select2();
        $(document).ready(function() {
            $('#btnFilter').click(function() {
                let filterPaginate = $('#filterPaginate option:selected').val();
                let search = $('#search').val();
                window.location.href = `/customers/view?forPage=${filterPaginate}&search=${search}`;
            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection