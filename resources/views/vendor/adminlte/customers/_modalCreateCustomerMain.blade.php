<!-- INICIA MODAL PARA CREAR CLIENTES -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="createCustomerMain" role="dialog" aria-labelledby="createCustomerMain">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-body" role="document">

                    <div class="modal-dialog modal-lg" style="width: 90%;">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Nuevo Cliente</h4>
                            </div>

                            <div class="box-body">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="customer"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">Cliente</a></li>
                                    <li role="certificate"><a href="#certificate" aria-controls="certificate" role="tab" data-toggle="tab">Datos del Certificado</a></li>
                                    <li role="certificate"><a href="#billing" aria-controls="billing" role="tab" data-toggle="tab">Facturación</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <br>
                                    <div role="tabpanel" class="tab-pane active" id="customer">
                                        <div class="row container-fluid">
                                            <div class="col-md-12 text-left">
                                                    <div class="col-md-6">
                                                        <h5 class="text-info">Datos Cliente</h5>
                                                        <label class="checkbox-inline"><input type="checkbox" name="isMainCheck" id="isMainCheck">¿Es sucursal?</label><br>
                                                        <div style="display: none !important;" id="divCustomerMain">
                                                            <select id="customerMainSelect" name="customerMainSelect" class="applicantsList-single form-control header-table" style="width: 100%;">
                                                                <option value="null" disabled selected>Selecciona el cliente principal</option>
                                                                @foreach($customersMain as $customer)
                                                                    <option value="{{ $customer->id }}">
                                                                        {{ $customer->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <label for="nameCustomerMainNew">Cliente/Empresa*:</label>
                                                        <input type="text" name="nameCustomerMainNew" id="nameCustomerMainNew" class="form-control">
                                                        <label for="phoneCustomerBranchNew">Teléfono:</label>
                                                        <input type="text" name="phoneCustomerBranchNew" id="phoneCustomerBranchNew" class="form-control">
                                                        <label for="contactCustomerMainNew">Contacto Principal:</label>
                                                        <input type="text" name="contactCustomerMainNew" id="contactCustomerMainNew" class="form-control">
                                                        <label for="phoneMainCustomerBranchNew">Teléfono Principal:</label>
                                                        <input type="text" name="phoneMainCustomerBranchNew" id="phoneMainCustomerBranchNew" class="form-control">
                                                        <label for="emailCustomerMainNew">Email:</label>
                                                        <input type="text" name="emailCustomerMainNew" id="emailCustomerMainNew" class="form-control">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <h5 class="text-info">Domicilio</h5>
                                                        <label for="addressCustomerMainNew">
                                                            @if($labels['street'] != null) {{ $labels['street']->label_spanish }}: @else Calle: @endif
                                                        </label>
                                                        <input type="text" name="addressCustomerMainNew" id="addressCustomerMainNew" class="form-control">
                                                        <label for="addressNumberCustomerMainNew">Número:</label>
                                                        <input type="text" name="addressNumberCustomerMainNew" id="addressNumberCustomerMainNew" class="form-control">
                                                        <label for="colonyCustomerMainNew">
                                                            @if($labels['colony'] != null) {{ $labels['colony']->label_spanish }}: @else Colonia: @endif
                                                        </label>
                                                        <input type="text" name="colonyCustomerMainNew" id="colonyCustomerMainNew" class="form-control">
                                                        <label for="municipalityCustomerMainNew">Municipio:</label>
                                                        <input type="text" name="municipalityCustomerMainNew" id="municipalityCustomerMainNew" class="form-control">
                                                        <label for="stateCustomerMainNew">
                                                            @if($labels['state'] != null) {{ $labels['state']->label_spanish }}: @else Estado: @endif
                                                        </label>
                                                        <input type="text" name="stateCustomerMainNew" id="stateCustomerMainNew" class="form-control">
                                                    </div>

                                                    <br>
                                                    <div class="formulario__mensaje" id="formulario__mensaje_tracing">
                                                        <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje_tracing"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                                        <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="certificate">
                                        <div class="row container-fluid">
                                            <div class="col-md-12 text-left">
                                                <div class="col-ms-3">
                                                    <label for="establishmentNew">Tipo de Servicio*:</label>
                                                    <select name="establishmentNew" id="establishmentNew" class="form-control" style="width: 100%;">
                                                        @foreach($establishments as $establishment)
                                                            <option value="{{ $establishment->id }}">{{ $establishment->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="sourceOriginNew">Fuente de Origen*:</label>
                                                    <select name="sourceOriginNew" id="sourceOriginNew" class="form-control" style="width: 100%;">
                                                        @foreach($sources as $source)
                                                            <option value="{{ $source->id }}">{{ $source->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="stateCustomerMainNew">Días de Expiración (Certificado)</label>
                                                    <input type="number" class="form-control" name="daysExpirationCertificateCustomerNew" id="daysExpirationCertificateCustomerNew" placeholder="30" value="0">
                                                    <span>Mostrar Precio (Certificado)</span>
                                                    <input type="checkbox" class="form-check-input" name="showPriceOrderServiceNew" id="showPriceOrderServiceNew" checked>
                                                    <br>
                                                    <span>Cuenta Portal</span>
                                                    <input type="checkbox" class="form-check-input" name="portalCustomerMainNew" id="portalCustomerMainNew">
                                                </div>
                                                <br>
                                                <div class="formulario__mensaje" id="formulario__mensaje_tracing">
                                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje_tracing"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="billing">
                                        <div class="row container-fluid">
                                            <div class="col-md-12 text-left">

                                                <div class="col-md-6">
                                                    <div class="col-md-12">
                                                        <label for="billingCustomerMainNew">Razón Social:</label>
                                                        <input type="text" name="billingCustomerMainNew" id="billingCustomerMainNew" class="form-control">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="rfcCustomerMainNew">{{ $profileJobCenter->rfc_country }}:</label>
                                                        <input type="text" name="rfcCustomerMainNew" id="rfcCustomerMainNew" class="form-control">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="addressBilling">Calle:</label>
                                                        <input type="text" id="addressBilling" class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label for="exteriorNumberBilling">Número Exterior:</label>
                                                        <input type="text" id="exteriorNumberBilling" class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label for="interiorNumberBilling">Número Interior:</label>
                                                        <input type="text" id="interiorNumberBilling" class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label for="postalCodeBilling">Código Postal:</label>
                                                        <input type="text" id="postalCodeBilling" class="form-control">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="colonyBilling">Colonia:</label>
                                                        <input type="text" id="colonyBilling" class="form-control">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="municipalityBilling">Municipio:</label>
                                                        <input type="text" id="municipalityBilling" class="form-control">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="stateBilling">Estado:</label>
                                                        <input type="text" id="stateBilling" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6" @if($company->id_code_country != 2) style="display: none;" @endif>
                                                    <label for="contactTwoCustomerMainNew">Contacto (Nombre)2:</label>
                                                    <input type="text" name="contactTwoCustomerMainNew" id="contactTwoCustomerMainNew" class="form-control">
                                                    <label for="contactTwoPhoneCustomerMainNew">Contacto Teléfono 2:</label>
                                                    <input type="text" name="contactTwoPhoneCustomerMainNew" id="contactTwoPhoneCustomerMainNew" class="form-control">
                                                    <label for="emailBillingCustomerMainNew">Email:</label>
                                                    <input type="text" name="emailBillingCustomerMainNew" id="emailBillingCustomerMainNew" class="form-control">
                                                    <label for="typeCfdiCustomerMainNew">Tipo CFDI:</label>
                                                    <select name="typeCfdiCustomerMainNew" id="typeCfdiCustomerMainNew" class="form-control">

                                                    </select>

                                                    <label for="regimeFiscalCustomerMainNew">Régimen Fiscal*:</label>
                                                    <select name="regimeFiscalCustomerMainNew" id="regimeFiscalCustomerMainNew" class="form-control">

                                                    <label for="methodPayCustomerMainNew">Método de Pago:</label>
                                                    <select name="methodPayCustomerMainNew" id="methodPayCustomerMainNew" class="form-control">

                                                    </select>
                                                    <label for="wayPayCustomerMainNew">Forma de Pago:</label>
                                                    <select name="wayPayCustomerMainNew" id="wayPayCustomerMainNew" class="form-control">

                                                    </select>
                                                    <label for="wayPayCustomerMainNew">Modo de Facturación:</label>
                                                    <select name="modeBilling" id="modeBilling" class="form-control">
                                                        @foreach($modesBilling as $mode)
                                                            <option value="{{ $mode->id }}">{{ $mode->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="text-danger text-bold" id="textConditionsCreate" style="cursor: pointer; display: none;"><i class="fa fa-info-circle" aria-hidden="true"></i> Avisos y Condiciones</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-12 form-group">
                                                        <label class="control-label" for="tipo">Cuenta Bancaria:</label>
                                                        <div class="input-group">
                                                            <select id="bankAccounts" class="form-control">
                                                            </select>
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-block btn-secondary">
                                                                <i class="fa fa fa-plus" aria-hidden="true"></i>
                                                            </button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="formulario__mensaje" id="formulario__mensaje_tracing">
                                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje_tracing"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row container-fluid" id="divInvoiceByAmount" style="display: none;">
                                            <div class="col-md-12 text-left">
                                                <div class="col-md-6">
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <label class="control-label" for="tipo">Asocia tu servicio al cátalogo del SAT:</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="productCodeSat" id="productCodeSat" value="70141605 - Servicios de manejo integrado de plagas">
                                                                <input type="hidden" name="productCodeSatId" id="productCodeSatId" value="70141605">
                                                                <div id="productCodeSatList"></div>
                                                                <span class="input-group-btn">
                                                                    <a href="http://200.57.3.89/PyS/catPyS.aspx" target="_blank" class="btn btn-block btn-secondary">
                                                                        <i class="fa fa fa-search" aria-hidden="true"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <label class="control-label" for="tipo">Define la unidad de tu servicio</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="productUnitSat" id="productUnitSat" value="E48 - Unidad de Servicio">
                                                                <input type="hidden" name="productUnitSatId" id="productUnitSatId" value="3">
                                                                <div id="productUnitSatList"></div>
                                                                <span class="input-group-btn">
                                                                    <a href="http://200.57.3.89/PyS/catUnidades.aspx" target="_blank" class="btn btn-block btn-secondary">
                                                                        <i class="fa fa fa-search" aria-hidden="true"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group" id="divAmountInvoice">
                                                        <div class="col-sm-12">
                                                            <label class="control-label" for="tipo">Monto Establecido (sin IVA)</label>
                                                            <input type="number" class="form-control" name="amountInvoice" id="amountInvoice" placeholder="Ej. 1400">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="row form-group">
                                                        <div class="col-sm-12">
                                                            <label class="control-label" for="tipo">Descripción Facturación (concepto)</label>
                                                            <input type="text" class="form-control" name="descriptionBilling" id="descriptionBilling" placeholder="Servicio de Control de Plagas...">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group" id="divQuantityInvoice">
                                                        <div class="col-sm-12">
                                                            <label class="control-label" for="tipo">Número de Facturas</label>
                                                            <input type="number" class="form-control" name="quantityInvoice" id="quantityInvoice" placeholder="Ej. 12" min="1">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group" id="divDateInvoice">
                                                        <div class="col-sm-12">
                                                            <label class="control-label" for="tipo">Fecha de facturación</label>
                                                            <input type="date" class="form-control" name="dateInvoice" id="dateInvoice" value="{{ \Carbon\Carbon::now()->toDateString() }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-primary" type="button" id="sendCreateCustomerMainSave" name="sendCreateCustomerMainSave">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA CREAR CLIENTES -->
