<!-- START MODAL FOR DOCUMENTATION CUSTOMER -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="documentationCustomer" role="dialog" aria-labelledby="documentationCustomer" style="overflow-y: scroll;">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document" >

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="text-secondary text-center">Documentación Carpeta MIP</h4>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Cliente: <span id="customerNameTitleModal"></span></h4>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <div class="col-md-6">
                                            <form action="{{ Route('customer_documents_post') }}" method="POST" id="formNewDocument" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input type="hidden" id="id_customer" name="id_customer">
                                                <label for="section_name">Nombre Documento*:</label>
                                                <input type="text" name="section_name" id="section_name" class="form-control" required>
                                                <label for="subtitle">Subtítulo (Separador):</label>
                                                <input type="text" name="subtitle" id="subtitle" class="form-control">
                                                <label for="description">Descripción (Separador):</label>
                                                <input type="text" name="description" id="description" class="form-control">
                                                <label for="documentFile">Documento (PDF)*:</label>
                                                <input type="file" name="documentFile" id="documentFile" class="form-control" required accept="application/pdf">
                                                <label for="position">Posición*:</label>
                                                <select name="position" id="position" required class="form-control"></select>
                                                <br>
                                                <button type="submit" class="btn btn-primary btn-block">Agregar Documento a Carpeta MIP</button>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <ul class="media-list">
                                                <li class="media">
                                                    <div class="media-left">
                                                        <a href="#">
                                                            <i class="fa fa-folder-open text-orange fa-2x"></i>
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">Carpeta MIP 2021</h4>
                                                        <div id="documentsForMIP"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="col-md-12" id="tableMipCertificates">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Fecha</th>
                                                    <th scope="col">Archivos</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <input class="form-control" type="text" name="filterDateMipCertificate" id="filterDateMipCertificate">
                                                    </td>
                                                    <td class="text-center">
                                                        <button id="buttonMipCertificate" name="buttonMipCertificate" type="button" class="btn btn-primary">
                                                            Certificados
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-control" type="text" name="filterDateMipOrder" id="filterDateMipOrder">
                                                    </td>
                                                    <td class="text-center">
                                                        <button id="buttonMipOrder" name="buttonMipOrder" type="button" class="btn btn-primary">
                                                            Ordenes
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <br><br><br><br><br><br><br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL FOR DOCUMENTATION CUSTOMER -->
