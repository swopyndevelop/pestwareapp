<!-- INICIA MODAL PARA CAMBIAR CONTRASEÑA -->
<div class="modal fade" id="dateYearCalendar" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body text-center">
                <h4>Seleccionar Año</h4>
                <select id="selectYear" class="form-control">
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                </select>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <a target="_blank">
                        <button class="btn btn-primary" type="button" id="btnCalendarCustomer">Descargar</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA CAMBIAR CONTRASEÑA -->