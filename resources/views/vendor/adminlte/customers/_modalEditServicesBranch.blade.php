<!-- START MODAL EDIT SERVICES CUSTOMER BRANCH -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalEditServicesByBranch" role="dialog" aria-labelledby="modalEditServicesByBranch">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document" style="width: 90% !important;">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <br>
                            <div class="box-body">
                                <input type="hidden" class="form-control" id="allIdsPayment" name="allIdsPayment">
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Múltiple Servicios de: <span id="labelTitleBranch"></span></h4>
                                <h2 class="modal-title text-center col-lg-12 text-success" id="labelTotalCash"></h2>
                                <div class="col-md-12 container-fluid">
                                    <br>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr class="bg-default">
                                            <th class="text-center">Edición Masiva:</th>
                                            <th class="text-center"></th>
                                            <th class="text-center"></th>
                                            <th class="text-center"><input class="form-control" type="time" name="timeInitial" id="timeInitial"></th>
                                            <th class="text-center"><input class="form-control" type="time" name="timeFinal" id="timeFinal"></th>
                                            <th class="text-center"><select class="form-control" name="techniciansAll" id="techniciansAll"></select></th>
                                            @if(Entrust::can('Eliminar Datos')  || Entrust::hasRole('Cuenta Maestra'))
                                                <th class="text-center"><input class="form-check-input" type="checkbox" name="deleteOrderAll" id="deleteOrderAll"></th>
                                            @endif
                                        </tr>
                                        <tr class="bg-primary">
                                            <th class="text-center"># Orden Servicio</th>
                                            <th class="text-center">Fecha Inicial</th>
                                            <th class="text-center">Fecha Final</th>
                                            <th class="text-center">Hora Inicial</th>
                                            <th class="text-center">Hora Final</th>
                                            <th class="text-center">Técnico</th>
                                            @if(Entrust::can('Eliminar Datos')  || Entrust::hasRole('Cuenta Maestra'))
                                                <th class="text-center">Eliminar Os</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <div id="container-table-services-branch">
                                            <tbody id="tableBodyDetailServicesBranch">

                                            </tbody>
                                        </div>
                                    </table>
                                </div>
                            </div>

                            <div class="text-center">

                                <div class="col-sm-12 formulario__mensaje" id="formulario__mensaje" style="margin: 10px; margin-right: 10px;">
                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                </div>
                                <br>
                                <div class="col-sm-12 text-center formulario__grupo formulario__grupo-btn-enviar">
                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se editaron los servicios correctamente.</p>
                                </div>

                                <button class="btn btn-primary btn-md" id="saveAllEditServicesBranch">Editar Servicios</button>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL EDIT SERVICES CUSTOMER BRANCH -->