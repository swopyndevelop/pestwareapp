<!-- START MODAL FOR ORDENES DE SERVICIO -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="serviceOrderByBranch" role="dialog" aria-labelledby="serviceOrderByBranch" style="overflow-y: scroll;">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document" >

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="row container-fluid">
                                    <h4 class="text-secondary text-center">Servicios</h4>
                                    <table class="table table-hover" id="tableServicesOrder">
                                        <thead>
                                        <tr class="bg-primary" style="font-size: x-small">
                                            <th># Cotización</th>
                                            <th>Servicio</th>
                                            <th>Responsable</th>
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                            <th>Dirección</th>
                                            <th>Opciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                    <h4 class="text-secondary text-center">Sucursales</h4>
                                    <table class="table table-hover" id="tableServicesBranches">
                                        <thead>
                                        <tr class="bg-primary" style="font-size: x-small">
                                            <th># Sucursal</th>
                                            <th>Nombre</th>
                                            <th>Responsable</th>
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                            <th>Dirección</th>
                                            <th>Opciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                               </div>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
<!--                                    <button class="btn btn-primary" type="button" id="saveCustomerByBranch">Guardar</button>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL FOR ORDENES DE SERVICIO -->
