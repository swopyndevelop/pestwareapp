<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Título </title>
</head>
<style>
    body{
        font-family:'sans-serif';
    }
    @page  {
        margin-right: 0;
        margin-bottom: 0;
        margin-top: 0;
    }
    .column {
        float: left;
        width: 50%;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
</style>
<body>
<div class="row">
    <div class="column">
        <br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br>
        <img src="{{$pdf_logo}}" alt="" width="97%">
    </div>
    <div class="column" align="center" style="background-color: {{ $company->cover_mip_color }}">
        <div style="height: 1080px">
            <br><br><br><br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br><br><br><br>
            <br><br><br><br><br>
            <h1 style="font-size: 20pt; font-weight:bold; color:white">{{ $data[0] }}</h1>
            <h1 style="font-size: 10pt; font-weight:bold; color:white">{{ $customer->name }}</h1>
        </div>
    </div>
</div>
</body>
</html>