<!-- START MODAL DE ELIMINAR CLIENTE -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="deleteClient" role="dialog" aria-labelledby="deleteClient" tabindex="-1">
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-dialog" role="document" >
          <div class="content modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>

              <div class="box-body">
                <div class="row container-fluid">
                    <div class="col-md-12 text-center">
                      <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">¿Estás seguro de eliminar el Cliente?
                        <br>
                        <b class="text-danger" id="nameClient"></b>
                      </h4>
                      <p class="text-info">
                          * Al borrar el cliente se eliminará lo siguiente: <br>
                          * Cotizaciones: <b class="text-danger" id="quotationsCount"></b> <br>
                          * Sucursales: <b class="text-danger" id="customerBranchCount"></b> <br>
                          * Ordenes de Servico: <b class="text-danger" id="servicesOrderCount"></b> <br>
                          * Monitoreo de Estaciones. <br>
                          * Monitoreo de Áreas. <br>
                          * Cuenta Portal.
                      </p>
                    </div>
                </div>
              </div>

              <div class="modal-footer">
                <div class="text-center">
                  <div class="row">
                      <button class="btn btn-primary" type="button" id="saveDeleteCustomer">Eliminar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- END MODAL DE ELIMINAR CLIENTE -->
