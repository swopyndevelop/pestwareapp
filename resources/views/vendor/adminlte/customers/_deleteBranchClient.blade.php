<!-- START MODAL DE ELIMINAR SUCURSAL -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="deleteCustomerBranch" role="dialog" aria-labelledby="deleteCustomerBranch" tabindex="-1">
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-dialog" role="document" >
          <div class="content modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>

              <div class="box-body">
                <div class="row container-fluid">
                    <div class="col-md-12 text-center">
                      <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">¿Estás seguro de eliminar la Sucursal?
                        <br>
                        <b class="text-danger" id="nameClientBranch"></b>
                      </h4>
                      <p class="text-info">
                          * Al borrar la sucursal se eliminará lo siguiente: <br>
                          * Ordenes de Servico <b class="text-danger"></b> <br>
                          * Monitoreo de Estaciones. <br>
                          * Monitoreo de Áreas. <br>
                      </p>
                    </div>
                </div>
              </div>

              <div class="modal-footer">
                <div class="text-center">
                  <div class="row">
                      <button class="btn btn-primary" type="button" id="saveDeleteBranchCustomer">Eliminar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- END MODAL DE ELIMINAR SUCURSAL -->
