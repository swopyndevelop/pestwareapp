<!-- START MODAL FOR DOCUMENTATION CUSTOMER -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalInvoiceForAmount" role="dialog" aria-labelledby="modalInvoiceForAmount" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document" >

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="text-secondary text-center text-primary">Factura por Monto Establecido</h4>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-sm-8">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Asocia tu producto al cátalogo del SAT:</label>
                                                    <input type="text" class="form-control" name="productCodeSat" id="productCodeSat" value="70141605 - Servicios de manejo integrado de plagas">
                                                    <input type="hidden" name="productCodeSatId" id="productCodeSatId">
                                                    <div id="productCodeSatList"></div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                    <a href="http://200.57.3.89/PyS/catPyS.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Clave</a>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-8">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Define la unidad de tu servicio</label>
                                                    <input type="text" class="form-control" name="productUnitSat" id="productUnitSat" value="E48 - Unidad de Servicio">
                                                    <input type="hidden" name="productUnitSatId" id="productUnitSatId">
                                                    <div id="productUnitSatList"></div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Buscador SAT</label>
                                                    <a href="http://200.57.3.89/PyS/catUnidades.aspx" target="_blank" class="btn btn-block btn-secondary">Búscar Unidad</a>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Monto Establecido (por cada factura)</label>
                                                    <input type="number" class="form-control" name="amount" id="amount" placeholder="Ej. 1400">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Descripción Facturación (concepto)</label>
                                                    <input type="text" class="form-control" name="description" id="description" placeholder="Servicio de Control de Plagas...">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Plazo de facturación (no. de facturas)</label>
                                                    <input type="number" class="form-control" name="quantity" id="quantity" placeholder="Ej. 12">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <label class="control-label" for="tipo" style="font-weight: bold;">Fecha de facturación</label>
                                                    <input type="date" class="form-control" name="dateInvoice" id="dateInvoice">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <button class="btn btn-primary">Programar Facturación</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL FOR DOCUMENTATION CUSTOMER -->
