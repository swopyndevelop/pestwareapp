<!-- INICIA MODAL PARA EDITAR CLIENTES -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="editCustomerMain" role="dialog" aria-labelledby="editCustomerMain">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-body" role="document" >

                    <div class="modal-dialog modal-lg" style="width: 90%;">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Cliente -> <span id="customerNameTitle"></span></h4>
                            </div>

                            <div class="box-body">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="customerEdit"><a href="#customerEdit" aria-controls="customer" role="tab" data-toggle="tab">Cliente</a></li>
                                    <li role="certificateEdit"><a href="#certificateEdit" aria-controls="certificate" role="tab" data-toggle="tab">Datos del Certificado</a></li>
                                    <li role="billingEdit"><a href="#billingEdit" aria-controls="billing" role="tab" data-toggle="tab">Facturación</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <br>
                                    <div role="tabpanel" class="tab-pane active" id="customerEdit">
                                        <div class="row container-fluid">
                                            <div class="col-md-12 text-left">
                                                <div class="col-md-6">
                                                    <h5 class="text-info">Datos Cliente</h5>
                                                    <label class="checkbox-inline"><input type="checkbox" name="isMainCheckEdit" id="isMainCheckEdit">¿Es sucursal?</label><br>
                                                    <div style="display: none !important;" id="divCustomerMainEdit">
                                                        <select id="customerMainSelectEdit" name="customerMainSelectEdit" class="applicantsList-single form-control header-table" style="width: 100%;">
                                                            <option value="null" disabled selected>Selecciona el cliente principal</option>
                                                            @foreach($customersMain as $customer)
                                                                <option value="{{ $customer->id }}">
                                                                    {{ $customer->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <label for="nameCustomerMain">Cliente/Empresa:</label>
                                                    <input type="text" name="nameCustomerMain" id="nameCustomerMain" class="form-control">
                                                    <label for="phoneCustomerBranch">Teléfono:</label>
                                                    <input type="text" name="phoneCustomerBranch" id="phoneCustomerBranch" class="form-control">
                                                    <label for="contactCustomerMain">Contacto Principal:</label>
                                                    <input type="text" name="contactCustomerMain" id="contactCustomerMain" class="form-control">
                                                    <label for="phoneMainCustomerBranch">Teléfono Principal:</label>
                                                    <input type="text" name="phoneMainCustomerBranch" id="phoneMainCustomerBranch" class="form-control">
                                                    <label for="emailCustomerMain">Email:</label>
                                                    <input type="text" name="emailCustomerMain" id="emailCustomerMain" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <h5 class="text-info">Domicilio</h5>
                                                    <label for="addressCustomerMain">
                                                        @if($labels['street'] != null) {{ $labels['street']->label_spanish }}: @else Calle: @endif
                                                    </label>
                                                    <input type="text" name="addressCustomerMain" id="addressCustomerMain" class="form-control">
                                                    <label for="addressNumberCustomerMain">Número:</label>
                                                    <input type="text" name="addressNumberCustomerMain" id="addressNumberCustomerMain" class="form-control">
                                                    <label for="colonyCustomerMain">
                                                        @if($labels['colony'] != null) {{ $labels['colony']->label_spanish }}: @else Colonia: @endif
                                                    </label>
                                                    <input type="text" name="colonyCustomerMain" id="colonyCustomerMain" class="form-control">
                                                    <label for="municipalityCustomerMain">Municipio:</label>
                                                    <input type="text" name="municipalityCustomerMain" id="municipalityCustomerMain" class="form-control">
                                                    <label for="stateCustomerMain">
                                                        @if($labels['state'] != null) {{ $labels['state']->label_spanish }}: @else Estado: @endif
                                                    </label>
                                                    <input type="text" name="stateCustomerMain" id="stateCustomerMain" class="form-control">
                                                </div>

                                                <br>
                                                <div class="formulario__mensaje" id="formulario__mensaje_tracing">
                                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje_tracing"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="certificateEdit">
                                        <div class="row container-fluid">
                                            <label>Días de Expiración (Certificado)</label>
                                            <input type="number" class="form-control" name="daysExpirationCertificateCustomer" id="daysExpirationCertificateCustomer" placeholder="30" value="0">
                                            <label>Mostrar Precio (Certificado)</label>
                                            <input type="checkbox" class="form-check-input" name="showPriceOrderService" id="showPriceOrderService">
                                            <br>
                                            <div id="showDivPortal">
                                                <span>Cuenta Portal</span>
                                                <input type="checkbox" class="form-check-input" name="portalCustomerMain" id="portalCustomerMain">
                                            </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="billingEdit">
                                        <div class="row container-fluid">
                                            <div class="col-md-6">
                                                <div class="col-md-12">
                                                    <label for="billingCustomerMain">Razón Social:</label>
                                                    <input type="text" name="billingCustomerMainNew" id="billingCustomerMain" class="form-control">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="rfcCustomerMain" id="rfcCustomerMainLabel">RFC:</label>
                                                    <input type="text" name="rfcCustomerMain" id="rfcCustomerMain" class="form-control">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="addressBillingUpd">Calle:</label>
                                                    <input type="text" id="addressBillingUpd" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="exteriorNumberBillingUpd">Número Exterior:</label>
                                                    <input type="text" id="exteriorNumberBillingUpd" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="interiorNumberBillingUpd">Número Interior:</label>
                                                    <input type="text" id="interiorNumberBillingUpd" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="postalCodeBillingUpd">Código Postal:</label>
                                                    <input type="text" id="postalCodeBillingUpd" class="form-control">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="colonyBillingUpd">Colonia:</label>
                                                    <input type="text" id="colonyBillingUpd" class="form-control">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="municipalityBillingUpd">Municipio:</label>
                                                    <input type="text" id="municipalityBillingUpd" class="form-control">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="stateBillingUpd">Estado:</label>
                                                    <input type="text" id="stateBillingUpd" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6" @if($company->id_code_country != 2) style="display: none;" @endif>
                                                <label for="contactTwoCustomerMain">Contacto 2:</label>
                                                <input type="text" name="contactTwoCustomerMain" id="contactTwoCustomerMain" class="form-control">
                                                <label for="contactTwoPhoneCustomerMain">Contacto Teléfono 2:</label>
                                                <input type="text" name="contactTwoPhoneCustomerMain" id="contactTwoPhoneCustomerMain" class="form-control">
                                                <label for="emailBillingCustomerMain">Email:</label>
                                                <input type="text" name="emailBillingCustomerMain" id="emailBillingCustomerMain" class="form-control">
                                                <label for="typeCfdiCustomerMainNew">Tipo CFDI:</label>
                                                <select name="typeCfdiCustomerMainNewEdit" id="typeCfdiCustomerMainNewEdit" class="form-control">

                                                </select>

                                                <label for="regimeFiscalCustomerMainEdit">Régimen Fiscal*:</label>
                                                <select name="regimeFiscalCustomerMainEdit" id="regimeFiscalCustomerMainEdit" class="form-control">

                                                <label for="methodPayCustomerMain">Método de Pago:</label>
                                                <select name="methodPayCustomerMainEdit" id="methodPayCustomerMainEdit" class="form-control">

                                                </select>
                                                <label for="wayPayCustomerMain">Forma de Pago:</label>
                                                <select name="wayPayCustomerMainEdit" id="wayPayCustomerMainEdit" class="form-control">

                                                </select>
                                                <label for="wayPayCustomerMainNew">Modo de Facturación:</label>
                                                <select name="modeBillingEdit" id="modeBillingEdit" class="form-control">
                                                </select>
                                                <span class="text-danger text-bold" id="textConditions" style="cursor: pointer;"><i class="fa fa-info-circle" aria-hidden="true"></i> Avisos y Condiciones</span>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <label class="control-label" for="tipo">Cuenta Bancaria:</label>
                                                <div class="input-group">
                                                    <select id="bankAccountsEdit" class="form-control">
                                                    </select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-block btn-secondary" data-toggle="modal" data-target="#addAccountBankModal">
                                                            <i class="fa fa fa-plus" aria-hidden="true"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row container-fluid" id="divInvoiceByAmountEdit" style="display: none;">
                                            <div class="col-md-12 text-left">
                                                <div class="col-md-6">
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <label class="control-label" for="tipo">Asocia tu servicio al cátalogo del SAT:</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="productCodeSatEdit" id="productCodeSatEdit" value="70141605 - Servicios de manejo integrado de plagas">
                                                                <input type="hidden" name="productCodeSatIdEdit" id="productCodeSatIdEdit" value="70141605">
                                                                <div id="productCodeSatListEdit"></div>
                                                                <span class="input-group-btn">
                                                                    <a href="http://200.57.3.89/PyS/catPyS.aspx" target="_blank" class="btn btn-block btn-secondary">
                                                                        <i class="fa fa fa-search" aria-hidden="true"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <label class="control-label" for="tipo">Define la unidad de tu servicio</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="productUnitSatEdit" id="productUnitSatEdit" value="E48 - Unidad de Servicio">
                                                                <input type="hidden" name="productUnitSatIdEdit" id="productUnitSatIdEdit" value="3">
                                                                <div id="productUnitSatListEdit"></div>
                                                                <span class="input-group-btn">
                                                                    <a href="http://200.57.3.89/PyS/catUnidades.aspx" target="_blank" class="btn btn-block btn-secondary">
                                                                        <i class="fa fa fa-search" aria-hidden="true"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group" id="divAmountInvoiceEdit">
                                                        <div class="col-sm-12">
                                                            <label class="control-label" for="tipo">Monto Establecido (sin IVA)</label>
                                                            <input type="number" class="form-control" name="amountInvoiceEdit" id="amountInvoiceEdit" placeholder="Ej. 1400">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="row form-group">
                                                        <div class="col-sm-12">
                                                            <label class="control-label" for="tipo">Descripción Facturación (concepto)</label>
                                                            <input type="text" class="form-control" name="descriptionBillingEdit" id="descriptionBillingEdit" placeholder="Servicio de Control de Plagas...">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group" id="divQuantityInvoiceEdit">
                                                        <div class="col-sm-12">
                                                            <label class="control-label" for="tipo">Número de Facturas</label>
                                                            <input type="number" class="form-control" name="quantityInvoiceEdit" id="quantityInvoiceEdit" placeholder="Ej. 12" min="1">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group" id="divDateInvoiceEdit">
                                                        <div class="col-sm-12">
                                                            <label class="control-label" for="tipo">Fecha de facturación</label>
                                                            <input type="date" class="form-control" name="dateInvoiceEdit" id="dateInvoiceEdit" value="{{ \Carbon\Carbon::now()->toDateString() }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-primary" type="button" id="sendEditCustomerMainSave" name="sendEditCustomerMainSave">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA EDITAR CLIENTES -->
