@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Histórico Cliente</h3>

                                                <ol class="breadcrumb" style="margin-top: 5px;">
                                                    <li><a href="{{ Route('index_customers') }}">Mis Clientes</a></li>
                                                    <li class="active">Histórico Cliente</li>
                                                </ol>

<!--                                                    <nav aria-label="...">
                                                    <ul class="pager">
                                                        <li class="previous"><a href="{{ Route('index_customers') }}"><span aria-hidden="true">&larr;</span> Regresar</a></li>
                                                        <li class="next"><a href="#">Clientes <span aria-hidden="true">&rarr;</span></a></li>
                                                    </ul>
                                                </nav>-->

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="text-center col-md-1">
                        <button type="submit" title="Nuevo Comentario" class="btn btn-primary" data-toggle="modal" data-target="#newCommentaryHistoryCustomer"><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                    <!-- /.box-body -->
                    <div class="container-fluid" style="padding-left: 130px">
                        <div class="text-center col-md-11">
                            <h4 class="text-primary">Línea del Tiempo</h4>
                        </div>
                        <div class="page">
                            <div class="timeline">
                                <div class="timeline__group">
                                    <div class="timeline__cards">
                                        <i class="fa fa-user-circle-o text-danger" aria-hidden="true" style="position: absolute; margin: 15px 0 0 -35px"> </i>
                                        <label for="" class="h4">{{ $customer->name }} @if($customer->establishment_name != null) | {{ $customer->establishment_name }} @endif</label>
                                        @foreach($historicalCustomers as $historicalCustomer)
                                        <div class="timeline__card card" style="background-color: transparent !important; border: 2px solid grey !important;">
                                            <header class="card__header">
                                                <time class="time" datetime="2008-02-02">
                                                    <i class="fa fa-star text-danger" aria-hidden="true" style="position: absolute; margin: 12px 0 0 -35px"> </i>
                                                    <span class="time__day badge card-badge-primary" style="position: absolute; margin: 10px 0 0 -120px">{{ Carbon\Carbon::parse($historicalCustomer->date)->toDateString() }}</span>
                                                    <i class="fa fa-arrow-left text-primary" aria-hidden="true"> </i>
                                                    <span class="time__month badge card-badge-primary">{{ Carbon\Carbon::parse($historicalCustomer->hour)->toTimeString() }}</span><label class="card__title r-title h5" style="margin-left: 5px"> {{ $historicalCustomer->title }}</label>
                                                </time>
                                            </header>
                                            <div class="card__content">
                                                <div class="col-md-9">
                                                    <p>{{ $historicalCustomer->commentary }}</p>
                                                </div>
                                                <div class="col-md-3 text-right">
                                                    <p>{{ $historicalCustomer->user_name }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        <i class="fa fa-user-circle-o text-danger" aria-hidden="true" style="position: absolute; margin: 15px 0 0 -35px"> </i>
                                        <i class="fa fa-arrow-left text-primary" aria-hidden="true"> </i>
                                        <span class="timeline__year time badge card-badge-primary" aria-hidden="true">{{ $customer->created_at }}</span>
                                        <label for="" class="h4"> Creación Cliente: {{ $customer->name }} @if($customer->establishment_name != null) | {{ $customer->establishment_name }} @endif</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('vendor.adminlte.customers.historicalcustomer._modalCommentaryHistoricalCustomer')
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/customerhistorical.js') }}"></script>
@endsection