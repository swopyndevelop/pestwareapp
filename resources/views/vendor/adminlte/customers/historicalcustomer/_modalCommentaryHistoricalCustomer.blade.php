<!-- INICIA MODAL PARA AGREGAR COMENTARIO -->
<div class="modal fade" id="newCommentaryHistoryCustomer" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body text-center">
                <input type="hidden" id="idCustomer" value="{{ $customer->id }}">
                <h4>Nuevo Comentario</h4>
                <label for="titleHistoricalCustomer" class="h5">Titulo</label>
                <input type="text" class="form-control" name="titleHistoricalCustomer" id="titleHistoricalCustomer" value="">
                <label for="commentaryHistoricalCustomer" class="h5">Comentario</label>
                <textarea name="commentaryHistoricalCustomer" id="commentaryHistoricalCustomer" cols="30" rows="10" class="form-control"></textarea>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-primary" type="button" id="saveCommentaryHistoricalCustomer">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA AGREGAR COMENTARIO -->