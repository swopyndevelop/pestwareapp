<!-- INICIA MODAL PARA EDITAR SUCURSALES DE CLIENTES -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="editCustomerBranch" role="dialog" aria-labelledby="editCustomerBranch">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" >

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info">Editar <span id="modalTitleCustomerBranch">:</span></h4>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <div class="col-md-6">
                                            <label for="nameCustomerBranch" id="nameCustomerBranchLabel"></label>
                                            <input type="text" name="nameCustomerBranch" id="nameCustomerBranch" class="form-control">
                                            <label for="managerCustomerBranch">Responsable:</label>
                                            <input type="text" name="managerCustomerBranch" id="managerCustomerBranch" class="form-control">
                                            <label for="phoneBranch">Teléfono:</label>
                                            <input type="text" id="phoneBranch" class="form-control">
                                            <label for="emailCustomerBranch">Email:</label>
                                            <input type="text" name="emailCustomerBranch" id="emailCustomerBranch" class="form-control">
                                            <label for="streetCustomerBranch">
                                                @if($labels['street'] != null) {{ $labels['street']->label_spanish }}: @else Calle: @endif
                                            </label>
                                            <input type="text" name="streetCustomerBranch" id="streetCustomerBranch" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="numberCustomerBranch">
                                                Número:
                                            </label>
                                            <input type="text" name="streetCustomerBranch" id="numberCustomerBranch" class="form-control">
                                            <label for="colonyCustomerBranch">
                                                @if($labels['colony'] != null) {{ $labels['colony']->label_spanish }}: @else Colonia: @endif
                                            </label>
                                            <input type="text" name="colonyCustomerBranch" id="colonyCustomerBranch" class="form-control">
                                            <label for="municipalityCustomerBranch">Municipio:</label>
                                            <input type="text" name="municipalityCustomerBranch" id="municipalityCustomerBranch" class="form-control">
                                            <label for="stateCustomerBranch">
                                                @if($labels['state'] != null) {{ $labels['state']->label_spanish }}: @else Estado: @endif
                                            </label>
                                            <input type="text" name="stateCustomerBranch" id="stateCustomerBranch" class="form-control">
                                            <label for="codePostalCustomerBranch">Codígo postal:</label>
                                            <input type="text" name="codePostalCustomerBranch" id="codePostalCustomerBranch" class="form-control">
                                        </div>
                                        <br>
                                        <div class="formulario__mensaje" id="formulario__mensaje_tracing">
                                            <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje_tracing"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                            <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-primary" type="button" id="sendEditCustomerBranchSave" name="sendEditCustomerBranchSave">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA EDITAR SUCURSALES DE CLIENTES -->
