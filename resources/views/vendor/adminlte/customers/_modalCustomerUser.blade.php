<!-- INICIA MODAL PARA CAMBIAR CONTRASEÑA -->
<div class="modal fade" id="customerUser" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body text-center">
                <h4>Portal de Clientes</h4>
                <h5>Agregar Usuario al Portal de Cliente</h5>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-primary" type="button" id="sendCustomerUserSave"
                    name="sendCustomerUserSave">Confirmar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA CAMBIAR CONTRASEÑA -->