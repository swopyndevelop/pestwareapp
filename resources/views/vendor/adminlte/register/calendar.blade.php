@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('css-personal')

@stop


@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <div class="col-lg-1">
                            <h3 class="box-title" id="titleScheduleH3">Agenda</h3>
                        </div>
                        <div class="col-lg-3">
                            <label class="text-sm" for="selectJobCenterCalendar">Filtrar agenda por Centro de Trabajo:</label>
                            <select name="selectJobCenterCalendar" id="selectJobCenterCalendar" class="applicantsList-single form-control">
                                @foreach($jobCenters as $jobCenter)
                                    @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                        <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                            {{ $jobCenter->name }}
                                        </option>
                                    @else
                                        <option value="{{ $jobCenter->id }}">
                                            {{ $jobCenter->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label class="text-sm" for="selectCustomersCalendar">Filtrar agenda por Clientes:</label>
                            <select name="selectCustomersCalendar" id="selectCustomersCalendar" class="applicantsList-single form-control">
                                <option value="0" @if($idCustomerFilter == 0) selected @endif>Todos</option>
                                @foreach($customers as $customer)
                                    @if($idCustomerFilter == $customer->id)
                                        <option value="{{ $customer->id }}" selected>
                                            {{ $customer->name }}
                                        </option>
                                    @else
                                    <option value="{{ $customer->id }}">
                                        {{ $customer->name }}
                                    </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label class="text-sm" for="selectTechniciansCalendar">Filtrar agenda por Técnico:</label>
                            <select id="selectTechniciansCalendar" class="applicantsList-single form-control">
                                <option value="0" @if($idEmployeeFilter == 0) selected @endif>Todos</option>
                                @foreach($technicians as $technician)
                                    @if($idEmployeeFilter == $technician->id)
                                        <option value="{{ $technician->id }}" selected>
                                            {{ $technician->name }}
                                        </option>
                                    @else
                                        <option value="{{ $technician->id }}">
                                            {{ $technician->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">
                        <input type="hidden" id="symbolCountry" value="{{ $symbol_country }}">
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="idOrder" id="idOrder" value="{{$idOrder}}">
                            <div class="col-md-12">
                                <div class="row">
                                    <div id="calendar" class="col-md-12" style="margin-bottom: 20px"></div>
                                    <div class="col-md-2">
                                        <select id="filterPaginate" name="filterPaginate" class="form-control">
                                            <option value="10" selected>10 por página</option>
                                            <option value="30">30 por página</option>
                                            <option value="50">50 por página</option>
                                            <option value="70">70 por página</option>
                                            <option value="100">100 por página</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        {!!Form::open(['method' => 'GET','route'=>'calendario'])!!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Buscar... Orden de Servicio" name="search" id="search">
                                            <span class="input-group-btn">
											<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
										</span>
                                        </div>
                                        {!!Form::close()!!}
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive" style="min-height: 500px;">

                                <table class="table table-hover text-center" id="tableQuotations">
                                    <thead class="table-general" id="homeThScheduleDiv">
                                    <tr>
                                        <th>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary-dark btn-md dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="transferAddButton">
                                                    <span class="fa fa-check-square-o"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a id="allPayment" style="cursor: pointer;"><i class="fa fa-money" aria-hidden="true" style="color: dodgerblue;"></i>Cobrar</a></li>
                                                    <li><a id="allInvoiced" style="cursor: pointer;"><i class="fa fa-sticky-note-o" aria-hidden="true" style="color: indianred;"></i>Facturación</a></li>
                                                </ul>
                                            </div>
                                        </th>
                                        <th scope="col"># Orden Servicio</th>
                                        <th scope="col">Fecha/Hora de creación</th>
                                        <th scope="col">Fecha/Hora de servicio</th>
                                        <th scope="col">Técnico</th>
                                        <th scope="col">Cliente/Empresa</th>
                                        <th scope="col">Teléfono</th>
                                        <th scope="col">Domicilio/Dirección</th>
                                        <th scope="col">Plaga</th>
                                        <th scope="col">Fuente de Origen</th>
                                        @if(!Entrust::can('No Mostrar Precio (Agenda)'))
                                        <th scope="col">Monto</th>
                                        @endif
                                        <th scope="col">Estatus</th>
                                    </tr>
                                    <tr>
                                        <th>
                                            <input type="checkbox" id="chkCheckAll">
                                        </th>
                                        <th class="col" @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendor y Empresarial" @endif>
                                            <div class="btn-group">
                                                <button @if(Auth::user()->id_plan == 1) disabled @endif type="button" class="btn btn-primary-dark btn-md dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="exportFilter">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a id="btnFilter" style="cursor: pointer;"><i class="glyphicon glyphicon-search" aria-hidden="true" style="color: dodgerblue;"></i>Filtrar</a></li>
                                                    <li><a id="btnExport" style="cursor: pointer;"><i class="fa fa-file-excel-o" aria-hidden="true" style="color: green;"></i>Exportar</a></li>
                                                </ul>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <input class="text-input" type="text" name="filterDateCreated" id="filterDateCreated">
                                        </th>
                                        <th scope="col">
                                            <input class="text-input" type="text" name="filterDateEvent" id="filterDateEvent" value="">
                                        </th>
                                        <th scope="col">
                                            <select id="selectFilterTechnician" class="applicantsList-single form-control header-table">
                                                <option value="0" selected>Todos</option>
                                                @foreach($technicians as $technician)
                                                    <option value="{{ $technician->id }}">
                                                        {{ $technician->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th scope="col">
                                            <select id="selectFilterCustomerName" class="applicantsList-single form-control">
                                                <option value="0" selected>Todos</option>
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id }}">
                                                        {{ $customer->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th scope="col">
                                            <select id="selectFilterCustomerPhone" class="applicantsList-single form-control header-table">
                                                <option value="0" selected>Todos</option>
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id }}">
                                                        {{ $customer->cellphone }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th scope="col">
                                            <select id="selectFilterAddress" class="applicantsList-single form-control">
                                                <option value="0" selected>Todos</option>
                                                @foreach($address as $ad)
                                                    <option value="{{ $ad->id }}">{{ $ad->address }}, {{ $ad->address_number }} {{ $ad->colony }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th scope="col">
                                            <select id="filterKey" class="applicantsList-single form-control header-table">
                                                <option value="0" selected>Todos</option>
                                                @foreach($keyPriceLists as $key)
                                                    <option value="{{ $key->id }}">{{ $key->key }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th scope="col">
                                            <select id="selectFilterSource" class="applicantsList-single form-control header-table">
                                                <option value="0" selected>Todos</option>
                                                @foreach($sources as $source)
                                                    <option value="{{ $source->id }}">{{ $source->name }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        @if(!Entrust::can('No Mostrar Precio (Agenda)'))
                                        <th scope="col">
                                            <select id="filterPayment" class="applicantsList-single form-control header-table">
                                                <option value="0" selected>Todos</option>
                                                <option value="2">Pagado</option>
                                                <option value="1">Adeudo</option>
                                                <option value="3">Crédito</option>
                                                <option value="4">Sin Garantía</option>
                                            </select>
                                        </th>
                                        @endif
                                        <th scope="col-md-3" style="text-align: center;">
                                            <select id="filterStatus" class="applicantsList-single form-control header-table">
                                                <option value="0" selected>Todos</option>
                                                <option value="1">Programado</option>
                                                <option value="4">Finalizado</option>
                                                <option value="2">Comenzado</option>
                                                <option value="3">Cancelado</option>
                                                <option value="5">No Realizado</option>
                                                <option value="6">Orden de Servicio</option>
                                                <option value="7">Garantía</option>
                                                <option value="8">Refuerzo</option>
                                                <option value="9">Seguimiento</option>
                                                <option value="10">Facturado</option>
                                            </select>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($order as $o)
                                        <tr id="trScheduleOrderDiv">
                                            <th>
                                                <input class="checkboxClass" type="checkbox" name="ids" id="ids" value="{{ $o->order }}">
                                            </th>
                                            <td scope="row">
                                                <div class="btn-group" id="btnScheduleOrderService">
                                                    <a class="dropdown-toggle" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                                        {{ $o->id_service_order}} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.register._menu2')
                                                </div>
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($o->date)->format('d/m/Y') }}
                                                <br>{{ \Carbon\Carbon::parse($o->date)->format('H:i') }} <br>
                                                {{$o->agente}}
                                            </td>
                                            @if($o->status_order == 10)
                                                <td></td>
                                            @else
                                                <td>{{ \Carbon\Carbon::parse($o->initial_date)->format('d/m/Y') }}
                                                    <br>{{ \Carbon\Carbon::parse($o->initial_hour)->format('H:i') }}
                                                </td>
                                            @endif
                                            @if($o->status_order == 10)
                                                <td></td>
                                            @else
                                                <td>
                                                    @if($o->is_main == 1)
                                                        Responsable: {{$o->name}}
                                                        <span class="text-info" data-toggle="tooltip"
                                                              title="@foreach($o->technicianAuxiliares as $technicianAuxiliar) {{$technicianAuxiliar->name}} @endforeach">
                                                            Auxiliares
                                                        </span>
                                                    @else {{$o->name}}
                                                    @endif
                                                </td>
                                            @endif
                                            <td>{{$o->cliente}} <br> {{$o->empresa}}</td>
                                            <td>{{$o->cellphone}} <br> {{ $o->cellphone_main }}</td>
                                            <td>{{$o->address}} #{{$o->address_number}}, {{$o->colony}}
                                                , {{$o->municipality}}, {{$o->state}}</td>
                                            <td>{{$o->key}}</td>
                                            <td>{{$o->source_name}}</td>
                                            @if(!Entrust::can('No Mostrar Precio (Agenda)'))
                                            <td style="text-align: center;">
                                                @if($o->id_status == 4)
                                                    @if($o->adeudo == 1)
                                                        <strong style="color: #b71c1c">Sub: {{ $symbol_country }}@convert($o->subtotal)</strong>
                                                        <br>
                                                        <strong style="color: #b71c1c">Tot: {{ $symbol_country }}@convert($o->total)</strong>
                                                        @if($o->credito == 1)
                                                            <strong style="color: #b71c1c">Crédito</strong>
                                                        @else
                                                            <strong style="color: #b71c1c">Adeudo</strong>
                                                        @endif
                                                    @else
                                                        @if($o->adeudo == 2)
                                                            <strong style="color: green;">Pagado</strong><br>
                                                            <strong>{{ $symbol_country }}@if($o->cashes->amount_received != null) @convert($o->cashes->amount_received) @endif / {{ $symbol_country }}@convert($o->total)</strong>
                                                        @else
                                                            <strong>{{ $symbol_country }}@convert($o->total)</strong>
                                                        @endif
                                                    @endif
                                                    @if($o->indications == 2)
                                                        <br>
                                                        <strong style="color: #b71c1c"
                                                                title="{{ $o->commentary_indications }}">Sin
                                                            Garantía</strong>
                                                    @endif
                                                        <a href="{{ route('index_billing', \Vinkla\Hashids\Facades\Hashids::encode($o->order)) }}" target="_blank"><strong style="color: #103195;">{{ $o->invoice }}</strong></a>
                                                @else
                                                    @if($o->id_status == 1 && $o->adeudo == 2)
                                                        <strong style="color: green;">Pagado</strong><br>
                                                    @endif
                                                    @if($o->total == 0 && $o->warranty != 0)
                                                        <strong style="color: #b71c1c">Garantía</strong>
                                                    @else
                                                        <strong>Sub: {{ $symbol_country }}@convert($o->subtotal)</strong>
                                                        <br>
                                                        <strong>Tot: {{ $symbol_country }}@convert($o->total)</strong>
                                                    @endif
                                                    <a href="{{ route('index_billing', \Vinkla\Hashids\Facades\Hashids::encode($o->order)) }}" target="_blank"><strong style="color: #103195;">{{ $o->invoice }}</strong></a>
                                                @endif
                                            </td>
                                            @endif
                                            <td style="text-align: center;" id="statusScheduleOrderDiv">
                                                @if($o->id_status == 1)
                                                    <br>
                                                    <span class="label label-primary">Programado</span>
                                                    @if($o->confirmed == 1)
                                                        <br>
                                                        <span class="label label-success">Confirmado</span>
                                                    @endif
                                                @elseif($o->id_status == 2)
                                                    <span class="label label-warning">Comenzado</span>
                                                @elseif($o->id_status == 3)
                                                    <span class="label label-danger" title="{{ $o->motive }}">Cancelado</span>
                                                @elseif($o->id_status == 4)
                                                    <span class="label label-success">Finalizado</span>
                                                    @if($o->rating != 0)
                                                        <br>
                                                        @if($o->rating == 1)
                                                            <span class="fa fa-star"
                                                                  title="{{ $o->rating_comments }}"></span>
                                                        @elseif($o->rating == 2)
                                                            <span class="fa fa-star"
                                                                  title="{{ $o->rating_comments }}"></span>
                                                            <span class="fa fa-star"></span>
                                                        @elseif($o->rating == 3)
                                                            <span class="fa fa-star"
                                                                  title="{{ $o->rating_comments }}"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                        @elseif($o->rating == 4)
                                                            <span class="fa fa-star"
                                                                  title="{{ $o->rating_comments }}"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                        @else
                                                            <span class="fa fa-star"
                                                                  title="{{ $o->rating_comments }}"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                        @endif
                                                    @endif
                                                @else
                                                    <span class="label label-default">No Programado</span>
                                                @endif
                                                @if($o->whatsapp == 1)
                                                    <br>
                                                    <a href="{{ $o->urlWhatsappShow }}" target="_blank" class="btn"
                                                       style="color: green; background-color: transparent;"><i
                                                                class="fa fa-whatsapp" aria-hidden="true"
                                                                style=" font-size: 1.3em;"></i></a>
                                                @endif
                                                @if($o->reminder == 1)
                                                    <i class="fa fa-bell-o" aria-hidden="true"
                                                       style=" font-size: 1.3em; color: green;"
                                                       title="Recordatorio Enviado"></i>
                                                @endif
                                                @if($o->tracing_comments != null) <br>
                                                <i class="fa fa-bell-o" aria-hidden="true"
                                                   style=" font-size: 1.3em; color: #1E8CC7;"
                                                   title="{{ $o->tracing_comments }}"></i><br>
                                                    @if($o->evidence_tracings != null)
                                                    <div id="selectorEvidence" style="cursor: pointer">
                                                        <span class="item" data-src="{{ env('URL_STORAGE_FTP').$o->evidence_tracings }}">
                                                            <img src="{{ env('URL_STORAGE_FTP').$o->evidence_tracings }}" alt="Evidencia" width="25px" height="25px">
                                                        </span>
                                                    </div>
                                                    @endif
                                                @endif
                                                @if($o->invoiced_status == 1)
                                                    <br>
                                                    Facturado
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $order->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>


    <!-- START MODAL DE CREAR EVENTO -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="addSchedule" role="dialog" aria-labelledby="addSchedule">
            <div class="row">
                <div class="col-md-12">
                    <!--Default box-->
                    <div class="modal-dialog modal-lg" role="document">

                        <div class="modal-content">
                            <div class="box" id="modalScheduleNewServiceDiv">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal"
                                                aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>

                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitleSchedule">Agendar
                                        Nuevo Servicio</h4>
                                    <div class="row">
                                        <hr>
                                        <h5 class="text-primary text-center" id="rangeDateSchedule"></h5>
                                        <div class="col-md-6">
                                            <input name="dateStart" id="dateStart" type="hidden">
                                            <input name="hourStart" id="hourStart" type="hidden">
                                            <input name="dateEnd" id="dateEnd" type="hidden">
                                            <input name="hourEnd" id="hourEnd" type="hidden">
                                            <label class="control-label" for="title"
                                                   style="font-weight: bold;">Titulo: </label>
                                            <input type="text" class="form-control" id="title" name="title"
                                                   placeholder="Fumigación Juan Carlos">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label" style="font-weight: bold;">Técnico Responsable: </label>
                                            <div @if(Auth::user()->companie == 37 || Auth::user()->companie == 371 ||
                                                    Auth::user()->companie == 338 || Auth::user()->companie == 451) class="input-group" @endif>
                                                <select name="techniciansEvent" id="techniciansEvent"
                                                        class="applicantsList-single form-control"
                                                        style="width: 100%; font-weight: bold;">
                                                    <option value="0" selected="selected" disabled="disabled">Selecciona un
                                                        técnico
                                                    </option>
                                                </select>
                                                <span class="input-group-btn">
                                                    @if(Auth::user()->companie == 37 || Auth::user()->companie == 371 ||
                                                        Auth::user()->companie == 338 || Auth::user()->companie == 451)
                                                    <button @if (Auth::user()->id_plan == 3) id="showSelectAditionalTechnicians"
                                                            @else title="Plan Corporativo"
                                                            @endif
                                                            data-toggle="tooltip" data-placement="right" class="btn btn-primary">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="rowSelectAditionalTechnicians" style="display: none;">
                                        <br>
                                        <div class="col-md-12">
                                            <label class="control-label" style="font-weight: bold;">Técnicos Auxiliares: </label>
                                            <div class="input-group">
                                                <select name="techniciansAuxiliariesEvent[]" id="techniciansAuxiliariesEvent" class="form-control" style="width: 100%; font-weight: bold;" multiple="multiple">
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" id="hideSelectAditionalTechnicians"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12" id="selectServicesDiv">
                                            <label for="orders" style="font-weight: bold;">OS/G/R/S/I:</label>
                                            <select name="ordersEvent" id="ordersEvent"
                                                    class="applicantsList-single form-control"
                                                    style="width: 100%; font-weight: bold;">
                                                <option value="0" selected="selected" disabled="disabled">Selecciona una
                                                    OS/G/R/S/I
                                                </option>
                                                @foreach($ordersNotSchedule as $o)
                                                    <option value="{{ $o->order }}" data-client="{{ $o->cliente }}"
                                                            data-order="{{ $o->id_service_order }}">
                                                        {{ $o->id_service_order }} / {{ $o->cliente }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="text-center">
                                        <button class="btn btn-primary btn-md" type="button"
                                                id="saveEvent" name="saveEvent">Agendar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL DE CREAR EVENTO -->

    <!-- START MODAL DE ACTUALIZAR EVENTO -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="updateSchedule" role="dialog" aria-labelledby="updateSchedule">
            <div class="row">
                <div class="col-md-12">
                    <!--Default box-->
                    <div class="modal-dialog modal-lg" role="document">

                        <div class="modal-content">
                            <div class="box" id="modalScheduleNewServiceDiv">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal"
                                                aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>

                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitleSchedule">Actualizar Servicio</h4>
                                    <div class="row">
                                        <hr>
                                        <h5 class="text-primary text-center" id="rangeDateScheduleUpdate"></h5>
                                        <div class="col-md-6">
                                            <input name="dateStartUpdate" id="dateStartUpdate" type="hidden">
                                            <input name="hourStartUpdate" id="hourStartUpdate" type="hidden">
                                            <input name="dateEndUpdate" id="dateEndUpdate" type="hidden">
                                            <input name="hourEndUpdate" id="hourEndUpdate" type="hidden">
                                            <input name="orderEventUpdate" id="orderEventUpdate" type="hidden">
                                            <label class="control-label" for="title"
                                                   style="font-weight: bold;">Titulo: </label>
                                            <input type="text" class="form-control" id="titleUpdate" name="titleUpdate"
                                                   placeholder="Fumigación Juan Carlos">
                                        </div>
                                        <div class="col-md-6" id="selecttechniciansDiv">
                                            <label for="techniciansEvent" style="font-weight: bold;">Técnicos
                                                Disponibles:</label>
                                            <select name="techniciansEventUpdate" id="techniciansEventUpdate"
                                                    class="applicantsList-single form-control"
                                                    style="width: 100%; font-weight: bold;">
                                                <option value="0" selected="selected" disabled="disabled">Selecciona un
                                                    técnico
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <div class="modal-footer">
                                    <div class="text-center">
                                        <button class="btn btn-primary btn-lg" type="button"
                                                id="updateEvent" name="updateEvent">Actualizar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL DE ACTUALIZAR EVENTO -->

    <!-- START MODAL ALL PAYMENTS -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="allPaymentModal" role="dialog" aria-labelledby="allPaymentModal">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal-dialog modal-lg" role="document" style="width: 90% !important;">
                        <div class="modal-context">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                                <br>
                                    <div class="box-body">
                                        <input type="hidden" class="form-control" id="allIdsPayment" name="allIdsPayment">
                                        <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Pago Múltiple Servicios</h4>
                                        <h2 class="modal-title text-center col-lg-12 text-success" id="labelTotalCash"></h2>
                                        <div class="col-md-12 container-fluid">
                                            <br>
                                            <table class="table table-hover">
                                                <thead>
                                                <tr class="bg-primary">
                                                    <th class="text-center"># Orden Servicio</th>
                                                    <th class="text-center">Método de Pago</th>
                                                    <th class="text-center">Forma de Pago</th>
                                                    <th class="text-center">Importe</th>
                                                    <th class="text-center">Fecha</th>
                                                    <th class="text-center">El cliente no pago</th>
                                                </tr>
                                                </thead>
                                                <div id="container-table-payments">
                                                    <tbody id="tableBodyDetailPayments">

                                                    </tbody>
                                                </div>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="text-center">

                                        <div class="col-sm-12 formulario__mensaje" id="formulario__mensaje" style="margin: 10px; margin-right: 10px;">
                                            <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                        </div>
                                        <br>
                                        <div class="col-sm-12 text-center formulario__grupo formulario__grupo-btn-enviar">
                                            <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                        </div>

                                        <button class="btn btn-primary btn-md" id="savePaymentAll">Cobrar Servicios</button>
                                    </div>
                                    <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL ALL PAYMENTS -->

    <!-- START MODAL ALL PAYMENTS -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="allInvoicedModal" role="dialog" aria-labelledby="allInvoicedModal">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal-dialog modal-lg" role="document" style="width: 90% !important;">
                        <div class="modal-context">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <div class="box-body">
                                    <input type="hidden" class="form-control" id="allIdsInvoiced" name="allIdsInvoiced">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Facturación Múltiple Servicios</h4>
                                    <h2 class="modal-title text-center col-lg-12 text-success" id="labelTotalCash"></h2>
                                    <div class="col-md-12 container-fluid">
                                        <br>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr class="bg-primary">
                                                <th class="text-center"># Orden Servicio</th>
                                                <th class="text-center">Facturar</th>
                                            </tr>
                                            </thead>
                                            <div id="container-table-payments">
                                                <tbody id="tableBodyDetailInvoices">

                                                </tbody>
                                            </div>
                                        </table>
                                    </div>
                                </div>

                                <div class="text-center">

                                    <div class="col-sm-12 formulario__mensaje" id="formulario__mensaje" style="margin: 10px; margin-right: 10px;">
                                        <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                    </div>
                                    <br>
                                    <div class="col-sm-12 text-center formulario__grupo formulario__grupo-btn-enviar">
                                        <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                    </div>

                                    <button class="btn btn-primary btn-md" id="saveInvoiceAll">Facturar Servicios</button>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL ALL PAYMENTS -->

    @include('vendor.adminlte.register._modalCash')
    @include('vendor.adminlte.customers._modalCreateBankAccount')
    @include('vendor.adminlte.register.billing._modalPurchaseFoliosCalendar')
@endsection

@section('personal-js')

    <script type="text/javascript" src="{{ URL::asset('js/build/calendar.js') }}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js"></script>
    <script>
        $("#scheduleQ").modal();
        $("#scheduleQ").on('hidden.bs.modal', function () {
            window.location.href = '{{route("index_register")}}'; //using a named route
        });
        $('#selectorEvidence').lightGallery({
            selector: '.item'
        });
    </script>

    <script>
        $("#techniciansAuxiliariesEvent").select2();
        $(".plagueList-single").select2();
        $(".applicantsList-single").select2();
    </script>

    <script>

        function loadTechniciansAvailable(initialDate, finalDate, initialHour, finalHour) {
            loaderPestWare('Buscando técnicos disponibles...');
            $.ajax({
                type: 'POST',
                url: route('load_technicians_available'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    initialDate: initialDate,
                    finalDate: finalDate,
                    initialHour: initialHour,
                    finalHour: finalHour
                },
                success: function (data) {
                    if (data.errors) {
                        Swal.close();
                        missingText('Algo salio mal');
                    } else {
                        let len = data.length;
                        if (len === 0) {
                            Swal.close();
                            $('#addSchedule').modal('hide');
                            missingText('No hay técnicos disponibles. Por favor selecciona otro horario.');
                        } else {
                            $('#rangeDateSchedule').html(`Fecha: ${initialDate} Hora: ${initialHour} - ${finalHour}`);
                            $("#techniciansEvent").empty();
                            $("#techniciansAuxiliariesEvent").empty();
                            for (let i = 0; i < len; i++) {
                                let id = data[i]['id'];
                                let name = data[i]['name'];

                                $("#techniciansEvent").append("<option value='" + id + "'>" + name + "</option>");
                                $("#techniciansAuxiliariesEvent").append("<option value='" + id + "'>" + name + "</option>");

                            }
                            Swal.close();
                        }
                    }
                }
            });
        }

        function loadTechniciansAvailableUpdate(initialDate, finalDate, initialHour, finalHour, title, eventId) {
            loaderPestWare('Buscando técnicos disponibles...');
            $.ajax({
                type: 'POST',
                url: route('load_technicians_available'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    initialDate: initialDate,
                    finalDate: finalDate,
                    initialHour: initialHour,
                    finalHour: finalHour,
                    id_event: eventId
                },
                success: function (data) {
                    if (data.errors) {
                        Swal.close();
                        missingText('Algo salio mal');
                    } else {
                        let len = data.length;
                        if (len === 0) {
                            Swal.close();
                            $('#updateSchedule').modal('hide');
                            missingText('No hay técnicos disponibles. Por favor selecciona otro horario.');
                        } else {
                            $('#rangeDateScheduleUpdate').html(`Fecha: ${initialDate} Hora: ${initialHour} - ${finalHour}`);
                            $('#titleUpdate').val(title);
                            let selectTechnicians = $("#techniciansEventUpdate");
                            selectTechnicians.empty();
                            for (let i = 0; i < len; i++) {
                                let id = data[i]['id'];
                                let name = data[i]['name'];

                                selectTechnicians.append("<option value='" + id + "'>" + name + "</option>");

                            }
                            Swal.close();
                        }
                    }
                }
            });
        }

        document.addEventListener('DOMContentLoaded', function () {

            let events = [
                    @foreach ($events as $event)
                {
                    @if($event->id_status == 4) title: '{{ $event->title }} \n (FINALIZADO)'
                    @elseif($event->id_status == 2) title: '{{ $event->title }} \n (COMENZADO)'
                    @else title: '{{ $event -> title }}' @endif,
                    id: '{{ \Vinkla\Hashids\Facades\Hashids::encode($event->id) }}',
                    start: '{{ $event->initial_date }}' + ' ' + '{{ $event->initial_hour }}',
                    end: '{{ $event->final_date }}' + ' ' + '{{ $event->final_hour }}',
                    @if($event->id_status == 4) color: '#00a157'
                    @elseif($event->id_status == 2) color: '#f39c12'
                    @else color: '{{ $event->color }}' @endif
                },
                @endforeach
            ];

            let min = '08:00:00';
            let max = '20:00:00';
            let modeFull = false;

            let calendarEl = document.getElementById('calendar');
            let company = '{{ auth()->user()->companie }}';
            let calendarView = 'timeGridWeek';
            if (company == 338) calendarView = 'dayGridMonth';

            let calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: ['interaction', 'dayGrid', 'timeGrid', 'momentTimezonePlugin', 'list'],
                timeZone: 'local',
                defaultView: calendarView,
                locale: 'es',
                customButtons: {
                    changeRange: {
                        text: '12/24',
                        click: function () {
                            if (modeFull) {
                                calendar.setOption('minTime', '08:00:00');
                                calendar.setOption('maxTime', '20:00:00');
                                modeFull = false;
                            } else {
                                calendar.setOption('minTime', '00:00:00');
                                calendar.setOption('maxTime', '24:00:00');
                                modeFull = true;
                            }
                        }
                    }
                },
                header: {
                    left: 'prev,next,changeRange',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay,listDay,monthYear'
                },
                minTime: min,
                maxTime: max,
                contentHeight: "auto",
                events: events,
                eventClick: function (info) {
                    getDataEvent(info.event.id);
                },
                selectable: true,
                selectMirror: true,
                select: function (arg) {

                    let initialHour = moment(arg.start).format('HH:mm');
                    let finalHour = moment(arg.end).format('HH:mm');
                    let initialDate = moment(arg.start).format('YYYY-MM-DD');
                    let finalDate = moment(arg.end).format('YYYY-MM-DD');
                    let title = 'Fumigación...';

                    //set values event
                    document.getElementById("dateStart").value = initialDate;
                    document.getElementById("dateEnd").value = finalDate;
                    document.getElementById("hourStart").value = initialHour;
                    document.getElementById("hourEnd").value = finalHour;

                    //show modal
                    $('#addSchedule').modal('show');

                    //load data technicians
                    loadTechniciansAvailable(initialDate, finalDate, initialHour, finalHour);

                    /*if (isEvent) {
                        calendar.addEvent({
                        title: title,
                        start: arg.start,
                        end: arg.end,
                        allDay: arg.allDay
                      })
                    }*/

                    calendar.unselect()
                },
                editable: true,
                eventDrop: function(info) {
                    updateEvent(info);
                },
                eventResize: function(info) {
                    updateEvent(info);
                }
            });

            calendar.render();
        });

        function updateEvent(info) {
            const initialHour = moment(info.event.start).format('HH:mm');
            const finalHour = moment(info.event.end).format('HH:mm');
            const initialDate = moment(info.event.start).format('YYYY-MM-DD');
            const finalDate = moment(info.event.end).format('YYYY-MM-DD');
            const title = info.event.title;

            //set values event
            document.getElementById("dateStartUpdate").value = initialDate;
            document.getElementById("dateEndUpdate").value = finalDate;
            document.getElementById("hourStartUpdate").value = initialHour;
            document.getElementById("hourEndUpdate").value = finalHour;
            document.getElementById("orderEventUpdate").value = info.event.id;

            Swal.fire({
                type: 'info',
                title: `¿Estás seguro de modificar el servicio de ${info.event.title}?`,
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: `Si`,
                denyButtonText: `No`,
            }).then((result) => {
                if (result.value) {
                    //show modal
                    $('#updateSchedule').modal('show');
                    //load data technicians
                    loadTechniciansAvailableUpdate(initialDate, finalDate, initialHour, finalHour, title, info.event.id);
                } else {
                    Swal.fire('Cambios descartados', '', 'info')
                    info.revert();
                }
            })
        }

        const symbolCountry = document.getElementById('symbolCountry').value;
        //get price from database
        function getDataEvent(id) {
            loaderPestWare('');
            $.ajax({
                type: 'GET',
                url: route('info_event', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        missingText('Algo salio mal');
                    } else {
                        data = data.data
                        var json = JSON.parse(JSON.stringify(data));
                        let urlBase = "https://pestwareapp.com/service/schedule?";
                        let urlGeolocation = `https://www.google.com/maps/search/?api=1&query=${data.latitude}%2C${data.longitude}`;
                        let url = urlBase + "idOS=" + data.id_event + "&dateCreated=0&dateEvent=0" +
                            "&idEmployee=0&customerName=0&customerPhone=0" +
                            "&customerAddress=0&key=0&payment=0&status=0";
                        let messageFinished = data.status == 4 ? '<strong style="font-size:15px; color: #00a157;">FINALIZADO</strong>' : '';
                        let footer = `
                            <div class="col-md-12 text-center">
                            ${data.companie == 37 || data.companie == 338 || data.companie == 220 ?
                            `${data.latitude != null && data.longitude != null ? `
                            <a href="${urlGeolocation}" target="_blank" class="btn btn-sm" style="margin-right: 10px; color: white"
                                data-toggle="tooltip" data-placement="top" title="Ubicación - Hora inicio:${data.start_event}">
                                <i class="fa fa-map-marker fa-2x" style="color: #0a6aa1"></i>
                            </a>
                            `: ''}`: ''
                            }
                            <a href="${route('edit_event', data.id_event_encrypt)}" class="btn btn-sm"
                                style="margin-right: 10px; background: white">
                                <i class="fa fa-calendar fa-2x" style="color: #0a6aa1" data-toggle="tooltip" data-placement="top" title="Editar Fecha y Técnico"></i>
                            </a>
                            ${data.is_shared == 1 && data.is_main == 1 || data.is_shared == 0 && data.is_main == 0 ?
                            `<a href="${route('edit_service', data.id_order)}" class="btn btn-sm"
                                style="margin-right: 10px; background: white">
                                <i class="fa fa-pencil fa-2x" style="color: #0a6aa1" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                            </a>` : ''};
                            <a href="${route('delete_view', data.id_order)}" class="btn btn-sm"
                                style="margin-right: 10px; background: white">
                                <i class="fa fa-trash fa-2x" style="color: red" data-toggle="tooltip" data-placement="top" title="Cancelar"></i>
                            </a>
                            ${data.is_shared == 1 && data.is_main == 1 || data.is_shared == 0 && data.is_main == 0 ?
                            `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#sendReminderWhatsappIndications"
                                data-id="${data.id_order_decrypt}"
                                data-url="${data.indicationsWhatsapp}"
                                data-cellphonecustomer="${data.cellphone}"
                                data-cellphonemain="${data.cellphone_main}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-whatsapp fa-2x" style="color: green" data-toggle="tooltip" data-placement="top" title="Enviar Indicaciones WhatsApp"></i>
                            </a>
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#sendReminderWhatsappSchedule"
                                data-id="${data.id_order_decrypt}"
                                data-url="${data.urlWhatsappReminder}"
                                data-cellphonecustomer="${data.cellphone}"
                                data-cellphonemain="${data.cellphone_main}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-bell-o fa-2x" style="color: green" data-toggle="tooltip" data-placement="top" title="Enviar Recordatorio WhatsApp"></i>
                            </a>` : ''};
                            <a href="${route('pdf_order_service_template_os', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Plantilla Orden de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            <a href="${route('pdf_order_service_template', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Plantilla Certificado">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            ${data.is_shared == 1 && data.is_main == 1 || data.is_shared == 0 && data.is_main == 0 ?
                            `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#newCashModal"
                                data-toggle="tooltip" data-placement="top" title="Cobrar"
                                data-idevent="${data.id_event}"
                                data-idorder="${data.id_order_decrypt}"
                                data-total="${data.total}"
                                data-serviceorder="${data.id_service_order}"
                                data-amountreceived="${data.amount_received}"
                                data-commentary="${data.commentary}"
                                data-payment="${data.payment}"
                                data-paymentmethod="${data.id_payment_method}"
                                data-paymentway="${data.id_payment_way}"
                                data-payday="${data.pay_day}"
                                data-isupdate="${data.update_cash}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-money fa-2x" style="color: green"></i>
                            </a>` : ''};
                            </div>
                        `;

                        let footerTwo = `
                            <div class="col-md-12 text-center">
                            ${data.companie == 37 || data.companie == 338 || data.companie == 220 ?
                            `${data.latitude != null && data.longitude != null ? `
                            <a href="${urlGeolocation}" target="_blank" class="btn btn-sm" style="margin-right: 10px; color: white"
                                data-toggle="tooltip" data-placement="top" title="Ubicación - Hora inicio:${data.start_event}">
                                <i class="fa fa-map-marker fa-2x" style="color: #0a6aa1"></i>
                            </a>
                            `: ''}`: ''
                        }
                            <a href="${route('edit_event', data.id_event_encrypt)}" class="btn btn-sm"
                                style="margin-right: 10px; background: white">
                                <i class="fa fa-calendar fa-2x" style="color: #0a6aa1" data-toggle="tooltip" data-placement="top" title="Editar Fecha y Técnico"></i>
                            </a>
                            ${data.is_shared == 1 && data.is_main == 1 || data.is_shared == 0 && data.is_main == 0 ?
                            `<a href="${route('edit_service', data.id_order)}" class="btn btn-sm"
                                style="margin-right: 10px; background: white">
                                <i class="fa fa-pencil fa-2x" style="color: #0a6aa1" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                            </a>` : ''};
                            <a href="${route('delete_view', data.id_order)}" class="btn btn-sm"
                                style="margin-right: 10px; background: white">
                                <i class="fa fa-trash fa-2x" style="color: red" data-toggle="tooltip" data-placement="top" title="Cancelar"></i>
                            </a>
                            ${data.is_shared == 1 && data.is_main == 1 || data.is_shared == 0 && data.is_main == 0 ?
                            `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#sendReminderWhatsappIndications"
                                data-id="${data.id_order_decrypt}"
                                data-url="${data.indicationsWhatsapp}"
                                data-cellphonecustomer="${data.cellphone}"
                                data-cellphonemain="${data.cellphone_main}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-whatsapp fa-2x" style="color: green" data-toggle="tooltip" data-placement="top" title="Enviar Indicaciones WhatsApp"></i>
                            </a>
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#sendReminderWhatsappSchedule"
                                data-id="${data.id_order_decrypt}"
                                data-url="${data.urlWhatsappReminder}"
                                data-cellphonecustomer="${data.cellphone}"
                                data-cellphonemain="${data.cellphone_main}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-bell-o fa-2x" style="color: green" data-toggle="tooltip" data-placement="top" title="Enviar Recordatorio WhatsApp"></i>
                            </a>` : ''};
                            <a href="${route('pdf_order_service_template_os', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Plantilla Orden de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            <a href="${route('pdf_order_service_template', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Plantilla Certificado">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            </div>
                        `;

                        let footerFinished =
                            `<div class="col-md-12 text-center">
                            ${data.companie == 37 || data.companie == 338 || data.companie == 220 ?
                                `${data.latitude != null && data.longitude != null ? `
                            <a href="${urlGeolocation}" target="_blank" class="btn btn-sm" style="margin-right: 10px; color: white"
                                data-toggle="tooltip" data-placement="top" title="Ubicación - Hora inicio:${data.start_event}">
                                <i class="fa fa-map-marker fa-2x" style="color: #0a6aa1"></i>
                            </a>
                            `: ''}`: ''
                            }
                            <a href="${route('order_view', data.id_order)}" class="btn btn-sm" style="margin-right: 10px; color: white"
                                data-toggle="tooltip" data-placement="top" title="Ver">
                                <i class="fa fa-search fa-2x" style="color: #0a6aa1"></i>
                            </a>
                            <a href="${route('pdf_service', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Certificado de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            ${data.is_shared == 1 && data.is_main == 1 || data.is_shared == 0 && data.is_main == 0 ?
                            `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#sendemail"
                                data-id="${data.id_order_decrypt}"
                                data-email="${data.email}"
                                data-station="${data.isInspection}"
                                data-area="${data.isAreaInspection}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; color: white">
                                <i class="fa fa-envelope fa-2x" style="color: #0a6aa1" data-toggle="tooltip" data-placement="top" title="Enviar Certificado"></i>
                            </a>
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#sendemailwhatsapp"
                                data-id="${data.id_order}"
                                data-urlwhatsapp="${data.urlWhatsappEmail}"
                                data-service="${data.urlWhatsappEmailService}"
                                data-order="${data.urlWhatsappEmailOrder}"
                                ${data.isInspection} == 1 ? data-urlinspection="${data.urlWhatsappEmailInspection}": ''
                                data-cellphone="${data.cellphone}"
                                data-cellphonemain="${data.cellphone_main}"
                                data-station="${data.isInspection}"
                                data-area="${data.isAreaInspection}"
                                ${data.isAreaInspection} == 1 ? data-urlarea="${data.isAreaInspectionPdf}": ''
                                ` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-whatsapp fa-2x" style="color: green" data-toggle="tooltip" data-placement="top" title="Enviar Certificado"></i>
                            </a>
                            ${data.rating == 0 ? `
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#raiting" data-id="${data.id_order_decrypt}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-star fa-2x" style="color: #a0a027" data-toggle="tooltip" data-placement="top" title="Evaluar"></i>
                            </a>` : ''};
                            ${data.warranty == 0 && data.indications != 2 ?
                                `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#warranty" data-id="${data.id_order_decrypt}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white" id="buttonWarrantyServiceOrder">
                                <i class="fa fa-exclamation-triangle fa-2x" style="color: orange" data-toggle="tooltip" data-placement="top" title="Garantía"></i>
                            </a>` : `<a style="display: none" <a/>`};
                            ${data.price_reinforcement != null && data.reinforcement == 0 ?
                                    `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#reinforcement" data-id="${data.id_order_decrypt}"` : ''}
                                    class="btn btn-sm" style="margin-right: 10px; background: white">
                                    <i class="fa fa-shield fa-2x" style="color: green" data-toggle="tooltip" data-placement="top" title="Refuerzo"></i>
                                </a>` : ''};
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#tracing" data-id="${data.id_order_decrypt}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-long-arrow-right fa-2x" style="color: #0090ff" data-toggle="tooltip" data-placement="top" title="Seguimiento"></i>
                            </a>
                            <a href="${route('pdf_order_service', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Orden de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            ${data.inspection == 0 ?
                           `<a href="${route('pdf_service', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Certificado de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            <a href="${route('pdf_order_service_template_os', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Plantilla Orden de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            <a href="${route('pdf_order_service_template', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Plantilla Certificado">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>`
                            : ''};
                                <a data-toggle="modal" data-target="#sendRequestSignature" data-id="${data.id_order}" data-urlwhatsapp="${data.urlWhatsappEmail}"
                                    class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-pencil fa-2x" style="color: cornflowerblue" data-toggle="tooltip" data-placement="top" title="Firma Cliente"></i>
                            </a>
                            ${data.isInspection == 1 ?
                                `<a href="${route('station_monitoring_pdf', data.id_order)}" target="_blank"
                                class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Monitoreo de Estaciones">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>` : ''};
                            ${data.isAreaInspection == 1 ?
                                `<a href="${route('area_inspection_pdf', data.id_order)}" target="_blank"
                                class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Monitoreo de Áreas">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>` : ''};
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#newCashModal"
                                data-toggle="tooltip" data-placement="top" title="Editar Pago"
                                data-idevent="${data.id_event}"
                                data-idorder="${data.id_order_decrypt}"
                                data-total="${data.total}"
                                data-serviceorder="${data.id_service_order}"
                                data-amountreceived="${data.amount_received}"
                                data-commentary="${data.commentary}"
                                data-payment="${data.payment}"
                                data-paymentmethod="${data.id_payment_method}"
                                data-paymentway="${data.id_payment_way}"
                                data-payday="${data.pay_day}"
                                data-isupdate="${data.update_cash}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-money fa-2x" style="color: green"></i>
                            </a>` : ''};
                            </div>`;


                        let footerFinishedTwo =
                            `<div class="col-md-12 text-center">
                            ${data.companie == 37 || data.companie == 338 || data.companie == 220 ?
                                `${data.latitude != null && data.longitude != null ? `
                            <a href="${urlGeolocation}" target="_blank" class="btn btn-sm" style="margin-right: 10px; color: white"
                                data-toggle="tooltip" data-placement="top" title="Ubicación - Hora inicio:${data.start_event}">
                                <i class="fa fa-map-marker fa-2x" style="color: #0a6aa1"></i>
                            </a>
                            `: ''}`: ''
                            }
                            <a href="${route('order_view', data.id_order)}" class="btn btn-sm" style="margin-right: 10px; color: white"
                                data-toggle="tooltip" data-placement="top" title="Ver">
                                <i class="fa fa-search fa-2x" style="color: #0a6aa1"></i>
                            </a>
                            ${data.is_shared == 1 && data.is_main == 1 || data.is_shared == 0 && data.is_main == 0 ?
                                `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#sendemail"
                                data-id="${data.id_order_decrypt}"
                                data-email="${data.email}"
                                data-station="${data.isInspection}"
                                data-area="${data.isAreaInspection}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; color: white">
                                <i class="fa fa-envelope fa-2x" style="color: #0a6aa1" data-toggle="tooltip" data-placement="top" title="Enviar Certificado"></i>
                            </a>
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#sendemailwhatsapp"
                                data-id="${data.id_order}"
                                data-urlwhatsapp="${data.urlWhatsappEmail}"
                                data-service="${data.urlWhatsappEmailService}"
                                data-order="${data.urlWhatsappEmailOrder}"
                                ${data.isInspection} == 1 ? data-urlinspection="${data.urlWhatsappEmailInspection}": ''
                                data-cellphone="${data.cellphone}"
                                data-cellphonemain="${data.cellphone_main}"
                                data-station="${data.isInspection}"
                                data-area="${data.isAreaInspection}"
                                ${data.isAreaInspection} == 1 ? data-urlarea="${data.isAreaInspectionPdf}": ''
                                ` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-whatsapp fa-2x" style="color: green" data-toggle="tooltip" data-placement="top" title="Enviar Certificado"></i>
                            </a>
                            ${data.rating == 0 ? `
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#raiting" data-id="${data.id_order_decrypt}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-star fa-2x" style="color: #a0a027" data-toggle="tooltip" data-placement="top" title="Evaluar"></i>
                            </a>` : ''};
                            ${data.warranty == 0 && data.indications != 2 ?
                                    `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#warranty" data-id="${data.id_order_decrypt}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white" id="buttonWarrantyServiceOrder">
                                <i class="fa fa-exclamation-triangle fa-2x" style="color: orange" data-toggle="tooltip" data-placement="top" title="Garantía"></i>
                            </a>` : `<a style="display: none" <a/>`};
                            ${data.price_reinforcement != null && data.reinforcement == 0 ?
                                    `<a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#reinforcement" data-id="${data.id_order_decrypt}"` : ''}
                                    class="btn btn-sm" style="margin-right: 10px; background: white">
                                    <i class="fa fa-shield fa-2x" style="color: green" data-toggle="tooltip" data-placement="top" title="Refuerzo"></i>
                                </a>` : ''};
                            <a ${data.id_plan != 1 ? `data-toggle="modal" data-target="#tracing" data-id="${data.id_order_decrypt}"` : ''}
                                class="btn btn-sm" style="margin-right: 10px; background: white">
                                <i class="fa fa-long-arrow-right fa-2x" style="color: #0090ff" data-toggle="tooltip" data-placement="top" title="Seguimiento"></i>
                            </a>
                            <a href="${route('pdf_order_service', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Orden de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            ${data.inspection == 0 ?
                                    `<a href="${route('pdf_service', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Certificado de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            <a href="${route('pdf_order_service_template_os', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Plantilla Orden de Servicio">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>
                            <a href="${route('pdf_order_service_template', data.id_order)}" target="_blank" class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Plantilla Certificado">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>`
                                    : ''};
                            ${data.isInspection == 1 ?
                                    `<a href="${route('station_monitoring_pdf', data.id_order)}" target="_blank"
                                class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Monitoreo de Estaciones">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>` : ''};
                            ${data.isAreaInspection == 1 ?
                                    `<a href="${route('area_inspection_pdf', data.id_order)}" target="_blank"
                                class="btn btn-sm" style="margin-right: 10px; background: white"
                                data-toggle="tooltip" data-placement="top" title="Monitoreo de Áreas">
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                                <i class="fa fa-file-pdf-o fa-2x" style="color: red"></i>
                            </a>` : ''};
                            ` : ''};
                            </div>`;


                        if(data.showPrice) {
                            footerFinished = footerFinishedTwo;
                            footer = footerTwo;
                        }

                        let labelTotal = `<strong style="font-size:15px; color: #1F282C;">Total: ${symbolCountry}${data.total}<strong><br><br>`;
                        if(data.showPrice) labelTotal = '';

                        Swal.close();
                        Swal.fire({
                            type: 'info',
                            showConfirmButton: false,
                            showCloseButton: true,
                            title: '<strong style="font-size:20px; color: #018243;"><a target="_blank" href="' + url + '">' + data.name + '&nbsp;&nbsp;&nbsp;&nbsp;' + data.id_service_order + '</a></strong>',
                            html: messageFinished + '<hr>' +
                                '<strong style="font-size:15px; color: #1F282C;">' + data.initial_date + '&nbsp;&nbsp;&nbsp;&nbsp;' + data.initial_hour + ' - ' + data.final_hour +'</strong><br><br>' +
                                '<strong style="font-size:15px; color: #1F282C;">' + data.plagues + '</strong><br><br>' +
                                labelTotal +
                                '<strong style="font-size:15px; color: #1F282C;">Teléfono: ' + data.cellphone + '<strong><br><br>' +
                                '<strong style="font-size:15px; color: #1F282C;">Cliente y/o Responsable: ' + data.customer + '<strong><br><br>' +
                                '<strong style="font-size:15px; color: #1F282C;">' + data.address + '<strong><br><br>' +
                                '<strong style="font-size:15px; color: #1F282C;">Técnico Asignado: ' + data.technical + '<strong>',
                            width: 800,
                            footer: data.status == 4 ? footerFinished : footer,
                        });
                    }
                }
            });
        }

        function missingText(textError) {
            swal({
                title: "¡Espera!",
                type: "error",
                text: textError,
                icon: "error",
                timer: 3000,
                showCancelButton: false,
                showConfirmButton: false
            });
        }

        function loaderPestWare(message) {
            Swal.fire({
                html: `<div class="content-loader">
                    <div class="loader">
                        <span style="--i:1;"></span>
                        <span style="--i:2;"></span>
                        <span style="--i:3;"></span>
                        <span style="--i:4;"></span>
                        <span style="--i:5;"></span>
                        <span style="--i:6;"></span>
                        <span style="--i:7;"></span>
                        <span style="--i:8;"></span>
                        <span style="--i:9;"></span>
                        <span style="--i:10;"></span>
                        <span style="--i:11;"></span>
                        <span style="--i:12;"></span>
                        <span style="--i:13;"></span>
                        <span style="--i:14;"></span>
                        <span style="--i:15;"></span>
                        <span style="--i:16;"></span>
                        <span style="--i:17;"></span>
                        <span style="--i:18;"></span>
                        <span style="--i:19;"></span>
                        <span style="--i:20;"></span>
                        <div class="insect"></div>
                    </div>
                </div>
                <p class="text-center" style="color: #ffffff"><b>${message}</b></p>`,
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false
            });
            $(".swal2-modal").css('background-color', 'transparent');
            $('.swal2-container.swal2-shown').css('background', 'rgb(11 12 12 / 55%)');
        }
    </script>

    <script>
        $(function (e) {
            $('#chkCheckAll').click(function () {
                $('.checkboxClass').prop('checked', $(this).prop('checked'));
            });
        });
    </script>

@endsection
