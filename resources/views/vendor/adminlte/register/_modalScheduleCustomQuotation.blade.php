@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <link href="{{ asset('/css/table-mobile/table-mobile.css') }}" rel="stylesheet" type="text/css" />
    <div class="content container-fluid spark-screen">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Programar Cotización Personalizada <b>{{ $quotationKey }}</b></h3>
                        <input type="hidden" id="customerId" value="{{ $customerId }}">
                        <input type="hidden" id="quotationId" value="{{ $quotationId }}">

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                                        <b>Múltiples Sucursales <span class="badge">{{ $countBranches }} Sucursales</span></b>
                                        <span class="badge pull-right">PASO 1</span>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-responsive-md table-sm table-bordered"
                                               @if($countOrders == 0) id="makeEditableBranch" @endif>
                                            <thead>
                                            <tr>
                                                <th>Clave</th>
                                                <th>Nombre</th>
                                                <th>Tipo</th>
                                                <th>Teléfono</th>
                                                <th>Correo Electrónico</th>
                                                <th>Nombre del Responsable/Encargado</th>
                                                <th>@if($labels['street'] != null) {{ $labels['street']->label_spanish }} @else Calle @endif</th>
                                                <th>Número</th>
                                                <th>@if($labels['colony'] != null) {{ $labels['colony']->label_spanish }} @else Colonia @endif</th>
                                                <th>Municipio</th>
                                                <th>@if($labels['state'] != null) {{ $labels['state']->label_spanish }} @else Estado @endif</th>
                                                <th>Código Postal</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($branches as $branch)
                                                <tr class="text-center">
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->branch_id }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->name }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->type }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->phone }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->email }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->manager }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->address }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->address_number }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->colony }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->municipality }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->state }}</td>
                                                    <td class="mobile-td-branch" style="text-align: right">{{ $branch->postal_code }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-4 align-self-start">
                                                <a class="btn btn-secondary-outline" type="button"
                                                   @if($countOrders > 0) style="display: none; float: right" @else @endif
                                                   id="importBranches">
                                                    <i class="fa fa-file-excel-o"></i> Importar con Excel
                                                </a>
                                                <a
                                                   href="{{ route('export_format_branches', $quotationId) }}"
                                                   @if($countOrders > 0) style="display: none; float: left; margin-left: 3px;" @else  @endif
                                                   class="btn btn-secondary-outline" type="button">
                                                    <i class="fa fa-file-excel-o"></i> Descargar formato
                                                </a>
                                            </div>
                                            <div class="col-md-4 text-center"  @if($countOrders > 0) style="display: none;" @endif>
                                                <span><button id="saveBranch" class="btn btn-primary">Guardar</button></span>
                                            </div>
                                            <div class="col-md-4 align-self-end">
                                                <button id="but_add_branch"
                                                        @if($countOrders > 0) style="display: none; float: right" @else style="float:right" @endif
                                                        class="btn btn-danger">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="row-config" @if($branches[0]->name == "" || $branches[0]->address == "" ||
                                $branches[0]->address_number == "" || $branches[0]->colony == "" || $branches[0]->municipality == "" ||
                                 $branches[0]->state == "" || count($services) > 0) style="display: none;" @endif>
                            <div class="col-md-12 table-responsive">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                        <b>Configuración Inicial</b>
                                        <span class="badge pull-right">PASO 2</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label for="paymentMethod">Método de pago en común</label>
                                                        <select name="paymentMethod" id="paymentMethod"
                                                                class="form-control">
                                                            <option value="0" selected disabled>Selecciona una
                                                                opción
                                                            </option>
                                                            @foreach($paymentMethods as $method)
                                                                <option value="{{ $method->id }}">{{ $method->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label for="paymentWay">Forma de pago en común</label>
                                                        <select name="paymentWay" id="paymentWay"
                                                                class="form-control">
                                                            <option value="0" selected disabled>Selecciona una
                                                                opción
                                                            </option>
                                                            @foreach($paymentWays as $pw)
                                                                <option value="{{ $pw->id }}">{{ $pw->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label for="typeSchedule">Tipo de Programación</label>
                                                        <select name="typeSchedule" id="typeSchedule" class="form-control">
                                                            <option value="0" selected disabled>Selecciona una opción</option>
                                                            <option value="1">Manual</option>
                                                            <option value="2" disabled>Inteligente</option>
                                                            <option value="3">Importar Con Excel</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="row-config" @if(count($services) == 0) style="display: none;" @endif>
                            <div class="col-md-12 table-responsive">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                        <b>Servicios Programados</b>
                                        <span class="badge pull-right">PROGRAMACIÓN REALIZADA</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <table class="table table-responsive-md table-sm table-hover">
                                                <thead>
                                                    <tr>
                                                        <th># Servicio</th>
                                                        <th>Sucursal</th>
                                                        <th>Fecha</th>
                                                        <th>Hora</th>
                                                        <th>Técnico</th>
                                                        <th>Editar</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php($a = 0)
                                                @foreach($services as $service)
                                                    @php($a = $service->id)
                                                    <tr>
                                                        <td>{{ $service->id_service_order }}</td>
                                                        <td>{{ $service->branche }}</td>
                                                        <td>{{ $service->initial_date }}</td>
                                                        <td>{{ $service->initial_hour }}
                                                            - {{ $service->final_hour }}</td>
                                                        <td>{{ $service->employee }}</td>
                                                        <td>
                                                            @include('vendor.adminlte.register._modalUpdateServiceCustom')
                                                            <a class="btn btn-primary btn-sm" data-toggle="modal"
                                                               data-target="#updateServiceOrder{{ $a }}">
                                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="row-intelligent" style="display: none;">
                            <div class="col-md-12 table-responsive">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                        <b>Programación de Múltiples Servicios <span class="badge">{{ $totalServices }} Servicios</span></b>
                                        <button class="btn btn-sm btn-primary-outline pull-right" style="padding: 2px 10px;" id="saveScheduleSmartPropose">Guardar Programación</button>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label for="initialDateIntelligent">Fecha y Hora Inicial</label>
                                                        <input class="form-control" type="text" name="initialDateIntelligent" id="initialDateIntelligent" placeholder="Seleccione fecha">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label for="paymentWay">Hora Final</label>
                                                        <input class="form-control" type="text" name="finalHourIntelligent" id="finalHourIntelligent" placeholder="Seleccione la hora">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label for="minutes">Duración del servicio (minutos)</label>
                                                        <input type="number" name="minutes" id="minutes" class="form-control" placeholder="Ejemplo: 60">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label for="techniciansIntelligent">Capacidad de Técnicos</label>
                                                        <select name="techniciansIntelligent[]" id="techniciansIntelligent"
                                                                class="form-control"
                                                                multiple="multiple" style="width: 100%;">
                                                            <option value="0" selected>Máxima</option>
                                                            @foreach($employees as $employee)
                                                                <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <label for="">Personalizar Semana</label>
                                                                <br>
                                                                <div class="col-md-1 col-days">
                                                                    <h5 class="check-days">L</h5>
                                                                    <input type="checkbox" name="monday" id="monday">
                                                                </div>
                                                                <div class="col-md-1 col-days">
                                                                    <h5 class="check-days">M</h5>
                                                                    <input type="checkbox" name="tuesday" id="tuesday">
                                                                </div>
                                                                <div class="col-md-1 col-days">
                                                                    <h5 class="check-days">M</h5>
                                                                    <input type="checkbox" name="wednesday" id="wednesday">
                                                                </div>
                                                                <div class="col-md-1 col-days">
                                                                    <h5 class="check-days">J</h5>
                                                                    <input type="checkbox" name="thursday" id="thursday">
                                                                </div>
                                                                <div class="col-md-1 col-days">
                                                                    <h5 class="check-days">V</h5>
                                                                    <input type="checkbox" name="friday" id="friday">
                                                                </div>
                                                                <div class="col-md-1 col-days">
                                                                    <h5 class="check-days">S</h5>
                                                                    <input type="checkbox" name="saturday" id="saturday">
                                                                </div>
                                                                <div class="col-md-1 col-days">
                                                                    <h5 class="check-days">D</h5>
                                                                    <input type="checkbox" name="sunday" id="sunday">
                                                                </div>
                                                            </div>
                                                            <div class="row" id="excludeDay" style="display:none">
                                                                <label for="excludeDay">Excluir día</label>
                                                                <input type="checkbox" id="excludeDay">
                                                                <br>
                                                                <label for="finalHourCustomDay">Diferente Hora</label>
                                                                <input class="form-control" type="text" name="finalHourCustomDay" id="finalHourCustomDay" placeholder="Seleccione la hora" style="width: 50%;">
                                                                <button class="btn btn-sm btn-secondary-outline">
                                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <br>
                                                        <button class="btn btn-sm btn-secondary-outline" id="scheduleIntelligentNew">
                                                            <i class="fa fa-check" aria-hidden="true"></i> Preview
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12" id="masterPanel">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="display: none;">
                            <div class="col-md-12 table-responsive">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                        <b>Rutas Inteligentes <span class="badge" id="totalKm"></span></b>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="col-md-9">
                                                <div id="globalRoute" style="height: 500px; width: 100%;"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="list-group" id="routes-list">

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="text-center" @if(count($services) == 0) style="display: none;" @endif>
                            <a href="{{ route('calendario') }}" class="btn btn-primary btn-md" type="button" id="saveTotalSchedule">Finalizar Programación</a>
                        </div>
                    </div>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
    @include('adminlte::register._modalImportBranchesExcel')
    @include('adminlte::register.customSchedule._modalScheduleManual')
    @include('adminlte::register.customSchedule._modalScheduleImportExcel')
    @include('adminlte::register.customSchedule._modalScheduleGoogleMaps')
    @include('adminlte::register.customSchedule._modalScheduleIntelligent')
@endsection

@section('personal-js')
    <script src="{{ asset('js/build/customSchedule.js') }}"></script>
    <script src="{{ asset('js/bootstable.js') }}"></script>
    <script src="https://use.fontawesome.com/92e776c0ac.js"></script>
    <script>
        $("#techniciansIntelligent").select2();
        $("#techniciansAuxiliariesCustom").select2();
    </script>
    <script>
        $(function () {

            $('#makeEditableBranch').SetEditable({$addButton: $('#but_add_branch')});
            let arrayConcepts = [];
            let isError = false;

            $('#showSelectAdditionalTechniciansCustom').click(function() {
                document.getElementById('rowSelectAdditionalTechniciansCustom').style.display = 'block';
            });
            $('#hideSelectAdditionalTechniciansCustom').click(function() {
                document.getElementById('rowSelectAdditionalTechniciansCustom').style.display = 'none';
            });

            function getDataTable() {
                let td = TableToCSV('makeEditableBranch', ',');
                let ar_lines = td.split("\n");
                let each_data_value = [];
                let errors = [];
                ar_lines.pop();

                ar_lines.forEach((element, i) => {
                    each_data_value[i] = ar_lines[i].split(",");
                });

                each_data_value.forEach((element, i) => {

                    // get data
                    let key = element[0];
                    let name = element[1];
                    let type = element[2]
                    let phone = element[3];
                    let email = element[4];
                    let manager = element[5];
                    let address = element[6];
                    let addressNumber = element[7];
                    let colony = element[8];
                    let municipality = element[9];
                    let state = element[10];
                    let postalCode = element[11];

                    // validations
                    if (name.length === 0) errors.push(`Debes ingresar el nombre de la sucursal. Fila: ${++i}`);
                    if (address.length === 0) errors.push(`Debes ingresar la calle de la sucursal. Fila: ${++i}`);
                    if (addressNumber.length === 0) errors.push(`Debes ingresar el número del domicilio. Fila: ${++i}`);
                    if (colony.length === 0) errors.push(`Debes ingresar la colonia de la sucursal. Fila: ${++i}`);
                    if (municipality.length === 0) errors.push(`Debes ingresar el municipio. Fila: ${++i}`);
                    if (state.length === 0) errors.push(`Debes ingresar el estado. Fila: ${++i}`);

                    // object json
                    arrayConcepts.push({
                        'key': key,
                        'name': name,
                        'type': type,
                        'phone': phone,
                        'email': email,
                        'manager': manager,
                        'address': address,
                        'address_number': addressNumber,
                        'colony': colony,
                        'municipality': municipality,
                        'state': state,
                        'postal_code': postalCode
                    });

                });

                isError = errors.length > 0;

                //console.log(arrayConcepts);
                //console.log(errors);
            }

            $('#saveBranch').on('click', function () {
                getDataTable();
                if (isError) {
                    missingTextC('Faltan datos por completar. Los campos: nombre, calle,' +
                        ' número, colonia, municipio y estado son obligatorios.');
                } else saveBranchesCustom();
            });

            $('#saveServices').on('click', function () {
                let customerId = $('#customerId').val();
                let quotationId = $('#quotationId').val();
                let dateServices = $('#dateServiceManual').val();
                let hourServices = $('#hourServiceManual').val();
                let technician = $('#technicians option:selected').val();
                let paymentMethod = $('#paymentMethod option:selected').val();
                let paymentWay = $('#paymentWay option:selected').val();
                if (dateServices.length === 0) missingTextC('Debes seleccionar una fecha por default para los servicios.');
                else if (hourServices.length === 0) missingTextC('Debes seleccionar una hora por default para los servicios.');
                else if (parseInt(technician) === 0) missingTextC('Debes seleccionar un técnico por default para los servicios.')
                else if (parseInt(paymentMethod) === 0) missingTextC('Debes seleccionar un método de pago por default para los servicios.')
                else if (parseInt(paymentWay) === 0) missingTextC('Debes seleccionar una forma de pago por default para los servicios.')
                else saveServiceOrders(dateServices, hourServices, technician, quotationId, customerId, paymentMethod, paymentWay);
            });

            $('#deleteServices').on('click', function () {
                Swal({
                    title: '¿Está seguro de eliminar las ordenes de servicio?',
                    text: "Al eliminar las ordenes podrá nuevamente generarlas desde cero.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si'
                }).then((result) => {
                    if (result.value) {
                        deleteServices();
                    }
                })
            });

            $('#scheduleQuotationCustom').on('click', function () {
                scheduleCustomQuotation();
            });

            function saveServiceOrders(date, hour, technician, quotationId, customerId, paymentMethod, paymentWay) {
                loaderPestWare('Generando ordenes de servicio...');

                $.ajax({
                    type: 'POST',
                    url: route('store_order_services'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        date: date,
                        hour: hour,
                        technician: technician,
                        id_quotation: quotationId,
                        customerId: customerId,
                        paymentMethod: paymentMethod,
                        paymentWay: paymentWay
                    },
                    success: function (data) {
                        if (data.code === 500) {
                            Swal.close();
                            missingTextC(data.message);
                        } else if (data.code === 201) {
                            Swal.close();
                            showToast('success-redirect','Ordenes de Servicio','Guardando correctamente',location.reload());
                        }
                    }
                });
            }

            function saveBranchesCustom() {
                loaderPestWare('Guardando sucursales...');

                let customerId = $('#customerId').val();
                let quotationId = $('#quotationId').val();

                $.ajax({
                    type: 'POST',
                    url: route('store_customer_branches'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        customerId: customerId,
                        quotationId: quotationId,
                        concepts: JSON.stringify(arrayConcepts)
                    },
                    success: function (data) {
                        if (data.code === 500) {
                            Swal.close();
                            missingText(data.message);
                        } else if (data.code === 201) {
                            Swal.close();
                            showToast('success-redirect','Sucursales','Guardando correctamente', location.reload());
                        }
                    }
                });
            }

            function deleteServices() {
                loaderPestWare('Eliminando Ordenes de Servicio...');

                let quotationId = $('#quotationId').val();

                $.ajax({
                    type: 'POST',
                    url: route('delete_order_service'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        quotationId: quotationId
                    },
                    success: function (data) {
                        if (data.code === 500) {
                            Swal.close();
                            missingText(data.message);
                        } else if (data.code === 201) {
                            Swal.close();
                            showToast('success-redirect','Ordenes de Servicio','Eliminando correctamente', location.reload());
                        }
                    }
                });
            }

            function scheduleCustomQuotation() {
                loaderPestWare('Programando Ordenes de Servicio...');

                let quotationId = $('#quotationId').val();

                $.ajax({
                    type: 'POST',
                    url: route('update_status_quotation_custom'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        quotationId: quotationId
                    },
                    success: function (data) {
                        if (data.code === 500) {
                            Swal.close();
                            missingText(data.message);
                        } else if (data.code === 201) {
                            Swal.close();
                            swal({
                                title: data.message,
                                type: "success",
                                timer: 2000,
                                showCancelButton: false,
                                showConfirmButton: false
                            }).then((result) => {
                                location.href = route('calendario');
                            })
                        }
                    }
                });
            }

            $('#importBranches').on('click',function() {
                $("#importExcelBranches").modal();
            });

            $('#importFileBranches').on('click', function () {
                let file = $('#fileImport').val();
                if (file)
                    loaderPestWare('Importando sucursales...');
                else missingText('Debes seleccionar un archivo de excel.');
            });

            function showToast(type, title, message, path) {
                if (type === 'success-redirect') {
                    toastr.success(message, title);
                    setTimeout(() => {
                        window.location.href = route(path);
                    }, 5000);
                }
                else if (type === 'success') toastr.success(message, title)
                else if (type === 'info') toastr.info(message, title)
                else if (type === 'warning') toastr.warning(message, title)
                else if (type === 'error') toastr.error(message, title)
            }

            function missingTextC(textError) {
                swal({
                    title: "¡Espera!",
                    type: "error",
                    text: textError,
                    icon: "error",
                    timer: 4000,
                    showCancelButton: false,
                    showConfirmButton: false
                });
            }

            function loaderPestWare(message) {
                Swal.fire({
                    html: `<div class="content-loader">
                    <div class="loader">
                        <span style="--i:1;"></span>
                        <span style="--i:2;"></span>
                        <span style="--i:3;"></span>
                        <span style="--i:4;"></span>
                        <span style="--i:5;"></span>
                        <span style="--i:6;"></span>
                        <span style="--i:7;"></span>
                        <span style="--i:8;"></span>
                        <span style="--i:9;"></span>
                        <span style="--i:10;"></span>
                        <span style="--i:11;"></span>
                        <span style="--i:12;"></span>
                        <span style="--i:13;"></span>
                        <span style="--i:14;"></span>
                        <span style="--i:15;"></span>
                        <span style="--i:16;"></span>
                        <span style="--i:17;"></span>
                        <span style="--i:18;"></span>
                        <span style="--i:19;"></span>
                        <span style="--i:20;"></span>
                        <div class="insect"></div>
                    </div>
                </div>
                <p class="text-center" style="color: #ffffff"><b>${message}</b></p>`,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false
                });
                $(".swal2-modal").css('background-color', 'transparent');
                $('.swal2-container.swal2-shown').css('background', 'rgb(11 12 12 / 55%)');
            }


        });
    </script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{ env('API_KEY_GOOGLE') }}&region=MX&language=es&libraries=&v=weekly"
            defer
    ></script>
@endsection