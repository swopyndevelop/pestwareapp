@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <!-- START MODAL DE EDITAR SERVICIO -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="scheduleD" role="dialog" aria-labelledby="newEmployeesModal">
            <div class="row">
                <div class="col-md-12">
                    <!--Default box-->
                    <div class="modal-dialog modal-lg" role="document" >

                        <div class="modal-content" >
                            <div class="box">
                                <div class="modal-header" >
                                    <div class="box-header" >
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body">
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Servicio</h4>
                                <input type="hidden" name="customer_idC" id="customer_idC" value="{{$order_sr->customer}}">
                                <input type="hidden" name="q_idC" id="q_idC" value="{{$order_sr->quotation}}">
                                <input type="hidden" name="o_id" id="o_id" value="{{$order_sr->order}}">
                                <hr>
                                <div class="row container-fluid">
                                    <div class="col-md-6">
                                        <span style="color:black">Tipo de Servicio</span>
                                        <label for="" class="control-label">Tipo de plaga: <span>@foreach($plague as $item)
                                                    {{$item->name}},
                                                @endforeach</span></label>
                                        <label for="" class="control-label"> ({{$area}} m2)</label>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <span style="color:black;">Nº de Orden de Servicio </span><span>{{$order_sr->id_service_order}}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row container-fluid">
                                    <div class="col-md-6">

                                        <h5 style="color:black">Datos del Cliente</h5>
                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__customerNameC">
                                                <label class="control-label" for="customerNameC">Cliente*: </label>
                                                <div class="">
                                                    <input type="text" class="formulario__input form-control" id="customerNameC" name="customerNameC" value="{{$order_sr->cliente}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Este campo sólo admite letras, espacios y acentos de 3 a 250 caracteres.</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__establishmentNameC">
                                                <label for="establishmentName">Empresa:</label>
                                                <div class="">
                                                    <input type="text" class="formulario__input form-control" id="establishmentNameC" name="establishmenNameC" value="{{$order_sr->empresa}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Correcto.</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__cellphoneC">
                                                <label for="cellphoneC">Celular:</label>
                                                <div class="">
                                                    <input type="number" name="cellphoneC" id="cellphoneC" class="formulario__input form-control" value="{{$order_sr->cellphone}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">
                                                    Este campo solo admite números, mínimo 10 y maximo 14 dígitos.</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__emailC">
                                                <label for="emailC">Correo electrónico:</label>
                                                <div class="">
                                                    <input type="text" name="emailC" id="emailC" class="formulario__input form-control" value="{{$order_sr->email}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">El correo electrónico tiene que ser válido por ejemplo: email@gmail.com</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__contactC">
                                                <label for="contactC">Contacto 2:</label>
                                                <div class="">
                                                    <input type="text" name="contactC" id="contactC" class="formulario__input form-control" value="{{$order_sr->contact_two_name}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Correcto.</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__cellphone_twoC">
                                                <label for="cellphone_twoC">Celular 2:</label>
                                                <div class="">
                                                    <input type="text" name="cellphone_twoC" id="cellphone_twoC" class="formulario__input form-control" value="{{$order_sr->contact_two_phone}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Correcto.</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <h5 style="color:black">Domicilio</h5>
                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__stateC">
                                                <label for="state">
                                                    @if($labels['state'] != null) {{ $labels['state']->label_spanish }}*: @else Estado*: @endif
                                                </label>
                                                <div class="">
                                                    <input type="text" class="formulario__input form-control" name="stateC" id="stateC" value="{{$order_sr->state}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Este campo sólo admite letras, espacios y acentos de 3 a 250 caracteres.</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__municipalityC">
                                                <label for="municipalityC">Municipio*:</label>
                                                <div class="">
                                                    <input type="text" class="formulario__input form-control" name="municipalityC" id="municipalityC" value="{{$order_sr->municipality}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Este campo sólo admite letras, espacios y acentos de 3 a 250 caracteres.</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__colonyC">
                                                <label for="colonyC">
                                                    @if($labels['colony'] != null) {{ $labels['colony']->label_spanish }}*: @else Colonia*: @endif
                                                </label>
                                                <div class="">
                                                    <input type="text" class="formulario__input form-control" name="colonyC" id="colonyC" value="{{$order_sr->colony}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Este campo admite 3 a 250 caracteres..</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-8 formulario__grupo" id="grupo__calleC">
                                                <label for="calleC">
                                                    @if($labels['street'] != null) {{ $labels['street']->label_spanish }}*: @else Calle*: @endif
                                                </label>
                                                <div class="">
                                                    <input type="text" class="formulario__input form-control" name="calleC" id="calleC" value="{{$order_sr->address}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                            </div>

                                            <div class="col-md-4 formulario__grupo" id="grupo__numC">
                                                <label for="numC">Num*:</label>
                                                <div class="">
                                                    <input type="text" name="numC" id="numC" class="formulario__input form-control" value="{{$order_sr->address_number}}">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error" style="font-size: 10px">Solo número enteros.</p>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12 formulario__grupo" id="grupo__mensajeC">
                                                <label for="reference">Comentarios al técnico</label>
                                                <div class="">
                                                    <textarea name="mensajeC" id="mensajeC" rows="4" class="formulario__input form-control">{{$order_sr->observations}}</textarea>
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                </div>
                                                <p class="formulario__input-error">Correcto.</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <h5 style="color:black">Condiciones de quién habita</h5>
                                                @if($pregnancy == null)
                                                    <input type="checkbox" name="embarazoC" id="embarazoC" value="1">
                                                @else
                                                    <input type="checkbox" name="embarazoC" id="embarazoC" value="1" checked>
                                                @endif
                                                <label class="control-label" for="embarazoC"> Embarazo</label>
                                                <br>
                                                @if($baby == null)
                                                    <input type="checkbox" name="bebesC" id="bebesC" value="2">
                                                @else
                                                    <input type="checkbox" name="bebesC" id="bebesC" value="2" checked>
                                                @endif
                                                <label class="control-label" for="bebesC"> Bebés</label>
                                                <br>
                                                @if($children == null)
                                                    <input type="checkbox" name="childrenC" id="childrenC" value="3">
                                                @else
                                                    <input type="checkbox" name="childrenC" id="childrenC" value="3" checked>
                                                @endif
                                                <label class="control-label" for="childrenC"> Niños</label>
                                                <br>
                                                @if($adult == null)
                                                    <input type="checkbox" name="adultoC" id="adultoC" value="4">
                                                @else
                                                    <input type="checkbox" name="adultoC" id="adultoC" value="4" checked>
                                                @endif
                                                <label class="control-label" for="adultoC"> Adulto mayor</label>
                                        </div>

                                        <div class="col-md-3">
                                            <h5 style="color:black">Personas con enfermedades</h5>
                                                @if ($respiratory == null)
                                                    <input type="checkbox" name="respiratoriasC" id="respiratoriasC" value="1">
                                                @else
                                                    <input type="checkbox" name="respiratoriasC" id="respiratoriasC" value="1" checked>
                                                @endif
                                                <label class="control-label" for="name"> Respiratorias</label>
                                                    <br>
                                                @if ($inmunologic == null)
                                                    <input type="checkbox" name="inmunologicoC" id="inmunologicoC" value="2">
                                                @else
                                                    <input type="checkbox" name="inmunologicoC" id="inmunologicoC" value="2" checked>
                                                @endif
                                                <label class="control-label" for="name"> Inmunológicas</label>
                                                    <br>
                                                @if ($renal == null)
                                                    <input type="checkbox" name="renalesC" id="renalesC" value="3">
                                                @else
                                                    <input type="checkbox" name="renalesC" id="renalesC" value="3" checked>
                                                @endif
                                                <label class="control-label" for="name"> Renales</label>
                                                    <br>
                                                @if ($cardiac == null)
                                                    <input type="checkbox" name="cardiacasC" id="cardiacasC" value="4">
                                                @else
                                                    <input type="checkbox" name="cardiacasC" id="cardiacasC" value="4" checked>
                                                @endif
                                                <label class="control-label" for="name"> Cardiacas</label>
                                                    <br>
                                                @if ($does_not == null)
                                                    <input type="checkbox" name="does_notC" id="does_notC" value="5">
                                                @else
                                                    <input type="checkbox" name="does_notC" id="does_notC" value="5" checked>
                                                @endif
                                                <label class="control-label" for="name"> No aplica</label>
                                                    <br>
                                                @if ($other == null)
                                                    <input type="checkbox" name="otherC" id="otherC" value="" onclick="specificC(this.checked);">
                                                @else
                                                    <input type="checkbox" name="otherC" id="otherC" value="" onclick="specificC(this.checked);" checked>
                                                @endif
                                                <label class="control-label" for="name"> Otros</label>
                                                    <br>
                                                @if($other == null)
                                                    <input type="text" name="specificC" id="specificC" disabled placeholder="Especifique">
                                                @else
                                                    <input type="text" name="specificC" id="specificC" placeholder="Especifique" value="{{$other->specification}}">
                                                @endif
                                        </div>

                                        <div class="col-md-3">
                                            <h5 style="color:black">Mascotas</h5>
                                                @if($dog == null)
                                                    <input type="checkbox" name="perroC" id="perroC" value="1">
                                                @else
                                                    <input type="checkbox" name="perroC" id="perroC" value="1" checked>
                                                @endif
                                                <label class="control-label" for="name"> Perros</label>
                                                    <br>
                                                @if($cat == null)
                                                    <input type="checkbox" name="gatoC" id="gatoC" value="2">
                                                @else
                                                    <input type="checkbox" name="gatoC" id="gatoC" value="2" checked>
                                                @endif
                                                <label class="control-label" for="name"> Gatos</label>
                                                    <br>
                                                @if($bird == null)
                                                    <input type="checkbox" name="aveC" id="aveC" value="3">
                                                @else
                                                    <input type="checkbox" name="aveC" id="aveC" value="3" checked>
                                                @endif
                                                <label class="control-label" for="name"> Aves</label>
                                                    <br>
                                                @if($fish == null)
                                                    <input type="checkbox" name="pezC" id="pezC" value="4">
                                                @else
                                                    <input type="checkbox" name="pezC" id="pezC" value="4" checked>
                                                @endif
                                                <label class="control-label" for="name"> Peces</label>
                                                    <br>
                                                @if($other_mascot == null)
                                                    <input type="checkbox" name="otroC" id="otroC" value="" onclick="mascotC(this.checked);">
                                                @else
                                                    <input type="checkbox" name="otroC" id="otroC" value="" onclick="mascotC(this.checked);" checked>
                                                @endif
                                                <label class="control-label" for="name"> Otros</label>
                                                    <br>
                                                @if($other_mascot == null)
                                                    <input type="text" name="mascotaC" id="mascotaC"  disabled  placeholder="Especifique">
                                                @else
                                                    <input type="text" name="mascotaC" id="mascotaC" placeholder="Especifique" value="{{$other_mascot->specification}}" >
                                                @endif
                                        </div>

                                        <div class="col-md-3">
                                            <h5 style="color:black">Método de pago</h5>
                                            <select name="pay_mC" id="pay_mC" class="applicantsList-single form-control">
                                                <option value="{{$order_sr->metodo}}" selected="true">
                                                    {{$order_sr->met_name}}
                                                </option>
                                                @foreach ($payment_method as $i)
                                                    @if ($order_sr->metodo != $i->id)
                                                        <option value="{{$i->id}}">
                                                            {{ (in_array($i, old('payment_method', []))) }} {{ $i->name }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <h5 style="color:black">Forma de pago</h5>
                                            <select name="pay_fC" id="pay_fC" class="applicantsList-single form-control">
                                                <option value="{{$order_sr->way}}" selected="true">
                                                    {{$order_sr->way_name}}
                                                </option>
                                                @foreach ($payment_way as $w)
                                                    @if ($order_sr->way != $w->id)
                                                        <option value="{{$w->id}}">
                                                            {{ (in_array($w, old('payment_way', []))) }} {{ $w->name }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-12">

                                        <h5 style="color:black">Datos de facturación</h5>
                                        <input type="checkbox" name="habilitarC" id="habilitarC" onclick="habilitarC(this.checked);" @if($order_sr->billing_mode == 2) checked @endif> <span class="text-primary">Facturar orden de servicio</span>

                                        <div class="row form-group container-fluid" id="divDataBilling">

                                            <div class="col-md-3">
                                                <label for="rfcC" class="control-label">{{$profile_job_center->rfc_country}}*: </label>
                                                <input type="text" class="form-control" name="rfcC" id="rfcC" value="{{$order_sr->rfc}}">
                                            </div>

                                            <div class="col-md-3">
                                                <label for="socialC" class="control-label">Razón Social*: </label>
                                                <input type="text" class="form-control" name="socialC" id="socialC" value="{{$order_sr->billing}}" >
                                            </div>

                                            <div class="col-md-3">
                                                <label for="address_rfcC" class="control-label">Domicilio*: </label>
                                                <input type="text" class="form-control" name="address_rfcC" id="address_rfcC" value="{{$order_sr->tax_residence}}">
                                            </div>

                                            <div class="col-md-3">
                                                <label for="e_mailC" class="control-label">Correo electrónico*: </label>
                                                <input type="text" class="form-control" name="e_mailC" id="e_mailC" value="{{$order_sr->e_mail}}">
                                            </div>

                                            <div class="col-md-3" @if(Auth::user()->companie != 37) style="display: none;" @endif>
                                                <label for="typeCfdiCustomerMainNew">Tipo CFDI:</label>
                                                <select name="typeCfdiCustomerMainNewEdit" id="typeCfdiCustomerMainNewEdit" class="form-control">
                                                    @if($order_sr->cfdi_type == null || $order_sr->cfdi_type == "")
                                                        <option value="0" selected disabled>Seleccionar Uso de la Factura</option>
                                                    @endif
                                                    @foreach($typesCfdi as $type)
                                                        <option @if($order_sr->cfdi_type == $type->Value) selected @endif value="{{ $type->Value }}">{{ $type->Value }} - {{ $type->Name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3" @if(Auth::user()->companie != 37) style="display: none;" @endif>
                                                <label for="methodPayCustomerMain">Método de Pago:</label>
                                                <select name="methodPayCustomerMainEdit" id="methodPayCustomerMainEdit" class="form-control">
                                                    @if($order_sr->payment_method_sat == null || $order_sr->payment_method_sat == "")
                                                        <option value="0" selected disabled>Selecciona un método de pago</option>
                                                    @endif
                                                    @foreach($paymentMethodsSat as $method)
                                                        <option @if($order_sr->payment_method_sat == $method->Value) selected @endif value="{{ $method->Value }}">{{ $method->Value }} - {{ $method->Name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3" @if(Auth::user()->companie != 37) style="display: none;" @endif>
                                                <div class="">
                                                    <label for="wayPayCustomerMain">Forma de Pago:</label>
                                                    <select name="wayPayCustomerMainEdit" id="wayPayCustomerMainEdit" class="form-control">
                                                        @if($order_sr->payment_way_sat == null || $order_sr->payment_way_sat == "")
                                                            <option value="0" selected disabled>Selecciona una forma de pago</option>
                                                        @endif
                                                        @foreach($paymentFormsSat as $form)
                                                            <option @if($order_sr->payment_way_sat == $form->Value) selected @endif value="{{ $form->Value }}">{{ $form->Value }} - {{ $form->Name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-12 text-right" @if(Entrust::can('No Mostrar Precio (Agenda)')) style="display: none" @endif>

                                        <div class="row">
                                            <input type="hidden" id="symbolCountry" value="{{ $symbol_country }}">
                                            <div class="col-sm-12 formulario__grupo">
                                                <br>
                                                <label for="" class="control-label" style="font-size: 20px;">Subtotal: <b style="font-size: 20px;">{{ $symbol_country }}@convert($order_sr->subtotal)</b></label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 formulario__grupo">
                                                <input type="hidden" name="i_sumC" id="i_sumC" value="{{$iva}}">
                                                <label for="" class="control-label" style="font-size: 20px;">
                                                    @if($labels['iva'] != null) {{ $labels['iva']->label_spanish }}
                                                    @else IVA
                                                    @endif: <b id="label-iva">{{ $symbol_country }}@if($order_sr->billing_mode == 2)@convert($iva) @else @convert(0) @endif</b></label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 formulario__grupo">
                                                <label for="" class="control-label" style="font-size: 20px;">Descuento: <b>{{ $symbol_country }}@convert($discount)</b></label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 formulario__grupo">
                                                <input type="hidden" name="i_tot" id="i_tot" value="{{ $order_sr->t }}">
                                                <input type="hidden" name="p_tot" id="p_tot" value="{{ $order_sr->subtotal }}">
                                                <label for="" class="control-label" style="font-size: 20px;"><b class="text-primary">Total: </b><b id="label-total">{{ $symbol_country }}@convert($order_sr->t)</b></label>
                                                <br>
                                                <label for="">Editar:</label>
                                                <input type="text" name="totalC" id="totalC" style="width:50px" value="{{$order_sr->t}}"><br><br>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <a href="{{ route('edit_quotation', \Vinkla\Hashids\Facades\Hashids::encode($order_sr->quotation))}} "class="btn btn-secondary-outline btn-sm">
                                                <span class="glyphicon glyphicon-pencil"></span> Editar cotización
                                            </a>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary" id="saveOrderC" name="saveOrderC">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL DE EDITAR SERVICIO -->
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/serviceEditSchedule.js') }}"></script>
    <script>
        $("#scheduleD").modal();
        $("#scheduleD").on('hidden.bs.modal', function() {
            let prevUrl = document.referrer;
            if (prevUrl === 'https://pestwareapp.com/index_register') {
                window.location.href = '{{route("index_register")}}'; //using a named route
            }else {
                window.location.href = '{{route("calendario")}}'; //using a named route
            }
        });
    </script>
    <script>
        const symbolCountry = document.getElementById('symbolCountry').value;
        //Habilitar campos de facturación
        function habilitarC(value){
            if(value == true){
                let suma, iva,tot = 0;
                iva = document.getElementById("i_sumC").value;
                tot = document.getElementById("p_tot").value;
                console.log(tot)
                suma = parseInt(iva) + parseInt(tot);
                const labelIva = `${symbolCountry}${iva}`;
                const labelTotal = `${symbolCountry}${suma}`;
                $('#label-iva').html(labelIva);
                $('#label-total').html(labelTotal);
                document.getElementById('totalC').value = suma;
                document.getElementById('divDataBilling').style.display = 'block';
            }else if(value == false){
                let resta, i, t = 0;
                i = document.getElementById("i_sumC").value;
                t = document.getElementById("i_tot").value;
                resta = parseInt(t) - parseInt(i);
                const labelTotal = `${symbolCountry}${t}`;
                $('#label-iva').html(`${symbolCountry}0.00`);
                $('#label-total').html(labelTotal);
                document.getElementById('totalC').value = t;
                document.getElementById('divDataBilling').style.display = 'none';
            }
        }
    </script>
    <script>
        //Habilitar los campos de otro en mascotas y enfermedades
        function mascotC(value){
            if (value == true){
                document.getElementById("mascotaC").disabled = false;
                $('#mascotaC').prop("required",true);
            }else if(value == false){
                document.getElementById("mascotaC").disabled = true;
                document.getElementById("mascotaC").value = "";
                $('#mascotaC').removeAttr("required");
            }
        }
        function specificC(value){
            if(value == true){
                document.getElementById("specificC").disabled = false;
            }else if(value == false){
                document.getElementById("specificC").disabled = true;
                document.getElementById("specificC").value = "";
            }
        }
    </script>
@endsection

