<!-- INICIA MODAL PARA SELECCIONAR ENVIO DE PDF ORDER -->
<div class="modal fade" id="selectsendorder" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
        <h3>Enviar PDF por:</h3>
        <br>
        <div class="row">
          <div class="col-md-6">
            <a href=""><i class="fa fa-envelope-o fa-5x" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-6">
            <a href=""><i class="fa fa-whatsapp fa-5x" aria-hidden="true" style="color: green;"></i></a>
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
<!-- TERMINA MODAL PARA SELECCIONAR ENVIO DE PDF ORDER -->