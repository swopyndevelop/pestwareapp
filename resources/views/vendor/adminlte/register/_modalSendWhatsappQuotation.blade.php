<!-- INICIA MODAL PARA ENVIAR COTIZACIÓN WHATSAPP -->
<div class="modal fade" id="sendWhatsappQuotation" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body text-center">
                <h4>Enviar cotización por WhatsApp:</h4>
                <h5 for="email" style="font-weight: bold;">Cliente:</h5>
                <input type="text" name="cellphoneClientQuotation" id="cellphoneClientQuotation" class="form-control">
                <h5 for="email" style="font-weight: bold;">Principal:</h5>
                <input type="text" name="cellphoneMainQuotation" id="cellphoneMainQuotation" class="form-control">
                <h5 for="email" style="font-weight: bold;">Otro:</h5>
                <input type="text" name="cellphoneOtherQuotation" id="cellphoneOtherQuotation" class="form-control">
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-primary btn-lg" type="button" id="sendWhatsappQuotationSave"
                    name="sendWhatsappQuotationSave">Enviar</button>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR COTIZACIÓN WHATSAPP -->