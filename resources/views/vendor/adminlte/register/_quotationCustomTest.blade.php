<!-- START MODAL CUSTOM QUOTE -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalQuotationCustomTest" role="dialog" aria-labelledby="ModalQuotationCustomTest" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 95% !important; height: 95% !important;">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Cotización Personalizada <b>v3</b></h4>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="control-label" for="customerNameCustom">Nombre del
                                                        cliente*: </label>
                                                    <input type="text" class="form-control"
                                                           id="customerNameCustom" name="customerNameCustom"
                                                           placeholder="Ejemplo: Juan Manuel">
                                                    <div id="customerNameListCustom"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div>
                                                    <label class="control-label" for="cellphoneCustom">Celular del Cliente: </label>
                                                    <input type="number" class="form-control"
                                                           id="cellphoneCustom" name="cellphoneCustom"
                                                           placeholder="Ejemplo: 4491234567">
                                                    <div id="numberListCustom"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"
                                                       for="establishmentNameCustom">Contacto Principal: </label>
                                                <input type="text" class="form-control"
                                                       id="establishmentNameCustom" name="establishmentNameCustom"
                                                       placeholder="PestWare App">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="cellphoneMainCustom">Celular del Contacto Principal: </label>
                                                <input type="number" class="form-control"
                                                       id="cellphoneMainCustom" name="cellphoneMainCustom"
                                                       placeholder="Ejemplo: 4491234567">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="emailCustomerCustom">Correo
                                                    electrónico: </label>
                                                <input type="text" class="form-control"
                                                       id="emailCustomerCustom" name="emailCustomerCustom"
                                                       placeholder="Ejemplo: correo@example.com">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="colonyCustom">
                                                    @if($labels['colony'] != null) {{ $labels['colony']->label_spanish }}*: @else Colonia: @endif
                                                </label>
                                                <input type="text" class="form-control"
                                                       id="colonyCustom" name="colonyCustom"
                                                       placeholder="Ejemplo: La México">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"
                                                       for="municipalityCustom">Municipio: </label>
                                                <input type="text" class="form-control"
                                                       id="municipalityCustom" name="municipalityCustom"
                                                       placeholder="Ejemplo: Aguascalientes">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label"
                                                       for="municipalityCustom">Descripción del servicio: </label>
                                                <select id="descriptions" class="form-control">
                                                    <option value="0" selected>Personalizada</option>
                                                    @foreach($descriptionsForCustomQuotes as $description)
                                                        <option value="{{ $description->id }}">{{ $description->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label" for="sourceOriginCustom">Acciones:</label><br>
                                                <button class="btn btn-secondary btn-sm" id="addDescription"><i class="fa fa-commenting-o" aria-hidden="true"></i> Guardar Descripción</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="establishmentCustom">Tipo de
                                                    servicio*: </label>
                                                <select name="establishmentCustom" id="establishmentCustom"
                                                        class="form-control"
                                                        style="width: 100%; font-weight: bold;">
                                                    <option value="0" disabled selected>Seleccione un tipo de
                                                        servicio
                                                    </option>
                                                    @foreach($establishments as $establishment)
                                                        <option value="{{ $establishment->id }}">{{ $establishment->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label" for="plaguesCustom">Tipo de plaga*: </label>
                                                <select name="plaguesCustom[]" id="plaguesCustom" class="form-control"
                                                        style="width: 100%; font-weight: bold;" multiple="multiple">
                                                    @foreach($plagues as $plague)
                                                        <option value="{{ $plague->id }},{{ $plague->plague_key }}">
                                                            {{ $plague->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            @if(\Illuminate\Support\Facades\Auth::user()->companie == 333)
                                            <div class="col-sm-6">
                                                @else <div class="col-sm-12">
                                                    @endif
                                                <label class="control-label" for="sourceOriginCustom">Fuente de
                                                    origen*: </label>
                                                <select name="sourceOriginCustom" id="sourceOriginCustom"
                                                        class="form-control"
                                                        style="width: 100%; font-weight: bold;">
                                                    <option value="0" disabled selected>Seleccione una opción
                                                    </option>
                                                    @foreach($sources as $source)
                                                        <option value="{{ $source->id }}">{{ $source->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-6" @if(Auth::user()->companie != 333) style="display: none;" @endif>
                                                <label class="control-label" for="pesticide">Plaguicidas: </label>
                                                <input type="text" class="formulario__input form-control"
                                                       id="pesticideCustom" name="pesticideCustom"
                                                       placeholder="Ejemplo: Biothrine">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label" for="messengerCustom">Messenger: </label>
                                                <input type="text" class="form-control" id="messengerCustom">
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label" for="inspectionSelect">Inspección: </label>
                                                <select name="inspectionSelect" id="inspectionSelect"
                                                        class="form-control select__input"
                                                        style="width: 100%; font-weight: bold;">
                                                    <option value="0" disabled selected>Sin inspección</option>
                                                    @foreach($inspections as $inspection)
                                                        <option value="{{ $inspection->id_quotation }}">{{ $inspection->id_service_order }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="descriptionTest" cols="30" rows="3" placeholder="Descripción del servicio"></textarea>
                                        <br>
                                    </div>
                                    <!-- Start New version -->
                                    <div class="row container-fluid">
                                        <div class="col-md-2">
                                            <label class="lb-custom-qt" for="type">Tipo de Concepto <i class="fa fa-info-circle"
                                                                                  data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="SERVICIO: Genera orden de servicio y se programa en la agenda. CONTABLE: Solo afecta el monto de la cotización y no se programa en la agenda. SUCURSAL: Nos permite ingresar más de un domicilio asociado a un mismo cliente, ideal para clientes multisucursal.."
                                                                                  aria-hidden="true"></i></label>
                                            <select id="type" class="form-control">
                                                <option value="1">Servicio</option>
                                                <option value="2">Contable</option>
                                                <option value="3">Sucursal</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="lb-custom-qt" for="conceptTest">Nombre del Concepto <i class="fa fa-info-circle"
                                                                                            data-toggle="tooltip"
                                                                                            data-placement="top"
                                                                                            title="SERVICIO: Fumigación, Control de Roedores, etc. CONTABLE: IVA, Descuento 10%, Insumos, etc. SUCURSAL: Pizzería Las Palmas, etc."
                                                                                            aria-hidden="true"></i></label>
                                            <input type="text" class="form-control" id="conceptTest" placeholder="Servicio de Fumigación">
                                        </div>
                                        <div class="col-md-1">
                                            <label class="lb-custom-qt" for="quantityTest">Cantidad <i class="fa fa-info-circle"
                                                                                  data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="Cantidad de ordenes de servicio o sucursales a programar del mismo concepto."
                                                                                  aria-hidden="true"></i></label>
                                            <input type="text" class="form-control" id="quantityTest" placeholder="1">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="lb-custom-qt" for="typeService">Frecuencia de Servicio <i class="fa fa-info-circle"
                                                                                         data-toggle="tooltip"
                                                                                         data-placement="top"
                                                                                         title="La frecuencia nos indicará como será la programción del plazo contratado."
                                                                                         aria-hidden="true"></i></label>
                                            <select id="typeService" class="form-control">
                                                <option value="1">Mensual</option>
                                                <option value="2">Bimestral</option>
                                                <option value="3">Trimestral</option>
                                                <option value="4">Cuatrimestral</option>
                                                <option value="5">Semestral</option>
                                                <option value="7">Único</option>
                                                <option value="6">Semanal</option>
                                                <option value="8">Personalizado</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="lb-custom-qt" for="monthTest">Frec. / período <i class="fa fa-info-circle"
                                                                                      data-toggle="tooltip"
                                                                                      data-placement="top"
                                                                                      title="Aplica únicamente para conceptos de tipo SERVICIO y SUCURSAL, a excepción de SEMANAL. Indica el número de servicios o visitas que se realizarán en el período seleccionado, por ejemplo: Si se ingresa 2 y se elige mensual, serán dos servicios por mes."
                                                                                      aria-hidden="true"></i></label>
                                            <input type="text" class="form-control" id="monthTest" placeholder="1">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="lb-custom-qt" for="contractTest">Plazo en contrato en meses <i class="fa fa-info-circle"
                                                                                                    data-toggle="tooltip"
                                                                                                    data-placement="top"
                                                                                                    title="Número de meses que que se van a programar o contrato establecido, por ejemplo: 12 (anual)."
                                                                                                    aria-hidden="true"></i></label>
                                            <input type="text" class="form-control" id="contractTest" placeholder="12">
                                        </div>
                                        <div class="col-md-1">
                                            <label class="lb-custom-qt" for="priceTest">Precio unitario <i class="fa fa-info-circle"
                                                                                      data-toggle="tooltip"
                                                                                      data-placement="top"
                                                                                      title="Costo individual por cada orden de servicio o visita del período seleccionado, a excepción de SEMANAL en este caso es costo mensual."
                                                                                      aria-hidden="true"></i></label>
                                            <input type="text" class="form-control" id="priceTest" placeholder="1400">
                                        </div>
                                        <div class="col-md-1" id="actionsHeader">
                                            <label class="lb-custom-qt" for="addTest">Acciones <i class="fa fa-info-circle"
                                                                             data-toggle="tooltip"
                                                                             data-placement="top"
                                                                             title="Puedes ingresar más de un concepto, usando los diferentes tipos de concepto y servicio para generar por ejemplo una póliza anual."
                                                                             aria-hidden="true"></i></label><br>
                                            <button class="btn btn-primary" id="addTest"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-responsive-md table-sm table-bordered table-hover" id="customTable">
                                            <thead>
                                                <tr>
                                                    <th>Tipo</th>
                                                    <th>Concepto</th>
                                                    <th>Cantidad</th>
                                                    <th>Frecuencia de Servicio</th>
                                                    <th>Frec. de aplicación / período</th>
                                                    <th>Plazo en contrato en meses</th>
                                                    <th>Precio unitario</th>
                                                    <th>Subtotal</th>
                                                    <th>Total</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- End New version -->

                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">Subtotal: <b id="subtotalTest"></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <label class="control-label text-primary" for="total" style="font-size: 1.5em;">Total: <b id="totalTest"></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelEditProfileDiv">Cancelar</button>
                                        <button class="btn btn-primary" type="button" id="saveQuotationCustomNewTest">Guardar</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('.vendor.adminlte.register._modalNewDescriptionCustomQuote')
<!-- END MODAL CUSTOM QUOTE -->
