<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="scheduleGoogleMaps" role="dialog" aria-labelledby="scheduleManual"
         tabindex="-1">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document">

                    <div class="content modal-content">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-primary" id="modalTitle">
                                    Programación Rutas Optimizadas Google Maps</h4>
                            </div>

                            <div class="box-body">
                                <div class="row">

                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="row">
                                        <button class="btn btn-primary" type="button" id="SaveDenyC"
                                                name="SaveDenyC">Programar Servicios
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE COTIZACION -->
