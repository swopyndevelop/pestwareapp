<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="scheduleIntelligent" role="dialog" aria-labelledby="scheduleIntelligent"
         tabindex="-1">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document" style="width: 90% !important; height: 90% !important;">

                    <div class="content modal-content">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-primary" id="modalTitle">
                                    Programación Manual <b>{{ $quotationKey }}</b></h4>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-responsive-md table-sm table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Concepto</th>
                                                <th>Cantidad</th>
                                                <th>Tipo de Servicio</th>
                                                <th>Frecuencia por período</th>
                                                <th>Plazo en contrato en meses</th>
                                                <th>Precio unitario</th>
                                                <th>Opciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($concepts as $concept)
                                                <tr class="text-center">
                                                    <td class="mobile-td" style="text-align: right">{{ $concept->concept }}</td>
                                                    <td class="mobile-td" style="text-align: right">{{ $concept->quantity }}</td>
                                                    <td class="mobile-td" style="text-align: right">{{ $concept->type_service }}</td>
                                                    <td class="mobile-td" style="text-align: right">{{ $concept->frecuency_month }}</td>
                                                    <td class="mobile-td" style="text-align: right">{{ $concept->term_month }}</td>
                                                    <td class="mobile-td" style="text-align: right">${{ $concept->unit_price }}</td>
                                                    <td class="mobile-td" style="text-align: right">
                                                        <button class="btn btn-sm btn-secondary-outline scheduleConcept"
                                                                data-concept="{{ $concept->concept }}"
                                                                data-customerId="{{ $customerId }}"
                                                                data-id="{{ $concept->id }}"
                                                                data-frecuency="{{ $concept->frecuency_month }}"
                                                                data-typeService="{{ $concept->id_type_service }}"
                                                                style="padding: 2px 10px;"
                                                                id="scheduleConcept-{{ \Illuminate\Support\Str::slug($concept->concept) }}"
                                                        >Programar</button>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>

                                        <div class="col-md-6">
                                            <label class="control-label" style="font-weight: bold;">Técnico Responsable: </label>
                                            <div @if(Auth::user()->companie == 37 || Auth::user()->companie == 371 || Auth::user()->companie == 338 || Auth::user()->companie == 451
                                                    || Auth::user()->companie == 652) class="input-group" @endif>
                                                <select name="techniciansDate" id="techniciansDate"
                                                        class="applicantsList-single form-control"
                                                        style="width: 100%; font-weight: bold;">
                                                    @foreach($employees as $employee)
                                                        <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="input-group-btn">
                                                    @if(Auth::user()->companie == 37 || Auth::user()->companie == 371 || Auth::user()->companie == 338 || Auth::user()->companie == 451 ||
                                                     Auth::user()->companie == 652)
                                                    <button @if (Auth::user()->id_plan == 3) id="showSelectAdditionalTechniciansCustom"
                                                            @else title="Plan Diamante"
                                                            @endif
                                                            data-toggle="tooltip" data-placement="right" class="btn btn-primary">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row col-md-6" id="rowSelectAdditionalTechniciansCustom" style="display: none;">
                                            <div class="col-md-12">
                                                <label class="control-label" style="font-weight: bold;">Técnicos Auxiliares: </label>
                                                <div class="input-group">
                                                    <select name="techniciansAuxiliariesCustom[]" id="techniciansAuxiliariesCustom" class="form-control" style="width: 100%; font-weight: bold;" multiple="multiple">
                                                        @foreach($employees as $employee)
                                                            <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-danger" id="hideSelectAdditionalTechniciansCustom"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <label class="control-label" style="font-weight: bold;">Comentarios al Técnico: </label>
                                            <textarea name="commentsTechnicians" id="commentsTechnicians" cols="15" rows="5" class="form-control"></textarea>
                                        </div>

                                    </div>
                                </div>
                                <div id="container-schedule">

                                </div>
                                <hr>
                                <div id="container-schedule-concepts">

                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="row">
                                        <a href="{{ route('calendario') }}" class="btn btn-primary" type="button" id="saveTotalSchedule">Ir a la Agenda</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE COTIZACION -->
