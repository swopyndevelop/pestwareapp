<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="scheduleImportExcel" role="dialog" aria-labelledby="scheduleManual"
         tabindex="-1">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document">

                    <div class="content modal-content">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-primary" id="modalTitle">
                                    Programación Importar Con Excel</h4>
                            </div>

                            <div class="box-body">
                                <form action="{{ route('schedule_excel_template_import') }}" method="POST" id="form" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('POST') }}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <h5 class="modal-title">Seleccione la lista de servicios en formato excel</h5>
                                                <br>
                                                <input type="hidden" value="{{ $quotationId }}" name="quotationId">
                                                <input type="file" class="form-control" id="fileImportSchedules" name="fileImportSchedules" required="required"
                                                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                                <label for="techniciansExcel">Técnico del Servicio:</label>
                                                <select class="form-control" name="techniciansExcel" id="techniciansExcel" style="width: 40%;">
                                                    @foreach($employees as $employee)
                                                        <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" id="downloadFileBranches" title="Descargar" class="btn btn-secondary btn-sm"><strong>Descargar Plantilla</strong></button>
                                        <button type="submit" id="importFileSchedules" title="Importar" class="btn btn-primary btn-sm"><strong>Importar Servicios</strong></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE COTIZACION -->
