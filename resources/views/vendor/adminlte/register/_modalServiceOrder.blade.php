@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- INICIA MODAL PARA MOSTRAR FICHA DEL PRODUCTO -->
<div class="modal fade" id="verOrden">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h3 style="text-align: center; color: black;">Orden de Servicio</h3>
				<div class="row">
					<div class="col-md-7">
						@if($imagen->pdf_logo == null)
						<img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="200px" height="200px">
						@else
						<img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="200px" height="200px">
						@endif
						<p><span style="color:black;font-size:9pt">Licencia No.{{$imagen->licence}} | {{$imagen->phone}} | Facebook: {{$imagen->facebook}}</span></p>
						<p><span style="font-size:12pt;color:black;"> Atención a {{$order->cliente}} </span>
							@if($order->empresa != null)
							<br><span style="font-size:10.5pt;font-weight:bold;color:black;">{{$order->empresa}}</span>
							@endif
							<br><span style="font-size:8pt;color:black;">{{$order->address}} #{{$order->address_number}},
							{{$order->colony}}, {{$order->municipality}}, {{$order->state}}</span>
							<br><span style="font-size:8pt;color:black;"> Tel. {{$order->cellphone}} </span><br>
							@if($order->email != null)
								<span style="font-size:8pt;color:black;"> {{$order->email}}</span></p>
							@endif
					</div>
					<div class="col-md-5">
						<p style="text-align: right;">
							<span style="color:black;font-weight:bold;font-size:11pt">No. SERVICIO: </span>  
							<span style="color:red;font-weight:bold;font-size:11pt">&nbsp;&nbsp;&nbsp;{{$order->id_service_order}}</span>
						<br><span style="color:black;font-weight:bold;font-size:11pt">TIPO DE SERVICIO: </span>  
							<span style="color:black;font-size:11pt">{{$order->establecimiento}}</span>
						<br><span style="color:black;font-weight:bold;font-size:11pt">IMPORTE: </span> 
							<span style="color:black;font-weight:bold;font-size:11pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$symbol_country . $order->total_service}}</span>
						<br><span style="color:black;font-weight:bold;font-size:11pt">FECHA: </span>
							<span style="color:black;font-size:11pt">&nbsp;&nbsp;&nbsp;<?php echo date("d/m/y",strtotime($order->initial_date))?></span>
						<br><span style="color:black;font-weight:bold;font-size:11pt">HORA DE ENTRADA: </span>
							<span style="color:black;font-size:11pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date("H:i",strtotime($order->start_event))?></span>
						<br><span style="color:black;font-weight:bold;font-size:11pt">HORA DE SALIDA: </span>
							<span style="color:black;font-size:11pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date("H:i",strtotime($order->final_event))?></span>
						<br><span style="color:black;font-weight:bold;font-size:11pt">TÉCNICO APLICADOR: </span>
						<span style="color:black;font-size:11pt">{{$order->nombre}}</span>
						<br><span style="color:black;font-weight:bold;font-size:11pt">AGENTE: </span>
						<span style="color:black;font-size:11pt">{{$order->agente}}</span>
					</p>  
					</div>
				</div>
				<div class="row">
					<div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
						<span style="font-size:11pt;font-weight:bold;color:black">DETALLE DEL SERVICIO </span>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">PLAGAS:</span>
							<span style="font-size:11pt;color:black">
								@foreach($plague2 as $item) {{$item->name}}, @endforeach
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">CONDICIONES DE QUIEN HABITA:</span>
							<span style="color:black">@foreach($habits as $habit) {{ $habit->condition }},@endforeach
                        		@foreach($inhabits as $inhabit) {{ $inhabit->name }},@endforeach
                        		@foreach($mascots as $mascot) {{ $mascot->name }},@endforeach </span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
							<span style="font-size:11pt;">
								{{ $order->observations }}
							</span>
						<p>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
						<span style="font-size:11pt;font-weight:bold;color:black">INSPECCION DEL LUGAR </span>
					</div>
					<div class="col-lg-11" style="border-bottom: 1px solid;border-color:black;margin-left:45px;margin-right:40px;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">PLAGAS:</span>
							<span style="font-size:11pt;color:black">
								@foreach($plagasGrade as $pg) {{ $pg->plaga }}: {{ $pg->infestacion }},@endforeach
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">AREAS DE ANIDAMIENTO:</span>
							<span style="font-size:11pt;color:black">
								{{ $inspection->nesting_areas }}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
							<span style="font-size:11pt;color:black">
								{{ $inspection->commentary }}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="border-bottom: 1px solid;border-color:black;margin-left:45px;margin-right:40px;" id="selector1">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS:</span>
							@foreach($imagesInspection as $imagIns)
								<span class="item" data-src="{{ $imagIns->url_s3 }}">
									<img src="{{ $imagIns->url_s3 }}" height="100" width="100">
								</span>
						 	@endforeach
						</p>
					</div>
					
				</div>
				<br><br>
				<div class="row">
					<div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
						<span style="font-size:11pt;font-weight:bold;color:black">CONDICIONES DEL LUGAR </span>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">CUMPLIO CON LAS INDICACIONES:</span>
							<span style="font-size:11pt;color:black">
								@if($condition->indications == 1)Si @elseif($condition->indications == 2)No @else Una parte @endif
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">ORDEN Y LIMPIEZA:</span>
							<span style="font-size:11pt;color:black">
								@foreach($order_cleanings as $oc) {{ $oc->cleaning }}, @endforeach							
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">ACCESOS RESTRINGIDOS:</span>
							<span style="font-size:11pt;color:black">
								{{ $condition->restricted_access }}							
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
							<span style="font-size:11pt;color:black">
								{{ $condition->commentary }}						
							</span>
						</p>
					</div>
					<div class="col-lg-11"  style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;" id="selector2">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS:</span>
							@foreach($imagesCondition as $imagCon)
								<span class="item" data-src="{{ $imagCon->url_s3 }}">
							    	<img src="{{ $imagCon->url_s3 }}" height="100" width="100">
								</span>
						 	@endforeach
						</p>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
						<span style="font-size:11pt;font-weight:bold;color:black">CONTROL DE PLAGAS</span>
					</div>
					@foreach($plague_controls as $pc)
						<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
							<br>
							<p><span style="font-size:11pt;font-weight:bold;color:black">AREA A CONTROLAR:</span>
								<span style="font-size:11pt;color:black">
									{{ $pc->control_areas }}								
								</span>
							</p>							
						</div>
						<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
							<br>
							<p><span style="font-size:11pt;font-weight:bold;color:black">METODOS DE APLICACION:</span>
								<span style="font-size:11pt;color:black">
									{{ $pc->methods }}								
								</span>
							</p>
						</div>
						<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
							<br>
							<p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
								<span style="font-size:11pt;color:black">
									{{ $pc->commentary }}							
								</span>
							</p>							
						</div>
						<br>
						<div class="col-lg-11" style="margin-right:40px;margin-left:45px;font-size:11pt">
							<table class="table table-bordered table-responsive">
								<thead>
									<tr>
										<td style="color:black;font-weight:bold">PLAGUICIDA APLICADO</td>
										<td style="color:black;font-weight:bold">DOSIS</td>
										<td style="color:black;font-weight:bold">CANTIDAD</td>
									</tr>
								</thead>
								<tbody>
									@foreach($pc->plague_controls as $pcp)
									<tr>
										<td style="font-size:11">{{$pcp->product}}</td>
										<td style="font-size:11">{{$pcp->dose}}</td>
										<td style="font-size:11">{{$pcp->quantity}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;" id="selector3">
							<br>
							<p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS:</span>
									@foreach($pc->imagesCtrlPlague as $imagCtrl)
										<span class="item" data-src="{{ $imagCtrl->url_s3 }}">
								    		<img src="{{ $imagCtrl->url_s3 }}" height="100" width="100">
										</span>
							 		@endforeach
								</p>
						</div>
					@endforeach
				</div>
				<br><br>
				<div class="row">
					<div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
						<span style="font-size:11pt;font-weight:bold;color:black">PAGO DEL SERVICIO</span>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">IMPORTE:</span>
							<span style="font-size:11pt;color:black">
								{{$symbol_country . $order->total_service}}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">METODO DE PAGO:</span>
							<span style="font-size:11pt;color:black">
								{{ $cashe->payment_method }}						
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">TIPO DE PAGO:</span>
							<span style="font-size:11pt;color:black">
								{{ $cashe->payment_type }}							
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">IMPORTE RECIBIDO:</span>
							<span style="font-size:11pt;color:black">
								{{$symbol_country . $cashe->amount_received }}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
							<span style="font-size:11pt;color:black">
								{{ $cashe->commentary }}
								@if($cashe->payment == 1)
									<strong style="color: #b71c1c">*El cliente no pago.</strong>
								@endif
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;" id="selector4">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS:</span>
							@foreach($imagesCashe as $imageCashe)
								<span class="item" data-src="{{ $imageCashe->url_s3 }}">
									<img src="{{ $imageCashe->url_s3 }}" height="100" width="100">
								</span>
						 	@endforeach
						</p>					
					</div>
                    <div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;" id="selector5">
                        <br>
                        <p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS MONITOREO:</span>
							@if($isInspections != null)
							@foreach($imagesMonitoring as $imageMonitoring)
								<span class="item" data-src="{{ $imageMonitoring->url_s3 }}">
									<img src="{{ $imageMonitoring->url_s3 }}" height="100" width="100">
								</span>
							@endforeach
							@endif
                        </p>
                    </div>
				</div>
				<br><br>
				<div class="row" align="center">
					<h4 style="margin-left: 15px; margin-top: 0px !important; color:black; text-align: center;">FIRMA DEL CLIENTE</h4>
					<div class="col-md-12">
						@if($firm)
						<img src="{{ $firmUrl }}" height="400" width="300" class="img-thumbnail">
						@endif
						<br>
						<label style="font-weight: bold; color: black;">{{ $order->cliente }}</label>
						@if($firm)
							@if($firm->other_name != null)<br><span style="text-align:center;font-size:9pt;font-weight:bold">{{$firm->other_name}}</span><br>@endif
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR FICHA DEL PRODUCTO-->
@endsection

@section('personal-js')
<script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js"></script> 
  <script>
    //$(".plagueList-single").select2();
    $("#verOrden").modal();
    $("#verOrden").on('hidden.bs.modal', function() {
		let prevUrl = document.referrer;
		if (prevUrl === 'https://pestwareapp.com/index_register') {
			window.location.href = '{{route("index_register")}}'; //using a named route
		}else {
			window.location.href = '{{route("calendario")}}'; //using a named route
		}
    });

    $('#selector1').lightGallery({
    	selector: '.item'
	});

	$('#selector2').lightGallery({
    	selector: '.item'
	});

	$('#selector3').lightGallery({
    	selector: '.item'
	});

	$('#selector4').lightGallery({
    	selector: '.item'
	});
	$('#selector5').lightGallery({
		selector: '.item'
	});
  </script>
@endsection