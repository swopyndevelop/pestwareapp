@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="scheduleC" role="dialog" aria-labelledby="cancelModal" tabindex="-1" >
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h3 class="modal-title text-center col-lg-12" id="modalTitle">Servicio cancelado</h3>
                </div>
              </div>

              <div class="box-body">
                <div class="row container-fluid">

                  <div class="form-group row col-sm-12">
                    <input type="hidden" id="idOrder" value="{{$order->id}}">
                    <label class="control-label col-sm-4 text-center" for="company">Motivo:</label>
                    <div class="col-sm-8">
                      <select name="reason" id="reason" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;">
                        <option value="" disabled selected>Seleccione el motivo</option>
                        <option value="Precio Alto">Precio Alto</option>
                        <option value="No le gusto el servicio" >No le gusto el servicio</option>
                        <option value="No funciono el tratamiento">No funciono el tratamiento</option>
                        <option value="Otro">Otro</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row col-sm-12">
                    <label class="control-label col-sm-4 text-center" for="company">Comentarios del cliente:</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="commentary" id="commentary" cols="30" rows="3" placeholder="Descripción del servicio"></textarea>
                    </div>
                  </div>

                </div>
              </div>

              <div class="modal-footer">
                <div class="text-center">
                  <button class="btn btn-primary btn-sm" type="button" id="Delete" name="Delete">Guardar</button>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
  <script type="text/javascript" src="{{ URL::asset('js/build/serviceCancel.js') }}"></script>
  <script>
  //$(".plagueList-single").select2();
  $("#scheduleC").modal();
  $("#scheduleC").on('hidden.bs.modal', function() {
    let prevUrl = document.referrer;
		if (prevUrl === 'https://pestwareapp.com/index_register') {
			window.location.href = '{{route("index_register")}}'; //using a named route
		}else {
			window.location.href = '{{route("calendario")}}'; //using a named route
		}
  });
</script>
@endsection
