

<!-- START MODAL DE FACTURAR SERVICIO -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalServiceInvoice" role="dialog" aria-labelledby="modalServiceInvoice" tabindex="-1">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-dialog" role="document" style="width: 70% !important;">

                    <div class="content modal-content" >
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-primary" id="modalTitle">Generar Factura de Servicio <b class="text-danger" id="orderFolioInvoice"></b></h4>
                            </div>

                            <div class="box-body">
                                <div class="row container-fluid">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12 pull-right" style="text-align: right !important;">
                                                <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">Subtotal: <b id="subtotalTest"></b></label>
                                            </div>
                                        </div>
                                        <div class="row" id="divIvaTest">
                                            <div class="col-md-12 pull-right" style="text-align: right !important;">
                                                <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">IVA: <b id="ivaTest"></b></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 pull-right" style="text-align: right !important;">
                                                <label class="control-label text-primary" for="total" style="font-size: 1.5em;">Total: <b id="totalTest"></b></label>
                                            </div>
                                            <div class="form-group" style="text-align: right !important;">
                                                <label class="checkbox-inline"><input type="checkbox" id="checkboxIvaAmount">Si este es el monto con IVA incluido selecciona esta casilla.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <div class="col-md-12">
                                            <label for="billingCustomerMainNew">Razón Social:</label>
                                            <input type="text" name="billingCustomerMainNew" id="billingCustomerMainNew" class="form-control">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="address">Calle:</label>
                                            <input type="text" id="address" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="exteriorNumber">Número Exterior:</label>
                                            <input type="text" id="exteriorNumber" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="interiorNumber">Número Interior:</label>
                                            <input type="text" id="interiorNumber" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="postalCode">Código Postal:</label>
                                            <input type="text" id="postalCode" class="form-control">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="colony">Colonia:</label>
                                            <input type="text" id="colony" class="form-control">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="municipality">Municipio:</label>
                                            <input type="text" id="municipality" class="form-control">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="state">Estado:</label>
                                            <input type="text" id="state" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <div class="col-md-12">
                                            <label for="rfcCustomerMainNew">RFC:</label>
                                            <input type="text" name="rfcCustomerMainNew" id="rfcCustomerMainNew" class="form-control">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="emailBillingCustomerMainNew">Email:</label>
                                            <input type="text" name="emailBillingCustomerMainNew" id="emailBillingCustomerMainNew" class="form-control">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="typeCfdiCustomerMainNew">Tipo CFDI:</label>
                                            <select id="typeCfdiCustomerMainNew" class="form-control"></select>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="methodPayCustomerMainNew">Método de Pago:</label>
                                            <select id="methodPayCustomerMainNew" class="form-control"></select>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="wayPayCustomerMainNew">Forma de Pago:</label>
                                            <select id="wayPayCustomerMainNew" class="form-control"></select>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label class="control-label" for="tipo">Cuenta Bancaria:</label>
                                            <div class="input-group">
                                                <select id="bankAccountsEdit" class="form-control">
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-block btn-secondary" data-toggle="modal" data-target="#addAccountBankModal">
                                                        <i class="fa fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="row">
                                        <button class="btn btn-primary" type="button" id="createInvoiceService">Facturar Servicio</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE COMPLEMENTO DE FACTURAR SERVICIO -->
