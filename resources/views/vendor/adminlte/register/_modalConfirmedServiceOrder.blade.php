<!-- INICIA MODAL PARA ENVIAR EMAIL SERVICIO -->
<div class="modal fade" id="confirmedServiceOrder" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body">
                <h4 id="labelConfirmed"></h4>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-primary btn-lg" type="button" id="btnConfirmedService">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL SERVICIO -->