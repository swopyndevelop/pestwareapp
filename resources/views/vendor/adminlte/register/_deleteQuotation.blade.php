<!-- START MODAL DE RECHAZAR COTIZACIÓN -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="deleteQuoatation" role="dialog" aria-labelledby="denyModal" tabindex="-1">
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-dialog" role="document" >

          <div class="content modal-content" >
            <div class="box">
              <div class="modal-header">
                <div class="box-header">
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>

              <div class="box-body">
                <div class="row container-fluid">
                    <div class="col-md-12 text-center">
                      <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">¿Estás seguro de eliminar la cotización?
                        <br>
                        <b class="text-danger" id="quotationKeyDelete"></b>
                      </h4>
                      <p>
                          * Se borrará lo siguiente: <br>
                          * Ordenes de Servicio: <b class="text-danger" id="orderServicesCount"></b> <br>
                          * Monitoreo de Estaciones (Relacionada a orden de servicio). <br>
                          * Monitoreo de Áreas (Relacionada a orden de servicio).
                      </p>
                    </div>
                </div>
              </div>

              <div class="modal-footer">
                <div class="text-center">
                  <div class="row">
                      <button class="btn btn-primary" type="button" id="deleteQuotationIndex">Eliminar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- END MODAL DE RECHAZAR COTIZACIÓN -->
