@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="scheduleQ" role="dialog" aria-labelledby="scheduleModal">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-context">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>

                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Programar Servicio</h4>
                                    <input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">
                                    <hr>
                                    <div class="row container-fluid">
                                        <div class="col-md-6">
                                            <span style="color:black">Tipo de Servicio</span>
                                            <label for="" class="control-label">Tipo de plaga: <span>@foreach($plagues as $item)
                                                        {{$item->name}},
                                                    @endforeach</span></label>
                                            <label for="" class="control-label"> ({{$area}} m2)</label>
                                        </div>
                                        <div class="col-lg-push-2 col-md-6">
                                            <span>Nº de Orden de Servicio </span><span><b class="text-primary">OS-{{$q->id}}</b></span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row container-fluid">
                                        <div class="col-md-6">

                                            <input type="hidden" name="customer_id" id="customer_id" value="{{$q->id_customer}}">
                                            <input type="hidden" name="q_id" id="q_id" value="{{$q->id}}">

                                            <h5 style="color:black">Datos del Cliente</h5>
                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__customerName">
                                                    <label for="customerName">Cliente*:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="customerName" id="customerName" value="{{$q->name}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__establishmentName">
                                                    <label for="establishmentName">Empresa:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="establishmentName" id="establishmentName" value="{{$q->establishment_name}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Correcto.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__cellphone">
                                                    <label for="cellphone">Celular:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="number" name="cellphone" id="cellphone" value="{{$q->cellphone}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">
                                                        Este campo solo admite números, mínimo 10 y maximo 14 dígitos.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__email">
                                                    <label for="email">Correo electrónico:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="email" id="email" value="{{$q->email}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">El correo electrónico tiene que ser válido por ejemplo: email@gmail.com</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__contact">
                                                    <label for="contact">Contacto 2:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="contact" id="contact" value="{{$q->contact_two_name}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Correcto.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__cellphone_two">
                                                    <label for="cellphone_two">Celular 2:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="number" name="cellphone_two" id="cellphone_two" value="{{$q->contact_two_phone}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Correcto.</p>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <h5 style="color:black">Domicilio</h5>
                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__state">
                                                    <label for="state">
                                                        @if($labels['state'] != null) {{ $labels['state']->label_spanish }}*: @else Estado*: @endif
                                                    </label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="state" id="state" value="{{$address_job_center->state}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__municipality">
                                                    <label for="municipality">Municipio*:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="municipality" id="municipality" value="{{$address_job_center->municipality}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__colony">
                                                    <label for="colony">
                                                        @if($labels['colony'] != null) {{ $labels['colony']->label_spanish }}*: @else Colonia*: @endif
                                                    </label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="colony" id="colony" value="{{$q->colony}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres..</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-8 formulario__grupo" id="grupo__calle">
                                                    <label for="calle">
                                                        @if($labels['street'] != null) {{ $labels['street']->label_spanish }}*: @else Calle*: @endif
                                                    </label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="calle" id="calle" value="{{$q->address}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                                </div>

                                                <div class="col-md-4 formulario__grupo" id="grupo__num">
                                                    <label for="num">Num*:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="num" id="num" value="{{$q->address_number}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error" style="font-size: 10px">Cualquier caracter</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 formulario__grupo" id="grupo__mensaje">
                                                    <label for="reference">Comentarios al técnico</label>
                                                    <div class="">
                                                        <textarea class="formulario__input form-control" rows="5" name="mensaje" id="mensaje"></textarea>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Correcto.</p>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-12" id="conditionsDiv">

                                            <div class="col-md-3">
                                                <h5 style="color:black">Condiciones de quién habita</h5>
                                                <input type="checkbox" name="embarazo" id="embarazo" value="1">
                                                <label class="control-label" for="embarazo"> Embarazo</label>
                                                <br>
                                                <input type="checkbox" name="bebes" id="bebes" value="2">
                                                <label class="control-label" for="bebes"> Bebés</label>
                                                <br>
                                                <input type="checkbox" name="children" id="children" value="3">
                                                <label class="control-label" for="children"> Niños</label>
                                                <br>
                                                <input type="checkbox" name="adulto" id="adulto" value="4">
                                                <label class="control-label" for="adulto">Adulto mayor</label>
                                            </div>

                                            <div class="col-md-3" id="sickPeopleDiv">
                                                <h5 style="color:black">Personas con enfermedades</h5>
                                                <input type="checkbox" name="respiratorias" id="respiratorias" value="1">
                                                <label class="control-label" for="name"> Respiratorias</label>
                                                <br>
                                                <input type="checkbox" name="inmunologico" id="inmunologico" value="2">
                                                <label class="control-label" for="name"> Inmunológicas</label>
                                                <br>
                                                <input type="checkbox" name="renales" id="renales" value="3">
                                                <label class="control-label" for="name"> Renales</label>
                                                <br>
                                                <input type="checkbox" name="cardiacas" id="cardiacas" value="4">
                                                <label class="control-label" for="name"> Cardiacas</label>
                                                <br>
                                                <input type="checkbox" name="does_not" id="does_not" value="5">
                                                <label class="control-label" for="name"> No aplica</label>
                                                <br>
                                                <input type="checkbox" name="other" id="other" value="" onclick="specific(this.checked);">
                                                <label class="control-label" for="name"> Otros</label>
                                                <input type="text" name="specific" id="specific" disabled placeholder="Especifique">
                                            </div>

                                            <div class="col-md-3" id="petsDiv">
                                                <h5 style="color:black">Mascotas</h5>
                                                <input type="checkbox" name="perro" id="perro" value="1">
                                                <label class="control-label" for="name"> Perros</label>
                                                <br>
                                                <input type="checkbox" name="gato" id="gato" value="2">
                                                <label class="control-label" for="name"> Gatos</label>
                                                <br>
                                                <input type="checkbox" name="ave" id="ave" value="3">
                                                <label class="control-label" for="name"> Aves</label>
                                                <br>
                                                <input type="checkbox" name="pez" id="pez" value="4">
                                                <label class="control-label" for="name"> Peces</label>
                                                <br>
                                                <input type="checkbox" name="otro" id="otro" value="" onclick="mascot(this.checked);">
                                                <label class="control-label" for="name"> Otros</label>
                                                <input type="text" name="mascota" id="mascota"  disabled  placeholder="Especifique">
                                            </div>

                                            <div class="col-md-3" id="methodPayDiv">
                                                <h5 style="color:black">Método de pago</h5>
                                                <select name="pay_m" id="pay_m" class="applicantsList-single form-control">
                                                    <option value="1">Efectivo</option>
                                                    @foreach ($method as $m)
                                                        @if($m->id != 1)
                                                            <option value="{{$m->id}}">{{$m->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <h5 style="color:black">Forma de pago</h5>
                                                <select name="pay_f" id="pay_f" class="applicantsList-single form-control">
                                                    <option value="2">Contado</option>
                                                    @foreach ($way as $w)
                                                        @if($w->id != null)
                                                            <option value="{{$w->id}}">{{$w->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>

                                        <div class="col-md-12" id="rfcDiv">

                                            <h5 style="color:black">Datos de Facturación</h5>
                                            <input type="checkbox" name="habilitar" id="habilitar" onclick="habilitar(this.checked);" @if($q->billing_mode == 2) checked @endif> <span class="text-primary">Facturar orden de servicio</span>
                                            <div class="row form-group" @if($q->billing_mode == 1) style="display: none;" @endif id="divDataBilling">
                                                <div class="col-sm-3 formulario__grupo" id="grupo__rfc">
                                                    <label style="color:black">{{$q->rfc_country}}:</label>
                                                    <div class="">
                                                        <input class="formulario__input form-control" type="text" name="rfc" id="rfc" value="{{$q->rfc}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Ingresar el RFC correctamente.</p>
                                                </div>

                                                <div class="col-sm-3 formulario__grupo" id="grupo__social">
                                                    <div class="">
                                                        <label>Razón Social*:</label>
                                                        <input class="formulario__input form-control" type="text" name="social" id="social" value="{{$q->billing}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error" style="font-size: 9px">La descripción debe ser de 3 a 250 caracteres.</p>
                                                </div>

                                                <div class="col-sm-3 formulario__grupo" id="grupo__address_rfc">
                                                    <div class="">
                                                        <label>Domicilio*:</label>
                                                        <input class="formulario__input form-control" type="text" name="address_rfc" id="address_rfc" value="{{$q->tax_residence}}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error"style="font-size: 9px">La descripción debe ser de 3 a 250 caracteres.</p>
                                                </div>


                                                <div class="col-sm-3 formulario__grupo" id="grupo__emailPurcharse">
                                                    <div class="">
                                                        <label>Email*:</label>
                                                        <input class="formulario__input form-control" type="text" name="emailPurcharse" id="emailPurcharse" value="{{ $q->email_billing }}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Favor de ingresar correo válido.</p>
                                                </div>

                                                <br>
                                                <hr>

                                                <div class="col-sm-3 formulario__grupo" id="grupo__social" @if(Auth::user()->companie != 37) style="display: none;" @endif>
                                                    <div>
                                                        <label for="typeCfdiCustomerMainNew">Tipo CFDI:</label>
                                                        <select name="typeCfdiCustomerMainNewEdit" id="typeCfdiCustomerMainNewEdit" class="form-control">
                                                            @if($q->cfdi_type == null || $q->cfdi_type == "")
                                                                <option value="0" selected disabled>Seleccionar Uso de la Factura</option>
                                                            @endif
                                                            @foreach($typesCfdi as $type)
                                                                <option @if($q->cfdi_type == $type->Value) selected @endif value="{{ $type->Value }}">{{ $type->Value }} - {{ $type->Name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 formulario__grupo" id="grupo__address_rfc" @if(Auth::user()->companie != 37) style="display: none;" @endif>
                                                    <div class="">
                                                        <label for="methodPayCustomerMain">Método de Pago:</label>
                                                        <select name="methodPayCustomerMainEdit" id="methodPayCustomerMainEdit" class="form-control">
                                                            @if($q->payment_method_sat == null || $q->payment_method_sat == "")
                                                                <option value="0" selected disabled>Selecciona un método de pago</option>
                                                            @endif
                                                            @foreach($paymentMethodsSat as $method)
                                                                <option @if($q->payment_method_sat == $method->Value) selected @endif value="{{ $method->Value }}">{{ $method->Value }} - {{ $method->Name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 formulario__grupo" id="grupo__address_rfc" @if(Auth::user()->companie != 37) style="display: none;" @endif>
                                                    <div class="">
                                                        <label for="wayPayCustomerMain">Forma de Pago:</label>
                                                        <select name="wayPayCustomerMainEdit" id="wayPayCustomerMainEdit" class="form-control">
                                                            @if($q->payment_way_sat == null || $q->payment_way_sat == "")
                                                                <option value="0" selected disabled>Selecciona una forma de pago</option>
                                                            @endif
                                                            @foreach($paymentFormsSat as $form)
                                                                <option @if($q->payment_way_sat == $form->Value) selected @endif value="{{ $form->Value }}">{{ $form->Value }} - {{ $form->Name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-12 text-right" id="labelTotalsDiv" @if(Entrust::can('No Mostrar Precio (Agenda)')) style="display: none" @endif>
                                            <input type="hidden" id="symbolCountry" value="{{ $symbol_country }}">
                                            <div class="row">
                                                <div class="col-sm-12 formulario__grupo">
                                                    <label for="" class="control-label" style="font-size: 20px;">Subtotal: <b style="font-size: 20px;">{{ $symbol_country }}@convert($q->price)</b></label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 formulario__grupo">
                                                    <input type="hidden" name="i_sum" id="i_sum" value="{{$iva}}">
                                                    <label for="" class="control-label" style="font-size: 20px;">
                                                        @if($labels['iva'] != null) {{ $labels['iva']->label_spanish }}
                                                        @else IVA
                                                        @endif:
                                                        <b id="label-iva">{{ $symbol_country }}@if($q->billing_mode == 2)@convert($iva) @else @convert(0) @endif</b></label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 formulario__grupo">
                                                    <label for="" class="control-label" style="font-size: 20px;">Descuento: <b>{{ $symbol_country }}@convert($discount)</b></label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 formulario__grupo">
                                                    <input type="hidden" name="i_tot" id="i_tot" value="{{ $q->total }}">
                                                    <label for="" class="control-label" style="font-size: 20px;"><b class="text-primary">Total: </b><b id="label-total">{{ $symbol_country }}@convert($q->total)</b></label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <a href="{{ route('edit_quotation',\Vinkla\Hashids\Facades\Hashids::encode($q->id))}}" title="Editar Cotización" class="btn btn-secondary-outline btn-sm">
                                                    <span class="glyphicon glyphicon-pencil"></span> Editar cotización
                                                </a>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="text-center">
                                    <div class="formulario__mensaje" id="formulario__mensaje" style="margin-left: 10px; margin-right: 10px;">
                                        <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                    </div><br>
                                    <div class="text-center formulario__grupo formulario__grupo-btn-enviar">
                                        <button type="submit" class="btn btn-primary" id="saveOrder" name="saveOrder" title="Programar Servicio">Guardar</button>
                                        <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                    </div>

                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/schedule.js') }}"></script>
  <script>
    $("#scheduleQ").modal();
    $("#scheduleQ").on('hidden.bs.modal', function() {
      window.location.href = '{{route("index_register")}}'; //using a named route
    });
  </script>
  <script>
        const symbolCountry = document.getElementById('symbolCountry').value;
        //Habilitar campos de facturación
          function habilitar(value){
            if(value == true){
              let suma, iva,tot = 0;
              iva = document.getElementById("i_sum").value;
              tot = document.getElementById("i_tot").value;
              suma = parseInt(iva) + parseInt(tot);
              const labelIva = `${symbolCountry}${iva}`;
              const labelTotal = `${symbolCountry}${suma}`;
              $('#label-iva').html(labelIva);
              $('#label-total').html(labelTotal);
              document.getElementById('divDataBilling').style.display = 'block';
            }else if(value == false){
              let resta, i, t = 0;
              i = document.getElementById("i_sum").value;
              t = document.getElementById("i_tot").value;
              resta = parseInt(t) - parseInt(i);
              const labelTotal = `${symbolCountry}${t}`;
              $('#label-iva').html(`${symbolCountry}0.00`);
              $('#label-total').html(labelTotal);
              document.getElementById('divDataBilling').style.display = 'none';
            }
          }
      </script>
      <script>
        //Habilitar los campos de otro en mascotas y enfermedades
        function mascot(value){
          if (value == true){
            document.getElementById("mascota").disabled = false;
            $('#mascota').prop("required",true);
          }else if(value == false){
            document.getElementById("mascota").disabled = true;
            document.getElementById("mascota").value = "";
            $('#mascota').removeAttr("required");
          }
        }
        function specific(value){
          if(value == true){
            document.getElementById("specific").disabled = false;
          }else if(value == false){
            document.getElementById("specific").disabled = true;
            document.getElementById("specific").value = "";
          }
        }
      </script>
@endsection
