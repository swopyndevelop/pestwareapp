@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="modal fade" id="mailP" role="dialog" aria-labelledby="mailPersonal">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <br>
                            <form action="{{route('save_config_mail')}}" method="POST" id="form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Personalizar Mails</h4>
                                    <div class="row">
                                        <div class="col-lg-push-1 col-md-12">
                                            <label for="Banner">Banner</label>
                                            <br>
                                            <div class="col-sm-6">
                                                <input type="file" name="banner" accept="image/*" class="form-control" required>
                                            </div>
                                            <br><br>
                                            <label for="description">Texto del e-mail</label>
                                            <br>
                                            <div class="col-sm-6">
                                                <textarea class="form-control"
                                                          cols="50"
                                                          rows="10"
                                                          id="description"
                                                          name="description"
                                                          required
                                                          placeholder="Queremos encontrar las mejores soluciones para ti en tu hogar o negocio. Gracias por confiar en nosotros, tenemos como objetivo cuidar y mejorar tu calidad de vida. Nuestros programas de control de plagas buscan optimizar el medio ambiente donde vives a través de soluciones que no pongan en riesgo tu salud.continuación encontrarás adjunta la cotización del servicio solicitado."></textarea>
                                            </div>
                                            <br><br>
                                            <label for="">Numero de Whatsapp</label>
                                            <br>
                                            <div class="col-sm-5">
                                                <input type="text" name="whatsapp" id="whatsapp" class="form-control">
                                            </div>
                                            <br><br>
                                            <label for="">Enlace de Messenger</label>
                                            <br>
                                            <div class="col-sm-5">
                                                <input type="text" name="messenger" id="messenger" class="form-control">
                                            </div>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary btn-md" name="SaveMail" id="SaveMail">Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('personal-js')
  <script>
    $("#mailP").modal();
    $("#mailP").on('hidden.bs.modal', function() {
      window.location.href = '{{route("index_product")}}'; //using a named route
    });
  </script>
@endsection
