<!-- INICIA MODAL PARA ENVIAR EMAIL SERVICIO -->
<div class="modal fade" id="sendemail" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="box-body container-fluid">
        <h4 class="modal-title text-center col-lg-12 text-info">Enviar Certificado</h4>
        <label for="email" style="font-weight: bold;">Correo Electrónico:</label>
        <input type="text" id="emailText" value="" class="form-control">
        <br>
        <label for="email" style="font-weight: bold;">Otro Correo Electrónico:</label>
        <br>
        <input type="text" name="email" id="email" class="form-control">

        <div class="col-md-12 text-center">
          <h4>Seleccionar Documentos</h4>
          <div class="custom-control col-md-6">
            <input type="checkbox" class="form-check-input" name="fileOrder" id="fileOrder" checked>
            <label class="custom-control-label" for="fileOrder">Orden de Servicio</label>
          </div>
          <div class="custom-control col-md-6">
            <input type="checkbox" class="form-check-input" name="fileCertificate" id="fileCertificate" checked>
            <label class="custom-control-label" for="fileCertificate">Certificado de Servicio</label>
          </div>

          <div class="custom-control col-md-6" id="showStation" style="display: none">
            <input type="checkbox" class="form-check-input" name="fileStation" id="fileStation">
            <label class="custom-control-label" for="fileStation">Inspección de Estaciones</label>
          </div>

          <div class="custom-control col-md-6" id="showArea" style="display: none">
            <input type="checkbox" class="form-check-input" name="fileArea" id="fileArea">
            <label class="custom-control-label" for="fileArea">Inspección de Áreas</label>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <div class="text-center">
          <button class="btn btn-primary btn-sm" type="button" id="sendEmailOrder" name="sendEmailOrder">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL SERVICIO -->