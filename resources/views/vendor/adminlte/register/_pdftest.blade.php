<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<title>Servicio</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
	}
    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }
    caption, td, th {
        padding: 0.3em;
    }
    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }
    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
        {{--//headers--}}
        <div style="margin: 0px;">
            <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

                <div style="text-align: center; margin-bottom: 0px; margin-top: 20px;">
                    <h2 style="font-weight: bold; margin-bottom: 0px">Factura (Cliente)</h2>
                </div>

                <div style="width: 130px; display: inline-block; vertical-align: top;">
                    <img src="https://storage.pestwareapp.com/pdf_logos/3FiRcBqskspdMjwdHEbPbu55dcqqnwORDDbjudy0.jpeg" alt="logo" width="130px">
                </div>

                <div style="width: 290px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
                    <p style="margin-top: 0px; margin-bottom: 0">
                        <span style="color:black;font-size:9.5pt">Datos a detalle de Factura: {{--{{$imagen->health_manager}} | Licencia No: {{$imagen->licence}}
                            | Tel: {{$imagen->phone}} | Razón Social: {{ $imagen->bussines_name }} |RFC: {{ $imagen->rfc }} | {{ $addressProfile->street }} #{{ $addressProfile->num_ext }}, {{ $addressProfile->location }},
                            {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. | Email: {{ $imagen->email }} | Facebook: {{$imagen->facebook}}.--}}
                        </span>
                    </p>
                </div>
                <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
                        <img src="data:image/png;base64" width="100px"><br>
                        <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Factura Qr</span>
                </div>
                <div style="width: 212px; display: inline-block; margin-top: 0px; font-size: 8pt; margin-left: 10px; vertical-align: top;">
                    <p style="margin-bottom: 2px; margin-top: 0px">
                        <span style="font-size:8pt; font-weight: bold">NO. SERVICIO: Test</span>
                        <span style="color:red;font-weight:bold;font-size:8pt;"></span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">TIPO DE SERVICIO: </span>
                        <span style="font-size:8pt;"></span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">IMPORTE: </span>
                        <span style="font-size:8pt;">$</span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">FECHA: </span>
                        <span style="font-size:8pt;"></span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">HORA DE ENTRADA: </span>
                        <span style="font-size:8pt;"></span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">HORA DE SALIDA: </span>
                        <span style="font-size:8pt;"></span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">TÉCNICO APLICADOR: </span>
                        <span style="font-size:8pt;"></span>
                    </p>
                </div>
            </div>
        </div>
        {{--//Body--}}
        <div style="text-align: left; ; margin: 0 20px 0 20px;">
            <p style="margin-top: 0px; margin-bottom: 7px">
                <span style="font-size:11pt; font-weight: bold"> Atención a "Empresa"</span><br>
                <span style="font-size:7pt">"Dirección Empresa"</span>
            </p>
            <p style="margin-top: 0px; margin-bottom: 0px">
                <span style="font-size:11pt; font-weight: bold"> Periodo: del 1 de Enero al 15 de Febrero</span><br>
                <span style="font-size:11pt; font-weight: bold"> Tipo: </span>
            </p>
        </div>
        <br><br>
        {{--//Charts--}}
        <div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; margin-top: -20px;">
            <table>
                <thead>
                <tr style="font-weight:bold">
                    <td>PLAGUICIDA APLICADO</td>
                    <td>INGREDIENTE</td>
                    <td>DOSIS</td>
                    <td>CANTIDAD</td>
                </tr>
                </thead>
                <tbody>
                    <tr style="font-size: 8pt">
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="border-color:black;margin-left:20px; margin-right: 22px; text-align: right; font-size: 9pt">
            <div style="width: 375px; display: inline-block; margin-top: 5px; vertical-align: top; text-align: left;">
                <p style="margin: 0px; font-weight: bold">Gráfica de Proporción de Insectos</p>
            </div>

            <div style="width: 375px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top; text-align: right;margin-right: -5px;">
                <p style="margin: 0px">
                    <span>
                        $1000
                    </span>
                </p>
            </div>


        </div>
        <br>
        <div style="border-color:black;margin-right:20px;margin-left:20px;font-size:9pt; text-align: center">
            <div style="width: 375px; display: inline-block; margin-top: 5px; vertical-align: top; text-align: center;">
                <p style="margin: 0px; font-weight: bold">Gráfica de Proporción de Insectos</p>
                <img src="https://storage.pestwareapp.com/pdf_licencias_sanitarias/fmhr8kDEIiAH33Ho3gS7sMPbaOvOjzscLeHb1sRW.png" width="80%" height="200px">
            </div>

            <div style="width: 375px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top; text-align: center;margin-right: -5px;">
                <p style="margin: 0px; font-weight: bold">Gráfica de Proporción de Condiciones del Lugar</p>
                <img src="https://storage.pestwareapp.com/pdf_licencias_sanitarias/fmhr8kDEIiAH33Ho3gS7sMPbaOvOjzscLeHb1sRW.png" width="80%" height="200px">
            </div>

        </div>
        <br>
        {{--//Footer--}}
        <footer style="position: absolute; bottom: 5px;">

        </footer>
</body>
</html>