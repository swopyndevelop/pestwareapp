<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Orden de Servicio</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
	}
    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }
    caption, td, th {
        padding: 0.3em;
    }
    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }
    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body style="background-image: url('https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/goncer/Cert2021_MedidasA4.png'); background-size: 100% 100%;">
        {{--//headers--}}
        <div style="margin: 0px;">
            <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

                <div style="text-align: center; margin-bottom: 0px; margin-top: 20px;">
                    <h2 style="font-weight: bold; margin-bottom: 0px">ORDEN DE SERVICIO</h2>
                </div>

                <div style="width: 130px; display: inline-block; vertical-align: top;">
                    @if($imagen->pdf_logo == null)
                        <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
                    @else
                        <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="130px">
                    @endif
                </div>

                <div style="width: 290px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
                    <p style="margin-top: 0px; margin-bottom: 0">
                      <span style="color:black;font-size:9.5pt">Responsable Sanitario: {{$jobCenterProfile->health_manager}} | Licencia No: {{$jobCenterProfile->license}}
                            | Whatsapp: {{$jobCenterProfile->whatsapp_personal}} | Teléfono: {{$jobCenterProfile->cellphone}} | Razón Social: {{ $jobCenterProfile->business_name }} |
                            {{ $jobCenterProfile->rfc_country }}: {{ $jobCenterProfile->rfc }}| @if($addressProfile) {{ $addressProfile->street }} #{{ $addressProfile->num_ext }}, {{ $addressProfile->location }},
                            {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. @endif | Email: {{ $jobCenterProfile->email_personal }} | Facebook: {{$jobCenterProfile->facebook_personal}}.
                        </span>
                    </p>
                </div>
                <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
                    @if($qrcode != null)
                        <img src="data:image/png;base64, {!! $qrcode !!}" width="100px"><br>
                        <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Licencia Sanitaria</span>
                    @endif
                </div>
                <div style="width: 212px; display: inline-block; margin-top: 0px; font-size: 8pt; margin-left: 10px; vertical-align: top;">
                    <p style="margin-bottom: 2px; margin-top: 0px">
                        <span style="font-size:8pt; font-weight: bold">NO. SERVICIO: </span>
                        <span style="color:red;font-weight:bold;font-size:8pt;">{{$order->id_service_order}}</span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">TIPO DE SERVICIO: </span>
                        <span style="font-size:8pt;">{{$order->establecimiento}}</span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">IMPORTE: </span>
                        <span style="font-size:8pt;">{{ $symbol_country }}{{$order->total}}</span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">FECHA: </span>
                        <span style="font-size:8pt;"><?php echo date("d/m/y",strtotime($order->initial_date))?></span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">HORA DE ENTRADA: </span>
                        <span style="font-size:8pt;"><?php echo date("H:i",strtotime($order->start_event))?></span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">HORA DE SALIDA: </span>
                        <span style="font-size:8pt;"><?php echo date("H:i",strtotime($order->final_event))?></span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">TÉCNICO APLICADOR: </span>
                        <span style="font-size:8pt;">{{$order->name}}</span>
                    </p>
                </div>
            </div>
        </div>
        <div style="text-align: left; ; margin: 0 20px 0 20px;">
            <p style="margin-top: 0px; margin-bottom: 7px">
                <span style="font-size:11pt; font-weight: bold"> Atención a {{ $order->cliente}} @if($order->empresa != null) | {{$order->empresa }} @endif</span><br>
                <span style="font-size:7pt">{{$order->address}} #{{$order->address_number}}, {{$order->colony}}, {{$order->municipality}}, {{$order->state}}
                | Tel: {{$order->cellphone}} | {{$order->email}}</span>
            </p>
        </div>
        <br><br>
        {{--//Body--}}
        <div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; margin-top: -30px; border: solid 1px">

            <div style="width: 358px; display: inline-block; margin-top: 0px; vertical-align: top">
                <span style="font-size:10pt;font-weight:bold;color:black; margin-top: -20px; margin-left: 10px;border-bottom: 2px solid;">
                    INSPECCIÓN DEL LUGAR
                </span>
                <br>
                <div style="border-bottom: 1px solid;border-color:black;margin-left: 10px">
                    <span style="font-size:9pt;font-weight:bold;color:black">PLAGA Y GRADO DE INFESTACION:</span>
                    <br>
                    <div style="margin-left:0px;font-size:9pt;margin-top:0px;">
                        @foreach($plaga as $p)
                            {{$p->plaga}} - {{$p->infestacion}},
                        @endforeach
                    </div>
                </div>
                <div style="border-color:black; margin-left: 10px; border-bottom: 1px solid;">
                    <span style="font-size:9pt;font-weight:bold;color:black;">ÁREAS DE ANIDAMIENTO</span><br>
                    <div style="font-size:9pt;margin-top:0px;">
                        {{$ani->nesting_areas}}
                    </div>
                </div>
                <div style="border-color:black; margin-left: 10px">
                    <span style="font-size:9pt;font-weight:bold;color:black;">COMENTARIOS</span><br>
                    <div style="font-size:9pt;margin-top:0px;">
                        {{$ani->commentary}}
                    </div>
                </div>
            </div>

            <div style="width: 1px; display: inline-block; margin-top: 0px">

            </div>

            <div style="width: 358px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top">
                <span style="font-size:10pt;font-weight:bold;color:black; margin-left: 10px; border-bottom: 2px solid;">
                    CONDICIONES DEL LUGAR
                </span>
                <br>
                <span style="font-size:9pt;font-weight:bolder;color:black; margin-left: 10px">
                    CUMPLIÓ CON INDICACIONES
                </span>
                <br>
                <div style="margin-left:10px;font-size:9pt;margin-top:0px;border-bottom: 1px solid black">
                    @if ($indi->indications == 1)
                        Sí
                    @elseif($indi->indications == 2)
                        No
                    @elseif($indi->indications == 3)
                        Una Parte
                    @endif
                </div>
                <span style="font-size:9pt;font-weight:bold;color:black; margin-left: 10px">ORDEN Y LIMPIEZA</span><br>
                <div style="margin-left:10px;font-size:9pt;margin-top:0px;border-bottom: 1px solid black">
                    @foreach ($ins as $item)
                        {{$item->order}},
                    @endforeach
                </div>
                <span style="font-size:9pt;font-weight:bold;color:black; margin-left: 10px;">ACCESOS RESTRINGIDOS</span><br>
                <div style="margin-left:10px;font-size:9pt;margin-top:0px; border-bottom: 1px solid;">
                    {{$indi->restricted_access}}
                </div>
                <span style="font-size:9pt;font-weight:bold;color:black; margin-left: 10px">COMENTARIOS</span><br>
                <div style="margin-left:10px;font-size:9pt;margin-top:0px;">
                    {{$indi->commentary}}
                </div>
            </div>

        </div>
        <br>
        <div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; margin-top: -20px; border: solid 1px">

            <div style="width: 358px; display: inline-block; margin-top: 0px; vertical-align: top">

                <div style="border-color:black;margin-left:10px;">
                    <span style="font-size:10pt;font-weight:bold;color:black; border-bottom: 2px solid;">
                        CONTROL DE PLAGAS
                    </span>
                </div>
                <div style="border-color:black;margin-left:10px; border-bottom: 1px solid;">
                    <span style="font-size:9pt;font-weight:bold;color:black;">ÁREA A CONTROLAR</span><br>
                    <div style="margin-left:0px;font-size:9pt; margin-top: 0px">
                        @foreach($area_c as $c)
                            {{$c->control_areas}},
                        @endforeach
                    </div>
                </div>
                <div style="border-color:black;margin-left:10px">
                    <span style="font-size:9pt;font-weight:bold;color:black">COMENTARIOS</span><br>
                    <div style="margin-left:0px;font-size:9pt; margin-top: 0px">
                        @foreach($area_c as $c)
                            {{$c->commentary}},
                        @endforeach
                    </div>
                </div>

            </div>

            <div style="width: 1px; display: inline-block; margin-top: 0px">

            </div>

            <div style="width: 358px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top">

                <div style="border-color:black;margin-left:10px">
                    <br>
                    <span style="font-size:9pt;font-weight:bold;color:black;">MÉTODO DE APLICACIÓN</span><br>
                    <div style="margin-left:0px;font-size:9pt;margin-top: 0px;">
                        @foreach($area_cd as $cd)
                            {{$cd->apli}},
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
        <br>
        <div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; margin-top: -20px; border: solid 1px">

            <div style="width: 725px; display: inline-block; margin-top: 0px; vertical-align: top">

                <div style="margin-left:8px;font-size:9pt">
                    <table>
                        <thead>
                        <tr>
                            <td style="color:black;font-weight:bold">PLAGUICIDA APLICADO</td>
                            <td style="color:black;font-weight:bold">INGREDIENTE</td>
                            <td style="color:black;font-weight:bold">DOSIS</td>
                            <td style="color:black;font-weight:bold">CANTIDAD</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($control as $cl)
                            <tr>
                                <td style="font-size:8pt">{{$cl->product}}</td>
                                <td style="font-size:8pt">{{$cl->active_ingredient}}</td>
                                <td style="font-size:8pt">{{$cl->dose}}</td>
                                <td style="font-size:8pt">{{$cl->cantidad}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div style="width: 1px; display: inline-block; margin-top: 0px">

            </div>

            <div style="width: 1px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top">

            </div>

        </div>

<!--        <div style="border-color:black;margin-left:20px; margin-right: 22px;">
                <span style="font-size:10pt;font-weight:bold;color:black; border-bottom: 2px solid">
                    COMENTARIOS PAGO
                </span>
        </div>
        <div style="border-bottom: 1px solid;border-color:black;margin-right:20px;margin-left:22px;font-size:9pt;">
        &lt;!&ndash;Tarjeta comentarios&ndash;&gt;
        </div>-->

        <br>
        {{-- Fotos--}}
        <div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; border: solid 1px;">
            <div style="text-align: center; margin: 0 0 0 0">
                <h5 style="margin-top: 0px">Inspección Del Lugar (Evidencia)</h5>
            </div>
            @foreach($inspectionPlaceImages as $inspectionPlaceImage)
                <span style="margin-left: 5px; margin-right: 5px;">
                    <img src="{{ $inspectionPlaceImage->file_route }}" height="70" width="70">
                </span>
            @endforeach
            <div style="text-align: center; margin: 0 0 0 0">
                <h5 style="margin-top: 0px">Condiciones Del Lugar (Evidencia)</h5>
            </div>
            @foreach($conditionPlaceImages as $conditionPlaceImage)
                <span style="margin-left: 5px; margin-right: 5px">
                    <img src="{{ $conditionPlaceImage->file_route }}" height="70" width="70">
                </span>
            @endforeach
            <div style="text-align: center; margin: 0 0 0 0">
                <h5 style="margin-top: 0px">Control de Plagas (Evidencia)</h5>
            </div>
            @foreach($plagueControlsImages as $plagueControlsImage)
                @foreach($plagueControlsImage->controlPlagueImage as $image)
                <span style="margin-left: 5px; margin-right: 5px">
                    <img src="{{ $image->file_route }}" height="70" width="70">
                </span>
                @endforeach
            @endforeach
        </div>

        {{-- Gráficas --}}
<!--        <div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; border: solid 1px; text-align: center">
            <h5 style="margin: 0 0 0 0">Gráficas</h5>
            <div style="width: 374px; display: inline-block; margin-top: 0px; vertical-align: top; text-align: center;">
                <img src="img.png" alt="" width="200" height="150" style="margin-bottom: 3px">
            </div>
            <div style="width: 374px; display: inline-block; margin-top: 0px; vertical-align: top; text-align: center;">
                <img src="img.png" alt="" width="200" height="150" style="margin-bottom: 3px">
            </div>
        </div>-->

        {{-- Footer--}}
        <footer style="position: absolute; bottom: 15px;">

            <div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; margin-top: 0px; border-bottom: solid 1px;">

                <div style="width: 372.5px; display: inline-block; margin-top: 5px; vertical-align: top; text-align: center;">
                    <div style="display: inline-block">
                        @if($f)
                            <img src="{{$f->file_route}}" alt="" width="100" height="40">
                        @endif
                        <p style="text-align:center;font-size:6pt;font-weight:bold; margin-top: 0; margin-bottom: 0">{{$order->cliente}}</p>
                        @if($f)
                            @if($f->other_name != null)
                                <p style="text-align:center;font-size:6pt;font-weight:bold; margin-top: 0; margin-bottom: 0">
                                    {{$f->other_name}}
                                </p>
                            @endif
                        @endif
                        <span style="text-align:center;font-size:6pt;">@if($addressProfile) {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. @endif <?php echo date("d/m/y",strtotime($order->initial_date))?> a
                            las <?php echo date("H:i",strtotime($order->final_event))?></span>
                        <div style="text-align:justify;font-size:5px; margin-bottom: 10px; margin-left: 6px; margin-right: 6px">
                            Firmo de conformidad y a mi entera satisfacción de haber recibido el Servicio de fumigación, acepto que se me
                            explicaron las indicaciones que debo realizar antes de ingresar al lugar fumigado, así como las precauciones a tomar.
                            Conozco los términos y funcionamiento de la garantía del servicio.
                        </div>
                    </div>
                </div>

                <div style="width: 202px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top; text-align: center;margin-right: -5px;">
                    <img src="{{$order->file_route_firm}}" width="100" height="40">
                    <p style="text-align:center;font-size:6pt; margin-top: 0; margin-bottom: 0">{{$order->name}}</p>
                    <p style="text-align:center;font-size:6pt;font-weight:bold; margin-bottom: 0; margin-top: 0">Firma del técnico</p><br>
                </div>

                <div style="width: 165px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: middle; text-align: center; margin-right: -5px;">
                    <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_sello }}" width="80px">
                </div>

            </div>

        </footer>
</body>
</html>