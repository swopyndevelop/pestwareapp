<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Servicio</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
		font-family: 'Montserrat'
	}
    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }
    caption, td, th {
        padding: 0.3em;
    }
    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }
    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
        <div>
                <div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
                    <img src="img/{{$imagen->logo}}" alt="logo" width="200px" height="200px">
                </div>
                <div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
                    <p><span style="color:black;font-weight:bold;font-size:11pt">No. SERVICIO: </span>  
                        <span style="color:red;font-weight:bold;font-size:11pt">{{$order->id_service_order}}</span>
                    <br><span style="color:black;font-weight:bold;font-size:11pt">IMPORTE: </span> 
                        <span style="color:black;font-weight:bold;font-size:11pt">${{$order->total}}</span>
                    <br><span style="color:black;font-weight:bold;font-size:11pt">FECHA: </span>
                        <?php echo date("d/m/y",strtotime($order->initial_date))?>
                    <br><span style="color:black;font-weight:bold;font-size:11pt">HORA DE ENTRADA: </span>
                        <?php echo date("H:i",strtotime($order->initial_hour))?>
                    <br><span style="color:black;font-weight:bold;font-size:11pt">HORA DE SALIDA: </span>
                        <?php echo date("H:i",strtotime($order->final_hour))?>
                    <br><span style="color:black;font-weight:bold;font-size:11pt">TÉCNICO APLICADOR: </span>
                    <span>{{$order->name}}</span></p>  
                </div>	
            </div>
            <br><br><br><br><br><br><br><br><br><br><br>
            <p><span style="color:black;font-size:9pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;Licencia No. {{$imagen->licence}}| {{$imagen->phone}}</span></p>
            <div style="margin: auto;padding-left:45px;margin-top:-25px">
                <p><span style="font-size:12pt"> Atención a {{$order->cliente}}</span>
                <br><span style="font-size:10.5pt;font-weight:bold">{{$order->empresa}}</span>
                <br><span style="font-size:8pt">{{$order->address}} #{{$order->address_number}},
                {{$order->colony}}, {{$order->municipality}}, {{$order->state}}</span>
                <br><span style="font-size:8pt"> Tel. {{$order->cellphone}} </span><br>
                @if($order->email != null)
                    <span style="font-size:8pt"> {{$order->email}}</span></p>
                @endif
            </div>
            <br><br><br><br><br><br><br>
            <div style="margin:auto;border-bottom: 1px solid;border-color:black;margin-left:45px;margin-right:40px">
                <span style="font-size:11pt;font-weight:bold;color:black">INSPECCIÓN DEL LUGAR </span>
            </div>
            <div style="margin-left:45px;margin-right:40px;">
                <div style="border-bottom: 1px solid;border-color:black;line-height: 0.9em;">
                    <span style="font-size:9.5pt;font-weight:bold;color:black">PLAGA Y GRADO <br>DE INFESTACION:</span>
                    <div style="margin-left:220px;font-size:9.5pt;margin-top:-40px;">
                        @foreach($plaga as $p)
                            {{$p->plaga}} - {{$p->infestacion}}, 
                        @endforeach
                    </div>
                </div>
                <div style="border-bottom: 1px solid;border-color:black;line-height: 0.9em;">
                    <span style="font-size:9.5pt;font-weight:bold;color:black">ÁREAS DE <br>ANIDAMIENTO</span>
                    <div style="margin-left:220px;font-size:9.5pt;margin-top:-40px;">
                            {{$ani->nesting_areas}}
                    </div>
                </div>
            </div>
            <br>
            <div style="margin:auto;border-bottom: 1px solid;border-color:black;margin-left:45px;margin-right:40px">
                <span style="font-size:11pt;font-weight:bold;color:black">CONDICIONES DEL LUGAR</span>
            </div>
            <div style="margin-left:45px;margin-right:40px">
                <div style="border-bottom: 1px solid;border-color:black;line-height: 1em;">
                    <span style="font-size:9.5pt;font-weight:bolder;color:black">CUMPLIÓ CON INDICACIONES</span>
                    <div style="margin-left:240px;font-size:9.5pt;margin-top:-25px;">
                        @if ($indi->indications == 1)
                            Sí
                        @elseif($indi->indications == 2)
                            No
                        @elseif($indi->indications == 3)
                            Una Parte
                        @endif
                    </div>
                </div>
                <div style="border-bottom: 1px solid;border-color:black;line-height: 1em;">
                    <span style="font-size:9.5pt;font-weight:bold;color:black">ORDEN Y LIMPIEZA</span>
                    <div style="margin-left:240px;font-size:9.5pt;margin-top:-25px;">
                        @foreach ($ins as $item)
                            {{$item->order}}, 
                        @endforeach
                    </div>
                </div>
                <div style="border-bottom: 1px solid;border-color:black;line-height: 1em;">
                    <span style="font-size:9.5pt;font-weight:bold;color:black">ACCESOS RESTRINGIDOS</span>
                    <div style="margin-left:240px;font-size:9.5pt;margin-top:-25px;">
                        {{$indi->restricted_access}}
                    </div>
                </div>
            </div>
            <br>
            <div style="margin:auto;border-bottom: 1px solid;border-color:black;margin-left:45px;margin-right:40px">
                <span style="font-size:11pt;font-weight:bold;color:black">CONTROL DE PLAGAS</span>
            </div>
            <div style="border-bottom: 1px solid;border-color:black;margin-right:40px;margin-left:45px">
                <span style="font-size:9.5pt;font-weight:bold;color:black">ÁREA A CONTROLAR</span>
                <div style="margin-left:240px;font-size:9.5pt;margin-top:-25px;">
                    @foreach($area_c as $c)
                        {{$c->control_areas}},
                    @endforeach
                </div>
            </div>
            <div style="border-bottom: 1px solid;border-color:black;margin-right:40px;margin-left:45px">
                <span style="font-size:9.5pt;font-weight:bold;color:black">MÉTODO DE APLICACIÓN</span>
                <div style="margin-left:240px;font-size:9.5pt;margin-top:-25px;">
                    @foreach($area_cd as $cd)
                        {{$cd->apli}},
                    @endforeach
                </div>
            </div>
            <br>
            <div style="margin-right:40px;margin-left:45px;font-size:9.5pt">
                <table>
                    <thead>
                        <tr>
                            <td style="color:black;font-weight:bold">PLAGUICIDA APLICADO</td>
                            <td style="color:black;font-weight:bold">INGREDIENTE</td>
                            <td style="color:black;font-weight:bold">DOSIS</td>
                            <td style="color:black;font-weight:bold">CANTIDAD</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($control as $cl)
                        <tr>
                            <td style="font-size:8.5">{{$cl->product}}</td>
                            <td style="font-size:8.5">{{$cl->active_ingredient}}</td>
                            <td style="font-size:8.5">{{$cl->dose}}</td>
                            <td style="font-size:8.5">{{$cl->cantidad}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div style="border-bottom: 1px solid;border-color:black;margin-right:40px;margin-left:45px">
                    <span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS</span>
            </div>
            <div style="border-bottom: 1px solid;border-color:black;margin-right:40px;margin-left:45px;font-size:9.5pt;line-height: 0.9em;">
                @foreach($area_c as $cd)
                    <span>{{$cd->commentary}}<br></span>
                @endforeach
            </div>
            <div style="width:350px;font-size:7pt;margin-left:45px;text-align:justify;float:left;margin-top:85px;line-height: 0.9em;">
                <p>Firmo de conformidad y a mi entera satisfacción de haber recibido el
                        Servicio de fumigación, acepto que se me explicaron las indicaciones
                        que debo realizar antes de ingresar al lugar fumigado, así como las
                        precauciones a tomar. Conozco los términos y funcionamiento de la
                        garantía del servicio.</p>
            </div>
            <div style="width:350px;font-size:7pt;margin-left:45px;text-align:justify;float:right;text-align:center;margin-top:-50px;line-height: 0.9em;">
                <img src="{{$f->file_route}}" alt="" width="150px" height="100px"><br>
                <span style="text-align:center;font-size:7pt;font-weight:bold">{{$order->cliente}}</span><br>
                <span style="text-align:center;font-size:7pt;">Aguascalientes, Ags. <?php echo date("d/m/y",strtotime($date))?> a 
                las <?php echo date("H:i",strtotime($date))?></span>
            </div>
        </div>
</body>
</html>