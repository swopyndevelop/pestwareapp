<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Orden de Servicio</title>
</head>
<style>
    @page {
        margin: 0;
    }
    body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
    }
    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }
    caption, td, th {
        padding: 0.3em;
    }
    tr, th, td {
        width: 20%;
    }
    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
{{--//headers--}}
<div style="margin: 0px;">
    <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

        <div style="text-align: center; margin-bottom: 0px; margin-top: 20px;">
            <h2 style="font-weight: bold; margin-bottom: 0px">ORDEN DE SERVICIO</h2>
        </div>

        <div style="width: 130px; display: inline-block; vertical-align: top;">
            @if($imagen->pdf_logo == null)
                <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
            @else
                <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="130px">
            @endif
        </div>

        <div style="width: 290px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
            <p style="margin-top: 0px; margin-bottom: 0">
                       <span style="color:black;font-size:9.5pt">Responsable Sanitario: {{$jobCenterProfile->health_manager}} | Licencia No: {{$jobCenterProfile->license}}
                            | Whatsapp: {{$jobCenterProfile->whatsapp_personal}} | Teléfono: {{$jobCenterProfile->cellphone}} | Razón Social: {{ $jobCenterProfile->business_name }} |
                            {{ $jobCenterProfile->rfc_country }}: {{ $jobCenterProfile->rfc }}| @if($addressProfile) {{ $addressProfile->street }} #{{ $addressProfile->num_ext }}, {{ $addressProfile->location }},
                           {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. @endif | Email: {{ $jobCenterProfile->email_personal }} | Facebook: {{$jobCenterProfile->facebook_personal}}.
                        </span>
            </p>
        </div>
        <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
            @if($qrcode != null)
                <img src="data:image/png;base64, {!! $qrcode !!}" width="100px"><br>
                <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Licencia Sanitaria</span>
            @endif
        </div>
        <div style="width: 212px; display: inline-block; margin-top: 0px; font-size: 8pt; margin-left: 10px; vertical-align: top;">
            <p style="margin-bottom: 2px; margin-top: 0px">
                <span style="font-size:8pt; font-weight: bold">NO. SERVICIO: </span>
                <span style="color:red; font-weight:bold;font-size:8pt;">{{$order->id_service_order}}</span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">TIPO DE SERVICIO: </span>
                <span style="font-size:8pt;">{{$order->establecimiento}}</span>
            </p>
            @if($order->show_price == 1 || $order->show_price_customer == 1)
                @if($order->show_price_customer == 1 && $order->show_price == 0)
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">IMPORTE: </span>
                        <span style="font-size:8pt;">{{ $symbol_country }}{{$order->total}}</span>
                    </p>
                @elseif($order->show_price_customer == 1 && $order->show_price == 1)
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">IMPORTE: </span>
                        <span style="font-size:8pt;">{{ $symbol_country }}{{$order->total}}</span>
                    </p>
                @endif
            @endif
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">FECHA: </span>
                <span style="font-size:8pt;"><?php echo date("d/m/y",strtotime($order->initial_date))?></span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">HORA DE ENTRADA:</span>
                <span style="font-size:8pt;">______________</span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">HORA DE SALIDA:</span>
                <span style="font-size:8pt;">________________</span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">TÉCNICO APLICADOR:</span>
                <span style="font-size:8pt;">{{$order->name}}</span>
            </p>
        </div>
    </div>
</div>
<div style="text-align: left; ; margin: 0 20px 0 20px;">
    <p style="margin-top: 0px; margin-bottom: 7px">
       <span style="font-size:11pt; font-weight: bold"> Atención a {{ $order->cliente}} @if($order->empresa != null) | {{$order->empresa }} @endif
           @if($order->billing != null) | {{$order->billing }} @endif
                </span><br>
        <span style="font-size:10pt">{{$order->address}} #{{$order->address_number}}, {{$order->colony}}, {{$order->municipality}}, {{$order->state}}
                | Tel: {{$order->cellphone}} | {{$order->email}}</span>
    </p>
</div>
<br><br>
{{--//Body--}}
<div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; margin-top: -30px; border: solid 1px">

    <div style="width: 358px; display: inline-block; margin-top: 0px; vertical-align: top">
                <span style="font-size:10pt;font-weight:bold;color:black; margin-top: -20px; margin-left: 10px;border-bottom: 2px solid;">
                    INSPECCIÓN DEL LUGAR
                </span>
        <br>
        <div style="margin-left: 10px">
            <span style="font-size:9pt;font-weight:bold;color:black">PLAGA Y GRADO DE INFESTACIÓN:</span>
            <br>
            <div style="margin-left:0px;font-size:9pt;margin-top:0px;">
                ______________________________________________________<br>
                ______________________________________________________
            </div>
        </div>
        <div style="margin-left: 10px">
            <span style="font-size:9pt;font-weight:bold;color:black;">ÁREAS DE ANIDAMIENTO</span><br>
            <div style="font-size:9pt;margin-top:0px;">
                ______________________________________________________<br>
                ______________________________________________________<br>
                ______________________________________________________
            </div>
        </div>

        <div style="margin-left: 10px">
            <span style="font-size:9pt;font-weight:bold;color:black;">COMENTARIOS</span><br>
            <div style="font-size:9pt;margin-top:0px;">
                ______________________________________________________<br>
                ______________________________________________________<br>
                ______________________________________________________
            </div>
        </div>

    </div>

    <div style="width: 1px; display: inline-block; margin-top: 0px">

    </div>

    <div style="width: 358px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top">
                <span style="font-size:10pt;font-weight:bold;color:black; margin-left: 10px; border-bottom: 2px solid;">
                    CONDICIONES DEL LUGAR
                </span>
        <br>
        <span style="font-size:9pt;font-weight:bolder;color:black; margin-left: 10px">
                    CUMPLIÓ CON INDICACIONES
                </span>
        <br>
        <div style="margin-left:10px;font-size:9pt;margin-top:0px;">
            ______________________________________________________<br>
            ______________________________________________________
        </div>
        <span style="font-size:9pt;font-weight:bold;color:black; margin-left: 10px">ORDEN Y LIMPIEZA</span><br>
        <div style="margin-left:10px;font-size:9pt;margin-top:0px;">
            ______________________________________________________<br>
            ______________________________________________________
        </div>
        <span style="font-size:9pt;font-weight:bold;color:black; margin-left: 10px">ACCESOS RESTRINGIDOS</span><br>
        <div style="margin-left:10px;font-size:9pt;margin-top:0px;">
            ______________________________________________________<br>
            ______________________________________________________<br>
            ______________________________________________________
        </div>

        <div style="margin-left: 10px">
            <span style="font-size:9pt;font-weight:bold;color:black;">COMENTARIOS</span><br>
            <div style="font-size:9pt;margin-top:0px;">
                ______________________________________________________<br>
                ______________________________________________________<br>
                ______________________________________________________
            </div>
        </div>
    </div>

</div>
<br>
<div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; margin-top: -20px; border: solid 1px">

    <div style="width: 358px; display: inline-block; margin-top: 0px; vertical-align: top">

        <div style="border-color:black;margin-left:10px;">
                    <span style="font-size:10pt;font-weight:bold;color:black; border-bottom: 2px solid;">
                        CONTROL DE PLAGAS
                    </span>
        </div>
        <div style="border-color:black;margin-left:10px">
            <span style="font-size:9pt;font-weight:bold;color:black">ÁREA A CONTROLAR</span><br>
            <div style="margin-left:0px;font-size:9pt; margin-top: 0px">
                ______________________________________________________<br>
                ______________________________________________________<br>
                ______________________________________________________
            </div>
        </div>

        <div style="margin-left: 10px">
            <span style="font-size:9pt;font-weight:bold;color:black;">COMENTARIOS</span><br>
            <div style="font-size:9pt;margin-top:0px;">
                ______________________________________________________<br>
                ______________________________________________________<br>
                ______________________________________________________
            </div>
        </div>

    </div>

    <div style="width: 1px; display: inline-block; margin-top: 0px">

    </div>

    <div style="width: 358px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top">

        <div style="border-color:black;margin-left:10px">
            <br>
            <span style="font-size:9pt;font-weight:bold;color:black;">MÉTODO DE APLICACIÓN</span><br>
            <div style="margin-left:0px;font-size:9pt;margin-top: 0px;">
                ______________________________________________________<br>
                ______________________________________________________
            </div>
        </div>
    </div>

</div>
<br>
<div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; margin-top: -20px; border: solid 1px">

    <div style="width: 725px; display: inline-block; margin-top: 0px; vertical-align: top">

        <div style="margin-left:8px;font-size:9pt">
            <table>
                <thead>
                <tr>
                    <td style="color:black;font-weight:bold">PLAGUICIDA APLICADO</td>
                    <td style="color:black;font-weight:bold">REGISTRO PLAGUICIDA</td>
                    <td style="color:black;font-weight:bold">INGREDIENTE</td>
                    <td style="color:black;font-weight:bold">DOSIS</td>
                    <td style="color:black;font-weight:bold">CANTIDAD</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                </tr>
                <tr>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                </tr>
                <tr>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                </tr>
                <tr>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                </tr>
                <tr>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                    <td style="font-size:8pt">______________________</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>

    <div style="width: 1px; display: inline-block; margin-top: 0px">

    </div>

    <div style="width: 1px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top">

    </div>

</div>

<div style="margin-left: 20px; margin-right: 18px; padding-left: 0; margin-top: 5px; border: solid 1px">

    <div style="width: 725px; display: inline-block; margin-top: 0; vertical-align: top; text-align: center">

         <span style="font-size:10pt;font-weight:bold;color:black;">
            EVIDENCIAS
        </span>
        <br><br><br><br><br><br><br><br><br><br><br>

    </div>

</div>

<br>
{{--//Footer--}}
<footer style="position: absolute; bottom: 15px;">

    <div style="margin-left: 20px; margin-right: 18px; padding-left: 0px; margin-top: 0px; border-bottom: solid 1px;">
        @if($jobCenterProfile->id == 291)
            <div style="text-align: center">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/conocer.jpg" alt="logo" width="100px" height="50px">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/tif-sqf.png" alt="logo" width="100px" height="50px">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/sedex.jpg" alt="logo" width="50px" height="50px">
            </div>
        @endif
        <div style="width: 372.5px; display: inline-block; margin-top: 5px; vertical-align: top; text-align: center;">
            <div style="display: inline-block">
                <p style="text-align:center;font-size:6pt; margin-top: 0; margin-bottom: 0">_____________________</p>
                <p style="text-align:center;font-size:6pt;font-weight:bold; margin-bottom: 0; margin-top: 0">Firma del Cliente</p>
                <br>
                <p style="text-align:center;font-size:6pt;font-weight:bold; margin-top: 0; margin-bottom: 0">{{$order->cliente}}</p>
                <span style="text-align:center;font-size:6pt;">@if($addressProfile) {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. @endif <?php echo date("d/m/y",strtotime($order->initial_date))?> a
                            las <?php echo date("H:i",strtotime($order->final_event))?></span>
                <div style="text-align:justify;font-size:5px; margin-bottom: 10px; margin-left: 6px; margin-right: 6px">
                    Firmo de conformidad y a mi entera satisfacción de haber recibido el Servicio de fumigación, acepto que se me
                    explicaron las indicaciones que debo realizar antes de ingresar al lugar fumigado, así como las precauciones a tomar.
                    Conozco los términos y funcionamiento de la garantía del servicio.
                </div>
            </div>
        </div>

        <div style="width: 202px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top; text-align: center;margin-right: -5px;">
            <p style="text-align:center;font-size:6pt; margin-top: 0; margin-bottom: 0">_____________________</p>
            <p style="text-align:center;font-size:6pt;font-weight:bold; margin-bottom: 0; margin-top: 0">Firma del técnico</p><br>
        </div>

        <div style="width: 165px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: middle; text-align: center; margin-right: -5px;">
            <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_sello }}" width="80px">
        </div>

    </div>

</footer>

</body>
</html>