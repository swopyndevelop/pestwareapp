<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Certificado de Servicio</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
        font-family: 'Arial Black', Helvetica, Arial, sans-serif
	}
    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }
    caption, td, th {
        padding: 0.3em;
    }
    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }
    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
        {{--//headers--}}
        <div style="margin: 0px; border: 1px solid; height: 200px; background-color: #16337a">
                <div style="width: 350px; display: inline-block; margin-left: 10px">
                    <p style="margin-top: 0px; margin-bottom: 0; color: white">
                        Logo
                    </p>
                </div>
                <div style="width: 650px; display: inline-block;">
                    <p style="margin-top: 80px; margin-bottom: 0; color: #6fae32; font-size: 35px; font-weight: bold">
                       SERVICIOS COMPLEMENTARIOS DE SANIDAD AMBIENTAL Y CONTROL DE PLAGAS URBANAS
                    </p>
                    <p style="margin-top: 15px; margin-bottom: 0; margin-left: 100px;color: white; font-size: 8pt">
                        Permiso Funcionamiento:
                    </p>
                    <p style="margin-top: 0px; margin-bottom: 0; margin-left: 150px;color: white; font-size: 8pt">
                        Correo Electrónico:
                    </p>
                </div>
        </div>

        {{--//Body--}}
        <div style="">

            <div style="width: 1100px; text-align: center">
                <p style="color: #16337a; font-size: 30pt; font-weight: bold; margin-bottom: 0px">CERTIFICADO DE SANITIZACIÓN</p>
            </div>

            <div style="width: 700px; text-align: center; margin-left: 300px">
                <p style="color: #16337a; font-size: 10pt; margin-bottom: 0px">De conformidad con la política de Buenas Prácticas de Higiene de este local, certificamos haber realizado
                el servicio complementario de Sanida Ambiental y Control de Plagas Urbanas con las actividades siguientes:</p>
            </div>

            <div style="width: 350px; display: inline-block; margin-left: 400px">
                <div style="width: 20px; height: 20px; background: white; border: 1px solid #16337a; margin-top: 30px; display: inline-block"></div>
                <div style="display: inline-block; margin-top: 30px;">
                    <p style="margin-top: 0px; margin-bottom: 0px; color: #16337a">Desinsectación</p>
                </div>
                <br>
                <div style="width: 20px; height: 20px; background: white; border: 1px solid #16337a; margin-top: 20px; display: inline-block"></div>
                <div style="display: inline-block; margin-top: 20px;">
                    <p style="margin-top: 0px; margin-bottom: 0px; color: #16337a">Desratización</p>
                </div>
                <br>
                <div style="width: 20px; height: 20px; background: white; border: 1px solid #16337a; margin-top: 15px; display: inline-block"></div>
                <div style="display: inline-block; margin-top: 15px;">
                    <p style="margin-top: 0px; margin-bottom: 0px; color: #16337a">Desinfección</p>
                </div>
            </div>

            <div style="width: 350px; display: inline-block;">
                <div style="width: 20px; height: 20px; background: white; border: 1px solid #16337a; margin-top: 30px; display: inline-block"></div>
                <div style="display: inline-block; margin-top: 30px;">
                    <p style="margin-top: 0px; margin-bottom: 0px; color: #16337a">Erradicación de aves plaga</p>
                </div>
                <br>
                <div style="width: 20px; height: 20px; background: white; border: 1px solid #16337a; margin-top: 20px; display: inline-block"></div>
                <div style="display: inline-block; margin-top: 20px;">
                    <p style="margin-top: 0px; margin-bottom: 0px; color: #16337a">Ventilación y mejora de ambientes</p>
                </div>
                <br>
                <div style="width: 20px; height: 20px; background: white; border: 1px solid #16337a; margin-top: 15px; display: inline-block"></div>
                <div style="display: inline-block; margin-top: 15px;">
                    <p style="margin-top: 0px; margin-bottom: 0px; color: #16337a">Otros</p>
                </div>
            </div>

            <br><br>

            <div style="width: 550px; display: inline-block">
                <div style="background-color: #16337a; height: 30px; width: 350px; margin-left: 200px; text-align: center">
                    <p style="color: white; font-size: 15pt; margin-bottom: 0px">Datos del cliente</p>
                </div>

                <div style="background-color: #6fae32; height: 20px; width: 150px; margin-left: 200px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Cliente</p>
                </div>
                <div style="background-color: #cbcdca; height: 20px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>
                <br>
                <div style="background-color: #6fae32; height: 20px; width: 150px; margin-left: 200px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Dirección</p>
                </div>
                <div style="background-color: white; height: 20px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>
                <br>
                <div style="background-color: #6fae32; height: 20px; width: 150px; margin-left: 200px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Giro del Negocio</p>
                </div>
                <div style="background-color: #cbcdca; height: 20px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>
                <br>
                <div style="background-color: #6fae32; height: 20px; width: 150px; margin-left: 200px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Área intervenida</p>
                </div>
                <div style="background-color: white; height: 20px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>
                <br>
                <div style="background-color: #6fae32; height: 20px; width: 150px; margin-left: 200px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Fecha de servicio</p>
                </div>
                <div style="background-color: #cbcdca; height: 20px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>
                <br>
                <div style="background-color: #6fae32; height: 20px; width: 150px; margin-left: 200px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Vigencia</p>
                </div>
                <div style="background-color: white; height: 20px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>

            </div>

            <div style="width: 400px; display: inline-block; margin-left: 80px">
                <div style="background-color: #16337a; height: 30px; width: 350px; text-align: center">
                    <p style="color: white; font-size: 15pt; margin-bottom: 3px">Datos de la intervención</p>
                </div>

                <div style="background-color: #6fae32; height: 40px; width: 150px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Vectores</p>
                </div>
                <div style="background-color: #cbcdca; height: 40px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>
                <br>
                <div style="background-color: #6fae32; height: 40px; width: 150px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Tratamientos</p>
                </div>
                <div style="background-color: white; height: 40px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>
                <br>
                <div style="background-color: #6fae32; height: 40px; width: 150px; display: inline-block">
                    <p style="color: white; font-size: 10pt; margin-top: 0px">Productos biocidas utilizados</p>
                </div>
                <div style="background-color: #cbcdca; height: 40px; width: 200px; display: inline-block; margin-left: -4px">
                    <p style="color: black; font-size: 9pt; margin-top: 0px"></p>
                </div>
            </div>

        </div>

        {{--//Footer--}}
        <footer style="position: absolute; bottom: 15px;">

        </footer>
</body>
</html>