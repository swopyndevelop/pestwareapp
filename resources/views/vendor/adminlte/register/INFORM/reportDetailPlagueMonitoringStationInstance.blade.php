<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>Reporte Gráfico</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
	}
    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }
    caption, td, th {
        padding: 0.3em;
    }
    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }
    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
        {{--//headers--}}
        <div style="margin: 0px;">
            <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

                <div style="text-align: center; margin-bottom: 0px; margin-top: 20px;">
                    <h2 style="font-weight: bold; margin-bottom: 0px">Reporte Gráfico de de Monitoreo de Estaciones</h2>
                    <br>
                </div>

                <div style="width: 130px; display: inline-block; vertical-align: top;">
                    @if($image->pdf_logo == null)
                        <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
                    @else
                        <img src="{{ env('URL_STORAGE_FTP').$image->pdf_logo }}" alt="logo" width="130px">
                    @endif
                </div>

                <div style="width: 490px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
                    <p style="margin-top: 0px; margin-bottom: 0">
                        <span style="color:black;font-size:9.5pt">Responsable Sanitario: {{$jobCenterProfile->health_manager}} | Licencia No: {{$jobCenterProfile->license}}
                            | Tel: {{$jobCenterProfile->whatsapp_personal}} | Razón Social: {{ $jobCenterProfile->business_name }} |RFC: {{ $jobCenterProfile->rfc }} | {{ $addressProfile->street }} #{{ $addressProfile->num_ext }}, {{ $addressProfile->location }},
                            {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. | Email: {{ $jobCenterProfile->email_personal }} | Facebook: {{$jobCenterProfile->facebook_personal}}.
                        </span>
                    </p>
                </div>
                <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
                    @if($qrcode != null)
                        <img src="data:image/png;base64, {!! $qrcode !!}" width="100px"><br>
                        <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Licencia Sanitaria</span>
                    @endif
                </div>
            </div>
        </div>
        {{--//Body--}}
        <div style="margin-left:20px;font-size:9pt; text-align: left;">
            <div style="width: 578px; display: inline-block; margin-top: 5px; vertical-align: top; text-align: left;">
                <p style="margin-top: 0px; margin-bottom: 7px">
                    <span style="font-size:11pt; font-weight: bold"> Atención a {{$customer->name}} @if($customer->establishment_name != null) | {{ $customer->establishment_name }} @endif</span><br>
                    <span style="font-size:7pt">{{$customer->address}} #{{$customer->address_number}}, {{$customer->colony}}, {{$customer->municipality}}, {{$customer->state}}
                | Tel: {{$customer->cellphone}} | {{$customer->email}}</span>
                </p>
                <p style="margin-top: 0px; margin-bottom: 0px">
                    <span style="font-size:11pt; font-weight: bold"> Periodo: {{ $period }}</span><br>
                    <span style="font-size:11pt; font-weight: bold"> Tipo: {{ $title }}</span>
                </p>
            </div>

            <div style="width: 150px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: bottom; text-align: right;margin-right: 20px;">
                <p style="margin: 0px; font-weight: bold">
                    General <br>
                    Todas las zonas <br>
                    Todos los Perímetros
                </p>
            </div>

        </div>
        <br><br>
        {{--//Charts--}}
        <div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; margin-top: -20px;">
            <img src="{{ env('URL_BASE_CHARTS') }}{{ $station }}Line-{{ $nameImage }}.png" width="100%" height="300px">
        </div>

        <div style="border-color:black;margin-left:20px; margin-right: 22px; text-align: right; font-size: 7pt">
            <p style="margin: 0px">
                <span>
                Gráfica de Tendencia de Consumo
                </span>
            </p>
        </div>
        <br>
        <br><br>
        <div style="border-color:black;margin-right:20px;margin-left:20px;font-size:9pt; text-align: center">
            <div style="width: 375px; display: inline-block; margin-top: 5px; vertical-align: top; text-align: center;">
                @if($station == "Baits")
                <img src="{{ env('URL_BASE_CHARTS') }}{{ $station }}Consume-{{ $nameImage }}.png" width="100%" height="200px">
                @endif
            </div>

            <div style="width: 375px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top; text-align: center;margin-right: -5px;">
                <img src="{{ env('URL_BASE_CHARTS') }}{{ $station }}Conditions-{{ $nameImage }}.png" width="100%" height="200px">
            </div>

        </div>
        <br>
        {{--//Footer--}}
        <footer style="position: absolute; bottom: 5px;">

        </footer>
</body>
</html>