<!-- INICIA MODAL PARA CREAR GARANTÍA -->
<div class="modal fade" id="warranty" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
				<div class="modal-body" style="text-align: center;">
					<h4 class="modal-title">¿Está seguro de crear una garantía?</h4>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<div class="text-center">
						<button type="button" title="Aceptar" class="btn btn-primary" id="saveWarrantyOrder"><strong>Aceptar</strong></button>
					</div>
				</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA CREAR GARANTÍA -->
