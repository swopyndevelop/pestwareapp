<!-- INICIA MODAL PARA ENVIAR EMAIL SERVICIO -->
<div class="modal fade" id="sendReminderWhatsappSchedule" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body">
                <h4>Enviar recordatorio por WhatsApp:</h4>
                <h5 for="email" style="font-weight: bold;">Cliente:</h5>
                <input type="text" name="cellphoneClient" id="cellphoneClient" class="form-control">
                <h5 for="email" style="font-weight: bold;">Principal:</h5>
                <input type="text" name="cellphoneMain" id="cellphoneMain" class="form-control">
                <h5 for="email" style="font-weight: bold;">Otro:</h5>
                <input type="text" name="cellphoneOther" id="cellphoneOther" class="form-control">
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-primary btn-lg" type="button" id="sendReminderWhatsappSave"
                    name="sendReminderWhatsappSave">Enviar</button>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL SERVICIO -->