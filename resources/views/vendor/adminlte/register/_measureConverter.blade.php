<!-- START MODAL DE CONVERTIDOR DE MEDIDAS -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="MC" role="dialog" aria-labelledby="newEmployeesModal">
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h3 style="color: black" class="modal-title text-center col-lg-12" id="modalTitle">Convertidor de medidas</h3>
                  <div class="form-group row col-lg-4">
                    <label class="control-label" for="name">Fecha</label>
                    <br>
                    <label class="control-label" for="name">12/12/2018</label>
                  </div>
                  <div class="form-group row col-lg-push-6 col-lg-5">
                    <label class="control-label col-lg-push-12" for="name">Número de cotización</label>
                    <br>
                    <label class="control-label col-lg-push-8" for="name">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; C-124869</label>
                  </div>
                </div>
              </div>

              <div class="box-body">
                <div class="row">

                  <div class="col-lg-push-1 col-md-6">
                    <label class="control-label" for="name">Nombre del cliente*: </label>
                    <br>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="customer" name="customer">
                    </div>

                    <br><br>

                    <label class="control-label" for="name">Celular*: </label>
                    <br>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="cellphone" name="cellphone">
                    </div>

                    <br><br>

                    <label class="control-label" for="name">Tipo de servicio*: </label>
                    <br>
                    <div class="col-sm-8">
                      <select name="establishment" id="establishment" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;"></select>
                    </div>

                    <br><br>

                    <label class="control-label" for="name">Tipo de plaga*: </label>
                    <br>
                    <div class="col-sm-8">
                      <select name="plague" id="plague" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;"></select>
                    </div>

                    <br><br>

                    <label class="control-label" for="name">Medidas de construcción*: </label>
                    <br>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="construction_measure" name="construction_measure">
                    </div>

                    <br><br>

                    <label class="control-label" for="name">Medidas de jardín: </label>
                    <br>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="garden_measure" name="garden_measure">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <label class="control-label" for="name">Empresa: </label>
                    <br>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="establishmentName" name="establishmentName">
                    </div>

                    <br><br>

                    <label class="control-label" for="name">Correo electrónico: </label>
                    <br>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="email" name="email">
                    </div>

                    <br><br>

                    <label class="control-label" for="name">Colonia*: </label>
                    <br>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="colony" name="colony">
                    </div>

                    <br><br>

                    <label class="control-label" for="name">Municipio*: </label>
                    <br>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="municipality" name="municipality">
                    </div>

                    <br><br>

                    <div class="col-sm-6">
                      <button class="btn btn-info btn-sm" border-color: black;" type="button" id="" name="">Convertidor de <br> medidas</button>
                    </div>
                    <div class="col-sm-6">
                      <button class="btn btn-info btn-sm" border-color: black;" type="button" id="" name="">Convertidor de <br> escuelas</button>
                    </div>
                    <br><br><br><br>
                    <div class="col-sm-6">
                      <button class="btn btn-info btn-sm" border-color: black;" type="button" id="" name="">Convertidor de <br> espacios</button>
                    </div>
                    <div class="col-sm-6">
                      <button class="btn btn-info btn-sm" border-color: black;" type="button" id="" name="">Cotización de <br> personalizada</button>
                    </div>

                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <div class="text-center">

                  <div class="col-sm-9">
                    <div class="col-md-4 col-lg-push-8">
                      <label class="control-label" for="name">Descuento: </label>
                      <select name="" id="" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;"></select>
                    </div>
                  </div>
                  <br>
                  <button class="btn btn-primary btn-lg" style="background-color: black; border-color: black;" type="button" id="newQuotation" name="newQuotation">Cotizar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- END MODAL DE CONVERTIDOR DE MEDIDAS-->
