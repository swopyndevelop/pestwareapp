<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalEditQuotationCustom" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Cotización Personalizada</h4>
                            </div>

                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <textarea class="form-control" name="descriptionE" id="descriptionE" cols="30" rows="3">{{ $quotation['quotation']->description }}</textarea>
                                        <br>
                                    </div>

                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-responsive-md table-sm table-bordered" id="makeEditable">
                                             <thead>
                                              <tr>
                                                <th>Concepto</th>
                                                <th>Cantidad</th>
                                                <th>Frec. de aplicación / mes</th>
                                                <th>Plazo en contrato en meses</th>
                                                <th>Precio unitario</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($quotation['customQuotations'] as $customQuotation)
                                                  <tr>
                                                    <td>{{ $customQuotation->concept }}</td>
                                                    <td>{{ $customQuotation->quantity }}</td>
                                                    <td>{{ $customQuotation->frecuency_month }}</td>
                                                    <td>{{ $customQuotation->term_month }}</td>
                                                    <td>{{ $customQuotation->unit_price }}</td>
                                                  </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <span style="float:right"><button id="but_addE" class="btn btn-danger">Agregar Fila</button></span>
                                    </div>

                                </div>
                            </div>

                                <div class="modal-footer">
                                    <div class="text-center">
                                        <div class="row">
                                            <div class="col-md-push-8 col-sm-2 text-right">
                                                <label class="control-label" for="sumaSubtotal" style="font-weight: bold; font-size: large; margin-bottom: 10px;">Suma subtotal</label>
                                                <label class="control-label" for="discountC" style="font-weight: bold; font-size: large; margin-bottom: 10px;">Descuento</label><br>
                                                <label class="control-label" for="totalC" style="font-weight: bold; font-size: large;">Total</label>
                                            </div>

                                            <div class="col-sm-push-8 col-sm-2">
                                                <input type="text" id="sumaSubtotalE" class="form-control" disabled="disabled" style="font-weight: bold;" value="{{ $quotation['quotation']->price or old('sumaSubtotalE') }}">
                                                <select name="discountCE" id="discountCE" class="form-control">
                                                    <option value="{{ $quotation['quotation']->percentage }}" selected="true" >
                                                        {{ $quotation['quotation']->discount_name }} / {{ $quotation['quotation']->percentage }}%
                                                    </option>
                                                    @foreach($discounts as $discount)
                                                        @if($quotation['quotation']->discount_id != $discount->id)
                                                            <option value="{{ $discount->percentage}}"
                                                                    {{ (in_array($discount, old('discounts', []))) }} >{{ $discount->title }} / {{ $discount->percentage }}%
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <input type="text" id="totalE" class="form-control" style="font-weight: bold; font-size: large;" disabled="disabled" value="{{ $quotation['quotation']->total or old('totalE') }}">
                                                <br>
                                                <button class="btn btn-secondary btn-md" type="button" id="calcularE" name="calcularE">Calcular</button>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary" type="button" id="saveQuotationCustomNewE" name="saveQuotationCustomNewE">Guardar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE COTIZACION -->
