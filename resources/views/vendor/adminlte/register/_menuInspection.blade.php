<ul class="dropdown-menu">
	@if($inspection->status == 4)
	<li data-toggle="tooltip" data-placement="bottom" title="">
		<a href="{{ Route('order_view', \Vinkla\Hashids\Facades\Hashids::encode($inspection->id))}}" target="_blank" style="cursor: pointer">
			<i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i> Ver
		</a>
	</li>
	<li data-toggle="tooltip" data-placement="bottom" title="">
		<a href="{{Route('pdf_order_service', \Vinkla\Hashids\Facades\Hashids::encode($inspection->id))}}" target="_blank" style="cursor: pointer">
			<i class="fa fa-file-text-o" aria-hidden="true" style="color: red;"></i> Descargar
		</a>
	</li>
	<li data-toggle="tooltip" data-placement="bottom" title="">
		<a href="{{ route('index_register') }}" style="cursor: pointer">
			<i class="fa glyphicon glyphicon-plus" aria-hidden="true" style="color: #2E86C1;"></i> Crear Cotización
		</a>
	</li>
	@endif
</ul>
