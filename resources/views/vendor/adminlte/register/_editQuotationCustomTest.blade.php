<!-- START MODAL CUSTOM QUOTE -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalQuotationCustomTest" role="dialog" aria-labelledby="ModalQuotationCustomTest" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important; height: 90% !important;">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Cotización Personalizada <b>v2</b></h4>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-4">
                                                <select id="descriptions" class="form-control">
                                                    <option value="0" selected>Personalizada</option>
                                                    @foreach($descriptionsForCustomQuotes as $description)
                                                        <option value="{{ $description->id }}">{{ $description->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-secondary btn-sm" id="addDescription"><i class="fa fa-commenting-o" aria-hidden="true"></i> Guardar Descripción</button>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="descriptionTest" cols="30" rows="3" placeholder="Descripción del servicio"></textarea>
                                        <br>
                                    </div>

                                    <!-- Start New version -->
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-responsive-md table-sm table-bordered">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <select id="type" class="form-control">
                                                        <option value="1">Servicio</option>
                                                        <option value="2">Contable</option>
                                                        <option value="3">Sucursal</option>
                                                    </select>
                                                </th>
                                                <th>
                                                    <input type="text" class="form-control" id="conceptTest" placeholder="Concepto">
                                                </th>
                                                <th>
                                                    <input type="text" class="form-control" id="quantityTest" placeholder="Cantidad">
                                                </th>
                                                <th>
                                                    <input type="text" class="form-control" id="monthTest" placeholder="Frec. de aplicación / mes">
                                                </th>
                                                <th>
                                                    <input type="text" class="form-control" id="contractTest" placeholder="Plazo en contrato en meses">
                                                </th>
                                                <th>
                                                    <input type="text" class="form-control" id="priceTest" placeholder="Precio unitario">
                                                </th>
                                                <th id="actionsHeader">
                                                    <button class="btn btn-primary" id="addTest">Agregar</button>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-responsive-md table-sm table-bordered table-hover" id="customTable">
                                            <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Concepto</th>
                                                <th>Cantidad</th>
                                                <th>Frec. de aplicación / mes</th>
                                                <th>Plazo en contrato en meses</th>
                                                <th>Precio unitario</th>
                                                <th>Subtotal</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- End New version -->

                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">Subtotal: <b id="subtotalTest"></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <label class="control-label text-primary" for="total" style="font-size: 1.5em;">Total: <b id="totalTest"></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelEditProfileDiv">Cancelar</button>
                                        <button class="btn btn-primary" type="button" id="saveQuotationCustomNewTest">Editar</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('.vendor.adminlte.register._modalNewDescriptionCustomQuote')
<!-- END MODAL CUSTOM QUOTE -->
