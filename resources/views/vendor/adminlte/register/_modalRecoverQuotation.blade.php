<!-- INICIA MODAL PARA RECUPERAR COTIZACIÓN -->
<div class="modal fade" id="status<?php echo $q->id; ?>" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<form action="{{ route('recover', ['quotation' => $q->id]) }}" method="POST" id="formRecover<?php echo $q->id; ?>">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<div class="modal-body">
					<h4 class="modal-title">¿Está seguro de que desea recuperar la cotización?</h4>
				<div class="col-md-8 col-md-offset-2">	

				</div>
		</div>
		<!-- Modal footer -->
		<div class="modal-footer">										
			<button type="submit" title="Aceptar" style="background-color: green; border-color: green;" class="btn btn-info" data-toggle="modal" data-target="#status<?php echo $q->id; ?>"><strong>Aceptar</strong></button>
		</form>	
		</div>
	</div>
	</div>
</div>
<!-- TERMINA MODAL PARA RECUPERAR COTIZACIÓN -->