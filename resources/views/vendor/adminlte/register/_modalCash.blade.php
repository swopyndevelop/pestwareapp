<!--START REGISTER CASHE-->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="newCashModal" role="dialog" aria-labelledby="newCashModal">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <br>
                            <form action="{{ Route('cash_add') }}" method="POST" id="formCashService">
                                {{ csrf_field() }}

                                <div class="box-body">
                                <input type="hidden" class="form-control" id="idEventCasheModal" name="idEventCasheModal">
                                <input type="hidden" class="form-control" id="idOrderCasheModal" name="idOrderCasheModal">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Importe a cobrar</h4>
                                    <h2 class="modal-title text-center col-lg-12 text-success" id="labelTotalCashModal"></h2>
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <br><br>
                                        <label class="control-label col-sm-3" for="name"># Servicio: </label>
                                        <div class="col-sm-9">
                                        <input type="text" class="form-control" id="idSo" name="idSo" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="company">Método de Pago*:</label>
                                        <div class="col-sm-9">
                                            <select name="paymentMethod" id="paymentMethod" class="form-control" style="width: 100%; font-weight: bold;" required>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <label class="control-label col-sm-3" for="company">Forma de Pago*:</label>
                                        <div class="col-sm-9">
                                            <select name="paymentType" id="paymentType" class="applicantsList-single3 form-control" style="width: 100%; font-weight: bold;" required>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row col-lg-push-1 col-lg-11 formulario__grupo" id="grupo__total">
                                        <label class="control-label col-sm-3" for="total">Importe Recibido*: </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control formulario__input" id="total" name="total" placeholder="Ejemplo: 10.00">
                                            <i class="formulario__validacion-estado-label fa fa-times-circle"></i>
                                        </div>
                                        <p class="formulario__input-error text-center">Solo se aceptan números y decimales.</p>
                                    </div>

                                    <div class="form-group row col-lg-push-1 col-lg-11 formulario__grupo" id="grupo__payDay" style="display: none">
                                        <label class="control-label col-sm-3" for="payDay">Fecha Pago: </label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control formulario__input" id="payDay" name="payDay">
                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                        </div>
                                    </div>

                                    <div class="form-group row col-lg-push-1 col-lg-11 formulario__grupo" id="grupo__commentary">
                                        <label class="control-label col-sm-3" for="company">Comentarios: </label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control formulario__input" id="commentary" name="commentary"></textarea>
                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                        </div>
                                        <p class="formulario__input-error">Los Comentarios deben ser de 3 a 250 caracteres.</p>
                                    </div>

                                    <div class="form-group row col-lg-push-1 col-lg-11">
                                        <div class="col-sm-9">
                                            <input type="checkbox" name="payment" id="payment" value="1">
                                            <label class="control-label" for="payment"> El cliente no pago</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">

                                    <div class="col-sm-12 formulario__mensaje" id="formulario__mensaje" style="margin: 10px; margin-right: 10px;">
                                        <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                    </div>
                                    <br>
                                    <div class="col-sm-12 text-center formulario__grupo formulario__grupo-btn-enviar">
                                        <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                    </div>

                                    <button class="btn btn-primary btn-md" type="submit" id="charge">Cobrar</button>
                                </div>
                                <br>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
