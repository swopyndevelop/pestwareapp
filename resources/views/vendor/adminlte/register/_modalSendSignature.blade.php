<!-- INICIA MODAL PARA ENVIAR EMAIL SERVICIO WHATSAPP -->
<div class="modal fade" id="sendRequestSignature" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="box-body">
        <h4 class="modal-title text-center col-lg-12 text-info">Solicitud Firma</h4>
        <label for="kigSignature" style="font-weight: bold;">Link Firma:</label>
        <a href="" id="hrefRequestSignature" target="_blank">
          <label  for="" style="font-weight: bold; cursor: pointer">Firma Cliente</label>
        </a>
        <br>
        <label for="kigSignature" style="font-weight: bold;">Enviar a Whatsapp:</label>
       <input type="text" name="cellphoneCustomerSignature" id="cellphoneCustomerSignature" class="form-control">
        <br>
        <button class="btn btn-primary btn-sm" type="button" id="hrefSendRequestSignature">
          <i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i> Whatsapp
        </button>

      </div>
      <div class="modal-footer">
        <div class="text-center">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL SERVICIO WHATSAPP-->