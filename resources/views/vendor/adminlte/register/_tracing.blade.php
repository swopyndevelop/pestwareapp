<!-- START MODAL DE SEGUIMIENTO -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="tracingQ" role="dialog" aria-labelledby="tracingModal">
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Seguimiento <b id="quotTracingKey"></b></h4>
                <input type="hidden" id="idQuotationT" name="idQuotationT">
              </div>

              <div class="box-body">
                <div class="row">
                  <div class="col-md-12 text-left">
                    <h4 style="text-align:center">Próxima llamada</h4>
                    <div class="col-md-6">
                      <label style="font-weight: bold;" class="control-label" for="dateTracing">Fecha de seguimiento: </label>
                      <br>
                      <input style="padding: 10px;" class="formulario__input form-control" type="date" name="dateTracing" id="dateTracing" value="{{ \Carbon\Carbon::now()->toDateString() }}">
                    </div>
                    <div class="col-md-6">
                      <label style="font-weight: bold;" class="control-label" for="hourTracing">Hora: </label>
                      <br>
                      <input style="padding: 10px;" class="formulario__input form-control" type="time" name="hourTracing" id="hourTracing" value="{{ \Carbon\Carbon::now()->timezone($timezone)->toTimeString() }}">
                    </div>
                    <div class="col-md-12 formulario__grupo" id="grupo__commentsT">
                      <br>
                      <div>
                        <span style="font-weight: bold;">Comentarios:</span>
                        <textarea class="formulario__input form-control" name="commentsT" id="commentsT" cols="30" rows="3" placeholder="Descripción del servicio"></textarea>
                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                      </div>
                      <p class="formulario__input-error">Los Comentarios deben ser de 3 a 250 caracteres.</p>
                    </div>
                    <br>
                    <div class="formulario__mensaje" id="formulario__mensaje_tracing">
                      <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje_tracing"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                      <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                    </div>
                  </div>
                  <div class="col-md-12 text-center">
                    <br>
                    <button class="btn btn-primary" type="button" id="saveQuotationTracing">Guardar Seguimiento</button>
                  </div>
                  <div class="col-md-12 container-fluid">
                    <br>
                    <table class="table table-hover">
                      <thead>
                        <tr class="bg-primary">
                          <th class="text-center">#</th>
                          <th class="text-center">Fecha</th>
                          <th class="text-center">Hora</th>
                          <th class="text-center">Comentarios</th>
                        </tr>
                      </thead>
                      <tbody id="tableBodyDetail">

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="modal-footer">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL DE SEGUIMIENTO -->
