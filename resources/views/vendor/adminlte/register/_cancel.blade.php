@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE COTIZACION -->
<div class="content container-fluid spark-screen">
  <div class="modal fade" id="cancelQ" role="dialog" aria-labelledby="cancelModal" tabindex="-1" >
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <h3 class="modal-title text-center col-lg-12" id="modalTitle">Cancelar Refuerzo / Servicio</h3>
              </div>

              <div class="box-body">
                <div class="row">

                  <div class="col-lg-push-1 col-md-6">
                  <input type="hidden" id="idQuotation" name="idQuotation" value="{{$quotation->id}}">
                    <label class="control-label" for="name">Motivo: </label>
                    <br>
                    <div class="col-sm-8">
                      <select name="reason" id="reason" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;">
                        <option value="" disabled selected>Seleccione el motivo</option>
                        <option value="Precio Alto">Precio Alto</option>
                        <option value="No le gusto el servicio" >No le gusto el servicio</option>
                        <option value="No funciono el tratamiento">No funciono el tratamiento</option>
                        <option value="Otro">Otro</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-lg-push-1 col-md-12">
                    <label class="control-label" for="commentary">Comentarios del cliente: </label>
                    <br>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="commentary" id="commmentary" cols="30" rows="3" placeholder="Descripción del servicio"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <div class="text-center">
                  <div class="row">
                      <button class="btn btn-primary btn-sm" style="background-color: green; border-color: black;" type="button" id="SaveCancelC" name="SaveCancelC">Guardar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
  <script type="text/javascript" src="{{ URL::asset('js/build/quotation_cancel.js') }}"></script>
  <script>
    $("#cancelQ").modal();
    $("#cancelQ").on('hidden.bs.modal', function() {
      window.location.href = '{{route("index_register")}}'; //using a named route
    });
  </script>
@endsection
