<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalQuotationCustom" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 style="color: black" class="modal-title text-center col-lg-12" id="modalTitle">Cotización Personalizada</h3>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-control" name="description" id="description" cols="30" rows="3" placeholder="Descripción del servicio"></textarea>
                                        <br>
                                    </div>

                                    <div class="col-sm-2">
                                        <label class="control-label" for="concept">Concepto</label>
                                        <input type="text" id="concept" class="swal2-input">
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="control-label" for="quantity">Cantidad</label>
                                        <input min="1" type="number" id="quantity" class="swal2-input">
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="control-label" for="frequency">Frec. de aplicación / mes</label>
                                        <input min="0" type="number" id="frequency" class="swal2-input">
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="control-label" for="term">Plazo en contrato en meses</label>
                                        <input min="1" type="number" id="term" class="swal2-input">
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="control-label" for="priceUnit">Precio unitario</label>
                                        <input type="number" min="1" id="priceUnit" class="swal2-input">
                                    </div>
                                    <div class="col-sm-2">
                                        <br>
                                        <button class="btn btn-danger glyphicon glyphicon-plus" type="button" id="addConcept"></button>
                                        <br><br>
                                    </div>

                                    <div class="col-md-12">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">Concepto</th>
                                                <th scope="col">Cantidad</th>
                                                <th scope="col">Frecuencia de aplicación por mes</th>
                                                <th scope="col">Plazo en contrato en meses</th>
                                                <th scope="col">Precio unitario</th>
                                                <th scope="col">Subtotal</th>
                                            </tr>
                                            </thead>
                                            <tbody id="dataTh">

                                            </tbody>
                                        </table>
                                    </div>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-md-push-8 col-sm-2 text-right">
                                            <label class="control-label" for="sumaSubtotal" style="font-weight: bold; font-size: large; margin-bottom: 10px;">Suma subtotal</label>
                                            <label class="control-label" for="discountC" style="font-weight: bold; font-size: large; margin-bottom: 10px;">Descuento</label><br>
                                            <label class="control-label" for="total" style="font-weight: bold; font-size: large;">Total</label>
                                        </div>
                                        <div class="col-sm-push-8 col-sm-2">
                                            <input type="text" id="sumaSubtotal" class="form-control" disabled="disabled" style="font-weight: bold;">
                                            <select name="discountC" id="discountC" class="form-control">
                                                <option value="0">Sin descuento / 0%</option>
                                                @foreach($discounts as $discount)
                                                    @if($discount->id != 3)
                                                        <option value="{{ $discount->percentage }}">{{ $discount->title }} / {{ $discount->percentage }}%</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <input type="text" id="total" class="form-control" style="font-weight: bold; font-size: large;" disabled="disabled">
                                            <br>
                                            <button class="btn btn-primary btn-lg" style="background-color: black; border-color: black;" type="button" id="calcular" name="calcular">Calcular</button>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-lg" style="background-color: black; border-color: black;" type="button" id="saveQuotationCustom" name="saveQuotationCustom">Guardar</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END MODAL DE COTIZACION -->
