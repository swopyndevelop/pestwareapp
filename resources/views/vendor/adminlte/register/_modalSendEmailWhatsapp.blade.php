<!-- INICIA MODAL PARA ENVIAR EMAIL SERVICIO WHATSAPP -->
<div class="modal fade" id="sendemailwhatsapp" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="box-body">
        <h4 class="modal-title text-center col-lg-12 text-info">Enviar Certificado</h4>
        <label for="cellphoneServiceOrder" style="font-weight: bold;">Número:</label>
        <input type="text" id="cellphoneServiceOrder" name="cellphoneServiceOrder" class="form-control">
        <label for="cellphoneMainServiceOrder" style="font-weight: bold;">Número principal:</label>
        <br>
        <input type="text" name="cellphoneMainServiceOrder" id="cellphoneMainServiceOrder" class="form-control">
        <label for="cellphoneOtherServiceOrder" style="font-weight: bold;">Otro Número:</label>
        <br>
        <input type="text" name="cellphoneOtherServiceOrder" id="cellphoneOtherServiceOrder" class="form-control">

        <div class="col-md-12 text-center">
          <h4>Seleccionar Documentos</h4>
          <div class="custom-control col-md-6">
            <input type="checkbox" class="form-check-input" name="fileOrderWhatsapp" id="fileOrderWhatsapp" checked>
            <label class="custom-control-label" for="fileOrderWhatsapp">Orden de Servicio</label>
          </div>
          <div class="custom-control col-md-6">
            <input type="checkbox" class="form-check-input" name="fileCertificateWhatsapp" id="fileCertificateWhatsapp" checked>
            <label class="custom-control-label" for="fileCertificateWhatsapp">Certificado de Servicio</label>
          </div>

          <div class="custom-control col-md-6" id="showStationWhatsapp" style="display: none">
            <input type="checkbox" class="form-check-input" name="fileStationWhatsapp" id="fileStationWhatsapp">
            <label class="custom-control-label" for="fileStationWhatsapp">Inspección de Estaciones</label>
          </div>

          <div class="custom-control col-md-6" id="showAreaWhatsapp" style="display: none">
            <input type="checkbox" class="form-check-input" name="fileAreaWhatsapp" id="fileAreaWhatsapp">
            <label class="custom-control-label" for="fileAreaWhatsapp">Inspección de Áreas</label>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <div class="text-center">
          <button class="btn btn-primary btn-sm" type="button" id="sendEmailOrderWhatsapp" name="sendEmailOrderWhatsapp">
            Enviar
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL SERVICIO WHATSAPP-->