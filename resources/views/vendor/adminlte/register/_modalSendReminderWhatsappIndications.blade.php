<!-- INICIA MODAL PARA ENVIAR EMAIL SERVICIO -->
<div class="modal fade" id="sendReminderWhatsappIndications" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body">
                <h4>Enviar indicaciones por WhatsApp:</h4>
                <h5 for="email" style="font-weight: bold;">Cliente:</h5>
                <input type="text" name="cellphoneClient" id="cellphoneClientIndications" class="form-control">
                <h5 for="email" style="font-weight: bold;">Principal:</h5>
                <input type="text" name="cellphoneMain" id="cellphoneMainIndications" class="form-control">
                <h5 for="email" style="font-weight: bold;">Otro:</h5>
                <input type="text" name="cellphoneOther" id="cellphoneOtherIndications" class="form-control">
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-primary btn-lg" type="button" id="sendReminderWhatsappIndicationsSave"
                    name="sendReminderWhatsappIndicationsSave">Enviar</button>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA ENVIAR EMAIL SERVICIO -->