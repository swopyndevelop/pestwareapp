@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
@if(Entrust::can('Cotizaciones'))
<div class="content container-fluid spark-screen" id="courseevaluation-CompanyEvaluation">
    <div class="row">
        <div class="col-md-12">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
            <h3 class="box-title" id="callTitleIndex">Call Center</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
                </button>
            </div>
                <input type="hidden" id="totalQuotations" name="totalQuotations" value="{{ $totalQuotations }}">
                <input type="hidden" id="totalProgrammed" name="totalProgrammed" value="{{ $totalProgrammed }}">
                <input type="hidden" id="totalProgrammed2" name="totalProgrammed2" @if($indexLastQuot != null) value="{{ $quotation[$indexLastQuot]->id }}" @endif>
                <input type="hidden" id="methodFieldPost" value="{{ $totalProgrammed }}">
                <input type="hidden" id="introJsInput" value="{{ auth()->user()->introjs }}">
                <input type="hidden" id="idCompanyAuth" value="{{ auth()->user()->companie }}">
                <input type="hidden" id="codeCountry" value="{{ $countryCode }}">
                <input type="hidden" id="introUpgrade" value="{{ auth()->user()->intro_upgrade }}">
        </div>

        <div class="box-body">
            <div class="row">
                <div @if(auth()->user()->companie != 338) class="col-md-9" @else class="col-md-12" @endif>
                <div class="row">
                    <div class="col-md-4">
                        <select name="selectJobCenter" id="selectJobCenter" class="form-control" style="font-weight: bold;">
                            @foreach($jobCenters as $jobCenter)
                                @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                    <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                        {{ $jobCenter->name }}
                                    </option>
                                @else
                                <option value="{{ $jobCenter->id }}">
                                    {{ $jobCenter->name }}
                                </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 text-center">
                        <button type="submit" title="Cotización Lista de Precio" class="btn btn-primary newQuotation" id="newQuotation"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        <button type="submit" title="Cotización Personalizada" class="btn btn-secondary newQuotationCustom" id="newQuotationCustom" @if(Auth::user()->id_plan == 1) disabled @endif><i class="fa fa-wrench" aria-hidden="true"></i></button>
                        <a href="{{ route('calendario') }}" class="btn btn-primary-dark"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-md-5">
                        {!!Form::open(['method' => 'GET','route'=>'index_register'])!!}
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Buscar # Cotización" name="search" id="search">
                        <input type="hidden" id="introJsInpunt" value="{{ auth()->user()->introjs }}">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                        </span>
                    </div>
                    {!!Form::close()!!}
                  </div>
              </div>
              <br>
              <div class="row">
                    <div class="col-md-4 hidden-graphics" style="text-align: center;">
                        <a href="{{ route('index_register') }}" type="button" class="btn btn-block btn-primary-dark" id="tabQuotations">Cotizaciones</a>
                        <h1 @if($totalProgrammed <= 50) style="color: #2C3E50;" @elseif($totalProgrammed > 50 && $totalProgrammed < 70) style="color: #F1C40F;" @else style="color: #16A085;"@endif  id="percentageQuotations">{{ $percentage }}%</h1>
                        <div style="text-align: center;"><h3>{{ $totalProgrammed }} / {{ $totalQuotations }}</h3></div>
                        <div style="text-align: center;"><h5>Programados &nbsp;&nbsp; Prospectos</h5></div>
                    </div>
                    <div @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="top" title="Plan Emprendor y Empresarial" @endif class="col-md-4 hidden-graphics" style="text-align: center;">
                      <button @if(Auth::user()->id_plan == 1) disabled @endif type="button" class="btn btn-block btn-default" id="tabReinforcements">Refuerzos</button>
                      <h1 style="color: #2C3E50;">0%</h1>
                      <div style="text-align: center;"><h3>0 / 0</h3></div>
                      <div style="text-align: center;"><h5>Programados &nbsp;&nbsp; Prospectos</h5></div>
                    </div>
                    <div @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="top" title="Plan Emprendor y Empresarial" @endif class="col-md-4 hidden-graphics" style="text-align: center;">
                          <button @if(Auth::user()->id_plan == 1) disabled @endif type="button" class="btn btn-block btn-default" id="tabRecurrent">Recurrentes</button>
                          <h1 style="color: #2C3E50;">0%</h1>
                          <div style="text-align: center;"><h3>0 / 0</h3></div>
                          <div style="text-align: center;"><h5>Programados &nbsp;&nbsp; Prospectos</h5></div>
                    </div>
              </div>
              <br>
              <!-- Table Data  -->
              <div class="row" id="masterContainer">
                    <div class="col-md-12 table-responsive" id="tabContainer">
                    <table class="table table-hover text-center" id="tableQuotations">
                        <thead class="table-general" id="tableTheadIndexDiv">
                        <tr>
                            <th>Cotización</th>
                            <th>Fecha</th>
                            <th>Agente</th>
                            <th>Cliente/Contacto</th>
                            <th>@if($labels['colony'] != null) {{ $labels['colony']->label_spanish }} @else Colonia @endif/Municipio</th>
                            <th>Plaga</th>
                            <th>Monto</th>
                            <th>Estatus</th>
                        </tr>
                        <tr>
                            <th class="text-center">
                                <span @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendor y Empresarial" @endif>
                                    <button id="btnFilterQuotation" class="btn btn-default btn-md">
                                        <i class="glyphicon glyphicon-search" @if(Auth::user()->id_plan == 1) disabled @endif></i> Filtrar
                                    </button>
                                </span>
                            </th>
                            <th scope="col">
                                <input class="text-input" type="text" name="filterDateQuotation" id="filterDateQuotation">
                            </th>
                            <th scope="col">
                                <select id="selectFilterEmployeeNameQuot" class="applicantsList-single form-control header-table">
                                    <option value="0" selected>Todos</option>
                                    @foreach($employees as $employee)
                                        <option value="{{ $employee->employee_id }}">
                                            {{ $employee->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </th>
                            <th scope="col">
                              <select id="selectFilterCustomerNameQuot" class="applicantsList-single form-control header-table">
                                  <option value="0" selected>Todos</option>
                                  @foreach($customers as $customer)
                                      <option value="{{ $customer->id }}">
                                          {{ $customer->name }}
                                      </option>
                                  @endforeach
                              </select>
                            </th>
                            <th scope="col">
                              <select id="selectFilterColony" class="applicantsList-single form-control header-table">
                                    <option value="0" selected>Todos</option>
                                    @foreach($address as $ad)
                                        <option value="{{ $ad->id }}">{{ $ad->address }}, {{ $ad->address_number }} {{ $ad->colony }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th scope="col">
                              <select id="filterKeyQuot" class="applicantsList-single form-control header-table">
                                  <option value="0" selected>Todos</option>
                                  @foreach($keyPriceLists as $key)
                                      <option value="{{ $key->id }}">
                                          {{ $key->key }}
                                      </option>
                                  @endforeach
                              </select>
                            </th>
                            <th scope="col">
                              <select id="filterPaymentQuot" class="applicantsList-single form-control header-table">
                                  <option value="0" selected>Todos</option>
                                  @foreach($discounts as $discount)
                                      <option value="{{ $discount->id }}, 1">{{ $discount->title }} / {{ $discount->percentage }}%</option>
                                  @endforeach
                                  <option disabled>____________________________________</option>
                                  @foreach($extras as $extra)
                                    <option value="{{ $extra->id }}, 2">{{ $extra->name }} / ${{ $extra->amount }}</option>
                                  @endforeach
                              </select>
                            </th>
                            <th scope="col">
                              <select id="filterStatusQuot" class="applicantsList-single form-control header-table">
                                  <option value="0" selected>Todos</option>
                                  <option value="4">Programado</option>
                                  <option value="10">Sin Programar</option>
                                  <option value="1">Seguimiento</option>
                                  <option value="3">Rechazado</option>
                                  <option value="14">Aprobado</option>
                              </select>
                            </th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($quotation as $q)
                        @if($q->fake == 0)
                        <tr>
                            <td scope="row">
                                <div class="btn-group" @if($q->id_quotation == 'C-3') id="menuTableUlDiv" @endif>
                                    <a id="tableIndexDiv" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                        {{ $q->id_quotation}} <span class="caret"></span>
                                    </a>
                                    @include('vendor.adminlte.register._menu')
                                </div>
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($q->date)->format('d/m/Y') }} <br>{{ \Carbon\Carbon::parse($q->date)->format('H:i') }}
                            </td>
                            <td>
                                {{$q->agente}}
                            </td>
                            <td>
                                {{ $q->name }}  <br> Cel: {{ $q->cellphone }} <br>
                                {{ $q->establishment_name }} <br> Cel: {{ $q->cellphone_main }}
                            </td>
                            <td>
                                {{ $q->colony }} <br> {{ $q->municipality }}
                            </td>
                            <td>
                                {{ $q->key }} <br>
                                {{ $q->name_list_price }}
                            </td>
                        <td>
                            Sub: {{ $symbol_country }}@convert($q->price) <br> Des: {{ $q->percentage }}% <br> @if($q->id_extra != 3) Ext: {{ $symbol_country }}@convert($q->amount) <br> @endif Tot: {{ $symbol_country }}@convert($q->total)
                        </td>
                        <td>
                            @if($q->id_status == 1)
                              @foreach($tracing as $t)
                                @if($t['id_quotation'] == $q->id)
                                  @if($t['color'] === 'green')
                                     <span class="label label-default">{{ $q->status }}</span> <br> <strong style="color: #00a157">{{ $t['tracing_date'] }}</strong> <br> {{ \Carbon\Carbon::parse($t['tracing_hour'])->format('H:i') }} @if($q->messenger != null) <a href="{{ $q->messenger }}" target="_blank" class="btn" style="background-color: transparent;"><i class="fa fa-facebook-square" aria-hidden="true" style=" font-size: 1.5em;"></i></a>@endif
                                  @elseif($t['color'] === 'yellow')
                                    {{ $q->status }} <br> <strong style="color: #FAC800">{{ $t['tracing_date'] }}</strong> <br> {{ \Carbon\Carbon::parse($t['tracing_hour'])->format('H:i') }} @if($q->messenger != null) <a href="{{ $q->messenger }}" target="_blank" class="btn" style="background-color: transparent;"><i class="fa fa-facebook-square" aria-hidden="true" style=" font-size: 1.5em;"></i></a>@endif
                                  @elseif($t['color'] === 'red')
                                     <span class="label label-primary">{{ $q->status }}</span> <br> <strong style="color: red">{{ $t['tracing_date'] }}</strong> <br> {{ \Carbon\Carbon::parse($t['tracing_hour'])->format('H:i') }} @if($q->messenger != null) <a href="{{ $q->messenger }}" target="_blank" class="btn" style="background-color: transparent;"><i class="fa fa-facebook-square" aria-hidden="true" style=" font-size: 1.5em;"></i></a>@endif
                                  @else <span class="label label-primary">{{ $q->status }}</span><br> <strong>{{ $t['tracing_date'] }}</strong> <br> {{ $t['tracing_hour'] }} @if($q->messenger != null) <a href="{{ $q->messenger }}" target="_blank" class="btn" style="background-color: transparent;"><i class="fa fa-facebook-square" aria-hidden="true" style=" font-size: 1.5em;"></i></a>@endif
                                  @endif
                                @endif
                              @endforeach
                            @else
                              @if($q->id_status == 2) <strong style="color: #b71c1c" title=""></strong>@elseif($q->id_status == 3) <strong style="color: #b71c1c" title="{{ $q->motiveRejected }}"><span class="label label-danger">Rechazado</span></strong>@else<span class="label label-primary">{{ $q->status }}</span>
                                    <br>@endif @if($q->messenger != null) <a href="{{ $q->messenger }}" target="_blank" class="btn" style="background-color: transparent;"><i class="fa fa-facebook-square" aria-hidden="true" style=" font-size: 1.5em;"></i></a>@endif
                            @endif
                            @if($q->cellphone != null)
                                @if($q->whatsapp == 1)
                                  <br>
                                  <a href="{{ $q->urlWhatsappShow }}" target="_blank" class="btn" style="color: green; background-color: transparent;"><i class="fa fa-whatsapp" aria-hidden="true" style=" font-size: 1.5em;"></i></a>
                                @endif
                            @endif
                            @if($q->approved == 1)
                                <br><a href="" data-toggle="tooltip" data-placement="top" title="Aprobada"><i class="fa fa-check" aria-hidden="true" style=" color: green"></i></a>
                            @endif
                            @if($q->send_email == 1)
                                <br><a data-toggle="tooltip" data-placement="top" title="Correo Enviado"><i class="fa fa-lg fa-envelope-o" aria-hidden="true" style=" color: blue"></i></a>
                            @endif
                        </td>
                      </tr>
                        @endif
                    @endforeach
                    </tbody>
                  </table>
                    <center>
                      {{ $quotation->links() }}
                    </center>
                </div>
              </div>
            </div>
            @if(auth()->user()->companie != 338)
            <div class="col-md-3">
              <div id="calendar1" class="col-md-12" style="margin-top: 0px !important;"></div>
            </div>
            @endif
          </div>
        </div>
        <!-- /.box -->
      </div>
    </div>
  </div>
@include('vendor.adminlte.register._quotation')
@include('adminlte::register._deny')
@include('adminlte::register._deleteQuotation')
@include('adminlte::register._tracing')
@include('adminlte::register._modalSendEmailQuotation')
@include('adminlte::register._modalSendWhatsappQuotation')
@include('adminlte::register._quotationCustomTest')
@include('vendor.adminlte.register._modalReinforcementOrder')
@include('vendor.adminlte.register._approvedQuotation')
  @endsection
  @endif

  @section('personal-js')

    <script type="text/javascript" src="{{ URL::asset('js/build/quotation.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/recurrent.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/reinforcement.js') }}"></script>
<!--    <script type="text/javascript" src="{{ URL::asset('js/build/instance.js') }}"></script>-->

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script>
      $("#plagues").select2();
      $("#plaguesCustom").select2();
      $(".applicantsList-single").select2();
      $('#newQuotation').on('click',function() {
          $('#newQ').modal("show");
      });
      $('#newQuotationCustom').on('click',function() {
          $('#ModalQuotationCustomTest').modal("show");
      });
    </script>

    <script>

      document.addEventListener('DOMContentLoaded', function() {

          let company = '{{ auth()->user()->companie }}';
          if (company != 338) {
              let events = [
                      @foreach ($events as $event)
                  {
                      @if($event->id_status == 4) title: '{{ $event -> title }} \n (FINALIZADO)'
                      @else title: '{{ $event -> title }}' @endif,
                      id: '{{ \Vinkla\Hashids\Facades\Hashids::encode($event->id) }}',
                      start: '{{ $event -> initial_date }}' + ' ' + '{{ $event -> initial_hour }}',
                      end: '{{ $event -> final_date }}' + ' ' + '{{ $event -> final_hour }}',
                      @if($event->id_status == 4) color: '#00a157'
                      @else color: '{{ $event -> color }}' @endif
                  },
                  @endforeach
              ];

              let min = '08:00:00';
              let max = '20:00:00';
              let modeFull = false;

              let calendarEl = document.getElementById('calendar1');

              let calendar = new FullCalendar.Calendar(calendarEl, {
                  plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
                  timeZone: 'UTC',
                  locale: 'es',
                  customButtons: {
                      changeRange: {
                          text: '12/24',
                          click: function () {
                              if (modeFull) {
                                  calendar.setOption('minTime', '08:00:00');
                                  calendar.setOption('maxTime', '20:00:00');
                                  modeFull = false;
                              }else {
                                  calendar.setOption('minTime', '00:00:00');
                                  calendar.setOption('maxTime', '24:00:00');
                                  modeFull = true;
                              }
                          }
                      }
                  },
                  height: 900,
                  defaultView: 'timeGridDay',
                  minTime: min,
                  maxTime: max,
                  contentHeight: "auto",
                  header: {
                      left: 'title',
                      right: 'prev,next,changeRange'
                  },
                  events: events,
                  eventClick: function(info) {
                      getDataEvent(info.event.id);
                  },
                  editable: true
              });

              calendar.render();
          }
      });

      //get price from database
    function getDataEvent(id){
          loader('');
            $.ajax({
                type: 'GET',
                url: route('info_event', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function (data) {
                    if (data.code == 500){
                        Swal.close();
                        missingText('Algo salio mal');
                    }
                    else{
                        data = data.data;
                        var json = JSON.parse(JSON.stringify(data));
                        let urlBase = "https://pestwareapp.com/service/schedule?";
                        let urlBaseDev = "http://127.0.0.1:8000/service/schedule?";
                        let url = urlBase + "search=" + data.id_service_order;
                        let messageFinished = data.status == 4 ? '<strong style="font-size:15px; color: #00a157;">FINALIZADO</strong>' : '';
                        let footer = '<a href="' + route('edit_event', id) + '"class="btn btn-sm btn-secondary" style="margin-right: 10px;"><i class="glyphicon glyphicon-calendar"></i> Editar</a> ' +
                            '<a href="' + route('edit_service', data.id_order) + '"class="btn btn-sm btn-primary" style="margin-right: 10px;"><i class="glyphicon glyphicon-pencil"></i> Editar OS</a> ' +
                            '<a href="' + route('delete_view', data.id_order) + '"class="btn btn-sm btn-danger" style="margin-right: 10px;"><i class="glyphicon glyphicon-remove"></i> Cancelar</a>';
                        let footerFinished = '<a href="' + route('order_view', data.id_order) + '"class="btn btn-sm btn-primary" style="margin-right: 10px;"><i class="glyphicon glyphicon-search"></i> Ver OS</a>';
                        Swal.close();
                        Swal.fire({
                          type: 'info',
                          showConfirmButton: false,
                          showCloseButton: true,
                          title: '<strong style="font-size:20px; color: #018243;"><a target="_blank" href="' + url + '">' + data.name + '&nbsp;&nbsp;&nbsp;&nbsp;' + data.id_service_order + '</a></strong>',
                          html: messageFinished + '<hr><strong style="font-size:15px; color: #1F282C;">' + data.initial_date + '&nbsp;&nbsp;&nbsp;&nbsp;' + data.initial_hour +'</strong><br><br>' +
                                '<strong style="font-size:15px; color: #1F282C;">' + data.plagues + '</strong><br><br>' +
                              '<strong style="font-size:15px; color: #1F282C;">Total: $' + data.total + '<strong><br><br>' +
                              '<strong style="font-size:15px; color: #1F282C;">Teléfono: ' + data.cellphone + '<strong><br><br>' +
                              '<strong style="font-size:15px; color: #1F282C;">' + data.address + '<strong><br><br>' +
                              '<strong style="font-size:15px; color: #1F282C;">Técnico Asignado: ' + data.technical + '<strong>',
                          width: 800,
                          footer: data.status == 4 ? footerFinished : footer,
                        });
                    }
                }
            });
        }

    function loader(message){
          Swal.fire({
              title: message,
              onBeforeOpen: () => {
                  Swal.showLoading()
              },
          }).then((result) => {

          })
      }

    function missingText(textError) {
          swal({
              title: "¡Espera!",
              type: "error",
              text: textError,
              icon: "error",
              timer: 3000,
              showCancelButton: false,
              showConfirmButton: false
          });
      }

    </script>

    <script src="{{ asset('js/bootstable.js') }}" ></script>
    <script src="https://use.fontawesome.com/92e776c0ac.js"></script>
    <script>
        $(function() {

            $('#makeEditable').SetEditable({ $addButton: $('#but_add')});
            let arrayConcepts = [];
            let subtotal = 0;
            let total = 0;
            let isError = false;
            let companyId = document.getElementById('idCompanyAuth').value;

            $('#submit_data').on('click',function() {
              let td = TableToCSV('makeEditable', ',');
              let ar_lines = td.split("\n");
              let each_data_value = [];
              arrayConcepts = [];
              let errors = [];
              total = 0;
              subtotal = 0;
              ar_lines.pop();

              ar_lines.forEach((element, i) => {
                  each_data_value[i] = ar_lines[i].split(",");
              });

              each_data_value.forEach((element, i) => {

                  subtotal = subtotal + (element[1] * element[2] * element[3] * element[4]);

                  // get data
                  let concept = element[0];
                  let quantity = element[1];
                  let frequency = element[2];
                  let term = element[3];
                  let priceUnit = element[4];
                  let subtotalConcept = element[1] * element[2] * element[3] * element[4];

                  // validations
                  if (companyId != 37 && companyId != 232 && companyId != 231 && companyId != 220 && companyId != 230 && companyId != 191) {
                      if (frequency > 3) errors.push('No puede ser mayor a 4. Solicite autorización a: soporte@pestwareapp.com');
                      if (term > 12) errors.push('Se requiere autorización. Contactar a: soporte@pestwareapp.com');
                  }

                  arrayConcepts.push({
                      'concept': concept,
                      'quantity': quantity,
                      'frequency': frequency,
                      'term': term,
                      'priceUnit': priceUnit,
                      'subtotal': subtotalConcept
                  });

              });

              isError = errors.length > 0;

              let discount = $('#discountC').val();
              let percentageDiscount = discount / 100;
              let totalDiscount = percentageDiscount * subtotal;
              total = subtotal - totalDiscount;
              $('#sumaSubtotal').val(subtotal);
              $('#total').val(total);

            });

            $('#saveQuotationCustomNew').on('click',function() {
                let totalBranches = arrayConcepts.length;
                if (isError) {
                    missingTextC('Se requiere autorización. Contactar a: soporte@pestwareapp.com')
                } else if(totalBranches > 10 && companyId != 37 && companyId != 232 && companyId != 231 && companyId != 220 && companyId != 230 && companyId != 191) {
                    missingTextC('Se requiere autorización. Contactar a: soporte@pestwareapp.com')
                } else saveQuotationCustom();
            });

            function saveQuotationCustom() {
              loaderC('');

              //get values input
              let description = $('#description').val();
              let customerName = $('#customerName').val();
              let establishmentName = $('#establishmentName').val();
              let cellphone = $('#cellphone').val();
              let establishment = $('#establishment').val();
              let email = $('#emailCustomer').val();
              let colony = $('#colony').val();
              let municipality = $('#municipality').val();
              let sourceOrigin = $('#sourceOrigin').val();
              let messenger = $('#messenger').val();
              let discount = $('#discountC').val();
              let plague = $('#plagues').val();
              let id_quotation = $('#id_quotation').text().trim();
              let contador = plague.length;
              let id_price_list = $('#idPriceListCustom').val();
              let jobCenter = $("#selectJobCenter option:selected").val();

              $.ajax({
                  type: 'POST',
                  url: route('add_quotation_custom'),
                  data: {
                      _token: $("meta[name=csrf-token]").attr("content"),
                      description: description,
                      arrayConcepts: JSON.stringify(arrayConcepts),
                      name: customerName,
                      establishmentName: establishmentName,
                      cellphone: cellphone,
                      establishment: establishment,
                      email: email,
                      address: colony,
                      municipality: municipality,
                      discount: discount,
                      price: subtotal,
                      total: total,
                      sourceOrigin: sourceOrigin,
                      messenger: messenger,
                      contador: contador,
                      plague: plague,
                      id_quotation: id_quotation,
                      id_price_list: id_price_list,
                      jobCenter: jobCenter
                  },
                  success: function (data) {
                      if (data.code == 500){
                          Swal.close();
                          //missingTextC('Algo salio mal, intenta de nuevo.');
                          missingText(data.message);
                      }
                      else if(data.code == 201){
                          Swal.close();
                          showToast("success-redirect","Cotizacion personalizada","Guardado Correctamente",'index_register');
                          $("#saveQuotationCustomNewTest").attr('disabled', true);
                          //LastQuotationC();
                          //generateC();
                      }
                  }
              });
            }

            //alerts messages

            function showToast(type, title, message, path) {
                if (type === 'success-redirect') {
                    toastr.success(message, title);
                    setTimeout(() => {
                        window.location.href = route(path);
                    }, 5000);
                }
                else if (type === 'success') toastr.success(message, title)
                else if (type === 'info') toastr.info(message, title)
                else if (type === 'warning') toastr.warning(message, title)
                else if (type === 'error') toastr.error(message, title)
            }


            function missingTextC(textError) {
                swal({
                    title: "¡Espera!",
                    type: "error",
                    text: textError,
                    icon: "error",
                    timer: 4000,
                    showCancelButton: false,
                    showConfirmButton: false
                });
            }

            function loaderC(message){
                Swal.fire({
                    title: message,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },
                }).then((result) => {

                })
            }

        });
    </script>

  @endsection
