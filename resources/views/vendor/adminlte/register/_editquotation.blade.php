@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <!-- START MODAL DE EDITAR COTIZACION -->
    <div class="container-fluid spark-screen">
        <div class="modal fade" id="editQuotation" role="dialog" aria-labelledby="editQuotation">
            <div class="row">
                <div class="col-md-12">
                    <!--Default box-->
                    <div class="modal-dialog modal-lg" role="document">

                        <div class="modal-content">
                            <div class="box">
                                <div class="modal-header">
                                    <div class="box-header">
                                        <button type="button" class="close btn-lg" data-dismiss="modal"
                                                aria-label="CERRAR">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <input type="hidden" id="id" name="id"
                                               value="{{ $quotation['quotation']->id }}">
                                        <input type="hidden" id="isCustomQuote"
                                               value="{{ $suma }}">
                                        <input type="hidden" id="id_customer" name="id_customer"
                                               value="{{ $quotation['quotation']->id_customer }}">
                                        <input type="hidden" id="id_plague_jer" name="id_plague_jer"
                                               value="{{ $quotation['quotation']->id_plague_jer }}">
                                        <input type="hidden" id="suma" name="suma" value="{{ $suma }}">
                                        <input type="hidden" id="validate" name="validate" value="{{ $validate }}">
                                        <input type="hidden" id="codeCountry" value="{{ $countryCode->code_country }}">
                                    </div>
                                </div>
                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Cotización</h4>
                                    <hr>

                                    <div class="row container-fluid">
                                        <div class="col-md-6">
                                            <span style="color:black"
                                                  id="dateE">Fecha: <?php echo date("d-m-Y", strtotime($quotation['quotation']->date)); ?></span>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span style="color:black;">Número de cotización: </span><span>{{ $quotation['quotation']->id_quotation }}</span>
                                        </div>
                                    </div>
                                    <hr>

                                    <form action="" method="PUT" id="formEditQuotation" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                    <div class="row container-fluid">
                                        <div class="col-md-6">

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__customerNameE">
                                                    <div>
                                                        <label class="control-label" for="customerNameE">Nombre del cliente*: </label>
                                                        <input type="text"
                                                               class="formulario__input form-control"
                                                               id="customerNameE" name="customerNameE"
                                                               value="{{ $quotation['quotation']->name or old('customerNameE') }}"
                                                               disabled
                                                               data-toggle="tooltip" data-placement="bottom" title="Editar en Módulo Clientes">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__cellphoneE">
                                                    <div>
                                                        <label class="control-label" for="cellphoneE">Celular del Cliente: </label>
                                                        <input type="number"
                                                               class="formulario__input form-control"
                                                               id="cellphoneE"
                                                               name="cellphoneE"
                                                               value="{{ $quotation['quotation']->phone_number or old('cellphoneE') }}"
                                                               disabled
                                                               data-toggle="tooltip" data-placement="bottom" title="Editar en Módulo Clientes">
                                                        <div id="numberList"></div>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo admite números, mínimo 10 y maximo 14 dígitos.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__establishmentNameE">
                                                    <div>
                                                        <label class="control-label" for="establishmentNameE">Contacto Principal: </label>
                                                        <input type="text"
                                                               class="formulario__input form-control"
                                                               id="establishmentNameE"
                                                               name="establishmentNameE"
                                                               value="{{ $quotation['quotation']->establishment_name or old('establishmentNameE') }}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__cellphoneMainE">
                                                    <div>
                                                        <label class="control-label" for="cellphoneE">Celular del Contacto Principal: </label>
                                                        <input type="number"
                                                               class="formulario__input form-control"
                                                               id="cellphoneMainE"
                                                               name="cellphoneMainE"
                                                               value="{{ $quotation['quotation']->cellphone_main or old('cellphoneMainE') }}">
                                                        <div id="numberList"></div>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo admite números, mínimo 10 y maximo 14 dígitos.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__emailE">
                                                    <div>
                                                        <label class="control-label" for="emailCustomer">Correo electrónico: </label>
                                                        <input type="text"
                                                               class="formulario__input form-control"
                                                               id="emailE"
                                                               name="emailE"
                                                               value="{{ $quotation['quotation']->email or old('emailE') }}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">El correo electrónico tiene que ser válido por ejemplo: email@gmail.com</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__colonyE">
                                                    <div>
                                                        <label class="control-label" for="colonyE">Colonia: </label>
                                                        <input type="text"
                                                               class="formulario__input form-control"
                                                               id="colonyE"
                                                               name="colonyE"
                                                               value="{{ $quotation['quotation']->colony or old('colonyE') }}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__municipalityE">
                                                    <div>
                                                        <label class="control-label" for="municipalityE">Municipio: </label>
                                                        <input type="text"
                                                               class="formulario__input form-control"
                                                               id="municipalityE"
                                                               name="municipalityE"
                                                               value="{{ $quotation['quotation']->municipality or old('municipalityE') }}">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250 caracteres.</p>
                                                </div>
                                            </div>

                                            <!--Firs Grid md-6-->
                                        </div>

                                        <div class="col-md-6">

                                                <div class="row form-group">
                                                    <div class="col-sm-12 formulario__grupo" id="grupo__establishmentE">
                                                        <label class="control-label" for="establishmentE">Tipo de servicio*: </label>
                                                        <select name="establishmentE" id="establishmentE" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                            <option value="{{ $quotation['quotation']->e_id }}" selected="true">
                                                                {{ $quotation['quotation']->e_name }}
                                                            </option>
                                                            @foreach($establishments as $establishment)
                                                                @if($quotation['quotation']->e_id != $establishment->id)
                                                                    <option value="{{ $establishment->id}}"
                                                                            {{ (in_array($establishment, old('establishments', []))) }} >{{ $establishment->name }}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-sm-6 formulario__grupo" id="grupo__construction_measureE">
                                                        <div>
                                                            <label class="control-label" for="construction_measureE">Medidas de construcción*: </label>
                                                            <input type="number"
                                                                   class="formulario__input form-control"
                                                                   id="construction_measureE"
                                                                   name="construction_measureE"
                                                                   value="{{ $quotation['quotation']->construction_measure or old('construction_measureE') }}">
                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        </div>
                                                        <p class="formulario__input-error" style="font-size: 10px">Este campo solo admite números.</p>
                                                    </div>

                                                    <div class="col-sm-6 text-center formulario__grupo">
                                                        <label class="control-label" for="construction_measure">Convertidores para calcular: </label><br>
                                                        <button class="btn btn-primary" type="button" id="MC"><i class="fa fa-arrows"></i></button>
                                                        <button class="btn btn-primary" type="button" id="CE" name="CE"><i class="fa fa-home"></i></button>
                                                        <button class="btn btn-primary" type="button" id="CS" name="CS"><i class="fa fa-th-large"></i></button>
                                                    </div>

                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-sm-12 formulario__grupo" id="grupo__plagueE">
                                                        <label class="control-label" for="plagueE">Tipo de plaga*: </label>
                                                        <select name="plagueE[]" id="plagueE" class="plagueList-single form-control select__input" style="width: 100%; font-weight: bold;" multiple="multiple">
                                                            @foreach($plagues as $plague)
                                                                <option value="{{ $plague->id }},{{ $plague->plague_key }}"
                                                                        @foreach($quotationPlagues as $quotationPlague)
                                                                        @if($quotationPlague->id == $plague->id)
                                                                        selected='selected'
                                                                        @endif
                                                                        @endforeach
                                                                        {{ (in_array($plague, old('plagueE', []))) }} >{{ $plague->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    @if(\Illuminate\Support\Facades\Auth::user()->companie == 333)
                                                    <div class="col-sm-6 formulario__grupo" id="grupo__sourceOriginE">
                                                        @else  <div class="col-sm-12 formulario__grupo" id="grupo__sourceOriginE">
                                                            @endif
                                                        <label class="control-label" for="sourceOriginE">Fuente de origen*: </label>
                                                        <select name="sourceOrigin" id="sourceOriginE" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                            <option value="{{ $quotation['quotation']->source_id }}"
                                                                    selected="true">
                                                                {{ $quotation['quotation']->source_name }}
                                                            </option>
                                                            @foreach($sources as $source)
                                                                @if($quotation['quotation']->source_id != $source->id)
                                                                    <option value="{{ $source->id}}"
                                                                            {{ (in_array($source, old('sourceOriginE', []))) }} >{{ $source->name }}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    @if(\Illuminate\Support\Facades\Auth::user()->companie == 333)
                                                            <div class="col-sm-6">
                                                                <label class="control-label" for="pesticideE">Plaguicidas: </label>
                                                                <input type="text" class="formulario__input form-control"
                                                                       id="pesticideE" name="pesticideE"
                                                                       placeholder="Ejemplo: Biothrine"
                                                                       value="{{ $quotation['quotation']->pesticide_product or old('pesticideE') }}">
                                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                            </div>
                                                    @endif
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-sm-12 formulario__grupo" id="grupo__messengerE">
                                                        <div>
                                                            <label class="control-label" for="messengerE">Messenger: </label>
                                                            <input type="text"
                                                                   class="formulario__input form-control"
                                                                   id="messengerE"
                                                                   name="messengerE"
                                                                   value="{{ $quotation['quotation']->messenger or old('messengerE') }}">
                                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                        </div>
                                                        <p class="formulario__input-error">La descripción del messenger debe ser de 3 a 250 caracteres..</p>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label class="control-label" for="discountE">Descuento: </label>
                                                        <select name="discountE" id="discountE" class="form-control" style="width: 100%; font-weight: bold;">
                                                            <option value="{{ $quotation['quotation']->discount_id }}"
                                                                    selected="true">
                                                                {{ $quotation['quotation']->discount_name }}
                                                                / {{ $quotation['quotation']->percentage }}%
                                                            </option>
                                                            @foreach($discounts as $discount)
                                                                @if($quotation['quotation']->discount_id != $discount->id)
                                                                    <option value="{{ $discount->id}}"
                                                                            {{ (in_array($discount, old('discounts', []))) }} >{{ $discount->title }}
                                                                        / {{ $discount->percentage }}%
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label class="control-label" for="extraE">Extra: </label>
                                                        <select name="extraE" id="extraE" class="form-control" style="width: 100%; font-weight: bold;">
                                                            <option value="{{ $quotation['quotation']->id_extra }}"
                                                                    selected="true">
                                                                {{ $quotation['quotation']->extra_name }} /
                                                                ${{ $quotation['quotation']->amount }}
                                                            </option>
                                                            @foreach($extras as $extra)
                                                                @if($quotation['quotation']->id_extra != $extra->id)
                                                                    <option value="{{ $extra->id}}"
                                                                            {{ (in_array($extra, old('extras', []))) }} >{{ $extra->name }}
                                                                        / ${{ $extra->amount }}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <br>
                                                    </div>
                                                        <input type="hidden" name="extraE" id="extraE"
                                                               value="{{$quotation['quotation']->id_extra}}">
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-sm-12 formulario__grupo" id="grupo__sucursal">
                                                        <label class="control-label" for="sourceOriginE">Sucursal*: </label>
                                                        <select name="sucursal" id="sucursal" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                            <option value="{{ $quotation['quotation']->id_job_center }}"
                                                                    selected="true">
                                                                {{ $quotation['quotation']->job_center_name }}
                                                            </option>
                                                            @foreach($sucursales as $sucursal)
                                                                @if($quotation['quotation']->id_job_center != $sucursal->id)
                                                                    <option value="{{ $sucursal->id}}"
                                                                            {{ (in_array($source, old('sucursal', []))) }} >{{ $sucursal->name }}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Precio: </label><br>
                                                        <label class="control-label" for="price" id="priceQ" style="font-size: 1.5em;"><b id="priceQuotLabelEdit">
                                                                {{ $symbol_country }} {{ $quotation['quotation']->price }}</b>
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label class="control-label">Precio con descuento: </label><br>
                                                        <label class="control-label text-primary" for="priceQD" id="priceQD" style="font-size: 1.5em;">
                                                            <b id="priceFinalQuotLabelEdit">{{  $symbol_country }} {{ $quotation['quotation']->total }}</b>
                                                        </label>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <div class="text-center">

                                        <div class="col-sm-12 text-right">
                                                @if($quotation['quotation']->id_status == 10 && $suma != 0)
                                            <span @if(Auth::user()->id_plan == 1 || Auth::user()->id_plan == 2) @endif data-toggle="tooltip" data-placement="bottom" title="Plan Empresarial">

                                            </span>
                                            @endif
                                        </div>

                                        <div class="col-sm-12 formulario__mensaje" id="formulario__mensaje" style="margin-top: 10px; margin-bottom: 10px;">
                                            <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                        </div>
                                        <br>
                                        <div class="col-sm-12 text-center formulario__grupo formulario__grupo-btn-enviar">
                                            <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                        </div>

                                        <div class="col-sm-12">
                                            <button class="btn btn-secondary btn-lg" type="button" id="priceQuotationE"
                                                    name="priceQuotation">Cotizar
                                            </button>
                                            <button class="btn btn-primary btn-lg" type="button" id="saveQuotationE"
                                                    name="saveQuotation">Guardar
                                            </button>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('vendor.adminlte.register._editQuotationCustomTest')
    <!-- END MODAL DE COTIZACION -->
@endsection

@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/updateQuote.js') }}"></script>
    <script src="{{ asset('js/bootstable.js') }}"></script>
    <script src="https://use.fontawesome.com/92e776c0ac.js"></script>
    <script>
        $('#updCustomQuoteOpen').on('click',function() {
            $('#ModalQuotationCustomTest').modal("show");
        });
    </script>
@endsection
