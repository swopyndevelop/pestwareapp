<!-- START MODAL CREATE NEW DESCRIPTION -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="newDescriptionCustomQuote" role="dialog" aria-labelledby="newDescriptionCustomQuote">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Agregar Descripción</h4>
                            </div>

                            <div class="box-body text-center">
                                <div class="col-xs-6 col-xs-offset-3">
                                    <div>
                                        <span style="font-weight: bold;">Nombre de la Descripción:</span>
                                        <input type="email" class="form-control" id="nameDescription">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <button class="btn btn-primary" id="saveNewDescriptionCustom">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CREATE NEW DESCRIPTION FOR QUOTE -->