<!-- START MODAL DE RECHAZAR COTIZACIÓN -->
<div class="container-fluid spark-screen">
  <div class="modal fade" id="denyQ" role="dialog" aria-labelledby="denyModal" tabindex="-1">
    <div class="row">
      <div class="col-md-12">
        <!--Default box-->
        <div class="modal-dialog modal-lg" role="document" >

          <div class="content modal-content" >
            <div class="box">
              <div class="modal-header" >
                <div class="box-header" >
                  <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Rechazar cotización <b id="quotationKey"></b></h4>
              </div>

              <div class="box-body">
                <div class="row container-fluid">
                    <div class="col-md-12 text-left">
                      <input type="hidden" name="idQuotation" id="idQuotation">

                      <div class="formulario__grupo" id="grupo__reason">
                        <div>
                          <span style="font-weight: bold;">Motivo del rechazo:</span>
                          <select name="reason" id="reason" class="select__input form-control" style="width: 100%; font-weight: bold;">
                            <option value="0" disabled selected>Seleccione el motivo</option>
                            <option value="Precio Alto">Precio Alto</option>
                            <option value="Servicio fuera de horario" >Servicio fuera de horario</option>
                            <option value="No hay disponibilidad de técnicos">No hay disponibilidad de técnicos</option>
                            <option value="Otro">Otro</option>
                          </select>
                        </div>
                      </div>

                      <br>
                      <div class="formulario__grupo" id="grupo__comments">
                        <div>
                          <span style="font-weight: bold;">Comentarios del cliente:</span>
                          <textarea class="formulario__input form-control" name="comments" id="comments" cols="30" rows="3" placeholder="Descripción del servicio"></textarea>
                          <i class="formulario__validacion-estado fa fa-times-circle"></i>
                        </div>
                        <p class="formulario__input-error">Los Comentarios deben ser de 3 a 250 caracteres.</p>
                      </div>
                    </div>
                </div>
                <br>
                <div class="formulario__mensaje" id="formulario__mensaje_deny">
                  <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                  <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                </div>
                <br>
              </div>

              <div class="modal-footer">
                <div class="text-center">
                  <div class="row">
                      <button class="btn btn-primary" type="button" id="saveQuotationDeny">Rechazar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- END MODAL DE RECHAZAR COTIZACIÓN -->
