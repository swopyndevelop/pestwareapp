<!-- START MODAL DE EDITAR OS -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="updateServiceOrder{{ $a }}" role="dialog" aria-labelledby="updateServiceOrder">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document" >

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body text-center">
                                <h4 class="text-center text-secondary">Editar Servicio <b>{{ $service->id_service_order }} ({{ $service->branche }})</b></h4>
                                <form action="{{ Route('update_order_service') }}" method="POST" id="formUpdateOrder">
                                    {{ csrf_field() }}
                                    <div class="col-md-6 col-xs-6">
                                        <br>
                                        <input type="hidden" name="id_event" id="id_event" value="{{$service->event_id}}">
                                        <label class="control-label" for="dateStart" style="font-weight: bold;">Fecha Inicio: </label>
                                        <br>
                                        <input type="date" name="dateStart" id="dateStart" value="{{$service->initial_date}}" required>
                                        <br><br>
                                        <label class="control-label" for="dateEnd" style="font-weight: bold;">Fecha Fin: </label>
                                        <br>
                                        <input type="date" name="dateEnd" id="dateEnd" value="{{$service->final_date}}" required>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <br>
                                        <label class="control-label" for="hourStart" style="font-weight: bold;">Hora Inicio: </label>
                                        <br>
                                        <input type="time" name="hourStart" id="hourStart" value="{{$service->initial_hour}}" required>
                                        <br><br>
                                        <label class="control-label" for="hourEnd" style="font-weight: bold;">Hora Fin: </label>
                                        <br>
                                        <input type="time" name="hourEnd" id="hourEnd" value="{{$service->final_hour}}" required>
                                        <br><br>
                                    </div>
                                    <label for="techniciansEvent" style="font-weight: bold;">Técnicos Disponibles:</label>
                                    <select name="techniciansEvent" id="techniciansEvent" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;" required>
                                        @foreach($employees as $technician)
                                            <option value="{{ $technician->id }}"
                                                    {{ (in_array($technician, old('technicians', []))) }}
                                                    <?php if ($service->id_employee == $technician->id): ?>
                                                    selected
                                            <?php endif ?>>{{ $technician->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Actualizar Servicio</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE CREAR EVENTO -->
