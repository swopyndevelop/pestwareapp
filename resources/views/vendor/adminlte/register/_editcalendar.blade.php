@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
    <!-- START MODAL DE EDITAR EVENTO -->
<div class="container-fluid spark-screen">
	<div class="modal fade" id="editSchedule" role="dialog" aria-labelledby="editSchedule">
		<div class="row">
			<div class="col-md-12">
				<!--Default box-->
				<div class="modal-dialog modal-lg" role="document" >

					<div class="modal-content">
						<div class="box">
							<div class="modal-header" >
								<div class="box-header" >
									<button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
							<div class="box-body">
								<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitleSchedule">Actualizar Servicio</h4>
								<hr>
								<div class="col-md-12">
									<h5 for="titleE" class="control-label" style="font-weight: bold;">Título</h5>
									<input class="form-control" type="text" value="{{ $event->title }}" id="titleE">
								</div>
								<div class="col-md-6 col-xs-12">
									<input type="hidden" name="id_event" id="id_event" value="{{$event->event}}">
									<input type="hidden" name="id_technician" id="id_technician" value="{{ $event->id_employee }}">
									<h5 class="control-label" for="dateStart" style="font-weight: bold;">Fecha Inicio: </h5>
									<input type="datetime-local" name="dateStartE" id="dateStartE" class="form-control" value="{{$event->initial_date}} {{$event->initial_hour}}" required>
								</div>
								<div class="col-md-6 col-xs-12">
									<h5 class="control-label" for="dateEnd" style="font-weight: bold;">Fecha Fin: </h5>
									<input type="datetime-local" name="dateEndE" id="dateEndE" class="form-control" value="{{$event->final_date}} {{$event->final_hour}}" required>
								</div>
								<!--No borrar este input-->
								<div style="visibility: hidden">
									<input type="datetime-local" value="">
								</div>

								<div class="row container-fluid">
									<br>
									<div class="col-md-12">
										<label class="control-label" style="font-weight: bold;">@if($event->is_shared == 1)Técnico Auxiliar @else Técnicos Disponibles: @endif</label>
										<div @if(Auth::user()->companie == 37 || Auth::user()->companie == 371 || Auth::user()->companie == 338 || Auth::user()->companie == 451
												|| Auth::user()->companie == 652) class="input-group" @endif>
											<select name="techniciansEventE" id="techniciansEventE" class="applicantsList-single form-control" style="width: 100%;font-weight: bold;" required>
											</select>
											<span class="input-group-btn">
												 @if(Auth::user()->companie == 37 || Auth::user()->companie == 371 || Auth::user()->companie == 338 || Auth::user()->companie == 451
												|| Auth::user()->companie == 652
														&& $event->is_shared == 0 && $event->is_main == 0 || $event->is_shared == 1 && $event->is_main == 1)
												<button @if (Auth::user()->id_plan == 3) id="showSelectAditionalTechniciansEdit"
														@else title="Plan Corporativo"
														@endif
														data-toggle="tooltip" data-placement="right" class="btn btn-primary">
													<i class="fa fa-plus" aria-hidden="true"></i>
												</button>
												@endif
											</span>
										</div>
									</div>
								</div>

								<div class="row container-fluid" id="rowSelectAditionalTechniciansEdit" @if($techniciansAuxiliariesCount == 0) style="display: none;" @endif>
									<br>
									<div class="col-md-12">
										<label class="control-label" style="font-weight: bold;">Técnicos Auxiliares: </label>
										<div class="input-group">
											<select name="techniciansAuxiliariesEventEdit[]" id="techniciansAuxiliariesEventEdit" class="applicantsList-single" style="width: 100%; font-weight: bold;" multiple="multiple">
												@foreach($technicians as $technician)
													<option value="{{ $technician->id }}"
															@foreach($techniciansAuxiliaries as $technicianAux)
															@if($technicianAux->id == $technician->id)
															selected='selected'
															@endif
															@endforeach
															{{ (in_array($technician, old('techniciansAuxiliariesEventEdit', []))) }} >{{ $technician->name }}
													</option>
												@endforeach
											</select>
											<span class="input-group-btn">
												<button class="btn btn-danger" id="hideSelectAditionalTechniciansEdit"><i class="fa fa-minus" aria-hidden="true"></i></button>
											</span>
										</div>
									</div>
								</div>

							</div>
							<div class="modal-footer">
								<div class="text-center">
									<button type = "submit" class="btn btn-primary btn-lg" id="saveEventE" name="saveEventE">Actualizar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END MODAL DE CREAR EVENTO -->
@endsection
@section('personal-js')

	<script type="text/javascript" src="{{ URL::asset('js/build/updateEvents.js') }}"></script>

	<script>
		let modalSchedule = $("#editSchedule");
		modalSchedule.modal();
		modalSchedule.on('hidden.bs.modal', function() {
			let prevUrl = document.referrer;
			if (prevUrl === 'https://pestwareapp.com/index_register') {
				window.location.href = '{{route("index_register")}}'; //using a named route
			}else {
				window.location.href = '{{route("calendario")}}'; //using a named route
			}
		});
		$(".applicantsList-single").select2();
	</script>
@endsection
