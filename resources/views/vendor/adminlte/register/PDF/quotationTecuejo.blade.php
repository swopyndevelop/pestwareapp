<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cotización</title>
</head>
<style>
    @page {
		margin: 0;
    }
	body{
        font-family: 'Montserrat';
	}
</style>
<body>
    <div>
        <img src="img/b1.png" alt="" width="100%">
        <img src="img/b2.png" alt="" style="float:right;margin-top:131px;padding-right:-13px">
    </div>
    <div style="padding-left:20px;text-align:justify;font-size:12pt;line-height: 1em;">
        <p>Este tipo de cucaracha se encuentra distribuida a lo largo de todo el país y en cada
            región se llama de diferente manera: <span style="font-style:italic">Tecuejos, Juanitos,
            Tatascuanes, Cucarachitos, etc.</span> <br> el nombre correcto es
            <span style="font-weight:bold">CUCHARACHA ALEMANA</span>.
        </p>
        <p>Esta especie disfruta de ambientes oscuros y cálidos, su tasa de reproducción es sumamente
            alta, logrando generar hasta 100 huevecillos por semana.
        </p>
        <p>El principal problema de esta plaga es el grado de suciedad que generan en las áreas de anidamiento, ya que depositan su
            excremento en cajones, alacenas, despensas, etc. Las cuáles tienen contacto directo con alimentos y utensilios que se utilizan
            a diario y representan un riesgo de transmisión de enfermedades gastrointestinales.
        </p>
        <p style="color:green;font-size:14pt;font-weight:bold">Contamos con todos los permisos para brindarte un servicio profesional así como tu Certificado de
            Fumigación.
        </p>
    </div>
    <div style="float:left;padding-left:30px">
        <br>
           <img src="img/body_t.png" alt="" width="150px" height="420px"> 
        </div>
    <div style="width:520px;font-size:13pt;text-align:center;background-color:lightgray;margin-left:15px;">
        <br>
        <span style="color:red;font-size:13pt;font-weight:bold">ZONAS DE ALERTA</span>
        <p style="text-align:justify;padding-right:15px;line-height: 1em;"><span style="font-weight:bold">Cocina.</span> Pueden estar en electrodomésticos,
            cajones y barras, atrás del refrigerador, de estufa o microondas y en la parte de
            abajo de cómodas o alacenas. Cajas de cartón, con fruta, refrescos o cervezas.
        </p>
        <p style="text-align:justify;padding-right:15px;line-height: 1em;"><span style="font-weight:bold">Recámaras.</span> En muebles de
            madera, bases de cama, clósets y cajones. Atrás de cabeceras, burós y cuadros decorativos.
        </p>
        <p style="text-align:justify;padding-right:15px;line-height: 1em;"><span style="font-weight:bold">Baño.</span> En muebles de madera,
            detrás del WC, en la tubería, coladeras, detrás de espejos.
        </p>
        <br>
    </div>
    <div style="float:right;padding-right:20px;font-size:10pt;text-align:center;width:250px;margin-top:-350px;">
        <p style="font-size:12pt;line-height: 1em;">Diseñemos un plan a la medida de tus necesidades. <br> El éxito está en el constante
            monitoreo y prevención de plagas
        </p>
        <span style="color:green;font-style:italic">Utilizamos productos <br> que no generan <br> riesgo de toxicidad</span>
        <br><br>
        <img src="img/senal2.png" alt="" style="padding-right:2px">
        <span style="color:green;font-style:italic;font-size:12pt">Protegemos a los que más quieres...SIEMPRE</span>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <div>
            <div style="float:left;padding-left:40px;padding-right:40px;padding-top:30px">
                <img src="img/Biofin_logo.png" alt="logo">
            </div>
            <div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:20px">
                <p><span style="color:green;font-weight:bold;font-size:12pt">COTIZACIÓN: </span>  
                    <span style="color:red;font-weight:bold;font-size:12pt">{{$quotation->id_quotation}}</span>
                <br><span style="color:green;font-weight:bold;font-size:12pt">FECHA: </span>
                    <?php echo date("d/m/y",strtotime($quotation->date))?> | <?php echo date("H:i",strtotime($quotation->date))?>
                <br><span style="color:green;font-weight:bold;font-size:12pt">AGENTE: </span>
                @if($user == null)	
                <span></span>
            @else 
                <span>{{$user->name}}</span>
            @endif
                <br><br><span style="color:green;font-size:11pt">Licencia No. 18AP010010016
                <br>WhatsApp: 4491507531 | www.biofin.mx
                <br>Facebook: BIOFIN Fumigaciones Orgánicas
                <br>Email: contacto@biofin.mx</span></p>
            </div>	
        </div>
        <br><br><br><br><br><br><br><br><br><br><br>
        <div style="margin: auto;padding-left:40px">
            <p><span style="font-size:14pt"> Atención a {{$quotation->name}}</span>
            <br><span style="font-size:11pt;font-weight:bold">{{$quotation->establishment_name}}</span>
            <br><span style="font-size:9pt">{{$quotation->address}}, {{$quotation->municipality}}</span>
            <br><span style="font-size:9pt"> Tel. {{$quotation->cellphone}}</span><br>
            @if($quotation->email != null)
                <span style="font-size:9pt"> {{$quotation->email}}</span></p>
            @endif
        </div>
        <br><br><br><br><br><br><br>
        <div style="magin:auto;padding-left:40px;padding-right:40px">
            <p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
            el Manejo Integral y Control de Plagas</p>
        </div>
        <div style="margin-left:40px;margin-right:40px;border-bottom: 2px solid;">
            <p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE SERVICIO: </span>{{$quotation->e_name}}</p>
        </div>
        <div style="margin-left:40px;margin-right:40px;border-bottom: 2px solid;">
        <p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE PLAGAS: </span>@foreach ( $plagues as $item)
            {{$item->name}},	
        @endforeach</p>
        </div>
        <div style="margin-left:40px;margin-right:40px;border-bottom: 2px solid;">
            <p><span style="font-size:12pt;font-weight:bold;color:green">AREA A FUMIGAR: </span>{{$area}} mt2</p>
        </div>
        <div style="margin:auto;padding-left:40px;padding-right:40px;height:250px;text-overflow:min-content">
            <p style="text-align:justify;line-height: 0.9em;font-size:11pt"><span style="font-size:12pt;font-weight:bold;color:green">DESCRIPCIÓN DEL SERVICIO</span>
            <br>@if($speech != null)
            {{$x}}
            @endif</p>
            @if($extra->id != 3)
		        <p style="text-align:right;line-height: 1em;font-size:11pt">* El monto extra presentado se refiere a: "<span style="font-weight:bold;color:green">{{$extra->description}}</span>"</p>
		    @endif
        </div>
        <div style="margin:auto;padding-left:40px;padding-right:40px">
            <div style="float:right;margin-top:30px">
                <br><br><br><br><br>
                <br><br><br><br><br>
                <br><br><br><br>
                <span style="color:green;font-size:12pt;font-weight:bold;">PRECIO: </span><span>${{$quotation->price}}.00</span> <br>
                <span style="color:green;font-size:12pt;font-weight:bold">DESCUENTO: </span><span>${{$d}}.00</span><br>
                <span style="color:green;font-size:12pt;font-weight:bold">EXTRA: </span><span>${{$e}}.00</span><br>
            <span style="color:green;font-size:14pt;font-weight:bold">SUBTOTAL: </span><span style="font-size:14pt;font-weight:bold">${{$subtotal}}.00</span> <br>
                <span style="font-size:8pt;text-align:right">*PRECIO MÁS IVA</span>
            </div>
            <br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br><br>
            <img src="img/Biofin_sello.png" alt="" width="85px" height="85px" style="float:right;margin-right:300px;margin-top:25px">	
        </div>
        <br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br>
        <div style="margin:auto;padding-left:40px;padding-right:40px">
            <p style="float:left;font-size:8pt">*Cotización válida por 30 días naturales.<br>
            *Cotización única e intransferible.<br>*Sujeta a restricciones. 
            No aplica ninguna otra<br>promoción o descuento.</p>
        </div>
</body>
</html>