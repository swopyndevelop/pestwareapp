<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<title>Cotización</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
		font-family: 'Montserrat'
	}
</style>
<body>
        <div>
                <div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
                    <img src="{{ asset( 'img/' . $imagen->pdf_logo) }}" alt="" width="200px" height="200px">
                </div>
                <div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
                    <p><span style="color:black;font-weight:bold;font-size:11pt">COTIZACIÓN: </span>  
                        <span style="color:red;font-weight:bold;font-size:11pt">{{$quotation}}</span>
                    <br><span style="color:black;font-weight:bold;font-size:11pt">FECHA: </span>
                        <?php echo date("d/m/y",strtotime($date))?> | <?php echo date("H:i",strtotime($date))?>
                    <br><span style="color:black;font-weight:bold;font-size:11pt">AGENTE: </span>
                    @if($user == null)	
                    <span></span>
                @else 
                    <span>{{$user->name}}</span>
                @endif
                <br><br><span style="color:black;font-size:10pt;font-weight:bold">Licencia No. {{$imagen->licence}}</span>
                <br><span style="font-weight:bold">WhatsApp:</span> {{$imagen->phone}}
                <br><span style="font-weight:bold">Facebook:</span> {{$imagen->facebook}}
                </div>	
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br>
            <div style="margin: auto;padding-left:40px">
                <p><span style="font-size:13pt"> Atención a {{$name}}</span>
                <br><span style="font-size:18pt;font-weight:bold">{{$establishmentName}}</span>
                <br><span style="font-size:8pt">{{$address}}, {{$municipality}}</span>
                <br><span style="font-size:8pt"> Tel. {{$cellphone}}</span><br>
                @if($email != null)
                    <span style="font-size:8pt"> {{$email}}</span></p>
                @endif
            </div>
            <br><br><br><br><br><br><br>
            <div style="magin:auto;padding-left:40px;padding-right:40px">
                <p style="font-size:11pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
                el Manejo Integral y Control de Plagas</p>
            </div>
            <div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
                <p><span style="font-size:11pt;font-weight:bold;color:black">TIPO DE SERVICIO: </span>{{$est->name}}</p>
            </div>
            <div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
            <p><span style="font-size:11pt;font-weight:bold;color:black">TIPO DE PLAGAS: </span> @foreach ( $nam as $item)
                {{$item->name}},
                @endforeach
            </p>
            </div>
            <div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
                <p><span style="font-size:11pt;font-weight:bold;color:black">AREA A FUMIGAR: </span>{{$area}} mt2</p>
            </div>
            <div style="margin:auto;padding-left:40px;padding-right:40px;height:250px;text-overflow:min-content">
                <p style="text-align:justify;line-height: 0.9em;font-size:10pt"><span style="font-size:11pt;font-weight:bold;color:black">DESCRIPCIÓN DEL SERVICIO</span>
                    <br><br>
                    {{$pricesList->description}}
                </p>
            </div>
        <br><br><br>
    <div style="margin:auto;padding-left:40px;padding-right:40px;">
        <div style="float:right;">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <span style="color:black;font-size:11pt;font-weight:bold;">PRECIO: </span><span>${{$price}}.00</span> <br>
            <span style="color:black;font-size:11pt;font-weight:bold">DESCUENTO: </span><span>${{$d}}.00</span><br>
            <span style="color:black;font-size:11pt;font-weight:bold">SUBTOTAL: </span><span>${{$pre}}.00</span><br>
            <span style="color:black;font-size:11pt;font-weight:bold">EXTRA: </span><span>${{$e}}.00</span><br>
            <span style="color:black;font-size:13pt;font-weight:bold">TOTAL: </span><span style="font-size:14pt;font-weight:bold">${{$subtotal}}.00</span> <br>
            <span style="font-size:6pt;text-align:right">*PRECIO MÁS IVA</span>
        </div>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <img src="{{ asset( 'img/' . $imagen->pdf_sello) }}" alt="" width="85px" height="85px" style="float:right;margin-right:300px;">
        <div>
        <br><br>
        <p style="float:left;font-size:6pt">*Cotización válida por 30 días naturales.<br>
            *Cotización única e intransferible.<br>*Sujeta a restricciones. 
            No aplica ninguna otra<br>promoción o descuento.
        </p>
        </div>
</body>
</html>