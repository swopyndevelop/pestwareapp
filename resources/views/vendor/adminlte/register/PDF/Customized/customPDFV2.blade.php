<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Servicio</title>
</head>
<style>
    @page {
        margin: 0;
    }

    body {
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
    }

    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }

    caption, td, th {
        padding: 0.3em;
    }

    th, td {
        border-bottom: 1px solid black;
        width: 20px;
    }

    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
{{--//headers--}}
<div style="margin: 0px;">
    <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

        <div style="text-align: center; margin-bottom: 0px; margin-top: 20px;">
            <h2 style="font-weight: bold; margin-bottom: 0px">COTIZACIÓN DE SERVICIO</h2>
        </div>

        <div style="width: 130px; display: inline-block; vertical-align: top;">
            @if($imagen->pdf_logo == null)
                <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
            @else
                <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="130px">
            @endif
        </div>

        <div style="width: 345px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
            <p style="margin-top: 0px; margin-bottom: 0">
                <span style="color:black;font-size:9.5pt">Responsable Sanitario: {{$jobCenterProfile->health_manager}} | Licencia No: {{$jobCenterProfile->license}}
                    | Whatsapp: {{$jobCenterProfile->whatsapp_personal}} | Teléfono: {{$jobCenterProfile->cellphone}} | Razón Social: {{ $jobCenterProfile->business_name }} |
                    {{ $jobCenterProfile->rfc_country }}: {{ $jobCenterProfile->rfc }}| @if($addressProfileJobCenter) {{ $addressProfileJobCenter->street }} #{{ $addressProfileJobCenter->num_ext }}, {{ $addressProfileJobCenter->location }},
                    {{ $addressProfileJobCenter->municipality }}, {{ $addressProfileJobCenter->state }}. @endif | Email: {{ $jobCenterProfile->email_personal }} | Facebook: {{$jobCenterProfile->facebook_personal}}.
                </span>
            </p>
        </div>
        <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
            @if($qrcode != null)
                <img src="data:image/png;base64, {!! $qrcode !!}" width="100px"><br>
                <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Licencia Sanitaria</span>
            @endif
        </div>

        <div style="width: 150px; display: inline-block; margin-top: 0px; font-size: 8pt; vertical-align: top; text-align: right">
            <p style="margin-bottom: 2px; margin-top: 0px">
                <span style="font-size:8pt; font-weight: bold">NO. COTIZACIÓN: </span>
                <span style="color:red;font-weight:bold;font-size:8pt;">{{$quotation->id_quotation}}</span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">FECHA: </span>
                <span style="font-size:8pt;"> <?php echo date("d/m/y",strtotime($quotation->date))?></span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">HORA: </span>
                <span style="font-size:8pt;"><?php echo date("H:i",strtotime($quotation->date))?></span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">AGENTE: </span>
                <span style="font-size:8pt;">@if($user == null)<span></span>@else<span>{{$user->name}}</span>@endif</span>
            </p>
        </div>

    </div>
</div>
<div style="text-align: left; ; margin: 0 20px 0 20px;">
    <p style="margin-top: 0px; margin-bottom: 7px">
        @if($jobCenterProfile->id == 291)
        <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/tca-logo.png" alt="logo" width="150px" height="40px">
            <br>
        @endif
        <span style="font-size:15pt; font-weight: bold"> Atención a {{$quotation->name}} @if($quotation->billing != null) | {{$quotation->billing}} @endif</span><br>
        <span style="font-size:10pt">
            @if($quotation->establishment_name != null) {{$quotation->establishment_name}} | @endif
            @if($quotation->address != null) Dirección: {{$quotation->address}} @endif
            @if($quotation->municipality != null) {{$quotation->municipality}} @endif
            @if($quotation->cellphone != null) | Tel: {{$quotation->cellphone}} | @endif
            @if($quotation->email != null ) Email: {{$quotation->email}} @endif
        </span>
    </p>
</div>
<br><br>
{{--//Body--}}
<div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; margin-top: -40px; border: solid 1px">
    @if($imagen->id_company != 268)
    <p style="font-size:11pt; margin-left: 5px; margin-right: 5px">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en
        el Manejo Integral y Control de Plagas
    </p>
    @else
        <p style="font-size:11pt; margin-left: 5px; margin-right: 5px">Atendiendo su solicitud, adjunto propuesta para nuestro plan empresarial del Software PestWare
            App.
        </p>
    @endif
    <p style="margin-left: 5px; margin-right: 5px"><span style="font-size:11pt;font-weight:bold;color:black">TIPO DE SERVICIO: </span>{{$quotation->e_name}}</p>
    @if($imagen->id_company != 268)
        <p style="margin-left: 5px; margin-right: 5px"><span style="font-size: 11pt;font-weight:bold;color:black">TIPO DE PLAGAS: </span>
            @foreach ( $plagues as $item)
                {{$item->name}},
            @endforeach
        </p>
    @endif
    <p style="line-height: 0.9em;font-size:10pt; margin-left: 5px; margin-right: 5px"><span style="font-size:11pt;font-weight:bold;color:black">DESCRIPCIÓN DEL SERVICIO</span>
        <br><br>
        {!! nl2br(e($quotation->description)) !!}
    </p>

</div>
{{--//Table--}}
<div style="margin-bottom: 140px; margin-left: 20px; margin-top: 0">

    <div style="width: 754px; display: inline-block; margin-top: -1px">

        <table style="width: 100%; text-align: center; border: 1px solid">
            <thead>
            <tr style="font-size: 9.5pt">
                @if($imagen->id_company != 268)
                <th style="font-size:9pt;color:black;font-weight:bold">CONCEPTO</th>
                @if($isCustomService == false)
                <th style="font-size:9pt;color:black;font-weight:bold">CANTIDAD</th>
                <th style="font-size:9pt;color:black;font-weight:bold">FRECUENCIA DE SERVICIO</th>
                <th style="font-size:9pt;color:black;font-weight:bold">FRECUENCIA<br><span style="font-size:9pt">PERÍODO</span></th>
                <th style="font-size:9pt;color:black;font-weight:bold">PLAZO<br><span style="font-size:9pt">CONTRATADO</span><br><span style="font-size:9pt">EN MESES</span></th>
                <th style="font-size:9pt;color:black;font-weight:bold">PRECIO<br>UNITARIO</th>
                @endif
                @if($imagen->id_company != 338 && $imagen->id_company != 457 && $imagen->id_company != 504)<th style="font-size:9pt;color:black;font-weight:bold">SUBTOTAL <br> MENSUAL</th>@endif
                    @if($imagen->id_company != 445)<th style="font-size:9pt;color:black;font-weight:bold">TOTAL</th>@endif
                @else
                <th style="font-size:9pt;color:black;font-weight:bold">CONCEPTO</th>
                <th style="font-size:9pt;color:black;font-weight:bold">CANTIDAD</th>
                <th style="font-size:9pt;color:black;font-weight:bold">FRECUENCIA<br><span style="font-size:9pt">MENSUAL</span></th>
                <th style="font-size:9pt;color:black;font-weight:bold">PLAZO<br><span style="font-size:9pt">CONTRATADO</span><br><span style="font-size:9pt">EN MESES</span></th>
                <th style="font-size:9pt;color:black;font-weight:bold">PRECIO<br>UNITARIO</th>
                <th style="font-size:9pt;color:black;font-weight:bold">SUBTOTAL</th>
                @endif
            </tr>
            </thead>
            <tbody style="font-size: 8pt">
            @foreach ($c as $item)
                @if($imagen->id_company != 268)
                <tr>
                    <td style="font-size:10pt">{{$item->concept}}</td>
                    @if($item->id_type_service != 8)
                    <td style="text-align:center;font-size:10pt">{{$item->quantity}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->type_service}}</td>
                    <td style="text-align:center;font-size:10pt">@if($item->id_type_service == 6 || $item->id_type_service== 7)--@else{{$item->frecuency_month}}@endif</td>
                    <td style="text-align:center;font-size:10pt">@if($item->id_type_service == 7)-- @else {{$item->term_month}} @endif</td>
                    <td style="text-align:center;font-size:10pt; margin-top: -10px; margin-bottom: 0px">{{ $symbol_country }}@convert($item->unit_price) @if($item->id_type_service != 8) <br>*Costo por aplicación @endif</td>
                    @endif
                    <td style="text-align:center;font-size:10pt; margin-top: -10px; margin-bottom: 0px">
                        {{ $symbol_country }}@convert($item->subtotal_calc) @if($item->id_type_service != 7 && $item->id_type_service != 8)<br>*Costo por período @endif
                    </td>
                    @if($imagen->id_company != 338 && $imagen->id_company != 445 && $imagen->id_company != 457 && $imagen->id_company != 504)
                        <td style="text-align:center;font-size:10pt; margin-top: -10px; margin-bottom: 0px">
                            {{ $symbol_country }}@convert($item->total)</td>
                    @endif
                </tr>
                @else
                    <tr>
                        <td style="font-size:10pt">{{$item->concept}}</td>
                        @if($item->id_type_service != 8)
                        <td style="text-align:center;font-size:10pt">{{$item->quantity}}</td>
                        <td style="text-align:center;font-size:10pt">{{$item->frecuency_month}}</td>
                        <td style="text-align:center;font-size:10pt">{{$item->term_month}}</td>
                        @endif
                        <td style="text-align:center;font-size:10pt; margin-top: -10px; margin-bottom: 0px">{{ $symbol_country }}@convert($item->unit_price)</td>
                        <td style="text-align:center;font-size:10pt; margin-top: -10px; margin-bottom: 0px">{{ $symbol_country }}@convert($item->subtotal)</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
</div>
{{--//Footer--}}
<footer style="position: absolute; bottom: 20px; margin-left: 20px; margin-right: 20px;">
    @if($imagen->id_company != 268)
        <div style="border-bottom: solid 1px;">
            @if($jobCenterProfile->id == 291)
            <div style="text-align: center">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/conocer.jpg" alt="logo" width="100px" height="50px">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/tif-sqf.png" alt="logo" width="100px" height="50px">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/sedex.jpg" alt="logo" width="50px" height="50px">
            </div>
            @endif
            <div style="width: 300px; display: inline-block; vertical-align: top;">
                <p style="font-size: 8pt; margin-top: 125px;">
                    {!! nl2br(e($pricesList->legend)) !!}
                </p>
            </div>
            <div style="width: 200px; display: inline-block; vertical-align: top; margin-top: 25px">
                <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_sello }}" alt="" width="150px" height="150px" style="margin-bottom: 5px">
            </div>
            <div style="width: 238px; display: inline-block; vertical-align: top; text-align: right; margin-top: 145px;">
                @if($imagen->id_company != 338 && $imagen->id_company != 457 && $imagen->id_company != 504 && $imagen->id_company != 644)
                    <p style="color:black;font-size:11pt;font-weight:bold; margin-top: 0px; margin-bottom: 0px">SUBTOTAL (MENSUAL): <span>{{ $symbol_country }}@convert($quotation->price)</span></p>
                    <p style="color:black;font-size:13pt;font-weight:bold; margin-bottom: 0px; margin-top: 0px">TOTAL: <span>{{ $symbol_country }}@convert($quotation->total)</span></p>
                @else
                    @if($imagen->id_company == 457 || $imagen->id_company == 504)
                        <p style="color:black;font-size:13pt;font-weight:bold; margin-bottom: 0px; margin-top: 0px">SUBTOTAL: <span>{{ $symbol_country }}@convert($quotation->price)</span></p>
                        <p style="color:black;font-size:13pt;font-weight:bold; margin-bottom: 0px; margin-top: 0px">ITBM: <span>{{ $symbol_country }}@convert($quotation->customTax)</span></p>
                        <p style="color:black;font-size:13pt;font-weight:bold; margin-bottom: 0px; margin-top: 0px">TOTAL: <span>{{ $symbol_country }}@convert($quotation->customTotal)</span></p>
                    @else
                        <p style="color:black;font-size:13pt;font-weight:bold; margin-bottom: 0px; margin-top: 0px">TOTAL: <span>{{ $symbol_country }}@convert($quotation->price)</span></p>
                    @endif
                @endif
            </div>
        </div>
    @else
        <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_sello }}" alt="" width="170px" height="170px" style="margin-bottom: 5px">
        <p style="font-size:8pt">
            *Cotización válida por 30 días naturales (Mayo 2021). Cotización única e intransferible. Sujeta a restricciones. No aplica ninguna otra promoción o descuento.
            <br>
            *Cada usuario es único e intransferible, si el sistema detecta mal uso de las cuentas se bloqueará y suspenderá el servicio por completo.
            <br>
            *Costo por aplicación.
            <br>
            *Precio más IVA.
            <br>
            *El costo por sucursal es por sede de operación con técnicos.
        </p>
    @endif

</footer>
</body>
</html>