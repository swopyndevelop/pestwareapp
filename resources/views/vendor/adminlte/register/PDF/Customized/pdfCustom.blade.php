<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Cotización</title>
</head>
<style>
    @page {
        margin: 0;
    }

    body {
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
    }

    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }

    caption, td, th {
        padding: 0.3em;
    }

    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }

    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
{{--//headers--}}
<div style="margin: 0px;">
    <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: 15px">

        <div style="width: 317px; display: inline-block; vertical-align: top;">
            @if($imagen->pdf_logo == null)
                <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="250px">
            @else
                <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="250px">
            @endif
        </div>

        <div style="width: 430px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
            <p style="margin-top: 0px; margin-bottom: 0; font-weight: bold; font-size: 14px; text-align: center; text-transform: uppercase">
                Profesionales en el control integral de plagas
            </p>
            <div style="background-color: #8f0505; color: #8f0505; height: 7px; width: 400px; margin-left: 15px">
            </div>
            <br><br>
            <p style="margin-top: 0px; margin-bottom: 0; font-weight: bold; font-size: 10px; text-align: right; text-transform: uppercase; padding-right: 30px">
                <span style="font-size:8pt; font-weight: bold">NO. COTIZACIÓN: </span>
                <span style="color:red;font-weight:bold;font-size:8pt;">{{$quotation->id_quotation}}</span> <br>
                {{ $addressProfileJobCenter->municipality }}, {{ $addressProfileJobCenter->state }}
                A {{ \Carbon\Carbon::parse($quotation->date)->format('d/m/Y') }}
            </p>
        </div>

    </div>
</div>

{{--//Body--}}
<div style="margin-left: 20px; margin-right: 20px; padding-left: 0px;">

    <p style="font-size:11pt; margin-left: 5px;margin-right: 5px">
        {{$quotation->name}} <br>
        @if($quotation->establishment_name != null) {{$quotation->establishment_name}} | @endif
        @if($quotation->address != null) Dirección: {{$quotation->address}} @endif
        @if($quotation->municipality != null) {{$quotation->municipality}} @endif
        @if($quotation->cellphone != null) | Tel: {{$quotation->cellphone}} | @endif
        @if($quotation->email != null ) Email: {{$quotation->email}} @endif
    </p>
    <p style="font-size:11pt; margin-left: 5px;margin-right: 5px; font-size: 14px">POR MEDIO DE LA PRESENTE NOS PERMITIMOS PONER A SU ATENTA
        CONSIDERACION LA COTIZACION PARA EL SERVICIO DE FUMIGACION CONTRA INSECTOS RASTREROS Y ROEDORES COMO SIGUE:
    </p>
    <p style="margin-left: 5px;margin-right: 5px"><u><span style="font-size: 11pt;font-weight:bold;color:black">PLAGAS A CONTROLAR: </span></u>
        @foreach ( $plagues as $item)
            {{$item->name}},
        @endforeach
    </p>
    <p style="margin-left: 5px;margin-right: 5px"><u><span style="font-size:11pt;font-weight:bold;color:black">AREA A FUMIGAR: </span></u>{{$area}} mt2</p>
    <p style="margin-left: 5px;margin-right: 5px"><u><span style="font-size:11pt;font-weight:bold;color:black">FRECUENCIA DEL SERVICIO: </span></u>
        @if($quotation->description == null)
            Servicio Único
        @else
            Servicio Mensual
        @endif
    </p>
    <p style="margin-left: 5px;margin-right: 5px"><u><span style="font-size:11pt;font-weight:bold;color:black">INSECTICIDAS A UTILIZAR: </span></u>{{ $quotation->pesticide_product }}</p>
    <p style="margin-left: 5px;margin-right: 5px"><u><span style="font-size:11pt;font-weight:bold;color:black">GARANTÍA: </span></u>{{ $pricesList->warranty }}</p>
    <p style="margin-left: 5px;margin-right: 5px; font-size: 20px;"><u><span style="font-size:11pt;font-weight:bold;color:black">IMPORTE DEL SERVICIO: </span></u>
        <u style="color: red"><span style="color: red">
           {{$symbol_country}} @convert($subtotal) + IVA
        </span></u>
    </p>
    <br><br>
    <p style="text-align:center;line-height: 0.9em;font-size:10pt; margin-left: 5px;margin-right: 5px"><span style="font-size:11pt;font-weight:bold;color:black">ALCANCES DEL SERVICIO</span>
        <br><br>
        @if($quotation->description != null)
            {!! nl2br(e($quotation->description)) !!}
        @else
            {!! nl2br(e($pricesList->description)) !!}
        @endif
    </p>
    <p style="font-size:11pt; margin-left: 5px;margin-right: 5px">
        Esperando poder servirle quedo a sus órdenes para cualquier duda y/o comentarios
    </p>
</div>
{{--//Footer--}}
<footer style="position: absolute; bottom: 40px;">

    <div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; margin-top: 0px;">
        <div style="width: 300px; text-align: center">
            <p style="margin-left: 20px">
                A T E N T A M E N T E<br><br>
                {{ $user->name }} <br>
                ASESOR TECNICO <br>
                <u>{{ $jobCenterProfile->email_personal }}</u><br>
                <u style="color: #0a6aa1">TEL: {{ $jobCenterProfile->cellphone }}</u>
            </p>
        </div>

    </div>
    <div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; margin-top: 0px;text-align: center">
        <div style="display: inline-block; width: 247px; text-align: center">
            <p style="margin: 0 0 0 0; color: #0a6aa1">
                {{ $jobCenterProfile->web_page }}
            </p>
        </div>
        <div style="display: inline-block; width: 247px; text-align: center">
            <p style="margin: 0 0 0 0; color: #0a6aa1">
                {{ $jobCenterProfile->cellphone }}
            </p>
        </div>
        <div style="display: inline-block; width: 247px; text-align: center">
            <p style="margin: 0 0 0 0; color: #0a6aa1">
                {{ $jobCenterProfile->email_personal }}
            </p>
        </div>
        <div style="background-color: #8f0505; color: #8f0505">
            -
        </div>
        <p style="margin: 0 0 0 0; font-size: 14px">
            {{ $addressProfileJobCenter->street }} {{ $addressProfileJobCenter->num_ext }} {{ $addressProfileJobCenter->location }}
            {{ $addressProfileJobCenter->municipality }}, {{ $addressProfileJobCenter->state }}, CP: {{ $addressProfileJobCenter->zip_code }}
        </p>
    </div>

</footer>
</body>
</html>