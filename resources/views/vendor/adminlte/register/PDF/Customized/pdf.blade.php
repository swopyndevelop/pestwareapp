<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Servicio</title>
</head>
<style>
    @page {
        margin: 0;
    }

    body {
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
    }

    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }

    caption, td, th {
        padding: 0.3em;
    }

    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }

    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
{{--//headers--}}
<div style="margin: 0px;">
    <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

        <div style="text-align: center; margin-bottom: 0px; margin-top: 20px;">
            <h2 style="font-weight: bold; margin-bottom: 0px">COTIZACIÓN DE SERVICIO</h2>
        </div>

        <div style="width: 130px; display: inline-block; vertical-align: top;">
            @if($imagen->pdf_logo == null)
                <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
            @else
                <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="130px">
            @endif
        </div>

        <div style="width: 345px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
            <p style="margin-top: 0px; margin-bottom: 0">
                <span style="color:black;font-size:9.5pt">Responsable Sanitario: {{$jobCenterProfile->health_manager}} | Licencia No: {{$jobCenterProfile->license}}
                    | Whatsapp: {{$jobCenterProfile->whatsapp_personal}} | Teléfono: {{$jobCenterProfile->cellphone}} | Razón Social: {{ $jobCenterProfile->business_name }} |
                    {{ $jobCenterProfile->rfc_country }}: {{ $jobCenterProfile->rfc }}| @if($addressProfileJobCenter) {{ $addressProfileJobCenter->street }} #{{ $addressProfileJobCenter->num_ext }}, {{ $addressProfileJobCenter->location }},
                    {{ $addressProfileJobCenter->municipality }}, {{ $addressProfileJobCenter->state }}. @endif | Email: {{ $jobCenterProfile->email_personal }} | Facebook: {{$jobCenterProfile->facebook_personal}}.
                </span>
            </p>
        </div>
        <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
            @if($qrcode != null)
                <img src="data:image/png;base64, {!! $qrcode !!}" width="100px"><br>
                <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Licencia Sanitaria</span>
            @endif
        </div>

        <div style="width: 150px; display: inline-block; margin-top: 0px; font-size: 8pt; vertical-align: top; text-align: right">
            <p style="margin-bottom: 2px; margin-top: 0px">
                <span style="font-size:8pt; font-weight: bold">NO. COTIZACIÓN: </span>
                <span style="color:red;font-weight:bold;font-size:8pt;">{{$quotation->id_quotation}}</span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">FECHA: </span>
                <span style="font-size:8pt;"> <?php echo date("d/m/y",strtotime($quotation->date))?></span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">HORA: </span>
                <span style="font-size:8pt;"><?php echo date("H:i",strtotime($quotation->date))?></span>
            </p>
            <p style="margin-bottom: 2px; margin-top: 2px">
                <span style="font-size:8pt; font-weight: bold">AGENTE: </span>
                <span style="font-size:8pt;">@if($user == null)<span></span>@else<span>{{$user->name}}</span>@endif</span>
            </p>
        </div>

    </div>
</div>
<div style="text-align: left; ; margin: 0 20px 0 20px;">
    <p style="margin-top: 0px; margin-bottom: 7px">
        @if($jobCenterProfile->id == 291)
            <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/tca-logo.png" alt="logo" width="150px" height="45px">
            <br>
        @endif
        <span style="font-size:15pt; font-weight: bold"> Atención a {{$quotation->name}}</span><br>
        <span style="font-size:10pt">
            @if($quotation->establishment_name != null) {{$quotation->establishment_name}} | @endif
            @if($quotation->address != null) Dirección: {{$quotation->address}} @endif
            @if($quotation->municipality != null) {{$quotation->municipality}} @endif
            @if($quotation->cellphone != null) | Tel: {{$quotation->cellphone}} | @endif
            @if($quotation->email != null ) Email: {{$quotation->email}} @endif
        </span>
    </p>
</div>
<br><br>
{{--//Body--}}
<div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; margin-top: -40px; border: solid 1px">

    <p style="font-size:11pt; margin-left: 5px;margin-right: 5px">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en
        el Manejo Integral y Control de Plagas
    </p>
    <p style="margin-left: 5px;margin-right: 5px"><span style="font-size:11pt;font-weight:bold;color:black">TIPO DE SERVICIO: </span>{{$quotation->e_name}}</p>
    <p style="margin-left: 5px;margin-right: 5px"><span style="font-size: 11pt;font-weight:bold;color:black">TIPO DE PLAGAS: </span>
        @foreach ( $plagues as $item)
            {{$item->name}},
        @endforeach
    </p>
    <p style="margin-left: 5px;margin-right: 5px"><span style="font-size:11pt;font-weight:bold;color:black">AREA A FUMIGAR: </span>{{$area}} mt2</p>
    <p style="text-align:justify;line-height: 0.9em;font-size:10pt; margin-left: 5px;margin-right: 5px"><span style="font-size:11pt;font-weight:bold;color:black">DESCRIPCIÓN DEL SERVICIO</span>
        <br><br>
        {!! nl2br(e($pricesList->description)) !!}
    </p>

</div>
{{--//Footer--}}
<footer style="position: absolute; bottom: 20px;">

    <div style="margin-left: 20px; margin-right: 20px; padding-left: 0px; margin-top: 0px; border-bottom: solid 1px;">
        @if($jobCenterProfile->id == 291)
            <div style="text-align: center">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/conocer.jpg" alt="logo" width="100px" height="50px">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/tif-sqf.png" alt="logo" width="100px" height="50px">
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/sedex.jpg" alt="logo" width="50px" height="50px">
            </div>
        @endif
        <div style="width: 295px; display: inline-block; vertical-align: top;">
            <p style="font-size:8pt; margin-top: 95px">
                {!! nl2br(e($pricesList->legend)) !!}
            </p>
        </div>
        <div style="width: 200px; display: inline-block; justify-content: left ;vertical-align: top; margin-top: 0px">
            <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_sello }}" alt="" width="170px" height="170px" style="margin-bottom: 5px">
        </div>
        <div style="width: 250px; display: inline-block; vertical-align: top; text-align: right; margin-top: 60px;">
            <span style="color:black;font-size:11pt;font-weight:bold;">PRECIO: </span><span>{{ $symbol_country }} @convert($quotation->price)</span><br>
            <span style="color:black;font-size:11pt;font-weight:bold">DESCUENTO: </span><span>{{ $symbol_country }} @convert($d)</span><br>
            <span style="color:black;font-size:11pt;font-weight:bold">SUBTOTAL: </span><span>{{ $symbol_country }} @convert($pre)</span><br>
            <span style="color:black;font-size:11pt;font-weight:bold">EXTRA: </span><span>{{ $symbol_country }} @convert($e)</span><br>
            <span style="color:black;font-size:13pt;font-weight:bold">TOTAL: </span><span style="font-size:14pt;font-weight:bold">{{ $symbol_country }} @convert($subtotal)</span>
        </div>
    </div>

</footer>
</body>
</html>