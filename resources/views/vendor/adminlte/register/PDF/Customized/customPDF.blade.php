<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Cotización</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
	}
</style>
<body>
        <div>
                <div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
                    <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" width="200px" height="200px">
                </div>
                <div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
                    <p><span style="color:black;font-weight:bold;font-size:12pt">COTIZACIÓN:</span>
                        <span style="color:red;font-weight:bold;font-size:12pt">{{$quotation->id_quotation}}</span>
                    <br><span style="color:black;font-weight:bold;font-size:12pt">FECHA: </span>
                        <?php echo date("d/m/y",strtotime($quotation->date))?> | <?php echo date("H:i",strtotime($quotation->date))?>
                    <br><span style="color:black;font-weight:bold;font-size:12pt">AGENTE: </span>
                    @if($user == null)
                    <span></span>
                @else
                    <span>{{$user->name}}</span>
                @endif
                <br><br><span style="color:black;font-size:11pt;font-weight:bold">Licencia No. {{$imagen->licence}}</span>
                <br><span style="font-weight:bold">WhatsApp:</span> {{$imagen->phone}}
                <br><span style="font-weight:bold">Facebook:</span> {{$imagen->facebook}}
                </div>
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br>
            <div style="margin: auto;padding-left:40px">
                <p><span style="font-size:14pt"> Atención a {{$quotation->name}}</span>
                <br><span style="font-size:11pt;font-weight:bold">{{$quotation->establishment_name}}</span>
                <br><span style="font-size:9pt">{{$quotation->address}}, {{$quotation->municipality}}</span>
                <br><span style="font-size:9pt"> Tel. {{$quotation->cellphone}}</span><br>
                @if($quotation->email != null)
                    <span style="font-size:9pt"> {{$quotation->email}}</span></p>
                @endif
            </div>
            <br><br><br><br><br><br><br>
            <div style="magin:auto;padding-left:40px;padding-right:40px">
                <p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en
                el Manejo Integral y Control de Plagas</p>
            </div>
            <div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
                <p><span style="font-size:12pt;font-weight:bold;color:black">TIPO DE SERVICIO: </span>{{$quotation->e_name}}</p>
            </div>
            <div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
            <p><span style="font-size:12pt;font-weight:bold;color:black">TIPO DE PLAGAS: </span>@foreach ( $plagues as $item)
                {{$item->name}},
            @endforeach</p>
            </div>
        <br>
        <div style="margin-bottom: 0px; text-align: left; padding-left: 32px; margin-top: 0px">

            <div style="width: 3px; display: inline-block;">
            </div>
            <div style="width: 715px; display: inline-block;border-bottom: 2px solid">
                <p style="text-align:justify;line-height: 1em;font-size:10.5pt; margin-bottom: 0px;"><span style="font-size:12pt;font-weight:bold;color:black">DESCRIPCIÓN DEL SERVICIO</span>
                    <br><br>
                    {{$quotation->description}}
                </p>
            </div>

        </div>

        {{--//Table Station Monitoring--}}
        <div style="margin-bottom: 0px; margin-left: 40px; margin-top: 0px">

            <div style="width: 713px; display: inline-block;border: 1px solid black; margin-top: 5px">

                <table style="width: 100%; text-align: center">
                    <thead>
                    <tr style="font-size: 9.5pt">
                        <th style="font-size:10pt;color:black;font-weight:bold">CONCEPTO</th>
                        <th style="font-size:10pt;color:black;font-weight:bold">CANTIDAD</th>
                        <th style="font-size:10pt;color:black;font-weight:bold">FRECUENCIA<br><span style="font-size:9pt">MENSUAL</span></th>
                        <th style="font-size:10pt;color:black;font-weight:bold">PLAZO<br><span style="font-size:9pt">CONTRATADO</span><br><span style="font-size:8pt">EN MESES</span></th>
                        <th style="font-size:10pt;color:black;font-weight:bold">PRECIO<br>UNITARIO</th>
                    </tr>
                    </thead>
                    <tbody style="font-size: 8pt">
                    @foreach ($c as $item)
                        <tr>
                            <td style="font-size:10pt">{{$item->concept}}</td>
                            <td style="text-align:center;font-size:10pt">{{$item->quantity}}</td>
                            <td style="text-align:center;font-size:10pt">{{$item->frecuency_month}}</td>
                            <td style="text-align:center;font-size:10pt">{{$item->term_month}}</td>
                            <td style="text-align:center;font-size:10pt; margin-top: -10px; margin-bottom: 0px">{{$item->unit_price}}<br>*Costo por aplicación</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--//Footer--}}
        <footer style="position: absolute; bottom: 10px;height: 50px">
                <div style="margin-left:40px; width:700px;text-align:left">
                    <span style="font-size:7pt">*Cotización válida por 30 días naturales. Cotización única e intransferible. Sujeta a restricciones. No aplica ninguna otra promoción o descuento.</span><br>
                    <span style="font-size:7pt">*Costo por aplicación.</span><br>
                    <span style="font-size:7pt">*Precio más IVA.</span>
                </div>
        </footer>
</body>
</html>