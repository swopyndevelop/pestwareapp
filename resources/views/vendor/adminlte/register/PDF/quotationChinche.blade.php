<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cotización</title>
</head>
<style>
    @page {
		margin: 0;
    }
	body{
        font-family: 'Montserrat';
	}
</style>
<body>
    <div>
        <img src="img/bannerch.png" alt="" width="100%">
    </div>
    <div style="margin-left:20px;margin-right:280px;text-align:justify;font-size:12pt;line-height: 1em;margin-top:-180px">
        <p>Este tipo de insecto se encuentra distribuida a lo largo de
            todo el país y se considera una plaga de salud pública, debido
            a la diversidad de los lugares donde se encuentra, como hoteles,
            hospitales, cines, transporte público, centrales de autobuses,
            aeropuertos, lavanderías y residencias</p>
    </div>
    <div style="margin-left:20px;margin-right:200px;text-align:justify;font-size:12pt;line-height: 1em;margin-top:-15px">
        <p>La chinche se transporta a través de nuestra ropa o equipaje y al llegar
            a casa busca estructuras concavas, oscuras, teniendo como preferencia
            materiales como la madera o textiles.</p>
            <img src="img/text1.png" alt="" width="20%">
    </div>
    <div style="margin-left:150px;margin-right:50px;text-align:justify;font-size:12pt;line-height: 1em;margin-top:-145px">
        <p>Su tasa de reproducción es sumamente alta, llegando a depositar de 6 a 10 huevos diarios y basta sólo una chinche hembra para desarrollar una gran
            plaga.</p>
    </div>
    <div style="margin-left:150px;margin-right:50px;text-align:justify;font-size:12pt;line-height: 1em;margin-top:-25px">
        <p>El principal problema de esta plaga es que se alimenta a través de la picadura
            de personas y animales, las cuales pueden generar infecciones.</p>
    </div>
    <div style="margin-left:20px;margin-right:50px;text-align:justify;font-size:12pt;line-height: 1em;margin-top:-15px">
        <p>Es un insecto que ha desarrollado un alto grado de inmunidad ya que se suelen utilizar remedios
            caseros e insecticidas domésticos para tratar de controlarlas, sin embargo el resultado
            es una plaga que no se termina.</p>
        <p style="color:green;font-size:14pt;font-weight:bold">Contamos con todos los permisos para brindarte un servicio profesional así
            como tu Certificado de Fumigación.</p>
    </div>
    <div style="margin-top:-30px">
        <br>
            <img src="img/bodych.png" alt="" width="150px" height="380px" style="float:left;padding-left:30px"> 
        <div style="width:520px;font-size:13pt;text-align:center;background-color:lightgray;margin-left:10px">
        <span style="color:red;font-size:13pt;font-weight:bold">ZONAS DE ALERTA</span>
        <p style="text-align:justify;padding-right:15px;line-height: 1em;"><span style="font-weight:bold">Recámara.</span> Las encontrarás en
            colchones, bases de cama, burós,
            cabeceras, contactos eléctricos
        </p>
        <p style="text-align:justify;padding-right:15px;line-height: 1em;"><span style="font-weight:bold">Clósets.</span>  Muebles de madera,
            entre la ropa, cobijas o sábanas.
        </p>
        <p style="text-align:justify;padding-right:15px;line-height: 1em;"><span style="font-weight:bold">Sala.</span>  En los sillones, las costuras,
            abajo del mueble, bies del sillón,
            pliegues del tapiz, y cojines.
        </p>
        <br>
        <img src="img/body_ch1.png" alt="" width="15%">
        <img src="img/body_ch2.png" alt="" width="15%">
        <img src="img/body_ch3.png" alt="" width="15%">
    </div>
    <div style="float:right;padding-right:20px;font-size:10pt;text-align:center;width:250px;margin-top:-350px">
        <p style="font-size:11.5pt;line-height: 1em;">Diseñemos un plan a la medida de tus necesidades. <br> El éxito está en el constante
            monitoreo y prevención de plagas
        </p>
        <span style="color:green;font-style:italic">Utilizamos productos <br> que no generan <br> riesgo de toxicidad</span>
        <br><br>
        <img src="img/senal2.png" alt="" style="padding-right:2px">
        <span style="color:green;font-style:italic;font-size:12pt">Protegemos a los que más quieres...SIEMPRE</span>
    </div>
</div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br>
    <div>
            <div style="float:left;padding-left:40px;padding-right:40px;padding-top:30px">
                <img src="img/Biofin_logo.png" alt="logo">
            </div>
            <div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:20px">
                <p><span style="color:green;font-weight:bold;font-size:12pt">COTIZACIÓN: </span>  
                    <span style="color:red;font-weight:bold;font-size:12pt">{{$quotation->id_quotation}}</span>
                <br><span style="color:green;font-weight:bold;font-size:12pt">FECHA: </span>
                    <?php echo date("d/m/y",strtotime($quotation->date))?> | <?php echo date("H:i",strtotime($quotation->date))?>
                <br><span style="color:green;font-weight:bold;font-size:12pt">AGENTE: </span>
                @if($user == null)	
                <span></span>
            @else 
                <span>{{$user->name}}</span>
            @endif
                <br><br><span style="color:green;font-size:11pt">Licencia No. 18AP010010016
                <br>WhatsApp: 4491507531 | www.biofin.mx
                <br>Facebook: BIOFIN Fumigaciones Orgánicas
                <br>Email: contacto@biofin.mx</span></p>
            </div>	
        </div>
        <br><br><br><br><br><br><br><br><br><br><br>
        <div style="margin: auto;padding-left:40px">
            <p><span style="font-size:14pt"> Atención a {{$quotation->name}}</span>
            <br><span style="font-size:11pt;font-weight:bold">{{$quotation->establishment_name}}</span>
            <br><span style="font-size:9pt">{{$quotation->address}}, {{$quotation->municipality}}</span>
            <br><span style="font-size:9pt"> Tel. {{$quotation->cellphone}}</span><br>
            @if($quotation->email != null)
                <span style="font-size:9pt"> {{$quotation->email}}</span></p>
            @endif
        </div>
        <br><br><br><br><br><br><br>
        <div style="magin:auto;padding-left:40px;padding-right:40px">
            <p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
            el Manejo Integral y Control de Plagas</p>
        </div>
        <div style="margin-left:40px;margin-right:40px;border-bottom: 2px solid;">
            <p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE SERVICIO: </span>{{$quotation->e_name}}</p>
        </div>
        <div style="margin-left:40px;margin-right:40px;border-bottom: 2px solid;">
        <p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE PLAGAS: </span>@foreach ( $plagues as $item)
            {{$item->name}},	
        @endforeach</p>
        </div>
        <div style="margin-left:40px;margin-right:40px;border-bottom: 2px solid;">
            <p><span style="font-size:12pt;font-weight:bold;color:green">AREA A FUMIGAR: </span>{{$area}} mt2</p>
        </div>
        <div style="margin:auto;padding-left:40px;padding-right:40px;height:250px;text-overflow:min-content">
            <p style="text-align:justify;line-height: 0.9em;font-size:11pt"><span style="font-size:12pt;font-weight:bold;color:green">DESCRIPCIÓN DEL SERVICIO</span>
            <br>@if($speech != null)
            {{$x}}
            @endif</p>
            @if($extra->id != 3)
		    <p style="text-align:right;line-height: 1em;font-size:11pt">* El monto extra presentado se refiere a: "<span style="font-weight:bold;color:green">{{$extra->description}}</span>"</p>
		    @endif
        </div>
        <div style="margin:auto;padding-left:40px;padding-right:40px">
            <div style="float:right;margin-top:5px">
                <br><br><br><br><br>
                <br><br><br><br><br>
                <br><br><br><br><br><br>
                <span style="color:green;font-size:12pt;font-weight:bold;">PRECIO: </span><span>${{$quotation->price}}.00</span> <br>
                <span style="color:green;font-size:12pt;font-weight:bold">DESCUENTO: </span><span>${{$d}}.00</span><br>
                <span style="color:green;font-size:12pt;font-weight:bold">EXTRA: </span><span>${{$e}}.00</span><br>
            <span style="color:green;font-size:14pt;font-weight:bold">SUBTOTAL: </span><span style="font-size:14pt;font-weight:bold">${{$subtotal}}.00</span> <br>
                <span style="font-size:8pt;text-align:right">*PRECIO MÁS IVA</span>
            </div>
            <br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br><br>
            <img src="img/Biofin_sello.png" alt="" width="85px" height="85px" style="float:right;margin-right:300px;margin-top:40px">	
        </div>
        <br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br>
        <div style="margin:auto;padding-left:40px;padding-right:40px">
            <p style="float:left;font-size:8pt">*Cotización válida por 30 días naturales.<br>
            *Cotización única e intransferible.<br>*Sujeta a restricciones. 
            No aplica ninguna otra<br>promoción o descuento.</p>
        </div>
</body>
</html>