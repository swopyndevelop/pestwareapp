<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Cotización</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
		font-family: 'Montserrat'
	}
</style>
<body>
        <div>
                <div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
                    <img src="img/Biofin_logo.png" alt="logo">
                </div>
                <div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
                    <p><span style="color:green;font-weight:bold;font-size:12pt">COTIZACIÓN: </span>  
                        <span style="color:red;font-weight:bold;font-size:12pt">{{$quotation}}</span>
                    <br><span style="color:green;font-weight:bold;font-size:12pt">FECHA: </span>
                        <?php echo date("d/m/y",strtotime($date))?> | <?php echo date("H:i",strtotime($date))?>
                    <br><span style="color:green;font-weight:bold;font-size:12pt">AGENTE: </span>
                    @if($user == null)	
                    <span></span>
                @else 
                    <span>{{$user->name}}</span>
                @endif
                    <br><br><span style="color:green;font-size:11pt">Licencia No. 18AP010010016
                    <br>WhatsApp: 4491507531 | www.biofin.mx
                    <br>Facebook: BIOFIN Fumigaciones Orgánicas
                    <br>Email: contacto@biofin.mx</span></p>
                </div>	
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br>
            <div style="margin: auto;padding-left:40px">
                <p><span style="font-size:14pt"> Atención a {{$name}}</span>
                <br><span style="font-size:11pt;font-weight:bold">{{$establishmentName}}</span>
                <br><span style="font-size:9pt">{{$address}}, {{$municipality}}</span>
                <br><span style="font-size:9pt"> Tel. {{$cellphone}}</span><br>
                @if($email != null)
                    <span style="font-size:9pt"> {{$email}}</span></p>
                @endif
            </div>
            <br><br><br><br><br><br><br>
            <div style="magin:auto;padding-left:40px;padding-right:40px">
                <p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
                el Manejo Integral y Control de Plagas</p>
            </div>
            <div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
                <p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE SERVICIO: </span>{{$est->name}}</p>
            </div>
            <div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
            <p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE PLAGAS: </span>@foreach ( $nam as $item)
                {{$item->name}},	
            @endforeach</p>
            </div>
            <div style="margin:auto;margin-left:40px;margin-right:40px;height:130px;text-overflow:min-content;">
                    <p style="text-align:justify;line-height: 1em;font-size:10.5pt"><span style="font-size:12pt;font-weight:bold;color:green">DESCRIPCIÓN DEL SERVICIO</span>
                    <br><br>
                    {{$description}}
                    </p>
                </div>
                <div style="margin:auto;margin-left:40px;margin-right:40px;text-overflow:min-content;">
                    <table>
                        <thead style="border-top: 2px solid;border-bottom: 2px solid">
                            <tr>
                                <th style="font-size:10pt;color:green;font-weight:bold">CONCEPTO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;CANTIDAD&nbsp;&nbsp;&nbsp;</th>
                                <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;FRECUENCIA<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">MENSUAL</span>&nbsp;&nbsp;&nbsp;</th>
                                <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PLAZO<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">CONTRATADO</span><br>&nbsp;&nbsp;&nbsp;<span style="font-size:8pt">EN MESES</span>&nbsp;&nbsp;&nbsp;</th>
                                <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PRECIO<br>&nbsp;&nbsp;&nbsp;UNITARIO&nbsp;&nbsp;&nbsp;</th>
                                <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;SUBTOTAL</th>
                            </tr> 
                        </thead>
                        <tbody>
                        @foreach ($array as $item)
                        <tr>
                            <td style="font-size:10pt">{{$item->concept}}</td>
                            <td style="text-align:center;font-size:10pt">{{$item->quantity}}</td>
                            <td style="text-align:center;font-size:10pt">{{$item->frecuency_month}}</td>
                            <td style="text-align:center;font-size:10pt">{{$item->term_month}}</td>
                            <td style="text-align:center;font-size:10pt">{{$item->unit_price}}</td>
                            <td style="text-align:center;font-size:10pt">{{$item->subtotal}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div style="margin-left:20px;margin-right:20px;line-height:1em;margin-top:-3px">
                    <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">PRECIO:<span style="color:black;font-weight:normal"> ${{$price}}.00</span></p>
                    <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">SUBTOTAL:<span style="color:black;"> ${{$subtotal}}.00</span></p>
                    <span style="font-size:8pt">*Cotización válida por 30 días naturales. Cotización única e intransferible. Sujeta a restricciones. No aplica ninguna otra promoción o descuento.</span>
                </div>
</body>
</html>