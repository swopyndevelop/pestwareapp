<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Cotización</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
		font-family: 'Montserrat'
	}
</style>
<body>
	<div>
		<img src="img/Banner_residencial_standar.png" alt="banner" width="100%">
	</div>
	<div style="padding-left:15px;padding-right:15px;text-align:center">
		<p style="font-size:13pt;text-align:justify;line-height:1em;">Somos una empresa especializada en el control de Plagas sustentable, utilizando productos biodegradables
			y orgánicos. Controlamos más de 600 tipos de insectos, así como roedores y
			otras espiecies.
		</p>
		<span style="font-size:14pt;text-align:center;color:green;font-weight:bold">Contamos con todos los permisos para brindarte un servicio profesional así
			como tu Certificado de Fumigación</span>
	</div>
	<div style="padding-left:100px;padding-top:0px;background-color:lightgray">
		<p style="font-size:16pt;text-align:center;color:red;font-weight:bold;padding-left:-90px">Algunas plagas que combatimos con esta
		fumigación</p>
		<p style="font-size:12pt;word-spacing:30pt">Cucarachas Arañas  Hormigas   <span style="word-spacing:-15px">  Grillos </span><span style="word-spacing:40pt"> Moscas Mosquitos</span></p>
		<img src="img/cucaracha.png" width="12%">
		<img src="img/arana.png" width="12%" style="margin-left:20px">
		<img src="img/Hormiga.png" width="12%" style="margin-left:20px">
		<img src="img/grillo.png" width="12%" style="margin-left:20px">
		<img src="img/mosca.png" width="12%" style="margin-left:20px">
		<img src="img/mosquito.png" width="12%" style="margin-left:20px">
	</div>
	<br>
	<div style="">
		<div style="float:right;padding-right:20px">
			<img src="img/body_residencial_standar.png" alt="body" width="340px" height="475px">
		</div>
		<div style="text-align:center;padding-left:50px;padding-right:20px;">
			<span style="font-size:16pt;font-weight:bold;">Preguntas Frecuentes</span>
			<p style="text-align:initial;font-size:11.8pt;line-height:1em;"><span style="font-weight:bold">1. ¿Cuánto tiempo tengo que dejar mi casa?<br></span>
			<span>Sólo tienes que estar UNA hora fuera de casa.</span>
			</p>
			<p style="text-align:initial;font-size:11.8pt;line-height:1em;"><span style="font-weight:bold">2. ¿Qué cuidados debo tener con mi mascota? </span>
			<span>Retira sus croquetas y agua, y espera a que el producto seque antes de entrar con ella.</span>
			</p>
			<p style="text-align:initial;font-size:11.8pt;line-height:1em;"><span style="font-weight:bold">3. ¿Cuánto dura el efecto de la fumigación?<br></span>
			<span>Nuestros insecticidas son biodegradables, por lo que el efecto es variable, dependiendo de la frecuencia
				de la limpieza, qué agentes limpiadores uses, así como de las condiciones clímaticas, por lo que el tiempo
				varía entre 30 y 120 días.</span>
			</p>
			<p style="text-align:initial;font-size:11.8pt;line-height:1em;"><span style="font-weight:bold">4. ¿Cada cuando debo fumigar?<br></span>
			<span>Recomendamos hacer una fumigación cada cambio de estación (cada 4 meses).</span>
			</p><br>
			<span style="color:green;font-style:italic;font-size:12pt">Protegemos a los que más quieres... SIEMPRE</span>
		</div>
	</div>
	<div class="footer">
	</div>

	<div>
		<div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
			<img src="img/Biofin_logo.png" alt="logo">
		</div>
		<div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
			<p><span style="color:green;font-weight:bold;font-size:12pt">COTIZACIÓN: </span>  
				<span style="color:red;font-weight:bold;font-size:12pt">{{$quotation}}</span>
			<br><span style="color:green;font-weight:bold;font-size:12pt">FECHA: </span>
				<?php echo date("d/m/y",strtotime($date))?> | <?php echo date("H:i",strtotime($date))?>
			<br><span style="color:green;font-weight:bold;font-size:12pt">AGENTE: </span>
			@if($user == null)	
			<span></span>
		@else 
			<span>{{$user->name}}</span>
		@endif			<br><br><span style="color:green;font-size:11pt">Licencia No. 18AP010010016
			<br>WhatsApp: 4491507531 | www.biofin.mx
			<br>Facebook: BIOFIN Fumigaciones Orgánicas
			<br>Email: contacto@biofin.mx</span></p>
		</div>	
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>
	<div style="margin: auto;padding-left:40px">
		<p><span style="font-size:14pt"> Atención a {{$name}}</span>
		<br><span style="font-size:11pt;font-weight:bold">{{$establishmentName}}</span>
		<br><span style="font-size:9pt">{{$address}}, {{$municipality}}</span>
		<br><span style="font-size:9pt"> Tel. {{$cellphone}}</span><br>
		@if($email != null)
			<span style="font-size:9pt"> {{$email}}</span></p>
		@endif
	</div>
	<br><br><br><br><br><br><br>
	<div style="magin:auto;padding-left:40px;padding-right:40px">
		<p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
		el Manejo Integral y Control de Plagas</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
		<p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE SERVICIO: </span>{{$est->name}}</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
	<p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE PLAGAS: </span>@foreach ( $nam as $item)
		{{$item->name}},	
	@endforeach</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
		<p><span style="font-size:12pt;font-weight:bold;color:green">AREA A FUMIGAR: </span>{{$area}} mt2</p>
	</div>
	<div style="margin:auto;margin-left:40px;margin-right:40px;height:250px;text-overflow:min-content">
		<p style="text-align:justify;line-height: 0.9em;font-size:11pt"><span style="font-size:12pt;font-weight:bold;color:green">DESCRIPCIÓN DEL SERVICIO</span>
	<br>@if($speech != null)
		{{$x}}
		@endif</p>
		@if($extra_value->id != 3)
		<p style="text-align:right;line-height: 1em;font-size:11pt">* El monto extra presentado se refiere a: "<span style="font-weight:bold;color:green">{{$extra_value->description}}</span>"</p>
		@endif
	</div>
	<div style="margin:auto;padding-left:40px;padding-right:40px">
		<div style="float:right;">
			<span style="color:green;font-size:12pt;font-weight:bold;">PRECIO: </span><span>${{$price}}.00</span> <br>
			<span style="color:green;font-size:12pt;font-weight:bold">DESCUENTO: </span><span>${{$d}}.00</span><br>
			<span style="color:green;font-size:12pt;font-weight:bold">EXTRA: </span><span>${{$e}}.00</span><br>
			<span style="color:green;font-size:14pt;font-weight:bold">SUBTOTAL: </span><span style="font-size:14pt;font-weight:bold">${{$subtotal}}.00</span> <br>
			<span style="font-size:8pt;text-align:right">*PRECIO MÁS IVA</span>
		</div>
		<br><br>
		<img src="img/Biofin_sello.png" alt="" width="85px" height="85px" style="float:right;margin-right:300px;margin-top:7px">	
	</div>
	<br>
	<div style="margin:auto;padding-left:40px;padding-right:40px">
		<p style="float:left;font-size:8pt">*Cotización válida por 30 días naturales.<br>
		*Cotización única e intransferible.<br>*Sujeta a restricciones. 
		No aplica ninguna otra<br>promoción o descuento.</p>
	</div>
</body>
</html>