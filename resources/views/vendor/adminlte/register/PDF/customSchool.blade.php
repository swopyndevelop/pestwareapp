<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Cotización</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
		font-family: 'Montserrat'
	}
</style>
<body>
	<div>
		<img src="img/banner_school.png" alt="banner" width="100%">
	</div>
	<div style="padding-left:15px;padding-right:15px;">
        <p style="font-size:14pt;text-align:justify;line-height: 1em;">Con un importante tráfico de alimentos, agua y almacenaje, las escuelas son
			un lugar ideal para plagas de insectos. <br>
			Es nuestro objetivo tratar estas plagas en ambientes educativos, desde
			guarderías hasta universidades.
        </p>
        <img src="img/senal2.png" alt="" style="float:left">
        <span style="font-size:14pt;color:green;font-style:italic;float:left">Utilizamos productos que no generan <br>
            riesgo de toxicidad en los niños.</span>
	</div>
	<br>
	<br>
    <br>
	<div style="padding-left:100px;padding-top:0px;background-color:lightgray;line-height: 0.9em;">
		<p style="margin-top:20px;font-size:16pt;text-align:center;color:red;font-weight:bold;padding-left:-90px">Insectos que puedes tener</p>
		<p style="font-size:11.5pt;word-spacing:27pt">Cucarachas Arañas  Hormigas Roedores<span style="word-spacing:27pt"> Moscas Mosquitos</span></p>
		<img src="img/cucaracha.png" width="12%">
		<img src="img/arana.png" width="12%" style="margin-left:20px">
		<img src="img/Hormiga.png" width="12%" style="margin-left:20px">
		<img src="img/roedor.png" width="12%" style="margin-left:20px">
		<img src="img/mosca.png" width="12%" style="margin-left:20px">
    <img src="img/mosquito.png" width="12%" style="margin-left:20px">
        <div style="font-size:10.5pt;margin-left:-60px;margin-right:20px">
        <span style="color:red">Control de Arañas.</span><span> Las arañas abundan en nuestro estado, pueden
            ser venenosas y  presentan un gran<br> riesgo  para tus alumnos, se debe mantener un constante
            monitoreo de zonas riesgosas.</span><br>
        <span style="color:red">Control de Cucarachas.</span><span> En centros de almacenaje es común encontrar cucarachas en cocinas, 
            come-<br>dores y áreas de sanitarios.</span><br>
        <span style="color:red">Mantenimiento de Sanitarios.</span><span> Sin importar que tan limpias se encuentren tus instalaciones, un baño <br>
			siempre es un foco de gérmenes al que hay que tener en el radar.</span>
        </div>
	</div>
	<p style="font-size:13pt;font-weight:bold;color:red;text-align:center">Zonas de Alerta</p>
	<img src="img/school1.png" alt="body" width="260px" height="240px" style="margin-left:150px;margin-top:30px">
	<img src="img/school2.png" alt="body" width="260px" height="240px" style="margin-right:50px;margin-top:30px">
    <div style="line-height: 1em;margin-top:-300px">
        <div style="padding-left:5px;font-size:11.5pt;text-align:left;width:350px;">
             <ul style="list-style:none">  
                <li>Juegos infantiles</li>
                <li>Jardines</li>
                <li>Areneros</li>
                <li>Baños</li>
                <li>Almacenes</li>
            </ul>
		</div>
        <div style="text-align:right;padding-right:40px;margin-top:-30px">
			<p style="font-size:11.5pt;"><span>Casilleros<br></span>
                <span>Cafetería</span><br><span>Comedores</span><br><span>Salones de Clase</span><br>
                <span>Espacios oscuros y secos</span><br><span>Espacio de cooperativa</span>
			</p>
			
		</div>
		<br><br>
		<div style="font-size:11.5pt;text-align:justify;padding-left:40px">
			<span style="font-size:11.5pt;text-align:justify;">Diseñemos un plan a la medida de tus necesidades para asegurar a todos los asistentes a tu
				<br> institución. El éxito está en el constante monitoreo y prevención de plagas.</span>
		</div>
        <div style="text-align:center;line-height: 1em;margin-left:40px;margin-right:40px;margin-top:-3px">
            <span style="color:green;font-style:italic;font-size:12pt;text-align:center">Protegemos a los que más quieres... SIEMPRE</span>
        </div>
            
		
	</div>
	<div class="footer">
	</div>

	<div>
		<div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
			<img src="img/Biofin_logo.png" alt="logo">
		</div>
		<div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
			<p><span style="color:green;font-weight:bold;font-size:12pt">COTIZACIÓN: </span>  
				<span style="color:red;font-weight:bold;font-size:12pt">{{$quotation->id_quotation}}</span>
			<br><span style="color:green;font-weight:bold;font-size:12pt">FECHA: </span>
				<?php echo date("d/m/y",strtotime($quotation->date))?> | <?php echo date("H:i",strtotime($quotation->date))?>
			<br><span style="color:green;font-weight:bold;font-size:12pt">AGENTE: </span>
			@if($user == null)	
			<span></span>
		@else 
			<span>{{$user->name}}</span>
		@endif			<br><br><span style="color:green;font-size:12pt">Licencia No. 18AP010010016
			<br>WhatsApp: 4491507531 | www.biofin.mx
			<br>Facebook: BIOFIN Fumigaciones Orgánicas
			<br>Email: contacto@biofin.mx</span></p>
		</div>	
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>
	<div style="margin: auto;padding-left:40px">
		<p><span style="font-size:14pt"> Atención a {{$quotation->name}}</span>
		<br><span style="font-size:11pt;font-weight:bold">{{$quotation->establishment_name}}</span>
		<br><span style="font-size:9pt">{{$quotation->address}}, {{$quotation->municipality}}</span>
		<br><span style="font-size:9pt"> Tel. {{$quotation->cellphone}}</span><br>
		@if($quotation->email != null)
			<span style="font-size:9pt"> {{$quotation->email}}</span></p>
		@endif
	</div>
	<br><br><br><br><br><br><br>
	<div style="magin:auto;padding-left:40px;padding-right:40px">
		<p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
		el Manejo Integral y Control de Plagas</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
		<p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE SERVICIO: </span>{{$quotation->e_name}}</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
	<p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE PLAGAS: </span>@foreach ( $plagues as $item)
		{{$item->name}},	
	@endforeach</p>
	</div>
	<div style="margin:auto;margin-left:40px;margin-right:40px;height:130px;text-overflow:min-content;">
            <p style="text-align:justify;line-height: 1em;font-size:10.5pt"><span style="font-size:12pt;font-weight:bold;color:green">DESCRIPCIÓN DEL SERVICIO</span>
            <br><br>
            {{$quotation->description}}
            </p>
        </div>
        <div style="margin:auto;margin-left:40px;margin-right:40px;text-overflow:min-content;">
            <table>
                <thead style="border-top: 2px solid;border-bottom: 2px solid">
                    <tr>
                        <th style="font-size:10pt;color:green;font-weight:bold">CONCEPTO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;CANTIDAD&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;FRECUENCIA<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">MENSUAL</span>&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PLAZO<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">CONTRATADO</span><br>&nbsp;&nbsp;&nbsp;<span style="font-size:8pt">EN MESES</span>&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PRECIO<br>&nbsp;&nbsp;&nbsp;UNITARIO&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;SUBTOTAL</th>
                    </tr> 
                </thead>
                <tbody>
                @foreach ($c as $item)
                <tr>
                    <td style="font-size:10pt">{{$item->concept}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->quantity}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->frecuency_month}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->term_month}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->unit_price}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->subtotal}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="margin-left:20px;margin-right:20px;line-height:1em;margin-top:-3px">
            <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">PRECIO:<span style="color:black;font-weight:normal"> ${{$quotation->price}}.00</span></p>
            <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">SUBTOTAL:<span style="color:black;"> ${{$quotation->total}}.00</span></p>
            <span style="font-size:8pt">*Cotización válida por 30 días naturales. Cotización única e intransferible. Sujeta a restricciones. No aplica ninguna otra promoción o descuento.</span>
        </div>