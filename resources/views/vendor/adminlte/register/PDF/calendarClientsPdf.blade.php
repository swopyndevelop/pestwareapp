<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Calendario de Visitas</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
	}
    table {
        width: 100%;
        /*border-bottom: 1px solid black;*/
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }
    caption, td, th {
        padding: 0.3em;
    }
    th, td {
       /* border-bottom: 1px solid black;*/
        width: 25%;
    }
    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
        {{--//headers--}}
        <div style="margin: 0px;">
            <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">
                <div style="text-align: center; margin-bottom: 0px; margin-top: 20px;">
                    <h2 style="font-weight: bold; margin-bottom: 0px">CALENDARIO DE VISITAS</h2>
                </div>

                <div style="width: 130px; display: inline-block; vertical-align: top;">
                    @if($company->pdf_logo == null)
                        <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
                    @else
                        <img src="{{ env('URL_STORAGE_FTP').$company->pdf_logo }}" alt="logo" width="130px">
                    @endif
                </div>

                <div style="width: 290px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
                    <p style="margin-top: 0px; margin-bottom: 0">
                        <span style="color:black;font-size:9.5pt">Responsable Sanitario: {{$jobCenterProfile->health_manager}} | Licencia No: {{$jobCenterProfile->license}}
                            | Whatsapp: {{$jobCenterProfile->whatsapp_personal}} | Teléfono: {{$jobCenterProfile->cellphone}} | Razón Social: {{ $jobCenterProfile->business_name }} |
                            {{ $jobCenterProfile->rfc_country }}: {{ $jobCenterProfile->rfc }}| @if($addressProfile) {{ $addressProfile->street }} #{{ $addressProfile->num_ext }}, {{ $addressProfile->location }},
                            {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. @endif | Email: {{ $jobCenterProfile->email_personal }} | Facebook: {{$jobCenterProfile->facebook_personal}}.
                        </span>
                    </p>
                </div>
                <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
                    @if($qrcode != null)
                        <img src="data:image/png;base64, {!! $qrcode !!}" width="100px"><br>
                        <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Licencia Sanitaria</span>
                    @endif
                </div>
                <div style="width: 212px; display: inline-block; margin-top: 0px; font-size: 8pt; margin-left: 10px; vertical-align: top;">
                    <p style="margin-bottom: 2px; margin-top: 0px">
                        <span style="font-size:8pt; font-weight: bold">NO. CLIENTE: </span>
                        <span style="color:red;font-weight:bold;font-size:8pt;">CL-{{$customer->id}}</span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 0px">
                        <span style="font-size:8pt; font-weight: bold">NOMBRE/EMPRESA: </span>
                        <span style="color:red;font-weight:bold;font-size:8pt;">{{$customer->name}} {{$customer->establishment_name}}</span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">PERIODO INICIAL: </span>
                        <span style="font-size:8pt;">{{ $period[0] }}</span>
                    </p>
                    <p style="margin-bottom: 2px; margin-top: 2px">
                        <span style="font-size:8pt; font-weight: bold">PERIODO FINAL: </span>
                        <span style="font-size:8pt;">{{ $period[1] }}</span>
                    </p>
                </div>

            </div>
        </div>
        <br>
        {{--//Body--}}
        @foreach($months as $key => $events)
        <div style="text-align: center; ; margin: 0 20px 0 20px;">
            <h5 style="margin: 0 0 0 0">
                @if($key == "January")Enero
                @elseif($key == "February")Febrero
                @elseif($key == "March")Marzo
                @elseif($key == "April")Abril
                @elseif($key == "May")Mayo
                @elseif($key == "June")Junio
                @elseif($key == "July")Julio
                @elseif($key == "August")Agosto
                @elseif($key == "September")Septiembre
                @elseif($key == "October")Octubre
                @elseif($key == "November")Noviembre
                @elseif($key == "December")Diciembre
                @endif
            </h5>
        </div>
        <table style="width: 100%; text-align: center; margin: 15px 20px 15px 20px; border: 1px solid">
            <thead>
            <tr style="font-size: 9.5pt">
                <th>Servicio</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Técnico</th>
                <th># OS</th>
            </tr>
            </thead>
            <tbody style="font-size: 8pt">
                @foreach($events as $event)
                <tr>
                    <td>{{ $event->title }}</td>
                    <td>{{ $event->initial_date }}</td>
                    <td>{{ $event->initial_hour }}</td>
                    <td>{{ $event->name }}</td>
                    <td>{{ $event->id_service_order }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endforeach

        {{--//Footer--}}
        <footer style="position: absolute; bottom: 15px;text-align: center; margin: 0 20px 0 20px;">

            <div style="width: 240px; display: inline-block; vertical-align: top; text-align: center;">
                @if($firms)
                    <img src="{{$firmUrl}}" width="80px" height="80px" style="margin-top: 0px"><br>
                    <p style="text-align: center;font-size:6pt;font-weight:bold; margin: 0 0 0 0">{{$customer->name}} {{$customer->establishment_name}}</p>
                @endif
            </div>

            <div style="width: 240px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top; text-align: center;">
                @if($firms)
                    <img src="{{$firmUrlTechnician}}" width="80px" height="80px" style="margin-top: 0px"><br>
                    <p style="text-align:center;font-size:6pt; font-weight: bold; margin: 0 0 0 0">{{ $firms->name }}</p>
                @endif
            </div>

            <div style="width: 240px; display: inline-block; font-size: 8pt; margin-top: 0px; vertical-align: top; text-align: center;">
                <img src="{{ env('URL_STORAGE_FTP').$company->pdf_sello }}" width="80px" height="80px"><br>
                <p style="text-align:center;font-size:6pt; font-weight: bold; margin: 0 0 0 0">Sello</p>
            </div>

        </footer>
</body>
</html>