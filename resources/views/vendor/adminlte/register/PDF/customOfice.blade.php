<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Cotización</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
		font-family: 'Montserrat'
	}
</style>
<body>
	<div>
		<img src="img/banner_office.png" alt="banner" width="100%">
	</div>
	<div style="padding-left:15px;padding-right:15px;">
    <p style="font-size:14pt;text-align:justify;line-height: 0.9em;">Con un importante tráfico de personas, alimentos, agua y almacenaje, las oficinas y bodegas
			son un lugar ideal para plagas.</p>
    <p style="font-size:14pt;text-align:justify;line-height: 0.9em;"> Es nuestro objetivo tratar estas plagas en ambientes corporativos para mantener y fomentar
				un ambiente limpio y seguro.</p>
	</div>
	<div style="padding-left:100px;padding-top:-5px;background-color:lightgray;line-height: 1em;">
		<p style="margin-top:20px;font-size:16pt;text-align:center;color:red;font-weight:bold;padding-left:-90px">Insectos que puedes tener</p>
		<p style="font-size:11.5pt;word-spacing:27pt">Cucarachas Arañas  Hormigas Roedores<span style="word-spacing:27pt"> Moscas Mosquitos</span></p>
		<img src="img/cucaracha.png" width="12%">
		<img src="img/arana.png" width="12%" style="margin-left:20px">
		<img src="img/Hormiga.png" width="12%" style="margin-left:20px">
		<img src="img/roedor.png" width="12%" style="margin-left:20px">
		<img src="img/mosca.png" width="12%" style="margin-left:20px">
    <img src="img/mosquito.png" width="12%" style="margin-left:20px">
        <div style="font-size:10.5pt;margin-left:-60px;margin-right:20px">
        <span style="color:red">Control de Arañas.</span><span> Las arañas abundan en nuestro estado, pueden
            ser venenosas y  presentan un gran<br> riesgo  para tus trabajadores, se debe mantener un constante
            monitoreo de zonas riesgosas.</span><br>
        <span style="color:red">Control de Cucarachas.</span><span> En oficinas corporativas es común encontrar cucarachas en cocinas, 
            come-<br>dores y áreas de sanitarios.</span><br>
        <span style="color:red">Control de Roedores.</span><span> Los roedores son una plaga que convive naturalmente con el ser humano y por <br>
            las características de almacenaje  de archivo e insumo de oficina es común encontarlos. Evita daños en tus instalaciones.</span>
        </div>
	</div>
	<br>
	<div style="">
		<div style="float:right;padding-right:40px">
			<img src="img/office.png" alt="body" width="350px" height="340px">
		</div>
		<div style="text-align:right;padding-left:60px;padding-right:40px">
			<span style="font-size:16pt;font-weight:bold;color:red;padding-right:40px">Zonas de Alerta</span>
			<p style="font-size:12pt;line-height: 1em;"><span>Área de Almacén<br></span>
                <span>Sanitarios</span><br><span>Comedores</span><br><span>Cocina</span><br>
                <span>Escritorios</span><br><span>Escaleras</span><br><span>Estacionamiento</span>
            </p>
            <br>
            <div class="notificacion" style="font-size:11pt">
                <img src="img/senal1.png" alt="" style="float:left">
                <span style="color:green; font-style:italic">Utilizaremos productos que no generan riesgo de toxicidad</span>
            </div>
            <br><br><br>
            <div style="text-align:center;line-height: 1em;">
            <span style="font-size:12pt;line-height: 1em;">Diseñaremos un plan a la medida de tus necesidades.</span><br>
            <span style="font-size:12pt;line-height: 1em;">El éxito está en el constante monitoreo y prevención de plagas.</span><br>
            <span style="color:green;font-style:italic;font-size:12pt;">Protegemos a los que más quieres... SIEMPRE</span>
            </div>
		</div>
	</div>
	<div class="footer">
	</div>

	<div>
		<div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
			<img src="img/Biofin_logo.png" alt="logo">
		</div>
		<div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
			<p><span style="color:green;font-weight:bold;font-size:12pt">COTIZACIÓN: </span>  
				<span style="color:red;font-weight:bold;font-size:12pt">{{$quotation->id_quotation}}</span>
			<br><span style="color:green;font-weight:bold;font-size:12pt">FECHA: </span>
				<?php echo date("d/m/y",strtotime($quotation->date))?> | <?php echo date("H:i",strtotime($quotation->date))?>
			<br><span style="color:green;font-weight:bold;font-size:12pt">AGENTE: </span>
			@if($user == null)	
				<span></span>
			@else 
				<span>{{$user->name}}</span>
			@endif
			<br><br><span style="color:green;font-size:11pt">Licencia No. 18AP010010016
			<br>WhatsApp: 4491507531 | www.biofin.mx
			<br>Facebook: BIOFIN Fumigaciones Orgánicas
			<br>Email: contacto@biofin.mx</span></p>
		</div>	
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>
	<div style="margin: auto;padding-left:40px">
		<p><span style="font-size:14pt"> Atención a {{$quotation->name}}</span>
		<br><span style="font-size:11pt;font-weight:bold">{{$quotation->establishment_name}}</span>
		<br><span style="font-size:9pt">{{$quotation->address}}, {{$quotation->municipality}}</span>
		<br><span style="font-size:9pt"> Tel. {{$quotation->cellphone}}</span><br>
		@if($quotation->email != null)
			<span style="font-size:9pt"> {{$quotation->email}}</span></p>
		@endif
	</div>
	<br><br><br><br><br><br><br>
	<div style="magin:auto;padding-left:40px;padding-right:40px">
		<p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
		el Manejo Integral y Control de Plagas</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
		<p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE SERVICIO: </span>{{$quotation->e_name}}</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
	<p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE PLAGAS: </span>@foreach ( $plagues as $item)
		{{$item->name}},	
	@endforeach</p>
	</div>
    <div style="margin:auto;margin-left:40px;margin-right:40px;height:130px;text-overflow:min-content;">
            <p style="text-align:justify;line-height: 1em;font-size:10.5pt"><span style="font-size:12pt;font-weight:bold;color:green">DESCRIPCIÓN DEL SERVICIO</span>
            <br><br>
            {{$quotation->description}}
            </p>
        </div>
        <div style="margin:auto;margin-left:40px;margin-right:40px;text-overflow:min-content;">
            <table>
                <thead style="border-top: 2px solid;border-bottom: 2px solid">
                    <tr>
                        <th style="font-size:10pt;color:green;font-weight:bold">CONCEPTO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;CANTIDAD&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;FRECUENCIA<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">MENSUAL</span>&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PLAZO<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">CONTRATADO</span><br>&nbsp;&nbsp;&nbsp;<span style="font-size:8pt">EN MESES</span>&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PRECIO<br>&nbsp;&nbsp;&nbsp;UNITARIO&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;SUBTOTAL</th>
                    </tr> 
                </thead>
                <tbody>
                @foreach ($c as $item)
                <tr>
                    <td style="font-size:10pt">{{$item->concept}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->quantity}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->frecuency_month}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->term_month}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->unit_price}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->subtotal}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="margin-left:20px;margin-right:20px;line-height:1em;margin-top:-3px">
            <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">PRECIO:<span style="color:black;font-weight:normal"> ${{$quotation->price}}.00</span></p>
            <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">SUBTOTAL:<span style="color:black;"> ${{$quotation->total}}.00</span></p>
            <span style="font-size:8pt">*Cotización válida por 30 días naturales. Cotización única e intransferible. Sujeta a restricciones. No aplica ninguna otra promoción o descuento.</span>
        </div>	
</body>
</html>