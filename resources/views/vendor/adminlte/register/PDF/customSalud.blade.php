<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Cotización</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
		font-family: 'Montserrat'
	}
</style>
<body>
	<div>
		<img src="img/banner_salud.png" alt="banner" width="100%">
	</div>
	<div style="padding-left:15px;padding-right:15px;text-align:center">
        <p style="font-size:12pt;text-align:justify;line-height: 0.9em;">Con un importante tráfico de personas las instituciones dedicadas al cuidado de la salud deben
			de contar con altos estándares de higiene para evitar la dispersión de enfermedades a través
			de vectores cómo las plagas.</p>
		<p style="font-size:12pt;text-align:justify;line-height: 0.9em;">Es nuestro objetivo trabajar de la mano contigo para cuidar la salud de tus clientes. Por la normativa
			de la Ley Federal de Salud Pública es obligación que los establecimientos dedicados a
			esta actividad, mantengan un ambiente libre de plagas que puedan transmitir enfermedades a
			la población.
		</p>
		<span style="font-size:12pt;text-align:center;color:green;font-weight:bold;line-height: 0.9em;">Contamos con todos los permisos para brindarte un servicio profesional así <br>
			como tu Certificado de Fumigación</span>
	</div>
	<div style="padding-left:100px;padding-top:0px;background-color:lightgray;line-height: 0.9em;">
		<p style="font-size:16pt;text-align:center;color:red;font-weight:bold;padding-left:-90px">Plagas que puedes tener</p>
		<p style="font-size:11.5pt;word-spacing:30pt">Cucarachas   Piojos  Chinches Roedores<span style="word-spacing:30pt"> Moscas Gérmenes</span></p>
		<img src="img/cucaracha.png" width="12%">
		<img src="img/piogo.png" width="12%" style="margin-left:20px">
		<img src="img/chinche.png" width="12%" style="margin-left:20px">
		<img src="img/roedor.png" width="12%" style="margin-left:20px">
		<img src="img/mosca.png" width="12%" style="margin-left:20px">
        <img src="img/germen.png" width="12%" style="margin-left:20px">
        <div style="padding-left:-60px;font-size:10.5pt">
        <span style="color:red">Control de Cucarachas.</span><span> El constante ingreso de insumos y cajas de cartón acarrean a la cucarcha ale- <br>mana,
			quien es el principal problema en estos lugares. La prevención es el mejor control de esta plaga.
			<br>Evita una mala experiencia de tus clientes.</span><br>
        <span style="color:red">Control de Chinches.</span><span> El alto tráfico de personas en habitaciones, salas de espera, etc. Propician el <br>
			riesgo de infestación de chinches. Evita formar parte del ciclo de transmisión de esta plaga.</span><br>
        <span style="color:red">Desinfección y Sanitización.</span><span> Los gérmenes y bacterias forman son un riesgo latente para la salud <br>de los
			pacientes. Es indispensable la constante desinfección de los espacios y ambientes para evitar la propagación
			de posibles enfermedades.</span>
        </div>
	</div>
	<br>
	<div style="">
        <div style="text-align:center;font-size:10pt;float:left;padding-left:40px;width:250px;line-height: 0.9em;">
            <span style="color:green;font-style:italic">Utilizamos productos <br>que no generan <br> riesgo de toxicidad</span>
			<br><br><br>
            <img src="img/senal2.png" alt="">
            <span style="font-size:12pt">Diseñaremos un plan a la <br> medida de tus necesidades.</span><br>
            <span style="font-size:12pt">El éxito está en el constante monitoreo y prevención de plagas.</span>
		</div>
		<div style="text-align:right;padding-left:60px;padding-right:20px;width:400px;line-height: 0.9em;">
			<span style="font-size:15pt;font-weight:bold;color:red">Zonas de Alerta</span>
			<p style="font-size:12pt"><span>Consultorios<br></span>
                <span>Áreas de recuperación</span><br><span>Quirófanos</span><br><span>Pasillos</span><br>
                <span>Laboratorios</span><br><span>Farmacia</span><br><span>Salas de espera</span><br><span>Baños</span>
                <br><span>Bodegas</span>
            </p>
            <br>
        </div>
        <div style="float:right;margin-top:-350px;padding-right:15px">
            <img src="img/boy_salud.png" alt="" height="280px">
        </div>
        <div style="text-align:initial;margin-top:300px; padding-left:60px">
            <span style="color:green;font-style:italic;font-size:12pt;padding-right:120px">Protegemos a los que más quieres... SIEMPRE</span>
        </div>
	</div>
	<div class="footer">
	</div>
	<div>
		<div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
			<img src="img/Biofin_logo.png" alt="logo">
		</div>
		<div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
			<p><span style="color:green;font-weight:bold;font-size:12pt">COTIZACIÓN: </span>  
				<span style="color:red;font-weight:bold;font-size:12pt">{{$quotation->id_quotation}}</span>
			<br><span style="color:green;font-weight:bold;font-size:12pt">FECHA: </span>
				<?php echo date("d/m/y",strtotime($quotation->date))?> | <?php echo date("H:i",strtotime($quotation->date))?>
			<br><span style="color:green;font-weight:bold;font-size:12pt">AGENTE: </span>
			@if($user == null)	
			<span></span>
		@else 
			<span>{{$user->name}}</span>
		@endif			<br><br><span style="color:green;font-size:11pt">Licencia No. 18AP010010016
			<br>WhatsApp: 4491507531 | www.biofin.mx
			<br>Facebook: BIOFIN Fumigaciones Orgánicas
			<br>Email: contacto@biofin.mx</span></p>
		</div>	
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>
	<div style="margin: auto;padding-left:40px">
		<p><span style="font-size:14pt"> Atención a {{$quotation->name}}</span>
		<br><span style="font-size:11pt;font-weight:bold">{{$quotation->establishment_name}}</span>
		<br><span style="font-size:9pt">{{$quotation->address}}, {{$quotation->municipality}}</span>
		<br><span style="font-size:9pt"> Tel. {{$quotation->cellphone}}</span><br>
		@if($quotation->email != null)
			<span style="font-size:9pt"> {{$quotation->email}}</span></p>
		@endif
	</div>
	<br><br><br><br><br><br><br>
	<div style="magin:auto;padding-left:40px;padding-right:40px">
		<p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
		el Manejo Integral y Control de Plagas</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
		<p><span style="font-size:12pt;font-weight:bold;color:green;">TIPO DE SERVICIO: </span>{{$quotation->e_name}}</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
	<p><span style="font-size:11pt;font-weight:bold;color:green">TIPO DE PLAGAS: </span>@foreach ( $plagues as $item)
		{{$item->name}},	
	@endforeach</p>
	</div>
    <div style="margin:auto;margin-left:40px;margin-right:40px;height:130px;text-overflow:min-content;">
            <p style="text-align:justify;line-height: 1em;font-size:10.5pt"><span style="font-size:12pt;font-weight:bold;color:green">DESCRIPCIÓN DEL SERVICIO</span>
            <br><br>
            {{$quotation->description}}
            </p>
        </div>
        <div style="margin:auto;margin-left:40px;margin-right:40px;text-overflow:min-content;">
            <table>
                <thead style="border-top: 2px solid;border-bottom: 2px solid">
                    <tr>
                        <th style="font-size:10pt;color:green;font-weight:bold">CONCEPTO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;CANTIDAD&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;FRECUENCIA<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">MENSUAL</span>&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PLAZO<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">CONTRATADO</span><br>&nbsp;&nbsp;&nbsp;<span style="font-size:8pt">EN MESES</span>&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PRECIO<br>&nbsp;&nbsp;&nbsp;UNITARIO&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;SUBTOTAL</th>
                    </tr> 
                </thead>
                <tbody>
                @foreach ($c as $item)
                <tr>
                    <td style="font-size:10pt">{{$item->concept}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->quantity}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->frecuency_month}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->term_month}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->unit_price}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->subtotal}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="margin-left:20px;margin-right:20px;line-height:1em;margin-top:-3px">
            <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">PRECIO:<span style="color:black;font-weight:normal"> ${{$quotation->price}}.00</span></p>
            <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">SUBTOTAL:<span style="color:black;"> ${{$quotation->total}}.00</span></p>
            <span style="font-size:8pt">*Cotización válida por 30 días naturales. Cotización única e intransferible. Sujeta a restricciones. No aplica ninguna otra promoción o descuento.</span>
        </div>
</body>
</html>