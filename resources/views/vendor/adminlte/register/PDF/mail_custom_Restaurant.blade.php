<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Cotización</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
		font-family: 'Montserrat'
	}
</style>
<body>
	<div>
		<img src="img/banner_r.png" alt="banner" width="100%">
	</div>
	<div style="padding-left:15px;padding-right:15px;text-align:center">
        <p style="font-size:12pt;text-align:justify;line-height:1em">Con un importante tráfico de alimentos, agua y almacenaje, los restaurantes y bares son el
			lugar ideal para plagas de insectos.</p>
		<p style="font-size:12pt;text-align:justify;line-height:1em">
            Es nuestro objetivo tratar estas plagas en ambientes de consumo de alimentos y bebidas. Por
			la normativa de la Ley Federal de Salud Pública es obligación de establecimientos de consumo
			de alimentos y bebidas mantener un ambiente libre de plagas que puedan transmitir enfermedades
			a la población.
		</p>
		<span style="font-size:14pt;text-align:center;color:green;font-weight:bold">Contamos con todos los permisos para brindarte un servicio profesional así <br>
			como tu Certificado de Fumigación</span>
	</div>
	<div style="padding-left:100px;padding-top:0px;background-color:lightgray;line-height: 1em;">
		<p style="margin-top:20px;font-size:16pt;text-align:center;color:red;font-weight:bold;padding-left:-90px">Insectos que puedes tener</p>
		<p style="font-size:11.5pt;word-spacing:27pt">Cucarachas Arañas  Hormigas Roedores<span style="word-spacing:27pt"> Moscas Mosquitos</span></p>
		<img src="img/cucaracha.png" width="12%">
		<img src="img/arana.png" width="12%" style="margin-left:20px">
		<img src="img/Hormiga.png" width="12%" style="margin-left:20px">
		<img src="img/roedor.png" width="12%" style="margin-left:20px">
		<img src="img/mosca.png" width="12%" style="margin-left:20px">
    <img src="img/mosquito.png" width="12%" style="margin-left:20px">
        <div style="font-size:10.5pt;margin-left:-60px;margin-right:20px">
        <span style="color:red">Control de Cucarachas.</span><span> El alto tráfico de alimentos, insumos y cajas de cartón acarrean a la cucaracha <br>
			alemana, quien es el principal problema en negocios donde hay alimentos o bebidas. La prevención es <br>
			el mejor control de esta plaga. Evita una mala experiencia de tus clientes.</span><br>
        <span style="color:red">Control de Mosca.</span><span> Las moscas además de dar una mala imagen a tu negocio, transmiten una <br>cantidad  
            importante de enfermedades a tus clientes. No permitas que esto se vuelva un problema.</span><br>
        <span style="color:red">Control de Roedores.</span><span> Los roedores son una plaga que convive naturalmente con el ser humano y por <br>
			las características de tu negocio es muy común encontrarlos. Evita daños en tus instalaciones.</span>
        </div>
	</div>
    <br>
	<div style="padding-right:10px;padding-left:10px;text-align:center">
		<div style="float:right;width:140px;padding-right:10px">
			<br><br><br>
			<img src="img/senal2.png" alt="" width="70%">  
			<p style="font-size:10pt;font-style:italic;color:green;width:150px;padding-right:10px" >Utilizamos productos que no generan riesgo de toxicidad.</p>
        </div>
        <div style="margin-left:300px;text-align:center">
			<img src="img/restaurant.png" alt="" width="60%" style="margin-left:auto;margin-right:auto;display:block">
		</div>  
		<div style="text-align:right; width:300px;padding-right:10px;margin-top:-300px">
			<span style="font-size:16pt;font-weight:bold;color:red">Zonas de Alerta</span>
			<p style="font-size:12pt;line-height:1em"><span>Juegos infantiles<br></span>
                <span>Baños</span><br><span>Almacén</span><br><span>Cocina</span><br>
                <span>Equipo de cocina y refrigeración</span><br><span>Espacios oscuros y secos</span><br><span>Estacionamiento</span>
			</p>
		</div>
		<br>
		<p style="font-size:11pt;line-height:0.9em;text-align:center">Diseñemos un plan a la medida de tus necesidades.
			<br> El éxito está en el constante monitoreo y prevención de plagas <br>
			<span style="color:green;font-style:italic;font-size:12pt;text-align:center">Protegemos a los que más quieres...SIEMPRE</span>
		</p>
	</div>
	<div>
		<div style="float:left;padding-left:40px;padding-right:40px;padding-top:40px">
			<img src="img/Biofin_logo.png" alt="logo" style="margin-top:20px">
		</div>
		<div style="float:right;text-align:justify;padding-left:40px;padding-right:40px;padding-top:40px">
			<p><span style="color:green;font-weight:bold;font-size:12pt">COTIZACIÓN: </span>  
				<span style="color:red;font-weight:bold;font-size:12pt">{{$quotation}}</span>
			<br><span style="color:green;font-weight:bold;font-size:12pt">FECHA: </span>
				<?php echo date("d/m/y",strtotime($quotation))?> | <?php echo date("H:i",strtotime($quotation))?>
			<br><span style="color:green;font-weight:bold;font-size:12pt">AGENTE: </span>
			@if($user == null)	
			<span></span>
		@else 
			<span>{{$user->name}}</span>
		@endif			<br><br><span style="color:green;font-size:11pt">Licencia No. 18AP010010016
			<br>WhatsApp: 4491507531 | www.biofin.mx
			<br>Facebook: BIOFIN Fumigaciones Orgánicas
			<br>Email: contacto@biofin.mx</span></p>
		</div>	
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>
	<div style="margin: auto;padding-left:40px">
		<p><span style="font-size:14pt"> Atención a {{$name}}</span>
		<br><span style="font-size:11pt;font-weight:bold">{{$establishmentName}}</span>
		<br><span style="font-size:9pt">{{$address}}, {{$municipality}}</span>
		<br><span style="font-size:9pt"> Tel. {{$cellphone}}</span><br>
		@if($email != null)
			<span style="font-size:9pt"> {{$email}}</span></p>
		@endif
	</div>
	<br><br><br><br><br><br><br>
	<div style="magin:auto;padding-left:40px;padding-right:40px">
		<p style="font-size:12pt">Atendiendo su solicitud, adjunto propuesta para obtener una solución integral en 
		el Manejo Integral y Control de Plagas</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
		<p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE SERVICIO: </span>{{$est->name}}</p>
	</div>
	<div style="margin:auto;border-bottom: 2px solid;margin-left:40px;margin-right:40px">
	<p><span style="font-size:12pt;font-weight:bold;color:green">TIPO DE PLAGAS: </span>@foreach ( $nam as $item)
		{{$item->name}},	
	@endforeach</p>
	</div>
    <div style="margin:auto;margin-left:40px;margin-right:40px;height:130px;text-overflow:min-content;">
            <p style="text-align:justify;line-height: 1em;font-size:10.5pt"><span style="font-size:12pt;font-weight:bold;color:green">DESCRIPCIÓN DEL SERVICIO</span>
            <br><br>
            {{$description}}
            </p>
        </div>
        <div style="margin:auto;margin-left:40px;margin-right:40px;text-overflow:min-content;">
            <table>
                <thead style="border-top: 2px solid;border-bottom: 2px solid">
                    <tr>
                        <th style="font-size:10pt;color:green;font-weight:bold">CONCEPTO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;CANTIDAD&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;FRECUENCIA<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">MENSUAL</span>&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PLAZO<br>&nbsp;&nbsp;&nbsp;<span style="font-size:9pt">CONTRATADO</span><br>&nbsp;&nbsp;&nbsp;<span style="font-size:8pt">EN MESES</span>&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;PRECIO<br>&nbsp;&nbsp;&nbsp;UNITARIO&nbsp;&nbsp;&nbsp;</th>
                        <th style="font-size:10pt;color:green;font-weight:bold">&nbsp;&nbsp;&nbsp;SUBTOTAL</th>
                    </tr> 
                </thead>
                <tbody>
                @foreach ($array as $item)
                <tr>
                    <td style="font-size:10pt">{{$item->concept}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->quantity}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->frecuency_month}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->term_month}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->unit_price}}</td>
                    <td style="text-align:center;font-size:10pt">{{$item->subtotal}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="margin-left:20px;margin-right:20px;line-height:1em;margin-top:-3px">
            <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">PRECIO:<span style="color:black;font-weight:normal"> ${{$price}}.00</span></p>
            <p style="text-align:right;color:green;font-weight:bold;font-size:11pt">SUBTOTAL:<span style="color:black;"> ${{$subtotal}}.00</span></p>
            <span style="font-size:8pt">*Cotización válida por 30 días naturales. Cotización única e intransferible. Sujeta a restricciones. No aplica ninguna otra promoción o descuento.</span>
        </div>
</body>
</html>