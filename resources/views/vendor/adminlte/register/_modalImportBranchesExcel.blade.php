<!-- INICIA MODAL PARA IMPORTAR SUCURSALES DESDE EXCEL -->
<div class="modal fade" id="importExcelBranches" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="box-body">
                <form action="{{ route('import_excel_branches', $customerId) }}" method="POST" id="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="modal-body">
                        <h5 class="modal-title">Seleccione la lista de sucursales en formato excel</h5>
                        <div class="col-md-8 col-md-offset-2">
                            <input type="file" class="form-control" id="fileImport" name="fileImport" required="required"
                                   accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        </div>
                    </div>
                    <br><br>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" id="importFileBranches" title="Importar" class="btn btn-primary btn-sm"><strong>Importar</strong></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA IMPORTAR SUCURSALES DESDE EXCEL -->