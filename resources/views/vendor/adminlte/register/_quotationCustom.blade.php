<!-- START MODAL DE COTIZACION -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalQuotationCustom" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Cotización Personalizada</h4>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-control" name="description" id="description" cols="30" rows="3" placeholder="Descripción del servicio"></textarea>
                                        <br>
                                    </div>

                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-responsive-md table-sm table-bordered" id="makeEditable">
                                             <thead>
                                              <tr>
                                                <th>Concepto</th>
                                                <th>Cantidad</th>
                                                <th>Frec. de aplicación / mes</th>
                                                <th>Plazo en contrato en meses</th>
                                                <th>Precio unitario</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        <span style="float:right"><button id="but_add" class="btn btn-danger">Agregar Fila</button></span>
                                    </div>

                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-md-push-8 col-sm-2 text-right">
                                            <label class="control-label" for="sumaSubtotal" style="font-weight: bold; font-size: large; margin-bottom: 10px;">Suma subtotal</label>
                                            <label class="control-label" for="discountC" style="font-weight: bold; font-size: large; margin-bottom: 10px;">Descuento</label><br>
                                            <label class="control-label" for="total" style="font-weight: bold; font-size: large;">Total</label>
                                        </div>
                                        <div class="col-sm-push-8 col-sm-2">
                                            <input type="text" id="sumaSubtotal" class="form-control" disabled="disabled" style="font-weight: bold;">
                                            <select name="discountC" id="discountC" class="form-control">
                                                <option value="0">Sin descuento / 0%</option>
                                                @foreach($discounts as $discount)
                                                    @if($discount->id != 3)
                                                        <option value="{{ $discount->percentage }}">{{ $discount->title }} / {{ $discount->percentage }}%</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <input type="text" id="total" class="form-control" style="font-weight: bold; font-size: large;" disabled="disabled">
                                            <br>
                                            <button class="btn btn-secondary" type="button" id="submit_data" name="calcular">Calcular</button>
                                        </div>
                                    </div>
                                    <div >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelEditProfileDiv">Cancelar</button>
                                        <button class="btn btn-primary" type="button" id="saveQuotationCustomNew" name="saveQuotationCustomNew">Guardar</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END MODAL DE COTIZACION -->
