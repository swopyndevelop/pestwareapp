<!-- js/app/adminlte/quatation/register.js-->

<!-- START MODAL DE COTIZACIÓN -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="newQ" role="dialog" aria-labelledby="newEmployeesModal">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">
                                    Cotización (Lista de Precio)</h4>
                            </div>
                            <div class="box-body">
                                <div class="row container-fluid">
                                    <div class="col-md-6">
                                        <span>Fecha: <b>{{$date}}</b></span>
                                    </div>
                                    <div class="col-lg-push-2 col-md-6">
                                        <span>Número de cotización: <b>{{ $id_q }}</b></span>
                                    </div>
                                </div>
                                <hr>

                                <form action="" method="POST" id="formNewQuotation" enctype="multipart/form-data" autocomplete="off">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="idPriceListCustom">
                                    <div class="row container-fluid">
                                        <div class="col-md-6">

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__customerName">
                                                    <div>
                                                        <label class="control-label" for="customerName">Nombre del
                                                            cliente*: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               autocomplete="off"
                                                               id="customerName" name="customerName"
                                                               placeholder="Ejemplo: Juan Manuel">
                                                        <div id="customerNameList"></div>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250
                                                        caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__cellphone">
                                                    <div>
                                                        <label class="control-label" for="cellphone">Celular del Cliente: </label>
                                                        <input type="number" class="formulario__input form-control"
                                                               id="cellphone" name="cellphone"
                                                               placeholder="Ejemplo: 4491234567">
                                                        <div id="numberList"></div>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo admite números,
                                                        mínimo 7 y maximo 14 dígitos.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__establishmentName">
                                                    <div>
                                                        <label class="control-label"
                                                               for="establishmentName">Contacto Principal: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="establishmentName" name="establishmentName"
                                                               placeholder="PestWare App">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250
                                                        caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__cellphoneMain">
                                                    <div>
                                                        <label class="control-label" for="cellphone_main">Celular del Contacto Principal: </label>
                                                        <input type="number" class="formulario__input form-control"
                                                               id="cellphoneMain" name="cellphoneMain"
                                                               placeholder="Ejemplo: 4491234567">
                                                        <div id="numberList"></div>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo admite números,
                                                        mínimo 7 y maximo 14 dígitos.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__emailCustomer">
                                                    <div>
                                                        <label class="control-label" for="emailCustomer">Correo
                                                            electrónico: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="emailCustomer" name="emailCustomer"
                                                               placeholder="Ejemplo: correo@example.com">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">El correo electrónico tiene que
                                                        ser válido por ejemplo: email@gmail.com</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__colony">
                                                    <div>
                                                        <label class="control-label" for="colony">
                                                            @if($labels['colony'] != null) {{ $labels['colony']->label_spanish }}*: @else Colonia*: @endif
                                                        </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="colony" name="colony"
                                                               placeholder="Ejemplo: La México">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250
                                                        caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__municipality">
                                                    <div>
                                                        <label class="control-label"
                                                               for="municipality">Municipio: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="municipality" name="municipality"
                                                               placeholder="Ejemplo: Aguascalientes">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250
                                                        caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__inspection">
                                                    <label class="control-label" for="inspection">Inspección: </label>
                                                    <select name="inspection" id="inspection"
                                                            class="form-control select__input"
                                                            style="width: 100%; font-weight: bold;">
                                                        <option value="0" disabled selected>Sin inspección
                                                        </option>
                                                        @foreach($inspections as $inspection)
                                                            <option value="{{ $inspection->id_quotation }}">{{ $inspection->id_service_order }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6" id="rigthQuotationDiv">

                                            <div class="row form-group">
                                              <div class="col-sm-12 formulario__grupo" id="grupo__establishment">
                                                <label class="control-label" for="establishment">Tipo de
                                                  servicio*: </label>
                                                <select name="establishment" id="establishment"
                                                        class="form-control select__input"
                                                        style="width: 100%; font-weight: bold;">
                                                  <option value="0" disabled selected>Seleccione un tipo de
                                                    servicio
                                                  </option>
                                                  @foreach($establishments as $establishment)
                                                    <option value="{{ $establishment->id }}">{{ $establishment->name }}</option>
                                                  @endforeach
                                                </select>
                                              </div>
                                            </div>

                                            <div class="row form-group" id="grupo__construction_measureDiv">
                                                <div class="col-sm-6 formulario__grupo"
                                                     id="grupo__construction_measure">
                                                    <div>
                                                        <label class="control-label" for="construction_measure">Medidas
                                                            de construcción (m²)*: </label>
                                                        <input type="number" class="formulario__input form-control"
                                                               id="construction_measure" name="construction_measure"
                                                               placeholder="Ejemplo: 90">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error" style="font-size: 10px">Este
                                                        campo solo admite números.</p>
                                                </div>

                                                <div class="col-sm-6 text-center formulario__grupo">
                                                    <label class="control-label" for="construction_measure">Convertidores
                                                        para calcular: </label><br>
                                                    <button class="btn btn-primary" type="button" id="MC"><i
                                                                class="fa fa-arrows"></i></button>
                                                    <button class="btn btn-primary" type="button" id="CE" name="CE"><i
                                                                class="fa fa-home"></i></button>
                                                    <button class="btn btn-primary" type="button" id="CS" name="CS"><i
                                                                class="fa fa-th-large"></i></button>
                                                </div>

                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__plagues">
                                                    <label class="control-label" for="plagues">Tipo de plaga*: </label>
                                                    <select name="plagues[]" id="plagues"
                                                            class="form-control select__input"
                                                            style="width: 100%; font-weight: bold;" multiple="multiple">
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                @if(\Illuminate\Support\Facades\Auth::user()->companie == 333)
                                                <div class="col-sm-6 formulario__grupo" id="grupo__sourceOrigin">
                                                    @else <div class="col-sm-12 formulario__grupo" id="grupo__sourceOrigin">
                                                    @endif
                                                    <label class="control-label" for="sourceOrigin">Fuente de origen*: </label>
                                                    <select name="sourceOrigin" id="sourceOrigin"
                                                            class="form-control select__input"
                                                            style="width: 100%; font-weight: bold;">
                                                        <option value="0" disabled selected>Seleccione una opción
                                                        </option>
                                                        @foreach($sources as $source)
                                                            <option value="{{ $source->id }}">{{ $source->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                    @if(\Illuminate\Support\Facades\Auth::user()->companie == 333)
                                                    <div class="col-sm-6">
                                                        <label class="control-label" for="pesticide">Plaguicidas: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="pesticide" name="pesticide"
                                                               placeholder="Ejemplo: Biothrine">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    @endif
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__messenger">
                                                    <div>
                                                        <label class="control-label" for="messenger">Messenger: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="messenger" name="messenger"
                                                               placeholder="Ejemplo: www.facebook.com/chat#">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">La descripción del messenger debe
                                                        ser de 3 a 250 caracteres..</p>
                                                </div>
                                            </div>

                                            <div class="row" id="disccountDiv"
                                                 @if(Auth::user()->id_plan == 1) data-toggle="tooltip"
                                                 data-placement="bottom" title="Plan Emprendedor y Empresarial"
                                                 disabled="true" @endif>
                                                <div class="col-sm-6">
                                                    <label class="control-label" for="discount">Descuento: </label>
                                                    <select name="discount" id="discount" class="form-control"
                                                            style="width: 100%; font-weight: bold;"
                                                            @if(Auth::user()->id_plan == 1) disabled @endif>
                                                        @foreach($discounts as $discount)

                                                            <option value="{{ $discount->id }}"
                                                                    @if($discount->title == "Sin Descuento") selected @endif>{{ $discount->title }}
                                                                / {{ $discount->percentage }}%
                                                            </option>

                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="control-label" for="extra">Extra: </label>
                                                    <select name="extra" id="extra" class="form-control"
                                                            style="width: 100%; font-weight: bold;"
                                                            @if(Auth::user()->id_plan == 1) disabled @endif>
                                                        @foreach($extras as $extra)
                                                            <option value="{{ $extra->id }}"
                                                                    @if($extra->name == "Sin Extra") selected @endif>{{ $extra->name }}
                                                                / ${{ $extra->amount }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <br>
                                            <div class="row" id="prizeDiv">
                                                <div class="col-sm-6">
                                                    <label class="control-label">Precio: </label><br>
                                                    <input type="hidden" id="symbolCountry"
                                                           value="{{ $symbol_country }}">
                                                    <label class="control-label" for="price" id="priceQ"
                                                           style="font-size: 1.5em;"><b
                                                                id="priceQuotLabel">{{ $symbol_country }}
                                                            0.00</b></label>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="control-label">Precio con descuento: </label><br>
                                                    <label class="control-label text-primary" for="priceQD" id="priceQD"
                                                           style="font-size: 1.5em;"><b
                                                                id="priceFinalQuotLabel">{{ $symbol_country }} 0.00</b></label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="text-center">
                                <div class="col-sm-12 formulario__mensaje" id="formulario__mensaje"
                                     style="margin-top: 10px; margin-bottom: 10px;">
                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b>
                                        Por favor complete los datos correctamente. </p>
                                </div>
                                <br>
                                <div class="col-sm-12 text-center formulario__grupo formulario__grupo-btn-enviar">
                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los
                                        datos correctamente!</p>
                                </div>

                                <div class="col-sm-12" id="saveQuotationDiv">
                                    <button class="btn btn-secondary btn-lg" type="button" id="priceQuotation"
                                            name="priceQuotation">Cotizar
                                    </button>
                                    <button class="btn btn-primary btn-lg" type="button" id="saveQuotation"
                                            name="saveQuotation">Guardar
                                    </button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL DE COTIZACIÓN -->

@include('vendor.adminlte.register._quotationCustom')
