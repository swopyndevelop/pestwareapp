<ul class="dropdown-menu">
	@if($q->id_status != 2)
		@if($q->isCustomQuotation == 0)
			<li><a href="{{ Route('edit_quotation', \Vinkla\Hashids\Facades\Hashids::encode($q->id)) }}"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i>Editar</a></li>
		@else
			@if($q->id_status == 10 || $q->id_status == 1)
				<li><a class="openModalEditCustomQuote" data-id="{{ $q->id }}" style="cursor: pointer;"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i>Editar</a></li>
			@endif
		@endif
		@if($q->id_status != 4)
			@if($q->isCustomQuotation)
				<li><a href="{{ Route('order_service_custom', $q->id) }}" target="_blank"><i class="fa fa-calendar-plus-o" aria-hidden="true" style="color: #2E86C1;"></i>Programar</a></li>
			@else
				<li><a href="{{ Route('order_service', \Vinkla\Hashids\Facades\Hashids::encode($q->id)) }}" @if($q->id == 4685) id="scheduleMenuDiv" @endif><i class="fa fa-calendar-plus-o" aria-hidden="true" style="color: #2E86C1;"></i>Programar</a></li>
			@endif
				<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif><a style="cursor: pointer;" @if(Auth::user()->id_plan != 1) class="openModalCancelQuot" data-toggle="modal" data-target="#denyQ" data-id="{{ $q->id }}" data-quotation="{{ $q->id_quotation }}" @endif><i class="fa fa-window-close-o" aria-hidden="true" style="color: red;"></i>Rechazar</a></li>
		@endif
	@endif
	@if($q->id_status == 2)
		<li><a style="cursor: pointer" data-toggle="modal" data-target="#status<?php echo $q->id; ?>"><i class="fa fa-check" aria-hidden="true" style="color: green;"></i>Recuperar</a></li>
		<!-- <li><a href="{{ Route('recover', $q->id)}}">Recuperar</a></li> -->
	@endif
	@if($q->isCustomQuotation == 0)
		<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{ Route('pdf_quotation', \Vinkla\Hashids\Facades\Hashids::encode($q->id)) }}" @endif target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar PDF</a></li>
	@else
		<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) href="{{Route('pdf_custom', \Vinkla\Hashids\Facades\Hashids::encode($q->id)) }}" @endif target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar PDF</a></li>
	@endif
	@if($q->cellphone != null)
		<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif><a style="cursor: pointer;" @if(Auth::user()->id_plan != 1) class="openModalWhatsappQuotation" data-toggle="modal" data-target="#sendWhatsappQuotation" data-id="{{ $q->id }}" data-url="{{ $q->urlWhatsapp }}" data-cellphone-customer="{{ $q->cellphone }}" data-cellphone-main="{{ $q->cellphone_main }}"@endif><i class="fa fa-whatsapp" aria-hidden="true" style="color: green;"></i>Enviar Cotización</a></li>
	@endif
	@if($q->construction_measure != null)
	<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif><a style="cursor: pointer;" @if(Auth::user()->id_plan != 1) class="openModalSendMailQuot" data-toggle="modal" data-target="#sendmailQuotation" data-id="{{ $q->id }}" data-quotation="{{ $q->id_quotation }}" data-email="{{ $q->email }}" @endif ><i class="fa fa-envelope-o" aria-hidden="true"></i>Enviar Cotización</a></li>
	@else
		<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif><a style="cursor: pointer;" @if(Auth::user()->id_plan != 1) data-toggle="modal" class="openModalSendMailQuot" data-target="#sendmailQuotation" data-id="{{ $q->id }}" data-quotation="{{ $q->id_quotation }}" data-email="{{ $q->email }}" @endif><i class="fa fa-envelope-o" aria-hidden="true"></i>Enviar Cotización</a></li>
	@endif
	@if($q->id_status != 4)
	<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif><a @if(Auth::user()->id_plan != 1) class="openModalTracingQuot" style="cursor: pointer" data-toggle="modal" data-target="#tracingQ" data-id="{{ $q->id }}" data-quotation="{{ $q->id_quotation }}" @endif><i class="fa fa-phone text-info" aria-hidden="true"></i>Seguimiento</a></li>
	@endif
	@if(Entrust::can('Eliminar Datos')  || Entrust::hasRole('Cuenta Maestra'))
	<li data-toggle="tooltip" data-placement="bottom" title="">
		<a class="openModalDeleteQuotation" style="cursor: pointer" data-toggle="modal" data-target="#deleteQuoatation"
		   data-id="{{ $q->id }}"
		   data-quotation="{{ $q->id_quotation }}"
		   data-service-orders="{{ $q->serviceOrdersCount }}">
			<i class="fa fa-trash-o" aria-hidden="true" style="color: red;"></i>Eliminar
		</a>
	</li>
	@endif
	<li data-toggle="tooltip" data-placement="bottom" title="">
		<a style="cursor: pointer" data-toggle="modal" data-target="#approveQuotation"
		   data-id="{{ Vinkla\Hashids\Facades\Hashids::encode($q->id) }}">
			<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>Aprobar
		</a>
	</li>
</ul>

@include('adminlte::register._modalRecoverQuotation')
