@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Inspecciones</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="col-lg-6 margin-mobile">
                            <button type="button" title="Nueva Inspección" class="btn btn-primary" data-toggle="modal" data-target="#newModalInspection">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="col-lg-6 margin-mobile">

                        </div>

                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Inspección</th>
                                        <th> Fecha </th>
                                        <th> Agente</th>
                                        <th> Cliente / Contacto</th>
                                        <th> Colonia / Municipio</th>
                                        <th> Técnico</th>
                                        <th> Estatus</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $a = 0; ?>
                                    @foreach($inspections as $inspection)
                                        <?php $a = $inspection->id; ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                        {{ $inspection->folio }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.register._menuInspection')
                                                </div>
                                            </td>
                                            <td>
                                                {{ $inspection->initial_date }} <br>
                                                {{ $inspection->initial_hour }}
                                            </td>
                                            <td>{{ $inspection->agent }}</td>
                                            <td>{{ $inspection->establishment_name }} <br>
                                                {{ $inspection->cellphone }} <br>
                                                {{ $inspection->customer_name }} <br>
                                                {{ $inspection->cellphone_main }} <br>
                                            </td>
                                            <td>{{ $inspection->colony }} <br>
                                                {{ $inspection->municipality }}
                                            </td>
                                            <td>
                                                {{ $inspection->technician }}
                                            </td>
                                            <td>
                                                @if($inspection->status == 10)
                                                    <span class="label label-default">Sin Programar</span>
                                                @endif
                                                @if($inspection->status == 1)
                                                    <span class="label label-primary">Programado</span>
                                                @endif
                                                @if($inspection->status == 4)
                                                    <span class="label label-success">Finalizado</span>
                                                @endif
                                                @if($inspection->status == 3)
                                                    <span class="label label-danger">Cancelado</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $inspections->links() }}
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
    @include('vendor.adminlte.register._modalInspection')
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/inspection.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>//Select Job Center
        $('#selectInspectionStation').on('change', function() {
            let InspectionStation = $(this).val();
            window.location.href = route('index_inspections_monitoring', InspectionStation);
        });
        $(".applicantsList-single").select2();
    </script>
@endsection