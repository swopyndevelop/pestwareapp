<!-- INICIA MODAL PARA RECUPERAR SERVICIO -->
<div class="modal fade" id="recovery-service<?php echo $o->order; ?>" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<form action="{{ route('order_recover', ['order' => $o->order]) }}" method="POST" id="formRecover<?php echo $o->order; ?>">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<div class="modal-body">
					<h4 class="modal-title">¿Está seguro de que desea recuperar la servicio?</h4>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<div class="text-center">
					<button type="submit" title="Aceptar" class="btn btn-primary" data-toggle="modal" data-target="#recovery-service<?php echo $o->order; ?>"><strong>Aceptar</strong></button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA RECUPERAR SERVICIO -->
