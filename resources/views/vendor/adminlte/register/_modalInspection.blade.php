<!-- START MODAL DE COTIZACIÓN -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="newModalInspection" role="dialog" aria-labelledby="newModalInspection">
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog modal-lg" role="document">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Nueva
                                    Inspección</h4>
                            </div>
                            <div class="box-body">
                                <div class="row container-fluid">
                                    <div class="col-md-6">
                                        <span>Fecha: <b>{{$date}}</b></span>
                                    </div>
                                    <div class="col-lg-push-2 col-md-6">
                                        <span>Número de Inspección: <b>{{ $folioInspection }}</b></span>
                                    </div>
                                </div>
                                <hr>

                                <form action="" method="POST" enctype="multipart/form-data"
                                      autocomplete="off">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="idPriceListCustom">
                                    <div class="row container-fluid">

                                        <div class="col-md-6">

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__customerName">
                                                    <div>
                                                        <label class="control-label" for="customerName">Nombre del
                                                            cliente*: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               autocomplete="off"
                                                               id="customerName" name="customerName"
                                                               placeholder="Ejemplo: Juan Manuel"
                                                               required>
                                                        <div id="customerNameList"></div>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250
                                                        caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__cellphone">
                                                    <div>
                                                        <label class="control-label" for="cellphone">Celular del
                                                            Cliente: </label>
                                                        <input type="number" class="formulario__input form-control"
                                                               id="cellphone" name="cellphone"
                                                               placeholder="Ejemplo: 4491234567">
                                                        <div id="numberList"></div>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo admite números,
                                                        mínimo 7 y maximo 14 dígitos.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__establishmentName">
                                                    <div>
                                                        <label class="control-label"
                                                               for="establishmentName">Contacto Principal: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="establishmentName" name="establishmentName"
                                                               placeholder="PestWare App">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250
                                                        caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__cellphoneMain">
                                                    <div>
                                                        <label class="control-label" for="cellphone_main">Celular del
                                                            Contacto Principal: </label>
                                                        <input type="number" class="formulario__input form-control"
                                                               id="cellphoneMain" name="cellphoneMain"
                                                               placeholder="Ejemplo: 4491234567">
                                                        <div id="numberList"></div>
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo solo admite números,
                                                        mínimo 7 y maximo 14 dígitos.</p>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__emailCustomer">
                                                    <div>
                                                        <label class="control-label" for="emailCustomer">Correo
                                                            electrónico: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="emailCustomer" name="emailCustomer"
                                                               placeholder="Ejemplo: correo@example.com">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">El correo electrónico tiene que
                                                        ser válido por ejemplo: email@gmail.com</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__colony">
                                                    <div>
                                                        <label class="control-label" for="colony">
                                                            @if($labels['colony'] != null) {{ $labels['colony']->label_spanish }}
                                                            *: @else Colonia*: @endif
                                                        </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="colony" name="colony"
                                                               placeholder="Ejemplo: La México">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250
                                                        caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-12 formulario__grupo" id="grupo__municipality">
                                                    <div>
                                                        <label class="control-label"
                                                               for="municipality">Municipio: </label>
                                                        <input type="text" class="formulario__input form-control"
                                                               id="municipality" name="municipality"
                                                               placeholder="Ejemplo: Aguascalientes">
                                                        <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                    </div>
                                                    <p class="formulario__input-error">Este campo admite 3 a 250
                                                        caracteres.</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">

                                                @if(\Illuminate\Support\Facades\Auth::user()->companie == 333)
                                                    <div class="col-sm-6 formulario__grupo" id="grupo__sourceOrigin">
                                                        @else
                                                            <div class="col-sm-12 formulario__grupo"
                                                                 id="grupo__sourceOrigin">
                                                                @endif
                                                                <label class="control-label" for="sourceOrigin">Fuente
                                                                    de origen*: </label>
                                                                <select name="sourceOrigin" id="sourceOrigin"
                                                                        class="form-control select__input"
                                                                        style="width: 100%; font-weight: bold;">
                                                                    <option value="0" disabled selected>Seleccione una
                                                                        opción
                                                                    </option>
                                                                    @foreach($sources as $source)
                                                                        <option value="{{ $source->id }}">{{ $source->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                    </div>

                                            </div>

                                        </div>
                                    </div>
                            </div>

                            <div class="modal-footer">
                                <div class="text-center">
                                    <div class="col-sm-12 formulario__mensaje" id="formulario__mensaje"
                                         style="margin-top: 10px; margin-bottom: 10px;">
                                        <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i>
                                            <b>Error:</b>Por favor complete los datos correctamente. </p>
                                    </div>
                                    <br>
                                    <div class="col-sm-12 text-center formulario__grupo formulario__grupo-btn-enviar">
                                        <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">
                                            Se enviaron los datos correctamente!</p>
                                    </div>

                                    <div class="col-sm-12">
                                        <button class="btn btn-primary btn-lg" type="button" id="btnSaveInspection">Guardar</button>
                                    </div>

                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE COTIZACIÓN -->
