<!-- INICIA MODAL PARA EVALUAR SERVICIO -->
<div class="modal fade" id="raiting" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
        <h3>¿Cómo calificarías este servicio?</h3>
        <br>
        <div id="rateBox"></div>
        <br>
        <textarea style="font-style: normal; font-size: 18px; width: 100%" name="commentsCustomer" id="commentsCustomer" rows="5" placeholder="Comentarios del cliente..."></textarea>
      </div>
      <div class="modal-footer">
        <div class="text-center">
          <button class="btn btn-primary btn-md" type="button" id="saveRating" name="saveRating">Evaluar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- TERMINA MODAL PARA EVALUAR SERVICIO -->
