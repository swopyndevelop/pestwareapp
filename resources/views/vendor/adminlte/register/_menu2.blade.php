<ul class="dropdown-menu">
	@if($o->is_shared == 1 && $o->is_main == 1 || $o->is_shared == 0 && $o->is_main == 0)
		<li>
			<a href="{{Route('edit_service', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}">
				<i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i>Editar
			</a>
		</li>
	@endif
	@if($o->id_status == 1)
		@if($o->is_shared == 1 && $o->is_main == 1 || $o->is_shared == 0 && $o->is_main == 0)
		<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
			<a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#sendReminderWhatsappIndications"
			   data-id="{{ $o->order }}"
			   data-url="{{ $o->urlWhatsappIndications }}"
			   data-cellphonecustomer="{{ $o->cellphone }}"
			   data-cellphonemain="{{ $o->cellphone_main }}"
			   @endif style="cursor: pointer"><i class="fa fa-whatsapp" aria-hidden="true" style="color: green;"></i>Enviar Indicaciones WhatsApp
			</a>
		</li>
		<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
			<a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#sendReminderWhatsappSchedule"
			   data-id="{{ $o->order }}"
			   data-url="{{ $o->urlWhatsappReminder }}"
			   data-cellphonecustomer="{{ $o->cellphone }}"
			   data-cellphonemain="{{ $o->cellphone_main }}"
			   @endif  style="cursor: pointer"><i class="fa fa-bell-o" aria-hidden="true" style="color: green;"></i>Enviar Recordatorio WhatsApp
			</a>
		</li>
		@endif
		@if($o->confirmed == 0)
			@if($o->is_shared == 1 && $o->is_main == 1 || $o->is_shared == 0 && $o->is_main == 0)
			<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
				<a @if(Auth::user()->id_plan != 1) @if($o->order == 2122) @endif
				   data-toggle="modal" data-target="#confirmedServiceOrder"
				   data-id="{{ $o->order }}"
				   data-confirmed="1"
				   data-label="¿Desear Confirmar el Servicio?" @endif
				   style="cursor: pointer">
					<i class="fa fa-check" aria-hidden="true" style="color: green;"></i>
					Confirmar
				</a>
			</li>
			@endif
		@else
			<li>
				<a data-toggle="modal" data-target="#confirmedServiceOrder"
				   data-id="{{ $o->order }}"
				   data-confirmed="0"
				   data-label="¿Desear Desconfirmar el Servicio?"
				   style="cursor: pointer">
					<i class="fa fa-times" aria-hidden="true" style="color: red;"></i>
					Desconfirmar
				</a>
			</li>
		@endif
		<li>
			<a @if(Auth::user()->id_plan != 1) href="{{Route('pdf_order_service_template', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}" target="_blank"@endif>
				<i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Plantilla</a>
		</li>
			@if($o->is_shared == 1 && $o->is_main == 1 || $o->is_shared == 0 && $o->is_main == 0)
				@if(!Entrust::can('No Mostrar Precio (Agenda)'))
				<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
					<a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#newCashModal"
					   data-idevent="{{ $o->id_event }}"
					   data-idorder="{{ $o->order }}"
					   data-total="{{ $o->total }}"
					   data-serviceorder="{{ $o->id_service_order }}"
					   data-amountreceived="@if($o->cashes != null){{ $o->cashes->amount_received }}@endif"
					   data-commentary="@if($o->cashes != null){{ $o->cashes->commentary }}@endif"
					   data-payment="@if($o->cashes != null){{ $o->cashes->payment }}@endif"
					   data-paymentmethod="@if($o->cashes != null){{ $o->cashes->id_payment_method }} @else 0 @endif"
					   data-paymentway="@if($o->cashes != null){{ $o->cashes->id_payment_way }} @else 0 @endif"
					   data-payday="@if($o->cashes != null){{ $o->cashes->updated_at }}@endif"
					   data-isupdate="@if($o->cashes != null) 1 @else 0 @endif"
					   @endif style="cursor: pointer">
						<i class="fa fa-money" aria-hidden="true" style="color: dodgerblue;"></i>@if($o->cashes != null) Editar Pago @else Cobrar @endif</a>
				</li>
				@endif
			@endif
	@endif
	@if($o->id_status == 3)
		<li><a data-toggle="modal" data-target="#recovery-service{{ $o->order }}" style="cursor: pointer"><i class="fa fa-check" aria-hidden="true" style="color: green;"></i>Recuperar</a></li>
	@endif
	@if($o->id_status != 4)
		{{--@if($o->is_shared == 1 && $o->is_main == 1 || $o->is_shared == 0 && $o->is_main == 0)
		<li>
			<a href="{{Route('edit_service', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}">
				<i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i>Editar
			</a>
		</li>
		@endif--}}
		@if($o->id_status != 3)
			<li><a href="{{Route('delete_view', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}"><i class="fa fa-ban" aria-hidden="true" style="color: red;"></i>Cancelar</a></li>
		@endif
	@endif
	@if($o->id_status == 4)
			<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
				<a @if(Auth::user()->id_plan != 1) href="{{ Route('order_view', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}" @endif>
					<i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver
				</a>
			</li>
			<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
				<a @if(Auth::user()->id_plan != 1) href="{{Route('pdf_service', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}" target="_blank"@endif>
					<i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Certificado de Servicio</a>
			</li>
			@if($o->is_shared == 1 && $o->is_main == 1 || $o->is_shared == 0 && $o->is_main == 0)
				<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
					<a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#sendemail"
					   data-id="{{ $o->order }}"
					   data-email="{{ $o->email }}"
					   data-station="{{ $o->isInspection }}"
					   data-area="{{ $o->isAreaInspection }}"
					   @endif style="cursor: pointer">
						<i class="fa fa-envelope" aria-hidden="true" style="color: #222d32;"></i>Enviar Certificado
					</a>
				</li>
				<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
					<a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#sendemailwhatsapp"
					   data-id="{{ $o->order }}"
					   data-urlwhatsapp="{{ $o->urlWhatsappEmail }}"
					   data-service="{{ $o->urlWhatsappEmailService }}"
					   data-order="{{ $o->urlWhatsappEmailOrder }}"
					   @if($o->isInspection == 1) data-urlinspection="{{ $o->urlWhatsappEmailInspection }}" @endif
					   data-cellphone="{{ $o->cellphone }}"
					   data-cellphonemain="{{ $o->cellphone_main }}"
					   data-station="{{ $o->isInspection }}"
					   data-area="{{ $o->isAreaInspection }}"
					   @if($o->isAreaInspection == 1) data-urlarea="{{ $o->isAreaInspectionPdf }}" @endif
					   @endif style="cursor: pointer">
						<i class="fa fa-whatsapp" aria-hidden="true" style="color: green;"></i>Enviar Certificado
					</a>
				</li>
				<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
					<a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#sendRequestSignature" @endif
					   data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($o->order) }}" style="cursor: pointer"
					   data-urlwhatsapp="{{ $o->urlWhatsappEmail }}">
						<i class="fa fa-pencil" aria-hidden="true" style="color: cornflowerblue;"></i>Solicitar Firma
					</a>
				</li>
			@if($o->rating == 0)
				<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
					<a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#raiting" data-id="{{ $o->order }}" @endif style="cursor: pointer">
						<i class="fa fa-star" aria-hidden="true" style="color: green;"></i>
						Evaluar
					</a>
				</li>
			@endif
			@if($o->warranty == 0 && $o->indications != 2)
				<li><a data-toggle="modal" data-target="#warranty" data-id="{{ $o->order }}" style="cursor: pointer">
						<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: #F1C40F;"></i>
						Garantía
					</a>
				</li>
			@endif
			@if($o->price_reinforcement != null)
				@if($o->reinforcement == 0)
					<li>
						<a data-toggle="modal" data-target="#reinforcement" data-id="{{ $o->order }}" style="cursor: pointer">
							<i class="fa fa-shield" aria-hidden="true" style="color: green;"></i>
							Refuerzo
						</a>
					</li>
				@endif
			@endif
			<li>
				<a data-toggle="modal" data-target="#tracing" data-id="{{ $o->order }}" style="cursor: pointer" >
					<i class="fa fa-long-arrow-right" aria-hidden="true" style="color: #222d32;"></i>
					Seguimiento
				</a>
			</li>
			<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
				<a @if(Auth::user()->id_plan != 1) href="{{Route('pdf_order_service', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}" target="_blank"@endif>
					<i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Orden de Servicio</a>
			</li>
			@if($o->inspection == 0)
<!--			<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
				<a @if(Auth::user()->id_plan != 1) href="{{Route('pdf_service', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}" target="_blank"@endif>
					<i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Certificado de Servicio</a>
			</li>-->
			<li>
				<a @if(Auth::user()->id_plan != 1) href="{{Route('pdf_order_service_template', \Vinkla\Hashids\Facades\Hashids::encode($o->order))}}" target="_blank"@endif>
					<i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Plantilla</a>
			</li>
			@endif
			@if($o->isInspection == 1)
			<li><a href="{{ route('station_monitoring_pdf', \Vinkla\Hashids\Facades\Hashids::encode($o->order)) }}" target="_blank"><i class="fa fa-file-text-o" aria-hidden="true" style="color: red;"></i>Descargar Monitoreo</a></li>
			@endif
			@if($o->isAreaInspection == 1)
				<li><a href="{{ route('area_inspection_pdf', \Vinkla\Hashids\Facades\Hashids::encode($o->order)) }}" target="_blank"><i class="fa fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar Monitoreo de Áreas</a></li>
			@endif
			@endif

			@if($o->is_shared == 1 && $o->is_main == 1 || $o->is_shared == 0 && $o->is_main == 0)
				@if(!Entrust::can('No Mostrar Precio (Agenda)'))
				<li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
					<a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#newCashModal"
					   data-idevent="{{ $o->id_event }}"
					   data-idorder="{{ $o->order }}"
					   data-total="{{ $o->total }}"
					   data-serviceorder="{{ $o->id_service_order }}"
					   data-amountreceived="@if($o->cashes != null){{ $o->cashes->amount_received }}@endif"
					   data-commentary="@if($o->cashes != null){{ $o->cashes->commentary }}@endif"
					   data-payment="@if($o->cashes != null){{ $o->cashes->payment }}@endif"
					   data-paymentmethod="@if($o->cashes != null){{ $o->cashes->id_payment_method }} @else 0 @endif"
					   data-paymentway="@if($o->cashes != null){{ $o->cashes->id_payment_way }} @else 0 @endif"
					   data-payday="@if($o->cashes != null){{ $o->cashes->updated_at }}@endif"
					   data-isupdate="@if($o->cashes != null) 1 @else 0 @endif"
					   @endif style="cursor: pointer">
						<i class="fa fa-money" aria-hidden="true" style="color: dodgerblue;"></i>@if($o->cashes != null) Editar Pago @else Cobrar @endif</a>
				</li>
				@endif
			@endif

		@endif
		@if($o->invoiced == 0)
			@if(!Entrust::can('No Mostrar Precio (Agenda)'))
			<li>
				<a data-toggle="modal" data-target="#modalServiceInvoice"
				   data-id="{{ $o->order }}"
				   data-total="{{ $o->total }}"
				   data-folio="{{ $o->id_service_order }}"
				   data-customer="{{ $o->customerIdEncrypt }}"
				   style="cursor: pointer">
					<i class="fa fa-file-text" aria-hidden="true" style="color: #2a4ab6;"></i>Facturar
				</a>
			</li>
			@endif
		@endif
</ul>

@include('vendor.adminlte.register._modalWarrantyOrder')
@include('vendor.adminlte.register._modalTracingOrder')
@include('vendor.adminlte.register._modalReinforcementOrder')
@include('vendor.adminlte.register._modalRaitingOrder')
@include('vendor.adminlte.register._modalSendEmail')
@include('vendor.adminlte.register._modalSendEmailWhatsapp')
@include('vendor.adminlte.register._modalSendSignature')
@include('vendor.adminlte.register._modalRecoverService')
@include('vendor.adminlte.register._modalSendReminderWhatsappSchedule')
@include('vendor.adminlte.register._modalConfirmedServiceOrder')
@include('vendor.adminlte.register._modalSendReminderWhatsappIndications')
@include('vendor.adminlte.register.billing._modalServiceInvoice')