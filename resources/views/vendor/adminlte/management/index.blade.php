@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 id="titleInstance" class="box-title">Administración General</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#banners" aria-controls="instances" role="tab" data-toggle="tab">Banner</a></li>
                                <li role="presentation"><a href="#templates" aria-controls="templates" role="tab" data-toggle="tab">Plantillas</a></li>
                                <li role="presentation"><a href="#emails" aria-controls="emails" role="tab" data-toggle="tab">Correo Masivo</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <br>

                                <div role="tabpanel" class="tab-pane active" id="banners">
                                    <div class="row container-fluid">
                                        <div class="col-md-6">

                                            <form action="{{ route('add_image_banner') }}" method="POST" id="form" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                {{ method_field('POST') }}
                                                <h5>Resolución Imagen 1920px X 1080px</h5>
                                                <label class="control-label">Banner 1</label>
                                                <input type="file" class="form-control" id="fileBannerOne" name="fileBannerOne" accept=".jpg">
                                                <label class="control-label">Banner 2</label>
                                                <input type="file" class="form-control" id="fileBannerTwo" name="fileBannerTwo" accept=".jpg">
                                                <label class="control-label">Banner 3</label>
                                                <input type="file" class="form-control" id="fileBannerThree" name="fileBannerThree" accept=".jpg">
                                                <hr>
                                                <button type="submit" class="btn btn-primary btn-sm"><strong>Guardar</strong></button>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="templates">
                                    <div id="editor-container" style="height: 650px">
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="emails">
                                    <div class="col-md-6">
                                        <h5 for="address" class="form-control-label">Importar Correos:</h5>

                                        <form action="{{ route('import_excel_email') }}" method="POST" id="form" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            {{ method_field('POST') }}
                                            <input type="file" class="form-control" id="fileImport" name="fileImport" required="required"
                                                   accept=".xls">
                                            <hr>
                                            <button type="submit" id="importFilePrices" title="Importar" class="btn btn-primary btn-sm"><strong>Importar</strong></button>
                                        </form>
                                    </div>

                                    <div class="col-md-6">

                                        <h5 for="address" class="form-control-label">Exportar Correos:</h5>

                                        <form action="{{ route('export_excel_email') }}" method="POST" id="form" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            {{ method_field('GET') }}
                                            <button type="submit" title="Importar" class="btn btn-primary btn-sm"><strong>Exportar</strong></button>
                                        </form>

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script src="//editor.unlayer.com/embed.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/management.js') }}"></script>
@endsection