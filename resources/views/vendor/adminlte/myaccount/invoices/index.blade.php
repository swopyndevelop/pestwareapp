@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box" style="height: max-content !important;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mis Facturas - PestWare App</h3>

                        <ol class="breadcrumb" style="margin-top: 5px;">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Mis Facturas</li>
                        </ol>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <a class="btn btn-primary btn-sm" data-toggle="modal" href="#modalDataBilling">
                            <i class="fa fa-id-card-o" aria-hidden="true"></i> Mis Datos de Facturación
                        </a>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <br><br>
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Factura</th>
                                        <th>Fecha / Hora</th>
                                        <th>Descripción</th>
                                        <th>Total</th>
                                        <th>Estatus</th>
                                        <th class="text-center">PDF / XML</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($invoices as $invoice)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                        {{ $invoice->folio_invoice }} <span class="caret"></span>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>{{ $invoice->date }}<br>{{ \Carbon\Carbon::parse($invoice->created_at)->format('H:i') }}</td>
                                            <td>{{ $invoice->description }}</td>
                                            <td>$@convert($invoice->amount)</td>
                                            <td>
                                                <b
                                                        @if($invoice->id_status == 14) style="color: #3C5898" @endif
                                                @if($invoice->id_status == 17) style="color: red" @endif
                                                        @if($invoice->id_status == 15) style="color: green" @endif
                                                >{{ $invoice->status }}
                                                </b>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ Route('download_document_invoice_company', ['invoiceId' => \Vinkla\Hashids\Facades\Hashids::encode($invoice->id), 'type' => 'pdf']) }}" target="_blank">
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 2em;color:red"></i>
                                                </a>
                                                <a href="{{ Route('download_document_invoice_company', ['invoiceId' => \Vinkla\Hashids\Facades\Hashids::encode($invoice->id), 'type' => 'Xml']) }}" target="_blank">
                                                    <i class="fa fa-file-excel-o" aria-hidden="true" style=" font-size: 2em;color:dimgrey"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $invoices->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('vendor.adminlte.myaccount.invoices._modalDataBilling')
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/myaccount.js') }}"></script>
@endsection