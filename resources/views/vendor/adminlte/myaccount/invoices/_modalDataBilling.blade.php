<!-- START MODAL CREATE CUSTOMER INVOICE -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="modalDataBilling" role="dialog" aria-labelledby="modalDataBilling" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 70% !important;">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Mis Datos de Facturación</h4>
                            </div>
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="text-info">Datos Generales</h5>
                                        <div class="col-md-12">
                                            <label for="billingCustomerMainNew">Razón Social:</label>
                                            <input type="text" id="billingCustomerMainNew" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->reason_social }}" @endif>
                                            <input type="text" hidden @if($companyDataBilling != null) value="{{ $companyDataBilling->cfdi_type }}" @else value="0" @endif id="cfdiType">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="rfcCustomerMainNew">RFC:</label>
                                            <input type="text" id="rfcCustomerMainNew" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->rfc }}" @endif>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="emailBillingCustomerMainNew">Email:</label>
                                            <input type="text" name="emailBillingCustomerMainNew" id="emailBillingCustomerMainNew" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->email }}" @endif>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="typeCfdiCustomerMainNew">Tipo CFDI:</label>
                                            <select id="typeCfdiCustomerMainNew" class="form-control"></select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <h5 class="text-info">Datos de Domicilio</h5>
                                        <div class="col-md-12">
                                            <label for="address">Calle:</label>
                                            <input type="text" id="address" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->billing_street }}" @endif>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="exteriorNumber">Número Exterior:</label>
                                            <input type="text" id="exteriorNumber" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->billing_exterior_number }}" @endif>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="interiorNumber">Número Interior:</label>
                                            <input type="text" id="interiorNumber" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->billing_interior_number }}" @endif>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="postalCode">Código Postal:</label>
                                            <input type="text" id="postalCode" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->billing_postal_code }}" @endif>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="colony">Colonia:</label>
                                            <input type="text" id="colony" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->billing_colony }}" @endif>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="municipality">Municipio:</label>
                                            <input type="text" id="municipality" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->billing_municipality }}" @endif>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="state">Estado:</label>
                                            <input type="text" id="state" class="form-control" @if($companyDataBilling != null) value="{{ $companyDataBilling->billing_state }}" @endif>
                                        </div>
                                    </div>
                                </div>
                                <br>


                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="text-center">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelCustomerBilling">Cancelar</button>
                                            <button class="btn btn-primary" type="button" id="saveCustomerBilling">Guardar</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CREATE CUSTOMER INVOICE -->
