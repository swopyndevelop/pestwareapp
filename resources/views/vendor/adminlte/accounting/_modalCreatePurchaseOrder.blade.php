<!-- START MODAL DE ORDEN DE COMPRA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalPurchaseOrder" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitleOrder">Orden de Compra</h4>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <span style="color:black;"># Orden de Compra </span><span id="folioPurchase">@if(empty($purchaseId)) OC-1000 @else OC-{{ $purchaseId->id }} @endif</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row form-group">

                                        <div class="col-sm-12 formulario__grupo" id="grupo__company">
                                            <div>
                                                <label class="control-label" style="font-weight: bold;" for="descriptionOrder">Nombre de la Empresa (Proveedor)</label>
                                                <input type="text" class="formulario__input form-control" id="company" name="company" placeholder="Ejemplo: PestWareApp">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                <div id="companyList"></div>
                                            </div>
                                            <p class="formulario__input-error">El nombre tiene que ser de 3 a 250 caracteres y solo puede contener letras, espacios y acentos.</p>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div class="row form-group">
                                        <div class="col-md-12 formulario__grupo" id="grupo__contactName">
                                            <label style="font-weight: bold;">Nombre de Contacto</label>
                                            <div class="">
                                                <input class="formulario__input form-control" type="text" name="contactName" id="contactName" placeholder="Ejemplo: Juan Manuel">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El nombre del gasto tiene que ser de 3 a 250 caracteres y solo puede contener letras, espacios y acentos.</p>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12 formulario__grupo" id="grupo__contactAddress">
                                            <label style="font-weight: bold;">Nombre de la Dirección</label>
                                            <div class="">
                                                <input class="formulario__input form-control" type="text" name="contactAddress" id="contactAddress" placeholder="Ejemplo: Av. Aguascalientes">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El nombre del gasto tiene que ser de 3 a 250 caracteres y solo puede contener letras, espacios y acentos.</p>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12 formulario__grupo" id="grupo__contactCellphone">
                                            <label style="font-weight: bold;">Celular</label>
                                            <div class="">
                                                <input class="formulario__input form-control" type="text" name="contactCellphone" id="contactCellphone" placeholder="Ejemplo: 4491234567">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El nombre del gasto tiene que ser de 3 a 250 caracteres y solo puede contener letras, espacios y acentos.</p>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12 formulario__grupo" id="grupo__contactEmail">
                                            <label style="font-weight: bold;">Correo</label>
                                            <div class="">
                                                <input class="formulario__input form-control" type="text" name="contactEmail" id="contactEmail" placeholder="Ejemplo: correo@gmail.com">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El nombre del gasto tiene que ser de 3 a 250 caracteres y solo puede contener letras, espacios y acentos.</p>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="row form-group">
                                        <div class="col-md-12 formulario__grupo" id="grupo__bank">
                                            <label style="font-weight: bold;">Banco</label>
                                            <div class="">
                                                <input class="formulario__input form-control" type="text" name="bank" id="bank" placeholder="Ejemplo: Bancomer">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El nombre del gasto tiene que ser de 3 a 250 caracteres y solo puede contener letras, espacios y acentos.</p>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12 formulario__grupo" id="grupo__accountHolder">
                                            <label style="font-weight: bold;">Titular de Cuenta</label>
                                            <div class="">
                                                <input class="formulario__input form-control" type="text" name="accountHolder" id="accountHolder" placeholder="Ejemplo: Juan Manuel Perez">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El nombre del gasto tiene que ser de 3 a 250 caracteres y solo puede contener letras, espacios y acentos.</p>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12 formulario__grupo" id="grupo__clabe">
                                            <div class="">
                                                <label style="font-weight: bold;">Clabe</label>
                                                <input class="formulario__input form-control" type="number" name="clabe" id="clabe" placeholder="Ejemplo: 123456781234567890">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El artículo o concepto debe ser de 3 a 250 caracteres.</p>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-6 formulario__grupo" id="grupo__accountNumber">
                                            <label style="font-weight: bold;">Número de cuenta</label>
                                            <div class="">
                                                <input class="formulario__input form-control" type="number" name="accountNumber" id="accountNumber" placeholder="Ejemplo: 1234567890">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El nombre del gasto tiene que ser de 3 a 250 caracteres y solo puede contener letras, espacios y acentos.</p>
                                        </div>

                                        <div class="col-md-6 formulario__grupo" id="grupo__rfc">
                                            <div class="">
                                                <label style="font-weight: bold;">{{ $profileJobCenterRfc }}</label>
                                                <input class="formulario__input form-control" type="text" name="rfc" id="rfc" placeholder="Ejemplo: Información Fiscal">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">Sólo acepta números y punto para decimales.</p>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-3">
                                    <div class="row form-group">

                                        <div class="col-sm-12 formulario__grupo" id="grupo__concept">
                                            <label class="control-label" for="concept" style="font-weight: bold;">Concepto*: </label>
                                            <select name="concept" id="concept" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                <option value="0" disabled selected>Seleccione una opción</option>
                                                @foreach($concepts as $concept)
                                                    @if($concept->type == 1)
                                                        <option value="{{ $concept->id }}">{{ $concept->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-9">

                                    <div class="row form-group">
                                        <div class="col-sm-4 formulario__grupo" id="grupo__paymentWay">
                                            <label class="control-label" for="paymentWay" style="font-weight: bold;">Forma de Pago: </label>
                                            <select name="paymentWay" id="paymentWay" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                @foreach($paymentWays as $paymentWay)
                                                    <option value="{{ $paymentWay->id }}">{{ $paymentWay->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-sm-4 formulario__grupo" id="grupo__days">
                                            <div class="">
                                                <label style="font-weight: bold;">Días de Crédito</label>
                                                <input class="formulario__input form-control" type="number" name="days" id="days" placeholder="15" value="0">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error">El artículo o concepto debe ser de 3 a 250 caracteres.</p>
                                        </div>

                                        <div class="col-sm-4 formulario__grupo" id="grupo__paymentMethod">
                                            <label class="control-label" for="paymentMethod" style="font-weight: bold;">Método de Pago: </label>
                                            <select name="paymentMethod" id="paymentMethod" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                @foreach($paymentMethods as $paymentMethod)
                                                    <option value="{{ $paymentMethod->id }}">{{ $paymentMethod->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row form-group">

                                        <div class="col-sm-9 formulario__grupo" id="grupo__descriptionOrder">
                                            <div>
                                                <label class="control-label" for="descriptionOrder" style="font-weight: bold;">Descripción de Compra</label>
                                                <input type="text" class="formulario__input form-control" id="descriptionOrder" name="descriptionOrder" placeholder="Ejemplo: Compra plaguicidas...">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                            </div>
                                            <p class="formulario__input-error" style="font-size: 10px">Este campo solo admite números.</p>
                                        </div>

                                        <div class="col-sm-3 formulario__grupo text-left" id="grupo__voucher">
                                            <label class="control-label" for="paymentMethod" style="font-weight: bold;">Tipo de Comprobante</label>
                                            <select name="voucher" id="voucher" class="form-control select__input" style="width: 100%; font-weight: bold;">
                                                <option value="0" disabled selected>Seleccione una opción</option>
                                                @foreach($vouchers as $voucher)
                                                    <option value="{{ $voucher->id }}">{{ $voucher->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <!-- Start New version -->
                                <div class="col-md-12">
                                    <label class="control-label" style="font-weight: bold;">Detalle de la Compra</label>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select id="products" class="form-control" style="width: 100%"></select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" id="quantityProduct" placeholder="Cantidad Envase" disabled>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" id="taxes" placeholder="Impuestos Aplicados" disabled>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" id="lastPrice" placeholder="Último Precio">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" id="quantity" placeholder="Cantidad">
                                        </div>
                                        <div class="col-md-1" id="actionsHeader">
                                            <button class="btn btn-primary" id="addTest">Agregar</button>
                                        </div>
                                    </div>
                                    <br>
                                </div>

                                <div class="col-md-12 table-responsive">
                                    <table class="table table-responsive-md table-sm table-bordered table-hover" id="customTable">
                                        <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Cantidad Envase</th>
                                            <th>Impuestos Aplicados</th>
                                            <th>Último Precio</th>
                                            <th>Cantidad</th>
                                            <th>Subtotal</th>
                                            <th>Total</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- End New version -->

                            </div>

                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <label class="control-label" for="sumaSubtotal" style="font-size: 1.2em;">Subtotal: <b id="subtotalTest"></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 pull-right">
                                        <label class="control-label text-primary" for="total" style="font-size: 1.5em;">Total: <b id="totalTest"></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelEditProfileDiv">Cancelar</button>
                                        <button class="btn btn-primary" type="button" id="savePurchaseOrder" name="savePurchaseOrder">Generar Orden Compra</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE ORDEN DE COMPRA -->
