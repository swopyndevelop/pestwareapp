@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen" id="entry">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Contabilidad</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 margin-mobile">
                                        <a type="submit" class="btn btn-primary-dark" data-toggle="modal" href="#ModalPurchaseOrder">Orden de Compra</a>
                                        <a type="submit" class="btn btn-primary-dark" data-toggle="modal" href="#ModalExpense">Captura de Gasto</a>
                                    </div>
                                    <div class="col-md-4 margin-mobile">
                                        <select name="selectJobCenterAccounting" id="selectJobCenterAccounting" class="form-control" style="font-weight: bold;">
                                            @foreach($jobCenters as $jobCenter)
                                                @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                                    <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                                        {{ $jobCenter->name }}
                                                    </option>
                                                @else
                                                    <option value="{{ $jobCenter->id }}">
                                                        {{ $jobCenter->name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 margin-mobile">
                                        {!!Form::open(['method' => 'GET','route'=>'index_accounting'])!!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Buscar (OC ó OG)" name="search" id="search">
                                            <span class="input-group-btn">
											<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
										</span>
                                        </div>
                                        {!!Form::close()!!}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table tablesorter table-hover text-center" id="tableProduct">
                                            <thead class="table-general">
                                                <tr class="small">
                                                    <th># OC/CG</th>
                                                    <th>Fecha Compra</th>
                                                    <th>Solicitante</th>
                                                    <th>Proveedor / Contacto</th>
                                                    <th>Descripción Compra</th>
                                                    <th>Tipo de Gasto</th>
                                                    <th>Concepto</th>
                                                    <th>Monto</th>
                                                    <th>Estatus</th>
                                                    <th>Estatus Pago</th>
                                                    <th>Info Pago</th>
                                                    <th>Estatus <br> Contable</th>
                                                    <th>Comprobante</th>
                                                </tr>
                                                <tr class="small">
                                                    <th>
                                                        <button id="btnFilterExpense" class="btn btn-default btn-md"><i class="glyphicon glyphicon-search"></i> Filtrar</button>
                                                    </th>
                                                    <th>
                                                        <input class="text-input" type="text" name="filterDateExpense" id="filterDateExpense">
                                                    </th>
                                                    <th>
                                                        <select id="selectFilterEmployee" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            @foreach($employees as $employee)
                                                                <option value="{{ $employee->employee_id }}">
                                                                    {{ $employee->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </th>
                                                    <th>
                                                        <select id="selectFilterProvider" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            @foreach($providers as $provider)
                                                                <option value="{{ $provider->id }}">
                                                                    {{ $provider->company }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="filterDescription" id="filterDescription" class="applicantsList-single form-control header-table">
                                                    </th>
                                                    <th>
                                                        <select id="selectFilterType" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            <option value="1">Gasto</option>
                                                            <option value="2">Compra</option>
                                                        </select>
                                                    </th>
                                                    <th>
                                                        <select id="selectFilterConcepts" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            @foreach($concepts as $concept)
                                                                <option value="{{ $concept->id }}">
                                                                    {{ $concept->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="filterTotal" id="filterTotal" class="applicantsList-single form-control header-table">
                                                    </th>
                                                    <th>
                                                        <select id="filterStatus" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            <option value="2">Cerrada</option>
                                                            <option value="1">Sin Ingresar</option>
                                                        </select>
                                                    </th>
                                                    <th>
                                                        <select id="filterStatusPayment" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            <option value="2">Pagado</option>
                                                            <option value="1">Por Pagar</option>
                                                        </select>
                                                    </th>
                                                    <th>
                                                        <select id="filterInfoPayment" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            <option value="2">Contado</option>
                                                            <option value="1">Crédito</option>
                                                        </select>
                                                    </th>
                                                    <th>
                                                        <select id="filterStatusAccount" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            <option value="2">Capturado</option>
                                                            <option value="1">Sin Capturar</option>
                                                        </select>
                                                    </th>
                                                    <th>
                                                        <select id="selectFilterVouchers" class="applicantsList-single form-control header-table">
                                                            <option value="0" selected>Todos</option>
                                                            @foreach($vouchers as $voucher)
                                                                <option value="{{ $voucher->id }}">
                                                                    {{ $voucher->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody class="small">
                                            <?php $a = 0; ?>
                                                @foreach($accountings as $ac)
                                                    <?php $a = $ac->folio; ?>
                                                    <tr>
                                                        <td>
                                                            <div class="btn-group">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                                    {{ $ac->folio }} <span class="caret"></span>
                                                                </a>
                                                                @include('vendor.adminlte.accounting._menu')
                                                            </div>
                                                        </td>
                                                        <td>{{ $ac->date }}</td>
                                                        <td>{{ $ac->solicitante }}</td>
                                                        @if($ac->type == 1)
                                                            <td>{{ $ac->proveedor }} <br> {{ $ac->contact_name }} <br> {{ $ac->contact_cellphone }}</td>
                                                        @else <td></td> @endif
                                                        <td>{{ $ac->description }}</td>
                                                        @if($ac->type == 1)
                                                            <td>Compra</td>
                                                        @else <td>Gasto</td> @endif
                                                        <td>{{ $ac->concepto }}</td>
                                                        <td>{{ $symbol_country }}{{ $ac->total }}</td>
                                                        <td>
                                                            @if($ac->type == 1)
                                                                @if($ac->has_entry == 0)
                                                                    <b class="text-danger">Sin Capturar</b>
                                                                @elseif ($ac->has_entry == 1)
                                                                    <b style="color: green">Cerrada</b>
                                                                @elseif ($ac->has_entry == 2)
                                                                    <b style="color: orange">Parcial</b>
                                                                @endif
                                                                @if($ac->has_entry != 0)
                                                                    <br>
                                                                    @foreach($ac->entries as $entry)
                                                                        <span class="text-secondary text-bold">{{ $entry->id_entry }}, </span>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </td>
                                                        @if($ac->status == 1)
                                                            <td class="text-center"><b class="text-danger">Por Pagar</b></td>
                                                        @else
                                                            <td class="text-center">
                                                                <b class="text-success">Pagado</b>
                                                                @if($ac->type == 2 && $ac->status == 2)
                                                                    @if($ac->refund == 1) <br> <b style="color: orange;">Reembolsado</b> @endif
                                                                @endif
                                                            </td>
                                                        @endif
                                                        <td>
                                                            {{ $ac->name_payment_way }} <br>
                                                            {{ $ac->typePayment }}
                                                        </td>
                                                        @if($ac->account_status == 1)
                                                            <td class="text-center"><b class="text-danger">Sin Capturar</b></td>
                                                        @elseif($ac->account_status == 2)
                                                            <td><b class="text-info">Capturado</b></td>
                                                        @else <td>Desconocido</td> @endif
                                                        <td class="text-center">
                                                            {{ $ac->voucher }}<br>
                                                            @foreach($ac->photosMonitoring as $photoMonitoring)
                                                                <div id="selector{{$photoMonitoring->id}}" style="cursor: pointer">
                                                                    @if($photoMonitoring->fileExtension != 'pdf')
                                                                        <a href="{{Route('download_archive_accounting', ['id' => $photoMonitoring->id, 'type' => $ac->type])}}" target="_blank">
                                                                            <i class="fa fa-file-image-o" aria-hidden="true" style=" font-size: 2.5em;color:blue"></i>
                                                                        </a>
                                                                    @else
                                                                        <a href="{{Route('download_archive_accounting', ['id' => $photoMonitoring->id, 'type' => $ac->type])}}" target="_blank">
                                                                            <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 2.5em;color:red"></i>
                                                                        </a>
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
<!--                                     Links-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('vendor.adminlte.accounting._modalCreatePurchaseOrder')
@include('vendor.adminlte.accounting._modalCreateExpense')
@endsection
@section('personal-js')

    <script type="text/javascript" src="{{ URL::asset('js/build/expense.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/purchase.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/accounting.js') }}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            @foreach($accountings as $ac)
                @foreach ($ac->photosMonitoring as $photoMonitoring)
                    $(`#selector{{$photoMonitoring->id}}`).lightGallery({
                        selector: '.item'
                    });
                @endforeach
            @endforeach
        });

    </script>

    <script>
        $(".select-search-products").select2();
    </script>

@endsection