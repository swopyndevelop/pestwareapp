@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE ORDEN DE COMPRA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalEditPurchaseOrder"  style="verflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <input type="number" name="id" id="id" hidden value="{{ $purchaseOrder->id }}">
                                </div>
                            </div>

                            <div class="box-body">
                                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Orden de Compra</h4>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <span style="color:black;"># Orden de Compra </span><span id="folioPurchase">{{ $purchaseOrder->folio }}</span>
                                    </div>
                                </div>
                                <div class="row form-group">

                                    <div class="col-md-12">
                                        <span style="font-weight: bold;">Proveedor</span>
                                        <input class="form-control" type="text" name="company" id="company" value="{{ $purchaseOrder->proveedor }}">
                                        <div id="companyList">

                                        </div>
                                    </div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-md-6">
                                        <span style="font-weight: bold;">Datos de Contacto</span>
                                        <input type="text" class="form-control" name="contactName" id="contactName" value="{{ $purchaseOrder->contact_name }}">
                                        <input type="text" class="form-control" name="contactAddress" id="contactAddress" value="{{ $purchaseOrder->contact_address }}">
                                        <input type="text" class="form-control" name="contactCellphone" id="contactCellphone" value="{{ $purchaseOrder->contact_cellphone }}">
                                        <input type="text" class="form-control" name="contactEmail" id="contactEmail" value="{{ $purchaseOrder->contact_email }}">
                                    </div>

                                    <div class="col-md-6">
                                        <span style="font-weight: bold;">Datos Bancarios</span>
                                        <input type="text" class="form-control" name="bank" id="bank" value="{{ $purchaseOrder->bank }}">
                                        <input type="text" class="form-control" name="accountHolder" id="accountHolder" value="{{ $purchaseOrder->account_holder }}">
                                        <input type="text" class="form-control" name="accountNumber" id="accountNumber" value="{{ $purchaseOrder->account_number }}">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="clabe" id="clabe" value="{{ $purchaseOrder->clabe }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="rfc" id="rfc" value="{{ $purchaseOrder->rfc }}">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-md-6">
                                        <span style="font-weight: bold;">Concepto</span>
                                        <select class="form-control" name="concept" id="concept">
                                            @foreach($concepts as $concept)
                                                @if($concept->type == 1)
                                                    @if($concept->id == $purchaseOrder->id_concept)
                                                        <option value="{{ $concept->id }}" selected>{{ $concept->name }}</option>
                                                    @else
                                                        <option value="{{ $concept->id }}">{{ $concept->name }}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <span style="font-weight: bold;">Forma de Pago</span>
                                        <select class="form-control" name="paymentWay" id="paymentWay">
                                            @foreach($paymentWays as $paymentWay)
                                                @if($paymentWay->id == $purchaseOrder->id_payment_way)
                                                    <option value="{{ $paymentWay->id }}" selected>{{ $paymentWay->name }}</option>
                                                @else
                                                    <option value="{{ $paymentWay->id }}">{{ $paymentWay->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <span style="font-weight: bold;">Días de Crédito</span>
                                        @if($purchaseOrder->credit_days != null)
                                            <input class="form-control" type="text" name="days" id="days" value="{{ $purchaseOrder->credit_days }}">
                                        @else
                                            <input class="form-control" type="text" name="days" id="days" placeholder="15" disabled>
                                        @endif
                                    </div>

                                    <div class="col-md-2">
                                        <span style="font-weight: bold;">Método de Pago</span>
                                        <select class="form-control" name="paymentMethod" id="paymentMethod">
                                            @foreach($paymentMethods as $paymentMethod)
                                                @if($paymentMethod->id == $purchaseOrder->id_payment_method)
                                                    <option value="{{ $paymentMethod->id }}" selected>{{ $paymentMethod->name }}</option>
                                                @else
                                                    <option value="{{ $paymentMethod->id }}">{{ $paymentMethod->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-md-12">
                                        <span style="font-weight: bold;">Descripción de Compra</span>
                                        <input type="text" class="form-control" name="descriptionOrder" id="descriptionOrder" value="{{ $purchaseOrder->description }}">
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-responsive-md table-sm table-bordered" id="makeEditablePurchaseOrderEdit">
                                            <thead>
                                            <tr>
                                                <th>Concepto / Artículo</th>
                                                <th>Cantidad</th>
                                                <th>Precio Unitario</th>
                                                <th>Subtotal</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($articlesPurchase as $ap)
                                                <tr>
                                                    <td>{{ $ap->concept }}</td>
                                                    <td>{{ $ap->quantity }}</td>
                                                    <td>{{ $ap->unit_price }}</td>
                                                    <td>{{ $ap->subtotal }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <span style="float:left"><button id="but_add_conceptEdit" class="btn btn-primary">+</button></span>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-lg-6 text-left">
                                        <span style="font-weight: bold;">Tipo de Comprobante</span>
                                        <select class="form-control" name="voucher" id="voucher">
                                            @foreach($vouchers as $voucher)
                                                @if($voucher->id == $purchaseOrder->id_voucher)
                                                    <option value="{{ $voucher->id }}" selected>{{ $voucher->name }}</option>
                                                @else
                                                    <option value="{{ $voucher->id }}">{{ $voucher->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-6 margin-mobile">
                                        <div class="input-group col-sm-4 pull-right">
                                            <span class="input-group-addon">Subtotal {{ $symbol_country }}</span>
                                            <input type="text" id="sumaSubtotalEdit" disabled="disabled" class="form-control text-bold" aria-label="0.00" value="{{ $purchaseOrder->total }}">
                                        </div>
                                        <div class="input-group col-sm-4 pull-right">
                                            <span class="input-group-addon">Total {{ $symbol_country }}</span>
                                            <input type="text" id="totalEdit" disabled="disabled" class="form-control text-bold" aria-label="0.00" value="{{ $purchaseOrder->total }}">
                                        </div>
                                        <button class="btn btn-secondary margin-mobile" type="button" id="calculateConceptEdit" name="calculateConceptEdit">Calcular</button>
                                    </div>
                                </div>
                                <hr>
                                <div class="text-center">
                                    <button class="btn btn-primary" type="button" id="editPurchaseOrder" name="editPurchaseOrder">Actualizar Orden Compra</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE ORDEN DE COMPRA -->
@endsection

@section('personal-js')
    <script>
        $(".plagueList-single").select2();
        $("#ModalEditPurchaseOrder").modal();
        $("#ModalEditPurchaseOrder").on('hidden.bs.modal', function() {
            window.location.href = '{{route("index_accounting")}}'; //using a named route
        });
    </script>
    <script src="{{ asset('js/bootstablePurchase.js') }}" ></script>
    <script src="https://use.fontawesome.com/92e776c0ac.js"></script>
    <script>
        $(function() {

            //autocomplete provider name
            $('#company').keyup(function () {
                let query = $(this).val();
                if (query != ''){
                    $.ajax({
                        type: 'GET',
                        url: route('autocomplete_company'),
                        data: {
                            _token: $("meta[name=csrf-token]").attr("content"),
                            query: query,
                        },
                        success: function (data) {
                            $('#companyList').fadeIn();
                            $('#companyList').html(data);
                        }
                    });
                }
            });

            $(document).on('click', 'li#getdataCompany', function () {
                loaderC('');
                $('#company').val($(this).text());
                $('#companyList').fadeOut();
                //cargar datos de proveedor
                let companyLoad = $('#company').val();
                $.ajax({
                    type: 'GET',
                    url: route('load_company'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        company: companyLoad
                    },
                    success: function (data) {
                        $('#contactName').val(data.contact_name).text();
                        $('#contactAddress').val(data.contact_address).text();
                        $('#contactCellphone').val(data.contact_cellphone).text();
                        $('#contactEmail').val(data.contact_email).text();
                        $('#bank').val(data.bank).text();
                        $('#accountHolder').val(data.account_holder).text();
                        $('#accountNumber').val(data.account_number).text();
                        $('#clabe').val(data.clabe).text();
                        $('#rfc').val(data.rfc).text();
                        Swal.close();
                    }
                });
                $('#companyList').fadeOut();
            });
            //end autocomplete

            //select payment way
            $('#paymentWay').on('change', function() {
                if ($(this).val() === "1") {
                    $("#days").prop("disabled", false);
                } else {
                    $("#days").prop("disabled", true);
                }
            });

            //select payment way
            let disableDays = false;
            $('#paymentWay').on('change', function() {
                if ($(this).val() === "1") {
                    $("#days").prop("disabled", false);
                    disableDays = true;
                } else {
                    $("#days").prop("disabled", true);
                    disableDays = false;
                }
            });

            //select job center
            $('#selectJobCenterAccounting').on('change', function() {
                let idJobCenter = $(this).val();
                window.location.href = route('index_accounting', idJobCenter);
            });

            //edit
            $('#makeEditablePurchaseOrderEdit').SetEditable({ $addButton: $('#but_add_conceptEdit')});
            var arrayConceptsEdit = [];
            var subtotalEdit = 0;
            var totalEdit = 0;

            $('#calculateConceptEdit').on('click',function() {
                calculateArticles();
            });

            function calculateArticles(){
                var td = TableToCSV('makeEditablePurchaseOrderEdit', ',');
                var ar_lines = td.split("\n");
                var each_data_value = [];
                arrayConceptsEdit = [];
                subtotalEdit = 0;
                totalEdit = 0;
                ar_lines.pop();
                for(i=0;i<ar_lines.length;i++)
                {
                    each_data_value[i] = ar_lines[i].split(",");
                }

                for(i=0;i<each_data_value.length;i++)
                {

                    subtotalEdit = subtotalEdit + (each_data_value[i][1] * each_data_value[i][2]);
                    totalEdit = subtotalEdit;
                    // object json
                    arrayConceptsEdit.push({
                        'concept': each_data_value[i][0],
                        'quantity': each_data_value[i][1],
                        'priceUnit': each_data_value[i][2],
                        'subtotal': each_data_value[i][1] * each_data_value[i][2]
                    });

                }

                $('#sumaSubtotalEdit').val(subtotalEdit);
                $('#totalEdit').val(totalEdit);
                console.log(arrayConceptsEdit);
            }

            $('#editPurchaseOrder').on('click',function() {
                //validations
                let company = $('#company').val();
                let contactName = $('#contactName').val();
                let contactAddress = $('#contactAddress').val();
                let contactCellphone = $('#contactCellphone').val();
                let contactEmail = $('#contactEmail').val();
                let bank = $('#bank').val();
                let accountHolder = $('#accountHolder').val();
                let accountNumber = $('#accountNumber').val();
                let clabe = $('#clabe').val();
                let rfc = $('#rfc').val();
                let concept = $("#concept option:selected").val();
                let days = $('#days').val();
                let descriptionOrder = $('#descriptionOrder').val();
                let voucher = $("#voucher option:selected").val();
                let total = $('#totalEdit').val();

                if (company === "") missingText('Ingresa el nombre del Proveedor');
                else if(company.length > 255) missingText('Debes ingresar un texto más corto que no exceda los 255 carácteres');
                else if(contactName === "") missingText('Ingresa el nombre del contacto');
                else if(contactName.length > 255) missingText('Debes ingresar un texto más corto que no exceda los 255 carácteres');
                else if(contactAddress === "") missingText('Ingresa la dirección del contacto');
                else if(contactAddress.length > 255) missingText('Debes ingresar un texto más corto que no exceda los 255 carácteres');
                else if(contactCellphone === "") missingText('Ingresa el número de celular del contacto');
                else if(contactCellphone.length > 14) missingText('Ingresa el número de celular no mayor a 14 digitos');
                else if(isNaN(contactCellphone)) missingText('Este campo sólo admite números');
                else if(contactEmail === "") missingText('Ingresa el correo electrónico del contacto');
                else if (!validateEmail(contactEmail)) missingText('Ingresa un correo valido');
                else if(bank === "") missingText('Ingresa el Banco asociado');
                else if(bank.length > 255) missingText('Debes ingresar un texto más corto que no exceda los 255 carácteres');
                else if(accountHolder === "") missingText('Ingresa el nombre del titular de la cuenta bancaria');
                else if(accountHolder.length > 255) missingText('Debes ingresar un texto más corto que no exceda los 255 digitos');
                else if(accountNumber === "") missingText('Ingresa el numero de cuenta bancaria');
                else if(accountNumber.length > 255) missingText('Debes ingresar la número de cuenta aque no exceda los 255 digitos');
                else if(isNaN(accountNumber)) missingText('Este campo sólo admite números');
                else if(clabe === "") missingText('Ingresa la cuenta interbancaria');
                else if(clabe.length > 255) missingText('Debes ingresar la cuenta interbancaria');
                else if(isNaN(clabe)) missingText('Este campo sólo admite números');
                else if(rfc === "") missingText('Ingresa el RFC');
                else if(rfc.length > 255) missingText('Debes ingresar el RFC no mayor a 255 carácteres)');
                else if(concept == 0) missingText('Debes seleccionar un concepto');
                else if(disableDays)
                {
                    if(days === "") missingText('Ingresa los días de crédito');
                    else {
                        // TODO: Fix code of sentence if
                        if(isNaN(days)) missingText('Este campo sólo admite números');
                        if(descriptionOrder.length > 255) missingText('Debes ingresar un texto más corto que no exceda los 255 carácteres');
                        if(voucher == 0) missingText('Debes de seleccionar un concepto del Tipo Comprobante');
                        if(total.length === 0) missingText('Debes presionar en calcular');
                        else editPurchaseOrder();
                    }
                }
                else if(descriptionOrder.length > 255) missingText('Debes ingresar un texto más corto que no exceda los 255 carácteres');
                else if(voucher == 0) missingText('Debes de seleccionar un concepto del Tipo Comprobante');
                else if(total.length === 0) missingText('Debes presionar en calcular');
                else editPurchaseOrder();
            });

            function editPurchaseOrder(){
                loaderC('');
                calculateArticles();
                //get values input
                let id = $('#id').val();
                let company = $('#company').val();
                let contactName = $('#contactName').val();
                let contactAddress = $('#contactAddress').val();
                let contactCellphone = $('#contactCellphone').val();
                let contactEmail = $('#contactEmail').val();
                let bank = $('#bank').val();
                let accountHolder = $('#accountHolder').val();
                let accountNumber = $('#accountNumber').val();
                let clabe = $('#clabe').val();
                let rfc = $('#rfc').val();
                let concept = $('#concept').val();
                let paymentWay = $('#paymentWay').val();
                let paymentMethod = $('#paymentMethod').val();
                let descriptionOrder = $('#descriptionOrder').val();
                let voucher = $('#voucher').val();
                let jobCenter = $('#selectJobCenterAccounting').val();
                let creditDays = $('#days').val();

                $.ajax({
                    type: 'POST',
                    url: route('edit_purchase'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        id: id,
                        company: company,
                        contactName: contactName,
                        contactAddress: contactAddress,
                        contactCellphone: contactCellphone,
                        contactEmail: contactEmail,
                        bank: bank,
                        accountHolder: accountHolder,
                        accountNumber: accountNumber,
                        clabe: clabe,
                        rfc: rfc,
                        concept: concept,
                        paymentWay: paymentWay,
                        paymentMethod: paymentMethod,
                        descriptionOrder: descriptionOrder,
                        total: totalEdit,
                        voucher: voucher,
                        jobCenter: jobCenter,
                        creditDays: creditDays,
                        arrayConcepts: JSON.stringify(arrayConceptsEdit),
                    },
                    success: function (data) {
                        if (data.errors){
                            Swal.close();
                            missingTextC('error al guardar');
                        }
                        else{
                            Swal.close();
                            correctC();
                            $("#savePurchaseOrder").attr("disabled", true);
                        }
                    }
                });
            }

            //alerts messages
            function correctC() {
                swal({
                    title: "Orden de Compra Actualizada Correctamente",
                    type: "success",
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false
                }).then((result) => {
                    window.location.href = route('index_accounting');
                })
            }

            function loaderC(message){
                Swal.fire({
                    title: message,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },
                }).then((result) => {

                })
            }

            function missingText(textError) {
                swal({
                    title: "¡Espera!",
                    type: "error",
                    text: textError,
                    icon: "error",
                    timer: 3000,
                    showCancelButton: false,
                    showConfirmButton: false
                });
            }

            function validateEmail(contactEmail) {
                const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(contactEmail).toLowerCase());
            }

        });
    </script>
@endsection
