<!-- START MODAL DE ORDEN DE COMPRA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalExpense" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>

                            <form action="{{ Route('add_expense') }}" method="POST" id="formNewExpense" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Captura de Gasto</h4>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <span style="color:black;"># Comprobación de Gasto </span><span>OG 438576</span>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 formulario__grupo" id="grupo__expense_name">
                                        <label style="font-weight: bold;">Nombre de Gasto</label>
                                            <div class="">
                                                <input class="formulario__input form-control" type="text" name="expense_name" id="expense_name" placeholder="Ejemplo: Gasolina.">
                                                    <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                                <input type="number" hidden name="jobCenter" id="jobCenter">
                                            </div>
                                        <p class="formulario__input-error">La descripción del gasto debe ser de 3 a 250 caracteres.</p>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 formulario__grupo" id="grupo__description_expense">
                                        <span style="font-weight: bold;">Descripción de Gasto</span>
                                        <div class="">
                                            <input class="formulario__input form-control" type="text" name="description_expense" id="description_expense" placeholder="Ejemplo: Tanque lleno.">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                        </div>
                                        <p class="formulario__input-error">La descripción del gasto debe ser de 3 a 250 caracteres.</p>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-6 formulario__grupo" id="grupo__conceptExpense">
                                        <div class="formulario__grupo">
                                            <span style="font-weight: bold;">Concepto</span>
                                            <select class="select__input form-control" name="conceptExpense" id="conceptExpense">
                                                <option value="0" disabled selected>Seleccione una opción</option>
                                                @foreach($concepts as $concept)
                                                    @if($concept->type == 2)
                                                        <option value="{{ $concept->id }}">{{ $concept->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6 formulario__grupo" id="grupo__typePayment">
                                        <div class="">
                                            <span style="font-weight: bold;">Forma de Pago</span>
                                            <select class="form-control" name="typePayment" id="typePayment">
                                                @foreach($paymentWays as $paymentWay)
                                                    <option value="{{ $paymentWay->id }}">{{ $paymentWay->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <p class="formulario__input-error">Error.</p>
                                    </div>

                                    <div class="col-md-6 formulario__grupo" id="grupo__daysExpense">
                                        <div class="">
                                        <span style="font-weight: bold;">Días de Crédito</span>
                                        <input class="formulario__input form-control" type="number" name="daysExpense" id="daysExpense" placeholder="15" value="0">
                                            <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                        </div>
                                        <p class="formulario__input-error">Solo acepta números, no mayor a 3 dígitos siendo un máximo de 365 días (año).</p>
                                    </div>

                                    <div class="col-md-6 formulario__grupo" id="grupo__typeMethod">
                                        <div class="">
                                            <span style="font-weight: bold;">Método de Pago</span>
                                            <select class="form-control" name="typeMethod" id="typeMethod">
                                                @foreach($paymentMethods as $paymentMethod)
                                                    <option value="{{ $paymentMethod->id }}">{{ $paymentMethod->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <p class="formulario__input-error">Error.</p>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-9 formulario__grupo" id="grupo__article">
                                        <div class="">
                                            <span style="font-weight: bold;">Concepto / Artículo</span>
                                            <input class="formulario__input form-control" type="text" name="article" id="article" placeholder="Ejemplo: Viaje.">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                        </div>
                                        <p class="formulario__input-error">El artículo o concepto debe ser de 3 a 250 caracteres.</p>
                                    </div>

                                    <div class="col-md-3 formulario__grupo" id="grupo__total">
                                        <div class="">
                                            <span style="font-weight: bold;">Monto</span>
                                            <input class="formulario__input form-control" type="number" name="total" id="totalExpense" placeholder="450.00">
                                                <i class="formulario__validacion-estado fa fa-times-circle"></i>
                                        </div>
                                        <p class="formulario__input-error">Sólo acepta números y punto para decimales.</p>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-sm-6 text-left formulario__grupo" id="grupo__voucherExpense">
                                        <div class="formulario__grupo">
                                            <span style="font-weight: bold;">Tipo de Comprobante</span>
                                            <select class="select__input form-control" name="voucherExpense" id="voucherExpense">
                                                <option value="0" disabled selected>Seleccione una opción</option>
                                                @foreach($vouchers as $voucher)
                                                    <option value="{{ $voucher->id }}">{{ $voucher->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 text-left formulario__grupo">
                                        <div class="">
                                            <span style="font-weight: bold;" class="text-left">Adjuntar Comprobante</span>
                                            <input class="form-control" type="file" name="comprobantArchive[]" id="comprobantArchive" multiple>
                                        </div>
                                        <p class="formulario__input-error">Error.</p>
                                    </div>
                                </div>

                                <br>
                                <div class="formulario__mensaje" id="formulario__mensaje">
                                    <p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
                                </div>
                                <br>
                                <div class="text-center formulario__grupo formulario__grupo-btn-enviar">
                                    <button class="btn btn-primary" type="submit" id="saveExpense">Generar Gasto</button>
                                    <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE ORDEN DE COMPRA -->
