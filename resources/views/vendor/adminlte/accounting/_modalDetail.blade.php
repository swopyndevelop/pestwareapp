<div class="modal fade" id="expenseDetail<?php echo $a; ?>" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle</h4>
                <div class="row container">
                    <div class="col-md-3">
                            <p style="font-weight: bold; color: black; font-size: 18px">{{ $ex->folio }}</p>
                            <p style="font-weight: bold; color: black; font-size: 15px">{{ $ex->date }}</p>
                            <p style="font-weight: bold; color: black; font-size: 15px">{{ $ex->solicitante }}</p>
                    </div>
                </div>
                    <div class="row" style="margin:10px">
                        <div class="col-md-12 table-responsive">
                            <table class="table tablesorter" id="tableProduct">
                                <thead>
                                    <tr>
                                        <th class="text-center col-sm-1">Concepto / Artículo</th>
                                        <th class="text-center col-sm-1">Monto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($articlesExpense as $p)
                                    @if($p->id_expense == $ex->id)
                                        <tr>
                                            <td class="text-center">{{ $p->concept }}</td>
                                            <td class="text-center">{{ $symbol_country }}{{ $p->total }}</td>
                                        </tr>
                                    @endif
                                    @endforeach 
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @if($ex->type == 2)
					<div class="row" style="margin: 10px;">
						@foreach ($archivesPurchase as $f)
							@if ($f->id_expense == $ex->id)
								<a href="{{Route('download_archive_accounting', ['id' => $f->id, 'type' => $ex->type])}}" target="_blank">
									@if($f->extension == "pptx")
										<i class="fa fa-file-powerpoint-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@elseif($f->extension == "doc" || $f->extension == "docx")
										<i class="fa fa-file-word-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@elseif($f->extension == "xls")
										<i class="fa fa-file-excel-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@elseif($f->extension == "pdf")
										<i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@elseif($f->extension == "png" || $f->extension == "jpg" || $f->extension == "JPG")
										<i class="fa fa-file-image-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@else
										<i class="fa fa-file-text-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@endif
								</a>
							@endif
						@endforeach
					</div>
				@endif
                </div>
            </div>
        </div>
    </div>
</div>