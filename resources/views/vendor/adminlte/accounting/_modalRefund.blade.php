<!-- START MODAL DE ORDEN DE COMPRA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalRefund{{ $ac->folio }}" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <form action="{{ Route('add_refund') }}" method="POST" id="form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body text-center">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Reembolso de Gasto</h4>
                                    <h5>Anexe aquí su comprobante de reembolso:</h5>
                                    <input type="number" name="id" value="{{ $ac->id }}" hidden>
                                    <br>
                                    <div class="col-sm- text-left">
                                        <span style="font-weight: bold;" class="text-left">Adjuntar Comprobante</span>
                                        <input class="form-control" type="file" name="comprobant[]" id="comprobant[]" multiple required>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <br>
                                    <div class="text-center">
                                        <button class="btn btn-primary" type="submit">Reembolsar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE ORDEN DE COMPRA -->
