<!-- INICIA MODAL PARA CAMBIAR STATUS CONTABLE -->
<div class="modal fade" id="status{{ $ac->folio }}" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<form action="{{ route('update_status_account') }}" method="POST" id="form">
				{{ csrf_field() }}
				{{ method_field('POST') }}
				<div class="modal-body text-center">
					@if($ac->account_status == 1)
						<h5>¿Está seguro de marcar como Capturada?</h5>
					@else
						<h5>¿Está seguro de marcar como Sin Capturar?</h5>
					@endif
				<div class="col-md-8 col-md-offset-2">
					<input type="text" hidden value="{{ $ac->id }}" id="id" name="id">
					<input type="text" hidden value="{{ $ac->type }}" id="type" name="type">
					@if($ac->account_status == 1)
						<input type="text" hidden value="2" id="status" name="status">
					@else
						<input type="text" hidden value="1" id="status" name="status">
					@endif
				</div>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<div class="text-center">
					<button type="submit" title="Aceptar" class="btn btn-primary" data-toggle="modal" data-target="#status{{ $ac->id }}"><strong>Aceptar</strong></button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA CAMBIAR STATUS CONTABLE -->
