@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<!-- START MODAL DE ORDEN DE COMPRA -->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="ModalExpenseEdit" role="dialog" aria-labelledby="newEmployeesModal" style="overflow-y: scroll;" >
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">
                <!--Default box-->
                <div class="modal-dialog" role="document" style="width: 90% !important;">

                    <div class="modal-content" >
                        <div class="box">
                            <div class="modal-header" >
                                <div class="box-header" >
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <form action="{{ Route('update_expense') }}" method="POST" id="form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Editar Captura de Gasto</h4>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <span style="color:black;"># Comprobación de Gasto </span><span>{{ $expense->folio }}</span>
                                        </div>
                                    </div>
                                    <div class="row form-group">

                                        <div class="col-md-12">
                                            <span style="font-weight: bold;">Nombre de Gasto</span>
                                            <input class="form-control" type="text" name="expense_name" id="expense_name" required value="{{ $expense->expense_name }}">
                                            <input type="number" hidden name="id" id="id" value="{{ $expense->id }}">
                                            <input type="number" hidden name="idArticle" id="idArticle" value="{{ $articleExpense->id }}">
                                        </div>

                                    </div>

                                    <div class="row form-group">

                                        <div class="col-md-12">
                                            <span style="font-weight: bold;">Descripción de Gasto</span>
                                            <input type="text" class="form-control" name="description_expense" id="description_expense" value="{{ $expense->description }}">
                                        </div>

                                    </div>
                                    <div class="row form-group">

                                        <div class="col-md-6">
                                            <span style="font-weight: bold;">Concepto</span>
                                            <select class="form-control" name="conceptExpense" id="conceptExpense" required>
                                                @foreach($concepts as $concept)
                                                    @if($concept->type == 1)
                                                        @if($concept->id == $expense->id_concept)
                                                            <option value="{{ $concept->id }}" selected>{{ $concept->name }}</option>
                                                        @else
                                                            <option value="{{ $concept->id }}">{{ $concept->name }}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-2">
                                            <span style="font-weight: bold;">Forma de Pago</span>
                                            <select class="form-control" name="typePayment" id="typePayment" required>
                                                @foreach($paymentWays as $paymentWay)
                                                    @if($paymentWay->id == $expense->id_payment_way)
                                                        <option value="{{ $paymentWay->id }}" selected>{{ $paymentWay->name }}</option>
                                                    @else
                                                        <option value="{{ $paymentWay->id }}">{{ $paymentWay->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-2">
                                            <span style="font-weight: bold;">Días de Crédito</span>
                                            @if($expense->credit_days != null)
                                                <input class="form-control" type="text" name="daysExpense" id="daysExpense" value="{{ $expense->credit_days }}">
                                            @else
                                                <input class="form-control" type="text" name="daysExpense" id="daysExpense" placeholder="15" value="0">
                                            @endif
                                        </div>

                                        <div class="col-md-2">
                                            <span style="font-weight: bold;">Método de Pago</span>
                                            <select class="form-control" name="typeMethod" id="typeMethod" required>
                                                @foreach($paymentMethods as $paymentMethod)
                                                    @if($paymentMethod->id == $expense->id_payment_method)
                                                        <option value="{{ $paymentMethod->id }}" selected>{{ $paymentMethod->name }}</option>
                                                    @else
                                                        <option value="{{ $paymentMethod->id }}">{{ $paymentMethod->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="row form-group">

                                        <div class="col-md-9">
                                            <span style="font-weight: bold;">Concepto / Artículo</span>
                                            <input class="form-control" type="text" name="article" id="article" required value="{{ $articleExpense->concept }}">
                                        </div>

                                        <div class="col-md-3">
                                            <span style="font-weight: bold;">Monto</span>
                                            <input class="form-control" type="text" name="total" id="total" required value="{{ $articleExpense->total }}">
                                        </div>

                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-sm-3 text-left">
                                            <span style="font-weight: bold;" class="text-left">Fecha de Pago</span>
                                            <input class="form-control" type="date" id="dateExpense" name="dateExpense" value="{{ $expense->date }}">
                                        </div>
                                        <div class="col-sm-3 text-left">
                                            <span style="font-weight: bold;">Tipo de Comprobante</span>
                                            <select class="form-control" name="voucherExpense" id="voucherExpense">
                                                @foreach($vouchers as $voucher)
                                                    @if($voucher->id == $expense->id_voucher)
                                                        <option value="{{ $voucher->id }}" selected>{{ $voucher->name }}</option>
                                                    @else
                                                        <option value="{{ $voucher->id }}">{{ $voucher->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-3 text-left">
                                            <span style="font-weight: bold;" class="text-left">Adjuntar Comprobante</span>
                                            <input class="form-control" type="file" name="comprobant[]" id="comprobant[]" multiple>
                                        </div>
                                        <div class="col-sm-3 text-left margin-mobile">
                                            @foreach ($archivesExpense as $f)
                                                @if ($f->id_expense == $expense->id)
                                                    <a href="{{Route('download_archive_accounting', ['id' => $f->id, 'type' => 2])}}" target="_blank">
                                                        @if($f->extension == "pptx")
                                                            <i class="fa fa-file-powerpoint-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
                                                        @elseif($f->extension == "doc" || $f->extension == "docx")
                                                            <i class="fa fa-file-word-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
                                                        @elseif($f->extension == "xls")
                                                            <i class="fa fa-file-excel-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
                                                        @elseif($f->extension == "pdf")
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
                                                        @elseif($f->extension == "png" || $f->extension == "jpg")
                                                            <i class="fa fa-file-image-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
                                                        @else
                                                            <i class="fa fa-file-text-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
                                                        @endif
                                                    </a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <button class="btn btn-primary" type="submit">Actualizar Gasto</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL DE ORDEN DE COMPRA -->
@endsection

@section('personal-js')
    <script>
        $(".plagueList-single").select2();
        $("#ModalExpenseEdit").modal();
        $("#ModalExpenseEdit").on('hidden.bs.modal', function() {
            window.location.href = '{{route("index_accounting")}}'; //using a named route
        });
    </script>
@endsection
