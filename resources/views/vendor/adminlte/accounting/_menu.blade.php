<ul class="dropdown-menu">
	<li><a data-toggle="modal" data-target="#accountingDetail<?php echo $a; ?>" style="cursor:pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a></li>
	@if($ac->account_status == 1)
		<li><a data-toggle="modal" data-target="#status{{ $ac->folio }}" style="cursor: pointer"><i class="fa fa-check" aria-hidden="true" style="color: green;"></i>Capturar SC</a></li>
	@else
		<li><a data-toggle="modal" data-target="#status{{ $ac->folio }}"><i class="fa fa-times" aria-hidden="true" style="color: red;"></i>Deshacer Capturar SC</a></li>
	@endif
	@if($ac->type == 1)
		<li><a class="openModalEditPurchaseOrder" data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($ac->id) }}" style="cursor: pointer;"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i>Editar</a></li>
		<li><a href="{{ route('view_pdf_purchase_order', \Vinkla\Hashids\Facades\Hashids::encode($ac->id)) }}" style="cursor: pointer" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar Orden De Compra</a></li>
		<li><a style="cursor: pointer;" data-toggle="modal" class="openModalSendMailPurchaseOrder" data-target="#sendmailPurchaseOrder"
			   data-id="{{ $ac->id }}"
			   data-purcharse-order="{{ $ac->folio }}"
			   data-contact-email-provider="{{ $ac->contact_email }}">
				<i class="fa fa-envelope-o" aria-hidden="true">
				</i>Enviar OC</a>
		</li>
		<li><a style="cursor: pointer;" data-toggle="modal" class="openModalWhatsappPurchaseOrder" data-target="#whatsappPurchaseOrder"
			   data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($ac->id) }}"
			   data-contact-cellphone-provider="{{ $ac->contact_cellphone }}">
				<i class="fa fa-whatsapp" aria-hidden="true" style="color: green">
				</i>Enviar Whatsapp OC</a>
		</li>
		@if(Entrust::can('Eliminar Datos')  || Entrust::hasRole('Cuenta Maestra'))
		<li><a style="cursor: pointer;" data-toggle="modal" class="openModalDeletePurchaseOrder" data-target="#deletePurchaseOrder"
			   data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($ac->id) }}">
				<i class="fa fa-trash" aria-hidden="true" style="color: red">
				</i>Eliminar OC</a>
		</li>
		@endif
	@else
		<li><a href="{{ Route('edit_expense', \Vinkla\Hashids\Facades\Hashids::encode($ac->id)) }}"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i>Editar</a></li>
	@endif
	@if($ac->status == 1)
		<li><a data-toggle="modal" data-target="#ModalPayment{{ $ac->folio }}" style="cursor:pointer"><i class="fa fa-money" aria-hidden="true" style="color: dodgerblue;"></i>Pagar</a></li>
	@endif
	@if($ac->type == 2 && $ac->status == 2 && $ac->refund != 1)
		<li><a data-toggle="modal" data-target="#ModalRefund{{ $ac->folio }}"><i class="fa fa-retweet" aria-hidden="true" style="color: orange;"></i>Reembolso</a></li>
	@endif
</ul>
@include('vendor.adminlte.accounting._modalViewDetail')
@include('vendor.adminlte.accounting._modalRefund')
@include('vendor.adminlte.accounting._modalPaymentStatus')
@include('vendor.adminlte.accounting._modalChangeStatusAccount')
@include('vendor.adminlte.accounting._modalSendEmailPurchaseOrder')
@include('vendor.adminlte.accounting._modalSendWhatsappPurchaseOrder')
@include('vendor.adminlte.accounting._modalDeletePurchaseOrder')