<!-- INICIA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA -->
<div class="modal fade" id="accountingDetail<?php echo $a; ?>" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle</h4>
				<div class="row container-fluid">
					<div class="col-md-3">
						<p style="font-weight: bold; color: black; font-size: 18px">{{ $ac->folio }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $ac->date }}</p>
						<p style="font-weight: bold; color: black; font-size: 15px">{{ $ac->solicitante }}</p>
					</div>
					@if($ac->type == 1)
						<div class="col-md-3">
							<label style="font-weight: bold; color: black;">Proveedor:</label> {{ $ac->proveedor }}
							<br>
							<label style="font-weight: bold; color: black;">Contacto:</label> {{ $ac->contact_name }}
							<br>
							<label style="font-weight: bold; color: black;">Teléfono:</label> {{ $ac->contact_cellphone }}
						</div>
					@endif
					<div class="col-md-6">
						<label style="font-weight: bold; color: black;">Tipo:</label> @if($ac->type == 1) Compra @else Gasto @endif
						<br>
						<label style="font-weight: bold; color: black;">Concepto:</label> {{ $ac->concepto }}
						<br>
						<label style="font-weight: bold; color: black;">Descripcion:</label> {{ $ac->description }}
						<br>
						<label style="font-weight: bold; color: black;">Total:</label> {{ $symbol_country }}{{ $ac->total }}
					</div>
				</div>

				@if($ac->type == 1)
					<div class="row" style="margin: 10px;">
						<div class="col-md-12 table-responsive">
							<table class="table tablesorter" id="tableProduct">
								<thead>
									<tr>
										<th class="text-center">Concepto / Artículo</th>
										<th class="text-center">Cantidad</th>
										<th class="text-center">Precio Unitario</th>
										<th class="text-center">Subtotal</th>
										<th class="text-center">Total</th>
									</tr>
								</thead>
								<tbody>
									@foreach($articlesPurchase as $p)
										@if($p->id_purchase_order == $ac->id)
											<tr>
												<td class="text-center">{{ $p->product }}</td>
												<td class="text-center">{{ $p->quantity }}</td>
												<td class="text-center">{{ $symbol_country }}{{ $p->unit_price }}</td>
												<td class="text-center">{{ $symbol_country }}{{ $p->subtotal }}</td>
												<td class="text-center">{{ $symbol_country }}{{ $p->total }}</td>
											</tr>
										@endif
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				@else
					<div class="row" style="margin: 10px;">
						<div class="col-md-12 table-responsive">
							<table class="table tablesorter" id="tableProduct">
								<thead>
								<tr>
									<th class="text-center">Concepto / Artículo</th>
									<th class="text-center">Monto</th>
								</tr>
								</thead>
								<tbody>
								@foreach($article_expenses as $p)
									@if($p->id_expense == $ac->id)
										<tr>
											<td class="text-center">{{ $p->concept }}</td>
											<td class="text-center">{{ $symbol_country }}{{ $p->total }}</td>
										</tr>
									@endif
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				@endif

				@if($ac->type == 2)
					<div class="row" style="margin: 10px;">
						@foreach ($archivesPurchase as $f)
							@if ($f->id_expense == $ac->id)
								<a href="{{Route('download_archive_accounting', ['id' => $f->id, 'type' => $ac->type])}}" target="_blank">
									@if($f->extension == "pptx")
										<i class="fa fa-file-powerpoint-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@elseif($f->extension == "doc" || $f->extension == "docx")
										<i class="fa fa-file-word-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@elseif($f->extension == "xls")
										<i class="fa fa-file-excel-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@elseif($f->extension == "pdf")
										<i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@elseif($f->extension == "png" || $f->extension == "jpg")
										<i class="fa fa-file-image-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@else
										<i class="fa fa-file-text-o" aria-hidden="true" style=" font-size: 3.5em;color:darkgrey"></i>
									@endif
								</a>
							@endif
						@endforeach
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DE LA ENTRADA-->
