@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen content" id="entry">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mis Gastos</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <br>
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table tablesorter table-hover" id="tableProduct" style="min-height: 500px;">
                                            <thead class="table-general">
                                            <tr>
                                                <th class="col-sm-1 text-center">#OG</th>
                                                <th class="col-sm-1 text-center">Gasto</th>
                                                <th class="col-sm-1 text-center">Concepto</th>
                                                <th class="col-sm-1 text-center">Fecha</th>
                                                <th class="col-sm-1 text-center">Descripción del Gasto</th>
                                                <th class="col-sm-1 text-center">Monto</th>
                                                <th class="col-sm-1 text-center">Estatus</th>
                                                <th class="col-sm-1 text-center">Estatus <br> Contable</th>
                                                <th class="col-sm-1 text-center">Comprobante</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $a = 0; ?>
                                            @foreach($expense as $ex)
                                            <?php $a = $ex->folio; ?>
                                            @if($ex !== null)
                                            <tr>
                                                <td class="col-sm-1 text-center">
                                                    <div class="btn-group">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                            {{ $ex->folio }} <span class="caret"></span>
                                                        </a>
                                                        @include('vendor.adminlte.accounting._menu2')
                                                    </div>
                                                </td>
                                                <td class="col-sm-1 text-center">{{$ex->expense_name}}</td>
                                                <td class="col-sm-1 text-center">{{$ex->concepto}}</td>
                                                <td class="col-sm-1 text-center">{{$ex->date}}</td>
                                                <td class="col-sm-1 text-center">{{$ex->description}}</td>
                                                <td class="col-sm-1 text-center">{{ $symbol_country }}{{$ex->total}}</td>
                                                @if($ex->status == 1)
                                                    <td class="text-center"><b class="text-danger">Por Pagar</b></td>
                                                @else
                                                    <td class="text-center">
                                                        <b class="text-success">Pagado</b>
                                                    @if($ex->type == 2 && $ex->status == 2)
                                                        @if($ex->refund == 1) <br> <b style="color: orange;">Reembolsado</b> @endif
                                                    @endif
                                                    </td>
                                                @endif
                                                @if($ex->account_status == 1)
                                                    <td class="text-center"><b class="text-danger">Sin Capturar</b></td>
                                                @elseif($ex->account_status == 2)
                                                    <td><b class="text-info">Capturado</b></td>
                                                @else <td>Desconocido</td> @endif
                                                <td class="text-center">
                                                    {{$ex->voucher}}<br>
                                                    {{--@foreach($ex->photosMonitoring as $photoMonitoring)
                                                        <div id="selectorPhotoMonitoring" style="cursor: pointer">
                                                            <span class="item" data-src="{{ storage_path('app/').$photoMonitoring->file_route }}">
									                            <img src="{{ storage_path('app/').$photoMonitoring->file_route }}" height="50" width="50">
                                                            </span>
                                                        </div>
                                                    @endforeach--}}
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- TODO: Add link paginate -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js"></script>
    <script>
        $('#selectorPhotoMonitoring').lightGallery({
            selector: '.item'
        });
    </script>
@endsection