<ul class="dropdown-menu">
    <li><a class="openModalShowAreaMonitoring" data-toggle="modal" data-target="#showAreaMonitoringModal" data-id="{{ $monitoring->id }}" data-monitoring="{{ $monitoring->id_monitoring }}" style="cursor:pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver Diagrama</a></li>
    @if($monitoring->status == 0 || $monitoring->status == 2)
        <li><a class="openModalUpdateAreaMonitoring"
               data-customerId="{{ $monitoring->customer_id }}"
               data-customerName="{{ $monitoring->customer . " / " . $monitoring->establishment_name }}
               @if($monitoring->id_customer_branch != 0 || $monitoring->id_customer_branch != null)
               {{ '/ Sucursal: ' . $monitoring->CustomerBranchName }}
               @endif"
               data-customerBranchId="{{ $monitoring->CustomerBranchId }}"
               data-id="{{ $monitoring->id }}"
               data-monitoring="{{ $monitoring->id_monitoring }}" style="cursor:pointer">
                <i class="fa fa-pencil-square-o" aria-hidden="true" style="color: darkgreen;"></i>Editar
            </a>
        </li>
        @if(Entrust::can('Eliminar Datos') || Entrust::hasRole('Cuenta Maestra'))
            @if($monitoring->lastInspection == null)
            <li><a data-toggle="modal" data-target="#deleteMonitoring{{ $monitoring->id }}" style="cursor:pointer"><i class="fa fa-trash" aria-hidden="true" style="color: red;"></i>Eliminar</a></li>
            @endif
        @endif
    @endif
    @if($monitoring->status == 0)
        <li><a href="{{ route('monitoring_print_qr', \Vinkla\Hashids\Facades\Hashids::encode($monitoring->id)) }}"><i class="fa fa-qrcode" aria-hidden="true" style="color: dodgerblue;"></i>Imprimir QRs</a></li>
    @endif
    @if($monitoring->status == 2)
        @if($monitoring->diffInSeconds == true)
            <li><a href="{{ route('monitoring_print_qr', \Vinkla\Hashids\Facades\Hashids::encode($monitoring->id)) }}"><i class="fa fa-qrcode" aria-hidden="true" style="color: dodgerblue;"></i>Imprimir QRs</a></li>
        @endif
    @endif
    <li><a data-toggle="modal" data-target="#modalCustomQr" style="cursor:pointer"><i class="fa fa-cogs" aria-hidden="true" style="color: #222d32;"></i>Configurar QRs</a></li>
</ul>
@include('vendor.adminlte.stationmonitoring._modalCustomQr')
@include('vendor.adminlte.stationmonitoring._modalConfirmDelete')
@include('vendor.adminlte.stationmonitoring._modalShowTree')