<!--START CREATE NEW AREA-->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="updateAreaMonitoringModal" role="dialog" aria-labelledby="updateAreaMonitoringModal">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="modal-title col-md-12">
                                    <div class="col-md-7">
                                        <h4 class="text-right text-info" id="modalTitle">Editar Área</h4>
                                    </div>
                                    <div class="col-md-5">
                                        <h4 class="text-right text-info" id="modalTitle"><b class="text-right folioMonitoringUpdate">M-###</b></h4>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>
                                    <div class="col-md-2 pull-right">
                                        <button type="button" class="btn btn-primary btn-sm" id="updateTreeStationMonitoring"><i class="glyphicon glyphicon-floppy-save"></i> Actualizar Área</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="container-tree-upd">
                                        <div id="jsUpdateTreeAreaMonitoring" class="demo" style="margin-top:1em; min-height:200px;"></div>
                                    </div>
                                </div>
                                <br><br>
                                <span class="text-secondary text-bold">*Importante: </span><span class="text-primary">Las estaciones que ya cuentan con inspección si son borradas se perderá dicha información.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END CREATE NEW AREA-->
