<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Qrs</title>
</head>
<style>
    @page {
        margin: 8px;
    }
    body{
        font-family: 'Montserrat'
    }
</style>
<body>
{{--//Tables Qrs--}}
<div style="margin-right:50px;margin-left:35px;">
    <table style="width: 66%; text-align: center">
        <thead>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                @foreach($row as $station)
                    <td style="font-size:8pt; width: 33%; border-style: dotted">

                        <div style="margin-bottom: -60px">

                            <div style="margin-bottom: 0px; text-align: left; padding-left: 10px; margin-top: 10px">

                                <div style="width: 45px; display: inline-block;">
                                    <img src="{{ env('URL_STORAGE_FTP') . $pdfLogo }}" alt="" width="40px" height="40px">
                                </div>
                                <div style="width: 180px; display: inline-block; margin-top: -8px">
                                    <span style="color:black;font-size:9pt; font-weight: bold;">{{ $station['instance'] }}</span>
                                </div>
                                <div style="width: 3px; display: inline-block;">
                                </div>

                            </div>

                            <div style="margin-bottom: -40px; text-align: left; padding-left: 20px; margin-top: 0px">

                                <div style="width: 3px; display: inline-block;">
                                </div>
                                <div style="width: 200px; display: inline-block;">
                                    <span style="color:black;font-size:6pt">Domicilio: {{ $station['address'] }}</span>
                                </div>
                                <div style="width: 3px; display: inline-block;">
                                </div>

                            </div>

                            <div style="width: 5px; display: inline-block; margin-top: 10px; margin-left: 5px; margin-right: 0px">
                            </div>

                            <div style="width: 230px; display: inline-block; margin-right: 7px; text-align: left; margin-top: 3px">
                                <br><span style="color:black;font-size:6pt; margin-left: 13px">
                                @if($customer->companie == 338) Resolución: {{ $station['licence'] }}
                                    @else Licencia: {{ $station['licence'] }}
                                    @endif
                            </span>
                                <br><span style="color:black;font-size:6pt; margin-left: 13px">Telefono: {{ $station['phone'] }}</span>
                            </div>

                        </div>
                        <br><br><br><br><br>
                        <div style="margin-bottom: -40px; text-align: center; padding-left: 30px">

                            <div style="width: 3px; display: inline-block;">
                            </div>
                            <div style="width: 160px; display: inline-block;">
                                <br><span style="color:black;font-size:7pt; font-weight: bold">{{ $station['title'] }}</span>
                                <br><span style="color:black;font-size:7pt; font-weight: bold">{{ $station['subtitle'] }}</span><br><br><br><br>
                                <img style="margin-bottom: -10px; margin-top: -2px" src="data:image/png;base64, {!! $station['qr'] !!}">
                                <p style="color:black;font-size:8pt; font-weight: bold; margin-top: -15px; margin-bottom: 0px">{{ $station['description'] }}</p>
                            </div>
                            <div style="width: 3px; display: inline-block;">
                            </div>

                        </div>
                        <br>
                        <div style="margin-bottom: -79px">
                            <div style="width: 145px; display: inline-block; margin: 5px">
                                <span style="color:black;font-size:5pt">Cliente: {{ $station['customer'] }}</span>
                                <br><span style="color:black;font-size:5pt; margin-bottom: -30px">Zona: {{ $station['zone'] }}</span>
                                <br><span style="color:black;font-size:5pt; margin-bottom: -50px">Perímetro: {{ $station['perimeter'] }}</span><br><br>
                                <div style="text-align: left; padding-left: 4px">
                                    <img src="https://storage.pestwareapp.com/logos_pwa/pwa_large_footer.png" height="15px" alt="logo_pwa_footer">
                                </div>
                            </div>
                            <div style="width: 80px; display: inline-block; text-align: center">
                                <span style="color:black;font-size:7pt">No. Estación</span>
                                <br><span style="color:black;font-size:22pt; font-weight: bold">{{ $station['station_number'] }}</span>
                            </div>
                        </div>

                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>