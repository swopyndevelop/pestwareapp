@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Inspecciones de Estaciones</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-lg-6 margin-mobile">
                            <select name="selectInspectionStation" id="selectInspectionStation" class="form-control" style="font-weight: bold;">
                                @foreach($jobCenters as $jobCenter)
                                    @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                        <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                            {{ $jobCenter->name }}
                                        </option>
                                    @else
                                        <option value="{{ $jobCenter->id }}">
                                            {{ $jobCenter->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 margin-mobile">

                        </div>
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Inspección</th>
                                        <th># Monitoreo</th>
                                        <th>Fecha / Hora de Inspección</th>
                                        <th>Cliente / Empresa</th>
                                        <th>Domicilio</th>
                                        <th># Estaciones</th>
                                    </tr>
                                    <tr>
                                        <th class="">
                                        <span @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
                                            <button id="btnFilterInspection" class="btn btn-default btn-md">
                                                <i class="glyphicon glyphicon-search" @if(Auth::user()->id_plan == 1) disabled @endif></i> Filtrar
                                            </button>
                                        </span>
                                        </th>
                                        <th scope="col">
                                            <input type="text" name="idMonitoringInspection" id="idMonitoringInspection" class="form-control">
                                        </th>
                                        <th scope="col">
                                            <input type="text" name="filterDateInspection" id="filterDateInspection" class="form-control" style="font-size: .7em">
                                        </th>
                                        <th scope="col">
                                            <select id="customersFilter" class="form-control applicantsList-single">
                                                <option value="0" selected>Todos</option>
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id }}">
                                                        {{ $customer->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th scope="col">
                                            <select id="addressFilter" class="form-control applicantsList-single">
                                                <option value="0" selected>Todos</option>
                                                @foreach($addressCustomers as $addressCustomer)
                                                    <option value="{{ $addressCustomer->id }}">
                                                        {{ $addressCustomer->address }}, {{ $addressCustomer->address_number }} {{ $addressCustomer->colony }}
                                                        {{ $addressCustomer->municipality }} {{ $addressCustomer->state }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th class="text-center">
                                            #
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $a = 0; ?>
                                    @foreach($inspections as $inspection)
                                        <?php $a = $inspection->id; ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                        {{ $inspection->id_inspection }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.stationmonitoring._menuInspections')
                                                </div>
                                            </td>
                                            <td>{{ $inspection->id_monitoring }}</td>
                                            <td>{{ $inspection->date }}<br>{{ $inspection->hour }}<br>{{ $inspection->technician }}</td>
                                            <td>{{ $inspection->customer }}<br>{{ $inspection->establishment_name }}</td>
                                            <td>{{ $inspection->address }} #{{ $inspection->address_number }},<br>{{ $inspection->colony }},<br>{{ $inspection->municipality }}, {{ $inspection->state }}</td>
                                            <td>
                                                @foreach($inspection->nodes as $node)
                                                    {{ $node->count }} {{ $node->name }}<br>
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $inspections->links() }}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
@section('personal-js')
    <script type="text/javascript" src="{{ URL::asset('js/build/stationMonitoring/inspections.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>//Select Job Center
        $('#selectInspectionStation').on('change', function() {
            let InspectionStation = $(this).val();
            window.location.href = route('index_inspections_monitoring', InspectionStation);
        });
        $(".applicantsList-single").select2();
    </script>
@endsection