<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL MONITOREO -->
<div class="modal fade" id="inspectionDetail<?php echo $a; ?>" tabindex="-1">
    <div class="modal-dialog modal-lg" style="width: 90%;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body modal-lg" style="width: 100%;">
                <h4 class="modal-title text-center text-info" id="modalTitle">Detalle de la Inspección</h4>
                <div class="row container">
                    <div class="col-md-6">
                        <label style="font-weight: bold; color: black;">NO. SERVICIO: </label> {{ $inspection->order }}
                        <br>
                        <label style="font-weight: bold; color: black;">NO.
                            MONITOREO: </label> {{ $inspection->id_monitoring }}
                        <br>
                        <label style="font-weight: bold; color: black;">TÉCNICO
                            APLICADOR: </label> {{ $inspection->technician }}
                    </div>
                    <div class="col-md-6 pull-right">
                        <label style="font-weight: bold; color: black;">FECHA: </label> {{ $inspection->date }}
                        <br>
                        <label style="font-weight: bold; color: black;">HORA: </label> {{ $inspection->hour }}
                    </div>
                </div>
                <div class="row" style="margin: 10px;">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-hover" id="tableProduct">
                            <thead>
                            <tr>
                                <th class="text-center">Zona</th>
                                <th class="text-center">Perímetro</th>
                                <th class="text-center">Tipo Estación</th>
                                <th class="text-center">Estación</th>
                                <th class="text-center">Actividad</th>
                                <th class="text-center">Plagas / Cantidad</th>
                                <th class="text-center">Condiciones</th>
                                <th class="text-center">Observaciones</th>
                                <th class="text-center">Acciones Correctivas</th>
                                <th class="text-center">Evidencias</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($inspection->stations as $station)
                                <tr>
                                    <td class="text-center">{{ $station->zone }}</td>
                                    <td class="text-center">{{ $station->perimeter }}</td>
                                    <td class="text-center">{{ $station->name }}</td>
                                    <td class="text-center">{{ $station->text }}</td>
                                    <td class="text-center">
                                        @if($station->value == "Sin Ingesta" || $station->value == "Sin Captura" || $station->value == "Captura Baja")
                                            <span style="color: green; font-weight: bold">{{ $station->value }}</span>
                                        @elseif($station->value == "Ingesta Parcial" || $station->value == "Captura Media")
                                            <span style="color: orange; font-weight: bold">{{ $station->value }}</span>
                                        @elseif($station->value == "Ingesta Total" || $station->value == "Captura Alta" || $station->value == "Captura Muy Alta")
                                            <span style="color: red; font-weight: bold">{{ $station->value }}</span>
                                        @else
                                            <span style="color: black; font-weight: bold">{{ $station->value }}</span>
                                        @endif
                                    </td>
                                    <td class="text-left">
                                        @if($station->plagues_count > 0)
                                            <ul>
                                                @foreach($station->plagues as $plague)
                                                    <li>{{ $plague->plague }}: {{ $plague->quantity_plague }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @foreach($station->conditions as $condition)
                                            {{ $condition->name }}
                                        @endforeach
                                    </td>
                                    <td class="text-center">{{ $station->actions }}</td>
                                    <td class="text-center">{{ $station->comments }}</td>
                                    <td class="text-center">
                                        @foreach($inspection->photos as $photo)
                                            @if($photo->id_station == $station->id_station)
                                                <div id="selectorDetailPhotoInspection" style="cursor: pointer">
                                                    <span class="item" data-src="{{ $photo->url_s3 }}">
                                                        <img src="{{ $photo->url_s3 }}" alt="station" width="50px" height="50px">
                                                    </span>
                                                </div>
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL MONITOREO -->