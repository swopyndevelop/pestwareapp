<!--START CREATE NEW AREA-->
<div class="container-fluid spark-screen">
    <div class="modal fade" id="newAreaMonitoringModal" role="dialog" aria-labelledby="newCashModal">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-context">
                        <div class="box">
                            <div class="modal-header">
                                <div class="box-header">
                                    <button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="modal-title col-md-12">
                                    <div class="col-md-7">
                                        <h4 class="text-right text-info" id="modalTitle">Nueva Área</h4>
                                    </div>
                                    <div class="col-md-5">
                                        <h4 class="text-right text-info" id="modalTitleFolio"><b class="text-right folioMonitoring">M-###</b></h4>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                        <select id="selectFilterCustomerName" class="applicantsList-single form-control header-table" style="width: 100%;">
                                            <option value="0" selected disabled>Buscar cliente...</option>
                                            @foreach($customers as $customer)
                                                <option value="{{ $customer->id }}" data-name="{{ $customer->name }}" data-establishment="{{ $customer->establishment_name }}">
                                                    {{ $customer->name }} / {{ $customer->establishment_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2 pull-right">
                                        <button type="button" class="btn btn-primary btn-sm" id="saveTreeStationMonitoring"><i class="glyphicon glyphicon-floppy-save"></i> Guardar Área</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="jsTreeAreaMonitoring" class="demo" style="margin-top:1em; min-height:200px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END CREATE NEW AREA-->
