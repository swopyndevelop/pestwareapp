<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Qrs</title>
</head>
<style>
    @page {
        margin: 8px;
    }
    body{
        font-family: 'Montserrat'
    }
</style>
<body>
{{--//Tables Qrs--}}
<div style="margin-right:3px; margin-left: 0;">
    <table style="width: 780px; text-align: center;">
        <thead>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                @foreach($row as $station)
                    <td style="font-size:8pt;">

                        <div style="margin-bottom: 0px; margin-top: 3px; border-style: dotted; width: 152px; height: 208px">

                            <img style="margin-bottom: 0px; margin-top: 5px" src="data:image/png;base64, {!! $station['qr'] !!}"><br>
                            <span style="color:black;font-size:5pt">Cliente: {{ $station['customer'] }}</span><br>
                            <span style="color:black;font-size:5pt;">Zona: {{ $station['zone'] }}</span><br>
                            <span style="color:black;font-size:5pt;">Perímetro: {{ $station['perimeter'] }}</span><br>
                            <span style="color:black;font-size:7pt">No. Estación: {{ $station['station_number'] }} </span><br>

                        </div>

                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>