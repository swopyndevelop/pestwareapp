@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Areas de Estaciones</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
<!--                                <a type="submit" class="btn btn-primary-dark" data-toggle="modal" href="#newAreaMonitoringModal">
                                    <i class="fa fa-home" aria-hidden="true"></i> Nueva Área
                                </a>-->
                                <a type="submit" class="openModalNewAreaV2 btn btn-primary-dark">
                                    <i class="fa fa-braille" aria-hidden="true"></i> Nueva Área
                                </a>
                            </div>
                            <div class="col-md-4 margin-mobile">
                                <select name="selectJobCenterStationArea" id="selectJobCenterStationArea" class="form-control" style="font-weight: bold;">
                                    @foreach($jobCenters as $jobCenter)
                                        @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                            <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                                {{ $jobCenter->name }}
                                            </option>
                                        @else
                                            <option value="{{ $jobCenter->id }}">
                                                {{ $jobCenter->name }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 margin-mobile">
                                {!!Form::open(['method' => 'GET','route'=>'index_station_monitoring'])!!}
                                {{ csrf_field() }}
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Buscar por: (Cliente)" name="search" id="search" value="">
                                    <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                </span>
                                </div>
                                {!!Form::close()!!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Monitoreo</th>
                                        <th>Fecha / Hora de Creación</th>
                                        <th>Cliente / Empresa</th>
                                        <th>Domicilio</th>
                                        <th># Estaciones</th>
                                        <th>Última Inspección</th>
                                        <th>QRs PDF</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($monitorings as $monitoring)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                                        {{ $monitoring->id_monitoring }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.stationmonitoring._menu')
                                                </div>
                                            </td>
                                            <td>{{ $monitoring->created_at }}<br>{{ $monitoring->user }}</td>
                                            <td>{{ $monitoring->customer }}<br>{{ $monitoring->establishment_name }}
                                                @if($monitoring->id_customer_branch != 0)
                                                    <br>
                                                    {{ 'Sucursal: ' . $monitoring->CustomerBranchName }}
                                                @endif
                                            </td>
                                            <td>
                                                @if($monitoring->id_customer_branch == 0)
                                                {{ $monitoring->address }} #{{ $monitoring->address_number }},
                                                <br>{{ $monitoring->colony }},
                                                <br>{{ $monitoring->municipality }}, {{ $monitoring->state }}
                                                @else
                                                {{ $monitoring->customerBranch->address }} #{{ $monitoring->customerBranch->address_number }},
                                                <br>{{ $monitoring->customerBranch->colony }},
                                                <br>{{ $monitoring->customerBranch->municipality }}, {{ $monitoring->customerBranch->state }}
                                                @endif
                                            </td>
                                            <td>
                                                @foreach($monitoring->nodes as $node)
                                                    {{ $node->count }} {{ $node->name }}<br>
                                                @endforeach
                                            </td>
                                            <td>
                                                @if($monitoring->lastInspection != null)
                                                    <a href="{{ route('index_inspections_monitoring') }}">{{ $monitoring->lastInspection->id_inspection }}</a><br>{{ $monitoring->lastInspection->created_at }}
                                                @else
                                                Sin Inspecciones
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($monitoring->status == 1)
                                                    <span class="label label-warning" data-toggle="tooltip"
                                                          data-placement="top"
                                                          title="Tu documento se esta generando y estará listo para descargar en unos minutos.">Generando PDF...</span>
                                                @endif
                                                @if($monitoring->url_pdf != null && $monitoring->diffInSeconds == false)
                                                    <a href="{{ Route('download_report_pdf_qrs', \Vinkla\Hashids\Facades\Hashids::encode($monitoring->id)) }}" target="_blank">
                                                        <i class="fa fa-file-pdf-o" aria-hidden="true" style=" font-size: 2.5em;color:red"></i>
                                                    </a>
                                                    <br>
                                                    <span data-toggle="tooltip"
                                                          data-placement="top"
                                                          title="Tu documento esta actualizado.">{{ $monitoring->date_updated_file }}</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $monitorings->links() }}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('vendor.adminlte.stationmonitoring._modalCreateArea')
    @include('vendor.adminlte.stationmonitoring._modalCreateAreaV2')
    @include('vendor.adminlte.stationmonitoring._modalUpdateArea')
    @include('vendor.adminlte.stationmonitoring._modalRelocateStation')
@endsection

@section('personal-js')
<script src ="{{ asset('/js/jstree-monitoring.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/build/stationMonitoring.js') }}"></script>
<script>$(".applicantsList-single").select2();</script>
@endsection
