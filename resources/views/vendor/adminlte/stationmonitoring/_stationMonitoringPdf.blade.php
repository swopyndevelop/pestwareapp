<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Reporte de Estaciones</title>
</head>
<style>
    @page {
        margin: 0;
    }
    body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
    }
    th, td {
        border-bottom: 1px solid black;
    }
</style>
<body>
<br>
<div style="margin: 0px; text-align: center">
    <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

        <div style="text-align: center; margin-bottom: 0px; margin-top: 15px;">
            <h2 style="font-weight: bold; margin-bottom: 0px;">MONITOREO DE ESTACIONES</h2>
        </div>

        <div style="width: 130px; display: inline-block; vertical-align: top;">
            @if($imagen->pdf_logo == null)
                <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
            @else
                <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="130px">
            @endif
        </div>

        <div style="width: 290px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
            <p style="margin-top: 0px; margin-bottom: 0">
                <span style="color:black;font-size:9.5pt">Responsable Sanitario: {{$jobCenterProfile->health_manager}} | Licencia No: {{$jobCenterProfile->license}}
                    | Whatsapp: {{$jobCenterProfile->whatsapp_personal}} | Teléfono: {{$jobCenterProfile->cellphone}} | Razón Social: {{ $jobCenterProfile->business_name }} |
                    {{ $jobCenterProfile->rfc_country }}: {{ $jobCenterProfile->rfc }}| @if($addressProfile) {{ $addressProfile->street }} #{{ $addressProfile->num_ext }}, {{ $addressProfile->location }},
                    {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. @endif | Email: {{ $jobCenterProfile->email_personal }} | Facebook: {{$jobCenterProfile->facebook_personal}}.
                </span>
            </p>
        </div>
        <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
            @if($qrcode != null)
                <img src="data:image/png;base64, {!! $qrcode !!}" width="100px"><br>
                <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Licencia Sanitaria</span>
            @endif
        </div>
        <div style="width: 212px; display: inline-block; margin-top: 0px; font-size: 8pt; margin-left: 10px; vertical-align: top;">
            <p style="margin-bottom: 2px; margin-top: 0px">
                <span style="font-size:8pt; font-weight: bold">NO. SERVICIO: </span>
                <span style="color:red;font-weight:bold;font-size:8pt;">{{ $inspection->id_service_order }}</span>
            </p>
            <p style="margin-top: 2px; margin-bottom: 2px">
                <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">NO. MONITOREO: </span>
                <span style="font-size:8pt;">{{ $inspection->id_monitoring }}</span>
            </p>
            <p style="margin-top: 2px; margin-bottom: 2px">
                <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">NO. INSPECCIÓN: </span>
                <span style="font-size:8pt;">{{ $inspection->id_inspection }}</span>
            </p>
            <p style="margin-top: 2px; margin-bottom: 2px">
                <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">FECHA: </span>
                <span style="font-size:8pt;">{{ $inspection->date }}</span>
            </p>
            <p style="margin-top: 2px; margin-bottom: 2px">
                <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">HORA DE ENTRADA: </span>
                <span style="font-size:8pt;">{{ $inspection->start_event }}</span>
            </p>
            <p style="margin-top: 2px; margin-bottom: 2px">
                <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">HORA DE SALIDA: </span>
                <span style="font-size:8pt;">{{ $inspection->final_event }}</span>
            </p>
            <p style="margin-top: 2px; margin-bottom: 2px">
                <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">TÉCNICO APLICADOR: </span>
                <span style="font-size:8pt;">{{ $inspection->technician }}</span>
            </p>
        </div>
    </div>
</div>
<br>
<div style="margin-right:20px;margin-left:20px;">

    {{--//Table Station Monitoring--}}
    <div style="">
        <p style="margin-top: 0px; margin-bottom: 7px">
            @if($jobCenterProfile->id == 291)
                <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/tca-logo.png" alt="logo" width="150px" height="40px">
                <br>
            @endif
            <span style="font-size:15pt; font-weight: bold"> Atención a {{$inspection->customer}}</span><br>
            <span style="font-size:10pt">
                @if($inspection->billing != null) {{$inspection->billing}} | @endif
                @if($inspection->establishment_name != null) {{$inspection->establishment_name}} | @endif
                @if($inspection->address != null) Dirección: {{$inspection->address}} @endif
                @if($inspection->municipality != null) {{$inspection->municipality}} @endif
                @if($inspection->cellphone != null) | Tel: {{$inspection->cellphone}} | @endif
                @if($inspection->email != null ) Email: {{$inspection->email}} @endif
        </span>
        </p>

        <div style="width: 750px; display: inline-block;border: 1px solid black; margin-top: 5px">

            <table style="width: 100%; text-align: center">
                <thead>
                <tr style="font-size: 9.5pt">
                    <th>Zona</th>
                    <th>Perímetro</th>
                    <th>Tipo Estación</th>
                    <th>Estación</th>
                    <th>Actividad</th>
                    <th>Plagas / Cantidad</th>
                    <th>Condiciones</th>
                    <th>Evidencias</th>
                    <th>Hora</th>
                </tr>
                </thead>
                <tbody style="font-size: 8pt">
                @foreach($stations as $station)
                    <tr>
                        <td>{{ $station->zone }}</td>
                        <td>{{ $station->perimeter }}</td>
                        <td>{{ $station->name }}</td>
                        <td>{{ $station->text }}</td>
                        <td>
                            @if($station->value == "Sin Ingesta" || $station->value == "Sin Captura" || $station->value == "Captura Baja")
                                <span style="color: green; font-weight: bold">{{ $station->value }}</span>
                            @elseif($station->value == "Ingesta Parcial" || $station->value == "Captura Media")
                                <span style="color: orange; font-weight: bold">{{ $station->value }}</span>
                            @elseif($station->value == "Ingesta Total" || $station->value == "Captura Alta" || $station->value == "Captura Muy Alta")
                                <span style="color: red; font-weight: bold">{{ $station->value }}</span>
                            @else
                                <span style="color: black; font-weight: bold">{{ $station->value }}</span>
                            @endif
                        </td>
                        <td style="text-align: left;">
                            @if($station->plagues_count > 0)
                            <ul>
                                @foreach($station->plagues as $plague)
                                    <li>{{ $plague->plague }}: {{ $plague->quantity_plague }}</li>
                                @endforeach
                            </ul>
                            @endif
                        </td>
                        <td>
                            @foreach($station->conditions as $condition)
                                {{ $condition->name }}
                            @endforeach
                        </td>
                        <td>
                            <br>
                            @foreach($images as $image)
                                @if($image->id_station == $station->id_station)
                                    <img src="{{ $image->url_s3 }}" alt="station" width="25px" height="25px">
                                @endif
                            @endforeach
                        </td>
                        <td>{{ $station->hour }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    {{--//Comments--}}
    <br>
    <div style="width: 750px;margin-top: 0px">
        <p style="font-size:9pt;font-weight:bold;color:black; border-bottom: 2px solid black; margin-bottom: 0px">Observaciones</p>
        <p style="font-size:9pt;border-bottom: 1px solid black; margin-top: 0px">
            @foreach($stations as $i => $station)
                @if ($station->comments != "")
                    <span style="font-weight:bold;color:black;">{{ $station->text }}: </span> <span>{{ $station->comments }}, </span>
                @endif
            @endforeach
        </p>
    </div>

    <div style="width: 750px;margin-top: 0px">
        <p style="font-size:9pt;font-weight:bold;color:black; border-bottom: 2px solid black; margin-bottom: 0px">Acciones Correctivas</p>
        <p style="font-size:9pt;border-bottom: 1px solid black; margin-top: 0px">
            @foreach($stations as $i => $station)
                @if ($station->actions != "")
                    <span style="font-weight:bold;color:black;">{{ $station->text }}: </span> <span>{{ $station->actions }}, </span>
                @endif
            @endforeach
        </p>
    </div>
    <br>
</div>
{{--//Footer--}}
<footer style="position: absolute; bottom: 0px;margin-right:20px;margin-left:20px;">
    @if($jobCenterProfile->id == 291)
        <div style="text-align: center">
            <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/conocer.jpg" alt="logo" width="100px" height="50px">
            <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/tif-sqf.png" alt="logo" width="100px" height="50px">
            <img src="https://pwa-public.s3.us-west-1.amazonaws.com/custom-certificates/eco-obregon/sedex.jpg" alt="logo" width="50px" height="50px">
        </div>
    @endif
    <div style="display: inline-block; text-align: center; width: 365px;">
            @if($customerFirm)
                <img src="{{ $firmUrl }}" alt="customerFirm" width="150px" height="50px"><br>
            @endif
            <span style="text-align:center;font-size:6pt;font-weight:bold">{{ $inspection->customer }}</span><br>
            @if($customerFirm)
                @if($customerFirm->other_name != null)
                <span style="text-align:center;font-size:6pt;font-weight:bold">{{$customerFirm->other_name}}</span><br>
                @endif
            @endif
            <span style="text-align:center;font-size:6pt;">@if($addressProfile) {{ $addressProfile->municipality }}, {{ $addressProfile->state }}. @endif <?php echo date("d/m/y",strtotime($date))?> a
                        las <?php echo date("H:i",strtotime($date))?></span>
            <div style="text-align:justify;line-height: 0.8em;font-size:5px">
                Firmo de conformidad y a mi entera satisfacción de haber recibido el Servicio de fumigación, acepto que se me
                explicaron las indicaciones que debo realizar antes de ingresar al lugar fumigado, así como las precauciones a tomar.
                Conozco los términos y funcionamiento de la garantía del servicio
            </div>
    </div>
    <div style="display: inline-block;text-align:center; width: 365px;">
        @if($firmUrlTechnician != null)
            <img src="{{ $firmUrlTechnician }}" alt="firmTechnician" width="150px" height="50px"><br>
        @endif
        <span style="text-align:center;font-size:6pt;">{{ $inspection->technician }}</span><br>
        <span style="text-align:center;font-size:6pt;font-weight:bold">Firma del técnico</span><br>
    </div>
</footer>
</body>
</html>
