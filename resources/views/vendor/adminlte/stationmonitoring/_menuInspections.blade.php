<ul class="dropdown-menu">
    <li><a data-toggle="modal" data-target="#inspectionDetail<?php echo $a; ?>" style="cursor: pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a></li>
    <li><a href="{{ route('station_monitoring_pdf', \Vinkla\Hashids\Facades\Hashids::encode($inspection->id_service_order)) }}" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar PDF</a></li>
    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
        <a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#sendEmailInspection"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($inspection->id_service_order) }}"
           data-email="{{ $inspection->email }}" @endif style="cursor: pointer">
            <i class="fa fa-envelope" aria-hidden="true" style="color: #222d32;"></i>Enviar Inspección
        </a>
    </li>
    <li @if(Auth::user()->id_plan == 1) data-toggle="tooltip" data-placement="bottom" title="Plan Emprendedor y Empresarial" @endif>
        <a @if(Auth::user()->id_plan != 1) data-toggle="modal" data-target="#sendEmailWhatsapp"
           data-id="{{ \Vinkla\Hashids\Facades\Hashids::encode($inspection->id_service_order) }}"
           data-urlinspection="{{ $inspection->urlWhatsappEmailInspection }}"
           data-cellphone="{{ $inspection->cellphone }}"
           data-cellphonemain="{{ $inspection->cellphone_main }}" @endif style="cursor: pointer">
            <i class="fa fa-whatsapp" aria-hidden="true" style="color: green;"></i>Enviar Inspección
        </a>
    </li>
</ul>
@include('vendor.adminlte.stationmonitoring._modalDetailInspection')
@include('vendor.adminlte.stationmonitoring._modalSendEmailInspection')
@include('vendor.adminlte.stationmonitoring._modalSendEmailWhatsapp')