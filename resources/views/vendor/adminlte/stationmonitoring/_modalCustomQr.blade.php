<!-- INICIA MODAL PARA MOSTRAR CUSTOM QR -->
<div class="modal fade" id="modalCustomQr" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Configurar QR</h4>
				<hr>
				<div class="row container-fluid">
					<div class="text-center">
						<h5>Control integral del Bajío (Empresa)</h5><br>

						<img src="{{ env('URL_STORAGE_FTP').\App\companie::find(auth()->user()->companie)->logo }}" width="30%" height="50px">
						<br><br>

						<div class="col-md-12">
							<div class="col-md-6 col-md-offset-3">

								<div class="row form-group">
									<div class="col-md-12 formulario__grupo" id="grupo__inputTitleCustomQr">
										<div class="">
											<input class="formulario__input form-control text-center" type="text" name="inputTitleCustomQr" id="inputTitleCustomQr" value="Estación de Control Titulo">
											<i class="formulario__validacion-estado fa fa-times-circle"></i>
										</div>
										<p class="formulario__input-error">El titulo debe tener de 3 a 250 caracteres.</p>
									</div>
								</div>

								<div class="row form-group">
									<div class="col-md-12 formulario__grupo" id="grupo__inputSubtitleCustomQr">
										<div class="">
											<input class="formulario__input form-control text-center" type="text" name="inputSubtitleCustomQr" id="inputSubtitleCustomQr" value="No alterar estación">
											<i class="formulario__validacion-estado fa fa-times-circle"></i>
										</div>
										<p class="formulario__input-error">El subtitulo debe tener de 3 a 250 caracteres.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<img src="{{ asset('img/customQrImage.png') }}" alt="QrMonitoring" width="300">
						</div>
					</div>
					<div class="text-center">
						<h5>Estación Cebadero</h5>
					</div>
					<div class="col-md-6">
						<div>
						<label class="text-bold-pwa">Cliente:</label><span> Empresa Prueba</span>
						</div>
						<div>
						<label class="text-bold-pwa">Perímetro: </label><span> Perímetro Prueba</span>
						</div>
					</div>
					<div class="col-md-6 text-right">
						<h4 class="text-bold-pwa"> No. Estación</h4>
						<h4 class="text-bold-pwa">1</h4>
					</div>

					<div class="text-right">
						<img src="{{ env('URL_STORAGE_FTP').\App\companie::find(auth()->user()->companie)->logo }}" width="30%" height="40px">
					</div>
				</div>
				<div class="modal-footer">
					<div class="text-center">

						<div class="formulario__mensaje" id="formulario__mensaje">
							<p><i class="fa fa-exclamation-triangle" id="formulario__mensaje"></i> <b>Error:</b> Por favor complete los datos correctamente. </p>
						</div>
						<br>
						<div class="text-center formulario__grupo formulario__grupo-btn-enviar">
							<button type="submit" class="btn btn-primary btn-md" id="saveBtnCustomQr">Guardar</button>
							<p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Se enviaron los datos correctamente!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR CUSTOM QR-->