<!-- INICIA MODAL PARA ELIMINAR MONITOREO -->
<div class="modal fade" id="deleteMonitoring<?php echo $monitoring->id; ?>" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <form action="{{ route('station_monitoring_tree_delete', ['id' => $monitoring->id]) }}" method="POST" id="form">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-body" style="text-align: center;">
                    <h4 class="modal-title">¿Está seguro de eliminar el monitoreo?</h4>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <div class="text-center">
                        <button type="submit" title="Eliminar Monitoreo" class="btn btn-primary" data-toggle="modal" data-target="#tracing<?php echo $monitoring->id; ?>"><strong>Aceptar</strong></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA ELIMINAR MONITOREO -->