<!-- INICIA MODAL PARA CAMBIAR CONTRASEÑA -->
<div class="modal fade" id="createReport" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="box-body text-center">
                <h4>Rango Reporte</h4>
                <input class="form-control" type="text" name="filterDateReportArea" id="filterDateReportArea">
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-primary" type="button" id="buttonGenerateReport"
                    name="buttonGenerateReport">Confirmar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA CAMBIAR CONTRASEÑA -->