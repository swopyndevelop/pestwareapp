@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="content container-fluid spark-screen">
        <div class="row">
            @include('adminlte::layouts.partials.session-messages')
            <div class="col-md-12">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Reportes de Clientes</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                <a type="submit" class="btn btn-primary-dark openModalNewAreaV2">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    Nueva Área
                                </a>
                            </div>
                        <div class="col-lg-5 margin-mobile">
                            <select name="selectJobCenterAreaMonitoring" id="selectJobCenterAreaMonitoring" class="form-control" style="font-weight: bold;">
                                @foreach($jobCenters as $jobCenter)
                                    @if($jobCenterSession->id_profile_job_center == $jobCenter->id)
                                        <option value="{{ $jobCenterSession->id_profile_job_center }}" selected>
                                            {{ $jobCenter->name }}
                                        </option>
                                    @else
                                        <option value="{{ $jobCenter->id }}">
                                            {{ $jobCenter->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-5 margin-mobile">
                            {!!Form::open(['method' => 'GET','route'=>'index_report_inspections_area'])!!}
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar por: (Cliente)" name="search" id="search" value="">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                </span>
                            </div>
                            {!!Form::close()!!}
                        </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th># Área</th>
                                        <th>Fecha / Hora de Creación</th>
                                        <th>Cliente / Empresa</th>
                                        <th>Domicilio</th>
                                        <th># Áreas</th>
                                        <th>Última Inspección</th>
                                        <th class="text-center">QRs PDF</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($a = 0)
                                    @foreach($customers as $customer)
                                        @php($a = $customer->id)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                                        CL-{{ $customer->id }} <span class="caret"></span>
                                                    </a>
                                                    @include('vendor.adminlte.reportsAreaMonitoring._menu')
                                                </div>
                                            </td>
                                            <td> <span class="text-primary">
                                                    {{ $customer->name }}
                                                </span>
                                            </td>
                                            <td>{{ $customer->establishment_name }}</td>
                                            <td>
                                                @if($customer->is_main == 0)
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>
                                                @else
                                                    <i class="fa fa-times text-danger" aria-hidden="true"></i>
                                                @endif
                                            </td>
                                            <td>{{--{{ $customer->quotationsCount }}--}}</td>
                                            <td>{{--{{ $customer->servicesOrderCount }}--}}</td>
                                            <td>
                                                td
                                            </td>
                                            <td>{{--{{ $customer->created_at->format('d-m-Y') }}--}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $customers->links() }}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
    @include('vendor.adminlte.reportsAreaMonitoring._modalReport')
@endsection

@section('personal-js')
            <script type="text/javascript" src="{{ URL::asset('js/build/reportAreaMonitoring.js') }}"></script>
            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection
