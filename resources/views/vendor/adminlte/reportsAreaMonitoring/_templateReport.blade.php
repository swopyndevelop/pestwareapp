<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Certificado de Servicio</title>
</head>
<style>
	@page {
		margin: 0;
    }
	body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
	}
    table {
        width: 100%;
        border-bottom: 1px solid black;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
        font-size: 10pt;
    }
    caption, td, th {
        padding: 0.3em;
    }
    th, td {
        border-bottom: 1px solid black;
        width: 25%;
    }
    caption {
        font-weight: bold;
        font-style: italic;
    }
</style>
<body>
        {{--//headers--}}
        <div style="margin: 0px; text-align: center">
            <div style="margin-bottom: 0px; text-align: left; padding-left: 20px; margin-top: -30px;">

                <div style="text-align: center; margin-bottom: 0px; margin-top: 15px;">
                    <h2 style="font-weight: bold; margin-bottom: 0px;">INSPECCIÓN DE ÁREAS CONTROLADAS</h2>
                </div>

                <div style="width: 130px; display: inline-block; vertical-align: top;">
                    @if($imagen->pdf_logo == null)
                        <img src="{{ env('URL_STORAGE_FTP')."pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg" }}" alt="logo" width="130px">
                    @else
                        <img src="{{ env('URL_STORAGE_FTP').$imagen->pdf_logo }}" alt="logo" width="130px">
                    @endif
                </div>

                <div style="width: 290px; display: inline-block; margin-top: 0px; text-align: justify; vertical-align: top; margin-right: 4px;">
                    <p style="margin-top: 0px; margin-bottom: 0">
                        <span style="color:black;font-size:9.5pt">
                            Responsable Sanitario: {{$jobCenterProfile->health_manager}} | Licencia No: {{$jobCenterProfile->license}}
                            | Tel: {{$jobCenterProfile->whatsapp_personal}} | Razón Social: {{ $jobCenterProfile->business_name }} | RFC: {{ $jobCenterProfile->rfc }} |
                            {{ $addressProfile->street }} #{{ $addressProfile->num_ext }}, {{ $addressProfile->location }},{{ $addressProfile->municipality }}, {{ $addressProfile->state }}. |
                            Email: {{ $jobCenterProfile->email_personal }} | Facebook: {{$jobCenterProfile->facebook_personal}}.
                        </span>
                    </p>
                </div>
                <div style="width: 110px; display: inline-block; margin-top: 5px; text-align: center; vertical-align: top;">
                    @if($qrcode != null)
                        <img src="data:image/png;base64, {!! $qrcode !!}" width="100px"><br>
                        <span style="font-size:6pt;font-weight:bold; margin-bottom: 0px">Licencia Sanitaria</span>
                    @endif
                </div>
                <div style="width: 212px; display: inline-block; margin-top: 0px; font-size: 8pt; margin-left: 10px; vertical-align: top;">
                    <p style="margin-bottom: 2px; margin-top: 0px">
                        <span style="font-size:8pt; font-weight: bold">NO. SERVICIO: </span>
                        <span style="color:red;font-weight:bold;font-size:8pt;">{{ $order->id_service_order }}</span>
                    </p>
                    <p style="margin-top: 2px; margin-bottom: 2px">
                        <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">NO. ÁREA: </span>
                        <span style="font-size:8pt;">{{ $order->id_area }}</span>
                    </p>
                    <p style="margin-top: 2px; margin-bottom: 2px">
                        <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">FECHA: </span>
                        <span style="font-size:8pt;">{{ $order->date_inspection }}</span>
                    </p>
                    <p style="margin-top: 2px; margin-bottom: 2px">
                        <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">HORA DE ENTRADA: </span>
                        <span style="font-size:8pt;">{{ $order->start_event }}</span>
                    </p>
                    <p style="margin-top: 2px; margin-bottom: 2px">
                        <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">HORA DE SALIDA: </span>
                        <span style="font-size:8pt;">{{ $order->final_event }}</span>
                    </p>
                    <p style="margin-top: 2px; margin-bottom: 2px">
                        <span style="font-size:8pt; margin-bottom: -15px; font-weight: bold">TÉCNICO RESPONSABLE: </span>
                        <span style="font-size:8pt;">{{ $order->technician }}</span>
                    </p>
                </div>
            </div>
        </div>
</body>
</html>