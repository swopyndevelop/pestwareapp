<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DevOps Pestware App</title>
</head>
<style>
        body{
            font-family: 'Open Sans', sans-serif
        }
        .down-img{
            position: absolute;
            display: block;
            left: 110px;
            align-content: center;
        }

    </style>
<body>
    <div class="container">
        <img src="{{ env('URL_STORAGE_FTP').'logos/8CbQmcXt9wB0xyZbP12VvCHsQedF42gBz3DBIGax.jpeg' }}" alt="banner">
    </div>
    <br><br>
    <div style="margin-left:20px;margin-right:20px">
        <br>
        <p style="font-size:25px;color:black;font-weight:bold;text-align:center">DevOps: Respaldo de base de datos generado correctamente.</p>
        <p style="font-size:20px;text-align:center;color:black;">
            El proceso de backup ha concluido satisfactoriamente:
            <br>
            {{ $data['path_backup'] }}
        </p>
        <hr>
    </div>
    <div style="margin-left:20px;margin-right:20px">
        <div class="down-img" style="align:center">
            <p>DevOps Pestware App | Copyright © 2023 | alberto.martinez@swopyn.com | +52 449 413 90 91</p>
            <br>
        </div>
    </div>
</body>
</html>