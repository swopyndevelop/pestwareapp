<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pestware App</title>
</head>
<style>
    body{
        font-family: 'Open Sans', sans-serif
    }
    .down-img{
        position: absolute;
        display: block;
        left: 110px;
        align-content: center;
    }

</style>
<body>
<div class="container">
    <img src="{{ env('URL_STORAGE_FTP').'logos/8CbQmcXt9wB0xyZbP12VvCHsQedF42gBz3DBIGax.jpeg' }}" alt="banner">
</div>
<br><br>
<div style="margin-left:20px;margin-right:20px">
    <br>
    <p style="font-size:25px;color:black;font-weight:bold;text-align:center">¡Hola! Estimado usuario de PestWare App</p>
    <p style="font-size:20px;text-align:center;color:black;">
        Has solicitado recuperar tu contraseña, para realizarlo has click en el enlace de abajo, en caso de no haber sido tu, ignora este correo y para mayor seguridad cambia tu contraseña actual.
        <br>
        ¡Mide, Controla y Mejora tu Negocio!
    </p>
    <br>
    <p style="font-size:15px;color:black;font-weight:bold;">Si fuiste tu, simplemente debes hacer click en el siguiente enlace:</p>
    <br>
    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;text-align:center">
        <tr>
            <td align="center" bgcolor="#19cca3" role="presentation" style="border:none;border-radius:6px;cursor:auto;padding:11px 20px;background:#1E8CC7;" valign="middle">
                <a href="{{ url('recover/password/show/form/' . $token) }}" style="background:#1E8CC7;color:#ffffff;font-family:Helvetica, sans-serif;font-size:18px;font-weight:600;line-height:120%;Margin:0;text-decoration:none;text-transform:none;" target="_blank">
                    Recuperar contraseña
                </a>
            </td>
        </tr>
    </table>
    <br>
</div>
<div style="margin-left:20px;margin-right:20px">
    <p style="text-align:center"><span style="font-size:25px;color:black;font-weight:bold;text-align:center">¿Tienes alguna duda?</span><br>
        <span style="font-size:17px;text-align:center;color:black;">Da click aquí para contactarnos: <br>
        </span></p>
    <div class="down-img" style="align:center">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="https://api.whatsapp.com/send?phone=524494139091" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/whatsapp.png')}}" alt="whatsapp" width="30%"></a>
        <a href="https://www.facebook.com/Pestware-App-104545547743003" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/facebook.png')}}" alt="facebook" width="30%"></a>
        <hr>
        <p>Pestware App | Copyright © 2021 | contacto@pestwareapp.com | +52 449 413 90 91</p>
        <br>
    </div>
</div>
</body>
</html>