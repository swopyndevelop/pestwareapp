<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pestware App</title>
</head>
<style>
        body{
            font-family: 'Open Sans', sans-serif
        }
        .down-img{
            position: absolute;
            display: block;
            left: 110px;
            align-content: center;
        }

    </style>
<body>
    <div class="container">
        <img src="{{ env('URL_STORAGE_FTP').'logos/8CbQmcXt9wB0xyZbP12VvCHsQedF42gBz3DBIGax.jpeg' }}" alt="banner">
    </div>
    <br><br>
    <div style="margin-left:20px;margin-right:20px">
        <br>
        <p style="font-size:25px;color:black;font-weight:bold;text-align:center">¡Hola! Estimado usuario de PestWare App</p>
        <p style="font-size:20px;text-align:center;color:black;">
            {{ $message }}
            <br>
            ¡Mide, Controla y Mejora tu Negocio!
        </p>
        <hr>
    </div>
    <div style="margin-left:20px;margin-right:20px">
        <p style="text-align:center"><span style="font-size:25px;color:black;font-weight:bold;text-align:center">¿Tienes alguna duda?</span><br>
            <span style="font-size:17px;text-align:center;color:black;">Da click aquí para contactarnos: <br>
        </span></p>
        <div class="down-img" style="align:center">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://api.whatsapp.com/send?phone=524494139091" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/whatsapp.png')}}" alt="whatsapp" width="30%"></a>
            <a href="https://www.facebook.com/Pestware-App-104545547743003" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/facebook.png')}}" alt="facebook" width="30%"></a>
            <hr>
            <p>Pestware App | Copyright © 2021 | contacto@pestwareapp.com | +52 449 413 90 91</p>
            <br>
        </div>
    </div>
</body>
</html>