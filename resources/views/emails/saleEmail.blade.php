<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Venta e-mail</title>
</head>
<style>
    body{
        font-family: 'Open Sans', sans-serif
    }
</style>
<body>
<div class="container">
    @if($personalMail->banner != null)
        <img src="{{ env('URL_STORAGE_FTP').$personalMail->banner }}" alt="banner">
    @else
        <img src="https://storage.pestwareapp.com/logos/8CbQmcXt9wB0xyZbP12VvCHsQedF42gBz3DBIGax.jpeg" alt="banner">
    @endif
</div>
<br><br>
<div style="margin-left:20px;margin-right:20px">
    <br>
    <p style="font-size:25px;color:red;font-weight:bold;text-align:center">
        {{$sale->customer_name}}
    </p>
    <p style="font-size:20px;text-align:center;color:black;">
        {{$personalMail->description}}
        <br>
        A continuación encontrarás adjunto el reporte del servicio solicitado.
    </p>
</div>
<div style="margin-left:20px;margin-right:20px">
    <p style="text-align:center"><span style="font-size:25px;color:black;font-weight:bold;text-align:center">¿Tienes alguna duda?</span><br>
        <span style="font-size:17px;text-align:center;color:black;">Da click aquí para contactarnos: <br>
        </span>
    </p>
    <div style="text-align: center">
        @if($personalMail->image_whatsapp != null)
            <a href="{{$personalMail->image_whatsapp}}" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/whatsapp.png')}}" width="15%"></a>
        @endif
        @if($personalMail->image_messenger != null)
            <a href="{{$personalMail->image_messenger}}" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/facebook.png')}}" alt="" width="15%"></a>
        @endif
    </div>
</div>
</body>
</html>