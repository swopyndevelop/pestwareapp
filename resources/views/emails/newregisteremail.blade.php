<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro de Nueva Empresa</title>
</head>
<style>
        body{
            font-family: 'Open Sans', sans-serif
        }
        .down-img{
            position: absolute;
            display: block;
            left: 110px;
            align-content: center;
        }
        table, th, td {
            border: 1px solid black;
        }

    </style>
<body>
    <div class="container">
        <img src="{{ asset('/img/pestware.jpeg') }}" alt="" width="100px" height="100px">
    </div>
    <br><br>
    <div style="margin-left:20px;margin-right:20px">
        <p style="font-size:25px;font-style:italic;color:purple;text-align:center">Se ha registrado una <br>nueva<span style="font-weight:bold"> Empresa.</span></p>
        <br>
        <p style="font-size:25px;color:purple;font-weight:bold;text-align:center">Datos de Contacto:</p>
        <hr>
        <h3><b>Nombre de la Empresa: </b>{{ $data['username'] }}</h3>
        <h3><b>Nombre del Contacto: </b>{{ $data['contact'] }}</h3>
        <h3><b>País: </b>{{ $data['country'] }}</h3>
        <h3><b>Teléfono: </b>{{ $data['phone'] }}</h3>
        <h3><b>Correo electrónico: </b>{{ $data['email'] }}</h3>
        <h3>----------------------------------------------------------------------------------</h3>
    </div>
</body>
</html>