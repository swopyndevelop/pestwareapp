<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bienvenido a Pestware App</title>
</head>
<style>
        body{
            font-family: 'Open Sans', sans-serif
        }
        .down-img{
            position: absolute;
            display: block;
            left: 110px;
            align-content: center;
        }

    </style>
<body>
    <div class="container">
        <img src="{{ env('URL_STORAGE_FTP').'logos/8CbQmcXt9wB0xyZbP12VvCHsQedF42gBz3DBIGax.jpeg' }}" alt="banner">
    </div>
    <br><br>
    <div style="margin-left:20px;margin-right:20px">
        <br>
        <p style="font-size:25px;color:black;font-weight:bold;text-align:center">¡Hola! {{ $contact->name }}, Bienvenido a PestWare App</p>
        <p style="font-size:20px;text-align:center;color:black;">
            Con Pestware App podrás tener toda la administración de tu negocio en la palma de tu mano, todos tus procesos estarán digitalizados
            e integrados en un solo lugar con el Software para Empresas de Control de Plagas más sencillo y amigable.
            <br>
            ¡Mide, Controla y Mejora tu Negocio!
        </p>
        <hr>
        <p style="font-size:20px;color:red;font-weight:bold;text-align:center">Por favor confirma tu correo electrónico.</p>
        <br>
        <p style="font-size:15px;color:black;font-weight:bold;">Para ello simplemente debes hacer click en el siguiente enlace:</p>
        <br>
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;text-align:center">
            <tr>
                <td align="center" bgcolor="#19cca3" role="presentation" style="border:none;border-radius:6px;cursor:auto;padding:11px 20px;background:#1E8CC7;" valign="middle">
                    <a href="{{ url('register/verify/' . $token) }}" style="background:#1E8CC7;color:#ffffff;font-family:Helvetica, sans-serif;font-size:18px;font-weight:600;line-height:120%;Margin:0;text-decoration:none;text-transform:none;" target="_blank">
                        Click para confirmar cuenta
                    </a>
                </td>
            </tr>
        </table>
        <hr>
        <h3><b>Nombre de la Empresa: </b>{{ $companie }}</h3>
        <h3><b># de Cliente: </b>PWA-{{ $companieId }} (Para cualquier requerimeinto de soporte se te solicitará tu # cliente.)</h3>
        <h3><b>Nombre del Contacto: </b>{{ $contact->name }}</h3>
        <h3><b>Teléfono: </b>{{ $contact->phone }}</h3>
        <h3><b>Correo electrónico: </b>{{ $contact->email }}</h3>
        <h3><b>Acceso Portal de Clientes: </b>https://portal.pestwareapp.com/</h3>
        <h3><b>Credenciales Portal de Clientes: </b>Tus clientes podrán ingresar con su número de teléfono y la contraseña por default: PestWareApp2021@</h3>
        <h3>** Es importante que por motivos de seguridad recomiendes a tus clientes cambiar su contraseña después del primer ingreso al portal **</h3>
    </div>
    <div style="margin-left:20px;margin-right:20px">
        <p style="text-align:center"><span style="font-size:25px;color:black;font-weight:bold;text-align:center">¿Tienes alguna duda?</span><br>
            <span style="font-size:17px;text-align:center;color:black;">Da click aquí para contactarnos: <br>
        </span></p>
        <div class="down-img" style="align:center">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://api.whatsapp.com/send?phone=524494139091" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/whatsapp.png')}}" alt="whatsapp" width="30%"></a>
            <a href="https://www.facebook.com/Pestware-App-104545547743003" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/facebook.png')}}" alt="facebook" width="30%"></a>
            <hr>
            <p>Pestware App | Copyright © 2021 | contacto@pestwareapp.com | +52 449 413 90 91</p>
            <br>
        </div>
    </div>
</body>
</html>