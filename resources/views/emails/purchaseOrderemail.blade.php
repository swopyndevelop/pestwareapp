<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cotización e-mail</title>
</head>
<style>
        body{
            font-family: 'Open Sans', sans-serif
        }
        .down-img{
            display: block;
            align-content: center;
        }

    </style>
<body>
    <div class="container">
        <img src="{{ env('URL_STORAGE_FTP').$banner->banner }}" alt="banner">
    </div>
    <br><br>
    <div style="margin-left:20px;margin-right:20px">
{{--        <p style="font-size:25px;font-style:italic;color:black;text-align:center">PROTEGEMOS A LOS QUE MÁS <br>QUIERES...<span style="font-weight:bold"> SIEMPRE</span></p>--}}
        <br>
        <p style="font-size:25px;color:black;font-weight:bold;text-align:center">{{$purchaseOrder->proveedor}}</p>
    </div>
    <div style="margin-left:20px;margin-right:20px">
        <p style="text-align:center"><span style="font-size:25px;color:black;font-weight:bold;text-align:center">¿Tienes alguna duda?</span><br>
        <span style="font-size:17px;text-align:center;color:black;">Da click aquí para contactarnos: <br>
        </span></p>
        <div class="down-img" style="align:center">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{{$whatsapp}}" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/whatsapp.png')}}" width="30%" ></a>
            <a href="{{$banner->image_messenger}}" target="_blank" style="text-align:center;"><img src="{{asset('img/mail/facebook.png')}}" alt="" width="30%"></a>
        </div>
    </div>  
</body>
</html>