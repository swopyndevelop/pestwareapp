<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>App Service Log</title>
</head>
<style>
    body{
        font-family: 'Open Sans', sans-serif
    }
    .down-img{
        position: absolute;
        display: block;
        left: 110px;
        align-content: center;
    }

</style>
<body>
<div class="container">
    <img src="{{ asset('/img/pestware.jpeg') }}" alt="" width="100px" height="100px">
</div>
<br><br>
<div style="margin-left:20px;margin-right:20px">
    <p style="font-size:25px;font-style:italic;color:#114f5c;text-align:center"><span style="font-weight:bold"> Service: [{{ $serviceData->id }}]  {{ $serviceData->title }}</span></p>
    <br>
    <p style="font-size:25px;color:#114f5c;font-weight:bold;text-align:center">Service Data Json:</p>
    <pre style="color: #2b2b2b">{{ $json }}</pre>
    <br>
    <a href="https://jsoneditoronline.org/">JSON Editor Online</a>
    <hr>
    <h3><b>Technical: </b>{{ $employee }}</h3>
    <h3><b>Company: </b>{{ $company }}</h3>
    <h3><b>Service Id: </b>{{ $serviceData->id_service_order }}</h3>
    <h3><b>Date: </b>{{ $serviceData->initial_date }}</h3>
    <h3><b>Initial Hour: </b>{{ $serviceData->initial_hour }}</h3>
    <hr>
    <br>
    <!--Padded button-->
    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;">
        <tr>
            <td align="center" bgcolor="#19cca3" role="presentation" style="border:none;border-radius:6px;cursor:auto;padding:11px 20px;background:white;" valign="middle">
                <p style="font-size:25pt;color:#103939;text-align:center">Data log:</p>
                <p><b>Id: </b>{{ $log->id }}</p>
                <p><b>Error message: </b>{{ $error }}</p>
                <p><b>Created at: </b>{{ $log->created_at }}</p>
            </td>
        </tr>
    </table>
    <br><br>
</div>
</body>
</html>