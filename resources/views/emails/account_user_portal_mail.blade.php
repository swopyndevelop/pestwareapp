<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @if($type == null)<title>Cuenta de PestWareApp</title>@else<title>Portal de clientes</title>@endif

</head>
<style>
    body{
        font-family: 'Open Sans', sans-serif
    }
    .down-img{
        position: absolute;
        display: block;
        left: 110px;
        align-content: center;
    }

</style>
<body>
<div class="container">
    <img src="{{ asset('/img/pestware.jpeg') }}" alt="" width="100px" height="100px">
</div>
<br><br>
<div style="margin-left:20px;margin-right:20px">
    <p style="font-size:25px;font-style:italic;color:purple;text-align:center"><span style="font-weight:bold"> Bienvenido a PestWare App</span></p>
    <br>
    <p style="font-size:20px;text-align:center;color:black;">
        Con Pestware App podrás tener toda la administración de tu negocio en la palma de tu mano, todos tus procesos estarán digitalizados
        e integrados en un solo lugar con el Software para Empresas de Control de Plagas más sencillo y amigable.
        <br>
        ¡Mide, Controla y Mejora tu Negocio!
    </p>
    <br>
    <p style="font-size:25px;color:purple;font-weight:bold;text-align:center">
        @if($type == null) Cuenta de PestWareApp @else Portal de clientes @endif
    </p>
    @if($type != null)
    <p>Aquí tienes la información de tu cuenta de acceso al portal de clientes, en donde encontraras un calendario con
    todos tus servicios programados y realizados, así como descarga de certificados, carpeta MIP y otra información.</p>
    @endif
    <hr>
    <p style="font-size:20px;color:red;font-weight:bold;text-align:center">Por favor confirma tu correo electrónico.</p>
    <br>
    <p style="font-size:15px;color:black;font-weight:bold;">Para ello simplemente debes hacer click en el siguiente enlace:</p>
    <br>
    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;text-align:center">
        <tr>
            <td align="center" bgcolor="#19cca3" role="presentation" style="border:none;border-radius:6px;cursor:auto;padding:11px 20px;background:#1E8CC7;" valign="middle">
                <a href="{{ url('register/verify/' . $token) }}" style="background:#1E8CC7;color:#ffffff;font-family:Helvetica, sans-serif;font-size:18px;font-weight:600;line-height:120%;Margin:0;text-decoration:none;text-transform:none;" target="_blank">
                    Click para confirmar cuenta
                </a>
            </td>
        </tr>
    </table>
    <hr>
    <h3><b>Usuario: </b>{{ $user }}</h3>
    <h3><b>Contraseña temporal: </b>{{ $password }}</h3>
    <br>
    <br>
    <p style="font-size:25pt;color:purple;text-align:center">¡Gracias por tu confianza!</p>
</div>
</body>
</html>