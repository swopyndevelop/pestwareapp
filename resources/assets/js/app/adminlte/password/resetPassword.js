import {loaderPestWareLogin} from "../pestware/loaderLogin";
import {expressionsAuth, setFieldsAuth, validateFieldAuth} from "../auth/validations";

$(document).ready(function() {

    $('#formResetPassword').on('submit', function(event) {
        event.preventDefault();
        const passwordNew = document.getElementById('passwordNew').value;
        const passwordConfirm = document.getElementById('passwordConfirm').value;
        const token = location.pathname.split('/').pop();

        if (passwordNew.trim() !== passwordConfirm.trim() || passwordNew.trim().length < 8 || passwordConfirm.trim().length < 8) {
            createErrors();
            showError('Las contraseñas no coinciden y/o deben ser de mínimo 8 caracteres.')
        } else resetPassword(passwordNew.trim(), passwordConfirm.trim(), token);
    });

    function resetPassword(passwordNew, passwordConfirm, token) {
        loaderPestWareLogin('');
        $.ajax({
            type: 'POST',
            url: '/reset/password/token',
            data: {
                _token: $("meta[name='csrf-token']").attr("content"),
                passwordNew: passwordNew,
                passwordConfirm: passwordConfirm,
                tokenAccount: token
            },
            success: function(data) {
                Swal.close();
                if (data.code === 200) {
                    Swal.fire({
                        type: 'success',
                        title: 'Contraseña Cambiada',
                        text: data.message
                    })
                    setTimeout(() => {
                        window.location.href = '/instance/login';
                    }, 3000);
                }
                else {
                    createErrors();
                    showError(data.message);
                }
            }
        });
    }

    function createErrors() {
        $('#message-errors').remove();
        let div = `<div class="alert-danger" id="message-errors"></div>`;
        $('#errors').append(div);
    }

    function showError(message) {
        $('#message-errors').append(`<ul><li>${message}</li></ul>`);
        setTimeout(() => {
            $('#message-errors').remove();
        }, 5000);
    }

    // Validations input event
    const inputPasswordNew = document.getElementById('passwordNew');
    const inputPasswordConfirm = document.getElementById('passwordConfirm');

    const fieldsResetForm = {
        inputPasswordNew: false,
        inputPasswordConfirm: false
    }

    setFieldsAuth(fieldsResetForm);

    const validateFormReset = (e) => {
        switch (e.target.name) {
            case "passwordNew":
                validateFieldAuth(expressionsAuth.password, e.target.value, 'inputPasswordNew', 'iconPasswordNew');
                break;
            case "passwordConfirm":
                validateFieldAuth(expressionsAuth.password, e.target.value, 'inputPasswordConfirm', 'iconPasswordConfirm');
                break;
        }
    }
    inputPasswordNew.addEventListener('keyup', validateFormReset);
    inputPasswordNew.addEventListener('blur', validateFormReset);

    inputPasswordConfirm.addEventListener('keyup', validateFormReset);
    inputPasswordConfirm.addEventListener('blur', validateFormReset);

});