/**
 * @author Alberto Martínez
 * @version 04/12/2020
 * tree/_modalAddressJobCenter.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    $("#saveAddressDataJobCenter").click(function() {

        let id = document.getElementById('jobCenterId').value;
        let street = document.getElementById('street').value;
        let num_ext = document.getElementById('num_ext').value;
        let locationColony = document.getElementById('location').value;
        let municipality = document.getElementById('municipality').value;
        let state = document.getElementById('state').value;
        let zip_code = document.getElementById('zip_code').value;

        if (street.length < 1) showToast('warning','Calle','Debe Ingresar la Calle');
        else if (num_ext.length < 1) showToast('warning','Número','Debe Ingresar el Número');
        else if (locationColony.length < 1) showToast('warning','Colonia','Debe Ingresar la Colonia');
        else if (municipality.length < 1) showToast('warning','Municipio','Debe Ingresar el Municipio');
        else if (state.length < 1) showToast('warning','Estado','Debe Ingresar el Estado');
        else if (zip_code.length < 1) showToast('warning','Código Postal','Debe Ingresar el Código Postal');
        else {
            loaderPestWare('Actualizando Domicilio...');
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                }
            });
            $.ajax({
                type: "POST",
                url: route('data_job_center_address_update'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    jobCenterId: id,
                    street: street,
                    zip_code: zip_code,
                    num_ext: num_ext,
                    municipality: municipality,
                    state: state,
                    locationColony: locationColony
                },
                success: function (data) {
                    if (data.code === 500) {
                        Swal.close();
                        showToast('error', data.message, 'Algo salio mal, intente de nuevo');
                    } else {
                        Swal.close();
                        showToast('success', 'Sucursal', data.message);
                        $("#saveAddressDataJobCenter").attr('disabled', true);
                        location.reload();
                    }
                }
            });
        }
    });


});