/**
 * @author Alberto Martínez
 * @version 04/12/2020
 * tree/_modalAddressJobCenter.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    $("#saveAddressJobCenter").click(function() {

        let id = document.getElementById('jobCenterId').value;
        let street = document.getElementById('street').value;
        let zip_code = document.getElementById('zip_code').value;
        let num_ext = document.getElementById('num_ext').value;
        let municipality = document.getElementById('municipality').value;
        let state = document.getElementById('state').value;
        let location = document.getElementById('location').value;

        loaderPestWare('Actualizando Domicilio...');
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: "POST",
            url: route('jobcenter_address_store'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                jobCenterId: id,
                street: street,
                zip_code: zip_code,
                num_ext: num_ext,
                municipality: municipality,
                state: state,
                location: location
            },
            success: function(data) {
                if (data.code === 500) {
                    Swal.close();
                    showToast('error', data.message, 'Algo salio mal, intente de nuevo');
                } else {
                    Swal.close();
                    showToast('success-redirect', data.message, 'Centro de trabajo actualizado.', 'tree_jobcenter');
                }
            }
        });
    });

});