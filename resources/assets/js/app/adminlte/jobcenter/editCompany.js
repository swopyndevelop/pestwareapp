/**
 * @author Alberto Martinez | Manuel Mendoza
 * @version 13/04/2021
 * register/index.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function () { // on load
    let jobCenterId = $('#jobCenterId').val();

    $("#buttonCompanySave").click(function() {
        let health_manager = $('#health_manager').val();
        let business_name = $('#business_name').val();
        let email_personal = $('#email_personal').val();
        let rfc = $('#rfc').val();
        let license = $('#license').val();
        let facebook_personal = $('#facebook_personal').val();
        let web_page = $('#web_page').val();
        let messenger_personal = $('#messenger_personal').val();
        let whatsapp_personal = $('#whatsapp_personal').val();
        let cellphoneGeneral = $('#cellphoneGeneral').val();
        let warning_service = $('#warning_service').val();
        let contract_service = $('#contract_service').val();
        if(rfc.length === 0) showToast('warning','Sucursal','Ingresar el RFC de la compañia');
        else if(health_manager.length > 250) showToast('warning','Sucursal','Responsable Sanitario No mayor a 250 carácteres.');
        else if(business_name.length > 250) showToast('warning','Sucursal','Razón Social No mayor a 250 carácteres.');
        else if(email_personal.length > 250) showToast('warning','Sucursal','Email No mayor a 250 carácteres.');
        else if(rfc.length > 250) showToast('warning','Sucursal','RFC No mayor a 250 carácteres.');
        else if(license.length > 250) showToast('warning','Sucursal','Licencia No. mayor a 250 carácteres.');
        else if(facebook_personal.length > 250) showToast('warning','Sucursal','Facebook No mayor a 250 carácteres.');
        else if(web_page.length > 250) showToast('warning','Sucursal','Facebook No mayor a 250 carácteres.');
        else if(messenger_personal.length > 250) showToast('warning','Sucursal','Messenger No mayor a 250 carácteres.');
        else if(whatsapp_personal.length > 250) showToast('warning','Sucursal','Whatsapp No mayor a 250 carácteres.');
        else if(warning_service.length > 5000) showToast('warning','Sucursal','Precauciones No mayor a 5000 carácteres.');
        else if(contract_service.length > 5000) showToast('warning','Sucursal','Contrato No mayor a 5000 carácteres.');
        else saveCompanyBranch(health_manager,business_name,email_personal,rfc,license,facebook_personal, web_page,
            messenger_personal,whatsapp_personal, cellphoneGeneral,warning_service,contract_service);
    });

    function saveCompanyBranch(health_manager,business_name,email_personal,rfc,license,facebook_personal, web_page,
                               messenger_personal,whatsapp_personal, cellphoneGeneral,warning_service,contract_service){
        loaderPestWare('Guardando Sucursal');
        $.ajax({
            type: 'POST',
            url: route('data_job_center_company_update'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                jobCenterId: jobCenterId,
                health_manager: health_manager,
                business_name: business_name,
                email_personal: email_personal,
                rfc: rfc,
                license: license,
                facebook_personal: facebook_personal,
                web_page: web_page,
                messenger_personal: messenger_personal,
                whatsapp_personal: whatsapp_personal,
                cellphoneGeneral: cellphoneGeneral,
                warning_service: warning_service,
                contract_service: contract_service
            },
            success: function (data) {
                if (data.errors){
                    Swal.close();
                    showToast('error', 'Error', data.message);
                }
                else{
                    Swal.close();
                    showToast('success', 'Sucursal', data.message);
                    location.reload();
                }
            }
        });
    }

    $("#buttonTaxSave").click(function() {

        let ivaProfileJobCenter = document.getElementById('ivaProfileJobCenter').value;

        loaderPestWare('Actualizando Impuestos...');
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: "POST",
            url: route('data_job_center_tax_update'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                jobCenterId: jobCenterId,
                ivaProfileJobCenter: ivaProfileJobCenter,
            },
            success: function(data) {
                if (data.code === 500) {
                    Swal.close();
                    showToast('error', data.message, 'Algo salio mal, intente de nuevo');
                } else {
                    Swal.close();
                    showToast('success', 'Sucursal', data.message);
                    location.reload();
                }
            }
        });
    });

    $("#buttonSsidSave").click(function() {

        let ssidProfileJobCenter = document.getElementById('ssid').value;

        loaderPestWare('Actualizando Datos...');
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: "POST",
            url: route('data_job_center_ssid_update'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                jobCenterId: jobCenterId,
                ssidProfileJobCenter: ssidProfileJobCenter,
            },
            success: function(data) {
                if (data.code === 500) {
                    Swal.close();
                    showToast('error', data.message, 'Algo salio mal, intente de nuevo');
                } else {
                    Swal.close();
                    showToast('success', 'Sucursal', data.message);
                    location.reload();
                }
            }
        });
    });

    $("#buttonLabelsSave").click(function() {

        let street =  document.getElementById('streetLabel').value;
        let colony = document.getElementById('colonyLabel').value;
        let state = document.getElementById('stateLabel').value;
        let iva =  document.getElementById('ivaLabel').value;
        let streetId =  document.getElementById('streetId').value;
        let colonyId = document.getElementById('colonyId').value;
        let stateId = document.getElementById('stateId').value;
        let ivaId = document.getElementById('ivaId').value;
        let rfcCountryProfileJobCenter = document.getElementById('rfcCountryProfileJobCenter').value;

        loaderPestWare('Actualizando etiquetas...');
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: "POST",
            url: route('data_job_center_label_update'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                jobCenterId: jobCenterId,
                street: street,
                colony: colony,
                state: state,
                iva: iva,
                streetId: streetId,
                colonyId: colonyId,
                stateId: stateId,
                ivaId: ivaId,
                rfcCountryProfileJobCenter: rfcCountryProfileJobCenter
            },
            success: function(data) {
                if (data.code === 500) {
                    Swal.close();
                    showToast('error', 'Algo salio mal, intente de nuevo',data.message);
                } else {
                    Swal.close();
                    showToast('success', 'Sucursal', data.message);
                    location.reload();
                }
            }
        });
    });

    // Validar archivo
    const imgBanner = document.getElementById('banner');
    imgBanner.addEventListener('change', (event) => {
        checkFile(event);
    });

    // Validate Img
    function checkFile(e) {

        const fileList = e.target.files;
        let error = false;
        let defaultImage = 'https://www.gravatar.com/avatar/8dd9506c61dfc3c43d1100df4fe21035.jpg?s=80&d=mm&r=g';
        let image = document.getElementById('imgBanner');
        for (let i = 0, file; file = fileList[i]; i++) {

            let sFileName = file.name;
            let sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            let iFileSize = file.size;
            // let iConvert = (file.size / 1048576).toFixed(2);

            if (!(sFileExtension === "png" ||
                sFileExtension === "jpg" ||
                sFileExtension === "jpeg") || iFileSize > 10485760) { /// 10 mb
                error = true;
            }
        }
        if (error) {
            missingText("El archivo ingresado no es aceptado o su tamaño excede lo aceptado");
            document.getElementById("banner").value = "";
            image.src = defaultImage;
        }
        if (!error) {
            image.src = URL.createObjectURL(e.target.files[0]);
        }
    }

});