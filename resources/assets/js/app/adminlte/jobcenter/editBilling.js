/**
 * @author Alberto Martinez | Manuel Mendoza
 * @version 13/04/2021
 * register/index.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function () {
    let jobCenterId = $('#jobCenterId').val();
    let rfcBilling, ivaBilling, emailBilling, addressBilling, addressNumberBilling, colonyBilling, municipalityBilling,
        stateBilling, postalCodeBilling, passwordBilling, regimenFiscal, textMail;

    // Load data selects.
    getCatalogsRegimensSAT();
    getJobCentersFolios();

    $("#buttonBillingSave").click(function() {
        rfcBilling = $('#rfcBilling').val();
        ivaBilling = $('#ivaBilling').val();
        emailBilling = $('#emailBilling').val();
        passwordBilling = $('#passwordBilling').val();
        addressBilling = $('#addressBilling').val();
        addressNumberBilling = $('#addressNumberBilling').val();
        colonyBilling = $('#colonyBilling').val();
        municipalityBilling = $('#municipalityBilling').val();
        stateBilling = $('#stateBilling').val();
        postalCodeBilling = $('#postalCodeBilling').val();
        regimenFiscal = $('#regimeFiscal option:selected').val();
        textMail = $('#descriptionBillingEmail').val();

        let validateEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/; // validate mail

        if(ivaBilling.length === 0) showToast('warning','Sucursal','Ingresar el IVA de la facturación');
        else if(emailBilling.length === 0) showToast('warning','Sucursal','Ingresar el Correo de la facturación');
        else if(!validateEmail.test(emailBilling)) showToast('warning','Sucursal','El correo no cumple con el formato requerido de la facturación');
        else if(addressBilling.length === 0) showToast('warning','Sucursal','Ingresar la Calle de la facturación');
        else if(addressNumberBilling.length === 0) showToast('warning','Sucursal','Ingresar el Número de la facturación');
        else if(colonyBilling.length === 0) showToast('warning','Sucursal','Ingresar la Colonia de la facturación');
        else if(municipalityBilling.length === 0) showToast('warning','Sucursal','Ingresar el Municipio de la facturación');
        else if(stateBilling.length === 0) showToast('warning','Sucursal','Ingresar el Estado de la facturación');
        else if(postalCodeBilling.length === 0) showToast('warning','Sucursal','Ingresar el Código Postal de la facturación');
        else if(postalCodeBilling.length > 5) showToast('warning','Sucursal','El Código Postal no cumple con el formato requerido de la facturación');
        else if(regimenFiscal === 0) showToast('warning','Sucursal','Debes seleccionar un Régimen Fiscal.');
        else saveBilling();
    });

    function saveBilling(){
        loaderPestWare('Guardando...');
        $.ajax({
            type: 'POST',
            url: route('data_job_center_billing'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                jobCenterId: jobCenterId,
                rfcBilling: rfcBilling,
                ivaBilling: ivaBilling,
                emailBilling: emailBilling,
                passwordBilling: passwordBilling,
                addressBilling: addressBilling,
                addressNumberBilling: addressNumberBilling,
                colonyBilling: colonyBilling,
                municipalityBilling: municipalityBilling,
                stateBilling: stateBilling,
                postalCodeBilling: postalCodeBilling,
                regimenFiscal: regimenFiscal,
                textMail: textMail
            },
            success: function (data) {
                Swal.close();
                if (data.code == 500) showToast('error', 'Error', data.message);
                else {
                    showToast('success', 'Datos Guardados', data.message);
                    document.getElementById('panelStep2').style.display = "block";
                    document.getElementById('divStatusConnectionSat').style.display = "none";
                    document.getElementById('validateSat').style.display = "block";
                }
            }
        });
    }

    $("#validateSat").click(function () {
        let validateRfc = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/;
        rfcBilling = $('#rfcBilling').val();
        passwordBilling = $('#passwordBilling').val();
        if(rfcBilling.length === 0) showToast('warning','Facturación','Ingresar el RFC del emisor.');
        else if(passwordBilling.length === 0) showToast('warning','Facturación','Debes ingresar la contraseña de la llave privada.');
        else if (!validateRfc.test(rfcBilling)) showToast('warning','Facturación','El RFC no cumple con el formato requerido de la facturación.');
        else {
            loaderPestWare('Validando datos con el SAT...');
            $.ajax({
                type: 'POST',
                url: route('validate_documents'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    jobCenterId: jobCenterId,
                    rfcBilling: rfcBilling,
                    passwordBilling: passwordBilling
                },
                success: function (data) {
                    Swal.close();
                    if (parseInt(data.code) === 500) {
                        if (data.message.includes('Ya existe un CSD asociado a este RFC')) {
                            document.getElementById('divStatusConnectionSat').style.backgroundColor = "#16A085";
                            document.getElementById('text-message-response').innerText = 'Conexión con el SAT establecida.';
                            document.getElementById('divStatusConnectionSat').style.display = "block";
                            document.getElementById('deleteSat').style.display = "block";
                            document.getElementById('validateSat').style.display = "none";
                        } else {
                            document.getElementById('divStatusConnectionSat').style.backgroundColor = "#E74C3C";
                            document.getElementById('text-message-response').innerText = data.message;
                            document.getElementById('divStatusConnectionSat').style.display = "block";
                        }
                    }
                    else {
                        document.getElementById('divStatusConnectionSat').style.backgroundColor = "#16A085";
                        document.getElementById('text-message-response').innerText = 'Conexión con el SAT establecida.';
                        document.getElementById('divStatusConnectionSat').style.display = "block";
                        document.getElementById('deleteSat').style.display = "block";
                        document.getElementById('validateSat').style.display = "none";
                    }
                }
            });
        }
    })

    $("#deleteSat").click(function () {
        loaderPestWare('Desconectando datos con el SAT...');
        $.ajax({
            type: 'DELETE',
            url: route('delete_csd_by_rfc'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                jobCenterId: jobCenterId
            },
            success: function (data) {
                Swal.close();
                if (parseInt(data.code) === 500) {
                    document.getElementById('divStatusConnectionSat').style.backgroundColor = "#E74C3C";
                    document.getElementById('text-message-response').innerText = data.message;
                } else {
                    document.getElementById('divStatusConnectionSat').style.display = "none";
                    document.getElementById('deleteSat').style.display = "none";
                    document.getElementById('validateSat').style.display = "block";
                }
            }
        });
    })

    $("#transferFolios").click(function () {
        const origin = document.getElementById('origin').value;
        const source = document.getElementById('source').value;
        const quantityAvailable = document.getElementById('quantityFoliosAvailable').value;
        const quantity = document.getElementById('quantityFolios').value;

        // Validations
        if (origin == source) showToast('warning', 'Destino', 'El destino y Origen no pueden ser iguales.');
        else if (parseInt(quantity) > parseInt(quantityAvailable)) showToast('warning', 'Cantidad', 'La cantidad no puede ser mayor a los folios disponibles.');
        else {
            loaderPestWare('Traspasando Folios');
            $.ajax({
                type: 'POST',
                url: route('transfer_folios_invoice'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    origin: origin,
                    source: source,
                    quantity: quantity
                },
                success: function (data) {
                    Swal.close();
                    if (parseInt(data.code) === 500) {
                        showToast('error', 'Folios', data.message);
                    } else {
                        showToast('success', 'Folios', data.message);
                        document.getElementById('quantityFoliosAvailable').value = '';
                        document.getElementById('quantityFolios').value = '';
                        getJobCentersFolios();
                    }
                }
            });
        }
    })

    $("#purchaseFolios-12").click(function () {
        purchasePackageFolios(12);
    })

    $("#purchaseFolios-13").click(function () {
        purchasePackageFolios(13);
    })

    $("#purchaseFolios-14").click(function () {
        purchasePackageFolios(14);
    })

    $("#purchaseFolios-15").click(function () {
        purchasePackageFolios(15);
    })

    $("#purchaseFolios-16").click(function () {
        purchasePackageFolios(16);
    })

    $("#purchaseFolios-17").click(function () {
        purchasePackageFolios(17);
    })

    $("#purchaseFolios-18").click(function () {
        purchasePackageFolios(18);
    })

    $("#purchaseFolios-19").click(function () {
        purchasePackageFolios(19);
    })

    function getCatalogsRegimensSAT() {
        loaderPestWare('');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_regimens'),
            data: {
                _token: token,
                jobCenterId: jobCenterId
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#regimeFiscal');
                    select.empty();
                    const {regimens, fiscalRegime} = response;
                    if (fiscalRegime == null) {
                        select.append(`<option value="0" selected disabled>Selecciona un Régimen fiscal:</option>`);
                    }
                    regimens.forEach((element) => {
                        if (fiscalRegime === element.Value) {
                            select.append(`<option selected value="${element.Value}">${element.Name}</option>`);
                        } else select.append(`<option value="${element.Value}">${element.Name}</option>`);
                    });
                }
            }
        });
    }

    function getJobCentersFolios() {
        loaderPestWare('');
        $('#tableMailAccounts td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_job_centers_folios'),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('warning', 'Error', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableMailAccounts');
                    const {jobCentersFolios} = response;

                    let select = $('#origin');
                    select.empty();
                    let selectSource = $('#source');
                    selectSource.empty();
                    select.append(`<option value="0" selected disabled>Selecciona el origen</option>`);
                    selectSource.append(`<option value="0" selected disabled>Selecciona el destino</option>`);

                    jobCentersFolios.forEach((element) => {
                        const {id, name, folios, folios_consumed} = element;
                        rows += `<tr>
                            <td class="text-center text-primary text-bold">
                                <p class="text-default">
                                    <strong>
                                        <i class="fa fa-building-o" aria-hidden="true"></i>
                                    </strong>
                                    <a>${name}</a>
                                </p>
                            </td>
                            <td class="text-center">${folios}</td>
                            <td class="text-center">${folios_consumed}</td>
                           
                        </tr>`;
                        select.append(`<option value="${id}">${name}</option>`);
                        selectSource.append(`<option value="${id}">${name}</option>`);
                    });
                    table.append(rows);
                    Swal.close();
                }
            }
        });
    }

    function getJobCentersFoliosById(id) {
        loaderPestWare('Validando folios disponibles...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_job_centers_folios_by_id', id),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('warning', 'Error', response.message);
                } else {
                    const folios = response.data.folios;
                    document.getElementById('quantityFoliosAvailable').value = folios;
                    if (folios == 0) document.getElementById('quantityFolios').disabled = true;
                    else document.getElementById('quantityFolios').disabled = false;
                    Swal.close();
                }
            }
        });
    }

    function updateIsFoliosConsumed(isFoliosConsumed) {
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'PUT',
            url: route('update_is_folios_consumed'),
            data: {
                _token: token,
                isFoliosConsumed: isFoliosConsumed
            },
            success: function(response) {
                console.log(response);
            }
        });
    }

    function updateIsBuyFolios(isBuyFolios) {
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'PUT',
            url: route('update_is_buy_folios'),
            data: {
                _token: token,
                isBuyFolios: isBuyFolios
            },
            success: function(response) {
                console.log(response);
            }
        });
    }

    function purchasePackageFolios(planId) {
        loaderPestWare('Adquiriendo Folios');
        $.ajax({
            type: 'POST',
            url: route('purchase_folios_billing'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                package: planId,
            },
            success: function (data) {
                Swal.close();
                if (parseInt(data.code) === 500) {
                    Swal.fire({
                        title: '<strong>Error al comprar folios.</strong>',
                        type: 'error',
                        html: data.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        Swal.close()
                    })
                } else {
                    showToast('success', 'Folios', data.message);
                }
            }
        });
    }

    // Events
    let optionConsume = document.getElementById('optConsume');
    let optionManual = document.getElementById('optManual');
    let divFolios = document.getElementById('divFolios');
    let divSelects = document.getElementById('divSelects');
    optionManual.addEventListener('change', (event) => {
        if (event.currentTarget.checked) {
            updateIsFoliosConsumed(0);
            divFolios.style.display = "none";
            divSelects.style.display = "block";
        }
    });
    optionConsume.addEventListener('change', (event) => {
        if (event.currentTarget.checked) {
            updateIsFoliosConsumed(1);
            divFolios.style.display = "block";
            divSelects.style.display = "none";
        }
    });

    const selectOrigin = document.getElementById('origin');
    selectOrigin.addEventListener('change', (event) => {
        const option = event.target.value;
        getJobCentersFoliosById(option);
    });

    const checkboxIsBuyFolios = document.getElementById('purchaseFolios');
    checkboxIsBuyFolios.addEventListener('change', (event) => {
        let isPurchase = event.currentTarget.checked;
        if (isPurchase) isPurchase = 1;
        else isPurchase = 0;
        updateIsBuyFolios(isPurchase);
    });

});