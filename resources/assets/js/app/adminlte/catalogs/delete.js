import * as moment from "moment";
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    // Load data open modal -->
    $('.openModalDeleteCatalogs').click(function () {
        let all = $(this);
        let dataId = all.attr('data-id');
        let dataTitle = all.attr('data-title');
        let dataRoute = all.attr('data-route');
        $('#passwordManagerCatalogs').val('');
        $('#modalTitleDeleteCatalogs').html(`¿Estás seguro que quieres eliminar: ${dataTitle}?`);

        //START Delete Name Catalog
        $('#deleteCatalogButtonModal').click(function(){
            const passwordManagerCatalogs = document.getElementById('passwordManagerCatalogs').value;
            deleteCatalog(dataId,dataRoute,dataTitle, passwordManagerCatalogs);
        });
    });

    function deleteCatalog(dataId, dataRoute, dataTitle, passwordManagerCatalogs){
        loaderPestWare(`Eliminando ${dataTitle}...`)
        $.ajax({
            type: 'delete',
            url: route(dataRoute,dataId),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                passwordManagerCatalogs: passwordManagerCatalogs
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error al guardar', data.message);
                } else if (data.code == 300) {
                    Swal.close();
                    showToast('warning', `Credenciales Incorrectas`, data.message);
                } else if (data.code == 201){
                    Swal.close();
                    showToast('success-redirect', `${dataTitle} eliminada`, data.message, data.url);
                    $("#deleteCatalogButtonModal").prop('disabled',true);
                }
            }
        });
    }

});
