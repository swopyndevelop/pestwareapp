//employees/kardex.blade.php
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function () {
    let selectJobCenterCatalogsListId = $('#selectJobCenterCatalogsList option:selected').val();
    //modal item update
    $('#updateItemCatalog').click(function () {

        let name = $('#nameItemCatalog').val();
        let urlUpdate = $('#urlItemCatalog').val();
        let type = parseInt($('#typeCatalog').val());

        if (type == 3) {
            let description = $('#description').val();
            let percentage = $('#percentage').val();
            loaderPestWare('Guardando Datos...');
            $.ajax({
                type: 'POST',
                url: route(urlUpdate),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    name: name,
                    description: description,
                    percentage: percentage,
                    selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    } else {
                        Swal.close();
                        showToast('success', 'Datos guardados', data.message);
                        $("#updateItemCatalog").prop('disabled',true);
                        correct();
                    }
                }
            });
        } else if (type == 4) {
            let description = $('#description').val();
            let amount = $('#amount').val();
            loaderPestWare('Guardando Datos...');
            $.ajax({
                type: 'POST',
                url: route(urlUpdate),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    name: name,
                    description: description,
                    amount: amount,
                    selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    } else {
                        Swal.close();
                        showToast('success', 'Datos guardados', data.message);
                        $("#updateItemCatalog").prop('disabled',true);
                        correct();
                    }
                }
            });
        } else if (type == 8) {
            let category = $('#plagueCategoryNew option:selected').val();
            let isMonitoringLight = document.getElementById('isMonitoringUVNew').checked;
            let isMonitoringCapture = document.getElementById('isMonitoringCaptureNew').checked;
            loaderPestWare('Guardando Datos...');
            $.ajax({
                type: 'POST',
                url: route(urlUpdate),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    name: name,
                    category: category,
                    isMonitoringCapture: isMonitoringCapture,
                    isMonitoringLight: isMonitoringLight,
                    selectJobCenterCatalogsListId: selectJobCenterCatalogsListId
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    } else {
                        Swal.close();
                        showToast('success', 'Datos guardados', data.message);
                        $("#updateItemCatalog").prop('disabled', true);
                        correct();
                    }
                }
            });
        } else if (type == 13) {
                let creditDays = $('#creditDays').val();
                loaderPestWare('Guardando Datos...');
                $.ajax({
                    type: 'POST',
                    url: route(urlUpdate),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        name: name,
                        creditDays: creditDays,
                        selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                    },
                    success: function (data) {
                        if (data.code == 500) {
                            Swal.close();
                            showToast('error', 'Error al guardar', data.message);
                        } else {
                            Swal.close();
                            showToast('success', 'Datos guardados', data.message);
                            $("#updateItemCatalog").prop('disabled',true);
                            correct();
                        }
                    }
                });
        } else if (type == 15) {
            let categorie = $('#categorie').val();
            loaderPestWare('Guardando Datos...');
            $.ajax({
                type: 'POST',
                url: route(urlUpdate),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    name: name,
                    categorie: categorie,
                    selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    } else {
                        Swal.close();
                        showToast('success', 'Datos guardados', data.message);
                        $("#updateItemCatalog").prop('disabled',true);
                        correct();
                    }
                }
            });
        } else if (type == 16) {
            let description = $('#description').val();
            let key = $('#key').val();
            loaderPestWare('Guardando Datos...');
            $.ajax({
                type: 'POST',
                url: route(urlUpdate),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    name: name,
                    description: description,
                    key: key,
                    selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    } else {
                        Swal.close();
                        showToast('success', 'Datos guardados', data.message);
                        $("#updateItemCatalog").prop('disabled',true);
                        correct();
                    }
                }
            });
        } else if (type == 17) {
                let key = $('#key').val();
                let typeArea = $('#typeArea').val();
                loaderPestWare('Guardando Datos...');
                $.ajax({
                    type: 'POST',
                    url: route(urlUpdate),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        name: name,
                        key: key,
                        typeArea: typeArea,
                        selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                    },
                    success: function (data) {
                        if (data.code == 500) {
                            Swal.close();
                            showToast('error', 'Error al guardar', data.message);
                        } else {
                            Swal.close();
                            showToast('success', 'Datos guardados', data.message);
                            $("#updateItemCatalog").prop('disabled',true);
                            correct();
                        }
                    }
                });
        } else if (type == 19) {
            let value = $('#value').val();
            loaderPestWare('Guardando Datos...');
            $.ajax({
                type: 'POST',
                url: route(urlUpdate),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    name: name,
                    value: value,
                    selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    } else {
                        Swal.close();
                        showToast('success', 'Datos guardados', data.message);
                        $("#updateItemCatalog").prop('disabled',true);
                        correct();
                    }
                }
            });
        } else if (type == 20) {
            let description = $('#description').val();
            loaderPestWare('Guardando Datos...');
            $.ajax({
                type: 'POST',
                url: route(urlUpdate),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    name: name,
                    description: description,
                    selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    } else {
                        Swal.close();
                        showToast('success', 'Datos guardados', data.message);
                        $("#updateItemCatalog").prop('disabled',true);
                        correct();
                    }
                }
            });
        } else {
            loaderPestWare('Guardando Datos...');
            $.ajax({
                type: 'POST',
                url: route(urlUpdate),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    name: name,
                    selectJobCenterCatalogsListId:selectJobCenterCatalogsListId
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    } else {
                        Swal.close();
                        showToast('success', 'Datos guardados', data.message);
                        $("#updateItemCatalog").prop('disabled',true);
                        correct();
                    }
                }
            });
        }

    });
    //end modal item update

    //alerts messages
    function correct() {
        window.location.reload();
    }

    //end sweet alert
});
