/**
 * @author Alberto Martínez
 * @version 05/08/2020
 * register/_modalCash.blade.php
 */

$(document).ready(function() {

    // Filters
    let initialDateSpend;
    let finalDateSpend;

    $(function() {

        $('input[name="filterDateSpend"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateSpend"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateSpend = picker.startDate.format('YYYY-MM-DD');
            finalDateSpend = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateSpend"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    $('#btnFilterSpend').click(function() {
        let idServiceOrder = $('#selectFilterServiceOrderSpend option:selected').val();
        let idCustomer = $('#selectFilterCustomerNameSpend option:selected').val();
        let pay = $('#filterPaySpend option:selected').val();
        let amount = $('#filterAmountSpend').val();
        let comments = $('#filterDescriptionSpend').val();
        let status = $('#filterStatusSpend option:selected').val();

        initialDateSpend = initialDateSpend == undefined ? null : initialDateSpend;
        finalDateSpend = finalDateSpend == undefined ? null : finalDateSpend;

        let urlBase = "https://pestwareapp.com/cash/index?";
        let urlBaseDev = "http://127.0.0.1:8000/cash/index?";
        let url = urlBaseDev + "initialDateSpend=" + initialDateSpend + "&finalDateSpend=" + finalDateSpend +
            "&idServiceOrder=" + idServiceOrder + "&idCustomer=" + idCustomer + "&pay=" + pay +
            "&amount=" + amount + "&comments=" + comments + "&status=" + status;

        window.location.href = url;
    });
});
