$(document).ready(function() {

    //Intro Js index_inventory
    let urlCurrent = window.location.pathname;
    const introJsPest = document.getElementById('introJsInpunt').value;
    const introIdEmployeeView = document.getElementById('introIdMovementEmp').value;
    //console.log(introIdEmployeeView);
    //const introIdMovementView = document.getElementById('introIdMoventChange').value;
    //console.log(introIdMovementView);
    //let viewInventoryMovement = `/inventory/movements/${introIdEmployeeView}`;

    
        if (introJsPest == 1){
            startInventoryMovements();
        }

        function startInventoryMovements(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#titleMevementsEmployee',
                        title:"Movimientos",
                        data:"",
                        position: "left",
                    },
                    {
                        element: '#tableProduct',
                        title:"Movimiento",
                        intro: "En la siguiente tabla podrás observar los datos más relevantes de cada uno de los movimientos " +
                            "de técnicos y sucursales.",
                        position: "left",
                    },
                    {
                        element:'#ulMovementButton',
                        title:"Movimiento",
                        intro: "Para ver el detalle de un movimiento deberás dar clic en la opción Ver.",
                        position: "left",
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false,
            });
            intro.start();
            intro.onexit(function() {
                updateAjaxIntroJs(0);
            });
            intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
                intro.exit();
                setTimeout(() => {
                    $('#movementMenDivInventoryDiv').addClass('open');
                    setTimeout(() => {
                        $(`#transferDetailCE${introIdEmployeeView}`).modal('show');
                        setTimeout(() => {
                            startDetailModalCE();
                        }, 500);
                    }, 500);
                }, 500);
            });
        }

        //startDetailModalCE();
        function startDetailModalCE(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#modalTitle',
                        title:"Detalle del traspaso",
                        position: "right",
                    },
                    {
                        element: '#divTransferLeftModal',
                        title:"Detalle del traspaso",
                        intro: "Dentro del detalle podrás observar información como: Folio, Fecha del movimiento y usuario.",
                        position: "right",
                    },
                    {
                        element: '#divTransferRigthModal',
                        title:"Detalle del Traspaso",
                        intro: "Dentro del detalle podrás observar información como: Origen (Proveedor), Destino (Sucursal o Centro de Trabajo) y Total.",
                        position: "right",
                    },
                    {
                        element: '#tableProductTransfer',
                        title:"Productos",
                        intro: "En está tabla encontrarás los productos detallados que pertenecen a la entrada.",
                        position: "right",
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false,
            });
            intro.start();
            intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
                $(`#transferDetailCE${introIdEmployeeView}`).click();
                intro.exit();
                setTimeout(() => {
                    startBussinessIntelligence();
                }, 1000);
            });
        }
        //startBussinessIntelligence();
        function startBussinessIntelligence() {
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#businessIntellinge',
                        title: "Inteligencia de Negocios",
                        intro: "<div class='text-center'><p>Da click en el botón Inteligencia para ir al módulo.</p>" +
                            "<button class='btn' id='bussinessInteligenceButton'>Inteligencia</button></div>",
                        position: "right",
                    },
                ],
                keyboardNavigation: false,
            });
            intro.start();
            document.getElementById('bussinessInteligenceButton').onclick = function () {

                updateAjaxIntroJs(1);

                setTimeout(() => {
                    window.location.href = route('monitoring');
                }, 1000);

                intro.exit();
            };
        }

    function updateAjaxIntroJs(introjs){
        $.ajax({
            type: 'POST',
            url: route('change_introjs_user'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                introjs: introjs
            },
            success: function(data) {
                //console.log('OK');
            }
        });
    }
});