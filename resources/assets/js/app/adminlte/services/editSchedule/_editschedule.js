/**
 * @author Alberto Martínez|Yomira Martínez
 * @version 08/05/2019
 * register/_editschedule.blade.php
 */



import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let o_id = $('#o_id').val();
    let pregnancyC, babyC, childrenC, adultC, respiratoryC, inmunologicC, renalC, cardiacC, does_notC, otherC
        ,dogC, catC, birdC, fishC, other_mascotC, habilitarC, pC, p1C, hC;

    $('#saveOrderC').click(function(){
        //get values inputs address
        let addressC = $('#calleC').val();
        let numberC = $('#numC').val();
        let referenceC = $('#referenceC').val();
        if(document.querySelector('input[name="embarazoC"]:checked')) {
            pregnancyC = 1;
        }
        if(document.querySelector('input[name="bebesC"]:checked')) {
            babyC = 2;
        }
        if(document.querySelector('input[name="childrenC"]:checked')) {
            childrenC = 3;
        }
        if(document.querySelector('input[name="adultoC"]:checked')) {
            adultC = 4;
        }
        if(document.querySelector('input[name="respiratoriasC"]:checked')) {
            respiratoryC = 1;
        }
        if(document.querySelector('input[name="inmunologicoC"]:checked')) {
            inmunologicC = 2;
        }
        if(document.querySelector('input[name="renalesC"]:checked')) {
            renalC = 3;
        }
        if(document.querySelector('input[name="cardiacasC"]:checked')) {
            cardiacC = 4;
        }
        if(document.querySelector('input[name="does_notC"]:checked')) {
            does_notC = 5;
        }
        if(document.querySelector('input[name="otherC"]:checked')) {
            otherC = 6;
            p1C = 'required';
        }
        if(document.querySelector('input[name="perroC"]:checked')) {
            dogC = 1;
        }
        if(document.querySelector('input[name="gatoC"]:checked')) {
            catC = 2;
        }
        if(document.querySelector('input[name="aveC"]:checked')) {
            birdC = 3;
        }
        if(document.querySelector('input[name="pezC"]:checked')) {
            fishC = 4;
        }
        if(document.querySelector('input[name="otroC"]:checked')) {
            other_mascotC = 5;
            pC = 'required';
        }
        if(document.querySelector('input[name="habilitarC"]:checked')) {
            habilitarC = 1;
            hC = 'required';
        }
        let specificC = $('#specificC').val();
        let mascotC =  $('#mascotaC').val();
        //get values input comments
        let commentsC = $('#mensajeC').val();
        let customer_idC = $('#customer_idC').val();
        let q_idC = $('#q_idC').val();
        let cellphoneC = $('#cellphoneC').val();
        let emailC = $('#emailC').val();
        let contactC = $('#contactC').val();
        let cellphone_twoC = $('#cellphone_twoC').val();
        let customerNameC = $('#customerNameC').val();
        let establishmentNameC = $('#establishmentNameC').val();
        let pay_mC = $('#pay_mC').val();
        let pay_fC = $('#pay_fC').val();
        let stateC = $('#stateC').val();
        let municipalityC = $('#municipalityC').val();
        let colonyC = $('#colonyC').val();
        let rfcC = $('#rfcC').val();
        let socialC = $('#socialC').val();
        let address_rfcC = $('#address_rfcC').val();
        let e_mailC = $('#e_mailC').val();
        let totalC = $('#totalC').val();

        let typeCfdiCustomerMainNew = $('#typeCfdiCustomerMainNewEdit option:selected').val();
        let methodPayCustomerMainNew = $('#methodPayCustomerMainEdit option:selected').val();
        let wayPayCustomerMainNew = $('#wayPayCustomerMainEdit option:selected').val();

        if(addressC.length === 0){
            missingText('Ingresa la calle correspondiente');
        }else if(numberC.length === 0){
            missingText('Ingresa el # Domicilio');
        }else if(customerNameC.length === 0){
            missingText('Ingresa el Nombre del cliente');
        }else if(stateC.length === 0){
            missingText('Ingresa el Estado correspondiente');
        }else if(municipalityC.length === 0){
            missingText('Ingresa el Municipio correspondiente');
        }else if(colonyC.length === 0){
            missingText('Ingresa la Colonia correspondiente');
        }else if(pay_mC === null){
            missingText('Selecciona un Método De pago');
        }else if(pay_fC === null){
            missingText('Selecciona una Forma de Pago');
        }else if(mascotC.length === 0 && pC != null){
            missingText('Ingresa la Mascota correspondiente');
        }else if(specificC.length === 0 && p1C != null){
            missingText('Ingresa la Enfermedad correspondiente');
        }else if(rfcC.length === 0 && hC != null){
            missingText('Ingresa el rfc correspondiente');
        }else if(socialC.length === 0 && hC != null){
            missingText('Ingresa la Razón Social correspondiente');
        }else if(address_rfcC.length === 0 && hC != null){
            missingText('Ingresa el Domicilio de Facturación correspondiente');
        }else{
            saveOrderC();
        }

        //Save Customer datas and Order Service in database
        function saveOrderC(){
            loaderPestWare('');
            $.ajax({
                type: 'PUT',
                url: route('update_order'),
                data:{
                    _token: $("meta[name=csrf-token]").attr("content"),
                    addressC: addressC,
                    numberC: numberC,
                    referenceC: referenceC,
                    pregnancyC: pregnancyC,
                    babyC: babyC,
                    childrenC: childrenC,
                    adultC: adultC,
                    respiratoryC: respiratoryC,
                    inmunologicC: inmunologicC,
                    renalC: renalC,
                    cardiacC: cardiacC,
                    does_notC: does_notC,
                    otherC: otherC,
                    specificC: specificC,
                    dogC: dogC,
                    catC: catC,
                    birdC: birdC,
                    fishC: fishC,
                    other_mascotC: other_mascotC,
                    mascotC: mascotC,
                    commentsC: commentsC,
                    customer_idC: customer_idC,
                    q_idC: q_idC,
                    o_id: o_id,
                    cellphoneC: cellphoneC,
                    emailC: emailC,
                    contactC: contactC,
                    cellphone_twoC: cellphone_twoC,
                    customerNameC: customerNameC,
                    establishmentNameC: establishmentNameC,
                    pay_mC: pay_mC,
                    pay_fC: pay_fC,
                    stateC: stateC,
                    municipalityC: municipalityC,
                    colonyC: colonyC,
                    habilitarC: habilitarC,
                    rfcC: rfcC,
                    socialC: socialC,
                    address_rfcC: address_rfcC,
                    e_mailC: e_mailC,
                    totalC: totalC,
                    typeCfdiCustomerMainNew: typeCfdiCustomerMainNew,
                    methodPayCustomerMainNew: methodPayCustomerMainNew,
                    wayPayCustomerMainNew: wayPayCustomerMainNew,
                },
                success: function (data) {
                    if (data.errors){
                        Swal.close();
                        showToast('error','!Espera!', 'Algo salió mal');
                    }
                    else{
                        Swal.close();
                        showToast('success-redirect','Servicio Actualizado', 'Servicio Guardado correctamente','calendario');
                        $("#saveOrderC").attr("disabled", true);
                    }
                }
            });
        }
    });

    function missingText(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
        });
    }

});
