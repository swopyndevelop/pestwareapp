//IntroJs
$(document).ready(function() {

    const introJsPest = document.getElementById('introJsInpunt').value;
    const lastOrderIntroJsNew = document.getElementById('q_id').value;
    let urlCurrent = window.location.pathname;
    let viewCalendar = '/service/schedule';
    //let viewOrderService = '/order/services/4948';
    let viewOrderService = `/order/services/${lastOrderIntroJsNew}`;

    if (urlCurrent == viewOrderService){
        if (introJsPest == 1){
            setTimeout(() => {
                startScheduleService();
            }, 1000);
        }
    }

    function startScheduleService(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: '#modalTitle',
                    title:"Programar Servicio",
                    intro: "Nombre",
                    position: "right",
                },
                {
                    element: '#grupo__customerName',
                    title:"Cliente",
                    intro: "El nombre del cliente se llena automáticamente, debido a que ya se ingreso al crear la cotización.",
                    position: "right",
                },
                {
                    element: '#grupo__establishmentName',
                    title:"Nombre comercial",
                    intro: "Los datos del cliente llenados previamente en la cotización podrán ser modificados desde aquí " +
                        "si así lo deseas.",
                    position: "right",
                },
                {
                    element: '#grupo__cellphone',
                    title:"Teléfono",
                    intro: "Los datos del cliente llenados previamente en la cotización podrán ser modificados desde aquí " +
                        "si así lo deseas.",
                    position: "right"
                },
                {
                    element: '#grupo__email',
                    title:"Correo",
                    intro: "Los datos del cliente llenados previamente en la cotización podrán ser modificados desde aquí " +
                        "si así lo deseas.",
                    position: "right"
                },
                {
                    element: '#grupo__state',
                    title:"Estado",
                    intro: "Los datos del cliente llenados previamente en la cotización podrán ser modificados desde aquí " +
                        "si así lo deseas.",
                    position: "right"
                },
                {
                    element: '#grupo__municipality',
                    title:"Municipio",
                    intro: "Los datos del cliente llenados previamente en la cotización podrán ser modificados desde aquí " +
                        "si así lo deseas.",
                    position: "right"
                },
                {
                    element: '#grupo__colony',
                    title:"Colonia",
                    intro: "Los datos del cliente llenados previamente en la cotización podrán ser modificados desde aquí " +
                        "si así lo deseas.",
                    position: "right"
                },
                {
                    element: '#grupo__calle',
                    title:"Calle",
                    intro: "Es indispensable ingresar la calle del domicilio de tu cliente para que el técnico pueda aprovechar" +
                        " la funcionalidad de GPS con la que cuenta la App Móvil de PestWare.",
                    position: "right"
                },
                {
                    element: '#grupo__num',
                    title:"Número",
                    intro: "Es indispensable ingresar el número del domicilio de tu cliente para que el técnico pueda aprovechar" +
                        " la funcionalidad de GPS con la que cuenta la App Móvil de PestWare.",
                    position: "right"
                },
                {
                    element: '#grupo__mensaje',
                    title:"Comentarios",
                    intro: "Aquí podrás ingresar observaciones, referencias o algún comentario importante que deba saber " +
                        "el técnico antes de comenzar el servicio.",
                    position: "right"
                },
                {
                    element: '#conditionsDiv',
                    title:"Condiciones",
                    intro: "Si tu cliente cuenta con alguna característica que pueda afectar a su salud o a la de su familia, puedes " +
                        "seleccionar la opción de la siguiente lista, además puedes seleccionar el método de pago.",
                    position: "top"
                },
                {
                    element: '#rfcDiv',
                    title:"Facturación",
                    intro: "En caso de que tu cliente requiera factura marca la casilla Facturación para ingresar los datos " +
                        "y calcular el IVA.",
                    position: "right"
                },
                {
                    element: '#labelTotalsDiv',
                    title:"Totales",
                    intro: "Finalmente observarás el costo final del servicio con el cual se programará. En caso que requieras " +
                        "modificar el costo por que el cliente así lo solicite podrás presionar el botón de Editar cotización.",
                    position: "right"
                },
                {
                    element: '#saveOrder',
                    title:"Guardar",
                    intro: "Da click en el botón guardar para agendar el servicio.",
                    position: "right"
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo",
            keyboardNavigation: false,
        });
        intro.start();
        intro.onexit(function() {
            updateAjaxIntroJs(0);
        });
        intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {

            setTimeout(() => {
                updateAjaxIntroJs(1);
                setTimeout(() => {
                    window.location.href = '../../service/schedule';
                }, 1000);
            }, 500);

            //intro.exit();
        });

    }

    if (urlCurrent == viewCalendar){
        if (introJsPest == 1){
            startSchedule();
        }
    }
    //startSchedule
    function startSchedule(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: '#titleScheduleH3',
                    title:"Agenda",
                    intro: "Agendar Servicio",
                    position: "right"
                },
                {
                    element: '#titleScheduleH3',
                    title:"Agenda",
                    intro: "Agendar Servicio",
                    position: "right"
                },
                {
                    element: '#homeThScheduleDiv',
                    title: "Ordenes de Servicio",
                    intro: "En la siguiente tabla podrás monitorear y gestionar todas las ordenes de servicio agendadas",
                    position: "right",
                },
                {
                    element: '#trScheduleOrderDiv',
                    title:"Orden de Servicio",
                    intro: "Una ves ptogramada una cotización se podrá observar en la siguiente tabla.",
                    position: "right",
                },
                {
                    element: '#statusScheduleOrderDiv',
                    title:"Estatus",
                    intro: "Con el estatus NO PROGRAMADO, el cual indica que falta agendar el servicio en el calendario.",
                    position: "left"
                },
                {
                    element: '#calendar',
                    title:"Calendario",
                    intro: "Selecciona un día y rango de horario arrastrando en el calendario para verificar la disponibildad de " +
                        "técnicos en ese horario.",
                    position: "left"
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo",
            keyboardNavigation: false,
        });
        intro.start();
        intro.onexit(function() {
            updateAjaxIntroJs(0);
        });
        intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
            $('#addSchedule').modal('show');
            intro.exit();
            setTimeout(() => {
                updateAjaxIntroJs(1);
            }, 500);
        });
    }
    function updateAjaxIntroJs(introjs){
        $.ajax({
            type: 'POST',
            url: route('change_introjs_user'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                introjs: introjs
            },
            success: function(data) {
                //console.log('OK');
            }
        });
    }
});