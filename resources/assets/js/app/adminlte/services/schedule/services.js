/**
 * @author Alberto Martínez|Yomira Martínez
 * @version 01/04/2019
 * register/_schedule.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {expressions, getFields, setFields, setOnlyField, validateField} from "../../validations/regex";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let q_id = $('#q_id').val();
    let pregnancy, baby, children, adult, respiratory, inmunologic, renal, cardiac, does_not,other,dog, cat,bird, fish,
        other_mascot, habilitar, p, p1, h;

    // validations section -> cellphone_two customerName establishmentName municipality
    const customerName = document.getElementById('customerName');
    const establishmentName = document.getElementById('establishmentName');
    const cellphone = document.getElementById('cellphone');
    const email = document.getElementById('email');
    const contact = document.getElementById('contact');
    const cellphone_two = document.getElementById('cellphone_two');
    const state = document.getElementById('state');
    const municipality = document.getElementById('municipality');
    const colony = document.getElementById('colony');
    const calle = document.getElementById('calle');
    const num = document.getElementById('num');
    const mensaje = document.getElementById('mensaje');
    const rfc = document.getElementById('rfc');
    const social = document.getElementById('social');
    const address_rfc = document.getElementById('address_rfc');
    const emailPurcharse = document.getElementById('emailPurcharse');
    let enable = document.getElementById('habilitar').checked;

    const fieldsScheduleForm = {
        customerName: false,
        establishmentName: false,
        cellphone: false,
        email: false,
        contact: false,
        cellphone_two: false,
        state: false,
        municipality: false,
        colony: false,
        calle: false,
        num: false,
        mensaje: false,
        rfc: true,
        social: true,
        address_rfc: true,
        emailPurcharse: true,
    }
    setFields(fieldsScheduleForm);

    const validateForm = (e) => {
        switch (e.target.name) {
            case "customerName":
                validateField(expressions.description, e.target.value, 'customerName');
                break;
            case "establishmentName":
                validateField(expressions.descriptionNotRequired, e.target.value, 'establishmentName');
                break;
            case "cellphone":
                validateField(expressions.descriptionNotRequired, e.target.value, 'cellphone');
                break;
            case "email":
                validateField(expressions.descriptionNotRequired, e.target.value, 'email');
                break;
            case "contact":
                validateField(expressions.descriptionNotRequired, e.target.value, 'contact');
                break;
            case "cellphone_two":
                validateField(expressions.descriptionNotRequired, e.target.value, 'cellphone_two');
                break;
            /*case "state":
                validateField(expressions.description, e.target.value, 'state');
                break;
            case "municipality":
                validateField(expressions.description, e.target.value, 'municipality');
                break;
            case "colony":
                validateField(expressions.description, e.target.value, 'colony');
                break;
            case "calle":
                validateField(expressions.description, e.target.value, 'calle');
                break;
            case "num":
                validateField(expressions.descriptionNumber, e.target.value, 'num');
                break;*/
            case "mensaje":
                validateField(expressions.descriptionNotRequired, e.target.value, 'mensaje');
                break;
            case "rfc":
                validateField(expressions.description, e.target.value, 'rfc');
                break;
            case "social":
                validateField(expressions.description, e.target.value, 'social');
                break;
            case "address_rfc":
                validateField(expressions.description, e.target.value, 'address_rfc');
                break;
            case "emailPurcharse":
                validateField(expressions.email, e.target.value, 'emailPurcharse');
                break;
        }
    }

    //Events
    customerName.addEventListener('keyup', validateForm);
    customerName.addEventListener('blur', validateForm);
    establishmentName.addEventListener('keyup', validateForm);
    establishmentName.addEventListener('blur', validateForm);
    cellphone.addEventListener('keyup', validateForm);
    cellphone.addEventListener('blur', validateForm);
    email.addEventListener('keyup', validateForm);
    email.addEventListener('blur', validateForm);
    contact.addEventListener('keyup', validateForm);
    contact.addEventListener('blur', validateForm);
    cellphone_two.addEventListener('keyup', validateForm);
    cellphone_two.addEventListener('blur', validateForm);
    /*state.addEventListener('keyup', validateForm);
    state.addEventListener('blur', validateForm);
    municipality.addEventListener('keyup', validateForm);
    municipality.addEventListener('blur', validateForm);
    colony.addEventListener('keyup', validateForm);
    colony.addEventListener('blur', validateForm);
    calle.addEventListener('keyup', validateForm);
    calle.addEventListener('blur', validateForm);
    num.addEventListener('keyup', validateForm);
    num.addEventListener('blur', validateForm);
     */
    mensaje.addEventListener('keyup', validateForm);
    mensaje.addEventListener('blur', validateForm);
    rfc.addEventListener('keyup', validateForm);
    rfc.addEventListener('blur', validateForm);
    social.addEventListener('keyup', validateForm);
    social.addEventListener('blur', validateForm);
    address_rfc.addEventListener('keyup', validateForm);
    address_rfc.addEventListener('blur', validateForm);
    emailPurcharse.addEventListener('keyup', validateForm);
    emailPurcharse.addEventListener('blur', validateForm)

    enable = document.querySelector('#habilitar');
    enable.addEventListener( 'change', function() {
            if (enable == true){
                setOnlyField('rfc', false);
                setOnlyField('social', false);
                setOnlyField('address_rfc', false);
                setOnlyField('emailPurcharse', false);
            }
            else
            {
                setOnlyField('rfc', true);
                setOnlyField('social', true);
                setOnlyField('address_rfc', true);
                setOnlyField('emailPurcharse', true);
                document.getElementById('grupo__rfc').classList.remove('formulario__grupo-incorrecto');
                document.querySelector(`#grupo__rfc .formulario__input-error`).classList.remove('formulario__input-error-activo');
                document.getElementById('grupo__social').classList.remove('formulario__grupo-incorrecto');
                document.querySelector(`#grupo__social .formulario__input-error`).classList.remove('formulario__input-error-activo');
                document.getElementById('grupo__address_rfc').classList.remove('formulario__grupo-incorrecto');
                document.querySelector(`#grupo__address_rfc .formulario__input-error`).classList.remove('formulario__input-error-activo');
                document.getElementById('grupo__emailPurcharse').classList.remove('formulario__grupo-incorrecto');
                document.querySelector(`#grupo__emailPurcharse .formulario__input-error`).classList.remove('formulario__input-error-activo');
            }
        }
    );

    $('#saveOrder').click(function(){
        //get values inputs address

        if(document.querySelector('input[name="embarazo"]:checked')) {
            pregnancy = 1;
        }
        if(document.querySelector('input[name="bebes"]:checked')) {
            baby = 2;
        }
        if(document.querySelector('input[name="children"]:checked')) {
            children = 3;
        }
        if(document.querySelector('input[name="adult"]:checked')) {
            adult = 4;
        }
        if(document.querySelector('input[name="respiratorias"]:checked')) {
            respiratory = 1;
        }
        if(document.querySelector('input[name="inmunologico"]:checked')) {
            inmunologic = 2;
        }
        if(document.querySelector('input[name="renales"]:checked')) {
            renal = 3;
        }
        if(document.querySelector('input[name="cardiacas"]:checked')) {
            cardiac = 4;
        }
        if(document.querySelector('input[name="does_not"]:checked')) {
            does_not = 5;
        }
        if(document.querySelector('input[name="other"]:checked')) {
            other = 6;
            p1 = 'required';
        }
        if(document.querySelector('input[name="perro"]:checked')) {
            dog = 1;
        }
        if(document.querySelector('input[name="gato"]:checked')) {
            cat = 2;
        }
        if(document.querySelector('input[name="ave"]:checked')) {
            bird = 3;
        }
        if(document.querySelector('input[name="pez"]:checked')) {
            fish = 4;
        }
        if(document.querySelector('input[name="otro"]:checked')) {
            other_mascot = 5;
            p = 'required';
        }
        if(document.querySelector('input[name="habilitar"]:checked')) {
            habilitar = 1;
            h = 'required';
        }

        let specific = $('#specific').val();
        let mascot =  $('#mascota').val();
        //get values input comments
        let customer_id = $('#customer_id').val();
        let customerName = $('#customerName').val();
        let establishmentName = $('#establishmentName').val();
        let cellphone = $('#cellphone').val();
        let email = $('#email').val();
        let contact = $('#contact').val();
        let cellphone_two = $('#cellphone_two').val();
        let state = $('#state').val();
        let municipality = $('#municipality').val();
        let colony = $('#colony').val();
        let address = $('#calle').val();
        let number = $('#num').val();
        let comments = $('#mensaje').val();

        let pay_m = $('#pay_m').val();
        let pay_f = $('#pay_f').val();

        let rfc = $('#rfc').val();
        let social = $('#social').val();
        let address_rfc = $('#address_rfc').val();
        let emailPurcharse = $('#emailPurcharse').val();

        let typeCfdiCustomerMainNew = $('#typeCfdiCustomerMainNewEdit option:selected').val();
        let methodPayCustomerMainNew = $('#methodPayCustomerMainEdit option:selected').val();
        let wayPayCustomerMainNew = $('#wayPayCustomerMainEdit option:selected').val();

        validateField(expressions.description, customerName, 'customerName');
        validateField(expressions.descriptionNotRequired, establishmentName, 'establishmentName');
        validateField(expressions.descriptionNotRequired, cellphone, 'cellphone');
        validateField(expressions.descriptionNotRequired, email, 'email');
        validateField(expressions.descriptionNotRequired, contact, 'contact');
        validateField(expressions.descriptionNotRequired, cellphone_two, 'cellphone_two');
        //validateField(expressions.description, state, 'state');
        //validateField(expressions.description, municipality, 'municipality');
        //validateField(expressions.description, colony, 'colony');
        //validateField(expressions.description, address, 'calle');
        //validateField(expressions.descriptionNumber, number, 'num');
        validateField(expressions.descriptionNotRequired, comments, 'mensaje');

        if (habilitar == true){
            validateField(expressions.description, rfc,'rfc');
            validateField(expressions.description, social, 'social');
            validateField(expressions.description, address_rfc, 'address_rfc');
            validateField(expressions.email, emailPurcharse, 'emailPurcharse');
        }

        console.log(getFields().customerName)
        console.log(getFields().establishmentName)
        console.log(getFields().cellphone)
        console.log(getFields().email)
        console.log(getFields().state)
        console.log(getFields().municipality)
        console.log(getFields().colony)
        console.log(getFields().calle)
        console.log(getFields().num)

        if(getFields().customerName && getFields().establishmentName && getFields().cellphone && getFields().email){
            loaderPestWare('Programando Servicio...');
            $.ajax({
                type: 'POST',
                url: route('order_store'),
                data:{
                    _token: $("meta[name=csrf-token]").attr("content"),
                    address: address,
                    number: number,
                    pregnancy: pregnancy,
                    baby: baby,
                    children: children,
                    adult: adult,
                    respiratory: respiratory,
                    inmunologic: inmunologic,
                    renal: renal,
                    cardiac: cardiac,
                    does_not: does_not,
                    other: other,
                    specific: specific,
                    dog: dog,
                    cat: cat,
                    bird: bird,
                    fish: fish,
                    other_mascot: other_mascot,
                    mascot: mascot,
                    comments: comments,
                    customer_id: customer_id,
                    q_id: q_id,
                    cellphone: cellphone,
                    email: email,
                    contact: contact,
                    cellphone_two: cellphone_two,
                    customerName: customerName,
                    establishmentName: establishmentName,
                    pay_m: pay_m,
                    pay_f: pay_f,
                    state: state,
                    municipality: municipality,
                    colony: colony,
                    habilitar: habilitar,
                    rfc: rfc,
                    social: social,
                    address_rfc: address_rfc,
                    e_mail: emailPurcharse,
                    typeCfdiCustomerMainNew: typeCfdiCustomerMainNew,
                    methodPayCustomerMainNew: methodPayCustomerMainNew,
                    wayPayCustomerMainNew: wayPayCustomerMainNew,
                },
                success: function (data) {
                    if (data.code === 500){
                        Swal.close();
                        showToast('error','Error al Guardar','Algo salio mal, intentalo de nuevo');
                    }
                    else if (data.code === 201) {
                        Swal.close();
                        showToast('success-redirect','Orden de Servicio generada', 'Datos Guardados correctamente','calendario');
                        $("#saveOrder").attr('disabled', true);
                    }
                }
            });
        }

        else{
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }
        //Save Customer datas and Order Service in database
    });

});

