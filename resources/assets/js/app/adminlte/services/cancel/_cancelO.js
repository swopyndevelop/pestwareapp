/**
 * @author Yomira Martínez
 * @version 06/05/2019
 * register/_cancelschedule.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";


$(document).ready(function(){

    let id_order, reason_o, comments_o;
    id_order = $('#idOrder').val();

    //START Cancel Quotation
    $('#Delete').click(function () {
        let mess = 'Cancelando Orden de Servicio...';
        loaderPestWare(mess);

        reason_o = $('#reason').val();
        comments_o = $('#commentary').val();

        //validations
        if (reason_o === null) {
            missingText('Seleccione un MOTIVO DE CANCELACION');
        } else if (comments_o.length === 0) {
            missingText('Falta COMENTARIO DEL CLIENTE');
        } else {
            CancelSave_O();
        }

        function CancelSave_O() {
            loaderPestWare('');
            $.ajax({
                type: 'POST',
                url: route('delete_order'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id_order: id_order,
                    reason_o: reason_o,
                    comments_o: comments_o
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error','!Espera!', 'Algo salió mal');
                    } else {
                        Swal.close();
                        showToast('success-redirect','Cancelación de Servicio', 'Su cancelación fue procesada','calendario');
                        $("#Delete").attr('disabled', true);
                    }
                }
            });
        }
    });

    function missingText(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
        });
    }

 });
