import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";
import * as moment from "moment";

$(document).ready(function(){

    let idOrder;
    let url;
    let cellphoneCustomer;
    let cellphoneMain;

    $('#sendReminderWhatsappSchedule').on('show.bs.modal', function(e) {
        swal.close();
        idOrder = $(e.relatedTarget).data().id;
        url = $(e.relatedTarget).data().url;
        cellphoneCustomer = $(e.relatedTarget).data().cellphonecustomer;
        $('#cellphoneClient').val(cellphoneCustomer);
        cellphoneMain = $(e.relatedTarget).data().cellphonemain;
        $('#cellphoneMain').val(cellphoneMain);

        $("#sendReminderWhatsappSave").click(function() {
            const cellphoneOther = document.getElementById('cellphoneOther').value;
            cellphoneCustomer = document.getElementById('cellphoneClient').value;
            cellphoneMain = document.getElementById('cellphoneMain').value;
            let urlSeparate = url.split('{');
            let urlCompleteCustomer = `${urlSeparate[0]}${cellphoneCustomer}${urlSeparate[1]}`;
            let urlCompleteMain = `${urlSeparate[0]}${cellphoneMain}${urlSeparate[1]}`;
            let urlCompleteOther = `${urlSeparate[0]}${cellphoneOther}${urlSeparate[1]}`;
            if (cellphoneCustomer !== '') window.open(urlCompleteCustomer, '_blank');
            if (cellphoneMain !== '') window.open(urlCompleteMain, '_blank');
            if (cellphoneOther !== '') window.open(urlCompleteOther, '_blank');

            sendWhatsappReminderOrder();
        });
    });

    function sendWhatsappReminderOrder(){
        loaderPestWare('');
        $.ajax({
            type: 'PUT',
            url: route('reminder_service'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                id: idOrder,
            },
            success: function (data) {
                if (data.errors){
                    Swal.close();
                    showToast('error','!Espera!', 'Algo salió mal');
                }
                else{
                    Swal.close();
                    location.reload();
                }
            }
        });
    }

});
