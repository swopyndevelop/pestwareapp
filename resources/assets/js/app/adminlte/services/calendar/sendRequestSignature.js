import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idOrder, dynamicUrl, urlwhatsapp, hrefRequestSignature, dynamicUrlLocal;

    $('#sendRequestSignature').on('show.bs.modal', function(e) {
        Swal.close();
        idOrder = $(e.relatedTarget).data().id;
        urlwhatsapp = $(e.relatedTarget).data().urlwhatsapp;
        dynamicUrl = `https://pestwareapp.com/signature-service/${idOrder}`;
        dynamicUrlLocal = `http://localhost:8000//signature-service/${idOrder}`;
        hrefRequestSignature = document.getElementById('hrefRequestSignature');
        hrefRequestSignature.href = dynamicUrl;
    });

    $('#hrefSendRequestSignature').click(function () {
        let cellphone = document.getElementById('cellphoneCustomerSignature').value;
        if (cellphone.length > 9) window.open(urlwhatsapp + cellphone + "&text=" + dynamicUrl, '_blank');
        else showToast('warning', 'Enviar Whatsapp', 'error al enviar favor de ingresar un número')

    });

});
