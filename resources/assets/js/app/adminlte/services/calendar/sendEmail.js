import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idOrder, email, emailInput, fileOrder, fileCertificate, fileStation, fileArea, showStation, showArea;

    $('#sendemail').on('show.bs.modal', function(e) {
        Swal.close();
        idOrder = $(e.relatedTarget).data().id;
        email = $(e.relatedTarget).data().email;
        showStation = $(e.relatedTarget).data().station;
        if (showStation === 1) {
            document.getElementById("showStation").style.display = "block";
            document.getElementById("fileStation").checked = "true";
        }
        showArea = $(e.relatedTarget).data().area;
        if (showArea === 1) {
            document.getElementById("showArea").style.display = "block";
            document.getElementById("fileArea").checked = "true";
        }
         if (email.length === 0) {
            $("#emailText").text('No hay ningún correo registrado.');
         }else{
            $("#emailText").val(email);
         }
    });

    $('#sendEmailOrder').click(function () {

        //get values input
        email = $('#emailText').val();
        emailInput = $('#email').val();
        fileOrder = document.getElementById('fileOrder').checked;
        fileCertificate = document.getElementById('fileCertificate').checked;
        fileStation = document.getElementById('fileStation').checked;
        fileArea = document.getElementById('fileArea').checked;
        if (emailInput.length === 0) {
            emailInput = 0;
        }

        if (email.length === 0) {
            email = 0;
        }

        if(email == 0 && emailInput == 0){
            showToast('warning','Correo no válido','Debes de ingresar un correo electrónico valido.');
        }
        else if (fileOrder === false && fileCertificate === false && fileStation === false && fileArea === false){
            showToast('warning','No se ha seleccionado Archivo','Debes de seleccionar al menos un Documento.');
        }
        else{
            sendEmailOrder();
        }

    });

    function sendEmailOrder(){
        loaderPestWare('');
            $.ajax({
                type: 'GET',
                url: route('mail_service'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: idOrder,
                    email: email,
                    emailInput: emailInput,
                    fileOrder: fileOrder,
                    fileCertificate: fileCertificate,
                    fileStation: fileStation,
                    fileArea: fileArea
                },
                success: function (data) {
                    if (data.errors){
                        Swal.close();
                        showToast('warning','Algo salió Mal','Error al enviar el correo')
                    }
                    else{
                        Swal.close();
                        showToast('success-redirect','Correo Enviado','El correo se envió correctamente','calendario');
                        $('.sendEmailOrder').prop('disabled', false);
                    }
                }
        });
    }

});
