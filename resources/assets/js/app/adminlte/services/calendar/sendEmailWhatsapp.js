import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idOrder, cellphoneServiceOrder, cellphoneMainServiceOrder, urlwhatsapp, urlService, urlOrder, urlinspection, urlArea,
    showStation, showArea, fileOrder, fileCertificate, fileStation, fileArea;

    $('#sendemailwhatsapp').on('show.bs.modal', function(e) {
        Swal.close();
        idOrder = $(e.relatedTarget).data().id;
        urlwhatsapp = $(e.relatedTarget).data().urlwhatsapp;
        urlService = $(e.relatedTarget).data().service;
        urlOrder = $(e.relatedTarget).data().order;
        urlinspection = $(e.relatedTarget).data().urlinspection;
        if(urlinspection === undefined) urlinspection = " ";
        urlArea = $(e.relatedTarget).data().urlarea;
        if(urlArea === undefined) urlArea = " ";
        cellphoneServiceOrder = $(e.relatedTarget).data().cellphone;
        cellphoneMainServiceOrder = $(e.relatedTarget).data().cellphonemain;
        showStation = $(e.relatedTarget).data().station;
        if (showStation === 1) {
            document.getElementById("showStationWhatsapp").style.display = "block";
            document.getElementById("fileStationWhatsapp").checked = "true";
        } else {
            document.getElementById("showStationWhatsapp").style.display = "none";
            document.getElementById("fileStationWhatsapp").checked = "false";
        }
        showArea = $(e.relatedTarget).data().area;
        if (showArea === 1) {
            document.getElementById("showAreaWhatsapp").style.display = "block";
            document.getElementById("fileAreaWhatsapp").checked = "true";
        } else  {
            document.getElementById("showAreaWhatsapp").style.display = "none";
            document.getElementById("fileAreaWhatsapp").checked = "false";
        }
         $("#cellphoneServiceOrder").val(cellphoneServiceOrder);
         $("#cellphoneMainServiceOrder").val(cellphoneMainServiceOrder);
    });

    $('#sendEmailOrderWhatsapp').click(function () {
        let cellphone = document.getElementById('cellphoneServiceOrder').value;
        let cellphoneMain = document.getElementById('cellphoneMainServiceOrder').value;
        let cellphoneOther = document.getElementById('cellphoneOtherServiceOrder').value;
        fileOrder = document.getElementById('fileOrderWhatsapp').checked;
        fileCertificate = document.getElementById('fileCertificateWhatsapp').checked;
        fileStation = document.getElementById('fileStationWhatsapp').checked;
        if (urlinspection === " ") fileStation = false;
        fileArea = document.getElementById('fileAreaWhatsapp').checked;
        if (urlArea === " ") fileArea = false;

        if (fileOrder === true && fileCertificate === true && fileStation === true && fileArea === true) {
            let urlPdf = urlService + " " + urlOrder + " " + urlinspection + " " + urlArea;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1 ) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
        }

        if (fileOrder === true && fileCertificate === true && fileStation === true && fileArea === false) {
            let urlPdf = urlService + " " + urlOrder + " " + urlinspection;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlPdf, '_blank');
        }

        if (fileOrder === true && fileCertificate === true && fileStation === false && fileArea === false) {
            let urlPdf = urlService + " " + urlOrder;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + "&text=" + cellphoneOther + urlPdf, '_blank');
        }

        if (fileOrder === true && fileCertificate === false && fileStation === false && fileArea === false) {
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlOrder, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlOrder, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + "&text=" + cellphoneOther + urlOrder, '_blank');
        }

        if (fileOrder === false && fileCertificate === true && fileStation === false && fileArea === false) {
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlService, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlService, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlService, '_blank');
        }

        if (fileOrder === false && fileCertificate === false && fileStation === true && fileArea === false) {
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlinspection, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlinspection, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlinspection, '_blank');
        }

        if (fileOrder === false && fileCertificate === false && fileStation === false && fileArea === true) {
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlArea , '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlArea, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlArea, '_blank');
        }

        if (fileOrder === false && fileCertificate === false && fileStation === true && fileArea === true) {
            let urlPdf = urlinspection + " " + urlArea;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlPdf, '_blank');
        }
        if (fileOrder === false && fileCertificate === true && fileStation === true && fileArea === false) {
            let urlPdf = urlService + " " + urlinspection;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlPdf, '_blank');
        }

        if (fileOrder === true && fileCertificate === true && fileStation === false && fileArea === true) {
            let urlPdf = urlOrder + " " + urlService + " " + urlArea;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlPdf, '_blank');
        }

        if (fileOrder === false && fileCertificate === true && fileStation === true && fileArea === true) {
            let urlPdf = urlService + " " + urlinspection + " " + urlArea;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlPdf, '_blank');
        }

        if (fileOrder === true && fileCertificate === false && fileStation === true && fileArea === true) {
            let urlPdf = urlOrder + " " + urlinspection + " " + urlArea;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlPdf, '_blank');
        }

        if (fileOrder === false && fileCertificate === true && fileStation === false && fileArea === true) {
            let urlPdf = urlService + " " + urlArea;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlPdf, '_blank');
        }

        if (fileOrder === true && fileCertificate === false && fileStation === false && fileArea === true) {
            let urlPdf = urlOrder + " " + urlArea;
            if (cellphone.length > 1) window.open(urlwhatsapp + cellphone + "&text=" + urlPdf, '_blank');
            if (cellphoneMain.length > 1) window.open(urlwhatsapp + cellphoneMain + "&text=" + urlPdf, '_blank');
            if (cellphoneOther.length > 1) window.open(urlwhatsapp + cellphoneOther + "&text=" + urlPdf, '_blank');
        }

        if (cellphone === '' && cellphoneMain === '' && cellphoneOther === '') {
            showToast('warning','Error al enviar','No hay ningún número registrado')
        }
        else {
            $('#sendemailwhatsapp').click();
        }

    });

});
