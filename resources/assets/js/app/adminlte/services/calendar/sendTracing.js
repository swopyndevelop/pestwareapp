import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){
    let idOrder;

    $('#tracing').on('show.bs.modal', function(e) {
        Swal.close();
        idOrder = $(e.relatedTarget).data().id;
    });

    $("#saveTracingOrder").on("click",function() {
        loaderPestWare('');
        $.ajax({
            type: 'POST',
            url: route('tracing_store',idOrder),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (data) {
                if (data.errors){
                    Swal.close();
                    showToast('warning','Algo salió Mal','Error al enviar');
                }
                else{
                    Swal.close();
                    showToast('success-redirect','Seguimiento','Datos Guardados Correctamente','calendario');
                    $('.saveTracingOrder').prop('disabled', false);
                }
            }
        });
    });

});
