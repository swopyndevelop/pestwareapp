import flatpickr from "flatpickr"
import {configPicker} from "../../../customschedule/configDatePickers";
import * as moment from "moment";
import {loaderPestWare} from "../../../pestware/loader";
import {showToast} from "../../../pestware/alerts";

$(document).ready(function(){

    let dateStartE = $('#dateStartE');
    let dateEndE = $('#dateEndE');
    let idEvent = $('#id_event').val();
    let selectTechnicianE = $('#techniciansEventE');
    let techniciansAuxiliaries = $('#techniciansAuxiliariesEventEdit');

    const startDate =  dateStartE.val().split(' ').shift();
    const endDate =  dateEndE.val().split(' ').shift();
    const startHour =  dateStartE.val().split(' ').pop();
    const endHour =  dateEndE.val().split(' ').pop();

    loadTechniciansAvailableUpdate(startDate, endDate, startHour, endHour, idEvent, false);

    let fpStart = flatpickr("#dateStartE", configPicker);
    let fpEnd = flatpickr("#dateEndE", configPicker);

    fpStart.config.onClose.push(function (selectedDates, dateStr) {
        const date = moment(dateStr);
        const startDate = date.format('YYYY-MM-DD');
        const startHour = date.format('H:mm');
        const endHour =  dateEndE.val().split(' ').pop();
        fpEnd.setDate(`${startDate} ${endHour}`);
        loadTechniciansAvailableUpdate(startDate, startDate, startHour, endHour, idEvent, false);
    });

    fpEnd.config.onClose.push(function (selectedDates, dateStr) {
        const date = moment(dateStr);
        const endDate = date.format('YYYY-MM-DD');
        const endHour = date.format('H:mm');
        const startDate =  dateStartE.val().split(' ').shift();
        const startHour =  dateStartE.val().split(' ').pop();
        loadTechniciansAvailableUpdate(startDate, endDate, startHour, endHour, idEvent, false);
    });

    $('#showSelectAditionalTechniciansEdit').click(function() {
        document.getElementById('rowSelectAditionalTechniciansEdit').style.display = 'block';
        loadTechniciansAvailableUpdate(startDate, endDate, startHour, endHour, idEvent, true);
    });
    $('#hideSelectAditionalTechniciansEdit').click(function() {
        document.getElementById('rowSelectAditionalTechniciansEdit').style.display = 'none';
        //$("#techniciansAuxiliariesEventEdit").empty();
    });

    function loadTechniciansAvailableUpdate(initialDate, finalDate, initialHour, finalHour, eventId, isSetSelectTwo) {
        const technicianId = document.getElementById('id_technician').value;
        loaderPestWare('Buscando técnicos disponibles...');
        $.ajax({
            type: 'POST',
            url: route('load_technicians_available'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                initialDate: initialDate,
                finalDate: finalDate,
                initialHour: initialHour,
                finalHour: finalHour,
                id_event: eventId
            },
            success: function (data) {
                if (data.errors) {
                    Swal.close();
                    showToast('error', 'Algo salio mal', 'Intenta de nuevo más tarde.')
                } else {
                    if (data.length === 0) {
                        Swal.close();
                        if (!isSetSelectTwo) selectTechnicianE.empty();
                        if (isSetSelectTwo) techniciansAuxiliaries.empty();
                        showToast('warning', 'No hay Técnicos Disponibles', 'Por favor selecciona otro horario.');
                    } else {
                        if (!isSetSelectTwo) selectTechnicianE.empty();
                        if (isSetSelectTwo) techniciansAuxiliaries.empty();
                        data.forEach((element) => {
                            if (!isSetSelectTwo) selectTechnicianE.append(`<option value="${element.id}" ${technicianId == element.id ? 'selected' : ''}>${element.name}</option>`);
                            if (isSetSelectTwo) techniciansAuxiliaries.append(`<option value="${element.id}" ${technicianId == element.id ? 'selected' : ''}>${element.name}</option>`);
                        });

                        Swal.close();
                    }
                }
            }
        });
    }

    $('#saveEventE').click(function() {

        //get values inputs address
        let dateStartE = $('#dateStartE');
        let dateEndE = $('#dateEndE');
        let selectTechnicianE = $('#techniciansEventE option:selected').val();
        let techniciansAuxiliaries = $('#techniciansAuxiliariesEventEdit').val();
        let title = $('#titleE').val();
        let idEvent = $('#id_event').val();

        const startDate =  dateStartE.val().split(' ').shift();
        const endDate =  dateEndE.val().split(' ').shift();
        const startHour =  dateStartE.val().split(' ').pop();
        const endHour =  dateEndE.val().split(' ').pop();

        // update event in database
        loaderPestWare('');
        $.ajax({
            type: 'POST',
            url: route('update_event_drop'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                dateStart: startDate,
                dateEnd: endDate,
                hourStart: startHour,
                hourEnd: endHour,
                techniciansEvent: selectTechnicianE,
                techniciansAuxiliaries: techniciansAuxiliaries,
                idEvent: idEvent,
                title: title
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error', 'Error al actualizar', data.message);
                } else {
                    Swal.close();
                    showToast('success-redirect', 'Servicio actualizado', data.message, 'calendario');
                }
            }
        });

    });

});