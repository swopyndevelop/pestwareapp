import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idOrder;
    let url;
    let cellphoneCustomer;
    let cellphoneMain;

    $('#sendReminderWhatsappIndications').on('show.bs.modal', function(e) {
        swal.close();
        idOrder = $(e.relatedTarget).data().id;
        url = $(e.relatedTarget).data().url;
        cellphoneCustomer = $(e.relatedTarget).data().cellphonecustomer;
        $('#cellphoneClientIndications').val(cellphoneCustomer);
        cellphoneMain = $(e.relatedTarget).data().cellphonemain;
        $('#cellphoneMainIndications').val(cellphoneMain);

        $("#sendReminderWhatsappIndicationsSave").click(function() {
            cellphoneCustomer = document.getElementById('cellphoneClientIndications').value;
            cellphoneMain = document.getElementById('cellphoneMainIndications').value;
            const cellphoneOtherQuotation = document.getElementById('cellphoneOtherIndications').value;
            let urlSeparate = url.split('{');
            let urlCompleteCustomer = `${urlSeparate[0]}${cellphoneCustomer}${urlSeparate[1]}`;
            let urlCompleteMain = `${urlSeparate[0]}${cellphoneMain}${urlSeparate[1]}`;
            let urlCompleteOther = `${urlSeparate[0]}${cellphoneOtherQuotation}${urlSeparate[1]}`;
            if (cellphoneCustomer !== '') window.open(urlCompleteCustomer, '_blank');
            if (cellphoneMain !== '') window.open(urlCompleteMain, '_blank');
            if (cellphoneOtherQuotation !== '') window.open(urlCompleteOther, '_blank');

            sendWhatsappOrder();
        });
    });

    function sendWhatsappOrder(){
        loaderPestWare('');
        $.ajax({
            type: 'PUT',
            url: route('whatsapp_service'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                id: idOrder
            },
            success: function (data) {
                if (data.errors){
                    Swal.close();
                    showToast('error','!Espera!', 'Algo salió mal');
                }
                else{
                    Swal.close();
                    location.reload();
                }
            }
        });
    }

});
