import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){
    let idOrder;

    $('#warranty').on('show.bs.modal', function(e) {
        Swal.close();
        idOrder = $(e.relatedTarget).data().id;
    });

    $("#saveWarrantyOrder").on("click",function() {
        loaderPestWare('');
        $.ajax({
            type: 'POST',
            url: route('warranty_store',idOrder),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (data) {
                if (data.errors){
                    Swal.close();
                    showToast('warning','Algo salió Mal','Error al enviar');
                }
                else{
                    Swal.close();
                    showToast('success-redirect','Garantía','Datos Guardados Correctamente','calendario');
                    $('.saveWarrantyOrder').prop('disabled', false);
                }
            }
        });
    });

});
