import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";
import {checkbox} from "ionicons/icons";

$(document).ready(function(){

    // Global vars.
    let orderId;
    let customerId;
    const token = $("meta[name=csrf-token]").attr("content");
    const validateRfc = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/;

    $('#modalServiceInvoice').on('show.bs.modal', function(e) {

        orderId = $(e.relatedTarget).data().id;
        customerId = $(e.relatedTarget).data().customer;
        document.getElementById('orderFolioInvoice').innerText = $(e.relatedTarget).data().folio;
        calculateAmountsInvoice(orderId, false);
        getCustomerData(customerId);
    });

    // Events
    const btnCreateInvoice = document.getElementById('createInvoiceService');
    btnCreateInvoice.onclick = function () {
        let checkbox = document.getElementById('checkboxIvaAmount').checked;
        const billing = document.getElementById('billingCustomerMainNew').value;
        const rfc = document.getElementById('rfcCustomerMainNew').value;
        const email = document.getElementById('emailBillingCustomerMainNew').value;
        const address = document.getElementById('address').value;
        const exteriorNumber = document.getElementById('exteriorNumber').value;
        const interiorNumber = document.getElementById('interiorNumber').value;
        const postalCode = document.getElementById('postalCode').value;
        const colony = document.getElementById('colony').value;
        const municipality = document.getElementById('municipality').value;
        const state = document.getElementById('state').value;
        const cfdiType = document.getElementById('typeCfdiCustomerMainNew').value;
        const paymentMethod = document.getElementById('methodPayCustomerMainNew').value;
        const paymentWay = document.getElementById('wayPayCustomerMainNew').value;
        const bankAccount = document.getElementById('bankAccountsEdit').value;
        // Validations
        if (billing.length === 0) showToast('warning', 'Razón Social', 'Debes ingresar la razón social');
        else if (rfc.length === 0) showToast('warning', 'RFC', 'Debes ingresar un RFC');
        else if (!validateRfc.test(rfc)) showToast('warning','RFC','El RFC no cumple con el formato requerido de la facturación.');
        else if (cfdiType == 0) showToast('warning','Uso del Cfdi','Debes seleccionar el uso de la factura.');
        else {
            const customer = {
                _token: token,
                orderId: orderId,
                isIva: checkbox,
                billing: billing,
                rfc: rfc,
                email: email,
                address: address,
                exteriorNumber: exteriorNumber,
                interiorNumber: interiorNumber,
                postalCode: postalCode,
                colony: colony,
                municipality: municipality,
                state: state,
                cfdiType: cfdiType,
                paymentMethod: paymentMethod,
                paymentWay: paymentWay,
                bankAccountSelect: bankAccount
            };
            createInvoiceByOrderId(customer);
        }
    }

    const checkboxIvaAmount = document.getElementById('checkboxIvaAmount');
    checkboxIvaAmount.addEventListener('change', (event) => {
        let isIva = event.currentTarget.checked;
        calculateAmountsInvoice(orderId, isIva);
    });

    function createInvoiceByOrderId(customer) {
        loaderPestWare('');
        $.ajax({
            type: 'POST',
            url: route('create_service_invoice'),
            data: customer,
            success: function(response) {
                Swal.close();
                if (response.code === 500) {
                    Swal.fire({
                        title: '<strong>Error al generar la factura.</strong>',
                        type: 'error',
                        html: response.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        $('#modalServiceInvoice').click();
                    })
                }
                else if (response.code === 404) $('#modalPurchaseFolios').modal("show");
                else showToast('success-redirect', 'Factura', response.message, 'calendario');
            }
        });
    }

    function calculateAmountsInvoice(orderId, isIva) {
        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('calculate_amounts_order'),
            data: {
                _token: token,
                orderId: orderId,
                isIva: isIva
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) showToast('warning', 'Error', response.message);
                else {
                    const {subtotal, iva, total} = response;
                    document.getElementById('subtotalTest').innerText = `$${subtotal}`;
                    document.getElementById('ivaTest').innerText = `$${iva}`;
                    document.getElementById('totalTest').innerText = `$${total}`;
                }
            }
        });
    }

    function getCustomerData(customerId) {
        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('get_data_customer_invoice', customerId),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('warning', 'Error', response.message);
                }
                else {
                    const {
                        billing,
                        rfc,
                        billing_colony,
                        billing_exterior_number,
                        billing_interior_number,
                        billing_municipality,
                        billing_postal_code,
                        billing_state,
                        billing_street,
                        email_billing,
                        cfdi_type,
                        payment_method,
                        payment_way
                    } = response.customer;
                    document.getElementById('billingCustomerMainNew').value = billing;
                    document.getElementById('address').value = billing_street;
                    document.getElementById('exteriorNumber').value = billing_exterior_number;
                    document.getElementById('interiorNumber').value = billing_interior_number;
                    document.getElementById('postalCode').value = billing_postal_code;
                    document.getElementById('colony').value = billing_colony;
                    document.getElementById('municipality').value = billing_municipality;
                    document.getElementById('state').value = billing_state;
                    document.getElementById('rfcCustomerMainNew').value = rfc;
                    document.getElementById('emailBillingCustomerMainNew').value = email_billing;
                    getCatalogsTypeCfdiSAT(cfdi_type);
                    getCatalogsPaymentFormsSAT(payment_way);
                    getCatalogsPaymentMethodsSAT(payment_method);
                    getBankAccounts(customerId);
                    Swal.close();
                }
            }
        });
    }

    $("#purchaseFolios-12").click(function () {
        purchasePackageFolios(12);
    })

    $("#purchaseFolios-13").click(function () {
        purchasePackageFolios(13);
    })

    $("#purchaseFolios-14").click(function () {
        purchasePackageFolios(14);
    })

    $("#purchaseFolios-15").click(function () {
        purchasePackageFolios(15);
    })

    $("#purchaseFolios-16").click(function () {
        purchasePackageFolios(16);
    })

    $("#purchaseFolios-17").click(function () {
        purchasePackageFolios(17);
    })

    $("#purchaseFolios-18").click(function () {
        purchasePackageFolios(18);
    })

    $("#purchaseFolios-19").click(function () {
        purchasePackageFolios(19);
    })

    $('#addAccountBank').click(function () {

        const bank = document.getElementById('bank').value;
        const account = document.getElementById('account').value;

        if (bank.length <= 0) showToast('warning','Banco','Favor de ingresar un banco valido.');
        else if(account.length !== 4) showToast('warning', 'Número de Cuenta', 'Favor de ingresar los 4 últimos dígitos de la cuenta.');
        else saveNewBankAccount(bank, account);

    });

    function purchasePackageFolios(planId) {
        loaderPestWare('Adquiriendo Folios');
        $.ajax({
            type: 'POST',
            url: route('purchase_folios_billing'),
            data: {
                _token: token,
                package: planId,
            },
            success: function (data) {
                Swal.close();
                if (parseInt(data.code) === 500) {
                    Swal.fire({
                        title: '<strong>Error al comprar folios.</strong>',
                        type: 'error',
                        html: data.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        Swal.close()
                    })
                } else {
                    showToast('success', 'Folios', data.message);
                }
            }
        });
    }

    function getCatalogsTypeCfdiSAT(typeCfdiCustomer) {
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_type_cfdi'),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#typeCfdiCustomerMainNew');
                    select.empty();
                    const {typeCfdi} = response;
                    if (typeCfdiCustomer === null) {
                        select.append(`<option value="0" selected disabled>Seleccionar Uso de la Factura:</option>`);
                    }
                    typeCfdi.forEach((element) => {
                        if (typeCfdiCustomer === element.Value) {
                            select.append(`<option selected value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                        } else select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                    });
                }
            }
        });
    }

    function getCatalogsPaymentFormsSAT(paymentForm) {
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_payment_forms'),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#wayPayCustomerMainNew');
                    select.empty();
                    const {paymentForms} = response;
                    if (paymentForm === null) {
                        select.append(`<option value="0" selected disabled>Selecciona una Forma de Pago:</option>`);
                    }
                    paymentForms.forEach((element) => {
                        if (paymentForm === element.Value) {
                            select.append(`<option selected value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                        } else select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                    });
                }
            }
        });
    }

    function getCatalogsPaymentMethodsSAT(paymentMethod) {
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_payment_methods'),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#methodPayCustomerMainNew');
                    select.empty();
                    const {paymentMethods} = response;
                    if (paymentMethod === null) {
                        select.append(`<option value="0" selected disabled>Selecciona un Método de Pago:</option>`);
                    }
                    paymentMethods.forEach((element) => {
                        if (paymentMethod === element.Value) {
                            select.append(`<option selected value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                        } else select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                    });
                }
            }
        });
    }

    function getBankAccounts(customerId) {
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('catalogs_bank_accounts', customerId),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#bankAccountsEdit');
                    select.empty();
                    const {bankAccounts} = response;
                    bankAccounts.forEach((element) => {
                        if (element.is_default  == 1) {
                            select.append(`<option selected value="${element.id}">${element.bank} - ${element.account}</option>`);
                        } else select.append(`<option value="${element.id}">${element.bank} - ${element.account}</option>`);
                    });
                }
            }
        });
    }

    function saveNewBankAccount(bank, account) {
        loaderPestWare('Guardando cuenta...')
        $.ajax({
            type: 'POST',
            url: route('add_bank_accounts'),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                customerId: customerId,
                bank: bank,
                account: account
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Cuenta', data.message);
                    $('#addAccountBankModal').click();
                }
                else {
                    showToast('success', 'Cuenta Bancaria', 'Se registro la nueva cuenta.');
                    let select = $('#bankAccountsEdit');
                    select.empty();
                    const {bankAccounts} = data;
                    bankAccounts.forEach((element) => {
                        if (element.is_default  == 1) {
                            select.append(`<option selected value="${element.id}">${element.bank} - ${element.account}</option>`);
                        } else select.append(`<option value="${element.id}">${element.bank} - ${element.account}</option>`);
                    });
                    Swal.close();
                    $('#addAccountBankModal').click();
                }
                document.getElementById('bank').value = '';
                document.getElementById('account').value = '';
            }
        });
    }

});
