/**
 * @author Manuel Mendoza
 * @version 30/11/2020
 * register/_modalInvoice.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    // Global lets
    let invoices = [];

    // Load data open modal -->
    $('#allInvoiced').click(function (e) {
        e.preventDefault();
        let allIds = [];
        $('input:checkbox[name=ids]:checked').each(function () {
            allIds.push($(this).val());
        });
        if (allIds.length === 0) missingText('Debes seleccionar por lo menos un servicio.')
        else {
            getInvoiceSelected(allIds);
            $('#allInvoicedModal').modal('show');
            $('#allIdsInvoiced').val(allIds);
        }
    });

    function getInvoiceSelected(ids) {
        loaderPestWare('');
        $('#tableBodyDetailInvoices td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'POST',
            url: route('invoices_selected_data'),
            data: {
                _token: token,
                ids: ids
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableBodyDetailInvoices');
                    const {orders} = response;

                    orders.forEach((element) => {
                        const {id, id_service_order, invoiced_status} = element;
                        invoices.push({
                            id: id,
                            folio: id_service_order,
                            invoiced_status: invoiced_status
                        });

                        rows += `<tr>
                                    <td class="text-center text-primary text-bold">${id_service_order}</td>
                                    <td class="text-center"><input type="checkbox" id="isInvoice-${id}" value="${invoiced_status}" ${invoiced_status ? 'checked' : ''}></td>
                                </tr>`;
                    });
                    table.append(rows);
                    Swal.close();
                }
            }
        });
    }

    $('#saveInvoiceAll').on('click', function(event) {

        let dataInvoices = [];

        invoices.forEach((payment) => {
            let id = payment.id;
            let isInvoice = $(`#isInvoice-${payment.id}`).is(":checked") ? 1 : 2;
            dataInvoices.push({
                id: id,
                isInvoice: isInvoice,
            });
        });

        // save invoices with ajax
        saveInvoiceServicesAll(dataInvoices);

    });

    function saveInvoiceServicesAll(data) {
        loaderPestWare('Actualizando Facturas');
        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            url: route('add_invoices'),
            type: "POST",
            data: {
                ids: true,
                invoices: data
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Error al guardar',data.message);
                } else if (data.code == 201) {
                    Swal.close();
                    showToast('success-redirect','Facturas actualizadas.' ,data.message,'calendario')
                }
            }
        });
    }

});
