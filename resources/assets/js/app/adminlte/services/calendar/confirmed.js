import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idOrder, confirmed, label;

    $('#confirmedServiceOrder').on('show.bs.modal', function(e) {
        swal.close();
        idOrder = $(e.relatedTarget).data().id;
        confirmed = $(e.relatedTarget).data().confirmed;
        label =  $(e.relatedTarget).data().label;
        $('#labelConfirmed').html(label);
    });

    $("#btnConfirmedService").click(function () {
        confirmedEventOrder();
    });

    function confirmedEventOrder() {
        loaderPestWare('');
        $.ajax({
            type: 'PUT',
            url: route('confirmed_service'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                id: idOrder,
                confirmed: confirmed
            },
            success: function (data) {
                if (data.errors) {
                    Swal.close();
                    showToast('error', '!Espera!', 'Algo salió mal');
                } else {
                    Swal.close();
                    showToast('success-redirect', 'Status', 'Se guardo el cambió correctamente', 'calendario');
                    $('#btnConfirmedService').attr("disabled", true);
                    window.location.reload()
                }
            }
        });
    }

});
