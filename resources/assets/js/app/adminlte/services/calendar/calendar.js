/**
 * @author Alberto Martínez|Yomira Martínez
 * @version 01/04/2019
 * register/_calendar.blade.php
 */


import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function() {

    //Intro Js
    const introJsPest = document.getElementById('introJsInpunt').value;
    if (introJsPest == 1){
        $('#addSchedule').modal('show');
        startScheduleModal();
    }
    //startScheduleModal();
    function startScheduleModal(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: '#modalTitleSchedule',
                    title:"Agenda",
                    intro: "Agendar Cotización",
                },
                {
                    title: "Orden de Servicio",
                    intro: "Para programar un servicio en tu agenda es importante que asignes la Orden de Servicio a un Técnico " +
                        "por lo que deberás de llenar la siguiente información.",
                    position: 'center'
                },
                {
                    element: '#selectServicesDiv',
                    title: "Orden de Servicio",
                    intro: "Selecciona de la lista la orden previamente programada.",
                },
                {
                    element: '#title',
                    title:"Título",
                    intro: "Aquí podrás modificar el título del  evento en la agenda, automáticamente se llenará con el nombre del Cliente.",
                },
                {
                    element: '#selecttechniciansDiv',
                    title:"Técnicos disponibles",
                    intro: "Selecciona de la lista el técnico que realizará el servicio.",
                },
                {
                    element: '#saveEvent',
                    title:"Agendar",
                    intro: "Finalmente da click en el botón agendar.",
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo",
        });
        intro.start();
        intro.onexit(function() {
            updateAjaxIntroJs(0);
        });
        intro.setOption('Listo', 'Agendar').start().oncomplete(function() {
            intro.exit();
            $('#addSchedule').click();
            setTimeout(() => {
                startScheduleAfter();
            }, 1000);
        });
    }

    //startScheduleAfter();
    function startScheduleAfter(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: '#trScheduleOrderDiv',
                    title:"Información Orden de Servicio",
                    intro: "Nombre",
                    position: "bottom",
                },
                {
                    element: '#calendar',
                    title:"Agenda",
                    intro: "Ahora que ya asignaste el servicio a tu técnico solo es necesario que programes el día y la hora en la que " +
                        "se llevara a cabo el servicio. Selecciona la hora del día a programar e ingresa la hora de fin del servicio.",
                    position: "bottom"
                },
                {
                    element: '#tableQuotations',
                    title:"Estatus",
                    intro: "En la tabla de servicios encontrarás la columna de estatus en la cual se cambiará dependiendo del status de progración " +
                        " los cuales serán los siguientes: Programado, Comenzado, Cancelado, Finalizado.",
                    position: "left"
                },
                {
                    element: '#btnScheduleOrderService',
                    title:"Orden de Servicio",
                    intro: "PestWare App ofrece una serie de opciones para gestionar una orden de servicio. para conocerlas " +
                        "hacemos click en el folio de la orden de servicio; Se desplegará un menú con las siguientes opciones: " +
                        "",
                    position: "left",
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo",
            keyboardNavigation: false,
        });
        intro.start();
        intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
            setTimeout(() => {
                $('#btnScheduleOrderService').addClass('open');
                startUlMenuTableSchedule();
            }, 1000);
        });
    }
    //startUlMenuTableSchedule();
    function startUlMenuTableSchedule(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    title:"Opciones de Orden",
                    intro: `<div class="text-center">
                                <a href="#"> Enviar Indicaciones</a>
                            </div>
                            <div class="text-center">
                                <a href="#"> Enviar Recordatorio</a>
                            </div>
                            <div class="text-center">
                                <a href="#"> Confirmar</a>
                            </div>
                            <div class="text-center">
                                <a href="#"> Editar</a>
                            </div>
                            <div class="text-center">
                                <a href="#"> Cancelar</a>
                            </div>
                            <div class="text-center">
                                <a href="#"> Cobrar</a>
                            </div>
                            <hr>
                            <div class="text-center">
                                <button id="demoButton" class="btn">Comenzar Demo</button>
                            </div><br>
                            <p>A continuación, podrás ver una demostración de cómo realizar un servicio haciendo uso de nuestra aplicación móvil:</p>
                            `,
                },
            ],
            keyboardNavigation: false,
        });
        intro.start();
        document.getElementById('demoButton').onclick = function() {
            intro.exit();
            setTimeout(() => {
                startCellPhoneIntro();
            }, 1000);
        };
    }
    //startCellPhoneIntro();
    function startCellPhoneIntro(){
        let intro = introJs();
        const urlBase = "https://storage.pestwareapp.com/IntroCel/"
        intro.setOptions({
            steps: [
                {
                    title:"Estatus",
                    intro:
                        `<p>Una ves agedado la orden de servicio cambiará de estatus a programado.</p>`,
                },
                {
                    title:"Notificación",
                    intro: `<div class="text-center">
                                </div>
                                <p>Al técnico le llegará una notificación en su celular al instante una vez agendado el
                                servicio, así como un recordatorio 45 y 15 minutos antes de la hora del servicio.</p>
                                `,
                },
                {
                    title:"Servicio",
                    intro: `<div class="text-center">
                                <img src="${urlBase}intro1.jpg" width="70%" title="Servicio">
                            </div>
                                <p>Cuando el Técnico entra a su sesión en la app móvil aparecerán todos los servicios a realizar en el día. Cada tarjeta 
                                muestra la información general del servicio como son Nombre del cliente, domicilio, teléfono precio, tipo de plaga, fecha y hora. 
                                Al presionar la tarjeta se abre el Detalle del Servicio para poder inicializarlo..</p>
                            `,
                },
                {
                    title:"Detalle del Servicio",
                    intro: `<div class="text-center">
                                <img src="${urlBase}intro3.jpg" width="70%" title="Detalle del Servicio">
                            </div>
                            <p>En esta sección se muestra la información general así como las condiciones del cliente y comentarios respecto al servicio a realizar, 
                            se cuenta con la función de marcación directa seleccionando el teléfono se abrirá automáticamente la pantalla para llamarle al cliente, 
                            de igual manera al seleccionar el domicilio se abrirá automáticamente Google Maps con las indicaciones para llegar a su domicilio. En 
                            esta sección se puede cancelar el servicio en caso que el cliente no se encuentre o bien iniciarlo.</p>
                            `,
                },{
                    title:"Comenzar Servicio",
                    intro: `<div class="text-center">
                                <img src="${urlBase}intro5.jpg" width="70%" title="Comenzar Servicio">
                            </div>
                            <p>Una vez iniciado el servicio se mostrará el formulario MIP el cual cuenta con 5 secciones las cuales deberán de ser llenadas por el 
                            técnico para generar el informe respectivo, las secciones son: Inspección del Lugar, Condiciones del Lugar, Control de Plagas, Pago del Servicio 
                            y Firma del Cliente.</p>
                            `,
                },{
                    title:"Inspección del lugar",
                    intro: `<div class="text-center">
                                <img src="${urlBase}intro6.jpg" width="70%" title="Inspección del lugar">
                            </div>
                                <p>En esta sección el técnico realizará la inspección del lugar, registrando las plagas encontradas junto con el grado de infestación, así como
                                 las áreas de anidamiento, podrá tomar fotografías de evidencia y opcionalmente ingresar algún comentario.</p>
                            `,
                },{
                    title:"Condiciones del lugar",
                    intro: `<div class="text-center">
                                <img src="${urlBase}intro7.jpg" width="70%" title="Condiciones del lugar">
                            </div>
                            <p>En esta sección el técnico registrará las condiciones en las que se encuentre el lugar, así como accesos
                            restringidos o comentarios. En cada una de las secciones puede hacer uso de la cámara o galería de fotos
                            para subir evidencias correspondientes.</p>
                            `,
                },
                {
                    title:"Control de plagas",
                    intro: `<div class="text-center">
                                <img src="${urlBase}intro8.jpg" width="70%" title="Control de plagas">
                            </div>
                            <p>Pestware App permite registrar múltiples áreas a controlar, anexando uno o más métodos de aplicación plaguicidas, 
                            dosis y cantidades aplicadas, fotografías de evidencia así como escaneo de códigos QR para el monitoreo de estaciones.</p>
                            `,
                },
                {
                    title:"Pago del servicio",
                    intro: `<div class="text-center">
                                <img src="${urlBase}intro9.jpg" width="70%" title="Pago del servicio">
                            </div>
                            <p>Pestware App te ayuda con la administración de tu negocio por lo que en esta sección podrá el técnico consultar y registrar el 
                            costo total del servicio, indicar la forma y método de pago, así como el monto recibido. En caso que no reciba el pago podrá marcarlo 
                            como no pagado para que se genere automáticamente el estatus de adeudo en la cartera de deudores.</p>`,
                },
                {
                    title:"Firma del cliente",
                    intro: `<div class="text-center">
                                    <img src="${urlBase}intro10.jpg" width="70%" title="Firma del cliente">
                                </div>
                                <p>Finalmente para concluir con el servicio, es necesario solicitar la firma de nuestro cliente, la cual se mostrará en el certificado. 
                                Para finalizar deberá pulsar el botón Finalizar servicio y en automático nos preguntará si deseamos enviar el certificado por correo 
                                electrónico en caso de haber sido registrado durante la cotización o programación o simplemente se podrá agregar uno o más correos en 
                                ese momento. Una vez concluido el servicio se podrá  enviar también el certificado por Whatsapp.</p>`,
                },
                {
                    element: '#trScheduleOrderDiv',
                    title:"Estatus",
                    intro: "En la tabla de servicios que se encuentra debajo del calendario podremos dar seguimiento a cada una de las Ordenes de Servicio y revisar el " +
                        "trabajo realizado en campo. En la columna de Estatus podremos ver si el servicio se encuentra programado, comenzado, finalizado o cancelado, así " +
                        "como si fue pagado, si le aplica garantía o si tiene adeudo.",
                    position: "left"
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo",
            keyboardNavigation: false,
        });
        intro.start();
        intro.setOption('Listo', 'Inventario').start().oncomplete(function() {
            intro.exit();

            updateAjaxIntroJs(1);

            setTimeout(() => {
                startInventory();
                //window.location.href = route('index_product');
            }, 1000);

        });
    }

    function startInventory(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: '#managmentMenu',
                    title:"Administrativo",
                    intro: "<div class='text-center'>" +
                        "<p>En esta sección cuentas con los módulos de Productos, Inventarios, Gastos y Cortes, Selecciona cada uno" +
                        " para conocer más de ellos.</p>" +
                        "<button class='btn' id='inventoryManagmentButton'>Administrativo</button>" +
                        "</div>",
                    position: "right"
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo",
            keyboardNavigation: false
        });
        intro.start();
        intro.onexit(function() {
            updateAjaxIntroJs(0);
        });
        document.getElementById('inventoryManagmentButton').onclick = function() {
            intro.exit();
            setTimeout(() => {
                $('#liMangmentSide').addClass('active');
                setTimeout(() => {
                    $('#liMangmentSideOpen').addClass('menu-open');
                    setTimeout(() => {
                        ulInventoryTheoric();
                    }, 500);
                }, 150);
            }, 150);
        }
    }

    //ulInventoryTheoric();
    function ulInventoryTheoric(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: '#liInventorySide',
                    title:"Inventario",
                    intro: "<div class='text-center'>" +
                        "<p>En esta sección podrás Controlar todo lo Relacionado a tu Inventario registrando Entradas de Producto a tu " +
                        "Almacén Principal, Realizar Traspasos a los Almacenes de tus Técnicos, Consultar las existencias, y Realizar Ajustes de Inventarios.</p>" +
                        "<button class='btn' id='inventoryTheoricButton'>Inventario</button></div>",
                    position: "right"
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo"
        });
        intro.start();
        document.getElementById('inventoryTheoricButton').onclick = function() {
            intro.exit();
            setTimeout(() => {
                $('#liInventorySide').addClass('active');
                setTimeout(() => {
                    $('#liInventorySide').addClass('menu-open');
                    setTimeout(() => {
                        ulMovementsInventory();
                    }, 500);
                }, 150);
            }, 150);
        }
    }

    //ulMovementsInventory();
    function ulMovementsInventory(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: '#aProductsDiv',
                    title:"Productos",
                    intro: "Pestware App te ayuda con el control de tu inventario por lo que es indispensable contar con los productos que " +
                        "utilizas en tus aplicación, hemos cargado una gran cantidad de productos con sus fichas técnicas, hojas de seguridad y " +
                        "precio de mercado, sin embargo puedes editar estos productos en todos sus campos o bien crear productos nuevos que no " +
                        "se encuentren.",
                    position: "right"
                },
                {
                    element: '#aProductsDiv',
                    title:"Productos",
                    intro: "PestWare App te brinda un amplio catálago de productos que puedes usar para tus primeros servicios," +
                        " sin embargo puedes agregar nuevos desde esta opción.",
                    position: "right"
                },
                {
                    element: '#aEntryDiv',
                    title:"Entrada de Producto",
                    intro: "Al momento de tu configuración inicial deberás de registrar todo el producto que tengas en tu Almacén principal (Oficinas o Bodega), " +
                        "así como el que tengan tu Técnicos. Posteriormente cada vez que hagas una compra a tu proveedor y te llegue la mercancía la registraras",
                    position: "right"
                },
                {
                    element: '#aTransferDiv',
                    title:"Traspaso de Producto",
                    intro: "PestWare App te permite controlar y monitorear de manera precisa y detallada cada uno de los " +
                        "consumos  que genera tus técnicos, es por ello que es necesario realizar traspasos entre tus diferentes tipos " +
                        "de almacenes (Centros de Trabajo, empleados y clientes).",
                    position: "right"
                },
                {
                    element: '#aInventoryTheoricDiv',
                    title:"Inventario Teórico",
                    intro: "En esta sección podrás consultar que productos, cantidad y valor tiene cada almacén, recuerda que en el caso de los técnicos cada vez que " +
                        "realizan un servicio ingresan el producto, dosis, cantidad utilizado y Pestware App automáticamente le descuenta ese producto utilizado " +
                        "de su inventario. En esta sección podrás revisar cada uno de los movimientos.",
                    position: "right"
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo",
            keyboardNavigation: false,
        });
        intro.start();
        intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
            intro.exit();

            updateAjaxIntroJs(1);
            setTimeout(() => {
                //startModalProduct();
                window.location.href = route('index_product');
            }, 1000);
        });
    }

    function updateAjaxIntroJs(introjs){
        $.ajax({
            type: 'POST',
            url: route('change_introjs_user'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                introjs: introjs
            },
            success: function(data) {
                //console.log('OK');
            }
        });
    }
    //End Intro Js

    let idJobCenterGlobal = $('#selectJobCenterCalendar').val();
    let idOrderGlobal = $('#idOrder').val();
    let initialDateFilter;
    let finalDateFilter;
    let initialDateEvent;
    let finalDateEvent;

    $('#selectJobCenterCalendar').on('change', function() {
        let idJobCenter = $(this).val();
        window.location.href = route('calendario', [0, 0, 0, idJobCenter]);
    });

    $('#selectCustomersCalendar').on('change', function() {
        let idCustomer = $(this).val();
        loaderPestWare('Filtrando por cliente...');
        window.location.href = route('calendario', [0, idCustomer, 0, idJobCenterGlobal]);
    });

    $('#selectTechniciansCalendar').on('change', function() {
        let idEmployee = $(this).val();
        loaderPestWare('Filtrando por técnico...');
        window.location.href = route('calendario', [0, 0, idEmployee, idJobCenterGlobal]);
    });

    $('#showSelectAditionalTechnicians').click(function() {
        document.getElementById('rowSelectAditionalTechnicians').style.display = 'block';
    });
    $('#hideSelectAditionalTechnicians').click(function() {
        document.getElementById('rowSelectAditionalTechnicians').style.display = 'none';
    });

    // Filters table
    /* $(function() {
        $('input[name="filterDateCreated"]').daterangepicker({
            opens: 'left',
            autoApply: true
        }, function(start, end, label) {
            initialDateFilter = start.format('YYYY-MM-DD');
            finalDateFilter = end.format('YYYY-MM-DD');
        });
    }); */

    $(function() {

        $('input[name="filterDateCreated"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateCreated"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateFilter = picker.startDate.format('YYYY-MM-DD');
            finalDateFilter = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateCreated"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    $(function() {

        $('input[name="filterDateEvent"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateEvent"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateEvent = picker.startDate.format('YYYY-MM-DD');
            finalDateEvent = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateEvent"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    $('#btnFilter').click(function() {
        let filterPaginate = $('#filterPaginate option:selected').val();
        let idEmployee = $('#selectFilterTechnician option:selected').val();
        let customerName = $('#selectFilterCustomerName option:selected').val();
        let customerPhone = $('#selectFilterCustomerPhone option:selected').val();
        let customerAddress = $('#selectFilterAddress option:selected').val();
        let key = $('#filterKey option:selected').val();
        let source = $('#selectFilterSource option:selected').val();
        let payment = $('#filterPayment option:selected').val();
        let status = $('#filterStatus option:selected').val();

        initialDateFilter = initialDateFilter == undefined ? null : initialDateFilter;
        finalDateFilter = finalDateFilter == undefined ? null : finalDateFilter;
        initialDateEvent = initialDateEvent == undefined ? null : initialDateEvent;
        finalDateEvent = finalDateEvent == undefined ? null : finalDateEvent;

        window.location.href = `/service/schedule?initialDateCreated=${initialDateFilter}&finalDateCreated=${finalDateFilter}
            &initialDateEvent=${initialDateEvent}&finalDateEvent=${finalDateEvent}&idEmployee=${idEmployee}
            &customerName=${customerName}&customerPhone=${customerPhone}&customerAddress=${customerAddress}&key=${key}
            &idSource=${source}&payment=${payment}&status=${status}&forPage=${filterPaginate}&export=false`;
    });

    $('#btnExport').click(function() {
        loaderPestWare('Exportando Reporte...');
        setTimeout(() => {
            Swal.close();
        }, 5000);
        let filterPaginate = $('#filterPaginate option:selected').val();
        let idEmployee = $('#selectFilterTechnician option:selected').val();
        let customerName = $('#selectFilterCustomerName option:selected').val();
        let customerPhone = $('#selectFilterCustomerPhone option:selected').val();
        let customerAddress = $('#selectFilterAddress option:selected').val();
        let key = $('#filterKey option:selected').val();
        let payment = $('#filterPayment option:selected').val();
        let status = $('#filterStatus option:selected').val();

        initialDateFilter = initialDateFilter == undefined ? null : initialDateFilter;
        finalDateFilter = finalDateFilter == undefined ? null : finalDateFilter;
        initialDateEvent = initialDateEvent == undefined ? null : initialDateEvent;
        finalDateEvent = finalDateEvent == undefined ? null : finalDateEvent;

        window.location.href = `/service/schedule?initialDateCreated=${initialDateFilter}&finalDateCreated=${finalDateFilter}
            &initialDateEvent=${initialDateEvent}&finalDateEvent=${finalDateEvent}&idEmployee=${idEmployee}
            &customerName=${customerName}&customerPhone=${customerPhone}&customerAddress=${customerAddress}&key=${key}
            &payment=${payment}&status=${status}&forPage=${filterPaginate}&export=true`;
    });

    $('#ordersEvent').on('change', function() {
        let title = $(this).find(':selected').data('client');
        let order = $(this).find(':selected').data('order');
        let type = '';
        if (order.charAt(0) === 'O') {
            type = 'Fumigación de ';
        } else if (order.charAt(0) === 'G') {
            type = 'Garantía de ';
        } else if (order.charAt(0) === 'R') {
            type = 'Refuerzo de ';
        } else if (order.charAt(0) === 'I') {
            type = 'Inspección de ';
        } else {
            type = 'Seguimiento de ';
        }
        $('#title').val(type + title);
    });

    $('#saveEvent').click(function() {

        //get values inputs address
        let dateStart = $('#dateStart').val();
        let dateEnd = $('#dateEnd').val();
        let hourStart = $('#hourStart').val();
        let hourEnd = $('#hourEnd').val();
        let techniciansEvent = $('#techniciansEvent').val();
        let techniciansAuxiliariesEvent = $('#techniciansAuxiliariesEvent').val();
        let idOrder = $('#ordersEvent').val();
        let title = $('#title').val();

        //console.log(idOrder);

        if (title.length === 0) {
            missingText('Ingresa el título que se mostrara en el calendario.');
        } else if (idOrder === null) {
            missingText('Selecciona una OS/G/R/S.');
        } else if (techniciansEvent === null) {
            missingText('Selecciona un técnico disponible.');
        } else {
            saveNewEvent();
        }

        //Save events in database
        function saveNewEvent() {
            loaderPestWare('');
            $.ajax({
                type: 'POST',
                url: route('event_store'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    dateStart: dateStart,
                    dateEnd: dateEnd,
                    hourStart: hourStart,
                    hourEnd: hourEnd,
                    techniciansEvent: techniciansEvent,
                    techniciansAuxiliariesEvent: techniciansAuxiliariesEvent,
                    idOrderService: idOrder,
                    title: title
                },
                success: function(data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error', 'Error al actualizar', data.message);
                    }
                    else {
                        Swal.close();
                        showToast('success-redirect','Agendación de Servicio', data.message,'calendario')
                    }
                }
            });
        }
    });

    $('#updateEvent').click(function() {

        //get values inputs address
        let dateStart = $('#dateStartUpdate').val();
        let dateEnd = $('#dateEndUpdate').val();
        let hourStart = $('#hourStartUpdate').val();
        let hourEnd = $('#hourEndUpdate').val();
        let techniciansEvent = $('#techniciansEventUpdate').val();
        let idOrder = $('#orderEventUpdate').val();
        let title = $('#titleUpdate').val();

        // update event in database
        loaderPestWare('');
        $.ajax({
            type: 'POST',
            url: route('update_event_drop'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                dateStart: dateStart,
                dateEnd: dateEnd,
                hourStart: hourStart,
                hourEnd: hourEnd,
                techniciansEvent: techniciansEvent,
                idEvent: idOrder,
                title: title
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error', 'Error al actualizar', data.message);
                } else {
                    Swal.close();
                    showToast('success-redirect', 'Servicio actualizado', data.message, 'calendario');
                }
            }
        });

    });

});

//alerts messages

function missingText(textError) {
    swal({
        title: "¡Espera!",
        type: "error",
        text: textError,
        icon: "error",
        timer: 3000,
        showCancelButton: false,
        showConfirmButton: false
    });
}
