import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idOrder;
    let raiting = 0;

    $("#rateBox").rate({
        length: 5,
        value: 0,
        readonly: false,
        size: '40px',
        selectClass: 'fxss_rate_select',
        incompleteClass: 'fxss_rate_no_all_select',
        customClass: 'custom_class',
        callback: function (object) {
            raiting = object.index;
        }
    });

    $('#raiting').on('show.bs.modal', function (e) {
        Swal.close();
        idOrder = $(e.relatedTarget).data().id;
    });

    $('#saveRating').click(function () {

        //get values input
        let comments = $('#commentsCustomer').val();
        if (comments.length < 1) showToast('warning','Comentarios','Los comentarios son obligatorios');
        else {
            loaderPestWare('');
            $.ajax({
                type: 'POST',
                url: route('store_raiting'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    idOrder: idOrder,
                    comments: comments,
                    raiting: ++raiting
                },
                success: function (data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error','!Espera!', 'Algo salió mal');
                    } else {
                        Swal.close();
                        showToast('success-redirect','Evaluación', 'Calificación enviada correctamente','calendario');
                        $("#saveRating").attr('disabled', true);
                    }
                }
            });
        }

    });

});
