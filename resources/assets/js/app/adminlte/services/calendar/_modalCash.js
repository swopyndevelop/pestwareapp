/**
 * @author Manuel Mendoza
 * @version 30/11/2020
 * register/_modalCash.blade.php
 */

import {expressions, getFields, setFields, validateField} from "../../validations/regex";
import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";
import * as moment from "moment";

$(document).ready(function(){

    // Global lets
    let payments = [];
    let paymentmethod, paymentway, payday, isupdate, idevent, idorder, total, serviceorder, amountreceived,
    commentaryCash, payment;

    // Load data open modal -->
    $('#newCashModal').on('show.bs.modal', function(e) {
        swal.close();
        paymentmethod = $(e.relatedTarget).data().paymentmethod;
        paymentway = $(e.relatedTarget).data().paymentway;
        getPaymentMethods(paymentmethod, paymentway);
        payday = $(e.relatedTarget).data().payday;
        const date = moment(payday);
        idevent = $(e.relatedTarget).data().idevent;
        idorder = $(e.relatedTarget).data().idorder;
        total = $(e.relatedTarget).data().total;
        serviceorder = $(e.relatedTarget).data().serviceorder;
        amountreceived = $(e.relatedTarget).data().amountreceived;
        commentaryCash = $(e.relatedTarget).data().commentary;
        payment = $(e.relatedTarget).data().payment;
        isupdate = $(e.relatedTarget).data().isupdate;

        $('#idEventCasheModal').val(idevent);
        $('#idOrderCasheModal').val(idorder);
        $('#labelTotalCashModal').html(`$${total}`);
        $('#idSo').val(serviceorder);
        $('#total').val(amountreceived);
        $('#commentary').html(commentaryCash);
        $('#payDay').val(date.format('YYYY-MM-DD'));

        if (payment == 1) document.getElementById("payment").checked = true;
        if (isupdate == 1) document.getElementById("grupo__payDay").style.display = "block";
    });

    $("#newCashModal").on("hidden.bs.modal", function () {
        document.getElementById("grupo__payDay").style.display = "none";
    });

    function getPaymentMethods(paymentId, paymentWayId) {
        loaderPestWare('');
        document.getElementById("payment").checked = false;
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('cash_payment_methods'),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error al cargar los datos', 'Algo salio mal, intente de nuevo');
                } else if (response.code === 200) {
                    // Load paymentMethods select
                    $("#paymentMethod").empty();
                    response.paymentMethods.forEach((element) => {
                        if (paymentId != element.id)
                            $("#paymentMethod").append(`<option value="${element.id}">${element.name}</option>`);
                        else $("#paymentMethod").append(`<option value="${element.id}" selected="true">${element.name}</option>`);
                    });
                    // Load paymentWay select
                    $("#paymentType").empty();
                    response.paymentWay.forEach((element) => {
                        if (paymentWayId != element.id)
                            $("#paymentType").append(`<option value="${element.id}">${element.name}</option>`);
                        else $("#paymentType").append(`<option value="${element.id}" selected="true">${element.name}</option>`);
                    });
                    Swal.close();
                }
            }
        });
    }


    // Validations -->
    const inputs = document.querySelectorAll('#formCashService input');
    const commentary = document.getElementById('commentary');

    const fieldsCashServiceForm = {
        modal: false,
        commentary: true
    }

    setFields(fieldsCashServiceForm);

    const validateForm = (e) => {
        switch (e.target.name) {
            case "total":
                validateField(expressions.totalDecimal, e.target.value, 'total');
                break;
            case "commentary":
                validateField(expressions.descriptionNotRequired, e.target.value, 'commentary');
                break;
        }
    }

    commentary.addEventListener('keyup', validateForm);
    commentary.addEventListener('blur', validateForm);

    inputs.forEach((input) => {
        input.addEventListener('keyup', validateForm);
        input.addEventListener('blur', validateForm);
    });

    // New Cash Service
    $('#formCashService').on('submit', function(event) {

        event.preventDefault();

        //Declarations validations Expenses
        let total = $('#total').val();
        let commentary = $('#commentary').val();

        validateField(expressions.totalDecimal, total, 'total');
        validateField(expressions.descriptionNotRequired, commentary, 'commentary');

        if(getFields().total && getFields().commentary){
            saveCashService();
        }

        else {
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }
    });

    function saveCashService() {
        loaderPestWare('Guardando Cobro');
        let fd = new FormData(document.getElementById("formCashService"));

        $.ajax({
            url: route('cash_add'),
            type: "POST",
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Error al guardar','Algo salió mal, intenta de nuevo');
                } else if (data.code == 201) {
                    Swal.close();
                    showToast('success-redirect','Pago actualizado','Se guardaron los datos correctamente','calendario')
                    $("#charge").attr("disabled", true);
                }
            }
        });
    }

    // All payments

    // Load data open modal -->
    $('#allPayment').click(function (e) {
        e.preventDefault();
        let allIds = [];
        $('input:checkbox[name=ids]:checked').each(function () {
            allIds.push($(this).val());
        });
        if (allIds.length === 0) missingText('Debes seleccionar por lo menos un servicio.')
        else {
            getPaymentSelected(allIds);
            $('#allPaymentModal').modal('show');
            $('#allIdsPayment').val(allIds);
        }
    });

    $('#savePaymentAll').on('click', function(event) {

        let dataPayments = [];

        payments.forEach((payment) => {
            let id = payment.id;
            let paymentMethod = $(`#paymentMethod-${payment.id} option:selected`).val();
            let paymentType = $(`#paymentWay-${payment.id} option:selected`).val();
            let total = $(`#total-${payment.id}`).val();
            let isPayment = $(`#isPayment-${payment.id}`).is(":checked") ? 1 : 2;
            let paymentDate = $(`#date-${payment.id}`).val();
            dataPayments.push({
                id: id,
                paymentMethod: paymentMethod,
                paymentType: paymentType,
                total: total,
                isPayment: isPayment,
                paymentDate: paymentDate
            });
        });

        // save payments with ajax
        saveCashServicesAll(dataPayments);

    });

    function saveCashServicesAll(data) {
        loaderPestWare('Guardando Cobros');
        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            url: route('cash_add'),
            type: "POST",
            data: {
                ids: true,
                payments: data
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Error al guardar',data.message);
                } else if (data.code == 201) {
                    Swal.close();
                    showToast('success-redirect','Pagos actualizados.' ,data.message,'calendario')
                }
            }
        });
    }

    function getPaymentSelected(ids) {
        loaderPestWare('');
        $('#tableBodyDetailPayments td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'POST',
            url: route('payments_selected_data'),
            data: {
                _token: token,
                ids: ids
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableBodyDetailPayments');
                    const {orders, paymentMethods, paymentWay} = response;

                    orders.forEach((element) => {
                        const {id, id_service_order, ipm, ipw, total} = element;
                        payments.push({
                            id: id,
                            folio: id_service_order
                        });

                        let optionsMethods = '';
                        paymentMethods.forEach((pm) => {
                            optionsMethods += `<option value="${pm.id}" ${ipm === pm.id ? 'selected': ''}>${pm.name}</option>`;
                        });
                        let optionsWay = '';
                        paymentWay.forEach((pw) => {
                            optionsWay += `<option value="${pw.id}" ${ipw === pw.id ? 'selected': ''}>${pw.name}</option>`;
                        });

                        rows += `<tr>
                                <td class="text-center text-primary text-bold">${id_service_order}</td>
                                <td class="text-center">
                                    <select class="form-control" id="paymentMethod-${id}">${optionsMethods}</select>
                                </td>
                                <td class="text-center">
                                    <select class="form-control" id="paymentWay-${id}">${optionsWay}</select>
                                </td>
                                <td class="text-center"><input class="form-control" type="text" id="total-${id}" value="${total}"></td>
                                <td class="text-center"><input class="form-control" type="date" id="date-${id}" value="${moment().format('YYYY-MM-DD')}"></td>
                                <td class="text-center"><input type="checkbox" id="isPayment-${id}"></td>
                        </tr>`;
                    });
                    table.append(rows);
                    Swal.close();
                }
            }
        });
    }

});
