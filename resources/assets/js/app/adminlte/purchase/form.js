/**
 * @author Alberto Martínez
 * @version 10/04/2021
 * register/_quotationCustomTest.blade.php
 */

import {loaderPestWare} from "../pestware/loader";

$(document).ready(function() {

    //autocomplete provider name
    $('#company').keyup(function () {
        let query = $(this).val();
        let jobCenter = $('#selectJobCenterAccounting').val();
        if (query != ''){
            $.ajax({
                type: 'GET',
                url: route('autocomplete_company'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    query: query,
                    jobCenter: jobCenter
                },
                success: function (data) {
                    $('#companyList').fadeIn();
                    $('#companyList').html(data);
                }
            });
        }
    });

    $(document).on('click', 'li#getdataCompany', function () {
        loaderPestWare('')
        $('#company').val($(this).text());
        $('#companyList').fadeOut();
        //cargar datos de proveedor
        let companyLoad = $('#company').val();
        $.ajax({
            type: 'GET',
            url: route('load_company'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                company: companyLoad
            },
            success: function (data) {
                $('#contactName').val(data.contact_name).text();
                $('#contactAddress').val(data.contact_address).text();
                $('#contactCellphone').val(data.contact_cellphone).text();
                $('#contactEmail').val(data.contact_email).text();
                $('#bank').val(data.bank).text();
                $('#accountHolder').val(data.account_holder).text();
                $('#accountNumber').val(data.account_number).text();
                $('#clabe').val(data.clabe).text();
                $('#rfc').val(data.rfc).text();
                Swal.close();
            }
        });
        $('#companyList').fadeOut();
    });
    //end autocomplete

    //select payment way
    /*let disableDays = false;
    $('#paymentWay').on('change', function() {
        let select = $('select[name="paymentWay"] option:selected').text();
        if (select === 'Crédito') {
            $("#days").prop("disabled", false);
            disableDays = true;
        } else {
            $("#days").prop("disabled", true);
            disableDays = false;
        }
    });*/

    //select job center
    $('#selectJobCenterAccounting').on('change', function() {
        let idJobCenter = $(this).val();
        window.location.href = route('index_accounting', idJobCenter);
    });

});
