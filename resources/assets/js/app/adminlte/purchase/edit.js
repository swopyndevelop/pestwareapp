/**
 * @author Alberto Martínez
 * @version 13/06/2021
 * register/_modalCreatePurchaseOrder.blade.php
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    $('.openModalEditPurchaseOrder').click(function () {

        $('#ModalPurchaseOrder').modal("show");
        document.getElementById('modalTitleOrder').innerText = "Editar Orden de Compra";
        document.getElementById('savePurchaseOrder').textContent = "Actualizar Orden de Compra";

        let all = $(this);
        let db = [];
        let totalGlobal = 0;
        let subtotalGlobal = 0;
        let taxesGlobal = 0;
        let purchaseId = all.attr('data-id');
        let btnAdd = document.getElementById('addTest');
        let productSelect = document.getElementById('products');
        let btnSavePurchase = document.getElementById('savePurchaseOrder');

        getProducts();

        document.getElementById('subtotalTest').innerText = `$0.00`;
        document.getElementById('totalTest').innerText = `$0.00`;

        initDB(purchaseId);

        function initDB(id) {
            loaderPestWare('');
            let token = $("meta[name=csrf-token]").attr("content");
            $.ajax({
                type: 'GET',
                url: route('edit_purchase_order', id),
                data: {
                    _token: token
                },
                success: function(response) {
                    Swal.close();
                    if (response.code === 500) {
                        showToast('error', 'Error al cargar', response.message);
                    } else {
                        setDataProvider(response);
                        //customerId = response.customer.id;
                        const {articlesPurchase} = response;
                        articlesPurchase.forEach((element) => {
                            const {
                                id_product,
                                product,
                                quantity,
                                quantityProduct,
                                unit,
                                taxes,
                                taxesValues,
                                unit_price,
                                subtotal,
                                total
                            } = element;
                            let indexProduct = indexMatchingText(productSelect, product);
                            db.push({
                                id: createId(),
                                productId: id_product,
                                productText: product,
                                quantityProduct: `${quantityProduct} ${unit}s`,
                                taxes: taxes,
                                taxesValues: taxesValues,
                                lastPrice: unit_price,
                                subtotal: subtotal,
                                total: total,
                                quantity: quantity,
                                index: indexProduct
                            });
                        });
                        listItems(db);
                    }
                }
            });
        }

        function setDataProvider(response) {
            const {purchaseOrder} = response;
            const {
                folio,
                proveedor,
                contact_name,
                contact_address,
                contact_cellphone,
                contact_email,
                bank,
                account_holder,
                account_number,
                clabe,
                rfc,
                description,
                id_concept,
                id_payment_way,
                id_payment_method,
                id_voucher
            } = purchaseOrder;

            const {concepts, paymentWays, paymentMethods, vouchers} = response;

            document.getElementById('folioPurchase').textContent = folio;
            document.getElementById('company').value = proveedor;
            document.getElementById('contactName').value = contact_name;
            document.getElementById('contactAddress').value = contact_address;
            document.getElementById('contactCellphone').value = contact_cellphone;
            document.getElementById('contactEmail').value = contact_email;
            document.getElementById('bank').value = bank;
            document.getElementById('accountHolder').value = account_holder;
            document.getElementById('accountNumber').value = account_number;
            document.getElementById('clabe').value = clabe;
            document.getElementById('rfc').value = rfc;
            document.getElementById('descriptionOrder').value = description;

            $("#concept").empty();
            concepts.forEach((element) => {
                let option = `<option value="${element.id}" ${id_concept === element.id ? 'selected': ''}`;
                option = option + `>${element.name}</option>`;
                $("#concept").append(option);
            });

            $("#paymentWay").empty();
            paymentWays.forEach((element) => {
                let option = `<option value="${element.id}" ${id_payment_way === element.id ? 'selected': ''}`;
                option = option + `>${element.name}</option>`;
                $("#paymentWay").append(option);
            });

            $("#paymentMethod").empty();
            paymentMethods.forEach((element) => {
                let option = `<option value="${element.id}" ${id_payment_method === element.id ? 'selected': ''}`;
                option = option + `>${element.name}</option>`;
                $("#paymentMethod").append(option);
            });

            $("#voucher").empty();
            vouchers.forEach((element) => {
                let option = `<option value="${element.id}" ${id_voucher === element.id ? 'selected': ''}`;
                option = option + `>${element.name}</option>`;
                $("#voucher").append(option);
            });
        }

        btnAdd.onclick = function () {
            let data = getInputs()
            const regexDecimal = /^[0-9]+([.][0-9]+)?$/;
            const regexInteger = /^[0-9]?\d{1,6}$/;
            if (!regexInteger.test(data.quantity)) showToast('warning', 'Datos incompletos', 'La cantidad debe ser númerica.');
            else if (!regexDecimal.test(data.lastPrice)) showToast('warning', 'Datos incompletos', 'El precio debe ser númerico.');
            else addRow(data)
        }

        productSelect.addEventListener('change', (event) => {
            const option = event.target.value;
            getDataProduct(option);
        });

        function getProducts() {
            loaderPestWare('');
            let token = $("meta[name=csrf-token]").attr("content");
            $.ajax({
                type: 'GET',
                url: route('get_products_purchase_order'),
                data: {
                    _token: token
                },
                success: function(response) {
                    Swal.close();
                    if (response.code === 500) {
                        showToast('error', 'Error', response.message);
                    } else {
                        let select = $('#products');
                        select.empty();
                        select.append(`<option value="0" selected disabled>Selecciona un Producto</option>`);
                        const {code, products} = response;
                        products.forEach((element) => {
                            select.append(`<option value="${element.id}">${element.name}</option>`);
                        });
                    }
                }
            });
        }

        function getDataProduct(id) {
            loaderPestWare('');
            $.ajax({
                type: 'GET',
                url: route('data_product_purchase_order'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: id
                },
                success: function(data) {
                    if (data.code === 500) {
                        Swal.close();
                        showToast('error','Error',data.message);
                    } else {
                        const {taxesValues, lastPrice} = data;
                        taxesGlobal = taxesValues;
                        const {quantity, unit} = data.product;
                        document.getElementById('quantityProduct').value = `${quantity} ${unit}s`;
                        document.getElementById('taxes').value = data.taxes;
                        document.getElementById('lastPrice').value = lastPrice;
                        Swal.close();
                    }
                }
            });
        }

        function getInputs() {
            let product = document.getElementById('products');
            let quantityProduct = document.getElementById('quantityProduct').value;
            let taxes = document.getElementById('taxes').value;
            let lastPrice = document.getElementById('lastPrice').value;
            let quantity = document.getElementById('quantity').value;
            const index = product.selectedIndex;
            const optionSelect = product.options[index];
            const subtotal = quantity * lastPrice;

            let total = subtotal;
            if (taxesGlobal !== 0) {
                taxesGlobal.forEach((element) => {
                    const tax = total * (element.value / 100);
                    total += tax;
                });
            }
            return {
                id: createId(),
                productId: optionSelect.value,
                productText: optionSelect.text,
                quantityProduct: quantityProduct,
                taxes: taxes,
                taxesValues: taxesGlobal,
                lastPrice: lastPrice,
                subtotal: subtotal,
                total: total,
                quantity: quantity,
                index: index
            }
        }

        function setInputs(data) {
            document.getElementById('products').selectedIndex = data.index;
            document.getElementById('quantityProduct').value = data.quantityProduct;
            document.getElementById('taxes').value = data.taxes;
            document.getElementById('lastPrice').value = data.lastPrice;
            document.getElementById('quantity').value = data.quantity;
        }

        function clearInputs() {
            document.getElementById('products').selectedIndex = 0;
            document.getElementById('quantityProduct').value = '';
            document.getElementById('taxes').value = '';
            document.getElementById('lastPrice').value = '';
            document.getElementById('quantity').value = '';
            document.getElementById('addTest').textContent = 'Agregar';
        }

        function addRow(data) {
            db.push(data);
            listItems(db);
            clearInputs();
        }

        // Models =>
        function getItemById(id) {
            return db.find(element => element.id === id)
        }

        function deleteItemById(id) {
            let dbTmp = db;
            db = [];
            dbTmp.forEach((element) => {
                if (element.id !== id) db.push(element);
            })
        }

        function calculatePrice(data) {
            let subtotal = 0;
            let total = 0;
            data.forEach((element) => {
                subtotal += element.subtotal
                total += element.total
            });
            subtotalGlobal = subtotal;
            totalGlobal = total;
            document.getElementById('subtotalTest').innerText = `$${subtotal.toFixed(2)}`;
            document.getElementById('totalTest').innerText = `$${total.toFixed(2)}`;
        }

        function listItems(data) {
            let tableRef = document.getElementById('customTable');
            for(let i = tableRef.rows.length - 1; i > 0; i--)
            {
                tableRef.deleteRow(i);
            }
            data.forEach((element) => {
                const actions = `<a id="updateItemRow-${element.id}" data-id="${element.id}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
            <a id="deleteItemRow-${element.id}" data-id="${element.id}" style="cursor: pointer;"><i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>`;

                let newRow = tableRef.insertRow();
                const subtotal = parseFloat(element.subtotal)
                const total = parseFloat(element.total)

                let newCellProduct  = newRow.insertCell(0);
                let newCellQuantityProduct  = newRow.insertCell(1);
                let newCellTaxes  = newRow.insertCell(2);
                let newCellLastPrice  = newRow.insertCell(3);
                let newCellQuantity  = newRow.insertCell(4);
                let newCellSubtotal  = newRow.insertCell(5);
                let newCellTotal = newRow.insertCell(6);
                let newCellActions  = newRow.insertCell(7);

                newCellProduct.appendChild(document.createTextNode(element.productText));
                newCellQuantityProduct.appendChild(document.createTextNode(element.quantityProduct));
                newCellTaxes.appendChild(document.createTextNode(element.taxes));
                newCellLastPrice.appendChild(document.createTextNode(element.lastPrice));
                newCellQuantity.appendChild(document.createTextNode(element.quantity));
                newCellSubtotal.appendChild(document.createTextNode(subtotal.toFixed(2)));
                newCellTotal.appendChild(document.createTextNode(total.toFixed(2)));
                newCellActions.innerHTML = actions;

                // Events =>
                $(`#updateItemRow-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    let item = getItemById(id);
                    setInputs(item);
                    taxesGlobal = item.taxesValues;
                    let btn = document.getElementById('addTest');
                    btn.textContent = 'Editar';
                    deleteItemById(id);
                });
                $(`#deleteItemRow-${element.id}`).click(function () {
                    let all = $(this);
                    deleteItemById(all.attr('data-id'));
                    listItems(db);
                    clearInputs();
                });
            });
            calculatePrice(db);
        }

        // Utils =>
        let createId = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        // Saved purchase order =>
        // Saved purchase order =>
        btnSavePurchase.onclick = function () {
            //validations
            let company = $('#company').val();
            let contactName = $('#contactName').val();
            let contactAddress = $('#contactAddress').val();
            let contactCellphone = $('#contactCellphone').val();
            let contactEmail = $('#contactEmail').val();
            let bank = $('#bank').val();
            let accountHolder = $('#accountHolder').val();
            let accountNumber = $('#accountNumber').val();
            let clabe = $('#clabe').val();
            let rfc = $('#rfc').val();
            let concept = $("#concept option:selected").val();
            let descriptionOrder = $('#descriptionOrder').val();
            let voucher = $("#voucher option:selected").val();

            if (company === "") showToast('error','Orden de Compra','Ingresa Proveedor','index_accounting');
            else if(company.length > 255) showToast('error','Orden de Compra','Debes ingresar un texto más corto que no exceda los 255 carácteres','index_accounting');
            else if(contactName === "") showToast('error','Orden de Compra','Ingresa el nombre del contacto','index_accounting');
            else if(contactName.length > 255) showToast('error','Orden de Compra','Debes ingresar un texto más corto que no exceda los 255 carácteres','index_accounting');
            else if(contactAddress === "") showToast('error','Orden de Compra','Ingresa la dirección del contacto','index_accounting');
            else if(contactAddress.length > 255) showToast('error','Orden de Compra','Debes ingresar un texto más corto que no exceda los 255 carácteres','index_accounting');
            else if(contactCellphone === "") showToast('error','Orden de Compra','Ingresa el numero de celular del contacto','index_accounting');
            else if(contactCellphone.length > 14) showToast('error','Orden de Compra','Ingresa el numero de celular no mayor a 14 digitos','index_accounting');
            else if(isNaN(contactCellphone))showToast('error','Orden de Compra','Solo se permiten numeros','index_accounting');
            else if(contactEmail === "") showToast('error','orden de Compra','Ingresa el correo electronico del contacto','index_accounting');
            else if (!validateEmail(contactEmail)) showToast('error','orden de Compra','Ingresa un  correo electronico valido','index_accounting');
            else if(bank === "") showToast('error','Orden de Compra','Ingresa el Banco asociado','index_accounting');
            else if(bank.length > 255) showToast('error','Orden de Compra','Debes ingresar un texto más corto que no exceda los 255 carácteres','index_accounting');
            else if(accountHolder === "") showToast('error','Orden de Compra','Ingresa el nombre del titular de la cuenta bancaria','index_accounting');
            else if(accountHolder.length > 255) showToast('error','Orden de Compra','Debes ingresar un texto más corto que no exceda los 255 digitos','index_accounting');
            else if(accountNumber === "") showToast('error','Orden de Compra','Ingresa el numero de cuenta bancaria','index_accounting');
            else if(accountNumber.length > 255) showToast('error','Orden de Compra','Debes ingresar la número de cuenta aque no exceda los 255 digitos','index_accounting');
            else if(isNaN(accountNumber)) showToast('error','Orden de Compra','Este campo sólo admite números','index_accounting');
            else if(clabe === "") showToast('error','Orden de Compra','Ingresa la cuenta interbancaria','index_accounting');
            else if(clabe.length > 255) showToast('error','Orden de Compra','Debes ingresar la cuenta interbancaria','index_accounting');
            else if(isNaN(clabe)) showToast('error','Orden de Compra','Este campo sólo admite números','index_accounting');
            else if(rfc === "") showToast('error','Orden de Compra','Ingresa el RFC','index_accounting');
            else if(rfc.length > 255) showToast('error','Orden de Compra','Debes ingresar el RFC no mayor a 255 carácteres','index_accounting');
            else if(concept == 0) showToast('error','Orden de Compra','Debes seleccionar un concepto','index_accounting');
            else if(descriptionOrder.length > 255) missingText('Debes ingresar un texto más corto que no exceda los 255 carácteres');
            else if(voucher == 0) showToast('error','Orden de Compra','Debes de seleccionar un  de Tipo Comprobante','index_accounting');
            else if (db.length < 1) showToast('warning', 'Datos incompletos', 'Debes ingresar por lo menos un producto.');
            else savePurchaseOrder();
        }

        function validateEmail(contactEmail) {
            const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(contactEmail).toLowerCase());
        }

        function savePurchaseOrder(){
            loaderPestWare('');

            //get values input
            let company = $('#company').val();
            let contactName = $('#contactName').val();
            let contactAddress = $('#contactAddress').val();
            let contactCellphone = $('#contactCellphone').val();
            let contactEmail = $('#contactEmail').val();
            let bank = $('#bank').val();
            let accountHolder = $('#accountHolder').val();
            let accountNumber = $('#accountNumber').val();
            let clabe = $('#clabe').val();
            let rfc = $('#rfc').val();
            let concept = $('#concept').val();
            let paymentWay = $('#paymentWay').val();
            let paymentMethod = $('#paymentMethod').val();
            let descriptionOrder = $('#descriptionOrder').val();
            let voucher = $('#voucher').val();
            let jobCenter = $('#selectJobCenterAccounting').val();
            let creditDays = $('#days').val();

            $.ajax({
                type: 'POST',
                url: route('edit_purchase'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    purchase_id: purchaseId,
                    company: company,
                    contactName: contactName,
                    contactAddress: contactAddress,
                    contactCellphone: contactCellphone,
                    contactEmail: contactEmail,
                    bank: bank,
                    accountHolder: accountHolder,
                    accountNumber: accountNumber,
                    clabe: clabe,
                    rfc: rfc,
                    concept: concept,
                    paymentWay: paymentWay,
                    paymentMethod: paymentMethod,
                    descriptionOrder: descriptionOrder,
                    subtotal: subtotalGlobal,
                    total: totalGlobal,
                    voucher: voucher,
                    jobCenter: jobCenter,
                    creditDays: creditDays,
                    arrayConcepts: JSON.stringify(db),
                },
                success: function (data) {
                    if (data.code === 500){
                        Swal.close();
                        showToast('error','Error',data.message);
                    }
                    else{
                        Swal.close();
                        showToast('success-redirect','Orden de Compra',data.message,'index_accounting');
                        $("#savePurchaseOrder").prop("disabled", true);
                    }
                }
            });
        }

    });

    function indexMatchingText(ele, text) {
        for (var i=0; i<ele.length;i++) {
            if (ele[i].childNodes[0].nodeValue === text){
                return i;
            }
        }
        return undefined;
    }

    $('#ModalPurchaseOrder').on('hidden.bs.modal', function () {
        location.reload();
    })

});
