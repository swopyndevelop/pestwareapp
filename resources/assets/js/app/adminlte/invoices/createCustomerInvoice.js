/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalCreateCustomerInvoice.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    // Global vars
    let token = $("meta[name=csrf-token]").attr("content");
    const validateRfc = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/;

    // Load data selects.
    getCatalogsTypeCfdiSAT();
    getCatalogsRegimensSAT();

    function getCatalogsTypeCfdiSAT() {
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_type_cfdi'),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#typeCfdiCustomerMainNew');
                    select.empty();
                    const {typeCfdi} = response;
                    select.append(`<option value="0" selected disabled>Seleccionar Uso de la Factura:</option>`);
                    typeCfdi.forEach((element) => {
                        select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                    });
                }
            }
        });
    }

    function getCatalogsRegimensSAT() {
        loaderPestWare('');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_regimens'),
            data: {
                _token: token,
                jobCenterId: 507
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#regimeFiscal');
                    select.empty();
                    const {regimens, fiscalRegime} = response;
                    if (fiscalRegime == null) {
                        select.append(`<option value="0" selected disabled>Selecciona un Régimen fiscal:</option>`);
                    }
                    regimens.forEach((element) => {
                        if (fiscalRegime === element.Value) {
                            select.append(`<option selected value="${element.Value}">${element.Name}</option>`);
                        } else select.append(`<option value="${element.Value}">${element.Name}</option>`);
                    });
                }
            }
        });
    }

    // Create product billing.
    const btnAddCustomer = document.getElementById('saveCustomerBilling');
    btnAddCustomer.addEventListener('click', function () {
        const name = document.getElementById('nameCustomerMainNew').value;
        const billing = document.getElementById('billingCustomerMainNew').value;
        const rfc = document.getElementById('rfcCustomerMainNew').value;
        const email = document.getElementById('emailBillingCustomerMainNew').value;
        const phone = document.getElementById('phoneCustomerBranchNew').value;
        const address = document.getElementById('address').value;
        const exteriorNumber = document.getElementById('exteriorNumber').value;
        const interiorNumber = document.getElementById('interiorNumber').value;
        const postalCode = document.getElementById('postalCode').value;
        const colony = document.getElementById('colony').value;
        const municipality = document.getElementById('municipality').value;
        const state = document.getElementById('state').value;
        const cfdiType = document.getElementById('typeCfdiCustomerMainNew').value;
        const regimeFiscal = document.getElementById('regimeFiscal').value;

        // Validations
        if (name.length === 0) showToast('warning', 'Nombre', 'Debes ingresar el nombre del cliente');
        else if (billing.length === 0) showToast('warning', 'Razón Social', 'Debes ingresar la razón social');
        else if (rfc.length === 0) showToast('warning', 'RFC', 'Debes ingresar un RFC');
        else if (!validateRfc.test(rfc)) showToast('warning','RFC','El RFC no cumple con el formato requerido de la facturación.');
        else if (cfdiType == 0) showToast('warning','Uso del Cfdi','Debes seleccionar el uso de la factura.');
        else if (regimeFiscal == 0) showToast('warning','Regimen Fiscal','Debes seleccionar un regimen fiscal.');
        else {
            const customer = {
                _token: token,
                name: name,
                billing: billing,
                rfc: rfc,
                email: email,
                phone: phone,
                address: address,
                exteriorNumber: exteriorNumber,
                interiorNumber: interiorNumber,
                postalCode: postalCode,
                colony: colony,
                municipality: municipality,
                state: state,
                cfdiType: cfdiType,
                regimeFiscal: regimeFiscal
            };
            createCustomerBilling(customer);
        }
    });

    function createCustomerBilling(customer) {
        loaderPestWare('Guardando Cliente...');
        $.ajax({
            type: 'POST',
            url: route('create_customer_billing'),
            data: customer,
            success: function(response) {
                Swal.close();
                if (response.code === 500) showToast('warning', 'Error', response.message);
                else {
                    showToast('success', 'Cliente', response.message);
                    // Load select products
                    let select = $('#customer');
                    select.empty();
                    const {customers} = response;
                    select.append(`<option value="0" selected>Público en general</option>`);
                    customers.forEach((element) => {
                        select.append(`<option value="${element.id}">${element.name}</option>`);
                    });
                    $('#modalCreateCustomerInvoice').click();
                }
            }
        });
    }

});
