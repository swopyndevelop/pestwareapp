/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalCreateInvoice.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    // Global vars
    let db = [];
    let currentItem;
    let totalGlobal = 0;
    let subtotalGlobal = 0;
    let ivaGlobal = 0;
    let iepsGlobal = 0;
    let ivaRetGlobal = 0;
    let isrGlobal = 0;
    let discountGlobal = 0;
    const token = $("meta[name=csrf-token]").attr("content");
    let btnAdd = document.getElementById('addTest');
    let btnCreateInvoice = document.getElementById('btnCreateCustomInvoice');
    let btnCancelInvoice = document.getElementById('btnCancelCustomInvoice');
    const productSelect = document.getElementById('productsBilling');

    // Events
    const selectDiscount = document.getElementById('selectDiscount')
    selectDiscount.addEventListener('change', (event) => {
        const option = event.target.value;
        if (parseInt(option) === 0) {
            document.getElementById('inputDiscount').disabled = true;
            document.getElementById('inputDiscount').value = 0;
        }
        else document.getElementById('inputDiscount').disabled = false;
    });

    const selectCurrency = document.getElementById('currency')
    selectCurrency.addEventListener('change', (event) => {
        const option = event.target.value;
        if (option === 'MXN') {
            document.getElementById('exchangeRate').disabled = true;
            document.getElementById('exchangeRate').value = '';
        }
        else document.getElementById('exchangeRate').disabled = false;
    });

    productSelect.addEventListener('change', (event) => {
        const option = event.target.value;
        getDataProduct(option);
    });

    btnAdd.onclick = function () {
        let data = getInputs()
        const regexDecimal = /^[0-9]+([.][0-9]+)?$/;
        const regexInteger = /^[0-9]?\d{1,6}$/;
        if (!regexInteger.test(data.quantityProduct)) showToast('warning', 'Datos incompletos', 'La cantidad debe ser númerica.');
        else if (!regexDecimal.test(data.price)) showToast('warning', 'Datos incompletos', 'El precio debe ser númerico.');
        else addRow(data)
    }

    btnCreateInvoice.onclick = function () {
        const customerId = document.getElementById('customer').value;
        const paymentFormSat = document.getElementById('paymentFormSat').value;
        const typeInvoice = document.getElementById('typeInvoice').value;
        const paymentMethodSat = document.getElementById('paymentMethodSat').value;

        const orderNumber = document.getElementById('orderNumber').value;
        const accountNumber = document.getElementById('accountNumber').value;
        const currency = document.getElementById('currency').value;
        const bankName = document.getElementById('bankName').value;
        const exchangeRate = document.getElementById('exchangeRate').value;
        const conditionsPayment = document.getElementById('conditionsPayment').value;
        const dateIssue = document.getElementById('dateIssue').value;
        const comments = document.getElementById('comments').value;

        // validations
        if (paymentFormSat == 0) showToast('warning', 'Forma de Pago', 'Debes seleccionar una forma de pago.');
        else if (paymentMethodSat == 0) showToast('warning', 'Método de Pago', 'Debes seleccionar un método de pago.');
        else if (db.length === 0) showToast('warning', 'Productos - Servicios', 'Debes ingresar por lo menos un producto.');
        else {
            let conceptsBilling = db;
            conceptsBilling.forEach((element) => {
                element.total = element.total.toFixed(2)
            });
            const data = {
                _token: token,
                customerId: customerId,
                paymentFormSat: paymentFormSat,
                typeInvoice: typeInvoice,
                paymentMethodSat: paymentMethodSat,
                orderNumber: orderNumber,
                accountNumber: accountNumber,
                currency: currency,
                bankName: bankName,
                exchangeRate: exchangeRate,
                conditionsPayment: conditionsPayment,
                dateIssue: dateIssue,
                comments: comments,
                items: conceptsBilling
            }
            createCustomInvoice(data);
        }
    }

    btnCancelInvoice.onclick = function () {
        $('#modalCreateSale').click();
    }

    // Backend Ajax.
    function getDataProduct(id) {
        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('get_product_billing', id),
            data: {
                _token: token,
                id: id
            },
            success: function(data) {
                const {unit_price} = data;
                document.getElementById('priceInput').value = `${unit_price}`;
                currentItem = data;
                Swal.close();
            }
        });
    }

    function createCustomInvoice(invoice) {
        loaderPestWare('');
        $.ajax({
            type: 'POST',
            url: route('create_custom_invoice'),
            data: invoice,
            success: function(response) {
                Swal.close();
                if (response.code === 500) {
                    Swal.fire({
                        title: '<strong>Error al generar la factura.</strong>',
                        type: 'error',
                        html: response.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        Swal.close()
                        $('#cancelInvoice').click();
                    })
                } else if (response.code === 404) $('#modalPurchaseFolios').modal("show");
                else {
                    document.getElementById('btnCreateCustomInvoice').disabled = true;
                    showToast('success-redirect', 'Factura', response.message, 'index_billing');
                }
            }
        });
    }

    // Models =>
    function addRow(data) {
        db.push(data);
        listItems(db);
        clearInputs();
    }

    function getItemById(id) {
        return db.find(element => element.id === id)
    }

    function deleteItemById(id) {
        let dbTmp = db;
        db = [];
        dbTmp.forEach((element) => {
            if (element.id !== id) db.push(element);
        })
    }

    function calculatePrice(data) {
        let subtotal = 0;
        let total = 0;
        let iva = 0;
        let ieps = 0;
        let ivaRet = 0;
        let isr = 0;
        let discount = 0;
        data.forEach((element) => {
            discount += element.totDiscount;
            subtotal += element.subtotal
            total += element.total
            if (element.iva !== 0) iva += element.iva;
            if (element.ieps !== 0) ieps += element.ieps;
            if (element.iva_ret !== 0) ivaRet += element.iva_ret;
            if (element.isr !== 0) isr += element.isr;
        });
        subtotalGlobal = subtotal;
        totalGlobal = total;
        if (discount !== 0) {
            discountGlobal = discount;
            document.getElementById('divDiscountTest').style.display = 'block';
            document.getElementById('discountTest').innerText = `$${discount.toFixed(2)}`;
        } else document.getElementById('divDiscountTest').style.display = 'none';
        if (iva !== 0) {
            ivaGlobal = iva;
            document.getElementById('divIvaTest').style.display = 'block';
            document.getElementById('ivaTest').innerText = `$${iva.toFixed(2)}`;
        } else document.getElementById('divIvaTest').style.display = 'none';
        if (ieps !== 0) {
            iepsGlobal = ieps;
            document.getElementById('divIepsTest').style.display = 'block';
            document.getElementById('iepsTest').innerText = `$${ieps.toFixed(2)}`;
        } else document.getElementById('divIepsTest').style.display = 'none';
        if (ivaRet !== 0) {
            ivaRetGlobal = ivaRet;
            document.getElementById('divIvaRetTest').style.display = 'block';
            document.getElementById('ivaRetTest').innerText = `$${ivaRet.toFixed(2)}`;
        } else document.getElementById('divIvaRetTest').style.display = 'none';
        if (isr !== 0) {
            isrGlobal = isr;
            document.getElementById('divIsrTest').style.display = 'block';
            document.getElementById('isrTest').innerText = `$${isr.toFixed(2)}`;
        } else document.getElementById('divIsrTest').style.display = 'none';

        document.getElementById('subtotalTest').innerText = `$${subtotal.toFixed(2)}`;
        document.getElementById('totalTest').innerText = `$${total.toFixed(2)}`;
    }

    function clearInputs() {
        clearSelectedDiscount();
        document.getElementById('productsBilling').selectedIndex = 0;
        document.getElementById('quantityProductInput').value = '1';
        document.getElementById('priceInput').value = '';
        document.getElementById('inputDiscount').disabled = true;
        document.getElementById('inputDiscount').value = 0;
        document.getElementById('addTest').textContent = 'Agregar';
    }

    function listItems() {
        let tableRef = document.getElementById('customTable');
        for(let i = tableRef.rows.length - 1; i > 0; i--)
        {
            tableRef.deleteRow(i);
        }
        db.forEach((element) => {
            const {id, productText, price, subtotal, taxes, keys, discount, total} = element;
            const actions = `<a id="updateItemRow-${id}" data-id="${id}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
            <a id="deleteItemRow-${id}" data-id="${id}" style="cursor: pointer;"><i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>`;

            let newRow = tableRef.insertRow();
            let newCellQuantityProduct  = newRow.insertCell(0);
            let newCellKeys  = newRow.insertCell(1);
            let newCellProduct  = newRow.insertCell(2);
            let newCellPrice  = newRow.insertCell(3);
            let newCellSubtotal  = newRow.insertCell(4);
            let newCellDiscount  = newRow.insertCell(5);
            let newCellTaxes  = newRow.insertCell(6);
            let newCellTotal = newRow.insertCell(7);
            let newCellActions  = newRow.insertCell(8);

            newCellQuantityProduct.appendChild(document.createTextNode(element.quantityProduct));
            newCellKeys.appendChild(document.createTextNode(keys));
            newCellProduct.appendChild(document.createTextNode(productText));
            newCellPrice.appendChild(document.createTextNode(price));
            newCellSubtotal.appendChild(document.createTextNode(subtotal));
            newCellDiscount.appendChild(document.createTextNode(discount));
            newCellTaxes.appendChild(document.createTextNode(taxes));
            newCellTotal.appendChild(document.createTextNode(total));
            newCellActions.innerHTML = actions;

            // Events =>
            $(`#updateItemRow-${element.id}`).click(function () {
                let all = $(this);
                const id = all.attr('data-id');
                let item = getItemById(id);
                setInputs(item);
                let btn = document.getElementById('addTest');
                btn.textContent = 'Editar';
                deleteItemById(id);
            });
            $(`#deleteItemRow-${element.id}`).click(function () {
                let all = $(this);
                deleteItemById(all.attr('data-id'));
                listItems(db);
                clearInputs();
            });
        });
        calculatePrice(db);
    }

    function setInputs(data) {
        document.getElementById('productsBilling').selectedIndex = data.index;
        document.getElementById('quantityProductInput').value = data.quantityProduct;
        document.getElementById('priceInput').value = data.price;
        setOptionSelectedDiscount(data.selectDiscount);
        document.getElementById('inputDiscount').value = data.discount;
        if (data.selectDiscount !== 1) document.getElementById('inputDiscount').disabled = false;
    }

    function getInputs() {
        let product = document.getElementById('productsBilling');
        let quantityProduct = document.getElementById('quantityProductInput').value;
        let price = document.getElementById('priceInput').value;
        let discount = document.getElementById('inputDiscount').value;
        let selectDiscount = document.getElementById('selectDiscount').value;
        const index = product.selectedIndex;
        const optionSelect = product.options[index];
        const subtotalOrigin = quantityProduct * price;
        let subtotal = quantityProduct * price;
        let totDiscount = 0;

        if (discount !== 0) {
            if (parseInt(selectDiscount) === 1) totDiscount = parseFloat(discount);
            if (parseInt(selectDiscount) === 2) totDiscount = (subtotal * discount) / 100;
            subtotal = subtotal - totDiscount;
        }

        const {iva, ieps, iva_ret, isr, product_code_billing, unit_code_billing} = currentItem;
        const keys = `${product_code_billing} - ${unit_code_billing}`;
        let taxes = '';
        if (iva !== '-') taxes += `IVA(${iva} - ${subtotal * iva}), `;
        if (ieps !== '-') taxes += `IEPS(${ieps} - ${subtotal * ieps}), `;
        if (iva_ret !== '-') taxes += `IVA RET(${iva_ret} - ${subtotal * iva_ret}), `;
        if (isr !== '-') taxes += `ISR(${isr} - ${subtotal * isr})`;


        let total = subtotal;
        let taxIva = 0;
        let taxIeps = 0;
        let taxIvaRet = 0;
        let taxIsr = 0;

        if (iva !== '-') {
            const tax = total * iva;
            total += tax;
            taxIva = tax;
        }
        if (ieps !== '-' && iva !== '-') {
            const tax = subtotal * ieps;
            const taxIvaI = (subtotal + tax) * iva;
            total = subtotal + tax;
            total += taxIvaI;
            taxIeps = tax;
            taxIva = taxIvaI;
        }
        if (ieps !== '-' && iva === '-') {
            const tax = total * ieps;
            total += tax;
            taxIeps = tax;
        }
        if (iva_ret !== '-') {
            const tax = subtotal * iva_ret;
            total -= tax;
            taxIvaRet = tax;
        }
        if (isr !== '-') {
            const tax = subtotal * isr;
            total -= tax;
            taxIsr = tax;
        }
        return {
            id: createId(),
            productId: optionSelect.value,
            productText: optionSelect.text,
            quantityProduct: quantityProduct,
            price: price,
            discount: discount,
            totDiscount: totDiscount,
            selectDiscount: selectDiscount,
            subtotal: subtotalOrigin,
            total: total,
            taxes: taxes,
            iva: taxIva,
            iva_ret: taxIvaRet,
            ieps: taxIeps,
            isr: taxIsr,
            keys: keys,
            index: index
        }
    }

    // Utils =>
    let createId = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    function clearSelectedDiscount() {
        let select = $('#selectDiscount');
        select.empty();
        select.append(`<option value="0">-</option>
                            <option value="1">$</option>
                            <option value="2">%</option>`);
    }

    function setOptionSelectedDiscount(option) {
        let select = $('#selectDiscount');
        select.empty();
        select.append(`<option value="0" ${option == 0 ? 'selected' : ''}>-</option>
                            <option value="1" ${option == 1 ? 'selected' : ''}>$</option>
                            <option value="2" ${option == 2 ? 'selected' : ''}>%</option>`);
    }

    $("#purchaseFolios-12").click(function () {
        purchasePackageFolios(12);
    })

    $("#purchaseFolios-13").click(function () {
        purchasePackageFolios(13);
    })

    $("#purchaseFolios-14").click(function () {
        purchasePackageFolios(14);
    })

    $("#purchaseFolios-15").click(function () {
        purchasePackageFolios(15);
    })

    $("#purchaseFolios-16").click(function () {
        purchasePackageFolios(16);
    })

    $("#purchaseFolios-17").click(function () {
        purchasePackageFolios(17);
    })

    $("#purchaseFolios-18").click(function () {
        purchasePackageFolios(18);
    })

    $("#purchaseFolios-19").click(function () {
        purchasePackageFolios(19);
    })

    function purchasePackageFolios(planId) {
        loaderPestWare('Adquiriendo Folios');
        $.ajax({
            type: 'POST',
            url: route('purchase_folios_billing'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                package: planId,
            },
            success: function (data) {
                Swal.close();
                if (parseInt(data.code) === 500) {
                    Swal.fire({
                        title: '<strong>Error al comprar folios.</strong>',
                        type: 'error',
                        html: data.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        Swal.close()
                    })
                } else {
                    showToast('success', 'Folios', data.message);
                }
            }
        });
    }

});
