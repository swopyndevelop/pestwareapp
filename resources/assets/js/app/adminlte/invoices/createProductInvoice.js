/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalComplementInvoice.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    // Global vars
    let token = $("meta[name=csrf-token]").attr("content");

    const btnCreateShowModal = document.getElementById('btnShowModalCreateProduct');
    btnCreateShowModal.addEventListener('click', function () {
        resetForm();
        document.getElementById('modalTitleProduct').innerText = 'Nuevo Producto';
        $('#modalCreateProductInvoice').modal("show");
    });

    // Get data selects.
    getCatalogsPaymentFormsSAT();
    getCatalogsPaymentMethodsSAT();

    function getCatalogsPaymentFormsSAT() {
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_payment_forms'),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#paymentFormSat');
                    select.empty();
                    const {paymentForms} = response;
                    select.append(`<option value="0" selected disabled>Selecciona una Forma de Pago:</option>`);
                    paymentForms.forEach((element) => {
                        select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                    });
                }
            }
        });
    }

    function getCatalogsPaymentMethodsSAT() {
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_payment_methods'),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#paymentMethodSat');
                    select.empty();
                    const {paymentMethods} = response;
                    select.append(`<option value="0" selected disabled>Selecciona un Método de Pago:</option>`);
                    paymentMethods.forEach((element) => {
                        select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                    });
                }
            }
        });
    }

    function resetForm() {
        document.getElementById('idProductInvoice').value = 0;
        document.getElementById('internalName').value = '';
        document.getElementById('numberIdentification').value = '';
        document.getElementById('descriptionProduct').value = '';
        document.getElementById('priceUnitProduct').value = '';
        document.getElementById('accountPredial').value = '';
        document.getElementById('productCodeSat').value = '';
        document.getElementById('productCodeSatId').value = '';
        document.getElementById('productUnitSat').value = '';
        document.getElementById('productUnitSatId').value = '';
        document.getElementById('iva').options[4].selected = true;
        document.getElementById('ieps').options[17].selected = true;
        document.getElementById('ivaRet').options[22].selected = true;
        document.getElementById('isr').options[15].selected = true;
    }

    // Events
    const checkboxMoreInputs = document.getElementById('checkboxMoreInputs');
    checkboxMoreInputs.addEventListener('click', function() {
        if(checkboxMoreInputs.checked) {
            document.getElementById('divMoreData').style.display = 'block';
        } else {
            document.getElementById('divMoreData').style.display = 'none';
        }
    });

    let optionTasa = document.getElementById('tasaCheck');
    let input = document.getElementById('cuota');
    let selectIeps = document.getElementById('ieps');
    optionTasa.addEventListener('change', (event) => {
        if (event.currentTarget.checked) {
            input.style.display = "none";
            selectIeps.style.display = "block";
            input.value = '';
        }
    });

    selectIeps.addEventListener('change', (event) => {
        const option = event.target.value;
        if (option !== '-') {
            document.getElementById('ivaRet').disabled = true;
            document.getElementById('isr').disabled = true;
            document.getElementById('ivaRet').options[22].selected = true;
            document.getElementById('isr').options[15].selected = true;
        }
        else {
            document.getElementById('ivaRet').disabled = false;
            document.getElementById('isr').disabled = false;
        }
    });

    let optionCuota = document.getElementById('cuotaCheck');
    optionCuota.addEventListener('change', (event) => {
        if (event.currentTarget.checked) {
            input.style.display = "block";
            selectIeps.style.display = "none";
        }
    });

    const productCodeSat = document.getElementById('productCodeSat');
    const productUnitSat = document.getElementById('productUnitSat');
    productCodeSat.onkeyup = function (ev) {
        let keyword = document.getElementById('productCodeSat').value;
        findKeysProductsSat(keyword);
    }
    productUnitSat.onkeyup = function (ev) {
        let keyword = document.getElementById('productUnitSat').value;
        findUnitsProductsSat(keyword);
    }
    function findKeysProductsSat(keyword) {
        if (keyword !== '') {
            $.ajax({
                type: 'GET',
                url: route('catalogs_sat_products_services', keyword),
                data: {
                    _token: token
                },
                success: function(data) {
                    let productCodeSatList = $('#productCodeSatList');
                    productCodeSatList.fadeIn();
                    productCodeSatList.html(data);
                }
            });
        }
    }
    function findUnitsProductsSat(keyword) {
        if (keyword !== '') {
            $.ajax({
                type: 'GET',
                url: route('catalogs_sat_products_units', keyword),
                data: {
                    _token: token
                },
                success: function(data) {
                    let productUnitSatList = $('#productUnitSatList');
                    productUnitSatList.fadeIn();
                    productUnitSatList.html(data);
                }
            });
        }
    }
    $(document).on('click', 'li#getdataProductKey', function() {
        let all = $(this);
        document.getElementById('productCodeSatId').value = all.attr('data-id');
        let productCodeSat = $('#productCodeSat');
        let productCodeSatList =  $('#productCodeSatList');
        productCodeSat.val(all.text());
        productCodeSatList.fadeOut();
    });

    $(document).on('click', 'li#getdataProductUnit', function() {
        let all = $(this);
        document.getElementById('productUnitSatId').value = all.attr('data-id');
        let productUnitSat = $('#productUnitSat');
        let productUnitSatList =  $('#productUnitSatList');
        productUnitSat.val(all.text());
        productUnitSatList.fadeOut();
    });
    // end events

    // Create product billing.
    const btnAddProduct = document.getElementById('saveProductBilling');
    btnAddProduct.addEventListener('click', function () {
        const name = document.getElementById('internalName').value;
        const identification_number = document.getElementById('numberIdentification').value;
        const description = document.getElementById('descriptionProduct').value;
        const unit_price = document.getElementById('priceUnitProduct').value;
        const property_account = document.getElementById('accountPredial').value;
        const unit_code_billing_id = document.getElementById('productUnitSatId').value;
        const product_code_billing_id = document.getElementById('productCodeSatId').value;
        const unit_code_billing = document.getElementById('productUnitSat').value;
        const product_code_billing = document.getElementById('productCodeSat').value;
        const iva = document.getElementById('iva').value;
        const ieps = document.getElementById('ieps').value;
        const iva_ret = document.getElementById('ivaRet').value;
        const isr = document.getElementById('isr').value;
        const id = document.getElementById('idProductInvoice').value;

        const titleModal = document.getElementById('modalTitleProduct').innerText;
        let isSave = true;
        if (titleModal === 'Editar Producto') isSave = false;

        // Validations
        if (name.length === 0) showToast('warning', 'Nombre del producto', 'Debes ingresar un nombre');
        else if (description.length === 0) showToast('warning', 'Descripción', 'Debes ingresar una descripción');
        else if (unit_price.length === 0) showToast('warning', 'Descripción', 'Debes ingresar una descripción');
        else if (unit_code_billing.length === 0) showToast('warning', 'Unidad de Producto', 'Debes ingresar una unidad');
        else if (product_code_billing.length === 0) showToast('warning', 'Código de Producto', 'Debes ingresar un código');
        else {
            const product = {
                _token: token,
                id: id,
                isSave: isSave,
                name: name,
                identification_number: identification_number,
                description: description,
                unit_price: unit_price,
                property_account: property_account,
                unit_code_billing_id: unit_code_billing_id,
                product_code_billing_id: product_code_billing_id,
                unit_code_billing: unit_code_billing,
                product_code_billing: product_code_billing,
                iva: iva,
                ieps: ieps,
                iva_ret: iva_ret,
                isr: isr
            };
            createProductBilling(product);
        }
    });

    function createProductBilling(product) {
        loaderPestWare('Guardando Producto...');
        $.ajax({
            type: 'POST',
            url: route('create_product_billing'),
            data: product,
            success: function(response) {
                Swal.close();
                if (response.code === 500) showToast('warning', 'Error', response.message);
                else {
                    showToast('success', 'Producto', response.message);
                    // Load select products
                    let select = $('#productsBilling');
                    select.empty();
                    const {products} = response;
                    select.append(`<option value="0" selected disabled>Selecciona un producto</option>`);
                    products.forEach((element) => {
                        select.append(`<option value="${element.id}">${element.name}</option>`);
                    });
                    $('#modalCreateProductInvoice').click();
                }
            }
        });
    }

});
