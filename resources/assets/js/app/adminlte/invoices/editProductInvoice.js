/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalComplementInvoice.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    // Global vars
    let token = $("meta[name=csrf-token]").attr("content");

    const btnEditShowModal = document.getElementById('btnShowModalEditProduct');
    btnEditShowModal.addEventListener('click', function () {
        const productId = document.getElementById('productsBilling').value;
        if (productId == 0) showToast('warning', 'Producto', 'Selecciona una producto.');
        else  {
            loaderPestWare('');
            $.ajax({
                type: 'GET',
                url: route('get_product_billing', productId),
                data: {
                    _token: token,
                    id: productId
                },
                success: function(data) {
                    const {
                        name,
                        identification_number,
                        description,
                        unit_price,
                        property_account,
                        unit_code_billing_id,
                        product_code_billing_id,
                        unit_code_billing,
                        product_code_billing,
                        iva,
                        ieps,
                        iva_ret,
                        isr
                    } = data;
                    document.getElementById('modalTitleProduct').innerText = 'Editar Producto';
                    document.getElementById('idProductInvoice').value = productId;
                    document.getElementById('internalName').value = name;
                    document.getElementById('numberIdentification').value = identification_number;
                    document.getElementById('descriptionProduct').value = description;
                    document.getElementById('priceUnitProduct').value = unit_price;
                    document.getElementById('accountPredial').value = property_account;
                    document.getElementById('productCodeSat').value = product_code_billing;
                    document.getElementById('productCodeSatId').value = product_code_billing_id;
                    document.getElementById('productUnitSat').value = unit_code_billing;
                    document.getElementById('productUnitSatId').value = unit_code_billing_id;
                    const optionsIva = document.getElementById('iva');
                    const optionsIeps = document.getElementById('ieps');
                    const optionsRet = document.getElementById('ivaRet');
                    const optionsIsr = document.getElementById('isr');
                    for ( let i = 0, len = optionsIva.options.length; i < len; i++ ) {
                        if (optionsIva.options[i].value === iva) optionsIva.options[i].selected = true;
                    }
                    for ( let i = 0, len = optionsIeps.options.length; i < len; i++ ) {
                        if (optionsIeps.options[i].value === ieps) optionsIeps.options[i].selected = true;
                    }
                    for ( let i = 0, len = optionsRet.options.length; i < len; i++ ) {
                        if (optionsRet.options[i].value === iva_ret) optionsRet.options[i].selected = true;
                    }
                    for ( let i = 0, len = optionsIsr.options.length; i < len; i++ ) {
                        if (optionsIsr.options[i].value === isr) optionsIsr.options[i].selected = true;
                    }
                    Swal.close();
                    $('#modalCreateProductInvoice').modal("show");
                }
            });
        }
    });

});
