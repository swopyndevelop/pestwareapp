/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalCancelInvoice.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    let invoiceId;
    let urlWhatsapp;

    $('.openModalSendWhatsAppInvoice').click(function () {
        let all = $(this);
        invoiceId = all.attr('data-id');
        const customerCellphone = all.attr('data-cellphone');
        $('#invoiceFolioSendWhatsApp').html(all.attr('data-folio'));
        document.getElementById('cellphoneCustomerInvoice').value = customerCellphone;
    });

    $('#sendInvoiceWhatsAppButton').click(function () {

        const urlBaseProd = 'https://pestwareapp.com/billing/download/invoice/';
        const urlBaseTest = 'https://test.pestwareapp.com/billing/download/invoice/';
        const urlBaseDev = 'http://127.0.0.1:8000/billing/download/invoice/';

        const urlPdfInvoice = `${urlBaseTest}${invoiceId}/pdf`;
        const urlXmlInvoice = `${urlBaseTest}${invoiceId}/Xml`;

        const cellphoneCustomerInvoice = document.getElementById('cellphoneCustomerInvoice').value;
        const cellphoneOtherInvoice = document.getElementById('cellphoneOtherInvoice').value;
        const codeCountry = document.getElementById('codeCountryModalInvoice').value;

        if (cellphoneCustomerInvoice.length <= 0) {
            showToast('warning','Teléfono Cliente','Favor de ingresar un teléfono valido.');
        }
        if (cellphoneCustomerInvoice.length !== 0) {
            urlWhatsapp = `https://api.whatsapp.com/send?phone=${codeCountry}${cellphoneCustomerInvoice}&text=Estimado+cliente%2C+te+compartimos+la+factura+del+servicio+realizado.%0A%0APDF%3A+${urlPdfInvoice}%0A%0AXML%3A+${urlXmlInvoice}`;
            window.open(urlWhatsapp, '_blank');
            $("#whatsappModalInvoice").click();
        }
        if (cellphoneOtherInvoice.length !== 0) {
            urlWhatsapp = `https://api.whatsapp.com/send?phone=${codeCountry}${cellphoneOtherInvoice}&text=Estimado+cliente%2C+te+compartimos+la+factura+del+servicio+realizado.%0A%0APDF%3A+${urlPdfInvoice}%0A%0AXML%3A+${urlXmlInvoice}`;
            window.open(urlWhatsapp, '_blank');
            $("#whatsappModalInvoice").click();
        }

    });

});
