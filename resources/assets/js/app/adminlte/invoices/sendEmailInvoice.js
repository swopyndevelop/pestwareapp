/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalCancelInvoice.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    let invoiceId;
    $('.openModalSenMailInvoice').click(function () {
        let all = $(this);
        invoiceId = all.attr('data-id');
        const customerEmail = all.attr('data-email');
        $('#invoiceFolioSendEmail').html(all.attr('data-folio'));
        document.getElementById('emailCustomerInvoice').value = customerEmail;
    });

    $('#sendEmailInvoiceButton').click(function () {

        const emailCustomerInvoice = document.getElementById('emailCustomerInvoice').value;
        const emailOtherInvoice = document.getElementById('emailOtherInvoice').value;
        let emails = [];

        if (emailCustomerInvoice.length < 0) showToast('warning','Correo Cliente','Favor de ingresar un correo valido.');

        if (emailCustomerInvoice.length !== 0) emails.push(emailCustomerInvoice);
        if (emailOtherInvoice.length !== 0) emails.push(emailOtherInvoice);
        sendInvoiceForEmail(emails);

    });

    function sendInvoiceForEmail(emails) {
        loaderPestWare('Enviando Factura...')
        $.ajax({
            type: 'POST',
            url: route('send_invoice_mail'),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                invoiceId: invoiceId,
                emails: emails
            },
            success: function (data) {
                Swal.close();
                if (data.code == 500){
                    showToast('error', 'Factura', data.message);
                }
                else {
                    showToast('success-redirect', 'Factura Enviada', data.message, 'index_billing');
                }
            }
        });
    }

});
