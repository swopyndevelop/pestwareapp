/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalComplementInvoice.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    let invoiceId;
    $('.openModalComplementInvoice').click(function () {
        let all = $(this);
        invoiceId = all.attr('data-id');
        $('#invoiceFolioComplement').html(all.attr('data-folio'));
        $('#amountInvoice').html(all.attr('data-amount'));
        getCatalogsPaymentFormsSAT();
    });

    $('#complementInvoice').click(function() {
        let amount = document.getElementById('amountPaymentInvoice').value;
        let paymentWay = document.getElementById('paymentWay').value;
        let paymentForm = document.getElementById('paymentForm').value;
        let dateHour = document.getElementById('dateInvoicePayment').value;
        let paymentFormSat = document.getElementById('paymentFormInvoice').value;
        let checkbox = document.getElementById('checkboxComplement').checked;
        let partiality = document.getElementById('partiality').value;
        let payerAccount = document.getElementById('payerAccount').value;
        let BeneficiaryAccount = document.getElementById('BeneficiaryAccount').value;
        let ForeignAccountNamePayer = document.getElementById('ForeignAccountNamePayer').value;
        let OperationNumber = document.getElementById('OperationNumber').value;
        let RfcIssuerPayerAccount = document.getElementById('RfcIssuerPayerAccount').value;
        let RfcReceiverBeneficiaryAccount = document.getElementById('RfcReceiverBeneficiaryAccount').value;

        const data = {
            _token: $("meta[name=csrf-token]").attr("content"),
            invoiceId: invoiceId,
            amount: amount,
            paymentForm: paymentForm,
            paymentWay: paymentWay,
            dateHour: dateHour,
            paymentFormSat: paymentFormSat,
            isComplement: checkbox,
            partiality: partiality,
            payerAccount: payerAccount,
            BeneficiaryAccount: BeneficiaryAccount,
            ForeignAccountNamePayer: ForeignAccountNamePayer,
            OperationNumber: OperationNumber,
            RfcIssuerPayerAccount: RfcIssuerPayerAccount,
            RfcReceiverBeneficiaryAccount: RfcReceiverBeneficiaryAccount
        }

        if (amount.trim().length === 0) showToast('warning', 'Factura', 'Debes ingresar el monto recibido.');
        else if (paymentWay == 0) showToast('warning', 'Factura', 'Debes seleccionar la forma de pago.');
        else if (paymentForm == 0) showToast('warning', 'Factura', 'Debes seleccionar el método de pago.');
        else if (dateHour.trim().length === 0) showToast('warning', 'Factura', 'Debes seleccionar la fecha y hora del pago.');
        else if (checkbox === true) {
            if (paymentFormSat == 0) showToast('warning', 'Factura', 'Debes seleccionar el método de pago SAT.');
            else if (partiality.trim().length == 0) showToast('warning', 'Factura', 'Debes ingresar por lo menos 1 en parcialidad.');
            //else if (payerAccount.trim().length != 10 || payerAccount.trim().length != 16 || payerAccount.trim().length != 18 || payerAccount.trim().length != 0) showToast('warning', 'Cuenta Ordenante', 'Debe ser de 10, 16 o 18 digitos.');
            //else if (BeneficiaryAccount.trim().length != 0 || BeneficiaryAccount.trim().length != 16 || BeneficiaryAccount.trim().length != 18 || BeneficiaryAccount.trim().length == 0) showToast('warning', 'Cuenta Beneficiario', 'Debe ser de 10, 16 o 18 digitos.');
            else complementInvoiceSat(data);
        }
        else complementInvoiceSat(data);
    });

    function complementInvoiceSat(data) {
        loaderPestWare('Generando Pago Factura...');
        $.ajax({
            type: 'POST',
            url: route('complement_invoice'),
            data: data,
            success: function (data) {
                Swal.close();
                if (data.code == 500){
                    Swal.fire({
                        title: '<strong>Error al generar el pago.</strong>',
                        type: 'error',
                        html: data.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        Swal.close()
                        $('#cancelInvoice').click();
                    })
                }
                else {
                    showToast('success-redirect', 'Factura Pagada', data.message, 'index_billing');
                }
            }
        });
    }

    function getCatalogsPaymentFormsSAT() {
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_payment_forms'),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    let select = $('#paymentFormInvoice');
                    select.empty();
                    const {paymentForms} = response;
                    select.append(`<option value="0" selected disabled>Selecciona una Forma de Pago:</option>`);
                    paymentForms.forEach((element) => {
                        select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                    });
                }
            }
        });
    }

    // Events
    const checkboxComplement = document.getElementById('checkboxComplement');
    checkboxComplement.addEventListener('click', function() {
        if(checkboxComplement.checked) {
            document.getElementById('divComplementInvoice').style.display = 'block';
        } else {
            document.getElementById('divComplementInvoice').style.display = 'none';
        }
    });

    const paymentFormSatSelect = document.getElementById('paymentFormInvoice');
    paymentFormSatSelect.addEventListener('change', (event) => {
        const option = event.target.value;
        if (option === '01' || option === '08' || option === '12' || option === '13' || option === '14' ||
            option === '15' || option === '17' || option === '23' || option === '24' || option === '25' ||
            option === '26' || option === '27' || option === '30' || option === '31') {
            document.getElementById('RfcIssuerPayerAccount').disabled = true;
            document.getElementById('RfcReceiverBeneficiaryAccount').disabled = true;
            document.getElementById('payerAccount').disabled = true;
            document.getElementById('BeneficiaryAccount').disabled = true;
            document.getElementById('ForeignAccountNamePayer').disabled = true;
        }else if (option === '05') {
            document.getElementById('ForeignAccountNamePayer').disabled = true;
        }else if (option === '06') {
            document.getElementById('RfcReceiverBeneficiaryAccount').disabled = true;
            document.getElementById('BeneficiaryAccount').disabled = true;
            document.getElementById('ForeignAccountNamePayer').disabled = true;
        } else resetInputsComplement();
    });

    function resetInputsComplement() {
        document.getElementById('RfcIssuerPayerAccount').disabled = false;
        document.getElementById('RfcReceiverBeneficiaryAccount').disabled = false;
        document.getElementById('payerAccount').disabled = false;
        document.getElementById('BeneficiaryAccount').disabled = false;
        document.getElementById('ForeignAccountNamePayer').disabled = false;
    }

});
