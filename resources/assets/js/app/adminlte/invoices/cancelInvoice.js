/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalCancelInvoice.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    let invoiceId;
    $('.openModalCancelInvoice').click(function () {
        let all = $(this);
        invoiceId = all.attr('data-id');
        $('#invoiceFolio').html(all.attr('data-folio'));
    });

    $('#cancelInvoiceSat').click(function(){
        cancelInvoiceSat(invoiceId);
    });

    function cancelInvoiceSat(id){
        loaderPestWare('Cancelando Factura...')
        $.ajax({
            type: 'delete',
            url: route('cancel_invoice_sat',id),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (data) {
                Swal.close();
                if (data.code == 500){
                    Swal.fire({
                        title: '<strong>Error al Cancelar Factura.</strong>',
                        type: 'error',
                        html: data.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        Swal.close()
                        $('#cancelInvoice').click();
                        location.reload();
                    })
                }
                else {
                    showToast('success-redirect', 'Factura Cancelada', data.message, 'index_billing');
                }
            }
        });
    }

});
