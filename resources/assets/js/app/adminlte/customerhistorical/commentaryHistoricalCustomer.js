/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 04/04/2021
 * customers/_modalCustomerUser.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
   let idCustomer, titleHistoricalCustomer, commentaryHistoricalCustomer;
   idCustomer = document.getElementById('idCustomer').value;

   $("#saveCommentaryHistoricalCustomer").click(function() {
      titleHistoricalCustomer = document.getElementById('titleHistoricalCustomer').value;
      commentaryHistoricalCustomer = document.getElementById('commentaryHistoricalCustomer').value;
      if (titleHistoricalCustomer.length < 1) showToast('warning','Título','El título no puede estar vacío');
      else if (commentaryHistoricalCustomer.length < 1) showToast('warning','Comentario','El comentario no puede estar vacío');
      else {
         sendCommentaryHistoricalCustomer();
      }
   });

   function sendCommentaryHistoricalCustomer(){
      loaderPestWare('');
      $.ajax({
         type: 'POST',
         url: route('add_commentary_historical_customer'),
         data: {
            _token: $("meta[name=csrf-token]").attr("content"),
            idCustomer: idCustomer,
            titleHistoricalCustomer: titleHistoricalCustomer,
            commentaryHistoricalCustomer: commentaryHistoricalCustomer
         },
         success: function (data) {
            if (data.code == 500){
               Swal.close();
               showToast('error', 'Error', data.message);
            }
            else{
               Swal.close();
               showToast('success', data.title, data.message);
               $("#saveCommentaryHistoricalCustomer").attr('disabled', true);
               location.reload();
            }
         }
      });
   }

});
