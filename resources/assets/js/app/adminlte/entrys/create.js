/**
 * @author Alberto Martínez
 * @version 06/09/2019
 * entrys/_create.blade.php
 * view _newproduct.blade.php
 */

import {expressions, getFields, setFields, validateField} from "../validations/regex";
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    //Intro Js index_inventory
    let urlCurrent = window.location.pathname;
    const introJsPest = document.getElementById('introJsInpunt').value;
    let viewEntriesCreate = '/entry/create';
    if (urlCurrent  === viewEntriesCreate) {
        if (introJsPest == 1){
            createInventory();
        }
        //createInventory();
        function createInventory(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#titleCreateEntry',
                        title:"Entrada de Producto",
                        intro:"",
                    },
                    {
                        element: '#sourceCreateEntry',
                        title:"Origen",
                        intro: "Lugar de origen de tu mercancía como puede ser el nombre de algún proveedor.",
                        position: "right",
                    },
                    {
                        element: '#destinyCreateEntry',
                        title:"Destino",
                        intro: "Centro de trabajo principal o alguna sucursal en especifico si así lo deseas.",
                        position: "right",
                    },
                    {
                        element: '#billCreateEntry',
                        title:"Factura",
                        intro: "La suma de todos los productos registrados deberán coincidar con el total de tu " +
                            "factura para que tus operaciones sean más prcisas y concisas.",
                        position: "left",
                    },
                    {
                        element: '#item_table',
                        title:"Producto",
                        intro: "Por cada producto deberás seleccionarlo de la lista desplegable, si es un producto nuevo, " +
                            "podrás registrarlo dando click en el boton azul, los datos del producto se cargaran en automatico " +
                            "y solo será necesario ingresar las unidades recibidas y el precio factura de ser necesario actualizarlo. ",
                        position: "right",
                    },
                    {
                        element: '#btnSaveEntry',
                        title:"Guardar",
                        intro: "Finalmente puedes adjuntar tu factura en formato pdf y pulsar en el boton Guardar.",
                        position: "right",
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false,
            });
            intro.start();
            intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
                intro.exit();

                updateAjaxIntroJs(1);

                setTimeout(() => {
                    window.location.href = route('index_transfer');
                }, 1000);
            });
        }
    }

    function updateAjaxIntroJs(introjs){
        $.ajax({
            type: 'POST',
            url: route('change_introjs_user'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                introjs: introjs
            },
            success: function(data) {
                //console.log('OK');
            }
        });
    }
    //End Intro Js

    let purchasesSelect = document.getElementById('purchasesOrder');

    purchasesSelect.addEventListener('change', (event) => {
        const option = event.target.value;
        if (option == 0) {
            document.getElementById('purchaseOrderId').value = "0";
            document.getElementById('origin').value = "";
            document.getElementById('total').innerText = `$00.00`;
            document.getElementById('addNewProduct').style.dsplay = "block";
            document.getElementById('addProduct').style.display = "block";
            let tableRef = document.getElementById('item_table');
            for(let i = tableRef.rows.length - 1; i > 0; i--)
            {
                tableRef.deleteRow(i);
            }
        } else getDataPurchase(option);
    });

    function getDataPurchase(id) {
        loaderPestWare('Cargando datos');
        $.ajax({
            type: 'GET',
            url: route('get_data_purchase_order'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                id_purchase: id
            },
            success: function(data) {
                if (data.code === 500) {
                    Swal.close();
                    showToast('error','Error',data.message);
                } else {
                    const {purchase, products} = data;
                    document.getElementById('addNewProduct').style.display = "none";
                    document.getElementById('addProduct').style.display = "none";
                    document.getElementById('purchaseOrderId').value = id;
                    document.getElementById('origin').value = purchase.company;
                    document.getElementById('total').innerText = `$${purchase.total}`;
                    listItems(products);
                    Swal.close();
                }
            }
        });
    }

    let productsDB = [];

    function listItems(data) {
        $('#item_table td').remove();
        let rows = '';
        let table = $('#item_table');

        data.forEach((element) => {
            const { id, product, id_product, quantityProduct, unit, quantity, subtotal, total, quantity_partial, is_partial} = element;
            if (quantity != quantity_partial) {
                let quantityFinal = quantity;
                if (is_partial == 1) quantityFinal = quantity - quantity_partial;
                productsDB.push({
                    id: id,
                    product: product,
                    id_product: id_product,
                    quantityProduct: quantityProduct,
                    unit: unit,
                    quantity: quantityFinal,
                    subtotal: subtotal,
                    total: total,
                });

                rows += `<tr>
                        <td class="text-primary text-bold">${product}</td>
                        <td class="">${quantityProduct} ${unit}s</td>
                        <td class=""><input class="form-control" type="number" id="quantity-${id}" value="${quantityFinal}"></td>
                        <td class="">${subtotal}</td>
                        <td class="">${total}</td>
                    </tr>`;
            }

        });
        table.append(rows);
        Swal.close();

    }

    document.addEventListener("DOMContentLoaded", function(event) {
        document.getElementById('btnSaveEntry').disabled = true;
    });

    $(document).on('click', '.add', function() {

        let destinity = $("#selectJobCenters option:selected").val();

        if (destinity == 0) showToast('warning','Sucursal','Selecciona la sucursal (destino)');
        else {
            loaderPestWare('Cargando productos...');
            $.ajax({
                type: 'GET',
                url: route('get_products_entry'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function(data) {
                    if (data.errors === 500) {
                        Swal.close();
                        showToast('error','Error al obtener los datos','Algo salio mal, intenta de nuevo.');
                    } else {
                        // Load products select
                        var options = '';
                        data.forEach((element) => {
                            options += `<option value="${element.id}">${element.name}</option>`;
                        });

                        var html = '';
                        html += '<tr>';
                        html += `<td><select name="selectProducts[]" id="selectProducts" class="applicantsList-single form-control selectProducts" style="width: 100%"><option value="0" disabled="" selected="">Seleccione una opción</option>${options}</select></td>`;
                        html += '<td><input type="text" readonly="readonly" name="quantity[]" class="form-control quantity" /></td>';
                        html += '<td><input type="number" min="1" name="units[]" class="form-control units" /></td>';
                        html += '<td><input type="text" readonly="readonly" name="priceBase[]" class="form-control priceBase" /></td>';
                        html += '<td><input type="number" step="any" name="priceFacture[]" class="form-control priceFacture" /></td>';
                        html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove" style="margin-left: 10px;"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
                        $('#item_table').append(html);
                        $(".applicantsList-single").select2();

                        let len = $('.selectProducts').length;

                        $('.selectProducts').each(function(index) {
                            if (index === (len - 1)) {
                                $(this).change(function() {
                                    let valId = $(this, "option:selected").val();
                                    getDataProduct(valId, index);
                                });
                            }
                        });
                        document.getElementById('btnSaveEntry').disabled = false;
                        Swal.close();
                    }
                }
            });
        }

    });

    function getDataProduct(id, index) {

        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('data_product_entry'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                id: id
            },
            success: function(data) {
                if (data.errors) {
                    Swal.close();
                    showToast('error','Error al cargar los datos','Algo salio mal, Intenta de nuevo');
                } else {

                    $('.quantity').each(function(i) {
                        if (index === i) {
                            $(this).val(`${data.quantity} ${data.unit}s`);
                        }
                    });

                    $('.priceBase').each(function(i) {
                        if (index === i) {
                            $(this).val(data.base_price);
                        }
                    });

                    Swal.close();
                }
            }
        });
    }

    $(document).on('click', '.remove', function() {
        $(this).closest('tr').remove();
    });

    const inputs = document.querySelectorAll('#formNewProduct input');
    const description = document.getElementById('description');
    const adjetive = document.getElementById('adjetive');

    const fieldsProductForm = {
        nombre: false,
        description: false,
        adjetive: false,
        uso: false,
        cantidad: false,
        ingredient: false,
        precio: false
    }

    setFields(fieldsProductForm);

    const validateForm = (e) => {
        switch (e.target.name) {

            case "nombre":
                validateField(expressions.description, e.target.value, 'nombre');
                break;
            case "description":
                validateField(expressions.descriptionLarge, e.target.value, 'description');
                break;
            case "adjetive":
                validateField(expressions.descriptionLarge, e.target.value, 'adjetive');
                break;
            case "uso":
                validateField(expressions.description, e.target.value, 'uso');
                break;
            case "cantidad":
                validateField(expressions.totalDecimal, e.target.value, 'cantidad');
                break;
            case "precio":
                validateField(expressions.totalDecimal, e.target.value, 'precio');
                break;
            case "ingredient":
                validateField(expressions.description, e.target.value, 'ingredient');
                break;
        }
    }

    description.addEventListener('keyup', validateForm);
    description.addEventListener('blur', validateForm);

    adjetive.addEventListener('keyup', validateForm);
    adjetive.addEventListener('blur', validateForm);

    inputs.forEach((input) => {
        input.addEventListener('keyup', validateForm);
        input.addEventListener('blur', validateForm);
    });

    // Class maked //Event Change conceptExpense
    const typeProduct = document.querySelector('#grupo__tipo');
    typeProduct.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__tipo').classList.remove('formulario__grupo-incorrecto');
        }
    });
    const displayProduct = document.querySelector('#grupo__presentacion');
    displayProduct.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__presentacion').classList.remove('formulario__grupo-incorrecto');
        }
    });
    const unitProduct = document.querySelector('#grupo__unidad');
    unitProduct.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__unidad').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // Class maked //Event Change plagues
    const taxes = document.querySelector('#grupo__taxes');
    taxes.addEventListener('change', (event) => {
        if (event.target.value.length != 0){
            document.getElementById('grupo__taxes').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // New Product
    $('#formNewProduct').on('submit', function(event) {

        event.preventDefault();

        let typeProduct = $('#tipo option:selected').val();
        let displayProduct = $("#presentacion option:selected").val();
        let unitProduct = $('#unidad option:selected').val();
        let taxes = $('#taxes').val();
        //let unit_code_billing = $('#unit_code_billing option:selected').val();
        //let is_available = document.getElementById('is_available').checked;

        if(typeProduct == 0){
            document.getElementById('grupo__tipo').classList.add('formulario__grupo-incorrecto');
        }
        if(displayProduct == 0){
            document.getElementById('grupo__presentacion').classList.add('formulario__grupo-incorrecto');
        }
        if(unitProduct == 0){
            document.getElementById('grupo__unidad').classList.add('formulario__grupo-incorrecto');
        }
        if(taxes.length == 0){
            showToast('warning','Impuestos','Ingresa un Impuesto');
        }

        //Declarations validations Expenses
        let nameProduct = $('#nombre').val();
        let descriptionProduct = $('#description').val();
        let adjetiveProduct = $("#adjetive").val();
        let useProduct = $('#uso').val();
        let quantityProduct = $("#cantidad").val();
        let prizeProduct = $("#precio").val();
        let ingredientProduct = $("#ingredient").val();

        validateField(expressions.description, nameProduct, 'nombre');
        validateField(expressions.descriptionLarge, descriptionProduct, 'description');
        validateField(expressions.descriptionLarge, adjetiveProduct, 'adjetive');
        validateField(expressions.description, useProduct, 'uso');
        validateField(expressions.totalDecimal, quantityProduct, 'cantidad');
        validateField(expressions.totalDecimal, prizeProduct, 'precio');
        validateField(expressions.description, ingredientProduct, 'ingredient');

        if(getFields().nombre && getFields().description && getFields().adjetive && getFields().uso
            && getFields().cantidad && getFields().precio && getFields().ingredient
            && typeProduct != 0 && displayProduct != 0 && unitProduct != 0 && taxes.length != 0){
            saveCaptureProduct();
        }

        else {
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }
    });

    $('#btnSaveEntry').on('click', function(event) {
        loaderPestWare('Guardando Entrada');
        let dataProducts = [];

        let products = [], quantityProducts = [], unitProducts = [], priceBaseProducts = [], priceInvoiceProducts = [];

        //validations
        let origin = $("input[name=origin]").val();
        let DestinyId = $("#selectJobCenters option:selected").val();
        let purchaseOrderId = document.getElementById('purchasesOrder').value;
        let error = '';

        if (origin.length === 0) missingText('Ingresa el NOMBRE DEL PROVEEDOR');
        else if (DestinyId == 0) missingText('Selecciona el DESTINO');
        else {
            $('.selectProducts').each(function() {
                var count = 1;
                if ($(this).val() == null) {
                    error += "<p>Debes seleccionar un producto</p>";
                    return false;
                } else {
                    products.push($(this).val());
                }
                count = count + 1;
            });

            $('.units').each(function(index) {
                var count = 1;
                let transfer = parseInt($(this).val());

                if ($(this).val() == '') {
                    error += `<p>Debes ingresar la cantidad de unidades recibidas.</p>`;
                    //return false;
                }

                if (transfer <= 0) {
                    error += `<p>Las unidades recibidas deben ser mayor a cero.</p>`;
                    //return false;
                }

                if ($(this).val() != '' && transfer > 0) {
                    unitProducts.push($(this).val())
                }

                count = count + 1;
            });

            $('.priceFacture').each(function() {
                var count = 1;
                if ($(this).val() == '') {
                    error += "<p>Debes ingresar el precio factura.</p>";
                    return false;
                } else {
                    priceInvoiceProducts.push($(this).val());
                }
                count = count + 1;
            });

            $('.quantity').each(function() {
                quantityProducts.push($(this).val());
            });

            $('.priceBase').each(function() {
                priceBaseProducts.push($(this).val());
            });

            productsDB.forEach((row) => {
                let id = row.id;
                let quantityId = $(`#quantity-${row.id}`).val();
                let productId = row.id_product;
                dataProducts.push({
                    id: id,
                    quantity: quantityId,
                    productId: productId
                });
            });

            $.ajaxSetup({
                headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
            });
            $.ajax({
                url: route('add_entry'),
                method: "POST",
                data: {
                    dataProducts : dataProducts,
                    selectProducts: products,
                    quantity: quantityProducts,
                    units: unitProducts,
                    priceBase: priceBaseProducts,
                    priceFacture: priceInvoiceProducts,
                    origin: origin,
                    selectJobCenters: DestinyId,
                    purchaseOrderId: purchaseOrderId
                },
                success: function(data) {
                    if (data.code == 201) {
                        Swal.close();
                        $('#item_table').find("tr:gt(0)").remove();
                        showToast('success-redirect','Entradas','Guardando Correctamente','index_entry')
                    } else {
                        Swal.close();
                        showToast('error','Error al guardar',data.message);
                    }
                }
            })
        }


    });

    /*$('#insert_form').on('submit', function(event) {

        event.preventDefault();
        var error = '';

        //validations
        let origin = $("input[name=origin]").val();
        let DestinyId = $("#selectJobCenters option:selected").val();

        if (origin.length === 0) missingText('Ingresa el NOMBRE DEL PROVEEDOR');
        else if (DestinyId == 0) missingText('Selecciona el DESTINO');
        else {

            $('.selectProducts').each(function() {
                var count = 1;
                if ($(this).val() == null) {
                    error += "<p>Debes seleccionar un producto</p>";
                    return false;
                }
                count = count + 1;
            });

            $('.units').each(function(index) {
                var count = 1;
                let transfer = parseInt($(this).val());

                if ($(this).val() == '') {
                    error += `<p>Debes ingresar la cantidad de unidades recibidas.</p>`;
                    //return false;
                }

                if (transfer <= 0) {
                    error += `<p>Las unidades recibidas deben ser mayor a cero.</p>`;
                    //return false;
                }

                count = count + 1;
            });

            $('.priceFacture').each(function() {
                var count = 1;
                if ($(this).val() == '') {
                    error += "<p>Debes ingresar el precio factura.</p>";
                    return false;
                }
                count = count + 1;
            });

            var fd = new FormData(document.getElementById("insert_form"));

            if (error == '') {
                //loaderPestWare('Guardando Entrada');
                $.ajax({
                    url: route('add_entry'),
                    method: "POST",
                    data: fd,
                    // Ambos importantes para que jQuery no transforme la información
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        if (data.code == 201) {
                            Swal.close();
                            $('#item_table').find("tr:gt(0)").remove();
                            showToast('success-redirect','Entradas','Guardando Correctamente','index_entry')
                        } else {
                            Swal.close();
                            showToast('error','Error al guardar',data.message);
                        }
                    }
                });
            } else {
                $('#error').html('<div class="alert alert-danger">' + error + '</div>');
            }
        }
    });*/

    // Validate type document and route
    const archiveProduct = document.getElementById('information');

    archiveProduct.addEventListener('change', (event) => {
        checkFile(event);
    });

    function saveCaptureProduct() {
        loaderPestWare('Guardando Producto');
        let fd = new FormData(document.getElementById("formNewProduct"));
        $.ajax({
            url: route('save_product'),
            type: "POST",
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Error al guardar','Algo salió mal, intenta de nuevo');
                } else if (data.code == 201) {
                    Swal.close();
                    if(urlCurrent == '/entry/create'){
                        $('#newP').click();
                    } else {
                        showToast('success-redirect','Productos','Guardando Correctamente','index_product');
                        $("#btnSaveProduct").attr('disabled', true);
                    }
                }
            }
        });
    }

    function missingText(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
        });
    }

    function checkFile(e) {

        const fileList = event.target.files;
        let error = false;
        for (let i = 0, file; file = fileList[i]; i++) {

            let sFileName = file.name;
            let sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            let iFileSize = file.size;
            // let iConvert = (file.size / 1048576).toFixed(2);

            /// OR together the accepted extensions and NOT it. Then OR the size cond.
            /// It's easier to see this way, but just a suggestion - no requirement.
            if (!(sFileExtension === "pdf" ||
                sFileExtension === "jpg" ||
                sFileExtension === "jpeg") || iFileSize > 10485760) { /// 10 mb
                error = true;
            }
        }
        if (error) showToast('error','Archivo Inválido',"El archivo ingresado no es aceptado o su tamaño excede lo aceptado");
    }

});
