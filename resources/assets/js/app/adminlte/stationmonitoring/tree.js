/**
 * @author Alberto Martínez
 * @version 26/12/2020
 * stationmonitoring/index.blade.php
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWareLogin} from "../pestware/loaderLogin";

$(document).ready(function(){

    let customerId, customerName;
    // counters stations
    let cebadero = 1;
    let captura = 1;
    let luz = 1;

    $('#selectFilterCustomerName').on('change', function() {
        customerId = $(this).val();
        //customerName = $("option:selected", this).text();
        let establishment = $("option:selected", this).attr('data-establishment');
        let name = $("option:selected", this).attr('data-name');
        $('.folioMonitoring').html(`M-${customerId}`);
        customerName = establishment.length > 0 ? establishment : name;
        const data = [
            { "id" : "1", "text" : customerName, "type" : "root" }
        ];
        setTreeCustomer(data);
    });

    let items = function($node) {
        //let tmpItems = $.jstree.defaults.contextmenu.items($node)
        let tmpItems = {};
        if ($node.type === 'root') {
            tmpItems['create'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Crear",
                "icon"              : "fa fa-plus",
                "submenu" 			: {
                    "create_zone" : {
                        "seperator_before" : false,
                        "seperator_after" : false,
                        "label" : "Zona",
                        "icon" : "fa fa-square-o",
                        action : function (data) {
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.create_node(obj, {}, "last", 'Zona', '', function (new_node) {
                                try {
                                    inst.edit(new_node);
                                } catch (ex) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                }
                            });
                        }
                    }
                }
            }
        }
        if ($node.type_node === 'Zona') {
            tmpItems['create'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Crear",
                "icon"              : "fa fa-plus",
                "submenu" 			: {
                    "create_perimetro" : {
                        "seperator_before" : false,
                        "seperator_after" : false,
                        "label" : "Perímetro",
                        "icon" : "fa fa-circle-o-notch",
                        action : function (data) {
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.create_node(obj, {}, "last", 'Perímetro', '', function (new_node) {
                                try {
                                    inst.edit(new_node);
                                } catch (ex) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                }
                            });
                        }
                    }
                }
            }
            tmpItems['remove'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Borrar",
                "icon"              : "fa fa-eraser",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        inst.delete_node(inst.get_selected());
                    }
                    else {
                        inst.delete_node(obj);
                    }
                }
            }
            tmpItems['rename'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Renombrar",
                "icon"				: "fa fa-pencil",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                    obj = inst.get_node(data.reference);
                    inst.edit(obj);
                }
            }
        }
        if ($node.type_node === 'Perímetro') {
            tmpItems['create'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Crear",
                "icon"              : "fa fa-plus",
                "submenu" 			: {
                    "create_cebadero" : {
                        "seperator_before" : false,
                        "seperator_after" : false,
                        "label" : "Cebadero",
                        "icon" : "fa fa-simplybuilt",
                        action : function (data) {
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.create_node(obj, {}, "last", 'Cebadero', cebadero++, function (new_node) {
                                try {
                                    inst.edit(new_node);
                                } catch (ex) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                }
                            });
                        }
                    },
                    "create_captura" : {
                        "seperator_before" : false,
                        "seperator_after" : false,
                        "label" : "Trampa Captura",
                        "icon" : "fa fa-inbox",
                        action : function (data) {
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.create_node(obj, {}, "last", 'Trampa de Captura', captura++, function (new_node) {
                                try {
                                    inst.edit(new_node);
                                } catch (ex) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                }
                            });
                        }
                    },
                    "create_luz" : {
                        "seperator_before" : false,
                        "seperator_after" : false,
                        "label" : "Trampa Luz",
                        "icon" : "fa fa-bars",
                        action : function (data) {
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.create_node(obj, {}, "last", 'Trampa de Luz', luz++, function (new_node) {
                                try {
                                    inst.edit(new_node);
                                } catch (ex) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                }
                            });
                        }
                    }
                }
            }
            tmpItems['remove'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Borrar",
                "icon"              : "fa fa-eraser",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        inst.delete_node(inst.get_selected());
                    }
                    else {
                        inst.delete_node(obj);
                    }
                }
            }
            tmpItems['rename'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Renombrar",
                "icon"				: "fa fa-pencil",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    inst.edit(obj);
                }
            }
        }
        if ($node.type_node === 'Cebadero') {
            tmpItems['remove'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Borrar",
                "icon"              : "fa fa-eraser",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        cebadero--;
                        inst.delete_node(inst.get_selected());
                    }
                    else {
                        inst.delete_node(obj);
                    }
                }
            }
            tmpItems['rename'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Renombrar",
                "icon"				: "fa fa-pencil",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    inst.edit(obj);
                }
            }
        }
        if ($node.type_node === 'Trampa de Luz') {
            tmpItems['remove'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Borrar",
                "icon"              : "fa fa-eraser",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        luz--;
                        inst.delete_node(inst.get_selected());
                    }
                    else {
                        inst.delete_node(obj);
                    }
                }
            }
            tmpItems['rename'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Renombrar",
                "icon"				: "fa fa-pencil",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    inst.edit(obj);
                }
            }
        }
        if ($node.type_node === 'Trampa de Captura') {
            tmpItems['remove'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Borrar",
                "icon"              : "fa fa-eraser",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        captura--;
                        inst.delete_node(inst.get_selected());
                    }
                    else {
                        inst.delete_node(obj);
                    }
                }
            }
            tmpItems['rename'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Renombrar",
                "icon"				: "fa fa-pencil",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    inst.edit(obj);
                }
            }
        }

        return tmpItems;
    }
    $("#jsTreeAreaMonitoring").jstree({
        core: {
            animation: 0,
            check_callback: true,
            force_text: true,
            themes: { stripes: true },
            data: []
        },
        types: {
            "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
            "root": { icon: "fa fa-home", valid_children: ["default", "file"] },
            "default": {
                icon: "fa fa-th-large",
                valid_children: ["default"]
            },
        },
        plugins: ["contextmenu", "dnd", "search", "state", "types", "wholerow"],
        contextmenu: {"items": items}
    });

    function setTreeCustomer(data) {
        const customerTree = $('#jsTreeAreaMonitoring');
        customerTree.jstree(true).settings.core.data = data;
        customerTree.jstree(true).refresh();
    }

    $("#saveTreeStationMonitoring").click(function() {
        loaderPestWareLogin('');
        const customerTree = $("#jsTreeAreaMonitoring");
        let tree = customerTree.jstree(true).get_json();
        let tree_flat = customerTree.jstree(true).get_json(null, { flat: true });
        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            type: 'POST',
            url: route('station_monitoring_tree_new'),
            data: {
                tree: JSON.stringify(tree_flat),
                customerId: customerId
            },
            success: function(data) {
                Swal.close();
                if (data.code == 500) showToast('error', 'Error al guardar.', data.message);
                else if (data.code == 201) {
                    document.getElementById("saveTreeStationMonitoring").disabled = true;
                    showToast('success-redirect', 'Monitoreo Guardado', 'Datos guardados correctamente.', 'index_station_monitoring');
                }
            }
        });
    });

});
