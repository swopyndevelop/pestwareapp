/**
 * @author Alberto Martínez
 * @version 26/12/2020
 * stationmonitoring/index.blade.php | update tree
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    $('.openModalUpdateAreaMonitoring').click(function () {

        let monitoringId;
        let customerId = 0;
        let selectCustomerBranchId = 0;

        $('#newAreaMonitoringModalv2').modal("show");

        document.getElementById('modalTitleModalAreaV2').innerText = "Editar Área";
        document.getElementById('finishedMonitoring').textContent = "Editar Área";

        let all = $(this);
        $('.folioMonitoring').html(all.attr('data-monitoring'));
        monitoringId = all.attr('data-id');
        const customerName = all.attr('data-customerName');
        selectCustomerBranchId = all.attr('data-customerBranchId');
        customerId = all.attr('data-customerId');

        let selectCustomerEdit = $("#selectFilterCustomerNameV2");
        selectCustomerEdit.empty();
        let option = `<option value="${customerId}">${customerName}</option>`;
        selectCustomerEdit.append(option);
        document.getElementById('selectFilterCustomerNameV2').disabled = true;
        document.getElementById('panelStep3').style.display = "block";
        document.getElementById('panelStep4').style.display = "block";
        document.getElementById('panelStep5').style.display = "block";

        getAllTreeByCustomer(customerId);
        getTreeData(monitoringId);

        $("#saveZone").click(function() {
            let nameZone = document.getElementById('nameZone').value;
            if (nameZone.length > 50) showToast('warning','Campo Zona','La zona no puede tener más de 50 caracteres');
            else if (nameZone.length > 0) {
                loaderPestWare('Guardando');
                $.ajax({
                    type: "POST",
                    url: route('station_monitoring_tree_create_zone'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        customerId: customerId,
                        nameZone: nameZone,
                        selectCustomerBranchId: selectCustomerBranchId
                    },
                    success: function(data) {
                        if (data.code == 500) {
                            Swal.close();
                            showToast('error','Error al guardar','Algo salió mal, intente de nuevo.')
                        } else if (data.code == 201) {
                            // Load data zones
                            loadDataZones(data);
                            // Load Tree diagram
                            getTreeData(data.monitoringId);
                            if (data.zones.length > 0) document.getElementById('panelStep4').style.display = "block";
                            else document.getElementById('panelStep4').style.display = "none";
                        }
                    }
                });
            } else showToast('warning','Espera...','El nombre de la zona es obligatorio.');

        });

        $("#savePerimeter").click(function() {
            let zoneSelect = document.getElementById('zoneSelect').value;
            let namePerimeter = document.getElementById('namePerimeter').value;
            if (namePerimeter.length > 26) showToast('warning','Campo Perimetro','La zona no puede tener más de 26 caracteres');
            else if (namePerimeter.length > 0) {
                loaderPestWare('Guardando');
                $.ajax({
                    type: "POST",
                    url: route('station_monitoring_tree_create_perimeter'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        customerId: customerId,
                        namePerimeter: namePerimeter,
                        idNodeZone: zoneSelect,
                        selectCustomerBranchId: selectCustomerBranchId
                    },
                    success: function(data) {
                        if (data.code == 500) {
                            Swal.close();
                            showToast('error','Error al guardar','Algo salió mal, intente de nuevo.')
                        } else if (data.code == 201) {
                            loadDataPerimeters(data);
                            // Load Tree diagram
                            getTreeData(data.monitoringId);
                            if (data.perimeters.length > 0) document.getElementById('panelStep5').style.display = "block";
                            else document.getElementById('panelStep5').style.display = "none";
                        }
                    }
                });
            } else showToast('warning','Espera...','El nombre del perímetro es obligatorio.');

        });

        $("#saveStation").click(function() {
            let perimeterSelect = document.getElementById('perimeterSelect').value;
            let typeStationsSelect = document.getElementById('typeStationsSelect').value;
            let initialRange = document.getElementById('initialRange').value;
            let finalRange = document.getElementById('finalRange').value;
            let loan = document.getElementById('loan').checked;

            if (initialRange > 0 && finalRange > 0) {
                loaderPestWare('Guardando');
                $.ajax({
                    type: "POST",
                    url: route('station_monitoring_tree_create_station'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        customerId: customerId,
                        perimeterSelect: perimeterSelect,
                        typeStationsSelect: typeStationsSelect,
                        initialRange: initialRange,
                        finalRange: finalRange,
                        loan: loan,
                        selectCustomerBranchId: selectCustomerBranchId
                    },
                    success: function(data) {
                        if (data.code == 500) {
                            Swal.close();
                            showToast('error','Error al guardar', data.message)
                        } else if (data.code == 201) {
                            loadDataStations(data);
                            // Load Tree diagram
                            getTreeData(data.monitoringId);
                            loadDataStationsMap(data);
                            Swal.close();
                        }
                    }
                });
            } else showToast('warning','Espera...','La cantidad debe ser mayor a 0.');

        })

        function getAllTreeByCustomer(id) {
            loaderPestWare('Cargando...');
            $.ajax({
                type: "GET",
                url: route('get_all_tree_by_customer', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    monitoringId : monitoringId
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al cargar',data.message)
                    } else {
                        // Load data tables
                        loadDataZones(data);
                        loadDataPerimeters(data);
                        loadDataStations(data);
                        // Load Tree diagram
                        getTreeData(data.monitoringId);
                        loadDataStationsMap(data);
                        showToast('success','Datos Cargados',data.message)
                    }
                }
            });
        }

        function deleteZoneById(id) {
            loaderPestWare('Eliminando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_delete_zone', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar',data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Zona Eliminada',data.message)
                    }
                }
            });
        }

        function updateZoneById(id, name) {
            loaderPestWare('Editando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_update_zone'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: id,
                    name: name
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al editar',data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Zona Editada',data.message)
                    }
                }
            });
        }

        function deletePerimeterById(id) {
            loaderPestWare('Eliminando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_delete_perimeter', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar',data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Perímetro Eliminado',data.message)
                    }
                }
            });
        }

        function updatePerimeterById(id, name) {
            loaderPestWare('Editando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_update_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: id,
                    name: name
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al editar',data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Perímetro Editada',data.message)
                    }
                }
            });
        }

        function relocateStationsByPerimeter(idStation, idNodeStation, numberStation, selectTypeStationUnique) {
            loaderPestWare('Reubicando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_relocate_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    idStation: idStation,
                    idNodeStation: idNodeStation,
                    numberStation: numberStation,
                    typeStation: selectTypeStationUnique
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al reubicar', data.message)
                    } else {
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estaciones Reubicadas', data.message)
                        $('#relocateStation').click();
                    }
                }
            });
        }

        function cancelStationsByPerimeter(idStation) {
            loaderPestWare('Cancelando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_cancel_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                   idStation: idStation
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estaciones Eliminadas', data.message)
                    }
                }
            });
        }

        function loanStationsByPerimeter(idStation) {
            loaderPestWare('Cambiando Comodato...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_loan_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    idStation: idStation
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estaciones Eliminadas', data.message)
                    }
                }
            });
        }

        function deleteStationsByPerimeterSingular(idStation) {
            loaderPestWare('Eliminando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_delete_stations_by_perimeter_singular'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    idStation: idStation
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estación Eliminada', data.message)
                    }
                }
            });
        }

        function deleteStationsByPerimeter(idTypeStation, parent) {
            loaderPestWare('Eliminando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_delete_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id_customer: customerId,
                    id_type_station: idTypeStation,
                    parent: parent
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estaciones Eliminadas', data.message)
                    }
                }
            });
        }

        function updateStationsByPerimeter(idTypeStation, parent, initialRange, finalRange) {
            loaderPestWare('Actualizando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_update_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id_customer: customerId,
                    id_type_station: idTypeStation,
                    parent: parent,
                    initialRange: initialRange,
                    finalRange: finalRange
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al actualizar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estaciones Actualizadas', data.message)
                    }
                }
            });
        }

        function updateTreeDate(id) {
            loaderPestWare('Actualizando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_update_date', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al actualizar', data.message)
                    } else {
                        document.getElementById('saveZone').disabled = true;
                        document.getElementById('savePerimeter').disabled = true;
                        document.getElementById('saveStation').disabled = true;
                        document.getElementById('finishedMonitoring').disabled = true;
                        showToast('success-redirect', 'Área Guardada', 'Se guardaron las estaciones', 'index_station_monitoring');
                    }
                }
            });
        }

        function loadDataZones(data) {
            let tableBodyZones = $('#tableBodyDetailZones');
            Swal.close();
            showToast('success','Datos Guardados','Zona agregada.')
            let selectZone = $("#zoneSelect");
            document.getElementById('nameZone').value = "";
            selectZone.empty();
            if (data.zones.length > 0) document.getElementById('tableZones').style.display = "block";
            else document.getElementById('tableZones').style.display = "none";

            let rows = '';
            data.zones.forEach((element) => {
                // Load zones select
                let option = `<option value="${element.id_node}"`;
                option = option + `>${element.text}</option>`;
                selectZone.append(option);
                // Load data table zones
                const actions = `<a id="updateItemZone-${element.id}" data-id="${element.id}" data-zone="${element.text}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
                                ${data.lastInspection == null ? `<a id="deleteItemZone-${element.id}"
                                                                    data-id="${element.id}"
                                                                    data-zone="${element.text}"
                                                                    style="cursor: pointer;">
                                <i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>` : ''}
                               `;
                rows += `<tr>
                                <td class="text-center">ZN-${element.id}</td>
                                <td class="text-center">${element.text}</td>
                                <td class="text-center">${actions}</td>
                            </tr>`;
            });
            tableBodyZones.html('');
            tableBodyZones.append(rows);
            data.zones.forEach((element) => {
                // Events =>
                $(`#updateItemZone-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    const zone = all.attr('data-zone');
                    Swal.fire({
                        title: 'Editar Zona',
                        input: 'text',
                        inputValue: zone,
                        inputAttributes: {
                            autocapitalize: 'off',
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Editar',
                        cancelButtonText: 'Cancelar',
                        showLoaderOnConfirm: true,
                        preConfirm: (data) => {
                            if (data.length > 0) updateZoneById(id, data.trim());
                            else showToast('warning', 'Espera..', 'Debes ingresar un nombre de zona.');
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            console.log('otro')
                        }
                    })
                });
                $(`#deleteItemZone-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    const zone = all.attr('data-zone');
                    Swal.fire({
                        title: '¿Está seguro de eliminar la zona?',
                        text: `Al eliminar la zona: ${zone}, se borrarán todos sus perímetros y estaciones asociadas.`,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            deleteZoneById(id);
                        }
                    })
                });
            });
        }

        function loadDataPerimeters(data) {
            let tableBodyPerimeters = $('#tableBodyDetailPerimeters');
            Swal.close();
            //$('#modalCustomQr').click();
            showToast('success','Datos Guardados','Perímetro agregado.');
            let selectPerimeter = $("#perimeterSelect");
            let selectTypeStations = $('#typeStationsSelect');
            document.getElementById('namePerimeter').value = "";
            if (data.perimeters.length > 0) document.getElementById('tablePerimeters').style.display = "block";
            else document.getElementById('tablePerimeters').style.display = "none";
            selectPerimeter.empty();
            selectTypeStations.empty();
            let rows = '';
            data.perimeters.forEach((element) => {
                // Load perimeters select
                let option = `<option value="${element.id_node}"`;
                option = option + `>${element.text}</option>`;
                selectPerimeter.append(option);
                // Load data table zones
                const actions = `<a id="updateItemPerimeter-${element.id}" data-id="${element.id}" data-perimeter="${element.text}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
                                ${data.lastInspection == null ? `<a id="deleteItemPerimeter-${element.id}" 
                                data-id="${element.id}" 
                                data-perimeter="${element.text}" style="cursor: pointer;">
                                <i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>` : ''}
                               `;
                rows += `<tr>
                                <td class="text-center">PR-${element.id}</td>
                                <td class="text-center">${element.text}</td>
                                <td class="text-center">${actions}</td>
                            </tr>`;
            });
            data.typeStations.forEach((element) => {
                // Load perimeters select
                let option = `<option value="${element.id}"`;
                option = option + `>${element.name}</option>`;
                selectTypeStations.append(option);
            });
            tableBodyPerimeters.html('');
            tableBodyPerimeters.append(rows);
            data.perimeters.forEach((element) => {
                // Events =>
                $(`#updateItemPerimeter-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    const perimeter = all.attr('data-perimeter');
                    Swal.fire({
                        title: 'Editar Perímetro',
                        input: 'text',
                        inputValue: perimeter,
                        inputAttributes: {
                            autocapitalize: 'off',
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Editar',
                        cancelButtonText: 'Cancelar',
                        showLoaderOnConfirm: true,
                        preConfirm: (data) => {
                            if (data.length > 0) updatePerimeterById(id, data.trim());
                            else showToast('warning', 'Espera..', 'Debes ingresar un nombre de perímetro.');
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            console.log('otro')
                        }
                    })
                });
                $(`#deleteItemPerimeter-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    const perimeter = all.attr('data-perimeter');
                    Swal.fire({
                        title: '¿Está seguro de eliminar el perímetro?',
                        text: `Al eliminar el perímetro: ${perimeter}, se borrarán todos sus estaciones asociadas.`,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            deletePerimeterById(id);
                        }
                    })
                });
            });
        }

        function loadDataStations(data) {
            let tableBody = $('#tableBodyDetail');
            showToast('success','Datos Guardados','Estaciones Guardadas.')
            document.getElementById('initialRange').value = '';
            document.getElementById('finalRange').value = '';
            document.getElementById('finishedMonitoring').style.visibility = "visible";

            if (data.stations.length > 0) document.getElementById('tableStations').style.display = "block";
            else document.getElementById('tableStations').style.display = "none";

            let rows = '';
            data.stations.forEach((element) => {
                const actions =  `<a id="updateItemRow-${element.id}-${element.parent}" data-parent="${element.parent}" data-idTypeStation="${element.id_type_station}" data-quantity="${element.quantity}" data-name="${element.type_station}" data-zone="${element.zone}" data-perimeter="${element.text}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
                                  <a id="deleteItemRow-${element.id}-${element.parent}" data-parent="${element.parent}" data-idTypeStation="${element.id_type_station}" style="cursor: pointer;"><i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>`;
                rows += `<tr>
                                <td class="text-center">${element.zone}</td>
                                <td class="text-center">${element.text}</td>
                                <td class="text-center">${element.type_station}</td>
                                <td class="text-center">${element.quantity}</td>
                                <td class="text-center">${data.lastInspection == null ? `${actions}` : ''} </td>
                            </tr>`;
            });
            tableBody.html('');
            tableBody.append(rows);
            data.stations.forEach((element) => {
                // Events =>
                $(`#updateItemRow-${element.id}-${element.parent}`).click(function () {
                    let all = $(this);
                    const quantity = all.attr('data-quantity');
                    const name = all.attr('data-name');
                    const zone = all.attr('data-zone');
                    const perimeter = all.attr('data-perimeter');
                    const parent = all.attr('data-parent');
                    const idTypeStation = all.attr('data-idTypeStation');
                    Swal.fire({
                        title: `Rango de ${name} en ${zone}: ${perimeter}`,
                        html:
                            '<input id="swal-input1" class="swal2-input" type="number" placeholder="Rango Inicial">' +
                            '<input id="swal-input2" class="swal2-input" type="number" placeholder="Rango Final">',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Editar',
                        cancelButtonText: 'Cancelar',
                        showLoaderOnConfirm: true,
                        preConfirm: (data) => {
                            let initialRange = document.getElementById('swal-input1').value;
                            let finalRange = document.getElementById('swal-input2').value;
                            if (initialRange > 0 && finalRange > 0) updateStationsByPerimeter(idTypeStation, parent, initialRange, finalRange);
                            else showToast('warning', 'Espera...', 'Los rangos debe ser mayor a 0.')
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            console.log('other');
                        }
                    })
                });
                $(`#deleteItemRow-${element.id}-${element.parent}`).click(function () {
                    let all = $(this);
                    const parent = all.attr('data-parent');
                    const idTypeStation = all.attr('data-idTypeStation');
                    Swal.fire({
                        title: 'Eliminar Estaciones',
                        text: "¿Está seguro de eliminar las estaciones seleccionadas?",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            deleteStationsByPerimeter(idTypeStation, parent);
                        }
                    })
                });
            });
        }

        function loadDataStationsMap(data) {
            let tableBody = $('#tableDetailStationMap');

            if (data.stationTrees.length > 0) document.getElementById('tableStationsMap').style.display = "block";
            else document.getElementById('tableStationsMap').style.display = "none";

            let rows = '';
            data.stationTrees.forEach((element) => {
                rows += `<tr>  ${element.is_station != 0 ?
                                `<td class="text-center">${element.parent_text}</td>
                                <td class="text-center">${element.text}</td>
                                <td class="text-center"><a data-toggle="modal" data-target="#relocateStation" 
                                data-id="${element.id}"
                                data-parent="${element.parent}"
                                data-text="${element.text}"
                                data-idNode="${element.id_node}"
                                data-idTypeStation="${element.id_type_station}"
                                style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i>
                                </td>
                                ${element.visible == 0 ?
                                `<td class="text-center"><a id="cancelItemRow-${element.id}" 
                                data-id="${element.id}" data-visible="Activar" style="cursor: pointer;">
                                <i class="fa fa-times fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>
                                </td>` 
                                :
                                `<td class="text-center"><a id="cancelItemRow-${element.id}" 
                                data-id="${element.id}" data-visible="Desactivar" style="cursor: pointer;">
                                <i class="fa fa-check fa-lg " aria-hidden="true" style="margin-left: 5px; color : green"></i></a>
                                </td>` };
                                ${element.loan == 0 ?
                                    `<td class="text-center"><a id="loanItemRow-${element.id}" 
                                data-id="${element.id}" data-loan="Activar" style="cursor: pointer;">
                                <i class="fa fa-times fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>
                                </td>`
                                    :
                                    `<td class="text-center"><a id="loanItemRow-${element.id}" 
                                data-id="${element.id}" data-loan="Desactivar" style="cursor: pointer;">
                                <i class="fa fa-check fa-lg " aria-hidden="true" style="margin-left: 5px; color : green"></i></a>
                                </td>` };
                                ${data.deleteData || data.master ?
                                `<td class="text-center"><a id="deleteItemRow-${element.id}" 
                                data-id="${element.id}" style="cursor: pointer;">
                                <i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>
                                </td>` : ''}; ` : ''};
                            </tr>`;
            });
            tableBody.html('');
            tableBody.append(rows);
            data.stationTrees.forEach((element) => {
                // Events =>
                $(`#cancelItemRow-${element.id}`).click(function () {
                    let all = $(this);
                    const idStation = all.attr('data-id');
                    const visible = all.attr('data-visible');
                    Swal.fire({
                        title: 'Estación',
                        text: `¿Está seguro deseas ${visible} la Estación?`,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            cancelStationsByPerimeter(idStation);
                        }
                    })
                });

                $(`#loanItemRow-${element.id}`).click(function () {
                    let all = $(this);
                    const idStation = all.attr('data-id');
                    const loan = all.attr('data-loan');
                    Swal.fire({
                        title: 'Estación',
                        text: `¿Está seguro deseas ${loan} la Estación comodato?`,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            loanStationsByPerimeter(idStation);
                        }
                    })
                });

                $(`#deleteItemRow-${element.id}`).click(function () {
                    let all = $(this);
                    const idStation = all.attr('data-id');
                    Swal.fire({
                        title: 'Eliminar Estación',
                        text: 'Al borrar la estación se borrarán las Inspecciones asociadas al Monitoreo. \n ¿Está seguro deseas eliminar la Estación? ',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            deleteStationsByPerimeterSingular(idStation);
                        }
                    })
                });
            });

        }

        // Relocate Station
        let idStationPerimeter, selectPerimeterSingular;
        $('#relocateStation').on('show.bs.modal', function(e) {
            idStationPerimeter = $(e.relatedTarget).data().id;
            let idParentNode = $(e.relatedTarget).data().parent;
            selectPerimeterSingular = $("#perimeterSelectSingular");
            let textStation = $(e.relatedTarget).data().text;
            let textStationSplit = textStation.split('#');
            let idTypeStation = $(e.relatedTarget).data().idtypestation;
            let selectTypeStationUnique = $("#selectTypeStationUnique");

            $('#inputNumberStation').val(textStationSplit[1]);
            $('#labelTextStation').html(textStation);

            $.ajax({
                type: "GET",
                url: route('get_perimeters_all_monitoring'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    customerId: customerId
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al cargar',data.message)
                    } else {
                        // Load perimeters select
                        selectPerimeterSingular.empty();
                        data.perimeters.forEach((element) => {
                            let option = '';
                            if (element.id_node === idParentNode) {
                                option = `<option selected value="${element.id_node}">${element.text}</option>`;
                            } else {
                                option = `<option value="${element.id_node}">${element.text}</option>`;
                            }
                            selectPerimeterSingular.append(option);
                        });
                        // Load type Stations
                        selectTypeStationUnique.empty();
                        data.typeStations.forEach((element) => {
                            let option = '';
                            if (element.id === idTypeStation) {
                                option = `<option selected value="${element.id}">${element.name}</option>`;
                            } else {
                                option = `<option value="${element.id}">${element.name}</option>`;
                            }
                            selectTypeStationUnique.append(option);
                        });

                    }
                }
            });

        });

        $('#btnRelocateStation').click(function () {
            let numberStation = document.getElementById('inputNumberStation').value;
            let selectTypeStationUnique = $('#selectTypeStationUnique').val();
            relocateStationsByPerimeter(idStationPerimeter, selectPerimeterSingular.val(), numberStation, selectTypeStationUnique);
        });

        function getTreeData(monitoringId) {
            //loaderPestWare('')
            let containerTree = $('#container-tree-pv');
            let treeAreaMonitoring = $("#jsShowTreeAreaMonitoringPv");
            const newTree = '<div id="jsShowTreeAreaMonitoringPv" class="demo" style="margin-top:1em; min-height:200px;"></div>';
            treeAreaMonitoring.remove();
            containerTree.append(newTree);
            let treeNewAreaMonitoring = $("#jsShowTreeAreaMonitoringPv");

            $.ajaxSetup({
                headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
            });
            $.ajax({
                type: 'GET',
                url: route('station_monitoring_tree_read', monitoringId),
                success: function(data) {
                    treeNewAreaMonitoring.jstree({
                        core: {
                            animation: 0,
                            check_callback: true,
                            force_text: true,
                            themes: { stripes: true },
                            data: data
                        },
                        types: {
                            "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
                            root: { icon: "fa fa-home", valid_children: ["default"] },
                            default: {
                                icon: "fa fa-th-large",
                                valid_children: ["default"]
                            },
                        },
                        plugins: ["dnd", "search", "state", "types"]
                    });
                    Swal.close();
                }
            });
        }

        $("#finishedMonitoring").click(function() {
            updateTreeDate(monitoringId);
        });

        $('#newAreaMonitoringModalv2').on('hidden.bs.modal', function () {
            location.reload();
        })

    });

});
