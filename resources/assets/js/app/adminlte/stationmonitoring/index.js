/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 26/04/2021
 * customers/_modalCustomerUser.blade.php
 */

$(document).ready(function(){
    //select job center stationmonitoing/index
    $('#selectJobCenterStationArea').on('change', function() {
        let idJobCenterStationArea = $(this).val();
        window.location.href = route('index_station_monitoring', idJobCenterStationArea);
    });

});