import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idOrder, cellphoneServiceOrder, cellphoneMainServiceOrder, urlinspection;

    $('#sendEmailWhatsapp').on('show.bs.modal', function(e) {
        Swal.close();
        idOrder = $(e.relatedTarget).data().id;
        urlinspection = $(e.relatedTarget).data().urlinspection;
        cellphoneServiceOrder = $(e.relatedTarget).data().cellphone;
        cellphoneMainServiceOrder = $(e.relatedTarget).data().cellphonemain;
        $("#cellphoneServiceOrder").val(cellphoneServiceOrder);
        $("#cellphoneMainServiceOrder").val(cellphoneMainServiceOrder);
    });

    $('#sendEmailOrderWhatsappButton').click(function () {
        let cellphone = document.getElementById('cellphoneServiceOrder').value;
        let cellphoneMain = document.getElementById('cellphoneMainServiceOrder').value;
        let cellphoneOtherServiceOrder = document.getElementById('cellphoneOtherServiceOrder').value;
        let urlSeparate = urlinspection.split('{');
        let urlCompleteCellphone = `${urlSeparate[0]}${cellphone}${urlSeparate[1]}${urlinspection}`;
        let urlCompleteMain = `${urlSeparate[0]}${cellphoneMain}${urlSeparate[1]}${urlinspection}`;
        let urlCompleteOther = `${urlSeparate[0]}${cellphoneOtherServiceOrder}${urlSeparate[1]}${urlinspection}`;
        if (cellphone !== '') window.open(urlCompleteCellphone, '_blank');
        if (cellphoneMain !== '') window.open(urlCompleteMain, '_blank');
        if (cellphoneOtherServiceOrder !== '') window.open(urlCompleteOther, '_blank');

        if (cellphone === '' && cellphoneMain === '' && cellphoneOtherServiceOrder === '') {
            showToast('warning','Error al enviar','No hay ningún número registrado')
        }
        else {
            $('#sendEmailWhatsapp').click();
        }

    });

});
