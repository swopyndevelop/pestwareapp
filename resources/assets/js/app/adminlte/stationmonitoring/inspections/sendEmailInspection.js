import {showToast} from "../../pestware/alerts";
import {loaderPestWare} from "../../pestware/loader";

$(document).ready(function(){

    let idOrder, email, emailInput;
    let emails = [];

    $('#sendEmailInspection').on('show.bs.modal', function(e) {
         idOrder = $(e.relatedTarget).data().id;
         email = $(e.relatedTarget).data().email;
         if (email.length === 0) {
            $("#emailText").text('No hay ningún correo registrado.');
         }else{
            $("#emailText").val(email);
         }
    });

    $('#sendEmailInspectionButton').click(function () {

        //get values input
        email = $('#emailText').val();
        emailInput = $('#email').val();
        if (emailInput.length === 0) {
            emailInput = 0;
        }

        if (email.length === 0) {
            email = 0;
        }

        if(email == 0 && emailInput == 0){
            showToast('warning','Correo no válido','Debes de ingresar un correo electrónico valido.');
        }else{
            emails.push(email);
            emails.push(emailInput);
            sendEmailInspection();
        }

    });

    function sendEmailInspection(){
        loaderPestWare('Cargando...');
            $.ajax({
                type: 'POST',
                url: route('mail_inspection'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: idOrder,
                    emails: emails
                },
                success: function (data) {
                    if (data.code == 500){
                        Swal.close();
                        showToast('warning','Algo salió Mal','Error al enviar el correo')
                    }
                    else{
                        Swal.close();
                        showToast('success-redirect','Correo Enviado',data.message,'index_inspections_monitoring');
                        $('.sendEmailInspectionButton').prop('disabled', false);
                    }
                }
        });
    }

});
