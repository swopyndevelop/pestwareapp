
$(document).ready(function(){
    // Filters Date
    let initialDateInspection, finalDateInspection, filterDateInspection;

        filterDateInspection = $('input[name="filterDateInspection"]');
        filterDateInspection.daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        filterDateInspection.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateInspection = picker.startDate.format('YYYY-MM-DD');
            finalDateInspection = picker.endDate.format('YYYY-MM-DD');
        });

        filterDateInspection.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    $('#btnFilterInspection').click(function() {
        let idMonitoringInspection = $('#idMonitoringInspection').val();
        let customersFilter = $('#customersFilter option:selected').val();
        let addressFilter = $('#addressFilter option:selected').val();
        initialDateInspection = initialDateInspection === undefined ? "null" : initialDateInspection;
        finalDateInspection = finalDateInspection === undefined ? "null" : finalDateInspection;
        window.location.href = `/inspection/monitoring/index?initialDateInspection=${initialDateInspection}&finalDateInspection=${finalDateInspection}&idMonitoringInspection=${idMonitoringInspection}&customersFilter=${customersFilter}&addressFilter=${addressFilter}`;
        initialDateInspection = "null";
        finalDateInspection = "null";
    });

});
