import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

/**
 * @author Alberto Martínez
 * @version 01/04/2019
 * register/index_inventory.blade.php
 */
$(document).ready(function() {

    //Select Job Center
    $('#selectJobCenterInventoryEntries').on('change', function() {
        let idJobCenterInventoryEntries = $(this).val();
        window.location.href = route('index_entry', idJobCenterInventoryEntries);
    });

    //select job center
    let idJobCenterInventory;
    $('#selectJobCenterInventory').on('change', function() {
        idJobCenterInventory = $(this).val();
        getDataJobCenters();
    });

    // Export Detail
    $('#exportExcelDetailInventory').click(function() {
        let storeIdDetail = document.getElementById("storeIdDetailIdJobCenter").value;
        if (storeIdDetail == 0) {
            let storeIdDetailEmployee = document.getElementById("storeIdDetailIdEmployee").value;
            loaderPestWare('Exportando Reporte Empleado...');
            setTimeout(() => {
                Swal.close();
            }, 3000);
            window.location.href = `/inventory/index?storeIdDetailEmployee=${storeIdDetailEmployee}&exportEmployee=true`;
        } else {
            loaderPestWare('Exportando Reporte Sucursal...');
            setTimeout(() => {
                Swal.close();
            }, 3000);
            window.location.href = `/inventory/index?storeIdDetail=${storeIdDetail}&export=true`;
        }

    });

    // Export General
    $('#exportExcelInventory').click(function() {
        loaderPestWare('Exportando Reporte...');
        setTimeout(() => {
            Swal.close();
        }, 5000);
        window.location.href = `/inventory/index?exportGeneral=true`;
    });

    // Export Detail Historical Inventory
    let storeIdDetail = 0, storeIdDetailEmployee = 0, storeIdDetailProduct= 0, storeIdCompany= 0;
    $('#exportExcelDetailHistoricalInventory').click(function() {
        storeIdDetail = document.getElementById("inventoryIdDetailIdJobCenter").value;
        storeIdDetailEmployee = document.getElementById("inventoryIdDetailIdIdEmployee").value;
        storeIdDetailProduct = document.getElementById("inventoryIdDetailIdIdProduct").value;
        storeIdCompany = document.getElementById("inventoryIdDetailIdIdCompany").value;
        if (storeIdDetail != 0) {
            loaderPestWare('Exportando Reporte Sucursal...');
            setTimeout(() => {
                Swal.close();
            }, 3000);
            window.location.href = `/inventory/index?idDetailHistoricalJobCenter=${storeIdDetail}&exportJobCenterHistorical=true`;
        }
        if (storeIdDetailEmployee != 0) {
            loaderPestWare('Exportando Reporte Empleado...');
            setTimeout(() => {
                Swal.close();
            }, 3000);
            window.location.href = `/inventory/index?idDetailHistoricalEmployee=${storeIdDetailEmployee}&exportEmployeeHistorical=true`;
        }
        if (storeIdDetailProduct != 0) {
            loaderPestWare('Exportando Reporte Producto...');
            setTimeout(() => {
                Swal.close();
            }, 3000);
            window.location.href = `/inventory/index?idDetailHistoricalProduct=${storeIdDetailProduct}&exportEmployeeHistorical=true`;
        }

        if (storeIdCompany != 0) {
            loaderPestWare('Exportando Reporte Compañia...');
            setTimeout(() => {
                Swal.close();
            }, 3000);
            window.location.href = `/inventory/index?idDetailHistoricalCompany=${storeIdCompany}`;
        }

    });

    // Filters
    let initialDateInventory, finalDateInventory, filterDateInput, filterNameInventory;
    filterDateInput = $('input[name="filterDateInventory"]');
    $(function() {

        filterDateInput.daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        filterDateInput.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateInventory = picker.startDate.format('YYYY-MM-DD');
            finalDateInventory = picker.endDate.format('YYYY-MM-DD');
        });

        filterDateInput.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    $('#btnFilterInventory').click(function() {
        filterNameInventory = $('#filterNameInventory').val();
        initialDateInventory = initialDateInventory == undefined ? null : initialDateInventory;
        finalDateInventory = finalDateInventory == undefined ? null : finalDateInventory;
        getDataJobCenters();
        initialDateInventory = null;
        finalDateInventory = null;
    });

    // Get DataInventoryEmployees
    let inventoriesDB = [];
    getDataJobCenters();

    function getDataJobCenters() {
        loaderPestWare('Cargando ...');
        $('#storeHouseJobCenters td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_job_centers'),
            data: {
                _token: token,
                filterNameInventory: filterNameInventory,
                initialDateInventory: initialDateInventory,
                finalDateInventory: finalDateInventory,
                idJobCenterInventory: idJobCenterInventory
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#storeHouseJobCenters');
                    const {collectionData, employees, products, symbol_country} = response;

                    // Select Employees
                    let selectEmployees = $("#usersFilter");
                    selectEmployees.empty();
                    selectEmployees.append(`<option value="0">Todos</option>`);
                    employees.forEach((element) => {
                        let option = `<option value="${element.id}">${element.name}</option>`;
                        selectEmployees.append(option);
                    });
                    selectEmployees.addClass("selectpicker");

                    // Select Products
                    let selectProducts = $("#productsFilter");
                    selectProducts.empty();
                    selectProducts.append(`<option value="0">Todos</option>`);
                    products.forEach((element) => {
                        let option = `<option value="${element.id}">${element.name}</option>`;
                        selectProducts.append(option);
                    });
                    selectProducts.addClass("selectpicker");

                    /*// Select Destinations
                    let selectDestinations = $("#destinationsFilter");
                    selectDestinations.empty();
                    selectDestinations.append(`
                                <option value="0" selected>Destino</option>
                                <option value="2">Todos</option>`);
                    selectDestinations.addClass("selectpicker");*/

                    // Class Select Search
                    $('.selectpicker').selectpicker();

                    collectionData.sort(sortByProperty("name"));
                    collectionData.forEach((element) => {
                        const {name, total_units, total, date, id_job_center, id_job_center_encrypt, id_employee, id_employee_encrypt} = element;
                        inventoriesDB.push({
                            name: name,
                            date: date,
                            total_units: total_units,
                            total: total,
                            id_job_center_encrypt: id_job_center_encrypt,
                            id_employee_encrypt: id_employee_encrypt
                        });

                        rows += `<tr>
                                <td class="text-primary text-bold">
                                <div class="btn-group"><a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                ${name}<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                ${id_job_center !== 0 ? 
                                    `<li><a data-toggle="modal" data-target="#storehouseDetail" data-idJobCenter="${id_job_center}" style="cursor:pointer">
                                        <i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a>
                                    </li>` : 
                                    `<li><a data-toggle="modal" data-target="#storehouseDetail" data-idEmployee="${id_employee}" data-idJobCenter="0" style="cursor:pointer">
                                        <i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver</a>
                                    </li>`
                                }
                                ${id_job_center !== 0 ?
                                    `<li><a data-toggle="modal" data-target="#storehouseHistorical" data-idJobCenter="${id_job_center}" style="cursor:pointer">
                                        <i class="fa fa-arrows-v" aria-hidden="true" style="color: grey;"></i>Histórico</a>
                                    </li>` :
                                    `<li><a data-toggle="modal" data-target="#storehouseHistorical" data-idEmployee="${id_employee}" data-idJobCenter="0" style="cursor:pointer">
                                        <i class="fa fa-arrows-v" aria-hidden="true" style="color: grey;"></i>Histórico</a>
                                    </li>`
                                }
                                ${id_job_center !== 0 ?
                                    `<li><a data-toggle="modal" data-target="#movementStorehouse" data-idJobCenter="${id_job_center}" data-idEmployee="0" style="cursor:pointer">
                                        <i class="fa fa-arrows" aria-hidden="true" style="color: #D35400;"></i>Movimientos</a>
                                    </li>` :
                                    `<li><a data-toggle="modal" data-target="#movementStorehouse" data-idEmployee="${id_employee}" data-idJobCenter="0" style="cursor:pointer">
                                        <i class="fa fa-arrows" aria-hidden="true" style="color: #D35400;"></i>Movimientos</a>
                                    </li>`
                                    }
                                </ul>
                                </div>
                                </td>
                                <td class="">${date}</td>
                                <td class="">${total_units}</td>
                                <td class="">${symbol_country}${total.toFixed(2)}</td>
                        </tr>`;
                    });
                    table.append(rows);
                    Swal.close();
                }
            }
        });
    }

    let idEmployee, idJobCenter, idProduct = 0, idCompany;
    $('#storehouseDetail').on('show.bs.modal', function(e) {
        idJobCenter = $(e.relatedTarget).data().idjobcenter;
        idEmployee = $(e.relatedTarget).data().idemployee;
        $("#storeIdDetailIdJobCenter").val(idJobCenter);
        $("#storeIdDetailIdEmployee").val(idEmployee);
        if (idJobCenter !== 0) getDataStoreHouse(idJobCenter)
        else getDataStoreHouseEmployee(idEmployee)
    });

    let idProductGlobalEmployee = 0, idProductGlobalJobCenter = 0;
    $('#storehouseHistorical').on('show.bs.modal', function(e) {
        idJobCenter = $(e.relatedTarget).data().idjobcenter;
        idEmployee = $(e.relatedTarget).data().idemployee;
        idProduct = $(e.relatedTarget).data().product;
        idCompany = $(e.relatedTarget).data().idcompany;
        if (idJobCenter === undefined) idJobCenter = 0;
        if (idEmployee === undefined) idEmployee = 0;
        if (idProduct === undefined) idProduct = 0;
        if (idCompany === undefined) idCompany = 0;
        $("#inventoryIdDetailIdJobCenter").val(idJobCenter);
        $("#inventoryIdDetailIdIdEmployee").val(idEmployee);
        $("#inventoryIdDetailIdIdProduct").val(idProduct);
        $("#inventoryIdDetailIdIdCompany").val(idCompany);
        if (idJobCenter !== 0 && idProduct === 0) getDataHistoricalInventory(idJobCenter)
        else if (idProduct !== 0 && idJobCenter !== 0) {
            idProductGlobalJobCenter = idProduct;
            getDataHistoricalInventoryProduct(idProductGlobalJobCenter, idJobCenter)
        }
        else if (idEmployee !== 0 && idProduct === 0) getDataHistoricalInventoryEmployee(idEmployee)
        else if (idProduct !== 0 && idEmployee !== 0) {
            idProductGlobalEmployee = idProduct;
            getDataHistoricalInventoryProductEmployee(idProductGlobalEmployee, idEmployee)
        }
        else if (idCompany !== 0) getDataHistoricalInventoryCompany(idCompany)
    });

    $('#storehouseHistorical').on('hidden.bs.modal', function () {
        idProductGlobalEmployee = 0;
        idProductGlobalJobCenter = 0;
    })

    $('#adjustmentEntry').on('show.bs.modal', function(e) {
        let idJobCenter = $(e.relatedTarget).data().idjobcenter;
        let entryExit = $(e.relatedTarget).data().entryexit;
        let title = $(e.relatedTarget).data().title;
        $('#adjustment_type').val(entryExit);
        $('#modalTitleEntryExit').html(title);
        if (idJobCenter === undefined) {
            let idStoreHouseEmployee = $(e.relatedTarget).data().id;
            getDataIdStoreHouseEmployee(idStoreHouseEmployee);
        }
        else getDataIdStoreHouse(idJobCenter);
    });

    let idJobCenterGlobal, idEmployeeGlobal;
    $('#movementStorehouse').on('show.bs.modal', function(e) {
        idJobCenter = $(e.relatedTarget).data().idjobcenter;
        idEmployee = $(e.relatedTarget).data().idemployee;
        $("#storeIdDetailIdJobCenter").val(idJobCenter);
        $("#storeIdDetailIdEmployee").val(idEmployee);
        idJobCenterGlobal = idJobCenter;
        idEmployeeGlobal = idEmployee;
        if (idJobCenter !== 0) getDataMovementStoreHouse(idJobCenter)
        else getDataMovementStoreHouseEmployee(idEmployee)
    });

    $('#movementStorehouse').on('hidden.bs.modal', function () {
        idJobCenterGlobal = 0;
        idEmployeeGlobal = 0;
    })

    let idMovement, typeMovementStoreHouse;
    let isAdjustment = 0;
    $('#detailMovementSoreHouse').on('show.bs.modal', function(e) {
        idMovement = $(e.relatedTarget).data().id;
        typeMovementStoreHouse = $(e.relatedTarget).data().type;
        idJobCenter = $(e.relatedTarget).data().idjobcenter;
        idEmployee = $(e.relatedTarget).data().idemployee;
        isAdjustment = 0;
        $("#storeIdDetailIdJobCenter").val(idJobCenter);
        $("#storeIdDetailIdEmployee").val(idEmployee);
        getDataDetailMovementStoreHouse(idJobCenter, idMovement, typeMovementStoreHouse, idEmployee, isAdjustment);
    });

    $('#detailMovementAdjustment').on('show.bs.modal', function(e) {
        idMovement = $(e.relatedTarget).data().id;
        typeMovementStoreHouse = $(e.relatedTarget).data().type;
        idJobCenter = $(e.relatedTarget).data().idjobcenter;
        idEmployee = $(e.relatedTarget).data().idemployee;
        isAdjustment = $(e.relatedTarget).data().adjustment
        $("#storeIdDetailIdJobCenter").val(idJobCenter);
        $("#storeIdDetailIdEmployee").val(idEmployee);
        getDataDetailMovementStoreHouse(idJobCenter, idMovement, typeMovementStoreHouse, idEmployee, isAdjustment);
    });

    $('#detailMovementAdjustment').on('hidden.bs.modal', function () {
        isAdjustment = 0;
    })

    $('#btnSaveSetting').click(function () {
        let id_storehouse = $('#id_storehouse').val();
        let adjustment_type = $('#adjustment_type').val();
        let units_before = $('#units_before').val();
        let quantity_before = $('#quantity_before').val();
        let total_before = $('#total_before').val();
        let units_adjustment = $('#units_adjustment').val();
        let quantity_adjustment = $('#quantity_adjustment').val();
        let total_adjustment = $('#total_adjustment').val();
        let type_entry = $('#type_entry').val();
        let units_entry = $('#units_entry').val();
        let comments = $('#comments').val();

        if (units_entry === '') showToast('warning','Unidades de Cantidad','Debes ingresar las unidades');
        else if (comments.length <= 1) showToast('warning','Comentarios','Debes ingresar algún comentario');
        else {
            loaderPestWare('Generando Ajuste...');
            $.ajax({
                type: "POST",
                url: route('store_adjustment'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id_storehouse: id_storehouse,
                    adjustment_type: adjustment_type,
                    units_before: units_before,
                    quantity_before: quantity_before,
                    total_before: total_before,
                    units_adjustment: units_adjustment,
                    quantity_adjustment: quantity_adjustment,
                    total_adjustment: total_adjustment,
                    type_entry: type_entry,
                    units_entry: units_entry,
                    comments: comments
                },
                success: function(data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error','Error al eliminar', data.message)
                    } else {
                        Swal.close();
                        showToast('success','Guardando Ajuste', data.message)
                        $('#adjustmentEntry').click();
                        $('#units_entry').val('');
                        $('#comments').val('');
                        if (idJobCenter === 0) getDataStoreHouseEmployee(idEmployee);
                        else getDataStoreHouse(idJobCenter);
                    }
                }
            });
        }

    });

    // Filters Variables
    let initialDateHistoricalInventory, finalDateHistoricalInventory, filterDateInventory, folioInventory = null,
        userInventory = 0, typeMovement = 0, productInventory= 0, destinationsFilter= 0, nameSource = null, nameDestiny = null;

    // Date Historical Inventory
    filterDateInventory = $('input[name="filterDateHistoricalInventory"]');
    filterDateInventory.daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    filterDateInventory.on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        initialDateHistoricalInventory = picker.startDate.format('YYYY-MM-DD');
        finalDateHistoricalInventory = picker.endDate.format('YYYY-MM-DD');
    });

    filterDateInventory.on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#btnFilterHistoricalInventory').click(function() {
        folioInventory = $('#folio').val();
        userInventory = $('#usersFilter option:selected').val();
        typeMovement = $('#typeMovementsFilter option:selected').val();
        productInventory = $('#productsFilter option:selected').val();
        /*destinationsFilter = $('#destinationsFilter option:selected').val();*/
        nameSource = $('#nameSource').val();
        nameDestiny = $('#nameDestiny').val();
        initialDateHistoricalInventory = initialDateHistoricalInventory === undefined ? null : initialDateHistoricalInventory;
        finalDateHistoricalInventory = finalDateHistoricalInventory === undefined ? null : finalDateHistoricalInventory;
        if (idJobCenter !== 0 && idProductGlobalJobCenter === 0) getDataHistoricalInventory(idJobCenter);
        if (idProductGlobalJobCenter !== 0 && idJobCenter !== 0) getDataHistoricalInventoryProduct(idProductGlobalJobCenter ,idJobCenter);
        if (idEmployee !== 0 && idProductGlobalEmployee === 0 && idJobCenter === 0 && idProductGlobalJobCenter === 0) getDataHistoricalInventoryEmployee(idEmployee);
        if (idProductGlobalEmployee !== 0 && idEmployee !== 0) getDataHistoricalInventoryProductEmployee(idProductGlobalEmployee, idEmployee)
        if (idCompany !== 0) getDataHistoricalInventoryCompany(idCompany)
    });

    // Filters Variables
    let initialDateMovementStoreHouse, finalDateMovementStoreHouse, filterDateMovementStoreHouse, folioMovementStoreHouse = null,
        nameSourceMovementStoreHouse = null, nameDestinyMovementStoreHouse = null;

    // Date Historical Inventory
    filterDateMovementStoreHouse = $('input[name="filterDateMovementStoreHouse"]');
    filterDateMovementStoreHouse.daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    filterDateMovementStoreHouse.on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        initialDateMovementStoreHouse = picker.startDate.format('YYYY-MM-DD');
        finalDateMovementStoreHouse = picker.endDate.format('YYYY-MM-DD');
    });

    filterDateMovementStoreHouse.on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#btnFilterMovementStoreHouse').click(function() {
        folioMovementStoreHouse = $('#folioMovementStoreHouse').val();
        nameSourceMovementStoreHouse = $('#nameSourceMovementStoreHouse').val();
        nameDestinyMovementStoreHouse = $('#nameDestinyMovementStoreHouse').val();
        if (idJobCenterGlobal !== 0) {
            getDataMovementStoreHouse(idJobCenter)
            initialDateMovementStoreHouse = null;
            finalDateMovementStoreHouse = null
        }
        if (idEmployeeGlobal !== 0) {
            initialDateMovementStoreHouse = null;
            finalDateMovementStoreHouse = null
            getDataMovementStoreHouseEmployee(idEmployee)
        }
    });

    function getDataHistoricalInventory(idJobCenter){
        loaderPestWare('Cargando Historial Sucursal...');
        $('#tableHistoricalInventory td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_view_historical_inventory_job_center', idJobCenter),
            data: {
                _token: token,
                folioInventory: folioInventory,
                userInventory: userInventory,
                typeMovement: typeMovement,
                productInventory: productInventory,
                nameSource: nameSource,
                nameDestiny: nameDestiny,
                initialDateHistoricalInventory: initialDateHistoricalInventory,
                finalDateHistoricalInventory: finalDateHistoricalInventory,
                destinationsFilter: destinationsFilter
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableHistoricalInventory');
                    const {historicalInventoriesDetail,symbol_country, profileJobCenter, stock, total, deleteData, master,
                        destinationsFilter} = response;
                    $('#nameHistoricalInventory').html(`${profileJobCenter.name}`);
                    $('#totalUnitsInventory').html(`${stock}`);
                    $('#valueTotalInventory').html(`${symbol_country}${total.toFixed(2)}`);

                    let productsDetail;
                    let productsDetailO = historicalInventoriesDetail.filter(res => res.source == profileJobCenter.name && res.is_destiny == 0);
                    let productsDetailD = historicalInventoriesDetail.filter(res => res.source != profileJobCenter.name && res.is_destiny == 1);
                    productsDetail = productsDetailO.concat(productsDetailD)
                    productsDetail.sort(sortByProperty("created_at")).reverse();

                    /*// Function Inversa filter
                    if (destinationsFilter == 0) {
                        dataFilterInventory = productsDetail.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[2] !== 'O';
                        } );

                            dataFilterInventory = dataFilterInventory.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[3] !== 'O';
                        } );
                        productsDetail = dataFilterInventory;
                    }*/

                    let sumQuantity = 0, sumFractionQuantity = 0, sumUnitPrice = 0, sumValueMovement = 0, sumValueInventory = 0;
                    productsDetail.forEach((element) => {
                        const {folio, date, hour, name_user, nameType, name_product, unit_product, source, destiny, quantity, before_stock, after_stock,
                            fraction_quantity, fraction_before_stock, fraction_after_stock, unit_price, value_movement, value_inventory} = element;
                        inventoriesDB.push({
                            folio: folio,
                            date: date,
                            hour: hour,
                            name_user: name_user,
                            nameType: nameType,
                            name_product: name_product,
                            unit_product: unit_product,
                            source: source,
                            destiny: destiny,
                            quantity: quantity,
                            before_stock: before_stock,
                            after_stock: after_stock,
                            fraction_quantity: fraction_quantity,
                            fraction_before_stock: fraction_before_stock,
                            fraction_after_stock: fraction_after_stock,
                            unit_price: unit_price,
                            value_movement: value_movement,
                            value_inventory: value_inventory
                        });

                        rows += `
                                <tr class="small">
                                <td class="text-primary text-bold">
                                    <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ${folio}
                                        </a>
                                    </div>
                                </td>
                                <td class="">${date} <br> ${hour}</td>
                                <td class="">${name_user}</td>
                                <td class="">${nameType}</td>
                                <td class="">${name_product}</td>
                                <td class="">${source}</td>
                                <td class="">${destiny}</td>
                                <td class="">${quantity}</td>
                                <td class="">${fraction_quantity} ${unit_product}s</td>
                                <td class="">${before_stock}</td>
                                <td class="">${after_stock}</td>
                                <td class="">${fraction_before_stock} ${unit_product}s</td>
                                <td class="">${fraction_after_stock} ${unit_product}s</td>
                                <td class="">${symbol_country}${unit_price.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_movement.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_inventory.toFixed(2)}</td>
                                </tr>`;

                        // Total Generals
                        sumQuantity = sumQuantity+quantity;
                        sumFractionQuantity = sumFractionQuantity+fraction_quantity;
                        sumUnitPrice = sumUnitPrice+unit_price;
                        sumValueMovement = sumValueMovement+value_movement;
                        sumValueInventory = sumValueInventory+value_inventory;

                    });
                    table.append(rows);

                    //Labels Totals Sum
                    $('#quantityTotal').html(sumQuantity);
                    $('#fractionQuantityTotal').html(sumFractionQuantity);
                    $('#unitPriceTotal').html(symbol_country + sumUnitPrice.toFixed(2));
                    $('#valueMovementTotal').html(symbol_country + sumValueMovement.toFixed(2));
                    $('#valueInventoryTotal').html(symbol_country + sumValueInventory.toFixed(2));

                    // reset filters
                    $('#folio').val('');
                    $('#filterDateHistoricalInventory').val('');
                    $('#nameSource').val('');
                    $('#nameDestiny').val('')
                    Swal.close();
                }
            }
        });
    }

    function getDataHistoricalInventoryProduct(idProductGlobalJobCenter, idJobCenter){
        loaderPestWare('Cargando Historial Producto Sucursal...');
        $('#tableHistoricalInventory td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_view_historical_inventory_product_center', [idProductGlobalJobCenter, idJobCenter]),
            data: {
                _token: token,
                folioInventory: folioInventory,
                userInventory: userInventory,
                typeMovement: typeMovement,
                productInventory: productInventory,
                nameSource: nameSource,
                nameDestiny: nameDestiny,
                initialDateHistoricalInventory: initialDateHistoricalInventory,
                finalDateHistoricalInventory: finalDateHistoricalInventory,
                destinationsFilter: destinationsFilter
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableHistoricalInventory');
                    let sumQuantity = 0, sumFractionQuantity = 0,sumUnitPrice = 0, sumValueMovement = 0, sumValueInventory = 0;
                    const {historicalInventoriesDetail,symbol_country, profileJobCenter, stock, total, initialDate, finalDate, folio, userId,
                        typeMovementId, productId, nameSourceSearch, nameDestinySearch, destinationsFilter} = response;

                    $('#nameHistoricalInventory').html(`${profileJobCenter.name}`);
                    $('#totalUnitsInventory').html(`${stock}`);
                    $('#valueTotalInventory').html(`${symbol_country}${total.toFixed(2)}`);

                    let productsDetail;
                    let productsDetailO = historicalInventoriesDetail.filter(res => res.source == profileJobCenter.name && res.is_destiny == 0);
                    let productsDetailD = historicalInventoriesDetail.filter(res => res.source != profileJobCenter.name && res.is_destiny == 1);
                    productsDetail = productsDetailO.concat(productsDetailD)
                    productsDetail.sort(sortByProperty("created_at")).reverse();
                    let dataFilterInventory = productsDetail;

                    /*// Function Inversa filter
                    if (destinationsFilter == 0) {
                        dataFilterInventory = productsDetail.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[2] !== 'O';
                        } );

                        dataFilterInventory = dataFilterInventory.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[3] !== 'O';
                        } );
                        productsDetail = dataFilterInventory;

                    }*/

                    if (initialDate != null && finalDate != null) {
                        dataFilterInventory = productsDetail.filter(dateProduct => dateProduct.date >= initialDate && dateProduct.date <= finalDate)
                        productsDetail = dataFilterInventory;
                    }

                    if (folio != null) {
                        let expresion = new RegExp(`${folio}.*`, "i");
                        dataFilterInventory = productsDetail.filter(folioHistorical => expresion.test(folioHistorical.folio));
                        productsDetail = dataFilterInventory;
                    }

                    if (userId != 0) {
                        dataFilterInventory = productsDetail.filter(user => user.id_user == userId);
                        productsDetail = dataFilterInventory;
                    }

                    if (typeMovementId != 0) {
                        dataFilterInventory = productsDetail.filter(typeMovementHistorical => typeMovementHistorical.id_type_movement_inventory == typeMovementId);
                        productsDetail = dataFilterInventory;
                    }

                    if (productId != 0) {
                        dataFilterInventory = productsDetail.filter(productHistorical => productHistorical.id_product == productId);
                        productsDetail = dataFilterInventory;
                    }

                    if (nameSourceSearch != null) {
                        let expresion = new RegExp(`${nameSourceSearch}.*`, "i");
                        dataFilterInventory = productsDetail.filter(nameHistorical => expresion.test(nameHistorical.source));
                        productsDetail = dataFilterInventory;
                    }

                    if (nameDestinySearch != null) {
                        let expresion = new RegExp(`${nameDestinySearch}.*`, "i");
                        dataFilterInventory = productsDetail.filter(nameHistorical => expresion.test(nameHistorical.destiny));
                        productsDetail = dataFilterInventory;
                    }

                    productsDetail.forEach((element) => {
                        const {folio, date, hour, name_user, nameType, name_product, unit_product, source, destiny, quantity, before_stock, after_stock,
                            fraction_quantity, fraction_before_stock, fraction_after_stock, unit_price, value_movement, value_inventory} = element;
                        inventoriesDB.push({
                            folio: folio,
                            date: date,
                            hour: hour,
                            name_user: name_user,
                            nameType: nameType,
                            name_product: name_product,
                            unit_product: unit_product,
                            source: source,
                            destiny: destiny,
                            quantity: quantity,
                            before_stock: before_stock,
                            after_stock: after_stock,
                            fraction_quantity: fraction_quantity,
                            fraction_before_stock: fraction_before_stock,
                            fraction_after_stock: fraction_after_stock,
                            unit_price: unit_price,
                            value_movement: value_movement,
                            value_inventory: value_inventory
                        });

                        rows += `
                                <tr class="small">
                                <td class="text-primary text-bold">
                                    <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ${folio}
                                        </a>
                                    </div>
                                </td>
                                <td class="">${date} <br> ${hour}</td>
                                <td class="">${name_user}</td>
                                <td class="">${nameType}</td>
                                <td class="">${name_product}</td>
                                <td class="">${source}</td>
                                <td class="">${destiny}</td>
                                <td class="">${quantity}</td>
                                <td class="">${fraction_quantity} ${unit_product}s</td>
                                <td class="">${before_stock}</td>
                                <td class="">${after_stock}</td>
                                <td class="">${fraction_before_stock} ${unit_product}s</td>
                                <td class="">${fraction_after_stock} ${unit_product}s</td>
                                <td class="">${symbol_country}${unit_price.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_movement.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_inventory.toFixed(2)}</td>
                                </tr>`;

                        // Total Generals
                        sumQuantity = sumQuantity+quantity;
                        sumFractionQuantity = sumFractionQuantity+fraction_quantity;
                        sumUnitPrice = sumUnitPrice+unit_price;
                        sumValueMovement = sumValueMovement+value_movement;
                        sumValueInventory = sumValueInventory+value_inventory;
                    });
                    table.append(rows);

                    //Labels Totals Sum
                    $('#quantityTotal').html(sumQuantity);
                    $('#fractionQuantityTotal').html(sumFractionQuantity);
                    $('#unitPriceTotal').html(symbol_country + sumUnitPrice.toFixed(2));
                    $('#valueMovementTotal').html(symbol_country + sumValueMovement.toFixed(2));
                    $('#valueInventoryTotal').html(symbol_country + sumValueInventory.toFixed(2));

                    // reset filters
                    $('#folio').val('');
                    $('#filterDateHistoricalInventory').val('');
                    $('#nameSource').val('');
                    $('#nameDestiny').val('')
                    Swal.close();
                }
            }
        });
    }

    function getDataHistoricalInventoryEmployee(idEmployee){
        loaderPestWare('Cargando Historial Empleado...');
        $('#tableHistoricalInventory td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_view_historical_inventory_employee', idEmployee),
            data: {
                _token: token,
                folioInventory: folioInventory,
                userInventory: userInventory,
                typeMovement: typeMovement,
                productInventory: productInventory,
                nameSource: nameSource,
                nameDestiny: nameDestiny,
                initialDateHistoricalInventory: initialDateHistoricalInventory,
                finalDateHistoricalInventory: finalDateHistoricalInventory,
                destinationsFilter: destinationsFilter
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableHistoricalInventory');
                    let sumQuantity = 0, sumFractionQuantity = 0,sumUnitPrice = 0, sumValueMovement = 0, sumValueInventory = 0;
                    const {historicalInventoriesDetail, symbol_country, employee, stock, total, initialDate, finalDate, folio, userId,
                        typeMovementId, productId, nameSourceSearch, nameDestinySearch, destinationsFilter} = response;
                    $('#nameHistoricalInventory').html(`${employee.name}`);
                    $('#totalUnitsInventory').html(`${stock}`);
                    $('#valueTotalInventory').html(`${symbol_country}${total.toFixed(2)}`);

                    let productsDetail;
                    let productsDetailO = historicalInventoriesDetail.filter(res => res.source == employee.name && res.is_destiny == 0);
                    let productsDetailD = historicalInventoriesDetail.filter(res => res.source != employee.name && res.is_destiny == 1);
                    productsDetail = productsDetailO.concat(productsDetailD)
                    productsDetail.sort(sortByProperty("created_at")).reverse();
                    let dataFilterInventory = productsDetail;

                   /* // Function Inversa filter
                    if (destinationsFilter == 0) {

                        dataFilterInventory = productsDetail.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[2] !== 'O';
                        } );

                        dataFilterInventory = dataFilterInventory.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[3] !== 'O';
                        } );
                        productsDetail = dataFilterInventory;

                    }*/

                    if (initialDate != null && finalDate != null) {
                        dataFilterInventory = productsDetail.filter(dateProduct => dateProduct.date >= initialDate && dateProduct.date <= finalDate)
                        productsDetail = dataFilterInventory;
                    }

                    if (folio != null) {
                        let expresion = new RegExp(`${folio}.*`, "i");
                        dataFilterInventory = productsDetail.filter(folioHistorical => expresion.test(folioHistorical.folio));
                        productsDetail = dataFilterInventory;
                    }

                    if (userId != 0) {
                        dataFilterInventory = productsDetail.filter(user => user.id_user == userId);
                        productsDetail = dataFilterInventory;
                    }

                    if (typeMovementId != 0) {
                        dataFilterInventory = productsDetail.filter(typeMovementHistorical => typeMovementHistorical.id_type_movement_inventory == typeMovementId);
                        productsDetail = dataFilterInventory;
                    }

                    if (productId != 0) {
                        dataFilterInventory = productsDetail.filter(productHistorical => productHistorical.id_product == productId);
                        productsDetail = dataFilterInventory;
                    }

                    if (nameSourceSearch != null) {
                        let expresion = new RegExp(`${nameSourceSearch}.*`, "i");
                        dataFilterInventory = productsDetail.filter(nameHistorical => expresion.test(nameHistorical.source));
                        productsDetail = dataFilterInventory;
                    }

                    if (nameDestinySearch != null) {
                        let expresion = new RegExp(`${nameDestinySearch}.*`, "i");
                        dataFilterInventory = productsDetail.filter(nameHistorical => expresion.test(nameHistorical.destiny));
                        productsDetail = dataFilterInventory;
                    }

                    productsDetail.forEach((element) => {
                        const {folio, date, hour, name_user, nameType, name_product, unit_product, source, destiny, quantity, before_stock, after_stock,
                            fraction_quantity, fraction_before_stock, fraction_after_stock, unit_price, value_movement, value_inventory} = element;
                        inventoriesDB.push({
                            folio: folio,
                            date: date,
                            hour: hour,
                            name_user: name_user,
                            nameType: nameType,
                            name_product: name_product,
                            unit_product: unit_product,
                            source: source,
                            destiny: destiny,
                            quantity: quantity,
                            before_stock: before_stock,
                            after_stock: after_stock,
                            fraction_quantity: fraction_quantity,
                            fraction_before_stock: fraction_before_stock,
                            fraction_after_stock: fraction_after_stock,
                            unit_price: unit_price,
                            value_movement: value_movement,
                            value_inventory: value_inventory
                        });

                        rows += `
                                <tr class="small">
                                <td class="text-primary text-bold">
                                    <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ${folio}
                                        </a>
                                    </div>
                                </td>
                                <td class="">${date} <br> ${hour}</td>
                                <td class="">${name_user}</td>
                                <td class="">${nameType}</td>
                                <td class="">${name_product}</td>
                                <td class="">${source}</td>
                                <td class="">${destiny}</td>
                                <td class="">${quantity}</td>
                                <td class="">${fraction_quantity} ${unit_product}s</td>
                                <td class="">${before_stock}</td>
                                <td class="">${after_stock}</td>
                                <td class="">${fraction_before_stock} ${unit_product}s</td>
                                <td class="">${fraction_after_stock} ${unit_product}s</td>
                                <td class="">${symbol_country}${unit_price.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_movement.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_inventory.toFixed(2)}</td>
                                </tr>`;

                        // Total Generals
                        sumQuantity = sumQuantity+quantity;
                        sumFractionQuantity = sumFractionQuantity+fraction_quantity;
                        sumUnitPrice = sumUnitPrice+unit_price;
                        sumValueMovement = sumValueMovement+value_movement;
                        sumValueInventory = sumValueInventory+value_inventory;

                    });
                    table.append(rows);

                    //Labels Totals Sum
                    $('#quantityTotal').html(sumQuantity);
                    $('#fractionQuantityTotal').html(sumFractionQuantity);
                    $('#unitPriceTotal').html(symbol_country + sumUnitPrice.toFixed(2));
                    $('#valueMovementTotal').html(symbol_country + sumValueMovement.toFixed(2));
                    $('#valueInventoryTotal').html(symbol_country + sumValueInventory.toFixed(2));

                    // reset filters
                    $('#folio').val('');
                    $('#filterDateHistoricalInventory').val('');
                    $('#nameSource').val('');
                    $('#nameDestiny').val('')
                    Swal.close();
                }
            }
        });
    }

    function getDataHistoricalInventoryProductEmployee(idProductGlobalEmployee, idEmployee){
        loaderPestWare('Cargando Historial Producto Empleado...');
        $('#tableHistoricalInventory td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_view_historical_inventory_product', [idProductGlobalEmployee,idEmployee]),
            data: {
                _token: token,
                folioInventory: folioInventory,
                userInventory: userInventory,
                typeMovement: typeMovement,
                productInventory: productInventory,
                nameSource: nameSource,
                nameDestiny: nameDestiny,
                initialDateHistoricalInventory: initialDateHistoricalInventory,
                finalDateHistoricalInventory: finalDateHistoricalInventory,
                destinationsFilter: destinationsFilter
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableHistoricalInventory');
                    let sumQuantity = 0, sumFractionQuantity = 0,sumUnitPrice = 0, sumValueMovement = 0, sumValueInventory = 0;
                    const {historicalInventoriesDetail, symbol_country, employee, stock, total, initialDate, finalDate, folio, userId,
                        typeMovementId, productId, nameSourceSearch, nameDestinySearch, destinationsFilter} = response;
                    $('#nameHistoricalInventory').html(`${employee.name}`);
                    $('#totalUnitsInventory').html(`${stock}`);
                    $('#valueTotalInventory').html(`${symbol_country}${total.toFixed(2)}`);

                    let productsDetail;
                    let productsDetailO = historicalInventoriesDetail.filter(res => res.source == employee.name && res.is_destiny == 0);
                    let productsDetailD = historicalInventoriesDetail.filter(res => res.source != employee.name && res.is_destiny == 1);
                    productsDetail = productsDetailO.concat(productsDetailD)
                    productsDetail.sort(sortByProperty("created_at")).reverse();
                    let dataFilterInventory = productsDetail;

                   /* // Function Inversa filter
                    if (destinationsFilter == 0) {
                        dataFilterInventory = productsDetail.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[2] !== 'O';
                        } );

                        dataFilterInventory = dataFilterInventory.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[3] !== 'O';
                        } );
                        productsDetail = dataFilterInventory;

                    }*/

                    if (initialDate != null && finalDate != null) {
                        dataFilterInventory = productsDetail.filter(dateProduct => dateProduct.date >= initialDate && dateProduct.date <= finalDate)
                        productsDetail = dataFilterInventory;
                    }

                    if (folio != null) {
                        let expresion = new RegExp(`${folio}.*`, "i");
                        dataFilterInventory = productsDetail.filter(folioHistorical => expresion.test(folioHistorical.folio));
                        productsDetail = dataFilterInventory;
                    }

                    if (userId != 0) {
                        dataFilterInventory = productsDetail.filter(user => user.id_user == userId);
                        productsDetail = dataFilterInventory;
                    }

                    if (typeMovementId != 0) {
                        dataFilterInventory = productsDetail.filter(typeMovementHistorical => typeMovementHistorical.id_type_movement_inventory == typeMovementId);
                        productsDetail = dataFilterInventory;
                    }

                    if (productId != 0) {
                        dataFilterInventory = productsDetail.filter(productHistorical => productHistorical.id_product == productId);
                        productsDetail = dataFilterInventory;
                    }

                    if (nameSourceSearch != null) {
                        let expresion = new RegExp(`${nameSourceSearch}.*`, "i");
                        dataFilterInventory = productsDetail.filter(nameHistorical => expresion.test(nameHistorical.source));
                        productsDetail = dataFilterInventory;
                    }

                    if (nameDestinySearch != null) {
                        let expresion = new RegExp(`${nameDestinySearch}.*`, "i");
                        dataFilterInventory = productsDetail.filter(nameHistorical => expresion.test(nameHistorical.destiny));
                        productsDetail = dataFilterInventory;
                    }

                    productsDetail.forEach((element) => {
                        const {folio, date, hour, name_user, nameType, name_product, unit_product, source, destiny, quantity, before_stock, after_stock,
                            fraction_quantity, fraction_before_stock, fraction_after_stock, unit_price, value_movement, value_inventory} = element;
                        inventoriesDB.push({
                            folio: folio,
                            date: date,
                            hour: hour,
                            name_user: name_user,
                            nameType: nameType,
                            name_product: name_product,
                            unit_product: unit_product,
                            source: source,
                            destiny: destiny,
                            quantity: quantity,
                            before_stock: before_stock,
                            after_stock: after_stock,
                            fraction_quantity: fraction_quantity,
                            fraction_before_stock: fraction_before_stock,
                            fraction_after_stock: fraction_after_stock,
                            unit_price: unit_price,
                            value_movement: value_movement,
                            value_inventory: value_inventory
                        });

                        rows += `
                                <tr class="small">
                                <td class="text-primary text-bold">
                                    <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ${folio}
                                        </a>
                                    </div>
                                </td>
                                <td class="">${date} <br> ${hour}</td>
                                <td class="">${name_user}</td>
                                <td class="">${nameType}</td>
                                <td class="">${name_product}</td>
                                <td class="">${source}</td>
                                <td class="">${destiny}</td>
                                <td class="">${quantity}</td>
                                <td class="">${fraction_quantity} ${unit_product}s</td>
                                <td class="">${before_stock}</td>
                                <td class="">${after_stock}</td>
                                <td class="">${fraction_before_stock} ${unit_product}s</td>
                                <td class="">${fraction_after_stock} ${unit_product}s</td>
                                <td class="">${symbol_country}${unit_price.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_movement.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_inventory.toFixed(2)}</td>
                                </tr>`;

                        // Total Generals
                        sumQuantity = sumQuantity+quantity;
                        sumFractionQuantity = sumFractionQuantity+fraction_quantity;
                        sumUnitPrice = sumUnitPrice+unit_price;
                        sumValueMovement = sumValueMovement+value_movement;
                        sumValueInventory = sumValueInventory+value_inventory;

                    });
                    table.append(rows);

                    //Labels Totals Sum
                    $('#quantityTotal').html(sumQuantity);
                    $('#fractionQuantityTotal').html(sumFractionQuantity);
                    $('#unitPriceTotal').html(symbol_country + sumUnitPrice.toFixed(2));
                    $('#valueMovementTotal').html(symbol_country + sumValueMovement.toFixed(2));
                    $('#valueInventoryTotal').html(symbol_country + sumValueInventory.toFixed(2));

                    // reset filters
                    $('#folio').val('');
                    $('#filterDateHistoricalInventory').val('');
                    $('#nameSource').val('');
                    $('#nameDestiny').val('')
                    Swal.close();
                }
            }
        });
    }

    function getDataHistoricalInventoryCompany(idCompany){
        loaderPestWare('Cargando Historial General...');
        $('#tableHistoricalInventory td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_view_historical_inventory_company', idCompany),
            data: {
                _token: token,
                folioInventory: folioInventory,
                userInventory: userInventory,
                typeMovement: typeMovement,
                productInventory: productInventory,
                nameSource: nameSource,
                nameDestiny: nameDestiny,
                initialDateHistoricalInventory: initialDateHistoricalInventory,
                finalDateHistoricalInventory: finalDateHistoricalInventory,
                destinationsFilter: destinationsFilter
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableHistoricalInventory');
                    const {historicalInventoriesDetail,symbol_country, company, stock, total, deleteData, master, destinationsFilter} = response;
                    $('#nameHistoricalInventory').html(`${company.name}`);
                    $('#totalUnitsInventory').html(`${stock}`);
                    $('#valueTotalInventory').html(`${symbol_country}${total.toFixed(2)}`);

                    let productsDetail = historicalInventoriesDetail;
                    productsDetail.sort(sortByProperty("created_at")).reverse();

                  /*  // Function Inversa filter
                    if (destinationsFilter == 0) {
                        dataFilterInventory = productsDetail.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[2] !== 'O';
                        } );

                        dataFilterInventory = dataFilterInventory.filter(function (element) {
                            let folioSplitSource = element.folio;
                            folioSplitSource = folioSplitSource.split('-')
                            return folioSplitSource[3] !== 'O';
                        } );
                        productsDetail = dataFilterInventory;

                    }*/

                    productsDetail.forEach((element) => {
                        const {folio, date, hour, name_user, nameType, name_product, unit_product, source, destiny, quantity, before_stock, after_stock,
                            fraction_quantity, fraction_before_stock, fraction_after_stock, unit_price, value_movement, value_inventory} = element;
                        inventoriesDB.push({
                            folio: folio,
                            date: date,
                            hour: hour,
                            name_user: name_user,
                            nameType: nameType,
                            name_product: name_product,
                            unit_product: unit_product,
                            source: source,
                            destiny: destiny,
                            quantity: quantity,
                            before_stock: before_stock,
                            after_stock: after_stock,
                            fraction_quantity: fraction_quantity,
                            fraction_before_stock: fraction_before_stock,
                            fraction_after_stock: fraction_after_stock,
                            unit_price: unit_price,
                            value_movement: value_movement,
                            value_inventory: value_inventory
                        });

                        rows += `
                                <tr class="small">
                                <td class="text-primary text-bold">
                                    <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ${folio}
                                        </a>
                                    </div>
                                </td>
                                <td class="">${date} <br> ${hour}</td>
                                <td class="">${name_user}</td>
                                <td class="">${nameType}</td>
                                <td class="">${name_product}</td>
                                <td class="">${source}</td>
                                <td class="">${destiny}</td>
                                <td class="">${quantity}</td>
                                <td class="">${fraction_quantity} ${unit_product}s</td>
                                <td class="">${before_stock}</td>
                                <td class="">${after_stock}</td>
                                <td class="">${fraction_before_stock} ${unit_product}s</td>
                                <td class="">${fraction_after_stock} ${unit_product}s</td>
                                <td class="">${symbol_country}${unit_price.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_movement.toFixed(2)}</td>
                                <td class="">${symbol_country}${value_inventory.toFixed(2)}</td>
                                </tr>`;
                    });
                    table.append(rows);

                    // reset filters
                    $('#folio').val('');
                    $('#filterDateHistoricalInventory').val('');
                    $('#nameSource').val('');
                    $('#nameDestiny').val('')
                    Swal.close();
                }
            }
        });
    }

    function getDataMovementStoreHouse(idJobCenter){
        loaderPestWare('Cargando Movimientos Sucursal...');
        $('#tableMovementStoreHouse td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('index_movements', idJobCenter),
            data: {
                _token: token,
                folioMovementStoreHouse: folioMovementStoreHouse,
                nameSourceMovementStoreHouse: nameSourceMovementStoreHouse,
                nameDestinyMovementStoreHouse: nameDestinyMovementStoreHouse,
                initialDateMovementStoreHouse: initialDateMovementStoreHouse,
                finalDateMovementStoreHouse: finalDateMovementStoreHouse,
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableMovementStoreHouse');
                    let movementsDB = [];
                    const {movements, nameStorehouse, symbol_country, folio, source, destiny, initialDate, finalDate} = response;
                    $('#labelMovementStoreHose').html(`${nameStorehouse}`);

                    let movementFilter = movements;
                    let dataFilterMovement = [];

                    if (folio != null) {
                        let expresion = new RegExp(`${folio}.*`, "i");
                        dataFilterMovement = movements.filter(folioMovement => expresion.test(folioMovement.idMovement));
                        movementFilter = dataFilterMovement;
                    }

                    if (initialDate != null && finalDate != null) {
                        dataFilterMovement = movements.filter(dateMovement => dateMovement.date >= initialDate && dateMovement.date <= finalDate)
                        movementFilter = dataFilterMovement;
                    }

                    if (source != null) {
                        let expresion = new RegExp(`${source}.*`, "i");
                        dataFilterMovement = movements.filter(nameMovement => expresion.test(nameMovement.origin));
                        movementFilter = dataFilterMovement;
                    }

                    if (destiny != null) {
                        let expresion = new RegExp(`${destiny}.*`, "i");
                        dataFilterMovement = movements.filter(nameMovement => expresion.test(nameMovement.destiny));
                        movementFilter = dataFilterMovement;
                    }

                    movementFilter.forEach((element) => {
                        const {id, type, date, username, origin, destiny,tot_unit, total, id_entry, id_transfer_cc, id_transfer_ce,
                            id_transfer_ec, id_adjustment, id_movement} = element;
                        movementFilter.push({
                            id: id,
                            type: type,
                            date: date,
                            username: username,
                            origin: origin,
                            destiny: destiny,
                            tot_unit: tot_unit,
                            total: total,
                            id_entry: id_entry,
                            id_transfer_cc: id_transfer_cc,
                            id_transfer_ce: id_transfer_ce,
                            id_transfer_ec: id_transfer_ec,
                            id_adjustment: id_adjustment,
                            id_movement: id_movement
                        });

                        rows += `<tr>
                                <td class="text-primary text-bold">
                                    <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           ${type === 'entry' ?  id_entry : ''}
                                           ${type === 'tcc' ?  id_transfer_cc : ''}
                                           ${type === 'tce' ?  id_transfer_ce : ''}
                                           ${type === 'tec' ?  id_transfer_ec : ''}
                                           ${type !== 'entry' && type !== 'tcc' &&  type !== 'tce' && type !== 'tec' ?  id_adjustment : ''}
                                           <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                        ${type !== 'entry' && type !== 'tcc' &&  type !== 'tce' && type !== 'tec' ?
                                            `<li style="cursor: pointer"><a data-toggle="modal" data-target="#detailMovementAdjustment"
                                               data-id="${id}"
                                               data-type="${type}"
                                               data-idJobCenter="${idJobCenter}"
                                               data-idEmployee="0"
                                               data-adjustment="1">
                                            <i class="fa fa-arrows-v" aria-hidden="true" style="color: grey"></i>Ver</a>
                                            </li>` : 
                                            `<li style="cursor: pointer"><a data-toggle="modal" data-target="#detailMovementSoreHouse" 
                                                data-id="${id}"
                                                data-type="${type}"
                                                data-idJobCenter="${idJobCenter}"
                                                data-idEmployee="0">
                                            <i class="fa fa-arrows-v" aria-hidden="true" style="color: grey"></i>Ver</a>
                                            </li>`}
                                          
                                        </ul> 
                                        
                                    </div>
                                    
                                </td>
                                <td class="">${date} <br> ${username}</td>
                                <td class="">${origin}</td>
                                <td class="">${destiny}</td>
                                <td class="">${tot_unit}</td>
                                <td class="">${symbol_country}${total.toFixed(2)}</td>
                        </tr>`;
                    });

                    table.append(rows);

                    // reset filters
                    $('#folioMovementStoreHouse').val('');
                    $('#filterDateMovementStoreHouse').val('');
                    $('#nameSourceMovementStoreHouse').val('');
                    $('#nameDestinyMovementStoreHouse').val('')
                    Swal.close();
                }
            }
        });
    }

    function getDataMovementStoreHouseEmployee(idEmployee){
        loaderPestWare('Cargando Movimientos Empleado...');
        $('#tableMovementStoreHouse td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('index_mov_emp', idEmployee),
            data: {
                _token: token,
                folioMovementStoreHouse: folioMovementStoreHouse,
                nameSourceMovementStoreHouse: nameSourceMovementStoreHouse,
                nameDestinyMovementStoreHouse: nameDestinyMovementStoreHouse,
                initialDateMovementStoreHouse: initialDateMovementStoreHouse,
                finalDateMovementStoreHouse: finalDateMovementStoreHouse,
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableMovementStoreHouse');
                    let movementsDB = [];
                    const {movements, nameStorehouse, symbol_country, folio, source, destiny, initialDate, finalDate} = response;
                    $('#labelMovementStoreHose').html(`${nameStorehouse.name}`);

                    let movementFilter = movements;
                    let dataFilterMovement = [];

                    if (folio != null) {
                        let expresion = new RegExp(`${folio}.*`, "i");
                        dataFilterMovement = movements.filter(folioMovement => expresion.test(folioMovement.idMovement));
                        movementFilter = dataFilterMovement;
                    }

                    if (initialDate != null && finalDate != null) {
                        dataFilterMovement = movements.filter(dateMovement => dateMovement.date >= initialDate && dateMovement.date <= finalDate)
                        movementFilter = dataFilterMovement;
                    }

                    if (source != null) {
                        let expresion = new RegExp(`${source}.*`, "i");
                        dataFilterMovement = movements.filter(nameMovement => expresion.test(nameMovement.origin));
                        movementFilter = dataFilterMovement;
                    }

                    if (destiny != null) {
                        let expresion = new RegExp(`${destiny}.*`, "i");
                        dataFilterMovement = movements.filter(nameMovement => expresion.test(nameMovement.destiny));
                        movementFilter = dataFilterMovement;
                    }

                    movementFilter.forEach((element) => {
                        const {id, type, date, username, origin, destiny,tot_unit, total, id_transfer_ee, id_transfer_ce,
                            id_transfer_ec, id_adjustment} = element;
                        movementsDB.push({
                            id: id,
                            type: type,
                            date: date,
                            username: username,
                            origin: origin,
                            destiny: destiny,
                            tot_unit: tot_unit,
                            total: total,
                            id_transfer_ee: id_transfer_ee,
                            id_transfer_ce: id_transfer_ce,
                            id_transfer_ec: id_transfer_ec,
                            id_adjustment: id_adjustment
                        });

                        rows += `<tr>
                                <td class="text-primary text-bold">
                                    <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           ${type === 'tee' ?  id_transfer_ee : ''}
                                           ${type === 'tce' ?  id_transfer_ce : ''}
                                           ${type === 'tec' ?  id_transfer_ec : ''}
                                           ${type !== 'tee' && type !== 'tce' && type !== 'tec' ? id_adjustment : ''}
                                           <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                        ${type !== 'tee' && type !== 'tce' && type !== 'tec' ?
                                            `<li style="cursor: pointer">
                                                <a data-toggle="modal" data-target="#detailMovementAdjustment"
                                                           data-id="${id}"
                                                           data-type="${type}"
                                                           data-idJobCenter="0"
                                                           data-idEmployee="${idEmployee}"
                                                           data-adjustment="1">
                                                <i class="fa fa-arrows-v" aria-hidden="true" style="color: grey"></i>Ver
                                                </a>
                                            </li>` :
                                            `<li style="cursor: pointer"><a data-toggle="modal" data-target="#detailMovementSoreHouse"
                                                                           data-id="${id}"
                                                                           data-type="${type}"
                                                                           data-idJobCenter="0"
                                                                           data-idEmployee="${idEmployee}">
                                                <i class="fa fa-arrows-v" aria-hidden="true" style="color: grey"></i>Ver</a>
                                            </li>`}
                                          
                                        </ul> 
                                        
                                    </div>
                                    
                                </td>
                                <td class="">${date} <br> ${username}</td>
                                <td class="">${origin}</td>
                                <td class="">${destiny}</td>
                                <td class="">${tot_unit}</td>
                                <td class="">${symbol_country}${total.toFixed(2)}</td>
                        </tr>`;
                    });

                    table.append(rows);

                    // reset filters
                    $('#folioMovementStoreHouse').val('');
                    $('#filterDateMovementStoreHouse').val('');
                    $('#nameSourceMovementStoreHouse').val('');
                    $('#nameDestinyMovementStoreHouse').val('')
                    Swal.close();
                }
            }
        });
    }

    function getDataDetailMovementStoreHouse(idJobCenter, idMovement, typeMovementStoreHouse, idEmployee, isAdjustment){
        loaderPestWare('Cargando Detalle Movimiento...');
        $('#tableDetailMovementStoreHouse td').remove();
        $('#tableDetailMovementAdjustment td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_detail_movements', idMovement),
            data: {
                _token: token,
                idJobCenter: idJobCenter,
                idEmployee: idEmployee,
                typeMovementStoreHouse: typeMovementStoreHouse,
                isAdjustment: isAdjustment
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    const {detailMovement, collectionMovements, symbol_country, totalByMovement, isAdjustment, adjustment, nameSource} = response;

                    if (isAdjustment == 0) {
                        // Modal Movement
                        $('#labelMovement').html(`${detailMovement.folio}`);
                        $('#labelDate').html(`${detailMovement.date}`);
                        $('#labelUserName').html(`${detailMovement.user_name}`);
                        $('#labelSourceMovement').html(`${detailMovement.origin}`);
                        $('#labelDestinyMovement').html(`${detailMovement.destiny}`);
                        $('#labelTotalMovement').html(`${symbol_country}${totalByMovement.toFixed(2)}`);

                        // Table Details
                        let rows = '';
                        let table = $('#tableDetailMovementStoreHouse');

                        collectionMovements.forEach((element) => {
                            const {id, name, quantity, unit, units, base_price, is_units} = element;
                            collectionMovements.push({
                                id: id,
                                name: name,
                                quantity: quantity,
                                unit: unit,
                                units: units,
                                is_units: is_units,
                                base_price: base_price,
                            });

                            rows += `<tr>
                                <td class="text-primary">${name}</td>
                                <td class="">${quantity} ${unit}</td>
                                <td class="">${units} ${is_units === 0 ? unit: ''} </td>
                                <td class="">${symbol_country}${base_price}</td>                               
                                <td class="">
                                    ${symbol_country}${is_units === 0 ? (base_price.toFixed(2) / quantity) * units : base_price.toFixed(2) * units}
                                </td>
                        </tr>`;
                        });

                        table.append(rows);
                    }

                    if (isAdjustment == 1) {
                        // Modal Adjustment
                        $('#labelAdjustment').html(`${adjustment.id_adjustment}`);
                        $('#labelDateAdjustment').html(`${adjustment.date}`);
                        $('#labelUserNameAdjustment').html(`${adjustment.username}`);
                        $('#labelSourceAdjustment').html(`${nameSource.name}`);
                        $('#labelDestinyAdjustment').html(`${nameSource.name}`);
                        $('#labelTotalAdjustment').html(`${symbol_country}${adjustment.total.toFixed(2)}`);

                        // Table Details
                        let rows = '';
                        let table = $('#tableDetailMovementAdjustment');

                        rows += `<tr>
                                <td class="text-primary">${adjustment.product}</td>
                                <td class="">${adjustment.quantity} ${adjustment.unit}s</td>
                                <td class="">${adjustment.units_adjustment}</td>
                                <td class="">${symbol_country}${adjustment.base_price}</td>                               
                                <td class="">${symbol_country}${adjustment.total}</td>
                                <td class="">${adjustment.type}</td>
                             </tr>`;

                        table.append(rows);

                    }

                    Swal.close();

                }
            }
        });
    }

    function getDataStoreHouse(idJobCenter){
        loaderPestWare('Cargando Productos...');
        $('#tableStoreHouseDetail td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_view_detail_store_house', idJobCenter),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    Swal.close();
                    let rows = '';
                    let table = $('#tableStoreHouseDetail');
                    const {storehousesDetail,symbol_country, profileJobCenter, stock, total, deleteData, master} = response;
                    $('#storeLabel').html(`${profileJobCenter.name}`);
                    $('#totalUnitsLabel').html(`${stock}`);
                    $('#totalGeneralLabel').html(`${total.toFixed(2)}`);

                    storehousesDetail.forEach((element) => {
                        const {id, product, stock, stock_other_units, total, id_product, base_price} = element;
                        inventoriesDB.push({
                            id: id,
                            product: product,
                            stock: stock,
                            stock_other_units: stock_other_units,
                            total: total,
                            id_product: id_product,
                            base_price: base_price
                        });

                        rows += `<tr>
                                <td class="text-primary text-bold">
                                    <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ${product}<span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                        ${deleteData || master ?
                                            `<li style="cursor: pointer"><a data-toggle="modal" data-target="#adjustmentEntry" 
                                            data-idjobcenter="${id}" 
                                            data-entryexit="APE-"
                                            data-title="Entrada">
                                            <i class="fa fa-chevron-right" aria-hidden="true" style="color: #138D75"></i>Entrada por Ajuste</a>
                                            </li>
	                                        <li style="cursor: pointer"><a data-toggle="modal" data-target="#adjustmentEntry" 
	                                        data-idjobcenter="${id}" 
	                                        data-entryexit="APS-"
	                                        data-title="Salida">
	                                        <i class="fa fa-chevron-left" aria-hidden="true" style="color: #922B21"></i>Salida por Ajuste</a>
	                                        </li>`: ''}
                                            <li style="cursor: pointer"><a data-toggle="modal" data-target="#storehouseHistorical" 
	                                        data-idJobCenter="${idJobCenter}" 
	                                        data-product="${id_product}">
	                                        <i class="fa fa-arrows-v" aria-hidden="true" style="color: grey"></i>Histórico</a>
	                                        </li>
                                        </ul> 
                                        
                                    </div>
                                    
                                </td>
                                <td class="">${stock}</td>
                                <td class="">${stock_other_units}</td>
                                <td class="">${symbol_country}${base_price}</td>
                                <td class="">${symbol_country}
                                    ${ stock_other_units === 0 ? '0' : (total.toFixed(2) / stock_other_units).toFixed(2) }
                                </td>
                                <td class="">${symbol_country}${total.toFixed(2)}</td>
                        </tr>`;
                    });
                    table.append(rows);
                }
            }
        });
    }

    function getDataStoreHouseEmployee(idEmployee){
        loaderPestWare('Cargando Productos...');
        $('#tableStoreHouseDetail td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_view_detail_store_house_employee', idEmployee),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    Swal.close();
                    let rows = '';
                    let table = $('#tableStoreHouseDetail');
                    const {storehousesDetail,symbol_country, employee, stock, total, deleteData, master} = response;
                    $('#storeLabel').html(`${employee.name}`);
                    $('#totalUnitsLabel').html(`${stock}`);
                    $('#totalGeneralLabel').html(`${total.toFixed(2)}`);
                    storehousesDetail.forEach((element) => {
                        const {id,product, stock, stock_other_units, total, id_product, base_price} = element;
                        inventoriesDB.push({
                            id: id,
                            product: product,
                            stock: stock,
                            stock_other_units: stock_other_units,
                            total: total,
                            id_product: id_product,
                            base_price: base_price
                        });

                        rows += `<tr>
                                <td class="text-primary text-bold">
                                <div class="btn-group" style="cursor: pointer">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ${product} <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                        ${deleteData || master ?
                                        `<li style="cursor: pointer"><a data-toggle="modal" data-target="#adjustmentEntry" 
                                            data-id="${id}" 
                                            data-entryexit="APE-"
                                            data-title="Entrada por Ajuste:"
                                            >
                                            <i class="fa fa-chevron-right" aria-hidden="true" style="color: #138D75"></i>Entrada por Ajuste</a>
                                            </li>
	                                        <li style="cursor: pointer"><a data-toggle="modal" data-target="#adjustmentEntry" 
	                                        data-id="${id}" 
	                                        data-entryexit="APS-"
	                                        data-title="Salida por Ajuste:"
	                                        >
	                                        <i class="fa fa-chevron-left" aria-hidden="true" style="color: #922B21"></i>Salida por Ajuste</a>
	                                        </li>` : ''
                                        }
                                         <li style="cursor: pointer"><a data-toggle="modal" data-target="#storehouseHistorical" 
	                                        data-idEmployee="${idEmployee}" 
	                                        data-idJobCenter="0" 
	                                        data-product="${id_product}">
	                                        <i class="fa fa-arrows-v" aria-hidden="true" style="color: grey"></i>Histórico</a>
	                                        </li>      
                                        </ul>                              
                                    </div>
                                </td>
                                <td class="">${stock}</td>
                                <td class="">${stock_other_units}</td>
                                <td class="">${symbol_country}${base_price}</td>
                                <td class="">${symbol_country}
                                    ${ stock_other_units === 0 ? '0' : (total.toFixed(2) / stock_other_units).toFixed(2) }
                                </td>
                                <td class="">${symbol_country}${total.toFixed(2)}</td>
                        </tr>`;
                    });
                    table.append(rows);
                }
            }
        });
    }

    function getDataIdStoreHouse(idStoreHouse){
        loaderPestWare('Cargando Productos...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_store_house', idStoreHouse),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    Swal.close();
                    const {storeHouse,symbol_country} = response;
                    $('#nameStoreHouseProduct').html(`${storeHouse.name}`);
                    $('#nameStoreHouse').html(`${storeHouse.name_job_center}`);
                    $('#nameProductTable').html(`${storeHouse.name}`);
                    $('#units_adjustment').val(`${storeHouse.stock}`);
                    $('#quantity_adjustment').val(`${storeHouse.stock_other_units}`);
                    $('#quantityUnits').html(`${storeHouse.name_unit}`);
                    $('#symbolCode').html(`${symbol_country}`);
                    $('#total_adjustment').val(`${storeHouse.total}`);

                    // inputs Hide
                    $('#id_storehouse').val(`${storeHouse.id}`);
                    $('#units_before').val(`${storeHouse.stock}`);
                    $('#quantity_before').val(`${storeHouse.stock_other_units}`);
                    $('#total_before').val(`${storeHouse.total}`);
                }
            }
        });
    }

    function getDataIdStoreHouseEmployee(idStoreHouseEmployee){
        loaderPestWare('Cargando servicios...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_data_store_house_employee', idStoreHouseEmployee),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    Swal.close();
                    const {storeHouse,symbol_country} = response;
                    $('#nameStoreHouseProduct').html(`${storeHouse.name}`);
                    $('#nameStoreHouse').html(`${storeHouse.name_employee}`);
                    $('#nameProductTable').html(`${storeHouse.name}`);
                    $('#units_adjustment').val(`${storeHouse.stock}`);
                    $('#quantity_adjustment').val(`${storeHouse.stock_other_units}`);
                    $('#quantityUnits').html(`${storeHouse.name_unit}`);
                    $('#symbolCode').html(`${symbol_country}`);
                    $('#total_adjustment').val(`${storeHouse.total}`);

                    // inputs Hide
                    $('#id_storehouse').val(`${storeHouse.id}`);
                    $('#units_before').val(`${storeHouse.stock}`);
                    $('#quantity_before').val(`${storeHouse.stock_other_units}`);
                    $('#total_before').val(`${storeHouse.total}`);

                }
            }
        });
    }

    // Delete Entry
    let idEntry;
    $('#entryDelete').on('show.bs.modal', function(e) {
        idEntry = $(e.relatedTarget).data().id;
        let folioEntry = $(e.relatedTarget).data().entry;
        $('#labelEntry').html(folioEntry);
    });

    $('#btnDeleteEntry').click(function () {
        loaderPestWare('Eliminando...');
        $.ajax({
            type: "POST",
            url: route('delete_entry'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idEntry: idEntry
            },
            success: function(data) {
                Swal.close();
                if (data.code == 500) {
                    showToast('error','Error al eliminar', data.message)
                } else {
                    showToast('success-redirect','Eliminación de Entrada', data.message, 'index_entry')
                    $('#btnDeleteEntry').attr("disabled", true);
                }
            }
        });
    });

    // Function orderBy Collection
    function sortByProperty(property){
        return function(a,b){
            if(a[property] > b[property])
                return 1;
            else if(a[property] < b[property])
                return -1;

            return 0;
        }
    }

});