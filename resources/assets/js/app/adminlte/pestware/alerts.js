export function showToast(type, title, message, path) {
    if (type === 'success-redirect') {
        toastr.success(message, title);
        setTimeout(() => {
            window.location.href = route(path);
        }, 5000);
    }
    else if (type === 'success') toastr.success(message, title)
    else if (type === 'info') toastr.info(message, title)
    else if (type === 'warning') toastr.warning(message, title)
    else if (type === 'error') toastr.error(message, title)
}
