import {loaderPestWare} from "./loader";
import {showToast} from "./alerts";

$(document).ready(function() {

    getNotifications();

    $('#sidebarToggle').click(function () {
        if ($('#body').hasClass('sidebar-collapse')) {
            changeSidebarCollapse(true);
        } else changeSidebarCollapse(false);
    });

    function changeSidebarCollapse(sidebarCollapse) {
        $.ajax({
            type: 'GET',
            url: route('change_sidebar_collapse'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                sidebarCollapse: sidebarCollapse
            }
        });
    }

    // IntroJs

    $('#questionIntroJs').click(function () {
        Swal.fire({
            title: 'Tutorial',
            text: "¿Deseas regresar al tutorial?",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            loaderPestWare('Cargando Tutorial...');
            if (result.value) {
                updateAjaxIntroJs(1);
                    setTimeout(() => {
                        location.reload();
                    }, 2500);
            } else {
                updateAjaxIntroJs(0);
            }
        })
    });

    //SideBar Intro Tuto

    $('#introQuoatationIndex').click(function () {
        loaderPestWare('Cargando Tutorial...');
        setTimeout(() => {
            updateAjaxIntroJs(1);
            window.location.href = route('index_register')
        }, 2500);
    });

    $('#introScheduleIndex').click(function () {
        loaderPestWare('Cargando Tutorial...');
        setTimeout(() => {
            updateAjaxIntroJs(1);
            window.location.href = route('calendario')
        }, 2500);
    });

    $('#introManagementIndex').click(function () {
        loaderPestWare('Cargando Tutorial...');
        setTimeout(() => {
            updateAjaxIntroJs(1);
            window.location.href = route('index_product')
        }, 2500);
    });

    $('#introBusinessIndex').click(function () {
        loaderPestWare('Cargando Tutorial...');
        setTimeout(() => {
            updateAjaxIntroJs(1);
            window.location.href = route('monitoring')
        }, 2500);
    });

    $('#introManelManagementIndex').click(function () {
        loaderPestWare('Cargando Tutorial...');
        setTimeout(() => {
            updateAjaxIntroJs(1);
            window.location.href = route('index_config_instance')
        }, 2500);
    });

    function updateAjaxIntroJs(introjs) {
        $.ajax({
            type: 'POST',
            url: route('change_introjs_user'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                introjs: introjs
            },
            success: function (data) {
                //console.log('OK');
            }
        });
    }

    function getNotifications() {
        $.ajax({
            type: 'GET',
            url: route('get_notifications_nav'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content")
            },
            success: function (data) {
                if (data.code === 200) {
                    const {notifications, count} = data;
                    document.getElementById('countNotifications').innerText = count;
                    document.getElementById('headerNotifications').innerText = `Tienes ${count} notificaciones.`;
                    let lis = '';
                    let ulNotifications = $('#ulNotifications');
                    notifications.forEach((element) => {
                        const {id, created_at, title, message, is_read} = element;
                        lis += `
                        <li id="ulNotifications" ${is_read === 0 ? 'style="background-color: rgba(136,136,136,0.18)"' : ''}>
                            <a class="markReadNotification" data-id="${id}" style="cursor: pointer;">
                                <h4>
                                    <small class="text-bold"><i class="fa fa-clock-o"></i> ${created_at}</small>
                                </h4>
                                <span class="text-bold">${title}</span>
                                <p style="margin-left: 0 !important;">${message}</p>
                            </a>
                        </li>`;
                    });
                    ulNotifications.html('');
                    ulNotifications.append(lis);
                    $('.markReadNotification').click(function () {
                        let all = $(this);
                        let notificationId = all.attr('data-id');
                        moreNotification(notificationId);
                    });
                    $('.markAllReadNotifications').click(function () {
                        markAllReadNotifications();
                    });
                }
            }
        });
    }

    function markAllReadNotifications() {
        $.ajax({
            type: 'POST',
            url: route('mark_all_read_notifications'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content")
            },
            success: function (data) {
                if (data.code === 200) {
                    showToast('success', 'Notificaciones', data.message);
                    getNotifications();
                }
            }
        });
    }

    function markReadNotification(notificationId) {
        $.ajax({
            type: 'POST',
            url: route('mark_read_notification', notificationId),
            data: {
                _token: $("meta[name=csrf-token]").attr("content")
            },
            success: function (data) {
                if (data.code === 200) {
                    getNotifications();
                }
            }
        });
    }

    function moreNotification(notificationId) {
        loaderPestWare('Cargando notificación...');
        $.ajax({
            type: 'GET',
            url: route('get_more_notification', notificationId),
            data: {
                _token: $("meta[name=csrf-token]").attr("content")
            },
            success: function (response) {
                const notification = response.data;
                const {title, message} = notification;
                if (response.code === 200) {
                    Swal.fire({
                        title: `<strong>${title}</strong>`,
                        type: 'info',
                        html: message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Marcar como leído',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar',
                        width: '70rem'
                    }).then((result) => {
                        if (result.value) markReadNotification(notificationId);
                        else Swal.close()
                    })
                } else showToast('warning', 'Notificación', response.message)
            }
        });
    }

});