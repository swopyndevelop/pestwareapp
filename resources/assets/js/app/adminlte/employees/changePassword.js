/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 04/04/2021
 * customers/_modalChangePassword.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
   let idEmployee;

   $('.openModalChangePassword').click(function () {
      idEmployee = $(this).data("id");

      $("#sendChangePasswordSave").click(function() {
         sendChangePassword();
      });
   });

   function sendChangePassword(){
      loaderPestWare('');
      $.ajax({
         type: 'POST',
         url: route('employees_change_password'),
         data: {
            _token: $("meta[name=csrf-token]").attr("content"),
            idEmployee: idEmployee
         },
         success: function (data) {
            if (data.errors){
               Swal.close();
               showToast('error', 'Error', data.message);
            }
            else{
               Swal.close();
               showToast('success', 'Resetear contraseña', data.message);
               $('#changePassword').click();
            }
         }
      });
   }

});
