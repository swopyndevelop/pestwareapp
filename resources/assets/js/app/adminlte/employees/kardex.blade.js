//employees/kardex.blade.php
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";
import {expressions, getFields, setFields, validateField} from "../validations/regex";
import {alert} from "ionicons/icons";

$(document).ready(function () { // 6,32 5,38 2,34

    if ($('#kardex_Employee').length) {
        //start modal new employees save
        const nameEmployee = document.getElementById('nameEmployee');
        const email = document.getElementById('email');

        //Update
        const nameEmployeeUpdate = document.getElementById('nameEmployeeUpdate');
        const emailUpdate = document.getElementById('emailUpdate');

        const fieldsKardexForm = {
            nameEmployee: false,
            email: false,
            nameEmployeeUpdate: false,
            emailUpdate: false
        }

        setFields(fieldsKardexForm);

        const validateForm = (e) => {
            switch (e.target.name) {
                case "nameEmployee":
                    validateField(expressions.name, e.target.value, 'nameEmployee');
                    break;
                case "email":
                    validateField(expressions.email, e.target.value, 'email');
                    break;
                case "nameEmployeeUpdate":
                    validateField(expressions.name, e.target.value, 'nameEmployeeUpdate');
                    break;
                case "emailUpdate":
                    validateField(expressions.email, e.target.value, 'emailUpdate');
                    break;
            }
        }

        nameEmployee.addEventListener('keyup', validateForm);
        nameEmployee.addEventListener('blur', validateForm);

        email.addEventListener('keyup', validateForm);
        email.addEventListener('blur', validateForm);

        const jobtitle = document.querySelector('#grupo__inputjobTitle');
        jobtitle.addEventListener('change', (event) => {
            if (event.target.value != ''){
                document.getElementById('grupo__inputjobTitle').classList.remove('formulario__grupo-incorrecto');
            }
        });

        const jobcenter = document.querySelector('#grupo__inputJobCenter');
        jobcenter.addEventListener('change', (event) => {
            if (event.target.value != ''){
                document.getElementById('grupo__inputJobCenter').classList.remove('formulario__grupo-incorrecto');
            }
        });

        $('#EmployeeNew').click(function () {

            let employeeIdNew = $('#employeeIdNew').val();
            let company = $('#newCompany').val();
            let nameEmployee = $('#nameEmployee').val();
            let email = $('#email').val();
            let jobtitle = $('#inputjobTitle option:selected').val();
            let jobcenter = $('#inputJobCenter option:selected').val();
            let color = $('#color').val();

            validateField(expressions.name, nameEmployee, 'nameEmployee');
            validateField(expressions.email, email, 'email');

            if(jobtitle == ''){
                document.getElementById('grupo__inputjobTitle').classList.add('formulario__grupo-incorrecto');
            }
            if(jobcenter == ''){
                document.getElementById('grupo__inputJobCenter').classList.add('formulario__grupo-incorrecto');
            }

            if(getFields().nameEmployee && getFields().email && jobtitle != '' && jobcenter != ''){
                saveEmployeeKardex();
            }

            else {
                document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
                setTimeout(() => {
                    document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
                }, 5000);
            }

            function saveEmployeeKardex()
            {
                loaderPestWare('Creando empleado...');

                $.ajax({
                    type: 'POST',
                    url: route('update_employees'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        employeeId: employeeIdNew,
                        company: company,
                        nameEmployee: nameEmployee,
                        email: email,
                        inputjobTitle: jobtitle,
                        inputJobCenter: jobcenter,
                        color: color
                    },
                    success: function (data) {
                        Swal.close();
                        if (data.errors) {
                            showToast('warning', '¡Espera!', 'Faltan datos por ingresar.');
                        } else {
                            showToast('success', 'Datos guardados correctamente.', 'Los datos de acceso se han enviado por correo.');
                            location.reload();
                        }
                    }
                });
            }
        })

        $('#newEmployee').click(function () {
            $('#modalTitle').text('Nuevo empleado');
            $('#EmployeeUp').text('Guardar');
            $('#nameEmployee').trigger('change');
            $('#inputjobTitle').val('');
            $('#inputJobCenter').val('');
        });

        //end modal new employees

        //modal employees update
        $('.btnUpdateEmployee').click(function () {
            let all = $(this);
            $('#Applicant_id').val(all.attr('data-id'));
            $.ajax({
                type: 'POST',
                url: route('searchEmployee'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    Employee_id: all.attr('data-id'),
                },
                success: function (data) {
                    if (data.errors) {
                    } else {
                        $('#employeeId').val(all.attr('data-id'));
                        $('#inputCompany').val(data.inputCompany);
                        $('#updatejobTitle').val(data.inputjobTitle).trigger('change.select2');
                        $('#updateJobCenter').val(data.inputJobCenter).trigger('change.select2');
                        $('#nameEmployeeUpdate').val(data.name);
                        $('#updateColor').val(data.color);
                        $('#emailUpdate').val(data.email);
                    }
                }
            });
        });

        // Inputs Update
        nameEmployeeUpdate.addEventListener('keyup', validateForm);
        nameEmployeeUpdate.addEventListener('blur', validateForm);

        emailUpdate.addEventListener('keyup', validateForm);
        emailUpdate.addEventListener('blur', validateForm);


        // Evento Actualizar empleado
        $('#EmployeeUp').click(function () {

            let employeeId = $('#employeeId').val();
            let company = $('#newCompany').val();
            let nameEmployeeUpdate = $('#nameEmployeeUpdate').val();
            let emailUpdate = $('#emailUpdate').val();
            let jobtitleUpdate = $('#updatejobTitle').val();
            let jobcenterUpdate = $('#updateJobCenter').val();
            let color = $('#updateColor').val();

            validateField(expressions.name, nameEmployeeUpdate, 'nameEmployeeUpdate');
            validateField(expressions.email, emailUpdate, 'emailUpdate');

            if(jobtitleUpdate == ''){
                document.getElementById('grupo__updatejobTitle').classList.add('formulario__grupo-incorrecto');
            }
            if(jobcenterUpdate == ''){
                document.getElementById('grupo__updateJobCenter').classList.add('formulario__grupo-incorrecto');
            }

            if(getFields().nameEmployeeUpdate && getFields().emailUpdate && jobtitleUpdate != '' && jobcenterUpdate != ''){
                updateEmployeeKardex();
            }

            else {
                document.getElementById('formulario__mensaje__update').classList.add('formulario__mensaje-activo');
                setTimeout(() => {
                    document.getElementById('formulario__mensaje__update').classList.remove('formulario__mensaje-activo');
                }, 5000);
            }

            function updateEmployeeKardex()
            {
                loaderPestWare('Actualizando empleado...');
                $.ajax({
                    type: 'POST',
                    url: route('update_employees'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        employeeId: employeeId,
                        company: company,
                        nameEmployee: nameEmployeeUpdate,
                        email: emailUpdate,
                        inputjobTitle: jobtitleUpdate,
                        inputJobCenter: jobcenterUpdate,
                        color: color
                    },
                    success: function (data) {
                        Swal.close();
                        if (data.errors) {
                            showToast('warning', '¡Espera!', 'Faltan datos por ingresar.');
                        } else {
                            showToast('success', 'Empleado Actualizado.', 'Datos guardados correctamente');
                            location.reload();
                        }
                    }
                });
            }
        })
        //end modal employees update
    }
});
