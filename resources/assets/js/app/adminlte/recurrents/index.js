/**
 * @author Alberto Martinez
 * @version 28/06/2020
 * register/index.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    $('#newQ').on('show.bs.modal', function(e) {

        let button = $(e.relatedTarget);
        if (button.data('client')) {
            loaderPestWare('Cargando datos...');

            let id = button.data('id');
            let name = button.data('client');
            let cellphone = button.data('cellphone');
            let establishment_name = button.data('establishment_name');
            let email = button.data('email');
            let colony = button.data('colony');
            let municipality = button.data('municipality');
            let area = button.data('area');
            let messenger = button.data('messenger');
            let idEstablishment = button.data('establishment_id');
            let idSourceOrigin = button.data('source_origin_id');
            let idDiscount = button.data('discount_id');
            let idExtra = button.data('extra_id');
            let price = button.data('price');
            let total = button.data('total');

            // Get data selects
            let token = $("meta[name=csrf-token]").attr("content");
            $.ajax({
                type: 'GET',
                url: route('filters_quotation', id),
                data: {
                    _token: token
                },
                success: function(data) {
                    if (data.response === 500) {
                        Swal.close();
                        showToast('error', 'Error al cargar los datos', 'Algo salio mal, intente de nuevo');
                    } else {
                        // Load establishment select
                        $("#establishment").empty();
                        data.establishment.forEach((element) => {
                            if (idEstablishment != element.id)
                                $("#establishment").append(`<option value="${element.id}">${element.name}</option>`);
                            else $("#establishment").append(`<option value="${element.id}" selected="true">${element.name}</option>`);
                        });
                        // Load plague select
                        $("#plagues").empty();
                        data.plagues.forEach((element) => {
                            let option = `<option value="${element.id},${element.plague_key}"`;
                            data.quotationPlagues.forEach((plague) => {
                                if (plague.id == element.id) {
                                    option += `selected = 'selected'`
                                }
                            });
                            option = option + `>${element.name}</option>`;
                            //>${element.name}</option>`);
                            $("#plagues").append(option);
                        });
                        // Load sourceOrigin select
                        $("#sourceOrigin").empty();
                        data.sourcesOrigin.forEach((element) => {
                            if (idSourceOrigin != element.id)
                                $("#sourceOrigin").append(`<option value="${element.id}">${element.name}</option>`);
                            else $("#sourceOrigin").append(`<option value="${element.id}" selected="true">${element.name}</option>`);
                        });
                        // Load discount select
                        $("#discount").empty();
                        data.discounts.forEach((element) => {
                            if (idDiscount != element.id)
                                $("#discount").append(`<option value="${element.id}">${element.title} / ${element.percentage}%</option>`);
                            else $("#discount").append(`<option value="${element.id}" selected="true">${element.title} / ${element.percentage}%</option>`);
                        });
                        // Load extra select
                        $("#extra").empty();
                        data.extras.forEach((element) => {
                            if (idExtra != element.id)
                                $("#extra").append(`<option value="${element.id}">${element.name} / $${element.amount}</option>`);
                            else $("#extra").append(`<option value="${element.id}" selected="true">${element.name} / $${element.amount}</option>`);
                        });
                        Swal.close();
                    }
                }
            });

            $('#customerName').val(name);
            $('#cellphone').val(cellphone);
            $('#establishmentName').val(establishment_name);
            $('#emailCustomer').val(email);
            $('#colony').val(colony);
            $('#municipality').val(municipality);
            $('#construction_measure').val(area);
            $('#messenger').val(messenger);
            document.getElementById("priceQ").innerHTML = price;
            document.getElementById("priceQD").innerHTML = total;
        }

    })

    $('#tabRecurrent').click(function() {

        $('#tabQuotations').removeClass("btn-primary-dark").addClass("btn-default");
        $('#tabReinforcements').removeClass("btn-primary-dark").addClass("btn-default");
        $('#tabRecurrent').removeClass("btn-default").addClass("btn-primary-dark");
        loaderPestWare('Cargando recurrentes...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('index_recurrent'),
            data: {
                _token: token
            },
            success: function(data) {
                if (data.response === 500) {
                    Swal.close();
                    showToast('error', 'Error al cargar recurrentes', 'Algo salio mal, intente de nuevo');
                } else {
                    createTable();
                    let rows = '';
                    data.data.forEach((element) => {
                        rows += `<tr>
                            <td scope="row">
                                <div class="btn-group">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ${element.client_id} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="${route('order_view', element.order)}"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver OS</a></li>
                                        <li><a data-toggle="modal" href="#newQ" data-client="${element.name}"
                                            data-cellphone="${element.cellphone}" data-establishment_name="${element.establishment_name}"
                                            data-email="${element.email}" data-colony="${element.colony}" data-municipality="${element.municipality}"
                                            data-area="${element.area}" data-messenger="${element.messenger}" data-establishment_id="${element.establishment_id}"
                                            data-source_origin_id="${element.source_origin_id}" data-discount_id="${element.discount_id}"
                                            data-extra_id="${element.extra_id}" data-price="${element.price}" data-total="${element.total}" data-id="${element.quotation_id}"><i class="fa glyphicon glyphicon-plus" aria-hidden="true" style="color: #2E86C1;"></i>Crear Cotización</a></li>
                                        <li><a href="${route('cancel_view', element.quotation_id)}""><i class="fa fa-times" aria-hidden="true" style="color: red;"></i>Cancelar</a></li>
                                        <li><a href="${route('pdf_service', element.order)}"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar PDF OS</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td scope="row">${element.name}<br>${element.establishment_name}</td>
                            <td scope="row">${element.cellphone}</td>
                            <td scope="row">${element.address} #${element.address_number}, ${element.colony}, ${element.state}, ${element.municipality}</td>
                            <td scope="row">${element.initial_date}</td>
                            <td scope="row"><strong title="${element.motive}" style="color: ${element.color}; font-size: small;">${element.status}</strong><br><strong style="color: #00a157">${element.dateNew}</strong>
                            <br><strong style="color: ${element.colorDays}">${element.diffInDays} días (${element.diffInMonths} meses)</strong></td>
                            <tr/>`;
                        $('#customerName').val(element.name);
                    });
                    $('#dataReinforcements').append(rows);
                    Swal.close();
                }
            }
        });

    }); // end tabReinforcements click

    function createTable() {
        $('#tabContainer').remove();
        let table =
            `<div class="col-md-12 table-responsive" style="min-height: 500px;" id="tabContainer">
            <table class="table tablesorter table-hover text-center" id="tableQuotations">
            <thead class="table-general">
            <tr>
            <th scope="col"># Cliente</th>
            <th scope="col">Cliente/Empresa</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Domicilio</th>
            <th scope="col">Último servicio</th>
            <th scope="col">Estatus</th>
            </tr>
            </thead>
            <tbody id="dataReinforcements">
            </tbody>
            </table>
            </div>`;

        $('#masterContainer').append(table);
    } // end createTable()

});