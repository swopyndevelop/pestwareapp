/**
 * @author Alberto Martínez
 * @version 26/12/2020
 * stationmonitoring/index.blade.php | show tree
 */

import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    $('.openModalShowAreaMonitoring').click(function () {
        let all = $(this);
        let idMonitoring = all.attr('data-id');
        let isInspection = all.attr('data-inspection');
        if (isInspection == 1) document.getElementById('inspectionShow').checked = true;
        else document.getElementById('orderShow').checked = true;
        $('#folioMonitoring').html(all.attr('data-monitoring'));
        getTreeData(idMonitoring);
    });

    function getTreeData(monitoringId) {
        loaderPestWare('')
        let containerTree = $('#container-tree');
        let treeAreaMonitoring = $("#jsShowTreeAreaMonitoring");
        const newTree = '<div id="jsShowTreeAreaMonitoring" class="demo" style="margin-top:1em; min-height:200px;"></div>';
        treeAreaMonitoring.remove();
        containerTree.append(newTree);
        let treeNewAreaMonitoring = $("#jsShowTreeAreaMonitoring");

        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            type: 'GET',
            url: route('area_monitoring_tree_read', monitoringId),
            success: function(data) {
                treeNewAreaMonitoring.jstree({
                    core: {
                        animation: 0,
                        check_callback: true,
                        force_text: true,
                        themes: { stripes: true },
                        data: data
                    },
                    types: {
                        "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
                        root: { icon: "fa fa-home", valid_children: ["default"] },
                        default: {
                            icon: "fa fa-th-large",
                            valid_children: ["default"]
                        },
                    },
                    plugins: ["dnd", "search", "state", "types"]
                });
                Swal.close();
            }
        });
    }

});
