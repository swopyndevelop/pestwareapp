/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 26/04/2021
 * areamonitoring/inspections.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    // Filters Date
    let initialDateInspection, finalDateInspection, filterDateInspection, idServiceOrderInspection = null,
        customersFilter = 0, addressFilter = 0;

    filterDateInspection = $('input[name="filterDateInspection"]');
    filterDateInspection.daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    filterDateInspection.on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        initialDateInspection = picker.startDate.format('YYYY-MM-DD');
        finalDateInspection = picker.endDate.format('YYYY-MM-DD');
    });

    filterDateInspection.on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#btnFilterInspection').click(function() {
        idServiceOrderInspection = $('#idServiceOrderInspection').val();
        customersFilter = $('#customersFilter option:selected').val();
        addressFilter = $('#addressFilter option:selected').val();
        initialDateInspection = initialDateInspection === undefined ? null : initialDateInspection;
        finalDateInspection = finalDateInspection === undefined ? null : finalDateInspection;
        getDataInspections();
    });

    // Load data table inspections.
    getDataInspections();

    function getDataInspections() {
        loaderPestWare('Cargando Inspecciones...');
        $.ajax({
            type: "GET",
            url: route('get_inspections_area'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idServiceOrderInspection: idServiceOrderInspection,
                customersFilter: customersFilter,
                addressFilter: addressFilter,
                initialDateInspection: initialDateInspection,
                finalDateInspection: finalDateInspection
            },
            success: function(data) {
                if (parseInt(data.code) === 500) {
                    Swal.close();
                    showToast('error','Error al guardar',data.message)
                } else {
                   // Draw data in table.
                    let tableBodyInspections = $('#tableBodyInspections');
                    let rows = '';
                    const {inspections} = data;
                    inspections.forEach((element) => {
                        const {id, id_area, id_service_order, date_inspection, hour_inspection, customer, email, urlWhatsappEmailAreaInspection,
                            establishment_name, address, address_number, colony, municipality, state, count_areas, id_order, cellphone,
                            cellphone_main, id_encoded} = element;
                        rows += `<tr>
                                <td>
                                    <div class="btn-group">
                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer">
                                            ${id_area} <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="openModalShowAreaInspection" data-id-order="${id_order}" data-id_service_order="${id_service_order}" data-id_area="${id_area}" data-date_inspection="${date_inspection}" data-hour_inspection="${hour_inspection}" style="cursor: pointer"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver Detalle</a></li>
                                            <li><a href="${route('area_inspection_pdf', id_encoded)}" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar PDF</a></li>
                                            <li>
                                            <a data-toggle="modal" data-target="#sendEmailInspectionArea"
                                                data-id="${id_encoded}"
                                                data-email="${email}" style="cursor: pointer">
                                                <i class="fa fa-envelope" aria-hidden="true" style="color: #222d32;"></i>Enviar Inspección Área
                                                </a>
                                            </li>
                                             <li>
                                                <a data-toggle="modal" data-target="#sendEmailWhatsapp"
                                                    data-id="${id_encoded}"
                                                    data-urlinspection="${urlWhatsappEmailAreaInspection}"
                                                    data-cellphone="${cellphone}"
                                                    data-cellphonemain="${cellphone_main}" style="cursor: pointer">
                                                    <i class="fa fa-whatsapp" aria-hidden="true" style="color: green;"></i>Enviar Inspección Área
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <td>${id_service_order}</td>
                                <td>${date_inspection}<br>${hour_inspection}</td>
                                <td>${customer}<br>${establishment_name}</td>
                                <td>${address} #${address_number},<br>${colony},<br>${municipality}, ${state}</td>
                                <td>${count_areas} áreas</td>
                            </tr>`;
                    });
                    tableBodyInspections.html('');
                    tableBodyInspections.append(rows);
                    $('.openModalShowAreaInspection').click(function () {
                        let all = $(this);
                        let orderId = all.attr('data-id-order');
                        let id_service_order = all.attr('data-id_service_order');
                        let id_area = all.attr('data-id_area');
                        let date_inspection = all.attr('data-date_inspection');
                        let hour_inspection = all.attr('data-hour_inspection');
                        getDetailInspectionByOrderId(orderId, id_service_order, id_area, date_inspection, hour_inspection);
                    });
                    $('#idServiceOrderInspection').val('');
                    filterDateInspection.val('');
                    initialDateInspection = null;
                    finalDateInspection = null;
                    Swal.close();
                }
            }
        });
    }

    function getDetailInspectionByOrderId(orderId, id_service_order, id_area, date_inspection, hour_inspection) {
        loaderPestWare('Generando Reporte. Espere un momento...');
        $.ajax({
            type: "GET",
            url: route('get_inspections_detail_area', orderId),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function(data) {
                if (parseInt(data.code) === 500) {
                    Swal.close();
                    showToast('warning','Error al cargar los datos',data.message)
                } else {
                    // Open Modal
                    $('#showAreaInspectionModal').modal("show");
                    // Draw data in table.
                    document.getElementById('labelNoService').innerText = id_service_order;
                    document.getElementById('labelIdArea').innerText = id_area;
                    document.getElementById('labelDate').innerText = date_inspection;
                    document.getElementById('labelHour').innerText = hour_inspection;

                    let tableBodyDetailInspection = $('#tableBodyDetailInspection');
                    let rows = '';
                    const {inspections} = data;
                    inspections.forEach((element) => {
                        const {zone, perimeter, area, technician, comments, plagues, hour_inspection, photos} = element;
                        let plaguesRow = '';
                        let photosRow = '';
                        plagues.forEach((plagueData) => {
                            const {plague, grade} = plagueData;
                            plaguesRow += `<li>${plague}: ${grade}</li>`;
                        });
                        photos.forEach((photoData) => {
                            const {url_s3} = photoData;
                            photosRow += `
                                <div id="selectorDetailPhotoInspection" style="cursor: pointer">
                                    <span class="item" data-src="${url_s3}">
                                        <img src="${url_s3}" alt="station" width="50px" height="50px">
                                    </span>
                               </div>`;
                        });
                        rows += `<tr>
                                <td class="text-center">${zone}</td>
                                <td class="text-center">${perimeter}</td>
                                <td class="text-center">${area}</td>
                                <td class="text-center">${technician}</td>
                                <td class="text-center">${comments === null ? 'Sin comentarios': comments}</td>
                                <td>
                                    <ul>
                                        ${plaguesRow}
                                    </ul>
                                </td>
                                <td class="text-center">${hour_inspection}</td>
                                <td class="text-center">
                                    
                                </td>
                            </tr>`;
                    });
                    tableBodyDetailInspection.html('');
                    tableBodyDetailInspection.append(rows);
                    Swal.close();
                }
            }
        });
    }

    let state = {
        'page': 1,
        'rows': 5,
        'window': 5,
    }

    function pagination(count, page, rows) {

        let trimStart = (page - 1) * rows
        let trimEnd = trimStart + rows

        //var trimmedData = querySet.slice(trimStart, trimEnd)

        let pages = Math.round(count / rows);

        return {
            'pages': pages,
        }
    }

    function pageButtons(pages) {
        let wrapper = document.getElementById('pagination-wrapper')
        wrapper.innerHTML = ``

        let maxLeft = (state.page - Math.floor(state.window / 2))
        let maxRight = (state.page + Math.floor(state.window / 2))

        if (maxLeft < 1) {
            maxLeft = 1
            maxRight = state.window
        }

        if (maxRight > pages) {
            maxLeft = pages - (state.window - 1)

            if (maxLeft < 1) {
                maxLeft = 1
            }
            maxRight = pages
        }

        for (let page = maxLeft; page <= maxRight; page++) {
            wrapper.innerHTML += `<li value=${page} class="page"><a>${page}</a></li>`
        }
        if (state.page != 1) {
            wrapper.innerHTML = `<li value=${1} class="page"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>` + wrapper.innerHTML
        }
        if (state.page != pages) {
            wrapper.innerHTML += `<li value=${pages} class="page"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>`
        }

        $('.page').on('click', function() {
            state.page = Number($(this).val())
            getData();
        })

    }

});