/**
 * @author Manuel Mendoza
 * @version 20/12/2020
 * register/_modalConfigStart2.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";
import {expressions, getFields, setFields, validateField} from "../validations/regex";

$(document).ready(function() {

    //fileds
    const inputTitleCustomQr = document.getElementById('inputTitleCustomQr');
    const inputSubtitleCustomQr = document.getElementById('inputSubtitleCustomQr');

    //validations
    const fieldsCustomQr = {
        inputTitleCustomQr: false,
        inputSubtitleCustomQr: false,
    }

    setFields(fieldsCustomQr);

    const validateForm = (e) => {
        switch (e.target.name) {

            case "inputTitleCustomQr":
                validateField(expressions.description, e.target.value, 'inputTitleCustomQr');
                break;
            case "inputSubtitleCustomQr":
                validateField(expressions.description, e.target.value, 'inputSubtitleCustomQr');
                break;
        }
    }

    inputTitleCustomQr.addEventListener('keyup', validateForm);
    inputTitleCustomQr.addEventListener('blur', validateForm);

    inputSubtitleCustomQr.addEventListener('keyup', validateForm);
    inputSubtitleCustomQr.addEventListener('blur', validateForm);


    $("#saveBtnCustomQr").click(function() {
        //fileds
        let inputTitleCustomQr = $('#inputTitleCustomQr').val();
        let inputSubtitleCustomQr = $('#inputSubtitleCustomQr').val();

        if (inputTitleCustomQr.length == 0) inputTitleCustomQr = 'Estación de Control';
        if (inputSubtitleCustomQr.length == 0) inputSubtitleCustomQr = 'No alterar Estación';

        validateField(expressions.description, inputTitleCustomQr, 'inputTitleCustomQr');
        validateField(expressions.description, inputSubtitleCustomQr, 'inputSubtitleCustomQr');

        if(getFields().inputTitleCustomQr && getFields().inputSubtitleCustomQr){
            saveCustomQr();
        }
        else {
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }
        function saveCustomQr() {
            loaderPestWare('Guardando');
            $.ajax({
                type: "POST",
                url: route('custom_qr'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    title: inputTitleCustomQr,
                    subtitle: inputSubtitleCustomQr
                },
                success: function(data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error','Error al guardar','Algo salió mal, intente de nuevo.')
                    } else if (data.code == 201) {
                        Swal.close();
                        $('#modalCustomQr').click();
                        showToast('success','Datos Guardados','Configuración QR guardada')
                    }
                }
            });
        }
    });
});