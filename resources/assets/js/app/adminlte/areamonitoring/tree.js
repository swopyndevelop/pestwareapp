/**
 * @author Alberto Martínez
 * @version 26/12/2020
 * stationmonitoring/index.blade.php
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWareLogin} from "../pestware/loaderLogin";

$(document).ready(function(){

    let zones = [];
    let perimeters = [];

    let customerId, customerName;
    // counters stations
    let area = 1;

    $('#selectFilterCustomerName').on('change', function() {
        customerId = $(this).val();
        //customerName = $("option:selected", this).text();
        let establishment = $("option:selected", this).attr('data-establishment');
        let name = $("option:selected", this).attr('data-name');
        $('.folioMonitoring').html(`A-${customerId}`);
        customerName = establishment.length > 0 ? establishment : name;
        const data = [
            { "id" : "1", "text" : customerName, "type" : "root" }
        ];
        setTreeCustomer(data);
    });

    let items = function($node) {
        //let tmpItems = $.jstree.defaults.contextmenu.items($node)
        let tmpItems = {};
        if ($node.type === 'root') {
            tmpItems['create'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Crear",
                "icon"              : "fa fa-plus",
                "submenu" 			: {
                    "create_zone" : {
                        "seperator_before" : false,
                        "seperator_after" : false,
                        "label" : "Zona",
                        "icon" : "fa fa-square-o",
                        action : function (data) {
                            let inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.create_node(obj, {}, "last", 'Zona', '', function (new_node) {
                                try {
                                    inst.edit(new_node);
                                } catch (ex) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                }
                            });
                        }
                    }
                }
            }
        }
        if ($node.type_node === 'Zona') {
            tmpItems['create'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Crear",
                "icon"              : "fa fa-plus",
                "submenu" 			: {
                    "create_perimetro" : {
                        "seperator_before" : false,
                        "seperator_after" : false,
                        "label" : "Perímetro",
                        "icon" : "fa fa-circle-o-notch",
                        action : function (data) {
                            let inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.create_node(obj, {}, "last", 'Perímetro', '', function (new_node) {
                                try {
                                    inst.edit(new_node);
                                } catch (ex) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                }
                            });
                        }
                    }
                }
            }
            tmpItems['remove'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Borrar",
                "icon"              : "fa fa-eraser",
                "action"			: function (data) {
                    let inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        inst.delete_node(inst.get_selected());
                    }
                    else {
                        inst.delete_node(obj);
                    }
                }
            }
            tmpItems['rename'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Renombrar",
                "icon"				: "fa fa-pencil",
                "action"			: function (data) {
                    let inst = $.jstree.reference(data.reference),
                    obj = inst.get_node(data.reference);
                    inst.edit(obj);
                }
            }
        }
        if ($node.type_node === 'Perímetro') {
            tmpItems['create'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Crear",
                "icon"              : "fa fa-plus",
                "submenu" 			: {
                    "create_area" : {
                        "seperator_before" : false,
                        "seperator_after" : false,
                        "label" : "Área",
                        "icon" : "fa fa-cube",
                        action : function (data) {
                            let inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.create_node(obj, {}, "last", 'Área', area++, function (new_node) {
                                try {
                                    inst.edit(new_node);
                                } catch (ex) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                }
                            });
                        }
                    },
                }
            }
            tmpItems['remove'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Borrar",
                "icon"              : "fa fa-eraser",
                "action"			: function (data) {
                    let inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        inst.delete_node(inst.get_selected());
                    }
                    else {
                        inst.delete_node(obj);
                    }
                }
            }
            tmpItems['rename'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Renombrar",
                "icon"				: "fa fa-pencil",
                "action"			: function (data) {
                    let inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    inst.edit(obj);
                }
            }
        }
        if ($node.type_node === 'Área') {
            tmpItems['remove'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Borrar",
                "icon"              : "fa fa-eraser",
                "action"			: function (data) {
                    let inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        area--;
                        inst.delete_node(inst.get_selected());
                    }
                    else {
                        inst.delete_node(obj);
                    }
                }
            }
            tmpItems['rename'] = {
                "separator_before"	: false,
                "separator_after"	: false,
                "_disabled"			: false,
                "label"				: "Renombrar",
                "icon"				: "fa fa-pencil",
                "action"			: function (data) {
                    let inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    inst.edit(obj);
                }
            }
        }

        return tmpItems;
    }
    $("#jsTreeAreaMonitoring")
        .jstree({
        core: {
            animation: 0,
            check_callback: true,
            force_text: true,
            themes: { stripes: true },
            data: []
        },
        types: {
            "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
            "root": { icon: "fa fa-home", valid_children: ["default", "file"] },
            "default": {
                icon: "fa fa-th-large",
                valid_children: ["default"]
            },
        },
        plugins: ["contextmenu", "dnd", "search", "state", "types", "wholerow"],
        contextmenu: {"items": items}
    });

    function setTreeCustomer(data) {
        const customerTree = $('#jsTreeAreaMonitoring');
        customerTree.jstree(true).settings.core.data = data;
        customerTree.jstree(true).refresh();
    }

    $("#saveTreeStationMonitoringClient").click(function() {
        loaderPestWareLogin('');
        let optionOne = document.getElementById('inspection').checked;
        let optionTwo = document.getElementById('order').checked;
        const customerTree = $("#jsTreeAreaMonitoring");
        let tree = customerTree.jstree(true).get_json();
        let tree_flat = customerTree.jstree(true).get_json(null, { flat: true });
        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            type: 'POST',
            url: route('station_monitoring_client_tree_new'),
            data: {
                tree: JSON.stringify(tree_flat),
                customerId: customerId,
                isInspection: optionOne,
                isOrder: optionTwo
            },
            success: function(data) {
                Swal.close();
                if (data.code == 500) showToast('error', 'Error al guardar.', data.message);
                else if (data.code == 201) {
                    document.getElementById("saveTreeStationMonitoringClient").disabled = true;
                    showToast('success-redirect', 'Monitoreo Guardado', 'Datos guardados correctamente.', 'index_area_station_monitoring');
                    $("#saveTreeStationMonitoringClient").attr('disabled', true);
                }
            }
        });
    });

    $("#loadNodesMultiple").click(function() {
        document.getElementById("configMultiple").style.display = "block";
        zones = [];
        perimeters = [];
        const customerTree = $("#jsTreeAreaMonitoring");
        let tree_flat = customerTree.jstree(true).get_json(null, { flat: true });
        tree_flat.forEach((node) => {
            console.log(node)
            if (node.type_node === 'Zona') zones.push(node);
            if (node.type_node === 'Perímetro') perimeters.push(node);
        });
        updateSelectZones();
        updateSelectPerimeters();
    });

    function updateSelectZones() {
        $("#zone").empty();
        zones.forEach((element) => {
            let option = `<option value="${element.id}"`;
            option = option + `>${element.text}</option>`;
            $("#zone").append(option);
        });
    }

    function updateSelectPerimeters() {
        $("#perimeter").empty();
        perimeters.forEach((element) => {
            let option = `<option value="${element.id}"`;
            option = option + `>${element.text}</option>`;
            $("#perimeter").append(option);
        });
    }

});
