/**
 * @author Alberto Martínez
 * @version 26/12/2020
 * stationmonitoring/index.blade.php | update tree
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    $('.openModalUpdateAreaMonitoring').click(function () {

        let monitoringId;
        let customerId = 0;

        $('#newAreaModalV2').modal("show");

        document.getElementById('finishedMonitoring').style.visibility = "visible";
        document.getElementById('modalTitleModalAreaV2').innerText = "Editar Área";
        document.getElementById('finishedMonitoring').textContent = "Editar Área";

        let checkBoxArea = document.getElementById("checkboxOnlyArea");
        checkBoxArea.addEventListener('change', (event) => {
            if (event.currentTarget.checked) {
                document.getElementById('initialRange').disabled = true;
                document.getElementById('finalRange').disabled = true;
            } else {
                document.getElementById('initialRange').disabled = false;
                document.getElementById('finalRange').disabled = false;
            }
        });

        let all = $(this);
        $('.folioMonitoring').html(all.attr('data-monitoring'));
        monitoringId = all.attr('data-id');
        const customerName = all.attr('data-customerName');
        customerId = all.attr('data-customerId');

        let selectCustomerEdit = $("#selectFilterCustomerNameV2");
        selectCustomerEdit.empty();
        let option = `<option value="${customerId}">${customerName}</option>`;
        selectCustomerEdit.append(option);
        document.getElementById('selectFilterCustomerNameV2').disabled = true;
        document.getElementById('panelStep2').style.display = "block";
        document.getElementById('panelStep3').style.display = "block";
        document.getElementById('panelStep4').style.display = "block";

        let isInspection = all.attr('data-inspection')
        if (isInspection == 1) document.getElementById('inspectionCheck').checked = true;
        else {
            document.getElementById('orderCheck').checked = true;
            document.getElementById('amount').style.display = "block";
            $('#amount').val(all.attr('data-amount'));
        }

        getAllTreeByCustomer(customerId);
        getTreeData(monitoringId);

        // Input Price
        let optionAmount = document.getElementById('orderCheck');
        let input = document.getElementById('amount');
        optionAmount.addEventListener('change', (event) => {
            if (event.currentTarget.checked) {
                input.style.display = "block";
            }
        });

        let optionAmountNo = document.getElementById('inspectionCheck');
        optionAmountNo.addEventListener('change', (event) => {
            if (event.currentTarget.checked) {
                input.style.display = "none";
                input.value = '';
            }
        });

        $("#saveZone").click(function() {
            let nameZone = document.getElementById('nameZone').value;
            let optionOne = document.getElementById('inspectionCheck').checked;
            let optionTwo = document.getElementById('orderCheck').checked;
            let amount = document.getElementById('amount').value;

            if (nameZone.length > 0) {
                loaderPestWare('Guardando');
                $.ajax({
                    type: "POST",
                    url: route('area_monitoring_tree_create_zone'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        customerId: customerId,
                        nameZone: nameZone,
                        isInspection: optionOne,
                        isOrder: optionTwo,
                        amount: amount
                    },
                    success: function(data) {
                        if (data.code == 500) {
                            Swal.close();
                            showToast('error','Error al guardar','Algo salió mal, intente de nuevo.')
                        } else if (data.code == 201) {
                            // Load data zones
                            loadDataZones(data);
                            // Load Tree diagram
                            getTreeData(data.areaId);
                            if (data.zones.length > 0) document.getElementById('panelStep3').style.display = "block";
                            else document.getElementById('panelStep3').style.display = "none";
                        }
                    }
                });
            } else showToast('warning','Espera...','El nombre de la zona es obligatorio.');

        });

        $("#savePerimeter").click(function() {
            let zoneSelect = document.getElementById('zoneSelect').value;
            let namePerimeter = document.getElementById('namePerimeter').value;

            if (namePerimeter.length > 0) {
                loaderPestWare('Guardando');
                $.ajax({
                    type: "POST",
                    url: route('area_monitoring_tree_create_perimeter'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        customerId: customerId,
                        namePerimeter: namePerimeter,
                        idNodeZone: zoneSelect
                    },
                    success: function(data) {
                        if (data.code == 500) {
                            Swal.close();
                            showToast('error','Error al guardar','Algo salió mal, intente de nuevo.')
                        } else if (data.code == 201) {
                            loadDataPerimeters(data);
                            // Load Tree diagram
                            getTreeData(data.areaId);
                            if (data.perimeters.length > 0) document.getElementById('panelStep4').style.display = "block";
                            else document.getElementById('panelStep4').style.display = "none";
                        }
                    }
                });
            } else showToast('warning','Espera...','El nombre del perímetro es obligatorio.');

        });

        $("#saveStation").click(function() {
            let perimeterSelect = document.getElementById('perimeterSelect').value;
            let nameArea = document.getElementById('nameArea').value;
            let initialRange = document.getElementById('initialRange').value;
            let finalRange = document.getElementById('finalRange').value;
            let isOnlyArea = checkBoxArea.checked;

            if (initialRange > 0 && finalRange > 0 || isOnlyArea) {
                loaderPestWare('Guardando');
                $.ajax({
                    type: "POST",
                    url: route('area_monitoring_tree_create_area'),
                    data: {
                        _token: $("meta[name=csrf-token]").attr("content"),
                        customerId: customerId,
                        perimeterSelect: perimeterSelect,
                        nameArea: nameArea,
                        initialRange: initialRange,
                        finalRange: finalRange,
                        isOnlyArea: isOnlyArea
                    },
                    success: function(data) {
                        if (data.code == 500) {
                            Swal.close();
                            showToast('error','Error al guardar', data.message)
                        } else if (data.code == 201) {
                            //loadDataStations(data);
                            // Load Tree diagram
                            document.getElementById('nameArea').value = '';
                            document.getElementById('initialRange').value = '';
                            document.getElementById('finalRange').value = '';
                            getTreeData(data.areaId);
                            Swal.close();
                        }
                    }
                });
            } else showToast('warning','Espera...','Los rangos deben ser mayor a 0 y el rango final debe ser mayor al inicial.');

        });

        function getAllTreeByCustomer(id) {
            loaderPestWare('Cargando...');
            $.ajax({
                type: "GET",
                url: route('area_get_all_tree_by_customer', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al cargar',data.message)
                    } else {
                        // Load data tables
                        loadDataZones(data);
                        loadDataPerimeters(data);
                        //loadDataStations(data); Add Update
                        loadDataStationsMap(data);
                        // Load Tree diagram
                        getTreeData(data.areaId);
                        showToast('success','Datos Cargados',data.message)
                    }
                }
            });
        }

        function deleteZoneById(id) {
            loaderPestWare('Eliminando...');
            $.ajax({
                type: "POST",
                url: route('area_monitoring_tree_delete_zone', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar',data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Zona Eliminada',data.message)
                    }
                }
            });
        }

        function updateZoneById(id, name) {
            loaderPestWare('Editando...');
            $.ajax({
                type: "POST",
                url: route('area_monitoring_tree_update_zone'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: id,
                    name: name
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al editar',data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Zona Editada',data.message)
                    }
                }
            });
        }

        function deletePerimeterById(id) {
            loaderPestWare('Eliminando...');
            $.ajax({
                type: "POST",
                url: route('area_monitoring_tree_delete_perimeter', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar',data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Perímetro Eliminado',data.message)
                    }
                }
            });
        }

        function updatePerimeterById(id, name) {
            loaderPestWare('Editando...');
            $.ajax({
                type: "POST",
                url: route('area_monitoring_tree_update_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: id,
                    name: name
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al editar',data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Perímetro Editado',data.message)
                    }
                }
            });
        }

        function relocateStationsByPerimeter(idArea, idNodeStation, nameArea) {
            loaderPestWare('Reubicando...');
            $.ajax({
                type: "POST",
                url: route('station_area_tree_relocate_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    idArea: idArea,
                    idNodeStation: idNodeStation,
                    nameArea: nameArea
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al reubicar', data.message)
                    } else {
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estaciones Reubicadas', data.message)
                        $('#relocateStation').click();
                    }
                }
            });
        }

        function cancelStationsByPerimeter(idArea) {
            loaderPestWare('Cancelando...');
            $.ajax({
                type: "POST",
                url: route('station_area_tree_cancel_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    idArea: idArea
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estaciones Areas', data.message)
                    }
                }
            });
        }

        function deleteStationsByPerimeterSingular(idArea) {
            loaderPestWare('Eliminando...');
            $.ajax({
                type: "POST",
                url: route('station_area_tree_delete_stations_by_perimeter_singular'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    idArea: idArea
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Estación (Área) Eliminada', data.message)
                    }
                }
            });
        }

        function deleteStationsByPerimeter(idTypeStation, parent) {
            loaderPestWare('Eliminando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_delete_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id_customer: customerId,
                    id_type_station: idTypeStation,
                    parent: parent
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al eliminar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Áreas Eliminadas', data.message)
                    }
                }
            });
        }

        function updateStationsByPerimeter(idTypeStation, parent, quantity) {
            loaderPestWare('Actualizando...');
            $.ajax({
                type: "POST",
                url: route('station_monitoring_tree_update_stations_by_perimeter'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id_customer: customerId,
                    id_type_station: idTypeStation,
                    parent: parent,
                    quantity: quantity
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al actualizar', data.message)
                    } else {
                        // Load data tables
                        getAllTreeByCustomer(customerId);
                        showToast('success','Áreas Actualizadas', data.message)
                    }
                }
            });
        }

        function loadDataZones(data) {
            let tableBodyZones = $('#tableBodyDetailZones');
            Swal.close();
            showToast('success','Datos Guardados','Zona agregada.')
            let selectZone = $("#zoneSelect");
            document.getElementById('nameZone').value = "";
            selectZone.empty();
            if (data.zones.length > 0) document.getElementById('tableZones').style.display = "block";
            else document.getElementById('tableZones').style.display = "none";

            let rows = '';
            data.zones.forEach((element) => {
                // Load zones select
                let option = `<option value="${element.id_node}"`;
                option = option + `>${element.text}</option>`;
                selectZone.append(option);
                // Load data table zones
                const actions = `<a id="updateItemZone-${element.id}" data-id="${element.id}" data-zone="${element.text}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
                                ${data.lastInspection == null ? `<a id="deleteItemZone-${element.id}" 
                                data-id="${element.id}" 
                                data-zone="${element.text}" style="cursor: pointer;">
                                <i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>` : ''} `;
                rows += `<tr>
                                <td class="text-center">ZN-${element.id}</td>
                                <td class="text-center">${element.text}</td>
                                <td class="text-center">${actions}</td>
                            </tr>`;
            });
            tableBodyZones.html('');
            tableBodyZones.append(rows);
            data.zones.forEach((element) => {
                // Events =>
                $(`#updateItemZone-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    const zone = all.attr('data-zone');
                    Swal.fire({
                        title: 'Editar Zona',
                        input: 'text',
                        inputValue: zone,
                        inputAttributes: {
                            autocapitalize: 'off',
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Editar',
                        cancelButtonText: 'Cancelar',
                        showLoaderOnConfirm: true,
                        preConfirm: (data) => {
                            if (data.length > 0) updateZoneById(id, data.trim());
                            else showToast('warning', 'Espera..', 'Debes ingresar un nombre de zona.');
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            console.log('otro')
                        }
                    })
                });
                $(`#deleteItemZone-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    const zone = all.attr('data-zone');
                    Swal.fire({
                        title: '¿Está seguro de eliminar la zona?',
                        text: `Al eliminar la zona: ${zone}, se borrarán todos sus perímetros y áreas asociadas.`,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            deleteZoneById(id);
                        }
                    })
                });
            });
        }

        function loadDataPerimeters(data) {
            let tableBodyPerimeters = $('#tableBodyDetailPerimeters');
            Swal.close();
            //$('#modalCustomQr').click();
            showToast('success','Datos Guardados','Perímetro agregado.');
            let selectPerimeter = $("#perimeterSelect");
            let selectTypeStations = $('#typeStationsSelect');
            document.getElementById('namePerimeter').value = "";
            if (data.perimeters.length > 0) document.getElementById('tablePerimeters').style.display = "block";
            else document.getElementById('tablePerimeters').style.display = "none";
            selectPerimeter.empty();
            let rows = '';
            data.perimeters.forEach((element) => {
                // Load perimeters select
                let option = `<option value="${element.id_node}"`;
                option = option + `>${element.text}</option>`;
                selectPerimeter.append(option);
                // Load data table zones
                const actions = `<a id="updateItemPerimeter-${element.id}" data-id="${element.id}" data-perimeter="${element.text}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
                                ${data.lastInspection == null ? 
                                `<a id="deleteItemPerimeter-${element.id}" 
                                data-id="${element.id}" 
                                data-perimeter="${element.text}" style="cursor: pointer;">
                                <i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>` : ''} `;
                rows += `<tr>
                                <td class="text-center">PR-${element.id}</td>
                                <td class="text-center">${element.text}</td>
                                <td class="text-center">${actions}</td>
                            </tr>`;
            });
            tableBodyPerimeters.html('');
            tableBodyPerimeters.append(rows);
            data.perimeters.forEach((element) => {
                // Events =>
                $(`#updateItemPerimeter-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    const perimeter = all.attr('data-perimeter');
                    Swal.fire({
                        title: 'Editar Perímetro',
                        input: 'text',
                        inputValue: perimeter,
                        inputAttributes: {
                            autocapitalize: 'off',
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Editar',
                        cancelButtonText: 'Cancelar',
                        showLoaderOnConfirm: true,
                        preConfirm: (data) => {
                            if (data.length > 0) updatePerimeterById(id, data.trim());
                            else showToast('warning', 'Espera..', 'Debes ingresar un nombre de perímetro.');
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            console.log('otro')
                        }
                    })
                });
                $(`#deleteItemPerimeter-${element.id}`).click(function () {
                    let all = $(this);
                    const id = all.attr('data-id');
                    const perimeter = all.attr('data-perimeter');
                    Swal.fire({
                        title: '¿Está seguro de eliminar el perímetro?',
                        text: `Al eliminar el perímetro: ${perimeter}, se borrarán todos sus áreas asociadas.`,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            deletePerimeterById(id);
                        }
                    })
                });
            });
        }

        function loadDataStations(data) {
            let tableBody = $('#tableBodyDetail');
            showToast('success','Datos Guardados','Áreas Guardadas.')
            //document.getElementById('quantityStation').value = '';
            document.getElementById('finishedMonitoring').style.visibility = "visible";

            if (data.stations.length > 0) document.getElementById('tableStations').style.display = "block";
            else document.getElementById('tableStations').style.display = "none";

            let rows = '';
            data.stations.forEach((element) => {
                const actions = `<a id="updateItemRow-${element.id}-${element.parent}" data-parent="${element.parent}" data-idTypeStation="${element.id_type_station}" data-quantity="${element.quantity}" data-name="${element.type_station}" data-zone="${element.zone}" data-perimeter="${element.text}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
            <a id="deleteItemRow-${element.id}-${element.parent}" data-parent="${element.parent}" data-idTypeStation="${element.id_type_station}" style="cursor: pointer;"><i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>`;
                rows += `<tr>
                                <td class="text-center">${element.zone}</td>
                                <td class="text-center">${element.text}</td>
                                <td class="text-center">${element.type_station}</td>
                                <td class="text-center">${element.quantity}</td>
                                <td class="text-center">${actions}</td>
                            </tr>`;
            });
            tableBody.html('');
            tableBody.append(rows);
            data.stations.forEach((element) => {
                // Events =>
                $(`#updateItemRow-${element.id}-${element.parent}`).click(function () {
                    let all = $(this);
                    const quantity = all.attr('data-quantity');
                    const name = all.attr('data-name');
                    const zone = all.attr('data-zone');
                    const perimeter = all.attr('data-perimeter');
                    const parent = all.attr('data-parent');
                    const idTypeStation = all.attr('data-idTypeStation');
                    Swal.fire({
                        title: `Número de ${name} en ${zone}: ${perimeter}`,
                        input: 'number',
                        inputValue: quantity,
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Editar',
                        cancelButtonText: 'Cancelar',
                        showLoaderOnConfirm: true,
                        preConfirm: (data) => {
                            if (data.length > 0 && data > 0) updateStationsByPerimeter(idTypeStation, parent, data);
                            else showToast('warning', 'Espera...', 'El número de áreas debe ser mayor a 0.')
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            console.log('other');
                        }
                    })
                });
                $(`#deleteItemRow-${element.id}-${element.parent}`).click(function () {
                    let all = $(this);
                    const parent = all.attr('data-parent');
                    const idTypeStation = all.attr('data-idTypeStation');
                    Swal.fire({
                        title: 'Eliminar Áreas',
                        text: "¿Está seguro de eliminar las áreas seleccionadas?",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            deleteStationsByPerimeter(idTypeStation, parent);
                        }
                    })
                });
            });
        }

        function loadDataStationsMap(data) {
            let tableBody = $('#tableDetailStationMap');

            if (data.stationAreas.length > 0) document.getElementById('tableStationsMap').style.display = "block";
            else document.getElementById('tableStationsMap').style.display = "none";

            let rows = '';
            data.stationAreas.forEach((element) => {
                rows += `<tr>
                                <td class="text-center">${element.text}</td>
                                <td class="text-center"><a data-toggle="modal" data-target="#relocateStation" 
                                data-id="${element.id}"
                                data-parent="${element.parent}"
                                data-text="${element.text}"
                                data-idNode="${element.id_node}"
                                style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i>
                                </td>
                                ${element.visible == 0 ?
                    `<td class="text-center"><a id="cancelItemRow-${element.id}" 
                                data-id="${element.id}" data-visible="Activar" style="cursor: pointer;">
                                <i class="fa fa-times fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>
                                </td>`
                    :
                    `<td class="text-center"><a id="cancelItemRow-${element.id}" 
                                data-id="${element.id}" data-visible="Desactivar" style="cursor: pointer;">
                                <i class="fa fa-check fa-lg " aria-hidden="true" style="margin-left: 5px; color : green"></i></a>
                                </td>` };
                                ${data.deleteData || data.master ?
                    `<td class="text-center"><a id="deleteItemRow-${element.id}" 
                                data-id="${element.id}" style="cursor: pointer;">
                                <i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>
                                </td>` : ''};
                            </tr>`;
            });
            tableBody.html('');
            tableBody.append(rows);
            data.stationAreas.forEach((element) => {
                // Events =>
                $(`#cancelItemRow-${element.id}`).click(function () {
                    let all = $(this);
                    const idArea = all.attr('data-id');
                    const visible = all.attr('data-visible');
                    Swal.fire({
                        title: 'Estación Area',
                        text: `¿Está seguro deseas ${visible} la Area?`,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            cancelStationsByPerimeter(idArea);
                        }
                    })
                });
                $(`#deleteItemRow-${element.id}`).click(function () {
                    let all = $(this);
                    const idArea = all.attr('data-id');
                    Swal.fire({
                        title: 'Eliminar Estación',
                        text: 'Al borrar la estación (Área) se borrarán las Inspecciones asociadas a la Orden. \n ¿Está seguro deseas eliminar la Estación (Area)? ',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            deleteStationsByPerimeterSingular(idArea);
                        }
                    })
                });
            });

        }

        // Relocate Station
        let idStationPerimeterArea, selectPerimeterSingularArea;
        $('#relocateStation').on('show.bs.modal', function(e) {
            idStationPerimeterArea = $(e.relatedTarget).data().id;
            let idParentNode = $(e.relatedTarget).data().parent;
            selectPerimeterSingularArea = $("#perimeterSelectSingular");
            let textStation = $(e.relatedTarget).data().text;
            $('#nameStationArea').val(textStation);
            $('#labelTextStation').html(textStation);
            selectPerimeterSingularArea.empty();

            $.ajax({
                type: "GET",
                url: route('get_perimeters_all_monitoring_area'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    customerId: customerId
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al cargar',data.message)
                    } else {
                        // Load perimeters select
                        data.perimeters.forEach((element) => {
                            let option = '';
                            if (element.id_node === idParentNode) {
                                option = `<option selected value="${element.id_node}">${element.text}</option>`;
                            } else {
                                option = `<option value="${element.id_node}">${element.text}</option>`;
                            }
                            selectPerimeterSingularArea.append(option);
                        });
                    }
                }
            });

        });

        $('#btnRelocateStation').click(function () {
            let nameArea = document.getElementById('nameStationArea').value;
            relocateStationsByPerimeter(idStationPerimeterArea, selectPerimeterSingularArea.val(), nameArea);
        });

        function getTreeData(areaId) {
            loaderPestWare('')
            let containerTree = $('#container-tree-pv');
            let treeAreaMonitoring = $("#jsShowTreeAreaMonitoringPv");
            const newTree = '<div id="jsShowTreeAreaMonitoringPv" class="demo" style="margin-top:1em; min-height:200px;"></div>';
            treeAreaMonitoring.remove();
            containerTree.append(newTree);
            let treeNewAreaMonitoring = $("#jsShowTreeAreaMonitoringPv");

            $.ajaxSetup({
                headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
            });
            $.ajax({
                type: 'GET',
                url: route('area_monitoring_tree_read', areaId),
                success: function(data) {
                    treeNewAreaMonitoring.jstree({
                        core: {
                            animation: 0,
                            check_callback: true,
                            force_text: true,
                            themes: { stripes: true },
                            data: data
                        },
                        types: {
                            "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
                            root: { icon: "fa fa-home", valid_children: ["default"] },
                            default: {
                                icon: "fa fa-th-large",
                                valid_children: ["default"]
                            },
                        },
                        plugins: ["dnd", "search", "state", "types"]
                    });
                    Swal.close();
                }
            });
        }

        function updateTreeDate(id) {
            loaderPestWare('Actualizando...');
            let optionOne = document.getElementById('inspectionCheck').checked;
            let optionTwo = document.getElementById('orderCheck').checked;
            let inputAmount = document.getElementById('amount').value;
            $.ajax({
                type: "POST",
                url: route('area_monitoring_tree_update_date', id),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    isInspection: optionOne,
                    isOrder: optionTwo,
                    amount: inputAmount
                },
                success: function(data) {
                    Swal.close();
                    if (data.code == 500) {
                        showToast('error','Error al actualizar', data.message)
                    } else {
                        document.getElementById('saveZone').disabled = true;
                        document.getElementById('savePerimeter').disabled = true;
                        document.getElementById('saveStation').disabled = true;
                        document.getElementById('finishedMonitoring').disabled = true;
                        showToast('success-redirect', 'Área Guardada', 'Se guardaron las estaciones', 'index_area_station_monitoring');
                    }
                }
            });
        }

        $("#finishedMonitoring").click(function() {
            updateTreeDate(monitoringId);
        });

        $('#newAreaModalV2').on('hidden.bs.modal', function () {
            location.reload();
        })

    });

});
