/**
 * @author Alberto Martínez
 * @version 26/12/2020
 * stationmonitoring/index.blade.php | update tree
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    let monitoringId;
    let treeNodes;

    // counters stations
    let area = 1;

    $('.openModalUpdateAreaMonitoring').click(function () {
        let all = $(this);
        $('.folioMonitoringUpdate').html(all.attr('data-monitoring'));
        let isInspection = all.attr('data-inspection');
        if (isInspection == 1) document.getElementById('inspectionUpd').checked = true;
        else document.getElementById('orderUpd').checked = true;
        monitoringId = all.attr('data-id');
        readTreeMonitoring(all.attr('data-id'));
        getTreeNodes();
    });

    function readTreeMonitoring(monitoringId) {
        let items = function($node) {
            let tmpItems = {};
            let typeArea = 'main';

            treeNodes.forEach((element) => {
                if (element.id === $node.a_attr.id_test) typeArea = element.type_node;
            });

            if (typeArea === 'main') {
                tmpItems['create'] = {
                    "separator_before": false,
                    "separator_after": false,
                    "_disabled": false,
                    "label": "Crear",
                    "icon": "fa fa-plus",
                    "submenu": {
                        "create_zone": {
                            "seperator_before": false,
                            "seperator_after": false,
                            "label": "Zona",
                            "icon": "fa fa-square-o",
                            action: function (data) {
                                let inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                                inst.create_node(obj, {}, "last", 'Zona', '', function (new_node) {
                                    try {
                                        inst.edit(new_node);
                                    } catch (ex) {
                                        setTimeout(function () {
                                            inst.edit(new_node);
                                        }, 0);
                                    }
                                });
                            }
                        }
                    }
                }

                tmpItems['copy'] = {
                    "separator_before": false,
                    "separator_after": false,
                    "icon": "fa fa-clone",
                    "label": "Copiar",
                    "action": function (data) {
                        let inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        if (inst.is_selected(obj)) {
                            inst.copy(inst.get_top_selected());
                        } else {
                            inst.copy(obj);
                        }
                    }
                }
            }

            if (typeArea === 'Zona') {
                tmpItems['create'] = {
                    "separator_before"	: false,
                    "separator_after"	: false,
                    "_disabled"			: false,
                    "label"				: "Crear",
                    "icon"              : "fa fa-plus",
                    "submenu" 			: {
                        "create_perimetro" : {
                            "seperator_before" : false,
                            "seperator_after" : false,
                            "label" : "Perímetro",
                            "icon" : "fa fa-circle-o-notch",
                            action : function (data) {
                                let inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                                inst.create_node(obj, {}, "last", 'Perímetro', '', function (new_node) {
                                    try {
                                        inst.edit(new_node);
                                    } catch (ex) {
                                        setTimeout(function () { inst.edit(new_node); },0);
                                    }
                                });
                            }
                        }
                    }
                }
                tmpItems['remove'] = {
                    "separator_before"	: false,
                    "separator_after"	: false,
                    "_disabled"			: false,
                    "label"				: "Borrar",
                    "icon"              : "fa fa-eraser",
                    "action"			: function (data) {
                        let inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        if(inst.is_selected(obj)) {
                            inst.delete_node(inst.get_selected());
                        }
                        else {
                            inst.delete_node(obj);
                        }
                    }
                }
                tmpItems['rename'] = {
                    "separator_before"	: false,
                    "separator_after"	: false,
                    "_disabled"			: false,
                    "label"				: "Renombrar",
                    "icon"				: "fa fa-pencil",
                    "action"			: function (data) {
                        let inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        inst.edit(obj);
                    }
                }
                tmpItems['paste'] = {
                    "separator_before": false,
                    "separator_after"	: false,
                    "icon": "fa fa-clipboard",
                    "label"				: "Pegar",
                    "_disabled"	: function (data) {
                        return !$.jstree.reference(data.reference).can_paste();
                    },
                    "action" : function (data) {
                        let inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        inst.paste(obj);
                    }
                }
            }

            if (typeArea === 'Perímetro') {
                tmpItems['create'] = {
                    "separator_before"	: false,
                    "separator_after"	: false,
                    "_disabled"			: false,
                    "label"				: "Crear",
                    "icon"              : "fa fa-plus",
                    "submenu" 			: {
                        "create_area" : {
                            "seperator_before" : false,
                            "seperator_after" : false,
                            "label" : "Área",
                            "icon" : "fa fa-simplybuilt",
                            action : function (data) {
                                let inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                                inst.create_node(obj, {}, "last", 'Área', area++, function (new_node) {
                                    try {
                                        inst.edit(new_node);
                                    } catch (ex) {
                                        setTimeout(function () { inst.edit(new_node); },0);
                                    }
                                });
                            }
                        }
                    }
                }
                tmpItems['remove'] = {
                    "separator_before"	: false,
                    "separator_after"	: false,
                    "_disabled"			: false,
                    "label"				: "Borrar",
                    "icon"              : "fa fa-eraser",
                    "action"			: function (data) {
                        let inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        if(inst.is_selected(obj)) {
                            inst.delete_node(inst.get_selected());
                        }
                        else {
                            inst.delete_node(obj);
                        }
                    }
                }
                tmpItems['rename'] = {
                    "separator_before"	: false,
                    "separator_after"	: false,
                    "_disabled"			: false,
                    "label"				: "Renombrar",
                    "icon"				: "fa fa-pencil",
                    "action"			: function (data) {
                        let inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        inst.edit(obj);
                    }
                }
            }

            if (typeArea === 'main') {
                if ($node.type === 'root') {
                    tmpItems['create'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Crear",
                        "icon"              : "fa fa-plus",
                        "submenu" 			: {
                            "create_zone" : {
                                "seperator_before" : false,
                                "seperator_after" : false,
                                "label" : "Zona",
                                "icon" : "fa fa-square-o",
                                action : function (data) {
                                    var inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);
                                    inst.create_node(obj, {}, "last", 'Zona', '', function (new_node) {
                                        try {
                                            inst.edit(new_node);
                                        } catch (ex) {
                                            setTimeout(function () { inst.edit(new_node); },0);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                if ($node.type_node === 'Zona') {
                    tmpItems['create'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Crear",
                        "icon"              : "fa fa-plus",
                        "submenu" 			: {
                            "create_perimetro" : {
                                "seperator_before" : false,
                                "seperator_after" : false,
                                "label" : "Perímetro",
                                "icon" : "fa fa-circle-o-notch",
                                action : function (data) {
                                    let inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);
                                    inst.create_node(obj, {}, "last", 'Perímetro', '', function (new_node) {
                                        try {
                                            inst.edit(new_node);
                                        } catch (ex) {
                                            setTimeout(function () { inst.edit(new_node); },0);
                                        }
                                    });
                                }
                            }
                        }
                    }
                    tmpItems['remove'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Borrar",
                        "icon"              : "fa fa-eraser",
                        "action"			: function (data) {
                            let inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            if(inst.is_selected(obj)) {
                                inst.delete_node(inst.get_selected());
                            }
                            else {
                                inst.delete_node(obj);
                            }
                        }
                    }
                    tmpItems['rename'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Renombrar",
                        "icon"				: "fa fa-pencil",
                        "action"			: function (data) {
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.edit(obj);
                        }
                    }
                }
                if ($node.type_node === 'Perímetro') {
                    tmpItems['create'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Crear",
                        "icon"              : "fa fa-plus",
                        "submenu" 			: {
                            "create_area" : {
                                "seperator_before" : false,
                                "seperator_after" : false,
                                "label" : "Área",
                                "icon" : "fa fa-simplybuilt",
                                action : function (data) {
                                    let inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);
                                    inst.create_node(obj, {}, "last", 'Área', area++, function (new_node) {
                                        try {
                                            inst.edit(new_node);
                                        } catch (ex) {
                                            setTimeout(function () { inst.edit(new_node); },0);
                                        }
                                    });
                                }
                            }
                        }
                    }
                    tmpItems['remove'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Borrar",
                        "icon"              : "fa fa-eraser",
                        "action"			: function (data) {
                            let inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            if(inst.is_selected(obj)) {
                                inst.delete_node(inst.get_selected());
                            }
                            else {
                                inst.delete_node(obj);
                            }
                        }
                    }
                    tmpItems['rename'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Renombrar",
                        "icon"				: "fa fa-pencil",
                        "action"			: function (data) {
                            let inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.edit(obj);
                        }
                    }
                }
                if ($node.type_node === 'Área') {
                    tmpItems['remove'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Borrar",
                        "icon"              : "fa fa-eraser",
                        "action"			: function (data) {
                            let inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            if(inst.is_selected(obj)) {
                                area--;
                                inst.delete_node(inst.get_selected());
                            }
                            else {
                                inst.delete_node(obj);
                            }
                        }
                    }
                    tmpItems['rename'] = {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false,
                        "label"				: "Renombrar",
                        "icon"				: "fa fa-pencil",
                        "action"			: function (data) {
                            let inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            inst.edit(obj);
                        }
                    }
                }
            }

            return tmpItems;
        }
        // load tree for update
        let containerTreeUpd = $('#container-tree-upd');
        let treeAreaMonitoringUpd = $("#jsUpdateTreeAreaMonitoring");
        const newTreeUpd = '<div id="jsUpdateTreeAreaMonitoring" class="demo" style="margin-top:1em; min-height:200px;"></div>';
        treeAreaMonitoringUpd.remove();
        containerTreeUpd.append(newTreeUpd);
        let treeNewAreaMonitoringUpd = $("#jsUpdateTreeAreaMonitoring");

        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            type: 'GET',
            url: route('area_monitoring_tree_read', monitoringId),
            success: function(data) {
                treeNewAreaMonitoringUpd.jstree({
                    core: {
                        animation: 0,
                        check_callback: true,
                        force_text: true,
                        themes: { stripes: true },
                        data: data
                    },
                    types: {
                        "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
                        root: { icon: "fa fa-home", valid_children: ["default"] },
                        default: {
                            icon: "fa fa-th-large",
                            valid_children: ["default"]
                        },
                    },
                    plugins: ["contextmenu", "dnd", "search", "state", "types", "wholerow"],
                    contextmenu: {"items": items}
                });
            }
        });
    }

    function getTreeNodes() {
        loaderPestWare('');

        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            type: 'GET',
            url: route('area_monitoring_node_area', monitoringId),
            success: function(data) {
                Swal.close();
                treeNodes = data;
                data.forEach((element) => {
                    if (element.type_node === 'Área') area++;
                });
            }
        });
    }

    $("#updateTreeStationMonitoring").click(function() {
        loaderPestWare('');
        const customerUpdateTree = $("#jsUpdateTreeAreaMonitoring");
        let tree_flat_update = customerUpdateTree.jstree(true).get_json(null, { flat: true });
        let optionOne = document.getElementById('inspectionUpd').checked;
        let optionTwo = document.getElementById('orderUpd').checked;
        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            type: 'POST',
            url: route('area_monitoring_tree_edit'),
            data: {
                tree: JSON.stringify(tree_flat_update),
                monitoringId: monitoringId,
                isInspection: optionOne,
                isOrder: optionTwo
            },
            success: function(data) {
                Swal.close();
                if (data.code == 500) showToast('error', 'Error al actualizar.', data.message);
                else if (data.code == 201) {
                    document.getElementById("updateTreeStationMonitoring").disabled = true;
                    showToast('success-redirect', 'Área Actualizada', 'Datos actualizados correctamente.', 'index_area_station_monitoring');
                }
            }
        });
    });

});
