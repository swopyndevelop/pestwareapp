/**
 * @author Alberto Martinez
 * @version 05/10/2020
 * register/index.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function () { // on load

    $('#myModalProfileEdit').on('show.bs.modal', function (e) {

        $.ajax({

            type: 'GET',
            url: route('edit_user_profile'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (data) {

                if (data.code === 500) {
                    faltante();
                } else {
                    document.getElementById("centerJob").value = data.nameBranch;
                    document.getElementById("workstation").value = data.nameJobTitle;
                }
            }
        });
    });

    // Validar archivo
    const achievementImgProfile = document.getElementById('achievementImgProfile');
    achievementImgProfile.addEventListener('change', (event) => {
        checkFile(event);
    });


    $('#formEditProfile').on('submit', function(event) {

        event.preventDefault();

        let isChangePassword = $('#changePassword').is(':checked');
        let password = $('#password').val();
        console.log(isChangePassword);
        if (isChangePassword && password.length < 6) missingText('La contraseña al menos debe tener 6 caracteres');

        else editCaptureProduct();

    });

    function editCaptureProduct() {
        loaderPestWare('Actualizando perfil...');
        let fd = new FormData(document.getElementById("formEditProfile"));

        $.ajax({
            type: 'POST',
            url: route('update_user_profile'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (data) {
                Swal.close();
                if (data.code === 500) {
                    showToast('warning', 'Espera...', data.message);
                } else {
                    showToast('success', 'Datos Actualizados', data.message);
                    location.reload();
                }
            }
        });
    }

    function faltante() {
        swal({
            title: "¡Espera!",
            text: "Algo salio mal",
            type: "error",
            timer: 2000,
            showCancelButton: false,
            showConfirmButton: false
        }).catch(swal.noop);
    }
    //end sweet alert

    function checkFile(e) {

        const fileList = e.target.files;
        let error = false;
        let defaultImage = 'https://www.gravatar.com/avatar/8dd9506c61dfc3c43d1100df4fe21035.jpg?s=80&d=mm&r=g';
        let image = document.getElementById('imgProfile');
        for (let i = 0, file; file = fileList[i]; i++) {

            let sFileName = file.name;
            let sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            let iFileSize = file.size;
            // let iConvert = (file.size / 1048576).toFixed(2);

            if (!(sFileExtension === "png" ||
                sFileExtension === "jpg" ||
                sFileExtension === "jpeg") || iFileSize > 10485760) { /// 10 mb
                error = true;
            }
        }
        if (error) {
            missingText("El archivo ingresado no es aceptado o su tamaño excede lo aceptado");
            document.getElementById("achievementImgProfile").value = "";
            image.src = defaultImage;
        }
        if (!error) {
            image.src = URL.createObjectURL(e.target.files[0]);
        }
    }
});