export const expressions = {
    name: /^[a-zA-ZÀ-ÿ\s]{3,250}$/, // Letras y espacios, pueden llevar acentos.
    description: /^.{3,250}$/, // Letras, números y símbolos de la A a la Z
    descriptionNumber: /^.{1,250}$/, // Letras, números y símbolos de la A a la Z
    descriptionLarge: /^(?=[\S\s]{3,5000}$)[\S\s]*/, // Letras, números y símbolos de la A a la Z
    descriptionNotRequired: /^.{0,250}$/, // Letras, números y símbolos de la A a la Z
    daysYear: /^(?:36[0-5]|3[0-5]\d|[12]\d{2}|[1-9]\d?)$/, // sólo números del anio 1-365
    totalDecimal: /^[0-9]+([.][0-9]+)?$/, // sólo números con decimales
    phone: /^\d{7,14}$/, // 7 a 14 números.
    email: /^[^\s@]+@[^\s@]+\.[^\s@]+$/, // email@email.com
    digits: /^[0-9]?\d{1,6}$/, // Números enteros.
    url: /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/, //páginas de internet
    rfc: /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/, // valida rfc
    accounterNumber: /^\d{10}$/, // Valida numero de cuenta 10 digítos
    clabe: /^\d{18}$/, // Valida clabe interbancaria de 18 digitos
    password: /^.{8,250}$/,
}

let fields = {};
export function setFields(fieldsForm) {
       fields = fieldsForm;
}

export function setOnlyField(field, value) {
    fields[field] = value;
}

export function getFields() {
    return fields;
}

export const validateField = (expresion, value, field) => {
    if(expresion.test(value)){
        document.getElementById(`grupo__${field}`).classList.remove('formulario__grupo-incorrecto');
        document.getElementById(`grupo__${field}`).classList.add('formulario__grupo-correcto');
        document.querySelector(`#grupo__${field} i`).classList.add('fa-check-circle');
        document.querySelector(`#grupo__${field} i`).classList.remove('fa-times-circle');
        document.querySelector(`#grupo__${field} .formulario__input-error`).classList.remove('formulario__input-error-activo');
        fields[field] = true;
    } else {
        document.getElementById(`grupo__${field}`).classList.add('formulario__grupo-incorrecto');
        document.getElementById(`grupo__${field}`).classList.remove('formulario__grupo-correcto');
        document.querySelector(`#grupo__${field} i`).classList.add('fa-times-circle');
        document.querySelector(`#grupo__${field} i`).classList.remove('fa-check-circle');
        document.querySelector(`#grupo__${field} .formulario__input-error`).classList.add('formulario__input-error-activo');
        fields[field] = false;
    }
}
