/**
 * @author Alberto Martínez
 * @version 06/09/2019
 * expense/_create.blade.php
 */

import {expressions} from "../validations/regex";
import {validateField} from "../validations/regex";
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";
import {setFields} from "../validations/regex";
import {setOnlyField} from "../validations/regex";
import {getFields} from "../validations/regex";

$(document).ready(function() {

    $('#saveExpense').on('click', function () {
        let jobCenter = $('#selectJobCenterAccounting').val();
        document.getElementById("jobCenter").value = jobCenter;
    });

    // Filters
    let initialDateExpense;
    let finalDateExpense;

    $(function () {

        $('input[name="filterDateExpense"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateExpense"]').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateExpense = picker.startDate.format('YYYY-MM-DD');
            finalDateExpense = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateExpense"]').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

    });

    $('#btnFilterExpense').click(function () {
        let idEmployee = $('#selectFilterEmployee option:selected').val();
        let idProvider = $('#selectFilterProvider option:selected').val();
        let description = $('#filterDescription').val();
        let type = $('#selectFilterType option:selected').val();
        let idConcept = $('#selectFilterConcepts option:selected').val();
        let total = $('#filterTotal').val();
        let statusPayment = $('#filterStatusPayment option:selected').val();
        let infoPayment = $('#filterInfoPayment option:selected').val();
        let statusAccount = $('#filterStatusAccount option:selected').val();
        let voucher = $('#selectFilterVouchers option:selected').val();

        initialDateExpense = initialDateExpense == undefined ? null : initialDateExpense;
        finalDateExpense = finalDateExpense == undefined ? null : finalDateExpense;

        let urlBase = "https://pestwareapp.com/accounting/index?";
        let urlBaseDev = "http://127.0.0.1:8000/accounting/index?";
        let url = urlBase + "initialDateExpense=" + initialDateExpense + "&finalDateExpense=" + finalDateExpense +
            "&idEmployee=" + idEmployee + "&idProvider=" + idProvider + "&description=" + description +
            "&type=" + type + "&idConcept=" + idConcept + "&total=" + total + "&statusPayment=" + statusPayment +
            "&infoPayment=" + infoPayment + "&statusAccount=" + statusAccount + "&voucher=" + voucher;

        window.location.href = url;
    });

    // Validate type document and route
    const comprobantArchive = document.getElementById('comprobantArchive');

    comprobantArchive.addEventListener('change', (event) => {
        checkFile(event);
    });

    // Validate form time in real
    const formulario = document.getElementById('formNewExpense');
    const inputs = document.querySelectorAll('#formNewExpense input');

    const fieldsExpenseForm = {
        expense_name: false,
        description_expense: false,
        article: false,
        total: false
    }

    setFields(fieldsExpenseForm);

    const validateForm = (e) => {
        switch (e.target.name) {

            case "expense_name":
                validateField(expressions.description, e.target.value, 'expense_name');
                break;
            case "description_expense":
                validateField(expressions.description, e.target.value, 'description_expense');
                break;
            case "article":
                validateField(expressions.description, e.target.value, 'article');
                break;
            case "total":
                validateField(expressions.totalDecimal, e.target.value, 'total');
                break;
        }
    }

    inputs.forEach((input) => {
        input.addEventListener('keyup', validateForm);
        input.addEventListener('blur', validateForm);
    });

    // Class maked //Event Change conceptExpense
    const conceptExpense = document.querySelector('#grupo__conceptExpense');
    conceptExpense.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__conceptExpense').classList.remove('formulario__grupo-incorrecto');
        }
    });

    //Event Change voucherExpense
    const voucherExpense = document.querySelector('#grupo__voucherExpense');
    voucherExpense.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__voucherExpense').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // New Expense
    $('#formNewExpense').on('submit', function(event) {

        event.preventDefault();

        let expense_name = $('#expense_name').val();
        let description_expense = $('#description_expense').val();
        let paymentWay = $('#typePayment option:selected').val();
        let article = $('#article').val();
        let totalExpense = $('#totalExpense').val();

        validateField(expressions.description, expense_name, 'expense_name');
        validateField(expressions.description, description_expense, 'description_expense');
        validateField(expressions.description, article, 'article');
        validateField(expressions.totalDecimal, totalExpense, 'total');

        const conceptExpense = $("#conceptExpense option:selected").val();
        const voucherExpense = $("#voucherExpense option:selected").val();

        if(conceptExpense == 0){
            document.getElementById('grupo__conceptExpense').classList.add('formulario__grupo-incorrecto');
        }
        if(voucherExpense == 0){
            document.getElementById('grupo__voucherExpense').classList.add('formulario__grupo-incorrecto');
        }

        if(getFields().expense_name && getFields().description_expense && getFields().article
            && getFields().total && conceptExpense != 0 && voucherExpense != 0){

            saveCaptureExpense();
        }
        else {
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }

    });

    function saveCaptureExpense() {
        loaderPestWare('Guardando Gasto...');
        let fd = new FormData(document.getElementById("formNewExpense"));

        $.ajax({
            url: route('add_expense'),
            type: "POST",
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error', 'Error al guardar', 'Algo salio mal, intente de nuevo.');
                } else if (data.code == 201) {
                    formulario.reset();
                    document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');

                    document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
                        icono.classList.remove('formulario__grupo-correcto');
                    });
                    Swal.close();
                    showToast('success-redirect', 'Gasto Guardado', 'Se guardo el gasto correctamente.', 'index_accounting');
                }
            }
        });
    }

    function checkFile(e) {

        const fileList = event.target.files;
        let error = false;
        for (let i = 0, file; file = fileList[i]; i++) {

            let sFileName = file.name;
            let sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            let iFileSize = file.size;
            // let iConvert = (file.size / 1048576).toFixed(2);

            if (!(sFileExtension === "pdf" ||
                    sFileExtension === "doc" ||
                    sFileExtension === "docx" ||
                    sFileExtension === "jpg" ||
                    sFileExtension === "jpeg" ||
                    sFileExtension === "xlsx" ||
                    sFileExtension === "xls" ||
                    sFileExtension === "ppt" ||
                    sFileExtension === "png" ||
                    sFileExtension === "pptx") || iFileSize > 10485760) { /// 10 mb
                error = true;
            }
        }
        if (error) {
            showToast('warning', 'Archivo Invalido', 'El archivo ingresado no es aceptado o su tamaño excede lo aceptado');
            document.getElementById("comprobantArchive").value = "";
        }
    }
});
