/**
 * @author Alberto Martínez
 * @version 06/09/2019
 * entrys/_create.blade.php
 * view _newproduct.blade.php
 */

import {expressions, getFields, setFields, validateField} from "../validations/regex";
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    let urlCurrent = window.location.pathname;
    const inputs = document.querySelectorAll('#formNewProduct input');
    const description = document.getElementById('description');
    const adjetive = document.getElementById('adjetive');
    const productCodeSat = document.getElementById('productCodeSat');
    const productUnitSat = document.getElementById('productUnitSat');
    let token = $("meta[name=csrf-token]").attr("content");

    productCodeSat.onkeyup = function (ev) {
        let keyword = document.getElementById('productCodeSat').value;
        findKeysProductsSat(keyword);
    }

    productUnitSat.onkeyup = function (ev) {
        let keyword = document.getElementById('productUnitSat').value;
        findUnitsProductsSat(keyword);
    }

    function findKeysProductsSat(keyword) {
        if (keyword !== '') {
            $.ajax({
                type: 'GET',
                url: route('catalogs_sat_products_services', keyword),
                data: {
                    _token: token
                },
                success: function(data) {
                    let productCodeSatList = $('#productCodeSatList');
                    productCodeSatList.fadeIn();
                    productCodeSatList.html(data);
                }
            });
        }
    }

    function findUnitsProductsSat(keyword) {
        if (keyword !== '') {
            $.ajax({
                type: 'GET',
                url: route('catalogs_sat_products_units', keyword),
                data: {
                    _token: token
                },
                success: function(data) {
                    let productUnitSatList = $('#productUnitSatList');
                    productUnitSatList.fadeIn();
                    productUnitSatList.html(data);
                }
            });
        }
    }

    $(document).on('click', 'li#getdataProductKey', function() {
        let all = $(this);
        document.getElementById('productCodeSatId').value = all.attr('data-id');
        let productCodeSat = $('#productCodeSat');
        let productCodeSatList =  $('#productCodeSatList');
        productCodeSat.val(all.text());
        productCodeSatList.fadeOut();
    });

    $(document).on('click', 'li#getdataProductUnit', function() {
        let all = $(this);
        document.getElementById('productUnitSatId').value = all.attr('data-id');
        let productUnitSat = $('#productUnitSat');
        let productUnitSatList =  $('#productUnitSatList');
        productUnitSat.val(all.text());
        productUnitSatList.fadeOut();
    });

    const fieldsProductForm = {
        nombre: false,
        description: false,
        adjetive: false,
        uso: false,
        cantidad: false,
        ingredient: false,
        precio: false
    }

    setFields(fieldsProductForm);

    const validateForm = (e) => {
        switch (e.target.name) {

            case "nombre":
                validateField(expressions.description, e.target.value, 'nombre');
                break;
            case "description":
                validateField(expressions.descriptionLarge, e.target.value, 'description');
                break;
            case "adjetive":
                validateField(expressions.descriptionLarge, e.target.value, 'adjetive');
                break;
            case "uso":
                validateField(expressions.description, e.target.value, 'uso');
                break;
            case "cantidad":
                validateField(expressions.totalDecimal, e.target.value, 'cantidad');
                break;
            case "precio":
                validateField(expressions.totalDecimal, e.target.value, 'precio');
                break;
            case "ingredient":
                validateField(expressions.description, e.target.value, 'ingredient');
                break;
        }
    }

    description.addEventListener('keyup', validateForm);
    description.addEventListener('blur', validateForm);

    adjetive.addEventListener('keyup', validateForm);
    adjetive.addEventListener('blur', validateForm);

    inputs.forEach((input) => {
        input.addEventListener('keyup', validateForm);
        input.addEventListener('blur', validateForm);
    });

    // Class maked //Event Change conceptExpense
    const typeProduct = document.querySelector('#grupo__tipo');
    typeProduct.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__tipo').classList.remove('formulario__grupo-incorrecto');
        }
    });
    const displayProduct = document.querySelector('#grupo__presentacion');
    displayProduct.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__presentacion').classList.remove('formulario__grupo-incorrecto');
        }
    });
    const unitProduct = document.querySelector('#grupo__unidad');
    unitProduct.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__unidad').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // Class maked //Event Change plagues
    const taxes = document.querySelector('#grupo__taxes');
    taxes.addEventListener('change', (event) => {
        if (event.target.value.length != 0){
            document.getElementById('grupo__taxes').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // New Product
    $('#formNewProduct').on('submit', function(event) {

        event.preventDefault();

        let typeProduct = $('#tipo option:selected').val();
        let displayProduct = $("#presentacion option:selected").val();
        let unitProduct = $('#unidad option:selected').val();
        let taxes = $('#taxes').val();
        //let unit_code_billing = $('#unit_code_billing option:selected').val();
        //let is_available = document.getElementById('is_available').checked;

        if(typeProduct == 0){
            document.getElementById('grupo__tipo').classList.add('formulario__grupo-incorrecto');
        }
        if(displayProduct == 0){
            document.getElementById('grupo__presentacion').classList.add('formulario__grupo-incorrecto');
        }
        if(unitProduct == 0){
            document.getElementById('grupo__unidad').classList.add('formulario__grupo-incorrecto');
        }
        if(taxes.length == 0){
            showToast('warning','Impuestos','Ingresa un Impuesto');
        }

        //Declarations validations Expenses
        let nameProduct = $('#nombre').val();
        let descriptionProduct = $('#description').val();
        let adjetiveProduct = $("#adjetive").val();
        let useProduct = $('#uso').val();
        let quantityProduct = $("#cantidad").val();
        let prizeProduct = $("#precio").val();
        let ingredientProduct = $("#ingredient").val();

        validateField(expressions.description, nameProduct, 'nombre');
        validateField(expressions.descriptionLarge, descriptionProduct, 'description');
        validateField(expressions.descriptionLarge, adjetiveProduct, 'adjetive');
        validateField(expressions.description, useProduct, 'uso');
        validateField(expressions.totalDecimal, quantityProduct, 'cantidad');
        validateField(expressions.totalDecimal, prizeProduct, 'precio');
        validateField(expressions.description, ingredientProduct, 'ingredient');

        if(getFields().nombre && getFields().description && getFields().adjetive && getFields().uso
            && getFields().cantidad && getFields().precio && getFields().ingredient
            && typeProduct != 0 && displayProduct != 0 && unitProduct != 0 && taxes.length != 0){
            saveCaptureProduct();
        }

        else {
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }
    });

    // Validate type document and route
    const archiveProduct = document.getElementById('information');

    archiveProduct.addEventListener('change', (event) => {
        checkFile(event);
    });

    function saveCaptureProduct() {
        loaderPestWare('Guardando Producto');
        let fd = new FormData(document.getElementById("formNewProduct"));
        $.ajax({
            url: route('save_product'),
            type: "POST",
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Error al guardar','Algo salió mal, intenta de nuevo');
                } else if (data.code == 201) {
                    Swal.close();
                    if(urlCurrent == '/entry/create'){
                        $('#newP').click();
                    } else {
                        showToast('success-redirect','Productos','Guardando Correctamente','index_product');
                        $("#btnSaveProduct").attr('disabled', true);
                    }
                }
            }
        });
    }

    function checkFile(e) {

        const fileList = event.target.files;
        let error = false;
        for (let i = 0, file; file = fileList[i]; i++) {

            let sFileName = file.name;
            let sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            let iFileSize = file.size;
            // let iConvert = (file.size / 1048576).toFixed(2);

            /// OR together the accepted extensions and NOT it. Then OR the size cond.
            /// It's easier to see this way, but just a suggestion - no requirement.
            if (!(sFileExtension === "pdf" ||
                sFileExtension === "jpg" ||
                sFileExtension === "jpeg") || iFileSize > 10485760) { /// 10 mb
                error = true;
            }
        }
        if (error) showToast('error','Archivo Inválido',"El archivo ingresado no es aceptado o su tamaño excede lo aceptado");
    }

});
