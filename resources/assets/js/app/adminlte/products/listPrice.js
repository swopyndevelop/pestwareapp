import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

/**
 * @author Manuel Mendoza
 * @version 17/02/2022
 * products/listPrices.blade.php
 */
$(document).ready(function() {

    let selectJobCenter = $('#selectJobCenterProducts option:selected').val();
    let productsDataBase = [];
    $('#modalListPrice').on('show.bs.modal', function(e) {
       getDataProducts();
    });

    function getDataProducts() {
        loaderPestWare('Cargando...');
        $('#tableListPrices td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('product_list_price', selectJobCenter),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    Swal.close();
                    let rows = '';
                    let table = $('#tableListPrices');
                    const {products, symbol_country} = response;

                    products.forEach((element) => {
                        const {name, base_price, taxes, sale_price, total_tax} = element;
                        productsDataBase.push({
                            name: name,
                            base_price: base_price,
                            taxes: taxes,
                            sale_price: sale_price,
                            total_tax: total_tax,
                        });

                        rows += `<tr>
                                <td class="text-primary text-bold">${name}</td>
                                <td>${symbol_country}${base_price.toFixed(2)}</td>
                                <td>${taxes}</td>
                                <td>${symbol_country}${sale_price.toFixed(2)}</td>
                                <td>${symbol_country}${total_tax.toFixed(2)}</td>
                        </tr>`;
                    });
                    table.append(rows);
                }
            }
        });
    }

    $('#exportListPrice').click(function() {

        loaderPestWare('Exportando listas de Precio...');
        setTimeout(() => {
            Swal.close();
        }, 3000);
        window.location.href = `/products/admin?idJobCenter=${selectJobCenter}&export=true`;

    });

});