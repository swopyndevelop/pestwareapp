import {showToast} from "../pestware/alerts";
import flatpickr from "flatpickr";
import {configPicker, configTimePicker} from "./configDatePickers";

$(document).ready(function() {
    const quotationId = $('#quotationId').val();
    if(window.location.pathname === `/order/services/custom/${quotationId}`){

        $('#typeSchedule').change(function() {
            const paymentMethod = document.getElementById('paymentMethod').value;
            const paymentWay = document.getElementById('paymentWay').value;
            const technician = document.getElementById('techniciansIntelligent').value;
            let option = parseInt($(this).val());
            if (paymentMethod != 0 && paymentWay != 0) optionsModalShow(option);
            else {
                $('#typeSchedule option').prop('selected', function() {
                    return this.defaultSelected;
                });
                showToast('warning', 'Completa los datos.', 'Debes seleccionar las opciones en común.');
            }

        });

        function optionsModalShow(option) {
            const row = document.getElementById('row-intelligent');
            if (option === 1) {
                row.style.display = 'none';
                $('#scheduleIntelligent').modal("show");
            }
            else if (option === 2) row.style.display = 'block';
            else if (option === 3) {
                row.style.display = 'none';
                $('#scheduleImportExcel').modal("show");
            }
        }

        flatpickr("#initialDateIntelligent", configPicker);
        flatpickr("#finalHourIntelligent", configTimePicker);
        flatpickr("#finalHourCustomDay", configTimePicker);

        const checkbox = document.getElementById('monday');
        let text = document.getElementById("excludeDay");

        checkbox.addEventListener('change', (event) => {
            if (event.target.checked) text.style.display = "block";
            else text.style.display = "none";
        });

        $("#scheduleIntelligent").on('hidden.bs.modal', function () {
            location.reload();
        });

        /*fp.config.onChange.push(function (selectedDates, dateStr, instance) {
            console.log(dateStr);
        });*/

    }
});