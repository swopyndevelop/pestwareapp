import flatpickr from "flatpickr";
import {configOnlyPicker, configPicker, configPickerMultiple, configTimePicker, configPickerMultipleCustom} from "./configDatePickers";
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";
import * as moment from "moment";

$(document).ready(function() {
    // Modal Schedule Intelligent
    const quotationId = $('#quotationId').val();
    if(window.location.pathname === `/order/services/custom/${quotationId}`)
    {
        moment.locale('es-mx');
        let dateGlobal;
        let hourGlobal;
        let weekDay;

        let firstDay;
        let secondDay;
        let hourFirstDay;
        let hourSecondDay;
        let firstWeekDay;
        let secondWeekDay;

        let firstDayThird;
        let secondDayThird;
        let thirdDayThird;
        let hourFirstDayThird;
        let hourSecondDayThird;
        let hourThirdDayThird;
        let firstWeekDayThird;
        let secondWeekDayThird;
        let thirdWeekDayThird;

        let firstDayFourth;
        let secondDayFourth;
        let thirdDayFourth;
        let fourthDayFourth;
        let hourFirstDayFourth;
        let hourSecondDayFourth;
        let hourThirdDayFourth;
        let hourFourthDayFourth;
        let firstWeekDayFourth;
        let secondWeekDayFourth;
        let thirdWeekDayFourth;
        let fourthWeekDayFourth;


        $('.scheduleConcept').click(function () {
            let all = $(this);
            createContainerSchedule(all);
        });

        function createOptionsForOnceAMonth(date) {
            return `
                <option value="1" id="optionOne">El día ${date.format('DD')} de cada mes</option>
                <option value="2" id="optionFirstDay">El primer ${date.format('dddd')} de cada mes</option>
                <option value="3" id="optionSecondDay">El segundo ${date.format('dddd')} de cada mes</option>
                <option value="4" id="optionThirdDay">El tercer ${date.format('dddd')} de cada mes</option>
                <option value="5" id="optionLastDay">El último ${date.format('dddd')} de cada mes</option>`;
        }

        function createOptionsForTwiceMonth(firstDay, secondDay) {
            return `
                <option value="6">El día ${firstDay.format('DD')} y ${secondDay.format('DD')} de cada mes</option>
                <option value="7">El primero y segundo ${firstDay.format('dddd')} de cada mes</option>
                <option value="8">El primero y tercer ${firstDay.format('dddd')} de cada mes</option>
                <option value="9">El primero y último ${firstDay.format('dddd')} de cada mes</option>
                <option value="10">El segundo y tercer ${firstDay.format('dddd')} de cada mes</option>
                <option value="11">El segundo y último ${firstDay.format('dddd')} de cada mes</option>
                <option value="12">El tercer y último ${firstDay.format('dddd')} de cada mes</option>
                <option value="0" disabled>---------------------------------------------------</option>
                <option value="13">El primer ${firstDay.format('dddd')} y el segundo ${secondDay.format('dddd')} de cada mes</option>
                <option value="14">El primer ${firstDay.format('dddd')} y el tercer ${secondDay.format('dddd')} de cada mes</option>
                <option value="15">El primer ${firstDay.format('dddd')} y el último ${secondDay.format('dddd')} de cada mes</option>
                <option value="16">El segundo ${firstDay.format('dddd')} y el tercer ${secondDay.format('dddd')} de cada mes</option>
                <option value="17">El segundo ${firstDay.format('dddd')} y el último ${secondDay.format('dddd')} de cada mes</option>
                <option value="18">El tercer ${firstDay.format('dddd')} y el último ${secondDay.format('dddd')} de cada mes</option>`;
        }

        function createOptionsForThirdMonth(firstDay, secondDay, thirdDay) {
            return `
                <option value="19">El día ${firstDay.format('DD')}, ${secondDay.format('DD')} y ${thirdDay.format('DD')} de cada mes</option>
                <option value="20">El primero, segundo y tercer ${firstDay.format('dddd')} de cada mes</option>
                <option value="21">El segundo, tercero y último ${firstDay.format('dddd')} de cada mes</option>
                <option value="22">El primero, segundo y último ${firstDay.format('dddd')} de cada mes</option>
                <option value="23">El primero, tercer y último ${firstDay.format('dddd')} de cada mes</option>
                <option value="0" disabled>---------------------------------------------------</option>
                <option value="24">El primer ${firstDay.format('dddd')}, segundo ${secondDay.format('dddd')} y tercer ${thirdDay.format('dddd')} de cada mes</option>
                <option value="25">El segundo ${firstDay.format('dddd')}, tercero ${secondDay.format('dddd')} y último ${thirdDay.format('dddd')} de cada mes</option>
                <option value="26">El primero ${firstDay.format('dddd')}, segundo ${secondDay.format('dddd')} y último ${thirdDay.format('dddd')} de cada mes</option>
                <option value="27">El primero ${firstDay.format('dddd')}, tercer ${secondDay.format('dddd')} y último ${thirdDay.format('dddd')} de cada mes</option>`;
        }

        function createOptionsForFourthMonth(firstDay, secondDay, thirdDay, fourthDay, isEquals) {
            return `
                <option value="28">El día ${firstDay.format('DD')}, ${secondDay.format('DD')}, ${thirdDay.format('DD')} y ${fourthDay.format('DD')} de cada mes</option>
                ${isEquals ? `<option value="29">Todos los ${firstDay.format('dddd')} de cada mes</option>` : `<option value="29" disabled>---------------------------------------------------</option>
                <option value="30">El primer ${firstDay.format('dddd')}, ${secondDay.format('dddd')}, ${thirdDay.format('dddd')} y ${fourthDay.format('dddd')} de cada mes</option>
                <option value="31">El segundo ${firstDay.format('dddd')}, ${secondDay.format('dddd')}, ${thirdDay.format('dddd')} y ${fourthDay.format('dddd')} de cada mes</option>
                <option value="32">El tercer ${firstDay.format('dddd')}, ${secondDay.format('dddd')}, ${thirdDay.format('dddd')} y ${fourthDay.format('dddd')} de cada mes</option>
                <option value="33">El último ${firstDay.format('dddd')}, ${secondDay.format('dddd')}, ${thirdDay.format('dddd')} y ${fourthDay.format('dddd')} de cada mes</option>
                <option value="0" disabled>---------------------------------------------------</option>
                <option value="34">El primer ${firstDay.format('dddd')}, segundo ${secondDay.format('dddd')}, tercer ${thirdDay.format('dddd')} y último ${fourthDay.format('dddd')} de cada mes</option>`}
                `;
        }

        function createOptionsHourForOnceAMonth(hour) {
            return `
                <option value="1">Todos a las ${hour.format('H:mm')}</option>
                <option value="2">Cada media hora después de las ${hour.format('H:mm')}</option>
                <option value="3">Cada hora después de las ${hour.format('H:mm')}</option>
                <option value="4">Cada 2 horas después de las ${hour.format('H:mm')}</option>
                <option value="5">Cada 3 horas después de las ${hour.format('H:mm')}</option>
                <option value="6">Cada 4 horas después de las ${hour.format('H:mm')}</option>`;
        }

        function createOptionsHourForTwiceAMonth(firstHour, secondHour) {
            return `
                <option value="1">Todos a las ${firstHour.format('H:mm')} y ${secondHour.format('H:mm')}</option>
                <option value="2">Cada media hora después de las ${firstHour.format('H:mm')} y ${secondHour.format('H:mm')}</option>
                <option value="3">Cada hora después de las ${firstHour.format('H:mm')} y ${secondHour.format('H:mm')}</option>
                <option value="4">Cada 2 horas después de las ${firstHour.format('H:mm')} y ${secondHour.format('H:mm')}</option>
                <option value="5">Cada 3 horas después de las ${firstHour.format('H:mm')} y ${secondHour.format('H:mm')}</option>
                <option value="6">Cada 4 horas después de las ${firstHour.format('H:mm')} y ${secondHour.format('H:mm')}</option>`;
        }

        function createOptionsHourForThirdAMonth(firstHour, secondHour, thirdHour) {
            return `
                <option value="1">Todos a las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')} y ${thirdHour.format('H:mm')}</option>
                <option value="2">Cada media hora después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')} y ${thirdHour.format('H:mm')}</option>
                <option value="3">Cada hora después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')} y ${thirdHour.format('H:mm')}</option>
                <option value="4">Cada 2 horas después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')} y ${thirdHour.format('H:mm')}</option>
                <option value="5">Cada 3 horas después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')} y ${thirdHour.format('H:mm')}</option>
                <option value="6">Cada 4 horas después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')} y ${thirdHour.format('H:mm')}</option>`;
        }

        function createOptionsHourForFourthAMonth(firstHour, secondHour, thirdHour, fourthHour) {
            return `
                <option value="1">Todos a las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')}, ${thirdHour.format('H:mm')} y ${fourthHour.format('H:mm')}</option>
                <option value="2">Cada media hora después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')}, ${thirdHour.format('H:mm')} y ${fourthHour.format('H:mm')}</option>
                <option value="3">Cada hora después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')}, ${thirdHour.format('H:mm')} y ${fourthHour.format('H:mm')}</option>
                <option value="4">Cada 2 horas después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')}, ${thirdHour.format('H:mm')} y ${fourthHour.format('H:mm')}</option>
                <option value="5">Cada 3 horas después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')}, ${thirdHour.format('H:mm')} y ${fourthHour.format('H:mm')}</option>
                <option value="6">Cada 4 horas después de las ${firstHour.format('H:mm')}, ${secondHour.format('H:mm')}, ${thirdHour.format('H:mm')} y ${fourthHour.format('H:mm')}</option>`;
        }

        function createOptionsDurationForOnceAMonth() {
            return `
                <option value="30">Con duración de media hora</option>
                <option value="1">Con duración de 1 hora</option>
                <option value="2">Con duración de 2 horas</option>
                <option value="3">Con duración de 3 horas</option>
                <option value="4">Con duración de 4 horas</option>
                <option value="5">Con duración de 5 horas</option>
                <option value="6">Con duración de 6 horas</option>
                <option value="7">Con duración de 7 horas</option>
                <option value="8">Con duración de 8 horas</option>`;
        }

        function createRowContainerSchedule(concept) {
            return `
            <div class="row" id="row-concept">
                <div class="col-md-12">
                    <p class="text-primary"><b>Programación de ${concept}</b></p>
                    <div class="col-md-3">
                        <input class="form-control" type="text" id="dateSchedule" placeholder="Selecciona una fecha">
                    </div>
                    <div class="col-md-3">
                        <select name="optionsDate" id="optionsDate" class="form-control">
                            
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="optionsHour" id="optionsHour" class="form-control">
                            
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="optionsDuration" id="optionsDuration" class="form-control">
                             
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-sm btn-secondary" id="saveScheduleConcept">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-sm btn-danger" id="closeScheduleConcept">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>`;
        }

        function createRowContainerScheduleDaily(concept) {
            return `
            <div class="row" id="row-concept">
                <div class="col-md-12">
                    <p class="text-primary"><b>Programación Diaria de ${concept}</b></p>
                    <div class="col-md-2">
                        <label for="dateSchedule">Fecha Inicial</label>
                        <input class="form-control" type="text" id="dateSchedule" placeholder="Selecciona una fecha">
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <div class="row">
                                <label for="">Personalizar Semana</label>
                                <br>
                                <div class="col-md-2 col-days">
                                    <input type="checkbox" name="mondayCheck" id="mondayCheck">
                                    <label for="monday" class="text-secondary text-bold">Lunes</label><br>
                                    <label for="paymentWay">Hora Inicio</label>
                                    <input class="form-control" type="text" name="initialHourMonday" id="initialHourMonday" placeholder="Seleccione la hora">
                                    <label for="paymentWay">Hora Final</label>
                                    <input class="form-control" type="text" name="finalHourMonday" id="finalHourMonday" placeholder="Seleccione la hora">
                                </div>
                                <div class="col-md-2 col-days">
                                    <input type="checkbox" name="tuesdayCheck" id="tuesdayCheck">
                                    <label for="tuesday" class="text-secondary text-bold">Martes</label><br>
                                    <label for="paymentWay">Hora Inicio</label>
                                    <input class="form-control" type="text" name="initialHourTuesday" id="initialHourTuesday" placeholder="Seleccione la hora">
                                    <label for="paymentWay">Hora Final</label>
                                    <input class="form-control" type="text" name="finalHourTuesday" id="finalHourTuesday" placeholder="Seleccione la hora">
                                </div>
                                <div class="col-md-2 col-days">
                                    <input type="checkbox" name="wednesdayCheck" id="wednesdayCheck">
                                    <label for="wednesday" class="text-secondary text-bold">Miércoles</label><br>
                                    <label for="paymentWay">Hora Inicio</label>
                                    <input class="form-control" type="text" name="initialHourWednesday" id="initialHourWednesday" placeholder="Seleccione la hora">
                                    <label for="paymentWay">Hora Final</label>
                                    <input class="form-control" type="text" name="finalHourWednesday" id="finalHourWednesday" placeholder="Seleccione la hora">
                                </div>
                                <div class="col-md-2 col-days">
                                    <input type="checkbox" name="thursdayCheck" id="thursdayCheck">
                                    <label for="thursday" class="text-secondary text-bold">Jueves</label><br>
                                    <label for="paymentWay">Hora Inicio</label>
                                    <input class="form-control" type="text" name="initialHourThursday" id="initialHourThursday" placeholder="Seleccione la hora">
                                    <label for="paymentWay">Hora Final</label>
                                    <input class="form-control" type="text" name="finalHourThursday" id="finalHourThursday" placeholder="Seleccione la hora">
                                </div>
                                <div class="col-md-2 col-days">
                                    <input type="checkbox" name="fridayCheck" id="fridayCheck">
                                    <label for="friday" class="text-secondary text-bold">Viernes</label><br>
                                    <label for="paymentWay">Hora Inicio</label>
                                    <input class="form-control" type="text" name="initialHourFriday" id="initialHourFriday" placeholder="Seleccione la hora">
                                    <label for="paymentWay">Hora Final</label>
                                    <input class="form-control" type="text" name="finalHourFriday" id="finalHourFriday" placeholder="Seleccione la hora">
                                </div>
                                <div class="col-md-2 col-days">
                                    <input type="checkbox" name="saturdayCheck" id="saturdayCheck">
                                    <label for="saturday" class="text-secondary text-bold">Sabádo</label><br>
                                    <label for="paymentWay">Hora Inicio</label>
                                    <input class="form-control" type="text" name="initialHourSaturday" id="initialHourSaturday" placeholder="Seleccione la hora">
                                    <label for="paymentWay">Hora Final</label>
                                    <input class="form-control" type="text" name="finalHourSaturday" id="finalHourSaturday" placeholder="Seleccione la hora">
                                </div>
                                <div class="col-md-2 col-days">
                                    <input type="checkbox" name="sundayCheck" id="sundayCheck">
                                    <label for="sunday" class="text-secondary text-bold">Domingo</label><br>
                                    <label for="paymentWay">Hora Inicio</label>
                                    <input class="form-control" type="text" name="initialHourSunday" id="initialHourSunday" placeholder="Seleccione la hora">
                                    <label for="paymentWay">Hora Final</label>
                                    <input class="form-control" type="text" name="finalHourSunday" id="finalHourSunday" placeholder="Seleccione la hora">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label for="dateSchedule">Acciones</label><br>
                        <button class="btn btn-sm btn-secondary" id="saveScheduleConcept">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-sm btn-danger" id="closeScheduleConcept">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>`;
        }

        function createRowContainerScheduleCustom(concept) {
            return `
            <div class="row" id="row-concept">
                <div class="col-md-12">
                    <p class="text-primary"><b>Programación Personalizada de ${concept}</b></p>
                    <div class="col-md-2">
                        <label for="durationServiceCustom">Duración (Minutos)</label>
                        <input class="form-control" type="number" id="durationServiceCustom">
                    </div>
                    <div class="col-md-8">
                        <label for="dateSchedule">Fechas de Servicio</label>
                        <input class="form-control" type="text" id="dateScheduleCustom" placeholder="Selecciona las Fechas">
                    </div>
                    <div class="col-md-2">
                        <label for="dateSchedule">Acciones</label><br>
                        <button class="btn btn-sm btn-secondary" id="saveScheduleConcept">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-sm btn-danger" id="closeScheduleConcept">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>`;
        }

        function createPanelSchedule(branches, concept, quantity) {
            return `<div class="panel panel-info">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                            <b>Programación de ${concept} <span class="badge">${quantity} Servicios</span> (vista previa)</b>
                            <button class="btn btn-sm btn-primary-outline pull-right" id="save-schedule-concept-${concept}" style="padding: 2px 10px;">Guardar</button>
                        </div>
                        <div class="panel-body" id="panelSchedules">
                            ${branches}
                        </div>
                    </div>`;
        }

        function createPanelPeriod(title, branches, key) {
            return `<div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                            <b>Período ${key} <span class="badge">${title}</span></b>
                        </div>
                        <div class="panel-body" id="panelSchedules">
                            <table class="table table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th># OS</th>
                                            <th>Concpeto / Sucursal</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                            <th>Técnico</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${branches}
                                    </tbody>
                            </table>
                        </div>
                    </div>`;
        }

        function createDivForPanelSchedule(concept) {
            return `<div id="concept-${concept}"></div>`;
        }

        function eventsRowContainerSchedule(conceptId, customerId, frecuency, concept) {
            $('.scheduleConcept').attr('disabled','disabled');
            const config = frecuency == 1 ? configPicker : configPickerMultiple;
            let fp = flatpickr("#dateSchedule", config);
            fp.config.onChange.push(function (selectedDates, dateStr, instance) {
                if (frecuency == 1) {
                    const date = moment(dateStr);
                    weekDay = date.format('d');
                    dateGlobal = date.format('YYYY-MM-DD');
                    hourGlobal = date.format('H:mm');
                    // Date
                    let optionsDate = $('#optionsDate');
                    optionsDate.children().remove();
                    optionsDate.append(createOptionsForOnceAMonth(date));
                    // Hour
                    let optionsHour = $('#optionsHour');
                    optionsHour.children().remove();
                    optionsHour.append(createOptionsHourForOnceAMonth(date));
                    // Duration
                    let optionsDuration = $('#optionsDuration');
                    optionsDuration.children().remove();
                    optionsDuration.append(createOptionsDurationForOnceAMonth());
                } else if (frecuency == 2) {
                    // start validate selected days
                    let selectedDatesStr = selectedDates.reduce(function(acc, ele) {
                        var str = instance.formatDate(ele, "d/m/Y");
                        acc = (acc == '') ? str : acc + ';' + str;
                        return acc;
                    }, '');
                    instance.set('enable', [function(date) {
                        if (selectedDates.length >= frecuency) {
                            var currDateStr = instance.formatDate(date, "d/m/Y")
                            var x = selectedDatesStr.indexOf(currDateStr);
                            return x != -1;
                        } else {
                            return true;
                        }
                    }]);
                    // end validate selected days
                    const dates = dateStr.split(',');
                    const momFirst = moment(dates[0]);
                    const momSecond = moment(dates[1]);
                    firstDay = momFirst.format('YYYY-MM-DD');
                    secondDay = momSecond.format('YYYY-MM-DD');
                    hourFirstDay = momFirst.format('H:mm');  //moment(dates[0].format('H:mm'));
                    hourSecondDay = momSecond.format('H:mm'); //moment(dates[1].format('H:mm'));
                    firstWeekDay = momFirst.format('d'); //moment(dates[0]).format('d');
                    secondWeekDay = momSecond.format('d'); //moment(dates[1]).format('d');
                    // Date
                    $('#optionsDate').children().remove();
                    $('#optionsDate').append(createOptionsForTwiceMonth(momFirst, momSecond));
                    // Hour
                    $('#optionsHour').children().remove();
                    $('#optionsHour').append(createOptionsHourForTwiceAMonth(momFirst, momSecond));
                    // Duration
                    $('#optionsDuration').children().remove();
                    $('#optionsDuration').append(createOptionsDurationForOnceAMonth());
                } else if (frecuency == 3) {
                    // start validate selected days
                    let selectedDatesStr = selectedDates.reduce(function(acc, ele) {
                        var str = instance.formatDate(ele, "d/m/Y");
                        acc = (acc == '') ? str : acc + ';' + str;
                        return acc;
                    }, '');
                    instance.set('enable', [function(date) {
                        if (selectedDates.length >= frecuency) {
                            var currDateStr = instance.formatDate(date, "d/m/Y")
                            var x = selectedDatesStr.indexOf(currDateStr);
                            return x != -1;
                        } else {
                            return true;
                        }
                    }]);
                    // end validate selected days
                    const dates = dateStr.split(',');
                    const momFirst = moment(dates[0]);
                    const momSecond = moment(dates[1]);
                    const momThird = moment(dates[2]);
                    firstDayThird = momFirst.format('YYYY-MM-DD');
                    secondDayThird = momSecond.format('YYYY-MM-DD');
                    thirdDayThird = momThird.format('YYYY-MM-DD');
                    hourFirstDayThird = momFirst.format('H:mm');
                    hourSecondDayThird = momSecond.format('H:mm');
                    hourThirdDayThird = momThird.format('H:mm');
                    firstWeekDayThird = momFirst.format('d');
                    secondWeekDayThird = momSecond.format('d');
                    thirdWeekDayThird = momThird.format('d');
                    // Date
                    $('#optionsDate').children().remove();
                    $('#optionsDate').append(createOptionsForThirdMonth(momFirst, momSecond, momThird));
                    // Hour
                    $('#optionsHour').children().remove();
                    $('#optionsHour').append(createOptionsHourForThirdAMonth(momFirst, momSecond, momThird));
                    // Duration
                    $('#optionsDuration').children().remove();
                    $('#optionsDuration').append(createOptionsDurationForOnceAMonth());
                } else if (frecuency == 4) {
                    // start validate selected days
                    let selectedDatesStr = selectedDates.reduce(function(acc, ele) {
                        var str = instance.formatDate(ele, "d/m/Y");
                        acc = (acc == '') ? str : acc + ';' + str;
                        return acc;
                    }, '');
                    instance.set('enable', [function(date) {
                        if (selectedDates.length >= frecuency) {
                            var currDateStr = instance.formatDate(date, "d/m/Y")
                            var x = selectedDatesStr.indexOf(currDateStr);
                            return x != -1;
                        } else {
                            return true;
                        }
                    }]);
                    // end validate selected days
                    const dates = dateStr.split(',');
                    const momFirst = moment(dates[0]);
                    const momSecond = moment(dates[1]);
                    const momThird = moment(dates[2]);
                    const momFourth = moment(dates[3]);
                    firstDayFourth = momFirst.format('YYYY-MM-DD');
                    secondDayFourth = momSecond.format('YYYY-MM-DD');
                    thirdDayFourth = momThird.format('YYYY-MM-DD');
                    fourthDayFourth = momFourth.format('YYYY-MM-DD');
                    hourFirstDayFourth = momFirst.format('H:mm');
                    hourSecondDayFourth = momSecond.format('H:mm');
                    hourThirdDayFourth = momThird.format('H:mm');
                    hourFourthDayFourth = momFourth.format('H:mm');
                    firstWeekDayFourth = momFirst.format('d');
                    secondWeekDayFourth = momSecond.format('d');
                    thirdWeekDayFourth = momThird.format('d');
                    fourthWeekDayFourth = momFourth.format('d');
                    // Date
                    let d1 = momFirst.format('dddd');
                    let d2 = momSecond.format('dddd');
                    let d3 = momThird.format('dddd');
                    let d4 = momFourth.format('dddd');
                    let isEquals = false;
                    if (d1 == d2 && d1 == d3 && d1 == d4) isEquals = true;
                    $('#optionsDate').children().remove();
                    $('#optionsDate').append(createOptionsForFourthMonth(momFirst, momSecond, momThird, momFourth, isEquals));
                    // Hour
                    $('#optionsHour').children().remove();
                    $('#optionsHour').append(createOptionsHourForFourthAMonth(momFirst, momSecond, momThird, momFourth));
                    // Duration
                    $('#optionsDuration').children().remove();
                    $('#optionsDuration').append(createOptionsDurationForOnceAMonth());
                }
            });
            $('#closeScheduleConcept').click(function () {
                $('#row-concept').remove();
                $('.scheduleConcept').prop('disabled', false);
            });
            $('#saveScheduleConcept').click(function () {
                createContainerScheduleConcepts(conceptId, customerId, concept, frecuency);
            });
        }

        function eventsRowContainerScheduleDaily(conceptId, customerId, frecuency, concept) {
            $('.scheduleConcept').attr('disabled','disabled');
            flatpickr("#dateSchedule", configOnlyPicker);
            flatpickr("#initialHourMonday", configTimePicker);
            flatpickr("#finalHourMonday", configTimePicker);
            flatpickr("#initialHourTuesday", configTimePicker);
            flatpickr("#finalHourTuesday", configTimePicker);
            flatpickr("#initialHourWednesday", configTimePicker);
            flatpickr("#finalHourWednesday", configTimePicker);
            flatpickr("#initialHourThursday", configTimePicker);
            flatpickr("#finalHourThursday", configTimePicker);
            flatpickr("#initialHourFriday", configTimePicker);
            flatpickr("#finalHourFriday", configTimePicker);
            flatpickr("#initialHourSaturday", configTimePicker);
            flatpickr("#finalHourSaturday", configTimePicker);
            flatpickr("#initialHourSunday", configTimePicker);
            flatpickr("#finalHourSunday", configTimePicker);
            $('#closeScheduleConcept').click(function () {
                $('#row-concept').remove();
                $('.scheduleConcept').prop('disabled', false);
            });
            $('#saveScheduleConcept').click(function () {
                createContainerScheduleConceptsDaily(conceptId, customerId, concept, frecuency, false);
            });
        }

        function eventsRowContainerScheduleCustom(conceptId, customerId, concept) {
            $('.scheduleConcept').attr('disabled','disabled');
            $('#closeScheduleConcept').click(function () {
                $('#row-concept').remove();
                $('.scheduleConcept').prop('disabled', false);
            });
            $('#saveScheduleConcept').click(function () {
                createContainerScheduleConceptsCustom(conceptId, customerId, concept, false);
            });
        }

        function createContainerScheduleConcepts(conceptId, customerId, concept, frecuency) {
            loaderPestWare('Calculando propuesta inteligente...');
            //if (weekDay === -1) weekDay = 6;
            //if (weekDay === 0) weekDay = 7;
            let optionDate = $('#optionsDate').val();
            let optionHour = $('#optionsHour').val();
            let optionsDuration = $('#optionsDuration').val();
            let conceptSlug = slugify(concept);
            if (frecuency == 1) {
                $.ajax({
                    type: 'POST',
                    url: route('schedule_intelligent_first_day'),
                    data:{
                        _token: $("meta[name=csrf-token]").attr("content"),
                        conceptId: conceptId,
                        customerId: customerId,
                        date: dateGlobal,
                        hour: hourGlobal,
                        weekDay: weekDay,
                        optionDate: optionDate,
                        optionHour: optionHour,
                        optionsDuration: optionsDuration,
                        save: false
                    },
                    success: function (response) {
                        if (response.code === 500){
                            Swal.close();
                            missingText('Algo salio mal');
                        }
                        else{
                            Swal.close();
                            let branches = '';
                            let quantity = response.length;
                            let month = response[0].month;
                            response.forEach((element) => {
                                if (element.month !== month) {
                                    branches += `<tr style="background-color: #F8F9F9">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>`;
                                    month = element.month;
                                }
                                branches += `<tr>
                                            <td>${element.folio}</td>
                                            <td>${element.name}</td>
                                            <td>${element.date}</td>
                                            <td>${element.hour}</td>
                                        </tr>`;
                            });
                            let containerConcepts = `
                        <div class="row" id="row-concept">
                            <div class="col-md-12">
                                <table class="table table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th># OS</th>
                                            <th>Concepto / Sucursal</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${branches}
                                    </tbody>
                                </table>
                            </div>
                        </div>`;
                            let panel = createPanelSchedule(containerConcepts, conceptSlug, quantity);
                            $(`#concept-${conceptSlug}`).children().remove();
                            $(`#concept-${conceptSlug}`).append(panel);
                            $(`#save-schedule-concept-${conceptSlug}`).click(function () {
                                // Todo: Add show dialog confirm
                                storeSchedulesConcept(conceptId, customerId, conceptSlug, frecuency, optionDate);
                            });
                        }
                    }
                });
            } else if (frecuency == 2) {
                $.ajax({
                    type: 'POST',
                    url: route('schedule_intelligent_second_day'),
                    data:{
                        _token: $("meta[name=csrf-token]").attr("content"),
                        conceptId: conceptId,
                        customerId: customerId,
                        firstDay: firstDay,
                        secondDay: secondDay,
                        firstWeekDay: firstWeekDay,
                        secondWeekDay: secondWeekDay,
                        hourFirstDay: hourFirstDay,
                        hourSecondDay: hourSecondDay,
                        optionDate: optionDate,
                        optionHour: optionHour,
                        optionsDuration: optionsDuration,
                        save: false
                    },
                    success: function (response) {
                        if (response.code === 500){
                            Swal.close();
                            missingText('Algo salio mal');
                        }
                        else{
                            Swal.close();
                            let branches = '';
                            let quantity = response.length;
                            let month = response[0].month;
                            response.forEach((element) => {
                                if (element.month !== month) {
                                    branches += `<tr style="background-color: #F8F9F9">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>`;
                                    month = element.month;
                                }
                                branches += `<tr>
                                            <td>${element.folio}</td>
                                            <td>${element.name}</td>
                                            <td>${element.date}</td>
                                            <td>${element.hour}</td>
                                        </tr>`;
                            });
                            let containerConcepts = `
                        <div class="row" id="row-concept">
                            <div class="col-md-12">
                                <table class="table table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th># OS</th>
                                            <th>Concepto / Sucursal</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${branches}
                                    </tbody>
                                </table>
                            </div>
                        </div>`;
                            let panel = createPanelSchedule(containerConcepts, conceptSlug, quantity);
                            $(`#concept-${conceptSlug}`).children().remove();
                            $(`#concept-${conceptSlug}`).append(panel);
                            $(`#save-schedule-concept-${conceptSlug}`).click(function () {
                                // Todo: Add show dialog confirm
                                storeSchedulesConcept(conceptId, customerId, conceptSlug, frecuency, optionDate);
                            });
                        }
                    }
                });
            } else if (frecuency == 3) {
                $.ajax({
                    type: 'POST',
                    url: route('schedule_intelligent_third_day'),
                    data:{
                        _token: $("meta[name=csrf-token]").attr("content"),
                        conceptId: conceptId,
                        customerId: customerId,
                        firstDay: firstDayThird,
                        secondDay: secondDayThird,
                        thirdDay: thirdDayThird,
                        firstWeekDay: firstWeekDayThird,
                        secondWeekDay: secondWeekDayThird,
                        thirdWeekDay: thirdWeekDayThird,
                        hourFirstDayThird: hourFirstDayThird,
                        hourSecondDayThird: hourSecondDayThird,
                        hourThirdDayThird: hourThirdDayThird,
                        optionDate: optionDate,
                        optionHour: optionHour,
                        optionsDuration: optionsDuration,
                        save: false
                    },
                    success: function (response) {
                        if (response.code === 500){
                            Swal.close();
                            missingText('Algo salio mal');
                        }
                        else{
                            Swal.close();
                            let branches = '';
                            let quantity = response.length;
                            let month = response[0].month;
                            response.forEach((element) => {
                                if (element.month !== month) {
                                    branches += `<tr style="background-color: #F8F9F9">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>`;
                                    month = element.month;
                                }
                                branches += `<tr>
                                            <td>${element.folio}</td>
                                            <td>${element.name}</td>
                                            <td>${element.date}</td>
                                            <td>${element.hour}</td>
                                        </tr>`;
                            });
                            let containerConcepts = `
                        <div class="row" id="row-concept">
                            <div class="col-md-12">
                                <table class="table table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th># OS</th>
                                            <th>Concepto / Sucursal</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${branches}
                                    </tbody>
                                </table>
                            </div>
                        </div>`;
                            let panel = createPanelSchedule(containerConcepts, conceptSlug, quantity);
                            $(`#concept-${conceptSlug}`).children().remove();
                            $(`#concept-${conceptSlug}`).append(panel);
                            $(`#save-schedule-concept-${conceptSlug}`).click(function () {
                                // Todo: Add show dialog confirm
                                storeSchedulesConcept(conceptId, customerId, conceptSlug, frecuency, optionDate);
                            });
                        }
                    }
                });
            } else if (frecuency == 4) {
                $.ajax({
                    type: 'POST',
                    url: route('schedule_intelligent_fourth_day'),
                    data:{
                        _token: $("meta[name=csrf-token]").attr("content"),
                        conceptId: conceptId,
                        customerId: customerId,
                        firstDay: firstDayFourth,
                        secondDay: secondDayFourth,
                        thirdDay: thirdDayFourth,
                        fourthDay: fourthDayFourth,
                        firstWeekDay: firstWeekDayFourth,
                        secondWeekDay: secondWeekDayFourth,
                        thirdWeekDay: thirdWeekDayFourth,
                        fourthWeekDay: fourthWeekDayFourth,
                        hourFirstDayFourth: hourFirstDayFourth,
                        hourSecondDayFourth: hourSecondDayFourth,
                        hourThirdDayFourth: hourThirdDayFourth,
                        hourFourthDayFourth: hourFourthDayFourth,
                        optionDate: optionDate,
                        optionHour: optionHour,
                        optionsDuration: optionsDuration,
                        save: false
                    },
                    success: function (response) {
                        if (response.code === 500){
                            Swal.close();
                            missingText('Algo salio mal');
                        }
                        else{
                            Swal.close();
                            let branches = '';
                            let quantity = response.length;
                            let month = response[0].month;
                            response.forEach((element) => {
                                if (element.month !== month) {
                                    branches += `<tr style="background-color: #F8F9F9">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>`;
                                    month = element.month;
                                }
                                branches += `<tr>
                                            <td>${element.folio}</td>
                                            <td>${element.name}</td>
                                            <td>${element.date}</td>
                                            <td>${element.hour}</td>
                                        </tr>`;
                            });
                            let containerConcepts = `
                        <div class="row" id="row-concept">
                            <div class="col-md-12">
                                <table class="table table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th># OS</th>
                                            <th>Concepto / Sucursal</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${branches}
                                    </tbody>
                                </table>
                            </div>
                        </div>`;
                            let panel = createPanelSchedule(containerConcepts, conceptSlug, quantity);
                            $(`#concept-${conceptSlug}`).children().remove();
                            $(`#concept-${conceptSlug}`).append(panel);
                            $(`#save-schedule-concept-${conceptSlug}`).click(function () {
                                // Todo: Add show dialog confirm
                                storeSchedulesConcept(conceptId, customerId, conceptSlug, frecuency, optionDate);
                            });
                        }
                    }
                });
            }
        }

        function createContainerScheduleConceptsDaily(conceptId, customerId, concept, frecuency, isSave) {
            loaderPestWare('Calculando propuesta inteligente...');
            let conceptSlug = slugify(concept);
            let date = document.getElementById('dateSchedule').value;
            let initialHourMonday = document.getElementById('initialHourMonday').value;
            let finalHourMonday = document.getElementById('finalHourMonday').value;
            let initialHourTuesday = document.getElementById('initialHourTuesday').value;
            let finalHourTuesday = document.getElementById('finalHourTuesday').value;
            let initialHourWednesday = document.getElementById('initialHourWednesday').value;
            let finalHourWednesday = document.getElementById('finalHourWednesday').value;
            let initialHourThursday = document.getElementById('initialHourThursday').value;
            let finalHourThursday = document.getElementById('finalHourThursday').value;
            let initialHourFriday = document.getElementById('initialHourFriday').value;
            let finalHourFriday = document.getElementById('finalHourFriday').value;
            let initialHourSaturday = document.getElementById('initialHourSaturday').value;
            let finalHourSaturday = document.getElementById('finalHourSaturday').value;
            let initialHourSunday = document.getElementById('initialHourSunday').value;
            let finalHourSunday = document.getElementById('finalHourSunday').value;
            let checkboxMonday = document.getElementById('mondayCheck');
            let checkboxTuesday = document.getElementById('tuesdayCheck');
            let checkboxWednesday = document.getElementById('wednesdayCheck');
            let checkboxThursday = document.getElementById('thursdayCheck');
            let checkboxFriday = document.getElementById('fridayCheck');
            let checkboxSaturday = document.getElementById('saturdayCheck');
            let checkboxSunday = document.getElementById('sundayCheck');
            const paymentMethod = document.getElementById('paymentMethod').value;
            const paymentWay = document.getElementById('paymentWay').value;
            const technician = document.getElementById('techniciansDate').value;
            let techniciansAuxiliaries = $('#techniciansAuxiliariesCustom').val();
            let commentsTechnicians = $('#commentsTechnicians').val();

            $.ajax({
                type: 'POST',
                url: route('schedule_intelligent_daily_services'),
                data:{
                    _token: $("meta[name=csrf-token]").attr("content"),
                    conceptId: conceptId,
                    customerId: customerId,
                    paymentMethod: paymentMethod,
                    paymentWay: paymentWay,
                    technician: technician,
                    date: date,
                    save: isSave,
                    checkboxMonday: checkboxMonday.checked,
                    checkboxTuesday: checkboxTuesday.checked,
                    checkboxWednesday: checkboxWednesday.checked,
                    checkboxThursday: checkboxThursday.checked,
                    checkboxFriday: checkboxFriday.checked,
                    checkboxSaturday: checkboxSaturday.checked,
                    checkboxSunday: checkboxSunday.checked,
                    initialHourMonday: initialHourMonday,
                    finalHourMonday: finalHourMonday,
                    initialHourTuesday: initialHourTuesday,
                    finalHourTuesday: finalHourTuesday,
                    initialHourWednesday: initialHourWednesday,
                    finalHourWednesday: finalHourWednesday,
                    initialHourThursday: initialHourThursday,
                    finalHourThursday: finalHourThursday,
                    initialHourFriday: initialHourFriday,
                    finalHourFriday: finalHourFriday,
                    initialHourSaturday: initialHourSaturday,
                    finalHourSaturday: finalHourSaturday,
                    initialHourSunday: initialHourSunday,
                    finalHourSunday: finalHourSunday,
                    techniciansAuxiliaries: techniciansAuxiliaries,
                    commentsTechnicians: commentsTechnicians
                },
                success: function (response) {
                    if (isSave) {
                        if (response.code === 500){
                            Swal.close();
                            showToast('error', 'Error al programar los servicios', 'Algo salio mal, intenta de nuevo.');
                        }
                        else{
                            Swal.close();
                            const label = `<span class="label label-primary pull-right" style="padding: 7px 10px;"><i class="fa fa-check-circle" aria-hidden="true"></i> Servicios Programados</span>`;
                            const labelTable = `<i class="fa fa-check-circle" style="color: #55b7a4" aria-hidden="true"></i>`;
                            $(`#save-schedule-concept-${conceptSlug}`).replaceWith(label);
                            $(`#scheduleConcept-${conceptSlug}`).replaceWith(labelTable);
                            showToast('success', response.message, 'Se programaron los servicios.');
                            $("#row-config").attr('disabled','disabled');
                            $('#row-concept').remove();
                            $('.scheduleConcept').prop('disabled', false);
                        }
                    } else {
                        if (response.code === 500){
                            Swal.close();
                            missingText('Algo salio mal');
                        }
                        else{
                            Swal.close();
                            let branches = '';
                            let quantity = response.length;
                            let month = response[0].month;
                            response.forEach((element) => {
                                if (element.month !== month) {
                                    branches += `<tr style="background-color: #F8F9F9">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>`;
                                    month = element.month;
                                }
                                branches += `<tr>
                                            <td>${element.folio}</td>
                                            <td>${element.name}</td>
                                            <td>${element.date}</td>
                                            <td>${element.hour}</td>
                                        </tr>`;
                            });
                            let containerConcepts = `
                        <div class="row" id="row-concept">
                            <div class="col-md-12">
                                <table class="table table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th># OS</th>
                                            <th>Concepto / Sucursal</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${branches}
                                    </tbody>
                                </table>
                            </div>
                        </div>`;
                            let panel = createPanelSchedule(containerConcepts, conceptSlug, quantity);
                            $(`#concept-${conceptSlug}`).children().remove();
                            $(`#concept-${conceptSlug}`).append(panel);
                            $(`#save-schedule-concept-${conceptSlug}`).click(function () {
                                // Todo: Add show dialog confirm
                                createContainerScheduleConceptsDaily(conceptId, customerId, concept, frecuency, true);
                            });
                        }
                    }
                }
            });

        }

        function createContainerScheduleConceptsCustom(conceptId, customerId, concept, isSave) {
            loaderPestWare('Calculando propuesta inteligente...');
            let conceptSlug = slugify(concept);
            let date = document.getElementById('dateScheduleCustom').value;
            let durationServiceCustom = document.getElementById('durationServiceCustom').value;
            let paymentMethod = document.getElementById('paymentMethod').value;
            let paymentWay = document.getElementById('paymentWay').value;
            let technician = document.getElementById('techniciansDate').value;
            let techniciansAuxiliaries = $('#techniciansAuxiliariesCustom').val();
            let commentsTechnicians = $('#commentsTechnicians').val();

            $.ajax({
                type: 'POST',
                url: route('schedule_intelligent_custom_services'),
                data:{
                    _token: $("meta[name=csrf-token]").attr("content"),
                    conceptId: conceptId,
                    customerId: customerId,
                    paymentMethod: paymentMethod,
                    paymentWay: paymentWay,
                    technician: technician,
                    date: date,
                    save: isSave,
                    durationServiceCustom: durationServiceCustom,
                    techniciansAuxiliaries: techniciansAuxiliaries,
                    commentsTechnicians: commentsTechnicians
                },
                success: function (response) {
                    if (isSave) {
                        if (response.code === 500){
                            Swal.close();
                            showToast('error', 'Error al programar los servicios', 'Algo salio mal, intenta de nuevo.');
                        }
                        else{
                            Swal.close();
                            const label = `<span class="label label-primary pull-right" style="padding: 7px 10px;"><i class="fa fa-check-circle" aria-hidden="true"></i> Servicios Programados</span>`;
                            const labelTable = `<i class="fa fa-check-circle" style="color: #55b7a4" aria-hidden="true"></i>`;
                            $(`#save-schedule-concept-${conceptSlug}`).replaceWith(label);
                            $(`#scheduleConcept-${conceptSlug}`).replaceWith(labelTable);
                            showToast('success', response.message, 'Se programaron los servicios.');
                            $("#row-config").attr('disabled','disabled');
                            $('#row-concept').remove();
                            $('.scheduleConcept').prop('disabled', false);
                        }
                    } else {
                        if (response.code === 500){
                            Swal.close();
                            missingText('Algo salio mal');
                        }
                        else{
                            Swal.close();
                            let branches = '';
                            let quantity = response.length;
                            let month = response[0].month;
                            response.forEach((element) => {
                                if (element.month !== month) {
                                    branches += `<tr style="background-color: #F8F9F9">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>`;
                                    month = element.month;
                                }
                                branches += `<tr>
                                            <td>${element.folio}</td>
                                            <td>${element.name}</td>
                                            <td>${element.date}</td>
                                            <td>${element.hour}</td>
                                        </tr>`;
                            });
                            let containerConcepts = `
                        <div class="row" id="row-concept">
                            <div class="col-md-12">
                                <table class="table table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th># OS</th>
                                            <th>Concepto / Sucursal</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${branches}
                                    </tbody>
                                </table>
                            </div>
                        </div>`;
                            let panel = createPanelSchedule(containerConcepts, conceptSlug, quantity);
                            $(`#concept-${conceptSlug}`).children().remove();
                            $(`#concept-${conceptSlug}`).append(panel);
                            $(`#save-schedule-concept-${conceptSlug}`).click(function () {
                                // Todo: Add show dialog confirm
                                createContainerScheduleConceptsCustom(conceptId, customerId, concept, true);
                            });
                        }
                    }
                }
            });

        }

        function slugify(text)
        {
            return text.toString().toLowerCase()
                .replace(/\s+/g, '-')           // Replace spaces with -
                .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                .replace(/^-+/, '')             // Trim - from start of text
                .replace(/-+$/, '');            // Trim - from end of text
        }

        function createContainerSchedule(data) {
            let conceptId = data.attr('data-id');
            let concept = data.attr('data-concept');
            let conceptSlug = slugify(concept);
            let customerId = data.attr('data-customerId');
            let frecuency = data.attr('data-frecuency');
            let typeService = data.attr('data-typeService');
            if (typeService == 6) $('#container-schedule').append(createRowContainerScheduleDaily(concept));
            if (typeService == 8) {
                $('#container-schedule').append(createRowContainerScheduleCustom(concept));
                $("#dateScheduleCustom").flatpickr(configPickerMultiple);
                eventsRowContainerScheduleCustom(conceptId, customerId, concept);
            }
            else $('#container-schedule').append(createRowContainerSchedule(concept));
            if (!document.getElementById(`concept-${conceptSlug}`)) {
                $('#container-schedule-concepts').append(createDivForPanelSchedule(conceptSlug));
            }
            if (typeService == 6) eventsRowContainerScheduleDaily(conceptId, customerId, frecuency, concept);
            else eventsRowContainerSchedule(conceptId, customerId, frecuency, concept);

        }

        function storeSchedulesConcept(conceptId, customerId, concept, frecuency, optionDate) {
            loaderPestWare('Programando Ordenes de Servicio');

            const paymentMethod = document.getElementById('paymentMethod').value;
            const paymentWay = document.getElementById('paymentWay').value;
            const technician = document.getElementById('techniciansDate').value;
            let techniciansAuxiliaries = $('#techniciansAuxiliariesCustom').val();
            let commentsTechnicians = $('#commentsTechnicians').val();

            let optionHour = $('#optionsHour').val();
            let optionsDuration = $('#optionsDuration').val();

            let conceptSlug = slugify(concept);

            if (frecuency == 1) {
                $.ajax({
                    type: 'POST',
                    url: route('schedule_intelligent_first_day'),
                    data:{
                        _token: $("meta[name=csrf-token]").attr("content"),
                        conceptId: conceptId,
                        customerId: customerId,
                        date: dateGlobal,
                        hour: hourGlobal,
                        weekDay: weekDay,
                        optionDate: optionDate,
                        paymentMethod: paymentMethod,
                        paymentWay: paymentWay,
                        technician: technician,
                        optionHour: optionHour,
                        optionsDuration: optionsDuration,
                        techniciansAuxiliaries: techniciansAuxiliaries,
                        commentsTechnicians: commentsTechnicians,
                        save: true,
                    },
                    success: function (response) {
                        if (response.code === 500){
                            Swal.close();
                            showToast('error', 'Error al programar los servicios', 'Algo salio mal, intenta de nuevo.');
                        }
                        else{
                            Swal.close();
                            const label = `<span class="label label-primary pull-right" style="padding: 7px 10px;"><i class="fa fa-check-circle" aria-hidden="true"></i> Servicios Programados</span>`;
                            const labelTable = `<i class="fa fa-check-circle" style="color: #55b7a4" aria-hidden="true"></i>`;
                            $(`#save-schedule-concept-${conceptSlug}`).replaceWith(label);
                            $(`#scheduleConcept-${conceptSlug}`).replaceWith(labelTable);
                            showToast('success', response.message, 'Se programaron los servicios.');
                            $("#row-config").attr('disabled','disabled');
                            $('#row-concept').remove();
                            $('.scheduleConcept').prop('disabled', false);
                        }
                    }
                });
            } else if (frecuency == 2) {
                $.ajax({
                    type: 'POST',
                    url: route('schedule_intelligent_second_day'),
                    data:{
                        _token: $("meta[name=csrf-token]").attr("content"),
                        conceptId: conceptId,
                        customerId: customerId,
                        firstDay: firstDay,
                        secondDay: secondDay,
                        firstWeekDay: firstWeekDay,
                        secondWeekDay: secondWeekDay,
                        hourFirstDay: hourFirstDay,
                        hourSecondDay: hourSecondDay,
                        optionDate: optionDate,
                        optionHour: optionHour,
                        optionsDuration: optionsDuration,
                        paymentMethod: paymentMethod,
                        paymentWay: paymentWay,
                        technician: technician,
                        techniciansAuxiliaries: techniciansAuxiliaries,
                        commentsTechnicians: commentsTechnicians,
                        save: true
                    },
                    success: function (response) {
                        if (response.code === 500){
                            Swal.close();
                            showToast('error', 'Error al programar los servicios', 'Algo salio mal, intenta de nuevo.');
                        }
                        else{
                            Swal.close();
                            const label = `<span class="label label-primary pull-right" style="padding: 7px 10px;"><i class="fa fa-check-circle" aria-hidden="true"></i> Servicios Programados</span>`;
                            const labelTable = `<i class="fa fa-check-circle" style="color: #55b7a4" aria-hidden="true"></i>`;
                            $(`#save-schedule-concept-${conceptSlug}`).replaceWith(label);
                            $(`#scheduleConcept-${conceptSlug}`).replaceWith(labelTable);
                            showToast('success', response.message, 'Se programaron los servicios.');
                            $("#row-config").attr('disabled','disabled');
                            $('#row-concept').remove();
                            $('.scheduleConcept').prop('disabled', false);
                        }
                    }
                });
            } else if (frecuency == 3) {
                $.ajax({
                    type: 'POST',
                    url: route('schedule_intelligent_third_day'),
                    data:{
                        _token: $("meta[name=csrf-token]").attr("content"),
                        conceptId: conceptId,
                        customerId: customerId,
                        firstDay: firstDayThird,
                        secondDay: secondDayThird,
                        thirdDay: thirdDayThird,
                        firstWeekDay: firstWeekDayThird,
                        secondWeekDay: secondWeekDayThird,
                        thirdWeekDay: thirdWeekDayThird,
                        hourFirstDayThird: hourFirstDayThird,
                        hourSecondDayThird: hourSecondDayThird,
                        hourThirdDayThird: hourThirdDayThird,
                        optionDate: optionDate,
                        optionHour: optionHour,
                        optionsDuration: optionsDuration,
                        paymentMethod: paymentMethod,
                        paymentWay: paymentWay,
                        technician: technician,
                        techniciansAuxiliaries: techniciansAuxiliaries,
                        commentsTechnicians: commentsTechnicians,
                        save: true
                    },
                    success: function (response) {
                        if (response.code === 500){
                            Swal.close();
                            showToast('error', 'Error al programar los servicios', 'Algo salio mal, intenta de nuevo.');
                        }
                        else{
                            Swal.close();
                            const label = `<span class="label label-primary pull-right" style="padding: 7px 10px;"><i class="fa fa-check-circle" aria-hidden="true"></i> Servicios Programados</span>`;
                            const labelTable = `<i class="fa fa-check-circle" style="color: #55b7a4" aria-hidden="true"></i>`;
                            $(`#save-schedule-concept-${conceptSlug}`).replaceWith(label);
                            $(`#scheduleConcept-${conceptSlug}`).replaceWith(labelTable);
                            showToast('success', response.message, 'Se programaron los servicios.');
                            $("#row-config").attr('disabled','disabled');
                            $('#row-concept').remove();
                            $('.scheduleConcept').prop('disabled', false);
                        }
                    }
                });
            } else if (frecuency == 4) {
                $.ajax({
                    type: 'POST',
                    url: route('schedule_intelligent_fourth_day'),
                    data:{
                        _token: $("meta[name=csrf-token]").attr("content"),
                        conceptId: conceptId,
                        customerId: customerId,
                        firstDay: firstDayFourth,
                        secondDay: secondDayFourth,
                        thirdDay: thirdDayFourth,
                        fourthDay: fourthDayFourth,
                        firstWeekDay: firstWeekDayFourth,
                        secondWeekDay: secondWeekDayFourth,
                        thirdWeekDay: thirdWeekDayFourth,
                        fourthWeekDay: fourthWeekDayFourth,
                        hourFirstDayFourth: hourFirstDayFourth,
                        hourSecondDayFourth: hourSecondDayFourth,
                        hourThirdDayFourth: hourThirdDayFourth,
                        hourFourthDayFourth: hourFourthDayFourth,
                        optionDate: optionDate,
                        optionHour: optionHour,
                        optionsDuration: optionsDuration,
                        paymentMethod: paymentMethod,
                        paymentWay: paymentWay,
                        technician: technician,
                        techniciansAuxiliaries: techniciansAuxiliaries,
                        commentsTechnicians: commentsTechnicians,
                        save: true
                    },
                    success: function (response) {
                        if (response.code === 500){
                            Swal.close();
                            showToast('error', 'Error al programar los servicios', 'Algo salio mal, intenta de nuevo.');
                        }
                        else{
                            Swal.close();
                            const label = `<span class="label label-primary pull-right" style="padding: 7px 10px;"><i class="fa fa-check-circle" aria-hidden="true"></i> Servicios Programados</span>`;
                            const labelTable = `<i class="fa fa-check-circle" style="color: #55b7a4" aria-hidden="true"></i>`;
                            $(`#save-schedule-concept-${conceptSlug}`).replaceWith(label);
                            $(`#scheduleConcept-${conceptSlug}`).replaceWith(labelTable);
                            showToast('success', response.message, 'Se programaron los servicios.');
                            $("#row-config").attr('disabled','disabled');
                            $('#row-concept').remove();
                            $('.scheduleConcept').prop('disabled', false);
                        }
                    }
                });
            }
        }
    }
});