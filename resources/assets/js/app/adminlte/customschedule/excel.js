import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function() {
    const quotationId = $('#quotationId').val();
    if(window.location.pathname === `/order/services/custom/${quotationId}`){

        $('#downloadFileBranches').click(function () {

            let customerId = $('#customerId').val();
            let quotationId = $('#quotationId').val();
            let dateServices = $('#dateServiceManual').val();
            let hourServices = $('#hourServiceManual').val();
            let technician = $('#techniciansExcel option:selected').val();
            let paymentMethod = $('#paymentMethod option:selected').val();
            let paymentWay = $('#paymentWay option:selected').val();

            const query = {
                _token: $("meta[name=csrf-token]").attr("content"),
                id_quotation: quotationId,
                customerId: customerId,
                date: dateServices,
                hour: hourServices,
                paymentMethod: paymentMethod,
                paymentWay: paymentWay,
                technician: technician
            }

            window.location = '../../../schedule/excel/template/export?' + $.param(query);
        })

        $('#importFileSchedules').on('click', function () {
            let file = $('#fileImportSchedules').val();
            if (file)
                loaderPestWare('Importando servicios...');
            else showToast('warning', 'Archivo vacío', 'Debes seleccionar un archivo de excel.');
        });

    }
});