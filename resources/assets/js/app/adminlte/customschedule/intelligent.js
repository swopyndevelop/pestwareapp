const {loaderPestWare} = require("../pestware/loader");
$(document).ready(function() {

    $('#scheduleIntelligentNew').click(function () {
        let quotationId = $('#quotationId').val();
        let completeDate = document.getElementById('initialDateIntelligent').value;
        let finalHour = document.getElementById('finalHourIntelligent').value;
        let duration = document.getElementById('minutes').value;
        let technicians = $('#techniciansIntelligent').val();
        let paymentMethod = document.getElementById('paymentMethod').value;
        let paymentWay = document.getElementById('paymentWay').value;
        completeDate = completeDate.split(' ');
        let week = {
            '1': document.getElementById('monday').checked,
            '2': document.getElementById('tuesday').checked,
            '3': document.getElementById('wednesday').checked,
            '4': document.getElementById('thursday').checked,
            '5': document.getElementById('friday').checked,
            '6': document.getElementById('saturday').checked,
            '0': document.getElementById('sunday').checked
        };
        let message = 'Calculando propuesta inteligente... Por favor espere un momento. (Este proceso suele tardar más de lo común.)';
        getProposalIntelligent(quotationId, completeDate[0], completeDate[1], finalHour, duration, technicians, week, paymentMethod, paymentWay, false, message);
    });

    function createPanelPeriod(title, branches, key) {
        return `<div class="panel panel-info">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                            <b>Período ${key} <span class="badge">${title}</span></b>
                        </div>
                        <div class="panel-body" id="panelSchedules">
                            ${branches}
                        </div>
                    </div>`;
    }

    function getProposalIntelligent(quotationId, initialDate, initialHour, finalHour, duration, technicians, week, paymentMethod, paymentWay, save, message) {
        loaderPestWare(message);

        /*let data = new FormData();
        data.append('quotationId', quotationId);
        data.append('initialDate', initialDate);
        data.append('initialHour', initialHour);
        data.append('finalHour', finalHour);
        data.append('duration', duration);
        data.append('technicians', technicians);
        data.append('week', week);

        let oReq = new XMLHttpRequest();
        let url = `/schedule/smart/proposal?quotationId=${quotationId}&initialDate=${initialDate}&initialHour=${initialHour}&finalHour=${finalHour}&duration=${duration}&technicians=${technicians}&week=${JSON.stringify(week)}`;

        oReq.addEventListener("progress", updateProgress);
        oReq.addEventListener("load", transferComplete);
        oReq.addEventListener("error", transferFailed);
        oReq.addEventListener("abort", transferCanceled);

        oReq.open('GET', url, true);
        oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        oReq.send();

        // progress on transfers from the server to the client (downloads)
        function updateProgress (oEvent) {
            console.log(oEvent);
            if (oEvent.lengthComputable) {
                let percentComplete = oEvent.loaded / oEvent.total * 100;
                console.log(percentComplete);
            } else {
                // Unable to compute progress information since the total size is unknown
            }
        }

        function transferComplete(evt) {
            console.log("The transfer is complete.");
            console.log(evt);
        }

        function transferFailed(evt) {
            console.log("An error occurred while transferring the file.");
        }

        function transferCanceled(evt) {
            console.log("The transfer has been canceled by the user.");
        }*/

        $.ajax({
            type: 'GET',
            url: route('smart_proposal'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                quotationId: quotationId,
                initialDate: initialDate,
                initialHour: initialHour,
                finalHour: finalHour,
                duration: duration,
                technicians: technicians,
                week: week,
                paymentMethod: paymentMethod,
                paymentWay: paymentWay,
                save: save
            },
            success: function (response) {
                if (response.code === 500) {
                    Swal.close();
                    missingText('Algo salio mal');
                } else {
                    //Swal.close();
                    let branches = '';
                    let periods = '';
                    let panelTime = '';
                    let months = '';
                    let key = 0;
                    let j = 0;
                    let smartRoutes = [];
                    let waypoints = [];
                    response.forEach((element) => {
                        key++;
                        j = 0;
                        /*let services = element.periods.sort(function(a,b) {
                            return new Date(a.date) - new Date(b.date);
                        });*/
                        element.periods.forEach((item) => {
                            j++;
                            let routes =  Object.keys(item.times);
                            routes.forEach((route) => {
                                item.times[route].forEach((time) => {
                                    branches += `<tr>
                                            <td class="text-primary col-md-1">${time.folio}</td>
                                            <td class="col-md-3">${time.branch}</td>
                                            <td class="col-md-1">${time.date}</td>
                                            <td class="col-md-1">${time.hour}</td>
                                            <td class="col-md-2">${time.technician}</td>
                                        </tr>`;
                                    // Load route in map
                                    waypoints.push({'location': time.address});
                                });
                                let destination;
                                if (waypoints.length == 1) destination = waypoints[0];
                                else destination = waypoints.pop();
                                let routeByTechnician = {
                                    'title': `Ruta de ${item.times[route][0].technician}`,
                                    'waypoints': waypoints,
                                    'destination': destination.location,
                                    'key': `${j}-${item.times[route][0].technician}-${key}`,
                                    'color': item.times[route][0].color,
                                    'technician': item.times[route][0].technician,
                                    'branch': item.times[route][0].branchShort
                                };
                                smartRoutes.push(routeByTechnician);
                                waypoints = [];
                                months += `<div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                                    <b>Ruta ${item.times[route][0].technician} <span class="badge">Aplicación ${j}</span></b>
                                                    <span class="badge pull-right" id="km-${j}-${item.times[route][0].technician}-${key}"></span>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <table class="table table-responsive table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>OS</th>
                                                                        <th>Sucursal</th>
                                                                        <th>Fecha</th>
                                                                        <th>Hora</th>
                                                                        <th>Técnico</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    ${branches}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div id="map-${j}-${item.times[route][0].technician}-${key}" style="height: 300px; width: 100%;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`;
                                branches = '';
                            });
                        });
                        periods += createPanelPeriod(element.name, months, key);
                        months = '';
                    });
                    const masterPanel = $('#masterPanel');
                    masterPanel.children().remove();
                    masterPanel.append(periods);
                    // Load route maps ot
                    console.log(smartRoutes);
                    smartRoutes.forEach((route) => {
                        createMap(route.waypoints, route.destination, route.key, route.color, route.technician, route.branch);
                    });
                    setTimeout(() => {
                        globalRoute(smartRoutes);
                        Swal.close();
                    }, 60000);
                }
            }
        });

    }

    function createMap(route, destination, key, color, technician, branchShort) {
        initMap(destination, route, key, color, technician, branchShort);
    }

    function initMap(destination, branchs, key, color, technician, branchShort) {
        const map = new google.maps.Map(document.getElementById(`map-${key}`), {
            zoom: 4,
            center: { lat: -24.345, lng: 134.46 },
        });
        const directionsService = new google.maps.DirectionsService();
        const directionsRenderer = new google.maps.DirectionsRenderer({
            polylineOptions: {
                strokeColor: color
            },
            /*markerOptions:{
                icon:{
                    path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                    scale: 3,
                    strokeColor: color
                },
                label: {
                    text: branchShort,
                    fontWeight: 'bold',
                    color: color
                }
            },*/
            draggable: true,
            map,
        });
        directionsRenderer.addListener("directions_changed", () => {
            computeTotalDistance(directionsRenderer.getDirections(), key);
        });
        displayRoute(
            branchs[0].location,
            destination,
            directionsService,
            directionsRenderer,
            branchs
        );
    }

    function globalRoute(routes) {
        const map = new google.maps.Map(document.getElementById('globalRoute'), {
            zoom: 4,
            center: { lat: -24.345, lng: 134.46 },
        });
        const colors = ['#C0392B', '#9B59B6', '#2980B9', '#1ABC9C', '#F1C40F', '#2C3E50', '#AAB7B8', '#7FB3D5', '#145A32', '#6E2C00', '#4A235A', '#00FFFF', '#008080'];
        const routesList = $('#routes-list');
        routes.forEach((route, i) => {
            const directionsService = new google.maps.DirectionsService();
            const directionsRenderer = new google.maps.DirectionsRenderer({
                polylineOptions: {
                    strokeColor: colors[i]
                },
                /*markerOptions:{
                    icon:{
                        path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                        scale: 3,
                        strokeColor: route.color
                    },
                    label: {
                        text: route.branch,
                        fontWeight: 'bold',
                        color: route.color
                    }
                },*/
                draggable: false,
                map,
            });
            displayRoute(
                route.waypoints[0].location,
                route.destination,
                directionsService,
                directionsRenderer,
                route.waypoints
            );
            let item = `<li class="list-group-item"><input type="color" disabled="disabled" value="${colors[i]}" style="border: none;"> ${route.technician}</li>`;
            routesList.append(item);
        });
    }

    function displayRoute(origin, destination, service, display, branchs) {
        service.route(
            {
                origin: origin,
                destination: destination,
                waypoints: branchs,
                travelMode: google.maps.TravelMode.DRIVING,
                avoidTolls: true,
            },
            (result, status) => {
                if (status === "OK") {
                    display.setDirections(result);
                    let leg = result.routes[0].legs[0];
                } else {
                    alert("Could not display directions due to: " + status);
                }
            }
        );
    }

    function computeTotalDistance(result, key) {
        let total = 0;
        const myroute = result.routes[0];

        for (let i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        total = total / 1000;
        document.getElementById(`km-${key}`).innerHTML = "Distancia Total " + total + " km";
    }

    $('#saveScheduleSmartPropose').click(function () {
        Swal.fire({
            title: 'Guardar Programación',
            text: "¿Deseas guardar la programación de servicios?",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                let quotationId = $('#quotationId').val();
                let completeDate = document.getElementById('initialDateIntelligent').value;
                let finalHour = document.getElementById('finalHourIntelligent').value;
                let duration = document.getElementById('minutes').value;
                let technicians = $('#techniciansIntelligent').val();
                let paymentMethod = document.getElementById('paymentMethod').value;
                let paymentWay = document.getElementById('paymentWay').value;
                completeDate = completeDate.split(' ');
                let week = {
                    '1': document.getElementById('monday').checked,
                    '2': document.getElementById('tuesday').checked,
                    '3': document.getElementById('wednesday').checked,
                    '4': document.getElementById('thursday').checked,
                    '5': document.getElementById('friday').checked,
                    '6': document.getElementById('saturday').checked,
                    '0': document.getElementById('sunday').checked
                };
                let message = 'Guardando propuesta inteligente... Por favor espere un momento. (Este proceso suele tardar más de lo común.)';
                getProposalIntelligent(quotationId, completeDate[0], completeDate[1], finalHour, duration, technicians, week, paymentMethod, paymentWay, true);
            } else {
                // not
            }
        })
    });

});