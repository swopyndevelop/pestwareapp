import {Spanish} from "flatpickr/dist/l10n/es";


export const configPicker = {
    locale: Spanish,
    enableTime: true,
    time_24hr: false,
    altInput: true,
    altFormat: "l, j F h:i K",
    dateFormat: 'Y-m-d H:i'
};

export const configOnlyPicker = {
    locale: Spanish,
    enableTime: false,
    time_24hr: false,
    altInput: true,
    altFormat: "l, j F"
};

export const configPickerMultiple = {
    locale: Spanish,
    enableTime: true,
    time_24hr: false,
    mode: 'multiple',
    altInput: true,
    altFormat: "D, j M h:i K",
    dateFormat: 'Y-m-d H:i'
};

export const configPickerMultipleCustom = {
    locale: Spanish,
    enableTime: false,
    time_24hr: false,
    mode: 'multiple',
    altInput: true,
    altFormat: "D, j M",
    dateFormat: 'Y-m-d'
};

export const configTimePicker = {
    enableTime: true,
    noCalendar: true,
    dateFormat: "H:i",
}