import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";


$(document).ready(function() {

    let initialDateEvent;
    let finalDateEvent;

    let btnFilter = document.getElementById('btnRoute');
    let btnGetCode = document.getElementById('btnGetCode');

    btnGetCode.onclick = function () {
        generateNewCode();
    }

    $(function() {

        $('input[name="filterDateEvent"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateEvent"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateEvent = picker.startDate.format('YYYY-MM-DD');
            finalDateEvent = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateEvent"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    btnFilter.onclick = function () {
        let employee = $('#technician option:selected').val();

        initialDateEvent = initialDateEvent == undefined ? null : initialDateEvent;
        finalDateEvent = finalDateEvent == undefined ? null : finalDateEvent;

        window.location.href = `/attendances?initialDate=${initialDateEvent}&finalDate=${finalDateEvent}
            &employee_id=${employee}`;
    }

    function generateNewCode()
    {
        loaderPestWare('Obteniendo código...');
        $.ajax({
            type: 'POST',
            url: route('generate_code_attendance'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (response) {
                Swal.close();
                if (response.code === 500){
                    showToast('error','Error', response.message);
                }
                else{
                    Swal.fire({
                        title: `<h1>${response.data}</h1>`,
                        type: 'success',
                        html: '<br><br><p style="color: #1E8CC7;">Ingresa este código en tu app para registrar tu asistencia.</p>',
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        Swal.close()
                    })
                }
            }
        });
    }
});