/**
 * @author Manuel Mendoza
 * @version 20/12/2020
 * register/_modalConfigStart2.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    //Intro Js
    const introJsPest = document.getElementById('introJsInput').value;
    let urlCurrent = window.location.pathname;
    let viewConfigStart = '/edit/companie/instance';
    let viewIndex = '/index_register';

    if (urlCurrent == viewConfigStart){
        if (introJsPest == 1){
            startInstance();
        }
        
        //startInstance();
        function startInstance(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#titleInstance',
                        title:"Configuración Inicial Y Personalización",
                        intro: "Pestware App se integra a tu negocio por lo que aquí podrás subir los logotipos y sellos para tus Certificados y " +
                            "Cotizaciones así como la imagen de tu espacio de trabajo.",
                        position: "right",
                    },
                    {
                        element: '#ConfigurationInstanceDivLeft',
                        title:"Configuración Inicial y Personalización",
                        intro: "Arrastra suelta tus logos en cada caja o bien da click y búscalo entre tus archivos.",
                        position: "right",
                    },
                    {
                        element: '#ConfigurationInstanceDivRight',
                        title:"Configuración Inicial y Personalización",
                        intro: "Agrega tu Número de Licencia o Permiso así como tu página de Facebook para que aparezca en tu documentación, también podrás cambiar el color de la barra de navegación. \n",
                        position: "right",
                    },
                    {
                        element:'#user_menu',
                        title: "Mi perfil",
                        intro: "Edita y completa tu información General. Aquí podrás cambiar tu foto de Perfil y modificar tu contraseña.",
                        position: 'left'
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false
            });
            intro.start();
            intro.onexit(function() {
                updateAjaxIntroJs(0);
            });
            intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
                intro.exit();
                setTimeout(() => {
                    $('#user_menu').addClass('open');
                    startUlAdministrator();
                }, 1000);
            });
        }

        //startUlAdministrator();
        function startUlAdministrator(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#editProfileDiv',
                        title:"Editar Perfil",
                        intro: "<div class='text-center'><p>Da click en el botón Editar Perfil para personalizar tus datos.</p>" +
                            "<button class='btn' id='editProfileNext'>Editar Perfil</button></div>",
                        position: "right",
                    },
                ],
                keyboardNavigation: false
            });
            intro.start();
            document.getElementById('editProfileNext').onclick = function() {
                $('#myModalProfileEdit').modal('show');
                intro.exit();
                setTimeout(() => {
                    startModalAdministrator();
                }, 1000);
            };
        }

        //startModalAdministrator();
        function startModalAdministrator(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#h4EditProfile',
                        title:"Nombre",
                        intro: "Mi cuenta",
                        position: "right"
                    },
                    {
                        element: '#nameEditProfileDiv',
                        title:"Nombre",
                        intro: "Aquí puedes editar el nombre de tu cuenta.",
                        position: "right"
                    },
                    {
                        element: '#passwordEditProfileDiv',
                        title:"Contraseña",
                        intro: "Aquí puedes editar la contraseña de tu cuenta.",
                        position: "right"
                    },
                    {
                        element: '#achievementImgProfile',
                        title:"Imagen de perfil",
                        intro: "Personaliza tu foto de perfil.",
                        position: "right"
                    },
                    {
                        element: '#btnUpdateButtonDiv',
                        title:"Actualizar",
                        intro: "Finalmente pulsa en el botón actualizar para guardar tus datos.",
                        position: "right"
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false
            });
            intro.start();
            intro.onexit(function() {
                updateAjaxIntroJs(0);
            });
            intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {

                setTimeout(() => {
                    updateAjaxIntroJs(1);
                    setTimeout(() => {
                        window.location.href = route('index_register');
                    }, 1000);
                }, 500);

            });
        }

    }
    function updateAjaxIntroJs(introjs){
        $.ajax({
            type: 'POST',
            url: route('change_introjs_user'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                introjs: introjs
            },
            success: function(data) {
                //console.log('OK');
            }
        });
    }
    //End Js

    //get id url
    let url = location.href;
    url = url.split('/');
    let idComp = url[6];
    $("#idCompanieLogo").val(idComp);
    $("#idCompanieMiniLogo").val(idComp);
    $("#idCompaniePDFlogo").val(idComp);
    $("#idCompaniePDFsello").val(idComp);

    //
    Dropzone.options.myDropzone = {
        maxFilesize: 1, //mb- Image files not above this size
        uploadMultiple: false, // set to true to allow multiple image uploads
        parallelUploads: 2, //all images should upload same time
        maxFiles: 1, //number of images a user should upload at an instance
        acceptedFiles: ".png,.jpg,.jpeg", //allowed file types, .pdf or anyother would throw error
        addRemoveLinks: true, // add a remove link underneath each image to
        autoProcessQueue: true // Prevents Dropzone from uploading dropped files immediately
    };

    //select payment way
    $('#theme').on('change', function() {
        if ($(this).val() === "1") {
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-blue.png");
        } else if($(this).val() === "2"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-white.png");
        } else if($(this).val() === "3"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-purple.png");
        } else if($(this).val() === "4"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-yellow.png");
        } else if($(this).val() === "5"){
            $("#themeImage").attr("src","https://pestwareapp.comimg/themes/skin-red.png");
        } else if($(this).val() === "6"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-green.png");
        }
    });
    $("#saveConfigStart").click(function() {
        //validations
        //get id url
        let url = location.href;
        url = url.split('/');
        let licencia = $("input[name=licencia]").val();
        let facebook = $("input[name=facebook]").val();
        let theme = $("#theme option:selected").val();
        let idCompanie = url[6];
        let user = url.pop();

        loaderPestWare('Guardando Configuración Inicial');
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: "GET",
            url: route('save_config_start'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                theme: theme,
                idCompanie: idCompanie,
                licencia: licencia,
                facebook: facebook,
                user: user
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Error al guardar','Algo salió mal, intente de nuevo.')
                } else {
                    Swal.close();
                    showToast('success-redirect','Datos Guardados','Configuración guardada','index_register')
                }
            }
        });
    });

});