/**
 * @author Manuel Mendoza
 * @version 20/12/2020
 * register/_modalConfigStart2.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";
import {expressions, getFields, setFields, validateField} from "../validations/regex";

$(document).ready(function() {
    //get id url
    let url = location.href;
    url = url.split('/');
    let idComp = url[6];
    $("#idCompanieLogo").val(idComp);
    $("#idCompanieMiniLogo").val(idComp);
    $("#idCompaniePDFlogo").val(idComp);
    $("#idCompaniePDFsello").val(idComp);

    //select payment way
    $('#theme').on('change', function() {
        if ($(this).val() === "1") {
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-blue.png");
        } else if($(this).val() === "2"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-white.png");;
        } else if($(this).val() === "3"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-purple.png");;
        } else if($(this).val() === "4"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-yellow.png");;
        } else if($(this).val() === "5"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-red.png");;
        } else if($(this).val() === "6"){
            $("#themeImage").attr("src","https://pestwareapp.com/img/themes/skin-green.png");;
        }
    });

    $("#updateConfigStart").click(function() {

        let theme = $("#theme option:selected").val();
        let updateColorMIP = $("input[name=updateColorMIP]").val();

        loaderPestWare('Guardando');
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: "POST",
            url: route('update_config_start'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                theme: theme,
                updateColorMIP: updateColorMIP
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Eror al guardar','Intentelo de nuevo');
                } else {
                    Swal.close();
                    showToast('success-redirect','Guardado','Se guardo la configuración correctamente','edit_config_instance');
                    $("#updateConfigStart").attr("disabled", true);
                }
            }
        });
    });

    $("#updateConfigStartMip").click(function() {

        let theme = $("#theme option:selected").val();
        let updateColorMIP = $("input[name=updateColorMIP]").val();

        loaderPestWare('Guardando');
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: "POST",
            url: route('update_config_start'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                theme: theme,
                updateColorMIP: updateColorMIP
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Eror al guardar','Intentelo de nuevo');
                } else {
                    Swal.close();
                    showToast('success-redirect','Guardado','Se guardo la configuración correctamente','edit_config_instance');
                    $("#updateConfigStartMip").attr("disabled", true);
                }
            }
        });
    });

    // validations section ->
    const iva_country = document.getElementById('iva_country');
    const isr_country = document.getElementById('isr_country');

    const fieldsTaxesForm = {
        iva_country: false,
        isr_country: false
    }
    setFields(fieldsTaxesForm);

    const validateForm = (e) => {
        switch (e.target.name) {
            case "iva_country":
                validateField(expressions.totalDecimal, e.target.value, 'iva_country');
                break;
            case "isr_country":
                validateField(expressions.totalDecimal, e.target.value, 'isr_country');
                break;
        }
    }

    iva_country.addEventListener('keyup', validateForm);
    iva_country.addEventListener('blur', validateForm);
    isr_country.addEventListener('keyup', validateForm);
    isr_country.addEventListener('blur', validateForm);

    $("#updateConfigStarTax").click(function() {

        let iva_country = $("input[name=iva_country]").val();
        let isr_country = $("input[name=isr_country]").val();

        validateField(expressions.totalDecimal, iva_country, 'iva_country');
        validateField(expressions.totalDecimal, isr_country, 'isr_country');

        if(getFields().iva_country && getFields().isr_country){

            loaderPestWare('Guardando');
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                }
            });
            $.ajax({
                type: "POST",
                url: route('update_config_start_tax'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    iva_country: iva_country,
                    isr_country: isr_country,
                },
                success: function(data) {
                    if (data.code == 500) {
                        Swal.close();
                        showToast('error','Eror al guardar','Intentelo de nuevo');
                    } else {
                        Swal.close();
                        showToast('success-redirect','Guardado','Se guardo la configuración correctamente','edit_config_instance');
                    }
                }
            });
        }
        else {
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }

    });

});