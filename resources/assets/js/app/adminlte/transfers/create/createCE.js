import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function() {

    document.getElementById('btnSaveTransferCE').disabled = true;

    $(document).on('click', '.addCE', function() {

        let origin = $("#selectJobCentersOrigin option:selected").val();
        let destinity = $("#selectEmployeesDestiny option:selected").val();

        if (destinity == 0) missingText('Selecciona la sucursal (destino)');
        else if (destinity == origin) missingText('La sucursal destino no puede ser igual al origen');
        else {
            loaderPestWare('Cargando productos en existencia...');
            $.ajax({
                type: 'POST',
                url: route('get_products_available'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: origin,
                    type: 1
                },
                success: function(data) {
                    if (data.errors === 500) {
                        Swal.close();
                        missingText('Algo salio mal, intenta de nuevo.');
                    } else {
                        // Load products select
                        var options = '';
                        data.forEach((element) => {
                            options += `<option value="${element.id}">${element.name}</option>`;
                        });

                        var html = '';
                        html += '<tr>';
                        html += `<td><select name="selectProductsCS[]" id="selectProductsCS" class="products-single form-control selectProductsCS" style="width: 100%"><option value="0" disabled="" selected="">Seleccione una opción</option>${options}</select></td>`;
                        html += '<td><input type="text" readonly="readonly" name="quantityCS[]" class="form-control quantityCS" /></td>';
                        html += '<td><input type="number" readonly="readonly" name="stockCS[]" class="form-control stockCS" /></td>';
                        html += '<td><input type="number" readonly="readonly" name="stockQuantityCS[]" class="form-control stockQuantityCS" /></td>';
                        html += `<td><select name="typeTransfer[]" id="typeTransfer" class="form-control typeTransfer"><option value="1">Unidades</option><option value="0">Fracción</option></select></td>`;
                        html += '<td><input type="number" min="1" name="unitsCS[]" class="form-control unitsCS" /></td>';
                        html += '<td><input type="text" readonly="readonly" name="priceBaseCS[]" class="form-control priceBaseCS" /></td>';
                        html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove" style="margin-left: 10px;"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
                        $('#item_table').append(html);
                        $(".products-single").select2();

                        let len = $('.selectProductsCS').length;

                        $('.selectProductsCS').each(function(index) {
                            if (index === (len - 1)) {
                                $(this).change(function() {
                                    let valId = $(this, "option:selected").val();
                                    getDataProduct(valId, index);
                                });
                            }
                        });
                        document.getElementById('btnSaveTransferCE').disabled = false;
                        Swal.close();
                    }
                }
            });
        }

    });

    function getDataProduct(id, index) {
        let origin = $("#selectJobCentersOrigin option:selected").val();
        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('data_product'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                id: id,
                origin: origin,
                campo: 's.id_job_center'
            },
            success: function(data) {
                if (data.errors) {
                    Swal.close();
                    missingText('Algo salio mal');
                } else {

                    $('.quantityCS').each(function(i) {
                        if (index === i) {
                            $(this).val(`${data.quantity} ${data.unit}s`);
                        }
                    });

                    $('.stockCS').each(function(i) {
                        if (index === i) {
                            $(this).val(data.stock);
                        }
                    });

                    $('.stockQuantityCS').each(function(i) {
                        if (index === i) {
                            $(this).val(data.stock_other_units);
                        }
                    });

                    $('.priceBaseCS').each(function(i) {
                        if (index === i) {
                            $(this).val(data.base_price);
                        }
                    });

                    Swal.close();
                }
            }
        });
    }

    $(document).on('click', '.remove', function() {
        $(this).closest('tr').remove();
    });

    $('#insert_formCE').on('submit', function(event) {

        event.preventDefault();
        var error = '';

        if ($('.selectProductsCS').length == 0) {
            missingText('Debes agregar por lo menos un producto.')
            return false;
        }

        $('.selectProductsCS').each(function() {
            var count = 1;
            if ($(this).val() == null) {
                error += "<p>Debes seleccionar un producto</p>";
                return false;
            }
            count = count + 1;
        });

        $('.unitsCS').each(function(index) {
            var count = 1;
            let stock = parseInt($('.stockCS').eq(index).val());
            let stockOther = parseInt($('.stockQuantityCS').eq(index).val());
            let typeTransfer = $('.typeTransfer').eq(index).val();
            let transfer = parseInt($(this).val());
            if ($(this).val() == '') {
                error += `<p>Debes ingresar la cantidad a traspasar.</p>`;
                //return false;
            }
            if (transfer > stock && typeTransfer == 1) {
                error += `<p>Las unidades a traspasar no pueden ser mayor a la existencia.</p>`;
                //return false;
            }
            if (transfer > stockOther && typeTransfer == 0) {
                error += `<p>La cantidad a traspasar no puede ser mayor a la existencia.</p>`;
                //return false;
            }
            if (transfer <= 0) {
                error += `<p>Las unidades a traspasar deben ser mayor a cero.</p>`;
                //return false;
            }
            count = count + 1;
        });

        var form_data = $(this).serialize();

        if (error == '') {
            loaderPestWare('Guardando traspaso');
            $.ajax({
                url: route('add_transfer_ce'),
                method: "POST",
                data: form_data,
                success: function(data) {
                    if (data.code == 201) {
                        Swal.close();
                        $('#item_table').find("tr:gt(0)").remove();
                        showToast('success-redirect','Traspasos','Guardando correctamente','index_transfer')
                    } else {
                        Swal.close();
                        missingText(data.message);
                    }
                }
            });
        } else {
            $('#error').html('<div class="alert alert-danger">' + error + '</div>');
            //missingText(error);
        }
    });

    function missingText(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
        });
    }

});