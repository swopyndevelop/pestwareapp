/**
 * @author Alberto Martinez
 * @version 05/10/2020
 * register/index.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function() {

    let symbolCountry = document.getElementById('symbolCountry').value;

    $('#transferDetailCEMP').on('show.bs.modal', function(e) {

        let button = $(e.relatedTarget);
        if (button.data('id')) {

            let id = button.data('id');
            let idtransfer = button.data('idtransfer');
            let date = button.data('date');
            let user = button.data('user');
            let origen = button.data('origen');
            let destiny = button.data('destiny');
            let total = button.data('total');

            document.getElementById("id_transfer_cemp").innerHTML = idtransfer;
            document.getElementById("dateCEMP").innerHTML = date;
            document.getElementById("usuarioCEMP").innerHTML = user;
            document.getElementById("origenCEMP").innerHTML = `Origen: ${origen}`;
            document.getElementById("destinyCEMP").innerHTML = `Destino: ${destiny}`;
            document.getElementById("totalCEMP").innerHTML = `Total: ${symbolCountry}${total}`;

            createTableDetailCEMP();
            let rows = '';
            productsModalCEMP.forEach((element) => {
                if (element.id_transfer_ee == id) {
                    rows += `<tr>
                        <td class="text-center">${element.name}</td>
                        <td class="text-center">${element.quantity} ${element.unit}</td>
                        <td class="text-center">${element.is_units === 1 ? `${element.units}` + ' Unidad' : `${element.units}` + ' Fracción' }</td>
                        <td class="text-center">${symbolCountry}${element.base_price}</td>
                        <td class="text-center">${symbolCountry}${element.is_units === 1 ? `${element.base_price * element.units}` : `${element.base_price * (element.units / element.quantity)}` }</td>
                        </tr>`;
                }
            });
            $('#tableBodyDetailCEMP').append(rows);
        }

    });

    // Load data table ----------------->

    let isLoadDataCEMP = true;
    let productsModalCEMP;

    $('#tab_transfers_cemp').click(function() {
        if (isLoadDataCEMP) {
            getDataCEMP();
            isLoadDataCEMP = false;
        }
    }); // end tabReinforcements click

    function getDataCEMP() {
        loaderPestWare('Cargando traspasos...')
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: `/transfers/data/cemp?page=${stateCEMP.page}`,
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error al cargar los datos.', 'Algo salio mal, intenta de nuevo');
                } else {
                    createTableCEMP();
                    let rows = '';
                    let dataPagination = paginationCEMP(response.data.total, response.data.current_page, response.data.per_page)
                    productsModalCEMP = response.productsData;
                    response.data.data.forEach((element) => {
                        rows += `<tr>
                            <td>
                                <div class="btn-group">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ${element.id_transfer_ee} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a data-toggle="modal" href="#transferDetailCEMP" 
                                                data-id="${element.id}"
                                                data-idtransfer="${element.id_transfer_ee}"
                                                data-date="${element.date}"
                                                data-user="${element.usuario}"
                                                data-origen="${element.origen}"
                                                data-destiny="${element.destiny}"
                                                data-total="${element.total}">
                                                <i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>${element.date}<br>${element.usuario}</td>
                            <td>${element.origen}</td>
                            <td>${element.destiny}</td>
                            <td>${element.tot_unit}</td>
                            <td>${symbolCountry}${element.total}</td>
                            <tr/>`;
                    });
                    $('#tableBodyTransfersCEMP').append(rows);
                    pageButtonsCEMP(dataPagination.pages)
                    Swal.close();
                }
            }
        });
    }

    function createTableCEMP() {
        $('#tableProductCEMP').remove();
        let table =
            `<table class="table tablesorter table-hover" id="tableProductCEMP">
                <thead>
                    <tr>
                        <th># Traspaso</th>
                        <th>Fecha/Usuario</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Unidades<br>Traspasadas</th>
                        <th>Total Inventario</th>
                    </tr>
                </thead>
                <tbody id="tableBodyTransfersCEMP">

                </tbody>
            </table>`;

        $('#masterContainerCEMP').append(table);
    } // end createTable()

    function createTableDetailCEMP() {
        $('#tableProductDetailCEMP').remove();
        let table =
            `<table class="table tablesorter" id="tableProductDetailCEMP">
                <thead class="table-general">
                    <tr>
                        <th class="text-center">Producto</th>
                        <th class="text-center">Cantidad Envase</th>
                        <th class="text-center">Unidades Traspasadas</th>
                        <th class="text-center">Precio Base</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody id="tableBodyDetailCEMP">
                        
                </tbody>
            </table>`;

        $('#masterDetailCEMP').append(table);
    } // end createTable()

    var stateCEMP = {
        'page': 1,
        'rows': 5,
        'window': 5,
    }

    function paginationCEMP(count, page, rows) {

        var trimStart = (page - 1) * rows
        var trimEnd = trimStart + rows

        //var trimmedData = querySet.slice(trimStart, trimEnd)

        var pages = Math.round(count / rows);

        return {
            'pages': pages,
        }
    }

    function pageButtonsCEMP(pages) {
        var wrapper = document.getElementById('pagination-wrapperCEMP')

        wrapper.innerHTML = ``

        var maxLeft = (stateCEMP.page - Math.floor(stateCEMP.window / 2))
        var maxRight = (stateCEMP.page + Math.floor(stateCEMP.window / 2))

        if (maxLeft < 1) {
            maxLeft = 1
            maxRight = stateCEMP.window
        }

        if (maxRight > pages) {
            maxLeft = pages - (stateCEMP.window - 1)

            if (maxLeft < 1) {
                maxLeft = 1
            }
            maxRight = pages
        }



        for (var page = maxLeft; page <= maxRight; page++) {
            wrapper.innerHTML += `<li value=${page} class="pageCEMP"><a>${page}</a></li>`
        }

        if (stateCEMP.page != 1) {
            wrapper.innerHTML = `<li value=${1} class="pageCEMP"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>` + wrapper.innerHTML
        }

        if (stateCEMP.page != pages) {
            wrapper.innerHTML += `<li value=${pages} class="pageCEMP"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>`
        }

        $('.pageCEMP').on('click', function() {

            stateCEMP.page = Number($(this).val())

            getDataCEMP();
        })

    }

});