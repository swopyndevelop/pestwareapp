/**
 * @author Alberto Martinez
 * @version 05/10/2020
 * register/index.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function() {

    let symbolCountry = document.getElementById('symbolCountry').value;

    $('#transferDetailCE').on('show.bs.modal', function(e) {

        let button = $(e.relatedTarget);
        if (button.data('id')) {

            let id = button.data('id');
            let idtransfer = button.data('idtransfer');
            let date = button.data('date');
            let user = button.data('user');
            let origen = button.data('origen');
            let destiny = button.data('destiny');
            let total = button.data('total');

            document.getElementById("id_transfer_ce").innerHTML = idtransfer;
            document.getElementById("dateCE").innerHTML = date;
            document.getElementById("usuarioCE").innerHTML = user;
            document.getElementById("origenCE").innerHTML = `Origen: ${origen}`;
            document.getElementById("destinyCE").innerHTML = `Destino: ${destiny}`;
            document.getElementById("totalCE").innerHTML = `Total: ${symbolCountry}${total}`;

            createTableDetailCE();
            let rows = '';
            productsModalCE.forEach((element) => {
                if (element.id_transfer_ce == id) {
                    rows += `<tr>
                        <td class="text-center">${element.name}</td>
                        <td class="text-center">${element.quantity} ${element.unit}</td>
                        <td class="text-center">${element.is_units === 1 ? `${element.units}` + ' Unidad' : `${element.units}` + ' Fracción' }</td>
                        <td class="text-center">${symbolCountry}${element.base_price}</td>
                        <td class="text-center">${symbolCountry}${element.is_units === 1 ? `${element.base_price * element.units}` : `${element.base_price * (element.units / element.quantity)}` }</td>
                        </tr>`;
                }
            });
            $('#tableBodyDetailCE').append(rows);
        }

    });

    // Load data table ----------------->

    let isLoadDataCE = true;
    let productsModalCE;

    $('#tab_transfers_ce').click(function() {
        if (isLoadDataCE) {
            getDataCE();
            isLoadDataCE = false;
        }
    }); // end tabReinforcements click

    function getDataCE() {
        loaderPestWare('Cargando traspasos...')
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: `/transfers/data/ce?page=${stateCE.page}`,
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error al cargar los datos.', 'Algo salio mal, intenta de nuevo');
                } else {
                    createTableCE();
                    let rows = '';
                    let dataPagination = paginationCE(response.data.total, response.data.current_page, response.data.per_page)
                    productsModalCE = response.productsData;
                    response.data.data.forEach((element) => {
                        rows += `<tr>
                            <td>
                                <div class="btn-group">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ${element.id_transfer_ce} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a data-toggle="modal" href="#transferDetailCE" 
                                                data-id="${element.id}"
                                                data-idtransfer="${element.id_transfer_ce}"
                                                data-date="${element.date}"
                                                data-user="${element.usuario}"
                                                data-origen="${element.origen}"
                                                data-destiny="${element.destiny}"
                                                data-total="${element.total}">
                                                <i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>${element.date}<br>${element.usuario}</td>
                            <td>${element.origen}</td>
                            <td>${element.destiny}</td>
                            <td>${element.tot_unit}</td>
                            <td>${symbolCountry}${element.total}</td>
                            <tr/>`;
                    });
                    $('#tableBodyTransfersCE').append(rows);
                    pageButtonsCE(dataPagination.pages)
                    Swal.close();
                }
            }
        });
    }

    function createTableCE() {
        $('#tableProductCE').remove();
        let table =
            `<table class="table tablesorter table-hover" id="tableProductCE">
                <thead>
                    <tr>
                        <th># Traspaso</th>
                        <th>Fecha/Usuario</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Unidades<br>Traspasadas</th>
                        <th>Total Inventario</th>
                    </tr>
                </thead>
                <tbody id="tableBodyTransfersCE">

                </tbody>
            </table>`;

        $('#masterContainerCE').append(table);
    } // end createTable()

    function createTableDetailCE() {
        $('#tableProductDetailCE').remove();
        let table =
            `<table class="table tablesorter" id="tableProductDetailCE">
                <thead class="table-general">
                    <tr>
                        <th class="text-center">Producto</th>
                        <th class="text-center">Cantidad Envase</th>
                        <th class="text-center">Unidades Traspasadas</th>
                        <th class="text-center">Precio Base</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody id="tableBodyDetailCE">
                        
                </tbody>
            </table>`;

        $('#masterDetailCE').append(table);
    } // end createTable()

    var stateCE = {
        'page': 1,
        'rows': 5,
        'window': 5,
    }

    function paginationCE(count, page, rows) {

        var trimStart = (page - 1) * rows
        var trimEnd = trimStart + rows

        //var trimmedData = querySet.slice(trimStart, trimEnd)

        var pages = Math.round(count / rows);

        return {
            'pages': pages,
        }
    }

    function pageButtonsCE(pages) {
        var wrapper = document.getElementById('pagination-wrapperCE')

        wrapper.innerHTML = ``

        var maxLeft = (stateCE.page - Math.floor(stateCE.window / 2))
        var maxRight = (stateCE.page + Math.floor(stateCE.window / 2))

        if (maxLeft < 1) {
            maxLeft = 1
            maxRight = stateCE.window
        }

        if (maxRight > pages) {
            maxLeft = pages - (stateCE.window - 1)

            if (maxLeft < 1) {
                maxLeft = 1
            }
            maxRight = pages
        }



        for (var page = maxLeft; page <= maxRight; page++) {
            wrapper.innerHTML += `<li value=${page} class="pageCE"><a>${page}</a></li>`
        }

        if (stateCE.page != 1) {
            wrapper.innerHTML = `<li value=${1} class="pageCE"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>` + wrapper.innerHTML
        }

        if (stateCE.page != pages) {
            wrapper.innerHTML += `<li value=${pages} class="pageCE"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>`
        }

        $('.pageCE').on('click', function() {

            stateCE.page = Number($(this).val())

            getDataCE();
        })

    }

});