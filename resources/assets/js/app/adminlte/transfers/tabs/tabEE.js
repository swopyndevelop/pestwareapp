/**
 * @author Alberto Martinez
 * @version 05/10/2020
 * register/index.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function() {

    let symbolCountry = document.getElementById('symbolCountry').value;

    $('#transferDetailEE').on('show.bs.modal', function(e) {

        let button = $(e.relatedTarget);
        if (button.data('id')) {

            let id = button.data('id');
            let idtransfer = button.data('idtransfer');
            let date = button.data('date');
            let user = button.data('user');
            let origen = button.data('origen');
            let destiny = button.data('destiny');
            let total = button.data('total');

            document.getElementById("id_transfer_ee").innerHTML = idtransfer;
            document.getElementById("dateEE").innerHTML = date;
            document.getElementById("usuarioEE").innerHTML = user;
            document.getElementById("origenEE").innerHTML = `Origen: ${origen}`;
            document.getElementById("destinyEE").innerHTML = `Destino: ${destiny}`;
            document.getElementById("totalEE").innerHTML = `Total: ${symbolCountry}${total}`;

            createTableDetailEE();
            let rows = '';
            productsModalEE.forEach((element) => {
                if (element.id_transfer_ee == id) {
                    rows += `<tr>
                        <td class="text-center">${element.name}</td>
                        <td class="text-center">${element.quantity} ${element.unit}</td>
                        <td class="text-center">${element.is_units === 1 ? `${element.units}` + ' Unidad' : `${element.units}` + ' Fracción' }</td>
                        <td class="text-center">${symbolCountry}${element.base_price}</td>
                        <td class="text-center">${symbolCountry}${element.is_units === 1 ? `${element.base_price * element.units}` : `${element.base_price * (element.units / element.quantity)}` }</td>
                        </tr>`;
                }
            });
            $('#tableBodyDetailEE').append(rows);
        }

    });

    // Load data table ----------------->

    let isLoadDataEE = true;
    let productsModalEE;

    $('#tab_transfers_ee').click(function() {
        if (isLoadDataEE) {
            getDataEE();
            isLoadDataEE = false;
        }
    }); // end tabReinforcements click

    function getDataEE() {
        loaderPestWare('Cargando traspasos...')
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: `/transfers/data/ee?page=${stateEE.page}`,
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error al cargar los datos.', 'Algo salio mal, intenta de nuevo');
                } else {
                    createTableEE();
                    let rows = '';
                    let dataPagination = paginationEE(response.data.total, response.data.current_page, response.data.per_page)
                    productsModalEE = response.productsData;
                    response.data.data.forEach((element) => {
                        rows += `<tr>
                            <td>
                                <div class="btn-group">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ${element.id_transfer_ee} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a data-toggle="modal" href="#transferDetailEE" 
                                                data-id="${element.id}"
                                                data-idtransfer="${element.id_transfer_ee}"
                                                data-date="${element.date}"
                                                data-user="${element.usuario}"
                                                data-origen="${element.origen}"
                                                data-destiny="${element.destiny}"
                                                data-total="${element.total}">
                                                <i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>${element.date}<br>${element.usuario}</td>
                            <td>${element.origen}</td>
                            <td>${element.destiny}</td>
                            <td>${element.tot_unit}</td>
                            <td>${symbolCountry}${element.total}</td>
                            <tr/>`;
                    });
                    $('#tableBodyTransfersEE').append(rows);
                    pageButtonsEE(dataPagination.pages)
                    Swal.close();
                }
            }
        });
    }

    function createTableEE() {
        $('#tableProductEE').remove();
        let table =
            `<table class="table tablesorter table-hover" id="tableProductEE">
                <thead>
                    <tr>
                        <th># Traspaso</th>
                        <th>Fecha/Usuario</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Unidades<br>Traspasadas</th>
                        <th>Total Inventario</th>
                    </tr>
                </thead>
                <tbody id="tableBodyTransfersEE">

                </tbody>
            </table>`;

        $('#masterContainerEE').append(table);
    } // end createTable()

    function createTableDetailEE() {
        $('#tableProductDetailEE').remove();
        let table =
            `<table class="table tablesorter" id="tableProductDetailEE">
                <thead class="table-general">
                    <tr>
                        <th class="text-center">Producto</th>
                        <th class="text-center">Cantidad Envase</th>
                        <th class="text-center">Unidades Traspasadas</th>
                        <th class="text-center">Precio Base</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody id="tableBodyDetailEE">
                        
                </tbody>
            </table>`;

        $('#masterDetailEE').append(table);
    } // end createTable()

    var stateEE = {
        'page': 1,
        'rows': 5,
        'window': 5,
    }

    function paginationEE(count, page, rows) {

        var trimStart = (page - 1) * rows
        var trimEnd = trimStart + rows

        //var trimmedData = querySet.slice(trimStart, trimEnd)

        var pages = Math.round(count / rows);

        return {
            'pages': pages,
        }
    }

    function pageButtonsEE(pages) {
        var wrapper = document.getElementById('pagination-wrapperEE')

        wrapper.innerHTML = ``

        var maxLeft = (stateEE.page - Math.floor(stateEE.window / 2))
        var maxRight = (stateEE.page + Math.floor(stateEE.window / 2))

        if (maxLeft < 1) {
            maxLeft = 1
            maxRight = stateEE.window
        }

        if (maxRight > pages) {
            maxLeft = pages - (stateEE.window - 1)

            if (maxLeft < 1) {
                maxLeft = 1
            }
            maxRight = pages
        }



        for (var page = maxLeft; page <= maxRight; page++) {
            wrapper.innerHTML += `<li value=${page} class="pageEE"><a>${page}</a></li>`
        }

        if (stateEE.page != 1) {
            wrapper.innerHTML = `<li value=${1} class="pageEE"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>` + wrapper.innerHTML
        }

        if (stateEE.page != pages) {
            wrapper.innerHTML += `<li value=${pages} class="pageEE"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>`
        }

        $('.pageEE').on('click', function() {

            stateEE.page = Number($(this).val())

            getDataEE();
        })

    }

});