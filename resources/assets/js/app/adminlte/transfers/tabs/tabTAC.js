/**
 * @author Alberto Martinez
 * @version 05/10/2020
 * register/index.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function() {

    let symbolCountry = document.getElementById('symbolCountry').value;

    $('#transferDetailTAC').on('show.bs.modal', function(e) {

        let button = $(e.relatedTarget);
        if (button.data('id')) {

            let id = button.data('id');
            let idtransfer = button.data('idtransfer');
            let date = button.data('date');
            let user = button.data('user');
            let origen = button.data('origen');
            let destiny = button.data('destiny');
            let total = button.data('total');

            document.getElementById("id_transfer_tac").innerHTML = idtransfer;
            document.getElementById("dateTAC").innerHTML = date;
            document.getElementById("usuarioTAC").innerHTML = user;
            document.getElementById("origenTAC").innerHTML = `Origen: ${origen}`;
            document.getElementById("destinyTAC").innerHTML = `Destino: ${destiny}`;
            document.getElementById("totalTAC").innerHTML = `Total: ${symbolCountry}${total}`;

            createTableDetailTAC();
            let rows = '';
            productsModalTAC.forEach((element) => {
                if (element.id_transfer_ee == id) {
                    rows += `<tr>
                        <td class="text-center">${element.name}</td>
                        <td class="text-center">${element.quantity} ${element.unit}</td>
                        <td class="text-center">${element.is_units === 1 ? `${element.units}` + ' Unidad' : `${element.units}` + ' Fracción' }</td>
                        <td class="text-center">${symbolCountry}${element.base_price}</td>
                        <td class="text-center">${symbolCountry}${element.is_units === 1 ? `${element.base_price * element.units}` : `${element.base_price * (element.units / element.quantity)}` }</td>
                        </tr>`;
                }
            });
            $('#tableBodyDetailTAC').append(rows);
        }

    });

    // Load data table ----------------->

    let isLoadDataTAC = true;
    let productsModalTAC;

    $('#tab_transfers_tac').click(function() {
        if (isLoadDataTAC) {
            getDataTAC();
            isLoadDataTAC = false;
        }
    }); // end tabReinforcements click

    function getDataTAC() {
        loaderPestWare('Cargando traspasos...')
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: `/transfers/data/tac?page=${stateTAC.page}`,
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error al cargar los datos.', 'Algo salio mal, intenta de nuevo');
                } else {
                    createTableTAC();
                    let rows = '';
                    let dataPagination = paginationTAC(response.data.total, response.data.current_page, response.data.per_page)
                    productsModalTAC = response.productsData;
                    response.data.data.forEach((element) => {
                        rows += `<tr>
                            <td>
                                <div class="btn-group">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ${element.id_transfer_ee} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a data-toggle="modal" href="#transferDetailTAC" 
                                                data-id="${element.id}"
                                                data-idtransfer="${element.id_transfer_ee}"
                                                data-date="${element.date}"
                                                data-user="${element.usuario}"
                                                data-origen="${element.origen}"
                                                data-destiny="${element.destiny}"
                                                data-total="${element.total}">
                                                <i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>${element.date}<br>${element.usuario}</td>
                            <td>${element.origen}</td>
                            <td>${element.destiny}</td>
                            <td>${element.tot_unit}</td>
                            <td>${symbolCountry}${element.total}</td>
                            <tr/>`;
                    });
                    $('#tableBodyTransfersTAC').append(rows);
                    pageButtonsTAC(dataPagination.pages)
                    Swal.close();
                }
            }
        });
    }

    function createTableTAC() {
        $('#tableProductTAC').remove();
        let table =
            `<table class="table tablesorter table-hover" id="tableProductTAC">
                <thead>
                    <tr>
                        <th># Traspaso</th>
                        <th>Fecha/Usuario</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Unidades<br>Traspasadas</th>
                        <th>Total Inventario</th>
                    </tr>
                </thead>
                <tbody id="tableBodyTransfersTAC">

                </tbody>
            </table>`;

        $('#masterContainerTAC').append(table);
    } // end createTable()

    function createTableDetailTAC() {
        $('#tableProductDetailTAC').remove();
        let table =
            `<table class="table tablesorter" id="tableProductDetailTAC">
                <thead class="table-general">
                    <tr>
                        <th class="text-center">Producto</th>
                        <th class="text-center">Cantidad Envase</th>
                        <th class="text-center">Unidades Traspasadas</th>
                        <th class="text-center">Precio Base</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody id="tableBodyDetailTAC">
                        
                </tbody>
            </table>`;

        $('#masterDetailTAC').append(table);
    } // end createTable()

    var stateTAC = {
        'page': 1,
        'rows': 5,
        'window': 5,
    }

    function paginationTAC(count, page, rows) {

        var trimStart = (page - 1) * rows
        var trimEnd = trimStart + rows

        //var trimmedData = querySet.slice(trimStart, trimEnd)

        var pages = Math.round(count / rows);

        return {
            'pages': pages,
        }
    }

    function pageButtonsTAC(pages) {
        var wrapper = document.getElementById('pagination-wrapperTAC')

        wrapper.innerHTML = ``

        var maxLeft = (stateTAC.page - Math.floor(stateTAC.window / 2))
        var maxRight = (stateTAC.page + Math.floor(stateTAC.window / 2))

        if (maxLeft < 1) {
            maxLeft = 1
            maxRight = stateTAC.window
        }

        if (maxRight > pages) {
            maxLeft = pages - (stateTAC.window - 1)

            if (maxLeft < 1) {
                maxLeft = 1
            }
            maxRight = pages
        }



        for (var page = maxLeft; page <= maxRight; page++) {
            wrapper.innerHTML += `<li value=${page} class="pageTAC"><a>${page}</a></li>`
        }

        if (stateTAC.page != 1) {
            wrapper.innerHTML = `<li value=${1} class="pageTAC"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>` + wrapper.innerHTML
        }

        if (stateTAC.page != pages) {
            wrapper.innerHTML += `<li value=${pages} class="pageTAC"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>`
        }

        $('.pageTAC').on('click', function() {

            stateTAC.page = Number($(this).val())

            getDataTAC();
        })

    }

});