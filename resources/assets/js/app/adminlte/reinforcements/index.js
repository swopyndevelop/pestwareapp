/**
 * @author Alberto Martinez
 * @version 28/06/2020
 * register/index.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    $('#tabReinforcements').click(function() {

        $('#tabQuotations').removeClass("btn-primary-dark").addClass("btn-default");
        $('#tabRecurrent').removeClass("btn-primary-dark").addClass("btn-default");
        $('#tabReinforcements').removeClass("btn-default").addClass("btn-primary-dark");
        loaderPestWare('Cargando refuerzos...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('index_reinforcements'),
            data: {
                _token: token
            },
            success: function(data) {
                if (data.response === 500) {
                    Swal.close();
                    showToast('error', 'Error al cargar refuerzos', 'Algo salio mal, intente de nuevo.');
                } else {
                    createTable();
                    let rows = '';
                    data.forEach((element) => {
                        rows += `<tr>
                            <td scope="row">
                                <div class="btn-group">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                                        ${element.id_service_order} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="${route('order_view', element.id_order_encoded)}" target="_blank"><i class="fa fa-eye" aria-hidden="true" style="color: #222d32;"></i>Ver OS</a></li>
                                        <li style="cursor: pointer;">
                                            <a data-toggle="modal" href="#reinforcement" data-id="${element.order}">
                                                <i class="fa fa-calendar-plus-o" aria-hidden="true" style="color: #2E86C1;"></i>Programar
                                            </a>
                                        </li>
                                        <li><a href="${route('cancel_view', element.id_quotation)}""><i class="fa fa-times" aria-hidden="true" style="color: red;"></i>Cancelar</a></li>
                                        <li><a href="${route('pdf_service', element.order)}"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>Descargar PDF OS</a></li>
                                    </ul>
                                    <!-- INICIA MODAL PARA CREAR REFUERZO -->
                                    <!-- TERMINA MODAL PARA CREAR REFUERZO -->
                                </div>
                            </td>
                            <td scope="row">${element.initial_date}<br>${element.initial_hour}</td>
                            <td scope="row">${element.agente}</td>
                            <td scope="row">${element.cliente}<br>${element.empresa}</td>
                            <td scope="row">${element.cellphone}</td>
                            <td scope="row">${element.address} #${element.address_number}, ${element.colony}, ${element.state}, ${element.municipality}</td>
                            <td scope="row">${element.key}</td>
                            <td scope="row">$${element.total}</td>
                            <td scope="row"><strong title="${element.motive}" style="color: ${element.color}; font-size: small;">${element.status}</strong><br><strong style="color: #00a157">${element.dateReinforcement}</strong>
                            <br><strong style="color: ${element.colorDays}">${element.diffInDays} días</strong></td>
                            <tr/>`;
                    });
                    $('#dataReinforcements').append(rows);
                    Swal.close();
                }
            }
        });

    }); // end tabReinforcements click

    function createTable() {
        $('#tabContainer').remove();
        let table =
            `<div class="col-md-12 table-responsive" style="min-height: 500px;" id="tabContainer">
            <table class="table tablesorter table-hover text-center" id="tableQuotations">
            <thead class="table-general">
            <tr>
            <th scope="col"># Orden Servicio</th>
            <th scope="col">Fecha/Hora de Servicio</th>
            <th scope="col">Técnico</th>
            <th scope="col">Cliente/Empresa</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Domicilio</th>
            <th scope="col">Plaga</th>
            <th scope="col">Costo del Refuerzo</th>
            <th scope="col">Estatus</th>
            </tr>
            </thead>
            <tbody id="dataReinforcements">
            </tbody>
            </table>
            </div>`;

        $('#masterContainer').append(table);
    } // end createTable()

});