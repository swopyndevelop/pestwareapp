import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
    let idOrder;

    $('#reinforcement').on('show.bs.modal', function(e) {
        Swal.close();
        idOrder = $(e.relatedTarget).data().id;
    });

    $("#saveReinforcementOrder").on("click",function() {
        loaderPestWare('');
        $.ajax({
            type: 'POST',
            url: route('reinforcement_store',idOrder),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (data) {
                if (data.errors){
                    Swal.close();
                    showToast('warning','Algo salió Mal','Error al enviar');
                }
                else{
                    Swal.close();
                    showToast('success-redirect','Refuerzo','Datos Guardados Correctamente','index_register');
                    $('.saveReinforcementOrder').prop('disabled', false);
                }
            }
        });
    });

});
