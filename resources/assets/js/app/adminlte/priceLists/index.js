/**
 * @author Alberto Martínez
 * @version 2021
 * register/_modalCreate.blade.php
 */

$(document).ready(function(){

    const productCodeSat = document.getElementById('productCodeSat');
    const productUnitSat = document.getElementById('productUnitSat');
    let token = $("meta[name=csrf-token]").attr("content");

    productCodeSat.onkeyup = function (ev) {
        let keyword = document.getElementById('productCodeSat').value;
        findKeysProductsSat(keyword);
    }

    productUnitSat.onkeyup = function (ev) {
        let keyword = document.getElementById('productUnitSat').value;
        findUnitsProductsSat(keyword);
    }

    function findKeysProductsSat(keyword) {
        if (keyword !== '') {
            $.ajax({
                type: 'GET',
                url: route('catalogs_sat_products_services', keyword),
                data: {
                    _token: token
                },
                success: function(data) {
                    let productCodeSatList = $('#productCodeSatList');
                    productCodeSatList.fadeIn();
                    productCodeSatList.html(data);
                }
            });
        }
    }

    function findUnitsProductsSat(keyword) {
        if (keyword !== '') {
            $.ajax({
                type: 'GET',
                url: route('catalogs_sat_products_units', keyword),
                data: {
                    _token: token
                },
                success: function(data) {
                    let productUnitSatList = $('#productUnitSatList');
                    productUnitSatList.fadeIn();
                    productUnitSatList.html(data);
                }
            });
        }
    }

    $(document).on('click', 'li#getdataProductKey', function() {
        let all = $(this);
        document.getElementById('productCodeSatId').value = all.attr('data-id');
        let productCodeSat = $('#productCodeSat');
        let productCodeSatList =  $('#productCodeSatList');
        productCodeSat.val(all.text());
        productCodeSatList.fadeOut();
    });

    $(document).on('click', 'li#getdataProductUnit', function() {
        let all = $(this);
        document.getElementById('productUnitSatId').value = all.attr('data-id');
        let productUnitSat = $('#productUnitSat');
        let productUnitSatList =  $('#productUnitSatList');
        productUnitSat.val(all.text());
        productUnitSatList.fadeOut();
    });

});
