/**
 * @author Manuel Mendoza
 * @version 2021
 * register/_deleteQuoteCover.blade.php
 */
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    $('.openModalDeleteQuoteCover').click(function () {
        let all = $(this);
        let dataId = all.attr('data-id');

        //START Delete QuoteCover
        $('#deleteQuoteCoverButton').click(function(){
            deleteQuoteCover(dataId);
        });
    });

    function deleteQuoteCover(dataId){
        loaderPestWare('Eliminando Portada Cotización...');
        $.ajax({
            type: 'post',
            url: route('delete_quote_cover_list_price'),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                dataId: dataId
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error al guardar', data.message);
                }
                else if (data.code == 201){
                    Swal.close();
                    showToast('success-redirect', 'Cotización Eliminada', data.message, 'list_prices');
                }
            }
        });
    }

 });
