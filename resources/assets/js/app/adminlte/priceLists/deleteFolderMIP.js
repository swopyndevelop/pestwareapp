/**
 * @author Manuel Mendoza
 * @version 2021
 * register/_deleteQuoteCover.blade.php
 */
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    $('.openModalDeleteFolderMip').click(function () {
        let all = $(this);
        let dataId = all.attr('data-id');

        //START Delete QuoteCover
        $('#deleteFolderMipButton').click(function(){
            deleteFolderMip(dataId);
        });
    });

    function deleteFolderMip(dataId){
        loaderPestWare('Eliminando Carpeta MIP...');
        $.ajax({
            type: 'post',
            url: route('delete_folder_mip_list_price'),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                dataId: dataId
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error al guardar', data.message);
                }
                else if (data.code == 201){
                    Swal.close();
                    showToast('success-redirect', 'Carpeta MIP', data.message, 'list_prices');
                }
            }
        });
    }

 });
