/**
 * @author Alberto Martínez
 * @version 2021
 * myaccount/_modalDataBilling.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {

    // Global vars
    let token = $("meta[name=csrf-token]").attr("content");
    const validateRfc = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/;

    // Load data selects.
    getCatalogsTypeCfdiSAT();

    function getCatalogsTypeCfdiSAT() {
        $.ajax({
            type: 'GET',
            url: route('catalogs_sat_type_cfdi'),
            data: {
                _token: token,
            },
            success: function(response) {
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    const cfdiType = document.getElementById('cfdiType').value;
                    let select = $('#typeCfdiCustomerMainNew');
                    select.empty();
                    const {typeCfdi} = response;
                    select.append(`<option value="0" selected disabled>Seleccionar Uso de la Factura:</option>`);
                    typeCfdi.forEach((element) => {
                        select.append(`<option ${element.Value === cfdiType ? 'selected': ''} value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                    });
                }
            }
        });
    }

    // Create product billing.
    const btnAddCustomer = document.getElementById('saveCustomerBilling');
    btnAddCustomer.addEventListener('click', function () {
        const billing = document.getElementById('billingCustomerMainNew').value;
        const rfc = document.getElementById('rfcCustomerMainNew').value;
        const email = document.getElementById('emailBillingCustomerMainNew').value;
        const address = document.getElementById('address').value;
        const exteriorNumber = document.getElementById('exteriorNumber').value;
        const interiorNumber = document.getElementById('interiorNumber').value;
        const postalCode = document.getElementById('postalCode').value;
        const colony = document.getElementById('colony').value;
        const municipality = document.getElementById('municipality').value;
        const state = document.getElementById('state').value;
        const cfdiType = document.getElementById('typeCfdiCustomerMainNew').value;

        // Validations
        if (billing.length === 0) showToast('warning', 'Razón Social', 'Debes ingresar la razón social');
        else if (rfc.length === 0) showToast('warning', 'RFC', 'Debes ingresar un RFC');
        else if (!validateRfc.test(rfc)) showToast('warning','RFC','El RFC no cumple con el formato requerido de la facturación.');
        else if (cfdiType == 0) showToast('warning','Uso del Cfdi','Debes seleccionar el uso de la factura.');
        else {
            const dataBilling = {
                _token: token,
                billing: billing,
                rfc: rfc,
                email: email,
                address: address,
                exteriorNumber: exteriorNumber,
                interiorNumber: interiorNumber,
                postalCode: postalCode,
                colony: colony,
                municipality: municipality,
                state: state,
                cfdiType: cfdiType
            };
            companyDataBilling(dataBilling);
        }
    });

    function companyDataBilling(dataBilling) {
        loaderPestWare('Guardando Datos...');
        $.ajax({
            type: 'POST',
            url: route('store_company_data_billing'),
            data: dataBilling,
            success: function(response) {
                Swal.close();
                if (response.code === 500) showToast('warning', 'Error', response.message);
                else {
                    showToast('success-redirect', 'Datos Facturación', response.message, 'index_invoices_my_account');
                }
            }
        });
    }

});
