/**
 * @author Alberto Martínez
 * @version 23/08/2021
 * customers/_modalEditServicesBranch.blade.php | update all services by customer branch id.
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    let servicesDB = [];
    let customerBranchId;
    let techniciansDB = [];
    let selectAllTechnicians = $('#techniciansAll');
    let timeInitial = $('#timeInitial');
    let timeFinal = $('#timeFinal');
    let deleteOrderAll = $('#deleteOrderAll');

    $('#modalEditServicesByBranch').on('show.bs.modal', function(e) {
        customerBranchId = $(e.relatedTarget).data().id;
        let customerName = $(e.relatedTarget).data().name;
        let customerNameBranch = $(e.relatedTarget).data().customername;
        $('#labelTitleBranch').html(`${customerName} | ${customerNameBranch}`);
        getServicesScheduleByBranchId(customerBranchId);
    });
    
    function getServicesScheduleByBranchId(branchId) {
        loaderPestWare('Cargando servicios...');
        $('#tableBodyDetailServicesBranch td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('services_by_branch', branchId),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableBodyDetailServicesBranch');
                    const {services, technicians, deletePermissionCan, deletePermissionHasRole} = response;
                    techniciansDB = technicians;

                    selectAllTechnicians.empty();

                    technicians.forEach((element) => {
                        // Load perimeters select
                        let option = `<option value="${element.id}"`;
                        option = option + `>${element.name}</option>`;
                        selectAllTechnicians.append(option);
                    });

                    services.forEach((element) => {
                        const {id, id_service_order, initial_date, final_date, initial_hour, final_hour, id_technician} = element;
                        servicesDB.push({
                            id: id,
                            folio: id_service_order,
                            initial_date: initial_date,
                            final_date: final_date,
                            initial_hour: initial_hour,
                            final_hour: final_hour,
                            id_technician: id_technician
                        });

                        let optionsTechnicians = '';
                        technicians.forEach((technician) => {
                            optionsTechnicians += `<option value="${technician.id}" ${id_technician === technician.id ? 'selected': ''}>${technician.name}</option>`;
                        });

                        rows += `<tr>
                                <td class="text-center text-primary text-bold">${id_service_order}</td>
                                <td class="text-center"><input class="form-control" type="date" id="initialDate-${id}" value="${initial_date}"></td>
                                <td class="text-center"><input class="form-control" type="date" id="finalDate-${id}" value="${final_date}"></td>
                                <td class="text-center"><input class="form-control" type="time" id="initialHour-${id}" value="${initial_hour}"></td>
                                <td class="text-center"><input class="form-control" type="time" id="finalHour-${id}" value="${final_hour}"></td>
                                <td class="text-center">
                                    <select class="form-control" id="technician-${id}">${optionsTechnicians}</select>
                                </td>
                                ${deletePermissionCan || deletePermissionHasRole ? `<td class="text-center"><input class="form-check-input" type="checkbox" id="deleteOrder-${id}"></td>` : ''}
                        </tr>`;
                    });
                    table.append(rows);
                    Swal.close();
                }
            }
        });
    }

    // Events Technician
    selectAllTechnicians.on('change', function() {
        servicesDB.forEach((row) => {
            let select = $(`#technician-${row.id}`).empty();
            techniciansDB.forEach((element) => {
                let option = `<option value="${element.id}" ${element.id == this.value ? 'selected': ''}`;
                option = option + `>${element.name}</option>`;
                select.append(option);
            });
        });
    });

    // Events Time Initial
    timeInitial.on('change', function() {
        servicesDB.forEach((row) => {
            $(`#initialHour-${row.id}`).val(timeInitial.val());
        });
    });

    // Events Time Final
    timeFinal.on('change', function() {
        servicesDB.forEach((row) => {
            $(`#finalHour-${row.id}`).val(timeFinal.val());
        });
    });

    // Events Delete Order
    deleteOrderAll.on('change', function() {
        servicesDB.forEach((row) => {
            $(`#deleteOrder-${row.id}`).prop('checked', $(this).prop('checked'));
        });
    });

    $('#saveAllEditServicesBranch').on('click', function(event) {

        let dataServices = [];

        servicesDB.forEach((row) => {
            let id = row.id;
            let technicianId = $(`#technician-${row.id} option:selected`).val();
            let timeInitialId = $(`#initialHour-${row.id}`).val();
            let timeFinalId = $(`#finalHour-${row.id}`).val();
            let initialDateId = $(`#initialDate-${row.id}`).val();
            let finalDateId = $(`#finalDate-${row.id}`).val();
            let deleteOrderId = $(`#deleteOrder-${row.id}`).prop('checked');
            dataServices.push({
                id: id,
                technicianId: technicianId,
                timeInitialId: timeInitialId,
                timeFinalId: timeFinalId,
                initialDateId: initialDateId,
                finalDateId: finalDateId,
                deleteOrderId: deleteOrderId,
            });
        });

        // save payments with ajax
        saveServicesAll(dataServices);

    });

    function saveServicesAll(data) {
        loaderPestWare('Guardando Servicios');
        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            url: route('update_by_branch'),
            type: "POST",
            data: {
                customerBranchId: customerBranchId,
                events: JSON.stringify(data),
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error','Error al guardar',data.message);
                } else if (data.code == 201) {
                    Swal.close();
                    showToast('success-redirect','Datos Actualizados.' ,data.message,'calendario');
                    $("#saveAllEditServicesBranch").attr('disabled', true);
                }
            }
        });
    }

});
