/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 04/04/2021
 * customers/_modalChangePassword.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

   document.getElementById('divCustomerMain').style.display = 'none';

   // Load selects data SAT
   getCatalogsPaymentFormsSAT();
   getCatalogsPaymentMethodsSAT();
   getCatalogsTypeCfdiSAT();
   getCatalogsRegimensSAT();

   let nameCustomerMain, contactCustomerMain, phoneCustomerBranch, phoneMainCustomerBranch, emailCustomerMain, addressCustomerMain,
       addressNumberCustomerMain, colonyCustomerMain, municipalityCustomerMain, stateCustomerMain, showPriceOrderService,
       daysExpirationCertificateCustomer, sourceOrigin, establishment, billingCustomerMain, rfcCustomerMain,
       portalCustomerMain, contactTwoCustomerMain, contactTwoPhoneCustomerMain, isMainCheck, customerMainSelect,
       emailBilling, typeCfdiCustomerMainNew, methodPayCustomerMainNew, wayPayCustomerMainNew, modeBilling, codeServiceSat,
       unitServiceSat, amountByInvoice, descriptionBilling, quantityInvoices, dateInvoice, productCodeSat, productUnitSat, regimeFiscalCustomerMainNew;

   $("#sendCreateCustomerMainSave").click(function() {
      nameCustomerMain = $('#nameCustomerMainNew').val().trim();
      contactCustomerMain = $('#contactCustomerMainNew').val().trim();
      phoneCustomerBranch = $('#phoneCustomerBranchNew').val().trim();
      phoneMainCustomerBranch = $('#phoneMainCustomerBranchNew').val().trim();
      emailCustomerMain = $('#emailCustomerMainNew').val().trim();
      addressCustomerMain = $('#addressCustomerMainNew').val().trim();
      addressNumberCustomerMain = $('#addressNumberCustomerMainNew').val().trim();
      colonyCustomerMain = $('#colonyCustomerMainNew').val().trim();
      municipalityCustomerMain = $('#municipalityCustomerMainNew').val().trim();
      stateCustomerMain = $('#stateCustomerMainNew').val().trim();
      billingCustomerMain = $('#billingCustomerMainNew').val();
      rfcCustomerMain = $('#rfcCustomerMainNew').val();
      contactTwoCustomerMain = $('#contactTwoCustomerMainNew').val();
      contactTwoPhoneCustomerMain = $('#contactTwoPhoneCustomerMainNew').val();
      showPriceOrderService = document.getElementById('showPriceOrderServiceNew').checked;
      portalCustomerMain = document.getElementById('portalCustomerMainNew').checked;
      isMainCheck = document.getElementById('isMainCheck').checked;
      customerMainSelect = $("#customerMainSelect option:selected").val();
      daysExpirationCertificateCustomer = $('#daysExpirationCertificateCustomerNew').val().trim();
      sourceOrigin = $("#sourceOriginNew option:selected").val();
      establishment = $("#establishmentNew option:selected").val();
      emailBilling = $('#emailBillingCustomerMainNew').val();
      typeCfdiCustomerMainNew = $('#typeCfdiCustomerMainNew option:selected').val();
      regimeFiscalCustomerMainNew = $('#regimeFiscalCustomerMainNew option:selected').val();
      methodPayCustomerMainNew = $('#methodPayCustomerMainNew option:selected').val();
      wayPayCustomerMainNew = $('#wayPayCustomerMainNew option:selected').val();
      modeBilling = $('#modeBilling option:selected').val();
      codeServiceSat = $('#productCodeSat').val();
      unitServiceSat = $('#productUnitSat').val();
      amountByInvoice = $('#amountInvoice').val();
      descriptionBilling = $('#descriptionBilling').val();
      quantityInvoices = $('#quantityInvoice').val();
      dateInvoice = $('#dateInvoice').val();
      productCodeSat = document.getElementById('productCodeSatId').value;
      productUnitSat = document.getElementById('productUnitSatId').value;

      if (nameCustomerMain.trim().length === 0) showToast('warning','Nombre (Cliente)','Debes ingresar el nombre del Cliente');
      else if (portalCustomerMain == true && phoneCustomerBranch.trim().length == 0) showToast('warning','Teléfono (Cliente)','Debes ingresar el número del Cliente para crear el portal');
      else if (parseInt(modeBilling) === 3) {
         if (parseInt(typeCfdiCustomerMainNew) === 0) showToast('warning', 'Uso de Cfdi', 'Debes seleccionar el uso de la factura.');
         else if (parseInt(regimeFiscalCustomerMainNew) === 0) showToast('warning', 'Regimen Fiscal', 'Debes seleccionar un regimen fiscal.');
         else if (parseInt(methodPayCustomerMainNew) === 0) showToast('warning', 'Método de Pago', 'Debes seleccionar un método de pago.');
         else if (parseInt(wayPayCustomerMainNew) === 0) showToast('warning', 'Forma de Pago', 'Debes seleccionar una forma de pago.');
         else if (codeServiceSat.trim().length === 0) showToast('warning', 'Servicio SAT', 'Debes ingresar un código de servicio asociado al SAT.');
         else if (unitServiceSat.trim().length === 0) showToast('warning', 'Unidad SAT', 'Debes ingresar un código de unidad asociado al SAT.');
         else if (amountByInvoice.trim().length === 0) showToast('warning', 'Monto Establecido', 'Debes ingresar un monto para cada factura.');
         else if (quantityInvoices.trim().length === 0) showToast('warning', 'Plazo (Cantidad de Facturas)', 'Debes ingresar por lo menos 1 (cantidad de facturas a programar).');
         else if (descriptionBilling.trim().length === 0) showToast('warning', 'Descripción de Facturación', 'Debes ingresar una descripción para el concepto de la factura.');
         else createCustomerMain();
      }
      else createCustomerMain();
   });

   function createCustomerMain(){
      const address = document.getElementById('addressBilling').value;
      const exteriorNumber = document.getElementById('exteriorNumberBilling').value;
      const interiorNumber = document.getElementById('interiorNumberBilling').value;
      const postalCode = document.getElementById('postalCodeBilling').value;
      const colony = document.getElementById('colonyBilling').value;
      const municipality = document.getElementById('municipalityBilling').value;
      const state = document.getElementById('stateBilling').value;
      loaderPestWare('');
      $.ajax({
         type: 'POST',
         url: route('customers_create_customer_main'),
         data: {
            _token: $("meta[name=csrf-token]").attr("content"),
            nameCustomerMain: nameCustomerMain,
            contactCustomerMain: contactCustomerMain,
            phoneCustomerBranch: phoneCustomerBranch,
            phoneMainCustomerBranch: phoneMainCustomerBranch,
            emailCustomerMain: emailCustomerMain,
            addressCustomerMain: addressCustomerMain,
            addressNumberCustomerMain: addressNumberCustomerMain,
            colonyCustomerMain: colonyCustomerMain,
            municipalityCustomerMain: municipalityCustomerMain,
            stateCustomerMain: stateCustomerMain,
            billingCustomerMain: billingCustomerMain,
            rfcCustomerMain: rfcCustomerMain,
            contactTwoCustomerMain: contactTwoCustomerMain,
            contactTwoPhoneCustomerMain: contactTwoPhoneCustomerMain,
            showPriceOrderService: showPriceOrderService,
            portalCustomerMain: portalCustomerMain,
            daysExpirationCertificateCustomer: daysExpirationCertificateCustomer,
            sourceOrigin: sourceOrigin,
            establishment: establishment,
            emailBilling: emailBilling,
            typeCfdiCustomerMainNew: typeCfdiCustomerMainNew,
            methodPayCustomerMainNew: methodPayCustomerMainNew,
            wayPayCustomerMainNew: wayPayCustomerMainNew,
            modeBilling: modeBilling,
            codeServiceSat: productCodeSat,
            codeServiceNameSat: codeServiceSat,
            unitServiceSat: productUnitSat,
            amountByInvoice: amountByInvoice,
            descriptionBilling: descriptionBilling,
            quantityInvoices: quantityInvoices,
            dateInvoice: dateInvoice,
            address: address,
            exteriorNumber: exteriorNumber,
            interiorNumber: interiorNumber,
            postalCode: postalCode,
            colony: colony,
            municipality: municipality,
            state: state,
            isMainCheck: isMainCheck,
            customerMainSelect: customerMainSelect,
            regimeFiscalCustomerMainNew: regimeFiscalCustomerMainNew
         },
         success: function (data) {
            if (data.code == 500){
               Swal.close();
               showToast('error', 'Error', data.message);
            }
            else {
               Swal.close();
               showToast('success-redirect', 'Cliente', data.message, 'index_customers');
               $("#sendCreateCustomerMainSave").attr("disabled", true);
            }
         }
      });
   }

   function getCatalogsPaymentFormsSAT() {
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_payment_forms'),
         data: {
            _token: token,
         },
         success: function(response) {
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#wayPayCustomerMainNew');
               select.empty();
               const {paymentForms} = response;
               select.append(`<option value="0" selected disabled>Selecciona una Forma de Pago:</option>`);
               paymentForms.forEach((element) => {
                  select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
               });
            }
         }
      });
   }

   function getCatalogsPaymentMethodsSAT() {
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_payment_methods'),
         data: {
            _token: token,
         },
         success: function(response) {
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#methodPayCustomerMainNew');
               select.empty();
               const {paymentMethods} = response;
               select.append(`<option value="0" selected disabled>Selecciona un Método de Pago:</option>`);
               paymentMethods.forEach((element) => {
                  select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
               });
            }
         }
      });
   }

   function getCatalogsTypeCfdiSAT() {
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_type_cfdi'),
         data: {
            _token: token,
         },
         success: function(response) {
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#typeCfdiCustomerMainNew');
               select.empty();
               const {typeCfdi} = response;
               select.append(`<option value="0" selected disabled>Seleccionar Uso de la Factura:</option>`);
               typeCfdi.forEach((element) => {
                  select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
               });
            }
         }
      });
   }

   function getCatalogsRegimensSAT() {
      loaderPestWare('');
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_regimens'),
         data: {
            _token: token,
            jobCenterId: 507
         },
         success: function(response) {
            Swal.close();
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#regimeFiscalCustomerMainNew');
               select.empty();
               const {regimens, fiscalRegime} = response;
               if (fiscalRegime == null) {
                  select.append(`<option value="0" selected disabled>Selecciona un Régimen fiscal:</option>`);
               }
               regimens.forEach((element) => {
                  if (fiscalRegime === element.Value) {
                     select.append(`<option selected value="${element.Value}">${element.Name}</option>`);
                  } else select.append(`<option value="${element.Value}">${element.Name}</option>`);
               });
            }
         }
      });
   }

   // Events
   const productCodeSatSearch = document.getElementById('productCodeSat');
   const productUnitSatSearch = document.getElementById('productUnitSat');
   let token = $("meta[name=csrf-token]").attr("content");

   productCodeSatSearch.onkeyup = function (ev) {
      let keyword = document.getElementById('productCodeSat').value;
      findKeysProductsSat(keyword);
   }

   productUnitSatSearch.onkeyup = function (ev) {
      let keyword = document.getElementById('productUnitSat').value;
      findUnitsProductsSat(keyword);
   }

   function findKeysProductsSat(keyword) {
      if (keyword !== '') {
         $.ajax({
            type: 'GET',
            url: route('catalogs_sat_products_services', keyword),
            data: {
               _token: token
            },
            success: function(data) {
               let productCodeSatList = $('#productCodeSatList');
               productCodeSatList.fadeIn();
               productCodeSatList.html(data);
            }
         });
      }
   }

   function findUnitsProductsSat(keyword) {
      if (keyword !== '') {
         $.ajax({
            type: 'GET',
            url: route('catalogs_sat_products_units', keyword),
            data: {
               _token: token
            },
            success: function(data) {
               let productUnitSatList = $('#productUnitSatList');
               productUnitSatList.fadeIn();
               productUnitSatList.html(data);
            }
         });
      }
   }

   $(document).on('click', 'li#getdataProductKey', function() {
      let all = $(this);
      document.getElementById('productCodeSatId').value = all.attr('data-id');
      let productCodeSat = $('#productCodeSat');
      let productCodeSatList =  $('#productCodeSatList');
      productCodeSat.val(all.text());
      productCodeSatList.fadeOut();
   });

   $(document).on('click', 'li#getdataProductUnit', function() {
      let all = $(this);
      document.getElementById('productUnitSatId').value = all.attr('data-id');
      let productUnitSat = $('#productUnitSat');
      let productUnitSatList =  $('#productUnitSatList');
      productUnitSat.val(all.text());
      productUnitSatList.fadeOut();
   });


   let selectBillingMode = document.getElementById('modeBilling');
   selectBillingMode.addEventListener('change', (event) => {
      const option = event.target.value;
      if (parseInt(option) === 1) {
         document.getElementById('divInvoiceByAmount').style.display = 'none';
         document.getElementById('textConditionsCreate').style.display = 'none';
      }
      if (parseInt(option) === 2) {
         document.getElementById('divInvoiceByAmount').style.display = 'none';
         document.getElementById('textConditionsCreate').style.display = 'block';
      }
      if (parseInt(option) === 3) {
         document.getElementById('divAmountInvoice').style.display = 'block';
         document.getElementById('divQuantityInvoice').style.display = 'block';
         document.getElementById('divDateInvoice').style.display = 'block';
         document.getElementById('divInvoiceByAmount').style.display = 'block';
         document.getElementById('textConditionsCreate').style.display = 'block';
      }
      if (parseInt(option) === 4) {
         document.getElementById('divAmountInvoice').style.display = 'none';
         document.getElementById('divQuantityInvoice').style.display = 'none';
         document.getElementById('divDateInvoice').style.display = 'none';
         document.getElementById('divInvoiceByAmount').style.display = 'block';
         document.getElementById('textConditionsCreate').style.display = 'block';
      }
      if (parseInt(option) === 5) document.getElementById('divInvoiceByAmount').style.display = 'none';
   });

   $('#textConditionsCreate').click(function () {
      const message = 'Al seleccionar esta opción autoriza a PestWare App que haga el cargo para compra de folios de' +
          ' manera automática. El sistema verifica todas las mañanas la cantidad de folios disponibles para llevar a' +
          ' cabo la facturación electrónica de manera satisfactoria, de ser necesario realizará compra de paquetes de' +
          ' manera inteligente para completar la cantidad de folios requeridos a facturar.';
      Swal.fire({
         title: `<strong>Aviso y Condiciones de Facturación Automática</strong>`,
         type: 'info',
         html: message,
         showCloseButton: true,
         showCancelButton: true,
         focusConfirm: false,
         confirmButtonText:
             '<i class="fa fa-thumbs-up"></i> Aceptar',
         confirmButtonAriaLabel: 'Thumbs up, great!',
         cancelButtonText:
             '<i class="fa fa-thumbs-down"></i> Cerrar',
         cancelButtonAriaLabel: 'Cerrar',
         width: '70rem'
      }).then((result) => {
         Swal.close()
      })
   })

   $('#isMainCheck').change(function() {
      if(this.checked) document.getElementById('divCustomerMain').style.display = 'block';
      else document.getElementById('divCustomerMain').style.display = 'none';
   });

});
