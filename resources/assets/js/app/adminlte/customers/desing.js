/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 04/04/2021
 * customers/_modalCustomerUser.blade.php
 */

$(document).ready(function(){
    //select job center
    $('#selectJobCenterCustomer').on('change', function() {
        let idJobCenterCustomer = $(this).val();
        window.location.href = route('index_customers', idJobCenterCustomer);
    });

    $("#buttonShow").click(function() {
        let showCellphone = document.getElementById("showCellphone");
        if (showCellphone.style.display === "none") {
            showCellphone.style.display = "block";
        } else {
            showCellphone.style.display = "none";
        }
    });

});