/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 04/04/2021
 * customers/_modalCustomerUser.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
   let idCustomer, customerCellphone;

   $('.openModalCustomerUser').click(function () {
      let all = $(this);
      idCustomer = $(this).data("id");
      customerCellphone = $(this).data("cellphone");

      $("#sendCustomerUserSave").click(function() {
         if (customerCellphone.length < 1) showToast('warning','Teléfono Cliente','El Teléfono no puede estar vacío');
         else {
            sendCustomerUser();
         }
      });

   });

   function sendCustomerUser(){
      loaderPestWare('');
      $.ajax({
         type: 'POST',
         url: route('customers_add_user'),
         data: {
            _token: $("meta[name=csrf-token]").attr("content"),
            idCustomer: idCustomer
         },
         success: function (data) {
            if (data.code == 500){
               Swal.close();
               showToast('error', 'Error', data.message);
            }
            else{
               Swal.close();
               showToast('success-redirect', data.title, data.message, 'index_customers');
               $("#sendCustomerUserSave").attr('disabled', true);
            }
         }
      });
   }

});
