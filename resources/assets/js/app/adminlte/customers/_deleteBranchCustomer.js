/**
 * @author Manuel Mendoza
 * @version 2021
 * register/_deleteCustomer.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
    let idBranchCustomer, serviceOrderByBranch, nameBranchCustomer;
    $('#deleteCustomerBranch').on('show.bs.modal', function(e) {
        idBranchCustomer = $(e.relatedTarget).data().id;
        nameBranchCustomer = $(e.relatedTarget).data().name;
        serviceOrderByBranch = $(e.relatedTarget).data().serviceorderbybranch;
        $('#nameClientBranch').html(nameBranchCustomer);
        $('#servicesOrderCountByBranch').html(serviceOrderByBranch);
    });

    $("#saveDeleteBranchCustomer").on("click",function() {
        loaderPestWare('');
        $.ajax({
            type: 'DELETE',
            url: route('customer_branch_delete'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idBranchCustomer: idBranchCustomer
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('warning','Algo salió Mal','Error al eliminar');
                }
                else{
                    Swal.close();
                    showToast('success-redirect','Eliminar','Datos Guardados Correctamente','index_customers');
                    $('.saveDeleteBranchCustomer').prop('disabled', false);
                }
            }
        });
    });

 });
