/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 04/04/2021
 * customers/_modalChangePassword.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
   let idCustomerClient, showPriceOrderServiceBoolean, isMainCheck, userId;

   $('.openModalEditCustomerMain').click(function () {
      let all = $(this);
      idCustomerClient = $(this).data("id");
      let serviceUntSat = all.attr('data-customer-main-unit_code_billing');
      $('#customerNameTitle').html(all.attr('data-customer-main-name'));
      $('#customerCellphoneTitle').html(all.attr('data-customer-main-cellphone'));
      $('#nameCustomerMain').val(all.attr('data-customer-main-name'));
      $('#contactCustomerMain').val(all.attr('data-customer-main-establishment-name'));
      $('#phoneCustomerBranch').val(all.attr('data-customer-main-cellphone'));
      $('#phoneMainCustomerBranch').val(all.attr('data-customer-main-cellphone-main'));
      $('#emailCustomerMain').val(all.attr('data-customer-main-email'));
      $('#addressCustomerMain').val(all.attr('data-customer-main-address'));
      $('#addressNumberCustomerMain').val(all.attr('data-customer-main-address-number'));
      $('#colonyCustomerMain').val(all.attr('data-customer-main-colony'));
      $('#municipalityCustomerMain').val(all.attr('data-customer-main-municipality'));
      $('#stateCustomerMain').val(all.attr('data-customer-main-state'));
      $('#billingCustomerMain').val(all.attr('data-customer-main-billing'));
      $('#rfcCustomerMain').val(all.attr('data-customer-main-rfc'));
      $('#taxResidenceCustomerMain').val(all.attr('data-customer-main-tax-residence'));
      $('#contactTwoCustomerMain').val(all.attr('data-customer-main-contact-name'));
      $('#contactTwoPhoneCustomerMain').val(all.attr('data-customer-main-contact-phone'));
      $('#rfcCustomerMainLabel').html(all.attr('data-customer-main-rfc-country'));
      $('#emailBillingCustomerMain').val(all.attr('data-customer-main-email-billing'));
      $('#amountInvoiceEdit').val(all.attr('data-customer-main-amount_billing'));
      $('#descriptionBillingEdit').val(all.attr('data-customer-main-description_billing'));
      $('#quantityInvoiceEdit').val(all.attr('data-customer-main-no_invoices'));
      $('#dateInvoiceEdit').val(all.attr('data-customer-main-date_billing'));
      $('#productUnitSatEdit').val(all.attr('data-customer-main-unit_code_billing_name'));
      $('#productCodeSatEdit').val(all.attr('data-customer-main-service_code_billing-name'));
      $('#productCodeSatIdEdit').val(all.attr('data-customer-main-service_code_billing'));
      $('#productUnitSatIdEdit').val(all.attr('data-customer-main-unit_code_billing'));
      $('#addressBillingUpd').val(all.attr('data-customer-main-billing_street'));
      $('#exteriorNumberBillingUpd').val(all.attr('data-customer-main-billing_exterior_number'));
      $('#interiorNumberBillingUpd').val(all.attr('data-customer-main-billing_interior_number'));
      $('#postalCodeBillingUpd').val(all.attr('data-customer-main-billing_postal_code'));
      $('#colonyBillingUpd').val(all.attr('data-customer-main-billing_colony'));
      $('#municipalityBillingUpd').val(all.attr('data-customer-main-billing_municipality'));
      $('#stateBillingUpd').val(all.attr('data-customer-main-billing_state'));

      showPriceOrderServiceBoolean =  all.attr('data-customer-main-show-price');
      if (showPriceOrderServiceBoolean == 1) $("#showPriceOrderService").prop("checked", true);
      else $("#showPriceOrderService").prop("checked", false);

      isMainCheck = all.attr('data-customer-main-is-main-check');
      if (isMainCheck == 0) {
         $(function() {
            $('#customerMainSelectEdit').val(all.attr('data-customer-main-select-id'));
            $('#customerMainSelectEdit').trigger('change');
         });
         document.getElementById('divCustomerMainEdit').style.display = 'block';
         document.getElementById("isMainCheckEdit").checked = true;
      } else {
         document.getElementById('divCustomerMainEdit').style.display = 'none';
         document.getElementById("isMainCheckEdit").checked = false;
      }

      $('#daysExpirationCertificateCustomer').val(all.attr('data-customer-main-days-expiration'));
      userId = all.attr('data-customer-main-user-id');
      let showDivPortal = document.getElementById("showDivPortal");
      if (userId.length < 1) {
         showDivPortal.style.display = "block";
      }
      else {
         showDivPortal.style.display = "none";
      }

      getCatalogsTypeCfdiSAT(all.attr('data-customer-main-cfdi-type'));
      getCatalogsPaymentFormsSAT(all.attr('data-customer-main-payment-way'));
      getCatalogsPaymentMethodsSAT(all.attr('data-customer-main-payment-method'));
      getCatalogsRegimensSAT(all.attr('data-customer-main-regime-fiscal'))
      getBillingModes(all.attr('data-customer-main-billing-mode'));
      getBankAccounts(idCustomerClient);

      if (all.attr('data-customer-main-billing-mode') == 3) document.getElementById('divInvoiceByAmountEdit').style.display = 'block';

   });

   $("#sendEditCustomerMainSave").click(function() {
      let nameCustomerMain = $('#nameCustomerMain').val().trim();
      let contactCustomerMain = $('#contactCustomerMain').val().trim();
      let phoneCustomerBranch = $('#phoneCustomerBranch').val().trim();
      let phoneMainCustomerBranch = $('#phoneMainCustomerBranch').val().trim();
      let emailCustomerMain = $('#emailCustomerMain').val().trim();
      let addressCustomerMain = $('#addressCustomerMain').val().trim();
      let addressNumberCustomerMain = $('#addressNumberCustomerMain').val().trim();
      let colonyCustomerMain = $('#colonyCustomerMain').val().trim();
      let municipalityCustomerMain = $('#municipalityCustomerMain').val().trim();
      let stateCustomerMain = $('#stateCustomerMain').val().trim();
      let billingCustomerMain = $('#billingCustomerMain').val().trim();
      let rfcCustomerMain = $('#rfcCustomerMain').val().trim();
      let contactTwoCustomerMain = $('#contactTwoCustomerMain').val().trim();
      let contactTwoPhoneCustomerMain = $('#contactTwoPhoneCustomerMain').val().trim();
      let showPriceOrderService = document.getElementById('showPriceOrderService').checked;
      let daysExpirationCertificateCustomer = $('#daysExpirationCertificateCustomer').val().trim();
      let typeCfdiCustomerMainNew = $('#typeCfdiCustomerMainNewEdit option:selected').val();
      let regimeFiscalCustomerMainEdit = $('#regimeFiscalCustomerMainEdit option:selected').val();
      let methodPayCustomerMainNew = $('#methodPayCustomerMainEdit option:selected').val();
      let wayPayCustomerMainNew = $('#wayPayCustomerMainEdit option:selected').val();
      let billingMode = $('#modeBillingEdit option:selected').val();
      let emailBillingCustomerMain = $('#emailBillingCustomerMain').val().trim();
      let codeServiceSat = $('#productCodeSatEdit').val();
      let unitServiceSat = $('#productUnitSatEdit').val();
      let amountByInvoice = $('#amountInvoiceEdit').val();
      let descriptionBilling = $('#descriptionBillingEdit').val();
      let quantityInvoices = $('#quantityInvoiceEdit').val();
      let dateInvoice = $('#dateInvoiceEdit').val();
      let productCodeSat = document.getElementById('productCodeSatIdEdit').value;
      let productUnitSat = document.getElementById('productUnitSatIdEdit').value;
      let isMainCheck = document.getElementById('isMainCheckEdit').checked;
      let customerMainSelect = $("#customerMainSelectEdit option:selected").val();

      if (nameCustomerMain.trim().length === 0) showToast('warning','Nombre (Cliente)','Debes ingresar el nombre del Cliente');
      else if (portalCustomerMain == true && phoneCustomerBranch.trim().length == 0) showToast('warning','Télefono (Cliente)','Debes ingresar el número del Cliente para crear el portal');
      else if (parseInt(billingMode) === 3) {
         if (parseInt(typeCfdiCustomerMainNew) === 0) showToast('warning', 'Uso de Cfdi', 'Debes seleccionar el uso de la factura.');
         if (parseInt(regimeFiscalCustomerMainEdit) === 0) showToast('warning', 'Regimen Fiscal', 'Debes seleccionar un regimen fiscal.');
         else if (parseInt(methodPayCustomerMainNew) === 0) showToast('warning', 'Método de Pago', 'Debes seleccionar un método de pago.');
         else if (parseInt(wayPayCustomerMainNew) === 0) showToast('warning', 'Forma de Pago', 'Debes seleccionar una forma de pago.');
         else if (codeServiceSat.trim().length === 0) showToast('warning', 'Servicio SAT', 'Debes ingresar un código de servicio asociado al SAT.');
         else if (unitServiceSat.trim().length === 0) showToast('warning', 'Unidad SAT', 'Debes ingresar un código de unidad asociado al SAT.');
         else if (amountByInvoice.trim().length === 0) showToast('warning', 'Monto Establecido', 'Debes ingresar un monto para cada factura.');
         else if (quantityInvoices.trim().length === 0) showToast('warning', 'Plazo (Cantidad de Facturas)', 'Debes ingresar por lo menos 1 (cantidad de facturas a programar).');
         else if (descriptionBilling.trim().length === 0) showToast('warning', 'Descripción de Facturación', 'Debes ingresar una descripción para el concepto de la factura.');
         else {
            editCustomerMain(
                nameCustomerMain,
                contactCustomerMain,
                phoneCustomerBranch,
                phoneMainCustomerBranch,
                emailCustomerMain, addressCustomerMain,
                addressNumberCustomerMain,
                colonyCustomerMain,
                municipalityCustomerMain,
                stateCustomerMain,
                showPriceOrderService,
                daysExpirationCertificateCustomer,
                billingCustomerMain,
                rfcCustomerMain,
                contactTwoCustomerMain,
                contactTwoPhoneCustomerMain,
                typeCfdiCustomerMainNew,
                methodPayCustomerMainNew,
                wayPayCustomerMainNew,
                billingMode,
                emailBillingCustomerMain,
                codeServiceSat,
                unitServiceSat,
                amountByInvoice,
                descriptionBilling,
                quantityInvoices,
                dateInvoice,
                productCodeSat,
                productUnitSat,
                isMainCheck,
                customerMainSelect,
                regimeFiscalCustomerMainEdit
            );
         }
      }
      else editCustomerMain(
          nameCustomerMain,
          contactCustomerMain,
          phoneCustomerBranch,
          phoneMainCustomerBranch,
          emailCustomerMain, addressCustomerMain,
          addressNumberCustomerMain,
          colonyCustomerMain,
          municipalityCustomerMain,
          stateCustomerMain,
          showPriceOrderService,
          daysExpirationCertificateCustomer,
          billingCustomerMain,
          rfcCustomerMain,
          contactTwoCustomerMain,
          contactTwoPhoneCustomerMain,
          typeCfdiCustomerMainNew,
          methodPayCustomerMainNew,
          wayPayCustomerMainNew,
          billingMode,
          emailBillingCustomerMain,
          codeServiceSat,
          unitServiceSat,
          amountByInvoice,
          descriptionBilling,
          quantityInvoices,
          dateInvoice,
          productCodeSat,
          productUnitSat,
          isMainCheck,
          customerMainSelect,
          regimeFiscalCustomerMainEdit
         );
   });

   function editCustomerMain(
        nameCustomerMain,
        contactCustomerMain,
        phoneCustomerBranch,
        phoneMainCustomerBranch,
        emailCustomerMain,
        addressCustomerMain,
        addressNumberCustomerMain,
        colonyCustomerMain,
        municipalityCustomerMain,
        stateCustomerMain,
        showPriceOrderService,
        daysExpirationCertificateCustomer,
        billingCustomerMain,
        rfcCustomerMain,
        contactTwoCustomerMain,
        contactTwoPhoneCustomerMain,
        typeCfdiCustomerMainNew,
        methodPayCustomerMainNew,
        wayPayCustomerMainNew,
        modeBilling,
        emailBillingCustomerMain,
        codeServiceSat,
        unitServiceSat,
        amountByInvoice,
        descriptionBilling,
        quantityInvoices,
        dateInvoice,
        productCodeSat,
        productUnitSat,
        isMainCheck,
        customerMainSelect,
        regimeFiscalCustomerMainEdit
        ){
      loaderPestWare('');
      const address = document.getElementById('addressBillingUpd').value;
      const exteriorNumber = document.getElementById('exteriorNumberBillingUpd').value;
      const interiorNumber = document.getElementById('interiorNumberBillingUpd').value;
      const postalCode = document.getElementById('postalCodeBillingUpd').value;
      const colony = document.getElementById('colonyBillingUpd').value;
      const municipality = document.getElementById('municipalityBillingUpd').value;
      const state = document.getElementById('stateBillingUpd').value;
      $.ajax({
         type: 'POST',
         url: route('customers_edit_customer_main'),
         data: {
            _token: $("meta[name=csrf-token]").attr("content"),
            idCustomerClient: idCustomerClient,
            nameCustomerMain: nameCustomerMain,
            contactCustomerMain: contactCustomerMain,
            phoneCustomerBranch: phoneCustomerBranch,
            phoneMainCustomerBranch: phoneMainCustomerBranch,
            emailCustomerMain: emailCustomerMain,
            addressCustomerMain: addressCustomerMain,
            addressNumberCustomerMain: addressNumberCustomerMain,
            colonyCustomerMain: colonyCustomerMain,
            municipalityCustomerMain: municipalityCustomerMain,
            stateCustomerMain: stateCustomerMain,
            billingCustomerMain: billingCustomerMain,
            rfcCustomerMain: rfcCustomerMain,
            contactTwoCustomerMain: contactTwoCustomerMain,
            contactTwoPhoneCustomerMain: contactTwoPhoneCustomerMain,
            showPriceOrderService: showPriceOrderService,
            daysExpirationCertificateCustomer: daysExpirationCertificateCustomer,
            typeCfdiCustomerMainNew: typeCfdiCustomerMainNew,
            methodPayCustomerMainNew: methodPayCustomerMainNew,
            wayPayCustomerMainNew: wayPayCustomerMainNew,
            modeBilling: modeBilling,
            emailBillingCustomerMain: emailBillingCustomerMain,
            codeServiceSat: productCodeSat,
            codeServiceNameSat: codeServiceSat,
            unitServiceSat: productUnitSat,
            amountByInvoice: amountByInvoice,
            descriptionBilling: descriptionBilling,
            quantityInvoices: quantityInvoices,
            dateInvoice: dateInvoice,
            address: address,
            exteriorNumber: exteriorNumber,
            interiorNumber: interiorNumber,
            postalCode: postalCode,
            colony: colony,
            municipality: municipality,
            state: state,
            isMainCheck: isMainCheck,
            customerMainSelect: customerMainSelect,
            regimeFiscalCustomerMainEdit: regimeFiscalCustomerMainEdit
         },
         success: function (data) {
            if (data.code == 500){
               Swal.close();
               showToast('error', 'Error', data.message);
            }
            else {
               Swal.close();
               showToast('success-redirect', 'Cliente', data.message, 'index_customers');
               $("#sendEditCustomerMainSave").attr("disabled", true);
            }
         }
      });
   }


   function getCatalogsTypeCfdiSAT(typeCfdiCustomer) {
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_type_cfdi'),
         data: {
            _token: token,
         },
         success: function(response) {
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#typeCfdiCustomerMainNewEdit');
               select.empty();
               const {typeCfdi} = response;
               if (typeCfdiCustomer.length === 0) {
                  select.append(`<option value="0" selected disabled>Seleccionar Uso de la Factura:</option>`);
               }
               typeCfdi.forEach((element) => {
                  if (typeCfdiCustomer === element.Value) {
                     select.append(`<option selected value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                  } else select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
               });
            }
         }
      });
   }

   function getCatalogsPaymentFormsSAT(paymentForm) {
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_payment_forms'),
         data: {
            _token: token,
         },
         success: function(response) {
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#wayPayCustomerMainEdit');
               select.empty();
               const {paymentForms} = response;
               if (paymentForm.length === 0) {
                  select.append(`<option value="0" selected disabled>Selecciona una Forma de Pago:</option>`);
               }
               paymentForms.forEach((element) => {
                  if (paymentForm === element.Value) {
                     select.append(`<option selected value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                  } else select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
               });
            }
         }
      });
   }

   function getCatalogsPaymentMethodsSAT(paymentMethod) {
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_payment_methods'),
         data: {
            _token: token,
         },
         success: function(response) {
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#methodPayCustomerMainEdit');
               select.empty();
               const {paymentMethods} = response;
               if (paymentMethod.length === 0) {
                  select.append(`<option value="0" selected disabled>Selecciona un Método de Pago:</option>`);
               }
               paymentMethods.forEach((element) => {
                  if (paymentMethod === element.Value) {
                     select.append(`<option selected value="${element.Value}">${element.Value} - ${element.Name}</option>`);
                  } else select.append(`<option value="${element.Value}">${element.Value} - ${element.Name}</option>`);
               });
            }
         }
      });
   }

   function getCatalogsRegimensSAT(fiscalRegimeCustomer) {
      loaderPestWare('');
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_regimens'),
         data: {
            _token: token,
            jobCenterId: 507
         },
         success: function(response) {
            Swal.close();
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#regimeFiscalCustomerMainEdit');
               select.empty();
               const {regimens, fiscalRegime} = response;
               if (fiscalRegimeCustomer == null) {
                  select.append(`<option value="0" selected disabled>Selecciona un Régimen fiscal:</option>`);
               }
               regimens.forEach((element) => {
                  if (fiscalRegimeCustomer === element.Value) {
                     select.append(`<option selected value="${element.Value}">${element.Name}</option>`);
                  } else select.append(`<option value="${element.Value}">${element.Name}</option>`);
               });
            }
         }
      });
   }

   function getBillingModes(billingMode) {
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_sat_billing_modes'),
         data: {
            _token: token,
         },
         success: function(response) {
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#modeBillingEdit');
               select.empty();
               const {billingModes} = response;
               billingModes.forEach((element) => {
                  if (billingMode == element.id) {
                     select.append(`<option selected value="${element.id}">${element.name}</option>`);
                  } else select.append(`<option value="${element.id}">${element.name}</option>`);
               });
               if (billingMode == 0) document.getElementById('textConditions').style.display = 'none';
            }
         }
      });
   }

   function getBankAccounts(customerId) {
      let token = $("meta[name=csrf-token]").attr("content");
      $.ajax({
         type: 'GET',
         url: route('catalogs_bank_accounts', customerId),
         data: {
            _token: token,
         },
         success: function(response) {
            if (response.code === 500) {
               showToast('warning', 'Error', response.message);
            } else {
               let select = $('#bankAccountsEdit');
               select.empty();
               const {bankAccounts} = response;
               bankAccounts.forEach((element) => {
                  if (element.is_default  == 1) {
                     select.append(`<option selected value="${element.id}">${element.bank} - ${element.account}</option>`);
                  } else select.append(`<option value="${element.id}">${element.bank} - ${element.account}</option>`);
               });
            }
         }
      });
   }

   // Events
   const productCodeSatSearch = document.getElementById('productCodeSatEdit');
   const productUnitSatSearch = document.getElementById('productUnitSatEdit');
   let token = $("meta[name=csrf-token]").attr("content");

   productCodeSatSearch.onkeyup = function (ev) {
      let keyword = document.getElementById('productCodeSatEdit').value;
      findKeysProductsSat(keyword);
   }

   productUnitSatSearch.onkeyup = function (ev) {
      let keyword = document.getElementById('productUnitSatEdit').value;
      findUnitsProductsSat(keyword);
   }

   function findKeysProductsSat(keyword) {
      if (keyword !== '') {
         $.ajax({
            type: 'GET',
            url: route('catalogs_sat_products_services', keyword),
            data: {
               _token: token
            },
            success: function(data) {
               let productCodeSatList = $('#productCodeSatListEdit');
               productCodeSatList.fadeIn();
               productCodeSatList.html(data);
            }
         });
      }
   }

   function findUnitsProductsSat(keyword) {
      if (keyword !== '') {
         $.ajax({
            type: 'GET',
            url: route('catalogs_sat_products_units', keyword),
            data: {
               _token: token
            },
            success: function(data) {
               let productUnitSatList = $('#productUnitSatListEdit');
               productUnitSatList.fadeIn();
               productUnitSatList.html(data);
            }
         });
      }
   }

   $(document).on('click', 'li#getdataProductKey', function() {
      let all = $(this);
      document.getElementById('productCodeSatIdEdit').value = all.attr('data-id');
      let productCodeSat = $('#productCodeSatEdit');
      let productCodeSatList =  $('#productCodeSatListEdit');
      productCodeSat.val(all.text());
      productCodeSatList.fadeOut();
   });

   $(document).on('click', 'li#getdataProductUnit', function() {
      let all = $(this);
      document.getElementById('productUnitSatIdEdit').value = all.attr('data-id');
      let productUnitSat = $('#productUnitSatEdit');
      let productUnitSatList =  $('#productUnitSatListEdit');
      productUnitSat.val(all.text());
      productUnitSatList.fadeOut();
   });


   let selectBillingMode = document.getElementById('modeBillingEdit');
   selectBillingMode.addEventListener('change', (event) => {
      const option = event.target.value;
      if (parseInt(option) === 1) {
         document.getElementById('divInvoiceByAmountEdit').style.display = 'none';
         document.getElementById('textConditions').style.display = 'none';
      }
      if (parseInt(option) === 2) {
         document.getElementById('divInvoiceByAmountEdit').style.display = 'none';
         document.getElementById('textConditions').style.display = 'block';
      }
      if (parseInt(option) === 3) {
         document.getElementById('divAmountInvoiceEdit').style.display = 'block';
         document.getElementById('divQuantityInvoiceEdit').style.display = 'block';
         document.getElementById('divDateInvoiceEdit').style.display = 'block';
         document.getElementById('divInvoiceByAmountEdit').style.display = 'block';
         document.getElementById('textConditions').style.display = 'block';
      }
      if (parseInt(option) === 4) {
         document.getElementById('divAmountInvoiceEdit').style.display = 'none';
         document.getElementById('divQuantityInvoiceEdit').style.display = 'none';
         document.getElementById('divDateInvoiceEdit').style.display = 'none';
         document.getElementById('divInvoiceByAmountEdit').style.display = 'block';
         document.getElementById('textConditions').style.display = 'block';
      }
      if (parseInt(option) === 5) document.getElementById('divInvoiceByAmountEdit').style.display = 'none';
   });

   $('#addAccountBank').click(function () {

      const bank = document.getElementById('bank').value;
      const account = document.getElementById('account').value;

      if (bank.length <= 0) showToast('warning','Banco','Favor de ingresar un banco valido.');
      else if(account.length !== 4) showToast('warning', 'Número de Cuenta', 'Favor de ingresar los 4 últimos dígitos de la cuenta.');
      else saveNewBankAccount(bank, account);

   });

   $('#textConditions').click(function () {
      const message = 'Al seleccionar esta opción autoriza a PestWare App que haga el cargo para compra de folios de' +
          ' manera automática. El sistema verifica todas las mañanas la cantidad de folios disponibles para llevar a' +
          ' cabo la facturación electrónica de manera satisfactoria, de ser necesario realizará compra de paquetes de' +
          ' manera inteligente para completar la cantidad de folios requeridos a facturar.';
      Swal.fire({
         title: `<strong>Aviso y Condiciones de Facturación Automática</strong>`,
         type: 'info',
         html: message,
         showCloseButton: true,
         showCancelButton: true,
         focusConfirm: false,
         confirmButtonText:
             '<i class="fa fa-thumbs-up"></i> Aceptar',
         confirmButtonAriaLabel: 'Thumbs up, great!',
         cancelButtonText:
             '<i class="fa fa-thumbs-down"></i> Cerrar',
         cancelButtonAriaLabel: 'Cerrar',
         width: '70rem'
      }).then((result) => {
         Swal.close()
      })
   })

   function saveNewBankAccount(bank, account) {
      loaderPestWare('Guardando cuenta...')
      $.ajax({
         type: 'POST',
         url: route('add_bank_accounts'),
         data:{
            _token: $("meta[name=csrf-token]").attr("content"),
            customerId: idCustomerClient,
            bank: bank,
            account: account
         },
         success: function (data) {
            if (data.code == 500){
               Swal.close();
               showToast('error', 'Cuenta', data.message);
               $('#addAccountBankModal').click();
            }
            else {
               showToast('success', 'Cuenta Bancaria', 'Se registro la nueva cuenta.');
               let select = $('#bankAccountsEdit');
               select.empty();
               const {bankAccounts} = data;
               bankAccounts.forEach((element) => {
                  if (element.is_default  == 1) {
                     select.append(`<option selected value="${element.id}">${element.bank} - ${element.account}</option>`);
                  } else select.append(`<option value="${element.id}">${element.bank} - ${element.account}</option>`);
               });
               Swal.close();
               $('#addAccountBankModal').click();
            }
            document.getElementById('bank').value = '';
            document.getElementById('account').value = '';
         }
      });
   }

   $('#isMainCheckEdit').change(function() {
      if(this.checked) document.getElementById('divCustomerMainEdit').style.display = 'block';
      else document.getElementById('divCustomerMainEdit').style.display = 'none';
   });

});
