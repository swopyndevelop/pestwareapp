/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 04/04/2021
 * customers/_modalChangePassword.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
   let idCustomer;

   $('.openModalChangePassword').click(function () {
      let all = $(this);
      idCustomer = $(this).data("id");

      //console.log(idCustomer);
      //console.log(all.attr('data-id'));

      $("#sendChangePasswordSave").click(function() {
         sendChangePassword();
      });
   });

   function sendChangePassword(){
      loaderPestWare('');
      $.ajax({
         type: 'POST',
         url: route('customers_change_password'),
         data: {
            _token: $("meta[name=csrf-token]").attr("content"),
            idCustomer: idCustomer
         },
         success: function (data) {
            if (data.errors){
               Swal.close();
               showToast('error', 'Error', data.message);
            }
            else{
               Swal.close();
               showToast('success-redirect', 'Cuenta de Portal', data.message, 'index_customers');
               $("#sendChangePasswordSave").attr('disabled', true);
            }
         }
      });
   }

});
