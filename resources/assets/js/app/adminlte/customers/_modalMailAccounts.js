/**
 * @author Alberto Martínez
 * @version 14/09/2021
 * customers/_modalMailAccounts.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    let idCustomer, customerName, idAccount;

    $('#modalMailAccounts').on('show.bs.modal', function(e) {
        idCustomer = $(e.relatedTarget).data().id;
        customerName = $(e.relatedTarget).data().customer;
        document.getElementById('labelTitleCustomerModalMails').innerText = customerName;
        getMailAccountsByCustomer(idCustomer);
    });

    function getMailAccountsByCustomer(idCustomer) {
        loaderPestWare('Cargando cuentas...');
        $('#tableMailAccounts td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_mail_accounts_by_customer', idCustomer),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableMailAccounts');
                    const {accounts} = response;

                    accounts.forEach((element) => {
                        const {id,document, name, job, email} = element;
                        rows += `<tr>
                            <td class="text-center">${document}</td>
                            <td class="text-center">${name}</td>
                            <td class="text-center">${job}</td>
                            <td class="text-center text-primary text-bold">
                                <p class="text-default">
                                    <strong>
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    </strong>
                                    <a href="mailto:${email}">${email}</a>
                                </p>
                            </td>
                            <td class="text-center">
                                <a data-toggle="modal" data-target="#editMailAccount" style="cursor: pointer"
                                    data-id="${id}"
                                    data-archive="${document}"
                                    data-name="${name}"
                                    data-job="${job}"
                                    data-email="${email}">
                                    <i class="fa fa-pencil text-info" aria-hidden="true"></i>
                                </a>
                                <a data-toggle="modal" data-target="#deleteModalMailAccount" style="cursor: pointer; margin-left: 3px"
                                    data-id="${id}"
                                    data-email="${email}">
                                    <i class="fa fa-trash text-danger" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>`;
                    });
                    table.append(rows);
                    Swal.close();
                }
            }
        });
    }

    $('#editMailAccount').on('show.bs.modal', function(e) {
        idAccount = $(e.relatedTarget).data().id;
        let documentUpd = $(e.relatedTarget).data().archive;
        let nameUpd = $(e.relatedTarget).data().name;
        let jobUpd = $(e.relatedTarget).data().job;
        let emailUpd = $(e.relatedTarget).data().email;
        // set data inputs edit modal.
        let option = 0;
        if (documentUpd === 'Orden de Servicio (pdf)') option = 1;
        if (documentUpd === 'Inspección de Estaciones (pdf)') option = 2;
        if (documentUpd === 'Inspección de Áreas (pdf)') option = 3;
        if (documentUpd === 'Factura electrónica (pdf y xml)') option = 4;

        document.getElementById('modalEditTitleMailAccount').innerText = customerName;
        document.getElementById('documentUpd').options[option].selected = true;
        document.getElementById('namePeopleUpd').value = nameUpd;
        document.getElementById('jobUpd').value = jobUpd;
        document.getElementById('emailAccountUpd').value = emailUpd;
    });

    $('#deleteModalMailAccount').on('show.bs.modal', function(e) {
        idAccount = $(e.relatedTarget).data().id;
        document.getElementById('nameMailAccount').innerText = $(e.relatedTarget).data().email;
    });

    $("#btnAddNewMailAccount").click(function() {
        const documentSend = document.getElementById('document').value;
        const name = document.getElementById('namePeople').value;
        const job = document.getElementById('job').value;
        const email = document.getElementById('emailAccount').value;
        if (documentSend == 0) showToast('warning', 'Documento', 'Debes seleccionar un documento.');
        else if (email.length === 0) showToast('warning', 'Correo', 'Debes ingresar un correo valido.');
        else addNewMailAccount(documentSend, name, job, email);
    });

    $("#btnUpdMailAccount").click(function() {
        const documentSend = document.getElementById('documentUpd').value;
        const name = document.getElementById('namePeopleUpd').value;
        const job = document.getElementById('jobUpd').value;
        const email = document.getElementById('emailAccountUpd').value;
        if (email.length === 0) showToast('warning', 'Correo', 'Debes ingresar un correo valido.');
        else updateMailAccount(documentSend, name, job, email);
    });

    $("#deleteMailAccount").click(function() {
        deleteMailAccount();
    });

    $("#saveConfigMailAccounts").click(function() {
        $('#modalMailAccounts').click();
        showToast('success', 'Cuentas de Correo', 'Datos Guardados correctamente.');
    });

    function addNewMailAccount(document, name, job, email) {
        loaderPestWare('Guardando cuenta...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'POST',
            url: route('create_mail_accounts_by_customer'),
            data: {
                _token: token,
                customerId: idCustomer,
                document: document,
                name: name,
                job: job,
                email: email
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) showToast('error', 'Error:', response.message);
                else {
                    showToast('success', 'Cuenta', response.message);
                    getMailAccountsByCustomer(idCustomer);
                }
            }
        });
    }

    function updateMailAccount(document, name, job, email) {
        loaderPestWare('Actualizando cuenta...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'POST',
            url: route('update_mail_accounts_by_customer'),
            data: {
                _token: token,
                id: idAccount,
                document: document,
                name: name,
                job: job,
                email: email
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) showToast('error', 'Error:', response.message);
                else {
                    showToast('success', 'Cuenta', response.message);
                    $('#editMailAccount').click();
                    getMailAccountsByCustomer(idCustomer);
                }
            }
        });
    }

    function deleteMailAccount() {
        loaderPestWare('Eliminando cuenta...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'DELETE',
            url: route('delete_mail_accounts_by_customer', idAccount),
            data: {
                _token: token,
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) showToast('error', 'Error:', response.message);
                else {
                    showToast('success', 'Cuenta', response.message);
                    $('#deleteModalMailAccount').click();
                    getMailAccountsByCustomer(idCustomer);
                }
            }
        });
    }

});
