/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 04/04/2021
 * customers/_modalCalendar.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
    let idCustomer;

    $('#dateYearCalendar').on('show.bs.modal', function(e) {
        idCustomer = $(e.relatedTarget).data().id;
    });

    $("#btnCalendarCustomer").click(function() {
        let selectYear = $("#selectYear option:selected").val();
        downloadCalendar(selectYear);
    });

    function downloadCalendar(selectYear){
        loaderPestWare('Descargando Calendario');
        $.ajax({
            type: 'GEt',
            url: route('customer_calendar', [idCustomer, selectYear]),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (data) {
                if (data.errors){
                    Swal.close();
                    showToast('error', 'Error', data.message);
                }
                else{
                    Swal.close();
                    window.open(route('customer_calendar', [idCustomer, selectYear]), '_blank');
                }
            }
        });
    }

});
