/**
 * @author Manuel Mendoza
 * @version 2021
 * register/_deleteCustomer.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){
    let idCustomer, nameClient, quotationsCount, servicesOrderCount, customerBranchCount;
    $('#deleteClient').on('show.bs.modal', function(e) {
        idCustomer = $(e.relatedTarget).data().id;
        nameClient = $(e.relatedTarget).data().client;
        quotationsCount = $(e.relatedTarget).data().quotationscount;
        servicesOrderCount = $(e.relatedTarget).data().servicesordercount;
        customerBranchCount = $(e.relatedTarget).data().customerbranchcount;
        $('#nameClient').html(nameClient);
        $('#quotationsCount').html(quotationsCount);
        $('#servicesOrderCount').html(servicesOrderCount);
        $('#customerBranchCount').html(customerBranchCount);
    });

    $("#saveDeleteCustomer").on("click",function() {
        loaderPestWare('');
        $.ajax({
            type: 'DELETE',
            url: route('customer_delete'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idCustomer: idCustomer
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('warning','Algo salió Mal','Error al eliminar');
                }
                else{
                    Swal.close();
                    showToast('success-redirect','Eliminar','Datos Guardados Correctamente','index_customers');
                    $('.saveDeleteCustomer').prop('disabled', false);
                }
            }
        });
    });

 });
