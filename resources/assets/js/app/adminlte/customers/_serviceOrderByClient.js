/**
 * @author Manuel Mendoza
 * @version 2021
 * register/_deleteCustomer.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    let idCustomer;
    let serviceOrdersDB = [];

    $('#serviceOrderByBranch').on('show.bs.modal', function(e) {
        idCustomer = $(e.relatedTarget).data().id;
        serviceOrders(idCustomer);
    });

    function serviceOrders(idCustomer) {
        loaderPestWare('Cargando servicios...');
        $('#tableServicesOrder td').remove();
        $('#tableServicesBranches td').remove();
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('services_customer_by_branch', idCustomer),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    let rows = '';
                    let table = $('#tableServicesOrder');
                    let rowsBranches = '';
                    let tableBranches = $('#tableServicesBranches');
                    const {customerBranches, deletePermissionCan, deletePermissionHasRole, customer} = response;

                    customerBranches.forEach((element) => {
                        const {id,id_quotation_folio, name, manager, phone, email, address_number, address, colony, municipality,
                            state, postal_code, idEncrypt, isBranch} = element;
                        serviceOrdersDB.push({
                            id:id,
                            id_quotation_folio: id_quotation_folio,
                            name: name,
                            manager: manager,
                            phone: phone,
                            email: email,
                            address_number: address_number,
                            address: address,
                            colony: colony,
                            municipality: municipality,
                            state: state,
                            postal_code: postal_code,
                            idEncrypt: idEncrypt,
                            isBranch: isBranch
                        });

                        if (isBranch === 0) {
                            rows += `<tr>
                                <td class="text-primary text-bold">${id_quotation_folio}</td>
                                <td class="">${name}</td>
                                <td class="">${manager}</td>
                                <td class="">${phone}</td>
                                <td class="">${email}</td>
                                <td class="">${address} ${address_number} ${colony} ${municipality} ${state} ${postal_code}</td>
                                <td class="">
                                <a class="openModalEditCustomerBranch" data-toggle="modal" data-target="#editCustomerBranch" style="cursor: pointer"
                                    data-id="${id}"
                                    data-isbranch="${isBranch}">
                                    <i class="fa fa-pencil text-info" aria-hidden="true"></i>
                                </a>
                                ${deletePermissionCan || deletePermissionHasRole ?
                                `<a data-toggle="modal" data-target="#deleteCustomerBranch" style="cursor: pointer; margin-left: 3px"
                                    data-id="${idEncrypt}"
                                    data-name="${name}">
                                    <i class="fa fa-trash text-danger" aria-hidden="true"></i>
                                    </a>` : ''}
                                <a data-toggle="modal" data-target="#modalEditServicesByBranch"  style="cursor: pointer; margin-left: 3px"
                                    data-id="${idEncrypt}"
                                    data-name="${name}"
                                    data-customername="${customer.name}">
                                    <i class="fa fa-calendar text-info" aria-hidden="true"></i>
                                </a>
                                </td>
                            </tr>`;
                        }

                        if (isBranch === 1) {
                            rowsBranches += `<tr>
                                <td class="text-primary text-bold">${id}</td>
                                <td class="">${name}</td>
                                <td class="">${manager}</td>
                                <td class="">${phone}</td>
                                <td class="">${email}</td>
                                <td class="">${address} ${address_number} ${colony} ${municipality} ${state} ${postal_code}</td>
                                <td class="">
                                <a class="openModalEditCustomerBranch" data-toggle="modal" data-target="#editCustomerBranch" style="cursor: pointer"
                                    data-id="${id}">
                                    <i class="fa fa-pencil text-info" aria-hidden="true"></i>
                                </a>
                                ${deletePermissionCan || deletePermissionHasRole ?
                                `<a data-toggle="modal" data-target="#deleteCustomerBranch" style="cursor: pointer; margin-left: 3px"
                                    data-id="${idEncrypt}"
                                    data-name="${name}">
                                    <i class="fa fa-trash text-danger" aria-hidden="true"></i>
                                    </a>` : ''}
                                <a data-toggle="modal" data-target="#modalEditServicesByBranch"  style="cursor: pointer; margin-left: 3px"
                                    data-id="${idEncrypt}"
                                    data-name="${name}"
                                    data-customername="${customer.name}">
                                    <i class="fa fa-calendar text-info" aria-hidden="true"></i>
                                </a>
                                </td>
                            </tr>`;
                        }

                    });
                    table.append(rows);
                    tableBranches.append(rowsBranches);

                    Swal.close();

                }
            }
        });
    }

    let idCustomerBranch, nameCustomerBranch, managerCustomerBranch, phoneCustomerBranch, emailCustomerBranch, streetCustomerBranch,
        numberCustomerBranch, colonyCustomerBranch, municipalityCustomerBranch, stateCustomerBranch, codePostalCustomerBranch;

    $('#editCustomerBranch').on('show.bs.modal', function(e) {
        idCustomerBranch = $(e.relatedTarget).data().id;
        let isBranch = $(e.relatedTarget).data().isbranch;
        if (isBranch === 0) {
            $('#modalTitleCustomerBranch').html('Servicio');
            $('#nameCustomerBranchLabel').html('Servicio:');
        }
        else {
            $('#modalTitleCustomerBranch').html('Sucursal');
            $('#nameCustomerBranchLabel').html('Nombre:');
        }
        getDataBranch(idCustomerBranch);
    });

    function getDataBranch(idCustomerBranch) {
        loaderPestWare('Cargando servicios...');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('customer_branch_edit', idCustomerBranch),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error:', response.message);
                } else {
                    Swal.close();
                    const {customerBranch} = response;
                    nameCustomerBranch = $('#nameCustomerBranch').val(customerBranch.name)
                    managerCustomerBranch = $('#managerCustomerBranch').val(customerBranch.manager);
                    phoneCustomerBranch = $('#phoneBranch').val(customerBranch.phone);
                    emailCustomerBranch = $('#emailCustomerBranch').val(customerBranch.email);
                    streetCustomerBranch = $('#streetCustomerBranch').val(customerBranch.address);
                    numberCustomerBranch = $('#numberCustomerBranch').val(customerBranch.address_number);
                    colonyCustomerBranch = $('#colonyCustomerBranch').val(customerBranch.colony);
                    municipalityCustomerBranch = $('#municipalityCustomerBranch').val(customerBranch.municipality);
                    stateCustomerBranch = $('#stateCustomerBranch').val(customerBranch.state);
                    codePostalCustomerBranch = $('#codePostalCustomerBranch').val(customerBranch.postal_code);
                }
            }
        });
    }

    $("#sendEditCustomerBranchSave").click(function() {
        if (nameCustomerBranch.length === 0) showToast('error','Nombre Sucursal (Cliente)','Debes ingresar el nombre del Cliente');
        else if (streetCustomerBranch.length === 0) showToast('error','Calle Sucursal','Debes ingresar la Calle');
        else if (numberCustomerBranch.length === 0) showToast('error','Número Sucursal','Debes ingresar el Número');
        else if (colonyCustomerBranch.length === 0) showToast('error','Colonia Sucursal','Debes ingresar la Colonia');
        else if (municipalityCustomerBranch.length === 0) showToast('error','Muncipio Sucursal','Debes ingresar el Municipio');
        else if (stateCustomerBranch.length === 0) showToast('error','Estado Sucursal','Debes ingresar el Estado');
        else  EditCustomerBranch();
    });

    function EditCustomerBranch(){
        loaderPestWare('Editando...');
        $.ajax({
            type: 'POST',
            url: route('customers_edit_customer_branch'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idCustomerBranch: idCustomerBranch,
                managerCustomerBranch: managerCustomerBranch.val().trim(),
                phoneCustomerBranch: phoneCustomerBranch.val().trim(),
                emailCustomerBranch: emailCustomerBranch.val().trim(),
                nameCustomerBranch: nameCustomerBranch.val().trim(),
                streetCustomerBranch: streetCustomerBranch.val().trim(),
                numberCustomerBranch: numberCustomerBranch.val().trim(),
                colonyCustomerBranch: colonyCustomerBranch.val().trim(),
                municipalityCustomerBranch: municipalityCustomerBranch.val().trim(),
                stateCustomerBranch: stateCustomerBranch.val().trim(),
                codePostalCustomerBranch: codePostalCustomerBranch.val().trim()
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error', data.message);
                }
                else{
                    Swal.close();
                    showToast('success', 'Sucursales Cliente', data.message);
                    $("#editCustomerBranch").click();
                    serviceOrders(idCustomer);
                }
            }
        });
    }

 });
