/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 26/05/2021
 * customers/_modalDocumentation.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";
import {expressions, getFields, validateField} from "../validations/regex";

$(document).ready(function(){

    let idCustomerClient;
    let nameFile = 'Certificados';
    let initialFilterDateMipCertificate = null;
    let finalFilterDateMipCertificate = null;
    let initialFilterDateMipOrder = null;
    let finalFilterDateMipOrder = null;

    $(function() {

        $('input[name="filterDateMipCertificate"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateMipCertificate"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialFilterDateMipCertificate = picker.startDate.format('YYYY-MM-DD');
            finalFilterDateMipCertificate = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateMipCertificate"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('input[name="filterDateMipOrder"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateMipOrder"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialFilterDateMipOrder = picker.startDate.format('YYYY-MM-DD');
            finalFilterDateMipOrder = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateMipOrder"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    $('.openModalDocumentationCustomer').click(function () {
        let all = $(this);
        idCustomerClient = all.data("id");
        document.getElementById('id_customer').value = idCustomerClient;
        $('#customerNameTitleModal').html(all.attr('data-customer-main-name'));
        getAllDocumentsByIdCustomer(idCustomerClient).then(function (data) {
            if (data.code === 201) {
                let listDocuments = document.getElementById('documentsForMIP');
                let pdfs = '';
                let selectPosition = $("#position");
                selectPosition.empty();
                selectPosition.append('<option value="0">Al final</option>');
                data.documents.forEach((element) => {
                    pdfs += `<i class="fa fa-file-pdf-o text-red fa-1x" aria-hidden="true"></i> ${element.section_name}<br>`;
                    // Load positions select
                    let option = `<option value="${element.id}"`;
                    option = option + `>Después de ${element.section_name}</option>`;
                    selectPosition.append(option);
                });
                listDocuments.innerHTML = pdfs;
                let tableMipCertificates = document.getElementById('tableMipCertificates');
                if (data.company.id == 652) tableMipCertificates.style.display = 'block';
                else tableMipCertificates.style.display = 'none';

            } else showToast('error', 'Error', data.message);
        });
    });

    let getAllDocumentsByIdCustomer = function(customerId) {
        loaderPestWare('');
        return fetch(`../../customer/documents/mip/${customerId}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function(result) {
            Swal.close();
            return result.json();
        });
    };

    $('#formNewDocument').on('submit', function(event) {
        event.preventDefault();
        saveDocument();
    });

    function saveDocument() {
        loaderPestWare('Guardando Documento');
        let fd = new FormData(document.getElementById("formNewDocument"));

        $.ajax({
            url: route('customer_documents_post'),
            type: "POST",
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.code === 500) {
                    Swal.close();
                    showToast('error','Error:',data.message);
                } else if (data.code === 201) {
                    Swal.close();
                    document.getElementById("formNewDocument").reset();
                    getAllDocumentsByIdCustomer(idCustomerClient).then(function (data) {
                        if (data.code === 201) {
                            let listDocuments = document.getElementById('documentsForMIP');
                            let pdfs = '';
                            let selectPosition = $("#position");
                            selectPosition.empty();
                            selectPosition.append('<option value="0">Al final</option>');
                            data.documents.forEach((element) => {
                                pdfs += `<i class="fa fa-file-pdf-o text-red fa-1x" aria-hidden="true"></i> ${element.section_name}<br>`;
                                // Load positions select
                                let option = `<option value="${element.id}"`;
                                option = option + `>Después de ${element.section_name}</option>`;
                                selectPosition.append(option);
                            });
                            listDocuments.innerHTML = pdfs;
                        } else showToast('error', 'Error', data.message);
                    });
                    showToast('success','Documento Guardado.',data.message);
                }
            }
        });
    }

    $('#buttonMipCertificate').click(function () {

        if (initialFilterDateMipCertificate == null) {
            showToast('warning', 'Fecha', 'No ingresaste la fecha Certificado');
            return
        }
        let initial_date = initialFilterDateMipCertificate;
        let final_date = finalFilterDateMipCertificate;
        getDataCustomerDocuments( 1, initial_date, final_date);

    });

    $('#buttonMipOrder').click(function () {

        if (initialFilterDateMipOrder == null) {
            showToast('warning', 'Fecha', 'No ingresaste la fecha Orden');
            return
        }
        let initial_date = initialFilterDateMipOrder;
        let final_date = finalFilterDateMipOrder;
        getDataCustomerDocuments(2, initial_date, final_date);

    });


    function getDataCustomerDocuments(fileDocument, initial_date, final_date){
        if (fileDocument === 2) nameFile = 'Ordenes' ;
        loaderPestWare('Descargando Documentación...')
        $.ajax({
            type: 'POST',
            url: route('customer_documents_separate_mip'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                customerId: idCustomerClient,
                fileDocument: fileDocument,
                initial_date: initial_date,
                final_date: final_date,
            },
            xhrFields: {
                responseType: 'blob' // Esperamos un blob (archivo) en la respuesta
            },
            success: function (data) {
                Swal.close();
/*
                if (data.code == 200) {
                    showToast('success', data.message, 'Se descargaron correctamente los documentos.');
                }

                if (data.code == 500) {
                    showToast('error', 'Algo salió mal', `No hay ${nameFile} del año ${year}`);
                }
*/
                if (data instanceof Blob || data instanceof File) {
                    const url = window.URL.createObjectURL(data);
                    const a = document.createElement('a');
                    a.href = url;
                    a.download = `${nameFile} Mip del ${initial_date} al ${final_date}.pdf`; // Nombre de archivo para la descarga
                    document.body.appendChild(a);
                    a.click();
                    window.URL.revokeObjectURL(url);
                    showToast('success', data.message, 'Se descargaron correctamente los documentos.');
                } else {
                    console.log("No es un objeto Blob o File");
                    showToast('error', 'Algo salió mal', data.message);
                }

            },
            error: function() {
                Swal.close();
                showToast('error', 'Algo salió mal', `No hay ${nameFile} del ${initial_date} al ${final_date} `);
        }
        });
    }

});
