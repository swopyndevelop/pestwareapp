/**
 * @author Yomira Martinez
 * @version 25/01/2019
 * cuts/_index.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function() {
    $('#click_b').click(function() {

        saveCut();

        function saveCut() {
            loaderPestWare('');
            $.ajax({
                type: 'GET',
                url: route('cut_cash', 0),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content")
                },
                success: function(data) {
                    if (data.status == 'fail') {
                        Swal.close();
                        missingText('Algo salio mal');
                        console.log(data.errors);
                    } else {
                        Swal.close();
                        showToast('success-redirect','Corte Diario','Guardando correctamente','cuts_tech');
                    }
                }
            });
        }

    });
});

//alerts messages

function missingText(textError) {
    swal({
        title: "¡Espera!",
        type: "error",
        text: textError,
        icon: "error",
        timer: 3000,
        showCancelButton: false,
        showConfirmButton: false
    });
}