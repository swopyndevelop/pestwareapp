/**
 * @author Yomira Martinez
 * @version 25/01/2019
 * cuts/_adeudo.blade.php

//Funcion de JavaScript para Adeduo
$(document).ready(function () {
   $('#ArqueoX').click(function(){
        let corter = $('#corter').val();
        let milx = $('#milx').val();
         let quinientosx = $('#quinientosx').val();
         let doscientosx = $('#doscientosx').val();
         let cienx = $('#cienx').val();
         let cincuentax = $('#cincuentax').val();
         let veintex = $('#veintex').val();
         let diezx = $('#diezx').val();
         let cincox = $('#cincox').val();
         let dosx = $('#dosx').val();
         let unox = $('#unox').val();
         let cincuenta_centavosx = $('#cincuenta_centavosx').val();
         let veinte_centavosx = $('#veinte_centavosx').val();
         let diez_centavosx = $('#diez_centavosx').val();

        Adeedo_Quit();

        function Adeedo_Quit() {
            loader('');
            $.ajax({
               type:'GET',
               url: route('cut_store'),
               data:{
                   _token: $("meta[name=csrf-token]").attr("content"),
                   corter:corter,
                   milx:milx,
                    quinientosx:quinientosx,
                    doscientosx:doscientosx,
                    cienx:cienx,
                    cincuentax:cincuentax,
                    veintex:veintex,
                    diezx:diezx,
                    cincox:cincox,
                    dosx:dosx,
                    unox:unox,
                    diez_centavosx:diez_centavosx,
                    cincuenta_centavosx:cincuenta_centavosx,
                    veinte_centavosx:veinte_centavosx
               },
               success: function (data) {
                   if (data.errors){
                       Swal.close();
                       missingText('Algo salio mal');
                       console.log(data.errors);
                   }
                   else{
                       Swal.close();
                       correct();
                   }
               }
            });
        }
   });
});

//alerts messages
function correct() {
    swal({
        title: "Datos guardados correctamente",
        type: "success",
        timer: 2000,
        showCancelButton: false,
        showConfirmButton: false
    }).then((result) => {
        window.location.reload()
    })
}

function missingText(textError) {
    swal({
        title: "¡Espera!",
        type: "error",
        text: textError,
        icon: "error",
        timer: 3000,
        showCancelButton: false,
        showConfirmButton: false
    });
}

function loader(message){
    Swal.fire({
        title: message,
        onBeforeOpen: () => {
            Swal.showLoading()
        },
    }).then((result) => {

    })
}
 */
