/**
 * @author Alberto Martínez
 * @version 16/01/2019
 * register/_quotation.blade.php
 */


import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function() {

    //autocomplete cellphone
    let customerIdFind = 0, jobCenterSelect= 332;
    function findCustomers(query) {
        if (query != '') {
            $.ajax({
                type: 'GET',
                url: route('autocomplete_cellphone'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    query: query,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    $('#numberList').fadeIn();
                    $('#numberList').html(data);
                }
            });
        }
    }

    //autocomplete customerName
    function findCustomersName(query) {
        if (query != '') {
            $.ajax({
                type: 'GET',
                url: route('autocomplete_customer_name'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    query: query,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    $('#customerNameList').fadeIn();
                    $('#customerNameList').html(data);
                }
            });
        }
    }

    $(document).on('click', 'li#getdataClient', function() {
        $('#cellphone').val($(this).text());
        $('#numberList').fadeOut();
        //cargar datos de cliente
        let cellphoneLoad = $('#cellphone').val();
        $.ajax({
            type: 'GET',
            url: route('load_client'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                cellphone: cellphoneLoad,
                jobCenterSelect: jobCenterSelect
            },
            success: function(data) {
                customerIdFind = data.id;
                $('#customerName').val(data.name).text();
                $('#establishmentName').val(data.establishment_name).text();
                $('#cellphoneMain').val(data.cellphone_main).text();
                $('#colony').val(data.colony).text();
                $('#municipality').val(data.municipality).text();
                $('#emailCustomer').val(data.email).text();
                $('#sourceOrigin').val(data.id_source_origin);
            }
        });
        $('#numberList').fadeOut();
    });

    $(document).on('click', 'li#getdataClientName', function() {
        $('#customerName').val($(this).text());
        $('#customerNameList').fadeOut();
        //cargar datos de cliente
        let customerName = $('#customerName').val();
        $.ajax({
            type: 'GET',
            url: route('load_client_name'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                customerName: customerName,
                jobCenterSelect: jobCenterSelect
            },
            success: function(data) {
                customerIdFind = data.id;
                $('#customerName').val(data.name).text();
                $('#establishmentName').val(data.establishment_name).text();
                $('#cellphone').val(data.cellphone).text();
                $('#cellphoneMain').val(data.cellphone_main).text();
                $('#colony').val(data.colony).text();
                $('#municipality').val(data.municipality).text();
                $('#emailCustomer').val(data.email).text();
                $('#sourceOrigin').val(data.id_source_origin);
            }
        });
        $('#customerNameList').fadeOut();
    });

    let cellphone = document.getElementById('cellphone');
    let customerName = document.getElementById('customerName');

    cellphone.addEventListener('keyup', function (e) {
        findCustomers($(this).val());
    });

    customerName.addEventListener('keyup', function (e) {
        findCustomersName($(this).val());
    });
    //end autocomplete

    // Save Quotation
    $('#btnSaveInspection').click(function(){
        let customerName =  $('#customerName').val();
        let establishmentName = $('#establishmentName').val();
        let cellphone = $('#cellphone').val();
        let cellphoneMain = $('#cellphoneMain').val();
        let colony = $('#colony').val();
        let municipality = $('#municipality').val();
        let emailCustomer = $('#emailCustomer').val();
        let sourceOrigin = $('#sourceOrigin').val();

        loaderPestWare('Guardando Inspección...')

        $.ajax({
            type: 'post',
            url: route('store_inspection'),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                customerIdFind: customerIdFind,
                name: customerName,
                establishmentName: establishmentName,
                cellphone: cellphone,
                cellphoneMain: cellphoneMain,
                colony: colony,
                municipality: municipality,
                email: emailCustomer,
                sourceOrigin: sourceOrigin
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error al guardar', data.message);
                }
                else if (data.code == 201){
                    Swal.close();
                    showToast('success-redirect', 'Inspección Guardada', data.message, 'calendario');
                }
            }
        });
    });

});