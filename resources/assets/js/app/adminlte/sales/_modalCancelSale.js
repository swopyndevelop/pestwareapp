/**
 * @author Manuel Mendoza
 * @version 30/11/2020
 * register/_modalCancelSale.blade.php
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    // Global lets
    let idSale;

    // Load data open modal -->
    $('#cancelSale').on('show.bs.modal', function(e) {
        idSale = $(e.relatedTarget).data().id;
    });

    $('#btnCancelSale').click(function () {
        console.log(idSale)
        sendCancelSale();
    });

    function sendCancelSale(){
        loaderPestWare('Cancelando Venta...')
        $.ajax({
            type: 'post',
            url: route('cancel_sale'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idSale: idSale,
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Algo salió mal', data.message);
                }
                else{
                    Swal.close();
                    showToast('success', 'Venta Enviada', data.message);
                    $("#btnCancelSale").prop("disabled", true);
                    location.reload();
                }
            }
        });
    }

});
