/**
 * @author Manuel Mendoza
 * @version 30/11/2020
 * register/_modalSendEmailSale.blade.php
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    // Global lets
    let idSale, email, emailOther;

    // Load data open modal -->
    $('#sendMailSale').on('show.bs.modal', function(e) {
        idSale = $(e.relatedTarget).data().id;
        email = $(e.relatedTarget).data().email;
        $('#emailSale').val(email);
    });

    $('#sendEmailSaleButton').click(function () {
        email = document.getElementById('emailSale').value;
        emailOther = document.getElementById('emailSaleOther').value;
        let emails = [];
        if (email.length < 0) showToast('warning','Email Cliente','Favor de ingresar un correo');
        if (email.length !== 0) emails.push(email);
        if (emailOther.length !== 0) emails.push(emailOther);
        sendEmailSale(emails);
    });

    function sendEmailSale(emails){
        loaderPestWare('Enviando Venta...')
        $.ajax({
            type: 'post',
            url: route('send_email_sale'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idSale: idSale,
                emails: emails
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Algo salió mal', data.message);
                }
                else{
                    Swal.close();
                    showToast('success', 'Venta Enviada', data.message);
                    $("#sendEmailSaleButton").prop("disabled", true);
                    location.reload();
                }
            }
        });
    }

});
