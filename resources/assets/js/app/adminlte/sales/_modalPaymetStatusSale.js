/**
 * @author Manuel Mendoza
 * @version 30/11/2020
 * register/_modalPaymentStatusSale.blade.php
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    // Global lets
    let idSale;

    // Load data open modal -->
    $('#ModalPaymentSale').on('show.bs.modal', function(e) {
        idSale = $(e.relatedTarget).data().id;
        document.getElementById("idSale").value = idSale;
    });

    // Validar archivo
    const fileVoucher = document.getElementById('fileVoucher');
    fileVoucher.addEventListener('change', (event) => {
        checkFile(event);
    });

    $('#formPaymentStatusSale').on('submit', function(event) {

        event.preventDefault();
        savePaymentStatus();

    });

    function savePaymentStatus() {
        loaderPestWare('Actualizando pago...');
        let fd = new FormData(document.getElementById("formPaymentStatusSale"));

        $.ajax({
            type: 'POST',
            url: route('payment_status_sale'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (data) {
                Swal.close();
                if (data.code === 500) {
                    showToast('warning', 'Espera...', data.message);
                } else {
                    showToast('success', 'Datos Actualizados', data.message);
                    location.reload();
                }
            }
        });
    }

    function checkFile(e) {

        const fileList = e.target.files;
        let error = false;
        let defaultImage = 'https://www.gravatar.com/avatar/8dd9506c61dfc3c43d1100df4fe21035.jpg?s=80&d=mm&r=g';
        let image = document.getElementById('imgProfile');
        for (let i = 0, file; file = fileList[i]; i++) {

            let sFileName = file.name;
            let sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            let iFileSize = file.size;
            // let iConvert = (file.size / 1048576).toFixed(2);

            if (!(sFileExtension === "pdf" ||
                sFileExtension === "jpg" ||
                sFileExtension === "jpeg") || iFileSize > 10485760) { /// 10 mb
                error = true;
            }
        }
        if (error) {
            missingText("El archivo ingresado no es aceptado o su tamaño excede lo aceptado");
            document.getElementById("fileVoucher").value = "";
            image.src = defaultImage;
        }
        if (!error) {
            image.src = URL.createObjectURL(e.target.files[0]);
        }
    }

});
