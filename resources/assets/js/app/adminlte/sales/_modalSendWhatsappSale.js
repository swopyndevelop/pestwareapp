/**
 * @author Manuel Mendoza
 * @version 30/11/2020
 * register/_modalSendWhatsappSale.blade.php
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){

    // Global lets
    let idSale, cellphone, cellphoneOther, codeCountry, urlWhatsapp;

    // Load data open modal -->
    $('#whatsappSale').on('show.bs.modal', function(e) {
        idSale = $(e.relatedTarget).data().id;
        cellphone = $(e.relatedTarget).data().cellphone;
        codeCountry = $(e.relatedTarget).data().codecounty;
        $('#cellphoneSale').val(cellphone);
    });

    $('#sendWhatsappSaleButton').click(function () {
        //http://127.0.0.1:8000/  https://pestwareapp.com
        let routeWhatsapp = `https://pestwareapp.com/pdf/sales/download/${idSale}`
        cellphone = document.getElementById('cellphoneSale').value;
        cellphoneOther = document.getElementById('cellphoneOtherSale').value;
        if (cellphone.length <= 0  || cellphoneOther.length <= 0) showToast('warning','Célular Cliente','Favor de ingresar un número');
        if (cellphone.length !== 0){
            urlWhatsapp = `https://api.whatsapp.com/send?phone=${codeCountry}${cellphone}&text=Venta: ${routeWhatsapp}`;
            window.open(urlWhatsapp, '_blank');
            $("#whatsappSale").click();
        }
        if (cellphoneOther.length !== 0){
            urlWhatsapp = `https://api.whatsapp.com/send?phone=${codeCountry}${cellphoneOther}&text=Venta: ${routeWhatsapp}`;
            window.open(urlWhatsapp, '_blank');
            $("#whatsappSale").click();
        }
    });


});
