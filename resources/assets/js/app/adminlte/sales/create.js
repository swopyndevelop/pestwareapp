/**
 * @author Alberto Martínez
 * @version 17 /07/2021
 * sales/_modalCreateSale.blade.php
 */

import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function() {

    let db = [];
    let totalGlobal = 0;
    let subtotalGlobal = 0;
    let taxesGlobal = 0;
    let btnAdd = document.getElementById('addTest');
    let productSelect = document.getElementById('products');
    let discountSelect = document.getElementById('selectDiscount');
    let storehouseSelect = document.getElementById('selectTypeSale');
    let btnSavePurchase = document.getElementById('saveSale');

    getProducts(storehouseSelect.value);

    document.getElementById('subtotalTest').innerText = `$0.00`;
    document.getElementById('totalTest').innerText = `$0.00`;

    btnAdd.onclick = function () {
        let data = getInputs()
        const regexDecimal = /^[0-9]+([.][0-9]+)?$/;
        const regexInteger = /^[0-9]?\d{1,6}$/;
        if (!regexInteger.test(data.quantity)) showToast('warning', 'Datos incompletos', 'La cantidad debe ser númerica.');
        else if (data.quantity < 1) showToast('warning', 'Datos incompletos', 'La cantidad debe ser mayor a 0.');
        else if (parseInt(data.quantity) > parseInt(data.stock)) showToast('warning', 'Datos incompletos', 'La cantidad no puede ser mayor a la existencia.');
        else if (!regexDecimal.test(data.lastPrice)) showToast('warning', 'Datos incompletos', 'El precio debe ser númerico.');
        else addRow(data)
    }

    productSelect.addEventListener('change', (event) => {
        const option = event.target.value;
        getDataProduct(option);
    });

    storehouseSelect.addEventListener('change', (event) => {
        const option = event.target.value;
        getProducts(option);
    });

    discountSelect.addEventListener('change', (event) => {
        calculatePrice(db);
    });

    $('#selectCustomer').on('change', function() {
        if (this.value == 0) {
            document.getElementById('customerNameOnly').disabled = false;
            document.getElementById('customerCellphoneOnly').disabled = false;
        } else {
            document.getElementById('customerNameOnly').disabled = true;
            document.getElementById('customerCellphoneOnly').disabled = true;
        }
    });

    function getProducts(storehouseId) {
        loaderPestWare('');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('get_products_sales', storehouseId),
            data: {
                _token: token
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) {
                    showToast('error', 'Error', response.message);
                } else {
                    let select = $('#products');
                    select.empty();
                    select.append(`<option value="0" selected disabled>Selecciona un Producto</option>`);
                    const {code, products} = response;
                    products.forEach((element) => {
                        select.append(`<option value="${element.id}">${element.name}</option>`);
                    });
                    if (products.length === 0) showToast('warning', 'Sin Existencias', 'El almacén seleccionado no cuenta con productos disponibles.')
                }
            }
        });
    }

    function getDataProduct(id) {
        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('data_product_sales'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                id: id,
                storehouse: storehouseSelect.value
            },
            success: function(data) {
                if (data.code === 500) {
                    Swal.close();
                    showToast('error','Error',data.message);
                } else {
                    const {taxesValues} = data;
                    taxesGlobal = taxesValues;
                    const {quantity, unit, sale_price, stock} = data.product;
                    document.getElementById('quantityProduct').value = `${quantity} ${unit}s`;
                    document.getElementById('taxes').value = data.taxes;
                    document.getElementById('lastPrice').value = sale_price;
                    document.getElementById('stock').value = stock;
                    Swal.close();
                }
            }
        });
    }

    function getInputs() {
        let product = document.getElementById('products');
        let quantityProduct = document.getElementById('quantityProduct').value;
        let taxes = document.getElementById('taxes').value;
        let lastPrice = document.getElementById('lastPrice').value;
        let quantity = document.getElementById('quantity').value;
        let stock = document.getElementById('stock').value;
        const index = product.selectedIndex;
        const optionSelect = product.options[index];
        const subtotal = quantity * lastPrice;

        let total = subtotal;
        if (taxesGlobal !== 0) {
            taxesGlobal.forEach((element) => {
                const tax = total * (element.value / 100);
                total += tax;
            });
        }
        return {
            id: createId(),
            productId: optionSelect.value,
            productText: optionSelect.text,
            quantityProduct: quantityProduct,
            taxes: taxes,
            taxesValues: taxesGlobal,
            lastPrice: lastPrice,
            subtotal: subtotal,
            total: total,
            quantity: quantity,
            stock: stock,
            index: index
        }
    }

    function setInputs(data) {
        document.getElementById('products').selectedIndex = data.index;
        document.getElementById('quantityProduct').value = data.quantityProduct;
        document.getElementById('taxes').value = data.taxes;
        document.getElementById('lastPrice').value = data.lastPrice;
        document.getElementById('quantity').value = data.quantity;
        document.getElementById('stock').value = data.stock;
    }

    function clearInputs() {
        document.getElementById('products').selectedIndex = 0;
        document.getElementById('quantityProduct').value = '';
        document.getElementById('taxes').value = '';
        document.getElementById('lastPrice').value = '';
        document.getElementById('quantity').value = '';
        document.getElementById('stock').value = '';
        document.getElementById('addTest').textContent = 'Agregar';
    }

    function addRow(data) {
        db.push(data);
        listItems(db);
        clearInputs();
    }

    // Models =>
    function getItemById(id) {
        return db.find(element => element.id === id)
    }

    function deleteItemById(id) {
        let dbTmp = db;
        db = [];
        dbTmp.forEach((element) => {
            if (element.id !== id) db.push(element);
        })
    }

    function calculatePrice(data) {
        let subtotal = 0;
        let total = 0;

        $.ajax({
            type: 'GET',
            url: route('data_discount_sales', discountSelect.value),
            data: {
                _token: $("meta[name=csrf-token]").attr("content")
            },
            success: function (response) {
                Swal.close();
                if (response.code === 500) {
                    showToast('warning', 'Error', response.message);
                } else {
                    const {discount} = response;
                    const {percentage} = discount;
                    data.forEach((element) => {
                        subtotal += element.subtotal
                        total += element.total
                    });

                    if (discount != null) {
                        let totalDiscount = (percentage / 100) * total;
                        console.log(total)
                        total = total - totalDiscount;
                    }
                    subtotalGlobal = subtotal;
                    totalGlobal = total;
                    document.getElementById('subtotalTest').innerText = `$${subtotal.toFixed(2)}`;
                    document.getElementById('totalTest').innerText = `$${total.toFixed(2)}`;
                }
            }
        });
    }

    function listItems(data) {
        let tableRef = document.getElementById('customTable');
        for(let i = tableRef.rows.length - 1; i > 0; i--)
        {
            tableRef.deleteRow(i);
        }
        data.forEach((element) => {
            const actions = `<a id="updateItemRow-${element.id}" data-id="${element.id}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
            <a id="deleteItemRow-${element.id}" data-id="${element.id}" style="cursor: pointer;"><i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>`;

            let newRow = tableRef.insertRow();
            const subtotal = parseFloat(element.subtotal)
            const total = parseFloat(element.total)

            let newCellProduct  = newRow.insertCell(0);
            let newCellQuantityProduct  = newRow.insertCell(1);
            let newCellTaxes  = newRow.insertCell(2);
            let newCellLastPrice  = newRow.insertCell(3);
            let newCellQuantity  = newRow.insertCell(4);
            let newCellSubtotal  = newRow.insertCell(5);
            let newCellTotal = newRow.insertCell(6);
            let newCellActions  = newRow.insertCell(7);

            newCellProduct.appendChild(document.createTextNode(element.productText));
            newCellQuantityProduct.appendChild(document.createTextNode(element.quantityProduct));
            newCellTaxes.appendChild(document.createTextNode(element.taxes));
            newCellLastPrice.appendChild(document.createTextNode(element.lastPrice));
            newCellQuantity.appendChild(document.createTextNode(element.quantity));
            newCellSubtotal.appendChild(document.createTextNode(subtotal.toFixed(2)));
            newCellTotal.appendChild(document.createTextNode(total.toFixed(2)));
            newCellActions.innerHTML = actions;

            // Events =>
            $(`#updateItemRow-${element.id}`).click(function () {
                let all = $(this);
                const id = all.attr('data-id');
                let item = getItemById(id);
                setInputs(item);
                let btn = document.getElementById('addTest');
                btn.textContent = 'Editar';
                deleteItemById(id);
            });
            $(`#deleteItemRow-${element.id}`).click(function () {
                let all = $(this);
                deleteItemById(all.attr('data-id'));
                listItems(db);
                clearInputs();
            });
        });
        calculatePrice(db, null);
    }

    // Utils =>
    let createId = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    // Saved purchase order =>
    btnSavePurchase.onclick = function () {
        //validations
        let customerNameOnly = document.getElementById('customerNameOnly').value;
        let customer = $("#selectCustomer option:selected").val();
        if (db.length < 1) showToast('warning', 'Datos incompletos', 'Debes ingresar por lo menos un producto.');
        else if (customerNameOnly.length == 0 && customer == 0) showToast('warning', 'Datos incompletos', 'Debes ingresar el nombre del Cliente (Mostrador).');
        else saveSale();
    }

    function saveSale(){
        loaderPestWare('Generando Venta...');

        //get values input
        let customer = $("#selectCustomer option:selected").val();
        let typeSale = $("#selectTypeSale option:selected").val();
        let date = document.getElementById('date').value;
        let description = document.getElementById('description').value;
        let paymentWay = $("#selectPaymentWay option:selected").val();
        let paymentMethod = $("#selectPaymentMethod option:selected").val();
        let voucher = $("#selectVoucher option:selected").val();
        let discount = $("#selectDiscount option:selected").val();
        let customerNameOnly = document.getElementById('customerNameOnly').value;
        let customerCellphoneOnly = document.getElementById('customerCellphoneOnly').value;

        $.ajax({
            type: 'POST',
            url: route('save_sale'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                customer: customer,
                typeSale: typeSale,
                date: date,
                paymentWay: paymentWay,
                paymentMethod: paymentMethod,
                subtotal: subtotalGlobal,
                total: totalGlobal,
                voucher: voucher,
                discount: discount,
                customerNameOnly: customerNameOnly,
                customerCellphoneOnly: customerCellphoneOnly,
                description: description,
                arrayConcepts: JSON.stringify(db),
            },
            success: function (data) {
                if (data.code === 500){
                    Swal.close();
                    showToast('error','Error', data.message);
                }
                else{
                    Swal.close();
                    showToast('success-redirect','Venta Registrada', data.message,'index_sales');
                    $("#saveSale").prop("disabled", true);
                }
            }
        });
    }

});
