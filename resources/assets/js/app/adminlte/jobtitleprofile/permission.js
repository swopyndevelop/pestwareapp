/**
 * @author Alberto Martínez
 * @version 01/12/2019
 * treejobprofile/_modalPermissions.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    $("#savePermissions").click(function() {
        //validations
        let permission = $('#permissions').val();
        let name = $('#namejobtitleprofile').val();
        let id = $('#idjobtitleprofile').val();

        loaderPestWare('');
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: "POST",
            url: route('store_permission'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                permission: permission,
                name: name,
                id: id
            },
            success: function(data) {
                if (data.errors) {
                    Swal.close();
                    missingText('Error al agregar los permisos');
                } else {
                    Swal.close();
                    showToast('success-redirect','Permisos','Guardando correctamente','tree_jobprofile');
                }
            }
        });
    });

    //alerts messages

    function missingText(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
        });
    }

});