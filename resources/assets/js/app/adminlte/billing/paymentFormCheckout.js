import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";


window.onload = function () {

    // Calculate total plan.
    let users = 0;
    let centers = 0;
    let usersB = 0;
    let centersB = 0;
    let plan = 0;

    const publishableKey = 'pk_live_51IsqQtHVGkiXew9drnQ2ZQ40yE89Qyve5ci7e3P6bAoBxQD5DoiA2gLZZMgtmnFm5EZlRENmOjxsC7zAzZc1Dnpd00oIkyM7UV';

    let getExtrasPlan = function() {
        loaderPestWare('');
        return fetch("../../stripe/current/extras/plan", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                _token: $("meta[name='csrf-token']").attr("content")
            })
        }).then(function(result) {
            Swal.close();
            return result.json();
        });
    };

    /**
     * Execute on load page for get data company plan.
     */
    getExtrasPlan().then(function (data) {
        if (data.plan === 2) {
            users = data.users;
            centers = data.centers;
            plan = data.plan;
            calculatePrice(plan, users, centers).then(function(data) {
                if (data.code === 200) {
                    updateLabelTotalUsers();
                    updateLabelTotalCenters();
                    updateLabelsTotal(data);
                } else showToast('warning', 'Error', data.message);
            });
        } else {
            usersB = data.users;
            centersB = data.centers;
            plan = data.plan;
            calculatePrice(plan, usersB, centersB).then(function(data) {
                if (data.code === 200) {
                    updateLabelTotalUsersB();
                    updateLabelTotalCentersB();
                    updateLabelsTotalB(data);
                } else showToast('warning', 'Error', data.message);
            });
        }
    });

    let calculatePrice = function(plan, users, centers) {
        loaderPestWare('Calculando precio...');
        return fetch("../../stripe/calculate/price/plan", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                users: users,
                centers: centers,
                plan: plan,
                _token: $("meta[name='csrf-token']").attr("content")
            })
        }).then(function(result) {
            Swal.close();
            return result.json();
        });
    };

    document.getElementById("addUser")
        .addEventListener("click", function(evt) {
            users++;
            plan = 2;
            calculatePrice(plan, users, centers).then(function(data) {
                if (data.code === 200) {
                    updateLabelTotalUsers();
                    updateLabelsTotal(data);
                } else {
                    users--;
                    showToast('warning', 'Error', data.message);
                }
            });
        });

    document.getElementById("minusUser")
        .addEventListener("click", function(evt) {
            if (users > 0) {
                users--;
                plan = 2;
                calculatePrice(plan, users, centers).then(function(data) {
                    if (data.code === 200) {
                        updateLabelTotalUsers();
                        updateLabelsTotal(data);
                    } else {
                        users++;
                        showToast('warning', 'Error', data.message);
                    }
                });
            }
        });

    document.getElementById("addCenter")
        .addEventListener("click", function(evt) {
            centers++;
            plan = 2;
            calculatePrice(plan, users, centers).then(function(data) {
                if (data.code === 200) {
                    updateLabelTotalCenters();
                    updateLabelsTotal(data);
                } else {
                    centers--;
                    showToast('warning', 'Error', data.message);
                }
            });
        });

    document.getElementById("minusCenter")
        .addEventListener("click", function(evt) {
            if (centers > 0) {
                centers--;
                plan = 2;
                calculatePrice(plan, users, centers).then(function(data) {
                    if (data.code === 200) {
                        updateLabelTotalCenters();
                        updateLabelsTotal(data);
                    } else {
                        centers++;
                        showToast('warning', 'Error', data.message);
                    }
                });
            }
        });

    function updateLabelTotalUsers() {
        document.getElementById("labelUsers").innerHTML = `${users} Usuarios`;
    }

    function updateLabelTotalCenters() {
        document.getElementById("labelCenters").innerHTML = `${centers} Sucursales`;
    }

    function updateLabelsTotal(data) {
        document.getElementById("totalUsers").innerHTML = `Usuarios: ${data.currency}${data.users}`;
        document.getElementById("totalCenters").innerHTML = `Sucursales: ${data.currency}${data.centers}`;
        document.getElementById("totalIVA").innerHTML = `IVA: ${data.currency}${data.iva}`;
        document.getElementById("subtotal").innerHTML = `Subtotal: ${data.currency}${data.price}`;
        document.getElementById("totalPlan1").innerHTML = `Total: ${data.currency}${data.total}`;
    }

    document.getElementById("addUserB")
        .addEventListener("click", function(evt) {
            usersB++;
            plan = 3;
            calculatePrice(plan, usersB, centersB).then(function(data) {
                if (data.code === 200) {
                    updateLabelTotalUsersB();
                    updateLabelsTotalB(data);
                } else {
                    usersB--;
                    showToast('warning', 'Error', data.message);
                }
            });
        });

    document.getElementById("minusUserB")
        .addEventListener("click", function(evt) {
            if (usersB > 0) {
                usersB--;
                plan = 3;
                calculatePrice(plan, usersB, centersB).then(function(data) {
                    if (data.code === 200) {
                        updateLabelTotalUsersB();
                        updateLabelsTotalB(data);
                    } else {
                        usersB++;
                        showToast('warning', 'Error', data.message);
                    }
                });
            }
        });

    document.getElementById("addCenterB")
        .addEventListener("click", function(evt) {
            centersB++;
            plan = 3;
            calculatePrice(plan, usersB, centersB).then(function(data) {
                if (data.code === 200) {
                    updateLabelTotalCentersB();
                    updateLabelsTotalB(data);
                } else {
                    centersB--;
                    showToast('warning', 'Error', data.message);
                }
            });
        });

    document.getElementById("minusCenterB")
        .addEventListener("click", function(evt) {
            if (centersB > 0) {
                centersB--;
                plan = 3;
                calculatePrice(plan, usersB, centersB).then(function(data) {
                    if (data.code === 200) {
                        updateLabelTotalCentersB();
                        updateLabelsTotalB(data);
                    } else {
                        centersB++;
                        showToast('warning', 'Error', data.message);
                    }
                });
            }
        });

    function updateLabelTotalUsersB() {
        document.getElementById("labelUsersB").innerHTML = `${usersB} Usuarios`;
    }

    function updateLabelTotalCentersB() {
        document.getElementById("labelCentersB").innerHTML = `${centersB} Sucursales`;
    }

    function updateLabelsTotalB(data) {
        document.getElementById("totalUsersB").innerHTML = `Usuarios: ${data.currency}${data.users}`;
        document.getElementById("totalCentersB").innerHTML = `Sucursales: ${data.currency}${data.centers}`;
        document.getElementById("totalIVAB").innerHTML = `IVA: ${data.currency}${data.iva}`;
        document.getElementById("subtotalB").innerHTML = `Subtotal: ${data.currency}${data.price}`;
        document.getElementById("totalPlan1B").innerHTML = `Total: ${data.currency}${data.total}`;
    }

    let stripe = Stripe(publishableKey);

    let createCheckoutSession = function(plan, users, centers) {
        loaderPestWare('Generando pago...');
        return fetch("../../stripe/create/checkout/session", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                users: users,
                centers: centers,
                plan: plan,
                _token: $("meta[name='csrf-token']").attr("content")
            })
        }).then(function(result) {
            Swal.close();
            return result.json();
        });
    };

    let btnActionA = document.getElementById("btnActionA");
    let btnActionB = document.getElementById("btnActionB");

    btnActionA.addEventListener("click", function(evt) {
        if (btnActionA.textContent === 'Seleccionar Plan') {
            createCheckoutSession(2, users, centers).then(function(data) {
                stripe
                    .redirectToCheckout({
                        sessionId: data.sessionId
                    })
                    .then(handleResult);
            });
        } else {
            loaderPestWare('Cargando...');
            fetch('../../stripe/customer/portal', {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    _token: $("meta[name='csrf-token']").attr("content")
                })
            })
                .then(function(response) {
                    return response.json()
                })
                .then(function(data) {
                    Swal.close();
                    window.location.href = data.url;
                })
                .catch(function(error) {
                    Swal.close();
                    console.error('Error:', error);
                });
        }
    });

    btnActionB.addEventListener("click", function(evt) {
        if (btnActionB.textContent === 'Seleccionar Plan') {
            createCheckoutSession(3, usersB, centersB).then(function(data) {
                stripe
                    .redirectToCheckout({
                        sessionId: data.sessionId
                    })
                    .then(handleResult);
            });
        } else {
            loaderPestWare('Cargando...');
            fetch('../../stripe/customer/portal', {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    _token: $("meta[name='csrf-token']").attr("content")
                })
            })
                .then(function(response) {
                    return response.json()
                })
                .then(function(data) {
                    Swal.close();
                    window.location.href = data.url;
                })
                .catch(function(error) {
                    Swal.close();
                    console.error('Error:', error);
                });
        }
    });

};