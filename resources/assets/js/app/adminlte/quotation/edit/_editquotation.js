/**
 * @author Alberto Martínez
 * @version 16/01/2019
 * register/_editquotation.blade.php
 */

import {expressions, getFields, setFields, validateField} from "../../validations/regex";
import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function() {

    let jobCenterSelect = $('#sucursal').val();
    //autocomplete cellphone
    function findCustomers(query) {
        if (query != '') {
            $.ajax({
                type: 'GET',
                url: route('autocomplete_cellphone'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    query: query,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    $('#numberList').fadeIn();
                    $('#numberList').html(data);
                }
            });
        }
    }

    $(".plagueList-single").select2();
    $("#editQuotation").modal();
    $("#editQuotation").on('hidden.bs.modal', function () {
        window.location.href = route('index_register'); //using a named route
    });

    $('#makeEditable').SetEditable({$addButton: $('#but_addE')});
    let arrayConceptsE = [];
    let subtotalE = 0;
    let totalE = 0;

    $('#calcularE').on('click', function () {
        let td = TableToCSV('makeEditable', ',');
        let ar_lines = td.split("\n");
        let each_data_value = [];
        arrayConceptsE = [];
        subtotalE = 0;
        totalE = 0;
        ar_lines.pop();

        ar_lines.forEach((element, i) => {
            each_data_value[i] = ar_lines[i].split(",");
        });

        each_data_value.forEach((element, i) => {

            subtotalE = subtotalE + (element[1] * element[2] * element[3] * element[4]);

            // get data
            let concept = element[0];
            let quantity = element[1];
            let frequency = element[2];
            let term = element[3];
            let priceUnit = element[4];
            let subtotalConcept = element[1] * element[2] * element[3] * element[4];

            arrayConceptsE.push({
                'concept': concept,
                'quantity': quantity,
                'frequency': frequency,
                'term': term,
                'priceUnit': priceUnit,
                'subtotal': subtotalConcept
            });

        });

        let discount = $('#discountCE').val();
        let percentageDiscount = discount / 100;
        let totalDiscount = percentageDiscount * subtotalE;
        totalE = subtotalE - totalDiscount;
        $('#sumaSubtotalE').val(subtotalE);
        $('#totalE').val(totalE);
    });

    $('#saveQuotationCustomNewE').on('click', function () {
        updateQuotationCustomE();
    });

    function updateQuotationCustomE() {
        loaderPestWare('');

        //get values input
        let idQuotation = $('#id').val();
        let descriptionE = $('#descriptionE').val();
        let discountE = $('#discountCE').val();

        $.ajax({
            type: 'PUT',
            url: route('update_quotation_custom'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idQuotation: idQuotation,
                description: descriptionE,
                arrayConcepts: JSON.stringify(arrayConceptsE),
                discount: discountE,
                total: totalE,
                price: subtotalE
            },
            success: function (data) {
                if (data.errors) {
                    Swal.close();
                    missingTextCE('error al guardar');
                } else {
                    Swal.close();
                    showToast('success-redirect','Datos Actualizados', data.message, 'index_register')
                    //correctCE();
                }
            }
        });
    }

    //select discount
    $('#discountE').on('change', function() {
        getPriceData(0);
    });

    //select discount
    $('#extraE').on('change', function() {
        getPriceData(0);
    });

    //select type service
    $('#establishmentE').on('change', function() {
        let serviceId = $(this).val();
        loaderPestWare('Buscando plagas');
        $.ajax({
            type: 'GET',
            url: route('plagues_by_service', serviceId),
            data: {
                _token: $("meta[name=csrf-token]").attr("content")
            },
            success: function(data) {
                Swal.close();
                if (data.code === 500) {
                    showToast('warning', 'Error:', data.message);
                    $("#plagueE").empty();
                }
                else {
                    // Load plague select
                    $("#plagueE").empty();
                    data.plagues.forEach((element) => {
                        let option = `<option value="${element.id},${element.plague_key}"`;
                        option = option + `>${element.name}</option>`;
                        $("#plagueE").append(option);
                    });
                }
            }
        });
    });

    let id_plague_jer;
    let id_price_list;
    let priceRefuerzo;
    let total;
    let totalComplete;
    let speech;
    let traps;
    let stations;
    let speechExtra;
    let speechComplete = '';
    let discountFinal;
    let discountPercentage;
    let isPhone = true;
    let idQuotationResponse;
    let cellphoneSpech;

    let customerName;
    let establishmentName;
    let cellphone;
    let cellphoneMain;
    let plague;
    let establishment;
    let email;
    let colony;
    let municipality;
    let sourceOrigin;
    let pesticide;
    let sucursal;
    let messenger;
    let extra;

    let construction_measure;
    let discount;
    let price;
    let priceD;
    let id_quotation;
    let id;
    let id_customer;
    let contador;
    let validate;

    //Validate time in real
    const formulario = document.getElementById('formEditQuotation');
    const inputs = document.querySelectorAll('#formEditQuotation input');

    const fieldsQuaotationForm = {
        customerNameE: false,
        cellphoneE: false,
        establishmentNameE: false,
        cellphoneMainE: false,
        emailE: false,
        colonyE: false,
        municipalityE: false,
        construction_measureE: false,
        messengerE: false
    }

    // Class maked //Event Change establishment
    const establishmentE = document.querySelector('#grupo__establishmentE');
    establishmentE.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__establishmentE').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // Class maked //Event Change sourceOrigin
    const sourceOriginE = document.querySelector('#grupo__sourceOriginE');
    sourceOriginE.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__sourceOriginE').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // Class maked //Event Change plagues
    const plagues = document.querySelector('#grupo__plagueE');
    plagues.addEventListener('change', (event) => {
        if (event.target.value.length != 0){
            document.getElementById('grupo__plagueE').classList.remove('formulario__grupo-incorrecto');
        }
    });

    setFields(fieldsQuaotationForm);

    const validateForm = (e) => {
        switch (e.target.name) {

            case "customerNameE":
                validateField(expressions.description, e.target.value, 'customerNameE');
                break;
            case "cellphoneE":
                validateField(expressions.phone, e.target.value, 'cellphoneE');
                break;
            case "establishmentNameE":
                validateField(expressions.description, e.target.value, 'establishmentNameE');
                break;
            case "cellphoneMainE":
                validateField(expressions.phone, e.target.value, 'cellphoneMainE');
                break;
            case "emailE":
                validateField(expressions.email, e.target.value, 'emailE');
                break;
            case "colonyE":
                validateField(expressions.description, e.target.value, 'colonyE');
                break;
            case "municipalityE":
                validateField(expressions.description, e.target.value, 'municipalityE');
                break;
            case "construction_measureE":
                validateField(expressions.digits, e.target.value, 'construction_measureE');
                break;
            case "messengerE":
                validateField(expressions.url, e.target.value, 'messengerE');
                break;

        }
    }

    inputs.forEach((input) => {
        if (input.name === 'cellphoneE') {
            input.addEventListener('keyup', function (e) {
                validateForm(e);
                findCustomers($(this).val());
            });
            input.addEventListener('blur', validateForm);
        } else {
            input.addEventListener('keyup', validateForm);
            input.addEventListener('blur', validateForm);
        }
    });

    //start modal EDIT customer and quotation
    $('#saveQuotationE').click(function() {

        //getPriceData(0);
        if (id_plague_jer == null) {
            id_plague_jer = $('#id_plague_jer').val();
        }

        customerName = $('#customerNameE').val();
        establishmentName = $('#establishmentNameE').val();
        cellphone = $('#cellphoneE').val();
        cellphoneMain = $('#cellphoneMainE').val();
        plague = $('#plagueE').val();
        establishment = $('#establishmentE').val();
        email = $('#emailE').val();
        colony = $('#colonyE').val();
        municipality = $('#municipalityE').val();
        sourceOrigin = $('#sourceOriginE').val();
        pesticide = $('#pesticideE').val();
        sucursal = $('#sucursal').val();
        messenger = $('#messengerE').val();
        extra = $('#extraE').val();

        construction_measure = $('#construction_measureE').val();
        discount = $('#discountE').val();
        id_quotation = $('#id_quotationE').text().trim();
        id = $('#id').val();
        id_customer = $('#id_customer').val();
        contador = plague.length;
        validate = $('#validate').val();

        cellphoneSpech = cellphoneMain;

        validateField(expressions.name, customerName, 'customerNameE');
        validateField(expressions.digits, construction_measure, 'construction_measureE');

        if(establishment == 0){
            document.getElementById('grupo__establishmentE').classList.add('formulario__grupo-incorrecto');
        }
        if(plague.length == 0){
            showToast('error','Por favor ingresa al menos una plaga','Ejemplo: Curacarachas, chinches, etc...')
        }
        if(sourceOrigin == 0){
            document.getElementById('grupo__sourceOriginE').classList.add('formulario__grupo-incorrecto');
        }

        if(getFields().customerNameE && getFields().construction_measureE && establishmentE != 0 && plague.length != 0 && sourceOriginE != 0){
            getPriceData(1);
            editQuotation();
        }
        else {
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }

    });
    //end modal EDIT customer

    //start get price quotation
    $('#priceQuotationE').click(function() {
        getPriceData(0);
    });

    function editQuotation() {
        let mess = 'Actualizando...';
        loaderPestWare(mess);
        $.ajax({
            type: 'PUT',
            url: route('update_quotation'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                name: customerName,
                establishmentName: establishmentName,
                cellphone: cellphone,
                cellphoneMain : cellphoneMain,
                plague: plague,
                establishment: establishment,
                email: email,
                address: colony,
                municipality: municipality,
                construction_measure: construction_measure,
                discount: discount,
                price: price,
                priceReinforcement: priceRefuerzo,
                id_quotation: id_quotation,
                contador: contador,
                id_plague_jer: id_plague_jer,
                id: id,
                id_customer: id_customer,
                total: total,
                sourceOrigin: sourceOrigin,
                pesticide: pesticide,
                messenger: messenger,
                validate: validate,
                extra: extra,
                sucursal: sucursal,
                description_whatsapp: speechComplete,
                id_price_list: id_price_list
            },
            success: function(data) {
                if (data.errors) {
                    Swal.close();
                    showToast('error', 'Error al actualizar cotización', 'Algo salio mal, intente de nuevo.');
                } else {
                    idQuotationResponse = data.id;
                    generateSpeeechTextEd()
                    Swal.close();
                    correct();
                    $("#saveQuotationE").prop("disabled", true);
                }
            }
        });
    }

    function getPriceData(isSave) {
        let mess = 'Cotizando...';
        loaderPestWare(mess);

        let plague = $('#plagueE').val();
        let construction_measure = $('#construction_measureE').val();
        let establishment = $('#establishmentE').val();
        let discount = $('#discountE').val();
        let extra = $('#extraE').val();
        let contador = plague.length;

        //validations
        if (establishment === null) {
            Swal.close();
            missingText('Ingresa el tipo de SERVICIO');
        } else if (contador === 0) {
            Swal.close();
            missingText('Ingresa por lo menos una PLAGA');
        } else if (construction_measure.length === 0) {
            Swal.close();
            missingText('Ingresa las MEDIDAS DE CONSTRUCCIÓN');
        } else {
            validatemin();
            getPrice();
        }

        function validatemin() {
            if (construction_measure < 50 && !(construction_measure.length === 0)) {
                construction_measure = 50;
            }
        }

        function getPrice() {
            $.ajax({
                type: 'GET',
                url: route('quotation_price'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    plague: plague,
                    construction_measure: construction_measure,
                    establishment: establishment,
                    contador: contador,
                    discount: discount,
                    extra: extra,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    if (data.response === 500) {
                        Swal.close();
                        missingText(data.message);
                    } else {
                        $('#priceQuotLabelEdit').html(`$${data.price}`);
                        $('#priceFinalQuotLabelEdit').html(`$${data.total}`);
                        id_plague_jer = data.id_plague;
                        priceRefuerzo = data.priceRefuerzo;
                        price = data.price;
                        total = data.total;
                        speech = data.speech;
                        traps = data.traps;
                        stations = data.stations;
                        speechExtra = data.extra;
                        discountPercentage = data.discount;
                        discountFinal = data.discountFinal;
                        id_price_list = data.id_price_list;
                        totalComplete = price + priceRefuerzo;
                        Swal.close();
                        if (isSave == 1) editQuotation();
                    }
                }
            });
        }
    }
    //end get price quotation

    function generateSpeeechTextEd() {
        let discount = $('#discountE').val();
        let discountText = $('#discountE option:selected').text();
        let establishment_id = $('#establishmentE').val();
        let discountSpeech = '';
        let urlWhatsapp = '';

        if (discountPercentage != 0) {
            discountSpeech = 'Durante este mes te otorgamos un ' + discountPercentage +
                '% de descuento, por lo que tu servicio quedaría en $' + discountFinal + '.';
        }

        speechComplete = speech + ' ' + discountSpeech + ' ' + speechExtra;
    }

    function speechTextEd(cellphone, isCellphone) {

        let discount = $('#discountE').val();
        let discountText = $('#discountE option:selected').text();
        let establishment_id = $('#establishmentE').val();
        let discountSpeech = '';
        let urlWhatsapp = '';
        let speechCompleteText = '';
        let codeCountry = $('#codeCountry').val();

        if (discountPercentage != 0) {
            discountSpeech = 'Durante este mes te otorgamos un ' + discountPercentage +
                '% de descuento, por lo que tu servicio quedaría en $' + discountFinal + '.';
        }

        speechComplete = speech;

        if (isCellphone) {
            Swal.fire({
                html: '<h4>' + speech + '<br><br>El costo de este servicio es de $' + price +
                    '<br>' + discountSpeech + '<br>' + speechExtra + '<br>' +
                    'El total de tu servicio es de $' + total + '</h4>',
                width: 1200,
                height: 500,
                focusConfirm: false,
                showCancelButton: true,
                showCloseButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'Copiar',
                cancelButtonText: 'WhatsApp',
                confirmButtonColor: '#aaa',
                cancelButtonColor: 'green',
                onClose: () => {
                    window.location.href = route('index_register')
                }
            }).then((result) => {
                if (result.value) {
                    speechCompleteText = speechComplete + ' El costo de este servicio es de $' + price +
                        discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de $' + total;
                    copySpeech(speechCompleteText);
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                )
                {
                    //sendWhatsapp
                    speechCompleteText = speechComplete + ' El costo de este servicio es de $' + price +
                        discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de $' + total;
                    urlWhatsapp = "https://api.whatsapp.com/send?phone=" + codeCountry + cellphone + "&text=" + speechCompleteText +
                        "%0d%0dCotización: https://pestwareapp.com/quotation/" + idQuotationResponse + "/PDF";
                    window.open(urlWhatsapp, '_blank');
                    window.location.href = route('index_register')
                }
            });
        } else {
            Swal.fire({
                html: '<h4>' + speech + '<br><br>El costo de este servicio es de $' + price + '<br>' +
                    discountSpeech + '<br>' + speechExtra + '<br>' +
                    'El total de tu servicio es de $' + total + '</h4>',
                width: 1200,
                height: 500,
                focusConfirm: false,
                showCloseButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'Copiar',
                confirmButtonColor: '#aaa',
                onClose: () => {
                    window.location.href = route('index_register')
                }
            }).then((result) => {
                if (result.value) {
                    speechCompleteText = speechCompleteText + ' El costo de este servicio es de $' + price +
                        discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de $' + total;
                    copySpeech(speechCompleteText);
                    correct();
                } else(
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                )
            });
        }
    }

    function copySpeech(textSpeech) {
        navigator.clipboard.writeText(textSpeech).then(function() {
            window.location.href = route('index_register')
        }, function(err) {
        });
    }

    //start converters
    $('#MC').click(function() {
        converter();
    });

    $('#CE').click(function() {
        converterSchool();
    });

    $('#CS').click(function() {
        converterSpaces();
    });

    async function converter() {
        const { value: formValues } = await Swal({
            title: 'Convertidor de Medidas',
            confirmButtonText: 'Calcular',
            html: '<input min="0" type="number" id="swal-input1" class="form-control" style="margin-bottom: 10px;" placeholder="Frente (mts)">' +
                '<input min="0" type="number" id="swal-input2" class="form-control" style="margin-bottom: 10px;" placeholder="Fondo (mts)">' +
                '<input min="0" type="number" id="swal-input3" class="form-control" placeholder="# pisos (mts)">',
            showCloseButton: true,
            focusConfirm: true,
            preConfirm: () => {
                return [
                    document.getElementById('swal-input1').value,
                    document.getElementById('swal-input2').value,
                    document.getElementById('swal-input3').value
                ]
            }
        });

        if (formValues) {
            total = document.getElementById('swal-input1').value * document.getElementById('swal-input2').value * document.getElementById('swal-input3').value;
            document.getElementById("construction_measureE").value = total;
        }
    }

    async function converterSchool() {
        const { value: formValues } = await Swal({
            title: 'Convertidor de Escuela',
            confirmButtonText: 'Calcular',
            width: '500px',
            html: '<div class="row">' +
                '<br>' +
                '<div class="col-md-4">' +
                '<h5>Salones</h5>' +
                '<input min="0" type="number" value="0" id="salones" class="form-control">' +
                '<h5>Bodegas</h5>' +
                '<input min="0" type="number" value="0" id="bodegas" class="form-control">' +
                '<h5>Laboratorios</h5>' +
                '<input min="0" type="number" value="0" id="laboratorios" class="form-control">' +
                '<h5>Sala de espera</h5>' +
                '<input min="0" type="number" value="0" id="salaEspera" class="form-control">' +
                '<h5>Juegos infantiles</h5>' +
                '<input min="0" type="number" value="0" id="juegosInfantiles" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Oficinas</h5>' +
                '<input min="0" type="number" value=0 id="oficinas" class="form-control">' +
                '<h5>Cooperativa</h5>' +
                '<input min="0" type="number" value=0 id="cooperativa" class="form-control">' +
                '<h5>Auditorio</h5>' +
                '<input min="0" type="number" value=0 id="auditorio" class="form-control">' +
                '<h5>Gimnasio</h5>' +
                '<input min="0" type="number" value=0 id="gimnasio" class="form-control">' +
                '<h5>Otros</h5>' +
                '<input min="0" type="number" value=0 id="otros" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Baños</h5>' +
                '<input min="0" type="number" value=0 id="baños" class="form-control">' +
                '<h5>Biblioteca</h5>' +
                '<input min="0" type="number" value=0 id="biblioteca" class="form-control">' +
                '<h5>Cafetería</h5>' +
                '<input min="0" type="number" value=0 id="cafeteria" class="form-control">' +
                '<h5>Cuarto de máquinas</h5>' +
                '<input min="0" type="number" value=0 id="cuartoMaquinas" class="form-control">' +
                '</div>' +
                '</div>',
            showCloseButton: true,
            focusConfirm: true,
            preConfirm: () => {
                return [
                    document.getElementById('salones').value,
                    document.getElementById('bodegas').value,
                    document.getElementById('laboratorios').value,
                    document.getElementById('salaEspera').value,
                    document.getElementById('juegosInfantiles').value,
                    document.getElementById('oficinas').value,
                    document.getElementById('cooperativa').value,
                    document.getElementById('auditorio').value,
                    document.getElementById('gimnasio').value,
                    document.getElementById('otros').value,
                    document.getElementById('baños').value,
                    document.getElementById('biblioteca').value,
                    document.getElementById('cafeteria').value,
                    document.getElementById('cuartoMaquinas').value
                ]
            }
        });

        if (formValues) {
           let suma = parseInt(document.getElementById('salones').value) +
                parseInt(document.getElementById('bodegas').value) +
                parseInt(document.getElementById('laboratorios').value) +
                parseInt(document.getElementById('salaEspera').value) +
                parseInt(document.getElementById('juegosInfantiles').value) +
                parseInt(document.getElementById('oficinas').value) +
                parseInt(document.getElementById('cooperativa').value) +
                parseInt(document.getElementById('auditorio').value) +
                parseInt(document.getElementById('gimnasio').value) +
                parseInt(document.getElementById('otros').value) +
                parseInt(document.getElementById('baños').value) +
                parseInt(document.getElementById('biblioteca').value) +
                parseInt(document.getElementById('cafeteria').value) +
                parseInt(document.getElementById('cuartoMaquinas').value);

            document.getElementById("construction_measureE").value = suma * 50;
        }
    }

    async function converterSpaces() {
        const { value: formValues } = await Swal({
            title: 'Convertidor de Espacios',
            confirmButtonText: 'Calcular',
            width: '500px',
            html: '<div class="row">' +
                '<br>' +
                '<div class="col-md-4">' +
                '<h5>Pisos</h5>' +
                '<input value="0" min="0" type="number" id="pisos" class="form-control">' +
                '<h5>Cocina</h5>' +
                '<input min="0" type="number" value=0 id="cocina" class="form-control">' +
                '<h5>Sala</h5>' +
                '<input min="0" type="number" value=0 id="sala" class="form-control">' +
                '<h5>Comedor</h5>' +
                '<input min="0" type="number" value=0 id="comedor" class="form-control">' +
                '<h5>Recibidor</h5>' +
                '<input min="0" type="number" value=0 id="recibidor" class="form-control">' +
                '<h5>Baños</h5>' +
                '<input min="0" type="number" value=0 id="baños" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Recamaras</h5>' +
                '<input min="0" type="number" value=0 id="recamaras" class="form-control">' +
                '<h5>Salas de TV</h5>' +
                '<input min="0" type="number" value=0 id="salaTv" class="form-control">' +
                '<h5>Patio o Jardín</h5>' +
                '<input min="0" type="number" value=0 id="patio" class="form-control">' +
                '<h5># Autos cochera</h5>' +
                '<input min="0" type="number" value=0 id="numeroAutosCochera" class="form-control">' +
                '<h5>Cuarto de lavado</h5>' +
                '<input min="0" type="number" value=0 id="cuartoLavado" class="form-control">' +
                '<h5>Cuarto de servicio</h5>' +
                '<input min="0" type="number" value=0 id="cuartoServicio" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Estudio</h5>' +
                '<input min="0" type="number" value=0 id="estudio" class="form-control">' +
                '<h5>Terraza</h5>' +
                '<input min="0" type="number" value=0 id="terraza" class="form-control">' +
                '<h5>Roof Garden</h5>' +
                '<input min="0" type="number" value=0 id="roofGarden" class="form-control">' +
                '<h5>Gimnasio</h5>' +
                '<input min="0" type="number" value=0 id="gimnasio" class="form-control">' +
                '<h5>Salon de juegos</h5>' +
                '<input min="0" type="number" value=0 id="salonJuegos" class="form-control">' +
                '</div>' +
                '</div>',
            showCloseButton: true,
            focusConfirm: true,
            preConfirm: () => {
                return [
                    document.getElementById('pisos').value,
                    document.getElementById('cocina').value,
                    document.getElementById('sala').value,
                    document.getElementById('comedor').value,
                    document.getElementById('recibidor').value,
                    document.getElementById('baños').value,
                    document.getElementById('recamaras').value,
                    document.getElementById('salaTv').value,
                    document.getElementById('patio').value,
                    document.getElementById('numeroAutosCochera').value,
                    document.getElementById('cuartoLavado').value,
                    document.getElementById('cuartoServicio').value,
                    document.getElementById('estudio').value,
                    document.getElementById('terraza').value,
                    document.getElementById('roofGarden').value,
                    document.getElementById('gimnasio').value,
                    document.getElementById('salonJuegos').value
                ]
            }
        });

        if (formValues) {
           let suma = (parseInt(document.getElementById('pisos').value) * 5) +
                (parseInt(document.getElementById('baños').value) * 10) +
                (parseInt(document.getElementById('cocina').value) * 20) +
                (parseInt(document.getElementById('comedor').value) * 20) +
                (parseInt(document.getElementById('cuartoLavado').value) * 15) +
                (parseInt(document.getElementById('cuartoServicio').value) * 15) +
                (parseInt(document.getElementById('recamaras').value) * 15) +
                (parseInt(document.getElementById('sala').value) * 20) +
                (parseInt(document.getElementById('salaTv').value) * 15) +
                (parseInt(document.getElementById('recibidor').value) * 10) +
                (parseInt(document.getElementById('patio').value) * 40) +
                (parseInt(document.getElementById('numeroAutosCochera').value) * 15) +
                (parseInt(document.getElementById('gimnasio').value) * 25) +
                (parseInt(document.getElementById('estudio').value) * 20) +
                (parseInt(document.getElementById('roofGarden').value) * 80) +
                (parseInt(document.getElementById('salonJuegos').value) * 40) +
                (parseInt(document.getElementById('terraza').value) * 25);

            document.getElementById("construction_measureE").value = suma;
        }
    }
    //end converters

    //alerts messages
    function correct() {
        showToast('success', 'Cotización Actualizada', 'Se actualizo la cotización.');
        setTimeout(() => {
            speechTextEd(cellphoneSpech, isPhone);
        }, 1000);
    }

    function missingText(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            showCancelButton: false,
            showConfirmButton: true
        });
    }
    //end alert messages

    function missingTextCE(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
        });
    }
});
