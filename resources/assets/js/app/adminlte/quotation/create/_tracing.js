/**
 * @author Alberto Martínez
 * @version 08/03/2019
 * register/_tracing.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";
import {expressions, setFields, validateField} from "../../validations/regex";

$(document).ready(function(){

    $('.openModalTracingQuot').click(function () {
        let all = $(this);
        getTracings(all.attr('data-id'));
        $('#idQuotationT').val(all.attr('data-id'));
        $("#quotTracingKey").html(all.attr('data-quotation'));
    });

    $("#tracingQ").on('hidden.bs.modal', function () {
        clearFormTracing();
    });

    // validations section ->
    const commentsTracing = document.getElementById('commentsT');

    const fieldsTracingQuotationForm = {
        commentsT: true
    }
    setFields(fieldsTracingQuotationForm);

    const validateFormTracing = (e) => {
        switch (e.target.name) {
            case "commentsT":
                validateField(expressions.descriptionNotRequired, e.target.value, 'commentsT');
                break;
        }
    }

    commentsTracing.addEventListener('keyup', validateFormTracing);
    commentsTracing.addEventListener('blur', validateFormTracing);

    //START Tracing Quotation
    $('#saveQuotationTracing').click(function(){

        let id_quotation = $('#idQuotationT').val();
        let dateTracing = $('#dateTracing').val();
        let hourTracing = $('#hourTracing').val();
        let commentsTracing = $('#commentsT').val();

        tracingSave(id_quotation, dateTracing, hourTracing, commentsTracing);

    });

    function getTracings(quotationId) {
        loaderPestWare('');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('tracing_view', quotationId),
            data: {
                _token: token
            },
            success: function(response) {
                if (response.code === 500) {
                    Swal.close();
                    showToast('error', 'Error al cargar', response.message);
                } else {
                    let rows = '';
                    response.data.forEach((element) => {
                        let commentary = element.commentary;
                        if (commentary === null) commentary = 'Sin comentarios.';
                        rows += `<tr>
                                <td class="text-center">${element.id}</td>
                                <td class="text-center">${element.tracing_date}</td>
                                <td class="text-center">${element.tracing_hour}</td>
                                <td class="text-center">${commentary}</td>
                        </tr>`;
                    });
                    $('#tableBodyDetail').append(rows);
                    Swal.close();
                }
            }
        });
    }

    function tracingSave(id_quotation, dateTracing, hourTracing, commentsTracing){
        loaderPestWare('Guardando Seguimiento...');
        $.ajax({
            type: 'POST',
            url: route('tracing_quotation'),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                id_quotation: id_quotation,
                dateTracing: dateTracing,
                hourTracing: hourTracing,
                commentsTracing: commentsTracing
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error al guardar', 'Algo salio mal, intente de nuevo.');
                }
                else{
                    clearFormTracing();
                    Swal.close();
                    showToast('success-redirect', 'Seguimiento Guardado', 'El seguimiento se guardo correctamente.', 'index_register');
                    $("#saveQuotationTracing").attr('disabled', true);
                }
            }
        });
    }

    function clearFormTracing() {
        $('#tableBodyDetail').html('');
        document.getElementById('commentsT').value = '';
        document.getElementById('grupo__commentsT').classList.remove('formulario__grupo-incorrecto');
    }

});
