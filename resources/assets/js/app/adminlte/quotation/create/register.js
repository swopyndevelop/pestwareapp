/**
 * @author Alberto Martínez
 * @version 16/01/2019
 * register/_quotation.blade.php
 */
import {expressions, getFields, setFields, validateField} from "../../validations/regex";
import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function() {

    const lastQuotationIntroJsNew = document.getElementById('totalProgrammed2').value;
    const introJsPest = document.getElementById('introJsInpunt').value;
    const symbolCountry = document.getElementById('symbolCountry').value;
    const introUpgrade = document.getElementById('introUpgrade').value;

    let urlCurrent = window.location.pathname;
    let viewIndex = '/index_register';
    let urlBack = document.referrer;
    let urlSeparate = urlBack.split('/');
    let viewInstance = 'edit';

    if (introUpgrade == 0) {
        startUpdateManual();

        function startUpdateManual(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        title: "Actualización PestWareApp",
                        intro: "Histórico de Cliente <br> " +
                            "Accesos directo en Calendario <br> " +
                            "Fecha de Vencimiento en Certificados <br> ",
                        position: "center"
                    },
                    {
                        title: "Histórico de Cliente",
                        intro:"Con el nuevo módulo Histórico de Cliente podrás dar seguimiento completo a tus clientes desde el primer día en que se creo, así como " +
                            "visualizar en tiempo real todo lo que sucede con tu cliente durante la operación del día a día. <br>" +
                            "<img src=\"https://pwa-public.s3.us-west-1.amazonaws.com/steps_upgrades/step_1.jpeg\" alt=\"\" width=\"500px\" height=\"250px\">",
                    },
                    {
                        title: "Accesos directo en calendario",
                        intro:"Accede a todas las opciones de un evento u orden de servicio desde el calendario con mayor facilidad. <br>" +
                            "<img src=\"https://pwa-public.s3.us-west-1.amazonaws.com/steps_upgrades/step_2.jpeg\" alt=\"\" width=\"500px\" height=\"250px\">",
                    },
                    {
                        title: "Fecha de Vencimiento en Certificados",
                        intro:"Configura fechas de vencimiento para los certificados de tus clientes por tipo de servicio. <br>" +
                            "<img src=\"https://pwa-public.s3.us-west-1.amazonaws.com/steps_upgrades/step_3.jpeg\" alt=\"\" width=\"500px\" height=\"250px\">",
                    },
                    {
                        title: "Ocultar Precio en Certificado y App",
                        intro:"Personaliza tus certificados para mostrar o no el precio en los certificados, así como a los técnicos en la app móvil. <br>" +
                            "<img src=\"https://pwa-public.s3.us-west-1.amazonaws.com/steps_upgrades/step_4.jpeg\" alt=\"\" width=\"500px\" height=\"250px\">",
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false
            });
            intro.start();
            intro.onexit(function() {

                Swal.fire({
                    title: 'Actualización PestWareApp',
                    text: "¿Desea ver más tarde?",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.value) {
                        //console.log('if')
                    } else {
                        updateAjaxIntroUpgrade(true);
                    }
                })

            });

        }

        function updateAjaxIntroUpgrade(introUpgrade){
            $.ajax({
                type: 'POST',
                url: route('change_intro_upgrade_user'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    introUpgrade: introUpgrade
                },
                success: function(data) {
                    //console.log('OK');
                }
            });
        }
    }

    /*if (urlCurrent == viewIndex){
        if (introJsPest == 1){
            setTimeout(() => {
                startMenuIndex();
            }, 1000);
        }
        //startMenuIndex();
        function startMenuIndex(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        intro: "Bienvenido al sistema PestWare App",
                        position: "center"
                    },
                    {
                        title: "Tutorial de Inicio",
                        intro: "A continuación te explicaremos paso a paso como utilizar cada una de las funcionalidades de Pestware App, puedes salir del" +
                            " Tutorial en cualquier momento dando click fuera del cuadro de diálogo. Si deseas retomar el tutorial dando click en el signo de " +
                            "interrogación que se encuentra en la parte superior Derecha." ,
                        position: "center"
                    },
                    {
                        title: "Pestware",
                        intro: "Bienvenido a PestWare App, con pestware app podrás tener toda la administración de tu negocio" +
                            " en la palma de tu mano, todos tus procesos estarán digitalizados e integrados en un sólo lugar con el" +
                            " software para empresas de control de plagas más sencillo y amigable.",
                        position: "center"
                    },
                    {
                        element: '#universityMenu',
                        title:"Pestware Academy",
                        intro: "Pestware Academy es la plataforma en la cual encontrarás diversos cursos y contenido digital " +
                            "para capacitarte y aprender día con día diferentes estrategias para mejorar tu negocio.",
                        position: "right"
                    },
                    {
                        element: '#attentionClientMenu',
                        title:"Anteción al Cliente",
                        intro: "El contacto con el cliente es fundamental, en este módulo podrás crear cotizaciones profesionales con tu imagen. " +
                            "Dar seguimiento a cada una de ellas será mas sencillo con todas las herramientas de éste módulo. Una vez que te " +
                            "contraten podrás convertirlas cotizaciones en Órdenes de Servicio y programarlas en tu Agenda de Trabajo. Si quieres ver " +
                            "todas las funcionalidades da click en el botón siguiente:",
                        position: "right"
                    },
                    {
                        title: "Video Explicativo",
                        intro:"<iframe width=\"560\" height=\"315\" src=\"https://player.vimeo.com/video/499418668\" frameborder=\"0\" allow=\"accelerometer; autoplay= true; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
                    },
                    {
                        element: '#scheduleMenu',
                        title:"Agenda",
                        intro: "En este módulo podrás programar, gestionar y monitorear todas tus Órdenes de Servicio. Pestware App se conecta a tu Whatsapp por lo " +
                            "que podrás enviar indicaciones de preparación, recordatorios o certificados por este medio. Podrás programar Garantías, Refuerzos o " +
                            "Seguimientos, así como visualizar los resultados de las aplicaciones, evaluar a tus técnicos, revisar tus servicios por cobrar entre más funciones."
                            ,
                        position: "right"
                    },
                    {
                        // element: '#monitorinStationsSide',
                        title:"Monitoreo",
                        intro: "Sabemos lo difícil que es llevar el seguimiento detallado de todas las estaciones de tus clientes por lo que lograrás eliminar al 100%" +
                            " los formularios en papel y la transcripción para tus reportes. Este módulo cuenta con etiquetas inteligentes por medio de códigos QR que " +
                            "te permitirán dar lectura a través de tu celular para registrar las actividades de cada estación de cebo, trampa de captura o lámparas UV. " +
                            "Podrás crear los diagramas de ubicación de estaciones así como revisar los reportes de las actividades registradas.",
                        position: "bottom"
                    },
                    {
                        element: '#managmentMenu',
                        title:"Administrativo",
                        intro: "Para que tu negocio prospere es importante que tengas el control de todos tus recursos por lo que Pestware App te ayuda con" +
                            " la administración de tu Inventario, Gastos y Cobranza, explora las herramientas que hemos diseñado para que logres conocer la " +
                            "situación financiera de tu negocio.\n",
                        position: "right"
                    },
                    {
                        element: '#businessIntellinge',
                        title:"Negocios",
                        intro: "Lo que no se mide no se controla, ahora contarás con Reportes Inteligentes que te permitirán conocer los resultados de tu negocio " +
                            "de manera ágil y sencilla, por medio de gráficas y estadísticos que te ayudarán a la toma de decisiones para mejorar tu servicio, " +
                            "eficientizar recursos y crear estrategias para captar y retener a tus clientes."
                        ,
                        position: "right"
                    },
                    {
                        element: '#settingPanelMenu',
                        title:"Panel de Configuración",
                        intro: "Pestware App es 100% personalizable y se adapta a tus necesidades por lo que podrás crear y administrar la estructura operacional " +
                            "de tu empresa. Podrás personalizar tus formatos y plantillas con tu logo, textos e información general de tu empresa. Crea tus propias " +
                            "listas de precio, catálogos de plagas, equipos de aplicación, formas de pago, tipos de servicio, etc."
                        ,
                        position: "right"
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false
            });
            intro.start();
            intro.onexit(function() {
                updateAjaxIntroJs(0);
            });
            intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {

                setTimeout(() => {
                    updateAjaxIntroJs(1);
                    setTimeout(() => {
                        startSettingPanel();
                    }, 1000);
                }, 500);
            });
            //intro.exit();

            /!* intro.setOption('Listo', 'Cotizar').start().oncomplete(function() {
                 $('#newQ').modal('show');
                 intro.exit();
                 setTimeout(() => {
                     startNewQuotation();
                 }, 1000);
             });*!/
        }

        //startSettingPanel();
        function startSettingPanel(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#settingPanelMenu',
                        title:"Panel de Configuración",
                        intro: "<div class='text-center'>" +
                            "<button class='btn' id='menuManagmentPanelButton'>Panel de Configuración</button></div>",
                        position: "right"
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false
            });
            intro.start();
            intro.onexit(function() {
                updateAjaxIntroJs(0);
            });
            document.getElementById('menuManagmentPanelButton').onclick = function() {
                intro.exit();
                setTimeout(() => {
                    $('#liPanelSettingPanel').addClass('active');
                    setTimeout(() => {
                        $('#liPanelSettingPanel').addClass('menu-open');
                        setTimeout(() => {
                            ulSideConfiguration();
                        }, 500);
                    }, 150);
                }, 150);
            }
        }

        //ulSideConfiguration();
        function ulSideConfiguration(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#liPanelSetting',
                        title:"Configuración",
                        intro: "<div class='text-center'>" +
                            "<button class='btn' id='sideConfigurationButton'>Configuración</button></div>",
                        position: "right"
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo"
            });
            intro.start();
            document.getElementById('sideConfigurationButton').onclick = function() {
                intro.exit();
                setTimeout(() => {
                    $('#liPanelSetting').addClass('active');
                    setTimeout(() => {
                        $('#ulMagngmentConfigSide').addClass('menu-open');
                        setTimeout(() => {
                            ulSideConfiguration2();
                        }, 500);
                    }, 150);
                }, 150);
            }
        }

        //ulSideConfiguration();
        function ulSideConfiguration2(){
        let intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: '#customConfigSideDiv',
                    title:"Personalizar",
                    intro: "<div class='text-center'>" +
                        "<button class='btn' id='customSideConfigurationButton'>Personalizar</button></div>",
                    position: "right"
                },
            ],
            nextLabel: "Siguiente",
            prevLabel: "Anterior",
            doneLabel: "Listo"
        });
        intro.start();
        document.getElementById('customSideConfigurationButton').onclick = function() {

            updateAjaxIntroJs(1);

            setTimeout(() => {
                window.location.href = route('edit_config_instance')
            }, 1000);

            intro.exit();
        }
    }
    }
    if (urlSeparate[3] == viewInstance){
        if (introJsPest == 1){
            setTimeout(() => {
                startQuotation();
            }, 1000);
        }
            //startQuotation();
        function startQuotation(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#callTitleIndex',
                        title:"Cotización",
                        intro: "",
                        position: "right"
                    },
                    {
                        element: '#newQuotation',
                        title:"Cotización",
                        intro: "Para crear una cotización lo primero será dar click en el botón azul (+).",
                        position: "right",
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false
            });
            intro.start();
            intro.onexit(function() {
                updateAjaxIntroJs(0);
            });
            intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
                $('#newQ').modal('show');
                intro.exit();
                setTimeout(() => {
                    startNewQuotation();
                }, 1000);
            });
        }

        //startNewQuotation();
        function startNewQuotation(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#modalTitle',
                        title:"Cotización Nueva",
                        position: "right"
                    },
                    {
                        element: '#grupo__customerName',
                        title:"Nombre del cliente",
                        intro: "Ingresa el nombre de tu cliente a cotizar.",
                        position: "right"
                    },
                    {
                        element: '#grupo__cellphone',
                        title:"Teléfono",
                        intro: "Si tu cliente cuenta con Whatsapp puedes aprovechar este campo para posteriormente enviarle la cotización, indicaciones, " +
                            "recordatorios y sus certificados. En caso que ya haya sido registrado anteriormente solo con ingresar el número de teléfono se " +
                            "llenarán el resto de los campos.",
                        position: "right"
                    },
                    {
                        element: '#grupo__establishmentName',
                        title:"Empresa",
                        intro: "En caso de que tu cliente tenga un negocio ingresa aquí su nombre comercial para identificarlo mejor.",
                        position: "right"
                    },
                    {
                        element: '#grupo__emailCustomer',
                        title:"Correo",
                        intro: "Si tu cliente cuenta con correo electrónico puedes aprovechar este campo para porteriormente enviarle " +
                            "la cotización, certificados y algún medio de comunicación.",
                        position: "right"
                    },
                    {
                        element: '#grupo__colony',
                        title:"Colonia",
                        intro: "Puedes aprovechar en adelantar algunos datos del domicilio de tu cliente como es la colonia.",
                        position: "right"
                    },
                    {
                        element: '#grupo__municipality',
                        title:"Municipio",
                        intro: "Puedes aprovechar en adelantar algunos datos del domicilio de tu cliente como es el municipio.",
                        position: "right"
                    },
                    {
                        element: '#grupo__establishment',
                        title:"Tipo de Servicio",
                        intro: "El tipo de servicio es fundamental e importante para realizar la cotización ya que el cotizador funciona" +
                            " a través de Listas de Precio las cuales se crean partiendo de los Tipos de Servicio.",
                        position: "top"
                    },
                    {
                        element: '#grupo__construction_measureDiv',
                        title:"Medidas de construcción",
                        intro: "Las medidas de construcción son la parte principal para determinar el costo dentro de la Lista de Precio por" +
                            " lo que deberás ingresar las medidas de los metros cuadrados. Cuentas con 3 convertidores que te ayudarán a calcular " +
                            "las medidas en caso que tu cliente no sepa sus dimensiones.",
                        position: "right"
                    },
                    {
                        element: '#MC',
                        title:"Medidas Generales",
                        intro: "En el siguiente convertidor para calcular las dimensiones solo requieres ingresar las medidas de frente, fondo y el número de pisos.",
                        position: "right"
                    },
                    {
                        element: '#CE',
                        title:"Medidas Escuelas",
                        intro: "Este convertidor es exclusivo para cuando requieras cotizar una Escuela, para determinar las medidas aproximadas solo es necesario " +
                            "ingresar la cantidad de salones, baños, laboratorios, bodegas, entre otras zonas específicas que podemos encontrar en escuelas.",
                        position: "right"
                    },
                    {
                        element: '#CS',
                        title:"Medidas de Espacio",
                        intro: "Podrás ingresar los espacios cómunes como cuartos, cocinas, baño etc que te ayudarán a sacar las medidas " +
                            "para poder ofrecer el mejor servicio.",
                        position: "right"
                    },
                    {
                        element: '#grupo__plagues',
                        title:"Tipo de plagas",
                        intro: "Finalmente para cotizar es necesario ingreasar una o más plagas con las que cuente tu cliente.",
                        position: "right"
                    },{
                        element: '#grupo__sourceOrigin',
                        title:"Fuente de Origen",
                        intro: "Es importante clasificar los medios por los cuales llegan tus clientes para posteriormente aprovechar " +
                            "está información.",
                        position: "right"
                    },{
                        element: '#grupo__messenger',
                        title:"Facebook",
                        intro: "En caso de que tu cliente te contacte por medio de facebook o messenger ingresa la liga del chat " +
                            "para posteriormente mantener una comunicación.",
                        position: "right"
                    },
                    {
                        element: '#disccountDiv',
                        title:"Descuentos y Extras",
                        intro: "El cotizador de pestware te brinda la posibilidad de ofrecer descuentos y extras 100% personalizables " +
                            "a tus clientes.",
                        position: "right"
                    },
                    {
                        element: '#prizeDiv',
                        title:"Totales",
                        intro: "Finalmente podrás observar el subtotal y total cálculado por el cotizador.",
                        position: "right"
                    },
                    {
                        element: '#quotationPersonalizedDiv',
                        title:"Cotización Personalizada",
                        intro: "Sabemos que pueden existir clientes con situaciones muy particulares por lo que si alguna de las listas " +
                            "de precios no se adaptan a sus necesidades o bien si tienes que cotizar múltiples puntos como sucursales " +
                            "utiliza esta función la  descripción, precio y cantidad podrán ser personalizados al momento.",
                        position: "right"
                    },
                    {
                        element: '#saveQuotationDiv',
                        title:"Guardar Cotización",
                        intro: "Para calcular el precio sin guardar la cotización presiona el botón de Cotizar para tener una vista previa de " +
                            "cuanto será el costo antes de dárselo al cliente por si requieres hacer algún cambio. Finalmente si todo es correcto " +
                            "presiona en el botón Guardar para que puedas darle seguimiento a la cotización o bien programarla en una Orden de Servicio.",
                        position: "right"
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false
            });
            intro.start();
            intro.setOption('Anterior', 'Siguiente').start().oncomplete(function() {
                $('#newQ').click();
                setTimeout(() => {
                    startMenuTableIndex();
                }, 1000);
            });
        }

        //startMenuTableIndex();
        function startMenuTableIndex(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#masterContainer',
                        title:"Status Cotizaciones",
                        position: "right"
                    },
                    {
                        element: '#tableTheadIndexDiv',
                        title:"Filtros",
                        intro: "Podrás hacer uso de múltiples filtros que te ayudarán a encontrar fácilmente cotizaciones previas así como " +
                            "analizar el estatus y estadístico de cada una. Selecciona el filtro e ingresa los datos a buscar o selecciona la " +
                            "opción indicada y posteriormente da click en el botón de Filtrar",
                        position: "right"
                    },
                    {
                        element: '#menuTableUlDiv',
                        title:"Opciones de Cotización",
                        intro: "Pestware App cuenta con diferentes funcionalidades para gestionar las cotizaciones que se realizan, para conocerlas " +
                            "da click en el folio de la cotización y se desplegará un menú con las siguientes opciones:",
                        position: "left"
                    },
                ],
                nextLabel: "Siguiente",
                prevLabel: "Anterior",
                doneLabel: "Listo",
                keyboardNavigation: false
            });
            intro.start();
            intro.setOption('Listo', 'Siguiente').start().oncomplete(function() {
                intro.exit();
                setTimeout(() => {
                    $('#menuTableUlDiv').addClass('open');
                    setTimeout(() => {
                        startUlMenuTableIndex();
                    }, 500);
                }, 500);
            });
        }

        //startUlMenuTableIndex();
        function startUlMenuTableIndex(){
            let intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: '#scheduleMenuDiv',
                        title:"Cotización",
                        intro: `<div class="text-center">
                                <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color: #222d32;"></i> Editar</a>
                            </div>
                            <div class="text-center">
                                <a href="#"><i class="fa fa-calendar-plus-o" aria-hidden="true" style="color: #2E86C1;"></i> Programar</a>
                            </div>
                            <div class="text-center">
                                <a href="#"><i class="fa fa-window-close-o" aria-hidden="true" style="color: red;"></i> Rechazar</a>
                            </div>
                            <div class="text-center">
                                <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i> Descargar pdf</a>
                            </div>
                            <div class="text-center">
                                <a href="#"><i class="fa fa-whatsapp" aria-hidden="true" style="color: green;"></i> Enviar Cotización</a>
                            </div>
                            <div class="text-center">
                                <a href="#"><i class="fa fa-envelope-o" aria-hidden="true" style="color: #222d32;"></i> Enviar Cotización</a>
                            </div>
                           <div class="text-center">
                                <a href="#"><i class="fa fa-phone text-info" aria-hidden="true" style="color: #222d32;"></i> Seguimiento</a>
                            </div>
                            <hr>
                            <div class="text-center">
                                <button class='btn' id='scheduleMenuNext'>Programar</button>
                            </div><br>
                            <div class="text-center"><p>Para realizar la programación (contratación) de un servicio y posteriormente agendar da click en la opción 
                            programar.</p></div>
                            `,
                        position: "right"
                    },
                ],
                keyboardNavigation: false
            });
            intro.start();
            document.getElementById('scheduleMenuNext').onclick = function() {

                updateAjaxIntroJs(1);

                setTimeout(() => {
                    window.location.href = `../order/services/${lastQuotationIntroJsNew}`;
                }, 1000);
                intro.exit();
            };
        }
        //End Js
    }

    function updateAjaxIntroJs(introjs){
        $.ajax({
            type: 'POST',
            url: route('change_introjs_user'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                introjs: introjs
            },
            success: function(data) {
                //console.log('OK');
            }
        });
    }
    //End introJs*/

    //Global variable
    let isConfirmedCustomer = false;
    let jobCenterSelect = $('#selectJobCenter option:selected').val();

    //autocomplete cellphone
    let customerIdFind = 0;
    function findCustomers(query) {
        if (query != '') {
            $.ajax({
                type: 'GET',
                url: route('autocomplete_cellphone'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    query: query,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    $('#numberList').fadeIn();
                    $('#numberList').html(data);
                }
            });
        }
    }

    //autocomplete customerName
    function findCustomersName(query) {
        if (query != '') {
            $.ajax({
                type: 'GET',
                url: route('autocomplete_customer_name'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    query: query,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    $('#customerNameList').fadeIn();
                    $('#customerNameList').html(data);
                }
            });
        }
    }

    $(document).on('click', 'li#getdataClient', function() {
        $('#cellphone').val($(this).text());
        $('#numberList').fadeOut();
        //cargar datos de cliente
        let cellphoneLoad = $('#cellphone').val();
        $.ajax({
            type: 'GET',
            url: route('load_client'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                cellphone: cellphoneLoad,
                jobCenterSelect: jobCenterSelect
            },
            success: function(data) {
                $('#customerName').val(data.name).text();
                $('#establishmentName').val(data.establishment_name).text();
                $('#cellphoneMain').val(data.cellphone_main).text();
                $('#colony').val(data.colony).text();
                $('#municipality').val(data.municipality).text();
                $('#emailCustomer').val(data.email).text();
                $('#sourceOrigin').val(data.id_source_origin);
            }
        });
        $('#numberList').fadeOut();
    });

    $(document).on('click', 'li#getdataClientName', function() {
        $('#customerName').val($(this).text());
        $('#customerNameList').fadeOut();
        //cargar datos de cliente
        let customerName = $('#customerName').val();
        $.ajax({
            type: 'GET',
            url: route('load_client_name'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                customerName: customerName,
                jobCenterSelect: jobCenterSelect
            },
            success: function(data) {
                customerIdFind = data.id;
                $('#customerName').val(data.name).text();
                $('#establishmentName').val(data.establishment_name).text();
                $('#cellphone').val(data.cellphone).text();
                $('#cellphoneMain').val(data.cellphone_main).text();
                $('#colony').val(data.colony).text();
                $('#municipality').val(data.municipality).text();
                $('#emailCustomer').val(data.email).text();
                $('#sourceOrigin').val(data.id_source_origin);
            }
        });
        $('#customerNameList').fadeOut();
    });

    //end autocomplete

    //select type service
    $('#establishment').on('change', function() {
        let serviceId = $(this).val();
        loaderPestWare('Buscando plagas');
        $.ajax({
            type: 'GET',
            url: route('plagues_by_service', serviceId),
            data: {
                _token: $("meta[name=csrf-token]").attr("content")
            },
            success: function(data) {
                Swal.close();
                if (data.code === 500) {
                    showToast('warning', 'Error:', data.message);
                    $("#plagues").empty();
                }
                else {
                    // Load plague select
                    $("#plagues").empty();
                    data.plagues.forEach((element) => {
                        let option = `<option value="${element.id},${element.plague_key}"`;
                        option = option + `>${element.name}</option>`;
                        $("#plagues").append(option);
                    });
                }
            }
        });
    });

    //select inspection service
    $('#inspection').on('change', function() {
        let idQuotation = $(this).val();
        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('load_client_by_quotation'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idQuotation: idQuotation
            },
            success: function(data) {
                $('#customerName').val(data.name).text();
                $('#establishmentName').val(data.establishment_name).text();
                $('#cellphoneMain').val(data.cellphone_main).text();
                $('#colony').val(data.colony).text();
                $('#municipality').val(data.municipality).text();
                $('#emailCustomer').val(data.email).text();
                $('#sourceOrigin').val(data.id_source_origin);
                Swal.close();
            }
        });
    });

    //select job center
    $('#selectJobCenter').on('change', function() {
        let idJobCenter = $(this).val();
        window.location.href = route('index_register', idJobCenter);
    });

    //select discount
    $('#discount').on('change', function() {
        getCalculatePrice(1);
    });

    //select discount
    $('#extra').on('change', function() {
        getCalculatePrice(1);
    });

    let initialDateQuotation;
    let finalDateQuotation;

    $(function() {

        $('input[name="filterDateQuotation"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateQuotation"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateQuotation = picker.startDate.format('YYYY-MM-DD');
            finalDateQuotation = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateQuotation"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    $('#btnFilterQuotation').click(function() {
        //let idOS = $('#selectFilterOS option:selected').val();
        let customerName = $('#selectFilterCustomerNameQuot option:selected').val();
        let agentName = $('#selectFilterEmployeeNameQuot option:selected').val();
        let customerColony = $('#selectFilterColony option:selected').val();
        let key = $('#filterKeyQuot option:selected').val();
        let payment = $('#filterPaymentQuot option:selected').val();
        let status = $('#filterStatusQuot option:selected').val();

        initialDateQuotation = initialDateQuotation == undefined ? null : initialDateQuotation;
        finalDateQuotation = finalDateQuotation == undefined ? null : finalDateQuotation;

        window.location.href = `/index_register?initialDateQuotation=${initialDateQuotation}&finalDateQuotation=${finalDateQuotation}
            &agentName=${agentName}&customerName=${customerName}&customerColony=${customerColony}&key=${key}&payment=${payment}&status=${status}`;
    });

    let id_plague_jer;
    let id_price_list = '';
    let speech;
    let price;
    let priceRefuerzo;
    let traps;
    let stations;
    let total;
    let totalComplete;
    let last_id;
    let complete;
    let speechExtra;
    let speechComplete = '';
    let discountFinal;
    let discountPercentage;
    let isPhone = true;
    let cellphoneSpech;
    let cellphoneMainContact;
    let idQuotationResponse;
    let idQuotationResponseEncrypt;

    function getCalculatePrice(type) {
        //loading message
        let mess = 'Cotizando...';

        //get values input
        let plague = $('#plagues').val();
        let construction_measure = $('#construction_measure').val();
        let garden_measure = $('#garden_measure').val();
        let establishment = $('#establishment option:selected').val();
        let discount = $('#discount').val();
        let extra = $('#extra').val();
        let contador = plague.length;

        if(establishment == 0){
            document.getElementById('grupo__establishment').classList.add('formulario__grupo-incorrecto');
        }

        validateField(expressions.digits, construction_measure, 'construction_measure');

        //validations
        if (contador === 0) {
            showToast('error', 'Ingrese por lo menos plaga', '');
        }
        else if(establishment != 0 && getFields().construction_measure && contador != 0) {
            //validatemin();
            getPrice();
        }

        function validatemin() {
            if (construction_measure < 50 && !(construction_measure.length === 0)) {
                construction_measure = 50;
            }

            if (garden_measure < 50 && !(garden_measure.length === 0)) {
                garden_measure = 50;
            }
        }

        //get price from database
        function getPrice() {
            loaderPestWare(mess);
            $.ajax({
                type: 'GET',
                url: route('quotation_price'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    plague: plague,
                    construction_measure: construction_measure,
                    garden_measure: garden_measure,
                    establishment: establishment,
                    contador: contador,
                    discount: discount,
                    extra: extra,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    if (data.response === 500) {
                        Swal.close();
                        showToast('warning', 'Error al calcular el precio', data.message);
                    } else if (data.id_price_list === 0){
                        Swal.close();
                        showToast('warning', 'Error al calcular el precio', 'No se encontró lista de precio para los parametros seleccionados.');
                    } else {
                        $('#priceQuotLabel').html(`${symbolCountry}${data.price}`);
                        $('#priceFinalQuotLabel').html(`${symbolCountry}${data.total}`);
                        id_plague_jer = data.id_plague;
                        speech = data.speech;
                        price = data.price;
                        total = data.total;
                        priceRefuerzo = data.priceRefuerzo;
                        traps = data.traps;
                        stations = data.stations;
                        speechExtra = data.extra;
                        discountPercentage = data.discount;
                        id_price_list = data.id_price_list;
                        totalComplete = price + priceRefuerzo;
                        discountFinal = data.discountFinal;
                        Swal.close();
                        if (type == 2) saveQuotationAjax();
                        if (type == 3) {
                            $('#idPriceListCustom').val(id_price_list);
                            $('#ModalQuotationCustomTest').modal('show');
                        }
                    }
                }
            });
        }
    }


    //start modal new customer and quotation
    $('#saveQuotation').click(function() {
        saveQuotation();
    });
    //end modal new customer

    //Validate time in real
    const formulario = document.getElementById('formNewQuotation');
    const inputs = document.querySelectorAll('#formNewQuotation input');

    const fieldsQuaotationForm = {
        customerName: false,
        cellphone: false,
        establishmentName: false,
        cellphoneMain: false,
        emailCustomer: false,
        colony: false,
        municipality: false,
        construction_measure: false,
        messenger: false
    }

    // Class maked //Event Change establishment
    const establishment = document.querySelector('#grupo__establishment');
    establishment.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__establishment').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // Class maked //Event Change sourceOrigin
    const sourceOrigin = document.querySelector('#grupo__sourceOrigin');
    sourceOrigin.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__sourceOrigin').classList.remove('formulario__grupo-incorrecto');
        }
    });

    // Class maked //Event Change plagues
    const plagues = document.querySelector('#grupo__plagues');
    plagues.addEventListener('change', (event) => {
        if (event.target.value.length != 0){
            document.getElementById('grupo__plagues').classList.remove('formulario__grupo-incorrecto');
        }
    });

    setFields(fieldsQuaotationForm);

    const validateForm = (e) => {
        switch (e.target.name) {

            case "customerName":
                validateField(expressions.description, e.target.value, 'customerName');
                break;
            case "cellphone":
                validateField(expressions.phone, e.target.value, 'cellphone');
                break;
            case "establishmentName":
                validateField(expressions.description, e.target.value, 'establishmentName');
                break;
            case "cellphoneMain":
                validateField(expressions.phone, e.target.value, 'cellphoneMain');
                break;
            case "emailCustomer":
                validateField(expressions.email, e.target.value, 'emailCustomer');
                break;
            case "colony":
                validateField(expressions.description, e.target.value, 'colony');
                break;
            case "municipality":
                validateField(expressions.description, e.target.value, 'municipality');
                break;
            case "construction_measure":
                validateField(expressions.digits, e.target.value, 'construction_measure');
                break;
            case "messenger":
                validateField(expressions.url, e.target.value, 'messenger');
                break;

        }
    }

    inputs.forEach((input) => {
        if (input.name === 'cellphone') {
            input.addEventListener('keyup', function (e) {
                validateForm(e);
                findCustomers($(this).val());
            });
            input.addEventListener('blur', validateForm);
        } else if(input.name === 'customerName') {
            input.addEventListener('keyup', function (e) {
                validateForm(e);
                findCustomersName($(this).val());
            });
            input.addEventListener('blur', validateForm);
        }
        else {
            input.addEventListener('keyup', validateForm);
            input.addEventListener('blur', validateForm);
        }
    });

    //save quotation from database
    function saveQuotation() {

        // Validate time real with class
        //get values input
        let customerName = $('#customerName').val();
        let cellphone = $('#cellphone').val();
        let cellphoneMain = $('#cellphoneMain').val();
        cellphoneSpech = cellphone;
        cellphoneMainContact = cellphoneMain;
        let plague = $('#plagues').val();
        let establishment = $("#establishment option:selected").val();
        let sourceOrigin = $("#sourceOrigin option:selected").val();

        let construction_measure = $('#construction_measure').val();

        validateField(expressions.description, customerName, 'customerName');
        validateField(expressions.digits, construction_measure, 'construction_measure');

        if(establishment == 0){
            document.getElementById('grupo__establishment').classList.add('formulario__grupo-incorrecto');
        }
        if(plague.length == 0){
            document.getElementById('grupo__plagues').classList.remove('formulario__grupo-incorrecto');
        }
        if(sourceOrigin == 0){
            document.getElementById('grupo__sourceOrigin').classList.add('formulario__grupo-incorrecto');
        }

        if(getFields().customerName && getFields().construction_measure && establishment != 0 && plague.length != 0 && sourceOrigin != 0){

            generateSpeeechText();
            getCalculatePrice(2);
        }
        else {
            document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }

    }

    function saveQuotationAjax() {

        //get values input
        let customerName = $('#customerName').val();
        let establishmentName = $('#establishmentName').val();
        let cellphone = $('#cellphone').val();
        let cellphoneMain = $('#cellphoneMain').val();
        let plague = $('#plagues').val();
        let establishment = $("#establishment option:selected").val();
        let jobCenter = $("#selectJobCenter option:selected").val();
        let email = $('#emailCustomer').val();
        let colony = $('#colony').val();
        let municipality = $('#municipality').val();
        let sourceOrigin = $("#sourceOrigin option:selected").val();
        let pesticide = $('#pesticide').val();
        let messenger = $('#messenger').val();
        let extra = $('#extra').val();
        let inspection = $("#inspection option:selected").val();

        let construction_measure = $('#construction_measure').val();
        let garden_measure = $('#garden_measure').val();
        let discount = $('#discount').val();
        let id_quotation = $('#id_quotation').text().trim();
        let contador = plague.length;

        loaderPestWare('Guardando Cotización...');
        $.ajax({
            type: 'POST',
            url: route('add_quotation'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                customerIdFind: customerIdFind,
                name: customerName,
                establishmentName: establishmentName,
                cellphone: cellphone,
                cellphoneMain: cellphoneMain,
                plague: plague,
                establishment: establishment,
                email: email,
                address: colony,
                municipality: municipality,
                construction_measure: construction_measure,
                garden_measure: garden_measure,
                discount: discount,
                price: price,
                priceReinforcement: priceRefuerzo,
                id_quotation: id_quotation,
                contador: contador,
                id_plague_jer: id_plague_jer,
                total: total,
                sourceOrigin: sourceOrigin,
                pesticide: pesticide,
                messenger: messenger,
                extra: extra,
                description_whatsapp: speechComplete,
                id_price_list: id_price_list,
                jobCenter: jobCenter,
                isConfirmedCustomer: isConfirmedCustomer,
                inspection: inspection
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    showToast('error', 'Error al guardar', data.message);
                } else if (data.code == 304) {
                    Swal.close();
                    Swal.fire({
                        title: '<strong>¡Advertencia!</strong>',
                        type: 'error',
                        html: data.message +'<br><br><p style="color: #1E8CC7;">Recuerda que el teléfono es único por ciente y NO puedes usarlo para registrar a más de un cliente.</p>',
                        showCloseButton: false,
                        showCancelButton: true,
                        focusConfirm: false,
                        allowOutsideClick: false,
                        confirmButtonText:
                            '<i class="fa fa-pencil-square-o"></i> Regresar y Cambiar Teléfono',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-floppy-o"></i> Guardar y Remplazar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        if (result.value) {
                            Swal.close();
                        } else if (!result.value) {
                            Swal.close();
                            isConfirmedCustomer = true;
                            saveQuotationAjax();
                        }
                    })
                } else if(data.code == 201) {
                    idQuotationResponse = data.id;
                    idQuotationResponseEncrypt = data.idEncrypt;
                    Swal.close();
                    $("#saveQuotation").attr('disabled', true);
                    let emails = [];
                    if (email.length !== 0) {
                        emails.push(email);
                        Swal.fire({
                            title: '<strong>¡Deseas Envíar la Cotización (Correo)!</strong>',
                            html: '<br><p style="color: #1E8CC7;">Si ahorita no deseas enviar la cotización en cualquier momento lo puedes hacer desde el Menú.</p>',
                            showCloseButton: false,
                            showCancelButton: true,
                            focusConfirm: false,
                            allowOutsideClick: false,
                            confirmButtonText: 'Sí',
                            confirmButtonAriaLabel: 'Thumbs up, great!',
                            cancelButtonAriaLabel: 'Cerrar'
                        }).then((result) => {
                            if (result.value) {
                                mail_generate(emails, idQuotationResponse);
                            } else {
                                Swal.close();
                                showToast('success-redirect', data.message, 'Se envió la guardó la cotización.','index_register');
                            }
                        })

                    } else correct();
                }
            }
        });
    }

    //start get price quotation
    $('#priceQuotation').click(function() {
        getCalculatePrice(1);
    });
    //end get price quotation

    function generateSpeeechText() {
        let discount = $('#discount').val();
        let discountText = $('#discount option:selected').text();
        let establishment_id = $('#establishment').val();
        let discountSpeech = '';
        let urlWhatsapp = '';

        if (discountPercentage != 0) {
            discountSpeech = 'Durante este mes te otorgamos un ' + discountPercentage +
                '% de descuento, por lo que tu servicio quedaría en '+ symbolCountry + discountFinal + '.';
        }
        //speechComplete = discountPercentage;
        speechComplete = speech + ' ' + discountSpeech + ' ' + speechExtra;
    }

    function speechText(cellphone, cellphoneMain, isCellphone) {

        let discount = $('#discount').val();
        let discountText = $('#discount option:selected').text();
        let establishment_id = $('#establishment').val();
        let codeCountry = $('#codeCountry').val();
        let discountSpeech = '';
        let urlWhatsapp = '';
        let speechCompleteText = '';

        if (discountPercentage != 0) {
            discountSpeech = 'Durante este mes te otorgamos un ' + discountPercentage +
                '% de descuento, por lo que tu servicio quedaría en '+ symbolCountry + discountFinal + '.';
        }

        speechCompleteText = speech;

        let speechWhatsapp;
        if (isCellphone || cellphoneMain.length > 0) {
            if (discountSpeech === '') {
                speechWhatsapp = '<h4>' + speechCompleteText + speechExtra + '<br>' + 'El total de tu servicio es de '+ symbolCountry + total + '</h4>';
            } else {
                speechWhatsapp = '<h4>' + speechCompleteText + '<br>El costo de este servicio es de '+ symbolCountry + price + '<br>' +
                    discountSpeech + '<br>' + speechExtra + '<br>' +
                    'El total de tu servicio es de '+ symbolCountry + total + '</h4>';
            }
            Swal.fire({
                html: speechWhatsapp,
                width: 1200,
                height: 500,
                focusConfirm: false,
                showCancelButton: true,
                showCloseButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'Copiar',
                cancelButtonText: 'WhatsApp',
                confirmButtonColor: '#aaa',
                cancelButtonColor: 'green',
                onClose: () => {
                    window.location.href = route('index_register')
                }
            }).then((result) => {
                if (result.value) {
                    if (discountSpeech === '') {
                        speechCompleteText = speechCompleteText + discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de '+ symbolCountry + total;
                    }
                    else {
                        speechCompleteText = speechCompleteText + ' El costo de este servicio es de '+ symbolCountry + price +
                           ' ' + discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de '+ symbolCountry + total;
                    }
                    copySpeech(speechCompleteText);
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    //sendWhatsapp
                    if (discountSpeech === '') {
                        speechCompleteText = speechCompleteText + discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de '+ symbolCountry + total;
                    }
                    else {
                        speechCompleteText = speechCompleteText + ' El costo de este servicio es de '+ symbolCountry + price +
                            ' ' + discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de '+ symbolCountry + total;
                    }
                    if(isCellphone) {
                        window.location.href = route('index_register')
                        urlWhatsapp = "https://api.whatsapp.com/send?phone="+ codeCountry + cellphone + "&text=" + speechCompleteText +
                            "%0d%0dCotización: https://pestwareapp.com/quotation/" + idQuotationResponseEncrypt + "/PDF";
                        window.open(urlWhatsapp, '_blank');
                    }
                    if(cellphoneMainContact.length > 0){
                        window.location.href = route('index_register')
                        urlWhatsapp = "https://api.whatsapp.com/send?phone="+ codeCountry + cellphoneMainContact + "&text=" + speechCompleteText +
                            "%0d%0dCotización: https://pestwareapp.com/quotation/" + idQuotationResponseEncrypt + "/PDF";
                        window.open(urlWhatsapp, '_blank');
                    }
                }
            });
        } else {
                if (discountSpeech === '') {
                    speechWhatsapp = '<h4>' + speechCompleteText + speechExtra + '<br>' + 'El total de tu servicio es de '+ symbolCountry + total + '</h4>';
                } else {
                    speechWhatsapp = '<h4>' + speechCompleteText + '<br>El costo de este servicio es de '+ symbolCountry + price + '<br>' +
                        discountSpeech + '<br>' + speechExtra + '<br>' +
                        'El total de tu servicio es de '+ symbolCountry + total + '</h4>';
                }

            Swal.fire({
                html: speechWhatsapp,
                width: 1200,
                height: 500,
                focusConfirm: false,
                showCloseButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'Copiar',
                confirmButtonColor: '#aaa',
                onClose: () => {
                    window.location.href = route('index_register')
                }
            }).then((result) => {
                if (result.value) {
                    if (discountSpeech === '') {
                        speechCompleteText = speechCompleteText + discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de '+ symbolCountry + total;
                    }
                    else {
                        speechCompleteText = speechCompleteText + ' El costo de este servicio es de '+ symbolCountry + price +
                            discountSpeech + ' ' + speechExtra + ' El total de tu servicio es de '+ symbolCountry + total;
                    }
                    copySpeech(speechCompleteText);
                    correct();
                } else(
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                )
            });
        }


    }

    //send mail PDF
    function mail_generate(emails, idQuotation) {
        loaderPestWare('Enviando cotización...')
        $.ajax({
            type: 'POST',
            url: route('mail_generate'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                quotationId: idQuotation,
                emails: emails
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', data.message, 'Algo salio mal, intente de nuevo.');
                    correct();
                }
                else{
                    Swal.close();
                    showToast('success', data.message, 'Se envió la cotización por correo.');
                    correct();
                }
            }
        });
    }

    function copySpeech(textSpeech) {
        navigator.clipboard.writeText(textSpeech).then(function() {
            window.location.href = route('index_register')
        }, function(err) {
            window.location.href = route('index_register')
        });
    }

    //show modals converters and custom quotation
    $('#MC').click(function() {
        converter();
    });

    $('#CE').click(function() {
        converterSchool();
    });

    $('#CS').click(function() {
        converterSpaces();
    });

    $('#QP').click(function() {
        //get values input
        let customerName = $('#customerName').val();
        let plague = $('#plagues').val();
        let contador = plague.length;
        let sourceOrigin = $('#sourceOrigin option:selected').val();
        let establishment = $("#establishment option:selected").val();

        validateField(expressions.description, customerName, 'customerName');

        if(sourceOrigin == 0){
            document.getElementById('grupo__sourceOrigin').classList.add('formulario__grupo-incorrecto');
        }

        if(establishment == 0){
            document.getElementById('grupo__establishment').classList.add('formulario__grupo-incorrecto');
        }

        if (contador === 0) {
            showToast('error', 'Ingrese por lo menos una plaga', '');
        }

        if(getFields().customerName && sourceOrigin != 0 && contador != 0) {
            getCalculatePrice(3);
        } else showToast('error', 'Debes ingresar todos los datos con *', '');
    });

    //alerts messages
    function correct() {
        showToast('success', 'Cotización Guardada', 'Se guardo la cotización correctamente.', );
        $.ajax({
            type: 'GET',
            url: route('get_plan_user'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (data) {
                if (data.plan == 2 || data.plan == 3) {
                    speechText(cellphoneSpech, cellphoneMainContact, isPhone);
                } else {
                    window.location.href = route('index_register')
                }
            }
        });
    }

    async function converter() {
        const { value: formValues } = await Swal({
            title: 'Convertidor de Medidas',
            confirmButtonText: 'Calcular',
            html:
                '<input min="0" type="number" id="swal-input1" class="form-control" style="margin-bottom: 10px" placeholder="Frente (mts)">' +
                '<input min="0" type="number" id="swal-input2" class="form-control" style="margin-bottom: 10px" placeholder="Fondo (mts)">' +
                '<input min="0" type="number" id="swal-input3" class="form-control" placeholder="# pisos (mts)">',
            showCloseButton: true,
            //showCancelButton: true,
            focusConfirm: false,
            //confirmButtonText:
            // '<i class="fa fa-thumbs-up"></i> Great!',
            //confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
            cancelButtonAriaLabel: 'Thumbs down',
            preConfirm: () => {
                return [
                    document.getElementById('swal-input1').value,
                    document.getElementById('swal-input2').value,
                    document.getElementById('swal-input3').value,
                    document.getElementById('swal-input1').value,
                    document.getElementById('swal-input2').value,
                    document.getElementById('swal-input3').value
                ]
            }
        });

        if (formValues) {
            total = document.getElementById('swal-input1').value * document.getElementById('swal-input2').value * document.getElementById('swal-input3').value;
            document.getElementById("construction_measure").value = total;
        }
    }

    async function converterSchool() {
        const { value: formValues } = await Swal({
            title: 'Convertidor de Escuela',
            confirmButtonText: 'Calcular',
            width: '500px',
            html: '<div class="row">' +
                '<br>' +
                '<div class="col-md-4">' +
                '<h5>Salones</h5>' +
                '<input min="0" type="number" value="0" id="salones" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Bodegas</h5>' +
                '<input min="0" type="number" value="0" id="bodegas" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Laboratorios</h5>' +
                '<input min="0" type="number" value="0" id="laboratorios" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Sala de espera</h5>' +
                '<input min="0" type="number" value="0" id="salaEspera" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Juegos infantiles</h5>' +
                '<input min="0" type="number" value="0" id="juegosInfantiles" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Oficinas</h5>' +
                '<input min="0" type="number" value=0 id="oficinas" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Cooperativa</h5>' +
                '<input min="0" type="number" value=0 id="cooperativa" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Auditorio</h5>' +
                '<input min="0" type="number" value=0 id="auditorio" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Gimnasio</h5>' +
                '<input min="0" type="number" value=0 id="gimnasio" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Otros</h5>' +
                '<input min="0" type="number" value=0 id="otros" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Baños</h5>' +
                '<input min="0" type="number" value=0 id="baños" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Biblioteca</h5>' +
                '<input min="0" type="number" value=0 id="biblioteca" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Cafetería</h5>' +
                '<input min="0" type="number" value=0 id="cafeteria" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Cuarto de máquinas</h5>' +
                '<input min="0" type="number" value=0 id="cuartoMaquinas" class="form-control">' +
                '</div>' +
                '</div>',
            showCloseButton: true,
            //showCancelButton: true,
            focusConfirm: false,
            //confirmButtonText:
            //  '<i class="fa fa-thumbs-up"></i> Great!',
            //confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
            cancelButtonAriaLabel: 'Thumbs down',
            preConfirm: () => {
                return [
                    document.getElementById('salones').value,
                    document.getElementById('bodegas').value,
                    document.getElementById('laboratorios').value,
                    document.getElementById('salaEspera').value,
                    document.getElementById('juegosInfantiles').value,
                    document.getElementById('oficinas').value,
                    document.getElementById('cooperativa').value,
                    document.getElementById('auditorio').value,
                    document.getElementById('gimnasio').value,
                    document.getElementById('otros').value,
                    document.getElementById('baños').value,
                    document.getElementById('biblioteca').value,
                    document.getElementById('cafeteria').value,
                    document.getElementById('cuartoMaquinas').value
                ]
            }
        });

        if (formValues) {
            let sumaS = parseInt(document.getElementById('salones').value) +
                parseInt(document.getElementById('bodegas').value) +
                parseInt(document.getElementById('laboratorios').value) +
                parseInt(document.getElementById('salaEspera').value) +
                parseInt(document.getElementById('juegosInfantiles').value) +
                parseInt(document.getElementById('oficinas').value) +
                parseInt(document.getElementById('cooperativa').value) +
                parseInt(document.getElementById('auditorio').value) +
                parseInt(document.getElementById('gimnasio').value) +
                parseInt(document.getElementById('otros').value) +
                parseInt(document.getElementById('baños').value) +
                parseInt(document.getElementById('biblioteca').value) +
                parseInt(document.getElementById('cafeteria').value) +
                parseInt(document.getElementById('cuartoMaquinas').value);

            document.getElementById("construction_measure").value = sumaS * 50;
        }
    }

    async function converterSpaces() {
        const { value: formValues } = await Swal({
            title: 'Convertidor de Espacios',
            confirmButtonText: 'Calcular',
            width: '500px',
            html: '<div class="row">' +
                '<br>' +
                '<div class="col-md-4">' +
                '<h5>Pisos</h5>' +
                '<input value="1" min="0" type="number" id="pisos" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Cocina</h5>' +
                '<input min="0" type="number" value="1" id="cocina" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Sala</h5>' +
                '<input min="0" type="number" value="1" id="sala" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Comedor</h5>' +
                '<input min="0" type="number" value="1" id="comedor" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Recibidor</h5>' +
                '<input min="0" type="number" value=0 id="recibidor" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Baños</h5>' +
                '<input min="0" type="number" value="1" id="baños" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Recamaras</h5>' +
                '<input min="0" type="number" value="1" id="recamaras" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Salas de TV</h5>' +
                '<input min="0" type="number" value=0 id="salaTv" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Patio o Jardín</h5>' +
                '<input min="0" type="number" value=0 id="patio" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5># Autos cochera</h5>' +
                '<input min="0" type="number" value=0 id="numeroAutosCochera" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Cuarto de lavado</h5>' +
                '<input min="0" type="number" value=0 id="cuartoLavado" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Cuarto de servicio</h5>' +
                '<input min="0" type="number" value=0 id="cuartoServicio" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Estudio</h5>' +
                '<input min="0" type="number" value=0 id="estudio" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Terraza</h5>' +
                '<input min="0" type="number" value=0 id="terraza" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Roof Garden</h5>' +
                '<input min="0" type="number" value=0 id="roofGarden" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Gimnasio</h5>' +
                '<input min="0" type="number" value=0 id="gimnasio" class="form-control">' +
                '</div>' +
                '<div class="col-md-4">' +
                '<h5>Salon de juegos</h5>' +
                '<input min="0" type="number" value=0 id="salonJuegos" class="form-control">' +
                '</div>' +
                '</div>',
            showCloseButton: true,
            //showCancelButton: true,
            focusConfirm: false,
            //confirmButtonText:
            //  '<i class="fa fa-thumbs-up"></i> Great!',
            //confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
            cancelButtonAriaLabel: 'Thumbs down',
            preConfirm: () => {
                return [
                    document.getElementById('pisos').value,
                    document.getElementById('cocina').value,
                    document.getElementById('sala').value,
                    document.getElementById('comedor').value,
                    document.getElementById('recibidor').value,
                    document.getElementById('baños').value,
                    document.getElementById('recamaras').value,
                    document.getElementById('salaTv').value,
                    document.getElementById('patio').value,
                    document.getElementById('numeroAutosCochera').value,
                    document.getElementById('cuartoLavado').value,
                    document.getElementById('cuartoServicio').value,
                    document.getElementById('estudio').value,
                    document.getElementById('terraza').value,
                    document.getElementById('roofGarden').value,
                    document.getElementById('gimnasio').value,
                    document.getElementById('salonJuegos').value
                ]
            }
        });

        if (formValues) {
            let sumaSp = (parseInt(document.getElementById('pisos').value) * 5) +
                (parseInt(document.getElementById('baños').value) * 10) +
                (parseInt(document.getElementById('cocina').value) * 20) +
                (parseInt(document.getElementById('comedor').value) * 20) +
                (parseInt(document.getElementById('cuartoLavado').value) * 15) +
                (parseInt(document.getElementById('cuartoServicio').value) * 15) +
                (parseInt(document.getElementById('recamaras').value) * 15) +
                (parseInt(document.getElementById('sala').value) * 20) +
                (parseInt(document.getElementById('salaTv').value) * 15) +
                (parseInt(document.getElementById('recibidor').value) * 10) +
                (parseInt(document.getElementById('patio').value) * 40) +
                (parseInt(document.getElementById('numeroAutosCochera').value) * 15) +
                (parseInt(document.getElementById('gimnasio').value) * 25) +
                (parseInt(document.getElementById('estudio').value) * 20) +
                (parseInt(document.getElementById('roofGarden').value) * 80) +
                (parseInt(document.getElementById('salonJuegos').value) * 40) +
                (parseInt(document.getElementById('terraza').value) * 25);

            document.getElementById("construction_measure").value = sumaSp;
        }

    }
    //end converters

});