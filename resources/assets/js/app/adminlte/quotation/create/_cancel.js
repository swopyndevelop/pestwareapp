/**
 * @author Yomira Martínez
 * @version 05/03/2019
 * register/_cancel.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){
    let id_quotation;
    let reason;
    let comments;

    //START Cancel Quotation
    $('#SaveCancelC').click(function(){
        let mess = 'Cancelando Cotización...'

        id_quotation = $('#idQuotation').val();
        reason = $('#reason').val();
        comments = $('#commmentary').val();

        //validations
        if(reason === null) showToast('warning', 'Motivo', 'Seleccione un motivo de cancelación');
        else if(comments.length <= 0) showToast('warning', 'Comentarios al Cliente', 'Ingresa algún comentario');
        else{
            CancelSave();
        }

        function CancelSave(){
            loaderPestWare(mess);
            $.ajax({
                type: 'POST',
                url: route('cancel_quotation'),
                data:{
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id_quotation:id_quotation,
                    reason: reason,
                    comments: comments
                },
                success: function (data) {
                    if (data.errors){
                        Swal.close();
                        showToast('warning', 'Algo salio mal, intente de nuevo.', '');
                    }
                    else{
                        Swal.close();
                        showToast('success-redirect', 'Datos guardados correctamente.', '', 'index_register');
                    }
                }
            });
        }
    });

 });