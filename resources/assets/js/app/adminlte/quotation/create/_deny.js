/**
 * @author Yomira Martínez
 * @version 05/03/2019
 * register/_deny.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";
import {setFields, validateField} from "../../validations/regex";

$(document).ready(function(){

    $('.openModalCancelQuot').click(function () {
        let all = $(this);
        $('#idQuotation').val(all.attr('data-id'));
        $("#quotationKey").html(all.attr('data-quotation'));
    });

    $("#denyQ").on('hidden.bs.modal', function () {
        clearForm();
    });

    // validations section ->
    const comments = document.getElementById('comments');

    const fieldsDenyQuotationForm = {
        reason: false,
        comments: true
    }
    setFields(fieldsDenyQuotationForm);

    const validateForm = (e) => {
        switch (e.target.name) {
            case "comments":
                validateField(expressions.descriptionNotRequired, e.target.value, 'comments');
                break;
        }
    }

    comments.addEventListener('keyup', validateForm);
    comments.addEventListener('blur', validateForm);

    const reason = document.querySelector('#grupo__reason');
    reason.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('grupo__reason').classList.remove('formulario__grupo-incorrecto');
        }
    });

    //START Deny Quotation
    $('#saveQuotationDeny').click(function(){

        const reason = $("#reason option:selected").val();
        const id_quotation = $('#idQuotation').val();
        const comments = $('#comments').val();

        if(reason == 0) document.getElementById('grupo__reason').classList.add('formulario__grupo-incorrecto');

        if(reason != 0) DenySave(id_quotation, reason, comments);
        else {
            document.getElementById('formulario__mensaje_deny').classList.add('formulario__mensaje-activo');
            setTimeout(() => {
                document.getElementById('formulario__mensaje_deny').classList.remove('formulario__mensaje-activo');
            }, 5000);
        }

    });

    function DenySave(id_quotation, reason, comments){
        loaderPestWare('Rechanzando Cotización...')
        $.ajax({
            type: 'POST',
            url: route('deny_quotation'),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                id_quotation:id_quotation,
                reason: reason,
                comments: comments
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error al guardar', data.message);
                }
                else if (data.code == 201){
                    clearForm();
                    document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');

                    document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
                        icono.classList.remove('formulario__grupo-correcto');
                    });
                    Swal.close();
                    showToast('success-redirect', 'Cotización Rechazada', data.message, 'index_register');
                    $("#saveQuotationDeny").attr('disabled', true);
                }
            }
        });
    }

    function clearForm() {
        document.getElementById('comments').value = '';
        document.getElementById('grupo__reason').classList.remove('formulario__grupo-incorrecto');
        document.getElementById('grupo__comments').classList.remove('formulario__grupo-incorrecto');
        $('#reason option').prop('selected', function() {
            return this.defaultSelected;
        });
    }

 });
