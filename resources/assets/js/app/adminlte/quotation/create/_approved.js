/**
 * @author Manuel Mendoza
 * @version 2021
 * register/_deleQuotation.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idQuotation;
    $('#approveQuotation').on('show.bs.modal', function(e) {
        idQuotation = $(e.relatedTarget).data().id;

        $('#btnApprovedQuotation').click(function(){

            //loaderPestWare('Aprobando Cotización...')
            $.ajax({
                type: 'post',
                url: route('approved_quotation'),
                data:{
                    _token: $("meta[name=csrf-token]").attr("content"),
                    idQuotation:  idQuotation
                },
                success: function (data) {
                    if (data.code == 500){
                        Swal.close();
                        showToast('error', 'Error al guardar', data.message);
                    }
                    else if (data.code == 201){
                        Swal.close();
                        showToast('success-redirect', 'Cotización Aprobada', data.message, 'index_register');
                    }
                }
            });
        });
    });

 });
