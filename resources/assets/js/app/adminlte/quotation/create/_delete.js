/**
 * @author Manuel Mendoza
 * @version 2021
 * register/_deleQuotation.blade.php
 */

import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";
import {setFields, validateField} from "../../validations/regex";

$(document).ready(function(){

    let quotationKeyDelete, serviceOrders;
    $('.openModalDeleteQuotation').click(function () {
        let all = $(this);
        $('#idQuotation').val(all.attr('data-id'));
        quotationKeyDelete = all.attr('data-quotation');
        $('#quotationKeyDelete').html(quotationKeyDelete);
        serviceOrders = all.attr('data-service-orders');
       $('#orderServicesCount').html(serviceOrders);
    });

    //START Deny Quotation
    $('#deleteQuotationIndex').click(function(){

        const id_quotation = $('#idQuotation').val();
        deleteQuotationSave(id_quotation);

    });

    function deleteQuotationSave(id_quotation){
        loaderPestWare('Eliminando Cotización...')
        $.ajax({
            type: 'delete',
            url: route('delete_quotation',id_quotation),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error al guardar', data.message);
                }
                else if (data.code == 201){
                    Swal.close();
                    showToast('success-redirect', 'Cotización Eliminada', data.message, 'index_register');
                }
            }
        });
    }

 });
