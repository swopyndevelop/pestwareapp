import {expressions, getFields, setFields, validateField} from "../../validations/regex";
import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    $('.openModalSendMailQuot').click(function () {
        let all = $(this);
        $('#idQuotationMail').val(all.attr('data-id'));
        $('#emailCustomerQuot').val(all.attr('data-email'));
        $("#quotationKeyEmail").html(all.attr('data-quotation'));
    });

    $("#sendmailQuotation").on('hidden.bs.modal', function () {
        clearFormEmailQuotation();
    });

    // validations section ->
    const emailQuot = document.getElementById('emailQuot');
    const emailCustomerQuot = document.getElementById('emailCustomerQuot');

    const fieldsEmailQuotationForm = {
        emailCustomerQuot: true,
        emailQuot: true
    }
    setFields(fieldsEmailQuotationForm);

    const validateForm = (e) => {
        switch (e.target.name) {
            case "emailCustomerQuot":
                validateField(expressions.email, e.target.value, 'emailCustomerQuot');
                break;
            case "emailQuot":
                validateField(expressions.email, e.target.value, 'emailQuot');
                break;
        }
    }

    emailQuot.addEventListener('keyup', validateForm);
    emailQuot.addEventListener('blur', validateForm);

    emailCustomerQuot.addEventListener('keyup', validateForm);
    emailCustomerQuot.addEventListener('blur', validateForm);

    $('#sendEmailQuotation').click(function () {

        const emailQuot = document.getElementById('emailQuot').value;
        const emailCustomerQuot = document.getElementById('emailCustomerQuot').value;
        const quotationId = $('#idQuotationMail').val();
        let emails = [];

        if (emailQuot.length !== 0) validateField(expressions.email, emailQuot, 'emailQuot');
        if (emailCustomerQuot !== 0) validateField(expressions.email, emailCustomerQuot, 'emailCustomerQuot');

        if (getFields().emailCustomerQuot && getFields().emailQuot) {
            if (emailQuot.length != 0) emails.push(emailQuot);
            if (emailCustomerQuot.length != 0) emails.push(emailCustomerQuot);
            sendEmailQuotation(quotationId, emails);
        }

    });

    function sendEmailQuotation(idQuotation, emails){
        loaderPestWare('Enviando cotización...')
        $.ajax({
            type: 'POST',
            url: route('mail_generate'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                quotationId: idQuotation,
                emails: emails
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Algo salió mal', data.message);
                }
                else{
                    Swal.close();
                    showToast('success', data.message, 'Se envió la cotización por correo.');
                    $("#sendmailQuotation").click();
                    location.reload()
                }
            }
        });
    }

    function clearFormEmailQuotation() {
        document.getElementById('emailQuot').value = '';
        document.getElementById('emailCustomerQuot').value = '';
        document.getElementById('grupo__emailQuot').classList.remove('formulario__grupo-incorrecto');
        document.getElementById('grupo__emailCustomerQuot').classList.remove('formulario__grupo-incorrecto');
    }

});
