/**
 * @author Alberto Martínez
 * @version 16/01/2019
 * register/_quotationCustom.blade.php
 */

$(document).ready(function() {

    let sumaSubtotal = 0;
    let total = 0;
    //let priceRefuerzo;
    let arrayConcepts = [];
    let key = 0;

    function addConcept() {
        //get values
        let concept = $('#concept').val();
        let quantity = $('#quantity').val();
        let frequency = $('#frequency').val();
        let term = $('#term').val();
        let priceUnit = $('#priceUnit').val();
        let subtotal = priceUnit * term * quantity;
        sumaSubtotal = sumaSubtotal + subtotal;

        $("#dataTh").append('<tr><td scope="row">' + concept + '</td><td scope="row">' + quantity + '</td><td scope="row">' + frequency + '</td><td scope="row">' + term + '</td><td scope="row">' + priceUnit + '</td><td scope="row">' + subtotal + '</td></tr>');
        $('#sumaSubtotal').val(sumaSubtotal);
        calcular();

        // object json
        arrayConcepts.push({
            'key': key,
            'concept': concept,
            'quantity': quantity,
            'frequency': frequency,
            'term': term,
            'priceUnit': priceUnit,
            'subtotal': subtotal
        });
        key++;
    }

    function resetInput() {
        $('#concept').val("");
        $('#quantity').val("");
        $('#frequency').val("");
        $('#term').val("");
        $('#priceUnit').val("");
        $('#subtotal').val("");
    }

    $('#addConcept').click(function() {
        addConcept();
        resetInput();
    });

    function calcular() {
        let discount = $('#discountC').val();
        let percentageDiscount = discount / 100;
        let totalDiscount = percentageDiscount * sumaSubtotal;
        total = sumaSubtotal - totalDiscount;
        $('#total').val(total);
    }

    $('#calcular').click(function() {
        calcular();
    });

    //save quotation custom
    $('#saveQuotationCustom').click(function() {
        saveQuotationCustom();
    });

    function saveQuotationCustom() {
        loaderC('');

        //get values input
        let description = $('#description').val();
        let customerName = $('#customerName').val();
        let establishmentName = $('#establishmentName').val();
        let cellphone = $('#cellphone').val();
        let establishment = $('#establishment').val();
        let email = $('#emailCustomer').val();
        let colony = $('#colony').val();
        let municipality = $('#municipality').val();
        let sourceOrigin = $('#sourceOrigin').val();
        let messenger = $('#messenger').val();
        let discount = $('#discountC').val();
        let plague = $('#plagues').val();
        let id_quotation = $('#id_quotation').text().trim();
        let contador = plague.length;

        $.ajax({
            type: 'POST',
            url: route('add_quotation_custom'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                description: description,
                arrayConcepts: JSON.stringify(arrayConcepts),
                name: customerName,
                establishmentName: establishmentName,
                cellphone: cellphone,
                establishment: establishment,
                email: email,
                address: colony,
                municipality: municipality,
                discount: discount,
                price: sumaSubtotal,
                total: total,
                sourceOrigin: sourceOrigin,
                messenger: messenger,
                contador: contador,
                plague: plague,
                id_quotation: id_quotation
            },
            success: function(data) {
                if (data.code == 500) {
                    Swal.close();
                    missingTextC('Algo salio mal, intenta de nuevo.');
                } else if (data.code == 201) {
                    Swal.close();
                    correctC();
                    LastQuotationC();
                    generateC(); // Send email quotation
                }
            }
        });
    }

    function generateC() {
        //get values input
        let description = $('#description').val();
        let customerName = $('#customerName').val();
        let establishmentName = $('#establishmentName').val();
        let cellphone = $('#cellphone').val();
        let establishment = $('#establishment').val();
        let email = $('#emailCustomer').val();
        let colony = $('#colony').val();
        let municipality = $('#municipality').val();
        let sourceOrigin = $('#sourceOrigin').val();
        let messenger = $('#messenger').val();
        let discount = $('#discountC').val();
        let plague = $('#plagues').val();
        let id_quotation = $('#id_quotation').text().trim();
        let contador = plague.length;

        $.ajax({
            type: 'GET',
            url: route('generate_custom'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                description: description,
                arrayConcepts: JSON.stringify(arrayConcepts),
                name: customerName,
                establishmentName: establishmentName,
                cellphone: cellphone,
                establishment: establishment,
                email: email,
                address: colony,
                municipality: municipality,
                discount: discount,
                price: sumaSubtotal,
                total: total,
                sourceOrigin: sourceOrigin,
                messenger: messenger,
                contador: contador,
                plague: plague,
                id_quotation: id_quotation
            }
        });
    }

    //last_id
    function LastQuotationC() {
        $.ajax({
            url: route('last_quotation'),
            method: 'GET',
            dataType: 'json',
            success: function(last_id) {
                last_id = last_id;
                let mens = 'https://pestwareapp.com/quotation/';
                let cont = '/custom/PDF';
                complete = mens.concat(last_id, cont);
                smsC();
            },
            error: function() {
                console.log('error');
            }
        });
    }
    //send SMS
    function smsC() {
        let cellphone = $('#cellphone').val();
        var msg = {
            "message": complete,
            "cellphones": [cellphone],
            "country": 1,
            "encode": true,
            "allow_responses": true
        };
        var settings = {
            url: "https://smsclouds.cloud/api/v1/message",
            method: "POST",
            data: JSON.stringify(msg),
            contentType: 'application/json',
            dataType: 'json',
            headers: {
                "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyNjgzZmIzZTRkOTcxMTA2YzIyOThiNmZkNmJhZWE4ODMzNmFiMDk3Y2FiZmI3MjZiMzRlODEzN2FjYmQ5MWVlZWQ1ZmNlNDYzOTM5NmVjIn0.eyJhdWQiOiI3IiwianRpIjoiYTI2ODNmYjNlNGQ5NzExMDZjMjI5OGI2ZmQ2YmFlYTg4MzM2YWIwOTdjYWJmYjcyNmIzNGU4MTM3YWNiZDkxZWVlZDVmY2U0NjM5Mzk2ZWMiLCJpYXQiOjE1NTE3NDYyOTQsIm5iZiI6MTU1MTc0NjI5NCwiZXhwIjoxNTgzMzY4Njk0LCJzdWIiOiI4NDMiLCJzY29wZXMiOltdfQ.ggRv72eZYb3kwluxZbLe741MhRXijsLV5bk5mj3PL2w-R9sNtpPqUjq2dm5k_Yp8KWA83Y0uwK_1X5AfLvLsteYWS6GWRNkr74DffH5RvzHj3DmZ0Qh8UNZuZM_DKKu_5oPjle00Ec6pGxuUHBIur1XO4lDa2-vcfns0lQUJEIpP3wdZKUm1HzZfhd-lOJi8jg30f_FaVh6bBQ7Q12Owm9NLAJl8arCi-hBRLmVLelTwjRfr5DaN3m4KVwtnF52puxs4RNiTBA3v--Vs63ejLPDzDfvh3-IW9C5RUHv3_DukXhmjoWG5LZ3nlr1KLX0JYtyVAHSl-8Oy71jcnYJ1kTEz24fBp89wetGinEDcdauVzlrIytKuVxGCzlM9yIYpKXM__x9anVSk-4FIxcEVHEEAEFmJemMRx-Kf-_Fq7JXCic4TdPBY2IzB9PIvHmYJCbfGYBQkjIUagLwt5ehY88yccB3dEwiw9gtndenAR0tYrFyUXAo28mtFU6E_6vx6PO2LZXYReLOfz_iYKUQvmFAZsuLHK1d-WXZy1nP6H2SR5JG7ZuI8GDffDXLvMGIFZzeOIOIW1tsoohEjBj3jsz3ovnN_ns0p7xPsaBITx7TfBtSqsHFgBc7STmZVwWPhT6qffTWZfMQMMOJrCle_tqv-irY0bmsdYgy7hVHIjks",
                "Accept": "application/json"
            }
        };
        $.ajax(settings).done(function(response) {
            console.log(response);
        });
    }

    //alerts messages
    function correctC() {
        swal({
            title: "Datos guardados correctamente",
            type: "success",
            timer: 2000,
            showCancelButton: false,
            showConfirmButton: false
        }).then((result) => {
            window.location.href = route('index_register');
        })
    }

    function missingTextC(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
        });
    }

    function loaderC(message) {
        Swal.fire({
            title: message,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
        }).then((result) => {

        })
    }
});