import {loaderPestWare} from "../../pestware/loader";
import {showToast} from "../../pestware/alerts";

$(document).ready(function(){

    let idOrder;
    let url;
    let cellphoneCustomer;
    let cellphoneMain;

    $('.openModalWhatsappQuotation').click(function () {
        let all = $(this);
        idOrder = $(this).data("id");
        url = $(this).data("url");
        cellphoneCustomer = all.attr('data-cellphone-customer');
        $('#cellphoneClientQuotation').val(cellphoneCustomer);
        cellphoneMain = all.attr('data-cellphone-main');
        $('#cellphoneMainQuotation').val(cellphoneMain);

        $("#sendWhatsappQuotationSave").click(function() {
            const cellphoneOtherQuotation = document.getElementById('cellphoneOtherQuotation').value;
            cellphoneCustomer = document.getElementById('cellphoneClientQuotation').value;
            cellphoneMain = document.getElementById('cellphoneMainQuotation').value;
            let urlSeparate = url.split('{');
            let urlCompleteCustomer = `${urlSeparate[0]}${cellphoneCustomer}${urlSeparate[1]}`;
            let urlCompleteMain = `${urlSeparate[0]}${cellphoneMain}${urlSeparate[1]}`;
            let urlCompleteOther = `${urlSeparate[0]}${cellphoneOtherQuotation}${urlSeparate[1]}`;
            if (cellphoneCustomer !== '') window.open(urlCompleteCustomer, '_blank');
            if (cellphoneMain !== '') window.open(urlCompleteMain, '_blank');
            if (cellphoneOtherQuotation !== '') window.open(urlCompleteOther, '_blank');

            sendWhatsappOrder();
        });
    });

    function sendWhatsappOrder(){
        loaderPestWare('');
            $.ajax({
                type: 'PUT',
                url: route('whatsapp_quotation'),
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: idOrder
                },
                success: function (data) {
                    if (data.errors){
                        Swal.close();
                        showToast('error', 'Error', 'Algo salio mal, intente de nuevo.');
                    }
                    else{
                        Swal.close();
                        location.reload();
                    }
                }
        });
    }

});