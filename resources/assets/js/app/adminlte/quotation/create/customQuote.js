/**
 * @author Alberto Martínez
 * @version 10/04/2021
 * register/_quotationCustomTest.blade.php
 */

import {showToast} from "../../pestware/alerts";
import {loaderPestWare} from "../../pestware/loader";

$(document).ready(function(){

    // Constants
    const monthly = 1;
    const bimonthly = 2;
    const quarterly = 3;
    const quarter = 4;
    const biannual = 5;
    const allDays = 6;

    let db = [];
    let totalGlobal = 0;
    let subtotalGlobal = 0;
    let isConfirmedCustomer = false;
    let btnAdd = document.getElementById('addTest');
    let btnSave = document.getElementById('saveQuotationCustomNewTest');
    let btnAddDescription = document.getElementById('addDescription');
    let btnSaveDescription = document.getElementById('saveNewDescriptionCustom');
    let type = document.getElementById('type');
    let typeService = document.getElementById('typeService');
    let descriptions = document.getElementById('descriptions');

    let inputCustomerName = document.getElementById('customerNameCustom');
    let inputCellphone = document.getElementById('cellphoneCustom');
    let jobCenterSelect = $('#selectJobCenter option:selected').val();

    let token = $("meta[name=csrf-token]").attr("content");
    $.ajax({
        type: 'GET',
        url: route('convert_to_money', 0),
        data: {
            _token: token
        },
        success: function(response) {
            document.getElementById('subtotalTest').innerText = `${response.symbol}${response.quantity}`;
            document.getElementById('totalTest').innerText = `${response.symbol}${response.quantity}`;
        }
    });

    //autocomplete cellphone
    let customerIdFind = 0;
    function findCustomers(query) {
        if (query !== '') {
            $.ajax({
                type: 'GET',
                url: route('autocomplete_cellphone'),
                data: {
                    _token: token,
                    query: query,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    let numberListCustom = $('#numberListCustom');
                    numberListCustom.fadeIn();
                    numberListCustom.html(data);
                }
            });
        }
    }

    //autocomplete customerName
    function findCustomersName(query) {
        if (query !== '') {
            $.ajax({
                type: 'GET',
                url: route('autocomplete_customer_name'),
                data: {
                    _token: token,
                    query: query,
                    jobCenterSelect: jobCenterSelect
                },
                success: function(data) {
                    let customerNameListCustom = $('#customerNameListCustom');
                    customerNameListCustom.fadeIn();
                    customerNameListCustom.html(data);
                }
            });
        }
    }

    $(document).on('click', 'li#getdataClient', function() {
        let cellphoneCustom = $('#cellphoneCustom');
        let numberListCustom =  $('#numberListCustom');
        cellphoneCustom.val($(this).text());
        numberListCustom.fadeOut();
        //cargar datos de cliente
        let cellphoneLoad = cellphoneCustom.val();
        $.ajax({
            type: 'GET',
            url: route('load_client'),
            data: {
                _token: token,
                cellphone: cellphoneLoad,
                jobCenterSelect: jobCenterSelect
            },
            success: function(data) {
                $('#customerNameCustom').val(data.name).text();
                $('#establishmentNameCustom').val(data.establishment_name).text();
                $('#cellphoneMainCustom').val(data.cellphone_main).text();
                $('#colonyCustom').val(data.colony).text();
                $('#municipalityCustom').val(data.municipality).text();
                $('#emailCustomerCustom').val(data.email).text();
                $('#sourceOriginCustom').val(data.id_source_origin);
            }
        });
        numberListCustom.fadeOut();
    });

    $(document).on('click', 'li#getdataClientName', function() {
        let customerNameCustom = $('#customerNameCustom');
        let customerNameList = $('#customerNameListCustom');
        customerNameCustom.val($(this).text());
        customerNameList.fadeOut();
        //cargar datos de cliente
        let customerName = customerNameCustom.val();
        $.ajax({
            type: 'GET',
            url: route('load_client_name'),
            data: {
                _token: token,
                customerName: customerName,
                jobCenterSelect: jobCenterSelect
            },
            success: function(data) {
                customerIdFind = data.id;
                $('#customerNameCustom').val(data.name).text();
                $('#establishmentNameCustom').val(data.establishment_name).text();
                $('#cellphoneCustom').val(data.cellphone).text();
                $('#cellphoneMainCustom').val(data.cellphone_main).text();
                $('#colonyCustom').val(data.colony).text();
                $('#municipalityCustom').val(data.municipality).text();
                $('#emailCustomerCustom').val(data.email).text();
                $('#sourceOriginCustom').val(data.id_source_origin);
            }
        });
        customerNameList.fadeOut();
    });

    //select inspection service
    $('#inspectionSelect').on('change', function() {
        let idQuotation = $(this).val();
        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('load_client_by_quotation'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idQuotation: idQuotation
            },
            success: function(data) {
                $('#customerNameCustom').val(data.name).text();
                $('#establishmentNameCustom').val(data.establishment_name).text();
                $('#cellphoneCustom').val(data.cellphone).text();
                $('#cellphoneMainCustom').val(data.cellphone_main).text();
                $('#colonyCustom').val(data.colony).text();
                $('#municipalityCustom').val(data.municipality).text();
                $('#emailCustomerCustom').val(data.email).text();
                $('#sourceOriginCustom').val(data.id_source_origin);
                Swal.close();
            }
        });
    });

    inputCustomerName.onkeyup = function (ev) {
        let customerNameCustom = document.getElementById('customerNameCustom').value;
        findCustomersName(customerNameCustom);
    }

    inputCellphone.onkeyup = function (ev) {
        let cellphoneCustom = document.getElementById('cellphoneCustom').value;
        findCustomers(cellphoneCustom);
    }

    btnAdd.onclick = function () {
        let data = getInputs()
        const regexDecimal = /^[0-9]+([.][0-9]+)?$/;
        const regexInteger = /^[0-9]?\d{1,6}$/;
        if (data.concept.length == 0) showToast('warning', 'Datos incompletos', 'El nombre del concepto es obligatorio');
        else if (!regexInteger.test(data.quantity)) showToast('warning', 'Datos incompletos', 'La cantidad debe ser númerica.');
        else if (!regexInteger.test(data.frequency) && data.typeId != 2 && data.typeServiceId != 6 && data.typeId != 8) showToast('warning', 'Datos incompletos', 'La frecuencia debe ser númerica.');
        else if (!regexInteger.test(data.contract) && data.typeId != 2 && data.typeId != 8) showToast('warning', 'Datos incompletos', 'El plazo debe ser númerico.');
        else if (data.price.length < 1) showToast('warning', 'Datos incompletos', 'El precio debe ser númerico.');
        else if (data.frequency > 4 && data.typeId != 8) showToast('warning', 'Datos incompletos', 'La frecuencia no debe ser mayor a 4');
        else if (data.contract > 36) showToast('warning', 'Requiere autorización', 'Contacte a soporte@pestwareapp.com');
        if (data.quantity > 800) showToast('warning', 'Requiere autorización', 'Contacte a soporte@pestwareapp.com');
        else addRow(data)
    }

    btnSave.onclick = function () {
        let customerName = document.getElementById('customerNameCustom').value;
        let cellphoneCustom = document.getElementById('cellphoneCustom').value;
        if (customerName.length < 1) showToast('warning', 'Datos incompletos', 'Debes ingresar el nombre del cliente.');
        else saveQuotationCustom();
    }

    btnAddDescription.onclick = function () {
        let description = document.getElementById('descriptionTest').value;
        if (description.length < 1) showToast('warning', 'Datos incompletos', 'Debes ingresar la descripción a guardar en el historial.');
        else $('#newDescriptionCustomQuote').modal("show");
    }

    btnSaveDescription.onclick = function () {
        let name = document.getElementById('nameDescription').value;
        let description = document.getElementById('descriptionTest').value;
        if (name.length < 1) showToast('warning', 'Datos incompletos', 'Debes ingresar un nombre para identificar la nueva descripción.');
        else saveDescription(description, name)
    }

    type.addEventListener('change', (event) => {
        const option = event.target.value;
        if (option == 2) {
            document.getElementById('monthTest').disabled = true;
        } else {
            document.getElementById('monthTest').disabled = false;
        }
    });

    typeService.addEventListener('change', (event) => {
        const option = event.target.value;
        if (option == 7) {
            document.getElementById('monthTest').value = 1;
            document.getElementById('contractTest').value = 1;
            document.getElementById('quantityTest').value = 1;
            document.getElementById('monthTest').disabled = true;
            document.getElementById('contractTest').disabled = true;
        }
        else {
            document.getElementById('monthTest').disabled = false;
            document.getElementById('contractTest').disabled = false;
        }
        if(option == 6){
            document.getElementById('monthTest').disabled = true;
        }
        if (option == 8) {
            document.getElementById('monthTest').value = 1;
            document.getElementById('quantityTest').value = 1;
            document.getElementById('monthTest').disabled = true;
        }
    });

    descriptions.addEventListener('change', (event) => {
        const option = event.target.value;
        if (option == 0) document.getElementById('descriptionTest').value = '';
        else getDescriptionById(option);
    });

    function getDescriptionById(id) {
        loaderPestWare('');
        $.ajax({
            type: 'GET',
            url: route('descriptions_custom_quote', id),
            data: {
                _token: token
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) {
                    showToast('error', 'Error al cargar', response.message);
                } else {
                    document.getElementById('descriptionTest').value = response.description;
                }
            }
        });
    }

    function saveDescription(description, name) {
        loaderPestWare('');
        $.ajax({
            type: 'POST',
            url: route('descriptions_custom_quote_save'),
            data: {
                _token: token,
                name: name,
                description: description,
                jobCenterSelect: jobCenterSelect
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) {
                    showToast('error', 'Error al guardar', response.message);
                } else {
                    let selectDescriptions = $('#descriptions');
                    selectDescriptions.empty();
                    selectDescriptions.append(`<option value="0" selected>Personalizada</option>`);
                    response.descriptions.forEach((element) => {
                        selectDescriptions.append(`<option value="${element.id}" ${response.id == element.id ? 'selected' : ''}>${element.name}</option>`);
                    });
                    $('#newDescriptionCustomQuote').click();
                    showToast('success', 'Descripción Guardada', response.message);
                }
            }
        });
    }

    function saveQuotationCustom() {

        //get values input
        let description = $('#descriptionTest').val();
        let customerName = $('#customerNameCustom').val();
        let establishmentName = $('#establishmentNameCustom').val();
        let cellphone = $('#cellphoneCustom').val();
        let establishment = $('#establishmentCustom option:selected').val();
        let cellphoneMain = $('#cellphoneMainCustom').val();
        let email = $('#emailCustomerCustom').val();
        let colony = $('#colonyCustom').val();
        let municipality = $('#municipalityCustom').val();
        let sourceOrigin = $('#sourceOriginCustom option:selected').val();
        let plague = $('#plaguesCustom').val();
        let pesticide = $('#pesticideCustom').val();
        let id_quotation = $('#id_quotation').text().trim();
        let contador = plague.length;
        let messengerCustom = $('#messengerCustom').val();
        let jobCenter = $("#selectJobCenter option:selected").val();
        let inspection = $("#inspectionSelect option:selected").val();

        if (establishment == 0)  showToast('warning', 'Tipo de Servicio', 'Seleccionar una concepto');
        else if (contador == 0)  showToast('warning', 'Plagas', 'Seleccionar alguna plaga por lo menos');
        else if (sourceOrigin == 0)  showToast('warning', 'Fuente de Origen', 'Seleccionar una concepto');
        else if (description.length <= 0)  showToast('warning', 'Descripción', 'Faltan agregar una descripción');
        else if (db.length < 1) showToast('warning', 'Datos incompletos', 'Debes ingresar por lo menos un concepto.');
        else {
            loaderPestWare('Guardando...');
            $.ajax({
                type: 'POST',
                url: route('add_quotation_custom'),
                data: {
                    _token: token,
                    description: description,
                    arrayConcepts: JSON.stringify(db),
                    name: customerName,
                    establishmentName: establishmentName,
                    cellphone: cellphone,
                    establishment: establishment,
                    cellphoneMain: cellphoneMain,
                    email: email,
                    colony: colony,
                    municipality: municipality,
                    price: subtotalGlobal,
                    total: totalGlobal,
                    sourceOrigin: sourceOrigin,
                    pesticide: pesticide,
                    contador: contador,
                    plague: plague,
                    id_quotation: id_quotation,
                    customerIdFind: customerIdFind,
                    messengerCustom: messengerCustom,
                    jobCenter: jobCenter,
                    isConfirmedCustomer: isConfirmedCustomer,
                    inspection: inspection
                },
                success: function (data) {

                    if (data.code == 500){
                        Swal.close();
                        showToast('error', 'Error', data.message)
                    }
                    else if (data.code == 304) {
                        Swal.fire({
                            title: '<strong>¡Advertencia!</strong>',
                            type: 'error',
                            html: data.message +'<br><br><p style="color: #1E8CC7;">Recuerda que el teléfono es único por ciente y NO puedes usarlo para registrar a más de un cliente.</p>',
                            showCloseButton: false,
                            showCancelButton: true,
                            focusConfirm: false,
                            allowOutsideClick: false,
                            confirmButtonText:
                                '<i class="fa fa-pencil-square-o"></i> Regresar y Cambiar Teléfono',
                            confirmButtonAriaLabel: 'Thumbs up, great!',
                            cancelButtonText:
                                '<i class="fa fa-floppy-o"></i> Guardar y Remplazar',
                            cancelButtonAriaLabel: 'Cerrar'
                        }).then((result) => {
                            if (result.value) {
                                Swal.close();
                            } else if (!result.value) {
                                Swal.close();
                                isConfirmedCustomer = true;
                                saveQuotationCustom();
                            }
                        })
                    }
                    else if(data.code == 201) {
                        Swal.close();
                        showToast('success-redirect', 'Cotización Guardada', data.message, 'index_register');
                        $("#saveQuotationCustomNewTest").prop("disabled", true);
                    }
                }
            });
        }

    }

    function getInputs() {
        let type = document.getElementById('type');
        let concept = document.getElementById('conceptTest').value;
        let quantity = document.getElementById('quantityTest').value;
        let typeService = document.getElementById('typeService');
        let month = document.getElementById('monthTest').value;
        let contract = document.getElementById('contractTest').value;
        let price = document.getElementById('priceTest').value;
        const index = type.selectedIndex;
        const indexType = typeService.selectedIndex;
        const optionSelect = type.options[index];
        const optionSelectType = typeService.options[indexType];
        return {
            id: createId(),
            typeId: optionSelect.value,
            typeText: optionSelect.text,
            concept: concept,
            quantity: quantity,
            typeServiceId: optionSelectType.value,
            typeServiceText: optionSelectType.text,
            frequency: parseInt(optionSelectType.value) === 6 ? '--': month,
            contract: contract,
            price: price
        }
    }

    function setInputs(data) {
        setOptionSelectedTypeConcept(data.typeId);
        document.getElementById('conceptTest').value = data.concept;
        document.getElementById('quantityTest').value = data.quantity;
        setOptionSelectedTypeService(data.typeServiceId);
        if (data.typeId == 2) {
            document.getElementById('monthTest').disabled = true;
        } else {
            if (data.typeServiceId == 6) document.getElementById('monthTest').disabled = true;
            else document.getElementById('monthTest').value = data.frequency;
            document.getElementById('contractTest').value = data.contract;
        }
        if (data.typeServiceId == 7) {
            document.getElementById('monthTest').disabled = true;
            document.getElementById('contractTest').disabled = true;
            document.getElementById('monthTest').value = 1;
            document.getElementById('contractTest').value = 1;
            document.getElementById('quantityTest').value = 1;
        }
        document.getElementById('priceTest').value = data.price;
    }

    function clearInputs() {
        document.getElementById('conceptTest').value = '';
        document.getElementById('quantityTest').value = '';
        document.getElementById('monthTest').value = '';
        document.getElementById('contractTest').value = '';
        document.getElementById('priceTest').value = '';
        document.getElementById('addTest').textContent = 'Agregar';
        document.getElementById('monthTest').disabled = false;
        document.getElementById('contractTest').disabled = false;
    }

    function addRow(data) {
        db.push(data);
        listItems(db);
        clearInputs();
    }

    // Models =>
    function getItemById(id) {
        return db.find(element => element.id === id)
    }

    function deleteItemById(id) {
        let dbTmp = db;
        db = [];
        dbTmp.forEach((element) => {
            if (element.id !== id) db.push(element);
        })
    }

    function calculatePrice(data) {
        let total = 0;
        let subtotalG = 0;
        data.forEach((element) => {
            const quantity = parseFloat(element.quantity)
            const frequency = element.typeId == 2 ? 1 : parseFloat(element.frequency)
            const contract = element.contract
            const price = parseFloat(element.price)
            let subtotal = quantity * frequency * contract * price;
            if (parseInt(element.typeServiceId) === bimonthly) subtotal = subtotal / 2;
            if (parseInt(element.typeServiceId) === quarterly) subtotal = subtotal / 3;
            if (parseInt(element.typeServiceId) === quarter) subtotal = subtotal / 4;
            if (parseInt(element.typeServiceId) === biannual) subtotal = subtotal / 6;
            if (parseInt(element.typeServiceId) === allDays) subtotal = quantity * contract * price;
            if (parseInt(element.typeServiceId) === allDays) {
                total += subtotal;
                subtotalG += quantity * price;
            } else {
                total += subtotal;
                subtotalG += quantity * frequency * price;
            }
        });
        totalGlobal = total;
        subtotalGlobal = subtotalG;
        $.ajax({
            type: 'GET',
            url: route('convert_to_money', subtotalG),
            data: {
                _token: token
            },
            success: function(response) {
                document.getElementById('subtotalTest').innerText = `${response.symbol}${response.quantity}`;
            }
        });
        $.ajax({
            type: 'GET',
            url: route('convert_to_money', total),
            data: {
                _token: token
            },
            success: function(response) {
                document.getElementById('totalTest').innerText = `${response.symbol}${response.quantity}`;
            }
        });
    }

    function listItems(data) {
        let tableRef = document.getElementById('customTable');
        for(let i = tableRef.rows.length - 1; i > 0; i--)
        {
            tableRef.deleteRow(i);
        }
        data.forEach((element) => {
            const actions = `<a id="updateItemRow-${element.id}" data-id="${element.id}" style="cursor: pointer;"><i class="fa fa-pencil fa-lg text-primary" aria-hidden="true"></a></i> 
            <a id="deleteItemRow-${element.id}" data-id="${element.id}" style="cursor: pointer;"><i class="fa fa-trash fa-lg text-danger" aria-hidden="true" style="margin-left: 5px;"></i></a>`;

            let newRow   = tableRef.insertRow();
            const quantity = parseFloat(element.quantity)
            const frequency = element.typeId == 2 ? 1 : parseFloat(element.frequency)
            const contract = element.contract
            const price = parseFloat(element.price)
            let subtotal = quantity * frequency * price;
            let total = quantity * frequency * contract * price;
            if (parseInt(element.typeServiceId) === bimonthly) total = total / 2;
            if (parseInt(element.typeServiceId) === quarterly) total = total / 3;
            if (parseInt(element.typeServiceId) === quarter) total = total / 4;
            if (parseInt(element.typeServiceId) === biannual) total = total / 6;
            if (parseInt(element.typeServiceId) === allDays) {
                total = quantity * contract * price;
                subtotal = quantity * price;
            }

            let newCellType  = newRow.insertCell(0);
            let newCellConcept  = newRow.insertCell(1);
            let newCellQuantity  = newRow.insertCell(2);
            let newCellTypeService  = newRow.insertCell(3);
            let newCellMonth  = newRow.insertCell(4);
            let newCellContract  = newRow.insertCell(5);
            let newCellPrice  = newRow.insertCell(6);
            let newCellSubtotal = newRow.insertCell(7);
            let newCellTotal = newRow.insertCell(8);
            let newCellActions  = newRow.insertCell(9);

            $.ajax({
                type: 'GET',
                url: route('convert_to_money', element.price),
                data: {
                    _token: token
                },
                success: function(response) {
                    newCellPrice.appendChild(document.createTextNode(response.quantity));
                }
            });
            $.ajax({
                type: 'GET',
                url: route('convert_to_money', subtotal.toString()),
                data: {
                    _token: token
                },
                success: function(response) {
                    newCellSubtotal.appendChild(document.createTextNode(response.quantity));
                }
            });
            $.ajax({
                type: 'GET',
                url: route('convert_to_money', total.toString()),
                data: {
                    _token: token
                },
                success: function(response) {
                    newCellTotal.appendChild(document.createTextNode(response.quantity));
                }
            });

            newCellType.appendChild(document.createTextNode(element.typeText));
            newCellConcept.appendChild(document.createTextNode(element.concept));
            newCellQuantity.appendChild(document.createTextNode(element.quantity));
            newCellTypeService.appendChild(document.createTextNode(element.typeServiceText));
            newCellMonth.appendChild(document.createTextNode(element.frequency));
            newCellContract.appendChild(document.createTextNode(element.contract));
            newCellActions.innerHTML = actions;

            // Events =>
            $(`#updateItemRow-${element.id}`).click(function () {
                let all = $(this);
                const id = all.attr('data-id');
                let item = getItemById(id);
                setInputs(item);
                let btn = document.getElementById('addTest');
                btn.textContent = 'Editar';
                deleteItemById(id);
            });
            $(`#deleteItemRow-${element.id}`).click(function () {
                let all = $(this);
                deleteItemById(all.attr('data-id'));
                listItems(db);
                clearInputs();
            });
        });
        calculatePrice(db);
    }

    // Utils =>
    let createId = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    function setOptionSelectedTypeConcept(option) {
        let selectTypes = $('#type');
        selectTypes.empty();
        selectTypes.append(`<option value="1" ${option == 1 ? 'selected' : ''}>Servicio</option>
                            <option value="2" ${option == 2 ? 'selected' : ''}>Contable</option>
                            <option value="3" ${option == 3 ? 'selected' : ''}>Sucursal</option>`);
    }

    function setOptionSelectedTypeService(option) {
        let selectTypes = $('#typeService');
        selectTypes.empty();
        selectTypes.append(`<option value="1" ${option == 1 ? 'selected' : ''}>Mensual</option>
                            <option value="2" ${option == 2 ? 'selected' : ''}>Bimestral</option>
                            <option value="3" ${option == 3 ? 'selected' : ''}>Trimestral</option>
                            <option value="4" ${option == 4 ? 'selected' : ''}>Cuatrimestral</option>
                            <option value="5" ${option == 5 ? 'selected' : ''}>Semestral</option>
                            <option value="7" ${option == 7 ? 'selected' : ''}>Único</option>
                            <option value="6" ${option == 6 ? 'selected' : ''}>Semanal</option>
                            <option value="8" ${option == 8 ? 'selected' : ''}>Personalizado</option>`);
    }

});
