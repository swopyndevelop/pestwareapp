import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";


$(document).ready(function() {

    let initialDateEvent;
    let finalDateEvent;

    let btnRoute = document.getElementById('btnRoute');

    btnRoute.onclick = function () {
        let employee = $("#technician option:selected").val();
        getTrackingByEmployee(employee);
    }

    $(function() {

        $('input[name="filterDateEvent"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateEvent"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateEvent = picker.startDate.format('YYYY-MM-DD');
            finalDateEvent = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateEvent"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    const map = new google.maps.Map(document.getElementById(`map-gps`), {
        zoom: 13,
        center: { lat: 21.91545, lng: -102.2790067 },
    });

    getLastPositions();

    function getLastPositions() {
        loaderPestWare('');
        let token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            type: 'GET',
            url: route('gps_last_positions'),
            data: {
                _token: token
            },
            success: function(response) {
                Swal.close();
                if (response.code === 500) {
                    showToast('error', 'Error', response.message);
                } else {
                    response.forEach((element) => {
                        const myLatLng = { lat: Number(element.lastPosition.dbLat), lng: Number(element.lastPosition.dbLon) };
                        const image =
                            "https://cdn3.iconfinder.com/data/icons/map-and-navigation-2-2/49/55-50.png";
                        // Esta es la información del marker que se va a mostrar, el contenido acepta HTML
                        let rows = '';
                        element.events.forEach((event) => {
                            let status = 'Programado';
                            if (event.id_status == 4) status = 'Finalizado';
                            if (event.id_status == 3) status = 'Cancelado';
                            if (event.id_status == 2) status = 'Comenzado';
                            rows += `<tr>
                                    <td class="text-center">${event.id}</td>
                                    <td class="text-center">${event.title}</td>
                                    <td class="text-center">${event.initial_date}</td>
                                    <td class="text-center">${event.initial_hour}</td>
                                    <td class="text-center">${status}</td>
                                </tr>`;
                        });

                        let infowindow = new google.maps.InfoWindow({
                            content: `
                                Técnico: <strong>${element.name}</strong><br>
                                Última actualización GPS: <strong>${element.lastPosition.date} ${element.lastPosition.hour}</strong>
                                <br><br>
                                <table class="table table-hover" style="width: max-content;n">
                                  <thead>
                                    <tr class="bg-primary">
                                      <th class="text-center"># OS</th>
                                      <th class="text-center">Servicio</th>
                                      <th class="text-center">Fecha</th>
                                      <th class="text-center">Hora</th>
                                      <th class="text-center">Estatus</th>
                                    </tr>
                                  </thead>
                                  <tbody id=´table-body-${element.id}´>
                                       ${rows}
                                  </tbody>
                                </table>
                            `
                        });
                        const marker = new google.maps.Marker({
                            position: myLatLng,
                            map,
                            label: element.name,
                            title: element.name,
                            icon: image
                        });
                        // Al hacer click sobre el marker mostraremos su información en una ventana
                        marker.addListener('click', function() {
                            infowindow.open(map, marker);
                        });
                    });
                }
            }
        });
    }

    function getTrackingByEmployee(employeeId)
    {
        loaderPestWare('Obteniendo ruta...');
        $.ajax({
            type: 'POST',
            url: route('gps_tracking'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                employee_id: employeeId,
                initial_date: initialDateEvent,
                final_date: finalDateEvent,
            },
            success: function (data) {
                if (data.code === 500){
                    Swal.close();
                    showToast('error','Error', data.message);
                }
                else{
                    const flightPlanCoordinates = [];
                    const first = data[0];
                    const last = data[data.length - 1];

                    const firstLatLng = { lat: Number(first.dbLat), lng: Number(first.dbLon) };
                    const image = "https://cdn4.iconfinder.com/data/icons/real-estate-flat-8/32/Real_estate_Home_location_marker_pin_map-54.png";

                    new google.maps.Marker({
                        position: firstLatLng,
                        map,
                        label: 'Inicio',
                        title: 'Inicio',
                        icon: image
                    });

                    data.forEach((element) => {
                        flightPlanCoordinates.push({
                            lat: Number(element.dbLat), lng: Number(element.dbLon)
                        });
                    })

                    const flightPath = new google.maps.Polyline({
                        path: flightPlanCoordinates,
                        geodesic: true,
                        strokeColor: "#FF0000",
                        strokeOpacity: 1.0,
                        strokeWeight: 2,
                    });

                    flightPath.setMap(map);
                    Swal.close();
                }
            }
        });
    }
});