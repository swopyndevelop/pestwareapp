import {loaderPestWareLogin} from "../pestware/loaderLogin";
import {expressionsAuth, getFieldsAuth, setFieldsAuth, validateFieldAuth} from "./validations";
import {expressions, getFields} from "../validations/regex";

$(document).ready(function() {
    //Validations
    let dataInput = {};
    const inputs = document.querySelectorAll('#formRegisterNewInstance input');

    const fieldsLogin = {
        inputCompany: false,
        inputContact: false,
        inputPhone: false,
        inputEmailRegister: false,
        terms: false,
    }

    setFieldsAuth(fieldsLogin);

    const validateLogin = (e) => {
        switch (e.target.name) {
            case "company":
                validateFieldAuth(expressions.description, e.target.value, 'inputCompany','iconCompany');
                break;
            case "contact":
                validateFieldAuth(expressions.description, e.target.value, 'inputContact','iconContact');
                break;
            case "phone":
                validateFieldAuth(expressions.phone, e.target.value, 'inputPhone','iconPhone');
                break;
            case "emailRegister":
                validateFieldAuth(expressions.email, e.target.value, 'inputEmailRegister','iconEmailRegister');
                break;
        }
    }

    inputs.forEach((input) => {
        input.addEventListener('keyup', validateLogin);
        input.addEventListener('blur', validateLogin);
    });

    let finger = 0;

    // Class maked //Event Change establishment
    const codeCountry = document.querySelector('#selectCodeRegister');
    codeCountry.addEventListener('change', (event) => {
        if (event.target.value != 0){
            document.getElementById('selectCodeRegister').classList.remove('error-select');
            document.getElementById('iconCodeRegister').classList.remove('icon-error');
        }
    });

    $('#formRegisterNewInstance').on('submit', function(event) {

        event.preventDefault();
        const username = document.getElementById('company').value;
        const contact = document.getElementById('contact').value;
        const phone = document.getElementById('phone').value;
        const email = document.getElementById('emailRegister').value;
        //const password = document.getElementById('passwordRegister').value;
        const codeCountry = $("#codeRegister option:selected").val();
        const terms = document.getElementById('terms');

        if(codeCountry == 0){
            document.getElementById('selectCodeRegister').classList.add('error-select');
            document.getElementById('iconCodeRegister').classList.add('icon-error');
        }

        // Validations
        validateFieldAuth(expressions.description, username, 'inputCompany','iconCompany');
        validateFieldAuth(expressions.description, contact, 'inputContact','iconContact');
        validateFieldAuth(expressions.phone, phone, 'inputPhone','iconPhone');
        validateFieldAuth(expressions.email, email, 'inputEmailRegister','iconEmailRegister');
        //validateFieldAuth(expressions.password, password, 'inputPasswordRegister','iconPasswordRegister');

        if(getFieldsAuth().inputCompany && getFieldsAuth().inputContact && getFieldsAuth().inputPhone &&
            getFieldsAuth().inputEmailRegister && codeCountry !=0 && terms.checked === true){
            fingerUser(username, contact, phone, email, codeCountry);
        }
        else if (terms.checked === false) showErrorsRegister('Debes aceptar los t�rminos y condiciones.');
        else fingerUser(username, contact, phone, email, codeCountry);
    });

    function register(username, contact, phone, email, codeCountry) {
        loaderPestWareLogin('');
        if (finger != 0){
            dataInput = {
                _token: $("meta[name='csrf-token']").attr("content"),
                username: username,
                contact: contact,
                phone: phone,
                email: email,
                codeCountry: codeCountry,
                ip: finger.ip,
                city: finger.city,
                country_code: finger.country_code,
                country_name: finger.country_name,
                latitude: finger.latitude,
                longitude: finger.longitude,
                region_code: finger.region_code,
                region_name: finger.region_name,
                time_zone: finger.time_zone,
                zip_code: finger.zip_code
            }
        }
        else {
            dataInput = {
                _token: $("meta[name='csrf-token']").attr("content"),
                username: username,
                contact: contact,
                phone: phone,
                email: email,
                codeCountry: codeCountry,
            }
        }
        $.ajax({
            type: 'POST',
            url: '/register/instance',
            data: dataInput,
            success: function(data) {
                if (data.code === 201) {
                    Swal.fire({
                        title: '<strong>Solicitud Registrada</strong>',
                        type: 'success',
                        html: data.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        if (result.value === true) {
                            window.location.href = '/';
                        } else if (result.dismiss === 'cancel' || result.dismiss === 'close' || result.dismiss === 'overlay' ) {
                            window.location.href = '/';
                        }
                    })
                }
                else if (data.code === 401) {
                    showErrorsRegister(data.message);
                    Swal.close();
                }
            }
        });
    }

    function fingerUser(username, contact, phone, email, codeCountry) {
        register(username, contact, phone, email, codeCountry);
        /*$.ajax({
            type: 'GET',
            url: 'https://freegeoip.app/json/',
            success: function(data) {
                finger = data;
                register(username, contact, phone, email, codeCountry);
            },
            error: function () {
                register(username, contact, phone, email, codeCountry);
            }
        });*/
    }

    function showErrorsRegister(messages) {
        createErrorsRegister();
        $('#message-errors-register').append(`<ul><li>${messages}</li></ul>`);
        setTimeout(() => {
            $('#message-errors-register').remove();
        }, 4000);
    }

    function createErrorsRegister() {
        $('#message-errors-register').remove();
        let div = `<div class="alert-danger" id="message-errors-register"></div>`;
        $('#errors-register').append(div);
    }

    // Validations input event
    const inputEmail = document.getElementById('email');
    const inputPassword = document.getElementById('password');

    const fieldsLoginForm = {
        inputEmail: false,
        inputPassword: false
    }

    setFieldsAuth(fieldsLoginForm);

    const validateFormLogin = (e) => {
        switch (e.target.name) {
            case "email":
                validateFieldAuth(expressionsAuth.email, e.target.value, 'inputEmail', 'iconEmail');
                break;
            case "password":
                validateFieldAuth(expressionsAuth.password, e.target.value, 'inputPassword', 'iconPassword');
                break;
        }
    }

    inputEmail.addEventListener('keyup', validateFormLogin);
    inputEmail.addEventListener('blur', validateFormLogin);
    inputPassword.addEventListener('keyup', validateFormLogin);
    inputPassword.addEventListener('blur', validateFormLogin);
});