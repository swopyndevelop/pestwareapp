import {loaderPestWareLogin} from "../pestware/loaderLogin";
import {expressionsAuth, setFieldsAuth, validateFieldAuth} from "./validations";

$(document).ready(function() {

    let finger = 0;

    // show messages
    if (location.pathname.includes('verified')) {
        createMessages();
        $('#message-errors').append('<ul><li>¡Cuenta verificada Correctamente!</li></ul>');
        setTimeout(() => {
            $('#message-errors').remove();
        }, 4000);
    }

    if (location.pathname.includes('verify')) {
        createErrors();
        $('#message-errors').append('<ul><li>Te enviamos un correo para confirmar tu cuenta. Ingresa a tu bandeja de entrada.</li></ul>');
        setTimeout(() => {
            $('#message-errors').remove();
            location.pathname = '/instance/login'
        }, 4000);
    }

    if (location.pathname.includes('not-verified')) {
        createErrors();
        $('#message-errors').append('<ul><li>Error al confirmar cuenta, intenta más tarde.</li></ul>');
        setTimeout(() => {
            $('#message-errors').remove();
            location.pathname = '/instance/login'
        }, 4000);
    }
    // end show messages

    $('#formLoginNew').on('submit', function(event) {
        event.preventDefault();
        const username = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        fingerUser(username, password);
    });

    function authenticate(username, password) {
        loaderPestWareLogin('');
        $.ajax({
            type: 'POST',
            url: '/login',
            data: {
                _token: $("meta[name='csrf-token']").attr("content"),
                email: username,
                password: password,
                finger: finger
            },
            success: function(data) {
                if (data.code === 200) window.location.href = '/index_register';
                else if (data.code === 301) {
                    Swal.close();
                    createErrors();
                    $('#message-errors').append(`<ul><li>${data.message}</li></ul>`);
                    document.getElementById("sendEmailBackup").style.display = "block";
                    setTimeout(() => {
                        $('#message-errors').remove();
                        document.getElementById("sendEmailBackup").style.display = "none";
                    }, 7000);
                }
                else if (data.code === 401) {
                    showErrors(data.message);
                    Swal.close();
                }
                else if (data.code === 302) {
                    Swal.fire({
                        title: '<strong>Tu cuenta está suspendida.</strong>',
                        type: 'error',
                        html: data.message +'<br><br><p style="color: #1E8CC7;">Si ya realizaste tu pago, ponte en contacto a: soporte@pestwareapp.com o +52 449 413 90 91.</p>',
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        Swal.close()
                    })
                }
            }
        });
    }

    function showErrors(messages) {
        let errors = '';

        if (!Array.isArray(messages) && !Array.isArray(messages.email) && !Array.isArray(messages.password)) {
            errors = messages;
        }
        else {
            if (Array.isArray(messages.email)) {
                messages.email.forEach((element) => {
                    errors += `<ul><li>${element}</li></ul>`;
                });
            } else if (messages.email !== undefined) errors += messages.email;

            if (Array.isArray(messages.password)) {
                messages.password.forEach((element) => {
                    errors += `<ul><li>${element}</li></ul>`;
                });
            } else if (messages.password !== undefined) errors += messages.password;
        }
        createErrors();
        $('#message-errors').append(errors);
        setTimeout(() => {
            $('#message-errors').remove();
        }, 4000);
    }

    function createErrors() {
        $('#message-errors').remove();
        let div = `<div class="alert-danger" id="message-errors"></div>`;
        $('#errors').append(div);
    }

    function createMessages() {
        $('#message-errors').remove();
        let div = `<div class="alert-success" id="message-errors"></div>`;
        $('#errors').append(div);
    }

    // Validations input event
    const inputEmail = document.getElementById('email');
    const inputPassword = document.getElementById('password');

    const fieldsLoginForm = {
        inputEmail: false,
        inputPassword: false
    }

    setFieldsAuth(fieldsLoginForm);

    const validateFormLogin = (e) => {
        switch (e.target.name) {
            case "email":
                validateFieldAuth(expressionsAuth.email, e.target.value, 'inputEmail', 'iconEmail');
                break;
            case "password":
                validateFieldAuth(expressionsAuth.password, e.target.value, 'inputPassword', 'iconPassword');
                break;
        }
    }

    inputEmail.addEventListener('keyup', validateFormLogin);
    inputEmail.addEventListener('blur', validateFormLogin);
    inputPassword.addEventListener('keyup', validateFormLogin);
    inputPassword.addEventListener('blur', validateFormLogin);

    $('#recoverPassword').on('click', function(event) {
        showRecoverPassword();
    });

    $('#sendEmailBackup').on('click', function(event) {
        console.log('resend click')
        const email = document.getElementById('email').value;
        reSendEmailConfirm(email);
    });

    function showRecoverPassword() {
        Swal.fire({
            title: 'Correo electrónico',
            input: 'email',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Recuperar contraseña',
            cancelButtonText: 'Cerrar',
            showLoaderOnConfirm: true,
            preConfirm: (email) => {
                return fetch(`../../recover/password/${email}`)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                            `Error: ${error}`
                        )
                    })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value.code === 404) {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops...',
                    text: result.value.message
                })
            } else {
                Swal.fire({
                    type: 'info',
                    title: 'Correo enviado...',
                    text: result.value.message
                })
            }
        })
    }

    function reSendEmailConfirm(email) {
        console.log('resend')
        loaderPestWareLogin('');
        $.ajax({
            type: 'POST',
            url: '/account/email/verify',
            data: {
                _token: $("meta[name='csrf-token']").attr("content"),
                email: email
            },
            success: function(data) {
                Swal.close()
                document.getElementById("sendEmailBackup").style.display = "block";
;                if (data.code === 200) {
                    Swal.fire({
                        title: '<strong>Correo de confirmación enviado</strong>',
                        type: 'success',
                        html: data.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Entendido',
                        confirmButtonAriaLabel: 'Thumbs up, great!',
                        cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i> Cerrar',
                        cancelButtonAriaLabel: 'Cerrar'
                    }).then((result) => {
                        if (result.value === true) {
                            window.location.href = '/instance/login/verify';
                        } else if (result.dismiss === 'cancel' || result.dismiss === 'close' || result.dismiss === 'overlay' ) {
                            window.location.href = '/instance/login/verify';
                        }
                    })
                }
                else {
                    Swal.fire({
                        type: 'warning',
                        title: 'Oops...',
                        text: data.message
                    })
                }
            }
        });
    }

    function fingerUser(username, password) {
        $.ajax({
            type: 'GET',
            url: 'https://freegeoip.app/json/',
            success: function(data) {
                finger = data;
                authenticate(username, password);
            },
            error: function () {
                authenticate(username, password);
            }
        });
    }
});