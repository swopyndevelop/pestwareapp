export const expressionsAuth = {
    email: /^[^\s@]+@[^\s@]+\.[^\s@]+$/, // email@email.com
    password: /^.{8,50}$/ // string length with range 8-50
}

let fieldsAuth = {};
export function setFieldsAuth(fieldsForm) {
    fieldsAuth = fieldsForm;
}

export function setOnlyFieldAuth(field, value) {
    fieldsAuth[field] = value;
}

export function getFieldsAuth() {
    return fieldsAuth;
}

export const validateFieldAuth = (expresion, value, field, icon) => {

    if(expresion.test(value)){
        document.getElementById(`${field}`).classList.remove('error-input');
        document.getElementById(`${icon}`).classList.remove('icon-error');
        fieldsAuth[field] = true;
    } else {
        document.getElementById(`${field}`).classList.add('error-input');
        document.getElementById(`${icon}`).classList.add('icon-error');
        fieldsAuth[field] = false;
    }
}
