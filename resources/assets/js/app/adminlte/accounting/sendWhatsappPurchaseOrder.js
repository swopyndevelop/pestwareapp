/**
 * @author Alberto Martínez | Manuel Mendoza
 * @version 26/04/2021
 * accounting/index.blade.php
 */

import {expressions, getFields, validateField} from "../validations/regex";
import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){
    let urlWhatsapp, cellphone, cellphoneOther, codeCountry;
    $('.openModalWhatsappPurchaseOrder').click(function () {
        let all = $(this);
        $('#idPurchaseOrderWhatsapp').val(all.attr('data-id'));
        $('#cellphonePurchaseOrderProvider').val(all.attr('data-contact-cellphone-provider'));
    });

    $("#whatsappPurchaseOrder").on('hidden.bs.modal', function () {
        document.getElementById('idPurchaseOrderWhatsapp').value = '';
        document.getElementById('cellphonePurchaseOrderProvider').value = '';
        document.getElementById('cellphonePurchaseOrderOther').value = '';
    });

    $('#sendWhatsappPurchaseOrderButton').click(function () {
        const idPurchaseOrderWhatsapp = $('#idPurchaseOrderWhatsapp').val();
        // https://pestwareapp.com   http://127.0.0.1:8000/
        const dataRoute = `https://pestwareapp.com/acounting/view/pdf/purchase/${idPurchaseOrderWhatsapp}`;
        const cellphonePurchaseOrderProvider = document.getElementById('cellphonePurchaseOrderProvider').value;
        const cellphonePurchaseOrderOther = document.getElementById('cellphonePurchaseOrderOther').value;
        const codeCountryPurchaseOrder = document.getElementById('codeCountryPurchaseOrder').value;

        if (cellphonePurchaseOrderProvider.length <= 0) {
            showToast('warning','celular Proveedor','Favor de ingresar un correo');
        }
        if (cellphonePurchaseOrderProvider.length !== 0){
            codeCountry = codeCountryPurchaseOrder;
            cellphone = cellphonePurchaseOrderProvider;
            urlWhatsapp = `https://api.whatsapp.com/send?phone=${codeCountry}${cellphone}&text=Orden de Compra: ${dataRoute}`;
            window.open(urlWhatsapp, '_blank');
            $("#whatsappPurchaseOrder").click();
        }
        if (cellphonePurchaseOrderOther.length !== 0){
            codeCountry = codeCountryPurchaseOrder;
            cellphoneOther = cellphonePurchaseOrderOther;
            urlWhatsapp = `https://api.whatsapp.com/send?phone=${codeCountry}${cellphoneOther}&text=Orden de Compra: ${dataRoute}`;
            window.open(urlWhatsapp, '_blank');
            $("#whatsappPurchaseOrder").click();
        }

    });

});