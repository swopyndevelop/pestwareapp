/**
 * @author Manuel Mendoza
 * @version 2021
 * register/_deleQuotation.blade.php
 */

import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";

$(document).ready(function(){

    $('.openModalDeletePurchaseOrder').click(function () {
        let all = $(this);
        //START Delete Purchase Order
        $('#deletePurchaseOrderSave').click(function(){
            const idPurchaseOrder = all.attr('data-id');
            deleteQuotationSave(idPurchaseOrder);
    });
    function deleteQuotationSave(idPurchaseOrder){
        loaderPestWare('Eliminando Orden de Compra...')
        $.ajax({
            type: 'delete',
            url: route('delete_purchase_order'),
            data:{
                _token: $("meta[name=csrf-token]").attr("content"),
                idPurchaseOrder: idPurchaseOrder
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Error al guardar', data.message);
                }
                else if (data.code == 201){
                    Swal.close();
                    showToast('success-redirect', 'Orden de Compra Eliminada', data.message, 'index_accounting');
                    $("#deletePurchaseOrderSave").prop("disabled", true);
                }
            }
        });
    }
    });

 });
