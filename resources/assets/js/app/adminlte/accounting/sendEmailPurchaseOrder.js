/**
 * @author Alberto Martínez | Manuel Mendoza
 * @version 26/12/2020
 * accounting/index.blade.php
 */

import {expressions, getFields, validateField} from "../validations/regex";
import {showToast} from "../pestware/alerts";
import {loaderPestWare} from "../pestware/loader";

$(document).ready(function(){
    $('.openModalSendMailPurchaseOrder').click(function () {
        let all = $(this);
        $('#idPurchaseOrderEmail').val(all.attr('data-id'));
        $('#emailPurchaseOrderProvider').val(all.attr('data-contact-email-provider'));
    });

    $("#sendmailPurchaseOrder").on('hidden.bs.modal', function () {
        document.getElementById('emailPurchaseOrderProvider').value = '';
        document.getElementById('emailPurchaseOrderOther').value = '';
    });

    $('#sendEmailPurchaseOrderButton').click(function () {

        const emailPurchaseOrderProvider = document.getElementById('emailPurchaseOrderProvider').value;
        const emailPurchaseOrderOther = document.getElementById('emailPurchaseOrderOther').value;
        const idPurchaseOrderEmail = $('#idPurchaseOrderEmail').val();
        let emails = [];

        if (emailPurchaseOrderProvider.length < 0) showToast('warning','Email Proveedor','Favor de ingresar un correo');
        //if (emailPurchaseOrderOther !== 0) showToast('warning','Email Otro','Favor de ingresar un correo');

        if (emailPurchaseOrderProvider.length !== 0) emails.push(emailPurchaseOrderProvider);
        if (emailPurchaseOrderOther.length !== 0) emails.push(emailPurchaseOrderOther);
        sendEmailPurchaseOrder(idPurchaseOrderEmail, emails);

    });

    function sendEmailPurchaseOrder(idPurchaseOrderEmail, emails){
        loaderPestWare('Enviando Orden De Compra...')
        $.ajax({
            type: 'post',
            url: route('send_purchase_order'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idPurchaseOrderEmail: idPurchaseOrderEmail,
                emails: emails
            },
            success: function (data) {
                if (data.code == 500){
                    Swal.close();
                    showToast('error', 'Algo salió mal', data.message);
                }
                else{
                    Swal.close();
                    showToast('success', 'Orden Enviada', data.message);
                    $("#sendmailPurchaseOrder").click();
                }
            }
        });
    }

});