/**
 * @author Alberto Martínez|Manuel Mendoza
 * @version 26/04/2021
 * customers/_modalCreateAreaV2.blade.php
 */

import {expressions, getFields, validateField} from "../validations/regex";
import {loaderPestWare} from "../pestware/loader";
import {showToast} from "../pestware/alerts";
import {alert} from "ionicons/icons";

$(document).ready(function(){

    let idArea;
    let initial_date = null;
    let final_date = null;

    $(function() {

        $('input[name="filterDateReportArea"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="filterDateReportArea"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initial_date = picker.startDate.format('YYYY-MM-DD');
            final_date = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="filterDateReportArea"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });

    $('.openModalCreateReport').click(function () {

        let all = $(this);
        idArea = all.attr('data-id');
        console.log(idArea);

    });

    $('#buttonGenerateReport').click(function () {

        if (initial_date == null) {
            showToast('warning', 'Fecha', 'No ingresaste la fecha de Reporte');
            return
        }
        console.log(final_date)
        getDataCustomerDocuments();

    });

    function getDataCustomerDocuments(){
        //loaderPestWare('Descargando Reporte...')
        $.ajax({
            type: 'POST',
            url: route('report_inspections_area'),
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                idArea: idArea,
                initial_date: initial_date,
                final_date: final_date,
            },
            xhrFields: {
                responseType: 'blob' // Esperamos un blob (archivo) en la respuesta
            },
            success: function (data) {
                Swal.close();

                if (data instanceof Blob || data instanceof File) {
                    const url = window.URL.createObjectURL(data);
                    const a = document.createElement('a');
                    a.href = url;
                    a.download = `Reporte de Areas del ${initial_date} al ${final_date}.pdf`; // Nombre de archivo para la descarga
                    document.body.appendChild(a);
                    a.click();
                    window.URL.revokeObjectURL(url);
                    showToast('success', data.message, 'Se descargaron correctamente los documentos.');
                } else {
                    console.log("No es un objeto Blob o File");
                    showToast('error', 'Algo salió mal', data.message);
                }

            },
            error: function() {
                Swal.close();
                showToast('error', 'Algo salió mal', `No hay ${nameFile} del ${initial_date} al ${final_date} `);
            }
        });
    }

});