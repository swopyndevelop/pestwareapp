import React, {useState, useEffect} from 'react'
import ReactDOM from 'react-dom'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import Axios from 'axios'
import Moment from 'moment';
import {extendMoment} from 'moment-range';
import {Line, Doughnut} from "react-chartjs-2";
import { data } from 'jquery';
import {loaderPestWare} from "../app/adminlte/pestware/loader";


const InitApp = () => {
    const dateFirst = new Date();
    const company = document.getElementById('companieMonitoring').value;
    const symbolCountry = document.getElementById('symbolCountry').value;
    const startOfMonth = new Date(dateFirst.getFullYear(), dateFirst.getMonth(), 1);
    const [startDate, setStartDate] = useState(startOfMonth)
    const [endDate, setEndDate] = useState(new Date())
    const [data0, setData0] = useState({})
    const [data1, setData1] = useState({})
    const [data2, setData2] = useState({})
    const [data3, setData3] = useState({})
    const [data4, setData4] = useState({})
    const [data5, setData5] = useState({})
    const [dataPlague0, setDataPlague0] = useState({})
    const [dataPlague1, setDataPlague1] = useState({})
    const [dataPlague2, setDataPlague2] = useState({})
    const [dataPlague3, setDataPlague3] = useState({})
    const [dataPlague4, setDataPlague4] = useState({})
    const [dataPlague5, setDataPlague5] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const [didMount, setDidMount] = useState(true)
    const [dataTable, setDataTable] = useState([])
    const [dataSelectJob, setDataSelectJob] = useState([])
    const [dataSelectEmp, setDataSelectEmp] = useState([])
    const [selectJob, setSelectJob] = useState(0)
    const [selectEmp, setSelectEmp] = useState(0)
    const [dataGroup, setDataGroup] = useState([])
    const [stateGroup, setStateGroup] = useState([])
    const [dataModal, setDataModal] = useState([])
    const [dataModalGs, setDataModalGs] = useState([])
    const [dataModalGi, setDataModalGi] = useState([])
    const [dataModalSi, setDataModalSi] = useState([])

    let period = `Movimientos del período ${Moment(startDate).format('DD')} de ${Moment(startDate).format('MMMM')} al ${Moment(endDate).format('DD')} de ${Moment(endDate).format('MMMM')}`;
    const optionsFormat = { style: 'currency', currency: 'MXN' };
    const numberFormat = new Intl.NumberFormat('es-MX', optionsFormat);
    let colorsRandom = [];
    let colorsTransparent = [];

    const generateRandomColor = () => {
        for (let i = 0; i < 100; i++) {
            const randomColor = Math.floor(Math.random()*16777215).toString(16);
            colorsRandom.push(`#${randomColor}`);
            colorsTransparent.push('#E8F6F3');
        }
    }

    const optionsChartLine = {
        scales: {
            yAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: 'Monto Total por Día'
                    },
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return `${numberFormat.format(value)}`;
                        },
                        min: 0,
                    },
                },
            ],
            xAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: period
                    },
                },
            ],
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    let label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += numberFormat.format(Math.round(tooltipItem.yLabel * 100) / 100);

                    return label;
                }
            }
        }
    }

    useEffect(() => {
        generateRandomColor();
        const getInitData = async () => {
            const responseJob = await Axios.get('../filter/jobCenters')
            setDataSelectJob(responseJob.data)
            const responseEmp = await Axios.get('../filter/employees')
            setDataSelectEmp(responseEmp.data)
        }
        const moment = extendMoment(Moment);

        if (didMount) {
            getInitData();
            let labelsArray = []
            let dataArray = []
            let rowsTable = []
            let rowsModal = []
            let dataGroupArray = []
            let dataTitleArray = ['Cotizaciones totales', 'Ventas Brutas', 'Ingreso Bruto', 'Gasto Total', 'Beneficio Bruto', 'Índice de Satisfacción']
            let dataPlague = []
            let labelPlague = []
            let range = moment.range(moment(startDate, 'YYYY-MM-DD'), moment(endDate, 'YYYY-MM-DD'))
            let daysArray = Array.from(range.by('day', 'YYYY-MM-DD'))
            let daysArrayFormat = daysArray.map(m => m.format('YYYY-MM-DD'))
            setIsLoading(true);
            loaderPestWare('Espere un momento');
            setData0({})
            setData1({})
            setData2({})
            setData3({})
            setData4({})
            setData5({})
            const getData = async () => {
                // const response = await Axios.post('/pestwareapp/public/api/js/reporting/global/total',{
                const response = await Axios.post('/api/js/reporting/global/total', {

                    user_id: selectEmp,
                    id_job_center: parseInt(selectJob),
                    company: company,
                    initial_date: Moment(startDate).format("YYYY-MM-DD"),
                    final_date: Moment(endDate).format("YYYY-MM-DD"),
                })

                dataGroupArray.push([dataTitleArray[0], response.data.quotationTotal.total, '#quotation-chart', true])
                dataGroupArray.push([dataTitleArray[1], response.data.grossSalesTotal.total, '#sales-chart', false])
                dataGroupArray.push([dataTitleArray[2], response.data.grossIncomeTotal.total, '#income-chart', false])
                dataGroupArray.push([dataTitleArray[3], response.data.grossProfitTotal.total, '#cost-chart', false])
                dataGroupArray.push([dataTitleArray[4], response.data.totalCost.total, '#profit-chart', false])
                dataGroupArray.push([dataTitleArray[5], response.data.satisfactionIndex.total, '#rating-chart', false])
                setStateGroup(dataGroupArray)

                Object.entries(response.data).map((chart) => {
                    let labelsArrayTemp = []
                    let dataArrayTemp = []
                    let rowsTableTemp = []

                    chart[1].groupByDay.forEach((element) => {

                        labelsArrayTemp.push(element.date)
                        dataArrayTemp.push(element.total)

                        rowsTableTemp.push([element.date, element.total])
                    });
                    labelsArray.push(labelsArrayTemp)
                    dataArray.push(dataArrayTemp)
                    rowsTable.push(rowsTableTemp)
                    rowsModal.push(chart);

                })

                let dataSetObj = {
                    labels: labelsArray[0],
                    datasets: [{
                        label: dataTitleArray[0],
                        data: dataArray[0],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData0(dataSetObj)

                dataSetObj = {
                    labels: labelsArray[1],
                    datasets: [{
                        label: dataTitleArray[1],
                        data: dataArray[1],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData1(dataSetObj)

                dataSetObj = {
                    labels: labelsArray[2],
                    datasets: [{
                        label: dataTitleArray[2],
                        data: dataArray[2],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData2(dataSetObj)

                dataSetObj = {
                    labels: labelsArray[3],
                    datasets: [{
                        label: dataTitleArray[3],
                        data: dataArray[3],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData3(dataSetObj)

                dataSetObj = {
                    labels: labelsArray[4],
                    datasets: [{
                        label: dataTitleArray[4],
                        data: dataArray[4],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData4(dataSetObj)

                dataSetObj = {
                    labels: labelsArray[5],
                    datasets: [{
                        label: dataTitleArray[5],
                        data: dataArray[5],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData5(dataSetObj)

                let labelPlagueTemp = []
                let dataPlagueTemp = []
                let percentage = 0
                let total = response.data.quotationTotal.total_number
                response.data.quotationTotal.plagueType.forEach((element) => {
                    labelPlagueTemp.push(`${element.key}: ${numberFormat.format(element.total)}`)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))
                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                percentage = 0
                total = response.data.grossSalesTotal.total_number
                response.data.grossSalesTotal.plagueType.forEach((element) => {
                    labelPlagueTemp.push(`${element.key}: ${numberFormat.format(element.total)}`)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))

                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                percentage = 0
                total = response.data.grossIncomeTotal.total_number
                response.data.grossIncomeTotal.plagueType.forEach((element) => {
                    labelPlagueTemp.push(`${element.key}: ${numberFormat.format(element.total)}`)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))
                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                percentage = 0
                total = response.data.grossProfitTotal.total_number
                response.data.grossProfitTotal.plagueType.forEach((element) => {
                    labelPlagueTemp.push(`${element.key}: ${numberFormat.format(element.total)}`)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))
                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                response.data.totalCost.plagueType.forEach((element, i) => {
                    let utilidadReal = 100 - element.total
                    i === 0 ? labelPlagueTemp.push(utilidadReal.toFixed(2) + '% de utilidad') : labelPlagueTemp.push('')
                    dataPlagueTemp.push(utilidadReal)
                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                percentage = 0
                total = response.data.satisfactionIndex.count
                response.data.satisfactionIndex.plagueType.forEach((element) => {
                    let label;
                    if (element.rating == 5) label = 'Excelente';
                    else if (element.rating == 4) label = 'Bueno';
                    else if (element.rating == 3) label = 'Regular';
                    else if (element.rating == 2) label = 'Malo';
                    else if (element.rating == 1) label = 'Decepcionante';
                    else label = 'Otro';
                    labelPlagueTemp.push(label)
                    percentage = (element.count / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))
                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                let dataSetPlagueObj = {
                    labels: labelPlague[0],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: colorsRandom,
                        hoverBackgroundColor: colorsTransparent,
                        data: dataPlague[0]
                    }]
                }
                setDataPlague0(dataSetPlagueObj)

                dataSetPlagueObj = {
                    labels: labelPlague[1],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: colorsRandom,
                        hoverBackgroundColor: colorsTransparent,
                        data: dataPlague[1]
                    }]
                }
                setDataPlague1(dataSetPlagueObj)

                dataSetPlagueObj = {
                    labels: labelPlague[2],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: colorsRandom,
                        hoverBackgroundColor: colorsTransparent,
                        data: dataPlague[2]
                    }]
                }
                setDataPlague2(dataSetPlagueObj)

                dataSetPlagueObj = {
                    labels: labelPlague[3],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: colorsRandom,
                        hoverBackgroundColor: colorsTransparent,
                        data: dataPlague[3]
                    }]
                }
                setDataPlague3(dataSetPlagueObj)

                dataSetPlagueObj = {
                    labels: labelPlague[4],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: [
                            '#17A589',
                            '#E8F6F3',
                            '#17A589',
                            '#3498DB',
                            '#9B59B6'
                        ],
                        hoverBackgroundColor: [
                            '#17A589',
                            '#E8F6F3',
                            '#D0ECE7',
                            '#D6EAF8',
                            '#EBDEF0'
                        ],
                        data: dataPlague[4]
                    }]
                }
                setDataPlague4(dataSetPlagueObj)

                dataSetPlagueObj = {
                    labels: labelPlague[5],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: colorsRandom,
                        hoverBackgroundColor: colorsTransparent,
                        data: dataPlague[5]
                    }]
                }
                setDataPlague5(dataSetPlagueObj)

                let result = new Array(daysArrayFormat.length).fill().map(() => new Array(6).fill(''))
                for (let i = 0; i <= daysArrayFormat.length - 1; i++) {
                    result[i][0] = daysArrayFormat[i]
                }
                Swal.close();
                for (let i = 0; i <= rowsTable.length - 1; i++) {
                    for (let j = 0; j <= rowsTable[i].length - 1; j++) {
                        result[daysArrayFormat.findIndex(day => day === rowsTable[i][j][0])][i + 1] = rowsTable[i][j][1]
                    }
                }
                setDataTable(result)
                setDataModal(response.data.quotationTotal.groupByDay)
                setDataModalGs(response.data.grossSalesTotal.groupByDay)
                setDataModalGi(response.data.grossIncomeTotal.groupByDay)
                setDataModalSi(response.data.satisfactionIndex.groupByDay)
                
            }
            getData()
            setIsLoading(false);
        } else {
            setDidMount(false)
            getInitData()
        }
    }, [startDate, endDate, selectJob, selectEmp]);


    return (
        <div className="container-fluid">
            <div className="row text-center" style={{marginTop: "10px"}}>
                <div className="col-md-6">
                    <div className="form-group" id="periodIntelligenceDiv">
                        <label><strong>Período</strong></label>
                        <div id="InitCalendar">
                            <DatePicker
                                selected={startDate}
                                onChange={date => setStartDate(date)}
                                selectsStart
                                startDate={startDate}
                                endDate={endDate}
                            />
                            <DatePicker
                                selected={endDate}
                                onChange={date => setEndDate(date)}
                                selectsEnd
                                startDate={startDate}
                                endDate={endDate}
                                minDate={startDate}
                            />
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="form-group" id="centerJobIntelligenceDiv">
                        <label><strong>Centro de trabajo</strong></label>
                        <select
                            className="form-control"
                            name="selectJob"
                            onChange={event => setSelectJob(event.target.value)}
                            value={selectJob}
                            style={{"width":"100%"}}
                        >
                            {dataSelectJob.map((job) => (
                                <option key={job.id + job.name} value={job.id}>{job.name}</option>
                            ))}
                        </select>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="form-group" id="employeeIntelligenceDiv">
                        <label><strong>Empleado</strong></label>
                        <select
                            className="form-control"
                            name="selectEmp"
                            onChange={event => setSelectEmp(event.target.value)}
                            value={selectEmp}
                            style={{"width":"100%"}}
                        >
                            {dataSelectEmp.map((emp) => (
                                <option key={emp.id + emp.name} value={emp.id}>{emp.name}</option>
                            ))}
                        </select>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12">
                    <div className="card card-primary">
                        <div className="card-header">
                            <i className="fa fa-pie-chart" aria-hidden="true" style={{color: "white"}}></i><span className="card-title text-white h4"> Inteligencia de Negocios </span>
                        </div>
                        <div className="card-body">
                            <div className="chart">
                                {isLoading ? (
                                    <div>Loading ...</div>
                                ) : (
                                    <div className="nav-tabs-custom">
                                        <ul className="nav nav-tabs pull-left">
                                            {stateGroup.map((t, i) => (
                                                <li className={i === 0 ? 'active' : ''}>
                                                    <a href={t[2]} data-toggle="tab">
                                                        {t[0]}
                                                        <br/>
                                                        <h3>{i === 5 ? <i className="fa fa-star" aria-hidden="true"></i> : symbolCountry}{t[1]}</h3>
                                                    </a>
                                                </li>
                                            ))}
                                        </ul>
                                        <div className="tab-content no-padding">
                                            <div className="chart tab-pane active" id="quotation-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={data0} options={optionsChartLine}/>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <Doughnut data={dataPlague0}
                                                            options={{
                                                                title: {
                                                                    display: true,
                                                                    text: 'Plagas',
                                                                    fontSize: 20
                                                                },
                                                                tooltips: {
                                                                    callbacks: {
                                                                        label: function(tooltipItem, data) {
                                                                            let label = data.labels[tooltipItem.index] || '';
                                                                            let percentage = data.datasets[0].data[tooltipItem.index];
                                                                            if (label) {
                                                                                label += ' / ';
                                                                            }
                                                                            label += percentage + '%';

                                                                            return label;
                                                                        }
                                                                    }
                                                                },
                                                                legend: {
                                                                    display: true,
                                                                    position: 'right'
                                                                }
                                                            }}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="chart tab-pane" id="sales-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={data1} options={optionsChartLine}/>
                                                    </div>
                                                    <div className="col-md-4 table-responsive">
                                                        <Doughnut data={dataPlague1}
                                                            options={{
                                                                title: {
                                                                    display: true,
                                                                    text: 'Plagas',
                                                                    fontSize: 20
                                                                },
                                                                tooltips: {
                                                                    callbacks: {
                                                                        label: function(tooltipItem, data) {
                                                                            let label = data.labels[tooltipItem.index] || '';
                                                                            let percentage = data.datasets[0].data[tooltipItem.index];
                                                                            if (label) {
                                                                                label += ' / ';
                                                                            }
                                                                            label += percentage + '%';

                                                                            return label;
                                                                        }
                                                                    }
                                                                },
                                                                legend: {
                                                                    display: true,
                                                                    position: 'right'
                                                                }
                                                            }}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="chart tab-pane" id="income-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={data2} options={optionsChartLine}/>
                                                    </div>
                                                    <div className="col-md-4 table-responsive">
                                                        <Doughnut data={dataPlague2}
                                                            options={{
                                                                title: {
                                                                    display: true,
                                                                    text: 'Plagas',
                                                                    fontSize: 20
                                                                },
                                                                tooltips: {
                                                                    callbacks: {
                                                                        label: function(tooltipItem, data) {
                                                                            let label = data.labels[tooltipItem.index] || '';
                                                                            let percentage = data.datasets[0].data[tooltipItem.index];
                                                                            if (label) {
                                                                                label += ' / ';
                                                                            }
                                                                            label += percentage + '%';

                                                                            return label;
                                                                        }
                                                                    }
                                                                },
                                                                legend: {
                                                                    display: true,
                                                                    position: 'right'
                                                                }
                                                            }}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="chart tab-pane" id="cost-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={data3} options={optionsChartLine}/>
                                                    </div>
                                                    <div className="col-md-4 table-responsive">
                                                        <Doughnut data={dataPlague3}
                                                                  options={{
                                                                      title: {
                                                                          display: true,
                                                                          text: 'Gastos',
                                                                          fontSize: 20
                                                                      },
                                                                      tooltips: {
                                                                          callbacks: {
                                                                              label: function(tooltipItem, data) {
                                                                                  let label = data.labels[tooltipItem.index] || '';
                                                                                  let percentage = data.datasets[0].data[tooltipItem.index];
                                                                                  if (label) {
                                                                                      label += ' / ';
                                                                                  }
                                                                                  label += percentage + '%';

                                                                                  return label;
                                                                              }
                                                                          }
                                                                      },
                                                                      legend: {
                                                                          display: true,
                                                                          position: 'right'
                                                                      }
                                                                  }}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="chart tab-pane" id="profit-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={data4} options={optionsChartLine}/>
                                                    </div>
                                                    <div className="col-md-4 table-responsive">
                                                        <Doughnut data={dataPlague4}
                                                            options={{
                                                                title: {
                                                                    display: true,
                                                                    text: 'Porcentaje de Utilidad',
                                                                    fontSize: 20
                                                                },
                                                                legend: {
                                                                    display: true,
                                                                    position: 'right'
                                                                }
                                                            }}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="chart tab-pane" id="rating-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={data5} options={optionsChartLine}/>
                                                    </div>
                                                    <div className="col-md-4 table-responsive">
                                                        <Doughnut data={dataPlague5}
                                                                  options={{
                                                                      title: {
                                                                          display: true,
                                                                          text: 'Porcentaje de Evaluación',
                                                                          fontSize: 20
                                                                      },
                                                                      tooltips: {
                                                                          callbacks: {
                                                                              label: function(tooltipItem, data) {
                                                                                  let label = data.labels[tooltipItem.index] || '';
                                                                                  let percentage = data.datasets[0].data[tooltipItem.index];
                                                                                  if (label) {
                                                                                      label += ' ';
                                                                                  }
                                                                                  label += percentage + '%';

                                                                                  return label;
                                                                              }
                                                                          }
                                                                      },
                                                                      legend: {
                                                                          display: true,
                                                                          position: 'right'
                                                                      }
                                                                  }}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12 table-responsive">
                    <table className="table tablesorter table-hover" id="tableProduct">
                        <thead>
                            <tr>
                                <th className="col-sm-1"># Fecha</th>
                                <th className="col-sm-1">Cotizaciones totales</th>
                                <th className="col-sm-1">Ventas Brutas</th>
                                <th className="col-sm-1">Ingreso Bruto</th>
                                <th className="col-sm-1">Gasto Total</th>
                                <th className="col-sm-1">Beneficio Bruto</th>
                            </tr>
                        </thead>
                        <tbody>
                        {dataTable.map((row) => (
                            (row[1] != '' || row[2] != '' || row[3] != '' || row[4] != '' || row[5] != '') ? (
                                <tr>
                                    <td className="col-sm-1"><a data-toggle="modal" data-target={'#detail'+row[0]}>{row[0]}</a>
                                        <div className="modal fade" id={'detail'+row[0]} tabIndex="-1">
                                            <div className="modal-dialog" style={{"width": "90%"}}>
                                                <div className="modal-content">
                                                    <div className="modal-header">
                                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                                        <h3>Detalle del día <span>{row[0]}</span></h3>
                                                    </div>
                                                    <div className="modal-body text-center">
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="col-md-12 table-responsive">
                                                                    <h5>Cotizaciones totales</h5>
                                                                    <table className="table tablesorter table-hover" id="tableProduct">
                                                                        <thead>
                                                                            <tr>
                                                                                <th className="col-sm-1 text-center">No. Cotizacion</th>
                                                                                <th className="col-sm-1 text-center">Fecha</th>
                                                                                <th className="col-sm-1 text-center">Cliente/Empresa</th>
                                                                                <th className="col-sm-1 text-center">Plaga</th>
                                                                                <th className="col-sm-1 text-center">Monto</th>
                                                                                <th className="col-sm-1 text-center">Estatus</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {dataModal.map(function callback(item, index) {
                                                                                if(item.date == row[0]) {
                                                                                    return dataModal[index].quotations.map(quot => {
                                                                                        return <tr>
                                                                                                    <td>{quot.id_quotation}</td>
                                                                                                    <td>{quot.created_at}</td>
                                                                                                    <td>{quot.customer}<br />{quot.company_customer}</td>
                                                                                                    <td>{quot.key}</td>
                                                                                                    <td>{symbolCountry}{quot.total}</td>
                                                                                                    <td>{quot.status}</td>
                                                                                                </tr>
                                                                                    })
                                                                                }
                                                                            })}
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <div className="col-md-12 table-responsive">
                                                                    <h5>Ventas Brutas</h5>
                                                                    <table className="table tablesorter table-hover" id="tableProduct">
                                                                        <thead>
                                                                            <tr>
                                                                                <th className="col-sm-1 text-center">No. OS</th>
                                                                                <th className="col-sm-1 text-center">Cliente</th>
                                                                                <th className="col-sm-1 text-center">Plaga</th>
                                                                                <th className="col-sm-1 text-center">Monto</th>
                                                                                <th className="col-sm-1 text-center">Tipo de pago</th>
                                                                                <th className="col-sm-1 text-center">Estatus</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {dataModalGs.map(function callback(item, index) {
                                                                                if(item.date == row[0]) {
                                                                                    return dataModalGs[index].events.map(event => {
                                                                                        return <tr>
                                                                                                    <td>{event.order}</td>
                                                                                                    <td>{event.customer}<br />{event.company}</td>
                                                                                                    <td>{event.key}</td>
                                                                                                    <td>{symbolCountry}{event.total}</td>
                                                                                                    <td>{event.payment_method}</td>
                                                                                                    <td>{event.payment == 2 ? 'Pagado' : 'Adeudo'}</td>
                                                                                                </tr>
                                                                                    })
                                                                                }
                                                                            })}
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="col-md-12 table-responsive">
                                                                    <h5>Ingreso Bruto</h5>
                                                                    <table className="table tablesorter table-hover" id="tableProduct">
                                                                        <thead>
                                                                            <tr>
                                                                                <th className="col-sm-1 text-center">No. OS</th>
                                                                                <th className="col-sm-1 text-center">Cliente</th>
                                                                                <th className="col-sm-1 text-center">Plaga</th>
                                                                                <th className="col-sm-1 text-center">Total</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {dataModalGi.map(function callback(item, index) {
                                                                                if(item.date == row[0]) {
                                                                                    return dataModalGi[index].payments.map(payment => {
                                                                                        return <tr>
                                                                                                    <td>{payment.order}</td>
                                                                                                    <td>{payment.customer}<br />{payment.company}</td>
                                                                                                    <td>{payment.key}</td>
                                                                                                    <td>{symbolCountry}{payment.total}</td>
                                                                                                </tr>
                                                                                    })
                                                                                }
                                                                            })}
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <div className="col-md-12 table-responsive">
                                                                    <h5>Gasto Total</h5>
                                                                    <table className="table tablesorter table-hover" id="tableProduct">
                                                                        <thead>
                                                                            <tr>
                                                                                <th className="col-sm-1 text-center">No. Gasto</th>
                                                                                <th className="col-sm-1 text-center">Usuario</th>
                                                                                <th className="col-sm-1 text-center">Concepto</th>
                                                                                <th className="col-sm-1 text-center">Monto</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {dataModal.map(function callback(item, index) {
                                                                                if(item.date == row[0]) {
                                                                                    return dataModal[index].quotations.map(task => {
                                                                                        return <tr>
                                                                                                    <td>{task.id_quotation}</td>
                                                                                                    <td>{task.total}</td>
                                                                                                    <td>{task.price}</td>
                                                                                                    <td>{task.construction_measure}</td>
                                                                                                </tr>
                                                                                    })
                                                                                }
                                                                            })}
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="col-md-12 table-responsive">
                                                                    <h5>Beneficio Bruto</h5>
                                                                    <table className="table tablesorter table-hover" id="tableProduct">
                                                                        <thead>
                                                                            <tr>
                                                                                <th className="col-sm-1 text-center">No. Cotizacion</th>
                                                                                <th className="col-sm-1 text-center">Total</th>
                                                                                <th className="col-sm-1 text-center">Precio</th>
                                                                                <th className="col-sm-1 text-center">Medidas de construccion</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {dataModal.map(function callback(item, index) {
                                                                                if(item.date == row[0]) {
                                                                                    return dataModal[index].quotations.map(task => {
                                                                                        return <tr>
                                                                                                    <td>{task.id_quotation}</td>
                                                                                                    <td>{task.total}</td>
                                                                                                    <td>{task.price}</td>
                                                                                                    <td>{task.construction_measure}</td>
                                                                                                </tr>
                                                                                    })
                                                                                }
                                                                            })}
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <div className="col-md-12 table-responsive">
                                                                    <h5>Índice De Satisfacción</h5>
                                                                    <table className="table tablesorter table-hover" id="tableProduct">
                                                                        <thead>
                                                                            <tr>
                                                                                <th className="col-sm-1 text-center">No. Servicio</th>
                                                                                <th className="col-sm-1 text-center">Técnico</th>
                                                                                <th className="col-sm-1 text-center">Evaluación</th>
                                                                                <th className="col-sm-1 text-center">Comentarios</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {dataModalSi.map(function callback(item, index) {
                                                                                if(item.date == row[0]) {
                                                                                    return dataModalSi[index].ratings.map(rating => {
                                                                                        return <tr>
                                                                                                    <td>{rating.order}</td>
                                                                                                    <td>{rating.employee}</td>
                                                                                                    <td>{rating.rating} <i className="fa fa-star" aria-hidden="true"></i></td>
                                                                                                    <td>{rating.comments}</td>
                                                                                                </tr>
                                                                                    })
                                                                                }
                                                                            })}
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="modal-footer">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{symbolCountry}{row[1]}</td>
                                    <td>{symbolCountry}{row[2]}</td>
                                    <td>{symbolCountry}{row[3]}</td>
                                    <td>{symbolCountry}{row[4]}</td>
                                    <td>{symbolCountry}{row[5]}</td>
                                </tr>
                            ) : ('')
                        ))
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

if (document.getElementById('InitApp')) {
    ReactDOM.render(<InitApp/>, document.getElementById('InitApp'))
}