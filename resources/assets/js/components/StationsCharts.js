import React, {useState, useEffect} from 'react'
import ReactDOM from 'react-dom'
import DatePicker from 'react-multi-date-picker'
import { DatePanel } from "react-multi-date-picker/plugins"
import Axios from 'axios'
import Moment from 'moment';
import {extendMoment} from 'moment-range';
import {Line, Doughnut, Bar} from "react-chartjs-2";
import Select from 'react-select'
import {loaderPestWare} from "../app/adminlte/pestware/loader";

const StationsCharts = () => {
    Moment.locale('es-mx');
    const dateFirst = new Date();
    const company = document.getElementById('companyMonitoring').value;
    const startOfMonth = new Date(dateFirst.getFullYear(), dateFirst.getMonth(), 1);
    const [startDate, setStartDate] = useState(startOfMonth)
    const [endDate, setEndDate] = useState(new Date())
    const [data0, setData0] = useState({})
    const [data1, setData1] = useState({})
    const [data2, setData2] = useState({})
    const [data3, setData3] = useState({})
    const [data4, setData4] = useState({})
    const [data5, setData5] = useState({})
    const [dataRefBaitsLine, setDataRefBaitsLine] = useState({})
    const [dataRefBaitsConditions, setDataRefBaitsConditions] = useState({})
    const [dataRefBaitsConsume, setDataRefBaitsConsume] = useState({})
    const [dataRefCaptureLine, setDataRefCaptureLine] = useState({})
    const [dataRefCaptureConditions, setDataRefCaptureConditions] = useState({})
    const [dataRefCaptureConsume, setDataRefCaptureConsume] = useState({})
    const [dataRefLightLine, setDataRefLightLine] = useState({})
    const [dataRefLightConditions, setDataRefLightConditions] = useState({})
    const [dataRefLightConsume, setDataRefLightConsume] = useState({})
    const [dataRefInsectsLine, setDataRefInsectsLine] = useState({})
    const [dataRefInsectsConditions, setDataRefInsectsConditions] = useState({})
    const [dataRefInsectsConsume, setDataRefInsectsConsume] = useState({})
    const [selectCustomer, setSelectCustomer] = useState(0)
    const [dataSelectCustomer, setDataSelectCustomer] = useState([])
    const [dataPlague0, setDataPlague0] = useState({})
    const [dataPlague1, setDataPlague1] = useState({})
    const [dataPlague2, setDataPlague2] = useState({})
    const [dataPlague3, setDataPlague3] = useState({})
    const [dataPlague4, setDataPlague4] = useState({})
    const [dataPlague5, setDataPlague5] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const [didMount, setDidMount] = useState(true)
    const [dataGroup, setDataGroup] = useState([])
    const [stateGroup, setStateGroup] = useState([])

    const [dataInsects, setDataInsects] = useState({})
    const [dataPlaguesLight, setDataPlaguesLight] = useState({})
    const [dataPlaguesCapture, setDataPlaguesCapture] = useState({})

    let period = `Inspecciones del período ${Moment(startDate).format('DD')} de ${Moment(startDate).format('MMMM')} al ${Moment(endDate).format('DD')} de ${Moment(endDate).format('MMMM')}`;

    const dataBar = {
        labels: ['Exterior', 'Interior', 'Lobby', '4', '5', '6'],
        datasets: [
            {
                label: 'Sin Ingesta',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: 'rgb(255, 99, 132)',
            },
            {
                label: 'Ingesta por Insecto',
                data: [2, 3, 20, 5, 1, 4],
                backgroundColor: 'rgb(54, 162, 235)',
            },
            {
                label: 'Ingesta Parcial',
                data: [3, 10, 13, 15, 22, 30],
                backgroundColor: 'rgb(75, 192, 192)',
            },
            {
                label: 'Ingesta Total',
                data: [3, 10, 13, 15, 22, 30],
                backgroundColor: 'rgb(28, 123, 400)',
            },
        ],
    }

    const optionsBar = {
        scales: {
            yAxes: [
                {
                    stacked: true,
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
            xAxes: [
                {
                    stacked: true,
                },
            ],
        },
    }

    const optionsBaitsLine = {
        scales: {
            yAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: '# de Cebaderos por tipo de consumo'
                    },
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value;
                        },
                        stepSize: 1,
                        min: 0,
                    },
                },
            ],
            xAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: period
                    },
                },
            ],
        },
        title: {
            display: true,
            fontSize: 14,
            text: 'Gráfica de Tendencia de Consumo'
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    let label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += Math.round(tooltipItem.yLabel * 100) / 100 + ' Cebaderos';

                    return label;
                }
            }
        }
    }
    const optionsBaitsDoughnut = {
        title: {
            display: true,
            fontSize: 14,
            text: 'Gráfica de Proporción de Condiciones de Estaciones'
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    let label = data.labels[tooltipItem.index] || '';
                    let percentage = data.datasets[0].data[tooltipItem.index];
                    if (label) {
                        label += ' / ';
                    }
                    label += percentage + '%';

                    return label;
                }
            }
        },
        legend: {
            display: true,
            position: 'right'
        }
    }

    const optionsCaptureLine = {
        scales: {
            yAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: '# de especies capturadas'
                    },
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value;
                        },
                        stepSize: 1,
                        min: 0,
                    },
                },
            ],
            xAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: period
                    },
                },
            ],
        },
        title: {
            display: true,
            fontSize: 14,
            text: 'Gráfica de Tendencia de Estaciones con Captura'
        }
    }

    const optionsLightLine = {
        scales: {
            yAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: '# de especies capturadas'
                    },
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value;
                        },
                        stepSize: 1,
                        min: 0,
                    },
                },
            ],
            xAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: period
                    },
                },
            ],
        },
        title: {
            display: true,
            fontSize: 14,
            text: 'Gráfica de Tendencia de Estaciones con Actividad'
        }
    }

    const optionsBaitsConsumeDoughnut = {
        title: {
            display: true,
            fontSize: 14,
            text: 'Gráfica de Proporción de Consumo'
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    let label = data.labels[tooltipItem.index] || '';
                    let percentage = data.datasets[0].data[tooltipItem.index];
                    if (label) {
                        label += ' / ';
                    }
                    label += percentage + '%';

                    return label;
                }
            }
        },
        legend: {
            display: true,
            position: 'right'
        }
    }

    const optionsInsectsConditionsDoughnut = {
        title: {
            display: true,
            fontSize: 14,
            text: 'Gráfica de Proporción de Condiciones'
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    let label = data.labels[tooltipItem.index] || '';
                    let percentage = data.datasets[0].data[tooltipItem.index];
                    if (label) {
                        label += ' / ';
                    }
                    label += percentage + '%';

                    return label;
                }
            }
        },
        legend: {
            display: true,
            position: 'right'
        }
    }

    const optionsInsectsLine = {
        scales: {
            yAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: 'Grado de infestación'
                    },
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            let grade = 'Nulo';

                            if (value == 25) grade = 'Bajo';
                            else if (value == 50) grade = 'Medio';
                            else if (value == 75) grade = 'Alto';
                            else if (value == 100) grade = 'Muy Alto';
                            return grade + ': ' + value;
                        },
                        stepSize: 25,
                        min: 0,
                    },
                },
            ],
            xAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: period
                    },
                },
            ],
        },
        title: {
            display: true,
            fontSize: 14,
            text: 'Gráfica de Tendencia de Plagas'
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    let label = data.datasets[tooltipItem.datasetIndex].label || '';
                    let grade = 'Nulo';

                    if (tooltipItem.value == 25) grade = 'Bajo';
                    else if (tooltipItem.value == 50) grade = 'Medio';
                    else if (tooltipItem.value == 75) grade = 'Alto';
                    else if (tooltipItem.value == 100) grade = 'Muy Alto';

                    if (label) {
                        label += ': ' + grade + '=';
                    }
                    label += Math.round(tooltipItem.yLabel * 100) / 100;

                    return label;
                }
            }
        }
    }

    useEffect(() => {
        const getInitData = async () => {
            const responseCustomers = await Axios.get('../../station/monitoring/reporting/customers')
            setDataSelectCustomer(responseCustomers.data)
        }
        const moment = extendMoment(Moment);

        if (didMount) {
            getInitData();
            let labelsArray = []
            let dataArrayBaitsNoIntake = []
            let dataArrayBaitsPartialIntake = []
            let dataArrayBaitsTotalIntake = []
            let dataArrayBaitsInsectIntake = []
            let dataArrayCapture = []
            let dataArrayLight = []
            let datasets = []
            let rowsTable = []
            let rowsModal = []
            let dataGroupArray = []
            let dataTitleArray = ['Cebaderos', 'Trampas de Captura', 'Lamparas UV', 'Control de Plagas']
            let dataTitleArrayBaits = ['Sin Ingesta', 'Ingesta por Insecto', 'Ingesta Parcial', 'Ingesta Total']
            let dataPlague = []
            let labelPlague = []
            let colors = []
            let colorsInsects = []
            let range = moment.range(moment(startDate, 'YYYY-MM-DD'), moment(endDate, 'YYYY-MM-DD'))
            let daysArray = Array.from(range.by('day', 'YYYY-MM-DD'))
            let daysArrayFormat = daysArray.map(m => m.format('YYYY-MM-DD'))
            let customerId = 3595;
            setIsLoading(true);
            loaderPestWare('Espere un momento');
            setData0({})
            setData1({})
            setData2({})
            setData3({})
            setData4({})
            setData5({})

            let labelsArrayInsects = []
            let datasetsInsects = []
            let plagues = []
            let grades = []

            let labelsArrayPlaguesLight = [];
            let datasetsPlaguesLight = [];
            let plaguesLight = [];
            let quantityLight = [];

            let labelsArrayPlaguesCapture = [];
            let datasetsPlaguesCapture = [];
            let plaguesCapture = [];
            let quantityCapture = [];

            const getData = async () => {
                const response = await Axios.post(`../../api/js/reporting/stations/monitoring`, {
                    customer_id: selectCustomer,
                    company_id: parseInt(company),
                    initial_date: Moment(startDate).format("YYYY-MM-DD"),
                    final_date: Moment(endDate).format("YYYY-MM-DD"),
                })

                colors = [
                    '#229954',
                    '#2980B9',
                    '#F39C12',
                    '#A93226'
                ]

                colorsInsects = [
                    '#742774',
                    '#2E86C1',
                    '#BA4A00',
                    '#229954',
                    '#D4AC0D',
                    '#2E4053',
                    '#2E4053',
                    '#A93226'
                ]

                dataGroupArray.push([dataTitleArray[0], response.data.bait_total, '#quotation-chart', true, 'fa fa-archive'])
                dataGroupArray.push([dataTitleArray[1], response.data.capture_total, '#sales-chart', false, 'fa fa-cube'])
                dataGroupArray.push([dataTitleArray[2], response.data.light_total, '#income-chart', false, 'fa fa-lightbulb-o'])
                dataGroupArray.push([dataTitleArray[3], '', '#insects-chart', false, 'fa fa-line-chart'])
                setStateGroup(dataGroupArray)

                // Section insects
                if (response.data.datasets.length > 0) {
                    response.data.datasets[0].plagues.forEach((plague, i) => {
                        plagues.push(plague)
                        response.data.datasets.forEach((service) => {
                            if (service.plagues.length > 0) {
                                i == 0 ? labelsArrayInsects.push(service.date) : null
                                if (service.plagues[i] !== undefined) {
                                    let grade = 100
                                    if (service.plagues[i].grade == "Nulo") {
                                        grade = 0
                                    } else if (service.plagues[i].grade == "Bajo") {
                                        grade = 25
                                    } else if (service.plagues[i].grade == "Medio") {
                                        grade = 50
                                    } else if (service.plagues[i].grade == "Alto") {
                                        grade = 75
                                    }
                                    grades.push(grade)
                                } else grades.push(0)
                            }
                        })
                        let obj = {
                            label: plague.plague,
                            data: grades,
                            fill: false,
                            backgroundColor: colorsInsects[i],
                            borderColor: colorsInsects[i]
                        }
                        datasetsInsects.push(obj)
                        grades = []
                    })
                    let dataSetObjInsects = {
                        labels: labelsArrayInsects,
                        datasets: datasetsInsects
                    }
                    setDataInsects(dataSetObjInsects)
                }
                // End section insects

                // Section station lights
                if (response.data.inspections.length > 0) {
                    response.data.inspections[0].light_stations.forEach((plague, i) => {
                        plaguesLight.push(plague)
                        response.data.inspections.forEach((inspection) => {
                            if (inspection.light_stations.length > 0) {
                                i == 0 ? labelsArrayPlaguesLight.push(inspection.date) : null
                                if (inspection.light_stations[i] !== undefined) {
                                    quantityLight.push(inspection.light_stations[i].quantity)
                                } else quantityLight.push(0);
                            }
                        })
                        let obj = {
                            label: plague.key,
                            data: quantityLight,
                            fill: false,
                            backgroundColor: colorsInsects[i],
                            borderColor: colorsInsects[i]
                        }
                        datasetsPlaguesLight.push(obj)
                        quantityLight = []
                    })
                    let dataSetObjPlaguesLight = {
                        labels: labelsArrayPlaguesLight,
                        datasets: datasetsPlaguesLight
                    }
                    setDataPlaguesLight(dataSetObjPlaguesLight)
                }
                // End section lights

                // Section station capture
                if (response.data.inspections.length > 0) {
                    response.data.inspections[0].capture_stations.forEach((plague, i) => {
                        plaguesCapture.push(plague)
                        response.data.inspections.forEach((inspection) => {
                            if (inspection.capture_stations.length > 0) {
                                i == 0 ? labelsArrayPlaguesCapture.push(inspection.date) : null
                                if (inspection.capture_stations[i] !== undefined) {
                                    quantityCapture.push(inspection.capture_stations[i].quantity)
                                } else quantityCapture.push(0);
                            }
                        })
                        let obj = {
                            label: plague.key,
                            data: quantityCapture,
                            fill: false,
                            backgroundColor: colorsInsects[i],
                            borderColor: colorsInsects[i]
                        }
                        datasetsPlaguesCapture.push(obj)
                        quantityCapture = []
                    })
                    let dataSetObjPlaguesCapture = {
                        labels: labelsArrayPlaguesCapture,
                        datasets: datasetsPlaguesCapture
                    }
                    setDataPlaguesCapture(dataSetObjPlaguesCapture)
                }
                // End section capture

                let labelsArrayTemp = []
                let rowsTableTemp = []
                response.data.inspections.forEach((element) => {

                    labelsArrayTemp.push(element.date)

                    //dataArrayTempCapture.push(element.capture_stations)
                    //dataArrayTempLight.push(element.light_stations)

                    //rowsTableTemp.push([element.date, element.bait_stations])

                    labelsArray.push(labelsArrayTemp)
                    dataArrayBaitsNoIntake.push(element.noIntakeBaitResponses)
                    dataArrayBaitsPartialIntake.push(element.partialIntakeBaitResponses)
                    dataArrayBaitsTotalIntake.push(element.totalIntakeBaitResponses)
                    dataArrayBaitsInsectIntake.push(element.insectIntakeBaitResponses)

                    //dataArrayCapture.push(dataArrayTempCapture)
                    //dataArrayLight.push(dataArrayTempLight)
                    rowsTable.push(rowsTableTemp)
                    rowsModal.push(element);

                })

                let objNoIntakeBait = {
                    label: dataTitleArrayBaits[0],
                    data: dataArrayBaitsNoIntake,
                    fill: false,
                    backgroundColor: colors[0],
                    borderColor: colors[0]
                }

                let objInsectIntakeBait = {
                    label: dataTitleArrayBaits[1],
                    data: dataArrayBaitsInsectIntake,
                    fill: false,
                    backgroundColor: colors[1],
                    borderColor: colors[1]
                }

                let objPartialIntakeBait = {
                    label: dataTitleArrayBaits[2],
                    data: dataArrayBaitsPartialIntake,
                    fill: false,
                    backgroundColor: colors[2],
                    borderColor: colors[2]
                }

                let objTotalIntakeBait = {
                    label: dataTitleArrayBaits[3],
                    data: dataArrayBaitsTotalIntake,
                    fill: false,
                    backgroundColor: colors[3],
                    borderColor: colors[3]
                }

                datasets.push(objNoIntakeBait)
                datasets.push(objPartialIntakeBait)
                datasets.push(objTotalIntakeBait)
                datasets.push(objInsectIntakeBait)

                let dataSetObj = {
                    labels: labelsArray[0],
                    datasets: datasets
                }
                setData0(dataSetObj)

                dataSetObj = {
                    labels: labelsArray[1],
                    datasets: [{
                        label: dataTitleArray[1],
                        data: dataArrayCapture[0],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData1(dataSetObj)

                dataSetObj = {
                    labels: labelsArray[2],
                    datasets: [{
                        label: dataTitleArray[2],
                        data: dataArrayLight[0],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData2(dataSetObj)

                dataSetObj = {
                    labels: labelsArray[3],
                    datasets: [{
                        label: dataTitleArray[3],
                        data: dataArrayLight[0],
                        fill: true,
                        backgroundColor: "rgba(75,192,192,0.2)",
                        borderColor: "rgba(75,192,192,1)"
                    }]
                }
                setData3(dataSetObj)

                let labelPlagueTemp = []
                let dataPlagueTemp = []
                let percentage = 0
                let total = response.data.conditionsBaitTotal
                response.data.bait_conditions.forEach((element) => {
                    labelPlagueTemp.push(element.key + ': ' + element.total)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))
                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                percentage = 0
                total = response.data.conditionsBaitTotal
                response.data.consumeBait.forEach((element) => {
                    labelPlagueTemp.push(element.key + ': ' + element.total)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))

                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                percentage = 0
                total = response.data.conditionsCaptureTotal
                response.data.capture_conditions.forEach((element) => {
                    labelPlagueTemp.push(element.key + ': ' + element.total)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))

                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                percentage = 0
                total = response.data.conditionsLightTotal
                response.data.light_conditions.forEach((element) => {
                    labelPlagueTemp.push(element.key + ': ' + element.total)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))
                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                labelPlagueTemp = []
                dataPlagueTemp = []
                percentage = 0
                total = response.data.conditionsServicesTotal
                response.data.conditionsServices.forEach((element) => {
                    labelPlagueTemp.push(element.key + ': ' + element.total)
                    percentage = (element.total / total) * 100
                    dataPlagueTemp.push(percentage.toFixed(2))
                })
                labelPlague.push(labelPlagueTemp)
                dataPlague.push(dataPlagueTemp)

                let dataSetPlagueObj = {
                    labels: labelPlague[0],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: [
                            '#C0392B',
                            '#F1C40F',
                            '#17A589',
                            '#3498DB',
                            '#9B59B6',
                            '#2C3E50',
                            '#7F8C8D',
                            '#F39C12',
                            '#27AE60',
                            '#2980B9'
                        ],
                        hoverBackgroundColor: [
                            '#E6B0AA',
                            '#FCF3CF',
                            '#D0ECE7',
                            '#D6EAF8',
                            '#EBDEF0'
                        ],
                        data: dataPlague[0]
                    }]
                }
                setDataPlague0(dataSetPlagueObj)

                dataSetPlagueObj = {
                    labels: labelPlague[1],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: [
                            '#F39C12',
                            '#A93226',
                            '#229954',
                            '#2980B9'
                        ],
                        hoverBackgroundColor: [
                            '#E6B0AA',
                            '#FCF3CF',
                            '#D0ECE7',
                            '#D6EAF8',
                            '#EBDEF0'
                        ],
                        data: dataPlague[1]
                    }]
                }
                setDataPlague1(dataSetPlagueObj)

                dataSetPlagueObj = {
                    labels: labelPlague[2],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: [
                            '#C0392B',
                            '#F1C40F',
                            '#17A589',
                            '#3498DB',
                            '#9B59B6',
                            '#2C3E50',
                            '#7F8C8D',
                            '#F39C12',
                            '#27AE60',
                            '#2980B9'
                        ],
                        hoverBackgroundColor: [
                            '#E6B0AA',
                            '#FCF3CF',
                            '#D0ECE7',
                            '#D6EAF8',
                            '#EBDEF0'
                        ],
                        data: dataPlague[2]
                    }]
                }
                setDataPlague2(dataSetPlagueObj)

                dataSetPlagueObj = {
                    labels: labelPlague[4],
                    datasets: [{
                        label: 'Rainfall',
                        backgroundColor: [
                            '#C0392B',
                            '#F1C40F',
                            '#17A589',
                            '#3498DB',
                            '#9B59B6',
                            '#2C3E50',
                            '#7F8C8D',
                            '#F39C12',
                            '#27AE60',
                            '#2980B9'
                        ],
                        hoverBackgroundColor: [
                            '#E6B0AA',
                            '#FCF3CF',
                            '#D0ECE7',
                            '#D6EAF8',
                            '#EBDEF0'
                        ],
                        data: dataPlague[4]
                    }]
                }
                setDataPlague5(dataSetPlagueObj)

                Swal.close();

            }
            getData()
            setIsLoading(false);
        } else {
            setDidMount(false)
            getInitData()
        }
    }, [startDate, endDate, selectCustomer]);

    async function shoot() {
        loaderPestWare('Generando reporte...');
        let imageBaitsLine = dataRefBaitsLine.chartInstance.toBase64Image()
        let imageBaitsConditions = dataRefBaitsConditions.chartInstance.toBase64Image()
        let imageBaitsConsume = dataRefBaitsConsume.chartInstance.toBase64Image()

        let imageCaptureLine = dataRefCaptureLine.chartInstance.toBase64Image()
        let imageCaptureConditions = dataRefCaptureConditions.chartInstance.toBase64Image()

        let imageLightLine = dataRefLightLine.chartInstance.toBase64Image()
        let imageLightConditions = dataRefLightConditions.chartInstance.toBase64Image()

        let imageInsectsLine = dataRefInsectsLine.chartInstance.toBase64Image()
        let imageInsectsConditions = dataRefInsectsConditions.chartInstance.toBase64Image()

        const response = await Axios.post(`../../api/js/reporting/upload/chart/image`, {
            chart_baits_line: imageBaitsLine,
            chart_baits_conditions: imageBaitsConditions,
            chart_baits_consume: imageBaitsConsume,
            chart_capture_line: imageCaptureLine,
            chart_capture_conditions: imageCaptureConditions,
            chart_light_line: imageLightLine,
            chart_light_conditions: imageLightConditions,
            chart_insects_line: imageInsectsLine,
            chart_insects_conditions: imageInsectsConditions,
        }).then((response) => {
            Swal.close();
            window.open(`../../api/js/reporting/report/stations/all/${selectCustomer}/${response.data}/${period}`, '_blank');
        }, (error) => {
            Swal.close();
            showToast('error', 'Algo salio mal', 'Intente de nuevo.');
        });
    }


    return (
        <div className="container-fluid">

            <div className="row text-left" style={{marginTop: "10px"}}>
                <div className="col-md-6">
                    <div className="form-group" id="periodIntelligenceDiv">
                        <label><strong>Seleccione un Período</strong></label>
                        <div id="InitCalendar">
                            <DatePicker
                                value={[startDate, endDate]}
                                onChange={ dates => {
                                    if (dates.length == 2) {
                                        setStartDate(dates[0].format())
                                        setEndDate(dates[1].format())
                                    }
                                }}
                                range={true}
                                plugins={[
                                    <DatePanel />
                                ]}
                                type="button"
                            />
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="form-group" id="centerJobIntelligenceDiv">
                        <label><strong>Cliente</strong></label>
                        <Select
                            options={
                                dataSelectCustomer.map((customer) => (
                                    { value: customer.id, label: customer.name }
                                ))
                            }
                            onChange={event => setSelectCustomer(event.value)}
                        />
                    </div>
                </div>
                <div className="col-md-2">
                    <label><strong>Exportar PDF</strong></label>
                    <button
                        className="btn btn-primary btn-md"
                        onClick={shoot}
                    >
                        Descargar Reportes PDF
                    </button>
                    <br/>
                </div>
            </div>

            <div className="row">
                <div className="col-md-12">
                    <div className="card card-primary">
                        <div className="card-header">
                            <i className="fa fa-pie-chart" aria-hidden="true" style={{color: "white"}}></i><span className="card-title text-white h4"> Reporte Gráfico de Monitoreo de Estaciones y Control de Plagas </span>
                        </div>
                        <div className="card-body">
                            <div className="chart">
                                {isLoading ? (
                                    <div>Loading ...</div>
                                ) : (
                                    <div className="nav-tabs-custom">
                                        <ul className="nav nav-tabs pull-left">
                                            {stateGroup.map((t, i) => (
                                                <li className={i === 0 ? 'active' : ''}>
                                                    <a href={t[2]} data-toggle="tab">
                                                        <h4><i className={t[4]} style={{color: "#1E8CC7"}}/> {t[0]} {i !== 3 ? `(${t[1]})` : ''}</h4>
                                                    </a>
                                                </li>
                                            ))}
                                        </ul>
                                        <div className="tab-content no-padding">
                                            <div className="chart tab-pane active" id="quotation-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={data0}
                                                              options={optionsBaitsLine}
                                                              ref={(ref) => setDataRefBaitsLine(ref)} />
                                                    </div>
                                                    <div className="col-md-4">
                                                        <div className="row">
                                                            <Doughnut data={dataPlague0}
                                                                      options={optionsBaitsDoughnut}
                                                                      ref={(ref) => setDataRefBaitsConditions(ref)}
                                                            />
                                                        </div>
                                                        <br/><br/>
                                                        <div className="row">
                                                            <Doughnut data={dataPlague1}
                                                                      options={optionsBaitsConsumeDoughnut} ref={(ref) => setDataRefBaitsConsume(ref)}/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="chart tab-pane" id="sales-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={dataPlaguesCapture}
                                                              options={optionsCaptureLine}
                                                              ref={(ref) => setDataRefCaptureLine(ref)}
                                                        />
                                                    </div>
                                                    <div className="col-md-4 table-responsive">
                                                        <Doughnut data={dataPlague1}
                                                                  options={optionsBaitsDoughnut}
                                                                  ref={(ref) => setDataRefCaptureConditions(ref)}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="chart tab-pane" id="income-chart">
                                                <div className="row">
                                                    <div className="col-md-8 table-responsive">
                                                        <Line data={dataPlaguesLight}
                                                              options={optionsLightLine}
                                                              ref={(ref) => setDataRefLightLine(ref)}
                                                        />
                                                    </div>
                                                    <div className="col-md-4 table-responsive">
                                                        <Doughnut data={dataPlague2}
                                                                  options={optionsBaitsDoughnut}
                                                                  ref={(ref) => setDataRefLightConditions(ref)}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="chart tab-pane" id="insects-chart">
                                                <div className="row">
                                                    <div className="col-md-8">
                                                        <Line data={dataInsects}
                                                              options={optionsInsectsLine}
                                                              ref={(ref) => setDataRefInsectsLine(ref)}
                                                        />
                                                    </div>
                                                    <div className="col-md-4 table-responsive">
                                                        <Doughnut data={dataPlague5}
                                                                  options={optionsInsectsConditionsDoughnut}
                                                                  ref={(ref) => setDataRefInsectsConditions(ref)}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

if (document.getElementById('StationsCharts')) {
    ReactDOM.render(<StationsCharts/>, document.getElementById('StationsCharts'))
}
