let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.react('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

//mix.js('resources/assets/js/app/**//*.js', 'public/js/build/build.js');

// Auth module
mix.js('resources/assets/js/app/adminlte/auth/*.js', 'public/js/build/authentication.js');
mix.js('resources/assets/js/app/adminlte/authInternal/*.js', 'public/js/build/authenticationInternal.js');

// Password module
mix.js('resources/assets/js/app/adminlte/password/*.js', 'public/js/build/reset.js');

// Quotation, Recurrents and Reinforcements module
mix.js('resources/assets/js/app/adminlte/quotation/create/*.js', 'public/js/build/quotation.js');
mix.js('resources/assets/js/app/adminlte/quotation/edit/*.js', 'public/js/build/updateQuote.js');
mix.js('resources/assets/js/app/adminlte/recurrents/*.js', 'public/js/build/recurrent.js');
mix.js('resources/assets/js/app/adminlte/reinforcements/*.js', 'public/js/build/reinforcement.js');
mix.js('resources/assets/js/app/adminlte/quotation/create/_cancel.js', 'public/js/build/quotation_cancel.js');

// Payments module
mix.js('resources/assets/js/app/adminlte/cashes/*.js', 'public/js/build/payment.js');

// Catalogs module
mix.js('resources/assets/js/app/adminlte/catalogs/*.js', 'public/js/build/catalog.js');

// Custom Schedule module
mix.js('resources/assets/js/app/adminlte/customschedule/*.js', 'public/js/build/customSchedule.js');

// Cuts module
mix.js('resources/assets/js/app/adminlte/cuts/*.js', 'public/js/build/cut.js');

// Employees module
mix.js('resources/assets/js/app/adminlte/employees/*.js', 'public/js/build/employee.js');

// Entrys module
mix.js('resources/assets/js/app/adminlte/entrys/*.js', 'public/js/build/entry.js');

// Expense module
mix.js('resources/assets/js/app/adminlte/expense/*.js', 'public/js/build/expense.js');

// Purchase module
mix.js('resources/assets/js/app/adminlte/purchase/*.js', 'public/js/build/purchase.js');

// Job Center module
mix.js('resources/assets/js/app/adminlte/jobcenter/*.js', 'public/js/build/jobcenter.js');

// Job Profile module
mix.js('resources/assets/js/app/adminlte/jobtitleprofile/*.js', 'public/js/build/jobtitleprofile.js');

// PestWare menu module
mix.js('resources/assets/js/app/adminlte/pestware/menu.js', 'public/js/build/menu.js');

// Profile module
mix.js('resources/assets/js/app/adminlte/profile/*.js', 'public/js/build/profile.js');

// Service Cancel module
mix.js('resources/assets/js/app/adminlte/services/cancel/*.js', 'public/js/build/serviceCancel.js');

// Service Schedule Schedule module
mix.js('resources/assets/js/app/adminlte/services/schedule/*.js', 'public/js/build/schedule.js');

// Service Update Schedule module
mix.js('resources/assets/js/app/adminlte/services/editSchedule/*.js', 'public/js/build/serviceEditSchedule.js');

// Service Calendar module
mix.js('resources/assets/js/app/adminlte/services/calendar/*.js', 'public/js/build/calendar.js');

// Transfer cc module
mix.js('resources/assets/js/app/adminlte/transfers/create/createCC.js', 'public/js/build/transferCreateCC.js');

// Transfer ce module
mix.js('resources/assets/js/app/adminlte/transfers/create/createCE.js', 'public/js/build/transferCreateCE.js');

// Transfer cem module
mix.js('resources/assets/js/app/adminlte/transfers/create/createCEM.js', 'public/js/build/transferCreateCEM.js');

// Transfer cs module
mix.js('resources/assets/js/app/adminlte/transfers/create/createCS.js', 'public/js/build/transferCreateCS.js');

// Transfer ec module
mix.js('resources/assets/js/app/adminlte/transfers/create/createEC.js', 'public/js/build/transferCreateEC.js');

// Transfer ee module
mix.js('resources/assets/js/app/adminlte/transfers/create/createEE.js', 'public/js/build/transferCreateEE.js');

// Transfer tabs module
mix.js('resources/assets/js/app/adminlte/transfers/tabs/*.js', 'public/js/build/transferTabs.js');

// Inventory module
mix.js('resources/assets/js/app/adminlte/inventory/*.js', 'public/js/build/inventory.js');

// Station Monitoring module
mix.js('resources/assets/js/app/adminlte/stationmonitoring/*.js', 'public/js/build/stationMonitoring.js');
mix.js('resources/assets/js/app/adminlte/stationmonitoring/inspections/*.js', 'public/js/build/stationMonitoring/inspections.js');

// Area Station Monitoring module Client
mix.js('resources/assets/js/app/adminlte/areamonitoring/*.js', 'public/js/build/areaStationMonitoring.js');
mix.js('resources/assets/js/app/adminlte/areamonitoring/inspections/*.js', 'public/js/build/areaInspections.js');

// Station Monitoring module
mix.js('resources/assets/js/app/adminlte/instance/*.js', 'public/js/build/instance.js');

// Movements Private Intro
mix.js('resources/assets/js/app/adminlte/introJsMovements/*.js', 'public/js/build/introJsMovements.js');

// Update calendar events
mix.js('resources/assets/js/app/adminlte/services/calendar/update/updateEvents.js', 'public/js/build/updateEvents.js');

// PricesList
mix.js('resources/assets/js/app/adminlte/priceLists/*.js', 'public/js/build/priceLists.js');

// Module Customers
mix.js('resources/assets/js/app/adminlte/customers/*.js', 'public/js/build/customer.js');

// Module Historical Customer
mix.js('resources/assets/js/app/adminlte/customerhistorical/*.js', 'public/js/build/customerhistorical.js');

// Module Accounting
mix.js('resources/assets/js/app/adminlte/accounting/*.js', 'public/js/build/accounting.js');

// Module Billing
mix.js('resources/assets/js/app/adminlte/billing/paymentForm.js', 'public/js/build/billing.js');

// Module Billing
mix.js('resources/assets/js/app/adminlte/billing/paymentFormCheckout.js', 'public/js/build/billingCheckout.js');

// Module Products
mix.js('resources/assets/js/app/adminlte/products/*.js', 'public/js/build/products.js');

// Module Sales
mix.js('resources/assets/js/app/adminlte/sales/*.js', 'public/js/build/sales.js');

// Module Invoices
mix.js('resources/assets/js/app/adminlte/invoices/*.js', 'public/js/build/invoices.js');
mix.js('resources/assets/js/app/adminlte/myaccount/*.js', 'public/js/build/myaccount.js');

// Giveaway
mix.js('resources/assets/js/app/adminlte/giveaway/*.js', 'public/js/build/giveaway.js');

// Management General
mix.js('resources/assets/js/app/adminlte/management/*.js', 'public/js/build/management.js');

//Index Inspection
mix.js('resources/assets/js/app/adminlte/inspection/*.js', 'public/js/build/inspection.js');

//Index Tracking gps
mix.js('resources/assets/js/app/adminlte/gps/*.js', 'public/js/build/gps.js');

//Index Attendances
mix.js('resources/assets/js/app/adminlte/attendance/*.js', 'public/js/build/attendance.js');

