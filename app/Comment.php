<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 * @package App
 * @property int $id
 * @property int $id_course
 * @property int $id_user
 * @property string $comments
 */


class Comment extends Model
{
    //
    protected $table = 'comments';

    protected $fillable = ['id_course','id_user','comments'];
}
