<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\entry_product
 *
 * @property int $id
 * @property int $id_entry
 * @property int $id_product
 * @property string $units
 * @property float $price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|entry_product newModelQuery()
 * @method static Builder|entry_product newQuery()
 * @method static Builder|entry_product query()
 * @method static Builder|entry_product whereCreatedAt($value)
 * @method static Builder|entry_product whereId($value)
 * @method static Builder|entry_product whereIdEntry($value)
 * @method static Builder|entry_product whereIdProduct($value)
 * @method static Builder|entry_product wherePrice($value)
 * @method static Builder|entry_product whereUnits($value)
 * @method static Builder|entry_product whereUpdatedAt($value)
 * @mixin Eloquent
 */
class entry_product extends Model
{
    //
    /**
     * @var string
     */
    protected $table = "entry_products";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_entry',
        'id_product',
        'units',
        'price'
    ];
}
