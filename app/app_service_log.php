<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class application_method
 *
 * @package App
 * @author Alberto Martínez
 * @version 17/08/2020
 * @property int $id
 * @property int $employee_id
 * @property int $company_id
 * @property int $event_id
 * @property string $json
 * @property string $error_message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|app_service_log newModelQuery()
 * @method static Builder|app_service_log newQuery()
 * @method static Builder|app_service_log query()
 * @method static Builder|app_service_log whereCompanyId($value)
 * @method static Builder|app_service_log whereCreatedAt($value)
 * @method static Builder|app_service_log whereEmployeeId($value)
 * @method static Builder|app_service_log whereErrorMessage($value)
 * @method static Builder|app_service_log whereEventId($value)
 * @method static Builder|app_service_log whereId($value)
 * @method static Builder|app_service_log whereJson($value)
 * @method static Builder|app_service_log whereUpdatedAt($value)
 * @mixin Eloquent
 */
class app_service_log extends Model
{
    /**
     * @var string
     */
    protected $table = "app_services_log";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'employee_id',
        'company_id',
        'event_id',
        'json',
        'error_message'
    ];
}
