<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\expense
 *
 * @property int $id
 * @property string $id_expense
 * @property string $expense_name
 * @property int $id_concept
 * @property string $date_expense
 * @property int $id_user
 * @property string|null $description_expense
 * @property float $total
 * @property int $status
 * @property int $id_payment_way
 * @property int|null $credit_days
 * @property int $id_payment_method
 * @property int $account_status
 * @property int $id_voucher
 * @property int $id_jobcenter
 * @property int $companie
 * @property string|null $date_cut
 * @property int|null $cut
 * @property int|null $id_cut
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|expense newModelQuery()
 * @method static Builder|expense newQuery()
 * @method static Builder|expense query()
 * @method static Builder|expense whereAccountStatus($value)
 * @method static Builder|expense whereCompanie($value)
 * @method static Builder|expense whereCreatedAt($value)
 * @method static Builder|expense whereCreditDays($value)
 * @method static Builder|expense whereCut($value)
 * @method static Builder|expense whereDateCut($value)
 * @method static Builder|expense whereDateExpense($value)
 * @method static Builder|expense whereDescriptionExpense($value)
 * @method static Builder|expense whereExpenseName($value)
 * @method static Builder|expense whereId($value)
 * @method static Builder|expense whereIdConcept($value)
 * @method static Builder|expense whereIdExpense($value)
 * @method static Builder|expense whereIdJobcenter($value)
 * @method static Builder|expense whereIdPaymentMethod($value)
 * @method static Builder|expense whereIdPaymentWay($value)
 * @method static Builder|expense whereIdUser($value)
 * @method static Builder|expense whereIdVoucher($value)
 * @method static Builder|expense whereStatus($value)
 * @method static Builder|expense whereTotal($value)
 * @method static Builder|expense whereUpdatedAt($value)
 * @mixin Eloquent
 */
class expense extends Model
{
    /**
     * @var string
     */
    protected $table = "expenses";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_expense',
        'expense_name',
        'id_concept',
        'date_expense',
        'id_user',
        'description_expense',
        'total',
        'status',
        'id_payment_way',
        'id_payment_method',
        'account_status',
        'id_voucher',
        'id_jobcenter',
        'companie',
        'id_cut',
        'visible'
    ];

}
