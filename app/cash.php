<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class cash
 *
 * @package App
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @property int $id
 * @property int $id_service_order
 * @property int $id_event
 * @property int|null $id_payment_method
 * @property int|null $id_payment_way
 * @property int $companie
 * @property string|null $amount_received
 * @property string|null $commentary
 * @property int|null $payment
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $visible
 * @property-read Collection|cash_picture[] $cash_pictures
 * @property-read int|null $cash_pictures_count
 * @method static Builder|cash newModelQuery()
 * @method static Builder|cash newQuery()
 * @method static Builder|cash query()
 * @method static Builder|cash whereAmountReceived($value)
 * @method static Builder|cash whereCommentary($value)
 * @method static Builder|cash whereCompanie($value)
 * @method static Builder|cash whereCreatedAt($value)
 * @method static Builder|cash whereId($value)
 * @method static Builder|cash whereIdEvent($value)
 * @method static Builder|cash whereIdPaymentMethod($value)
 * @method static Builder|cash whereIdPaymentWay($value)
 * @method static Builder|cash whereIdServiceOrder($value)
 * @method static Builder|cash wherePayment($value)
 * @method static Builder|cash whereUpdatedAt($value)
 * @mixin Eloquent
 */
class cash extends Model
{
    /**
     * @var string
     */
    protected $table = "cashes";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_service_order',
        'id_event',
        'id_payment_method',
        'id_payment_way',
        'amount_received',
        'commentary',
        'payment',
        'companie',
        'updated_at',
        'visible'
    ];

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    * Función para relación uno a muchos con tabla cash_pictures
    */
    public function cash_pictures()
    {
        return $this->hasMany(cash_picture::class);
    }
}
