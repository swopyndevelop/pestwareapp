<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class contact
 *
 * @package App
 * @author Olga Rodríguez
 * @version 13/03/2019
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property int|null $job_title_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|contact newModelQuery()
 * @method static Builder|contact newQuery()
 * @method static Builder|contact query()
 * @method static Builder|contact whereCreatedAt($value)
 * @method static Builder|contact whereEmail($value)
 * @method static Builder|contact whereId($value)
 * @method static Builder|contact whereJobTitleId($value)
 * @method static Builder|contact whereName($value)
 * @method static Builder|contact wherePhone($value)
 * @method static Builder|contact whereUpdatedAt($value)
 * @mixin Eloquent
 */
class contact extends Model
{
    /**
     * @var string
     */
    protected $table = "contacts";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'phone',
        'email',
        'job_title_id'
    ];
}
