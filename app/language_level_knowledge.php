<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class language_level_knowledge
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @method static Builder|language_level_knowledge newModelQuery()
 * @method static Builder|language_level_knowledge newQuery()
 * @method static Builder|language_level_knowledge query()
 * @mixin Eloquent
 */

class language_level_knowledge extends Model
{
    /**
     * @var string
     */
    protected $table = "language_level_knowledges";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'value'
    ];
}
