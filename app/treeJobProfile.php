<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class treeJobProfile
 *
 * @package App
 * @author Olga Rodríguez
 * @version 11/02/2019
 * @property int $id_inc
 * @property int $id
 * @property int $company_id
 * @property string $parent
 * @property string $text
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|treeJobProfile newModelQuery()
 * @method static Builder|treeJobProfile newQuery()
 * @method static Builder|treeJobProfile query()
 * @method static Builder|treeJobProfile whereCompanyId($value)
 * @method static Builder|treeJobProfile whereCreatedAt($value)
 * @method static Builder|treeJobProfile whereId($value)
 * @method static Builder|treeJobProfile whereIdInc($value)
 * @method static Builder|treeJobProfile whereParent($value)
 * @method static Builder|treeJobProfile whereText($value)
 * @method static Builder|treeJobProfile whereUpdatedAt($value)
 * @mixin Eloquent
 */

class treeJobProfile extends Model
{
    /**
     * @var string
     */
    protected $table = 'tree_job_profiles';
	protected $primarykey='id_inc';

	/**
     * @var array
     */
	protected $fillable = ['id_inc','id','company_id','parent','text'];
}
