<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class mascot
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|service_order[] $service_order
 * @property-read int|null $service_order_count
 * @method static Builder|mascot newModelQuery()
 * @method static Builder|mascot newQuery()
 * @method static Builder|mascot query()
 * @method static Builder|mascot whereCreatedAt($value)
 * @method static Builder|mascot whereId($value)
 * @method static Builder|mascot whereName($value)
 * @method static Builder|mascot whereUpdatedAt($value)
 * @mixin Eloquent
 */
class mascot extends Model
{
    /**
     * @var string
     */
    protected $table = "mascots";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];

    /**
    * @author Olga Rodríguez
    * @version 12/01/2019
    * Función para relación muchos a muchos con tabla customer_datas.
    */
    public function service_order()
    {
        return $this->belongsToMany(service_order::class);
    }
}
