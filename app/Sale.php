<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Sale
 * @package App
 * @property int $id
 * @property string $id_sale
 * @property int $id_customer
 * @property int $id_user
 * @property int $id_payment_way
 * @property int $id_payment_method
 * @property int $id_voucher
 * @property int $profile_job_center_id
 * @property int $id_company
 * @property int $id_status
 * @property string $visible
 * @property string|null $url_file_voucher
 * @property string $date
 * @property double $unit_price
 * @property double $subtotal
 * @property double $total
 * @property string $description
 * @property int $id_storehouse
 * @mixin Eloquent
 */
class Sale extends Model
{
    /**
     * @var string
     */
    protected $table = "sales";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_sale',
        'id_customer',
        'id_user',
        'id_payment_way',
        'id_payment_method',
        'id_voucher',
        'profile_job_center_id',
        'id_company',
        'id_status',
        'visible',
        'url_file_voucher',
        'date',
        'unit_price',
        'subtotal',
        'total',
        'description',
        'id_storehouse'
    ];
}
