<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Log_request
 *
 * @package App
 * @author Manuel Mendoza
 * @version 03/18/2021
 * @property int $id
 * @property int|null $id_user
 * @property int|null $id_profile_job_center
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin Eloquent
 */

class JobCenterSession extends Model
{
    protected $table = 'job_center_session';
    protected $fillable = ['id_user','id_profile_job_center'];
}
