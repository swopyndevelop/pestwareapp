<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\PlagueList
 *
 * @property int $id
 * @property int $price_list_id
 * @property int $plague_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|PlagueList newModelQuery()
 * @method static Builder|PlagueList newQuery()
 * @method static Builder|PlagueList query()
 * @method static Builder|PlagueList whereCreatedAt($value)
 * @method static Builder|PlagueList whereId($value)
 * @method static Builder|PlagueList wherePlagueId($value)
 * @method static Builder|PlagueList wherePriceListId($value)
 * @method static Builder|PlagueList whereUpdatedAt($value)
 * @mixin Eloquent
 */
class PlagueList extends Model
{
    /**
     * @var string
     */
    protected $table = "plagues_list";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'price_list_id',
        'plague_id'
    ];
}
