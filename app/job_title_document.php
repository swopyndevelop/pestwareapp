<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_document
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_documents_id
 * @property int $job_title_profiles_id
 * @property string|null $document_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_document newModelQuery()
 * @method static Builder|job_title_document newQuery()
 * @method static Builder|job_title_document query()
 * @method static Builder|job_title_document whereCreatedAt($value)
 * @method static Builder|job_title_document whereDocumentName($value)
 * @method static Builder|job_title_document whereId($value)
 * @method static Builder|job_title_document whereJobTitleDocumentsId($value)
 * @method static Builder|job_title_document whereJobTitleProfilesId($value)
 * @method static Builder|job_title_document whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_document extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_documents";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_documents_id',
        'job_title_profiles_id',
        'document_name'
    ];
}
