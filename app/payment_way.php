<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class payment_way
 *
 * @package App
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $id_company
 * @property int|null $credit_days
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|payment_way newModelQuery()
 * @method static Builder|payment_way newQuery()
 * @method static Builder|payment_way query()
 * @method static Builder|payment_way whereCreatedAt($value)
 * @method static Builder|payment_way whereId($value)
 * @method static Builder|payment_way whereName($value)
 * @method static Builder|payment_way whereUpdatedAt($value)
 * @mixin Eloquent
 */

class payment_way extends Model
{
    /**
     * @var string
     */
    protected $table = "payment_ways";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'credit_days',
        'id_company',
        'visible',
        'profile_job_center_id',
    ];
}
