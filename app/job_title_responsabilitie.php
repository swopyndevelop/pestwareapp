<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_responsabilitie
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_responsabilities_id
 * @property int $job_title_profiles_id
 * @property string|null $responsability_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_responsabilitie newModelQuery()
 * @method static Builder|job_title_responsabilitie newQuery()
 * @method static Builder|job_title_responsabilitie query()
 * @method static Builder|job_title_responsabilitie whereCreatedAt($value)
 * @method static Builder|job_title_responsabilitie whereId($value)
 * @method static Builder|job_title_responsabilitie whereJobTitleProfilesId($value)
 * @method static Builder|job_title_responsabilitie whereJobTitleResponsabilitiesId($value)
 * @method static Builder|job_title_responsabilitie whereResponsabilityName($value)
 * @method static Builder|job_title_responsabilitie whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_responsabilitie extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_responsabilities";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_responsabilities_id',
        'job_title_profiles_id',
        'responsability_name'
    ];
}
