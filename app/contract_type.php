<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class contract_type
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property string $contract_name
 * @property mixed $contract_structure
 * @property string|null $contract_example
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|contract_type newModelQuery()
 * @method static Builder|contract_type newQuery()
 * @method static Builder|contract_type query()
 * @method static Builder|contract_type whereContractExample($value)
 * @method static Builder|contract_type whereContractName($value)
 * @method static Builder|contract_type whereContractStructure($value)
 * @method static Builder|contract_type whereCreatedAt($value)
 * @method static Builder|contract_type whereId($value)
 * @method static Builder|contract_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class contract_type extends Model
{
    /**
     * @var string
     */
    protected $table = "contract_types";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'contract_name',
        'contract_structure',
        'contract_example'
    ];
}
