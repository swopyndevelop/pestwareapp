<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class canceled_service_order
 *
 * @package App
 * @author Olga Rodríguez
 * @version 02/05/2019
 * @property int $id
 * @property int $id_service_order
 * @property string $reason
 * @property string|null $commentary
 * @property string $date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|canceled_service_order newModelQuery()
 * @method static Builder|canceled_service_order newQuery()
 * @method static Builder|canceled_service_order query()
 * @method static Builder|canceled_service_order whereCommentary($value)
 * @method static Builder|canceled_service_order whereCreatedAt($value)
 * @method static Builder|canceled_service_order whereDate($value)
 * @method static Builder|canceled_service_order whereId($value)
 * @method static Builder|canceled_service_order whereIdServiceOrder($value)
 * @method static Builder|canceled_service_order whereReason($value)
 * @method static Builder|canceled_service_order whereUpdatedAt($value)
 * @mixin Eloquent
 */
class canceled_service_order extends Model
{
    /**
     * @var string
     */
    protected $table = "canceled_service_orders";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_service_order',
        'reason',
        'commentary',
        'date'
    ];
}
