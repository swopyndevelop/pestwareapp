<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class BillingMode
 *
 * @package App
 * @author Manuel Mendoza
 * @version 29/03/2021
 * @property int $id
 * @property string $name
 * @property string $description
 * @property Carbon|null $updated_at
 * @method static Builder|extra newModelQuery()
 * @method static Builder|extra newQuery()
 * @method static Builder|extra query()
 * @method static Builder|extra whereAmount($value)
 * @method static Builder|extra whereCreatedAt($value)
 * @method static Builder|extra whereDescription($value)
 * @method static Builder|extra whereId($value)
 * @method static Builder|extra whereIdCompany($value)
 * @method static Builder|extra whereName($value)
 * @method static Builder|extra whereUpdatedAt($value)
 * @mixin Eloquent
 */
class BillingMode extends Model
{
    /** @var string */
    protected $table = "billing_modes";
    /** @var array */
    protected $fillable = [
        'id',
        'name',
        'description',
    ];
}
