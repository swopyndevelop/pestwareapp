<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class inhabitant_type
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|service_order[] $service_order
 * @property-read int|null $service_order_count
 * @method static Builder|inhabitant_type newModelQuery()
 * @method static Builder|inhabitant_type newQuery()
 * @method static Builder|inhabitant_type query()
 * @method static Builder|inhabitant_type whereCreatedAt($value)
 * @method static Builder|inhabitant_type whereId($value)
 * @method static Builder|inhabitant_type whereName($value)
 * @method static Builder|inhabitant_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class inhabitant_type extends Model
{
    /**
     * @var string
     */
    protected $table = "inhabitant_types";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];

    /**
    * @author Olga Rodríguez
    * @version 12/01/2019
    *Función para relación muchos a muchos con tabla customer_datas.
    */
    public function service_order()
    {
        return $this->belongsToMany(service_order::class);
    }
}
