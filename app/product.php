<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class product
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property int $id_type_product
 * @property string $name
 * @property string $description
 * @property string $characteristics
 * @property string $suggested_use
 * @property int $id_presentation
 * @property int $id_unit
 * @property float|null $quantity
 * @property string|null $active_ingredient
 * @property string|null $register
 * @property float $base_price
 * @property double|null $sale_price
 * @property string|null $unit_code_billing
 * @property string|null $product_code_billing
 * @property int $id_companie
 * @property int $profile_job_center_id
 * @property string $image_product
 * @property boolean $is_available
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|aditional_info[] $aditional_infos
 * @property-read int|null $aditional_infos_count
 * @property string $visible
 * @method static Builder|product newModelQuery()
 * @method static Builder|product newQuery()
 * @method static Builder|product query()
 * @method static Builder|product whereActiveIngredient($value)
 * @method static Builder|product whereBasePrice($value)
 * @method static Builder|product whereCharacteristics($value)
 * @method static Builder|product whereCreatedAt($value)
 * @method static Builder|product whereDescription($value)
 * @method static Builder|product whereId($value)
 * @method static Builder|product whereIdCompanie($value)
 * @method static Builder|product whereIdPresentation($value)
 * @method static Builder|product whereIdTypeProduct($value)
 * @method static Builder|product whereIdUnit($value)
 * @method static Builder|product whereName($value)
 * @method static Builder|product whereQuantity($value)
 * @method static Builder|product whereSuggestedUse($value)
 * @method static Builder|product whereUpdatedAt($value)
 * @mixin Eloquent
 */
class product extends Model
{
    /**
     * @var string
     */
    protected $table = "products";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_type_product',
        'name',
        'description',
        'characteristics',
        'suggested_use',
        'id_presentation',
        'id_unit',
        'quantity',
        'active_ingredient',
        'register',
        'base_price',
        'sale_price',
        'unit_code_billing',
        'product_code_billing',
        'image_product',
        'id_companie',
        'profile_job_center_id',
        'is_available',
        'visible'
    ];

    /**
    * @author Olga Rodríguez
    * @version 05/06/2019
    *Función para relación uno a muchos con tabla aditional_infos
    */
    public function aditional_infos()
    {
        return $this->hasMany(aditional_info::class);
    }
}
