<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class cash_picture
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/06/2019
 * @property int $id
 * @property int $id_service_order
 * @property string $file_route
 * @property string|null $other_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|service_firms newModelQuery()
 * @method static Builder|service_firms newQuery()
 * @method static Builder|service_firms query()
 * @method static Builder|service_firms whereCreatedAt($value)
 * @method static Builder|service_firms whereFileRoute($value)
 * @method static Builder|service_firms whereId($value)
 * @method static Builder|service_firms whereIdServiceOrder($value)
 * @method static Builder|service_firms whereUpdatedAt($value)
 * @mixin Eloquent
 */
class service_firms extends Model
{
    /**
     * @var string
     */
    protected $table = "service_firms";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_service_order',
        'file_route',
        'other_name'
    ];
}