<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeArea
 * @package App
 * @property int $id
 * @property string $name
 */
class TypeArea extends Model
{
    protected $table = 'type_areas';

    protected $fillable = ['name'];
}
