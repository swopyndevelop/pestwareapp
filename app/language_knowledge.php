<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class language_knowledge
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @method static Builder|language_knowledge newModelQuery()
 * @method static Builder|language_knowledge newQuery()
 * @method static Builder|language_knowledge query()
 * @mixin Eloquent
 */
class language_knowledge extends Model
{
    /**
     * @var string
     */
    protected $table = "language_knowledges";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'language'
    ];
}
