<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_attitude
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_attitudes_id
 * @property int $job_title_profiles_id
 * @property string|null $attitude_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_attitude newModelQuery()
 * @method static Builder|job_title_attitude newQuery()
 * @method static Builder|job_title_attitude query()
 * @method static Builder|job_title_attitude whereAttitudeName($value)
 * @method static Builder|job_title_attitude whereCreatedAt($value)
 * @method static Builder|job_title_attitude whereId($value)
 * @method static Builder|job_title_attitude whereJobTitleAttitudesId($value)
 * @method static Builder|job_title_attitude whereJobTitleProfilesId($value)
 * @method static Builder|job_title_attitude whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_attitude extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_attitudes";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_attitudes_id',
        'job_title_profiles_id',
        'attitude_name'
    ];
}
