<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


/**
 * Class Log_request
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/19/2021
 * @property int $id
 * @property string|null $url
 * @property string|null $endpoint
 * @property string|null $method
 * @property string|null $ip
 * @property string|null $user
 * @property string|null $duration
 * @property string|null $cookie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class LogRequestApi extends Model
{
    protected $table = "log_requests_api";

    protected $fillable = [
        'id',
        'url',
        'endpoint',
        'method',
        'ip',
        'user',
        'duration',
        'cookie',
    ];
}
