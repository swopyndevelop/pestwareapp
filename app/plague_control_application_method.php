<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class plague_control_application_method
 *
 * @package App
 * @author Alberto Martínez
 * @version 11/08/2019
 * @property int $id
 * @property int $plague_control_id
 * @property int $id_application_method
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|plague_control_application_method newModelQuery()
 * @method static Builder|plague_control_application_method newQuery()
 * @method static Builder|plague_control_application_method query()
 * @method static Builder|plague_control_application_method whereCreatedAt($value)
 * @method static Builder|plague_control_application_method whereId($value)
 * @method static Builder|plague_control_application_method whereIdApplicationMethod($value)
 * @method static Builder|plague_control_application_method wherePlagueControlId($value)
 * @method static Builder|plague_control_application_method whereUpdatedAt($value)
 * @mixin Eloquent
 */
class plague_control_application_method extends Model
{
    /**
     * @var string
     */
    protected $table = "plague_controls_application_methods";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'plague_control_id',
        'id_application_method'
    ];
}