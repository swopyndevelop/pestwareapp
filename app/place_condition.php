<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class place_condition
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property int $id_service_order
 * @property int|null $indications
 * @property string|null $restricted_access
 * @property string|null $commentary
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|condition_picture[] $condition_pictures
 * @property-read int|null $condition_pictures_count
 * @property-read Collection|order_cleaning[] $order_cleanings
 * @property-read int|null $order_cleanings_count
 * @method static Builder|place_condition newModelQuery()
 * @method static Builder|place_condition newQuery()
 * @method static Builder|place_condition query()
 * @method static Builder|place_condition whereCommentary($value)
 * @method static Builder|place_condition whereCreatedAt($value)
 * @method static Builder|place_condition whereId($value)
 * @method static Builder|place_condition whereIdServiceOrder($value)
 * @method static Builder|place_condition whereIndications($value)
 * @method static Builder|place_condition whereRestrictedAccess($value)
 * @method static Builder|place_condition whereUpdatedAt($value)
 * @mixin Eloquent
 */
class place_condition extends Model
{
    /**
     * @var string
     */
    protected $table = "place_conditions";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_service_order',
        'indications',
        'restricted_access',
        'commentary'
    ];

     /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación muchos a muchos con tabla order_cleanings
    */
    public function order_cleanings()
    {
        return $this->belongsToMany(order_cleaning::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación uno a muchos con tabla condition_pictures
    */
    public function condition_pictures()
    {
        return $this->hasMany(condition_picture::class);
    }
}
