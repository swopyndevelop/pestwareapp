<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class tracing
 *
 * @package App
 * @author Olga Rodríguez
 * @version 07/03/2019
 * @property int $id
 * @property int $id_quotation
 * @property string $tracing_date
 * @property string $tracing_hour
 * @property string|null $commentary
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|tracing newModelQuery()
 * @method static Builder|tracing newQuery()
 * @method static Builder|tracing query()
 * @method static Builder|tracing whereCommentary($value)
 * @method static Builder|tracing whereCreatedAt($value)
 * @method static Builder|tracing whereId($value)
 * @method static Builder|tracing whereIdQuotation($value)
 * @method static Builder|tracing whereTracingDate($value)
 * @method static Builder|tracing whereTracingHour($value)
 * @method static Builder|tracing whereUpdatedAt($value)
 * @mixin Eloquent
 */
class tracing extends Model
{
    /**
     * @var string
     */
    protected $table = "tracings";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_quotation',
        'tracing_date',
        'tracing_hour',
        'commentary'
    ];
}
