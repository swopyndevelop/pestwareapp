<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\cash_cut
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property float|null $total_efec
 * @property float|null $sub_efec
 * @property float|null $total_transf
 * @property float|null $sub_transf
 * @property float|null $total_cred
 * @property float|null $sub_cred
 * @property float|null $total_deb
 * @property float|null $sub_deb
 * @property float|null $total_cheq
 * @property float|null $sub_cheq
 * @property float|null $total_depos
 * @property float|null $sub_depos
 * @property float $total
 * @property float|null $subtotal
 * @property float|null $total_arqueo
 * @property float $tot_credit
 * @property float|null $total_adeudo
 * @property float $tot_other
 * @property float|null $total_gastos
 * @property float|null $desfase
 * @property int|null $arqueo
 * @property int $depositado
 * @property int|null $delivered
 * @property int $sucursal_id
 * @property int $companie
 * @property string $date
 * @property string|null $time
 * @property int $realize
 * @property int $programs
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|cash_cut newModelQuery()
 * @method static Builder|cash_cut newQuery()
 * @method static Builder|cash_cut query()
 * @method static Builder|cash_cut whereArqueo($value)
 * @method static Builder|cash_cut whereCompanie($value)
 * @method static Builder|cash_cut whereCreatedAt($value)
 * @method static Builder|cash_cut whereDate($value)
 * @method static Builder|cash_cut whereDelivered($value)
 * @method static Builder|cash_cut whereDepositado($value)
 * @method static Builder|cash_cut whereDesfase($value)
 * @method static Builder|cash_cut whereId($value)
 * @method static Builder|cash_cut whereName($value)
 * @method static Builder|cash_cut wherePrograms($value)
 * @method static Builder|cash_cut whereRealize($value)
 * @method static Builder|cash_cut whereSubCheq($value)
 * @method static Builder|cash_cut whereSubCred($value)
 * @method static Builder|cash_cut whereSubDeb($value)
 * @method static Builder|cash_cut whereSubDepos($value)
 * @method static Builder|cash_cut whereSubEfec($value)
 * @method static Builder|cash_cut whereSubTransf($value)
 * @method static Builder|cash_cut whereSubtotal($value)
 * @method static Builder|cash_cut whereSucursalId($value)
 * @method static Builder|cash_cut whereTime($value)
 * @method static Builder|cash_cut whereTotCredit($value)
 * @method static Builder|cash_cut whereTotOther($value)
 * @method static Builder|cash_cut whereTotal($value)
 * @method static Builder|cash_cut whereTotalAdeudo($value)
 * @method static Builder|cash_cut whereTotalArqueo($value)
 * @method static Builder|cash_cut whereTotalCheq($value)
 * @method static Builder|cash_cut whereTotalCred($value)
 * @method static Builder|cash_cut whereTotalDeb($value)
 * @method static Builder|cash_cut whereTotalDepos($value)
 * @method static Builder|cash_cut whereTotalEfec($value)
 * @method static Builder|cash_cut whereTotalGastos($value)
 * @method static Builder|cash_cut whereTotalTransf($value)
 * @method static Builder|cash_cut whereUpdatedAt($value)
 * @method static Builder|cash_cut whereUserId($value)
 * @mixin Eloquent
 */
class cash_cut extends Model
{
    /**
     * @var string
     */
    protected $table = "cash_cuts";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'user_id',
        'total_efec',
        'total_transf',
        'total_cred',
        'total_adeudo',
        'total_deb',
        'total_cheq',
        'total_depos',
        'total',
        'total_arqueo',
        'desfase',
        'arqueo',
        'depositado',
        'sucursal_id',
        'date',
        'time',
        'delivered',
        'programs',
        'realize',
        'companie'
    ];
}
