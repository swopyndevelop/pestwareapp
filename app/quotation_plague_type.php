<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class quotation_plague_type
 *
 * @package App
 * @author Olga Rodríguez
 * @version 22/01/2019
 * @method static Builder|quotation_plague_type newModelQuery()
 * @method static Builder|quotation_plague_type newQuery()
 * @method static Builder|quotation_plague_type query()
 * @mixin Eloquent
 */
class quotation_plague_type extends Model
{
    /**
     * @var string
     */
    protected $table = "quotation_plague_type";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'quotation_id',
        'plague_type_id'
    ];
}
