<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class statuse
 *
 * @package App
 * @author Olga Rodríguez
 * @version 07/03/2019
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|statuse newModelQuery()
 * @method static Builder|statuse newQuery()
 * @method static Builder|statuse query()
 * @method static Builder|statuse whereCreatedAt($value)
 * @method static Builder|statuse whereId($value)
 * @method static Builder|statuse whereName($value)
 * @method static Builder|statuse whereUpdatedAt($value)
 * @mixin Eloquent
 */
class statuse extends Model
{
    /**
     * @var string
     */
    protected $table = "statuses";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];
}
