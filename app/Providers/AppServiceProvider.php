<?php

namespace App\Providers;

use App\Notifications\LogsNotification;
use App\QueueNotification;
use Blade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use Queue;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setLocale(config('app.locale'));

        Queue::failing(function (JobFailed $event) {
            $queNotification = new QueueNotification();
            $queNotification->notify(new LogsNotification($event->job->getRawBody(), $event->exception->getMessage(), 3));
            // $event->connectionName
            // $event->job
            // $event->exception
        });

        Blade::directive('convert', function ($money) {
            return "<?php echo number_format($money, 2); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
