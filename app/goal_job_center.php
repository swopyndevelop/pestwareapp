<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class goal_job_center
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property string $goal_job_centers_id
 * @property int $profile_job_centers_id
 * @property int $value_january
 * @property int $value_february
 * @property int $value_march
 * @property int $value_april
 * @property int $value_may
 * @property int $value_june
 * @property int $value_july
 * @property int $value_august
 * @property int $value_september
 * @property int $value_october
 * @property int $value_november
 * @property int $value_december
 * @property int $value_spTotal
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|goal_job_center newModelQuery()
 * @method static Builder|goal_job_center newQuery()
 * @method static Builder|goal_job_center query()
 * @method static Builder|goal_job_center whereCreatedAt($value)
 * @method static Builder|goal_job_center whereGoalJobCentersId($value)
 * @method static Builder|goal_job_center whereId($value)
 * @method static Builder|goal_job_center whereProfileJobCentersId($value)
 * @method static Builder|goal_job_center whereUpdatedAt($value)
 * @method static Builder|goal_job_center whereValueApril($value)
 * @method static Builder|goal_job_center whereValueAugust($value)
 * @method static Builder|goal_job_center whereValueDecember($value)
 * @method static Builder|goal_job_center whereValueFebruary($value)
 * @method static Builder|goal_job_center whereValueJanuary($value)
 * @method static Builder|goal_job_center whereValueJuly($value)
 * @method static Builder|goal_job_center whereValueJune($value)
 * @method static Builder|goal_job_center whereValueMarch($value)
 * @method static Builder|goal_job_center whereValueMay($value)
 * @method static Builder|goal_job_center whereValueNovember($value)
 * @method static Builder|goal_job_center whereValueOctober($value)
 * @method static Builder|goal_job_center whereValueSeptember($value)
 * @method static Builder|goal_job_center whereValueSpTotal($value)
 * @mixin Eloquent
 */
class goal_job_center extends Model
{
    /**
     * @var string
     */
    protected $table = "goal_job_centers";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'goal_job_centers_id',
        'profile_job_centers_id',
        'value_january',
        'value_february',
        'value_march',
        'value_april',
        'value_may',
        'value_june',
        'value_july',
        'value_august',
        'value_september',
        'value_october',
        'value_november',
        'value_december',
        'value_spTotal'
    ];
}
