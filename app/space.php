<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class space
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $quantity
 * @property string $factor
 * @property string $operation
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|space newModelQuery()
 * @method static Builder|space newQuery()
 * @method static Builder|space query()
 * @method static Builder|space whereCreatedAt($value)
 * @method static Builder|space whereFactor($value)
 * @method static Builder|space whereId($value)
 * @method static Builder|space whereName($value)
 * @method static Builder|space whereOperation($value)
 * @method static Builder|space whereQuantity($value)
 * @method static Builder|space whereType($value)
 * @method static Builder|space whereUpdatedAt($value)
 * @mixin Eloquent
 */
class space extends Model
{
    /**
     * @var string
     */
    protected $table = "spaces";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'type',
        'name',
        'quantity',
        'factor',
        'operation'
    ];
}
