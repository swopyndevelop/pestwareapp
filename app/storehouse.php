<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\storehouse
 *
 * @property int $id
 * @property string $id_store
 * @property int|null $id_job_center
 * @property int $companie
 * @property int|null $id_employee
 * @property int $id_product
 * @property int $stock
 * @property float|null $stock_other_units
 * @property float $total
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|storehouse newModelQuery()
 * @method static Builder|storehouse newQuery()
 * @method static Builder|storehouse query()
 * @method static Builder|storehouse whereCompanie($value)
 * @method static Builder|storehouse whereCreatedAt($value)
 * @method static Builder|storehouse whereId($value)
 * @method static Builder|storehouse whereIdEmployee($value)
 * @method static Builder|storehouse whereIdJobCenter($value)
 * @method static Builder|storehouse whereIdProduct($value)
 * @method static Builder|storehouse whereIdStore($value)
 * @method static Builder|storehouse whereStock($value)
 * @method static Builder|storehouse whereStockOtherUnits($value)
 * @method static Builder|storehouse whereTotal($value)
 * @method static Builder|storehouse whereUpdatedAt($value)
 * @mixin Eloquent
 */
class storehouse extends Model
{
    /**
     * @var string
     */
    protected $table = "storehouses";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_store',
        'id_job_center',
        'id_employee',
        'id_product',
        'stock',
        'stock_other_units',
        'total',
        'companie'
    ];
}
