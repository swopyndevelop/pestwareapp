<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class product_type
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $id_company
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|product_type newModelQuery()
 * @method static Builder|product_type newQuery()
 * @method static Builder|product_type query()
 * @method static Builder|product_type whereCreatedAt($value)
 * @method static Builder|product_type whereId($value)
 * @method static Builder|product_type whereIdCompany($value)
 * @method static Builder|product_type whereName($value)
 * @method static Builder|product_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class product_type extends Model
{
    /**
     * @var string
     */
    protected $table = "product_types";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'profile_job_center_id',
        'visible'
    ];
}
