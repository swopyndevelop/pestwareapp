<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class general_information_job_center
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property string|null $general_information_job_centers_id
 * @property int|null $profile_job_centers_id
 * @property string|null $hour_init
 * @property string|null $hour_final
 * @property string|null $start_workdays
 * @property string|null $final_workdays
 * @property float|null $small_box
 * @property mixed|null $file_small_box
 * @property float|null $breakeven
 * @property mixed|null $rent_contract
 * @property string|null $validity_rent_contract
 * @property mixed|null $hacienda
 * @property string|null $validity_hacienda
 * @property mixed|null $commercial_permission
 * @property string|null $validity_commercial_permission
 * @property mixed|null $fumigation
 * @property string|null $validity_fumigation
 * @property mixed|null $extinguisher
 * @property string|null $validity_extinguisher
 * @property mixed|null $salubrity
 * @property string|null $validity_salubrity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|general_information_job_center newModelQuery()
 * @method static Builder|general_information_job_center newQuery()
 * @method static Builder|general_information_job_center query()
 * @method static Builder|general_information_job_center whereBreakeven($value)
 * @method static Builder|general_information_job_center whereCommercialPermission($value)
 * @method static Builder|general_information_job_center whereCreatedAt($value)
 * @method static Builder|general_information_job_center whereExtinguisher($value)
 * @method static Builder|general_information_job_center whereFileSmallBox($value)
 * @method static Builder|general_information_job_center whereFinalWorkdays($value)
 * @method static Builder|general_information_job_center whereFumigation($value)
 * @method static Builder|general_information_job_center whereGeneralInformationJobCentersId($value)
 * @method static Builder|general_information_job_center whereHacienda($value)
 * @method static Builder|general_information_job_center whereHourFinal($value)
 * @method static Builder|general_information_job_center whereHourInit($value)
 * @method static Builder|general_information_job_center whereId($value)
 * @method static Builder|general_information_job_center whereProfileJobCentersId($value)
 * @method static Builder|general_information_job_center whereRentContract($value)
 * @method static Builder|general_information_job_center whereSalubrity($value)
 * @method static Builder|general_information_job_center whereSmallBox($value)
 * @method static Builder|general_information_job_center whereStartWorkdays($value)
 * @method static Builder|general_information_job_center whereUpdatedAt($value)
 * @method static Builder|general_information_job_center whereValidityCommercialPermission($value)
 * @method static Builder|general_information_job_center whereValidityExtinguisher($value)
 * @method static Builder|general_information_job_center whereValidityFumigation($value)
 * @method static Builder|general_information_job_center whereValidityHacienda($value)
 * @method static Builder|general_information_job_center whereValidityRentContract($value)
 * @method static Builder|general_information_job_center whereValiditySalubrity($value)
 * @mixin Eloquent
 */

class general_information_job_center extends Model
{
    /**
     * @var string
     */
    protected $table = "general_information_job_centers";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'general_information_job_centers_id',
        'profile_job_centers_id',
        'hour_init',
        'hour_final',
        'start_workdays',
        'final_workdays',
        'small_box',
        'file_small_box',
        'breakeven',
        'rent_contract',
        'validity_rent_contract',
        'hacienda',
        'validity_hacienda',
        'commercial_permission',
        'validity_commercial_permission',
        'fumigation',
        'validity_fumigation',
        'extinguisher',
        'validity_extinguisher',
        'salubrity',
        'validity_salubrity'
    ];
}
