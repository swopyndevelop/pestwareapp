<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class place_inspection
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property int $id_service_order
 * @property string|null $nesting_areas
 * @property string|null $commentary
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|inspection_picture[] $inspection_pictures
 * @property-read int|null $inspection_pictures_count
 * @property-read Collection|plague_type[] $plague_types
 * @property-read int|null $plague_types_count
 * @method static Builder|place_inspection newModelQuery()
 * @method static Builder|place_inspection newQuery()
 * @method static Builder|place_inspection query()
 * @method static Builder|place_inspection whereCommentary($value)
 * @method static Builder|place_inspection whereCreatedAt($value)
 * @method static Builder|place_inspection whereId($value)
 * @method static Builder|place_inspection whereIdServiceOrder($value)
 * @method static Builder|place_inspection whereNestingAreas($value)
 * @method static Builder|place_inspection whereUpdatedAt($value)
 * @mixin Eloquent
 */
class place_inspection extends Model
{
    /**
     * @var string
     */
    protected $table = "place_inspections";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_service_order',
        'nesting_areas',
        'commentary'
    ];

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación muchos a muchos con tabla plague_types
    */
    public function plague_types()
    {
        return $this->belongsToMany(plague_type::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación uno a muchos con tabla inspection_pictures
    */
    public function inspection_pictures()
    {
        return $this->hasMany(inspection_picture::class);
    }
}
