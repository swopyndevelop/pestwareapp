<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\HistoricalInventory
 *
 * @property int $id
 * @property string $folio
 * @property string $date
 * @property string $hour
 * @property int $id_user
 * @property int $id_type_movement_inventory
 * @property int $id_product
 * @property string $source
 * @property string $destiny
 * @property int $quantity
 * @property int $fraction_quantity
 * @property int $before_stock
 * @property int $after_stock
 * @property int $fraction_before_stock
 * @property int $fraction_after_stock
 * @property double $unit_price
 * @property double $value_movement
 * @property double $value_inventory
 * @property boolean $is_destiny
 * @property int $id_profile_job_center
 * @property int $id_company
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin Eloquent
 */

class HistoricalInventory extends Model
{
    /**
     * @var string
     */
    protected $table = "historical_inventories";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'folio',
        'date',
        'hour',
        'id_user',
        'id_type_movement_inventory',
        'id_product',
        'source',
        'destiny',
        'quantity',
        'fraction_quantity',
        'before_stock',
        'after_stock',
        'fraction_before_stock',
        'fraction_after_stock',
        'unit_price',
        'visible',
        'value_movement',
        'value_inventory',
        'is_destiny',
        'id_profile_job_center',
        'id_company'
    ];
}
