<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MonitoringChecklist
 * @package App
 * @property int $id
 * @property int $id_type_station
 * @property string $name
 * @property int $id_company
 */

class MonitoringChecklist extends Model
{
    protected $table = 'monitoring_checklists';

    protected $fillable = ['id_type_station','name','id_company'];
}
