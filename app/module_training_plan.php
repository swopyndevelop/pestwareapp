<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class module_training_plan
 *
 * @package App
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @property int $id
 * @property int $module_id
 * @property int $training_plan_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|module_training_plan newModelQuery()
 * @method static Builder|module_training_plan newQuery()
 * @method static Builder|module_training_plan query()
 * @method static Builder|module_training_plan whereCreatedAt($value)
 * @method static Builder|module_training_plan whereId($value)
 * @method static Builder|module_training_plan whereModuleId($value)
 * @method static Builder|module_training_plan whereTrainingPlanId($value)
 * @method static Builder|module_training_plan whereUpdatedAt($value)
 * @mixin Eloquent
 */
class module_training_plan extends Model
{
    /**
     * @var string
     */
    protected $table = "module_training_plan";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'module_id',
        'training_plan_id'
    ];
}
