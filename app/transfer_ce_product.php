<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\transfer_ce_product
 *
 * @property int $id
 * @property int $id_transfer_ce
 * @property int $id_product
 * @property string $units
 * @property int|null $is_units
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|transfer_ce_product newModelQuery()
 * @method static Builder|transfer_ce_product newQuery()
 * @method static Builder|transfer_ce_product query()
 * @method static Builder|transfer_ce_product whereCreatedAt($value)
 * @method static Builder|transfer_ce_product whereId($value)
 * @method static Builder|transfer_ce_product whereIdProduct($value)
 * @method static Builder|transfer_ce_product whereIdTransferCe($value)
 * @method static Builder|transfer_ce_product whereIsUnits($value)
 * @method static Builder|transfer_ce_product whereUnits($value)
 * @method static Builder|transfer_ce_product whereUpdatedAt($value)
 * @mixin Eloquent
 */
class transfer_ce_product extends Model
{
    /**
     * @var string
     */
    protected $table = "transfer_ce_products";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_transfer_ce',
        'id_product',
        'units'
    ];
}
