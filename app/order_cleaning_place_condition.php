<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class order_cleaning_place_condition
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/06/2019
 * @property int $id
 * @property int $place_condition_id
 * @property int $order_cleaning_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|order_cleaning_place_condition newModelQuery()
 * @method static Builder|order_cleaning_place_condition newQuery()
 * @method static Builder|order_cleaning_place_condition query()
 * @method static Builder|order_cleaning_place_condition whereCreatedAt($value)
 * @method static Builder|order_cleaning_place_condition whereId($value)
 * @method static Builder|order_cleaning_place_condition whereOrderCleaningId($value)
 * @method static Builder|order_cleaning_place_condition wherePlaceConditionId($value)
 * @method static Builder|order_cleaning_place_condition whereUpdatedAt($value)
 * @mixin Eloquent
 */
class order_cleaning_place_condition extends Model
{
    /**
     * @var string
     */
    protected $table = "order_cleaning_place_condition";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'place_condition_id',
        'order_cleaning_id'
    ];
}
