<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\deposit_cut
 *
 * @property int $id
 * @property int $id_cash_cut
 * @property int $user_id
 * @property mixed $image
 * @property string $date
 * @property string $hour
 * @property float $total
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|deposit_cut newModelQuery()
 * @method static Builder|deposit_cut newQuery()
 * @method static Builder|deposit_cut query()
 * @method static Builder|deposit_cut whereCreatedAt($value)
 * @method static Builder|deposit_cut whereDate($value)
 * @method static Builder|deposit_cut whereHour($value)
 * @method static Builder|deposit_cut whereId($value)
 * @method static Builder|deposit_cut whereIdCashCut($value)
 * @method static Builder|deposit_cut whereImage($value)
 * @method static Builder|deposit_cut whereTotal($value)
 * @method static Builder|deposit_cut whereUpdatedAt($value)
 * @method static Builder|deposit_cut whereUserId($value)
 * @mixin Eloquent
 */
class deposit_cut extends Model
{
    protected $table = "deposit_cuts";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_cash_cut',
        'user_id',
        'image',
        'date',
        'hour',
        'total'
    ];
}
