<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class training
 *
 * @package App
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @property int $id
 * @property string $title
 * @property string $duration
 * @property string $description
 * @property string $start
 * @property string $end
 * @property string $course
 * @property string|null $job_title_name
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|module[] $modules
 * @property-read int|null $modules_count
 * @method static Builder|training newModelQuery()
 * @method static Builder|training newQuery()
 * @method static Builder|training query()
 * @method static Builder|training whereCompanie($value)
 * @method static Builder|training whereCourse($value)
 * @method static Builder|training whereCreatedAt($value)
 * @method static Builder|training whereDescription($value)
 * @method static Builder|training whereDuration($value)
 * @method static Builder|training whereEnd($value)
 * @method static Builder|training whereId($value)
 * @method static Builder|training whereJobTitleName($value)
 * @method static Builder|training whereStart($value)
 * @method static Builder|training whereTitle($value)
 * @method static Builder|training whereUpdatedAt($value)
 * @mixin Eloquent
 */
class training extends Model
{
    /**
     * @var string
     */
    protected $table = "trainings";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'duration',
        'description',
        'start',
        'end',
        'course',
        'job_title_name',
        'companie'
    ];
    /**
    * @author Olga Rodríguez
    * @version 10/07/2019
    *Función para relación muchos a muchos con tabla modules
    */
    public function modules()
    {
        return $this->belongsToMany(module::class);
    }
}
