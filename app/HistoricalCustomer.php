<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\HistoricalCustomerController
 *
 * @property int $id
 * @property string $name
 * @property int $id_customer
 * @property int $id_user
 * @property string $title
 * @property string $commentary
 * @property Carbon|null $date
 * @property Carbon|null $hour
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Permission newModelQuery()
 * @method static Builder|Permission newQuery()
 * @method static Builder|Permission query()
 * @method static Builder|Permission whereCreatedAt($value)
 * @method static Builder|Permission whereDescription($value)
 * @method static Builder|Permission whereDisplayName($value)
 * @method static Builder|Permission whereId($value)
 * @method static Builder|Permission whereName($value)
 * @method static Builder|Permission whereUpdatedAt($value)
 * @mixin Eloquent
 */

class HistoricalCustomer extends Model
{
    /**
     * @var string
     */
    protected $table = "historical_customers";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_customer',
        'id_user',
        'title',
        'commentary',
        'date',
        'hour'
    ];
}
