<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title
 *
 * @package App
 * @author Olga Rodríguez
 * @version 13/03/2019
 * @property int $id
 * @property string $name
 * @property string $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title newModelQuery()
 * @method static Builder|job_title newQuery()
 * @method static Builder|job_title query()
 * @method static Builder|job_title whereCreatedAt($value)
 * @method static Builder|job_title whereDescription($value)
 * @method static Builder|job_title whereId($value)
 * @method static Builder|job_title whereName($value)
 * @method static Builder|job_title whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title extends Model
{
    /**
     * @var string
     */
    protected $table = "job_titles";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description'
    ];
}
