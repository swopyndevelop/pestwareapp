<?php

namespace App\Mail;

use App\companie;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * @author Manuel Mendoza | Alberto Martinez
 * @version 23/04/2021
 */

class AreaInspectionMail extends  Mailable
{
    use Queueable, SerializesModels;

    private $pdf;
    private $banner;
    private $whatsapp;
    private $dataPdfInspection;

    /**
     * Quotation Mail constructor.
     * @param $pdf
     * @param $banner
     * @param $whatsapp
     * @param $dataPdfInspection
     */
    public function __construct($pdf, $banner, $whatsapp, $dataPdfInspection)
    {
        $this->pdf = $pdf;
        $this->banner = $banner;
        $this->whatsapp = $whatsapp;
        $this->dataPdfInspection = $dataPdfInspection;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = companie::find(\Auth::user()->companie);
        return $this->subject($company->name . ' Área Inspección')
            ->markdown('emails.areaInspectionEmail')
            ->with(['banner' => $this->banner, 'whatsapp' => $this->whatsapp, 'dataPdfInspectionArea' => $this->dataPdfInspection])
            ->attachData($this->pdf, 'Servicio de Área Inspección: '.$this->dataPdfInspection['order']->id_area.'.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}