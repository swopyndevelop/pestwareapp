<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class AccountPortalCustomerMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $password;
    private $url;
    private $company;
    private $type;
    private $token;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $password
     * @param $url
     * @param $company
     * @param $type
     * @param $token
     */
    public function __construct($user, $password, $url, $company, $type, $token)
    {
        $this->user = $user;
        $this->password = $password;
        $this->url = $url;
        $this->company = $company;
        $this->type = $type;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Cuenta Portal de Clientes";
        if ($this->type == null) $subject = "Cuenta PestWareApp";
        return $this->subject($subject)
            ->markdown('emails.account_user_portal_mail')
            ->with([
                'user' => $this->user,
                'password' => $this->password,
                'url' => $this->url,
                'company' => $this->company,
                'type' => $this->type,
                'token' => $this->token
            ]);
    }
}
