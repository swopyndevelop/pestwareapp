<?php


namespace App\Mail;


use Illuminate\Mail\Mailable;

class DevOpsBackupMail extends Mailable
{
    private $data;

    /**
     * DevOpsBackupMail constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('DevOps Notification')->markdown('emails.backup_db_mail')
            ->with(['data' => $this->data]);
    }
}