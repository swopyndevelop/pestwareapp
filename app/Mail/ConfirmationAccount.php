<?php


namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmationAccount extends Mailable
{
    use Queueable, SerializesModels;

    private $title;
    private $message;
    private $token;

    /**
     * Notification mail message.
     *
     * @param $title
     * @param $message
     * @param $token
     */
    public function __construct($title, $message, $token)
    {
        $this->title = $title;
        $this->message = $message;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->title)->markdown('emails.resend_email_confirmation')
            ->with(['message' => $this->message, 'token' => $this->token]);
    }
}