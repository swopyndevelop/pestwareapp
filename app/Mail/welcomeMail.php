<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class welcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    private $id;
    private $token;
    private $companie;
    private $companieId;

    /**
     * Create a new message instance.
     *
     * @param $id
     * @param $companie
     * @param $companieId
     * @param $token
     */
    public function __construct($id, $companie, $companieId, $token)
    {
        $this->id = $id;
        $this->token = $token;
        $this->companie = $companie;
        $this->companieId = $companieId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contact = DB::table('contacts')->where('id', $this->id)->first();

        return $this->subject('Bienvenido')->markdown('emails.enterprise_mail')
            ->with(['contact' => $contact, 'companie' => $this->companie, 'companieId' => $this->companieId, 'token' => $this->token]);
    }
}
