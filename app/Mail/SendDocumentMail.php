<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendDocumentMail extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        if ($this->data['xml'] == null) {
            return $this->subject($this->data['subjectText'])
                ->markdown('emails.generalDocEmail')
                ->with([
                    'banner' => $this->data['banner'],
                    'api_whats' => $this->data['urlWhatsApp'],
                    'messengerPersonal' => $this->data['messengerPersonal'],
                    'customer' => $this->data['customer'],
                    'accountName' => $this->data['accountName']
                ])->attachData(
                    base64_decode($this->data['pdf']),
                    $this->data['nameFile'],
                    ['mime' => $this->data['mime']]
                );
        } else {
            return $this->subject($this->data['subjectText'])
                ->markdown('emails.generalDocEmail')
                ->with([
                    'banner' => $this->data['banner'],
                    'api_whats' => $this->data['urlWhatsApp'],
                    'messengerPersonal' => $this->data['messengerPersonal'],
                    'customer' => $this->data['customer'],
                    'accountName' => $this->data['accountName']
                ])->attachData(
                    base64_decode(end($this->data['pdf'])),
                    $this->data['nameFile'],
                    ['mime' => $this->data['mime'].'.pdf']
                )->attachData(
                    base64_decode(end($this->data['xml'])),
                    $this->data['nameFile'].'.xml',
                    ['mime' => 'application/xml']
                );
        }
    }
}