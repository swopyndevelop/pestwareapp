<?php

namespace App\Mail;

use App\profile_job_center;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;
use Illuminate\Http\Request;

/**
 * @author Yomira Martínez
 * @version 18/01/2019
 * Mail; archivo para el envio de emails de cotizaciones.
 */

class QuotatioMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $id;
    private $salida;
    private $company;
    private $type;
    private $whatsappPersonal;
    private $messengerPersonal;
    private $jobCenterProfile;

    /**
     * Quotation Mail constructor.
     * @param $user
     * @param $id
     * @param $salida
     * @param $companie
     * @param $type
     * @param $whatsappPersonal
     * @param $messengerPersonal
     * @param $jobCenterProfile
     */
    public function __construct($user, $id, $salida, $companie, $type, $whatsappPersonal, $messengerPersonal, $jobCenterProfile)
    {
        $this->user = $user;
        $this->id = $id;
        $this->salida = $salida;
        $this->company = $companie;
        $this->type = $type;
        $this->whatsappPersonal = $whatsappPersonal;
        $this->messengerPersonal = $messengerPersonal;
        $this->jobCenterProfile = $jobCenterProfile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $quotation = DB::table('quotations as q')->join('customers as c','c.id','q.id_customer')
            ->join('establishment_types as e','e.id','c.establishment_id')
            ->join('customer_datas as cd','cd.customer_id','c.id')
            ->select('q.id','q.created_at as date','q.total','q.construction_measure','q.garden_measure','q.id_status','c.name','c.establishment_name',
            'c.cellphone','e.id as e_id','e.name as e_name',
            'cd.address','cd.reference_address','cd.email', 'cd.phone_number', 'c.municipality','q.id_quotation')
            ->where('q.id',$this->id)->first();
        $banner = DB::table('personal_mails')->where('profile_job_center_id',$this->jobCenterProfile)->first();
        $nombre = DB::table('companies')->where('id',$this->company)->first();
        $country_code = DB::table('countries')->where('id', $nombre->id_code_country)->first();
        $api_whats = 'https://api.whatsapp.com/send?phone='. $country_code->code_country . $this->whatsappPersonal;

        return $this->subject($nombre->name.' '.'Cotización Control de Plagas')
            ->markdown('emails.quotationmail')
            ->with(['quotation' => $quotation, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $this->messengerPersonal])
            ->attachData($this->salida, 'Cotización '.$quotation->id_quotation.'.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
