<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class AppServiceLogMail extends Mailable
{
    use Queueable, SerializesModels;

    private $employee;
    private $company;
    private $event;
    private $json;
    private $error;
    private $log;

    /**
     * Create a new message instance.
     *
     * @param $employee
     * @param $company
     * @param $event
     * @param $json
     * @param $error
     * @param $log
     */
    public function __construct($employee, $company, $event, $json, $error, $log)
    {
        $this->employee = $employee;
        $this->company = $company;
        $this->event = $event;
        $this->json = $json;
        $this->error = $error;
        $this->log = $log;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('App Service Log')
            ->markdown('emails.app_service_log_mail')
            ->with([
                'employee' => $this->employee,
                'company' => $this->company,
                'serviceData' => $this->event,
                'json' => $this->json,
                'error' => $this->error,
                'log' => $this->log
            ]);
    }
}
