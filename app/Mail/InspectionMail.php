<?php

namespace App\Mail;

use App\companie;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * @author Manuel Mendoza | Alberto Martinez
 * @version 23/04/2021
 */

class InspectionMail extends  Mailable
{
    use Queueable, SerializesModels;

    private $pdf;
    private $banner;
    private $whatsapp;
    private $dataStationInspection;

    /**
     * Quotation Mail constructor.
     * @param $pdf
     * @param $banner
     * @param $whatsapp
     * @param $dataStationInspection
     */
    public function __construct($pdf, $banner, $whatsapp, $dataStationInspection)
    {
        $this->pdf = $pdf;
        $this->banner = $banner;
        $this->whatsapp = $whatsapp;
        $this->dataStationInspection = $dataStationInspection;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = companie::find(\Auth::user()->companie);
        return $this->subject($company->name . ' Inspección')
            ->markdown('emails.inspectionEmail')
            ->with(['banner' => $this->banner, 'whatsapp' => $this->whatsapp, 'dataStationInspection' => $this->dataStationInspection])
            ->attachData($this->pdf, 'Servicio de Inspección: '.$this->dataStationInspection['inspection']->id_inspection.'.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}