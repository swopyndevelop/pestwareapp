<?php

namespace App\Mail;

use App\companie;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * @author Manuel Mendoza | Alberto Martinez
 * @version 23/04/2021
 */

class PurchaseOrderMail extends  Mailable
{
    use Queueable, SerializesModels;

    private $pdf;
    private $banner;
    private $whatsapp;
    private $purchaseOrder;

    /**
     * Quotation Mail constructor.
     * @param $pdf
     * @param $banner
     * @param $whatsapp
     * @param $purchaseOrder
     */
    public function __construct($pdf, $banner, $whatsapp, $purchaseOrder)
    {
        $this->pdf = $pdf;
        $this->banner = $banner;
        $this->whatsapp = $whatsapp;
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = companie::find(\Auth::user()->companie);
        return $this->subject($company->name . ' Orden de Compra')
            ->markdown('emails.purchaseOrderemail')
            ->with(['banner' => $this->banner, 'whatsapp' => $this->whatsapp, 'purchaseOrder' => $this->purchaseOrder])
            ->attachData($this->pdf, 'Orden de Compra '.$this->purchaseOrder->folio.'.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}