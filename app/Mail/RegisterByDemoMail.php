<?php


namespace App\Mail;


use Illuminate\Mail\Mailable;

class RegisterByDemoMail extends Mailable
{
    private $data;

    /**
     * RegisterByDemoMail constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Bienvenido')->markdown('emails.register_demo_mail')
            ->with(['data' => $this->data]);
    }
}