<?php

namespace App\Mail;

use App\companie;
use Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceMail extends Mailable
{

    use Queueable, SerializesModels;

    private $pdfInvoice;
    private $xmlInvoice;
    private $banner;
    private $whatsapp;
    private $invoice;
    private $body;
    private $greeting;
    private $email;
    private $isBillingPrivate;

    /**
     * Send invoice for mail.
     *
     * @param $pdfInvoice
     * @param $xmlInvoice
     * @param $banner
     * @param $whatsapp
     * @param $invoice
     * @param $body
     * @param $greeting
     * @param $email
     * @param $isBillingPrivate
     */
    public function __construct($pdfInvoice, $xmlInvoice, $banner, $whatsapp, $invoice, $body, $greeting, $email, $isBillingPrivate)
    {
        $this->pdfInvoice = $pdfInvoice;
        $this->xmlInvoice = $xmlInvoice;
        $this->banner = $banner;
        $this->whatsapp = $whatsapp;
        $this->invoice = $invoice;
        $this->body = $body;
        $this->greeting = $greeting;
        $this->email = $email;
        $this->isBillingPrivate = $isBillingPrivate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = companie::find($this->invoice->id_company);
        if ($this->isBillingPrivate) $customerName = $company->name;
        else $customerName = $this->invoice->customer_name;
        return $this->subject($company->name . ' - Factura ' . $this->invoice->folio_invoice)
            ->markdown('emails.invoiceMail')
            ->with(['banner' => $this->banner, 'whatsapp' => $this->whatsapp, 'invoice' => $this->invoice, 'body' => $this->body, 'greeting' => $this->greeting, 'email' => $this->email, 'customerName' => $customerName])
            ->attachData(base64_decode(end($this->pdfInvoice)), $this->invoice->folio_invoice . ' - ' . $company->name . '.pdf', ['mime' => 'application/pdf',])
            ->attachData(base64_decode(end($this->xmlInvoice)), $this->invoice->folio_invoice . ' - ' . $company->name . '.xml', ['mime' => 'application/xml',]);
    }
}