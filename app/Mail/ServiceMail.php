<?php

namespace App\Mail;

use App\profile_job_center;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;
use Illuminate\Http\Request;

class ServiceMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $id;
    private $pdf;
    private $pdfOrder;
    private $pdfMonitoring;
    private $pdfArea;
    private $pdfInvoice;
    private $xmlInvoice;
    private $company;
    private $jobCenterProfile;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $id
     * @param $pdf
     * @param $pdfOrder
     * @param $pdfMonitoring
     * @param $pdfArea
     * @param $pdfInvoice
     * @param $xmlInvoice
     * @param $company
     * @param $jobCenterProfile
     */
    public function __construct($user, $id, $pdf, $pdfOrder, $pdfMonitoring, $pdfArea, $pdfInvoice, $xmlInvoice, $company, $jobCenterProfile)
    {
        $this->user = $user;
        $this->id = $id;
        $this->pdf = base64_encode($pdf);
        $this->pdfOrder = base64_encode($pdfOrder);
        $this->pdfMonitoring = base64_encode($pdfMonitoring);
        $this->pdfArea = base64_encode($pdfArea);
        $this->pdfInvoice = $pdfInvoice;
        $this->xmlInvoice = $xmlInvoice;
        $this->company = $company;
        $this->jobCenterProfile = $jobCenterProfile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = DB::table('events as e')
            ->join('employees as em','e.id_employee','em.id')
            ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
            ->join('service_orders as so','e.id_service_order','so.id')
            ->join('payment_methods as pm','so.id_payment_method','pm.id')
            ->join('payment_ways as pw','so.id_payment_way','pw.id')
            ->join('users as u','so.user_id','u.id')
            ->join('statuses as st','so.id_status','st.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('establishment_types as et','q.establishment_id','et.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            ->select('so.id as order','so.id_service_order','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality', 'q.id_plague_jer','q.total',
                'pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status', 'so.id_job_center',
                'e.id as event','c.colony','q.id as quotation','e.final_hour','e.final_date','so.address as a_rfc', 'so.email as e_rfc','so.bussiness_name',
                'so.observations','cd.billing','d.id as discount', 'd.percentage','ex.id as extra','ex.amount','q.construction_measure','q.garden_measure',
                'q.price', 'q.establishment_id as e_id','pm.id as pm_id','pw.id as pw_id','pm.name as metodo','pw.name as tipo', 'cd.email')
            ->where('so.id', $this->id)
            ->first();

        $banner = DB::table('personal_mails')->where('profile_job_center_id',$this->jobCenterProfile)->first();
        $messengerPersonal = profile_job_center::find($this->jobCenterProfile);
        $nombre = DB::table('companies')->where('id',$this->company)->first();
        $country_code = DB::table('countries')->where('id', $nombre->id_code_country)->first();
        $api_whats = 'https://api.whatsapp.com/send?phone='. $country_code->code_country .$banner->image_whatsapp;

        $titleFileCertificate = 'Certificado de Servicio ';
        if ($this->company == 457 || $this->company == 504 || $this->company == 628) $titleFileCertificate = 'Factura ';

        if($this->pdf != null && $this->pdfOrder == null && $this->pdfMonitoring == null && $this->pdfArea == null && $this->pdfInvoice == null && $this->xmlInvoice == null) {;
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate .$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf == null && $this->pdfOrder != null && $this->pdfMonitoring == null && $this->pdfArea == null && $this->pdfInvoice == null && $this->xmlInvoice == null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdfOrder), 'Orden de Servicio '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf == null && $this->pdfOrder == null && $this->pdfMonitoring != null && $this->pdfArea == null && $this->pdfInvoice == null && $this->xmlInvoice == null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdfMonitoring), 'Inspección '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf == null && $this->pdfOrder == null && $this->pdfMonitoring == null && $this->pdfArea != null && $this->pdfInvoice == null && $this->xmlInvoice == null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdfArea), 'Inspección de Área '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf == null && $this->pdfOrder == null && $this->pdfMonitoring == null && $this->pdfArea == null && $this->pdfInvoice != null && $this->xmlInvoice != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode(end($this->pdfInvoice)), 'Factura '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode(end($this->xmlInvoice)), 'Factura '.$order->id_service_order.'.xml', ['mime' => 'application/xml',]);
        }

        if($this->pdf != null && $this->pdfOrder != null && $this->pdfMonitoring != null && $this->pdfArea != null && $this->pdfInvoice != null && $this->xmlInvoice != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate . $order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfOrder), 'Orden de Servicio '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfMonitoring), 'Inspección '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfArea), 'Inspección de Área '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode(end($this->pdfInvoice)), 'Factura '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode(end($this->xmlInvoice)), 'Factura '.$order->id_service_order.'.xml', ['mime' => 'application/xml',]);
        }

        if($this->pdf != null && $this->pdfOrder != null && $this->pdfMonitoring != null && $this->pdfArea != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate . $order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfOrder), 'Orden de Servicio '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfMonitoring), 'Inspección '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfArea), 'Inspección de Área '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf != null && $this->pdfOrder != null && $this->pdfMonitoring != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate . $order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfOrder), 'Orden de Servicio '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfMonitoring), 'Inspección '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf != null && $this->pdfOrder != null && $this->pdfArea != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate . $order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfOrder), 'Orden de Servicio '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfArea), 'Inspección de Área '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf != null && $this->pdfMonitoring != null && $this->pdfArea != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate . $order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfMonitoring), 'Inspección '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfArea), 'Inspección de Área '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf != null && $this->pdfMonitoring != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate . $order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfMonitoring), 'Inspección '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf != null && $this->pdfArea != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate . $order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfArea), 'Inspección de Área '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdfOrder != null && $this->pdfArea != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdfOrder), 'Orden de Servicio '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfArea), 'Inspección de Área '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

        if($this->pdf != null && $this->pdfOrder != null) {
            return $this->subject($nombre->name.' '.'Reporte Control de Plagas')
                ->markdown('emails.servicemail')
                ->with(['order' => $order, 'banner' => $banner, 'api_whats' => $api_whats, 'messengerPersonal' => $messengerPersonal])
                ->attachData(base64_decode($this->pdf), $titleFileCertificate . $order->id_service_order.'.pdf', ['mime' => 'application/pdf',])
                ->attachData(base64_decode($this->pdfOrder), 'Orden de Servicio '.$order->id_service_order.'.pdf', ['mime' => 'application/pdf',]);
        }

    }
}
