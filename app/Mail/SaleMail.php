<?php

namespace App\Mail;

use App\companie;
use App\personal_mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * @author Manuel Mendoza | Alberto Martinez
 * @version 23/04/2021
 */

class SaleMail extends  Mailable
{
    use Queueable, SerializesModels;

    private $pdf;
    private $salePdf;

    /**
     * Quotation Mail constructor.
     * @param $pdf
     * @param $salePdf
     */
    public function __construct($pdf, $salePdf)
    {
        $this->pdf = $pdf;
        $this->salePdf = $salePdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = $this->salePdf['sale']->companie_name;
        $personalMail = personal_mail::where('profile_job_center_id',$this->salePdf['sale']->pjc_id)->first();
        return $this->subject($company . ' Venta')
            ->markdown('emails.saleEmail')
            ->with(['personalMail' => $personalMail, 'sale' => $this->salePdf['sale']])
            ->attachData($this->pdf, 'Venta '.$this->salePdf['sale']->folio.'.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}