<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReportControlPlaguesMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$name,$pdf,$quotation)
    {
        //
        $this->user = $user;
        $this->name = $name;
        $this->quotation = $quotation;
        $this->pdf = base64_encode($pdf);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $client = $this->name;

          return $this->subject('Reporte Control de Plagas BIOFIN')->markdown('emails.quotationgenerate')->with(['client' => $client])->attachData(base64_decode($this->pdf), 'Control de Plagas '.$this->quotation.'.pdf', [
            'mime' => 'application/pdf',
        ]);
    }
}
