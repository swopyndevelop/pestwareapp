<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class MailGenerate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$name,$salida,$companie, $quotation)
    {
        //
        $this->user = $user;
        $this->name = $name;
        $this->companie = $companie;
        $this->salida = base64_encode($salida);
        $this->quotation = $quotation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $client = $this->name;
        $banner = DB::table('personal_mails')->where('id_company',$this->companie)->first();
        $nombre = DB::table('companies')->where('id',$this->companie)->first();
        $api_whats = 'https://api.whatsapp.com/api/send?phone='.$banner->image_whatsapp;

          return $this->subject($nombre->name.' '.'Cotización Control de Plagas BIOFIN')->markdown('emails.quotationgenerate')->with(['client' => $client, 'banner' => $banner, 'api_whats' => $api_whats])->attachData(base64_decode($this->salida), 'Cotización '.$this->quotation.'.pdf', [
            'mime' => 'application/pdf',
        ]);
    }
}
