<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use DB;

/**
 * @author Alberto Martínez
 * @version 26/11/2019
 * Mail; archivo para el envio de emails cuando se registra un nuevo cliente en el sistema.
 */

class NewRegisterCompanieMail extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Nuevo Registro de Empresa')->markdown('emails.newregisteremail')->with(['data' => $this->data]);
    }
}
