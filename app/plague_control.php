<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class plague_control
 *
 * @package App
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @property int $id
 * @property int $id_service_order
 * @property string|null $control_areas
 * @property string|null $commentary
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|plague_control_picture[] $plague_control_pictures
 * @property-read int|null $plague_control_pictures_count
 * @method static Builder|plague_control newModelQuery()
 * @method static Builder|plague_control newQuery()
 * @method static Builder|plague_control query()
 * @method static Builder|plague_control whereCommentary($value)
 * @method static Builder|plague_control whereControlAreas($value)
 * @method static Builder|plague_control whereCreatedAt($value)
 * @method static Builder|plague_control whereId($value)
 * @method static Builder|plague_control whereIdServiceOrder($value)
 * @method static Builder|plague_control whereUpdatedAt($value)
 * @mixin Eloquent
 */
class plague_control extends Model
{
    /**
     * @var string
     */
    protected $table = "plague_controls";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_service_order',
        'control_areas',
        'quantity',
        'commentary'
    ];

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación uno a muchos con tabla plague_control_pictures
    */
    public function plague_control_pictures()
    {
        return $this->hasMany(plague_control_picture::class);
    }
}
