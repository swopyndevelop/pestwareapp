<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class product_unit
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $id_company
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|product_unit newModelQuery()
 * @method static Builder|product_unit newQuery()
 * @method static Builder|product_unit query()
 * @method static Builder|product_unit whereCreatedAt($value)
 * @method static Builder|product_unit whereId($value)
 * @method static Builder|product_unit whereIdCompany($value)
 * @method static Builder|product_unit whereName($value)
 * @method static Builder|product_unit whereUpdatedAt($value)
 * @mixin Eloquent
 */
class product_unit extends Model
{
    /**
     * @var string
     */
    protected $table = "product_units";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'profile_job_center_id',
        'visible'
    ];
}
