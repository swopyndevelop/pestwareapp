<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductBilling
 * @package App
 * @package App
 * @property int $id
 * @property string $name
 * @property string $identification_number
 * @property string $description
 * @property double $unit_price
 * @property string $property_account
 * @property string $unit_code_billing_id
 * @property string $product_code_billing_id
 * @property string $unit_code_billing
 * @property string $product_code_billing
 * @property string $iva
 * @property string $ieps
 * @property string $iva_ret
 * @property string $isr
 * @property int $profile_job_center_id
 * @property int $id_company
 * @mixin Eloquent
 */
class ProductBilling extends Model
{
    protected $table = 'products_billing';

    protected $fillable = [
        'name',
        'identification_number',
        'description',
        'unit_price',
        'property_account',
        'unit_code_billing_id',
        'product_code_billing_id',
        'unit_code_billing',
        'product_code_billing',
        'iva',
        'ieps',
        'iva_ret',
        'isr',
        'profile_job_center_id',
        'id_company'
    ];
}
