<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\voucher
 *
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $companie
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|voucher newModelQuery()
 * @method static Builder|voucher newQuery()
 * @method static Builder|voucher query()
 * @method static Builder|voucher whereCompanie($value)
 * @method static Builder|voucher whereCreatedAt($value)
 * @method static Builder|voucher whereId($value)
 * @method static Builder|voucher whereName($value)
 * @method static Builder|voucher whereUpdatedAt($value)
 * @mixin Eloquent
 */
class voucher extends Model
{
    /**
     * @var string
     */
    protected $table = "vouchers";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'companie',
        'visible',
        'profile_job_center_id',
    ];

}
