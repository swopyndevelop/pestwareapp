<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\inventory_adjustment
 *
 * @property int $id
 * @property string $id_adjustment
 * @property int $id_user
 * @property int $id_storehouse
 * @property int $id_company
 * @property string $comments
 * @property int $units_before
 * @property float $quantity_before
 * @property float $total_before
 * @property int $units_adjustment
 * @property float $quantity_adjustment
 * @property float $total_adjustment
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|inventory_adjustment newModelQuery()
 * @method static Builder|inventory_adjustment newQuery()
 * @method static Builder|inventory_adjustment query()
 * @method static Builder|inventory_adjustment whereComments($value)
 * @method static Builder|inventory_adjustment whereCreatedAt($value)
 * @method static Builder|inventory_adjustment whereId($value)
 * @method static Builder|inventory_adjustment whereIdAdjustment($value)
 * @method static Builder|inventory_adjustment whereIdCompany($value)
 * @method static Builder|inventory_adjustment whereIdStorehouse($value)
 * @method static Builder|inventory_adjustment whereIdUser($value)
 * @method static Builder|inventory_adjustment whereQuantityAdjustment($value)
 * @method static Builder|inventory_adjustment whereQuantityBefore($value)
 * @method static Builder|inventory_adjustment whereTotalAdjustment($value)
 * @method static Builder|inventory_adjustment whereTotalBefore($value)
 * @method static Builder|inventory_adjustment whereUnitsAdjustment($value)
 * @method static Builder|inventory_adjustment whereUnitsBefore($value)
 * @method static Builder|inventory_adjustment whereUpdatedAt($value)
 * @mixin Eloquent
 */
class inventory_adjustment extends Model
{
    /**
     * @var string
     */
    protected $table = "inventory_adjustments";

    /**
     * @var array
     */

    protected $fillable = [
        'id',
        'id_adjustment',
        'id_user',
        'id_storehouse',
        'id_company',
        'comments',
        'units_before',
        'quantity_before',
        'total_before',
        'units_adjustment',
        'quantity_adjustment',
        'total_adjustment'
    ];
}
