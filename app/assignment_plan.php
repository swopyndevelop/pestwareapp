<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class assignment_plan
 *
 * @package App
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|training_plan[] $training_plans
 * @property-read int|null $training_plans_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|assignment_plan newModelQuery()
 * @method static Builder|assignment_plan newQuery()
 * @method static Builder|assignment_plan query()
 * @method static Builder|assignment_plan whereCreatedAt($value)
 * @method static Builder|assignment_plan whereId($value)
 * @method static Builder|assignment_plan whereUpdatedAt($value)
 * @mixin Eloquent
 */
class assignment_plan extends Model
{
    /**
     * @var string
     */
    protected $table = "assignment_plans";

    /**
     * @var array
     */
    protected $fillable = [
        'id'
    ];

    /**
    * @author Olga Rodríguez
    * @version 10/07/2019
    *Función para relación muchos a muchos con tabla users
    */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 10/07/2019
    *Función para relación muchos a muchos con tabla training_plans
    */
    public function training_plans()
    {
        return $this->belongsToMany(training_plan::class);
    }
}
