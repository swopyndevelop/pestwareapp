<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerDocumentation
 * @package App
 * @property int $id
 * @property int $id_customer
 * @property string $section_name
 * @property string $subtitle
 * @property string $description
 * @property string $url_document
 * @property int $position
 * @property string $visible
 * @mixin Eloquent
 */
class CustomerDocumentation extends Model
{
    /**
     * @var string
     */
    protected $table = "customer_documentation";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_customer',
        'section_name',
        'subtitle',
        'description',
        'url_document',
        'position',
        'visible'
    ];
}
