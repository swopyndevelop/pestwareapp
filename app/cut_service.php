<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\cut_service
 *
 * @property int $id
 * @property int $id_cut
 * @property int $id_service
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|cut_service newModelQuery()
 * @method static Builder|cut_service newQuery()
 * @method static Builder|cut_service query()
 * @method static Builder|cut_service whereCreatedAt($value)
 * @method static Builder|cut_service whereId($value)
 * @method static Builder|cut_service whereIdCut($value)
 * @method static Builder|cut_service whereIdService($value)
 * @method static Builder|cut_service whereUpdatedAt($value)
 * @mixin Eloquent
 */
class cut_service extends Model
{
    protected $table = "cut_services";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_cut',
        'id_service'
    ];
}
