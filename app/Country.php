<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Course
 * @package App
 * @property int $id
 * @property string $name
 * @property string $code_country
 * @property double $iva_country
 * @property double $isr_country
 * @property string|null $coin_country
 * @property string|null $symbol_country
 * @mixin Eloquent
 */

class Country extends Model
{
    //
    //
    protected $table = 'countries';

    protected $fillable =
        [
            'name',
            'code_country',
            'iva_country',
            'isr_country',
            'coin_country',
            'symbol_country'
        ];
}
