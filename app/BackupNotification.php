<?php namespace App;


namespace App;


use Illuminate\Notifications\Notifiable;

class BackupNotification
{
    use Notifiable;

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return "https://hooks.slack.com/services/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";
    }
}