<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class general_document
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property string|null $text
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|general_document newModelQuery()
 * @method static Builder|general_document newQuery()
 * @method static Builder|general_document query()
 * @method static Builder|general_document whereCreatedAt($value)
 * @method static Builder|general_document whereId($value)
 * @method static Builder|general_document whereText($value)
 * @method static Builder|general_document whereUpdatedAt($value)
 * @mixin Eloquent
 */
class general_document extends Model
{
    /**
     * @var string
     */
    protected $table = "general_documents";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'text'
    ];
}
