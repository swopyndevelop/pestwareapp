<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\CourseProgress
 *
 * @property int $id
 * @property int $user_id
 * @property int $training_id
 * @property int $progress
 * @property float|null $course_grades
 * @property int|null $correct
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read training $training
 * @method static Builder|CourseProgress newModelQuery()
 * @method static Builder|CourseProgress newQuery()
 * @method static Builder|CourseProgress query()
 * @method static Builder|CourseProgress whereCorrect($value)
 * @method static Builder|CourseProgress whereCourseGrades($value)
 * @method static Builder|CourseProgress whereCreatedAt($value)
 * @method static Builder|CourseProgress whereId($value)
 * @method static Builder|CourseProgress whereProgress($value)
 * @method static Builder|CourseProgress whereTrainingId($value)
 * @method static Builder|CourseProgress whereUpdatedAt($value)
 * @method static Builder|CourseProgress whereUserId($value)
 * @mixin Eloquent
 */
class CourseProgress extends Model
{
    protected $table='course_progresses';
    protected $primarykey = 'id';
    protected $fillable = ['id','user_id','training_id','progress','course_grades', 'correct'];

    public function training(){
    	return $this->belongsTo(Training::class);
    }
}
