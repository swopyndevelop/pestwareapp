<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class extra
 *
 * @package App
 * @author Manuel Mendoza
 * @version 29/03/2019
 * @property int $id
 * @property string $folio_invoice
 * @property string $id_invoice
 * @property string $uuid
 * @property int $id_customer
 * @property string|null $description
 * @property float $amount
 * @property int $id_status
 * @property Carbon $date
 * @property int $id_company
 * @property int|null $invoice_complement
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|extra newModelQuery()
 * @method static Builder|extra newQuery()
 * @method static Builder|extra query()
 * @method static Builder|extra whereAmount($value)
 * @method static Builder|extra whereCreatedAt($value)
 * @method static Builder|extra whereDescription($value)
 * @method static Builder|extra whereId($value)
 * @method static Builder|extra whereIdCompany($value)
 * @method static Builder|extra whereName($value)
 * @method static Builder|extra whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Invoice extends Model
{
    /** @var string */
    protected $table = "invoices";

    /** @var array */
    protected $fillable = [
        'id',
        'folio_invoice',
        'id_invoice',
        'uuid',
        'id_customer',
        'description',
        'amount',
        'id_status',
        'date',
        'id_company',
        'invoice_complement'
    ];
}
