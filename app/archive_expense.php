<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\archive_expense
 *
 * @property int $id
 * @property int $id_expense
 * @property string $file_route
 * @property int|null $type
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|archive_expense newModelQuery()
 * @method static Builder|archive_expense newQuery()
 * @method static Builder|archive_expense query()
 * @method static Builder|archive_expense whereCompanie($value)
 * @method static Builder|archive_expense whereCreatedAt($value)
 * @method static Builder|archive_expense whereFileRoute($value)
 * @method static Builder|archive_expense whereId($value)
 * @method static Builder|archive_expense whereIdExpense($value)
 * @method static Builder|archive_expense whereType($value)
 * @method static Builder|archive_expense whereUpdatedAt($value)
 * @mixin Eloquent
 */
class archive_expense extends Model
{
    /**
     * @var string
     */
    protected $table = "archive_expenses";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_expense',
        'file_route',
        'type',
        'companie'
    ];

}
