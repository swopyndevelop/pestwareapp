<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class quotation_custom_quote
 *
 * @package App
 * @author Olga Rodríguez
 * @version 16/01/2019
 * @property int $id
 * @property int $quotation_id
 * @property int $custom_quote_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|quotation_custom_quote newModelQuery()
 * @method static Builder|quotation_custom_quote newQuery()
 * @method static Builder|quotation_custom_quote query()
 * @method static Builder|quotation_custom_quote whereCreatedAt($value)
 * @method static Builder|quotation_custom_quote whereCustomQuoteId($value)
 * @method static Builder|quotation_custom_quote whereId($value)
 * @method static Builder|quotation_custom_quote whereQuotationId($value)
 * @method static Builder|quotation_custom_quote whereUpdatedAt($value)
 * @mixin Eloquent
 */
class quotation_custom_quote extends Model
{
    /**
     * @var string
     */
    protected $table = "quotation_custom_quote";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'quotation_id',
        'custom_quote_id'
    ];
}
