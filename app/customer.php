<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class customer
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property string $name
 * @property int|null $user_id
 * @property string|null $establishment_name
 * @property string|null $cellphone
 * @property string|null $cellphone_main
 * @property int $establishment_id
 * @property string|null $colony
 * @property string|null $municipality
 * @property int|null $source_origin_id
 * @property int $id_profile_job_center
 * @property int $companie
 * @property boolean $show_price
 * @property int|null $days_expiration_certificate
 * @property string $visible
 * @property boolean $is_main
 * @property int|null $customer_main_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|customer newModelQuery()
 * @method static Builder|customer newQuery()
 * @method static Builder|customer query()
 * @method static Builder|customer whereCellphone($value)
 * @method static Builder|customer whereColony($value)
 * @method static Builder|customer whereCompanie($value)
 * @method static Builder|customer whereCreatedAt($value)
 * @method static Builder|customer whereEstablishmentId($value)
 * @method static Builder|customer whereEstablishmentName($value)
 * @method static Builder|customer whereId($value)
 * @method static Builder|customer whereMunicipality($value)
 * @method static Builder|customer whereName($value)
 * @method static Builder|customer whereSourceOriginId($value)
 * @method static Builder|customer whereUpdatedAt($value)
 * @method static Builder|customer whereUserId($value)
 * @mixin Eloquent
 */
class customer extends Model
{
    /**
     * Table name.
     * @var string
     */
    protected $table = "customers";

    /**
     * array of attributes table
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'establishment_name',
        'cellphone',
        'cellphone_main',
        'plague_id',
        'establishment_id',
        'colony',
        'municipality',
        'source_origin_id',
        'id_profile_job_center',
        'companie',
        'visible',
        'show_price',
        'days_expiration_certificate',
        'is_main',
        'customer_main_id'
    ];
}
