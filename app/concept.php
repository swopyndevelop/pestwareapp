<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\concept
 *
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $type
 * @property int $id_company
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|concept newModelQuery()
 * @method static Builder|concept newQuery()
 * @method static Builder|concept query()
 * @method static Builder|concept whereCreatedAt($value)
 * @method static Builder|concept whereId($value)
 * @method static Builder|concept whereIdCompany($value)
 * @method static Builder|concept whereName($value)
 * @method static Builder|concept whereType($value)
 * @method static Builder|concept whereUpdatedAt($value)
 * @mixin Eloquent
 */
class concept extends Model
{
    /**
     * @var string
     */
    protected $table = "concepts";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'type',
        'visible',
        'profile_job_center_id'
    ];

}
