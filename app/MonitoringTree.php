<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MonitoringTree
 * @package App
 * @property int $id
 * @property int $id_monitoring
 * @property int $id_company
 * @property string $id_node
 * @property string $parent
 * @property string $text
 * @property int $id_type_station
 * @property string $visible
 * @property boolean $loan
 * @mixin Eloquent
 */

class MonitoringTree extends Model
{
    protected $table = 'monitoring_trees';

    protected $fillable = [
        'id_monitoring',
        'id_company',
        'id_node',
        'parent',
        'text',
        'id_type_station',
        'visible',
        'loan'
    ];
}
