<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Course
 * @package App
 * @property int $id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property string $duration
 */

class Course extends Model
{
    //
    protected $table = 'courses';

    protected $fillable = ['name','description','price','duration'];
}
