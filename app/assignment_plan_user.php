<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class assignment_plan_user
 *
 * @package App
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @property int $id
 * @property int $assignment_plan_id
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|assignment_plan_user newModelQuery()
 * @method static Builder|assignment_plan_user newQuery()
 * @method static Builder|assignment_plan_user query()
 * @method static Builder|assignment_plan_user whereAssignmentPlanId($value)
 * @method static Builder|assignment_plan_user whereCreatedAt($value)
 * @method static Builder|assignment_plan_user whereId($value)
 * @method static Builder|assignment_plan_user whereUpdatedAt($value)
 * @method static Builder|assignment_plan_user whereUserId($value)
 * @mixin Eloquent
 */
class assignment_plan_user extends Model
{
    /**
     * @var string
     */
    protected $table = "assignment_plan_user";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'assignment_plan_id',
        'user_id'
    ];
}
