<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerBankAccount
 * @package App
 * @property int $id
 * @property int $id_customer
 * @property string $bank
 * @property string $account
 * @property boolean $is_default
 * @mixin Eloquent
 */
class CustomerBankAccount extends Model
{
    protected $table = 'customer_bank_accounts';

    protected $fillable = [
        'id_customer',
        'bank',
        'account',
        'is_default'
    ];
}
