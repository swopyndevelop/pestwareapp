<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class treeJobCenter
 *
 * @package App
 * @author Olga Rodríguez
 * @version 11/02/2019
 * @property int $id_inc
 * @property int $id
 * @property int $company_id
 * @property string $parent
 * @property string $text
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|treeJobCenter newModelQuery()
 * @method static Builder|treeJobCenter newQuery()
 * @method static Builder|treeJobCenter query()
 * @method static Builder|treeJobCenter whereCompanyId($value)
 * @method static Builder|treeJobCenter whereCreatedAt($value)
 * @method static Builder|treeJobCenter whereId($value)
 * @method static Builder|treeJobCenter whereIdInc($value)
 * @method static Builder|treeJobCenter whereParent($value)
 * @method static Builder|treeJobCenter whereText($value)
 * @method static Builder|treeJobCenter whereUpdatedAt($value)
 * @mixin Eloquent
 */
class treeJobCenter extends Model
{
    /**
     * @var string
     */
    protected $table = 'tree_job_centers';
	protected $primarykey='id_inc';

	/**
     * @var array
     */
	protected $fillable = ['id_inc','id','company_id','parent','text'];
}
