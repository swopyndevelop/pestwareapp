<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class pay_type
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|pay_type newModelQuery()
 * @method static Builder|pay_type newQuery()
 * @method static Builder|pay_type query()
 * @method static Builder|pay_type whereCreatedAt($value)
 * @method static Builder|pay_type whereId($value)
 * @method static Builder|pay_type whereName($value)
 * @method static Builder|pay_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class pay_type extends Model
{
    /**
     * @var string
     */
    protected $table = "pay_types";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];
}
