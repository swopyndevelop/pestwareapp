<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_office_function
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_office_functions_id
 * @property int $job_title_profiles_id
 * @property string|null $function
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_office_function newModelQuery()
 * @method static Builder|job_office_function newQuery()
 * @method static Builder|job_office_function query()
 * @method static Builder|job_office_function whereCreatedAt($value)
 * @method static Builder|job_office_function whereFunction($value)
 * @method static Builder|job_office_function whereId($value)
 * @method static Builder|job_office_function whereJobOfficeFunctionsId($value)
 * @method static Builder|job_office_function whereJobTitleProfilesId($value)
 * @method static Builder|job_office_function whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_office_function extends Model
{
    /**
     * @var string
     */
    protected $table = "job_office_functions";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_office_functions_id',
        'job_title_profiles_id',
        'function'
    ];
}
