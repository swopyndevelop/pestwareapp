<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ReportsSlack extends Model
{
    use Notifiable;

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return "https://hooks.slack.com/services/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";
    }
}
