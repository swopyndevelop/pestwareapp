<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_experience
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_experiences_id
 * @property int $job_title_profiles_id
 * @property string|null $experience_name
 * @property string|null $experience_time
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_experience newModelQuery()
 * @method static Builder|job_title_experience newQuery()
 * @method static Builder|job_title_experience query()
 * @method static Builder|job_title_experience whereCreatedAt($value)
 * @method static Builder|job_title_experience whereExperienceName($value)
 * @method static Builder|job_title_experience whereExperienceTime($value)
 * @method static Builder|job_title_experience whereId($value)
 * @method static Builder|job_title_experience whereJobTitleExperiencesId($value)
 * @method static Builder|job_title_experience whereJobTitleProfilesId($value)
 * @method static Builder|job_title_experience whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_experience extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_experiences";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_experiences_id',
        'job_title_profiles_id',
        'experience_name',
        'experience_time'
    ];
}
