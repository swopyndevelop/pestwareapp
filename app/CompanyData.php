<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyData extends Model
{
    /**
     * @var string
     */
    protected $table = "company_datas";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_company',
        'city',
        'country_code',
        'country_name',
        'ip',
        'latitude',
        'longitude',
        'region_code',
        'region_name',
        'time_zone',
        'zip_code'
    ];
}
