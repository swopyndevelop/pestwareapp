<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CheckListImage
 * @package App
 * @property int $id
 * @property int $id_inspection
 * @property int $id_station
 * @property string $urlImage
 */

class CheckListImage extends Model
{
    protected $table = 'check_list_images';

    protected $fillable = ['id_inspection', 'id_station', 'urlImage'];
}
