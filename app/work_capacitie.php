<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class work_capacitie
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int|null $work_capacities_id
 * @property int $profile_job_centers_id
 * @property int $jobtitle_capacity
 * @property int $quantity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|work_capacitie newModelQuery()
 * @method static Builder|work_capacitie newQuery()
 * @method static Builder|work_capacitie query()
 * @method static Builder|work_capacitie whereCreatedAt($value)
 * @method static Builder|work_capacitie whereId($value)
 * @method static Builder|work_capacitie whereJobtitleCapacity($value)
 * @method static Builder|work_capacitie whereProfileJobCentersId($value)
 * @method static Builder|work_capacitie whereQuantity($value)
 * @method static Builder|work_capacitie whereUpdatedAt($value)
 * @method static Builder|work_capacitie whereWorkCapacitiesId($value)
 * @mixin Eloquent
 */
class work_capacitie extends Model
{
    /**
     * @var string
     */
    protected $table = "work_capacities";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'work_capacities_id',
        'profile_job_centers_id',
        'jobtitle_capacity',
        'quantity'
    ];
}
