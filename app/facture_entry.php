<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\facture_entry
 *
 * @property int $id
 * @property int $id_entry
 * @property string $file_route
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|facture_entry newModelQuery()
 * @method static Builder|facture_entry newQuery()
 * @method static Builder|facture_entry query()
 * @method static Builder|facture_entry whereCreatedAt($value)
 * @method static Builder|facture_entry whereFileRoute($value)
 * @method static Builder|facture_entry whereId($value)
 * @method static Builder|facture_entry whereIdEntry($value)
 * @method static Builder|facture_entry whereUpdatedAt($value)
 * @mixin Eloquent
 */
class facture_entry extends Model
{
    //
    /**
     * @var string
     */
    protected $table = "facture_entrys";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_entry',
        'file_route'
    ];
}
