<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class application_method
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $id_company
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|application_method newModelQuery()
 * @method static Builder|application_method newQuery()
 * @method static Builder|application_method query()
 * @method static Builder|application_method whereCreatedAt($value)
 * @method static Builder|application_method whereId($value)
 * @method static Builder|application_method whereIdCompany($value)
 * @method static Builder|application_method whereName($value)
 * @method static Builder|application_method whereUpdatedAt($value)
 * @mixin Eloquent
 */
class application_method extends Model
{
    /**
     * @var string
     */
    protected $table = "application_methods";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'visible',
        'profile_job_center_id'
    ];
}
