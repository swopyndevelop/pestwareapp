<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    /** @var string */
    protected $table = "attendances";
    /** @var array */
    protected $fillable = [
        'id',
        'id_employee',
        'id_company',
        'date',
        'start_hour',
        'end_hour',
        'start_latitude',
        'start_longitude',
        'end_latitude',
        'end_longitude',
    ];
}
