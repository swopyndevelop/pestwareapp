<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class DescriptionCustomQuote
 *
 * @package App
 * @author Alberto Martínez
 * @version 11/04/2021
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $id_company
 * @property int $profile_job_center_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin Eloquent
 */
class DescriptionCustomQuote extends Model
{
    /**
     * @var string
     */
    protected $table = "description_custom_quotes";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'id_company',
        'profile_job_center_id'
    ];
}
