<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Product_Tax
 *
 * @package App
 * @author Manuel Mendoza
 * @version 01/06/2021
 * @property int $id
 * @property int $id_product
 * @property int $id_tax
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|discount_quotation newModelQuery()
 * @method static Builder|discount_quotation newQuery()
 * @method static Builder|discount_quotation query()
 * @method static Builder|discount_quotation whereCreatedAt($value)
 * @method static Builder|discount_quotation whereDiscountId($value)
 * @method static Builder|discount_quotation whereId($value)
 * @method static Builder|discount_quotation whereQuotationId($value)
 * @method static Builder|discount_quotation whereUpdatedAt($value)
 * @mixin Eloquent
 */

class Product_Tax extends Model
{
    /**
     * @var string
     */
    protected $table = "product_tax";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_product',
        'id_tax'
    ];
}
