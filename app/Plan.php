<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Plan
 * @package App
 * @property int $id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property int $quantity
 * @property string $type
 * @property string $id_conekta
 * @property string $id_price_stripe
 * @mixin Eloquent
 */
class Plan extends Model
{
    protected $table = 'plans';

    protected $fillable = ['name', 'description', 'price', 'quantity', 'type', 'id_conekta', 'id_price_stripe'];
}
