<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class discount_quotation
 *
 * @package App
 * @author Olga Rodríguez
 * @version 20/01/2019
 * @property int $id
 * @property int $quotation_id
 * @property int $discount_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|discount_quotation newModelQuery()
 * @method static Builder|discount_quotation newQuery()
 * @method static Builder|discount_quotation query()
 * @method static Builder|discount_quotation whereCreatedAt($value)
 * @method static Builder|discount_quotation whereDiscountId($value)
 * @method static Builder|discount_quotation whereId($value)
 * @method static Builder|discount_quotation whereQuotationId($value)
 * @method static Builder|discount_quotation whereUpdatedAt($value)
 * @mixin Eloquent
 */
class discount_quotation extends Model
{
    /**
     * @var string
     */
    protected $table = "discount_quotation";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'quotation_id',
        'discount_id'
    ];
}
