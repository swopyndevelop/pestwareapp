<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class inspection_picture
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/06/2019
 * @property int $id
 * @property int $place_inspection_id
 * @property string $file_route
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read place_inspection $place_inspection
 * @method static Builder|inspection_picture newModelQuery()
 * @method static Builder|inspection_picture newQuery()
 * @method static Builder|inspection_picture query()
 * @method static Builder|inspection_picture whereCreatedAt($value)
 * @method static Builder|inspection_picture whereFileRoute($value)
 * @method static Builder|inspection_picture whereId($value)
 * @method static Builder|inspection_picture wherePlaceInspectionId($value)
 * @method static Builder|inspection_picture whereUpdatedAt($value)
 * @mixin Eloquent
 */
class inspection_picture extends Model
{
    /**
     * @var string
     */
    protected $table = "inspection_pictures";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'place_inspection_id',
        'file_route'
    ];

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación uno a muchos con tabla place_inspections
    */
    public function place_inspection()
    {
        return $this->belongsTo(place_inspection::class);
    }
}
