<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\article_expense
 *
 * @property int $id
 * @property int $id_expense
 * @property string $concept
 * @property float $total
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|article_expense newModelQuery()
 * @method static Builder|article_expense newQuery()
 * @method static Builder|article_expense query()
 * @method static Builder|article_expense whereCompanie($value)
 * @method static Builder|article_expense whereConcept($value)
 * @method static Builder|article_expense whereCreatedAt($value)
 * @method static Builder|article_expense whereId($value)
 * @method static Builder|article_expense whereIdExpense($value)
 * @method static Builder|article_expense whereTotal($value)
 * @method static Builder|article_expense whereUpdatedAt($value)
 * @mixin Eloquent
 */
class article_expense extends Model
{
    /**
     * @var string
     */
    protected $table = "article_expenses";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_expense',
        'concept',
        'total'
    ];

}
