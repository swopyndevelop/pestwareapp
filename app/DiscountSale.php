<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class DiscountSale
 *
 * @package App
 * @author Manuel/Alberto
 * @version 03/08/2021
 * @property int $id
 * @property int $id_sale
 * @property int $id_discount
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|discount_quotation newModelQuery()
 * @method static Builder|discount_quotation newQuery()
 * @method static Builder|discount_quotation query()
 * @method static Builder|discount_quotation whereCreatedAt($value)
 * @method static Builder|discount_quotation whereDiscountId($value)
 * @method static Builder|discount_quotation whereId($value)
 * @method static Builder|discount_quotation whereQuotationId($value)
 * @method static Builder|discount_quotation whereUpdatedAt($value)
 * @mixin Eloquent
 */
class DiscountSale extends Model
{
    /**
     * @var string
     */
    protected $table = "discounts_sale";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_sale',
        'id_discount'
    ];
}
