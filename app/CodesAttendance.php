<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodesAttendance extends Model
{
    /** @var string */
    protected $table = "codes_attendance";
    /** @var array */
    protected $fillable = [
        'id',
        'code',
        'date',
        'hour',
        'is_active'
    ];
}
