<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\entry
 *
 * @property int $id
 * @property string $id_entry
 * @property int $id_user
 * @property string $origin
 * @property int $id_job_center
 * @property int $companie
 * @property float|null $total
 * @property string $visible
 * @property int $id_purchase
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|entry newModelQuery()
 * @method static Builder|entry newQuery()
 * @method static Builder|entry query()
 * @method static Builder|entry whereCompanie($value)
 * @method static Builder|entry whereCreatedAt($value)
 * @method static Builder|entry whereId($value)
 * @method static Builder|entry whereIdEntry($value)
 * @method static Builder|entry whereIdJobCenter($value)
 * @method static Builder|entry whereIdUser($value)
 * @method static Builder|entry whereOrigin($value)
 * @method static Builder|entry whereTotal($value)
 * @method static Builder|entry whereUpdatedAt($value)
 * @mixin Eloquent
 */
class entry extends Model
{
    //
    /**
     * @var string
     */
    protected $table = "entrys";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_entry',
        'id_user',
        'origin',
        'id_job_center',
        'total',
        'created_at',
        'companie',
        'visible',
        'id_purchase'
    ];
}
