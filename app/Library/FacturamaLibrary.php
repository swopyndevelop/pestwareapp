<?php

namespace App\Library;

use Auth;
use Exception;
use Facturama\Client;
use stdClass;

class FacturamaLibrary
{

    /**
     * Create new instance of Facturama for authenticate in api.
     */
    private static function getInstance() {
        $password = env('FACTURAMA_PASSWORD');
        //if (Auth::user()->companie == 187 || Auth::user()->companie == 220) $password = env('FACTURAMA_DEV_PASSWORD');
        return new Client(env('FACTURAMA_USERNAME'), $password);
        //$baseUri = 'https://apisandbox.facturama.mx';
        //$instance->setApiUrl($baseUri);
    }

    #region SECTION CSDs (RFC, .cert, .key and password)
    /**
     * Load csds for create cfdi.
     *
     * @param $params
     * @return array|stdClass|null
     */
    public static function loadDocuments($params) {
        try {
            return self::getInstance()->post('api-lite/csds',$params);
        } catch (Exception $exception) {
            return [
                'code' => 500,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Delete csd by rfc.
     *
     * @param $rfc
     * @return array|stdClass|null
     */
    public static function deleteCSDbyRFC($rfc) {
        try {
            return self::getInstance()->delete('api-lite/csds/'.$rfc);
        } catch (Exception $exception) {
            return [
                'code' => 500,
                'message' => $exception->getMessage()
            ];
        }
    }
    #endregion SECTION CSDs (RFC, .cert, .key and password)

    #region SECTION CFDI (INVOICE)
    /**
     * Create new invoice. (CFDI)
     * @param $params
     * @return array
     */
    public static function createInvoice($params) {
        try {
            $response = self::getInstance()->post('api-lite/3/cfdis', $params);
            return [
                'code' => 201,
                'data' => $response
            ];
        } catch (Exception $exception) {
            return [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Download invoice by id facturama and type document. (html, Xml, pdf)
     */
    public static function downloadInvoice($invoiceId, $typeDocument) {
        $document = $typeDocument;
        $type = 'IssuedLite';
        $id = $invoiceId;
        $params = [];
        return self::getInstance()->get('cfdi/'.$document.'/'.$type.'/'.$id, $params);
    }

    /**
     * Get all invoices by status and type.
     * @param $status
     * @param $type
     * @return array|stdClass|null
     */
    public static function getInvoicesByFilters($status, $type) {
        try {
            $params = [
                'type' => 'issuedLite',
                'status' => 'all',
            ];
            return self::getInstance()->get('Cfdi', $params);
        } catch (Exception $exception) {
            return [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * CFDI cancellation.
     * https://apisandbox.facturama.mx/guias/api-multi/cfdi/cancelacion
     * @param $invoiceId
     * @return array
     */
    public static function cancelInvoice($invoiceId) {
        try {
            $data = self::getInstance()->delete('api-lite/cfdis/'.$invoiceId);
            return [
                'code' => 200,
                'data' => $data
            ];
        } catch (Exception $exception) {
            return [
                'code' => 500,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * CFDI complement.
     * https://apisandbox.facturama.mx/guias/api-web/cfdi/complemento-pago
     * @param $params
     * @return array
     */
    public static function complementInvoice($params) {
        try {
            $data = self::getInstance()->post('api-lite/2/cfdis/', $params);
            return [
                'code' => 201,
                'data' => $data
            ];
        } catch (Exception $exception) {
            return [
                'code' => 500,
                'message' => $exception->getMessage()
            ];
        }
    }
    #endregion SECTION CFDI (INVOICE)

    #region CATALOGS SAT
    /**
     * The product and service codes are specified by the SAT,
     * They are used to determine the turn or context and therefore one must be placed that is consistent with the concept.
     * Get catalogs for products or services by keyword.
     * @param $keyword //Ej. desarrollo
     * @return array|stdClass|null
     */
    public static function getCatalogsProductsOrServicesByKeyword($keyword) {
        try {
            return self::getInstance()->get('catalogs/ProductsOrServices', ['keyword' => $keyword]);
        } catch (Exception $exception) {
            return [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * The postal code is the element used as the place of dispatch, it is the only mandatory value to be used as the fiscal address of the issuer.
     * The postal code used in the CFDI must be one of those included in this list, otherwise, the CFDI will be rejected by the PAC.
     * Get catalogs of postal codes by keyword.
     * @param $keyword //Ej. 7818
     * @return array|stdClass|null
     */
    public static function getCatalogsPostalCodesByKeyword($keyword) {
        try {
            return self::getInstance()->get('catalogs/PostalCodes', ['keyword' => $keyword]);
        } catch (Exception $exception) {
            return [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * The currency in the CFDI must be one of this catalog established by the SAT.
     * @return array|stdClass|null
     */
    public static function getCatalogsCurrencies() {
        try {
            return self::getInstance()->get('catalogs/currencies');
        } catch (Exception $exception) {
            return [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Catalog with the payment methods specified by the SAT, the value placed in the CFDI must be one of this list.
     * @return array|stdClass|null
     */
    public static function getCatalogsPaymentForms() {
        try {
            return self::getInstance()->get('catalogs/PaymentForms');
        } catch (Exception $exception) {
            return [];
        }
    }

    /**
     * Catalog with the methods specified by the SAT, the value placed in the CFDI must be one of this list.
     * @return array|stdClass|null
     */
    public static function getCatalogsPaymentMethods() {
        try {
            return self::getInstance()->get('catalogs/PaymentMethods');
        } catch (Exception $exception) {
            return [];
        }
    }

    /**
     * Catalog with the tax regimes specified by the SAT, the value placed in the CFDI must be one of this list.
     * @return array|stdClass|null
     */
    public static function getCatalogsFiscalRegimens() {
        try {
            return self::getInstance()->get('catalogs/FiscalRegimens');
        } catch (Exception $exception) {
            return [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Catalog with the CFDI uses specified by the SAT, the value placed in the CFDI must be one of this list.
     * The consultation of uses of the cfdi, requires an RFC since the uses vary according to whether it is a physical or moral person.
     * The use of CFDI usually goes in the field CfdiUse in the node Receiver.
     * @return array|stdClass|null
     */
    public static function getCatalogsCfdiUses() {
        try {
            return self::getInstance()->get('catalogs/CfdiUses');
        } catch (Exception $exception) {
            return [];
        }
    }

    /**
     * The catalog of CFDI names is an internal Facturama catalog and allows you to select the descriptive "name" that the PDF will have,
     * This name is shown with red letters on the top of the receipt.
     * @return array|stdClass|null
     */
    public static function getCatalogsCfdiNames() {
        try {
            return self::getInstance()->get('catalogs/NameIds');
        } catch (Exception $exception) {
            return [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
        }
    }
    #endregion

}