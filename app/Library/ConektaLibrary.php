<?php

namespace App\Library;

use Conekta;
use Dompdf\Exception;

class ConektaLibrary {

    /**
     *
     */
    private static function setPrivateApiKey() {
        Conekta\Conekta::setApiKey(env('CONEKTA_PRIVATE_KEY'));
    }

    /**
     * @param $clientData
     * @param $subscriptionPlan
     * @param $conektaTokenId
     * @return bool|mixed
     */
    public static function createClient($clientData, $subscriptionPlan, $conektaTokenId) {
        self::setPrivateApiKey();

        $data = [
            'name'  => $clientData->name,
            'email' => $clientData->email,
            'phone' => $clientData->phone,
            'corporate' => true,
            'plan_id' => $subscriptionPlan,
            'payment_sources' => [[
                'type' => "card",
                'token_id' => $conektaTokenId,
            ]]
        ];

        try {
            Conekta\Conekta::setApiVersion("2.0.0");
            $customer = Conekta\Customer::create($data);
        } catch(Conekta\ProcessingError $e) {
            return false;
        } catch(Conekta\ParameterValidationError $e) {
            return false;
        } catch(Exception $e) {
            return false;
        }

        return $customer;
    }

    /**
     * @param $conektaPlanId
     * @param $amount
     * @param $currency
     * @return bool
     */
    public static function createPlan($conektaPlanId, $amount, $currency) {
        self::setPrivateApiKey();

        $data = [
            'id'  => $conektaPlanId,
            'name'  => $conektaPlanId,
            'amount' => $amount,
            'currency' => $currency,
            'interval' => 'month',
            'frequency' => 1
        ];

        try {
            $plan = Conekta\Plan::create($data);
        } catch(Conekta\ProcessingError $e) {
            return false;
        } catch(Conekta\ParameterValidationError $e) {
            return false;
        } catch(Exception $e) {
            return false;
        }

        return $plan->id;
    }

    /**
     * @param $conektaPlanId
     * @return bool|mixed
     */
    public static function getPlan($conektaPlanId) {
        self::setPrivateApiKey();

        try {
            $plan = Conekta\Plan::find($conektaPlanId);
        } catch(Conekta\ProcessingError $e) {
            return false;
        } catch(Conekta\ParameterValidationError $e) {
            return false;
        } catch(Conekta\ResourceNotFoundError $e) {
            return false;
        } catch(Exception $e) {
            return false;
        }

        return $plan;
    }

}