<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Area
 * @package App
 * @package App
 * @property int $id
 * @property string $id_area
 * @property int $id_user
 * @property int $id_customer
 * @property int $is_inspection
 * @property string $visible
 * @property string $status
 * @property string $date_updated_file
 * @property int|null $no_parts
 * @property double|null $amount
 * @property string $date_updated_tree
 * @mixin Eloquent
 */
class Area extends Model
{
    protected $table = 'areas';

    protected $fillable = [
        'id_area',
        'id_user',
        'id_customer',
        'isInspection',
        'visible',
        'status',
        'date_updated_file',
        'date_updated_tree',
        'no_parts',
        'amount'
    ];
}
