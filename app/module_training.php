<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class module_training
 *
 * @package App
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @property int $id
 * @property int $module_id
 * @property int $training_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|module_training newModelQuery()
 * @method static Builder|module_training newQuery()
 * @method static Builder|module_training query()
 * @method static Builder|module_training whereCreatedAt($value)
 * @method static Builder|module_training whereId($value)
 * @method static Builder|module_training whereModuleId($value)
 * @method static Builder|module_training whereTrainingId($value)
 * @method static Builder|module_training whereUpdatedAt($value)
 * @mixin Eloquent
 */
class module_training extends Model
{
    /**
     * @var string
     */
    protected $table = "module_training";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'module_id',
        'training_id'
    ];
}
