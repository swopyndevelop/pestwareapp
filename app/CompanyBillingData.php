<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class CompanyBillingData
 *
 * @package App
 * @author Alberto Martínez
 * @version 05/12/2021
 * @property int $id
 * @property string $reason_social
 * @property string $rfc
 * @property string $email
 * @property string|null $cfdi_type
 * @property string|null $billing_street
 * @property string|null $billing_exterior_number
 * @property string|null $billing_interior_number
 * @property string|null $billing_colony
 * @property string|null $billing_postal_code
 * @property string|null $billing_municipality
 * @property string|null $billing_state
 * @property int $id_company
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin Eloquent
 */
class CompanyBillingData extends Model
{
    /**
     * Table name.
     * @var string
     */
    protected $table = "company_billing_data";

    /**
     * array of attributes table
     * @var array
     */
    protected $fillable = [
        'id',
        'reason_social',
        'rfc',
        'email',
        'cfdi_type',
        'billing_street',
        'billing_exterior_number',
        'billing_interior_number',
        'billing_colony',
        'billing_postal_code',
        'billing_municipality',
        'billing_state',
        'id_company'
    ];
}
