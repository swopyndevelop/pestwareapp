<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class employee
 *
 * @package App
 * @author Olga Rodríguez
 * @version 13/03/2019
 * @property int $id
 * @property int $employee_id
 * @property string $name
 * @property int $id_company
 * @property int|null $job_title_profile_id
 * @property int|null $profile_job_center_id
 * @property string|null $color
 * @property int|null $status
 * @property string|null $file_route_firm
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $visible
 * @method static Builder|employee newModelQuery()
 * @method static Builder|employee newQuery()
 * @method static Builder|employee query()
 * @method static Builder|employee whereColor($value)
 * @method static Builder|employee whereCreatedAt($value)
 * @method static Builder|employee whereEmployeeId($value)
 * @method static Builder|employee whereFileRouteFirm($value)
 * @method static Builder|employee whereId($value)
 * @method static Builder|employee whereIdCompany($value)
 * @method static Builder|employee whereJobTitleProfileId($value)
 * @method static Builder|employee whereName($value)
 * @method static Builder|employee whereProfileJobCenterId($value)
 * @method static Builder|employee whereStatus($value)
 * @method static Builder|employee whereUpdatedAt($value)
 * @mixin Eloquent
 */
class employee extends Model
{
    /**
     * @var string
     */
    protected $table = "employees";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'employee_id',
        'name',
        'id_company',
        'job_title_profile_id',
        'profile_job_center_id',
        'color',
        'visible'
    ];
}
