<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class DiscountSale
 *
 * @package App
 * @author Manuel/Alberto
 * @version 03/08/2021
 * @property int $id
 * @property string $name
 * @property string|null $url_web
 * @property string|null $url_facebook
 * @property string|null $number_whatsapp
 * @property string|null $number_phone
 * @property string|null $email
 * @property string|null $contact
 * @property string|null $address
 * @property string|null $colony
 * @property string|null $municipality
 * @property string|null $state
 * @property string|null $zip_code
 * @property string|null $license
 * @property string|null $date
 * @property string|null $health_officer
 * @property string|null $rfc
 * @property string|null $observations
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|discount_quotation newModelQuery()
 * @method static Builder|discount_quotation newQuery()
 * @method static Builder|discount_quotation query()
 * @method static Builder|discount_quotation whereCreatedAt($value)
 * @method static Builder|discount_quotation whereDiscountId($value)
 * @method static Builder|discount_quotation whereId($value)
 * @method static Builder|discount_quotation whereQuotationId($value)
 * @method static Builder|discount_quotation whereUpdatedAt($value)
 * @mixin Eloquent
 */

class EmailMassive extends Model
{
    /**
     * @var string
     */
    protected $table = "email_massives";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'url_web',
        'url_facebook',
        'number_whatsapp',
        'number_phone',
        'email',
        'contact',
        'address',
        'colony',
        'municipality',
        'state',
        'zip_code',
        'license',
        'date',
        'health_officer',
        'rfc',
        'observations'
    ];
}
