<?php


namespace App\Common;


use App\CustomLabel;

class CommonLabel
{
    public function getLabelsByJobCenter($idProfileJobCenter){

        $colony = self::mainQuery($idProfileJobCenter)->where('l.name', Constants::$COLONY)->first();
        $street = self::mainQuery($idProfileJobCenter)->where('l.name', Constants::$STREET)->first();
        $state = self::mainQuery($idProfileJobCenter)->where('l.name', Constants::$STATE)->first();
        $iva = self::mainQuery($idProfileJobCenter)->where('l.name', Constants::$IVA)->first();

        return [
            'street' => $street,
            'colony' => $colony,
            'state' => $state,
            'iva' => $iva
        ];
    }

    private function mainQuery($idProfileJobCenter){
        return CustomLabel::join('labels as l','custom_labels.id_label','l.id')
            ->where('id_profile_job_center', $idProfileJobCenter)
            ->select('custom_labels.*','l.id as idLabel');
    }
}