<?php


namespace App\Common;


class CommonFormats
{
    public static function convertToMoney($quantity) {
        return number_format($quantity, 2);
    }
}