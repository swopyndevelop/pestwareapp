<?php

namespace App\Common;

use App\Notifications;

class CommonNotifications
{
    /**
     * Created at new notification.
     * @param $title
     * @param $message
     * @param $jobCenterId
     * @param $companyId
     */
    public static function create($title, $message, $jobCenterId, $companyId) {
        $notification = new Notifications();
        $notification->title = $title;
        $notification->message = $message;
        $notification->id_profile_job_center = $jobCenterId;
        $notification->id_company = $companyId;
        $notification->save();
    }
}