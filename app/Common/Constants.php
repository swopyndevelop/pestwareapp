<?php


namespace App\Common;


class Constants
{
    public static $STREET = 'calle';
    public static $COLONY = 'colonia';
    public static $STATE = 'estado';
    public static $IVA = 'iva';

    public static $BILLING_MODE_INVOICE_BY_EACH_SERVICE = 2;
    public static $BILLING_MODE_INVOICE_BY_AMOUNT = 3;
    public static $BILLING_MODE_INVOICE_MONTH = 4;

    public static $STATUS_INVOICE_VALID = 14;
    public static $STATUS_INVOICE_VALID_PAID = 15;
    public static $STATUS_INVOICE_VALID_EXPIRED = 16;
    public static $STATUS_INVOICE_CANCELED = 17;

    public static $CERTIFICATE_PDF = "Certificado de Servicio (pdf)";
    public static $ORDER_SERVICE_PDF = "Orden de Servicio (pdf)";
    public static $INSPECTION_STATIONS_PDF = "Inspección de Estaciones (pdf)";
    public static $INSPECTION_AREAS_PDF = "Inspección de Áreas (pdf)";
    public static $INVOICE_PDF_XML = "Factura electrónica (pdf y xml)";

    public static $PLAN_TYPE_BILLING = "BILLING";
    public static $PLAN_TYPE_STRIPE = "STRIPE";
}