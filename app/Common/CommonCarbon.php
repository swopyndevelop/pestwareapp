<?php


namespace App\Common;


use App\employee;
use App\profile_job_center;

class CommonCarbon
{
    /*For Mexico (México):

    Northwest --------- America/Tijuana

    Pacific ----------- America/Mazatlan or America/Chihuahua

    Central ----------- America/Mexico_City or America/Matamoros or America/Monterrey or America/Merida

    South east -------- America/Cancun

    Source: http://www.cenam.mx/hora_oficial/*/

    public static function getTimeZoneByEmployee($employeeId) {
        $employee = employee::find($employeeId);
        $jobCenter = profile_job_center::where('id', $employee->profile_job_center_id)->first();
        return $jobCenter->timezone;
    }

    public static function getTimeZoneByJobCenter($jobCenterId) {
        $jobCenter = profile_job_center::find($jobCenterId);
        return $jobCenter->timezone;
    }
}