<?php


namespace App\Common;


use Carbon\Carbon;
use Storage;

class CommonImage
{
    public static function getTemporaryUrl($path, $minutes)
    {
        return Storage::disk('s3')->temporaryUrl($path, Carbon::now()->addMinutes($minutes));
    }

    public static function getTemporaryUrlPublic($path, $minutes)
    {
        return Storage::disk('s3p')->temporaryUrl($path, Carbon::now()->addMinutes($minutes));
    }
}