<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class companie
 *
 * @package App
 * @author Olga Rodríguez
 * @version 13/03/2019
 * @property int $id
 * @property int|null $id_company
 * @property string $name
 * @property string|null $email
 * @property string|null $domain_portal
 * @property string $rfc
 * @property string $address
 * @property string|null $logo
 * @property string|null $mini_logo
 * @property string|null $pdf_logo
 * @property string|null $pdf_sello
 * @property int $id_theme
 * @property string|null $facebook
 * @property mixed|null $image
 * @property int $active
 * @property string $phone
 * @property string|null $reminder_whatsapp
 * @property string|null $licence
 * @property string|null $pdf_sanitary_license
 * @property string|null $warnings_service
 * @property string|null $contract_service
 * @property int|null $company_contract_id
 * @property int|null $contact_id
 * @property string|null $bussines_name
 * @property string|null $health_manager
 * @property string|null $tax_regime
 * @property string|null $bank_account
 * @property int|null $pay_type_id
 * @property int|null $company_type
 * @property int|null $specialty
 * @property int|null $no_employees
 * @property string $cover_mip_color
 * @property string $id_stripe_customer
 * @property string $id_plan
 * @property int|null $no_branch_office
 * @property int|null $id_plan_center
 * @property int $id_code_country
 * @property boolean $is_folios_consumed
 * @property boolean $is_buy_folios
 * @property boolean $is_active_billing
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|companie newModelQuery()
 * @method static Builder|companie newQuery()
 * @method static Builder|companie query()
 * @method static Builder|companie whereActive($value)
 * @method static Builder|companie whereAddress($value)
 * @method static Builder|companie whereBankAccount($value)
 * @method static Builder|companie whereBussinesName($value)
 * @method static Builder|companie whereCompanyContractId($value)
 * @method static Builder|companie whereCompanyType($value)
 * @method static Builder|companie whereContactId($value)
 * @method static Builder|companie whereCreatedAt($value)
 * @method static Builder|companie whereDomainPortal($value)
 * @method static Builder|companie whereFacebook($value)
 * @method static Builder|companie whereId($value)
 * @method static Builder|companie whereIdCompany($value)
 * @method static Builder|companie whereIdTheme($value)
 * @method static Builder|companie whereImage($value)
 * @method static Builder|companie whereLicence($value)
 * @method static Builder|companie whereLogo($value)
 * @method static Builder|companie whereMiniLogo($value)
 * @method static Builder|companie whereName($value)
 * @method static Builder|companie whereNoBranchOffice($value)
 * @method static Builder|companie whereNoEmployees($value)
 * @method static Builder|companie wherePayTypeId($value)
 * @method static Builder|companie wherePdfLogo($value)
 * @method static Builder|companie wherePdfSello($value)
 * @method static Builder|companie wherePhone($value)
 * @method static Builder|companie whereRfc($value)
 * @method static Builder|companie whereSpecialty($value)
 * @method static Builder|companie whereTaxRegime($value)
 * @method static Builder|companie whereUpdatedAt($value)
 * @mixin Eloquent
 */
class companie extends Model
{
    /**
     * @var string
     */
    protected $table = "companies";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_company',
        'name',
        'email',
        'rfc',
        'address',
        'logo',
        'image',
        'phone',
        'reminder_whatsapp',
        'company_contract_id',
        'contact_id',
        'bussines_name',
        'health_name',
        'tax_regime',
        'pay_type_id',
        'company_type',
        'specialty',
        'no_employees',
        'no_branch_office',
        'active',
        'id_theme',
        'cover_mip_color',
        'facebook',
        'licence',
        'pdf_sanitary_license',
        'warnings_service',
        'contract_service',
        'id_stripe_customer',
        'id_plan',
        'id_plan_center',
        'is_folios_consumed',
        'is_buy_folios',
        'is_active_billing'
    ];
}
