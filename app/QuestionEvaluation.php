<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\QuestionEvaluation
 *
 * @property int $id
 * @property string|null $question_evaluations_id
 * @property int $trainings_id
 * @property string|null $job_title_name
 * @property string $question
 * @property string $type
 * @property float|null $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|QuestionEvaluation newModelQuery()
 * @method static Builder|QuestionEvaluation newQuery()
 * @method static Builder|QuestionEvaluation query()
 * @method static Builder|QuestionEvaluation whereCreatedAt($value)
 * @method static Builder|QuestionEvaluation whereId($value)
 * @method static Builder|QuestionEvaluation whereJobTitleName($value)
 * @method static Builder|QuestionEvaluation whereQuestion($value)
 * @method static Builder|QuestionEvaluation whereQuestionEvaluationsId($value)
 * @method static Builder|QuestionEvaluation whereTrainingsId($value)
 * @method static Builder|QuestionEvaluation whereType($value)
 * @method static Builder|QuestionEvaluation whereUpdatedAt($value)
 * @method static Builder|QuestionEvaluation whereValue($value)
 * @mixin Eloquent
 */
class QuestionEvaluation extends Model
{
    protected $table = "question_evaluations";
    protected $primarykey = "id";
    protected $fillable = ['id', 'question_evaluations_id', 'job_title_name','question', 'type', 'value'];
}
