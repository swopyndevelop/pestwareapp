<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class payment_method
 *
 * @package App
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $companie
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|payment_method newModelQuery()
 * @method static Builder|payment_method newQuery()
 * @method static Builder|payment_method query()
 * @method static Builder|payment_method whereCompanie($value)
 * @method static Builder|payment_method whereCreatedAt($value)
 * @method static Builder|payment_method whereId($value)
 * @method static Builder|payment_method whereName($value)
 * @method static Builder|payment_method whereUpdatedAt($value)
 * @mixin Eloquent
 */

class payment_method extends Model
{
    /**
     * @var string
     */
    protected $table = "payment_methods";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'companie',
        'visible',
        'profile_job_center_id',
    ];
}
