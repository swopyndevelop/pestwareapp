<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class plague_control_product
 *
 * @package App
 * @author Alberto Martínez
 * @version 11/08/2019
 * @property int $id
 * @property int $plague_control_id
 * @property int $id_product
 * @property string $dose
 * @property float $quantity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|plague_control_product newModelQuery()
 * @method static Builder|plague_control_product newQuery()
 * @method static Builder|plague_control_product query()
 * @method static Builder|plague_control_product whereCreatedAt($value)
 * @method static Builder|plague_control_product whereDose($value)
 * @method static Builder|plague_control_product whereId($value)
 * @method static Builder|plague_control_product whereIdProduct($value)
 * @method static Builder|plague_control_product wherePlagueControlId($value)
 * @method static Builder|plague_control_product whereQuantity($value)
 * @method static Builder|plague_control_product whereUpdatedAt($value)
 * @mixin Eloquent
 */
class plague_control_product extends Model
{
    /**
     * @var string
     */
    protected $table = "plague_controls_products";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'plague_control_id',
        'id_product',
        'dose',
        'quantity'
    ];
}