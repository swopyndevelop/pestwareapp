<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_profile
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_profiles_id
 * @property string|null $job_title_name
 * @property int $companie
 * @property string|null $job_title_objetive
 * @property int|null $min_salary
 * @property int|null $max_salary
 * @property int|null $function_type
 * @property int|null $min_age
 * @property int|null $max_age
 * @property string|null $gender
 * @property string|null $civil_status
 * @property string|null $scholarship
 * @property string|null $college_career
 * @property int|null $interview_type
 * @property string|null $contract
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_profile newModelQuery()
 * @method static Builder|job_title_profile newQuery()
 * @method static Builder|job_title_profile query()
 * @method static Builder|job_title_profile whereCivilStatus($value)
 * @method static Builder|job_title_profile whereCollegeCareer($value)
 * @method static Builder|job_title_profile whereCompanie($value)
 * @method static Builder|job_title_profile whereContract($value)
 * @method static Builder|job_title_profile whereCreatedAt($value)
 * @method static Builder|job_title_profile whereFunctionType($value)
 * @method static Builder|job_title_profile whereGender($value)
 * @method static Builder|job_title_profile whereId($value)
 * @method static Builder|job_title_profile whereInterviewType($value)
 * @method static Builder|job_title_profile whereJobTitleName($value)
 * @method static Builder|job_title_profile whereJobTitleObjetive($value)
 * @method static Builder|job_title_profile whereJobTitleProfilesId($value)
 * @method static Builder|job_title_profile whereMaxAge($value)
 * @method static Builder|job_title_profile whereMaxSalary($value)
 * @method static Builder|job_title_profile whereMinAge($value)
 * @method static Builder|job_title_profile whereMinSalary($value)
 * @method static Builder|job_title_profile whereScholarship($value)
 * @method static Builder|job_title_profile whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_profile extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_profiles";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_profiles_id',
        'job_title_name',
        'job_title_objetive',
        'min_salary',
        'max_salary',
        'function_type',
        'min_age',
        'max_age',
        'gender',
        'civil_status',
        'scholarship',
        'college_career',
        'interview_type',
        'contract',
        'companie'
    ];
}
