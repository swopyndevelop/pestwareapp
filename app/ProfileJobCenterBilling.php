<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProfileJobCenterBilling
 * @package App
 * @property int $id
 * @property int $id_profile_job_center
 * @property string $rfc
 * @property string $iva
 * @property string $email
 * @property string $address
 * @property string $address_number
 * @property string $colony
 * @property string $municipality
 * @property string $state
 * @property string $code_postal
 * @property string $file_certificate
 * @property string $file_key
 * @property string $password
 * @property string $fiscal_regime
 * @property boolean $status_connection_sat
 * @property string|null $text_mail
 * @property int $folios
 * @property int $folios_consumed
 * @property int $folios_reserved
 * @property boolean $night_billing_approved
 * @mixin Eloquent
 */
class ProfileJobCenterBilling extends Model
{
    /** * @var string */
    protected $table = "profile_job_centers_billing";

    /** * @var array */
    protected $fillable = [
        'id',
        'id_profile_job_center',
        'rfc',
        'iva',
        'email',
        'address',
        'address_number',
        'colony',
        'municipality',
        'state',
        'code_postal',
        'file_certificate',
        'file_key',
        'password',
        'fiscal_regime',
        'status_connection_sat',
        'text_mail',
        'folios',
        'folios_consumed',
        'folios_reserved',
        'night_billing_approved'
    ];
}
