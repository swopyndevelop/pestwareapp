<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class quotation
 *
 * @package App
 * @author Olga Rodríguez
 * @version 16/01/2019
 * @property int $id
 * @property string $id_quotation
 * @property int $id_customer
 * @property int|null $construction_measure
 * @property int|null $garden_measure
 * @property int|null $id_plague_jer
 * @property int|null $establishment_id
 * @property int|null $spaces_to_fumigate
 * @property string|null $description
 * @property string|null $description_whatsapp
 * @property string|null $messenger
 * @property int|null $whatsapp
 * @property boolean $email
 * @property float $total
 * @property int|null $price
 * @property int|null $price_reinforcement
 * @property int|null $id_status
 * @property float|null $iva
 * @property int|null $user_id
 * @property int|null $id_job_center
 * @property int $companie
 * @property int|null $id_extra
 * @property int|null $id_price_list
 * @property string|null $pesticide_product
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $visible
 * @property boolean $approved
 * @property-read Collection|custom_quote[] $custom_quote
 * @property-read int|null $custom_quote_count
 * @property-read Collection|discount[] $discount
 * @property-read int|null $discount_count
 * @property-read Collection|plague_type[] $plague_type
 * @property-read int|null $plague_type_count
 * @method static Builder|quotation newModelQuery()
 * @method static Builder|quotation newQuery()
 * @method static Builder|quotation query()
 * @method static Builder|quotation whereCompanie($value)
 * @method static Builder|quotation whereConstructionMeasure($value)
 * @method static Builder|quotation whereCreatedAt($value)
 * @method static Builder|quotation whereDescription($value)
 * @method static Builder|quotation whereDescriptionWhatsapp($value)
 * @method static Builder|quotation whereEstablishmentId($value)
 * @method static Builder|quotation whereGardenMeasure($value)
 * @method static Builder|quotation whereId($value)
 * @method static Builder|quotation whereIdCustomer($value)
 * @method static Builder|quotation whereIdExtra($value)
 * @method static Builder|quotation whereIdJobCenter($value)
 * @method static Builder|quotation whereIdPlagueJer($value)
 * @method static Builder|quotation whereIdPriceList($value)
 * @method static Builder|quotation whereIdQuotation($value)
 * @method static Builder|quotation whereIdStatus($value)
 * @method static Builder|quotation whereIva($value)
 * @method static Builder|quotation whereMessenger($value)
 * @method static Builder|quotation wherePrice($value)
 * @method static Builder|quotation wherePriceReinforcement($value)
 * @method static Builder|quotation whereSpacesToFumigate($value)
 * @method static Builder|quotation whereTotal($value)
 * @method static Builder|quotation whereUpdatedAt($value)
 * @method static Builder|quotation whereUserId($value)
 * @method static Builder|quotation whereWhatsapp($value)
 * @mixin Eloquent
 */

class quotation extends Model
{
    /**
     * @var string
     */
    protected $table = "quotations";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_quotation',
        'id_customer',
        'construction_measure',
        'garden_measure',
        'id_plague_jer',
        'establishment_id',
        'spaces_to_fumigate',
        'description',
        'messenger',
        'email',
        'total',
        'id_status',
        'user_id',
        'id_job_center',
        'companie',
        'pesticide_product',
        'visible',
        'approved'
    ];

    /**
    * @author Olga Rodríguez
    * @version 12/01/2019
    *Función para relación muchos a muchos con tabla custom_quotes
    */
    public function custom_quote()
    {
        return $this->belongsToMany(custom_quote::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 12/01/2019
    *Función para relación muchos a muchos con tabla discounts
    */
    public function discount()
    {
        return $this->belongsToMany(discount::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 22/01/2019
    *Función para relación muchos a muchos con tabla plague_types
    */
    public function plague_type()
    {
        return $this->belongsToMany(plague_type::class);
    }

}
