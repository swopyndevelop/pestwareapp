<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class habit_condition
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property string $condition
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|service_order[] $service_order
 * @property-read int|null $service_order_count
 * @method static Builder|habit_condition newModelQuery()
 * @method static Builder|habit_condition newQuery()
 * @method static Builder|habit_condition query()
 * @method static Builder|habit_condition whereCondition($value)
 * @method static Builder|habit_condition whereCreatedAt($value)
 * @method static Builder|habit_condition whereId($value)
 * @method static Builder|habit_condition whereUpdatedAt($value)
 * @mixin Eloquent
 */
class habit_condition extends Model
{
    /**
     * @var string
     */
    protected $table = "habit_conditions";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'condition'
    ];

    /**
    * @author Olga Rodríguez
    * @version 12/01/2019
    *Función para relación muchos a muchos con tabla customer_datas.
    */
    public function service_order()
    {
        return $this->belongsToMany(service_order::class);
    }
}
