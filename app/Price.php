<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Price
 *
 * @property int $id
 * @property int $price_list_id
 * @property int $area
 * @property int $quantity
 * @property float $price_one
 * @property float|null $price_two
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Price newModelQuery()
 * @method static Builder|Price newQuery()
 * @method static Builder|Price query()
 * @method static Builder|Price whereArea($value)
 * @method static Builder|Price whereCreatedAt($value)
 * @method static Builder|Price whereId($value)
 * @method static Builder|Price wherePriceListId($value)
 * @method static Builder|Price wherePriceOne($value)
 * @method static Builder|Price wherePriceTwo($value)
 * @method static Builder|Price whereQuantity($value)
 * @method static Builder|Price whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Price extends Model
{
    /**
     * @var string
     */
    protected $table = "prices";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'price_list_id',
        'area',
        'quantity',
        'price_one',
        'price_two'
    ];
}
