<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class plague_type
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property string|null $plague_key
 * @property int $id_categorie
 * @property int $is_monitoring
 * @property int $id_company
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|place_inspection[] $place_inspections
 * @property-read int|null $place_inspections_count
 * @property-read Collection|quotation[] $quotation
 * @property-read int|null $quotation_count
 * @method static Builder|plague_type newModelQuery()
 * @method static Builder|plague_type newQuery()
 * @method static Builder|plague_type query()
 * @method static Builder|plague_type whereCreatedAt($value)
 * @method static Builder|plague_type whereId($value)
 * @method static Builder|plague_type whereIdCompany($value)
 * @method static Builder|plague_type whereName($value)
 * @method static Builder|plague_type wherePlagueKey($value)
 * @method static Builder|plague_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class plague_type extends Model
{
    /**
     * @var string
     */
    protected $table = "plague_types";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'profile_job_center_id',
        'plague_key',
        'id_categorie',
        'is_monitoring',
        'visible'
    ];

    /**
    * @author Olga Rodríguez
    * @version 22/01/2019
    *Función para relación muchos a muchos con tabla quotations.
    */
    public function quotation()
    {
        return $this->belongsToMany(quotation::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación muchos a muchos con tabla place_inspections
    */
    public function place_inspections()
    {
        return $this->belongsToMany(place_inspection::class);
    }
}
