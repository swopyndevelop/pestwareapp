<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class customer_data_inhabitant_type
 *
 * @package App
 * @author Olga Rodríguez
 * @version 12/01/2019
 * @property int $id
 * @property int $customer_data_id
 * @property int $inhabitant_types_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|customer_data_inhabitant_type newModelQuery()
 * @method static Builder|customer_data_inhabitant_type newQuery()
 * @method static Builder|customer_data_inhabitant_type query()
 * @method static Builder|customer_data_inhabitant_type whereCreatedAt($value)
 * @method static Builder|customer_data_inhabitant_type whereCustomerDataId($value)
 * @method static Builder|customer_data_inhabitant_type whereId($value)
 * @method static Builder|customer_data_inhabitant_type whereInhabitantTypesId($value)
 * @method static Builder|customer_data_inhabitant_type whereUpdatedAt($value)
 * @mixin Eloquent
 */

class customer_data_inhabitant_type extends Model
{
    /**
     * @var string
     */
    protected $table = "customer_data_inhabitant_type";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'customer_data_id',
        'inhabitant_types_id'
    ];
}
