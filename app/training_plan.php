<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class training_plan
 *
 * @package App
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @property int $id
 * @property string $name
 * @property string $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|assignment_plan[] $assignment_plans
 * @property-read int|null $assignment_plans_count
 * @property-read Collection|module[] $modules
 * @property-read int|null $modules_count
 * @method static Builder|training_plan newModelQuery()
 * @method static Builder|training_plan newQuery()
 * @method static Builder|training_plan query()
 * @method static Builder|training_plan whereCreatedAt($value)
 * @method static Builder|training_plan whereDescription($value)
 * @method static Builder|training_plan whereId($value)
 * @method static Builder|training_plan whereName($value)
 * @method static Builder|training_plan whereUpdatedAt($value)
 * @mixin Eloquent
 */
class training_plan extends Model
{
    /**
     * @var string
     */
    protected $table = "training_plans";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description'
    ];
    /**
    * @author Olga Rodríguez
    * @version 10/07/2019
    *Función para relación muchos a muchos con tabla modules
    */
    public function modules()
    {
        return $this->belongsToMany(module::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 10/07/2019
    *Función para relación muchos a muchos con tabla assignment_plans
    */
    public function assignment_plans()
    {
        return $this->belongsToMany(assignment_plan::class);
    }
}
