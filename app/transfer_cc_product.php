<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\transfer_cc_product
 *
 * @property int $id
 * @property int $id_transfer_cc
 * @property int $id_product
 * @property string $units
 * @property int|null $is_units
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|transfer_cc_product newModelQuery()
 * @method static Builder|transfer_cc_product newQuery()
 * @method static Builder|transfer_cc_product query()
 * @method static Builder|transfer_cc_product whereCreatedAt($value)
 * @method static Builder|transfer_cc_product whereId($value)
 * @method static Builder|transfer_cc_product whereIdProduct($value)
 * @method static Builder|transfer_cc_product whereIdTransferCc($value)
 * @method static Builder|transfer_cc_product whereIsUnits($value)
 * @method static Builder|transfer_cc_product whereUnits($value)
 * @method static Builder|transfer_cc_product whereUpdatedAt($value)
 * @mixin Eloquent
 */
class transfer_cc_product extends Model
{
    /**
     * @var string
     */
    protected $table = "transfer_cc_products";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_transfer_cc',
        'id_product',
        'units'
    ];
}
