<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StationCondition
 * @package App
 * @property int $id,
 * @property int $id_node
 * @property int $id_condition
 * @property int $id_inspection
 */
class StationCondition extends Model
{
    /**
     * @var string
     */
    protected $table = "station_conditions";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_node',
        'id_condition',
        'id_inspection'
    ];
}
