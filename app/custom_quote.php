<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class custom_quote
 *
 * @package App
 * @author Olga Rodríguez
 * @version 16/01/2019
 * @property int $id
 * @property int $id_quotation
 * @property string $concept
 * @property int $quantity
 * @property int $frecuency_month
 * @property int $term_month
 * @property float $unit_price
 * @property float $subtotal
 * @property int $id_type_concepts
 * @property int $id_type_service
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|quotation[] $quotation
 * @property-read int|null $quotation_count
 * @method static Builder|custom_quote newModelQuery()
 * @method static Builder|custom_quote newQuery()
 * @method static Builder|custom_quote query()
 * @method static Builder|custom_quote whereConcept($value)
 * @method static Builder|custom_quote whereCreatedAt($value)
 * @method static Builder|custom_quote whereFrecuencyMonth($value)
 * @method static Builder|custom_quote whereId($value)
 * @method static Builder|custom_quote whereIdQuotation($value)
 * @method static Builder|custom_quote whereQuantity($value)
 * @method static Builder|custom_quote whereSubtotal($value)
 * @method static Builder|custom_quote whereTermMonth($value)
 * @method static Builder|custom_quote whereUnitPrice($value)
 * @method static Builder|custom_quote whereUpdatedAt($value)
 * @mixin Eloquent
 */

class custom_quote extends Model
{
    /**
     * @var string
     */
    protected $table = "custom_quotes";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_quotation',
        'concept',
        'quantity',
        'frecuency_month',
        'term_month',
        'unit_price',
        'subtotal',
        'id_type_concepts',
        'id_type_service'
    ];

    /**
    * @author Olga Rodríguez
    * @version 12/01/2019
    * Función para relación muchos a muchos con tabla quotations.
    */
    public function quotation()
    {
        return $this->belongsToMany(quotation::class);
    }
}
