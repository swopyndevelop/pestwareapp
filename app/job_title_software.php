<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_software
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_softwares_id
 * @property int $job_title_profiles_id
 * @property string|null $software_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_software newModelQuery()
 * @method static Builder|job_title_software newQuery()
 * @method static Builder|job_title_software query()
 * @method static Builder|job_title_software whereCreatedAt($value)
 * @method static Builder|job_title_software whereId($value)
 * @method static Builder|job_title_software whereJobTitleProfilesId($value)
 * @method static Builder|job_title_software whereJobTitleSoftwaresId($value)
 * @method static Builder|job_title_software whereSoftwareName($value)
 * @method static Builder|job_title_software whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_software extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_softwares";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_softwares_id',
        'job_title_profiles_id',
        'software_name'
    ];
}
