<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InvoiceOrder
 * @package App
 * @property int $id
 * @property int $id_invoice
 * @property int $id_service_order
 * @mixin Eloquent
 */
class InvoiceOrder extends Model
{
    protected $table = 'invoice_orders';

    protected $fillable = ['id_invoice', 'id_service_order'];
}
