<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\PriceList
 *
 * @property int $id
 * @property string $name
 * @property string $key
 * @property int $hierarchy
 * @property string $description
 * @property string|null $pdf
 * @property string|null $portada
 * @property int $establishment_id
 * @property int $indications_id
 * @property int $profile_job_center_id
 * @property int $company_id
 * @property int $user_id
 * @property int $status
 * @property int|null $reinforcement_days
 * @property int $customer_portal
 * @property string $visible
 * @property boolean $show_price
 * @property string|null $warranty
 * @property int $is_disinfection
 * @property int $days_expiration_certificate
 * @property string $unit_code_billing
 * @property string $service_code_billing
 * @property string $description_billing
 * @property string|null $legend
 * @property boolean $individual_price
 * @property double|null $value_individual_price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|PriceList newModelQuery()
 * @method static Builder|PriceList newQuery()
 * @method static Builder|PriceList query()
 * @method static Builder|PriceList whereCompanyId($value)
 * @method static Builder|PriceList whereCreatedAt($value)
 * @method static Builder|PriceList whereDescription($value)
 * @method static Builder|PriceList whereEstablishmentId($value)
 * @method static Builder|PriceList whereHierarchy($value)
 * @method static Builder|PriceList whereId($value)
 * @method static Builder|PriceList whereIndicationsId($value)
 * @method static Builder|PriceList whereKey($value)
 * @method static Builder|PriceList whereName($value)
 * @method static Builder|PriceList wherePdf($value)
 * @method static Builder|PriceList wherePortada($value)
 * @method static Builder|PriceList whereReinforcementDays($value)
 * @method static Builder|PriceList whereStatus($value)
 * @method static Builder|PriceList whereUpdatedAt($value)
 * @method static Builder|PriceList whereUserId($value)
 * @mixin Eloquent
 */
class PriceList extends Model
{
    /**
     * @var string
     */
    protected $table = "price_lists";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'key',
        'hierarchy',
        'description',
        'establishment_id',
        'indications_id',
        'profile_job_center_id',
        'company_id',
        'user_id',
        'status',
        'pdf',
        'portada',
        'customer_portal',
        'visible',
        'show_price',
        'days_expiration_certificate',
        'warranty',
        'is_disinfection',
        'unit_code_billing',
        'service_code_billing',
        'description_billing',
        'legend',
        'individual_price',
        'value_individual_price'
    ];
}
