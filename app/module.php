<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class module
 *
 * @package App
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|training_plan[] $training_plans
 * @property-read int|null $training_plans_count
 * @property-read Collection|training[] $trainings
 * @property-read int|null $trainings_count
 * @method static Builder|module newModelQuery()
 * @method static Builder|module newQuery()
 * @method static Builder|module query()
 * @method static Builder|module whereCreatedAt($value)
 * @method static Builder|module whereId($value)
 * @method static Builder|module whereName($value)
 * @method static Builder|module whereUpdatedAt($value)
 * @mixin Eloquent
 */
class module extends Model
{
    /**
     * @var string
     */
    protected $table = "modules";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];

    /**
    * @author Olga Rodríguez
    * @version 10/07/2019
    *Función para relación muchos a muchos con tabla trainings
    */
    public function trainings()
    {
        return $this->belongsToMany(training::class);
    }
    
    /**
    * @author Olga Rodríguez
    * @version 10/07/2019
    *Función para relación muchos a muchos con tabla training_plans
    */
    public function training_plans()
    {
        return $this->belongsToMany(training_plan::class);
    }
}
