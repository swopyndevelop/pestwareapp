<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class inhabitant_type_service_order
 *
 * @package App
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @property int $id
 * @property int $service_order_id
 * @property int $inhabitant_type_id
 * @property string|null $specification
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|inhabitant_type_service_order newModelQuery()
 * @method static Builder|inhabitant_type_service_order newQuery()
 * @method static Builder|inhabitant_type_service_order query()
 * @method static Builder|inhabitant_type_service_order whereCreatedAt($value)
 * @method static Builder|inhabitant_type_service_order whereId($value)
 * @method static Builder|inhabitant_type_service_order whereInhabitantTypeId($value)
 * @method static Builder|inhabitant_type_service_order whereServiceOrderId($value)
 * @method static Builder|inhabitant_type_service_order whereSpecification($value)
 * @method static Builder|inhabitant_type_service_order whereUpdatedAt($value)
 * @mixin Eloquent
 */
class inhabitant_type_service_order extends Model
{
    /**
     * @var string
     */
    protected $table = "inhabitant_type_service_order";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'service_order_id',
        'inhabitant_type_id',
        'specification'
    ];
}
