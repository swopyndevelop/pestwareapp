<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class address_job_center
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $address_job_centers_id
 * @property int $profile_job_centers_id
 * @property string $street
 * @property int $num_ext
 * @property string|null $num_int
 * @property string $district
 * @property int $zip_code
 * @property string $location
 * @property string $municipality
 * @property string $state
 * @property string $country
 * @property string $ubication
 * @property string $phone
 * @property string $email
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|address_job_center newModelQuery()
 * @method static Builder|address_job_center newQuery()
 * @method static Builder|address_job_center query()
 * @method static Builder|address_job_center whereAddressJobCentersId($value)
 * @method static Builder|address_job_center whereCountry($value)
 * @method static Builder|address_job_center whereCreatedAt($value)
 * @method static Builder|address_job_center whereDistrict($value)
 * @method static Builder|address_job_center whereEmail($value)
 * @method static Builder|address_job_center whereId($value)
 * @method static Builder|address_job_center whereLocation($value)
 * @method static Builder|address_job_center whereMunicipality($value)
 * @method static Builder|address_job_center whereNumExt($value)
 * @method static Builder|address_job_center whereNumInt($value)
 * @method static Builder|address_job_center wherePhone($value)
 * @method static Builder|address_job_center whereProfileJobCentersId($value)
 * @method static Builder|address_job_center whereState($value)
 * @method static Builder|address_job_center whereStreet($value)
 * @method static Builder|address_job_center whereUbication($value)
 * @method static Builder|address_job_center whereUpdatedAt($value)
 * @method static Builder|address_job_center whereZipCode($value)
 * @mixin Eloquent
 */
class address_job_center extends Model
{
    /**
     * @var string
     */
    protected $table = "address_job_centers";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'address_job_centers_id',
        'profile_job_centers_id',
        'street',
        'num_ext',
        'num_int',
        'district',
        'zip_code',
        'location',
        'municipality',
        'state',
        'country',
        'ubication',
        'phone',
        'email'
    ];
}
