<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class service_information
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property int $plague_id
 * @property int $establishment_id
 * @property string|null $description
 * @property string|null $indications
 * @property string|null $description_pdf
 * @property string|null $pdf
 * @property int $id_company
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|service_information newModelQuery()
 * @method static Builder|service_information newQuery()
 * @method static Builder|service_information query()
 * @method static Builder|service_information whereCreatedAt($value)
 * @method static Builder|service_information whereDescription($value)
 * @method static Builder|service_information whereDescriptionPdf($value)
 * @method static Builder|service_information whereEstablishmentId($value)
 * @method static Builder|service_information whereId($value)
 * @method static Builder|service_information whereIdCompany($value)
 * @method static Builder|service_information whereIndications($value)
 * @method static Builder|service_information wherePdf($value)
 * @method static Builder|service_information wherePlagueId($value)
 * @method static Builder|service_information whereUpdatedAt($value)
 * @mixin Eloquent
 */
class service_information extends Model
{
    /**
     * @var string
     */
    protected $table = "service_informations";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'plague_id',
        'description',
        'indications',
        'establishment_id',
        'id_jobcenter',
        'pdf',
        'description_pdf'
    ];
}
