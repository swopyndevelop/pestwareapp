<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MonitoringNode
 * @package App
 * @property int $id
 * @property int $id_monitoring_tree
 * @property int $id_type_area
 * @property string $name
 * @mixin Eloquent
 */

class MonitoringNode extends Model
{
    protected $table = 'monitoring_nodes';

    protected $fillable = ['id_monitoring_tree','id_type_area','name'];
}
