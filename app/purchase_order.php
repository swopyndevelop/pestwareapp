<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\purchase_order
 *
 * @property int $id
 * @property string $id_purchase_order
 * @property int $id_provider
 * @property int $id_concept
 * @property string $date_order
 * @property int $id_user
 * @property string|null $description_order
 * @property float $total
 * @property string $has_entry
 * @property int $status
 * @property int $id_payment_way
 * @property int|null $credit_days
 * @property int $id_payment_method
 * @property int $account_status
 * @property int $id_voucher
 * @property int $id_jobcenter
 * @property int $companie
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|purchase_order newModelQuery()
 * @method static Builder|purchase_order newQuery()
 * @method static Builder|purchase_order query()
 * @method static Builder|purchase_order whereAccountStatus($value)
 * @method static Builder|purchase_order whereCompanie($value)
 * @method static Builder|purchase_order whereCreatedAt($value)
 * @method static Builder|purchase_order whereCreditDays($value)
 * @method static Builder|purchase_order whereDateOrder($value)
 * @method static Builder|purchase_order whereDescriptionOrder($value)
 * @method static Builder|purchase_order whereId($value)
 * @method static Builder|purchase_order whereIdConcept($value)
 * @method static Builder|purchase_order whereIdJobcenter($value)
 * @method static Builder|purchase_order whereIdPaymentMethod($value)
 * @method static Builder|purchase_order whereIdPaymentWay($value)
 * @method static Builder|purchase_order whereIdProvider($value)
 * @method static Builder|purchase_order whereIdPurchaseOrder($value)
 * @method static Builder|purchase_order whereIdUser($value)
 * @method static Builder|purchase_order whereIdVoucher($value)
 * @method static Builder|purchase_order whereStatus($value)
 * @method static Builder|purchase_order whereTotal($value)
 * @method static Builder|purchase_order whereUpdatedAt($value)
 * @mixin Eloquent
 */
class purchase_order extends Model
{
    /**
     * @var string
     */
    protected $table = "purchase_orders";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_purchase_order',
        'id_provider',
        'id_concept',
        'date_order',
        'id_user',
        'description_order',
        'total',
        'status',
        'id_payment_way',
        'credit_days',
        'id_payment_method',
        'account_status',
        'id_voucher',
        'id_jobcenter',
        'companie',
        'visible',
        'has_entry'
    ];

}
