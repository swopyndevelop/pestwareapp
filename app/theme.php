<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\theme
 *
 * @method static Builder|theme newModelQuery()
 * @method static Builder|theme newQuery()
 * @method static Builder|theme query()
 * @mixin Eloquent
 */
class theme extends Model
{
    protected $table = 'themes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name'];
}
