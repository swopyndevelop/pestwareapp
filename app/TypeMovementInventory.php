<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * App\TypeMovementInventory
 *
 * @property int $id
 * @property string $name
 * @mixin Eloquent
 */

class TypeMovementInventory extends Model
{
    /**
     * @var string
     */
    protected $table = "type_movements_inventories";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];
}
