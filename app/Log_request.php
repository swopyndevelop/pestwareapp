<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Log_request
 *
 * @package App
 * @author Manuel Mendoza
 * @version 03/18/2021
 * @property int $id
 * @property string|null $url
 * @property string|null $method
 * @property string|null $ip
 * @property string|null $user
 * @property string|null $duration
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */

class Log_request extends Model
{
    protected $table = "log_requests";

    protected $fillable = [
        'id',
        'url',
        'method',
        'ip',
        'user',
        'duration',
    ];
}
