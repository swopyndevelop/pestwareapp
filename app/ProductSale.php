<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Sale
 * @package App
 * @property int $id
 * @property string $id_sale
 * @property int $id_product
 * @property int quantity
 * @property double $sale_price
 * @property double $subtotal
 * @property double $total
 * @mixin Eloquent
 */
class ProductSale extends Model
{
    /**
     * @var string
     */
    protected $table = "products_sale";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_sale',
        'id_product',
        'quantity',
        'sale_price',
        'subtotal',
        'total'
    ];
}
