<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\transfer_center_center
 *
 * @property int $id
 * @property string $id_transfer_cc
 * @property int $id_user
 * @property int $id_job_center_origin
 * @property int $id_job_center_destiny
 * @property float $total
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|transfer_center_center newModelQuery()
 * @method static Builder|transfer_center_center newQuery()
 * @method static Builder|transfer_center_center query()
 * @method static Builder|transfer_center_center whereCompanie($value)
 * @method static Builder|transfer_center_center whereCreatedAt($value)
 * @method static Builder|transfer_center_center whereId($value)
 * @method static Builder|transfer_center_center whereIdJobCenterDestiny($value)
 * @method static Builder|transfer_center_center whereIdJobCenterOrigin($value)
 * @method static Builder|transfer_center_center whereIdTransferCc($value)
 * @method static Builder|transfer_center_center whereIdUser($value)
 * @method static Builder|transfer_center_center whereTotal($value)
 * @method static Builder|transfer_center_center whereUpdatedAt($value)
 * @mixin Eloquent
 */
class transfer_center_center extends Model
{
    /**
     * @var string
     */
    protected $table = "transfer_center_centers";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_transfer_cc',
        'id_user',
        'id_job_center_origin',
        'id_job_center_destiny',
        'total',
        'created_at',
        'companie'
    ];
}
