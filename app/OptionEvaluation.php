<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\OptionEvaluation
 *
 * @property int $id
 * @property string|null $option_evaluations_id
 * @property string $question_evaluation_id
 * @property string $option
 * @property string $validation
 * @property float|null $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|OptionEvaluation newModelQuery()
 * @method static Builder|OptionEvaluation newQuery()
 * @method static Builder|OptionEvaluation query()
 * @method static Builder|OptionEvaluation whereCreatedAt($value)
 * @method static Builder|OptionEvaluation whereId($value)
 * @method static Builder|OptionEvaluation whereOption($value)
 * @method static Builder|OptionEvaluation whereOptionEvaluationsId($value)
 * @method static Builder|OptionEvaluation whereQuestionEvaluationId($value)
 * @method static Builder|OptionEvaluation whereUpdatedAt($value)
 * @method static Builder|OptionEvaluation whereValidation($value)
 * @method static Builder|OptionEvaluation whereValue($value)
 * @mixin Eloquent
 */
class OptionEvaluation extends Model
{
    protected $table = "option_evaluations";
    protected $primarykey = "option_evaluations_id";
    protected $fillable = ['id', 'option_evaluations_id', 'question_evaluations_id','option', 'validation', 'value'];
}
