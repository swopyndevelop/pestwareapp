<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class plague_type_quotation
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/02/2019
 * @property int $id
 * @property int $quotation_id
 * @property int $plague_type_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|plague_type_quotation newModelQuery()
 * @method static Builder|plague_type_quotation newQuery()
 * @method static Builder|plague_type_quotation query()
 * @method static Builder|plague_type_quotation whereCreatedAt($value)
 * @method static Builder|plague_type_quotation whereId($value)
 * @method static Builder|plague_type_quotation wherePlagueTypeId($value)
 * @method static Builder|plague_type_quotation whereQuotationId($value)
 * @method static Builder|plague_type_quotation whereUpdatedAt($value)
 * @mixin Eloquent
 */
class plague_type_quotation extends Model
{
        /**
     * @var string
     */
    protected $table = "plague_type_quotation";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'quotation_id',
        'plague_type_id'
    ];
}
