<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class PlagueResponseInspection
 *
 * @package App
 * @author Alberto Martínez
 * @version 19/02/2021
 * @property int $id
 * @property int $id_station_response
 * @property int $id_plague
 * @property int $quantity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|plague_type newModelQuery()
 * @method static Builder|plague_type newQuery()
 * @method static Builder|plague_type query()
 * @method static Builder|plague_type whereCreatedAt($value)
 * @method static Builder|plague_type whereId($value)
 * @method static Builder|plague_type whereIdCompany($value)
 * @method static Builder|plague_type whereName($value)
 * @method static Builder|plague_type wherePlagueKey($value)
 * @method static Builder|plague_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class PlagueResponseInspection extends Model
{
    /**
     * @var string
     */
    protected $table = "plagues_response_inspection";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_station_response',
        'id_plague',
        'quantity'
    ];
}
