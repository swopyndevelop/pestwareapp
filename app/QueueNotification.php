<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class QueueNotification extends Model
{
    use Notifiable;

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return "https://hooks.slack.com/services/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";
    }
}
