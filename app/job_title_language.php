<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_language
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_languages_id
 * @property int $job_title_profiles_id
 * @property string|null $language
 * @property string|null $level
 * @property int|null $level_write
 * @property int|null $level_read
 * @property int|null $level_speak
 * @property int|null $level_listen
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_language newModelQuery()
 * @method static Builder|job_title_language newQuery()
 * @method static Builder|job_title_language query()
 * @method static Builder|job_title_language whereCreatedAt($value)
 * @method static Builder|job_title_language whereId($value)
 * @method static Builder|job_title_language whereJobTitleLanguagesId($value)
 * @method static Builder|job_title_language whereJobTitleProfilesId($value)
 * @method static Builder|job_title_language whereLanguage($value)
 * @method static Builder|job_title_language whereLevel($value)
 * @method static Builder|job_title_language whereLevelListen($value)
 * @method static Builder|job_title_language whereLevelRead($value)
 * @method static Builder|job_title_language whereLevelSpeak($value)
 * @method static Builder|job_title_language whereLevelWrite($value)
 * @method static Builder|job_title_language whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_language extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_languages";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_languages_id',
        'job_title_profiles_id',
        'language',
        'level',
        'level_write',
        'level_read',
        'level_speak',
        'level_listen'
    ];
}
