<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\archive_order
 *
 * @property int $id
 * @property int $id_purchase_order
 * @property string $file_route
 * @property int|null $type
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|archive_order newModelQuery()
 * @method static Builder|archive_order newQuery()
 * @method static Builder|archive_order query()
 * @method static Builder|archive_order whereCompanie($value)
 * @method static Builder|archive_order whereCreatedAt($value)
 * @method static Builder|archive_order whereFileRoute($value)
 * @method static Builder|archive_order whereId($value)
 * @method static Builder|archive_order whereIdPurchaseOrder($value)
 * @method static Builder|archive_order whereType($value)
 * @method static Builder|archive_order whereUpdatedAt($value)
 * @mixin Eloquent
 */
class archive_order extends Model
{
    /**
     * @var string
     */
    protected $table = "archive_orders";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_purchase_order',
        'file_route',
        'type'
    ];

}
