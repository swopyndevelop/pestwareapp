<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
/**
 * Class Monitoring
 * @package App
 * @property int $id
 * @property string $id_monitoring
 * @property int $id_user
 * @property int $id_customer
 * @property int|null $id_customer_branch
 * @property string $visible
 * @property string $status
 * @property string $date_updated_file
 * @property int|null $no_parts
 * @property string $date_updated_tree
 * @mixin Eloquent
 */
class Monitoring extends Model
{
    protected $table = 'monitorings';

    protected $fillable = [
        'id_monitoring',
        'id_user',
        'id_customer',
        'id_customer_branch',
        'visible',
        'status',
        'date_updated_file',
        'date_updated_tree',
        'no_parts'];
}
