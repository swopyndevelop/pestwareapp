<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class infestation_degree
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $companie
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|infestation_degree newModelQuery()
 * @method static Builder|infestation_degree newQuery()
 * @method static Builder|infestation_degree query()
 * @method static Builder|infestation_degree whereCompanie($value)
 * @method static Builder|infestation_degree whereCreatedAt($value)
 * @method static Builder|infestation_degree whereId($value)
 * @method static Builder|infestation_degree whereName($value)
 * @method static Builder|infestation_degree whereUpdatedAt($value)
 * @mixin Eloquent
 */
class infestation_degree extends Model
{
    /**
     * @var string
     */
    protected $table = "infestation_degrees";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'profile_job_center_id',
        'companie',
        'visible'
    ];
}
