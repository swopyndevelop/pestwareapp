<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class profile_job_center
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $profile_job_centers_id
 * @property string|null $name
 * @property string|null $email_personal
 * @property string|null $business_name
 * @property string $rfc
 * @property string|null $license
 * @property string|null $messenger_personal
 * @property string|null $whatsapp_personal
 * @property string|null $cellphone
 * @property string|null $facebook_personal
 * @property string|null $web_page
 * @property string|null $health_manager
 * @property string|null $sanitary_license
 * @property string|null $warning_service
 * @property string|null $contract_service
 * @property string|null $code
 * @property int $companie
 * @property mixed|null $image
 * @property string|null $type
 * @property int|null $total
 * @property string $iva
 * @property string $rfc_country
 * @property string $timezone
 * @property string $ssid
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|profile_job_center newModelQuery()
 * @method static Builder|profile_job_center newQuery()
 * @method static Builder|profile_job_center query()
 * @method static Builder|profile_job_center whereCode($value)
 * @method static Builder|profile_job_center whereCompanie($value)
 * @method static Builder|profile_job_center whereCreatedAt($value)
 * @method static Builder|profile_job_center whereId($value)
 * @method static Builder|profile_job_center whereImage($value)
 * @method static Builder|profile_job_center whereName($value)
 * @method static Builder|profile_job_center whereProfileJobCentersId($value)
 * @method static Builder|profile_job_center whereTotal($value)
 * @method static Builder|profile_job_center whereType($value)
 * @method static Builder|profile_job_center whereUpdatedAt($value)
 * @mixin Eloquent
 */
class profile_job_center extends Model
{
    /**
     * @var string
     */
    protected $table = "profile_job_centers";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'profile_job_centers_id',
        'name',
        'email_personal',
        'business_name',
        'rfc',
        'license',
        'messenger_personal',
        'whatsapp_personal',
        'cellphone',
        'facebook_personal',
        'web_page',
        'health_manager',
        'sanitary_license',
        'warning_service',
        'contract_service',
        'code',
        'image',
        'type',
        'total',
        'iva',
        'rfc_country',
        'timezone',
        'ssid',
        'companie'
    ];
}
