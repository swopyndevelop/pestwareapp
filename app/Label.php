<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Label
 * @package App
 * @property int $id
 * @property string $name
 * @mixin Eloquent
 */

class Label extends Model
{
    /**
     * @var string
     */
    protected $table = "labels";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];
}
