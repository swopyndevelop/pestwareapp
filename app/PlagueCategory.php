<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


/**
 * Class PlagueCategory
 *
 * @package App
 * @author Alberto Martínez
 * @version 19/02/2021
 * @property int $id
 * @property string $name
 * @property int $id_company
 * @property int $profile_job_center_id
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|plague_type newModelQuery()
 * @method static Builder|plague_type newQuery()
 * @method static Builder|plague_type query()
 * @method static Builder|plague_type whereCreatedAt($value)
 * @method static Builder|plague_type whereId($value)
 * @method static Builder|plague_type whereIdCompany($value)
 * @method static Builder|plague_type whereName($value)
 * @method static Builder|plague_type wherePlagueKey($value)
 * @method static Builder|plague_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class PlagueCategory extends Model
{
    /**
     * @var string
     */
    protected $table = "plague_categories";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'profile_job_center_id',
        'id_company',
        'visible'
    ];
}
