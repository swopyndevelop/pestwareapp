<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class aditional_info
 *
 * @package App
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @property int $id
 * @property int $product_id
 * @property string $file_route
 * @property string|null $original_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read product $product
 * @method static Builder|aditional_info newModelQuery()
 * @method static Builder|aditional_info newQuery()
 * @method static Builder|aditional_info query()
 * @method static Builder|aditional_info whereCreatedAt($value)
 * @method static Builder|aditional_info whereFileRoute($value)
 * @method static Builder|aditional_info whereId($value)
 * @method static Builder|aditional_info whereOriginalName($value)
 * @method static Builder|aditional_info whereProductId($value)
 * @method static Builder|aditional_info whereUpdatedAt($value)
 * @mixin Eloquent
 */
class aditional_info extends Model
{
    /**
     * @var string
     */
    protected $table = "aditional_infos";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'product_id',
        'file_route',
        'original_name'
    ];

    /**
    * @author Olga Rodríguez
    * @version 05/06/2019
    * Función para relación uno a muchos con tabla products
    */
    public function product()
    {
        return $this->belongsTo(product::class);
    }
}
