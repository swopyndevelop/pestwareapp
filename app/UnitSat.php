<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * UnitSat
 *
 * @property int $id
 * @property string $type
 * @property string $key
 * @property string $name
 * @mixin Eloquent
 */
class UnitSat extends Model
{
    /** @var string */
    protected $table = "units_sat";

    protected $fillable = [
        'id',
        'type',
        'key',
        'name',
    ];
}
