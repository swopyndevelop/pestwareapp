<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CheckListMonitoringResponse
 * @package App
 * @property int $id
 * @property int $id_station
 * @property int $id_inspection
 * @property int $id_check_monitoring_option
 * @property string $value
 * @property string $comments
 * @property string $actions
 * @property int $quantity
 * @property string $hour
 * @mixin Eloquent
 */

class CheckListMonitoringResponse extends Model
{
    protected $table = 'check_monitoring_responses';

    protected $fillable = ['id_station', 'id_inspection', 'id_check_monitoring_option','value','comments', 'actions', 'quantity', 'hour'];
}
