<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Area_tree
 * @package App
 * @property int $id
 * @property int $id_area
 * @property int $id_company
 * @property string $id_node
 * @property string $type_node
 * @property string $parent
 * @property string $text
 * @property int $id_type_area
 * @property string $visible
 * @mixin Eloquent
 */

class Area_tree extends Model
{
    //
    protected $table = 'areas_tree';

    protected $fillable = [
        'id_area',
        'id_company',
        'id_node',
        'type_node',
        'parent',
        'text',
        'id_type_area',
        'visible'
    ];
}
