<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\provider
 *
 * @property int $id
 * @property string $company
 * @property string $contact_name
 * @property string $contact_address
 * @property string $contact_cellphone
 * @property string $contact_email
 * @property string $bank
 * @property string $account_holder
 * @property string $account_number
 * @property string $clabe
 * @property string $rfc
 * @property int $profile_job_center_id
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|provider newModelQuery()
 * @method static Builder|provider newQuery()
 * @method static Builder|provider query()
 * @method static Builder|provider whereAccountHolder($value)
 * @method static Builder|provider whereAccountNumber($value)
 * @method static Builder|provider whereBank($value)
 * @method static Builder|provider whereClabe($value)
 * @method static Builder|provider whereCompanie($value)
 * @method static Builder|provider whereCompany($value)
 * @method static Builder|provider whereContactAddress($value)
 * @method static Builder|provider whereContactCellphone($value)
 * @method static Builder|provider whereContactEmail($value)
 * @method static Builder|provider whereContactName($value)
 * @method static Builder|provider whereCreatedAt($value)
 * @method static Builder|provider whereId($value)
 * @method static Builder|provider whereRfc($value)
 * @method static Builder|provider whereUpdatedAt($value)
 * @mixin Eloquent
 */
class provider extends Model
{
   /**
     * @var string
     */
    protected $table = "providers";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'company',
        'contact_name',
        'contact_address',
        'contact_cellphone',
        'contact_email',
        'bank',
        'account_holder',
        'account_number',
        'clabe',
        'rfc',
        'profile_job_center_id',
        'companie'
    ];

}
