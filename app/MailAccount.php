<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MailAccount
 * @package App
 * @property int $id
 * @property int $id_customer
 * @property string $document
 * @property string|null $name
 * @property string|null $job
 * @property string $email
 * @mixin Eloquent
 */
class MailAccount extends Model
{
    protected $table = 'mail_accounts';

    protected $fillable = [
        'id_customer',
        'document',
        'name',
        'job',
        'email'
    ];
}
