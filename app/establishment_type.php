<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class establishment_type
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property string $name
 * @property int $id_company
 * @property int $profile_job_center_id
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|establishment_type newModelQuery()
 * @method static Builder|establishment_type newQuery()
 * @method static Builder|establishment_type query()
 * @method static Builder|establishment_type whereCreatedAt($value)
 * @method static Builder|establishment_type whereId($value)
 * @method static Builder|establishment_type whereIdCompany($value)
 * @method static Builder|establishment_type whereName($value)
 * @method static Builder|establishment_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class establishment_type extends Model
{
    /**
     * @var string
     */
    protected $table = "establishment_types";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'profile_job_center_id',
        'id_company',
        'visible'
    ];
}
