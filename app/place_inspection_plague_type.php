<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class place_inspection_plague_type
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/06/2019
 * @property int $id
 * @property int $place_inspection_id
 * @property int|null $plague_type_id
 * @property int|null $id_infestation_degree
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|place_inspection_plague_type newModelQuery()
 * @method static Builder|place_inspection_plague_type newQuery()
 * @method static Builder|place_inspection_plague_type query()
 * @method static Builder|place_inspection_plague_type whereCreatedAt($value)
 * @method static Builder|place_inspection_plague_type whereId($value)
 * @method static Builder|place_inspection_plague_type whereIdInfestationDegree($value)
 * @method static Builder|place_inspection_plague_type wherePlaceInspectionId($value)
 * @method static Builder|place_inspection_plague_type wherePlagueTypeId($value)
 * @method static Builder|place_inspection_plague_type whereUpdatedAt($value)
 * @mixin Eloquent
 */
class place_inspection_plague_type extends Model
{
    /**
     * @var string
     */
    protected $table = "place_inspection_plague_type";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'place_inspection_id',
        'plague_type_id',
        'id_infestation_degree'
    ];
}
