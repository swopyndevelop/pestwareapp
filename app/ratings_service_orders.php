<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class ratings_service_orders
 *
 * @package App
 * @author Alberto Martínez
 * @version 18/08/2019
 * @property int $id
 * @property int $id_event
 * @property int $user_id
 * @property int $id_service_order
 * @property int $rating
 * @property string|null $comments
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ratings_service_orders newModelQuery()
 * @method static Builder|ratings_service_orders newQuery()
 * @method static Builder|ratings_service_orders query()
 * @method static Builder|ratings_service_orders whereComments($value)
 * @method static Builder|ratings_service_orders whereCreatedAt($value)
 * @method static Builder|ratings_service_orders whereId($value)
 * @method static Builder|ratings_service_orders whereIdEvent($value)
 * @method static Builder|ratings_service_orders whereIdServiceOrder($value)
 * @method static Builder|ratings_service_orders whereRating($value)
 * @method static Builder|ratings_service_orders whereUpdatedAt($value)
 * @method static Builder|ratings_service_orders whereUserId($value)
 * @mixin Eloquent
 */
class ratings_service_orders extends Model
{
    /**
     * @var string
     */
    protected $table = "ratings_service_orders";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'id_service_order',
        'rating',
        'comments'
    ];
}