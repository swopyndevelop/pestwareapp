<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomQr
 * @package App
 * @property int $id
 * @property int $id_monitoring
 * @property string $title
 * @property string $subtitle
 * @property string $description
 */

class CustomQr extends Model
{
    protected $table = 'custom_qrs';

    protected $fillable = ['id_monitoring','title','subtitle','description'];
}
