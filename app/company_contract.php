<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class company_contract
 *
 * @package App
 * @author Olga Rodríguez
 * @version 13/03/2019
 * @property int $id
 * @property mixed $contract
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|company_contract newModelQuery()
 * @method static Builder|company_contract newQuery()
 * @method static Builder|company_contract query()
 * @method static Builder|company_contract whereContract($value)
 * @method static Builder|company_contract whereCreatedAt($value)
 * @method static Builder|company_contract whereId($value)
 * @method static Builder|company_contract whereUpdatedAt($value)
 * @mixin Eloquent
 */
class company_contract extends Model
{
    /**
     * @var string
     */
    protected $table = "company_contracts";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'contract'
    ];
}
