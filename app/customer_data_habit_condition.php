<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class customer_data_habit_condition
 *
 * @package App
 * @author Olga Rodríguez
 * @version 12/01/2019
 * @property int $id
 * @property int $customer_data_id
 * @property int $habit_condition_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|customer_data_habit_condition newModelQuery()
 * @method static Builder|customer_data_habit_condition newQuery()
 * @method static Builder|customer_data_habit_condition query()
 * @method static Builder|customer_data_habit_condition whereCreatedAt($value)
 * @method static Builder|customer_data_habit_condition whereCustomerDataId($value)
 * @method static Builder|customer_data_habit_condition whereHabitConditionId($value)
 * @method static Builder|customer_data_habit_condition whereId($value)
 * @method static Builder|customer_data_habit_condition whereUpdatedAt($value)
 * @mixin Eloquent
 */
class customer_data_habit_condition extends Model
{
    /**
     * @var string
     */
    protected $table = "customer_data_habit_condition";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'customer_data_id',
        'habit_condition_id'
    ];
}
