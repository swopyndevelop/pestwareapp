<?php

namespace App\Http\Controllers\Jobtitleprofile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class JobofficefunctionController extends Controller
{
    protected $rules =
    [
    'Oficina' => 'Required',
    'Id_Profile' => 'Required',
    ];

    public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} else {
        //add data 
			$office = New JobOfficeFunction;
			$office->job_office_functions_id = $request->Id_Profile;
			$office->job_title_profiles_id = $request->Id_Profile;
			$office->function = $request->Oficina;

			$office->save();

			return response()->json($office);
		}
	}
	public function delete(Request $request){
		$tmp = DB::table('job_office_functions')->where('id', $request->Id)->delete();
        return response()->json($tmp);
	}
}
