<?php

namespace App\Http\Controllers\Jobtitleprofile;

use App\JobTitleSoftware;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class JobtitlesoftwareController extends Controller
{
    protected $rules =
    [
    'Software' => 'Required',
    'Id_Profile' => 'Required',
    ];

    public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} else {
			$job = New JobTitleSoftware;
			$job->job_title_softwares_id = $request->Id_Profile;
			$job->job_title_profiles_id = $request->Id_Profile;
			$job->software_name = $request->Software;

			$job->save();

			return response()->json($job);
		}
	}
	public function delete(Request $request){
		$tmp = DB::table('job_title_softwares')->where('id', $request->Id)->delete();
        return response()->json($tmp);
	}
}
