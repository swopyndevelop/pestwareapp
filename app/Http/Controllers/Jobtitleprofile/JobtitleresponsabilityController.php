<?php

namespace App\Http\Controllers\Jobtitleprofile;

use App\JobTitleResponsability;
use App\JobTitleFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class JobtitleresponsabilityController extends Controller
{
    protected $rules =
    [
    'Responsabilidad' => 'Required',
    'Id_Profile' => 'Required',
    ];


    public function add(Request $request){
		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} else {
        //add data 
			$jobtitle = New JobTitleResponsability;
			$jobtitle->job_title_responsabilities_id = $request->Id_Profile;
			$jobtitle->job_title_profiles_id = $request->Id_Profile;
			$jobtitle->responsability_name = $request->Responsabilidad;

			$jobtitle->save();

			return response()->json($jobtitle);
		}
	}
	 public function delete(Request $request){
	 	$tmp = DB::table('job_title_responsabilities')->where('id', $request->Id)->delete();
        return response()->json($tmp);
	}
}
