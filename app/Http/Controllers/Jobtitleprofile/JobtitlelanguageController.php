<?php

namespace App\Http\Controllers\Jobtitleprofile;

use App\JobTitleLanguage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class JobtitlelanguageController extends Controller
{
	protected $rules =
    [
    'Lenguaje' => 'Required',
    'languageread' => 'Required',
    'languagespeak' => 'Required',
    'languagewrite' => 'Required',
    'languagelisten' => 'Required',
    'Id_Profile' => 'Required',
    ];

	public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} else {
        //add data 

			$lang =New JobTitleLanguage;
			$level = (($request->languagewrite+$request->languageread+$request->languagespeak+$request->languagelisten)/4);
			$lang->job_title_languages_id = $request->Id_Profile;
			$lang->job_title_profiles_id = $request->Id_Profile;
			$lang->language = $request->Lenguaje;
			$lang->level_write = $request->languagewrite;
			$lang->level_read = $request->languageread;
			$lang->level_speak = $request->languagespeak;
			$lang->level_listen = $request->languagelisten;
			$lang->level = $level;
			$lang->save();

			return response()->json($lang);
		}
	}
	public function delete(Request $request)
	{
		$tmp = DB::table('job_title_languages')->where('id', $request->Id)->delete();
		dd();
        return response()->json($tmp);
	}
	
}
