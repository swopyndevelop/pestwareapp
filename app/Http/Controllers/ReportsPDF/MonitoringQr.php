<?php


namespace App\Http\Controllers\ReportsPDF;


use App\companie;
use App\customer;
use App\customer_branche;
use App\CustomQr;
use App\Monitoring;
use App\Notifications\LogsNotification;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class MonitoringQr
{
    public static function build($id, $companyId, $partIndex)
    {
        try {
            $monitoring = Monitoring::find($id);
            $customer = customer::find($monitoring->id_customer);
            $customerBranch = customer_branche::find($monitoring->id_customer_branch);
            $customQrs = CustomQr::where('id_monitoring', $id)->first();
            $company = companie::find($companyId);
            $pdfLogo = $company->pdf_logo;
            $sizeQr = 100;
            if ($companyId === 646) $sizeQr = 140;

            if (!$customQrs) {
                $customQr = new CustomQr;
                $customQr->id_monitoring = $id;
                $customQr->title = 'Estación de control';
                $customQr->subtitle = 'No alterar estación';
                $customQr->save();
                $customQrs = $customQr;
            }

            $totalsStations = DB::table('monitoring_trees as mt')
                ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
                ->join('type_areas as ta', 'mn.id_type_area', 'ta.id')
                ->join('type_stations as ts', 'mt.id_type_station', 'ts.id')
                ->where('mt.id_monitoring', $id)
                ->where('ta.type', 'station')
                ->select('mn.name', 'mt.id as id_node', 'mt.parent', 'mt.text', 'mt.id', 'mt.id_monitoring', 'ts.id_type_area')
                ->get();

            $profileJobCenter = DB::table('profile_job_centers')->where('companie', $company->id)->first();
            $addressProfile = DB::table('address_job_centers')->where('profile_job_centers_id', $profileJobCenter->profile_job_centers_id)->first();
            if ($addressProfile) $addressCompany = $addressProfile->street . " #" . $addressProfile->num_ext . ", " . $addressProfile->location;
            else $addressCompany = "";

            $stations = new Collection();

            if ($partIndex != -1) {
                $parts = $totalsStations->chunk(45);
                foreach ($parts as $i => $part) {
                    if ($i == $partIndex) {
                        foreach ($part as $station) {
                            $perimeter = DB::table('monitoring_trees')
                                ->where('id_monitoring', $station->id_monitoring)
                                ->where('id_node', $station->parent)
                                ->first();
                            $perimeterText = $perimeter->text;
                            $zone = DB::table('monitoring_trees')
                                ->where('id_monitoring', $station->id_monitoring)
                                ->where('id_node', $perimeter->parent)
                                ->first();
                            $zoneText = $zone->text;
                            $stationNumber = explode('#', $station->text);
                            $data = [
                                'station' => $station->text,
                                'type' => $station->id_type_area == 5 ? "Trampa de Captura" : $station->name,
                                'id_monitoring' => $id,
                                'monitoring' => 'M-' . $customer->id,
                                'instance' => substr($company->name, 0, 30),
                                'licence' => $profileJobCenter->license,
                                'phone' => $profileJobCenter->whatsapp_personal,
                                'address' => substr($addressCompany, 0,80),
                                'address_two' => substr($addressCompany, 28),
                                'title' => $customQrs->title,
                                'subtitle' => $customQrs->subtitle,
                                'description' => 'Estación ' . $station->name,
                                'station_number' => $stationNumber[1],
                                //'customer' => $customerBranch == null ? substr($customer->name, 0,28) : substr($customerBranch->name,0,28), //TODO: Name By Branch
                                'customer' => substr($customer->name, 0,28),
                                'perimeter' => substr($perimeterText, 0, 26),
                                'zone' => $zoneText,
                                'id_node' => $station->id_node
                            ];
                            $qrcode = base64_encode(QrCode::format('svg')->size($sizeQr)->errorCorrection('H')->generate(implode(',', $data)));
                            $data['qr'] = $qrcode;
                            $stations->push($data);
                        }
                    }
                }
            }

            else {
                foreach ($totalsStations as $station) {
                    $perimeter = DB::table('monitoring_trees')
                        ->where('id_monitoring', $station->id_monitoring)
                        ->where('id_node', $station->parent)
                        ->first();
                    $perimeterText = $perimeter->text;
                    $zone = DB::table('monitoring_trees')
                        ->where('id_monitoring', $station->id_monitoring)
                        ->where('id_node', $perimeter->parent)
                        ->first();
                    $zoneText = $zone->text;
                    $stationNumber = explode('#', $station->text);
                    $data = [
                        'station' => $station->text,
                        'type' => $station->id_type_area == 5 ? "Trampa de Captura" : $station->name,
                        'id_monitoring' => $id,
                        'monitoring' => 'M-' . $customer->id,
                        'instance' => substr($company->name, 0, 30),
                        'licence' => $profileJobCenter->license,
                        'phone' => $profileJobCenter->whatsapp_personal,
                        'address' => substr($addressCompany, 0,80),
                        'address_two' => substr($addressCompany, 28),
                        'title' => $customQrs->title,
                        'subtitle' => $customQrs->subtitle,
                        'description' => 'Estación ' . $station->name,
                        'station_number' => $stationNumber[1],
                        //'customer' => $customerBranch == null ? substr($customer->name, 0,28) : substr($customerBranch->name,0,28), //TODO: Name By Branch
                        'customer' => substr($customer->name, 0,28),
                        'perimeter' => substr($perimeterText, 0, 26),
                        'zone' => $zoneText,
                        'id_node' => $station->id_node
                    ];
                    $qrcode = base64_encode(QrCode::format('svg')->size($sizeQr)->errorCorrection('H')->generate(implode(',', $data)));
                    $data['qr'] = $qrcode;
                    $stations->push($data);
                }
            }

            $rows = new Collection();
            $stationsTmp = new Collection();
            $count = 0;
            if ($companyId === 646) {
                // Group by 5 elements
                foreach ($stations as $k => $station) {
                    $count++;
                    $stationsTmp->push($station);
                    if ($count == 5) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    } elseif ($count == 4 && $k == $stations->count() - 1) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    } elseif ($count == 3 && $k == $stations->count() - 1) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    } elseif ($count == 2 && $k == $stations->count() - 1) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    } elseif ($count == 1 && $k == $stations->count() - 1) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    }
                }
            }

            else {
                // Group by 3 elements
                foreach ($stations as $k => $station) {
                    $count++;
                    $stationsTmp->push($station);
                    if ($count == 3) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    } elseif ($count == 2 && $k == $stations->count()-1) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    } elseif ($count == 1 && $k == $stations->count()-1) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    }
                }
            }

            return [
                'rows' => $rows,
                'stations' => $stations,
                'customer' => $customer,
                'pdfLogo' => $pdfLogo,
                'folio' => $monitoring->id_monitoring,
                'no_parts' => $monitoring->no_parts
            ];
        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification($exception->getLine(), $exception->getMessage(), 3));
            return [];
        }
    }
}