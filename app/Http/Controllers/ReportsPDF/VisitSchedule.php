<?php


namespace App\Http\Controllers\ReportsPDF;


use App\address_job_center;
use App\Common\CommonImage;
use App\companie;
use App\customer;
use App\Http\Controllers\Security\SecurityController;
use App\profile_job_center;
use Carbon\Carbon;
use DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class VisitSchedule
{
    public static function build($customerId, $date)
    {
        $customer = customer::find($customerId);
        $company = companie::find($customer->companie);
        $jobCenterProfile = profile_job_center::find($customer->id_profile_job_center);
        $addressProfile = address_job_center::where('address_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();
        $sanitary_license_qr = env('URL_STORAGE_FTP') . $jobCenterProfile->sanitary_license;
        if ($jobCenterProfile->sanitary_license != null) $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
        else $qrcode = null;
        $events = self::getAllEventsByCustomer($customerId, $date);
        $firms = self::getFirstEventByCustomer($customerId, $date);
        $period = self::getPeriodMonth($events);
        $firmUrl = "";
        $firmUrlTechnician = "";
        if ($firms) {
            $firmUrl = CommonImage::getTemporaryUrl($firms->file_route, 5);
            if ($firms->file_route_firm != null) $firmUrlTechnician = CommonImage::getTemporaryUrlPublic($firms->file_route_firm, 5);
        }

        return [
            'months' => $events,
            'period' => $period,
            'customer' => $customer,
            'company' => $company,
            'jobCenterProfile' => $jobCenterProfile,
            'addressProfile' => $addressProfile,
            'qrcode' => $qrcode,
            'firms' => $firms,
            'firmUrl' => $firmUrl,
            'firmUrlTechnician' => $firmUrlTechnician
        ];
    }

    private static function getAllEventsByCustomer($customerId, $date) {
        return DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('employees as emp', 'ev.id_employee', 'emp.id')
            ->where('q.id_customer', $customerId)
            ->whereYear('initial_date', $date)
            ->orderBy('initial_date')
            ->get()
            ->groupBy(function($val) {
                setlocale(LC_ALL, 'es_MX');
                $mes = Carbon::parse($val->initial_date);
                $mes->format('F');
                return $mes->formatLocalized('%B');
            });
    }

    private static function getFirstEventByCustomer($customerId, $date) {
        return DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('employees as e', 'ev.id_employee', 'e.id')
            ->join('service_firms as sf', 'so.id', 'sf.id_service_order')
            ->where('q.id_customer', $customerId)
            ->whereYear('initial_date', $date)
            ->where('ev.id_status', 4)
            ->select('e.file_route_firm','e.name','sf.file_route')
            ->first();
    }

    private static function getPeriodMonth($events) {
        $firstKey = "";
        $lastKey = "";

        $counter = 0;
        foreach ($events as $key => $event) {
            if ($counter == 0) $firstKey = $key;
            $lastKey = $key;
            $counter++;
        }
        return [self::translateMonth($firstKey), self::translateMonth($lastKey)];
    }

    private static function translateMonth($month) {
        if($month == "January") $month = "Enero";
        elseif($month == "February") $month = "Febrero";
        elseif($month == "March") $month = "Marzo";
        elseif($month == "April") $month = "Abril";
        elseif($month == "May") $month = "Mayo";
        elseif($month == "June") $month = "Junio";
        elseif($month == "July") $month = "Julio";
        elseif($month == "August") $month = "Agosto";
        elseif($month == "September") $month = "Septiembre";
        elseif($month == "October") $month = "Octubre";
        elseif($month == "November") $month = "Noviembre";
        elseif($month == "December") $month = "Diciembre";
        return $month;
    }
}