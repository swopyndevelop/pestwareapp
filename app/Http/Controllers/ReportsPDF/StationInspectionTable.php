<?php


namespace App\Http\Controllers\ReportsPDF;


use App\Common\CommonImage;
use App\profile_job_center;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class StationInspectionTable
{
    public static function build($id, $company)
    {
        $inspection = DB::table('station_inspections as si')
            ->join('monitorings as m', 'si.id_monitoring', 'm.id')
            ->join('employees as em', 'si.id_technician', 'em.id')
            ->join('service_orders as so', 'si.id_service_order', 'so.id')
            ->join('events as ev', 'si.id_service_order', 'ev.id_service_order')
            ->join('quotations as qu', 'so.id_quotation', 'qu.id')
            ->join('customers as cu', 'qu.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'cd.customer_id','cu.id')
            ->where('si.id_service_order', $id)
            ->select('si.id', 'si.id_inspection', 'so.id_service_order', 'm.id_monitoring', 'si.date', 'ev.start_event',
                'ev.final_event', 'em.name as technician', 'em.file_route_firm', 'cu.name as customer', 'so.id_job_center','cu.establishment_name',
                'cd.address','cu.municipality','cu.cellphone','cd.email','cd.billing')
            ->first();

        $stations = DB::table('check_monitoring_responses as cmr')
            ->join('monitoring_trees as mt', 'cmr.id_station', 'mt.id')
            ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
            ->where('cmr.id_inspection', $inspection->id)
            ->select('mt.id as id_node_tree', 'cmr.*', 'mt.*', 'mn.*', 'cmr.id as id_response_station')
            ->get();
        $stations->map(function($station) use ($inspection){
            $perimeter = DB::table('monitoring_trees')
                ->where('id_monitoring', $station->id_monitoring)
                ->where('id_node', $station->parent)
                ->first();
            $perimeterText = $perimeter->text;
            $zone = DB::table('monitoring_trees')
                ->where('id_monitoring', $station->id_monitoring)
                ->where('id_node', $perimeter->parent)
                ->first();
            $zoneText = $zone->text;
            $station->perimeter = $perimeterText;
            $station->zone = $zoneText;
            $conditions = DB::table('station_conditions as sc')
                ->join('monitoring_conditions as mc', 'sc.id_condition', 'mc.id')
                ->where('sc.id_inspection', $inspection->id)
                ->where('sc.id_node', $station->id_node_tree)
                ->select('mc.name')
                ->get();
            $station->conditions = $conditions;
            $plagues = DB::table('plagues_response_inspection as pri')
                ->join('plague_types as pt', 'pri.id_plague', 'pt.id')
                ->where('pri.id_station_response', $station->id_response_station)
                ->select('pt.name as plague', 'pri.quantity as quantity_plague')
                ->get();
            $station->plagues = $plagues;
            $station->plagues_count = $plagues->count();
        });

        $groupByStations = $stations;

        $images = DB::table('check_list_images')->where('id_inspection', $inspection->id)->get();
        foreach ($images as $image) {
            $images->map(function ($image) {
                $image->url_s3 = CommonImage::getTemporaryUrl($image->urlImage, 5);
            });
        }

        $customerFirm = DB::table('service_firms as sf')
            ->select('sf.file_route','sf.other_name')
            ->where('sf.id_service_order', $id)->first();

        $firmUrl = "";
        $firmUrlTechnician = "";
        if ($customerFirm) {
            $firmUrl = CommonImage::getTemporaryUrl($customerFirm->file_route, 5);
        }
        if ($inspection->file_route_firm != null) $firmUrlTechnician = CommonImage::getTemporaryUrlPublic($inspection->file_route_firm, 5);

        $date = Carbon::now();

        $idCompanie = $company;
        $jobCenterProfile = profile_job_center::where('id', $inspection->id_job_center)->first();
        $addressProfile = DB::table('address_job_centers')->where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

        $imagen = DB::table('companies')
            ->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook', 'warnings_service', 'contract_service', 'pdf_sanitary_license',
                'rfc', 'bussines_name', 'health_manager', 'email')->where('id', $idCompanie)->first();
        $sanitary_license_qr = env('URL_STORAGE_FTP') . $jobCenterProfile->sanitary_license;

        if ($jobCenterProfile->sanitary_license != null) $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
        else $qrcode = null;

        return [
            'stations' => $groupByStations,
            'inspection' => $inspection,
            'images' => $images,
            'customerFirm' => $customerFirm,
            'date' => $date,
            'imagen' => $imagen,
            'jobCenterProfile' => $jobCenterProfile,
            'addressProfile' => $addressProfile,
            'qrcode' => $qrcode,
            'firmUrl' => $firmUrl,
            'firmUrlTechnician' => $firmUrlTechnician
        ];
    }
}