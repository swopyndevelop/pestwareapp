<?php


namespace App\Http\Controllers\ReportsPDF;


use App\Area_tree;
use App\Common\CommonImage;
use App\companie;
use App\Console\Commands\DbDump;
use App\customer_branche;
use App\Http\Controllers\Business\CommonCompany;
use App\profile_job_center;
use App\ShareServiceOrder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ServiceCertificate
{
    public static function build($id)
    {
        $order = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
            ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
            ->join('service_orders as so','e.id_service_order','so.id')
            ->join('payment_methods as pm','so.id_payment_method','pm.id')
            ->join('payment_ways as pw','so.id_payment_way','pw.id')
            ->join('users as u','so.user_id','u.id')
            ->join('statuses as st','so.id_status','st.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('establishment_types as et','q.establishment_id','et.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->select('so.id as order', 'so.id_service_order', 'so.created_at as date', 'e.initial_hour',
                'e.initial_date', 'em.name', 'em.file_route_firm', 'c.name as cliente','c.establishment_name as empresa',
                'c.cellphone','cd.address','c.municipality', 'q.id_plague_jer','pt.plague_key',
                'u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                'e.id as event','c.colony','q.id as quotation','e.final_hour','e.final_date','so.address as a_rfc',
                'so.email as e_rfc','so.bussiness_name', 'so.observations','cd.billing','d.id as discount',
                'd.percentage','ex.id as extra','ex.amount','q.construction_measure','q.garden_measure','q.price',
                'q.establishment_id as e_id','pm.id as pm_id','pw.id as pw_id','pm.name as metodo','pw.name as tipo',
                'so.id_job_center', 'cd.email', 'q.companie as compania','e.start_event', 'e.final_event', 'pl.show_price',
                'et.name as establecimiento', 'so.customer_branch_id', 'so.total', 'cd.customer_id',
                'pl.portada as pdf_mip', 'pjc.sanitary_license', 'pl.days_expiration_certificate', 'c.show_price as show_price_customer',
                'so.date_expiration_certificate', 'so.area_node_id', 'c.days_expiration_certificate as days_expiration_certificate_customer',
                'pl.is_disinfection','so.is_main','so.is_shared','em.id as employeeId')
            ->where('so.id',$id)
            ->first();

        $imagen = DB::table('companies')
            ->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook', 'warnings_service',
                'contract_service', 'pdf_sanitary_license', 'rfc','bussines_name', 'health_manager', 'email')
            ->where('id', $order->compania)
            ->first();
        $sanitary_license_qr = env('URL_STORAGE_FTP') . $order->sanitary_license;

        if ($order->sanitary_license != null) {
            $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
        } else $qrcode = null;

        $employeeAuxiliares = ShareServiceOrder::join('employees as e','shares_service_order.id_employee','e.id')
            ->join('service_orders as so','shares_service_order.id_service_order','so.id')
            ->join('events as ev','ev.id_service_order','so.id')
            ->where('id_service_order_main',$id)
            ->where('shares_service_order.id_employee','<>', $order->employeeId)
            ->where('ev.id_status', 4)
            ->select('e.name','shares_service_order.id_service_order')
            ->get();

        $plagues = collect([]);
        $placeInspectionsMerge = new Collection();
        $order_cleaning_place_conditions = collect([]);
        $placeConditionsMerge = new Collection();
        $plague_controls = collect();
        $application_methods = collect();
        $plague_controls_products = collect();

        if ($employeeAuxiliares) {
            foreach ($employeeAuxiliares as $employeeAuxiliar){
                //Grado de Infestacion de Plagas
                $plagues = DB::table('place_inspection_plague_type as pt')
                    ->join('place_inspections as pi','pt.place_inspection_id','pi.id')
                    ->join('plague_types as pl','pt.plague_type_id','pl.id')
                    ->join('infestation_degrees as ie','pt.id_infestation_degree','ie.id')
                    ->select('pl.name as plaga','ie.name as infestacion')
                    ->where('pi.id_service_order',$employeeAuxiliar->id_service_order)
                    ->get();

                $place_inspections = DB::table('place_inspections')->where('id_service_order',$employeeAuxiliar->id_service_order)->first();
                $placeInspectionsMerge->push($place_inspections);

                //Orden Y Limpieza
                $order_cleaning_place_conditions = DB::table('order_cleaning_place_condition as co')
                    ->join('place_conditions as pc','co.place_condition_id','pc.id')
                    ->join('order_cleanings as o','co.order_cleaning_id','o.id')
                    ->select('co.place_condition_id','o.name as order')
                    ->where('pc.id_service_order',$employeeAuxiliar->id_service_order)
                    ->get();

                // Areas restringidas
                $place_conditions = DB::table('place_conditions')
                    ->where('id_service_order',$employeeAuxiliar->id_service_order)
                    ->select('restricted_access')
                    ->first();
                $placeConditionsMerge->push($place_conditions);

                // Área a Controlar
                $plague_controls = DB::table('plague_controls as pc')
                    ->join('service_orders as so','pc.id_service_order','so.id')
                    ->join('events as e','e.id_service_order','so.id')
                    ->join('employees as em','e.id_employee','em.id')
                    ->select('pc.id_service_order','pc.control_areas','pc.commentary','em.name')
                    ->where('pc.id_service_order',$employeeAuxiliar->id_service_order)
                    ->get();

                // Métodos de aplicación
                $application_methods = DB::table('plague_controls as pc')
                    ->join('plague_controls_application_methods as pca','pc.id','pca.plague_control_id')
                    ->join('application_methods as am','pca.id_application_method','am.id')
                    ->select('id_service_order','control_areas','am.name as apli')
                    ->where('id_service_order',$employeeAuxiliar->id_service_order)
                    ->get();

                //Control de Plagas Productos
                $plague_controls_products = DB::table('plague_controls_products as pc')
                    ->join('plague_controls as pl','pc.plague_control_id','pl.id')
                    ->join('service_orders as so','pl.id_service_order','so.id')
                    ->join('events as e','e.id_service_order','so.id')
                    ->join('employees as em','e.id_employee','em.id')
                    ->join('products as p','pc.id_product','p.id')
                    ->join('product_units as pu', 'p.id_unit', 'pu.id')
                    ->select('pl.id_service_order','p.name as product','pc.dose','pc.quantity','p.id as id_product','pl.control_areas',
                        'p.active_ingredient', 'p.register', 'pc.quantity as cantidad','em.name as name_employee', 'pu.name as type_unit')
                    ->where('pl.id_service_order',$employeeAuxiliar->id_service_order)
                    ->get();
            }
        }

        //Grado de Infestacion de Plagas
        $plaga = DB::table('place_inspection_plague_type as pt')
            ->join('place_inspections as pi','pt.place_inspection_id','pi.id')
            ->join('plague_types as pl','pt.plague_type_id','pl.id')
            ->join('infestation_degrees as ie','pt.id_infestation_degree','ie.id')
            ->select('pl.name as plaga','ie.name as infestacion')
            ->where('pi.id_service_order',$id)
            ->get();

        $employeeAuxiliares != null ? $plaguesMerge = $plagues->merge($plaga) : $plaguesMerge = $plaga;

        //Condiciones del Lugar
        $ins = DB::table('order_cleaning_place_condition as co')
            ->join('place_conditions as pc','co.place_condition_id','pc.id')
            ->join('order_cleanings as o','co.order_cleaning_id','o.id')
            ->select('co.place_condition_id','o.name as order')
            ->where('pc.id_service_order',$id)
            ->get();
        $employeeAuxiliares != null ? $orderCleaningPlaceConditionsMerge = $order_cleaning_place_conditions->merge($ins) : $orderCleaningPlaceConditionsMerge = $ins;

        $ani = DB::table('place_inspections')->where('id_service_order',$id)->first();
        $employeeAuxiliares != null ? $placeInspectionsMerge = $placeInspectionsMerge->push($ani) : $placeInspectionsMerge = $ani;

        //Control de Plagas
        $control = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('events as e','e.id_service_order','so.id')
            ->join('employees as em','e.id_employee','em.id')
            ->join('products as p','pc.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('pl.id_service_order','p.name as product','pc.dose','pc.quantity','p.id as id_product','pl.control_areas',
                'p.active_ingredient', 'p.register','pc.quantity as cantidad','em.name as name_employee', 'pu.name as type_unit')
            ->where('pl.id_service_order', $id)
            ->get();

        $employeeAuxiliares != null ? $plagueControlProductsMerge = $plague_controls_products->merge($control) : $plagueControlProductsMerge = $control;

        $area_c = DB::table('plague_controls as pc')
            ->join('service_orders as so','pc.id_service_order','so.id')
            ->join('events as e','e.id_service_order','so.id')
            ->join('employees as em','e.id_employee','em.id')
            ->select('pc.id_service_order','pc.control_areas','pc.commentary','em.name')
            ->where('pc.id_service_order',$id)
            ->get();
        $employeeAuxiliares != null ? $plagueControlsMerge = $plague_controls->merge($area_c) : $plagueControlsMerge = $area_c;

        $area_cd = DB::table('plague_controls as pc')
            ->join('plague_controls_application_methods as pca','pc.id','pca.plague_control_id')
            ->join('application_methods as am','pca.id_application_method','am.id')
            ->select('id_service_order','control_areas','am.name as apli')
            ->where('id_service_order',$id)
            ->get();
        $employeeAuxiliares != null ? $applicationMethodsMerge = $application_methods->merge($area_cd) : $applicationMethodsMerge = $area_cd;

        $indi = DB::table('place_conditions')->where('id_service_order',$id)->select('indications')->first();
        $place_conditions_restrict = DB::table('place_conditions')->where('id_service_order',$id)->select('restricted_access')->first();
        $employeeAuxiliares != null ? $placeConditionsMerge = $placeConditionsMerge->push($place_conditions_restrict) : $placeConditionsMerge = $place_conditions_restrict;

        $f = DB::table('service_firms as sf')
            ->select('sf.file_route', 'sf.other_name')
            ->where('sf.id_service_order',$id)->first();

        $firmUrl = "";
        $firmUrlTechnician = "";
        if ($f) {
            $firmUrl = CommonImage::getTemporaryUrl($f->file_route, 5);
        }
        if ($order->file_route_firm != null) $firmUrlTechnician = CommonImage::getTemporaryUrlPublic($order->file_route_firm, 5);

        //Ajuste de datos si es cotización personalizada
        if ($order->customer_branch_id != null) {
            $customerBranch = customer_branche::find($order->customer_branch_id);
            $order->total = $order->total;
            $order->empresa = $customerBranch->name;
            $order->address = $customerBranch->address;
            $order->address_number = $customerBranch->address_number;
            $order->colony = $customerBranch->colony;
            $order->municipality = $customerBranch->municipality;
            $order->state = $customerBranch->state;
        }

        $symbol_country = CommonCompany::getSymbolByCountryWithCompanyId($order->compania);
        $jobCenterProfile = profile_job_center::where('id', $order->id_job_center)->first();
        $addressProfile = DB::table('address_job_centers')
            ->where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

        // Validación para ordenes de tipo qr área con certificado.
        $textNode = "";
        if ($order->area_node_id != null) {
            $areaTree = Area_tree::find($order->area_node_id);
            $textNode = $areaTree->text;
        }
        $order->textNode = $textNode;

        $order->customTax = $order->total * 0.07;
        $order->customTotal = $order->customTax + $order->total;

        return [
            'order' => $order,
            'jobCenterProfile' => $jobCenterProfile,
            'ins' => $orderCleaningPlaceConditionsMerge,
            'plaga' => $plaguesMerge,
            'control' => $plagueControlProductsMerge,
            'f' => $f,
            'area_c' => $plagueControlsMerge,
            'ani' => $placeInspectionsMerge,
            'indi' => $indi,
            'placeConditionsMerge' => $placeConditionsMerge,
            'area_cd' => $applicationMethodsMerge,
            'imagen'=> $imagen,
            'qrcode' => $qrcode,
            'addressProfile' => $addressProfile,
            'symbol_country' => $symbol_country,
            'firmUrl' => $firmUrl,
            'firmUrlTechnician' => $firmUrlTechnician,
            'employeeAuxiliares' => $employeeAuxiliares
        ];
    }
}