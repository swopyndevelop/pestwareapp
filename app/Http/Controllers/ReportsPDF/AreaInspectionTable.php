<?php

namespace App\Http\Controllers\ReportsPDF;

use App\Area;
use App\Area_tree;
use App\AreaInspectionPhotos;
use App\Common\CommonImage;
use App\employee;
use App\profile_job_center;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AreaInspectionTable
{
    public static function build($id)
    {
        $order = DB::table('service_orders as so')
            ->join('events as ev', 'ev.id_service_order', 'so.id')
            ->join('employees as emp', 'ev.id_employee', 'emp.id')
            ->join('area_inspections as ai', 'ai.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as cu', 'q.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
            ->join('areas as a', 'a.id_customer', 'cu.id')
            ->where('so.id', $id)
            ->select('so.id_service_order', 'ai.date_inspection', 'ai.hour_inspection', 'cu.name as customer',
                'cu.establishment_name', 'cu.colony', 'cu.municipality', 'cd.address', 'cd.address_number', 'cd.state',
                'so.id', 'a.id_area', 'ev.start_event', 'ev.final_event', 'emp.name as technician', 'ev.id_job_center',
                'cu.cellphone','cd.email', 'cu.id as customer_id','so.companie','cd.billing')
            ->first();

        $inspections = DB::table('area_inspections as ai')
            ->where('ai.id_service_order', $id)
            ->get();
        $area = Area::where('id_customer', $order->customer_id)->first();
        $inspections = (new AreaInspectionTable)->mapInspections($inspections, $area->id);

        // Data for headers report pdf
        $customerFirm = DB::table('service_firms as sf')
            ->select('sf.file_route')
            ->where('sf.id_service_order', $id)->first();
        $date = Carbon::now();
        $idCompanie = $order->companie;
        $image = DB::table('companies')
            ->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook', 'warnings_service',
                'contract_service', 'pdf_sanitary_license', 'rfc','bussines_name','health_manager','email')
            ->where('id', $idCompanie)
            ->first();
        $jobCenterProfile = profile_job_center::where('id', $order->id_job_center)->first();
        $sanitary_license_qr = env('URL_STORAGE_FTP') . $jobCenterProfile->sanitary_license;
        $addressProfile = DB::table('address_job_centers')->where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

        if ($jobCenterProfile->sanitary_license != null) $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
        else $qrcode = null;

        return [
            'inspections' => $inspections,
            'images' => $image,
            'customerFirm' => $customerFirm,
            'date' => $date,
            'imagen' => $image,
            'addressProfile' => $addressProfile,
            'jobCenterProfile' => $jobCenterProfile,
            'qrcode' => $qrcode,
            'order' => $order
        ];
    }

    private function mapInspections($inspections, $areaId) {
        foreach ($inspections as $inspection) {
            $inspections->map(function ($inspection) use ($areaId) {
                $area = Area_tree::find($inspection->id_area_node);
                $perimeter = Area_tree::where('id_node', $area->parent)->where('id_area', $areaId)->first();
                $zone = Area_tree::where('id_node', $perimeter->parent)->where('id_area', $areaId)->first();
                $plagues = DB::table('area_inspections_plagues as aip')
                    ->join('plague_types as pt', 'aip.id_plague', 'pt.id')
                    ->join('infestation_degrees as id', 'aip.id_infestation_degree', 'id.id')
                    ->select('pt.name as plague', 'id.name as grade')
                    ->where('id_area_inspection', $inspection->id)
                    ->get();
                $photos = AreaInspectionPhotos::where('id_area_inspection', $inspection->id)->get();
                foreach ($photos as $image) {
                    $photos->map(function ($image) {
                        $image->url_s3 = CommonImage::getTemporaryUrl($image->photo, 5);
                    });
                }
                $technician = employee::find($inspection->id_technician);
                $inspection->area = $area->text;
                $inspection->zone = $zone->text;
                $inspection->perimeter = $perimeter->text;
                $inspection->plagues = $plagues;
                $inspection->technician = $technician->name;
                $inspection->photos = $photos;
            });
        }
        return $inspections;
    }
}