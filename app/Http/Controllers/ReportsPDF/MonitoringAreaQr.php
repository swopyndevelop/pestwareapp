<?php


namespace App\Http\Controllers\ReportsPDF;


use App\Area;
use App\companie;
use App\customer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class MonitoringAreaQr
{
    public static function build($id, $companyId, $partIndex)
    {
        $areaMonitoring = Area::find($id);
        $customer = customer::find($areaMonitoring->id_customer);
        $company = companie::find($companyId);

        $totalAreas = DB::table('areas_tree as at')
            ->where('at.id_area', $id)
            ->where('at.type_node', 'Área')
            ->get();

        $areas = new Collection();
        foreach ($totalAreas as $area) {
            $perimeter = DB::table('areas_tree')
                ->where('id_area', $area->id_area)
                ->where('id_node', $area->parent)
                ->first();
            $perimeterText = $perimeter->text;
            $zone = DB::table('areas_tree')
                ->where('id_area', $area->id_area)
                ->where('id_node', $perimeter->parent)
                ->first();
            $zoneText = $zone->text;
            $stationNumber = $area->text;
            $data = [
                'area' => $area->text,
                'id_area' => $area->id,
                'instance' => substr($company->name, 0, 30),
                'perimeter' => substr($perimeterText, 0, 26),
                'zone' => $zoneText,
                'customer' => substr($customer->name, 0, 28),
            ];
            $qrcode = base64_encode(QrCode::format('svg')->size(50)->errorCorrection('H')->generate($area->id));
            $data['qr'] = $qrcode;
            $areas->push($data);
        }

        $sheetCards = $areas->chunk(30);
        foreach ($sheetCards as $i => $sheetCard) {
            if ($i == $partIndex) {
                // Group by 3 elements
                $rows = new Collection();
                $stationsTmp = new Collection();
                $count = 0;
                foreach ($sheetCard as $k => $area) {
                    $count++;
                    $stationsTmp->push($area);
                    if ($count == 3) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    } elseif ($count == 2 && $k == $areas->count()-1) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    } elseif ($count == 1 && $k == $areas->count()-1) {
                        $rows->push($stationsTmp);
                        $count = 0;
                        $stationsTmp = new Collection();
                    }
                }
                return [
                    'rows' => $rows,
                    'sheetCard' => $sheetCard,
                    'folio' => $areaMonitoring->id_area,
                    'customer' => $customer,
                    'no_parts' => $areaMonitoring->no_parts,
                    'company' => $company
                ];
            }
        }

        return [
            'rows' => [],
            'customer' => $customer
        ];
    }
}