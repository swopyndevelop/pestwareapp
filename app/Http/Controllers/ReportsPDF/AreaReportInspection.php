<?php

namespace App\Http\Controllers\ReportsPDF;

use App\Area;
use App\Area_tree;
use App\AreaInspectionPhotos;
use App\Common\CommonImage;
use App\employee;
use App\profile_job_center;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AreaReportInspection
{
    public static function build($id)
    {
        $area = DB::table('areas as a')
            ->join('customers as c', 'a.id_customer', 'c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('profile_job_centers as pjc', 'c.id_profile_job_center', 'pjc.id')
            ->join('address_job_centers as adj', 'adj.profile_job_centers_id', 'pjc.id')
            ->join('companies as com', 'pjc.companie', 'com.id')
            ->where('a.id_customer', $id)
            ->select('a.*')
            ->first();
        dd($area);

        return [
            'data' => $area,
        ];
    }

}