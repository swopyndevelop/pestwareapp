<?php


namespace App\Http\Controllers\ReportsPDF;


use App\customer_branche;
use App\Http\Controllers\Business\CommonCompany;
use App\profile_job_center;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ServiceOrderTemplate
{
    public static function build($id)
    {
        $order = DB::table('events as e')->join('employees as em', 'e.id_employee', 'em.id')
            ->join('profile_job_centers as pjc', 'e.id_job_center', 'pjc.id')
            ->join('service_orders as so', 'e.id_service_order', 'so.id')
            ->join('payment_methods as pm', 'so.id_payment_method', 'pm.id')
            ->join('payment_ways as pw', 'so.id_payment_way', 'pw.id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('statuses as st', 'so.id_status', 'st.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('establishment_types as et', 'q.establishment_id', 'et.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->select('so.id as order', 'so.id_service_order', 'so.created_at as date', 'e.initial_hour', 'e.initial_date', 'em.name', 'em.file_route_firm',
                'c.name as cliente', 'c.establishment_name as empresa', 'c.cellphone', 'cd.address', 'c.municipality', 'pl.show_price',
                'q.id_plague_jer', 'q.total', 'pt.plague_key', 'u.name as agente', 'st.name as status', 'cd.address_number', 'cd.state', 'cd.email', 'e.id_status',
                'e.id as event', 'c.colony', 'q.id as quotation', 'e.final_hour', 'e.final_date', 'so.address as a_rfc', 'so.email as e_rfc', 'so.bussiness_name',
                'so.observations', 'cd.billing', 'd.id as discount', 'd.percentage', 'ex.id as extra', 'ex.amount', 'q.construction_measure', 'q.garden_measure', 'q.price',
                'q.establishment_id as e_id', 'pm.id as pm_id', 'pw.id as pw_id', 'pm.name as metodo', 'pw.name as tipo', 'cd.email', 'q.companie as compania',
                'so.id_job_center', 'e.start_event', 'e.final_event', 'et.name as establecimiento', 'so.customer_branch_id', 'so.total as total_order',
                'c.days_expiration_certificate as days_expiration_certificate_customer', 'pl.is_disinfection','c.show_price as show_price_customer')
            ->where('so.id', $id)->first();

        $imagen = DB::table('companies')
            ->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook', 'warnings_service', 'contract_service',
                'pdf_sanitary_license', 'rfc', 'bussines_name', 'health_manager', 'email')
            ->where('id', $order->compania)->first();
        $jobCenterProfile = profile_job_center::where('id', $order->id_job_center)->first();
        $sanitary_license_qr = env('URL_STORAGE_FTP') . $jobCenterProfile->sanitary_license;
        $addressProfile = DB::table('address_job_centers')->where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

        if ($jobCenterProfile->sanitary_license != null) $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
        else $qrcode = null;
        $symbol_country = CommonCompany::getSymbolByCountryWithCompanyId($order->compania);

        $f = DB::table('service_firms as sf')
            ->select('sf.file_route', 'sf.other_name')
            ->where('sf.id_service_order', $id)->first();

        //Ajuste de datos si es cotización personalizada
        if ($order->customer_branch_id != null) {
            $customerBranch = customer_branche::find($order->customer_branch_id);
            $order->total = $order->total_order;
            $order->empresa = $customerBranch->name;
            $order->address = $customerBranch->address;
            $order->address_number = $customerBranch->address_number;
            $order->colony = $customerBranch->colony;
            $order->municipality = $customerBranch->municipality;
            $order->state = $customerBranch->state;
        } else $order->total = $order->total_order;

        return [
            'order' => $order,
            'addressProfile' => $addressProfile,
            'jobCenterProfile' => $jobCenterProfile,
            'f' => $f,
            'imagen' => $imagen,
            'qrcode' => $qrcode,
            'symbol_country' => $symbol_country,
        ];
    }
}