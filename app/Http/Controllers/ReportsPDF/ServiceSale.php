<?php


namespace App\Http\Controllers\ReportsPDF;

use App\address_job_center;
use App\DiscountSale;
use App\Http\Controllers\Business\CommonCompany;
use App\Sale;
use Illuminate\Support\Facades\DB;

class ServiceSale
{
    public static function build($id)
    {
        $sale = Sale::join('customers as c','sales.id_customer', 'c.id')
            ->join('users as u','sales.id_user','u.id')
            ->join('payment_ways as pw','sales.id_payment_way','pw.id')
            ->join('payment_methods as pm', 'sales.id_payment_method', 'pm.id')
            ->join('vouchers as v','sales.id_voucher','v.id')
            ->join('profile_job_centers as pjc','sales.profile_job_center_id','pjc.id')
            ->join('companies as com','sales.id_company','com.id')
            ->join('customer_datas as cd','c.id','cd.customer_id')
            ->join('employees as e','sales.id_storehouse','e.id')
            ->select('sales.id as saleId', 'sales.id_sale as folio', 'sales.date', 'sales.subtotal',
                'sales.total','sales.created_at', 'sales.description as description', 'u.name as user_name','c.name as customer_name',
                'pw.name as payment_way_name', 'pm.name as payment_method_name', 'v.name as voucher_name',
                'pjc.name as profile_job_center_name', 'c.name as companie_name', 'pjc.id as pjc_id',
                'pjc.profile_job_centers_id as profile_job_center_id', 'com.pdf_logo','pjc.business_name',
                'pjc.whatsapp_personal','pjc.rfc','pjc.email_personal','pjc.facebook_personal','c.establishment_name', 'c.colony',
                'c.municipality','c.cellphone','cd.email','cd.rfc','com.pdf_sello','e.name as type_sale','com.id as companyId')
            ->where('sales.id', $id)
            ->first();

        $addressProfile = address_job_center::where('profile_job_centers_id', $sale->profile_job_center_id)->first();
        $symbol_country = CommonCompany::getSymbolByCountryPublic($sale->companyId);

        $products_sale = DB::table('products_sale as ps')
            ->join('products as p', 'ps.id_product', 'p.id')
            ->where('ps.id_sale', $sale->saleId)
            ->select('ps.*', 'p.name as product_name')
            ->get();

        $discounts_sale = DiscountSale::join('discounts as d','discounts_sale.id_discount','d.id')
            ->where('id_sale', $id)
            ->select('d.percentage')
            ->first();
        $totalDiscounts = ($discounts_sale->percentage/100) * $sale->subtotal;

        return [
            'sale' => $sale,
            'addressProfile' => $addressProfile,
            'symbol_country' => $symbol_country,
            'products_sale' => $products_sale,
            'totalDiscounts' => $totalDiscounts,
            'discounts_sale' => $discounts_sale
        ];
    }
}