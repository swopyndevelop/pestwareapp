<?php


namespace App\Http\Controllers\ReportsPDF;


use App\address_job_center;
use App\companie;
use App\Country;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Quotations\CommonQuotation;
use App\Http\Controllers\Users\CommonUser;
use App\profile_job_center;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QuotationPdf
{
    public static function build($quotationId,$quotation){
        $user = CommonUser::getUserById($quotation->user_id);
        $discount = CommonQuotation::getDiscountByQuot($quotationId);
        $imagen = CommonCompany::getCompanyById($quotation->companie);
        $jobCenterProfile = profile_job_center::where('id', $quotation->id_job_center)->first();
        $addressProfileJobCenter = address_job_center::where('address_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();
        $d = 0;

        if ($discount != null) {
            $d1 = $discount->percentage/100;
            $d = $quotation->price*$d1;
            $pre = $quotation->price - $d;
            $subtotal = $quotation->total;
        }
        else $subtotal = $quotation->total;

        if ($quotation->garden_measure == null) $area = $quotation->construction_measure;
        else $area = $quotation->construction_measure + $quotation->garden_measure;

        //Obtener el extra
        $extra = DB::table('extras')->select('id','description','amount')->where('id',$quotation->id_extra)->first();
        if ($extra->amount == 0) $e = 0;
        else $e = $extra->amount;

        $plague = DB::table('plague_type_quotation as qpt')->join('plague_types as pt','pt.id','qpt.plague_type_id')
            ->select('pt.name','pt.id as plague')->where('qpt.quotation_id', $quotationId)->get();

        $plague2 = collect($plague)->toArray();
        $pricesList = DB::table('price_lists')
            ->where('id', $quotation->id_price_list)
            ->first();

        $indications = DB::table('indications')
            ->where('id',$pricesList->indications_id)
            ->first();

        $ruta = storage_path().'/app/'.$pricesList->pdf;
        $countryCode = companie::where('id_company',$imagen->id_company)->first();
        $symbol_country_code = Country::where('id', $countryCode->id_code_country)->first();
        $symbol_country = $symbol_country_code->symbol_country;
        $qrcode = null;
        $sanitary_license_qr = env('URL_STORAGE_FTP') . $jobCenterProfile->sanitary_license;
        if ($jobCenterProfile->sanitary_license != null) {
            $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
        }

        $concepts = DB::table('custom_quotes as cq')
            ->join('type_services as ts', 'cq.id_type_service', 'ts.id')
            ->select('cq.*', 'ts.name as type_service')
            ->where('id_quotation',$quotationId)
            ->get();

        $isCustomService = false;
        foreach ($concepts as $concept) {
            if ($concept->id_type_service == 8) $isCustomService = true;
            $concepts->map(function ($concept) {
                $concept->subtotal_calc = $concept->quantity * $concept->frecuency_month * $concept->unit_price;
                if ($concept->id_type_service == 6) {
                    $concept->subtotal_calc = $concept->quantity * $concept->unit_price;
                }
                $concept->total = $concept->subtotal;
            });
        }

        $quotation->customTax = $quotation->price * 0.07;
        $quotation->customTotal = $quotation->customTax + $quotation->price;

        return[
            'imagen' => $imagen,
            'jobCenterProfile' => $jobCenterProfile,
            'quotation' => $quotation,
            'user' => $user,
            'plague2' => $plague2,
            'plagues' => $plague,
            'area' => $area,
            'indications' => $indications,
            'extra' => $extra,
            'e' => $e,
            'd' => $d,
            'subtotal' => $subtotal,
            'pricesList' => $pricesList,
            'pre' => $pre,
            'symbol_country' => $symbol_country,
            'ruta' => $ruta,
            'addressProfileJobCenter' => $addressProfileJobCenter,
            'c' => $concepts,
            'qrcode' => $qrcode,
            'isCustomService' => $isCustomService
        ];
    }

}