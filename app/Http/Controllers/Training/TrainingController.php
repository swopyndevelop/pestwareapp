<?php

namespace App\Http\Controllers\Training;

use App\Http\Controllers\Security\SecurityController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\training;
use App\training_plan;
use App\tracking;
use App\jobTitle_profile;
use App\treeJobProfile;
use App\CourseProgress;
use App\User;
use App\assignment_plan_user;
use App\assignment_plan;
use App\module;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;
use Validator;
use Image;
use Carbon\Carbon;
use App\CutBox;
use ZipArchive;

class TrainingController extends Controller
{

	protected $rules =
	[
		'Titulo'      => 'Required',
		'Duración'    => 'Required',
		'Descripcion' =>'Required',
		'Inicio'      => 'Required',
		'Fin'         => 'Required',
		'Curso'       => 'Required'
	];

	public function index(){
		$training = training::simplePaginate(10);
		return view('adminlte::training.index')->with(['trainings' => $training]);

	}

	public function create(){
		// Crear enlace simbolico para hosting //NOTA: SOLO EJECUTAR UNA VEZ, DESPUÉS COMENTAR O ELIMINAR.
	    //symlink('/home/swopyn5/test.pestwareapp.com/storage/app/public/iSpring', '/home/swopyn5/public_html/test.pestwareapp.com/public/iSpring');
        //symlink('/home/swopyn5/pestwareapp.com/storage/app/public/iSpring', '/home/swopyn5/public_html/test.pestwareapp.com/public/iSpring');
		return view('adminlte::training.create');
	}



	public function add(Request $request){

		$training = New training;
		$training->title = $request->Titulo;
		$training->duration = $request->Duracion;
		$training->description = $request->Descripcion;
		$training->start = $request->Inicio;
		$training->end = $request->Fin;
		$training->course = 'iSpring/' . $request->Titulo . '/res/index.html';
		Storage::putFileAs('public/iSpring/',$request->file('Curso'), $request->Titulo . '.zip');
		$training->save();

        $zip = new ZipArchive();
        $zip->open(storage_path('app/public/iSpring/' . $request->Titulo . '.zip'), ZipArchive::CREATE);
        $zip->extractTo(storage_path('app/public/iSpring/' . $request->Titulo . '/'));
        $zip->close();

		return redirect()->route('training_index');

	}


	public function edit(training $training){
		return view('adminlte::training.edit')->with(['training' => $training]);

	}

	public function update(training $training, Request $request){
		$training->title = $request->Titulo;
		$training->duration = $request->Duracion;
		$training->description = $request->Descripcion;
		$training->start = $request->Inicio;
		$training->end = $request->Fin;

		if (!empty($request->Curso)) {
			$training->course = 'iSpring/' . $request->Titulo . '/res/index.html';
			Storage::putFileAs('public/iSpring/',$request->file('Curso'), $request->Titulo . '.zip');
			Storage::putFile('public',$request->file('Curso'));
			$zip = new ZipArchive();
        	$zip->open(storage_path('app/public/iSpring/' . $request->Titulo . '.zip'), ZipArchive::CREATE);
        	$zip->extractTo(storage_path('app/public/iSpring/' . $request->Titulo . '/'));
        	$zip->close();
		}

		$training->save();

		return redirect()->route('training_index');

	}


	public function play(training $training){
    	//$storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

    	//$training =($training->course);

    	//$training = $storagePath.$training->course;
		return view('adminlte::training.course')->with(['training' => $training]);
	}

	public function study(){

		$usr = DB::table('employees')->where('employee_id', auth()->user()->id)->first();

		$trainingsUser = DB::table('training_plans as tp')
			->join('assignment_plan_training_plan as atp', 'tp.id', 'atp.training_plan_id')
			->join('assignment_plan_user as apu', 'atp.assignment_plan_id', 'apu.assignment_plan_id')
			->join('module_training_plan as mtp', 'tp.id', 'mtp.training_plan_id')
			->join('modules as m', 'mtp.module_id', 'm.id')
            ->join('module_training as mt', 'mtp.module_id', 'mt.module_id')
            ->join('trainings as t', 'mt.training_id', 't.id')
			->where('user_id', auth()->user()->id)
            ->orderby('t.created_at','ASC')
			->get();

		//dd($trainingsUser);

		$training = $trainingsUser;
		//dd($training);

		//dd($training);

		$array = array();
		foreach ($training as $t) {
			$isConteined = false;
			foreach ($array as $item) {
				if($t->training_plan_id == $item)
					$isConteined = true;
			}
			if(!$isConteined)
				array_push($array,$t->training_plan_id);
		}
		//dd($array);

		$countPlans = DB::table('training_plans as tp')
			->whereIn('id', $array)
			->orderby('id','ASC')
			->simplePaginate(10);

		//dd($countPlans);

		$countModules = DB::table('modules as m')
			->join('module_training_plan as mtp', 'm.id', 'mtp.module_id')
			->whereIn('mtp.training_plan_id', $array)
			->orderby('m.name','ASC')
			->simplePaginate(10);

		//dd($countModules);

		$progress = DB::table('course_progresses')->where('user_id', auth()->user()->id)->get();

		return view('adminlte::training.study')->with(['trainings' => $training, 'progress' => $progress, 'usuario' => $usr, 'plans' => $countPlans, 'modules' => $countModules]);
	}

	public function personalStudy($id){
        $trainingsUser = DB::table('training_plans as tp')
            ->join('assignment_plan_training_plan as atp', 'tp.id', 'atp.training_plan_id')
            ->join('assignment_plan_user as apu', 'atp.assignment_plan_id', 'apu.assignment_plan_id')
            ->join('module_training_plan as mtp', 'tp.id', 'mtp.training_plan_id')
            ->join('modules as m', 'mtp.module_id', 'm.id')
            ->join('module_training as mt', 'mtp.module_id', 'mt.module_id')
            ->join('trainings as t', 'mt.training_id', 't.id')
            ->orderby('t.created_at','ASC')
            ->where('tp.id', $id)
            ->get();

        $training = $trainingsUser;

        $array = array();
        foreach ($training as $t) {
            $isConteined = false;
            foreach ($array as $item) {
                if($t->training_plan_id == $item)
                    $isConteined = true;
            }
            if(!$isConteined)
                array_push($array,$t->training_plan_id);
        }

        $countPlans = DB::table('training_plans as tp')
            ->whereIn('id', $array)
            ->orderby('id','ASC')
            ->simplePaginate(10);

        $countModules = DB::table('modules as m')
            ->join('module_training_plan as mtp', 'm.id', 'mtp.module_id')
            ->whereIn('mtp.training_plan_id', $array)
            ->orderby('m.name','ASC')
            ->simplePaginate(10);

        $progress = DB::table('course_progresses')->where('user_id', auth()->user()->id)->get();

        return view('adminlte::training.personal_study')->with(['trainings' => $training, 'progress' => $progress, 'plans' => $countPlans, 'modules' => $countModules]);
    }

    public function moduleStudy($id){
        $trainingsUser = DB::table('modules as m')
            ->join('module_training as mt', 'm.id', 'mt.module_id')
            ->join('trainings as t', 'mt.training_id', 't.id')
            ->orderby('t.created_at','ASC')
            ->where('m.id', $id)
            ->get();
        //dd($trainingsUser);

        $training = $trainingsUser;

        $array = array();
        foreach ($training as $t) {
            $isConteined = false;
            foreach ($array as $item) {
                if($t->module_id == $item)
                    $isConteined = true;
            }
            if(!$isConteined)
                array_push($array,$t->module_id);
        }

        $countModules = DB::table('modules as m')
            ->whereIn('id', $array)
            ->orderby('id','ASC')
            ->simplePaginate(10);
        //dd($countModules);

        $progress = DB::table('course_progresses')->where('user_id', auth()->user()->id)->get();

        return view('adminlte::training.module_study')->with(['trainings' => $training, 'progress' => $progress, 'modules' => $countModules]);
    }

	public function progress(Request $request){
		$progress = DB::table('course_progresses')->where('user_id', $request->user)->where('training_id',$request->training_id)->first();

		$trackingTemp = DB::table('employees')->where('employee_id',$request->user)->first();
		$tracking = tracking::where('user_id', $trackingTemp->employee_id)->first();

		if (count($progress) <= 0){
			$training = New CourseProgress;
			$training->user_id = $request->user;
			$training->training_id = $request->training_id;
			$training->progress = $request->slider;
			$training->save();

			$tracking->lms = $request->slider;
			$tracking->save();

		}else{
			$id = $progress->id;
			$training = CourseProgress::find($id);


		}
		if ($training->progress < $request->slider){
			$training->progress= $request->slider;
			$training->save();

			$tracking->lms = $request->slider;
			$tracking->save();
		}
	}

	public function kardex($user){
    	//encuentra todos los planes de entrenamiento donde esta el usuario
		$usr = DB::table('employees')->where('employee_id', $user)->first();
		$training = DB::table('job_title_profile_training_plan as jtp')
		->join('training_training_plan as ttp','ttp.training_plan_id','jtp.training_plan_id')
		->join('trainings','ttp.training_id','trainings.id')->join('course_progresses as cp','trainings.id','cp.training_id')->where('job_title_profile_id',$usr->job_title_profile_id)->where('cp.user_id', $user)->select('trainings.*','jtp.training_plan_id as TrainingPlan','cp.progress')
		->simplePaginate(10);

		//$kardex = CourseProgress::where('user_id',$request->training)->get();
		return view('adminlte::training.kardex')->with(['kardexs' => $training, 'usuario' => $user]);
	}

	public function kardexUser($user)
	{
		$usr = DB::table('employees')->where('employee_id', $user)->first();
		$trainingJobs = DB::table('job_title_profile_training_plan as jtp')
		    ->join('training_training_plan as ttp','ttp.training_plan_id','jtp.training_plan_id')
		    ->join('trainings','ttp.training_id','trainings.id')
            ->where('job_title_profile_id',$usr->job_title_profile_id)
            ->select('trainings.*','jtp.training_plan_id as TrainingPlan')
            ->get();

		$trainingsUser = DB::table('training_plans as tp')
            ->join('training_plan_user as tpu','tp.id','tpu.training_plan_id')
            ->join('training_training_plan as ttp','tp.id','ttp.training_plan_id')
            ->join('trainings','ttp.training_id','trainings.id')
            ->where('user_id',$user)
            ->select('trainings.*','ttp.training_plan_id as TrainingPlan')
            ->get();

		$training = $trainingJobs->merge($trainingsUser)->unique();

		$array = array();
		foreach ($training as $t) {
			$isConteined = false;
			foreach ($array as $item) {
				if($t->TrainingPlan == $item)
					$isConteined = true;
			}
			if(!$isConteined)
				array_push($array,$t->TrainingPlan);
		}

		$countCourses = DB::table('training_plans as tp')->whereIn('id', $array)->simplePaginate(10);
		$progress = DB::table('course_progresses')->where('user_id', $user)->get();
		return view('adminlte::training.kardex')->with(['trainings' => $training, 'progress' => $progress, 'usuario' => $usr, 'plans' => $countCourses]);
	}

	public function indexassign(){
		$trainingplan = training_plan::simplePaginate(10);
		return view('adminlte::training.index_assign')->with(['trainingplans' => $trainingplan]);

	}

	public function createassign(){
		$training = training::all();
		$user = User::all();
		$module = module::all();
		$job_title = treeJobProfile:: all();
		return view('adminlte::training.createassign')->with(['trainings' => $training,'users' => $user, 'job_titles' => $job_title, 'modules' => $module ]);

	}

	public function storeassign(Request $request){
		$trainingplan = new training_plan;
		$trainingplan->name = $request->Nombre;
		$trainingplan->description = $request->Descripcion;
		$trainingplan->save();

		//$trainingplan->user()->sync($request->users);
		//$trainingplan->training()->sync($request->course);
		$trainingplan->modules()->sync($request->module);
		//$trainingplan->job_title()->sync($request->jobtitles);
		// $training = Training::all();
		// $user = User::all();
		// $jobtitle = JobTitleProfile::all();
		$trainingplan = training_plan::simplePaginate(10);
		return view('adminlte::training.index_assign')->with(['trainingplans' => $trainingplan]);

	}

	public function createassignplan(){
		$user = DB::table('users')->where('companie',auth()->user()->companie)->get();
		$plan = training_plan::all();
		return view('adminlte::training.createassignplan')->with(['plans' => $plan,'users' => $user]);

	}

	public function storeassignplan(Request $request){
		$assignmentplan = new assignment_plan;
		$assignmentplan->save();

		$assignmentplan->training_plans()->sync($request->plans);
        $assignmentplan->users()->sync($request->users);

		return redirect()->route('training_create_assign_plan');
	}

	public function createModule(){
        $training = training::all();
        $module = DB::table('modules as m')->orderby('m.name','ASC')->paginate(10);
		return view('adminlte::training.createmodule')->with(['trainings' => $training, 'modules' => $module]);
	}

	public function addModule(Request $request){
		$module = new module;
		$module->name = $request->moduleName;
		$module->save();
        $module->trainings()->sync($request->course);

		return redirect()->route('training_create_assign');
	}

    public function deleteModule(module $module)
    {
        $module->delete();

        return redirect()->route('new_module');
    }

    public function editModule(module $module){
        $module = module::find($module->id);
        $training = training::all();
        $modulesTraining = $module->trainings;

        return view('adminlte::training.editmodule')->with(['modulesTraining' => $modulesTraining, 'trainings' => $training, 'module' => $module]);
    }

    public function moduleUpdate($trainingModule, Request $request){
        $trainingModule = module::find($trainingModule);

        $trainingModule->name = $request->moduleName;

        $trainingModule->save();

        $trainingModule->trainings()->sync($request->course);

        session()->flash('message_update','Modulo Actualizado');

        return redirect()->route('new_module');
    }

	public function editassign(training_plan $trainingplan){
		$trainingplan = training_plan::find($trainingplan->id);
        $module = module::all();
        $modulesplan = $trainingplan->modules;

		return view('adminlte::training.editassign')->with(['trainingplan' => $trainingplan, 'modulesplan' => $modulesplan, 'modules' => $module]);
	}


	public function updateassign(training_plan $trainingplan, Request $request){
		$trainingplan = training_plan::find($trainingplan->id);
		$trainingplan->name = $request->Nombre;
		$trainingplan->description = $request->Descripcion;

		$trainingplan->save();
		$trainingplan->modules()->sync($request->module);

		session()->flash('message_update','Plan Actualizado');

		$trainingplan = training_plan::simplePaginate(10);
		return view('adminlte::training.index_assign')->with(['trainingplans' => $trainingplan]);
	}

	public function delete (training $training)
	{
		$training->delete();

		return redirect()->route('training_index');
	}

	/**
	Comentarios
	**/
	public function deleteassign(training_plan $trainingplan){
		$trainingplan->delete();
		$trainingplan = training_plan::simplePaginate(10);
		return view('adminlte::training.index_assign')->with(['trainingplans' => $trainingplan]);
	}
}




