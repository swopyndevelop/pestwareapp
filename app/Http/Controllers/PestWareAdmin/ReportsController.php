<?php

namespace App\Http\Controllers\PestWareAdmin;

use App\customer_data;
use App\Http\Controllers\Security\SecurityController;
use App\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{

    public function getReportAllCompanies($token) {
        if ($token === env('DB_PASSWORD')) {
            $data = CommonPestWareAdmin::getStatusOfTheCompanies();
            $this->exportDataExcel($data, 'Reporte Instancias PWA', 'Instancias al día de Hoy');
        } else SecurityController::abort();
    }

    private function exportDataExcel($data, $fileName, $sheetName)
    {
        try {
            Excel::create($fileName, function($excel) use ($data, $sheetName) {
                $excel->sheet($sheetName, function($sheet) use ($data) {
                    $sheet->fromArray($data);
                });
            })->export('xls');
        }catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Function for import data with csv.
     * @return JsonResponse
     */
    public function importDataTest()
    {
        $pathComplete = 'storage/app/customer_datas.csv';
        try {
            Excel::load($pathComplete, function ($reader) {
                // Getting all results
                $rows = $reader->get();
                foreach ($rows as $row) {
                    $customer_data = new customer_data();
                    $customer_data->customer_id = $row->customer_id;
                    $customer_data->address = $row->address;
                    $customer_data->address_number = $row->address_number;
                    $customer_data->state = $row->state;
                    $customer_data->reference_address = $row->reference_address;
                    $customer_data->email = $row->email;
                    $customer_data->phone_number = $row->phone_number;
                    $customer_data->pay_type_id = $row->pay_type_id;
                    $customer_data->billing = $row->billing;
                    $customer_data->rfc = $row->rfc;
                    $customer_data->tax_residence = $row->tax_residence;
                    $customer_data->save();
                }
            });
            return \Response::json('ok');
        } catch (\Exception $exception) {
            return \Response::json($exception);
        }
    }
}
