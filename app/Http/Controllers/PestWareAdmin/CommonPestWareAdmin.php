<?php


namespace App\Http\Controllers\PestWareAdmin;


use App\companie;
use DB;

class CommonPestWareAdmin
{
    public static function getStatusOfTheCompanies()
    {
        return self::getAllCompanies()->get();
    }

    public static function getAllCompanies() {
        return companie::join('contacts as con', 'companies.contact_id', 'con.id')
            ->join('countries as country', 'companies.id_code_country', 'country.id')
            ->select('companies.id as #-Instancia', 'companies.name as Empresa', 'con.name as contacto',
                'con.phone as telefono contacto', 'con.email as correo contacto', 'companies.created_at as fecha alta',
                'companies.rfc as RFC', 'companies.facebook', 'companies.licence as licencia sanitaria',
                'companies.health_manager as responsable sanitario', 'companies.no_employees as # usuarios',
                'companies.no_branch_office as # sucursales', 'country.name as pais');
    }
}