<?php

namespace App\Http\Controllers;

use App\companie;
use App\Http\Controllers\Security\SecurityController;
use Illuminate\Http\Request;
use App\personal_mail;
use Illuminate\Support\Facades\Auth;


class PersonalmailController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function index()
    {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        return view('vendor.adminlte.register.MAILS.create');
    }

    public function save(Request $request)
    {
        $idJobCenterRoute = $request->get('jobCenterRoute');
        try {
            $mail = personal_mail::where('profile_job_center_id', $request->get('jobCenterMail'))->first();
            if (!$mail) $mail = new personal_mail;
            if ($request->file('banner') != null){
                $filename = $request->file('banner')->store(
                    'logos', 'ftp'
                );
            }
            else $filename = $mail->banner;
            $mail->banner = $filename;
            $mail->profile_job_center_id = $request->get('jobCenterMail');
            $mail->id_company = \Auth::user()->companie;
            $mail->description = $request->get('description');
            $mail->image_whatsapp = $request->get('whatsapp');
            $mail->image_messenger = $request->get('messenger');
            $mail->reminder_whatsapp = $request->get('reminderWhatsapp');
            $mail->email_tracing = $request->get('emailTracing');
            $mail->save();

            $idJobCenterRoute = SecurityController::encodeId($idJobCenterRoute);
            return redirect()->route('data_job_center', $idJobCenterRoute)->with('success', 'Datos Actualizados.');
        }
        catch (\Exception $exception)
        {
            return redirect()->route('data_job_center', $idJobCenterRoute)->with('error', 'Algo salió mal, Intentalo de nuevo');
        }
    }
}
