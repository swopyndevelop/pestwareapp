<?php

namespace App\Http\Controllers\PriceLists;

use App\EmailMassive;
use App\PlagueList;
use App\Price;
use App\PriceList;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    private $message = "Datos importados correctamente.";

    /**
     * Export data table to file excel.
     *
     * @param $id
     */
    public function export($id) {
        $priceList = PriceList::find($id);
        $key = $priceList->key;
        try {
            Excel::create($priceList->name, function($excel) use ($id, $key) {

                $excel->sheet($key, function($sheet) use ($id) {

                    $pricesList = Price::where('price_list_id', $id)
                        ->select('area', 'quantity', 'price_one', 'price_two')
                        ->get();

                    $sheet->fromArray($pricesList);

                });
            })->export('xls');
        }catch (\Exception $exception) {
            redirect()->route('list_prices');
        }

    }

    public function exportFormat() {
        try {
            $data = array(
                array('area' =>50, 'quantity' => 1, 'price_one' => 150, 'price_two' => 200)
            );
            Excel::create('Formato lista de precios', function($excel) use ($data) {

                $excel->sheet('Mi lista de precios', function($sheet) use ($data) {

                    $sheet->fromArray($data);

                });
            })->export('xls');
        }catch (\Exception $exception) {
            redirect()->route('list_prices');
        }
    }

    public function importCreate(Request $request) {
        // Get requests
        $document = $request->file('fileImport');
        $plagues = $request->plagues;

        // Create new Price list
        $priceList = new PriceList;
        $priceList->name = $request->name;
        $priceList->key = $request->key;
        $priceList->hierarchy = $request->hierarchy;
        $priceList->description = $request->description;
        $priceList->establishment_id = $request->establishment;
        $priceList->indications_id = $request->indications;
        $priceList->company_id = Auth::user()->companie;
        $priceList->user_id = Auth::user()->id;
        $priceList->status = 1;
        $save = $priceList->save();

        if ($save) {
            $id = $priceList->id;
            // Saved array plagues of price list.
            foreach ((array) $plagues as $plague) {
                $plagueList = new PlagueList;
                $plagueList->price_list_id = $id;
                $plagueList->plague_id = $plague;
                $plagueList->save();
            }

            // Upload file document
            $path = $this->uploadFileInStorage($document, $id);
            $this->import($path, $id);

            return redirect()->route('edit_prices', $id)->with('message', $this->message);
        }else return redirect()->route('list_prices');

    }

    public function importUpdate(Request $request) {
        // Get requests
        $id = $request->get('idPriceList');
        $document = $request->file('fileImport');

        // Upload file document
        $path = $this->uploadFileInStorage($document, $id);
        $this->import($path, $id);

        return redirect()->route('edit_prices', $id)->with('message', $this->message);
    }

    public function importEmailMassive(Request $request) {
        // Get requests
        $fileXlsImport = $request->file('fileImport');
        // Upload file document
        $path = $this->fileStorageEmail($fileXlsImport);
        $this->importEmail($path);

        return redirect()->route('edit_config_instance')->with('message', $this->message);
    }

    public function exportEmailMassive(Request $request) {
        try {
            $sendEmail = EmailMassive::all();
            $date = Carbon::now()->toDateString();
            $fileName = 'Correos-' . $date;
            $sheetName = 'correos';
            Excel::create($fileName, function($excel) use ($sendEmail, $sheetName) {
                $excel->sheet($sheetName, function($sheet) use ($sendEmail) {
                    $sheet->fromArray($sendEmail);
                });
            })->export('xls');

        }
        catch (\Exception $exception) {
            return redirect()->route('edit_config_instance')->with('warning', 'Algo salió mal intente de nuevo.');
        }
        return redirect()->route('edit_config_instance')->with('message', $this->message);
    }

    public function import($path, $id) {

        if ($path != null) {
            $pathComplete = 'storage/app/' . $path;
            try {
                Excel::load($pathComplete, function($reader) use ($id) {

                    // Getting all results
                    $rows = $reader->get();

                    // Validate format document
                    $isFormat = $this->validateFormat($rows);
                    if ($isFormat) {
                        Price::where('price_list_id', $id)->delete();
                        foreach ($rows as $row) {
                            if ($row->area != null) {
                                $price = new Price;
                                $price->price_list_id = $id;
                                $price->area = $row->area;
                                $price->quantity = $row->quantity;
                                $price->price_one = $row->price_one;
                                $price->price_two = $row->price_two;
                                $price->save();
                            }
                            // Enviar datos del nuevo form lista d eprecios por post y este boton de impoortar serviciria como guardar
                            // agregar validaciones para  tener los datos esenciales requeridos
                            // asi crear registro en lista de precios y precios
                            // finalmente redireccionar a edit para visualizar los datos cargados
                        }
                    }else{
                        $this->message = "Formato incorrecto, asegurese de subir el archivo con el formato de exportar a Excel.";
                    }

                });
            }catch (\Exception $exception) {
                $this->message = "Error al importar los datos.";
            }
        }else {
            $this->message = "No ha sido posible cargar el archivo.";
        }

    }

    public function importEmail($path) {

        if ($path != null) {
            $pathComplete = 'storage/app/' . $path;
            try {
                Excel::load($pathComplete, function($reader) {

                    // Getting all results
                    $rows = $reader->get();

                    // Validate format document
                    $isFormat = $this->validateFormatEmail($rows);
                    if ($isFormat) {
                        foreach ($rows as $row) {
                            if ($row->name != null) {
                                $emailMassive = new EmailMassive();
                                $emailMassive->name = $row->name;
                                $emailMassive->url_web = $row->url_web;
                                $emailMassive->url_facebook = $row->url_facebook;
                                $emailMassive->number_whatsapp = $row->number_whatsapp;
                                $emailMassive->number_phone = $row->number_phone;
                                $emailMassive->email = $row->email;
                                $emailMassive->contact = $row->contact;
                                $emailMassive->address = $row->address;
                                $emailMassive->colony = $row->colony;
                                $emailMassive->municipality = $row->municipality;
                                $emailMassive->state = $row->state;
                                $emailMassive->zip_code = $row->zip_code;
                                $emailMassive->license = $row->address;
                                $emailMassive->date = $row->date;
                                $emailMassive->health_officer = $row->health_officer;
                                $emailMassive->rfc = $row->rfc;
                                $emailMassive->observations = $row->observations;
                                $emailMassive->save();
                            }
                        }
                    }else{
                        $this->message = "Formato incorrecto, asegurese de subir el archivo con el formato de exportar a Excel.";
                    }

                });
            }catch (\Exception $exception) {
                dd($exception->getLine());
                $this->message = "Error al importar los datos.";
            }
        }else {
            $this->message = "No ha sido posible cargar el archivo.";
        }

    }

    private function uploadFileInStorage($file, $idPriceList) {
        return Storage::putFileAs(
            'public/prices_lists', $file, $idPriceList . '.xls'
        );
    }

    private function fileStorageEmail($file) {
        return Storage::putFileAs('public/mails', $file, 'Email_Masivo' . '.xls');
    }

    private function validateFormat($columns) {
        if (count($columns) > 0)
            if ($columns[0]->area && $columns[0]->quantity && $columns[0]->price_one)
                return true;
        return false;
    }

    private function validateFormatEmail($columns) {
        if (count($columns) > 0)
            if ($columns[0]->name && $columns[0]->url_web && $columns[0]->url_facebook && $columns[0]->number_whatsapp
                && $columns[0]->number_phone && $columns[0]->email && $columns[0]->contact && $columns[0]->address
                && $columns[0]->colony && $columns[0]->municipality && $columns[0]->state && $columns[0]->zip_code
                && $columns[0]->license && $columns[0]->date && $columns[0]->health_officer && $columns[0]->rfc
                && $columns[0]->observations)
                return true;
        return false;
    }
}
