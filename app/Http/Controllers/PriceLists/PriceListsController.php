<?php

namespace App\Http\Controllers\PriceLists;

use App\companie;
use App\employee;
use App\establishment_type;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\Indication;
use App\JobCenterSession;
use App\Library\FacturamaLibrary;
use App\plague_type;
use App\PriceList;
use App\PlagueList;
use App\Price;
use App\profile_job_center;
use App\treeJobCenter;
use App\UnitSat;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Storage;


class PriceListsController extends Controller
{
    public function create($jobcenter = 0, Request $request){
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $idJobCenter = 0;
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        $search = $request->get('search');
        $priceLists = DB::table('price_lists as pl')
            ->join('indications as in', 'pl.indications_id', 'in.id')
            ->join('users as u', 'pl.user_id', 'u.id')
            ->join('establishment_types as et', 'pl.establishment_id', 'et.id')
            ->select('pl.*', 'in.name as indications', 'in.description as indications_description', 'et.name as establishment', 'u.name as username', 'pl.customer_portal')
            ->where('pl.profile_job_center_id', $profileJobCenter)
            ->where('pl.name', 'like', '%' . $search . '%')
            ->orderBy('name','asc')
            ->paginate(10);

        $prices = DB::table('prices')->get();

        foreach ($priceLists as $pl) {
            $priceLists->map(function($pl){
                $plagues = DB::table('plagues_list as pl')
                    ->where('price_list_id', $pl->id)
                    ->join('plague_types as pt', 'pl.plague_id', 'pt.id')
                    ->select('pt.name as plague')
                    ->get();
                $plaguesString = "";
                foreach ($plagues as $plague) {
        			if($plaguesString == "") $plaguesString = $plague->plague;
                	else $plaguesString = $plaguesString . ", " . $plague->plague;
                }
                $pl->plagues = $plaguesString;
            });
        }

        $establishments = establishment_type::where('profile_job_center_id', $profileJobCenter)->orderBy('name','asc')->get();
        $plagues = plague_type::where('profile_job_center_id', $profileJobCenter)->orderBy('name','asc')->get();
        $indications = Indication::where('profile_job_center_id', $profileJobCenter)->orderBy('name','asc')->get();
        $symbol_country = CommonCompany::getSymbolByCountry();
        $company = companie::find(Auth::user()->companie);

        return view('vendor.adminlte.catalogs.pricelists.index')
            ->with(['plagues' => $plagues, 'establishments' => $establishments, 'indications' => $indications, 'priceLists' => $priceLists, 'jobCenters' => $jobCenters,
                'prices' => $prices,'symbol_country' => $symbol_country, 'treeJobCenterMain' => $treeJobCenterMain, 'jobCenterSession' => $jobCenterSession, 'company' => $company
            ]);
    }

    public function store(Request $request){

        $customerPortal = 0;
        $show_price = 1;
        $isDisinfection = 0;
        $isIndividualPrice = 0;
        $individualPrice = 0;

        if ($request->get('customerPortal') == "true") $customerPortal = 1;
        if ($request->get('customerShowPrice') == "false") $show_price = 0;
        if ($request->get('isDisinfection') == "true") $isDisinfection = 1;
        if ($request->get('isIndividualPrice') == "true") {
            $isIndividualPrice = 1;
            $individualPrice = $request->get('priceValue');
        }

    	$priceList = new PriceList;
    	$priceList->name = $request->get('name');
    	$priceList->key = $request->get('key');
    	$priceList->hierarchy = $request->get('hierarchy');
    	$priceList->description = $request->get('description');
    	$priceList->establishment_id = $request->get('establishment');
    	$priceList->indications_id = $request->get('indications');
    	$priceList->profile_job_center_id = $request->get('selectJobCenterPriceList');
    	$priceList->company_id = Auth::user()->companie;
    	$priceList->user_id = Auth::user()->id;
    	$priceList->status = 1;
    	$priceList->reinforcement_days = $request->get('reinforcementDays');
        $priceList->customer_portal = $customerPortal;
        $priceList->show_price = $show_price;
        $priceList->days_expiration_certificate = $request->get('dateExpirationCertificate');
        $priceList->warranty = $request->get('warrantyQuotation');
        $priceList->is_disinfection = $isDisinfection;
        $priceList->description_billing = $request->get('descriptionBilling');
        $priceList->legend = $request->get('legend');
        $priceList->unit_code_billing = $request->get('serviceUnitSatId');
        $priceList->service_code_billing = $request->get('serviceCodeSatId');
        $priceList->individual_price = $isIndividualPrice;
        $priceList->value_individual_price = $individualPrice;
    	$priceList->save();

    	//Guardar los precios por area pertenecientes a la lista creada.
        $json = $request->input('arrayConcepts');
        $array = json_decode($json);

        foreach($array as $obj){
            $prices = new Price;
            $prices->price_list_id = $priceList->id;
            $prices->area = $obj->area;
            $prices->quantity = $obj->quantity;
            $prices->price_one = $obj->price_one;
            $prices->price_two = $obj->price_two;
            $prices->save();
        }

        //Guardar las plagas pertenecientes a la lista creada.
        $plagues = $request->plagues;
        foreach ($plagues as $plague) {
        	$plagueList = new PlagueList;
        	$plagueList->price_list_id = $priceList->id;
        	$plagueList->plague_id = $plague;
        	$plagueList->save();
        }

    }

    public function edit($id)
    {
        $id = SecurityController::decodeId($id);
        $priceLists_id = PriceList::find($id);

        // Middleware to deny access to data from another company
        if ($priceLists_id->company_id != auth()->user()->companie) {
            SecurityController::abort();
        }

        $priceLists = DB::table('price_lists as pl')
                        ->join('indications as in', 'pl.indications_id', 'in.id')
                        ->join('users as u', 'pl.user_id', 'u.id')
                        ->join('establishment_types as et', 'pl.establishment_id', 'et.id')
                        ->select('pl.*', 'in.name as indications', 'in.description as indications_description',
                            'et.name as establishment', 'u.name as username', 'pl.customer_portal', 'pl.profile_job_center_id',
                            'pl.unit_code_billing', 'pl.service_code_billing')
                        ->where('pl.id', $id)
                        ->first();

        $plaguesList = DB::table('plagues_list')->where('price_list_id', $id)->get();
        $prices = DB::table('prices')->where('price_list_id', $id)->get();
        $plagues = plague_type::where('profile_job_center_id', $priceLists->profile_job_center_id)->orderBy('name','asc')->get();
        $establishments = establishment_type::where('profile_job_center_id', $priceLists->profile_job_center_id)->orderBy('name','asc')->get();
        $indications = Indication::where('profile_job_center_id', $priceLists->profile_job_center_id)->orderBy('name','asc')->get();
        $productUnitSat = UnitSat::find($priceLists->unit_code_billing);
        if ($priceLists->unit_code_billing == null || $priceLists->unit_code_billing == 0) $productCodeSat = null;
        else {
            $keyword = $priceLists->service_code_billing != null ? $priceLists->service_code_billing : '0000';
            $productCodeSat = FacturamaLibrary::getCatalogsProductsOrServicesByKeyword($keyword);
            $productCodeSat = count($productCodeSat) > 0 ? $productCodeSat[0] : null;
        }
        
        $company = companie::find(Auth::user()->companie);

        return view('vendor.adminlte.catalogs.pricelists._modalEdit')
            ->with([
                'plagues' => $plagues,
                'establishments' => $establishments,
                'indications' => $indications,
                'pricesList' => $priceLists,
                'plaguesList' => $plaguesList,
                'prices' => $prices,
                'productUnitSat' => $productUnitSat,
                'productCodeSat' => $productCodeSat,
                'company' => $company
            ]);
    }

    public function update(Request $request)
    {
        $customerPortal = 0;
        $show_price = 1;
        $isDisinfection = 0;
        $isIndividualPrice = 0;
        $individualPrice = 0;

        if ($request->get('customerPortal') == "true") $customerPortal = 1;
        if ($request->get('customerShowPrice') == "false") $show_price = 0;
        if ($request->get('isDisinfection') == "true") $isDisinfection = 1;
        if ($request->get('isIndividualPrice') == "true") {
            $isIndividualPrice = 1;
            $individualPrice = $request->get('priceValue');
        }

        $priceList = PriceList::find($request->get('id'));
        $priceList->name = $request->get('name');
        $priceList->key = $request->get('key');
        $priceList->hierarchy = $request->get('hierarchy');
        $priceList->description = $request->get('description');
        $priceList->establishment_id = $request->get('establishment');
        $priceList->indications_id = $request->get('indications');
        $priceList->reinforcement_days = $request->get('reinforcementDays');
        $priceList->customer_portal = $customerPortal;
        $priceList->show_price = $show_price;
        $priceList->days_expiration_certificate = $request->get('dateExpirationCertificate');
        $priceList->warranty = $request->get('warrantyQuotation');
        $priceList->is_disinfection = $isDisinfection;
        $priceList->description_billing = $request->get('descriptionBilling');
        $priceList->legend = $request->get('legend');
        $priceList->unit_code_billing = $request->get('serviceUnitSatId');
        $priceList->service_code_billing = $request->get('serviceCodeSatId');
        $priceList->individual_price = $isIndividualPrice;
        $priceList->value_individual_price = $individualPrice;
        $priceList->save();

        //Actualiza los precios por area pertenecientes a la lista creada.
        $json = $request->input('arrayConcepts');
        $array = json_decode($json);

        DB::table('prices')->where('price_list_id', $request->id)->delete();

        foreach($array as $obj){
            $prices = new Price;
            $prices->price_list_id = $priceList->id;
            $prices->area = $obj->area;
            $prices->quantity = $obj->quantity;
            $prices->price_one = $obj->price_one;
            $prices->price_two = $obj->price_two;
            $prices->save();
        }

        //Guardar las plagas pertenecientes a la lista creada.
        DB::table('plagues_list')->where('price_list_id', $request->id)->delete();

        $plagues = $request->plagues;
        foreach ($plagues as $plague) {
            $plagueList = new PlagueList;
            $plagueList->price_list_id = $priceList->id;
            $plagueList->plague_id = $plague;
            $plagueList->save();
        }
    }

    public function delete($id)
    {
    	$priceList = PriceList::find($id);
    	$priceList->delete();

    	return redirect()->route('list_prices')->with('success', 'Lista Eliminada Correctamente.');
    }

    public function changeStatus($id, Request $request)
    {
    	$priceList = PriceList::find($id);
    	$priceList->status = $request->status;
    	$priceList->save();

    	return redirect()->route('list_prices');
    }

    public function view_pdf($id)
    {
        $id = SecurityController::decodeId($id);
        $priceLists = DB::table('price_lists as pl')
                        ->join('indications as in', 'pl.indications_id', 'in.id')
                        ->join('users as u', 'pl.user_id', 'u.id')
                        ->join('establishment_types as et', 'pl.establishment_id', 'et.id')
                        ->select('pl.*', 'in.name as indications', 'in.description as indications_description', 'et.name as establishment', 'u.name as username')
                        ->where('pl.id', $id)
                        ->first();
        return view('vendor.adminlte.catalogs.pricelists._viewpdf')->with(['priceLists' => $priceLists]);
    }

    public function pdf(Request $request){

        $price = $request->priceList;
        $pdf = PriceList::find($price);
        $logo = $request->file('plantilla');
        $filename = $logo->store('pdf');
        $pdf->pdf = $filename;
        $pdf->save();

        return redirect()->route('list_prices');
    }

    public function download($id)
    {
        $pdf = PriceList::find($id);
        $ruta = storage_path().'/app/'.$pdf->pdf;
        return response()->download($ruta);
    }

    public function deleteQuoteCoverListPrice(Request $request){
        try{
            $idListPrice = $request->get('dataId');
            $listPrice = PriceList::find($idListPrice);

            if ($listPrice->company_id != Auth::user()->companie) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal, intente de nuevo.'
                ]);
            }

            $urlPdf = $listPrice->pdf;
            $listPrice->pdf = null;
            $listPrice->save();
            Storage::delete($urlPdf);

            return response()->json([
                'code' => 201,
                'message' => 'Portada eliminada correctamente.',
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function view_portada($id)
    {
        $id = SecurityController::decodeId($id);
        $priceLists = DB::table('price_lists as pl')
                        ->join('indications as in', 'pl.indications_id', 'in.id')
                        ->join('users as u', 'pl.user_id', 'u.id')
                        ->join('establishment_types as et', 'pl.establishment_id', 'et.id')
                        ->select('pl.*', 'in.name as indications', 'in.description as indications_description', 'et.name as establishment', 'u.name as username')
                        ->where('pl.id', $id)
                        ->first();
        return view('vendor.adminlte.catalogs.pricelists._viewportada')->with(['priceLists' => $priceLists]);
    }

    public function add_pdf(Request $request){

        $price = $request->priceList;
        $pdf = PriceList::find($price);
        $logo = $request->file('portada');
        $filename = $logo->store('folder_mip');
        $pdf->portada = $filename;
        $pdf->save();

        return redirect()->route('list_prices');
    }

    public function downloadMip($id)
    {
        $pdf = PriceList::find($id);
        $ruta = storage_path().'/app/'.$pdf->portada;
        return response()->download($ruta);
    }

    public function deleteFolderMipListPrice(Request $request){
        try{
            $idListPrice = $request->get('dataId');
            $listPrice = PriceList::find($idListPrice);

            if ($listPrice->company_id != Auth::user()->companie) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal, intente de nuevo.'
                ]);
            }

            $urlPdf = $listPrice->portada;
            $listPrice->portada = null;
            $listPrice->save();
            Storage::delete($urlPdf);

            return response()->json([
                'code' => 201,
                'message' => 'Eliminando Carpeta MIP.',
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

}
