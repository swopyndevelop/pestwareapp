<?php

namespace App\Http\Controllers;

use App\CheckListMonitoringResponse;
use App\customer_branche;
use App\event;
use App\MonitoringTree;
use App\PlagueResponseInspection;
use App\service_order;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TestImportsController extends Controller
{

    private $message = "Datos importados correctamente.";

    public function format()
    {
        try {
            $data = array(
                array('branch_id' =>50, 'customer_id' => 1, 'name' => 150, 'manager' => 200, 'phone' => '4494157769', 'email' => '', 'address_number' => '', 'address' => '', 'colony' => '', 'municipality' => '', 'state' => '', 'postal_code' => '')
            );
            Excel::create('Formato sucursales cliente', function($excel) use ($data) {

                $excel->sheet('Mi lista de precios', function($sheet) use ($data) {

                    $sheet->fromArray($data);

                });
            })->export('xls');
        }catch (\Exception $exception) {
            redirect()->route('index_register');
        }
    }

    public function import() {

        $path = "import.xlsx";

        if ($path != null) {
            $pathComplete = 'storage/app/' . $path;
            try {
                Excel::load($pathComplete, function($reader) {

                    // Getting all results
                    $rows = $reader->get();

                    foreach ($rows as $row) {
                        if ($row->branch_id != null) {
                            $price = new customer_branche;
                            $price->branch_id = $row->branch_id;
                            $price->customer_id = $row->customer_id;
                            $price->name = $row->name;
                            $price->manager = $row->manager;
                            $price->phone = $row->phone;
                            $price->email = $row->email;
                            $price->address_number = $row->address_number;
                            $price->address = $row->address;
                            $price->colony = $row->colony;
                            $price->municipality = $row->municipality;
                            $price->state = $row->state;
                            $price->postal_code = $row->postal_code;
                            $price->save();
                        }
                    }

                });
            }catch (\Exception $exception) {
                $this->message = $exception->getMessage();
            }
        }else {
            $this->message = "No ha sido posible cargar el archivo.";
        }

        print($this->message);

    }

    public function importEvents() {

        $path = "importEventsZac.xlsx";

        if ($path != null) {
            $pathComplete = 'storage/app/' . $path;
            try {
                Excel::load($pathComplete, function($reader) {

                    // Getting all results
                    $rows = $reader->get();

                    foreach ($rows as $row) {
                        if ($row->title != null) {
                            $event = new event;
                            $event->title = $row->title;
                            $event->id_employee = $row->id_employee;
                            $event->id_service_order = $row->id_service_order;
                            $event->initial_hour = $row->initial_hour;
                            $event->final_hour = $row->final_hour;
                            $event->initial_date = $row->initial_date;
                            $event->final_date = $row->final_date;
                            $event->start_event = $row->start_event;
                            $event->final_event = $row->final_event;
                            $event->id_job_center = $row->id_job_center;
                            $event->companie = $row->companie;
                            $event->id_status = $row->id_status;
                            $event->x_cut = $row->x_cut;
                            $event->date_cut = $row->date_cut;
                            $event->service_type = $row->service_type;
                            $event->save();
                        }
                    }

                });
            }catch (\Exception $exception) {
                $this->message = $exception->getMessage();
            }
        }else {
            $this->message = "No ha sido posible cargar el archivo.";
        }

        print($this->message);

    }

    public function subtotalServiceOrder($id) {
        $subtotals = service_order::where('companie', $id)
            ->where('subtotal', 0)->get();
        foreach ($subtotals as $subtotal) {
            $subtotal->subtotal = $subtotal->total;
            $subtotal->save();
        }
        dd('ok');
    }

    public function instanceSicom($id) {
        $plagues = PlagueResponseInspection::join('check_monitoring_responses as cmr', 'plagues_response_inspection.id_station_response','cmr.id')
            ->where('cmr.id_inspection', $id)
            ->select('plagues_response_inspection.id_station_response', 'plagues_response_inspection.id_plague', 'plagues_response_inspection.quantity',
                'cmr.id_station')
            ->get();
        foreach ($plagues as $plague){
            $newStation = CheckListMonitoringResponse::where('id_station', $plague->id_station)
            ->where('id_inspection', 670)->first();
            $newPlague = new PlagueResponseInspection();
            $newPlague->id_station_response = $newStation->id;
            $newPlague->id_plague = $plague->id_plague;
            $newPlague->quantity = $plague->quantity;
            $newPlague->save();
        }
        dd('Save');

    }

}
