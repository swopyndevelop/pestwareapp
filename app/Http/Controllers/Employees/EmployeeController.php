<?php

namespace App\Http\Controllers\Employees;

use App\companie;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Mail\AccountPortalCustomerMail;
use App\profile_job_center;
use App\treeJobCenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use App\User;
use App\employee;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{

    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

	protected $rules =
	[
		'nameEmployee'   => 'Required',
        'email'          => 'Required|email|max:255|unique:users',
		'inputjobTitle'  => 'Required',
		'inputJobCenter' => 'Required'
	];

    protected $rulesUpdate =
        [
            'nameEmployee'   => 'Required',
            'email'          => 'Required|email|max:255',
            'inputjobTitle'  => 'Required',
            'inputJobCenter' => 'Required'
        ];

	public function kardex($jobcenter = 0){
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->join('job_title_profiles as jtp','employees.job_title_profile_id', 'jtp.id')
            ->join('tree_job_profiles as tjf', 'jtp.job_title_profiles_id','tjf.id_inc')
            ->select('employees.profile_job_center_id','tjf.parent','tjf.id')
            ->first();

        $jt = DB::table('job_title_profiles')->where('companie', Auth::user()->companie)->orderBy('job_title_name','asc')->get();

        if ($employee->parent != '#' && $employee->id != 1 || $employee->parent != 1 && $employee->id != 'j1_4') {
            $jt = DB::table('job_title_profiles as jtp')->join('tree_job_profiles as tjf', 'jtp.job_title_profiles_id','tjf.id_inc')
                ->select('jtp.*','tjf.parent')
                ->where('jtp.companie', Auth::user()->companie)
                ->orderBy('jtp.job_title_name')
                ->get();
        }

        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $idJobCenter = 0;
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }
        $jobCentersUnique = DB::table('profile_job_centers')->where('id', $jobcenter)->get();

        $search = \Request::get('search');
		$employee = DB::table('employees as e')
            ->join('companies as com', 'e.id_company', '=', 'com.id')
            ->join('users as u','e.employee_id','=','u.id')
            ->join('job_title_profiles as jp','e.job_title_profile_id', '=', 'jp.id')
            ->join('profile_job_centers as jc', 'e.profile_job_center_id', '=','jc.id')
            ->join('tree_job_centers as tjc', 'jc.profile_job_centers_id', '=', 'tjc.id_inc')
            ->select('e.*', 'u.email', 'jp.job_title_name as jobTitleName', 'jc.name as jobCenter', 'jc.profile_job_centers_id', 'jp.job_title_profiles_id' ,'com.name as company', 'tjc.parent as Zone')
            ->where('e.profile_job_center_id', $profileJobCenter)
            ->where('e.name', 'like', '%' . $search . '%')
            ->where('status', '<>', 200);

		$jc = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->orderBy('name','asc')->get();
		$zones = DB::table('tree_job_centers')->where('company_id', Auth::user()->companie)->orderBy('text','asc')->get();
		$usr = DB::table('employees')->where('employee_id', auth()->user()->id)->first();

		$users = User::where('companie', Auth::user()->companie)->get();
		$companie = DB::table('companies')->where('id_company', Auth::user()->companie)->first();

		$isCreated = true;
		if ($employee->count() == $companie->no_employees) $isCreated = false;

		return view('adminlte::employees.kardex', [
		    'employee' => $employee->paginate(10),
            'zones' => $zones,
            'usuario' => $usr,
            'jobTitle' => $jt,
            'jobCenter' => $jc,
            'users' => $users,
            'companie' => $companie,
            'isCreated' => $isCreated,
            'jobCenters' => $jobCenters,
            'jobCenterSession' => $jobCenterSession,
            'jobcenter' => $jobcenter,
            'jobCentersUnique' => $jobCentersUnique]);
	}

	public function kardexEvaluations($jobCenterID, $jobTitle){

		$employee = DB::table('employees as e')
		->join('companies as com', 'e.id_company', '=', 'com.id')
		->join('users as u','e.employee_id','=','u.id')
		->join('job_title_profiles as jp','e.job_title_profile_id', '=', 'jp.id')
		->join('profile_job_centers as jc', 'e.profile_job_center_id', '=','jc.id')
		->join('tree_job_centers as tjc', 'jc.profile_job_centers_id', '=', 'tjc.id_inc')
		->where('tjc.id_inc',$jobCenterID)
		->orWhere('jp.job_title_profiles_id',$jobTitle)
		->select('e.*', 'u.email', 'jp.job_title_name as jobTitleName', 'jc.name as jobCenter', 'jc.profile_job_centers_id', 'com.name as company', 'tjc.parent as Zone')->get();

		$progress = DB::table('course_progresses');
		$grades = DB::table('training_user_grades');

		$zones = DB::table('tree_job_centers')->where('company_id', Auth::user()->companie)->get();

		return view('adminlte::employees.generalkardex', ['employee' => $employee, 'zones' => $zones, 'progress' => $progress, 'grades' => $grades]);
	}

	public function kardexSucursales($search)
	{
		$raw;
		$column;
		$title = $search;
		if ($search == 'jobcenters') {
			$raw = DB::table('tree_job_centers')->where('id',1)->get();
			$region;

			$region1 = DB::table('tree_job_centers')->where('tree_job_centers.parent', 1 )->get();
			$regiones = array();
			$zonas = array();
			$determinantes = array();
			$sucursal = array();
			$empleado = array();

			foreach ($region1 as $cont => $region ) {
				$sumzonas=0;

				$zona1 = DB::table('tree_job_centers')->where('parent', $region->id)->get();

				foreach ($zona1 as $key => $zona) {
					$det1 = DB::table('tree_job_centers')->where('parent',$zona->id)->get();
					$sumdet = 0;

					foreach ($det1 as $contDet => $det) {

						$suc1 = DB::table('tree_job_centers')->where('parent', $det->id)->get();

						$sum = 0;

						foreach ($suc1 as $contsuc => $suc) {

							$temp = DB::table('employees')->join('profile_job_centers as jobTreeCenter','jobTreeCenter.id','employees.profile_job_center_id')->where('jobTreeCenter.profile_job_centers_id', $suc->id_inc)->select(DB::raw('AVG(course_percentage) as promedio'))->first();

							$promedio = 0;
							if($temp->promedio != null)
								$promedio = $temp->promedio;
							array_push($sucursal, ['text' => $suc->text , 'id' => $suc->id, 'parent_id' => $det->id, 'parent' => $det->text, 'promedio' => $promedio]);
							$emp1 = DB::table('employees')->join('profile_job_centers as jobTreeCenter','jobTreeCenter.id','employees.profile_job_center_id')->where('jobTreeCenter.profile_job_centers_id', $suc->id_inc)->get();

							foreach ($emp1 as $contemp => $emp) {
								$empleado1 = DB::table('employees')->where('employee_id',$emp->employee_id)->first();

								array_push($empleado, ['text' => $empleado1->name , 'id' => $empleado1->employee_id, 'parent_id' => $suc->id, 'promedio' => $empleado1->course_percentage]);

							}

						}


						for ($i=0; $i < count($sucursal); $i++) {
							if($sucursal[$i]['parent_id'] == $det->id)
								$sum += (float) $sucursal[$i]['promedio'];
						}
						if(count($suc1)>0)
						array_push($determinantes, ['text' => $det->text , 'id' => $det->id, 'parent_id' => $zona->id, 'parent' => $zona->text, 'promedio' => ($sum / count($suc1))]);
					else
						array_push($determinantes, ['text' => $det->text , 'id' => $det->id, 'parent_id' => $zona->id, 'parent' => $zona->text, 'promedio' => 0]);
					}
					for ($i=0; $i < count($determinantes); $i++) {
						if($determinantes[$i]['parent_id'] == $zona->id)
							$sumdet += (float) $determinantes[$i]['promedio'];
					}
					if(count($det1)>0){
						array_push($zonas, ['text' => $zona->text , 'id' => $zona->id, 'parent_id' => $region->id, 'parent' => $region->text, 'promedio' => ($sumdet / count($det1))]);
					}
					else{
						array_push($zonas, ['text' => $zona->text , 'id' => $zona->id, 'parent_id' => $region->id, 'parent' => $region->text, 'promedio' => 0]);
					}
				}
				for ($i=0; $i < count($zonas); $i++) {
					if($zonas[$i]['parent_id'] == $region->id)
						$sumzonas += (float) $zonas[$i]['promedio'];
				}
				array_push($regiones, ['text' => $region->text , 'id' => $region->id, 'parent' => 'Circle K', 'promedio' => ($sumzonas / count($zona1))]);
			}
			$column = $regiones;

						return view('adminlte::employees.generalsearch', ['column' => $column,'zonas' => $zonas, 'determinantes' => $determinantes, 'sucursal' => $sucursal,'empleado' => $empleado, 'title' => $search]);
		}
		elseif ($search == 'jobtitles'){
			$raw = DB::table('tree_job_profiles')->get();
			$column = DB::table('employees')->join('job_title_profiles as jobTitle','jobTitle.id','employees.job_title_profile_id')->select('jobTitle.job_title_profiles_id as jobCenterID', DB::raw('AVG(course_percentage) as promedio'))->groupBy('job_title_profiles_id')->get();
		}

		//return view('adminlte::employees.generalsearch', ['raw' => $raw, 'column' => $column, 'title' => $title]);
	}

	public function index($idEmployee)
	{

		$employee = null;
		if ($idEmployee != 0)
		{
			$employee = DB::table('employees as e')
                ->join('companies as c','c.id', '=', 'e.id_company')
                ->select('e.name','e.id as id', 'e.job_title_profile_id as jobTitle', 'e.profile_job_center_id as jobCenter', 'c.name as company')
                ->where('e.id', $idEmployee)->first();
		}
		else{
			$employee = null;
		}
		$jt = DB::table('job_title_profiles')->where('company', Auth::user()->companie)->get();
		$jc = DB::table('profile_job_centers')->where('company', Auth::user()->companie)->get();
		return view('adminlte::employees.index')->with(['jobTitle' => $jt, 'jobCenter' => $jc, 'employee' => $employee]);
	}

	public function update(Request $request)
	{
        $validator = Validator::make(Input::all(), $this->rules);
        $validatorUpdate = Validator::make(Input::all(), $this->rulesUpdate);

        $employee = employee::where('employee_id', $request->get('employeeId'))->first();

        if (empty($employee)){

            if ($validator->fails()) {
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
            }
            else {

                // generate random 10 char password from below chars
                $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
                $password = substr($random, 0, 10);
                $passwordEncrypt = bcrypt($password);

                $user = new User;
                $user->name = $request->nameEmployee;
                $user->email = $request->email;
                $user->password = $passwordEncrypt;
                $user->companie = Auth::user()->companie;
                $user->id_plan = $this->PLAN_BUSINESS; //TODO: Check plan contract company
                $user->confirmation_code = str_random(50);
                $user->save();

                $employee = new employee;
                $employee->employee_id = $user->id;
                $employee->name = $request->nameEmployee;
                $employee->id_company = Auth::user()->companie;
                $employee->job_title_profile_id = $request->inputjobTitle;
                $employee->profile_job_center_id = $request->inputJobCenter;
                $employee->color = $request->color;
                $employee->save();

                $idJobTitle = DB::table('job_title_profiles')->where('id', $request->inputjobTitle)->first();
                $role = DB::table('roles')->where('display_name', $idJobTitle->job_title_profiles_id)->first();

                //create new assignment rol user.
                $roleUser = DB::table('role_user')->insert(['user_id' => $user->id, 'role_id' => $role->id]);

                // Send data account access to system
                $companie = companie::find(Auth::user()->companie);
                config(['mail.from.name' => $companie->name]);
                $url = "https://pestwareapp.com/login";
                $email = $request->email;
                $result = filter_var($email, FILTER_VALIDATE_EMAIL);
                if ($result != false) {
                    Mail::to($email)->send(new AccountPortalCustomerMail(
                        $email, $password, $url, $companie->name, null, $user->confirmation_code));
                }

                return redirect()->route('kardex_employees');
            }
		}else{

            if ($validatorUpdate->fails()) {
                return Response::json(array('errors' => $validatorUpdate->getMessageBag()->toArray()));
            }
            else {
                $user = User::find($employee->employee_id);
                $user->name = $request->nameEmployee;
                $user->email = $request->email;
                $user->save();

                $employee->name = $request->nameEmployee;
                $employee->job_title_profile_id = $request->inputjobTitle;
                $employee->profile_job_center_id = $request->inputJobCenter;
                $employee->color = $request->color;
                $employee->save();

                $idJobTitle = DB::table('job_title_profiles')->where('id', $request->inputjobTitle)->first();
                $role = DB::table('roles')->where('display_name', $idJobTitle->job_title_profiles_id)->first();
                $user_role = DB::table('role_user')->where('user_id',$user->id)->first();

                //create new assignment rol user.
                if($user_role->role_id != 2){
                    DB::table('role_user')->where('user_id', $user->id)->update(['role_id' => $role->id]);
                }

                return redirect()->route('kardex_employees');
            }
        }
	}

	public function searchEmployee(Request $request)
	{
		$tempo = DB::table('employees')
            ->join('companies as c','c.id','employees.id_company')
            ->join('users as u', 'employees.employee_id', 'u.id')
            ->select('employees.name','c.name as inputCompany', 'job_title_profile_id as inputjobTitle',
                'profile_job_center_id as inputJobCenter', 'employees.color', 'u.email')
            ->where('employee_id', $request->Employee_id)->first();
		return response()->json($tempo);
	}

	public function updateStatus($employee_status, Request $request)
    {
        $status = $request->get('StatusEdit' . $employee_status);

        $sta = DB::table('employees')->where('id', '=', $employee_status)->update(['status' => $status]);

        return redirect()->route('kardex_employees');
    }

    public function changePassword(Request $request){
        try {
            $idEmployee = $request->get('idEmployee');
            $idEmployee = employee::find($idEmployee);
            $user = User::find($idEmployee->employee_id);
            $user->password = bcrypt('PestWareApp2021@');
            $user->confirmed = 1;
            $user->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se reseteo correctamente la contraseña.'
            ]);
        }
        catch (\Exception $exception) {
            return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function showTrackingMap()
    {
        $employees = employee::where('id_company', \Auth::user()->companie)
            ->where('status', '<>', 200)
            ->get();
        return view('adminlte::gps.index')->with(['technicians' => $employees]);
    }

    public function getLastPosition()
    {
        $employees = employee::where('id_company', \Auth::user()->companie)
            ->where('gps', '!=', null)->get();
        foreach ($employees as $employee) {
            $lastPosition = DB::table('tracking')
                ->where('id_employee', $employee->id)
                ->latest()
                ->first();
            $employee->lastPosition = $lastPosition;
            $events = DB::table('events')->where('id_employee', $employee->id)
                ->where('initial_date', Carbon::now()->toDateString())
                ->get();
            $employee->events = $events;
        }
        return response()->json($employees);
    }

    public function getTracking(Request $request)
    {
        $id = $request->get('employee_id');
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');
        $tracking = DB::table('tracking')
            ->where('id_employee', $id)
            ->whereDate('date', '>=', $initialDate)
            ->whereDate('date', '<=', $finalDate)
            ->get();
        return response()->json($tracking);
    }

}
