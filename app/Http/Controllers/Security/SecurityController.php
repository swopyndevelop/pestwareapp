<?php

namespace App\Http\Controllers\Security;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Vinkla\Hashids\Facades\Hashids;

class SecurityController extends Controller
{
    // Function abort view
    public static function abort()
    {
        return abort(404);
    }

    /**
     * @param $id
     * Decode id with Hashid
     */
    public static function decodeId($id)
    {
        $decodeId = Hashids::decode($id);
        if (empty($decodeId)) return abort(404);
        else return $decodeId[0];
    }

    /**
     * @param $id
     * Encode id with Hashid
     */
    public static function encodeId($id)
    {
        $encodeId = Hashids::encode($id);
        if (empty($encodeId)) return abort(404);
        else return $encodeId;
    }

}
