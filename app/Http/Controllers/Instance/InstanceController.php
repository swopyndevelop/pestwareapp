<?php

namespace App\Http\Controllers\Instance;
use App\address_job_center;
use App\aditional_info;
use App\Country;
use App\customer;
use App\entry;
use App\entry_product;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\Indication;
use App\MonitoringCondition;
use App\Notifications\LogsNotification;
use App\payment_way;
use App\personal_mail;
use App\PlagueCategory;
use App\PlagueList;
use App\Price;
use App\PriceList;
use App\product;
use App\profile_job_center;
use App\storehouse;
use App\Tax;
use App\transfer_ce_product;
use App\transfer_center_employee;
use App\TypeStation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\source_origin;
use App\application_method;
use App\concept;
use App\establishment_type;
use App\infestation_degree;
use App\order_cleaning;
use App\payment_method;
use App\product_presentation;
use App\product_type;
use App\product_unit;
use App\voucher;
use App\treeJobCenter;
use App\treeJobProfile;
use App\Role;
use App\job_title_profile;
use App\plague_type;
use App\extra;
use App\discount;
use App\User;
use App\companie;
use App\company_payment;
use App\employee;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class InstanceController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function viewConfigStart(){
        return view('adminlte::auth._modalConfigStart');
    }

    public function addPictureLogo(Request $request)
    {
        $idCompanie = auth()->user()->companie;

        $filename = $request->file('file')->store(
            'logos', 'ftp'
        );

        $picture = companie::where('id', $idCompanie)->first();
        $picture->logo = $filename;
        $picture->save();
    }

    public function addPictureMiniLogo(Request $request)
    {
        $idCompanie = auth()->user()->companie;

        $filename = $request->file('file')->store(
            'mini_logos', 'ftp'
        );

        $picture = companie::where('id', $idCompanie)->first();
        $picture->mini_logo = $filename;
        $picture->save();
    }
    
    public function addPDFLogo(Request $request)
    {
        $idCompanie = auth()->user()->companie;

        $filename = $request->file('file')->store(
            'pdf_logos', 'ftp'
        );

        $picture = companie::where('id', $idCompanie)->first();
        $picture->pdf_logo = $filename;
        $picture->save();
    }

    public function addPDFSello(Request $request)
    {
        $idCompanie = auth()->user()->companie;

        $filename = $request->file('file')->store(
            'pdf_sellos', 'ftp'
        );

        $picture = companie::where('id', $idCompanie)->first();
        $picture->pdf_sello = $filename;
        $picture->save();
    }

    public function addPDFSanitaryLicense(Request $request)
    {
        $idCompanie = auth()->user()->companie;

        $filename = $request->file('file')->store(
            'pdf_licencias_sanitarias', 'ftp'
        );

        $picture = companie::where('id', $idCompanie)->first();
        $picture->pdf_sanitary_license = $filename;
        $picture->save();
    }

    public function storeConfigStart(Request $request) {
        try {
            $idCompanie = auth()->user()->companie;
            $companie = companie::find($idCompanie);
            $companie->id_theme = $request->theme;
            $companie->licence = $request->licencia;
            $companie->facebook = $request->facebook;
            $companie->save();

            return response()->json([
                'code' => 201,
                'message' => 'Datos guardados correctamente.'
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public static function createNewInstance($companieId, $userId){

        try {

            DB::beginTransaction();

            $companie = companie::find($companieId);

            //Create storehouse clients
            $userStorehouseClient = user::where('companie', 1)->get();
            foreach ($userStorehouseClient as $user) {
                $userStorehouse = user::find($user->id);
                $new = $userStorehouse->replicate();
                $new->companie = $companieId;
                $new->email = "almacen.clientes." . $companieId . "@example.com";
                $new->save();

                $employee = new employee;
                $employee->employee_id = $new->id;
                $employee->name = $new->name;
                $employee->id_company = $new->companie;
                $employee->status = 200;
                $employee->color = "#1E8CC7";
                $employee->save();
            }

            //Create tree jobcenter
            $treeJobCenter = new treeJobCenter;
            $treeJobCenter->id = 1;
            $treeJobCenter->company_id = $companieId;
            $treeJobCenter->parent = "#";
            $treeJobCenter->text = $companie->name;
            $treeJobCenter->save();

            // Create profile job center
            $profileJobCenter = new profile_job_center;
            $profileJobCenter->profile_job_centers_id = $treeJobCenter->id;
            $profileJobCenter->name = $treeJobCenter->text;
            $profileJobCenter->companie = $companieId;
            $profileJobCenter->save();

            $addressJobCenter = new address_job_center;
            $addressJobCenter->address_job_centers_id = $treeJobCenter->id;
            $addressJobCenter->profile_job_centers_id = $treeJobCenter->id;
            $addressJobCenter->street = "Conocido";
            $addressJobCenter->num_ext = 1;
            $addressJobCenter->zip_code = 20000;
            $addressJobCenter->location = "Conocido";
            $addressJobCenter->municipality = "Conocido";
            $addressJobCenter->state = "Conocido";
            $addressJobCenter->save();

            //Create tree jobprofile
            $treeProfArray = array([
                'id' => '1',
                'company_id' => $companieId,
                'parent' => "#",
                'text' => "Director de Operaciones",
                'description' => "Rol para el puesto de director de operaciones."
            ]);
            array_push($treeProfArray, [
                'id' => 'j1_1',
                'company_id' => $companieId,
                'parent' => "1",
                'text' => "Técnico Aplicador",
                'description' => "Rol para el puesto de técnico aplicador."
            ]);
            array_push($treeProfArray, [
                'id' => 'j1_3',
                'company_id' => $companieId,
                'parent' => "1",
                'text' => "Servicio al Cliente",
                'description' => "Rol para el puesto de servicio al cliente."
            ]);
            array_push($treeProfArray, [
                'id' => 'j1_4',
                'company_id' => $companieId,
                'parent' => "1",
                'text' => "Administrativo",
                'description' => "Rol para el puesto de Administrativo."
            ]);
            array_push($treeProfArray, [
                'id' => 'j1_5',
                'company_id' => $companieId,
                'parent' => "1",
                'text' => "Contador",
                'description' => "Rol para el puesto de Contador."
            ]);
            foreach ($treeProfArray as $data) {
                $treeJobProfile = new treeJobProfile;
                $treeJobProfile->id = $data['id'];
                $treeJobProfile->company_id = $data['company_id'];
                $treeJobProfile->parent = $data['parent'];
                $treeJobProfile->text = $data['text'];
                $treeJobProfile->save();

                $rol = new Role;
                $rol->name = $data['text'];
                $rol->display_name = $treeJobProfile->id;
                $rol->description = $data['description'];
                $rol->id_company = $data['company_id'];
                $rol->save();

                $jobTitleProf = new job_title_profile;
                $jobTitleProf->job_title_profiles_id = $treeJobProfile->id;
                $jobTitleProf->job_title_name = $data['text'];
                $jobTitleProf->companie = $data['company_id'];
                $jobTitleProf->save();
            }

            //Assign permission roles
            $roleTechnician = DB::table('roles')->where('name', 'Técnico Aplicador')
                ->where('id_company', $companieId)->first();
            DB::table('permission_role')->insert(['permission_id' => 3, 'role_id' => $roleTechnician->id]);
            DB::table('permission_role')->insert(['permission_id' => 7, 'role_id' => $roleTechnician->id]);
            DB::table('permission_role')->insert(['permission_id' => 9, 'role_id' => $roleTechnician->id]);
            DB::table('permission_role')->insert(['permission_id' => 15, 'role_id' => $roleTechnician->id]);

            //Create employee master account
            $user = DB::table('users')
                ->select('id','name','companie')->where('id', $userId)->first();

            $jtp = DB::table('job_title_profiles')->select('id')->where('companie', $companieId)->first();
            $employee = new employee;
            $employee->employee_id = $user->id;
            $employee->name = $user->name;
            $employee->id_company = $user->companie;
            $employee->job_title_profile_id = $jtp->id;
            $employee->profile_job_center_id = $profileJobCenter->id;
            $employee->color = '#1E8CC7';
            $employee->file_route_firm = "https://pestwareapp.com/profile/firms/demo_sign_technician.JPG";
            $employee->status = 1;
            $employee->save();

            //Create discounts
            $discounts = discount::where('id_company', 40)->get();
            foreach ($discounts as $d) {
                $discount = discount::find($d->id);
                $new = $discount->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create extras
            $extras = extra::where('id_company', 40)->get();
            foreach ($extras as $e) {
                $extra = extra::find($e->id);
                $new = $extra->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create plagues start
            $plagues = plague_type::where('id_company', 40)->get();
            foreach ($plagues as $p) {
                $plague = plague_type::find($p->id);
                $new = $plague->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create plague categories start
            $plagueCategories = PlagueCategory::where('id_company', 40)->get();
            foreach ($plagueCategories as $p) {
                $cat = PlagueCategory::find($p->id);
                $new = $cat->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create source origins start
            $sourceOrigins = source_origin::where('companie', 40)->get();
            foreach ($sourceOrigins as $s) {
                $source = source_origin::find($s->id);
                $new = $source->replicate();
                $new->companie = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create application methods start
            $applicationMethods = application_method::where('id_company', 40)->get();
            foreach ($applicationMethods as $am) {
                $application = application_method::find($am->id);
                $new = $application->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create type stations start
            $typeStations = TypeStation::where('id_company', 40)->where('profile_job_center_id', 76)->get();
            foreach ($typeStations as $ts) {
                $type = TypeStation::find($ts->id);
                $new = $type->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create concepts start
            $conceptsTypeOne = concept::where('id_company', 40)->where('type', 1)->get();
            foreach ($conceptsTypeOne as $cto) {
                $conceptO = concept::find($cto->id);
                $new = $conceptO->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            $conceptsTypeTwo = concept::where('id_company', 40)->where('type', 2)->get();
            foreach ($conceptsTypeTwo as $cto) {
                $conceptT = concept::find($cto->id);
                $new = $conceptT->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create establishment types start
            $establishmentTypes = establishment_type::where('id_company', 40)->get();
            foreach ($establishmentTypes as $est) {
                $establishment = establishment_type::find($est->id);
                $new = $establishment->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create establishment types start
            $infestationDegrees = infestation_degree::where('companie', 40)->get();
            foreach ($infestationDegrees as $ind) {
                $infestation = infestation_degree::find($ind->id);
                $new = $infestation->replicate();
                $new->companie = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create order cleanings start
            $orderCleanings = order_cleaning::where('companie', 40)->get();
            foreach ($orderCleanings as $or) {
                $order = order_cleaning::find($or->id);
                $new = $order->replicate();
                $new->companie = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create payment methods start
            $paymentMethods = payment_method::where('companie', 40)->get();
            foreach ($paymentMethods as $py) {
                $payment = payment_method::find($py->id);
                $new = $payment->replicate();
                $new->companie = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create payment way start
            $paymentWays = payment_way::where('id_company', 40)->get();
            foreach ($paymentWays as $pw) {
                $payment = payment_way::find($pw->id);
                $new = $payment->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create product presentations start
            $productPresentations = product_presentation::where('id_company', 40)->get();
            foreach ($productPresentations as $pr) {
                $payment = product_presentation::find($pr->id);
                $new = $payment->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create product types start
            $productTypes = product_type::where('id_company', 40)->get();
            foreach ($productTypes as $pr) {
                $payment = product_type::find($pr->id);
                $new = $payment->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create product units start
            $productUnits = product_unit::where('id_company', 40)->get();
            foreach ($productUnits as $pr) {
                $payment = product_unit::find($pr->id);
                $new = $payment->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create vouchers start
            $vouchers = voucher::where('companie', 40)->get();
            foreach ($vouchers as $pr) {
                $payment = voucher::find($pr->id);
                $new = $payment->replicate();
                $new->companie = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create taxes start
            $taxes = Tax::where('id_company', 40)->get();
            foreach ($taxes as $tx) {
                $tax = Tax::find($tx->id);
                $new = $tax->replicate();
                $new->id_company = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create monitoring conditions
            $monitoringConditions = MonitoringCondition::where('id_company', 1)->get();
            foreach ($monitoringConditions as $monitoringCondition) {
                $condition = MonitoringCondition::find($monitoringCondition->id);
                $new = $condition->replicate();
                $new->id_company = $companieId;
                $new->save();
            }

            //Create indications
            $indications = Indication::where('company_id', 40)->get();
            foreach ($indications as $ind) {
                $indication = Indication::find($ind->id);
                $new = $indication->replicate();
                $new->company_id = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();
            }

            //Create personal mails
            $personalMail = personal_mail::where('id_company', 40)->first();
            $new = $personalMail->replicate();
            $new->id_company = $companieId;
            $new->profile_job_center_id = $profileJobCenter->id;
            $new->save();

            //$fileName = explode('.', $personalMail->banner);
            //$fileNameNewBanner = $fileName[0] . "-" . $companieId . ".jpeg";
            //Storage::copy($personalMail->banner, $fileNameNewBanner);

            //Create prices list
            $pricesList = PriceList::where('company_id', 40)->get();
            foreach ($pricesList as $pl) {
                $fileNameNew = "";
                $fileNameNewMIP = "";
                if ($pl->pdf != null) {
                    $fileName = explode('.', $pl->pdf);
                    $fileNameNew = $fileName[0] . "-" . $companieId . ".pdf";
                    Storage::copy($pl->pdf, $fileNameNew);
                }
                if ($pl->portada != null) {
                    $fileName = explode('.', $pl->portada);
                    $fileNameNewMIP = $fileName[0] . "-" . $companieId . ".pdf";
                    Storage::copy($pl->portada, $fileNameNewMIP);
                }

                $list = PriceList::find($pl->id);
                $new = $list->replicate();
                $new->company_id = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->user_id = $userId;
                $new->status = 1;
                $new->pdf = $fileNameNew;
                $new->portada = $fileNameNewMIP;
                $new->reinforcement_days = 21;
                $establishmentBefore = establishment_type::find($pl->establishment_id);
                $establishment = establishment_type::where('name', $establishmentBefore->name)->where('id_company', $companieId)->first();
                $new->establishment_id = $establishment->id;
                $indicationsBefore = Indication::find($pl->indications_id);
                $indications = Indication::where('name', $indicationsBefore->name)->where('company_id', $companieId)->first();
                $new->indications_id = $indications->id;
                $new->save();
                $prices = Price::where('price_list_id', $pl->id)->get();
                foreach ($prices as $price) {
                    $p = Price::find($price->id);
                    $newP = $p->replicate();
                    $newP->price_list_id = $new->id;
                    $newP->save();
                }
                $plagues = PlagueList::where('price_list_id', $pl->id)->get();
                foreach ($plagues as $plague) {
                    $plag = PlagueList::find($plague->id);
                    $plagueBefore = plague_type::find($plag->plague_id);
                    $plagueId = plague_type::where('name', $plagueBefore->name)->where('id_company', $companieId)->first();
                    $newPlague = $plag->replicate();
                    $newPlague->price_list_id = $new->id;
                    $newPlague->plague_id = $plagueId->id;
                    $newPlague->save();
                }
            }

            //Create products
            $products = product::where('id_companie', 40)->get();
            $type = product_type::where('name', 'Plaguicida')->where('id_company', $companieId)->first();
            foreach ($products as $p) {
                $product = product::find($p->id);
                $new = $product->replicate();
                $new->id_companie = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->id_type_product = $type->id;
                $presentationBefore = product_presentation::find($p->id_presentation);
                $presentation = product_presentation::where('name', $presentationBefore->name)->where('id_company', $companieId)->first();
                $new->id_presentation = $presentation->id;
                $unitBefore = product_unit::find($p->id_unit);
                $unit = product_unit::where('name', $unitBefore->name)->where('id_company', $companieId)->first();
                $new->id_unit = $unit->id;
                $new->id_companie = $companieId;
                $new->profile_job_center_id = $profileJobCenter->id;
                $new->save();

                //Aditional info pdf
                $aditionalPDF = aditional_info::where('product_id', $p->id)->get();
                foreach ($aditionalPDF as $item) {
                    $fileName = explode('.', $item->file_route);
                    $fileNameNew = $fileName[0] . "-" . $companieId . ".pdf";
                    Storage::copy($item->file_route, $fileNameNew);

                    $aditional = aditional_info::find($item->id);
                    $newAditional = $aditional->replicate();
                    $newAditional->product_id = $new->id;
                    $newAditional->file_route = $fileNameNew;
                    $newAditional->save();

                }
            }

            //Create Entry
            $product = product::where('name', 'Bayer - Bothrine Flow')->where('id_companie', $companieId)->first();
            $entry = new entry;
            $entry->id_entry = "E-1";
            $entry->id_user = $userId;
            $entry->origin = "Prueba Entrada";
            $entry->id_job_center = $profileJobCenter->id;
            $entry->companie = $companieId;
            $entry->total = $product->base_price;
            $entry->save();

            $entryProduct = new entry_product;
            $entryProduct->id_entry = $entry->id;
            $entryProduct->id_product = $product->id;
            $entryProduct->units = 2;
            $entryProduct->price = $product->base_price * 2;
            $entryProduct->save();

            //Transfer storehouse
            //variables
            $totalStore = $product->base_price * 2;
            $quantityProduct = DB::table('products')->select('quantity')->where('id', $product->id)->first();
            $quantityProduct = $quantityProduct->quantity * 2;

            //update storehouse destinity
            $isStorehouse = DB::table('storehouses')->where('id_job_center', $profileJobCenter->id)
                ->where('id_product', $product)
                ->first();
            if (empty($isStorehouse)) {
                $storehouse = new storehouse();
                $storehouse->id_store = "A-0001";
                $storehouse->id_job_center = $profileJobCenter->id;
                $storehouse->id_product = $product->id;
                $storehouse->stock = 2;
                $storehouse->stock_other_units = $quantityProduct;
                $storehouse->total = $totalStore;
                $storehouse->companie = $companieId;
                $storehouse->save();
                $storehouseId = storehouse::find($storehouse->id);
                $storehouseId->id_store = "A-" . $storehouse->id;
                $storehouseId->save();
            }else {
                $storehouse = storehouse::find($isStorehouse->id);
                $storehouse->stock = $storehouse->stock + 1;
                $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                $storehouse->total = $storehouse->total + $totalStore;
                $storehouse->save();
            }

            //Create transfer SE
            $transfer = new transfer_center_employee();
            $transfer->id_transfer_ce = 'TCE-1';
            $transfer->id_user = $userId;
            $transfer->total = 1098;
            $transfer->id_job_center_origin = $profileJobCenter->id;
            $transfer->id_employee_destiny = $employee->id;
            $transfer->save();

            $transferCCProduct = new transfer_ce_product();
            $transferCCProduct->id_transfer_ce = $transfer->id;
            $transferCCProduct->id_product = $product->id;
            $transferCCProduct->units = 1;
            $transferCCProduct->is_units = 1;
            $transferCCProduct->save();

            $storehouse = new storehouse();
            $storehouse->id_store = "A-0002";
            $storehouse->id_employee = $employee->id;
            $storehouse->id_product = $product->id;
            $storehouse->stock = 1;
            $storehouse->stock_other_units = $product->quantity;
            $storehouse->total = $product->base_price;
            $storehouse->companie = $companieId;
            $storehouse->save();
            $storehouseId = storehouse::find($storehouse->id);
            $storehouseId->id_store = "A-" . $storehouse->id;
            $storehouseId->save();

            //update storehouse origin
            $storehouseOrigin = DB::table('storehouses')->where('id_job_center', $profileJobCenter->id)
                ->where('id_product', $product->id)
                ->first();
            $storehouseO = storehouse::find($storehouseOrigin->id);
            $storehouseO->stock = 1;
            $storehouseO->stock_other_units = $product->quantity;
            $storehouseO->total = $product->base_price;
            $storehouseO->save();

            //Create test data instance
            $testData = new TestDataInstance($companieId, $userId, $profileJobCenter->id);
            $testData->createTestData();

            DB::commit();
            return true;

        } catch (\Exception $exception) {
            User::first()->notify(new LogsNotification($exception->getLine(), $exception->getMessage(), 3));
            DB::rollBack();
            return false;
        }

    }

    public function editConfigStart() {
        //Validate plan free
        if(Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $plan = DB::table('plans')->where('id', Auth::user()->id_plan)->first();
        $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
        $countryCode = DB::table('countries')
            ->where('id', $companie->id_code_country)
            ->first();
        $dateUpdate=$countryCode->updated_at;
        $dateUpdateModified = Carbon::parse($dateUpdate)->toDateString();

        $pdfOrImage = $companie->pdf_sanitary_license;
        if($pdfOrImage != null) {
            $pdfOrImageExplode = explode(".", $pdfOrImage);
            $pdfOrImageExplode = $pdfOrImageExplode[1];
        } else $pdfOrImageExplode = null;

        $planExtras = [];
        if($plan->id == 3) { // Empresarial
            if(($userExtras = $companie->no_employees - 6) > 0) {
                $planExtras[] = "Con $userExtras usuario(s) extra";
            }
        } elseif($plan->id == 2) { // Emprendedor
            if(($userExtras = $companie->no_employees - 3) > 0) {
                $planExtras[] = "Con $userExtras usuario(s) extra";
            }
        }
        if($plan->id != 1) {
            if(($branchOfficeExtras =  $companie->no_branch_office - 1) > 0) {
                $planExtras[] = "Con $branchOfficeExtras sucursal(es) extra";
            }
        }

        // check if the company has a payment source
        $companyPayment = DB::table('company_payment')->where('id_company', Auth::user()->companie)->first();

        $subscriptionPaymentDate = null;
        if($companie->conekta_subscription_payment_date != null) {
            $companie->conekta_subscription_payment_date;
            $subscriptionPaymentDate = 'Fecha de creación de suscripción: '.$companie->conekta_subscription_payment_date;
        }

        return view('adminlte::auth._modalConfigStartEdit')
            ->with(
                [
                    'companie' => $companie,
                    'pdfOrImageExplode' => $pdfOrImageExplode,
                    'countryCode' => $countryCode,
                    'dateUpdateModified' => $dateUpdateModified,
                    'planName' => $plan->name,
                    'planExtras' => $planExtras,
                    'companyPayment' => $companyPayment,
                    'subscriptionPaymentDate' => $subscriptionPaymentDate
                ]);
    }

    public function updateConfigStart(Request $request){
        try {
            $companie = companie::find(Auth::user()->companie);
            $companie->id_theme = $request->get('theme');
            $companie->cover_mip_color = $request->get('updateColorMIP');
            $companie->save();

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados correctamente.'
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
    public function updateConfigStartTax(Request $request){
        try {
            $companie = companie::find(Auth::user()->companie);
            $idCodeCountry = $companie->id_code_country;
            $country = Country::find($idCodeCountry);
            $country->iva_country = $request->get('iva_country');
            $country->isr_country = $request->get('isr_country');
            $country->save();

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados correctamente.'
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
