<?php


namespace App\Http\Controllers\Instance;


use App\application_method;
use App\canceled_service_order;
use App\cash;
use App\cash_picture;
use App\condition_picture;
use App\customer;
use App\customer_data;
use App\discount;
use App\discount_quotation;
use App\establishment_type;
use App\event;
use App\extra;
use App\habit_condition_service_order;
use App\infestation_degree;
use App\inspection_picture;
use App\mascot_service_order;
use App\Notifications\LogsNotification;
use App\order_cleaning;
use App\order_cleaning_place_condition;
use App\payment_method;
use App\place_condition;
use App\place_inspection;
use App\place_inspection_plague_type;
use App\plague_control;
use App\plague_control_application_method;
use App\plague_control_picture;
use App\plague_control_product;
use App\plague_type;
use App\plague_type_quotation;
use App\PriceList;
use App\product;
use App\quotation;
use App\ratings_service_orders;
use App\rejected_quotation;
use App\service_firms;
use App\service_order;
use App\source_origin;
use App\storehouse;
use App\tracing;
use App\transfer_ee_product;
use App\transfer_employee_employee;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TestDataInstance
{
    private $userId;
    private $extraId;
    private $companyId;
    private $customerId;
    private $jobCenterId;
    private $priceListId;
    private $quotationId;
    private $discountId;
    private $lastQuotation;
    private $firstQuotation;
    private $establishmentId;

    /**
     * TestDataInstance constructor.
     * @param $companyId
     * @param $userId
     * @param $jobCenterId
     */
    public function __construct($companyId, $userId, $jobCenterId)
    {
        $this->companyId = $companyId;
        $this->userId = $userId;
        $this->jobCenterId = $jobCenterId;
    }

    /**
     * Create test data instance
     */
    public function createTestData() {
        try {
            DB::beginTransaction();
            $this->createTestCustomer();
            $this->createQuotationsTest();
            $this->createOrdersTest();
            DB::commit();
        } catch (\Exception $exception) {
            User::first()->notify(new LogsNotification($exception->getMessage(), $exception->getLine(), 3));
            DB::rollBack();
        }

    }

    /**
     * Create a test customer
     */
    private function createTestCustomer()
    {
        $establishment = establishment_type::where('profile_job_center_id', $this->jobCenterId)
            ->where('name', 'Residencial')
            ->first();
        $this->establishmentId = $establishment->id;
        $sourceOrigin = source_origin::where('profile_job_center_id', $this->jobCenterId)
            ->where('name', 'Recomendación')
            ->first();

        $customer = new customer;
        $customer->name = "Cliente Prueba";
        $customer->establishment_name = "Empresa Prueba";
        $customer->cellphone = "4494139091";
        $customer->establishment_id = $establishment->id;
        $customer->colony = "Bosques del Prado Norte";
        $customer->municipality = "Aguascalientes";
        $customer->source_origin_id = $sourceOrigin->id;
        $customer->companie = $this->companyId;
        $customer->id_profile_job_center = $this->jobCenterId;
        $customer->save();

        $customerDatas = new customer_data;
        $customerDatas->customer_id = $customer->id;
        $customerDatas->address = "Avenida Universidad";
        $customerDatas->address_number = "1001";
        $customerDatas->state = "Aguascalientes";
        $customerDatas->email = "contacto@pestwareapp.com";
        $customerDatas->phone_number = "4494139091";
        $customerDatas->save();

        $this->customerId = $customer->id;
    }

    /**
     * Create a test quotations
     */
    private function createQuotationsTest()
    {
        $extra = extra::where('id_company', $this->companyId)->where('name', 'Sin Extra')->first();
        $this->extraId = $extra->id;

        $quotations = array_fill(0, 3, '');

        foreach ($quotations as $i => $quotation) {

            $status = 10;
            $folio = "C-1";
            $createdAt = Carbon::now()->toDateTimeString();
            if ($i == 0) {
                $status = 1;
                $priceList = PriceList::where('company_id', $this->companyId)->where('name', 'Cucaracha Alemana')->first();
                $this->priceListId = $priceList->id;
            }
            if ($i == 1) {
                $status = 3;
                $folio = "C-2";
                $priceList = PriceList::where('company_id', $this->companyId)->where('name', 'Chinches')->first();
                $this->priceListId = $priceList->id;
                $createdAt = Carbon::now()->addSecond()->toDateTimeString();
            }
            if ($i == 2) {
                $status = 4;
                $folio = "C-3";
                $priceList = PriceList::where('company_id', $this->companyId)->where('name', 'Mantenimiento Residencial')->first();
                $this->priceListId = $priceList->id;
                $createdAt = Carbon::now()->addSeconds(2)->toDateTimeString();
            }

            $quotation = new quotation;
            $quotation->id_quotation = $folio;
            $quotation->id_customer = $this->customerId;
            $quotation->construction_measure = 300;
            $quotation->id_plague_jer = 1;
            $quotation->establishment_id = $this->establishmentId;
            $quotation->total = 665;
            $quotation->price = 950;
            $quotation->price_reinforcement = 550;
            $quotation->id_status = $status;
            $quotation->user_id = $this->userId;
            $quotation->id_job_center = $this->jobCenterId;
            $quotation->companie = $this->companyId;
            $quotation->id_extra = $this->extraId;
            $quotation->id_price_list = $this->priceListId;
            $quotation->created_at = $createdAt;
            $quotation->save();

            $this->quotationId = $quotation->id;

            $discount = discount::where('id_company', $this->companyId)
                ->where('description', 'Descuento del 30%')
                ->first();
            $this->discountId = $discount->id;

            $discountQuotation = new discount_quotation;
            $discountQuotation->quotation_id = $quotation->id;
            $discountQuotation->discount_id = $this->discountId;
            $discountQuotation->save();

            $plagues = plague_type::where('id_company', $this->companyId)
                ->whereIn('name', ['Arañas', 'Cucaracha Alemana', 'Hormigas', 'Moscas', 'Mosquitos'])
                ->get();

            foreach ($plagues as $plague) {
                $plagueQuotation = new plague_type_quotation;
                $plagueQuotation->quotation_id = $this->quotationId;
                $plagueQuotation->plague_type_id = $plague->id;
                $plagueQuotation->save();
            }

            //type quotation
            if ($i == 0) {
                $tracing = new tracing;
                $tracing->id_quotation = $quotation->id;
                $tracing->tracing_date = Carbon::now()->toDateString();
                $tracing->tracing_hour = Carbon::now()->toTimeString();
                $tracing->commentary = "Cliente quedo de confirmar el día de mañana por la tarde.";
                $tracing->save();
                $this->firstQuotation = $quotation->id;
            }
            if ($i == 1) {
                $rejected = new rejected_quotation;
                $rejected->id_quotation = $quotation->id;
                $rejected->reason = "Otro";
                $rejected->commentary = "Al cliente le parece caro el servicio.";
                $rejected->date = Carbon::now()->toDateString();
                $rejected->save();
            }
            if ($i == 2) {
                $this->lastQuotation = $quotation->id;
            }

        }

    }

    private function createOrdersTest() {
        $paymentMethod = payment_method::where('name', 'Transferencia')->where('companie', $this->companyId)->first();

        $status = 4;
        $folio = 'OS-1';

        $service = new service_order;
        $service->id_service_order = $folio;
        $service->id_quotation = $this->firstQuotation;
        $service->id_payment_method = $paymentMethod->id;
        $service->id_payment_way = 2;
        $service->observations = "Favor de tocar fuerte, timbre no funciona.";
        $service->total = 665;
        $service->user_id = $this->userId;
        $service->id_job_center = $this->jobCenterId;
        $service->companie = $this->companyId;
        $service->id_status = $status;
        $service->save();

        $habitCondOne = new habit_condition_service_order;
        $habitCondOne->service_order_id = $service->id;
        $habitCondOne->habit_condition_id = 1;
        $habitCondOne->save();

        $habitCond = new habit_condition_service_order;
        $habitCond->service_order_id = $service->id;
        $habitCond->habit_condition_id = 3;
        $habitCond->save();

        $mascot = new mascot_service_order;
        $mascot->service_order_id = $service->id;
        $mascot->mascot_id = 1;
        $mascot->save();

        $idEmployee = DB::table('employees')->where('employee_id', $this->userId)->first()->id;

        $event = new event;
        $event->title = 'Fumigación de Cliente Prueba';
        $event->id_service_order = $service->id;
        $event->id_employee = $idEmployee;
        $event->id_job_center = $service->id_job_center;
        $event->companie = $this->companyId;
        $event->initial_hour = "09:00";
        $event->final_hour = "11:00";
        $event->initial_date = Carbon::now()->toDateString();
        $event->final_date = Carbon::now()->toDateString();
        $event->start_event = "09:05";
        $event->final_event = "10:45";
        $event->id_status = 4;
        $event->service_type = 1;
        $event->save();

        // App
        $placeInspection = new place_inspection;
        $placeInspection->id_service_order = $service->id;
        $placeInspection->nesting_areas = "habitaciones, baño Marcos de puertas, muebles y cocina";
        $placeInspection->commentary = "La cucaracha se encuentra distribuida en toda la casa concentrada principalmnete en sitios cercanos a cocina y baño.";
        $placeInspection->save();

        $inspectionPicture = new inspection_picture;
        $inspectionPicture->place_inspection_id =$placeInspection->id;
        $inspectionPicture->file_route = "services/place_inspections/demo_insp_1.jpeg";
        $inspectionPicture->save();

        $plague1 = plague_type::where('name', 'Mosca Metálica')->where('id_company', $this->companyId)->first();
        $degree1 = infestation_degree::where('name', 'Bajo')->where('companie', $this->companyId)->first();

        $plague2 = plague_type::where('name', 'Arañas')->where('id_company', $this->companyId)->first();
        $degree2 = infestation_degree::where('name', 'Nulo')->where('companie', $this->companyId)->first();

        $plague3 = plague_type::where('name', 'Hormigas')->where('id_company', $this->companyId)->first();
        $degree3 = infestation_degree::where('name', 'Medio')->where('companie', $this->companyId)->first();

        $plague4 = plague_type::where('name', 'Mosquitos')->where('id_company', $this->companyId)->first();
        $degree4 = infestation_degree::where('name', 'Bajo')->where('companie', $this->companyId)->first();

        $plague5 = plague_type::where('name', 'Cucaracha Alemana')->where('id_company', $this->companyId)->first();
        $degree5 = infestation_degree::where('name', 'Muy alto')->where('companie', $this->companyId)->first();

        $placeInspectionPlague = new place_inspection_plague_type;
        $placeInspectionPlague->place_inspection_id = $placeInspection->id;
        $placeInspectionPlague->plague_type_id = $plague1->id;
        $placeInspectionPlague->id_infestation_degree = $degree1->id;
        $placeInspectionPlague->save();

        $placeInspectionPlague = new place_inspection_plague_type;
        $placeInspectionPlague->place_inspection_id = $placeInspection->id;
        $placeInspectionPlague->plague_type_id = $plague2->id;
        $placeInspectionPlague->id_infestation_degree = $degree2->id;
        $placeInspectionPlague->save();

        $placeInspectionPlague = new place_inspection_plague_type;
        $placeInspectionPlague->place_inspection_id = $placeInspection->id;
        $placeInspectionPlague->plague_type_id = $plague3->id;
        $placeInspectionPlague->id_infestation_degree = $degree3->id;
        $placeInspectionPlague->save();

        $placeInspectionPlague = new place_inspection_plague_type;
        $placeInspectionPlague->place_inspection_id = $placeInspection->id;
        $placeInspectionPlague->plague_type_id = $plague4->id;
        $placeInspectionPlague->id_infestation_degree = $degree4->id;
        $placeInspectionPlague->save();

        $placeInspectionPlague = new place_inspection_plague_type;
        $placeInspectionPlague->place_inspection_id = $placeInspection->id;
        $placeInspectionPlague->plague_type_id = $plague5->id;
        $placeInspectionPlague->id_infestation_degree = $degree5->id;
        $placeInspectionPlague->save();

        $placeCondition = new place_condition;
        $placeCondition->id_service_order = $service->id;
        $placeCondition->indications = 2;
        $placeCondition->restricted_access = "Muebles pesados en cocina y no siguio las preparaciones.";
        $placeCondition->commentary = "Requiere refuerzo";
        $placeCondition->save();

        $conditionPicture = new condition_picture;
        $conditionPicture->place_condition_id =$placeCondition->id;
        $conditionPicture->file_route = "services/place_conditions/demo_cond_1.jpeg";
        $conditionPicture->save();

        $orderCleaning1 = order_cleaning::where('name', 'Presencia de Alimentos')->where('companie', $this->companyId)->first();
        $orderCleaning2 = order_cleaning::where('name', 'Restos de Basura')->where('companie', $this->companyId)->first();
        $orderCleaning3 = order_cleaning::where('name', 'Artículos de Cocina expuestos')->where('companie', $this->companyId)->first();
        $orderCleaning4 = order_cleaning::where('name', 'Desorden')->where('companie', $this->companyId)->first();

        $cleaning = new order_cleaning_place_condition;
        $cleaning->place_condition_id = $placeCondition->id;
        $cleaning->order_cleaning_id = $orderCleaning1->id;
        $cleaning->save();

        $cleaning = new order_cleaning_place_condition;
        $cleaning->place_condition_id = $placeCondition->id;
        $cleaning->order_cleaning_id = $orderCleaning2->id;
        $cleaning->save();

        $cleaning = new order_cleaning_place_condition;
        $cleaning->place_condition_id = $placeCondition->id;
        $cleaning->order_cleaning_id = $orderCleaning3->id;
        $cleaning->save();

        $cleaning = new order_cleaning_place_condition;
        $cleaning->place_condition_id = $placeCondition->id;
        $cleaning->order_cleaning_id = $orderCleaning4->id;
        $cleaning->save();

        $plagueControl = new plague_control;
        $plagueControl->id_service_order = $service->id;
        $plagueControl->control_areas = "Casa en general";
        $plagueControl->commentary = "es importante mejorar la limpieza retirando y evitando acumulación de restos orgánicos para prevenir proliferación de fauna nociva";
        $plagueControl->save();

        $plaguePicture = new plague_control_picture;
        $plaguePicture->plague_control_id =$plagueControl->id;
        $plaguePicture->file_route = "services/plague_controls/demo_plague_1.jpeg";
        $plaguePicture->save();

        $plaguePicture = new plague_control_picture;
        $plaguePicture->plague_control_id =$plagueControl->id;
        $plaguePicture->file_route = "services/plague_controls/demo_plague_2.jpeg";
        $plaguePicture->save();

        $method1 = application_method::where('name', 'Aspersión')->where('id_company', $this->companyId)->first();
        $method2 = application_method::where('name', 'Nebulización')->where('id_company', $this->companyId)->first();

        $plagueControlMethod = new plague_control_application_method;
        $plagueControlMethod->plague_control_id = $plagueControl->id;
        $plagueControlMethod->id_application_method = $method1->id;
        $plagueControlMethod->save();

        $plagueControlMethod = new plague_control_application_method;
        $plagueControlMethod->plague_control_id = $plagueControl->id;
        $plagueControlMethod->id_application_method = $method2->id;
        $plagueControlMethod->save();

        $product = product::where('name', 'Bayer - Bothrine Flow')->where('id_companie', $this->companyId)->first();

        $plagueProduct = new plague_control_product;
        $plagueProduct->plague_control_id = $plagueControl->id;
        $plagueProduct->id_product = $product->id;
        $plagueProduct->dose = "15mlxLt";
        $plagueProduct->quantity = 60;
        $plagueProduct->save();

        //Create transfer employee to client
        $storehouseClient = DB::table('employees')->where('id_company', $this->companyId)
            ->where('status', 200)->first();
        $user = DB::table('employees')->where('employee_id', $this->userId)->first();
        $transfer = new transfer_employee_employee();
        $transfer->id_transfer_ee = 'TAC-1';
        $transfer->id_user = $this->userId;
        $transfer->id_employee_origin = $user->id;
        $transfer->id_employee_destiny = $storehouseClient->id;
        $transfer->companie = $this->companyId;
        $transfer->save();

        //Update storehouse
        $storehouse = DB::table('storehouses')->where('id_employee', $user->id)->where('id_product', $product->id)->first();
        $priceByQuantity = $product->base_price / $product->quantity;
        $totalStore = $priceByQuantity * 60;
        $stock = storehouse::find($storehouse->id);
        $stock->stock = 1;
        $stock->stock_other_units = $product->quantity - 60;
        $stock->total = $stock->total - $totalStore;
        $stock->save();

        // Update storehouse customer
        $storehouseCustomer = DB::table('storehouses')->where('id_employee', $storehouseClient->id)->where('id_product', $product->id)->first();
        if (empty($storehouseCustomer)) {
            $storehouseCust = new storehouse();
            $storehouseCust->id_store = "A-0001";
            $storehouseCust->id_employee = $storehouseClient->id;
            $storehouseCust->id_product = $product->id;
            $storehouseCust->stock = 1;
            $storehouseCust->stock_other_units = 60;
            $storehouseCust->total = $totalStore;
            $storehouseCust->companie = $this->companyId;
            $storehouseCust->save();
            $storehouseId = storehouse::find($storehouseCust->id);
            $storehouseId->id_store = "A-" . $storehouseCust->id;
            $storehouseId->save();
        }else{
            $storehouseCust = storehouse::find($storehouseCustomer->id);
            $storehouseCust->stock_other_units = $stock->stock_other_units + 60;
            $storehouseCust->total = $stock->total + $totalStore;
            $storehouseCust->save();
        }

        //Transfer products
        $transferCCProduct = new transfer_ee_product();
        $transferCCProduct->id_transfer_ee = $transfer->id;
        $transferCCProduct->id_product = $product->id;
        $transferCCProduct->units = 60;
        $transferCCProduct->is_units = 0;
        $transferCCProduct->save();

        //Update total transfer
        $entryTransfer = transfer_employee_employee::find($transfer->id);
        $entryTransfer->total = $entryTransfer->total + $totalStore;
        $entryTransfer->save();

        $cashe = new cash;
        $cashe->id_service_order = $service->id;
        $cashe->id_event = $event->id;
        $cashe->id_payment_method = $paymentMethod->id;
        $cashe->id_payment_way = 2;
        $cashe->companie = $this->companyId;
        $cashe->amount_received = 665;
        $cashe->commentary = "El cliente mostró el recibo de transferencia con #rastreo: 2812200";
        $cashe->payment = 2;
        $cashe->save();

        $cashePicture = new cash_picture;
        $cashePicture->cash_id =$cashe->id;
        $cashePicture->file_route = "services/cash/demo_cashe_1.png";
        $cashePicture->save();

        $firm = new service_firms;
        $firm->id_service_order = $service->id;
        $firm->file_route = "services/firms/demo_firm_1.JPG";
        $firm->save();

        $rating = new ratings_service_orders;
        $rating->user_id = $this->userId;
        $rating->id_service_order = $service->id;
        $rating->id_event = $event->id;
        $rating->rating = 5;
        $rating->comments = "Muy Responsable, nos explico todo, el seguimiento y demas.";
        $rating->save();

        //$this->createReinforcement($service->id);
        $this->createTracing($service->id);
        $this->createLastOrder();

    }

    private function createReinforcement($idOrder) {
        $order = service_order::find($idOrder);
        $order->reinforcement = 1;
        $order->save();
        $eventOld = event::where('id_service_order', $idOrder)->first();

        $reinforcement = new service_order;
        $reinforcement->id_service_order = 'R-' . $order->id_quotation;
        $reinforcement->id_quotation = $order->id_quotation;
        $reinforcement->id_payment_method = $order->id_payment_method;
        $reinforcement->id_payment_way = $order->id_payment_way;
        $reinforcement->bussiness_name = $order->bussiness_name;
        $reinforcement->address = $order->address;
        $reinforcement->email = $order->email;
        $reinforcement->observations = $order->observations;
        $reinforcement->total = 550;
        $reinforcement->user_id = $this->userId;
        $reinforcement->id_job_center = $order->id_job_center;
        $reinforcement->companie = $this->companyId;
        $reinforcement->id_status = 4;
        $reinforcement->reinforcement = 1;
        $reinforcement->save();

        $event = new event;
        $event->title = 'Refuerzo de Cliente Prueba';
        $event->id_service_order = $reinforcement->id;
        $event->id_employee = $eventOld->id_employee;
        $event->id_job_center = $reinforcement->id_job_center;
        $event->companie = $this->companyId;
        $event->initial_hour = "09:00";
        $event->final_hour = "11:00";
        $event->initial_date = Carbon::now()->toDateString();
        $event->final_date = Carbon::now()->toDateString();
        $event->id_status = 1;
        $event->service_type = 2;
        $event->save();

    }

    private function createWarranty($idOrder) {
        $order = service_order::find($idOrder);
        $order->warranty = 1;
        $order->save();

        $eventOld = event::where('id_service_order', $idOrder)->first();

        $warranty = new service_order;
        $warranty->id_service_order = 'G-' . $order->id_quotation;
        $warranty->id_quotation = $order->id_quotation;
        $warranty->id_payment_method = $order->id_payment_method;
        $warranty->id_payment_way = $order->id_payment_way;
        $warranty->bussiness_name = $order->bussiness_name;
        $warranty->address = $order->address;
        $warranty->email = $order->email;
        $warranty->observations = $order->observations;
        $warranty->total = 0;
        $warranty->user_id = $this->userId;
        $warranty->id_job_center = $order->id_job_center;
        $warranty->companie = $this->companyId;
        $warranty->id_status = 4;
        $warranty->warranty = 1;
        $warranty->save();

        $event = new event;
        $event->title = 'Garantía de Cliente Prueba';
        $event->id_service_order = $warranty->id;
        $event->id_employee = $eventOld->id_employee;
        $event->id_job_center = $warranty->id_job_center;
        $event->companie = $this->companyId;
        $event->initial_hour = "11:30";
        $event->final_hour = "13:30";
        $event->initial_date = Carbon::now()->toDateString();
        $event->final_date = Carbon::now()->toDateString();
        $event->id_status = 1;
        $event->service_type = 3;
        $event->save();
    }

    private function createTracing($idOrder) {
        $order = service_order::find($idOrder);
        $order->tracing = $order->tracing++;
        $order->save();

        $eventOld = event::where('id_service_order', $idOrder)->first();

        $tracing = new service_order;
        $tracing->id_service_order = 'S-1-1';
        $tracing->id_quotation = $order->id_quotation;
        $tracing->id_payment_method = $order->id_payment_method;
        $tracing->id_payment_way = $order->id_payment_way;
        $tracing->bussiness_name = $order->bussiness_name;
        $tracing->address = $order->address;
        $tracing->email = $order->email;
        $tracing->observations = $order->observations;
        $tracing->total = 0;
        $tracing->user_id = $this->userId;
        $tracing->id_job_center = $order->id_job_center;
        $tracing->companie = $this->companyId;
        $tracing->id_status = 2;
        $tracing->tracing = 1;
        $tracing->save();

        $event = new event;
        $event->title = 'Seguimiento de Cliente Prueba';
        $event->id_service_order = $tracing->id;
        $event->id_employee = $eventOld->id_employee;
        $event->id_job_center = $tracing->id_job_center;
        $event->companie = $this->companyId;
        $event->initial_hour = "15:00";
        $event->final_hour = "17:00";
        $event->initial_date = Carbon::now()->addDay(2)->toDateString();
        $event->final_date = Carbon::now()->addDay(2)->toDateString();
        $event->id_status = 3;
        $event->service_type = 4;
        $event->save();

        $deny = new canceled_service_order;
        $deny->id_service_order = $tracing->id;
        $deny->reason = "Otro";
        $deny->commentary = "Salio de la ciudad";
        $deny->date = Carbon::now()->toDateString();
        $deny->save();
    }

    private function createLastOrder() {
        $paymentMethod = payment_method::where('name', 'Efectivo')->where('companie', $this->companyId)->first();

        $status = 4;
        $folio = 'OS-3';

        $service = new service_order;
        $service->id_service_order = $folio;
        $service->id_quotation = $this->lastQuotation;
        $service->id_payment_method = $paymentMethod->id;
        $service->id_payment_way = 2;
        $service->observations = "Favor de tocar fuerte, timbre no funciona.";
        $service->total = 550;
        $service->user_id = $this->userId;
        $service->id_job_center = $this->jobCenterId;
        $service->companie = $this->companyId;
        $service->id_status = $status;
        $service->save();

        $idEmployee = DB::table('employees')->where('employee_id', $this->userId)->first()->id;

        $event = new event;
        $event->title = 'Fumigación de Cliente Prueba';
        $event->id_service_order = $service->id;
        $event->id_employee = $idEmployee;
        $event->id_job_center = $service->id_job_center;
        $event->companie = $this->companyId;
        $event->initial_hour = "11:30";
        $event->final_hour = "13:30";
        $event->initial_date = Carbon::now()->toDateString();
        $event->final_date = Carbon::now()->toDateString();
        $event->id_status = 1;
        $event->service_type = 1;
        $event->save();
    }
}