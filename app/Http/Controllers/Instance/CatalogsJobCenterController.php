<?php

namespace App\Http\Controllers\Instance;

use App\aditional_info;
use App\application_method;
use App\concept;
use App\discount;
use App\establishment_type;
use App\extra;
use App\Indication;
use App\infestation_degree;
use App\MonitoringCondition;
use App\Notifications\LogsNotification;
use App\order_cleaning;
use App\payment_method;
use App\payment_way;
use App\personal_mail;
use App\plague_type;
use App\PlagueList;
use App\Price;
use App\PriceList;
use App\product;
use App\product_presentation;
use App\product_type;
use App\product_unit;
use App\source_origin;
use App\User;
use App\voucher;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CatalogsJobCenterController extends Controller
{
    public function index(Request $request){

        $profileJobCenters = [
            [
                'companyId' => 231,
                'profileJobCenterId' => 257,
                'profileMain' => 251,
                'userId' => 717
            ],
            [
                'companyId' => 269,
                'profileJobCenterId' => 302,
                'profileMain' => 281,
                'userId' => 1
            ],
            [
                'companyId' => 269,
                'profileJobCenterId' => 303,
                'profileMain' => 281,
                'userId' => 1052
            ],
            [
                'companyId' => 37,
                'profileJobCenterId' => 255,
                'profileMain' => 73,
                'userId' => 16
            ],
            [
                'companyId' => 244,
                'profileJobCenterId' => 270,
                'profileMain' => 268,
                'userId' => 921
            ],
            [
                'companyId' => 244,
                'profileJobCenterId' => 283,
                'profileMain' => 268,
                'userId' => 921
            ],
            [
                'companyId' => 244,
                'profileJobCenterId' => 290,
                'profileMain' => 268,
                'userId' => 921
            ],
            [
                'companyId' => 244,
                'profileJobCenterId' => 291,
                'profileMain' => 268,
                'userId' => 921
            ],
            [
                'companyId' => 244,
                'profileJobCenterId' => 293,
                'profileMain' => 268,
                'userId' => 921
            ],
            [
                'companyId' => 244,
                'profileJobCenterId' => 295,
                'profileMain' => 268,
                'userId' => 921
            ],
        ];
        foreach ($profileJobCenters as $profileJobCenter){
            $this->UpdateCatalogs(
                $profileJobCenter['profileJobCenterId'],
                $profileJobCenter['userId']
            );
        }

    }

    public static function UpdateCatalogs($profileJobCenterId, $userId){
        $companyCurrentId = \Auth::user()->companie;
        $companyParent = $companyCurrentId; //40
        //Create personal mails
        $personalMail = personal_mail::where('id_company', $companyParent)->first();
        $new = $personalMail->replicate();
        $new->id_company = $companyCurrentId;
        $new->profile_job_center_id = $profileJobCenterId;
        $new->save();

        // TODO: Add Logo Banner
        //$fileName = explode('.', $personalMail->banner);
        //$fileNameNewBanner = $fileName[0] . "-" . $profileJobCenterId . ".jpeg";
        //Storage::copy($personalMail->banner, $fileNameNewBanner);

        //Create discounts
        $discounts = discount::where('id_company', $companyParent)->get();
        foreach ($discounts as $d) {
            $discount = discount::find($d->id);
            $new = $discount->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create extras
        $extras = extra::where('id_company', $companyParent)->get();
        foreach ($extras as $e) {
            $extra = extra::find($e->id);
            $new = $extra->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create plagues start
        $plagues = plague_type::where('id_company', $companyParent)->get();
        foreach ($plagues as $p) {
            $plague = plague_type::find($p->id);
            $new = $plague->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create source origins start
        $sourceOrigins = source_origin::where('companie', $companyParent)->get();
        foreach ($sourceOrigins as $s) {
            $source = source_origin::find($s->id);
            $new = $source->replicate();
            $new->companie = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create application methods start
        $applicationMethods = application_method::where('id_company', $companyParent)->get();
        foreach ($applicationMethods as $am) {
            $application = application_method::find($am->id);
            $new = $application->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create concepts start
        $conceptsTypeOne = concept::where('id_company', $companyParent)->where('type', 1)->get();
        foreach ($conceptsTypeOne as $cto) {
            $conceptO = concept::find($cto->id);
            $new = $conceptO->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        $conceptsTypeTwo = concept::where('id_company', $companyParent)->where('type', 2)->get();
        foreach ($conceptsTypeTwo as $cto) {
            $conceptT = concept::find($cto->id);
            $new = $conceptT->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create establishment types start
        $establishmentTypes = establishment_type::where('id_company', $companyParent)->get();
        foreach ($establishmentTypes as $est) {
            $establishment = establishment_type::find($est->id);
            $new = $establishment->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create establishment types start
        $infestationDegrees = infestation_degree::where('companie', $companyParent)->get();
        foreach ($infestationDegrees as $ind) {
            $infestation = infestation_degree::find($ind->id);
            $new = $infestation->replicate();
            $new->companie = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create order cleanings start
        $orderCleanings = order_cleaning::where('companie', $companyParent)->get();
        foreach ($orderCleanings as $or) {
            $order = order_cleaning::find($or->id);
            $new = $order->replicate();
            $new->companie = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create payment methods start
        $paymentMethods = payment_method::where('companie', $companyParent)->get();
        foreach ($paymentMethods as $py) {
            $payment = payment_method::find($py->id);
            $new = $payment->replicate();
            $new->companie = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create indications
        $indications = Indication::where('company_id', $companyParent)->get();
        foreach ($indications as $ind) {
            $indication = Indication::find($ind->id);
            $new = $indication->replicate();
            $new->company_id = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create payment way start
        $paymentWays = payment_way::where('id_company', $companyParent)->get();
        foreach ($paymentWays as $pw) {
            $payment = payment_way::find($pw->id);
            $new = $payment->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create product presentations start
        $productPresentations = product_presentation::where('id_company', $companyParent)->get();
        foreach ($productPresentations as $pr) {
            $payment = product_presentation::find($pr->id);
            $new = $payment->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create product types start
        $productTypes = product_type::where('id_company', $companyParent)->get();
        foreach ($productTypes as $pr) {
            $payment = product_type::find($pr->id);
            $new = $payment->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create product units start
        $productUnits = product_unit::where('id_company', $companyParent)->get();
        foreach ($productUnits as $pr) {
            $payment = product_unit::find($pr->id);
            $new = $payment->replicate();
            $new->id_company = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create vouchers start
        $vouchers = voucher::where('companie', $companyParent)->get();
        foreach ($vouchers as $pr) {
            $payment = voucher::find($pr->id);
            $new = $payment->replicate();
            $new->companie = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->save();
        }

        //Create prices list
        $pricesList = PriceList::where('company_id', $companyParent)->get();
        foreach ($pricesList as $pl) {
            $fileNameNew = "";
            $fileNameNewMIP = "";
            if ($pl->pdf != null) {
                $fileName = explode('.', $pl->pdf);
                $random = str_shuffle('abcdefghjkl');
                $nameRandom = substr($random, 0, 5);
                $fileNameNew = $fileName[0] . "-" . $profileJobCenterId . $nameRandom . ".pdf";
                Storage::copy($pl->pdf, $fileNameNew);
            }
            if ($pl->portada != null) {
                $random = str_shuffle('abcdefghjkl');
                $nameRandom = substr($random, 0, 5);
                $fileName = explode('.', $pl->portada);
                $fileNameNewMIP = $fileName[0] . "-" . $profileJobCenterId .$nameRandom . ".pdf";
                Storage::copy($pl->portada, $fileNameNewMIP);
            }

            $list = PriceList::find($pl->id);
            $new = $list->replicate();
            $new->company_id = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->user_id = $userId;
            $new->status = 1;
            $new->pdf = $fileNameNew;
            $new->portada = $fileNameNewMIP;
            $new->reinforcement_days = 21;
            $establishmentBefore = establishment_type::find($pl->establishment_id);
            $establishment = establishment_type::where('name', $establishmentBefore->name)->where('profile_job_center_id', $profileJobCenterId)->first();
            $new->establishment_id = $establishment->id;
            $indicationsBefore = Indication::find($pl->indications_id);
            $indications = Indication::where('name', $indicationsBefore->name)->where('profile_job_center_id', $profileJobCenterId)->first();
            $new->indications_id = $indications->id;
            $new->save();
            $prices = Price::where('price_list_id', $pl->id)->get();
            foreach ($prices as $price) {
                $p = Price::find($price->id);
                $newP = $p->replicate();
                $newP->price_list_id = $new->id;
                $newP->save();
            }
            $plagues = PlagueList::where('price_list_id', $pl->id)->get();
            foreach ($plagues as $plague) {
                $plag = PlagueList::find($plague->id);
                $plagueBefore = plague_type::find($plag->plague_id);
                $plagueId = plague_type::where('name', $plagueBefore->name)->where('profile_job_center_id', $profileJobCenterId)->first();
                $newPlague = $plag->replicate();
                $newPlague->price_list_id = $new->id;
                $newPlague->plague_id = $plagueId->id;
                $newPlague->save();
            }
        }

        //Create products
        $products = product::where('id_companie', $companyParent)->get();
        $type = product_type::where('name', 'Plaguicida')->where('id_company', $companyParent)->first();
        foreach ($products as $p) {
            $product = product::find($p->id);
            $new = $product->replicate();
            $new->id_companie = $companyCurrentId;
            $new->profile_job_center_id = $profileJobCenterId;
            $new->id_type_product = $type->id;
            $presentationBefore = product_presentation::find($p->id_presentation);
            $presentation = product_presentation::where('name', $presentationBefore->name)->where('id_company', $companyParent)->first();
            $new->id_presentation = $presentation->id;
            $unitBefore = product_unit::find($p->id_unit);
            $unit = product_unit::where('name', $unitBefore->name)->where('id_company', $companyParent)->first();
            $new->id_unit = $unit->id;
            $new->id_companie = $companyCurrentId;
            $new->save();

            //Aditional info pdf
            $aditionalPDF = aditional_info::where('product_id', $p->id)->get();
            foreach ($aditionalPDF as $item) {
                $fileName = explode('.', $item->file_route);
                $random = str_shuffle('abcdefghjkl');
                $nameRandom = substr($random, 0, 5);
                $fileNameNew = $fileName[0] . "-" . $profileJobCenterId . $nameRandom . ".pdf";
                Storage::copy($item->file_route, $fileNameNew);

                $aditional = aditional_info::find($item->id);
                $newAditional = $aditional->replicate();
                $newAditional->product_id = $new->id;
                $newAditional->file_route = $fileNameNew;
                $newAditional->save();

            }
        }

    }

    public function addJobCenterCatalogs(Request $request){

        try {
            $jobCenterId = $request->get('job_center_id');
            $companieId = $request->get('company_id');

            //Create discounts
            discount::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create extras
            extra::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create plagues start
            plague_type::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create source origins start
            source_origin::where('companie', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create application methods start
            application_method::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create concepts start
            concept::where('id_company', $companieId)->where('type', 1)->update(['profile_job_center_id' => $jobCenterId]);

            concept::where('id_company', $companieId)->where('type', 2)->update(['profile_job_center_id' => $jobCenterId]);

            //Create establishment types start
            establishment_type::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create establishment types start
            infestation_degree::where('companie', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create order cleanings start
            order_cleaning::where('companie', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create payment methods start
            payment_method::where('companie', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create indications
            Indication::where('company_id', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create payment way start
            payment_way::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create product presentations start
            product_presentation::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create product types start
            product_type::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create product units start
            product_unit::where('id_company', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create vouchers start
            voucher::where('companie', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create prices list
            PriceList::where('company_id', $companieId)->update(['profile_job_center_id' => $jobCenterId]);

            //Create products
            product::where('id_companie', $companieId)->update(['profile_job_center_id' => $jobCenterId]);
            return \Response::json([
                'correct' => 'Ok'
            ]);
        } catch (Exception $exception) {
            return \Response::json([
                'error' => $exception->getMessage()
            ]);
        }

    }
}