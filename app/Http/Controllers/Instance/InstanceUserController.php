<?php

namespace App\Http\Controllers\Instance;

use App\companie;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstanceUserController extends Controller
{
    public function getCurrentPlan() {
        $plan = User::find(\Auth::user()->id)->id_plan;
        return response()->json([
            'code' => 200,
            'plan' => $plan
        ]);
    }

    public function showPlans() {
        $company = companie::find(\Auth::user()->companie);
        return view('vendor.adminlte.plans.purchasePlan')->with(['company' => $company]);
    }
}
