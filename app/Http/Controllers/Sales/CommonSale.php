<?php


namespace App\Http\Controllers\Sales;


use App\Sale;

class CommonSale
{
    public static function getFolioSale($companyId)
    {
        $sale = Sale::where('id_company', $companyId)->latest()->first();
        if ($sale) {
            if ($sale->id_sale == 'V-1') {
                return 'V-2';
            } else {
                $folio = explode('-', $sale->id_sale);
                return 'V-'.++$folio[1];
            }
        } else return 'V-1';
    }
}