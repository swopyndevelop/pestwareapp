<?php

namespace App\Http\Controllers\Sales;

use App\address_job_center;
use App\archive_expense;
use App\archive_order;
use App\article_order;
use App\Country;
use App\customer;
use App\customer_data;
use App\discount;
use App\DiscountSale;
use App\employee;
use App\establishment_type;
use App\expense;
use App\HistoricalInventory;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\ReportsPDF\ServiceSale;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Mail\PurchaseOrderMail;
use App\Mail\SaleMail;
use App\payment_method;
use App\payment_way;
use App\product;
use App\Product_Tax;
use App\ProductSale;
use App\profile_job_center;
use App\purchase_order;
use App\Sale;
use App\storehouse;
use App\transfer_ee_product;
use App\transfer_employee_employee;
use App\voucher;
use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PDF;
use Response;

class SaleController extends Controller
{
    private $STATUS_SALE_SOLD = 11;
    private $STATUS_SALE_CANCELLED = 12;
    private $STATUS_SALE_PAY = 13;

    public function index() {
        $employee = employee::where('employee_id', Auth::user()->id)->first();
        $customers = customer::where('id_profile_job_center', $employee->profile_job_center_id)->get();
        $payment_ways = payment_way::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        $payment_methods = payment_method::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        $vouchers = voucher::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        $discounts = discount::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        $storehouses = employee::where('status', 500)
            ->where('profile_job_center_id', $employee->profile_job_center_id)->get();

        $sales = DB::table('sales as s')
            ->join('customers as c', 's.id_customer', 'c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('users as u', 's.id_user', 'u.id')
            ->join('payment_ways as pw', 's.id_payment_way', 'pw.id')
            ->join('payment_methods as pm', 's.id_payment_method', 'pm.id')
            ->join('vouchers as v', 's.id_voucher', 'v.id')
            ->join('statuses as st', 's.id_status', 'st.id')
            ->join('employees as e', 's.id_storehouse', 'e.id')
            ->where('s.profile_job_center_id', $employee->profile_job_center_id)
            ->select('s.id', 's.id_sale', 'c.name as customer', 'u.name as username', 'pw.name as payment_way',
                'pm.name as payment_method', 'v.name as voucher', 'st.name as status', 's.date', 's.subtotal', 's.total',
                's.created_at', 'e.name as storehouse','cd.email','c.cellphone','s.url_file_voucher','s.id_status','s.updated_at')
            ->orderBy('s.updated_at','desc')
            ->paginate(10);
        $now = Carbon::now()->toDateString();
        $code_country = CommonCompany::getCodeByCountry();

        return view('vendor.adminlte.sales.index')->with([
            'customers' => $customers,
            'payment_ways' => $payment_ways,
            'payment_methods' => $payment_methods,
            'vouchers' => $vouchers,
            'discounts' => $discounts,
            'storehouses' => $storehouses,
            'sales' => $sales,
            'now' => $now,
            'code_country' => $code_country
        ]);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();
            // Get data.
            $customerId = $request->get('customer');
            $typeSale = $request->get('typeSale');
            $paymentWayId = $request->get('paymentWay');
            $paymentMethodId = $request->get('paymentMethod');
            $voucherId = $request->get('voucher');
            $userId = Auth::user()->id;
            $employee = employee::where('employee_id', $userId)->first();
            $jobCenterId = $employee->profile_job_center_id;
            $companyId = $employee->id_company;
            $subtotal = $request->get('subtotal');
            $total = $request->get('total');
            $date = $request->get('date');
            $description = $request->get('description');
            $discountId = $request->get('discount');
            $customerName = $request->get('customerNameOnly');
            $customerCellphone = $request->get('customerCellphoneOnly');

            if ($customerId == 0) {
                $establishment = establishment_type::where('profile_job_center_id',$jobCenterId)->first();
                $customer = new customer();
                $customer->name = $customerName;
                $customer->cellphone = $customerCellphone;
                $customer->establishment_id = $establishment->id;
                $customer->id_profile_job_center = $jobCenterId;
                $customer->companie = $companyId;
                $customer->save();
                $customerData = new customer_data();
                $customerData->customer_id = $customer->id;
                $customerData->save();
                $customerId = $customer->id;
            }

            //Create new sale.
            $sale = new Sale();
            $sale->id_sale = CommonSale::getFolioSale($companyId);
            $sale->id_customer = $customerId;
            $sale->id_user = $userId;
            $sale->id_payment_way = $paymentWayId;
            $sale->id_payment_method = $paymentMethodId;
            $sale->id_voucher = $voucherId;
            $sale->profile_job_center_id = $jobCenterId;
            $sale->id_company = $companyId;
            $sale->id_status = $this->STATUS_SALE_SOLD;
            $sale->date = $date;
            $sale->subtotal = $subtotal;
            $sale->total = $total;
            $sale->description = $description;
            $sale->id_storehouse = $typeSale;
            $sale->save();

            $discountSale = new DiscountSale();
            $discountSale->id_sale = $sale->id;
            $discountSale->id_discount = $discountId;
            $discountSale->save();

            //Add concepts products of sale.
            $json = $request->input('arrayConcepts');
            $array = json_decode($json);

            foreach($array as $obj) {
                $productSale = new ProductSale();
                $productSale->id_sale = $sale->id;
                $productSale->id_product = $obj->productId;
                $productSale->quantity = $obj->quantity;
                $productSale->sale_price = $obj->lastPrice;
                $productSale->subtotal = $obj->subtotal;
                $productSale->total = $obj->total;
                $productSale->save();
            }

            //Create transfer employee to client
            $storehouseClient = employee::where('id_company', $companyId)->where('status', 200)->first();
            $transfer = new transfer_employee_employee();
            $transfer->id_transfer_ee = 'TAC-' . $sale->id_sale;
            $transfer->id_user = $userId;
            $transfer->id_employee_origin = $typeSale;
            $transfer->id_employee_destiny = $storehouseClient->id;
            $transfer->companie = $companyId;
            $transfer->save();
            $total = 0;
            $source = employee::find($typeSale)->name;
            $destiny = employee::find($storehouseClient->id)->name;

            foreach ($array as $obj) {

                $product = product::find($obj->productId);

                $storehouseOriginPrice = storehouse::where('id_employee', $typeSale)
                    ->where('id_product', $product->id)
                    ->first();
                $units = $obj->quantity;
                $price = $storehouseOriginPrice->total / $storehouseOriginPrice->stock_other_units;

                $transferCCProduct = new transfer_ee_product();
                $transferCCProduct->id_transfer_ee = $transfer->id;
                $transferCCProduct->id_product = $product->id;
                $transferCCProduct->units = $units;
                $transferCCProduct->is_units = 1;
                $transferCCProduct->save();

                $totalStore = $price * $units;
                $quantityProduct = $product->quantity * $units;

                $total += $totalStore;
                $valueMovement = $price * $quantityProduct;

                // History Inventory Before destiny
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = $transfer->id_transfer_ee . '-D';
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = \Illuminate\Support\Facades\Auth::id();
                $historicalInventory->id_type_movement_inventory = 10;
                $historicalInventory->id_product = $product->id;
                $historicalInventory->source = $source;
                $historicalInventory->destiny = $destiny;
                $historicalInventory->quantity = $units;
                $historicalInventory->fraction_quantity = $quantityProduct;
                $historicalInventory->before_stock = $units;
                $historicalInventory->after_stock = $units;
                $historicalInventory->fraction_before_stock = $quantityProduct;
                $historicalInventory->fraction_after_stock = $quantityProduct;
                $historicalInventory->unit_price = $price;
                $historicalInventory->value_movement = $valueMovement;
                $historicalInventory->value_inventory = $valueMovement;
                $historicalInventory->is_destiny = 1;
                $historicalInventory->id_profile_job_center = $product->profile_job_center_id;
                $historicalInventory->id_company = Auth::user()->companie;
                $historicalInventory->save();

                //update storehouse destiny
                $isStorehouse = storehouse::where('id_employee', $storehouseClient->id)
                    ->where('id_product', $product->id)
                    ->first();
                if (empty($isStorehouse)) {
                    $storehouse = new storehouse();
                    $storehouse->id_store = "A-0001";
                    $storehouse->id_employee = $storehouseClient->id;
                    $storehouse->id_product = $product->id;
                    $storehouse->stock = $units;
                    $storehouse->stock_other_units = $quantityProduct;
                    $storehouse->total = $valueMovement;
                    $storehouse->companie = Auth::user()->companie;
                    $storehouse->save();
                    $storehouseId = storehouse::find($storehouse->id);
                    $storehouseId->id_store = "A-" . $storehouse->id;
                    $storehouseId->save();

                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = 0;
                    $updateHistoricalInventory->fraction_before_stock = 0;
                    $updateHistoricalInventory->save();
                }
                else{
                    // Calculate stock units
                    $storehouseOrigin = storehouse::where('id_employee', $storehouseClient->id)
                        ->where('id_product', $product->id)
                        ->first();
                    $stockFinal = $storehouseOrigin->stock_other_units + $quantityProduct;
                    $stockFinal = $stockFinal / $product->quantity;
                    $stockFinal = ceil($stockFinal);
                    $stockFinal = intval($stockFinal);

                    $storehouse = storehouse::find($isStorehouse->id);

                    // update Historical Inventory Destiny
                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = $storehouse->stock;
                    $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                    $updateHistoricalInventory->after_stock = $stockFinal;
                    $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + $quantityProduct;
                    $updateHistoricalInventory->unit_price = $price;
                    $updateHistoricalInventory->value_movement = $valueMovement;
                    $updateHistoricalInventory->value_inventory = $storehouse->total + $valueMovement;
                    $updateHistoricalInventory->save();

                    $storehouse->stock = $stockFinal;
                    $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                    if ($storehouse->stock == 0 && $storehouse->stock_other_units == 0) $storehouse->total = 0;
                    else $storehouse->total = $storehouse->total + $valueMovement;
                    $storehouse->save();
                }

                // History Inventory Before Origin
                $historicalInventoryOrigin = new HistoricalInventory();
                $historicalInventoryOrigin->folio = $transfer->id_transfer_ee . '-O';
                $historicalInventoryOrigin->date = Carbon::now()->toDateString();
                $historicalInventoryOrigin->hour = Carbon::now()->toTimeString();
                $historicalInventoryOrigin->id_user = \Illuminate\Support\Facades\Auth::id();
                $historicalInventoryOrigin->id_type_movement_inventory = 10;
                $historicalInventoryOrigin->id_product = $product->id;
                $historicalInventoryOrigin->source = $source;
                $historicalInventoryOrigin->destiny = $destiny;
                $historicalInventoryOrigin->quantity = $units;
                $historicalInventoryOrigin->fraction_quantity = $quantityProduct;
                $historicalInventoryOrigin->before_stock = $units;
                $historicalInventoryOrigin->after_stock = $units;
                $historicalInventoryOrigin->fraction_before_stock = $quantityProduct;
                $historicalInventoryOrigin->fraction_after_stock = $quantityProduct;
                $historicalInventoryOrigin->unit_price = $price;
                $historicalInventoryOrigin->value_movement = $valueMovement;
                $historicalInventoryOrigin->value_inventory = $valueMovement;
                $historicalInventoryOrigin->is_destiny = 0;
                $historicalInventoryOrigin->id_profile_job_center = $product->profile_job_center_id;
                $historicalInventoryOrigin->id_company = Auth::user()->companie;
                $historicalInventoryOrigin->save();

                //update storehouse origin
                $storehouseOrigin = storehouse::where('id_employee', $typeSale)
                    ->where('id_product', $product->id)
                    ->first();

                // Calculate stock units
                $stockFinal = $storehouseOrigin->stock_other_units - $quantityProduct;
                $stockFinal = $stockFinal / $product->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);

                $storehouseO = storehouse::find($storehouseOrigin->id);

                // update Historical Inventory Origin
                $updateHistoricalInventoryOrigin = HistoricalInventory::find($historicalInventoryOrigin->id);
                $updateHistoricalInventoryOrigin->before_stock = $storehouseO->stock;
                $updateHistoricalInventoryOrigin->fraction_before_stock = $storehouseO->stock_other_units;
                $updateHistoricalInventoryOrigin->after_stock = $stockFinal;
                $updateHistoricalInventoryOrigin->fraction_after_stock = $storehouseO->stock_other_units - $quantityProduct;
                $updateHistoricalInventoryOrigin->unit_price = $price;
                $updateHistoricalInventoryOrigin->value_movement = $valueMovement;
                $updateHistoricalInventoryOrigin->value_inventory = $storehouseO->total - $valueMovement;
                $updateHistoricalInventoryOrigin->save();

                $storehouseO->stock = $stockFinal;
                $storehouseO->stock_other_units = $storehouseO->stock_other_units - $quantityProduct;
                if ($storehouseO->stock == 0 && $storehouseO->stock_other_units == 0) $storehouseO->total = 0;
                else $storehouseO->total = $storehouseO->total - $valueMovement;
                $storehouseO->save();

                // update Unit Price Sale for backup
                $saleUnitPrice = Sale::find($sale->id);
                $saleUnitPrice->unit_price = $price;
                $saleUnitPrice->save();
            }

            $entryTransfer = transfer_employee_employee::find($transfer->id);
            $entryTransfer->total = $total;
            $entryTransfer->save();

            if ($sale->profile_job_center_id == 268){
                $salePdf = ServiceSale::build($sale->id);
                $pdfSendProvider = PDF::loadView('vendor.adminlte.sales._salesPdf', $salePdf);
                $email = 'cobranza@ecofumigaciones.com.mx';
                $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
                if($result != false){
                    Mail::to($email)->send(new SaleMail($pdfSendProvider->output(), $salePdf));
                }
            }

            DB::commit();
            return Response::json([
                'code' => 201,
                'message' => 'Venta creada correctamente.'
            ]);
        } catch (Exception $exception) {
            DB::rollBack();
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta más tarde.'
            ]);
        }
    }

    public function viewPdfSales($id){
        try {
            $salesId = SecurityController::decodeId($id);
            $salePdf = ServiceSale::build($salesId);
            $pdfSale = PDF::loadView('vendor.adminlte.sales._salesPdf', $salePdf)->setPaper('A4');
            return $pdfSale->stream();
        }
        catch (Exception $e){
            return redirect()->route('index_sales')->with('error', 'Algo salió mal.');
        }
    }

    public function getDiscountById($id)
    {
        try {
            $discount = discount::find($id);

            return Response::json([
                'code' => 200,
                'discount' => $discount
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Error al cargar los datos del descuento.'
            ]);
        }

    }

    public function getProductsForSale($storehouseId)
    {
        try {
            $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
            $products = DB::table('storehouses as sh')
                ->join('products as p', 'sh.id_product', 'p.id')
                ->where('p.profile_job_center_id', $jobCenterSession->id_profile_job_center)
                ->where('p.sale_price', '<>', null)
                ->where('sh.id_employee', $storehouseId)
                ->where('sh.stock', '<>', 0)
                ->where('sh.stock_other_units', '<>', 0)
                ->select('p.id', 'p.name')
                ->orderBy('p.name','asc')
                ->get();

            return Response::json([
                'code' => 200,
                'products' => $products
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Error al cargar los productos, intenta de nuevo.'
            ]);
        }

    }

    public function getDataProductForPurchaseOrder(Request $request)
    {
        try {
            $storehouseId = $request->get('storehouse');
            $productId = $request->get('id');

            $product = DB::table('storehouses as sh')
                ->join('products as p', 'sh.id_product', 'p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->where('p.sale_price', '<>', null)
                ->where('sh.id_employee', $storehouseId)
                ->where('sh.id_product', $productId)
                ->where('sh.stock', '<>', 0)
                ->where('sh.stock_other_units', '<>', 0)
                ->select('p.id', 'p.name', 'p.quantity', 'pu.name as unit', 'p.sale_price', 'sh.stock')
                ->first();

            $taxes = Product_Tax::where('id_product', $productId)
                ->join('taxes as tx', 'product_tax.id_tax', 'tx.id')
                ->orderBy('tx.value', 'desc')
                ->get();

            $taxesLabel = "Sin impuestos";
            $taxesValues = 0;
            if ($taxes->count() > 0) {
                $taxesLabel = "";
                $taxesValues = $taxes;
                foreach ($taxes as $tax) {
                    $taxesLabel = $taxesLabel . $tax->name . ", ";
                }
            }

            return Response::json([
                'code' => 200,
                'product' => $product,
                'taxes' => $taxesLabel,
                'taxesValues' => $taxesValues
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Error al cargar los productos, intenta de nuevo.'
            ]);
        }
    }

    public function sendEmailSale(Request $request){
        try {
            $idSale = SecurityController::decodeId($request->get('idSale'));
            $salePdf = ServiceSale::build($idSale);
            $emails = $request->get('emails');
            $pdfSendProvider = PDF::loadView('vendor.adminlte.sales._salesPdf', $salePdf);
            config(['mail.from.name' => $salePdf['sale']->profile_job_center_name]);

            foreach ($emails as $email) {
                $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
                if($result != false){
                    Mail::to($email)->send(new SaleMail($pdfSendProvider->output(), $salePdf));
                }
            }
            return \response()->json([
                'code' => 201,
                'message' => 'Venta Enviada'
            ]);
        }
        catch (\Exception $e){
            return \response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intentalo de nuevo'
            ]);
        }
    }

    public function paymentStatus(Request $request){
        try {
            $idSale = SecurityController::decodeId($request->get('idSale'));
            $sale = Sale::find($idSale);
            $sale->id_status = $this->STATUS_SALE_PAY;
            $fileVoucher = $request->file('fileVoucher');
            $filename = $fileVoucher->store('sales', 's3');
            $sale->url_file_voucher = $filename;
            $sale->save();

            return \response()->json([
                'code' => 201,
                'message' => 'Pago Registrado'
            ]);
        } catch (\Exception $e){
            return \response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intentalo de nuevo'
            ]);
        }

    }

    public function downloadVoucher($id)
    {
        $sale = Sale::find(SecurityController::decodeId($id));
        return Storage::disk('s3')->response($sale->url_file_voucher);
    }

    public function cancelSale(Request $request){
        try {
            DB::beginTransaction();
            $idSale = $request->get('idSale');
            $idSale = SecurityController::decodeId($idSale);
            $sale = Sale::find($idSale);
            $productsSale = ProductSale::where('id_sale', $idSale)->get();

            foreach ($productsSale as $productSale) {
                $sale = Sale::find($productSale->id_sale);
                $storehouseOrigin = storehouse::where('id_employee', $sale->id_storehouse)
                    ->where('id_product', $productSale->id_product)
                    ->first();
                $product = product::find($productSale->id_product);

                //Employee Origin
                $employeeOrigin = employee::find($sale->id_storehouse);

                //Employee Client Destiny
                $employeeDestiny = employee::where('id_company', $sale->id_company)->where('status', 200)->first();

                // Add Inventory historical Destiny
                $historicalInventoryDestiny = new HistoricalInventory();
                $historicalInventoryDestiny->folio = 'TAC-' . $sale->id_sale . '-D';
                $historicalInventoryDestiny->date = Carbon::now()->toDateString();
                $historicalInventoryDestiny->hour = Carbon::now()->toTimeString();
                $historicalInventoryDestiny->id_user = Auth::user()->id;
                $historicalInventoryDestiny->id_type_movement_inventory = 10;
                $historicalInventoryDestiny->id_product = $product->id;
                $historicalInventoryDestiny->source = $employeeOrigin->name;
                $historicalInventoryDestiny->destiny = $employeeDestiny->name;
                $historicalInventoryDestiny->quantity = $productSale->quantity;
                $historicalInventoryDestiny->fraction_quantity = $product->quantity * $productSale->quantity;
                $historicalInventoryDestiny->before_stock = $productSale->quantity;
                $historicalInventoryDestiny->after_stock =  $productSale->quantity;
                $historicalInventoryDestiny->fraction_before_stock = $product->quantity * $productSale->quantity;
                $historicalInventoryDestiny->fraction_after_stock = $product->quantity * $productSale->quantity;
                $historicalInventoryDestiny->unit_price = $sale->unit_price;
                $historicalInventoryDestiny->value_movement = $sale->unit_price * $product->quantity;
                $historicalInventoryDestiny->value_inventory = $sale->unit_price * $product->quantity;
                $historicalInventoryDestiny->is_destiny = 1;
                $historicalInventoryDestiny->id_profile_job_center = $product->profile_job_center_id;
                $historicalInventoryDestiny->id_company = Auth::user()->companie;
                $historicalInventoryDestiny->save();

                // storeHouse Destiny
                $storeHouseDestiny = storehouse::where('id_employee', $employeeDestiny->id)
                    ->where('id_product', $productSale->id_product)
                    ->first();
                if (empty($storeHouseDestiny)) {
                    $storehouse = new storehouse();
                    $storehouse->id_store = "A-0001";
                    $storehouse->id_employee = $employeeDestiny->id;
                    $storehouse->id_product = $product->id;
                    $storehouse->stock = $productSale->quantity;
                    $storehouse->stock_other_units = $product->quantity * $productSale->quantity;
                    $storehouse->total = $productSale->quantity * $product->base_price;
                    $storehouse->companie = Auth::user()->companie;
                    $storehouse->save();
                    $storehouseId = storehouse::find($storehouse->id);
                    $storehouseId->id_store = "A-" . $storehouse->id;
                    $storehouseId->save();
                }
                else {
                    // Decrement Inventory historical Destiny
                    $updateHistoricalInventoryDestiny = HistoricalInventory::find($historicalInventoryDestiny->id);
                    $updateHistoricalInventoryDestiny->before_stock = $storeHouseDestiny->stock;
                    $updateHistoricalInventoryDestiny->after_stock =  $storeHouseDestiny->stock - $productSale->quantity;
                    $updateHistoricalInventoryDestiny->fraction_before_stock = $storeHouseDestiny->stock_other_units;
                    $updateHistoricalInventoryDestiny->fraction_after_stock = $storeHouseDestiny->stock_other_units - ($product->quantity * $productSale->quantity);
                    $updateHistoricalInventoryDestiny->unit_price = $sale->unit_price;
                    $updateHistoricalInventoryDestiny->value_movement = $sale->unit_price * $product->quantity * $productSale->quantity;
                    $updateHistoricalInventoryDestiny->value_inventory = $storeHouseDestiny->total - ($sale->unit_price * $product->quantity * $productSale->quantity);
                    $updateHistoricalInventoryDestiny->id_profile_job_center = $product->profile_job_center_id;
                    $updateHistoricalInventoryDestiny->id_company = Auth::user()->companie;
                    $updateHistoricalInventoryDestiny->save();

                    //restore Storehouse Destiny
                    $storeHouseDestiny->stock = $storeHouseDestiny->stock - $productSale->quantity;
                    $storeHouseDestiny->stock_other_units = $storeHouseDestiny->stock_other_units - ($product->quantity * $productSale->quantity);
                    if ($storeHouseDestiny->stock == 0 && $storeHouseDestiny->stock_other_units == 0) $storeHouseDestiny->total = 0;
                    else $storeHouseDestiny->total = $storeHouseDestiny->total - ($sale->unit_price * $product->quantity * $productSale->quantity);
                    $storeHouseDestiny->save();
                }

                // Add Inventory historical Origin
                $historicalInventoryOrigin = new HistoricalInventory();
                $historicalInventoryOrigin->folio = 'TAC-' . $sale->id_sale . '-O';
                $historicalInventoryOrigin->date = Carbon::now()->toDateString();
                $historicalInventoryOrigin->hour = Carbon::now()->toTimeString();
                $historicalInventoryOrigin->id_user = Auth::user()->id;
                $historicalInventoryOrigin->id_type_movement_inventory = 10;
                $historicalInventoryOrigin->id_product = $product->id;
                $historicalInventoryOrigin->source = $employeeOrigin->name;
                $historicalInventoryOrigin->destiny = $employeeDestiny->name;
                $historicalInventoryOrigin->quantity = $productSale->quantity;
                $historicalInventoryOrigin->fraction_quantity = $product->quantity * $productSale->quantity;
                $historicalInventoryOrigin->before_stock = $storehouseOrigin->stock;
                $historicalInventoryOrigin->after_stock =  $storehouseOrigin->stock + $productSale->quantity;
                $historicalInventoryOrigin->fraction_before_stock = $storehouseOrigin->stock_other_units;
                $historicalInventoryOrigin->fraction_after_stock = $storehouseOrigin->stock_other_units + ($product->quantity * $productSale->quantity);
                $historicalInventoryOrigin->unit_price = $sale->unit_price;
                $historicalInventoryOrigin->value_movement = $sale->unit_price * $product->quantity * $productSale->quantity;
                $historicalInventoryOrigin->value_inventory = $storehouseOrigin->total + ($sale->unit_price * $product->quantity * $productSale->quantity);
                $historicalInventoryOrigin->is_destiny = 0;
                $historicalInventoryOrigin->id_profile_job_center = $product->profile_job_center_id;
                $historicalInventoryOrigin->id_company = Auth::user()->companie;
                $historicalInventoryOrigin->save();

                //restore Storehouse Origin
                $storehouseOrigin->stock = $storehouseOrigin->stock + $productSale->quantity;
                $storehouseOrigin->stock_other_units = $storehouseOrigin->stock_other_units + ($product->quantity * $productSale->quantity);
                if ($storehouseOrigin->stock == 0 && $storehouseOrigin->stock_other_units == 0) $storehouseOrigin->total = 0;
                else $storehouseOrigin->total = $storehouseOrigin->total + ($sale->unit_price * $product->quantity * $productSale->quantity);
                $storehouseOrigin->save();
            }

            //dd($updateHistoricalInventoryDestiny);


            // Cancel
            $sale->id_status = 12;
            $sale->save();

            DB::commit();
            return Response::json([
                'code' => 201,
                'message' => 'Venta cancelada correctamente.'
            ]);
        }
        catch (Exception $exception) {
            DB::rollBack();
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta más tarde.'
            ]);
        }
    }
}
