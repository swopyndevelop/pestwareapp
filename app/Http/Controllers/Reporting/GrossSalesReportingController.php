<?php

namespace App\Http\Controllers\Reporting;

use App\event;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GrossSalesReportingController extends Controller
{
    public function getAllEventsByFilter(Request $request){

        //options filter
        $employee = $request->get('user_id');
        $jobCenter = $request->get('id_job_center');
        $company = 37;
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');

        $eventsResponse = $this->getAllEvents($employee, $jobCenter, $company, $initialDate, $finalDate);

        return response()->json($eventsResponse);
    }

    public function getAllEvents($employee, $jobCenter, $company, $initialDate, $finalDate) {

        $events = $this->getAllEventsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $eventsForPlague = $this->getAllEventsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $total = $this->getAllEventsModel($employee, $jobCenter, $company, $initialDate, $finalDate)
            ->select(DB::raw('SUM(so.total) as total'))
            ->first()->total;

        $count = $events->count();
        $eventsData = $events->select('ev.id', 'ev.id_service_order', 'so.total', 'ev.title', 'ev.id_status',
            'ev.initial_hour', 'ev.initial_date', 'so.id_service_order as order', 'cu.name as customer',
            'cu.establishment_name as company', 'pl.key', 'pm.name as payment_method', 'ca.payment')
            ->get();

        $sum = $events->select(DB::raw('Date(ev.initial_date) as date'), DB::raw('SUM(so.total) as total'), DB::raw('count(*) as count'))
            ->groupBy(DB::raw('Date(ev.initial_date)'))
            ->get();

        $eventForPlague = $eventsForPlague->select(DB::raw('SUM(so.total) as total'), DB::raw('count(*) as count'), 'pl.key')
            ->groupBy('pl.key')
            ->get();

        $data = new Collection();
        foreach ($sum as $item) {

            foreach ($eventsData as $eventData) {
                if (Carbon::parse($eventData->initial_date)->toDateString() === $item->date) {
                    $data->push($eventData);
                }
            }

            $item->events = $data;
            $data = new Collection();
        }

        return [
            'total' => number_format($total, 2, '.', ','),
            'total_number' => $total,
            'count' => $count,
            'plagueType' => $eventForPlague,
            'groupByDay' => $sum
        ];

    }

    public function getAllEventsModel($employee, $jobCenter, $company, $initialDate, $finalDate) {
        $events = DB::table('events as ev')->where('ev.companie', $company)
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as cu', 'q.id_customer', 'cu.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->join('cashes as ca', 'ca.id_service_order', 'so.id')
            ->join('payment_methods as pm', 'ca.id_payment_method', 'pm.id')
            ->where('ev.id_status', '<>', 1) //1 => Programado
            ->where('ev.id_status', '<>', 3) //3 => Cancelado
            ->whereDate('ev.initial_date', '>=', $initialDate)
            ->whereDate('ev.initial_date', '<=', $finalDate);

        if($jobCenter != 0) {
            $events->where('ev.id_job_center', $jobCenter);
        }elseif ($employee != 0) {
            $events->where('id_employee', $employee);
        }
        return $events;
    }
}
