<?php

namespace App\Http\Controllers\Reporting;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GrossIncomeReportingController extends Controller
{
    public function getAllPaymentsByFilter(Request $request){

        //options filter
        $employee = $request->get('user_id');
        $jobCenter = $request->get('id_job_center');
        $company = 37;
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');

        $paymentsResponse = $this->getAllPayments($employee, $jobCenter, $company, $initialDate, $finalDate);

        return response()->json($paymentsResponse);
    }

    public function getAllPayments($employee, $jobCenter, $company, $initialDate, $finalDate) {

        $cashesTotal = $this->getAllPaymentsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $cashes = $this->getAllPaymentsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $cashesForPlagues = $this->getAllPaymentsModel($employee, $jobCenter, $company, $initialDate, $finalDate);

        $count = $cashes->count();
        $total = $cashesTotal->select(DB::raw('SUM(ca.amount_received) as total'))->first();
        $payments = $cashes->select('ca.id', 'ca.id_service_order', 'ca.amount_received', 'ca.payment',
            'ca.id_payment_method', 'ca.updated_at', 'so.id_service_order as order', 'cu.name as customer',
            'cu.establishment_name as company', 'pl.key', 'so.total')
            ->get();

        $sum = $cashes->select(DB::raw('Date(ca.updated_at) as date'), DB::raw('SUM(ca.amount_received) as total'), DB::raw('count(*) as count'))
            ->groupBy(DB::raw('Date(ca.updated_at)'))
            ->get();

        $cashForPlaguesGroup = $cashesForPlagues->select(DB::raw('SUM(ca.amount_received) as total'), DB::raw('count(*) as count'), 'pl.key')
            ->groupBy('pl.key')
            ->get();

        $data = new Collection();
        foreach ($sum as $item) {

            foreach ($payments as $payment) {
                if (Carbon::parse($payment->updated_at)->toDateString() === $item->date) {
                    $data->push($payment);
                }
            }

            $item->payments = $data;
            $data = new Collection();
        }

        return [
            'total' => number_format($total->total, 2, '.', ','),
            'total_number' => $total->total,
            'count' => $count,
            'plagueType' => $cashForPlaguesGroup,
            'groupByDay' => $sum
        ];
    }

    public function getAllPaymentsModel($employee, $jobCenter, $company, $initialDate, $finalDate) {
        $cashes = DB::table('cashes as ca')->where('ca.companie', $company)
            ->join('events as ev', 'ca.id_event', 'ev.id')
            ->join('service_orders as so', 'ca.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as cu', 'q.id_customer', 'cu.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->where('ca.payment', 2)
            ->whereDate('ca.updated_at', '>=', $initialDate)
            ->whereDate('ca.updated_at', '<=', $finalDate);

        if($jobCenter != 0) {
            $cashes->where('ev.id_job_center', $jobCenter);
        }elseif ($employee != 0) {
            $cashes->where('ev.id_employee', $employee);
        }
        return $cashes;
    }
}
