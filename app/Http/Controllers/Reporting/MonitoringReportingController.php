<?php

namespace App\Http\Controllers\Reporting;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Response;

class MonitoringReportingController extends Controller
{
    private $BAIT_STATION = 4;
    private $CAPTURE_STATION = 5;
    private  $LIGHT_STATION = 6;

    private $NO_INTAKE = 1;
    private $PARTIAL_INTAKE = 2;
    private $TOTAL_INTAKE = 3;
    private $INSECT_INTAKE = 22;

    public function getAllInspectionsByFilter(Request $request)
    {
        $customerId = $request->get('customer_id');
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');
        $companyId = $request->get('company_id');

        if ($customerId == 0) {
            $customers = DB::table('monitorings as m')
                ->join('customers as c', 'm.id_customer', 'c.id')
                ->where('c.companie', $companyId)
                ->select('c.id', 'c.name', 'c.establishment_name')
                ->get();
            $customerId = $customers[0]->id;
        }

        return Response::json($this->getInspectionsByStations($customerId, $initialDate, $finalDate));
    }

    private function getInspectionsByStations($customerId, $initialDate, $finalDate)
    {
        $inspections = $this->getModelInspections($customerId, $initialDate, $finalDate)->get();

        $conditionsBait = $this->getConditionsByStation($customerId, $initialDate, $finalDate, $this->BAIT_STATION);
        $conditionsCapture = $this->getConditionsByStation($customerId, $initialDate, $finalDate, $this->CAPTURE_STATION);
        $conditionsLight = $this->getConditionsByStation($customerId, $initialDate, $finalDate, $this->LIGHT_STATION);

        $consumeBait = $this->getConsumeByStation($customerId, $initialDate, $finalDate, $this->BAIT_STATION);

        $conditionsServices = $this->getConditionsByService($customerId, $initialDate, $finalDate);

        $conditionsBaitTotal = 0;
        $conditionsCaptureTotal = 0;
        $conditionsLightTotal = 0;
        $conditionsServicesTotal = 0;
        foreach ($conditionsBait as $cb) $conditionsBaitTotal += $cb->count;
        foreach ($conditionsCapture as $cc) $conditionsCaptureTotal += $cc->count;
        foreach ($conditionsLight as $cl) $conditionsLightTotal += $cl->count;
        foreach ($inspections as $inspection) {
            $inspections->map(function($inspection){

                $noIntakeBaitResponses = DB::table('check_monitoring_responses as resp')
                    ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
                    ->where('mn.id_type_area', $this->BAIT_STATION)
                    ->where('resp.id_inspection', $inspection->id)
                    ->where('resp.id_check_monitoring_option', $this->NO_INTAKE);

                $partialIntakeBaitResponses = DB::table('check_monitoring_responses as resp')
                    ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
                    ->where('mn.id_type_area', $this->BAIT_STATION)
                    ->where('resp.id_inspection', $inspection->id)
                    ->where('resp.id_check_monitoring_option', $this->PARTIAL_INTAKE);

                $totalIntakeBaitResponses = DB::table('check_monitoring_responses as resp')
                    ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
                    ->where('mn.id_type_area', $this->BAIT_STATION)
                    ->where('resp.id_inspection', $inspection->id)
                    ->where('resp.id_check_monitoring_option', $this->TOTAL_INTAKE);

                $insectIntakeBaitResponses = DB::table('check_monitoring_responses as resp')
                    ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
                    ->where('mn.id_type_area', $this->BAIT_STATION)
                    ->where('resp.id_inspection', $inspection->id)
                    ->where('resp.id_check_monitoring_option', $this->INSECT_INTAKE);

                $captureResponses = DB::table('check_monitoring_responses as resp')
                    ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
                    ->join('plagues_response_inspection as pri', 'resp.id', 'pri.id_station_response')
                    ->join('plague_types as p', 'pri.id_plague', 'p.id')
                    ->where('mn.id_type_area', $this->CAPTURE_STATION)
                    ->where('resp.id_inspection', $inspection->id)
                    ->select(DB::raw('SUM(pri.quantity) as quantity'), DB::raw('count(*) as count'), 'p.name as key')
                    ->groupBy(['p.name'])
                    ->get();

                $lightResponses = DB::table('check_monitoring_responses as resp')
                    ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
                    ->join('plagues_response_inspection as pri', 'resp.id', 'pri.id_station_response')
                    ->join('plague_types as p', 'pri.id_plague', 'p.id')
                    ->where('mn.id_type_area', $this->LIGHT_STATION)
                    ->where('resp.id_inspection', $inspection->id)
                    ->select(DB::raw('SUM(pri.quantity) as quantity'), DB::raw('count(*) as count'), 'p.name as key')
                    ->groupBy(['p.name'])
                    ->get();

                $inspection->noIntakeBaitResponses = $noIntakeBaitResponses->count();
                $inspection->partialIntakeBaitResponses = $partialIntakeBaitResponses->count();
                $inspection->totalIntakeBaitResponses = $totalIntakeBaitResponses->count();
                $inspection->insectIntakeBaitResponses = $insectIntakeBaitResponses->count();
                $inspection->capture_stations = $captureResponses;
                $inspection->light_stations = $lightResponses;
            });
        }
        foreach ($conditionsServices as $cs) $conditionsServicesTotal += $cs->count;

        // Get services by customer and date
        $services = $this->getServicesByCustomerAndDate($initialDate, $finalDate, $customerId);

        // Get plagues with grade infestation
        $plaguesWithGrade = $this->getPlaguesWithGradeInfestation($services);

        return [
            'inspections' => $inspections,
            'bait_conditions' => $conditionsBait,
            'capture_conditions' => $conditionsCapture,
            'light_conditions' => $conditionsLight,
            'consumeBait' => $consumeBait,
            'bait_total' => $this->getTotalStationsByType($this->BAIT_STATION, $customerId),
            'capture_total' => $this->getTotalStationsByType($this->CAPTURE_STATION, $customerId),
            'light_total' => $this->getTotalStationsByType($this->LIGHT_STATION, $customerId),
            'conditionsBaitTotal' => $conditionsBaitTotal,
            'conditionsCaptureTotal' => $conditionsCaptureTotal,
            'conditionsLightTotal' => $conditionsLightTotal,
            'datasets' => $plaguesWithGrade,
            'conditionsServicesTotal' => $conditionsServicesTotal,
            'conditionsServices' => $conditionsServices
        ];
    }

    private function getModelInspections($customerId, $initialDate, $finalDate)
    {
        return DB::table('station_inspections as si')
            ->join('monitorings as m', 'si.id_monitoring', 'm.id')
            ->where('m.id_customer', $customerId)
            ->select('si.id', 'si.id_inspection', 'si.date')
            ->whereDate('si.date', '>=', $initialDate)
            ->whereDate('si.date', '<=', $finalDate);

    }

    private function getConditionsByStation($customerId, $initialDate, $finalDate, $station)
    {
        return DB::table('station_inspections as si')
            ->join('monitorings as m', 'si.id_monitoring', 'm.id')
            ->join('check_monitoring_responses as resp', 'resp.id_inspection', 'si.id')
            ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
            ->join('station_conditions as sc', 'resp.id_station', 'sc.id_node')
            ->join('monitoring_conditions as mc', 'sc.id_condition', 'mc.id')
            ->where('m.id_customer', $customerId)
            ->where('mn.id_type_area', $station)
            ->whereDate('si.date', '>=', $initialDate)
            ->whereDate('si.date', '<=', $finalDate)
            ->select(DB::raw('count(*) as total'), DB::raw('count(*) as count'), 'mc.name as key')
            ->groupBy(['mc.name'])
            ->get();
    }

    private function getConsumeByStation($customerId, $initialDate, $finalDate, $station)
    {
        return DB::table('station_inspections as si')
            ->join('monitorings as m', 'si.id_monitoring', 'm.id')
            ->join('check_monitoring_responses as resp', 'resp.id_inspection', 'si.id')
            ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
            ->where('m.id_customer', $customerId)
            ->where('mn.id_type_area', $station)
            ->whereDate('si.date', '>=', $initialDate)
            ->whereDate('si.date', '<=', $finalDate)
            ->select(DB::raw('count(*) as total'), DB::raw('count(*) as count'), 'resp.value as key')
            ->orderBy('key', 'asc')
            ->groupBy(['resp.value'])
            ->get();
    }

    private function getTotalStationsByType($station, $customerId)
    {
        return DB::table('monitoring_trees as mt')
            ->join('monitorings as m', 'mt.id_monitoring', 'm.id')
            ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
            ->join('type_areas as ta', 'mn.id_type_area', 'ta.id')
            ->where('m.id_customer', $customerId)
            ->where('mn.id_type_area', $station)
            ->select(DB::raw('count(*) as count'))
            ->get()[0]->count;
    }

    // Section Plague Grades --->
    private function getServicesByCustomerAndDate($initialDate, $finalDate, $customerId)
    {
        return DB::table('events as e')
            ->join('service_orders as so', 'e.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->join('place_inspections as pi', 'pi.id_service_order', 'so.id')
            ->select('so.id as id_order', 'pi.id as id_place_inspection', 'e.initial_date as date')
            ->whereDate('e.initial_date', '>=', $initialDate)
            ->whereDate('e.initial_date', '<=', $finalDate)
            ->where('c.id', $customerId)
            ->get();
    }

    private function getPlaguesWithGradeInfestation($services)
    {

        foreach ($services as $service) {

            $services->map(function ($service) {
                $plagues = $this->getPlaguesByService($service->id_place_inspection);
                $service->plagues = $plagues;
            });

        }

        return $services;
    }

    private function getPlaguesByService($id)
    {
        return DB::table('place_inspection_plague_type as pipt')
            ->join('plague_types as p', 'pipt.plague_type_id', 'p.id')
            ->join('infestation_degrees as ind', 'pipt.id_infestation_degree', 'ind.id')
            ->select('p.name as plague', 'ind.name as grade')
            ->where('place_inspection_id', $id)
            ->orderBy('plague', 'asc')
            ->get();
    }

    private function getPlaguesByResponseStation($id, $inspectionId)
    {
        $captureResponses = DB::table('check_monitoring_responses as resp')
            ->join('monitoring_nodes as mn', 'resp.id_station','mn.id_monitoring_tree')
            ->where('mn.id_type_area', $this->CAPTURE_STATION)
            ->where('resp.id_inspection', $inspectionId);

        return DB::table('plagues_response_inspection as pri')
            ->join('plague_types as p', 'pri.id_plague', 'p.id')
            ->select('p.name as plague', 'pri.quantity')
            ->where('pri.id_station_response', $id)
            ->orderBy('p.plague', 'asc')
            ->get();
    }

    private function getConditionsByService($customerId, $initialDate, $finalDate)
    {
        return DB::table('events as e')
            ->join('service_orders as so', 'e.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->join('place_conditions as pc', 'pc.id_service_order', 'so.id')
            ->join('order_cleaning_place_condition as ocpc', 'ocpc.place_condition_id', 'pc.id')
            ->join('order_cleanings as oc', 'ocpc.order_cleaning_id', 'oc.id')
            ->where('c.id', $customerId)
            ->whereDate('e.initial_date', '>=', $initialDate)
            ->whereDate('e.initial_date', '<=', $finalDate)
            ->select(DB::raw('count(*) as total'), DB::raw('count(*) as count'), 'oc.name as key')
            ->groupBy(['oc.name'])
            ->get();
    }
    // End section
}
