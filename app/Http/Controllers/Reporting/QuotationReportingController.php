<?php

namespace App\Http\Controllers\Reporting;

use App\employee;
use App\quotation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuotationReportingController extends Controller
{

    public function getAllQuotationsByFilter(Request $request)
    {

        //options filter
        $employee = $request->get('user_id');
        $jobCenter = $request->get('id_job_center');
        $company = 37;
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');

        $quotationsResponse = $this->getAllQuotations($employee, $jobCenter, $company, $initialDate, $finalDate);

        return response()->json($quotationsResponse);
    }

    public function getAllQuotations($employee, $jobCenter, $company, $initialDate, $finalDate)
    {

        $quotationsTotal = $this->getAllQuotationsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $quotations = $this->getAllQuotationsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $quotForPlagues = $this->getAllQuotationsModel($employee, $jobCenter, $company, $initialDate, $finalDate);

        $count = $quotations->count();
        $quot = $quotations->select('q.id', 'q.id_quotation', 'q.total', 'q.price', 'pl.key',
            'q.created_at', 'cu.name as customer', 'cu.establishment_name as company_customer', 'st.name as status'
            )
            ->get();
        $total = $quotationsTotal->select(DB::raw('SUM(q.total) as total'))->first();

        $sum = $quotations->select(DB::raw('Date(q.created_at) as date'), DB::raw('SUM(q.total) as total'), DB::raw('count(*) as count'))
            ->groupBy(DB::raw('Date(q.created_at)'))
            ->get();

        $quotForPlaguesGroup = $quotForPlagues->select(DB::raw('SUM(q.total) as total'), DB::raw('count(*) as count'), 'pl.key')
            ->groupBy('pl.key')
            ->get();

        $data = new Collection();
        foreach ($sum as $item) {

            foreach ($quot as $quotation) {
                if (Carbon::parse($quotation->created_at)->toDateString() === $item->date) {
                    $data->push($quotation);
                }
            }

            $item->quotations = $data;
            $data = new Collection();
        }

        return [
            'total' => number_format($total->total, 2, '.', ','),
            'total_number' => $total->total,
            'count' => $count,
            'plagueType' => $quotForPlaguesGroup,
            'groupByDay' => $sum
        ];
    }

    public function getAllQuotationsModel($employee, $jobCenter, $company, $initialDate, $finalDate)
    {

        $user_id = employee::find($employee);

        $quotations = DB::table('quotations as q')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->join('customers as cu', 'q.id_customer', 'cu.id')
            ->join('statuses as st', 'q.id_status', 'st.id')
            ->where('q.companie', $company)
            ->whereDate('q.created_at', '>=', $initialDate)
            ->whereDate('q.created_at', '<=', $finalDate);

        if ($jobCenter != 0) {
            $quotations->where('q.id_job_center', $jobCenter);
        } elseif ($employee != 0) {
            $quotations->where('q.user_id', $user_id->employee_id);
        }

        return $quotations;
    }
}
