<?php

namespace App\Http\Controllers\Reporting;

use App\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class TotalCostReportingController extends Controller
{
    //VENTAS TOTALES
    public function total(Request $request)
    {
        $employee = $request->get('user_id');
        $jobCenter = $request->get('id_job_center');
        $company = 37;
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');

        $costsResponse = $this->getTotalCost($employee, $jobCenter, $company, $initialDate, $finalDate);

        return response()->json($costsResponse);
    }

    public function getTotalCost($employee, $jobCenter, $company, $initialDate, $finalDate) {

        $total = 0;
        $total1 = 0;
        $total2 = 0;

        $user_id = employee::find($employee);

        $events = DB::table('events as ev')->where('ev.companie', $company)
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->where('ev.id_status', '<>', 1) //1 => Programado
            ->where('ev.id_status', '<>', 3) //3 => Cancelado
            ->whereDate('ev.initial_date', '>=', $initialDate)
            ->whereDate('ev.initial_date', '<=', $finalDate);

        if($jobCenter != 0) {
            $events->where('ev.id_job_center', $jobCenter);
        }elseif ($employee != 0) {
            $events->where('id_employee', $employee);
        }

        foreach($events->get() as $event) {
            $total1 = $total1 + $event->total;
        }

        $count1 = $events->count();
        $eventsData = $events->select('ev.id', 'ev.id_service_order', 'so.total', 'ev.title', 'ev.id_status', 'ev.initial_hour', 'ev.initial_date')
            ->get();

        $sum1 = $events->select(DB::raw('Date(initial_date) as date'), DB::raw('SUM(total) as total'), DB::raw('count(*) as count'))
            ->groupBy(DB::raw('Date(initial_date)'))
            ->get();

        /*$data1 = new Collection();
        foreach ($sum1 as $item) {

            foreach ($eventsData as $eventData) {
                if (Carbon::parse($eventData->initial_date)->toDateString() === $item->date) {
                    $data1->push($eventData);
                }
            }

            $item->events = $data1;
            $data1 = new Collection();
        }*/



        $expenses = DB::table('expenses')
            ->where('companie',$company)
            ->whereBetween('date_expense', [$initialDate, $finalDate]);

        if($jobCenter != 0){
            $expenses->where('id_jobcenter',$jobCenter);
        }
        if($employee != 0){
            $expenses->where('id_user',$user_id->employee_id);
        }

        foreach($expenses->get() as $expense) {
            $total2 = $total2 + $expense->total;
        }

        $count2 = $expenses->count();
        $expensesData = $expenses->select('id_expense', 'expense_name', 'description_expense', 'total', 'status', 'credit_days', 'date_expense')
            ->get();

        $sum2 = $expenses->select(DB::raw('date_expense as datee'), DB::raw('SUM(total) as total'), DB::raw('count(*) as count'));
            //->groupBy(DB::raw('date_expense'))
            //->get();

        /*$data2 = new Collection();
        foreach ($sum2 as $item) {

            foreach ($expensesData as $expenseData) {
                if (Carbon::parse($expenseData->date_expense)->toDateString() === $item->date) {
                    $data2->push($expenseData);
                }
            }

            $item->expenses = $data2;
            $data2 = new Collection();
        }*/

        //TRASPASOS

        $total3 = 0;
        $tt = 'TAC';

        $transfers = DB::table('transfer_employee_employees')
            ->where('companie', $company)
            ->whereDate('created_at', '>=', $initialDate)
            ->whereDate('created_at', '<=', $finalDate)
            ->where('id_transfer_ee','like','%'.$tt.'%');

        /*if($jobCenter != 0){
            $expenses->where('id_jobcenter',$jobCenter);
        }*/
        if($employee != 0){
            $transfers->where('id_user',$user_id->employee_id);
        }

        foreach($transfers->get() as $transfer) {
            $total3 = $total3 + $transfer->total;
        }

        $count3 = $transfers->count();
        $transfersData = $transfers->select('id_transfer_ee', 'total', 'created_at')
            ->get();

        $sum3 = $transfers->select(DB::raw('Date(created_at) as date'), DB::raw('SUM(total) as total'), DB::raw('count(*) as count'))
            //->union($sum2)
            ->groupBy(DB::raw('Date(date)'))
            ->get();

        $merge = $sum3->merge($sum2);
        //$merge = $merge->merge($sum3);

        /*$data3 = new Collection();
        foreach ($sum3 as $item) {

            foreach ($transfersData as $transferData) {
                if (Carbon::parse($transferData->created_at)->toDateString() === $item->date) {
                    $data3->push($transferData);
                }
            }

            $item->transfers = $data3;
            $data3 = new Collection();
        }*/

        $total4 = 0;

        $total4 = $total2 + $total3;
        $count = $count1 + $count2 + $count3;

        $total = $total1 - $total4;

        $to = 0;
        $summ = $sum1;
        $su =    $sum3;
        $matriz = array();
        $date;
        $countx= 0;
        foreach($summ as $s){
            $date = $s->date;
            foreach($su as $suu){
                if($s->date == $suu->date){
               $to = $s->total - $suu->total;
                $countx = $s->count + $suu->count;
                array_push($matriz, ['date'=>$date, 'total'=>$to, 'count' =>$countx]);
                }
            }
        }

        $sales = $total1;
        $expen = $total2 + $total3;
        if ($sales != 0) $utili = bcdiv($expen, $sales, 2) * 100;
        else $utili = 0;
        $falt = 100 - $utili;

        return [
            'total' => number_format($total, 2, '.', ','),
            'total_number' => $total,
            'count' => $count,
            'plagueType' => [
                [
                    'total' => $utili, 'count' => 1, 'key' => "Porcentaje de Utilidad"
                ],
                [
                    'total' => $falt, 'count' => 1, 'key' => "Faltante"
                ]
            ],
            'groupByDay' => $matriz,
            //'sales' => $summ,
            //'expenses' => $su
        ];

    }
}
