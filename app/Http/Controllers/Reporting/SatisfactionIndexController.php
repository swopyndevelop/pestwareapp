<?php

namespace App\Http\Controllers\Reporting;

use App\employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;

class SatisfactionIndexController extends Controller
{
    public function getAllRatingsByFilter(Request $request)
    {
        //options filter
        $employee = $request->get('user_id');
        $jobCenter = $request->get('id_job_center');
        $company = 37;
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');

        $ratingsResponse = $this->getRatings($employee, $jobCenter, $company, $initialDate, $finalDate);

        return response()->json($ratingsResponse);        

    }

    public function getRatings($employee, $jobCenter, $company, $initialDate, $finalDate)
    {
        $ratings = $this->getAllRatingsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $ratingsTotal = $this->getAllRatingsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $ratingsForGroup = $this->getAllRatingsModel($employee, $jobCenter, $company, $initialDate, $finalDate);
        $rait = $ratings->select('rso.id', 'so.id_service_order as order', 'em.name as employee', 'rso.rating', 'rso.comments', 'ev.initial_date')->get();
        $count = $ratings->count();
        $ratingsSelect = $ratings->select('rso.id', 'rso.rating')->get();
        $rating = $ratingsTotal->select(DB::raw('AVG(rso.rating) as rating'))->first()->rating;

        $sum = $ratings->select(DB::raw('Date(ev.initial_date) as date'), DB::raw('AVG(rso.rating) as total'), DB::raw('count(*) as count'))
            ->groupBy(DB::raw('Date(ev.initial_date)'))
            ->get();

        $ratForGroup = $ratingsForGroup->select(DB::raw('count(*) as count'), 'rso.rating')
            ->groupBy('rso.rating')
            ->get();

        $data = new Collection();
        foreach ($sum as $item) {
            foreach ($rait as $rt) {
                if (Carbon::parse($rt->initial_date)->toDateString() === $item->date)
                    $data->push($rt);
            }
            $item->ratings = $data;
            $data = new Collection();
        }

        return [
            'total' => number_format($rating, 2),
            'total_number' => intval($rating),
            'count' => $count,
            'plagueType' => $ratForGroup,
            'groupByDay' => $sum
        ];

    }

    public function getAllRatingsModel($employee, $jobCenter, $company, $initialDate, $finalDate)
    {

        $user_id = employee::find($employee);

        $ratings = DB::table('ratings_service_orders as rso')
            ->join('events as ev', 'rso.id_event', 'ev.id')
            ->join('profile_job_centers as jc', 'ev.id_job_center', 'jc.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->whereDate('ev.initial_date', '>=', $initialDate)
            ->whereDate('ev.initial_date', '<=', $finalDate);

        if ($jobCenter != 0) $ratings->where('jc.id', $jobCenter);
        elseif ($employee != 0) $ratings->where('ev.id_employee', $employee);

        return $ratings;
    }
}
