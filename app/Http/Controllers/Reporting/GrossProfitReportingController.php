<?php

namespace App\Http\Controllers\Reporting;

use App\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class GrossProfitReportingController extends Controller
{
    public function getExpensesByFilters(Request $request)
    {
        $employee = $request->get('user_id');
        $jobCenter = $request->get('id_job_center');
        $company = 37;
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');

        $expensesResponse = $this->getExpenses($employee, $jobCenter, $company, $initialDate, $finalDate);

        return response()->json($expensesResponse);

    }

    public function getExpenses($employee, $jobCenter, $company, $initialDate, $finalDate) {

        $total1 = 0;
        $user_id = employee::find($employee);
        $expenses = $this->getExpensesModel($employee, $jobCenter, $company, $initialDate, $finalDate, $user_id);
        $expensesForType = $this->getExpensesModel($employee, $jobCenter, $company, $initialDate, $finalDate, $user_id);

        foreach($expenses->get() as $expense) {
            $total1 = $total1 + $expense->total;
        }

        $count1 = $expenses->count();
        $expensesData = $expenses->select('e.id_expense', 'e.expense_name', 'e.description_expense', 'e.total', 'e.status', 'e.credit_days', 'e.date_expense')
            ->get();

        $sum1 = $expenses->select(DB::raw('e.date_expense as date'), DB::raw('SUM(e.total) as total'), DB::raw('count(*) as count'))
            ->groupBy(DB::raw('date_expense'))
            ->get();

        $expensesForTypes = $expensesForType->select(DB::raw('SUM(e.total) as total'), DB::raw('count(*) as count'), 'c.name as key')
            ->groupBy('key')
            ->get();

        /*$data1 = new Collection();
        foreach ($sum1 as $item) {

            foreach ($expensesData as $expenseData) {
                if (Carbon::parse($expenseData->date_expense)->toDateString() === $item->date) {
                    $data1->push($expenseData);
                }
            }

            $item->expenses = $data1;
            $data1 = new Collection();
        }*/

        //TRASPASOS

        $total2 = 0;
        $tt = 'TAC';

        $transfers = DB::table('transfer_employee_employees')
            ->where('companie', $company)
            ->whereDate('created_at', '>=', $initialDate)
            ->whereDate('created_at', '<=', $finalDate)
            //->whereBetween('created_at',[$initialDate,$finalDate])
            ->where('id_transfer_ee','like','%'.$tt.'%');

        //PURCHASE ORDERS

        $total3 = 0;

        $purchaseOrders = DB::table('purchase_orders as po')
            ->join('concepts as c', 'po.id_concept', 'c.id')
            ->where('po.companie', $company)
            ->whereDate('po.created_at', '>=', $initialDate)
            ->whereDate('po.created_at', '<=', $finalDate)
            ->where('c.name', '<>', 'INSUMO');

        $sum3 = $purchaseOrders->select(DB::raw('po.date_order as date'), DB::raw('SUM(po.total) as total'), DB::raw('count(*) as count'))
            ->groupBy(DB::raw('date_order'))
            ->get();

        /*if($jobCenter != 0){
            $expenses->where('id_jobcenter',$jobCenter);
        }*/
        if($employee != 0){
            $transfers->where('id_user',$user_id->employee_id);
        }

        foreach($transfers->get() as $transfer) {
            $total2 = $total2 + $transfer->total;
        }

        foreach($purchaseOrders->get() as $purchase) {
            $total3 = $total3 + $purchase->total;
        }

        $count2 = $transfers->count();
        $expensesForTypes->push(['total' => $total2, 'count' => $count2, 'key' => 'CONSUMO']);
        $expensesForTypes->push(['total' => $total3, 'count' => $count2, 'key' => 'ORDENES DE COMPRA']);
        $transfersData = $transfers->select('id_transfer_ee', 'total', 'created_at')
            ->get();

        $sum2 = $transfers->select(DB::raw('Date(created_at) as date'), DB::raw('SUM(total) as total'), DB::raw('count(*) as count'))
            //->union($sum1)
            ->groupBy(DB::raw('Date(date)'))
            ->get();

        $merge = $sum1->merge($sum2);
        $merge = $merge->merge($sum3);

        $grouped = $merge->groupBy('date');
        $grouped->toArray();
        $groupByDate = new Collection();

        foreach ($grouped as $group) {
            $total = 0;
            $count = 0;
            $date = "";
            foreach ($group as $item) {
                $total += $item->total;
                $count += $item->count;
                $date = $item->date;
            }
            $groupByDate->push([
                'date' => $date,
                'total' => $total,
                'count' => $count
            ]);
        }

        $dataGroup = $groupByDate->sortBy('date')->values()->all();

        /*$data2 = new Collection();
        foreach ($sum2 as $item) {

            foreach ($transfersData as $transferData) {
                if (Carbon::parse($transferData->created_at)->toDateString() === $item->date) {
                    $data2->push($transferData);
                }
            }

            $item->transfers = $data2;
            $data2 = new Collection();
        }*/

        $total = $total1 + $total2 + $total3;
        $count = $count1 + $count2;

        //$sum = $sum1->merge($sum2);

        return [
            'total' => number_format($total, 2, '.', ','),
            'total_number' => $total,
            'count' => $count,
            'plagueType' => $expensesForTypes,
            'groupByDay' => $dataGroup
        ];

    }

    public function getExpensesModel($employee, $jobCenter, $company, $initialDate, $finalDate, $user_id) {

        $expenses = DB::table('expenses as e')
            ->join('concepts as c', 'e.id_concept', 'c.id')
            ->where('e.companie', $company)
            //->whereBetween('date_expense', [$initialDate, $finalDate]);
            ->whereDate('e.date_expense', '>=', $initialDate)
            ->whereDate('e.date_expense', '<=', $finalDate);

        if($jobCenter != 0){
            $expenses->where('e.id_jobcenter',$jobCenter);
        }
        if($employee != 0){
            $expenses->where('e.id_user',$user_id->employee_id);
        }
        return $expenses;
    }
}
