<?php

namespace App\Http\Controllers\Reporting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FiltersReportingController extends Controller
{
    public function getAllJobCenters() {
        $jobCenters = DB::table('profile_job_centers')
            ->where('companie', Auth::user()->companie)
            ->select('id', 'name')
            ->get();
        $defaultItem = collect([
            'id' => 0,
            'name' => 'Todas las sucursales',
        ]);
        $jobCenters->prepend($defaultItem);
        return response()->json($jobCenters);
    }

    public function getAllEmployees() {
        $employees = DB::table('employees')
            ->where('id_company', Auth::user()->companie)
            ->where('status', '<>', 200) // status 200 = Almacen clientes
            ->select('id', 'name')
            ->get();
        $defaultItem = collect([
            'id' => 0,
            'name' => 'Todos los empleados',
        ]);
        $employees->prepend($defaultItem);
        return response()->json($employees);
    }

    public function globalTotal(Request $request) {

        //options filter
        $employee = $request->get('user_id');
        $jobCenter = $request->get('id_job_center');
        $company = $request->get('company');
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');

        $qrc = new QuotationReportingController();
        $quotationsResponse = $qrc->getAllQuotations($employee, $jobCenter, $company, $initialDate, $finalDate);

        $gir = new GrossIncomeReportingController();
        $grossIncomeResponse = $gir->getAllPayments($employee, $jobCenter, $company, $initialDate, $finalDate);

        $gpr = new GrossProfitReportingController();
        $grossProfitResponse = $gpr->getExpenses($employee, $jobCenter, $company, $initialDate, $finalDate);

        $gsr = new GrossSalesReportingController();
        $grossSalesResponse = $gsr->getAllEvents($employee, $jobCenter, $company, $initialDate, $finalDate);

        $tcr = new TotalCostReportingController();
        $totalCostResponse = $tcr->getTotalCost($employee, $jobCenter, $company, $initialDate, $finalDate);

        $rso = new SatisfactionIndexController();
        $ratingResponse = $rso->getRatings($employee, $jobCenter, $company, $initialDate, $finalDate);

        return response()->json([
            'quotationTotal' => $quotationsResponse,
            'grossSalesTotal' => $grossSalesResponse,
            'grossIncomeTotal' => $grossIncomeResponse,
            'grossProfitTotal' => $grossProfitResponse,
            'totalCost' => $totalCostResponse,
            'satisfactionIndex' => $ratingResponse
        ]);

    }

}
