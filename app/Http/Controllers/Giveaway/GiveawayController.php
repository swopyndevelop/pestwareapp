<?php

namespace App\Http\Controllers\Giveaway;

use App\Common\CommonImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class GiveawayController extends Controller
{
    public function index() {
        $bannerOne = CommonImage::getTemporaryUrl('banners/BannerOne.jpg', 5);
        $bannerTwo = CommonImage::getTemporaryUrl('banners/BannerTwo.jpg', 5);
        $bannerThree = CommonImage::getTemporaryUrl('banners/BannerThree.jpg', 5);

        return view('vendor.adminlte.giveaway.index')->with(
        [
            'bannerOne' => $bannerOne,
            'bannerTwo' => $bannerTwo,
            'bannerThree' => $bannerThree,
        ]);
    }
}
