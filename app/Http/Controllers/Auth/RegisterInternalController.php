<?php

namespace App\Http\Controllers\Auth;

use App\CompanyData;
use App\contact;
use App\Country;
use App\Http\Controllers\Instance\InstanceController;
use App\Mail\ConfirmationAccount;
use App\Mail\NewRegisterCompanieMail;
use App\Mail\NotificationMail;
use App\Mail\RecoverPasswordMail;
use App\Mail\RegisterByDemoMail;
use App\Mail\welcomeMail;
use App\Notifications\LogsNotification;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mail;
use App\companie;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

/**
 * Class RegisterController
 * @package %%NAMESPACE%%\Http\Controllers\Auth
 */
class RegisterInternalController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;


    use RegistersUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('adminlte::auth.register');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/register/companie/instance';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function registerInstance(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response()->json([
                'code' => 401,
                'message' => 'Ya existe una cuenta con los datos ingresados.',
                'data' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

        $isFinger = $request->exists('ip');
        $instance = $this->create($request->all(), 1, $isFinger);

        if ($instance['status'] != false) {
            event(new Registered($user = $instance['data']));
            return response()->json([
                'code' => 201,
                'message' => 'Se ha enviado un correo de confirmación a tu cuenta. Para poder continuar primero confirma tu cuenta de correo electrónico.'
            ]);
        }
        else {
            return response()->json([
                'code' => 401,
                'message' => $instance['message']
            ]);
        }

        //$this->guard()->login($instance['data']);
    }

    public function registerInstanceApi(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response()->json([
                'code' => 401,
                'message' => 'Completa los datos correctamente.',
                'data' => $validator->getMessageBag()
            ]);
        }

        //$instance = $this->create($request->all(), 2);
        $instance = $this->registerByDemo($request->all());
        if (!$instance) {
            return response()->json([
                'code' => 401,
                'message' => 'Algo salio mal, intenta de nuevo'
            ]);
        }

        /*if ($instance['status'] != false) event(new Registered($user = $instance['data']));
        else {
            return response()->json([
                'code' => 401,
                'message' => $instance['message']
            ]);
        }*/

        //$this->guard()->login($instance['data']);

        return response()->json([
            'code' => 201,
            'message' => 'ok'
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'sometimes|required|max:255',
            'contact'  => 'required|max:191',
            'email'    => 'required|email|max:191|unique:users',
            'password' => 'required|min:8',
            'phone'    => 'required|min:7|max:14',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @param $type
     * @return array|false
     */
    protected function create(array $data, $type, $isFinger)
    {

        try {

            DB::beginTransaction();

            if ($type == 1) $codeCountry = $data['codeCountry'];
            else $codeCountry = 2;

            $contact = new contact;
            $contact->name = $data['contact'];
            $contact->phone = $data['phone'];
            $contact->email = $data['email'];
            $contact->save();

            $companie = new companie;
            $companie->name = $data['username'];
            $companie->phone = $data['phone'];
            $companie->contact_id = $contact->id;
            $companie->domain_portal = Str::slug($data['username']);
            $companie->id_theme = 1;
            $companie->cover_mip_color = "#1E8CC7";
            $companie->logo = "logos/Ne1f6LZKM2EXX6bFzwMDHQOrUXKnojdngSOTJfDO.png";
            $companie->mini_logo = "mini_logos/vOetciTB7TcFXBbNLwBVIhN6ZqPWFsBJHGwaxOz3.jpeg";
            $companie->pdf_logo = "pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg";
            $companie->pdf_sello = "pdf_sellos/3ZxJOJSgZPm93Ev0VofwAPnJbxEClFHn8vd5hzRF.jpeg";
            $companie->no_employees = 2;
            $companie->no_branch_office = 1;
            $companie->id_plan_center = 4;
            $companie->id_code_country = $codeCountry;
            $companie->reminder_whatsapp = 'Si no estas en condiciones de recibirnos o deseas cambiar la cita por favor avisános, tienes 24 horas para hacer cambios o cancelación ya que de lo contrario se tendrá que pagar el servicio completo para posteriormente reprogramarte. RECUERDA: Al solicitar un servicio con fecha y hora es un contrato de palabra y tenemos el compromiso de reservar este horario solo para ti. En caso de no contestar se considera el servicio confirmado.';
            $companie->save();

            $country = Country::find($codeCountry);
            $data['country'] = $country->name . ' (+' . $country->code_country . ')';

            if ($isFinger) {
                $companyData = new CompanyData;
                $companyData->id_company = $companie->id;
                $companyData->city = $data['city'];
                $companyData->country_code = $data['country_code'];
                $companyData->country_name = $data['country_name'];
                $companyData->ip = $data['ip'];
                $companyData->latitude = $data['latitude'];
                $companyData->longitude = $data['longitude'];
                $companyData->region_code = $data['region_code'];
                $companyData->region_name = $data['region_name'];
                $companyData->time_zone = $data['time_zone'];
                $companyData->zip_code = $data['zip_code'];
                $companyData->save();
            }

            $fields = [
                'name'     => $data['contact'],
                'email'    => $data['email'],
                'companie' => $companie->id,
                'password' => bcrypt($data['password']),
                'id_plan' => $this->PLAN_BUSINESS,
                'confirmation_code' => str_random(50),
            ];
            if (config('auth.providers.users.field','email') === 'username' && isset($data['username'])) {
                $fields['username'] = $data['username'];
            }

            $user = User::create($fields);
            $roleUser = DB::table('role_user')->insert(['user_id' => $user->id, 'role_id' => 2]); //Rol 2 => Cuenta Maestra (Ya tiene asignado todos los permisos).

            $companieUpdate = companie::find($companie->id);
            $companieUpdate->id_company = $companieUpdate->id;
            $companieUpdate->save();

            $instance = InstanceController::createNewInstance($companie->id, $user->id);
            if (!$instance) return ['status' => false, 'message' => 'Error en 141.'];

            //Notify create new instance email
            $emails = ['saul@pestwareapp.com', 'arnold.martinez@pestwareapp.com', 'alberto.martinez@swopyn.com'];
            foreach ($emails as $email) {
                $result = filter_var($email, FILTER_VALIDATE_EMAIL);
                config(['mail.from.name' => 'Pestware App']);
                if($result != false){
                    Mail::to($email)->send(new NewRegisterCompanieMail($data));
                }
            }
            //Welcome customer email
            $email = $data['email'];
            $result = filter_var($email, FILTER_VALIDATE_EMAIL);
            config(['mail.from.name' => 'Pestware App']);
            if($result != false){
                Mail::to($email)->send(new welcomeMail($contact->id, $companie->name, $companie->id, $user->confirmation_code));
            }

            DB::commit();
            return ['status' => true, 'data' => $user];

        } catch (\Exception $exception) {
            DB::rollBack();
            User::first()->notify(new LogsNotification($exception->getMessage(), $exception->getLine(), 3));
            return ['status' => false, 'message' => 'Algo salió mal, intenta de Nuevo'];
        }

    }

    protected function registerByDemo($data)
    {
        try {
            //Notify create new instance email
            $emails = ['saul@pestwareapp.com', 'arnold.martinez@pestwareapp.com', 'alberto.martinez@swopyn.com'];
            foreach ($emails as $email) {
                $result = filter_var($email, FILTER_VALIDATE_EMAIL);
                config(['mail.from.name' => 'Pestware App']);
                if($result != false){
                    Mail::to($email)->send(new NewRegisterCompanieMail($data));
                }
            }
            //Welcome customer email
            $email = $data['email'];
            $result = filter_var($email, FILTER_VALIDATE_EMAIL);
            config(['mail.from.name' => 'Pestware App']);
            if($result != false){
                Mail::to($email)->send(new RegisterByDemoMail($data));
            }
            return true;
        } catch (\Exception $exception) {
            return false;
        }

    }

    public function verify($token) {
        $user = User::where('confirmation_code', $token)->first();

        if (!$user) {
            return redirect('/instance/login/not-verified');
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return redirect('/instance/login/verified');
    }

    public function recoverPasswordMail($email) {
        $user = User::where('email', $email)
            ->where('confirmed', 1)
            ->first();

        if (!$user) {
            return response()->json([
                'code' => 404,
                'message' => 'El correo no existe o aún no ha sido verificado.'
            ]);
        }

        $token = str_random(50);
        $user->confirmation_code = $token;
        $user->save();

        $subject = "Recuperación de contraseña - PestWare App";
        $message = "";
        $result = filter_var($email, FILTER_VALIDATE_EMAIL);
        config(['mail.from.name' => 'Soporte - PestWare App']);
        if($result != false){
            Mail::to($email)->send(new RecoverPasswordMail($subject, $message, $token));
        }
        return response()->json([
            'code' => 200,
            'message' => 'Revisa tu correo electrónico para continuar.'
        ]);
    }

    public function recoverPasswordShowForm() {
        return view('vendor.adminlte.auth.passwords.reset');
    }

    public function recoverPasswordByToken(Request $request) {
        try {
            $token = $request->get('tokenAccount');
            $password = $request->get('passwordNew');
            $passwordConfirm = $request->get('passwordConfirm');

            if ($password !== $passwordConfirm) {
                return \Response::json([
                    'code' => 500,
                    'message' => 'Las contraseñas no coinciden.'
                ]);
            }

            if (strlen($password) < 8) {
                return \Response::json([
                    'code' => 500,
                    'message' => 'La contraseña debe ser mayor a 8 caracteres.'
                ]);
            }

            $user = User::where('confirmation_code', $token)->first();
            $passwordEncrypt = bcrypt($password);

            if (!$user) {
                return \Response::json([
                    'code' => 500,
                    'message' => 'Solicitud no autorizada.'
                ]);
            }

            $user->confirmation_code = null;
            $user->password = $passwordEncrypt;
            $user->save();

            return \Response::json([
                'code' => 200,
                'message' => 'Se cambio la contraseña correctamente.'
            ]);
        }catch (\Exception $exception) {
            return \Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta mas tarde.'
            ]);
        }
    }

    public function reSendEmailConfirm(Request $request) {
        try {
            $email = $request->get('email');
            $user = User::where('email', $email)
                ->where('confirmed', 0)
                ->first();

            if (!$user) {
                return response()->json([
                    'code' => 404,
                    'message' => 'El correo no existe.'
                ]);
            }

            $token = str_random(50);
            $user->confirmation_code = $token;
            $user->save();

            $subject = "Confirmación de correo - PestWare App";
            $message = "";
            $result = filter_var($email, FILTER_VALIDATE_EMAIL);
            config(['mail.from.name' => 'Soporte - PestWare App']);
            if($result != false){
                Mail::to($email)->send(new ConfirmationAccount($subject, $message, $token));
            }
            return response()->json([
                'code' => 200,
                'message' => 'Revisa tu correo electrónico para continuar.'
            ]);
        }catch (\Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
