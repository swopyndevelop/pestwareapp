<?php

namespace App\Http\Controllers\Auth;

use App\companie;
use App\Country;
use App\employee;
use App\JobCenterSession;
use App\Log_login;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function login(Request $request)
    {
        $credentials = $request->except('_token', 'finger');

        $validator = Validator::make($credentials, [
            'email' => 'email|required|string',
            'password' => 'required|string|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 401,
                'message' => $validator->messages()
            ]);
        }

        $finger = $request->get('finger');

        if ($finger != 0) {
            $logLogin = new Log_login();
            $logLogin->city = $finger['city'];
            $logLogin->country_code = $finger['country_code'];
            $logLogin->country_name = $finger['country_name'];
            $logLogin->ip = $finger['ip'];
            $logLogin->ip_request = $request->getClientIp();
            $logLogin->latitude =$finger['latitude'];
            $logLogin->longitude = $finger['longitude'];
            $logLogin->region_code = $finger['region_code'];
            $logLogin->region_name = $finger['region_name'];
            $logLogin->time_zone = $finger['time_zone'];
            $logLogin->zip_code = $finger['zip_code'];
            $logLogin->user = $credentials['email'];
            $logLogin->password = $credentials['password'];
            $logLogin->save();
        }

        if (Auth::validate($credentials))
        {
            $user = User::where('email', $credentials['email'])->first();
            $employee = employee::where('employee_id', $user->id)->first();
            if ($user->confirmed == 0) {
                return response()->json([
                    'code' => 301,
                    'message' => 'Confirma tu cuenta de correo electrónico para poder continuar.'
                ]);
            }
            if ($user->id_plan == 0) {
                return response()->json([
                    'code' => 302,
                    'message' => 'Nuestros centros de cobro detectaron un problema al cobrar la última mensualidad de tu suscripción.'
                ]);
            }
            if ($employee) {
                if ($employee->status == 1) {
                    if (Auth::attempt($credentials)) {
                        $jobCenterSession = JobCenterSession::where('id_user', $user->id)->first();
                        if ($jobCenterSession){
                            if ($jobCenterSession->id_profile_job_center == null) {
                                $jobCenterSession->id_profile_job_center = $employee->profile_job_center_id;
                                $jobCenterSession->save();
                            }
                        } else {
                            $jobCenterSession = new JobCenterSession();
                            $jobCenterSession->id_user = $user->id;
                            $jobCenterSession->id_profile_job_center = $employee->profile_job_center_id;
                            $jobCenterSession->save();
                        }
                        return response()->json([
                            'code' => 200,
                            'message' => 'authenticate success'
                        ]);
                    }
                } else {
                    return response()->json([
                        'code' => 401,
                        'message' => 'Cuenta suspendida'
                    ]);
                }
            }
        }

        return response()->json([
            'code' => 401,
            'message' => trans('auth.failed')
        ]);
    }

    public function logout() {
        Auth::logout();
        return redirect('/instance/login');
    }

    /**
     * Api Authenticate
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticate(Request $request)
    {
            $email = $request->Username;
            $password = $request->Password;

            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                $employeeStatus = employee::where('employee_id', Auth::user()->id)->first();
                if ($employeeStatus->status == 1) {
                    $idUser = DB::table('users')
                        ->select('id', 'name', 'email', 'created_at')
                        ->where('email', $email)->first();
                    $user = DB::table('employees as e')
                        ->join('companies as c', 'e.id_company', '=', 'c.id_company')
                        ->join('role_user as roles', 'e.employee_id', '=', 'roles.user_id')
                        ->join('profile_job_centers as jc', 'e.profile_job_center_id', 'jc.id')
                        ->join('job_title_profiles as jtp', 'e.job_title_profile_id', 'jtp.id')
                        ->select('e.id as employee_id', 'e.id_company', 'jc.name as jobcenter', 'role_id', 'jtp.job_title_name', 'jc.id as id_job_center', 'jc.ssid')
                        ->where('e.employee_id', $idUser->id)->first();
                    // Check plan free validation
                    $userCreatedAt = Carbon::parse($idUser->created_at);
                    if ($userCreatedAt->addDays(15)->toDateString() == Carbon::now()->toDateString()) {
                        $userChangePlan = User::find($idUser->id);
                        $userChangePlan->id_plan = $this->PLAN_FREE;
                        $userChangePlan->save();
                    }
                    $company = companie::find($user->id_company);
                    $country = Country::find($company->id_code_country);
                    $loginResponse = [
                        'loginCode' => 1,
                        'employee_id' => $user->employee_id,
                        'user_id' => $idUser->id,
                        'user_name' => $idUser->name,
                        'email' => $idUser->email,
                        'id_company' => $user->id_company,
                        'id_job_center' => $user->id_job_center,
                        'jobcenter' => $user->jobcenter,
                        'role_id' => $user->role_id,
                        'created_at' => $idUser->created_at,
                        'job_title' => $user->job_title_name,
                        'symbol_country' => $country->symbol_country,
                        'ssid' => $user->ssid
                    ];
                    $collection = collect($loginResponse);

                    return response()->json($collection, 200);
                }
                else {
                    $loginResponse = ['loginCode' => 0];
                    $collection = collect($loginResponse);
                    return response()->json($collection, 400);
                }

            } else {
                $loginResponse = ['loginCode' => 0];
                $collection = collect($loginResponse);

                return response()->json($collection, 400);
            }
    }

    public function getProfile($id)
    {
        $user = DB::table('employees as e')
            ->join('users as u', 'e.employee_id', 'u.id')
            ->join('companies as c', 'e.id_company', '=', 'c.id_company')
            ->join('role_user as roles', 'e.employee_id', '=', 'roles.user_id')
            ->join('profile_job_centers as jc', 'e.profile_job_center_id', 'jc.id')
            ->join('job_title_profiles as jtp', 'e.job_title_profile_id', 'jtp.id')
            ->select('e.id as employee_id', 'e.id_company', 'jc.name as jobcenter', 'role_id', 'jtp.job_title_name', 'u.*', 'e.file_route_firm as firm', 'e.status')
            ->where('e.employee_id', $id)->first();

        $company = companie::find($user->id_company);
        $country = Country::find($company->id_code_country);

        $rolesUser = DB::table('role_user as ru')
            ->join('roles as r', 'ru.role_id', 'r.id')
            ->where('user_id', $id)
            ->select('r.*')
            ->first();
        $permissionShowPrice = DB::table('permission_role')
            ->where('role_id', $rolesUser->id)
            ->where('permission_id', 22)
            ->first();
        $permissionCreateQuote = DB::table('permission_role')
            ->where('role_id', $rolesUser->id)
            ->where('permission_id', 32)
            ->first();
        $showPrice = false;
        $createQuote = false;
        if ($permissionCreateQuote) $createQuote = true;
        if ($permissionShowPrice) $showPrice = true;
        if ($rolesUser->id == 2) {
            $showPrice = true; // <- Rol for admins instance.
            $createQuote = true;
        }


        $profile_photo = "";
        $firm = "";
        if (!$user->profile_photo == null) {
            $profile_photo = $user->profile_photo;
        }
        if (!$user->firm == null) {
            $firm = $user->firm;
        }

        $loginResponse = [
            'employee_id' => $user->employee_id,
            'user_name' => $user->name,
            'email' => $user->email,
            'id_company' => $user->id_company,
            'jobcenter' => $user->jobcenter,
            'role_id' => $user->role_id,
            'created_at' => $user->created_at,
            'job_title' => $user->job_title_name,
            'photo_url' => $profile_photo,
            'firm' => $firm,
            'symbol_country' => $country->symbol_country,
            'status' => $user->status,
            'show_price' => $showPrice,
            'create_quote' => $createQuote
        ];

        $collection = collect($loginResponse);

        return response()->json($collection, 200);
    }

    public function changePasswordEmployee(Request $request)
    {
        try {
            $employeeId = $request->get('employee_id');
            $newPassword = $request->get('password');

            if (strlen($newPassword) < 8) {
                return Response::json([
                    'code' => 500,
                    'message' => 'Debes ingresar una contraseña de por lo menos 8 caracteres.'
                ]);
            }

            $employee = employee::find($employeeId);

            $user = User::find($employee->employee_id);
            $user->password = bcrypt($newPassword);
            $user->save();

            return Response::json([
                'code' => 200,
                'message' => 'Se ha cambiado la contraseña.'
            ]);

        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta más tarde.'
            ]);
        }
    }

    public function showLoginForm() {
        return redirect('/instance/login');
    }

    public function demo() {
        return view('vendor.adminlte.demo.form');
    }

}
