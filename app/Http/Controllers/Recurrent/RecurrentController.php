<?php

namespace App\Http\Controllers\Recurrent;

use App\customer;
use App\discount;
use App\establishment_type;
use App\extra;
use App\JobCenterSession;
use App\plague_type;
use App\quotation;
use App\source_origin;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RecurrentController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;
    //
    public function getRecurrent() {

        try {
            //Validate plan free
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                return response()->json(['response' => 500, 'message' => 'Algo salio mal, intenta de nuevo.']);
            } else {
                // Get all customers
                $customers = customer::all();

                // Get all services by customer && return recurrent
                $servicesCustomer = $this->getRecurrentServices($customers);

                return response()->json(['data' => $servicesCustomer]);
            }

        }catch (\Exception $exception) {
            return response()->json(['response' => 500, 'message' => 'Algo salio mal, intenta de nuevo.']);
        }

    }

    private function getAllServicesByCustomer($customerId) {
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        return DB::table('events as e')
            ->join('service_orders as s', 'e.id_service_order', 's.id')
            ->join('quotations as q', 's.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->select('c.id as client_id', 'c.name', 'c.establishment_name', 'c.cellphone', 'c.colony', 's.id as order',
                'c.municipality', 'cd.address', 'address_number', 'state', 'e.initial_date', 'q.id as quotation_id',
                'q.construction_measure as area', 'q.messenger', 'cd.email', 'q.establishment_id', 'c.source_origin_id',
                'qd.discount_id', 'q.id_extra as extra_id', 'q.price', 'q.total', 'q.id_status as status_quotation')
            ->where('s.id_job_center', $jobCenterSession)
            ->where('e.id_status', 4)
            ->where('q.id', '<>', 2)
            ->where('c.id', $customerId)
            ->get();
    }

    private function getRecurrentServices($customers) {
        $customersCollection = new Collection();
        foreach ($customers as $customer) {
            $servicesCustomer = $this->getAllServicesByCustomer($customer->id);
            $greater = 1000;
            $service = null;
            foreach ($servicesCustomer as $item) {
                $months = $this->calculateMonths($item->initial_date);
                if ($months['data']) {
                    if ($months['days'] < $greater) {
                        $greater = $months['days'];
                        $servicesCustomer->map(function ($serviceCustomer) use ($months) {
                            $serviceCustomer->dateNew = $months['dateNew'];
                            $serviceCustomer->diffInDays = $months['days'];
                            $serviceCustomer->diffInMonths = $months['months'];
                            if ($months['months'] > 6) $serviceCustomer->colorDays = "#b71c1c";
                            else $serviceCustomer->colorDays = "#0277bd";
                            if ($serviceCustomer->establishment_name == null) $serviceCustomer->establishment_name = "";
                            if ($serviceCustomer->status_quotation == 2) {
                                $motive = DB::table('canceled_quotations')->where('id_quotation', $serviceCustomer->id_quotation)->first();
                                $serviceCustomer->motive = $motive->reason . ", " . $motive->commentary;
                                $serviceCustomer->status = "Cancelado";
                                $serviceCustomer->color = "#b71c1c";
                            }else {
                                $serviceCustomer->status = "No Programado";
                                $serviceCustomer->color = "#b71c1c";
                                $serviceCustomer->motive = "";
                            }
                        });
                        $service = $item;
                    }
                }
            }
            if ($service) $customersCollection->push($service);
        }
        return $customersCollection;
    }

    private function calculateMonths($dateService) {
        $dateService = Carbon::parse($dateService);
        $now = Carbon::now();
        $months = $dateService->diffInMonths($now);
        $diffInDays = $dateService->diffInDays($now);
        $dateNew = $dateService->addMonths(6);
        $return = false;
        if ($months >= 6) $return = true;

        return [
            'data' => $return,
            'months' => $months,
            'days' => $diffInDays,
            'dateNew' => $dateNew->format('d-m-Y')
        ];
    }

    public function getDataSelects($idQuotation) {
        $quotation = quotation::find($idQuotation);
        $quotationPlague = $quotation->plague_type;
        $plague = plague_type::where('profile_job_center_id', $quotation->id_job_center)->get();
        $discounts = discount::where('profile_job_center_id', $quotation->id_job_center)->get();
        $extras = extra::where('profile_job_center_id', $quotation->id_job_center)->get();
        $sourceOrigin = source_origin::where('profile_job_center_id', $quotation->id_job_center)->get();
        $establishment = establishment_type::where('profile_job_center_id', $quotation->id_job_center)->get();

        return response()->json(['plagues' => $plague, 'discounts' => $discounts, 'extras' => $extras,
            'sourcesOrigin' => $sourceOrigin, 'establishment' => $establishment, 'quotationPlagues' => $quotationPlague
        ]);
    }
}
