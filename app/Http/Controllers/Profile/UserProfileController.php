<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use DB;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserProfileController extends Controller
{

    public function getProfileByUser(){
        
        try{
            $EmployeeProfile = DB::table('employees as e')
            ->join('profile_job_centers as pjc' , 'e.profile_job_center_id' , 'pjc.id')
            ->select('pjc.name as nameBranch')
            ->where('e.employee_id', Auth::user()->id)
            ->first();

            $employees = DB::table('employees as e')
            ->join('job_title_profiles as jtp' , 'e.job_title_profile_id' , 'jtp.id')
            ->select('jtp.job_title_name as nameJobTitle')
            ->where('e.employee_id', Auth::user()->id)
            ->first();

            return response()->json([
                'code'=> 201,
                'nameJobTitle' => $employees->nameJobTitle, 
                'nameBranch' => $EmployeeProfile->nameBranch
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'code'=> 500,
                'message'=> "Algo ha salido mal"
            ]);
        }
    }

     // Update profile
     public function updateProfile(Request $request){
        try {
            $message = "Datos actualizados.";

            $data = $request->all();
            $requestPassword = $data['password'];
            $requestAchievementImgProfile = $request->file('achievementImgProfile');
            $profile = user::find(Auth::user()->id);
            $profile->name = $data['name'];

            if (!empty($requestPassword)){
                $profile->password = bcrypt($data['password']);
                $message = "La contraseña se ha cambiado";
            }

            if (!empty($requestAchievementImgProfile)){
                $extension = $requestAchievementImgProfile->getClientOriginalExtension();
                $save = Storage::disk('s3p')->put('/profile/photos/' . Auth::user()->id . '.' . $extension, File::get($requestAchievementImgProfile));
                if (!$save) {
                    return response()->json([
                        'code' => 500,
                        'message' => 'No fue posible actualizar la foto de perfil.'
                    ]);
                }
                $profile->profile_photo = "profile/photos/" . Auth::user()->id . '.'. $extension;
                $message = "La imagen de perfil se ha cambiado";
            }
            $profile->save();
            return response()->json([
                'code' => 201,
                'message' => $message
            ]);

        } catch (\Exception $ex ) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
