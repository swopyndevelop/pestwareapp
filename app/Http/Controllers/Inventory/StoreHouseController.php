<?php

namespace App\Http\Controllers\Inventory;

use App\companie;
use App\employee;
use App\entry;
use App\entry_product;
use App\HistoricalInventory;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Catalogs\CommonBranch;
use App\Http\Controllers\Security\SecurityController;
use App\inventory_adjustment;
use App\JobCenterSession;
use App\product;
use App\profile_job_center;
use App\storehouse;
use App\transfer_cc_product;
use App\transfer_ce_product;
use App\transfer_center_center;
use App\transfer_center_employee;
use App\transfer_ec_product;
use App\transfer_ee_product;
use App\transfer_employee_center;
use App\transfer_employee_employee;
use App\treeJobCenter;
use App\TypeMovementInventory;
use App\User;
use Carbon\Carbon;
use Entrust;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;
use Exception;

class StoreHouseController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function index($jobcenter = 0, Request $request)
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        //Export Excel By request
        $company = Auth::user()->companie;
        $queryStoreHouse = storehouse::join('products as p', 'p.id', 'storehouses.id_product', 'p.')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->where('storehouses.companie', $company)->orderBy('p.name');
        if ($request->get('export') == 'true'){
            $storehousesDetail =  $queryStoreHouse->select('storehouses.id','storehouses.id_job_center', 'p.name as product','storehouses.stock',
                'storehouses.stock_other_units', 'pu.name as unit', 'storehouses.total')->get();
        }
        elseif ($request->get('exportEmployee') == 'true') {
            $storehousesDetail = $queryStoreHouse->select('storehouses.id','storehouses.id_employee', 'p.name as product','storehouses.stock',
                'storehouses.stock_other_units', 'pu.name as unit', 'storehouses.total')->get();
        }
        elseif ($request->get('exportGeneral') == 'true') {
            $storehousesDetail = $queryStoreHouse->select('storehouses.id', 'storehouses.id_job_center as centro_de_trabajo','storehouses.id_employee as Empleado', 'p.name as product','storehouses.stock',
                'storehouses.stock_other_units', 'pu.name as unit','storehouses.total')->get();
            foreach ($storehousesDetail as $storehouseDetail) {
                $storehousesDetail->map(function ($storehouseDetail) {
                    if ($storehouseDetail->centro_de_trabajo == null) {
                        $storehouseDetail->centro_de_trabajo = 'NA';
                        $employee = employee::find($storehouseDetail->Empleado);
                        if (!$employee) $storehouseDetail->Empleado = 'NA';
                        else $storehouseDetail->Empleado = $employee->name;
                    }
                    if ($storehouseDetail->Empleado == null) {
                        $storehouseDetail->Empleado = 'NA';
                        $profileJobCenter = profile_job_center::find($storehouseDetail->centro_de_trabajo);
                        if (!$profileJobCenter) $storehouseDetail->centro_de_trabajo = 'NA';
                        else $storehouseDetail->centro_de_trabajo = $profileJobCenter->name;
                    }
                });
            }
        }
        else {
            $storehousesDetail = $queryStoreHouse->select('storehouses.*', 'p.name as product', 'pu.name as unit')->get();
        }

        if ($request->get('export') == 'true') {
            try {
                $storeIdDetail = $request->get('storeIdDetail');
                $exportCollection = $storehousesDetail->where('id_job_center', $storeIdDetail);
                $fileName = 'Detalle-Inventario-Teorico';
                $sheetName = 'Detalle del inventario teorico';
                Excel::create($fileName, function($excel) use ($exportCollection, $sheetName) {
                    $excel->sheet($sheetName, function($sheet) use ($exportCollection) {
                        $sheet->fromArray($exportCollection);
                    });
                })->export('xls');

            }catch (\Exception $exception) {
                return redirect()->route('index_inventory')->with('warning', 'Algo salió mal intente de nuevo.');
            }
        }

        if ($request->get('exportEmployee') == 'true') {
            try {
                $storeIdDetailEmployee = $request->get('storeIdDetailEmployee');
                $exportCollection = $storehousesDetail->where('id_employee', $storeIdDetailEmployee);
                $fileName = 'Detalle-Inventario-Teorico-Empleado';
                $sheetName = 'Inventario Teorico Empleado';
                Excel::create($fileName, function($excel) use ($exportCollection, $sheetName) {
                    $excel->sheet($sheetName, function($sheet) use ($exportCollection) {
                        $sheet->fromArray($exportCollection);
                    });
                })->export('xls');

            }catch (\Exception $exception) {
                return redirect()->route('index_inventory')->with('warning', 'Algo salió mal intente de nuevo.');
            }
        }

        if ($request->get('exportGeneral') == 'true') {
            try {
                $exportCollection = $storehousesDetail;
                $nameCompany = companie::find($company)->name;
                $fileName = 'Inventario-Teorico-' . $nameCompany;
                $sheetName = 'Inventario teorico';
                Excel::create($fileName, function($excel) use ($exportCollection, $sheetName) {
                    $excel->sheet($sheetName, function($sheet) use ($exportCollection) {
                        $sheet->fromArray($exportCollection);
                    });
                })->export('xls');

            }catch (\Exception $exception) {
                return redirect()->route('index_inventory')->with('warning', 'Algo salió mal intente de nuevo.');
            }
        }

        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $products = product::where('id_companie', Auth::user()->companie)->get();
        $employees = employee::join('users as u','u.id','employees.employee_id')
            ->where('id_company', Auth::user()->companie)
            ->select('u.id','u.name')
            ->get();
        if ($treeJobCenterMain->parent != '#') {
            $employees = employee::join('users as u','u.id','employees.employee_id')
                ->where('profile_job_center_id', $employee->profile_job_center_id)
                ->select('u.id','u.name')
                ->get();
            $products = product::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        }
        // Filters
        $typeMovements = TypeMovementInventory::all();

        // Export Excel Inventory
        $historicalJobCenter = $request->get('idDetailHistoricalJobCenter');
        $historicalEmployee = $request->get('idDetailHistoricalEmployee');
        $historicalProduct = $request->get('idDetailHistoricalProduct');
        $historicalCompany = $request->get('idDetailHistoricalCompany');

        $historicalInventory = HistoricalInventory::join('products as p', 'p.id', 'historical_inventories.id_product')
        ->join('product_units as pu','pu.id','p.id_unit')
        ->join('users as u', 'u.id', 'historical_inventories.id_user')
        ->join('type_movements_inventories as tmi', 'tmi.id', 'historical_inventories.id_type_movement_inventory')
        ->select('historical_inventories.folio', 'historical_inventories.date as Fecha', 'historical_inventories.hour as Hora',
            'u.name as Usuario', 'tmi.name as Movimiento', 'p.name as Producto', 'pu.name as Unidades', 'historical_inventories.source as Origen',
            'historical_inventories.destiny as Destino', 'historical_inventories.quantity as Unidad', 'historical_inventories.fraction_quantity as Cantidad',
            'historical_inventories.before_stock as AntesU', 'historical_inventories.after_stock as DespuesU', 'historical_inventories.fraction_before_stock as AntesC',
            'historical_inventories.fraction_after_stock as DespuesC', 'historical_inventories.unit_price as PrecioUnitario',
            'historical_inventories.value_movement as ValorMovimiento', 'historical_inventories.value_inventory as ValorInventario');

        if ($historicalJobCenter != null) {
            try {
                $name = profile_job_center::find($historicalJobCenter)->name;
                $exportCollection = $historicalInventory->where('historical_inventories.destiny', $name)
                    ->orWhere('historical_inventories.source', $name)
                    ->get();
                $fileName = 'Inventario-Historico-' . $name;
                $sheetName = 'Histórico';
                Excel::create($fileName, function($excel) use ($exportCollection, $sheetName) {
                    $excel->sheet($sheetName, function($sheet) use ($exportCollection) {
                        $sheet->fromArray($exportCollection);
                    });
                })->export('xls');

            }catch (\Exception $exception) {
                return redirect()->route('index_inventory')->with('warning', 'Algo salió mal intente de nuevo.');
            }

        }

        if ($historicalEmployee != null) {
            try {
                $employeeName = employee::find($historicalEmployee)->name;
                $exportCollection = $historicalInventory->where('historical_inventories.destiny', $employeeName)
                    ->orWhere('historical_inventories.source', $employeeName)
                    ->get();
                $fileName = 'Inventario-Historico-' . $employeeName;
                $sheetName = 'Histórico';
                Excel::create($fileName, function($excel) use ($exportCollection, $sheetName) {
                    $excel->sheet($sheetName, function($sheet) use ($exportCollection) {
                        $sheet->fromArray($exportCollection);
                    });
                })->export('xls');

            }catch (\Exception $exception) {
                return redirect()->route('index_inventory')->with('warning', 'Algo salió mal intente de nuevo.');
            }

        }

        if ($historicalProduct != null) {
            try {
                $nameProduct = product::find($historicalProduct)->name;
                $exportCollection = $historicalInventory->where('historical_inventories.id_product', $historicalProduct)->get();
                $fileName = 'Inventario-Historico-' . $nameProduct;
                $sheetName = 'Histórico';
                Excel::create($fileName, function($excel) use ($exportCollection, $sheetName) {
                    $excel->sheet($sheetName, function($sheet) use ($exportCollection) {
                        $sheet->fromArray($exportCollection);
                    });
                })->export('xls');

            }catch (\Exception $exception) {
                return redirect()->route('index_inventory')->with('warning', 'Algo salió mal intente de nuevo.');
            }

        }

        if ($historicalCompany != null) {
            try {
                $company = companie::find($historicalCompany);
                $exportCollection = $historicalInventory->where('historical_inventories.id_company', $company->id)->get();
                $fileName = 'Inventario-Historico-' . $company->name;
                $sheetName = 'Histórico';
                Excel::create($fileName, function($excel) use ($exportCollection, $sheetName) {
                    $excel->sheet($sheetName, function($sheet) use ($exportCollection) {
                        $sheet->fromArray($exportCollection);
                    });
                })->export('xls');

            }catch (\Exception $exception) {
                return redirect()->route('index_inventory')->with('warning', 'Algo salió mal intente de nuevo.');
            }

        }

        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $symbol_country = CommonCompany::getSymbolByCountry();
    	return view('vendor.adminlte.inventory.index')
            ->with([
                'storehousesDetail' => $storehousesDetail,
                'employees' => $employees,
                'typeMovements' => $typeMovements,
                'products' => $products,
                'symbol_country' => $symbol_country,
                'jobCenterSession' => $dataJobCenter[0],
                'jobCenters' => $dataJobCenter[1],
            ]);
    }

    public function getDataJobCenters(Request $request){
        try {
            $filterNameInventory = $request->get('filterNameInventory');
            $initialDateInventory = $request->get('initialDateInventory');
            $finalDateInventory = $request->get('finalDateInventory');
            $idJobCenterInventory = $request->get('idJobCenterInventory');
            $dataJobCenter = CommonBranch::getBranchByJobCenter($idJobCenterInventory);

            $employee = employee::where('employee_id', Auth::user()->id)
                ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
                ->first();
            $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
            $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

            $jobCenters = DB::table('profile_job_centers')
                ->where('companie', Auth::user()->companie)
                ->where('id', $dataJobCenter[2]);
            $employees = DB::table('employees')
                ->where('id_company', Auth::user()->companie)
                ->where('profile_job_center_id', $dataJobCenter[2]);

            $products = product::where('profile_job_center_id', $dataJobCenter[2])->get();

            if ($treeJobCenterMain->parent != '#') {
                $employees = DB::table('employees')->where('profile_job_center_id', $employee->profile_job_center_id);
                $jobCenters = DB::table('profile_job_centers')->where('id', $employee->profile_job_center_id);
            }

            if ($filterNameInventory != 'null') $jobCenters->where('name', 'like', '%'. $filterNameInventory .'%');
            if ($initialDateInventory != null && $finalDateInventory != null) {
                $jobCenters
                    ->whereDate('created_at', '>=', $initialDateInventory)
                    ->whereDate('created_at', '<=', $finalDateInventory);
            }
            $jobCentersFilter = $jobCenters->orderBy('name')->get();

            $collectionJobCenters = collect();
            foreach ($jobCentersFilter as $jb) {
                $storehouses = storehouse::where('id_job_center', $jb->id)->first();
                $units = DB::table('storehouses')->select(DB::raw('SUM(stock) as total_units'))->groupBy('id_job_center')->where('id_job_center', $jb->id)->first();
                $total = DB::table('storehouses')->select(DB::raw('SUM(total) as total'))->groupBy('id_job_center')->where('id_job_center', $jb->id)->first();
                $date = DB::table('storehouses')->select('updated_at as date')->where('id_job_center', $jb->id)->orderby('updated_at','DESC')->take(1)->first();
                if (!empty($storehouses)) {
                    $collectionJobCenters->push([
                        'name' => $jb->name,
                        'total_units' => $units->total_units,
                        'total' => $total->total,
                        'date' => $date->date,
                        'id_job_center' => $jb->id,
                        'id_job_center_encrypt' => SecurityController::encodeId($jb->id),
                        'id_employee' => 0,
                    ]);
                }
            }

            if ($filterNameInventory != 'null') $employees->where('name', 'like', '%'. $filterNameInventory .'%');
            if ($initialDateInventory != null && $finalDateInventory != null) {
                $employees
                    ->whereDate('created_at', '>=', $initialDateInventory)
                    ->whereDate('created_at', '<=', $finalDateInventory);
            }
            $employeesFilter = $employees->orderBy('name')->get();

            $collectionEmployees = collect();
            foreach ($employeesFilter as $emp) {
                $storehouses = storehouse::where('id_employee', $emp->id)->first();
                $units = DB::table('storehouses')->select(DB::raw('SUM(stock) as total_units'))->groupBy('id_employee')->where('id_employee', $emp->id)->first();
                $total = DB::table('storehouses')->select(DB::raw('SUM(total) as total'))->groupBy('id_employee')->where('id_employee', $emp->id)->first();
                $date = DB::table('storehouses')->select('updated_at as date')
                    ->where('id_employee', $emp->id)->orderby('updated_at','DESC')->take(1)->first();
                if (!empty($storehouses)) {
                    $collectionEmployees->push([
                        'name' => $emp->name,
                        'total_units' => $units->total_units,
                        'total' => $total->total,
                        'date' => $date->date,
                        'id_employee' => $emp->id,
                        'id_employee_encrypt' =>  SecurityController::encodeId($emp->id),
                        'id_job_center' => 0,
                    ]);
                }
            }

            $collectionData = $collectionJobCenters->merge($collectionEmployees);

            $symbol_country = CommonCompany::getSymbolByCountry();

            return response()->json([
                'code' => 200,
                'collectionData' => $collectionData,
                'products' => $products,
                'employees' => $employees->get(),
                'symbol_country' => $symbol_country
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataViewDetailStoreHouse($idJobCenter){
        try {
            $company = auth()->user()->companie;
            $profileJobCenter = profile_job_center::find($idJobCenter);
            $queryStoreHouse = storehouse::join('products as p', 'p.id', 'storehouses.id_product', 'p.')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->where('storehouses.companie', $company)
                ->where('storehouses.stock', '>=', 1)
                ->where('id_job_center', $idJobCenter);
            $storehousesDetail = $queryStoreHouse->select('storehouses.*', 'p.name as product', 'pu.name as unit',
                'p.id as id_product', 'p.base_price')
                ->orderBy('p.name')
                ->get();
            $stock = 0;
            $total = 0;
            foreach ($storehousesDetail as $storehouseDetail){
                $stock += $storehouseDetail->stock;
                $total += $storehouseDetail->total;
            }

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');
            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'storehousesDetail' => $storehousesDetail,
                'profileJobCenter' => $profileJobCenter,
                'stock' => $stock,
                'total' => $total,
                'deleteData' => $deleteData,
                'master' => $master,
                'symbol_country' => $symbol_country
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataStoreHouse($idStoreHouse){
        try {
            $storeHouse = storehouse::join('products as p', 'storehouses.id_product','p.id')
                ->join('profile_job_centers as pjc','storehouses.id_job_center','pjc.id')
                ->join('product_units as pu','p.id_unit','pu.id')
                ->select('storehouses.*','p.name','pjc.name as name_job_center','pu.name as name_unit')
                ->where('storehouses.id', $idStoreHouse)
                ->first();

            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'storeHouse' => $storeHouse,
                'symbol_country' => $symbol_country
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataStoreHouseEmployee($idStoreHouseEmployee){
        try {
            $storeHouse = storehouse::join('products as p', 'storehouses.id_product','p.id')
                ->join('employees as e','storehouses.id_employee','e.id')
                ->join('product_units as pu','p.id_unit','pu.id')
                ->select('storehouses.*','p.name','e.name as name_employee','pu.name as name_unit')
                ->where('storehouses.id', $idStoreHouseEmployee)
                ->first();

            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'storeHouse' => $storeHouse,
                'symbol_country' => $symbol_country
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataViewDetailStoreHouseEmployee($idEmployee){
        try {
            $company = auth()->user()->companie;
            $employee = employee::find($idEmployee);
            $queryStoreHouse = storehouse::join('products as p', 'p.id', 'storehouses.id_product', 'p.')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->where('storehouses.companie', $company)
                ->where('storehouses.stock', '>=', 1)
                ->where('id_employee', $idEmployee);
            $storehousesDetail = $queryStoreHouse->select('storehouses.*', 'p.name as product', 'pu.name as unit',
                'p.id as id_product', 'p.base_price')
                ->orderBy('p.name')
                ->get();

            $stock = 0;
            $total = 0;
            foreach ($storehousesDetail as $storehouseDetail){
                $stock += $storehouseDetail->stock;
                $total += $storehouseDetail->total;
            }

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');
            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'storehousesDetail' => $storehousesDetail,
                'employee' => $employee,
                'stock' => $stock,
                'total' => $total,
                'deleteData' => $deleteData,
                'master' => $master,
                'symbol_country' => $symbol_country
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function showMovementsByStorehouse($id, Request $request)
    {
        //$id = SecurityController::decodeId($id);

        $folio = $request->get('folioMovementStoreHouse');
        $source = $request->get('nameSourceMovementStoreHouse');
        $destiny = $request->get('nameDestinyMovementStoreHouse');
        $initialDate = $request->get('initialDateMovementStoreHouse');
        $finalDate = $request->get('finalDateMovementStoreHouse');

        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) SecurityController::abort();

        $profile_job_centers_id = profile_job_center::find($id);
        // Middleware to deny access to data from another company
        if ($profile_job_centers_id->companie != auth()->user()->companie) SecurityController::abort();

        // Obtiene el nombre del alamacen
        $nameStorehouse = DB::table('profile_job_centers')->where('id', $id)->first()->name;

        // Obtiene todas las entradas de la sucursal por Id.
        $entry = DB::table('entrys as e')->join('users as u','e.id_user','u.id')
            ->join('profile_job_centers as pjc', 'e.id_job_center', 'pjc.id')
            ->select('e.id','e.id_entry','e.origin','e.id_job_center','e.id_user','e.total','e.created_at as date','u.name as username', 'pjc.name as destiny')
            ->where('e.companie', Auth::user()->companie)
            ->where('e.id_job_center', $id)
            ->get();
        foreach ($entry as $ent) {
            $entry->map(function($ent){
                $unidades = DB::table('entry_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_entry')->where('id_entry', $ent->id)->first();
                $ent->tot_unit = $unidades->tot_unit;
                $ent->type = "entry";
            });
        }

        /*$productsEntry = DB::table('entry_products as e')->join('products as p','e.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('e.id as e_p','e.id_entry','e.id_product','e.units','e.price','p.name','p.quantity','p.base_price', 'pu.name as unit')
            ->where('p.id_companie', Auth::user()->companie)
            ->get();*/

        $factura = DB::table('facture_entrys as fe')->join('entrys as e','fe.id_entry','e.id')
            ->where('e.companie', Auth::user()->companie)
            ->select('fe.id','fe.id_entry', 'fe.file_route')->get();

        // Obtiene todos los traspasos de la sucursal por Id.
        $transfer_cc = DB::table('transfer_center_centers as tc')->join('users as u','tc.id_user','u.id')
            ->join('profile_job_centers as pj','tc.id_job_center_origin','pj.id')
            ->join('profile_job_centers as pjc','tc.id_job_center_destiny','pjc.id')
            ->select('tc.id','tc.id_transfer_cc','tc.id_user','pj.name as origin','pjc.name as destiny','u.name as username',
                'tc.total', 'tc.created_at as date')
            ->where('tc.id_job_center_origin', $id)
            ->orWhere('tc.id_job_center_destiny', $id)
            ->get();
        foreach ($transfer_cc as $tcc) {
            $transfer_cc->map(function($tcc){
                $unidades = DB::table('transfer_cc_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_cc')->where('id_transfer_cc', $tcc->id)->first();
                $tcc->tot_unit = $unidades->tot_unit;
                $tcc->type = "tcc";

            });
        }

      /*  $productsTransferCC = DB::table('transfer_cc_products as e')
            ->join('products as p','e.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('e.id as e_p','e.id_transfer_cc','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit' , 'e.is_units')
            ->where('p.id_companie', Auth::user()->companie)
            ->get();*/

        $transfer_ce = DB::table('transfer_center_employees as te')->join('users as u','te.id_user','u.id')
            ->join('profile_job_centers as pj','te.id_job_center_origin','pj.id')
            ->join('employees as e','te.id_employee_destiny','e.id')
            ->select('te.id','te.id_transfer_ce','te.id_user','pj.name as origin','e.name as destiny','u.name as username',
                'te.total', 'te.created_at as date')
            ->where('te.id_job_center_origin', $id)
            ->get();
        foreach ($transfer_ce as $tce) {
            $transfer_ce->map(function($tce){
                $unidades = DB::table('transfer_ce_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ce')->where('id_transfer_ce', $tce->id)->first();
                $tce->tot_unit = $unidades->tot_unit;
                $tce->type = "tce";

            });
        }

      /*  $productsTransferCE = DB::table('transfer_ce_products as e')
            ->join('products as p','e.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('e.id as e_p','e.id_transfer_ce','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
            ->where('p.id_companie', Auth::user()->companie)
            ->get();*/

        $transfer_ec = DB::table('transfer_employee_centers as tce')->join('users as u','tce.id_user','u.id')
            ->join('profile_job_centers as pj','tce.id_job_center_destiny','pj.id')
            ->join('employees as e','tce.id_employee_origin','e.id')
            ->select('tce.id','tce.id_transfer_ec','tce.id_user','pj.name as destiny','e.name as origin','u.name as username',
                'tce.total', 'tce.created_at as date')
            ->where('tce.id_job_center_destiny', $id)
            ->get();
        foreach ($transfer_ec as $tec) {
            $transfer_ec->map(function($tec){
                $unidades = DB::table('transfer_ec_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ec')->where('id_transfer_ec', $tec->id)->first();
                $tec->tot_unit = $unidades->tot_unit;
                $tec->type = "tec";

            });
        }

     /*   $productsTransferEC = DB::table('transfer_ec_products as e')
            ->join('products as p','e.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('e.id as e_p','e.id_transfer_ec','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
            ->where('p.id_companie', Auth::user()->companie)
            ->get();*/

        $adjustments = DB::table('inventory_adjustments as ia')
            ->join('storehouses as s', 'ia.id_storehouse', 's.id')
            ->join('profile_job_centers as pj','s.id_job_center','pj.id')
            ->join('users as u', 'ia.id_user', 'u.id')
            ->join('products as p', 's.id_product', 'p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('ia.id', 'ia.id_adjustment', 'ia.id_user', 'pj.name as origin', 'pj.name as destiny', 'u.name as username',
                'ia.total_adjustment as total', 'ia.created_at as date', 'ia.units_adjustment as tot_unit', 'ia.comments as type',
                'p.name as product', 'p.quantity', 'pu.name as unit', 'p.base_price', 'ia.units_before', 'ia.quantity_before', 'ia.total_before',
                'ia.units_adjustment', 'ia.quantity_adjustment', 'ia.total_adjustment')
            ->where('s.id_job_center', $id)
            ->get();

        $movementsCollection = collect();
        $movements = $transfer_cc->merge($transfer_ce);
        $movements = $movements->merge($transfer_ec);
        $movements = $movements->merge($adjustments);
        $movements = $movements->merge($entry);
        $movements = $movements->sortBy('date', SORT_REGULAR, true);
        $movementsCollection = $movements->merge($movementsCollection);

        foreach ($movementsCollection as $movementCollection) {
            $movementsCollection->map(function($movementCollection){
                $movementCollection->idMovement = 0;
                if ($movementCollection->type == 'entry') $movementCollection->idMovement = $movementCollection->id_entry;
                if ($movementCollection->type == 'tcc') $movementCollection->idMovement = $movementCollection->id_transfer_cc;
                if ($movementCollection->type == 'tce') $movementCollection->idMovement = $movementCollection->id_transfer_ce;
                if ($movementCollection->type == 'tec') $movementCollection->idMovement = $movementCollection->id_transfer_ec;
                if ($movementCollection->type != 'entry' && $movementCollection->type != 'tcc' && $movementCollection->type != 'tce' && $movementCollection->type != 'tec') {
                    $movementCollection->idMovement = $movementCollection->id_adjustment;
                }
                $date = explode(' ',$movementCollection->date);
                $movementCollection->dateCarbon = $date[0];
            });
        }

        $filtered = $movementsCollection;
        if ($folio == null && $source == null && $destiny == null && $initialDate == null && $finalDate == null)
            $filtered = $movementsCollection->filter(function ($value) {
                return $value->dateCarbon >= Carbon::now()->toDateString() && $value->dateCarbon <= Carbon::now()->toDateString();
            });

        $symbol_country = CommonCompany::getSymbolByCountry();

       return response()->json([
            'code' => 200,
            'movements' => $filtered,
            //'productsTransferEC' => $productsTransferEC,
            //'productsTransferCC' => $productsTransferCC,
            //'productsTransferCE' => $productsTransferCE,
            'nameStorehouse' => $nameStorehouse,
            //'productsEntry' => $productsEntry,
            'factura' => $factura,
            'symbol_country' => $symbol_country,
           'folio' => $folio,
           'source' => $source,
           'destiny' => $destiny,
           'initialDate' => $initialDate,
           'finalDate' => $finalDate
        ]);

        /*return view('vendor.adminlte.inventory.jobcenter._movements')->with(['movements' => $movements, 'productsTransferEC' => $productsTransferEC,
            'productsTransferCC' => $productsTransferCC, 'productsTransferCE' => $productsTransferCE, 'nameStorehouse' => $nameStorehouse,
            'productsEntry' => $productsEntry, 'factura' => $factura
        ]);*/
    }

    public function showMovementsByEmployee($id, Request $request)
    {
        $folio = $request->get('folioMovementStoreHouse');
        $source = $request->get('nameSourceMovementStoreHouse');
        $destiny = $request->get('nameDestinyMovementStoreHouse');
        $initialDate = $request->get('initialDateMovementStoreHouse');
        $finalDate = $request->get('finalDateMovementStoreHouse');

        //$id = SecurityController::decodeId($id);
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $employee_id = employee::find($id);
        // Middleware to deny access to data from another company
        if ($employee_id->id_company != auth()->user()->companie) {
            SecurityController::abort();
        }

        // Obtiene el nombre del alamacen
        $nameStorehouse = DB::table('employees')->where('id', $id)->first();

        // Obtiene todos los traspasos del almacen por Id.
        $transfer_ee = DB::table('transfer_employee_employees as t')->join('users as u','t.id_user','u.id')
            ->join('employees as e1','t.id_employee_origin','e1.id')
            ->join('employees as e2','t.id_employee_destiny','e2.id')
            ->select('t.id','t.id_transfer_ee','t.id_user','e1.name as origin','e2.name as destiny','u.name as username',
                't.total', 't.created_at as date')
            ->where('t.id_employee_origin', $id)
            ->orWhere('t.id_employee_destiny', $id)
            ->get();

        foreach ($transfer_ee as $tee) {
            $transfer_ee->map(function($tee){
                $unidades = DB::table('transfer_ee_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ee')->where('id_transfer_ee', $tee->id)->first();
                $tee->tot_unit = $unidades->tot_unit;
                $tee->type = "tee";

            });
        }

        /*$productsTransferEE = DB::table('transfer_ee_products as e')
            ->join('products as p','e.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('e.id as e_p','e.id_transfer_ee','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
            ->where('p.id_companie', Auth::user()->companie)
            ->get();*/

        $transfer_ce = DB::table('transfer_center_employees as te')->join('users as u','te.id_user','u.id')
            ->join('profile_job_centers as pj','te.id_job_center_origin','pj.id')
            ->join('employees as e','te.id_employee_destiny','e.id')
            ->select('te.id','te.id_transfer_ce','te.id_user','pj.name as origin','e.name as destiny','u.name as username',
                'te.total', 'te.created_at as date')
            ->where('te.id_employee_destiny', $id)
            ->get();
        foreach ($transfer_ce as $tce) {
            $transfer_ce->map(function($tce){
                $unidades = DB::table('transfer_ce_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ce')->where('id_transfer_ce', $tce->id)->first();
                $tce->tot_unit = $unidades->tot_unit;
                $tce->type = "tce";

            });
        }

       /* $productsTransferCE = DB::table('transfer_ce_products as e')
            ->join('products as p','e.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('e.id as e_p','e.id_transfer_ce','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
            ->where('p.id_companie', Auth::user()->companie)
            ->get();*/

        $transfer_ec = DB::table('transfer_employee_centers as tce')->join('users as u','tce.id_user','u.id')
            ->join('profile_job_centers as pj','tce.id_job_center_destiny','pj.id')
            ->join('employees as e','tce.id_employee_origin','e.id')
            ->select('tce.id','tce.id_transfer_ec','tce.id_user','pj.name as destiny','e.name as origin','u.name as username',
                'tce.total', 'tce.created_at as date')
            ->where('tce.id_employee_origin', $id)
            ->get();

        foreach ($transfer_ec as $tec) {
            $transfer_ec->map(function($tec){
                $unidades = DB::table('transfer_ec_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ec')->where('id_transfer_ec', $tec->id)->first();
                $tec->tot_unit = $unidades->tot_unit;
                $tec->type = "tec";

            });
        }
/*
        $productsTransferEC = DB::table('transfer_ec_products as e')
            ->join('products as p','e.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('e.id as e_p','e.id_transfer_ec','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
            ->where('p.id_companie', Auth::user()->companie)
            ->get();*/

        $adjustments = DB::table('inventory_adjustments as ia')
            ->join('storehouses as s', 'ia.id_storehouse', 's.id')
            ->join('employees as e', 's.id_employee', 'e.id')
            ->join('users as u', 'ia.id_user', 'u.id')
            ->join('products as p', 's.id_product', 'p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('ia.id', 'ia.id_adjustment', 'ia.id_user', 'e.name as origin', 'e.name as destiny', 'u.name as username',
                'ia.total_adjustment as total', 'ia.created_at as date', 'ia.units_adjustment as tot_unit', 'ia.comments as type',
                'p.name as product', 'p.quantity', 'pu.name as unit', 'p.base_price', 'ia.units_before', 'ia.quantity_before', 'ia.total_before',
                'ia.units_adjustment', 'ia.quantity_adjustment', 'ia.total_adjustment')
            ->where('s.id_employee', $id)
            ->get();

        $movementsCollection = collect();
        $movements = $transfer_ee->merge($transfer_ce);
        $movements = $movements->merge($transfer_ec);
        $movements = $movements->merge($adjustments);
        $movements = $movements->sortBy('date', SORT_REGULAR, true);
        $movementsCollection = $movements->merge($movementsCollection);

        foreach ($movementsCollection as $movementCollection) {
            $movementsCollection->map(function($movementCollection){
                $movementCollection->idMovement = 0;
                if ($movementCollection->type == 'tee') $movementCollection->idMovement = $movementCollection->id_transfer_ee;
                if ($movementCollection->type == 'tce') $movementCollection->idMovement = $movementCollection->id_transfer_ce;
                if ($movementCollection->type == 'tec') $movementCollection->idMovement = $movementCollection->id_transfer_ec;
                if ($movementCollection->type != 'tee' && $movementCollection->type != 'tce' && $movementCollection->type != 'tec') {
                    $movementCollection->idMovement = $movementCollection->id_adjustment;
                }
                $date = explode(' ',$movementCollection->date);
                $movementCollection->dateCarbon = $date[0];
            });
        }

        $filtered = $movementsCollection;
        if ($folio == null && $source == null && $destiny == null && $initialDate == null && $finalDate == null)
        $filtered = $movementsCollection->filter(function ($value) {
            return $value->dateCarbon >= Carbon::now()->toDateString() && $value->dateCarbon <= Carbon::now()->toDateString();
        });

        $symbol_country = CommonCompany::getSymbolByCountry();

        return response()->json([
            'code' => 200,
            'movements' => $filtered,
            //'productsTransferEC' => $productsTransferEC,
            //'productsTransferCE' => $productsTransferCE,
            'nameStorehouse' => $nameStorehouse,
            //'productsTransferEE' => $productsTransferEE,
            'symbol_country' => $symbol_country,
            'folio' => $folio,
            'source' => $source,
            'destiny' => $destiny,
            'initialDate' => $initialDate,
            'finalDate' => $finalDate
        ]);


       /* return view('vendor.adminlte.inventory.employee._movements')->with(['movements' => $movements, 'productsTransferEC' => $productsTransferEC,
            'productsTransferCE' => $productsTransferCE, 'nameStorehouse' => $nameStorehouse,
            'productsTransferEE' => $productsTransferEE
        ]);*/

    }

    public function getDataHistoricalInventoryJobCenter($idJobCenter, Request $request){
        try {
            // Filters
            $folioInventory = $request->get('folioInventory');
            $initialDateHistoricalInventory = $request->get('initialDateHistoricalInventory');
            $finalDateHistoricalInventory = $request->get('finalDateHistoricalInventory');
            $userInventory = $request->get('userInventory');
            $typeMovement = $request->get('typeMovement');
            $productInventory = $request->get('productInventory');
            $nameSource = $request->get('nameSource');
            $nameDestiny = $request->get('nameDestiny');
            $destinationsFilter =$request->get('destinationsFilter');

            $profileJobCenter = profile_job_center::find($idJobCenter);
            $queryHistoricalInventory = HistoricalInventory::join('products as p', 'p.id', 'historical_inventories.id_product')
                ->join('product_units as pu','pu.id','p.id_unit')
                ->join('users as u', 'u.id', 'historical_inventories.id_user')
                ->join('type_movements_inventories as tmi', 'tmi.id', 'historical_inventories.id_type_movement_inventory')
                ->where('historical_inventories.id_profile_job_center', $profileJobCenter->id);

            if ($folioInventory != null ) $queryHistoricalInventory->where('historical_inventories.folio','like','%'. $folioInventory . '%');
            if ($initialDateHistoricalInventory != null && $finalDateHistoricalInventory != null) {
                $queryHistoricalInventory->whereDate('historical_inventories.date', '>=', $initialDateHistoricalInventory)
                    ->whereDate('historical_inventories.date', '<=', $finalDateHistoricalInventory);
            } else {
                $queryHistoricalInventory->whereDate('historical_inventories.date', '>=', Carbon::now()->toDateString())
                    ->whereDate('historical_inventories.date', '<=', Carbon::now()->toDateString());
            }
            if ($userInventory != 0) $queryHistoricalInventory->where('u.id', $userInventory);
            if ($typeMovement != 0) $queryHistoricalInventory->where('tmi.id', $typeMovement);
            if ($productInventory != 0) $queryHistoricalInventory->where('p.id', $productInventory);
            if ($nameSource != null ) $queryHistoricalInventory->where('historical_inventories.source','like','%'. $nameSource . '%');
            if ($nameDestiny != null ) $queryHistoricalInventory->where('historical_inventories.destiny','like','%'. $nameDestiny . '%');

            $historicalInventoriesDetail = $queryHistoricalInventory->select('historical_inventories.*',
                'p.name as name_product', 'u.name as name_user', 'tmi.name as nameType','pu.name as unit_product')
                ->orderBy('historical_inventories.created_at','desc')
                ->get();

            // Totals generals
            $stock = 0;
            $total = 0;
            foreach ($historicalInventoriesDetail as $historicalInventoryDetail){
                $stock += $historicalInventoryDetail->after_stock;
                $total += $historicalInventoryDetail->value_inventory;
            }

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');
            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'historicalInventoriesDetail' => $historicalInventoriesDetail,
                'profileJobCenter' => $profileJobCenter,
                'stock' => $stock,
                'total' => $total,
                'deleteData' => $deleteData,
                'master' => $master,
                'symbol_country' => $symbol_country,
                'destinationsFilter' => $destinationsFilter
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataHistoricalInventoryProductCenter($idProduct, $idJobCenter, Request $request){
        try {
            // Filters
            $folioInventory = $request->get('folioInventory');
            $initialDateHistoricalInventory = $request->get('initialDateHistoricalInventory');
            $finalDateHistoricalInventory = $request->get('finalDateHistoricalInventory');
            $userInventory = $request->get('userInventory');
            $typeMovement = $request->get('typeMovement');
            $productInventory = $request->get('productInventory');
            $nameSource = $request->get('nameSource');
            $nameDestiny = $request->get('nameDestiny');
            $destinationsFilter =$request->get('destinationsFilter');

            $profileJobCenter = profile_job_center::find($idJobCenter);
            $queryHistoricalInventorySource = HistoricalInventory::join('products as p', 'p.id', 'historical_inventories.id_product')
                ->join('product_units as pu','pu.id','p.id_unit')
                ->join('users as u', 'u.id', 'historical_inventories.id_user')
                ->join('type_movements_inventories as tmi', 'tmi.id', 'historical_inventories.id_type_movement_inventory')
                ->where('historical_inventories.source', $profileJobCenter->name)
                ->where('historical_inventories.id_product', $idProduct)
                ->select('historical_inventories.*', 'p.name as name_product', 'u.name as name_user', 'tmi.name as nameType','pu.name as unit_product')
                ->get();

            $queryHistoricalInventoryDestinity = HistoricalInventory::join('products as p', 'p.id', 'historical_inventories.id_product')
                ->join('product_units as pu','pu.id','p.id_unit')
                ->join('users as u', 'u.id', 'historical_inventories.id_user')
                ->join('type_movements_inventories as tmi', 'tmi.id', 'historical_inventories.id_type_movement_inventory')
                ->where('historical_inventories.destiny', $profileJobCenter->name)
                ->where('historical_inventories.id_product', $idProduct)
                ->select('historical_inventories.*', 'p.name as name_product', 'u.name as name_user', 'tmi.name as nameType','pu.name as unit_product')
                ->get();

            $dataFilters = $queryHistoricalInventorySource->merge($queryHistoricalInventoryDestinity);

            // Totals generals
            $stock = 0;
            $total = 0;
            foreach ($dataFilters as $dataFilter){
                $stock += $dataFilter->after_stock;
                $total += $dataFilter->value_inventory;
            }

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');
            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'historicalInventoriesDetail' => $dataFilters,
                'profileJobCenter' => $profileJobCenter,
                'stock' => $stock,
                'total' => $total,
                'deleteData' => $deleteData,
                'master' => $master,
                'symbol_country' => $symbol_country,
                'initialDate' => $initialDateHistoricalInventory,
                'finalDate' => $finalDateHistoricalInventory,
                'folio' => $folioInventory,
                'userId' => $userInventory,
                'typeMovementId' => $typeMovement,
                'productId' => $productInventory,
                'nameSourceSearch' => $nameSource,
                'nameDestinySearch' => $nameDestiny,
                'destinationsFilter' => $destinationsFilter
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataHistoricalInventoryEmployee($idEmployee, Request $request){
        try {
            // Filters
            $folioInventory = $request->get('folioInventory');
            $initialDateHistoricalInventory = $request->get('initialDateHistoricalInventory');
            $finalDateHistoricalInventory = $request->get('finalDateHistoricalInventory');
            $userInventory = $request->get('userInventory');
            $typeMovement = $request->get('typeMovement');
            $productInventory = $request->get('productInventory');
            $nameSource = $request->get('nameSource');
            $nameDestiny = $request->get('nameDestiny');
            $destinationsFilter =$request->get('destinationsFilter');

            $employee = employee::find($idEmployee);
            $queryHistoricalInventory = HistoricalInventory::join('products as p', 'p.id', 'historical_inventories.id_product')
                ->join('product_units as pu','pu.id','p.id_unit')
                ->join('users as u', 'u.id', 'historical_inventories.id_user')
                ->join('type_movements_inventories as tmi', 'tmi.id', 'historical_inventories.id_type_movement_inventory')
                ->where('historical_inventories.destiny', $employee->name)
                ->orWhere('historical_inventories.source', $employee->name)
                ->select('historical_inventories.*', 'p.name as name_product', 'u.name as name_user', 'tmi.name as nameType','pu.name as unit_product')
                ->orderBy('historical_inventories.created_at','desc')
                ->get();

            $dataFilters = $queryHistoricalInventory;

            // Totals generals
            $stock = 0;
            $total = 0;
            foreach ($dataFilters as $dataFilter){
                $stock += $dataFilter->after_stock;
                $total += $dataFilter->value_inventory;
            }

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');
            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'historicalInventoriesDetail' => $dataFilters,
                'employee' => $employee,
                'stock' => $stock,
                'total' => $total,
                'deleteData' => $deleteData,
                'master' => $master,
                'symbol_country' => $symbol_country,
                'initialDate' => $initialDateHistoricalInventory,
                'finalDate' => $finalDateHistoricalInventory,
                'folio' => $folioInventory,
                'userId' => $userInventory,
                'typeMovementId' => $typeMovement,
                'productId' => $productInventory,
                'nameSourceSearch' => $nameSource,
                'nameDestinySearch' => $nameDestiny,
                'destinationsFilter' => $destinationsFilter
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataHistoricalInventoryProduct($idProduct, $idEmployee,Request $request){
        try {
            // Filters

            $folioInventory = $request->get('folioInventory');
            $initialDateHistoricalInventory = $request->get('initialDateHistoricalInventory');
            $finalDateHistoricalInventory = $request->get('finalDateHistoricalInventory');
            $userInventory = $request->get('userInventory');
            $typeMovement = $request->get('typeMovement');
            $productInventory = $request->get('productInventory');
            $nameSource = $request->get('nameSource');
            $nameDestiny = $request->get('nameDestiny');
            $destinationsFilter =$request->get('destinationsFilter');

            $employee = employee::find($idEmployee);
            $queryHistoricalInventorySource = HistoricalInventory::join('products as p', 'p.id', 'historical_inventories.id_product')
                ->join('product_units as pu','pu.id','p.id_unit')
                ->join('users as u', 'u.id', 'historical_inventories.id_user')
                ->join('type_movements_inventories as tmi', 'tmi.id', 'historical_inventories.id_type_movement_inventory')
                ->where('historical_inventories.id_product', $idProduct)
                ->where('historical_inventories.source', $employee->name)
                ->select('historical_inventories.*', 'p.name as name_product', 'u.name as name_user', 'tmi.name as nameType','pu.name as unit_product')
                ->get();

            $queryHistoricalInventoryDestiny = HistoricalInventory::join('products as p', 'p.id', 'historical_inventories.id_product')
                ->join('product_units as pu','pu.id','p.id_unit')
                ->join('users as u', 'u.id', 'historical_inventories.id_user')
                ->join('type_movements_inventories as tmi', 'tmi.id', 'historical_inventories.id_type_movement_inventory')
                ->where('historical_inventories.id_product', $idProduct)
                ->where('historical_inventories.destiny', $employee->name)
                ->select('historical_inventories.*', 'p.name as name_product', 'u.name as name_user', 'tmi.name as nameType','pu.name as unit_product')
                ->get();

            $dataFilters = $queryHistoricalInventorySource->merge($queryHistoricalInventoryDestiny);

            // Totals generals
            $stock = 0;
            $total = 0;
            foreach ($dataFilters as $dataFilter){
                $stock += $dataFilter->after_stock;
                $total += $dataFilter->value_inventory;
            }

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');
            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'historicalInventoriesDetail' => $dataFilters,
                'employee' => $employee,
                'stock' => $stock,
                'total' => $total,
                'deleteData' => $deleteData,
                'master' => $master,
                'symbol_country' => $symbol_country,
                'initialDate' => $initialDateHistoricalInventory,
                'finalDate' => $finalDateHistoricalInventory,
                'folio' => $folioInventory,
                'userId' => $userInventory,
                'typeMovementId' => $typeMovement,
                'productId' => $productInventory,
                'nameSourceSearch' => $nameSource,
                'nameDestinySearch' => $nameDestiny,
                'destinationsFilter' => $destinationsFilter
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataHistoricalInventoryCompany($idCompany, Request $request){
        try {
            // Filters
            $folioInventory = $request->get('folioInventory');
            $initialDateHistoricalInventory = $request->get('initialDateHistoricalInventory');
            $finalDateHistoricalInventory = $request->get('finalDateHistoricalInventory');
            $userInventory = $request->get('userInventory');
            $typeMovement = $request->get('typeMovement');
            $productInventory = $request->get('productInventory');
            $nameSource = $request->get('nameSource');
            $nameDestiny = $request->get('nameDestiny');
            $idProduct = $request->get('idProduct');
            $destinationsFilter =$request->get('destinationsFilter');

            $company = companie::find($idCompany);
            $queryHistoricalInventory = HistoricalInventory::join('products as p', 'p.id', 'historical_inventories.id_product')
                ->join('product_units as pu','pu.id','p.id_unit')
                ->join('users as u', 'u.id', 'historical_inventories.id_user')
                ->join('type_movements_inventories as tmi', 'tmi.id', 'historical_inventories.id_type_movement_inventory')
                ->where('historical_inventories.id_company', $company->id);

            if ($folioInventory != null ) $queryHistoricalInventory->where('historical_inventories.folio','like','%'. $folioInventory . '%');
            if ($initialDateHistoricalInventory != null && $finalDateHistoricalInventory != null) {
                $queryHistoricalInventory->whereDate('historical_inventories.date', '>=', $initialDateHistoricalInventory)
                    ->whereDate('historical_inventories.date', '<=', $finalDateHistoricalInventory);
            } else {
                $queryHistoricalInventory->whereDate('historical_inventories.date', '>=', Carbon::now()->toDateString())
                    ->whereDate('historical_inventories.date', '<=', Carbon::now()->toDateString());
            }
            if ($userInventory != 0) $queryHistoricalInventory->where('u.id', $userInventory);
            if ($typeMovement != 0) $queryHistoricalInventory->where('tmi.id', $typeMovement);
            if ($productInventory != 0) $queryHistoricalInventory->where('p.id', $productInventory);
            if ($nameSource != null ) $queryHistoricalInventory->where('historical_inventories.source','like','%'. $nameSource . '%');
            if ($nameDestiny != null ) $queryHistoricalInventory->where('historical_inventories.destiny','like','%'. $nameDestiny . '%');
            if ($idProduct != 0) $queryHistoricalInventory->where('historical_inventories.id_product', $idProduct);

            $historicalInventoriesDetail = $queryHistoricalInventory->select('historical_inventories.*',
                'p.name as name_product', 'u.name as name_user', 'tmi.name as nameType','pu.name as unit_product')
                ->orderBy('historical_inventories.created_at','desc')
                ->get();

            // Totals generals
            $stock = 0;
            $total = 0;
            foreach ($historicalInventoriesDetail as $historicalInventoryDetail){
                $stock += $historicalInventoryDetail->after_stock;
                $total += $historicalInventoryDetail->value_inventory;
            }

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');
            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 200,
                'historicalInventoriesDetail' => $historicalInventoriesDetail,
                'company' => $company,
                'stock' => $stock,
                'total' => $total,
                'deleteData' => $deleteData,
                'master' => $master,
                'symbol_country' => $symbol_country,
                'destinationsFilter' => $destinationsFilter
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataDetailMovements($id, Request $request)
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $idJobCenter = $request->get('idJobCenter');
        $idEmployee = $request->get('idEmployee');
        $typeMovementStoreHouse = $request->get('typeMovementStoreHouse');
        $isAdjustment = $request->get('isAdjustment');

        // Detail By Movement Job Center and Employee
        $detailMovement = new Collection();
        $collectionMovements = new Collection();
        $adjustment = new Collection();
        $totalByMovement = 0;
        $nameSource = 0;

        if ($idJobCenter != 0) {
            $profile_job_centers_id = profile_job_center::find($idJobCenter);
            // Middleware to deny access to data from another company
            if ($profile_job_centers_id->companie != auth()->user()->companie) {
                SecurityController::abort();
            }

            //Entry
            if ($typeMovementStoreHouse == 'entry') {
                $detailMovement = entry::join('profile_job_centers as pjc', 'entrys.id_job_center', 'pjc.id')
                    ->join('users as u','entrys.id_user','u.id')
                    ->where('entrys.id', $id)
                    ->select('entrys.id as idMovement', 'entrys.id_entry as folio', 'entrys.origin', 'pjc.name as destiny', 'u.name as user_name',
                        'entrys.created_at as date')
                    ->first();
                    $collectionMovements = entry_product::join('products as p','entry_products.id_product','p.id')
                        ->join('product_units as pu', 'p.id_unit', 'pu.id')
                        ->select('entry_products.id as e_p','entry_products.id_entry','entry_products.id_product','entry_products.units',
                            'entry_products.price','p.name','p.quantity','p.base_price', 'pu.name as unit')
                        ->where('entry_products.id_entry', $detailMovement->idMovement)
                        ->where('p.profile_job_center_id', $idJobCenter)
                        ->get();
                    foreach ($collectionMovements as $collectionMovement){
                        $totalByMovement += $collectionMovement->base_price * $collectionMovement->units;
                    }
            }

            // Transfer Center to Center
            if ($typeMovementStoreHouse == 'tcc') {
                $detailMovement = transfer_center_center::join('users as u','transfer_center_centers.id_user','u.id')
                    ->join('profile_job_centers as pj','transfer_center_centers.id_job_center_origin','pj.id')
                    ->join('profile_job_centers as pjc','transfer_center_centers.id_job_center_destiny','pjc.id')
                    ->select('transfer_center_centers.id as idMovement','transfer_center_centers.id_transfer_cc as folio',
                        'pj.name as origin', 'pjc.name as destiny', 'u.name as user_name', 'transfer_center_centers.created_at as date')
                    ->where('transfer_center_centers.id', $id)
                    ->first();
                $collectionMovements = transfer_cc_product::join('products as p','transfer_cc_products.id_product','p.id')
                    ->join('product_units as pu', 'p.id_unit', 'pu.id')
                    ->select('transfer_cc_products.id as e_p','transfer_cc_products.id_transfer_cc','transfer_cc_products.id_product',
                        'transfer_cc_products.units','p.name','p.quantity','p.base_price', 'pu.name as unit' , 'transfer_cc_products.is_units')
                    ->where('transfer_cc_products.id_transfer_cc', $detailMovement->idMovement)
                    ->where('p.profile_job_center_id', $idJobCenter)
                    ->get();
                foreach ($collectionMovements as $collectionMovement){
                    if ($collectionMovement->is_units == 1) $totalByMovement += $collectionMovement->base_price * $collectionMovement->units;
                    else $totalByMovement += ($collectionMovement->base_price * $collectionMovement->units) / $collectionMovement->quantity;
                }
            }

            // Transfer Center to Employee
            if ($typeMovementStoreHouse == 'tce') {
                $detailMovement = transfer_center_employee::join('users as u','transfer_center_employees.id_user','u.id')
                    ->join('profile_job_centers as pj','transfer_center_employees.id_job_center_origin','pj.id')
                    ->join('employees as e','transfer_center_employees.id_employee_destiny','e.id')
                    ->select('transfer_center_employees.id as idMovement','transfer_center_employees.id_transfer_ce as folio',
                        'pj.name as origin','e.name as destiny','u.name as user_name', 'transfer_center_employees.created_at as date')
                    ->where('transfer_center_employees.id', $id)
                    ->first();
                $collectionMovements = transfer_ce_product::join('products as p','transfer_ce_products.id_product','p.id')
                    ->join('product_units as pu', 'p.id_unit', 'pu.id')
                    ->select('transfer_ce_products.id as e_p','transfer_ce_products.id_transfer_ce','transfer_ce_products.id_product',
                        'transfer_ce_products.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'transfer_ce_products.is_units')
                    ->where('transfer_ce_products.id_transfer_ce', $detailMovement->idMovement)
                    ->where('p.profile_job_center_id', $idJobCenter)
                    ->get();
                foreach ($collectionMovements as $collectionMovement){
                    if ($collectionMovement->is_units == 1) $totalByMovement += $collectionMovement->base_price * $collectionMovement->units;
                    else $totalByMovement += ($collectionMovement->base_price * $collectionMovement->units) / $collectionMovement->quantity;
                }
            }

            // Transfer Employee to Center
            if ($typeMovementStoreHouse == 'tec') {
                $detailMovement = transfer_employee_center::join('users as u','transfer_employee_centers.id_user','u.id')
                    ->join('profile_job_centers as pj','transfer_employee_centers.id_job_center_destiny','pj.id')
                    ->join('employees as e','transfer_employee_centers.id_employee_origin','e.id')
                    ->select('transfer_employee_centers.id as idMovement','transfer_employee_centers.id_transfer_ec as folio',
                        'pj.name as destiny','e.name as origin','u.name as user_name', 'transfer_employee_centers.created_at as date')
                    ->where('transfer_employee_centers.id', $id)
                    ->first();
                $collectionMovements = transfer_ec_product::join('products as p','transfer_ec_products.id_product','p.id')
                    ->join('product_units as pu', 'p.id_unit', 'pu.id')
                    ->select('transfer_ec_products.id as e_p','transfer_ec_products.id_transfer_ec','transfer_ec_products.id_product',
                        'transfer_ec_products.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'transfer_ec_products.is_units')
                    ->where('transfer_ec_products.id_transfer_ec', $detailMovement->idMovement)
                    ->where('p.profile_job_center_id', $idJobCenter)
                    ->get();
                foreach ($collectionMovements as $collectionMovement){
                    if ($collectionMovement->is_units == 1) $totalByMovement += $collectionMovement->base_price * $collectionMovement->units;
                    else $totalByMovement += ($collectionMovement->base_price * $collectionMovement->units) / $collectionMovement->quantity;
                }
            }

        }

        if ($idEmployee != 0) {
            $employee = employee::find($idEmployee);
            // Middleware to deny access to data from another company
            if ($employee->id_company != auth()->user()->companie) {
                SecurityController::abort();
            }

            // Transfer Employee to Employee
            if ($typeMovementStoreHouse == 'tee') {
                $detailMovement = transfer_employee_employee::join('users as u','transfer_employee_employees.id_user','u.id')
                    ->join('employees as e1','transfer_employee_employees.id_employee_origin','e1.id')
                    ->join('employees as e2','transfer_employee_employees.id_employee_destiny','e2.id')
                    ->select('transfer_employee_employees.id as idMovement','transfer_employee_employees.id_transfer_ee as folio',
                       'e1.name as origin','e2.name as destiny','u.name as user_name', 'transfer_employee_employees.created_at as date')
                    ->where('transfer_employee_employees.id', $id)
                    ->first();
                $collectionMovements = transfer_ee_product::join('products as p','transfer_ee_products.id_product','p.id')
                    ->join('product_units as pu', 'p.id_unit', 'pu.id')
                    ->select('transfer_ee_products.id as e_p','transfer_ee_products.id_transfer_ee','transfer_ee_products.id_product',
                        'transfer_ee_products.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'transfer_ee_products.is_units')
                    ->where('transfer_ee_products.id_transfer_ee', $detailMovement->idMovement)
                    ->get();
                foreach ($collectionMovements as $collectionMovement){
                if ($collectionMovement->is_units == 1) $totalByMovement += $collectionMovement->base_price * $collectionMovement->units;
                else $totalByMovement += ($collectionMovement->base_price * $collectionMovement->units) / $collectionMovement->quantity;
                }
            }

            // Transfer Center to Employee
            if ($typeMovementStoreHouse == 'tce') {
                $detailMovement = transfer_center_employee::join('users as u','transfer_center_employees.id_user','u.id')
                    ->join('profile_job_centers as pj','transfer_center_employees.id_job_center_origin','pj.id')
                    ->join('employees as e','transfer_center_employees.id_employee_destiny','e.id')
                    ->select('transfer_center_employees.id as idMovement','transfer_center_employees.id_transfer_ce as folio',
                        'pj.name as origin','e.name as destiny','u.name as user_name', 'transfer_center_employees.created_at as date')
                    ->where('transfer_center_employees.id', $id)
                    ->first();
                $collectionMovements = transfer_ce_product::join('products as p','transfer_ce_products.id_product','p.id')
                    ->join('product_units as pu', 'p.id_unit', 'pu.id')
                    ->select('transfer_ce_products.id as e_p','transfer_ce_products.id_transfer_ce','transfer_ce_products.id_product',
                        'transfer_ce_products.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'transfer_ce_products.is_units')
                    ->where('transfer_ce_products.id_transfer_ce', $detailMovement->idMovement)
                    ->get();
                foreach ($collectionMovements as $collectionMovement){
                    if ($collectionMovement->is_units == 1) $totalByMovement += $collectionMovement->base_price * $collectionMovement->units;
                    else $totalByMovement += ($collectionMovement->base_price * $collectionMovement->units) / $collectionMovement->quantity;
                }
            }

            // Transfer Employee to Center
            if ($typeMovementStoreHouse == 'tec') {
                $detailMovement = transfer_employee_center::join('users as u','transfer_employee_centers.id_user','u.id')
                    ->join('profile_job_centers as pj','transfer_employee_centers.id_job_center_destiny','pj.id')
                    ->join('employees as e','transfer_employee_centers.id_employee_origin','e.id')
                    ->select('transfer_employee_centers.id as idMovement','transfer_employee_centers.id_transfer_ec as folio',
                        'pj.name as destiny','e.name as origin','u.name as user_name', 'transfer_employee_centers.created_at as date')
                    ->where('transfer_employee_centers.id', $id)
                    ->first();
                $collectionMovements = transfer_ec_product::join('products as p','transfer_ec_products.id_product','p.id')
                    ->join('product_units as pu', 'p.id_unit', 'pu.id')
                    ->select('transfer_ec_products.id as e_p','transfer_ec_products.id_transfer_ec','transfer_ec_products.id_product',
                        'transfer_ec_products.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'transfer_ec_products.is_units')
                    ->where('transfer_ec_products.id_transfer_ec', $detailMovement->id)
                    ->get();
                foreach ($collectionMovements as $collectionMovement){
                    if ($collectionMovement->is_units == 1) $totalByMovement += $collectionMovement->base_price * $collectionMovement->units;
                    else $totalByMovement += ($collectionMovement->base_price * $collectionMovement->units) / $collectionMovement->quantity;
                }
            }

        }

        if ($isAdjustment == 1) {

            $adjustment = inventory_adjustment::join('storehouses as s', 'inventory_adjustments.id_storehouse', 's.id')
                ->join('users as u', 'inventory_adjustments.id_user', 'u.id')
                ->join('products as p', 's.id_product', 'p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('inventory_adjustments.id', 'inventory_adjustments.id_adjustment', 'inventory_adjustments.id_user',
                    'u.name as username', 'inventory_adjustments.total_adjustment as total', 'inventory_adjustments.created_at as date',
                    'inventory_adjustments.units_adjustment as tot_unit', 'inventory_adjustments.comments as type', 'p.name as product',
                    'p.quantity', 'pu.name as unit', 'p.base_price', 'inventory_adjustments.units_before', 'inventory_adjustments.quantity_before',
                    'inventory_adjustments.total_before', 'inventory_adjustments.units_adjustment', 'inventory_adjustments.quantity_adjustment',
                    'inventory_adjustments.total_adjustment', 's.id_job_center', 's.id_employee')
                ->where('inventory_adjustments.id', $id)
                ->first();

            $nameSource = profile_job_center::find($adjustment->id_job_center);
            if ($nameSource == null) $nameSource = employee::find($adjustment->id_employee);
        }

        $symbol_country = CommonCompany::getSymbolByCountry();

        return response()->json([
            'code' => 200,
            'detailMovement' => $detailMovement,
            'collectionMovements' => $collectionMovements,
            'symbol_country' => $symbol_country,
            'totalByMovement' => $totalByMovement,
            'isAdjustment' => $isAdjustment,
            'adjustment' => $adjustment,
            'nameSource' => $nameSource
        ]);

        /*return view('vendor.adminlte.inventory.jobcenter._movements')->with(['movements' => $movements, 'productsTransferEC' => $productsTransferEC,
            'productsTransferCC' => $productsTransferCC, 'productsTransferCE' => $productsTransferCE, 'nameStorehouse' => $nameStorehouse,
            'productsEntry' => $productsEntry, 'factura' => $factura
        ]);*/
    }

    public function reportDataStudio()
    {
        return view('vendor.adminlte.inventory.report_datastudio');
    }

}
