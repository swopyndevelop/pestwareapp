<?php

namespace App\Http\Controllers\Inventory;

use App\employee;
use App\HistoricalInventory;
use App\inventory_adjustment;
use App\profile_job_center;
use App\storehouse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AdjustmentController extends Controller
{
    public function storeAdjustment(Request $request)
    {
        try {
            DB::beginTransaction();

            // Get request values
            $unitsEntry = $request->get('units_entry');
            $adjustmentType = $request->get('adjustment_type');
            $storehouseFind = storehouse::find($request->get('id_storehouse'));
            $product = DB::table('products')
                ->select('quantity', 'base_price','profile_job_center_id')
                ->where('id', $storehouseFind->id_product)
                ->first();

            if ($storehouseFind->id_job_center == null) $nameSourceDestiny = employee::find($storehouseFind->id_employee)->name;
            else $nameSourceDestiny = profile_job_center::find($storehouseFind->id_job_center)->name;

            $typeEntry = $request->get('type_entry');

            if ($typeEntry == 0) {
                if ($storehouseFind->total == 0) $price = $product->base_price / $product->quantity;
                else $price = $storehouseFind->total / $storehouseFind->stock_other_units;
                $totalStore = $price * $unitsEntry;
                $quantityProduct = $product->quantity * $unitsEntry;
                $unitComplete = $unitsEntry;
            }
            else {
                if ($storehouseFind->total == 0) $price = $product->base_price / $product->quantity;
                else $price = $storehouseFind->total / $storehouseFind->stock_other_units;
                $totalStore = $price * $unitsEntry;
                $quantityProduct = $unitsEntry;
                $unitComplete = $quantityProduct / $product->quantity;
            }

            $valueUnitPrice = $price;
            $valueMovement = $price * $quantityProduct;

            // Create Adjustment
            $adjustment = new inventory_adjustment();
            $adjustment->id_adjustment = $request->get('adjustment_type');
            $adjustment->id_user = Auth::id();
            $adjustment->id_storehouse = $request->get('id_storehouse');
            $adjustment->id_company = Auth::user()->companie;
            $adjustment->comments = $request->get('comments');
            $adjustment->units_before = $request->get('units_before');
            $adjustment->quantity_before = $request->get('quantity_before');
            $adjustment->total_before = $request->get('total_before');
            $adjustment->units_adjustment = $request->get('units_entry');
            $adjustment->quantity_adjustment = $quantityProduct;
            $adjustment->total_adjustment = $totalStore;
            $adjustment->save();

            $adjustmentUpdate = inventory_adjustment::find($adjustment->id);
            $adjustmentUpdate->id_adjustment = $request->get('adjustment_type') . $adjustment->id;
            $adjustmentUpdate->save();

            $storehouse = storehouse::find($request->get('id_storehouse'));
            $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $storehouseFind->id_product)->first();

            // Update Storehouse product
            if ($adjustmentType == 'APE-') {
                // Calculate stock units
                $stockFinal = $storehouse->stock_other_units + $quantityProduct;
                $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);
                // History Inventory Before
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = $adjustmentUpdate->id_adjustment;
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = Auth::id();
                $historicalInventory->id_type_movement_inventory = 3;
                $historicalInventory->id_product = $storehouseFind->id_product;
                $historicalInventory->source = $nameSourceDestiny;
                $historicalInventory->destiny = $nameSourceDestiny;
                $historicalInventory->quantity = ceil($unitComplete);
                $historicalInventory->fraction_quantity = $quantityProduct;
                $historicalInventory->before_stock = $storehouseFind->stock;
                $historicalInventory->after_stock = $stockFinal;
                $historicalInventory->fraction_before_stock = $storehouseFind->stock_other_units;
                $historicalInventory->fraction_after_stock = $storehouseFind->stock_other_units + $quantityProduct;
                $historicalInventory->unit_price = $valueUnitPrice;
                $historicalInventory->value_movement = $valueMovement;
                $historicalInventory->value_inventory = $storehouseFind->total + $valueMovement;
                $historicalInventory->is_destiny = 0;
                $historicalInventory->id_profile_job_center = $product->profile_job_center_id;
                $historicalInventory->id_company = Auth::user()->companie;
                $historicalInventory->save();

                $storehouse->stock = $stockFinal;
                $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                if ( $storehouse->stock == 0 && $storehouse->stock_other_units == 0) $storehouse->total = 0;
                else $storehouse->total = $storehouse->total + $valueMovement;
                $storehouse->save();
            }
            else if ($adjustmentType == 'APS-') {
                // Calculate stock units
                $stockFinal = $storehouse->stock_other_units - $quantityProduct;
                $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);
                // History Inventory Before
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = $adjustmentUpdate->id_adjustment;
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = Auth::id();
                $historicalInventory->id_type_movement_inventory = 2;
                $historicalInventory->id_product = $storehouseFind->id_product;
                $historicalInventory->source = $nameSourceDestiny;
                $historicalInventory->destiny = $nameSourceDestiny;
                $historicalInventory->quantity = ceil($unitComplete);
                $historicalInventory->fraction_quantity = $quantityProduct;
                $historicalInventory->before_stock = $storehouseFind->stock;
                $historicalInventory->after_stock = $storehouseFind->stock - ceil($unitComplete);
                $historicalInventory->fraction_before_stock = $storehouseFind->stock_other_units;
                $historicalInventory->fraction_after_stock = $storehouseFind->stock_other_units - $quantityProduct;
                $historicalInventory->unit_price = $valueUnitPrice;
                $historicalInventory->value_movement = $valueMovement;
                $historicalInventory->value_inventory = $storehouseFind->total - $valueMovement;
                $historicalInventory->is_destiny = 0;
                $historicalInventory->id_profile_job_center = $product->profile_job_center_id;
                $historicalInventory->id_company = Auth::user()->companie;
                $historicalInventory->save();

                $storehouse->stock = $stockFinal;
                $storehouse->stock_other_units = $storehouse->stock_other_units - $quantityProduct;
                if ( $storehouse->stock == 0 && $storehouse->stock_other_units == 0) $storehouse->total = 0;
                else $storehouse->total = $storehouse->total + $valueMovement;
                $storehouse->save();
            }

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Se guardo correctamente el Ajuste'
            ]);
        }
        catch (\Exception $exception) {
            return response()->json(
                [
                'code' => 500,
                'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }
}
