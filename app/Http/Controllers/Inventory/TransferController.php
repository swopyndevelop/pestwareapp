<?php

namespace App\Http\Controllers\Inventory;

use App\HistoricalInventory;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\profile_job_center;
use App\treeJobCenter;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\employee;
use App\storehouse;
use App\transfer_cc_product;
use App\transfer_ce_product;
use App\transfer_center_center;
use App\transfer_center_employee;
use App\transfer_ee_product;
use App\transfer_employee_employee;
use App\transfer_employee_center;
use App\transfer_ec_product;

class TransferController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function getTransfersCC()
    {
        try {
            $transfer_cc = DB::table('transfer_center_centers as tc')->join('users as u','tc.id_user','u.id')
            ->join('profile_job_centers as pj','tc.id_job_center_origin','pj.id')
            ->join('profile_job_centers as pjc','tc.id_job_center_destiny','pjc.id')
            ->select('tc.id','tc.id_transfer_cc','tc.id_user','pj.name as origen','pjc.name as destiny','u.name as usuario',
                'tc.total', 'tc.created_at as date')
            ->where('tc.companie', Auth::user()->companie)
                ->orderBy('tc.created_at','desc')
            ->paginate(10);
            
            foreach ($transfer_cc as $tcc) {
                $transfer_cc->map(function($tcc){
                    $unidades = DB::table('transfer_cc_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_cc')->where('id_transfer_cc', $tcc->id)->first();
                    $tcc->tot_unit = $unidades->tot_unit;
                    
                });
            }
            
            $productsTransferCC = DB::table('transfer_cc_products as e')
                ->join('products as p','e.id_product','p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('e.id as e_p','e.id_transfer_cc','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
                ->where('p.id_companie', Auth::user()->companie)
                ->get();

            return response()->json([
                'code' => 200,
                'data' => $transfer_cc,
                'productsData' => $productsTransferCC
            ]);

        } catch (Exception $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function getTransfersCE()
    {
        try {
            $transfer_ce = DB::table('transfer_center_employees as te')->join('users as u','te.id_user','u.id')
                ->join('profile_job_centers as pj','te.id_job_center_origin','pj.id')
                ->join('employees as e','te.id_employee_destiny','e.id')
                ->select('te.id','te.id_transfer_ce','te.id_user','pj.name as origen','e.name as destiny','u.name as usuario',
                    'te.total', 'te.created_at as date')
                ->where('e.id_company', Auth::user()->companie)
                ->orderBy('te.created_at','desc')
                ->paginate(5);
            foreach ($transfer_ce as $tce) {
                $transfer_ce->map(function($tce){
                    $unidades = DB::table('transfer_ce_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ce')->where('id_transfer_ce', $tce->id)->first();
                    $tce->tot_unit = $unidades->tot_unit;
                    
                });
            }
            $productsTransferCE = DB::table('transfer_ce_products as e')
                ->join('products as p','e.id_product','p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('e.id as e_p','e.id_transfer_ce','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
                ->where('p.id_companie', Auth::user()->companie)
                ->get();

            return response()->json([
                'code' => 200,
                'data' => $transfer_ce,
                'productsData' => $productsTransferCE
            ]);

        } catch (Exception $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function getTransfersEE()
    {
        try {
            $title = 'TEE';
            $transfer_ee = DB::table('transfer_employee_employees as t')->join('users as u','t.id_user','u.id')
            ->join('employees as e1','t.id_employee_origin','e1.id')
            ->join('employees as e2','t.id_employee_destiny','e2.id')
            ->select('t.id','t.id_transfer_ee','t.id_user','e1.name as origen','e2.name as destiny','u.name as usuario',
                't.total', 't.created_at as date')
            ->where('t.companie', Auth::user()->companie)
            ->where('t.id_transfer_ee', 'like', '%' . $title . '%')
                ->orderBy('t.created_at','desc')
            ->paginate(10);
            foreach ($transfer_ee as $tee) {
                $transfer_ee->map(function($tee){
                    $unidades = DB::table('transfer_ee_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ee')->where('id_transfer_ee', $tee->id)->first();
                    $tee->tot_unit = $unidades->tot_unit;
                    
                });
            }
            $productsTransferEE = DB::table('transfer_ee_products as e')
                ->join('products as p','e.id_product','p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('e.id as e_p','e.id_transfer_ee','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
                ->where('p.id_companie', Auth::user()->companie)
                ->get();

            return response()->json([
                'code' => 200,
                'data' => $transfer_ee,
                'productsData' => $productsTransferEE
            ]);

        } catch (Exception $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function getTransfersEC()
    {
        try {
            $title = 'TEC';
            $transfer_ec = DB::table('transfer_employee_centers as tce')->join('users as u','tce.id_user','u.id')
                ->join('profile_job_centers as pj','tce.id_job_center_destiny','pj.id')
                ->join('employees as e','tce.id_employee_origin','e.id')
                ->select('tce.id','tce.id_transfer_ec','tce.id_user','pj.name as destiny','e.name as origen','u.name as usuario',
                    'tce.total', 'tce.created_at as date')
                ->where('tce.companie', Auth::user()->companie)
                ->where('tce.id_transfer_ec', 'like', '%' . $title . '%')
                ->orderBy('tce.created_at','desc')
            ->paginate(10);
            foreach ($transfer_ec as $tec) {
                $transfer_ec->map(function($tec){
                    $unidades = DB::table('transfer_ec_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ec')->where('id_transfer_ec', $tec->id)->first();
                    $tec->tot_unit = $unidades->tot_unit;
                    
                });
            }
            $productsTransferEC = DB::table('transfer_ec_products as e')
                ->join('products as p','e.id_product','p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('e.id as e_p','e.id_transfer_ec','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
                ->where('p.id_companie', Auth::user()->companie)
                ->get();

            return response()->json([
                'code' => 200,
                'data' => $transfer_ec,
                'productsData' => $productsTransferEC
            ]);

        } catch (Exception $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function getTransfersCS()
    {
        try {
            $title = 'TCLC';
            $transfer_cs = DB::table('transfer_employee_centers as tce')->join('users as u','tce.id_user','u.id')
                ->join('profile_job_centers as pj','tce.id_job_center_destiny','pj.id')
                ->join('employees as e','tce.id_employee_origin','e.id')
                ->select('tce.id','tce.id_transfer_ec','tce.id_user','pj.name as destiny','e.name as origen','u.name as usuario',
                    'tce.total', 'tce.created_at as date')
                ->where('tce.companie', Auth::user()->companie)
                ->where('tce.id_transfer_ec', 'like', '%' . $title . '%')
                ->orderBy('tce.created_at','desc')
                ->paginate(10);
            foreach ($transfer_cs as $tcs) {
                $transfer_cs->map(function($tcs){
                    $unidades = DB::table('transfer_ec_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ec')->where('id_transfer_ec', $tcs->id)->first();
                    $tcs->tot_unit = $unidades->tot_unit;
                    
                });
            }
            $productsTransferCS = DB::table('transfer_ec_products as e')
                ->join('products as p','e.id_product','p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('e.id as e_p','e.id_transfer_ec','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
                ->where('p.id_companie', Auth::user()->companie)
                ->get();

            return response()->json([
                'code' => 200,
                'data' => $transfer_cs,
                'productsData' => $productsTransferCS
            ]);

        } catch (Exception $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function getTransfersCEMP()
    {
        try {
            $title = 'TCLE';
            $transfer_cemp = DB::table('transfer_employee_employees as t')->join('users as u','t.id_user','u.id')
            ->join('employees as e1','t.id_employee_origin','e1.id')
            ->join('employees as e2','t.id_employee_destiny','e2.id')
            ->select('t.id','t.id_transfer_ee','t.id_user','e1.name as origen','e2.name as destiny','u.name as usuario',
                't.total', 't.created_at as date')
            ->where('t.companie', Auth::user()->companie)
            ->where('t.id_transfer_ee', 'like', '%' . $title . '%')
                ->orderBy('t.created_at','desc')
            ->paginate(10);
            foreach ($transfer_cemp as $tee) {
                $transfer_cemp->map(function($tee){
                    $unidades = DB::table('transfer_ee_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ee')->where('id_transfer_ee', $tee->id)->first();
                    $tee->tot_unit = $unidades->tot_unit;
                    
                });
            }
            $productsTransferCEMP = DB::table('transfer_ee_products as e')
                ->join('products as p','e.id_product','p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('e.id as e_p','e.id_transfer_ee','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
                ->where('p.id_companie', Auth::user()->companie)
                ->get();

            return response()->json([
                'code' => 200,
                'data' => $transfer_cemp,
                'productsData' => $productsTransferCEMP
            ]);

        } catch (Exception $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function getTransfersTAC()
    {
        try {
            $title = 'TAC';
            $transfer_tac = DB::table('transfer_employee_employees as t')->join('users as u','t.id_user','u.id')
            ->join('employees as e1','t.id_employee_origin','e1.id')
            ->join('employees as e2','t.id_employee_destiny','e2.id')
            ->select('t.id','t.id_transfer_ee','t.id_user','e1.name as origen','e2.name as destiny','u.name as usuario',
                't.total', 't.created_at as date')
            ->where('t.companie', Auth::user()->companie)
            ->where('t.id_transfer_ee', 'like', '%' . $title . '%')
                ->orderBy('t.created_at','desc')
            ->paginate(10);
            foreach ($transfer_tac as $tee) {
                $transfer_tac->map(function($tee){
                    $unidades = DB::table('transfer_ee_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_transfer_ee')->where('id_transfer_ee', $tee->id)->first();
                    $tee->tot_unit = $unidades->tot_unit;
                    
                });
            }
            $productsTransferTAC = DB::table('transfer_ee_products as e')
                ->join('products as p','e.id_product','p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('e.id as e_p','e.id_transfer_ee','e.id_product','e.units','p.name','p.quantity','p.base_price', 'pu.name as unit', 'e.is_units')
                ->where('p.id_companie', Auth::user()->companie)
                ->get();

            return response()->json([
                'code' => 200,
                'data' => $transfer_tac,
                'productsData' => $productsTransferTAC
            ]);

        } catch (Exception $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function indexTransfer()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $symbolCountry = CommonCompany::getSymbolByCountry();

        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        return view('vendor.adminlte.transfers.index')->with(['symbolCountry' => $symbolCountry, 'treeJobCenterMain' => $treeJobCenterMain]);
    }

    public function getProducts(Request $request)
    {
        try {
            $id = $request->get('id');
            $destinity = $request->get('destinity');
            $type = $request->get('type');

            $querie = DB::table('storehouses as s')
                ->join('products as p', 's.id_product', 'p.id')
                ->join('product_types as pt', 'p.id_type_product', 'pt.id')
                ->select('p.id', 'p.name')
                ->where('s.stock_other_units', '>', 0)
                ->where('p.id_companie', Auth::user()->companie);

            $querieDestiny = DB::table('products as p')
                ->join('product_types as pt', 'p.id_type_product', 'pt.id')
                ->select('p.id', 'p.name')
                ->where('p.profile_job_center_id', $destinity);

            if ($type == 1) {
                $products = $querie->where('s.id_job_center', $id)->get();
            }
            else if ($type == 3) {
                $querie->where('pt.name', '<>', 'Plaguicida');
                $querie->where('pt.name', '<>', 'Insumo');
                $products = $querie->where('s.id_employee', $id)->get();
            }
            else {
                $products = $querie->where('s.id_employee', $id)->get();
            }

        } catch (Exception $th) {
            return response()->json(['errors' => 500]);
        }

        return response()->json($products);
    }

    public function getProductsForTransferCC(Request $request)
    {
        try {
            $id = $request->get('id');
            $destinity = $request->get('destinity');
            $type = $request->get('type');

            $querie = DB::table('storehouses as s')
                ->join('products as p', 's.id_product', 'p.id')
                ->join('product_types as pt', 'p.id_type_product', 'pt.id')
                ->select('p.id', 'p.name')
                ->where('s.stock_other_units', '>', 0)
                ->where('p.id_companie', Auth::user()->companie);

            $querieDestiny = DB::table('products as p')
                ->join('product_types as pt', 'p.id_type_product', 'pt.id')
                ->select('p.id', 'p.name')
                ->where('p.profile_job_center_id', $destinity);

            if ($type == 1) {
                $products = $querie->where('s.id_job_center', $id)->get();
            }
            else if ($type == 3) {
                $querie->where('pt.name', '<>', 'Plaguicida');
                $querie->where('pt.name', '<>', 'Insumo');
                $products = $querie->where('s.id_employee', $id)->get();
            }
            else {
                $products = $querie->where('s.id_employee', $id)->get();
            }

        } catch (Exception $th) {
            return response()->json(['errors' => 500]);
        }

        return response()->json([
            'products' => $products,
            'productsDestiny' => $querieDestiny->get()
        ]);
    }

    public function createTransferCC()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $products = DB::table('products')->where('id_companie', Auth::user()->companie)->orderBy('name','asc')->get();
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->orderBy('name','asc')->get(); //Arreglo para select de job centers

        return view('vendor.adminlte.transfers._createCC')->with(['products' => $products, 'jobCenters' => $jobCenters]);
    }

    public function createTransferCE()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->orderBy('name','asc')->get();
        $employees = employee::where('id_company', Auth::user()->companie)->where('status', '<>', 200)
            ->where('status', '<>', 0)->orderBy('name','asc')->get();
        if ($treeJobCenterMain->parent != '#') {
            $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->orderBy('name','asc')->get();
            $employees = employee::where('profile_job_center_id', $employee->profile_job_center_id)->where('status', '<>', 200)
                ->where('status', '<>', 0)->orderBy('name','asc')->get();
        }
        $products = DB::table('products')->where('id_companie', Auth::user()->companie)->orderBy('name','asc')->get();

        return view('vendor.adminlte.transfers._createCE')->with(['products' => $products, 'jobCenters' => $jobCenters, 'employees' => $employees]);
    }

    public function createTransferEE()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $products = DB::table('products')->where('id_companie', Auth::user()->companie)->orderBy('name','asc')->get();
        $employees = employee::where('id_company', Auth::user()->companie)->where('status', '<>', 200)->orderBy('name','asc')->get();
        $employeesDestiny = employee::where('id_company', Auth::user()->companie)->where('status', '<>', 200)
            ->where('status', '<>', 0)->orderBy('name','asc')->get();

        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        if ($treeJobCenterMain->parent != '#') {
            $employees = employee::where('profile_job_center_id', $employee->profile_job_center_id)->where('status', '<>', 200)->orderBy('name','asc')->get();
            $employeesDestiny = employee::where('profile_job_center_id', $employee->profile_job_center_id)->where('status', '<>', 200)
                ->where('status', '<>', 0)->orderBy('name','asc')->get();
        }

        return view('vendor.adminlte.transfers._createEE')->with(['products' => $products, 'employees' => $employees, 'employeesDestiny' => $employeesDestiny]);
    }

    public function createTransferEC()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $products = DB::table('products')->where('id_companie', Auth::user()->companie)->orderBy('name','asc')->get();
        $employees = employee::where('id_company', Auth::user()->companie)->where('status', '<>', 200)->orderBy('name','asc')->get(); //Arreglo para el select de Empleados
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->orderBy('name','asc')->get();

        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        if ($treeJobCenterMain->parent != '#') {
            $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->orderBy('name','asc')->get();
            $employees = employee::where('profile_job_center_id', $employee->profile_job_center_id)->where('status', '<>', 200)->orderBy('name','asc')->get();
        }

        return view('vendor.adminlte.transfers._createEC')->with(['products' => $products, 'employees' => $employees, 'jobCenters' => $jobCenters]);
    }

    public function createTransferCS()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $products = DB::table('products')->where('id_companie', Auth::user()->companie)->orderBy('name','asc')->get();
        $employees = employee::where('id_company', Auth::user()->companie)->where('status', 200)->orderBy('name','asc')->get();
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->orderBy('name','asc')->get();

        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        if ($treeJobCenterMain->parent != '#') {
            $jobCenters = DB::table('profile_job_centers')->where('id', $employee->profile_job_center_id)->orderBy('name','asc')->get();
        }

        return view('vendor.adminlte.transfers._createCS')->with(['products' => $products, 'employees' => $employees, 'jobCenters' => $jobCenters]);
    }

    public function createTransferCEM()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $products = DB::table('products')->where('id_companie', Auth::user()->companie)->orderBy('name','asc')->get();
        $employees = employee::where('id_company', Auth::user()->companie)->where('status', '<>', 200)
            ->where('status', '<>', 0)->orderBy('name','asc')->get();
        $storehouseCustomer = employee::where('id_company', Auth::user()->companie)->where('status', 200)->orderBy('name','asc')->get();

        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        if ($treeJobCenterMain->parent != '#') {
            $employees = employee::where('profile_job_center_id', $employee->profile_job_center_id)->where('status', '<>', 200)
                ->where('status', '<>', 0)->orderBy('name','asc')->get();
        }

        return view('vendor.adminlte.transfers._createCEM')->with(['products' => $products, 'employees' => $employees, 'storehouseCustomer' => $storehouseCustomer]);
    }

    //PARTE 1 TRASPASOS SUCURSAL A SUSCURSAL

    public function add_transfer_ss(Request $request)
    {

        try {

            DB::beginTransaction();
            
            $products = $request->get('selectProductsCS');
            $productsDestiny = $request->get('selectProductsCSDestiny');
        
            $transfer = new transfer_center_center();
            $transfer->id_transfer_cc = CommonInventory::getFolioTransferSS();
            $transfer->id_user = auth()->user()->id;
            $transfer->id_job_center_origin = $request->selectJobCentersOrigin;
            $transfer->id_job_center_destiny = $request->selectJobCentersDestiny;
            $transfer->companie = Auth::user()->companie;
            $transfer->save();

            $source = profile_job_center::find($request->selectJobCentersOrigin)->name;
            $destiny = profile_job_center::find($request->selectJobCentersDestiny)->name;

            $total = 0;

            foreach ($products as $key => $product) {
                
                $quantity = $request->get('quantityCS')[$key];
                $stock = $request->get('stockCS')[$key];
                $units = $request->get('unitsCS')[$key];
                $price = $request->get('priceBaseCS')[$key];
                $typeTransfer = $request->get('typeTransfer')[$key];

                $transferCCProduct = new transfer_cc_product();
                $transferCCProduct->id_transfer_cc = $transfer->id;
                $transferCCProduct->id_product = $product;
                $transferCCProduct->units = $units;
                $transferCCProduct->is_units = $typeTransfer;
                $transferCCProduct->save();

                $quantityProduct = DB::table('products')->select('quantity')->where('id', $product)->first();

                if ($typeTransfer == 0) {
                    $totalStore = ($price / $quantityProduct->quantity) * $units;
                    $unistCalculate = $units / $quantityProduct->quantity;
                    $quantityProduct = $units;
                    $units = ceil($unistCalculate);
                    $units = intval($units);
                } 
                else {
                    $totalStore = $price * $units;
                    $quantityProduct = $quantityProduct->quantity * $units;
                }

                $total += $totalStore;

                //update storehouse origin
                $storehouseOriginInventory = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersOrigin)
                    ->where('id_product', $product)
                    ->first();

                // History Inventory Before source
                $sourceHistoricalInventory = new HistoricalInventory();
                $sourceHistoricalInventory->folio = $transfer->id_transfer_cc . '-O';
                $sourceHistoricalInventory->date = Carbon::now()->toDateString();
                $sourceHistoricalInventory->hour = Carbon::now()->toTimeString();
                $sourceHistoricalInventory->id_user = Auth::id();
                $sourceHistoricalInventory->id_type_movement_inventory = 4;
                $sourceHistoricalInventory->id_product = $product;
                $sourceHistoricalInventory->source = $source;
                $sourceHistoricalInventory->destiny = $destiny;
                $sourceHistoricalInventory->quantity = $units;
                $sourceHistoricalInventory->fraction_quantity = $quantityProduct;
                $sourceHistoricalInventory->before_stock = $storehouseOriginInventory->stock;
                $sourceHistoricalInventory->after_stock = $storehouseOriginInventory->stock - $units;
                $sourceHistoricalInventory->fraction_before_stock = $storehouseOriginInventory->stock_other_units;
                $sourceHistoricalInventory->fraction_after_stock = $storehouseOriginInventory->stock_other_units - $quantityProduct;
                $sourceHistoricalInventory->unit_price = $price;
                $sourceHistoricalInventory->value_movement = $price * $units;
                $sourceHistoricalInventory->value_inventory = $price * ($storehouseOriginInventory->stock - $units);
                $sourceHistoricalInventory->id_profile_job_center = $request->selectJobCentersOrigin;
                $sourceHistoricalInventory->id_company = Auth::user()->companie;
                $sourceHistoricalInventory->save();

                // History Inventory Before destiny
                $destinyHistoricalInventory = new HistoricalInventory();
                $destinyHistoricalInventory->folio = $transfer->id_transfer_cc . '-D';
                $destinyHistoricalInventory->date = Carbon::now()->toDateString();
                $destinyHistoricalInventory->hour = Carbon::now()->toTimeString();
                $destinyHistoricalInventory->id_user = Auth::id();
                $destinyHistoricalInventory->id_type_movement_inventory = 4;
                $destinyHistoricalInventory->id_product = $productsDestiny[$key];
                $destinyHistoricalInventory->source = $source;
                $destinyHistoricalInventory->destiny = $destiny;
                $destinyHistoricalInventory->quantity = $units;
                $destinyHistoricalInventory->fraction_quantity = $quantityProduct;
                $destinyHistoricalInventory->before_stock = $units;
                $destinyHistoricalInventory->after_stock = $units;
                $destinyHistoricalInventory->fraction_before_stock = $quantityProduct;
                $destinyHistoricalInventory->fraction_after_stock = $quantityProduct;
                $destinyHistoricalInventory->unit_price = $price;
                $destinyHistoricalInventory->value_movement = $price * $units;
                $destinyHistoricalInventory->value_inventory = $price * $units;
                $destinyHistoricalInventory->id_profile_job_center = $request->selectJobCentersOrigin;
                $destinyHistoricalInventory->id_company = Auth::user()->companie;
                $destinyHistoricalInventory->save();

                //update storehouse destinity
                $isStorehouse = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersDestiny)
                    ->where('id_product', $productsDestiny[$key])
                    ->first();

                if (empty($isStorehouse)) {
                    $storehouse = new storehouse();
                    $storehouse->id_store = "A-0001";
                    $storehouse->id_job_center = $request->selectJobCentersDestiny;
                    $storehouse->id_product = $productsDestiny[$key];
                    $storehouse->stock = $units;
                    $storehouse->stock_other_units = $quantityProduct;
                    $storehouse->total = $totalStore;
                    $storehouse->companie = Auth::user()->companie;
                    $storehouse->save();
                    $storehouseId = storehouse::find($storehouse->id);
                    $storehouseId->id_store = "A-" . $storehouse->id;
                    $storehouseId->save();
                }else{
                    // Calculate stock units
                    $storehouseOrigin = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersDestiny)
                        ->where('id_product', $productsDestiny[$key])
                        ->first();
                    $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $productsDestiny[$key])->first();
                    $stockFinal = $storehouseOrigin->stock_other_units + $quantityProduct;
                    $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                    $stockFinal = ceil($stockFinal);
                    $stockFinal = intval($stockFinal);

                    $storehouse = storehouse::find($isStorehouse->id);
                    // update Historical Inventory source
                    $sourceUpdateHistoricalInventory = HistoricalInventory::find($sourceHistoricalInventory->id);
                    $sourceUpdateHistoricalInventory->before_stock = $storehouseOriginInventory->stock;
                    $sourceUpdateHistoricalInventory->fraction_before_stock = $storehouseOriginInventory->stock_other_units;
                    $sourceUpdateHistoricalInventory->after_stock = $storehouseOriginInventory->stock - $units;
                    $sourceUpdateHistoricalInventory->fraction_after_stock = $storehouseOriginInventory->stock_other_units - $quantityProduct;
                    $sourceUpdateHistoricalInventory->unit_price = $price;
                    $sourceUpdateHistoricalInventory->value_movement = $price * $units;
                    $sourceUpdateHistoricalInventory->value_inventory = ($storehouseOriginInventory->stock - $units) * $price;
                    $sourceUpdateHistoricalInventory->save();

                    // update Historical Inventory Destiny
                    $updateHistoricalInventory = HistoricalInventory::find($destinyHistoricalInventory->id);
                    $updateHistoricalInventory->before_stock = $storehouse->stock;
                    $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                    $updateHistoricalInventory->after_stock = $storehouse->stock + $units;
                    $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + $quantityProduct;
                    $updateHistoricalInventory->unit_price = $price;
                    $updateHistoricalInventory->value_movement = $price * $units;
                    $updateHistoricalInventory->value_inventory = ($storehouse->stock + $units) * $price;
                    $updateHistoricalInventory->save();

                    $storehouse->stock = $stockFinal;
                    $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                    $storehouse->total = $storehouse->total + $totalStore;
                    $storehouse->save();
                }

                //update storehouse origin
                $storehouseOrigin = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersOrigin)
                    ->where('id_product', $product)
                    ->first();
                $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                // Calculate stock units
                $stockFinal = $storehouseOrigin->stock_other_units - $quantityProduct;
                $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);

                $storehouseO = storehouse::find($storehouseOrigin->id);
                $storehouseO->stock = $stockFinal;
                $storehouseO->stock_other_units = $storehouseO->stock_other_units - $quantityProduct;
                $storehouseO->total = $storehouseO->total - $totalStore;
                $storehouseO->save();

            }

            $entryTransfer = transfer_center_center::find($transfer->id);
            $entryTransfer->total = $total;
            $entryTransfer->save();

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (Exception $th) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    //PARTE 2 TRASPASOS SUCURSAL A EMPLEADOS

    public function add_transfer_se(Request $request)
    {

        try {
            
            DB::beginTransaction();

            $products = $request->get('selectProductsCS');
        
            $transfer = new transfer_center_employee();
            $transfer->id_transfer_ce = CommonInventory::getFolioTransferSE();
            $transfer->id_user = auth()->user()->id;
            $transfer->id_job_center_origin = $request->selectJobCentersOrigin;
            $transfer->id_employee_destiny = $request->selectEmployeesDestiny;
            $transfer->save();
            $source = profile_job_center::find($request->selectJobCentersOrigin)->name;
            $destiny = employee::find($request->selectEmployeesDestiny)->name;

            $total = 0;

            foreach ($products as $key => $product) {

                $units = $request->get('unitsCS')[$key];
                 // storehouse origin
                $storehouseOriginPrice = storehouse::where('id_job_center', $request->selectJobCentersOrigin)
                    ->where('id_product', $product)
                    ->first();
                $price = 0;
                $typeTransfer = $request->get('typeTransfer')[$key];

                $transferCCProduct = new transfer_ce_product();
                $transferCCProduct->id_transfer_ce = $transfer->id;
                $transferCCProduct->id_product = $product;
                $transferCCProduct->units = $units;
                $transferCCProduct->is_units = $typeTransfer;
                $transferCCProduct->save();

                $productTransfer = DB::table('products')
                    ->select('quantity')
                    ->where('id', $product)
                    ->first();

                $price = $storehouseOriginPrice->total / $storehouseOriginPrice->stock_other_units;
                if ($typeTransfer == 0) {
                    $totalStore = $price * $units;
                    $unistCalculate = $units / $productTransfer->quantity;
                    $quantityProduct = $units;
                    $units = ceil($unistCalculate);
                    $units = intval($units);
                }
                else {
                    $totalStore = $price * $units;
                    $quantityProduct = $productTransfer->quantity * $units;
                }

                $total += $totalStore;
                $valueUnitPrice = $price;
                $valueMovement = $price * $quantityProduct;

                // History Inventory Before destiny
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = $transfer->id_transfer_ce . '-D';
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = Auth::id();
                $historicalInventory->id_type_movement_inventory = 5;
                $historicalInventory->id_product = $product;
                $historicalInventory->source = $source;
                $historicalInventory->destiny = $destiny;
                $historicalInventory->quantity = $units;
                $historicalInventory->fraction_quantity = $quantityProduct;
                $historicalInventory->before_stock = $units;
                $historicalInventory->after_stock = $units;
                $historicalInventory->fraction_before_stock = $quantityProduct;
                $historicalInventory->fraction_after_stock = $quantityProduct;
                $historicalInventory->unit_price = $valueUnitPrice;
                $historicalInventory->value_movement = $valueMovement;
                $historicalInventory->value_inventory = $valueMovement;
                $historicalInventory->is_destiny = 1;
                $historicalInventory->id_profile_job_center = $request->selectJobCentersOrigin;
                $historicalInventory->id_company = Auth::user()->companie;
                $historicalInventory->save();

                //update storehouse destinity
                $isStorehouse = DB::table('storehouses')->where('id_employee', $request->selectEmployeesDestiny)
                    ->where('id_product', $product)
                    ->first();
                if (empty($isStorehouse)) {
                    $storehouse = new storehouse();
                    $storehouse->id_store = "A-0001";
                    $storehouse->id_employee = $request->selectEmployeesDestiny;
                    $storehouse->id_product = $product;
                    $storehouse->stock = $units;
                    $storehouse->stock_other_units = $quantityProduct;
                    $storehouse->total = $valueMovement;
                    $storehouse->companie = Auth::user()->companie;
                    $storehouse->save();
                    $storehouseId = storehouse::find($storehouse->id);
                    $storehouseId->id_store = "A-" . $storehouse->id;
                    $storehouseId->save();

                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = 0;
                    $updateHistoricalInventory->fraction_before_stock = 0;
                    $updateHistoricalInventory->save();
                }
                else{
                    // Calculate stock units
                    $storehouseOrigin = DB::table('storehouses')->where('id_employee', $request->selectEmployeesDestiny)
                        ->where('id_product', $product)
                        ->first();
                    $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                    $stockFinal = $storehouseOrigin->stock_other_units + $quantityProduct;
                    $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                    $stockFinal = ceil($stockFinal);
                    $stockFinal = intval($stockFinal);

                    $storehouse = storehouse::find($isStorehouse->id);

                    // update Historical Inventory Destiny
                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = $storehouse->stock;
                    $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                    $updateHistoricalInventory->after_stock = $stockFinal;
                    $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + $quantityProduct;
                    $updateHistoricalInventory->unit_price = $valueUnitPrice;
                    $updateHistoricalInventory->value_movement = $valueMovement;
                    $updateHistoricalInventory->value_inventory = $storehouse->total + $valueMovement;
                    $updateHistoricalInventory->save();

                    $storehouse->stock = $stockFinal;
                    $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                    if ($storehouse->stock == 0 && $storehouse->stock_other_units == 0) $storehouse->total = 0;
                    else $storehouse->total = $storehouse->total + $valueMovement;
                    $storehouse->save();
                }

                // History Inventory Before origin
                $historicalInventoryOrigin = new HistoricalInventory();
                $historicalInventoryOrigin->folio = $transfer->id_transfer_ce . '-O';
                $historicalInventoryOrigin->date = Carbon::now()->toDateString();
                $historicalInventoryOrigin->hour = Carbon::now()->toTimeString();
                $historicalInventoryOrigin->id_user = Auth::id();
                $historicalInventoryOrigin->id_type_movement_inventory = 5;
                $historicalInventoryOrigin->id_product = $product;
                $historicalInventoryOrigin->source = $source;
                $historicalInventoryOrigin->destiny = $destiny;
                $historicalInventoryOrigin->quantity = $units;
                $historicalInventoryOrigin->fraction_quantity = $quantityProduct;
                $historicalInventoryOrigin->before_stock = $units;
                $historicalInventoryOrigin->after_stock = $units;
                $historicalInventoryOrigin->fraction_before_stock = $quantityProduct;
                $historicalInventoryOrigin->fraction_after_stock = $quantityProduct;
                $historicalInventoryOrigin->unit_price = $valueUnitPrice;
                $historicalInventoryOrigin->value_movement = $valueMovement;
                $historicalInventoryOrigin->value_inventory = $valueMovement;
                $historicalInventoryOrigin->is_destiny = 0;
                $historicalInventoryOrigin->id_profile_job_center = $request->selectJobCentersOrigin;
                $historicalInventoryOrigin->id_company = Auth::user()->companie;
                $historicalInventoryOrigin->save();

                //update storehouse origin
                $storehouseOrigin = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersOrigin)
                    ->where('id_product', $product)
                    ->first();
                $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                // Calculate stock units
                $stockFinal = $storehouseOrigin->stock_other_units - $quantityProduct;
                $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);

                $storehouseO = storehouse::find($storehouseOrigin->id);

                // update Historical Inventory Origin
                $updateHistoricalInventoryOrigin = HistoricalInventory::find($historicalInventoryOrigin->id);
                $updateHistoricalInventoryOrigin->before_stock = $storehouseO->stock;
                $updateHistoricalInventoryOrigin->fraction_before_stock = $storehouseO->stock_other_units;
                $updateHistoricalInventoryOrigin->after_stock = $stockFinal;
                $updateHistoricalInventoryOrigin->fraction_after_stock = $storehouseO->stock_other_units - $quantityProduct;
                $updateHistoricalInventoryOrigin->unit_price = $valueUnitPrice;
                $updateHistoricalInventoryOrigin->value_movement = $valueMovement;
                $updateHistoricalInventoryOrigin->value_inventory = $storehouseO->total - $valueMovement;
                $updateHistoricalInventoryOrigin->save();

                $storehouseO->stock = $stockFinal;
                $storehouseO->stock_other_units = $storehouseO->stock_other_units - $quantityProduct;
                if ($storehouseO->stock == 0 && $storehouseO->stock_other_units == 0) $storehouseO->total = 0;
                else $storehouseO->total = $storehouseO->total - $valueMovement;
                $storehouseO->save();

            }

            $entryTransfer = transfer_center_employee::find($transfer->id);
            $entryTransfer->total = $total;
            $entryTransfer->save();

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (Exception $th) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    //PARTE 3 TRASPASOS EMPLEADO A EMPLEADO

    public function add_transfer_ee(Request $request)
    {

        try {
            
            DB::beginTransaction();

            $products = $request->get('selectProductsCS');

            $transfer = new transfer_employee_employee();
            $transfer->id_transfer_ee = CommonInventory::getFolioTransferEE();
            $transfer->id_user = auth()->user()->id;
            $transfer->id_employee_origin = $request->selectEmployeesOrigin;
            $transfer->id_employee_destiny = $request->selectEmployeesDestiny;
            $transfer->companie = Auth::user()->companie;
            $transfer->save();
            $source = employee::find($request->selectEmployeesOrigin)->name;
            $destiny = employee::find($request->selectEmployeesDestiny)->name;

            $total = 0;

            foreach ($products as $key => $product) {

                $storehouseOriginPrice = storehouse::where('id_employee', $request->selectEmployeesOrigin)
                    ->where('id_product', $product)
                    ->first();
                $price = 0;
                $units = $request->get('unitsCS')[$key];
                $typeTransfer = $request->get('typeTransfer')[$key];

                $transferCCProduct = new transfer_ee_product();
                $transferCCProduct->id_transfer_ee = $transfer->id;
                $transferCCProduct->id_product = $product;
                $transferCCProduct->units = $units;
                $transferCCProduct->is_units = $typeTransfer;
                $transferCCProduct->save();

                $productTransfer = DB::table('products')
                    ->select('quantity','profile_job_center_id')
                    ->where('id', $product)
                    ->first();

                $price = $storehouseOriginPrice->total / $storehouseOriginPrice->stock_other_units;
                if ($typeTransfer == 0) {
                    $totalStore = $price * $units;
                    $unistCalculate = $units / $productTransfer->quantity;
                    $quantityProduct = $units;
                    $units = ceil($unistCalculate);
                    $units = intval($units);
                } 
                else {
                    $totalStore = $price * $units;
                    $quantityProduct = $productTransfer->quantity * $units;
                }

                $total += $totalStore;
                $valueUnitPrice = $price;
                $valueMovement = $price * $quantityProduct;

                // History Inventory Before destiny
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = $transfer->id_transfer_ee . '-D';
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = Auth::id();
                $historicalInventory->id_type_movement_inventory = 6;
                $historicalInventory->id_product = $product;
                $historicalInventory->source = $source;
                $historicalInventory->destiny = $destiny;
                $historicalInventory->quantity = $units;
                $historicalInventory->fraction_quantity = $quantityProduct;
                $historicalInventory->before_stock = $units;
                $historicalInventory->after_stock = $units;
                $historicalInventory->fraction_before_stock = $quantityProduct;
                $historicalInventory->fraction_after_stock = $quantityProduct;
                $historicalInventory->unit_price = $valueUnitPrice;
                $historicalInventory->value_movement = $valueMovement;
                $historicalInventory->value_inventory = $valueMovement;
                $historicalInventory->is_destiny = 1;
                $historicalInventory->id_profile_job_center = $productTransfer->profile_job_center_id;
                $historicalInventory->id_company = Auth::user()->companie;
                $historicalInventory->save();

                //update storehouse destinity
                $isStorehouse = DB::table('storehouses')->where('id_employee', $request->selectEmployeesDestiny)
                    ->where('id_product', $product)
                    ->first();
                if (empty($isStorehouse)) {
                    $storehouse = new storehouse();
                    $storehouse->id_store = "A-0001";
                    $storehouse->id_employee = $request->selectEmployeesDestiny;
                    $storehouse->id_product = $product;
                    $storehouse->stock = $units;
                    $storehouse->stock_other_units = $quantityProduct;
                    $storehouse->total = $valueMovement;
                    $storehouse->companie = Auth::user()->companie;
                    $storehouse->save();
                    $storehouseId = storehouse::find($storehouse->id);
                    $storehouseId->id_store = "A-" . $storehouse->id;
                    $storehouseId->save();

                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = 0;
                    $updateHistoricalInventory->fraction_before_stock = 0;
                    $updateHistoricalInventory->save();
                }else{
                    // Calculate stock units
                    $storehouseOrigin = DB::table('storehouses')->where('id_employee', $request->selectEmployeesDestiny)
                        ->where('id_product', $product)
                        ->first();
                    $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                    $stockFinal = $storehouseOrigin->stock_other_units + $quantityProduct;
                    $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                    $stockFinal = ceil($stockFinal);
                    $stockFinal = intval($stockFinal);

                    $storehouse = storehouse::find($isStorehouse->id);

                    // update Historical Inventory Destiny
                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = $storehouse->stock;
                    $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                    $updateHistoricalInventory->after_stock = $stockFinal;
                    $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + $quantityProduct;
                    $updateHistoricalInventory->unit_price = $valueUnitPrice;
                    $updateHistoricalInventory->value_movement = $valueMovement;
                    $updateHistoricalInventory->value_inventory = $storehouse->total + $valueMovement;
                    $updateHistoricalInventory->save();

                    $storehouse->stock = $stockFinal;
                    $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                    if ($storehouse->stock == 0 && $storehouse->stock_other_units == 0) $storehouse->total = 0;
                    else $storehouse->total = $storehouse->total + $valueMovement;
                    $storehouse->save();
                }

                // History Inventory Before Origin
                $historicalInventoryOrigin = new HistoricalInventory();
                $historicalInventoryOrigin->folio = $transfer->id_transfer_ee . '-O';
                $historicalInventoryOrigin->date = Carbon::now()->toDateString();
                $historicalInventoryOrigin->hour = Carbon::now()->toTimeString();
                $historicalInventoryOrigin->id_user = Auth::id();
                $historicalInventoryOrigin->id_type_movement_inventory = 6;
                $historicalInventoryOrigin->id_product = $product;
                $historicalInventoryOrigin->source = $source;
                $historicalInventoryOrigin->destiny = $destiny;
                $historicalInventoryOrigin->quantity = $units;
                $historicalInventoryOrigin->fraction_quantity = $quantityProduct;
                $historicalInventoryOrigin->before_stock = $units;
                $historicalInventoryOrigin->after_stock = $units;
                $historicalInventoryOrigin->fraction_before_stock = $quantityProduct;
                $historicalInventoryOrigin->fraction_after_stock = $quantityProduct;
                $historicalInventoryOrigin->unit_price = $valueUnitPrice;
                $historicalInventoryOrigin->value_movement = $valueMovement;
                $historicalInventoryOrigin->value_inventory = $valueMovement;
                $historicalInventoryOrigin->is_destiny = 0;
                $historicalInventoryOrigin->id_profile_job_center = $productTransfer->profile_job_center_id;
                $historicalInventoryOrigin->id_company = Auth::user()->companie;
                $historicalInventoryOrigin->save();

                //update storehouse origin
                $storehouseOrigin = DB::table('storehouses')->where('id_employee', $request->selectEmployeesOrigin)
                    ->where('id_product', $product)
                    ->first();

                $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                // Calculate stock units
                $stockFinal = $storehouseOrigin->stock_other_units - $quantityProduct;
                $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);

                $storehouseO = storehouse::find($storehouseOrigin->id);

                // update Historical Inventory Origin
                $updateHistoricalInventoryOrigin = HistoricalInventory::find($historicalInventoryOrigin->id);
                $updateHistoricalInventoryOrigin->before_stock = $storehouseO->stock;
                $updateHistoricalInventoryOrigin->fraction_before_stock = $storehouseO->stock_other_units;
                $updateHistoricalInventoryOrigin->after_stock = $stockFinal;
                $updateHistoricalInventoryOrigin->fraction_after_stock = $storehouseO->stock_other_units - $quantityProduct;
                $updateHistoricalInventoryOrigin->unit_price = $valueUnitPrice;
                $updateHistoricalInventoryOrigin->value_movement = $valueMovement;
                $updateHistoricalInventoryOrigin->value_inventory = $storehouseO->total - $valueMovement;
                $updateHistoricalInventoryOrigin->save();

                $storehouseO->stock = $stockFinal;
                $storehouseO->stock_other_units = $storehouseO->stock_other_units - $quantityProduct;
                if ($storehouseO->stock == 0 && $storehouseO->stock_other_units == 0) $storehouseO->total = 0;
                else $storehouseO->total = $storehouseO->total - $valueMovement;
                $storehouseO->save();
            }

            $entryTransfer = transfer_employee_employee::find($transfer->id);
            $entryTransfer->total = $total;
            $entryTransfer->save();

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (Exception $th) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    //PARTE 3 TRASPASOS EMPLEADO A SUCURSAL

    public function add_transfer_ec(Request $request)
    {

        try {

            DB::beginTransaction();

            $products = $request->get('selectProductsCS');

            $transfer = new transfer_employee_center();
            $transfer->id_transfer_ec = CommonInventory::getFolioTransferEC();
            $transfer->id_user = auth()->user()->id;
            $transfer->id_employee_origin = $request->selectEmployeesOrigin;
            $transfer->id_job_center_destiny = $request->selectJobCentersDestiny;
            $transfer->companie = Auth::user()->companie;
            $transfer->save();
            $source = employee::find($request->selectEmployeesOrigin)->name;
            $destiny = profile_job_center::find($request->selectJobCentersDestiny)->name;

            $total = 0;

            foreach ($products as $key => $product) {

                $storehouseOriginPrice = storehouse::where('id_employee', $request->selectEmployeesOrigin)
                    ->where('id_product', $product)
                    ->first();
                $price = 0;
                $units = $request->get('unitsCS')[$key];
                $typeTransfer = $request->get('typeTransfer')[$key];

                $transferCCProduct = new transfer_ec_product();
                $transferCCProduct->id_transfer_ec = $transfer->id;
                $transferCCProduct->id_product = $product;
                $transferCCProduct->units = $units;
                $transferCCProduct->is_units = $typeTransfer;
                $transferCCProduct->save();

                $productTransfer = DB::table('products')
                    ->select('quantity','profile_job_center_id')
                    ->where('id', $product)
                    ->first();

                $price = $storehouseOriginPrice->total / $storehouseOriginPrice->stock_other_units;
                if ($typeTransfer == 0) {
                    $totalStore = $price * $units;
                    $unistCalculate = $units / $productTransfer->quantity;
                    $quantityProduct = $units;
                    $units = ceil($unistCalculate);
                    $units = intval($units);
                } 
                else {
                    $totalStore = $price * $units;
                    $quantityProduct = $productTransfer->quantity * $units;
                }

                $total += $totalStore;
                $valueUnitPrice = $price;
                $valueMovement = $price * $quantityProduct;

                // History Inventory Before destiny
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = $transfer->id_transfer_ec . '-D';
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = Auth::id();
                $historicalInventory->id_type_movement_inventory = 7;
                $historicalInventory->id_product = $product;
                $historicalInventory->source = $source;
                $historicalInventory->destiny = $destiny;
                $historicalInventory->quantity = $units;
                $historicalInventory->fraction_quantity = $quantityProduct;
                $historicalInventory->before_stock = $units;
                $historicalInventory->after_stock = $units;
                $historicalInventory->fraction_before_stock = $quantityProduct;
                $historicalInventory->fraction_after_stock = $quantityProduct;
                $historicalInventory->unit_price = $valueUnitPrice;
                $historicalInventory->value_movement = $valueMovement;
                $historicalInventory->value_inventory = $valueMovement;
                $historicalInventory->is_destiny = 1;
                $historicalInventory->id_profile_job_center = $productTransfer->profile_job_center_id;
                $historicalInventory->id_company = Auth::user()->companie;
                $historicalInventory->save();

                //update storehouse destinity
                $isStorehouse = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersDestiny)
                    ->where('id_product', $product)
                    ->first();
                if (empty($isStorehouse)) {
                    $storehouse = new storehouse();
                    $storehouse->id_store = "A-0001";
                    $storehouse->id_job_center = $request->selectJobCentersDestiny;
                    $storehouse->id_product = $product;
                    $storehouse->stock = $units;
                    $storehouse->stock_other_units = $quantityProduct;
                    $storehouse->total = $valueMovement;
                    $storehouse->companie = Auth::user()->companie;
                    $storehouse->save();
                    $storehouseId = storehouse::find($storehouse->id);
                    $storehouseId->id_store = "A-" . $storehouse->id;
                    $storehouseId->save();

                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = 0;
                    $updateHistoricalInventory->fraction_before_stock = 0;
                    $updateHistoricalInventory->save();
                }
                else{
                    // Calculate stock units
                    $storehouseOrigin = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersDestiny)
                        ->where('id_product', $product)
                        ->first();
                    $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                    $stockFinal = $storehouseOrigin->stock_other_units + $quantityProduct;
                    $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                    $stockFinal = ceil($stockFinal);
                    $stockFinal = intval($stockFinal);

                    $storehouse = storehouse::find($isStorehouse->id);

                    // update Historical Inventory Destiny
                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = $storehouse->stock;
                    $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                    $updateHistoricalInventory->after_stock = $stockFinal;
                    $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + $quantityProduct;
                    $updateHistoricalInventory->unit_price = $valueUnitPrice;
                    $updateHistoricalInventory->value_movement = $valueMovement;
                    $updateHistoricalInventory->value_inventory = $storehouse->total + $valueMovement;
                    $updateHistoricalInventory->save();

                    $storehouse->stock = $stockFinal;
                    $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                    if ($storehouse->stock == 0 && $storehouse->stock_other_units == 0) $storehouse->total = 0;
                    else $storehouse->total = $storehouse->total + $valueMovement;
                    $storehouse->save();
                }

                // History Inventory Before Origin
                $historicalInventoryOrigin = new HistoricalInventory();
                $historicalInventoryOrigin->folio = $transfer->id_transfer_ec . '-O';
                $historicalInventoryOrigin->date = Carbon::now()->toDateString();
                $historicalInventoryOrigin->hour = Carbon::now()->toTimeString();
                $historicalInventoryOrigin->id_user = Auth::id();
                $historicalInventoryOrigin->id_type_movement_inventory = 7;
                $historicalInventoryOrigin->id_product = $product;
                $historicalInventoryOrigin->source = $source;
                $historicalInventoryOrigin->destiny = $destiny;
                $historicalInventoryOrigin->quantity = $units;
                $historicalInventoryOrigin->fraction_quantity = $quantityProduct;
                $historicalInventoryOrigin->before_stock = $units;
                $historicalInventoryOrigin->after_stock = $units;
                $historicalInventoryOrigin->fraction_before_stock = $quantityProduct;
                $historicalInventoryOrigin->fraction_after_stock = $quantityProduct;
                $historicalInventoryOrigin->unit_price = $valueUnitPrice;
                $historicalInventoryOrigin->value_movement = $valueMovement;
                $historicalInventoryOrigin->value_inventory = $valueMovement;
                $historicalInventoryOrigin->is_destiny = 0;
                $historicalInventoryOrigin->id_profile_job_center = $productTransfer->profile_job_center_id;
                $historicalInventoryOrigin->id_company = Auth::user()->companie;
                $historicalInventoryOrigin->save();

                //update storehouse origin
                $storehouseOrigin = DB::table('storehouses')->where('id_employee', $request->selectEmployeesOrigin)
                    ->where('id_product', $product)
                    ->first();
                $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                // Calculate stock units
                $stockFinal = $storehouseOrigin->stock_other_units - $quantityProduct;
                $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);

                $storehouseO = storehouse::find($storehouseOrigin->id);

                // update Historical Inventory Origin
                $updateHistoricalInventoryOrigin = HistoricalInventory::find($historicalInventoryOrigin->id);
                $updateHistoricalInventoryOrigin->before_stock = $storehouseO->stock;
                $updateHistoricalInventoryOrigin->fraction_before_stock = $storehouseO->stock_other_units;
                $updateHistoricalInventoryOrigin->after_stock = $stockFinal;
                $updateHistoricalInventoryOrigin->fraction_after_stock = $storehouseO->stock_other_units - $quantityProduct;
                $updateHistoricalInventoryOrigin->unit_price = $valueUnitPrice;
                $updateHistoricalInventoryOrigin->value_movement = $valueMovement;
                $updateHistoricalInventoryOrigin->value_inventory = $storehouseO->total - $valueMovement;
                $updateHistoricalInventoryOrigin->save();

                $storehouseO->stock = $stockFinal;
                $storehouseO->stock_other_units = $storehouseO->stock_other_units - $quantityProduct;
                if ($storehouseO->stock == 0 && $storehouseO->stock_other_units == 0) $storehouseO->total = 0;
                else $storehouseO->total = $storehouseO->total - $valueMovement;
                $storehouseO->save();
            }

            $entryTransfer = transfer_employee_center::find($transfer->id);
            $entryTransfer->total = $total;
            $entryTransfer->save();

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (Exception $th) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function addTransferCustomerToJobCenter(Request $request)
    {
        try {

            DB::beginTransaction();

            $products = $request->get('selectProductsCS');

            $transfer = new transfer_employee_center();
            $transfer->id_transfer_ec = 'TCLC-0000';
            $transfer->id_user = auth()->user()->id;
            $transfer->id_employee_origin = $request->selectEmployeesOrigin;
            $transfer->id_job_center_destiny = $request->selectJobCentersDestiny;
            $transfer->companie = Auth::user()->companie;
            $transfer->save();
            $source = employee::find($request->selectEmployeesOrigin)->name;
            $destiny = profile_job_center::find($request->selectJobCentersDestiny)->name;

            $total = 0;

            foreach ($products as $key => $product) {

                $storehouseOriginPrice = storehouse::where('id_employee', $request->selectEmployeesOrigin)
                    ->where('id_product', $product)
                    ->first();
                $price = 0;
                $units = $request->get('unitsCS')[$key];
                $typeTransfer = $request->get('typeTransfer')[$key];

                $transferCCProduct = new transfer_ec_product();
                $transferCCProduct->id_transfer_ec = $transfer->id;
                $transferCCProduct->id_product = $product;
                $transferCCProduct->units = $units;
                $transferCCProduct->is_units = $typeTransfer;
                $transferCCProduct->save();

                $productTransfer = DB::table('products')
                    ->select('quantity','profile_job_center_id')
                    ->where('id', $product)
                    ->first();

                $price = $storehouseOriginPrice->total / $storehouseOriginPrice->stock_other_units;
                if ($typeTransfer == 0) {
                    $totalStore = $price * $units;
                    $unistCalculate = $units / $productTransfer->quantity;
                    $quantityProduct = $units;
                    $units = ceil($unistCalculate);
                    $units = intval($units);
                } 
                else {
                    $totalStore = $price * $units;
                    $quantityProduct = $productTransfer->quantity * $units;
                }

                $total += $totalStore;
                $valueUnitPrice = $price;
                $valueMovement = $price * $quantityProduct;

                // History Inventory Before destiny
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = 'TCLC-' . $transfer->id . '-D';
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = Auth::id();
                $historicalInventory->id_type_movement_inventory = 8;
                $historicalInventory->id_product = $product;
                $historicalInventory->source = $source;
                $historicalInventory->destiny = $destiny;
                $historicalInventory->quantity = $units;
                $historicalInventory->fraction_quantity = $quantityProduct;
                $historicalInventory->before_stock = $units;
                $historicalInventory->after_stock = $units;
                $historicalInventory->fraction_before_stock = $quantityProduct;
                $historicalInventory->fraction_after_stock = $quantityProduct;
                $historicalInventory->unit_price = $valueUnitPrice;
                $historicalInventory->value_movement = $valueMovement;
                $historicalInventory->value_inventory = $valueMovement;
                $historicalInventory->is_destiny = 1;
                $historicalInventory->id_profile_job_center = $productTransfer->profile_job_center_id;
                $historicalInventory->id_company = Auth::user()->companie;
                $historicalInventory->save();

                // Update storehouse destinity
                $isStorehouse = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersDestiny)
                    ->where('id_product', $product)
                    ->first();
                if (empty($isStorehouse)) {
                    $storehouse = new storehouse();
                    $storehouse->id_store = "A-0001";
                    $storehouse->id_job_center = $request->selectJobCentersDestiny;
                    $storehouse->id_product = $product;
                    $storehouse->stock = $units;
                    $storehouse->stock_other_units = $quantityProduct;
                    $storehouse->total = $valueMovement;
                    $storehouse->companie = Auth::user()->companie;
                    $storehouse->save();
                    $storehouseId = storehouse::find($storehouse->id);
                    $storehouseId->id_store = "A-" . $storehouse->id;
                    $storehouseId->save();

                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = 0;
                    $updateHistoricalInventory->fraction_before_stock = 0;
                    $updateHistoricalInventory->save();
                }
                else{
                    // Calculate stock units
                    $storehouseOrigin = DB::table('storehouses')->where('id_job_center', $request->selectJobCentersDestiny)
                        ->where('id_product', $product)
                        ->first();
                    $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                    $stockFinal = $storehouseOrigin->stock_other_units + $quantityProduct;
                    $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                    $stockFinal = ceil($stockFinal);
                    $stockFinal = intval($stockFinal);

                    $storehouse = storehouse::find($isStorehouse->id);

                    // update Historical Inventory Destiny
                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = $storehouse->stock;
                    $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                    $updateHistoricalInventory->after_stock = $stockFinal;
                    $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + $quantityProduct;
                    $updateHistoricalInventory->unit_price = $valueUnitPrice;
                    $updateHistoricalInventory->value_movement = $valueMovement;
                    $updateHistoricalInventory->value_inventory = $storehouse->total + $valueMovement;
                    $updateHistoricalInventory->save();

                    $storehouse->stock = $stockFinal;
                    $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                    if ($storehouse->stock = 0 && $storehouse->stock_other_units == 0) $storehouse->total = 0;
                    else $storehouse->total = $storehouse->total + $valueMovement;
                    $storehouse->save();
                }

                // History Inventory Before origin
                $historicalInventoryOrigin = new HistoricalInventory();
                $historicalInventoryOrigin->folio = 'TCLC-' . $transfer->id . '-O';
                $historicalInventoryOrigin->date = Carbon::now()->toDateString();
                $historicalInventoryOrigin->hour = Carbon::now()->toTimeString();
                $historicalInventoryOrigin->id_user = Auth::id();
                $historicalInventoryOrigin->id_type_movement_inventory = 8;
                $historicalInventoryOrigin->id_product = $product;
                $historicalInventoryOrigin->source = $source;
                $historicalInventoryOrigin->destiny = $destiny;
                $historicalInventoryOrigin->quantity = $units;
                $historicalInventoryOrigin->fraction_quantity = $quantityProduct;
                $historicalInventoryOrigin->before_stock = $units;
                $historicalInventoryOrigin->after_stock = $units;
                $historicalInventoryOrigin->fraction_before_stock = $quantityProduct;
                $historicalInventoryOrigin->fraction_after_stock = $quantityProduct;
                $historicalInventoryOrigin->unit_price = $valueUnitPrice;
                $historicalInventoryOrigin->value_movement = $valueMovement;
                $historicalInventoryOrigin->value_inventory = $valueMovement;
                $historicalInventoryOrigin->is_destiny = 0;
                $historicalInventoryOrigin->id_profile_job_center = $productTransfer->profile_job_center_id;
                $historicalInventoryOrigin->id_company = Auth::user()->companie;
                $historicalInventoryOrigin->save();

                // Update storehouse origin
                $storehouseOrigin = DB::table('storehouses')->where('id_employee', $request->selectEmployeesOrigin)
                    ->where('id_product', $product)
                    ->first();
                $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                // Calculate stock units
                $stockFinal = $storehouseOrigin->stock_other_units - $quantityProduct;
                $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);

                $storehouseO = storehouse::find($storehouseOrigin->id);

                // update Historical Inventory Origin
                $updateHistoricalInventoryOrigin = HistoricalInventory::find($historicalInventoryOrigin->id);
                $updateHistoricalInventoryOrigin->before_stock = $storehouseO->stock;
                $updateHistoricalInventoryOrigin->fraction_before_stock = $storehouseO->stock_other_units;
                $updateHistoricalInventoryOrigin->after_stock = $stockFinal;
                $updateHistoricalInventoryOrigin->fraction_after_stock = $storehouseO->stock_other_units - $quantityProduct;
                $updateHistoricalInventoryOrigin->unit_price = $valueUnitPrice;
                $updateHistoricalInventoryOrigin->value_movement = $valueMovement;
                $updateHistoricalInventoryOrigin->value_inventory = $storehouseO->total - $valueMovement;
                $updateHistoricalInventoryOrigin->save();

                $storehouseO->stock = $stockFinal;
                $storehouseO->stock_other_units = $storehouseO->stock_other_units - $quantityProduct;
                if ($storehouseO->stock == 0 && $storehouseO->stock_other_units == 0) $storehouseO->total = 0;
                else $storehouseO->total = $storehouseO->total - $valueMovement;
                $storehouseO->save();
            }

            $transferUpdate = transfer_employee_center::find($transfer->id);
            $transferUpdate->id_transfer_ec = 'TCLC-' . $transfer->id;
            $transferUpdate->total = $total;
            $transferUpdate->save();

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (Exception $th) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function addTransferCustomerToEmployee(Request $request)
    {
        try {

            DB::beginTransaction();

            $products = $request->get('selectProductsCS');

            $transfer = new transfer_employee_employee();
            $transfer->id_transfer_ee = 'TCLE-0000';
            $transfer->id_user = auth()->user()->id;
            $transfer->id_employee_origin = $request->selectEmployeesOrigin;
            $transfer->id_employee_destiny = $request->selectEmployeesDestiny;
            $transfer->companie = Auth::user()->companie;
            $transfer->save();
            $source = employee::find($request->selectEmployeesOrigin)->name;
            $destiny = employee::find($request->selectEmployeesDestiny)->name;

            $total = 0;

            foreach ($products as $key => $product) {

                $storehouseOriginPrice = storehouse::where('id_employee', $request->selectEmployeesOrigin)
                    ->where('id_product', $product)
                    ->first();
                $price = 0;
                $units = $request->get('unitsCS')[$key];
                $typeTransfer = $request->get('typeTransfer')[$key];

                $transferCCProduct = new transfer_ee_product();
                $transferCCProduct->id_transfer_ee = $transfer->id;
                $transferCCProduct->id_product = $product;
                $transferCCProduct->units = $units;
                $transferCCProduct->is_units = $typeTransfer;
                $transferCCProduct->save();

                $productTransfer = DB::table('products')
                    ->select('quantity','profile_job_center_id')
                    ->where('id', $product)
                    ->first();

                $price = $storehouseOriginPrice->total / $storehouseOriginPrice->stock_other_units;
                if ($typeTransfer == 0) {
                    $totalStore = $price * $units;
                    $unistCalculate = $units / $productTransfer->quantity;
                    $quantityProduct = $units;
                    $units = ceil($unistCalculate);
                    $units = intval($units);
                } 
                else {
                    $totalStore = $price * $units;
                    $quantityProduct = $productTransfer->quantity * $units;
                }

                $total += $totalStore;
                $valueUnitPrice = $price;
                $valueMovement = $price * $quantityProduct;

                // History Inventory Before destiny
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = 'TCLE-' . $transfer->id . '-D';
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = Auth::id();
                $historicalInventory->id_type_movement_inventory = 9;
                $historicalInventory->id_product = $product;
                $historicalInventory->source = $source;
                $historicalInventory->destiny = $destiny;
                $historicalInventory->quantity = $units;
                $historicalInventory->fraction_quantity = $quantityProduct;
                $historicalInventory->before_stock = $units;
                $historicalInventory->after_stock = $units;
                $historicalInventory->fraction_before_stock = $quantityProduct;
                $historicalInventory->fraction_after_stock = $quantityProduct;
                $historicalInventory->unit_price = $valueUnitPrice;
                $historicalInventory->value_movement = $valueMovement;
                $historicalInventory->value_inventory = $valueMovement;
                $historicalInventory->is_destiny = 1;
                $historicalInventory->id_profile_job_center = $productTransfer->profile_job_center_id;
                $historicalInventory->id_company = Auth::user()->companie;
                $historicalInventory->save();

                // Update storehouse destinity
                $isStorehouse = DB::table('storehouses')->where('id_employee', $request->selectEmployeesDestiny)
                    ->where('id_product', $product)
                    ->first();
                if (empty($isStorehouse)) {
                    $storehouse = new storehouse();
                    $storehouse->id_store = "A-0001";
                    $storehouse->id_employee = $request->selectEmployeesDestiny;
                    $storehouse->id_product = $product;
                    $storehouse->stock = $units;
                    $storehouse->stock_other_units = $quantityProduct;
                    $storehouse->total = $valueMovement;
                    $storehouse->companie = Auth::user()->companie;
                    $storehouse->save();
                    $storehouseId = storehouse::find($storehouse->id);
                    $storehouseId->id_store = "A-" . $storehouse->id;
                    $storehouseId->save();

                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = 0;
                    $updateHistoricalInventory->fraction_before_stock = 0;
                    $updateHistoricalInventory->save();
                }
                else{
                    // Calculate stock units
                    $storehouseOrigin = DB::table('storehouses')->where('id_employee', $request->selectEmployeesDestiny)
                        ->where('id_product', $product)
                        ->first();
                    $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                    $stockFinal = $storehouseOrigin->stock_other_units + $quantityProduct;
                    $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                    $stockFinal = ceil($stockFinal);
                    $stockFinal = intval($stockFinal);

                    $storehouse = storehouse::find($isStorehouse->id);

                    // update Historical Inventory Destiny
                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = $storehouse->stock;
                    $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                    $updateHistoricalInventory->after_stock = $stockFinal;
                    $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + $quantityProduct;
                    $updateHistoricalInventory->unit_price = $valueUnitPrice;
                    $updateHistoricalInventory->value_movement = $valueMovement;
                    $updateHistoricalInventory->value_inventory = $storehouse->total + $valueMovement;
                    $updateHistoricalInventory->save();

                    $storehouse->stock = $stockFinal;
                    $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                    if ($storehouse->stock == 0 && $storehouse->stock_other_units == 0) $storehouse->total = 0;
                    else $storehouse->total = $storehouse->total + $valueMovement;
                    $storehouse->save();
                }

                // History Inventory Before origin
                $historicalInventoryOrigin = new HistoricalInventory();
                $historicalInventoryOrigin->folio = 'TCLE-' . $transfer->id . '-O';
                $historicalInventoryOrigin->date = Carbon::now()->toDateString();
                $historicalInventoryOrigin->hour = Carbon::now()->toTimeString();
                $historicalInventoryOrigin->id_user = Auth::id();
                $historicalInventoryOrigin->id_type_movement_inventory = 9;
                $historicalInventoryOrigin->id_product = $product;
                $historicalInventoryOrigin->source = $source;
                $historicalInventoryOrigin->destiny = $destiny;
                $historicalInventoryOrigin->quantity = $units;
                $historicalInventoryOrigin->fraction_quantity = $quantityProduct;
                $historicalInventoryOrigin->before_stock = $units;
                $historicalInventoryOrigin->after_stock = $units;
                $historicalInventoryOrigin->fraction_before_stock = $quantityProduct;
                $historicalInventoryOrigin->fraction_after_stock = $quantityProduct;
                $historicalInventoryOrigin->unit_price = $valueUnitPrice;
                $historicalInventoryOrigin->value_movement = $valueMovement;
                $historicalInventoryOrigin->value_inventory = $valueMovement;
                $historicalInventoryOrigin->is_destiny = 0;
                $historicalInventoryOrigin->id_profile_job_center = $productTransfer->profile_job_center_id;
                $historicalInventoryOrigin->id_company = Auth::user()->companie;
                $historicalInventoryOrigin->save();

                // Update storehouse origin
                $storehouseOrigin = DB::table('storehouses')->where('id_employee', $request->selectEmployeesOrigin)
                    ->where('id_product', $product)
                    ->first();

                $quantityProductForCalculate = DB::table('products')->select('quantity')->where('id', $product)->first();
                // Calculate stock units
                $stockFinal = $storehouseOrigin->stock_other_units - $quantityProduct;
                $stockFinal = $stockFinal / $quantityProductForCalculate->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);    

                $storehouseO = storehouse::find($storehouseOrigin->id);

                // update Historical Inventory Origin
                $updateHistoricalInventoryOrigin = HistoricalInventory::find($historicalInventoryOrigin->id);
                $updateHistoricalInventoryOrigin->before_stock = $storehouseO->stock;
                $updateHistoricalInventoryOrigin->fraction_before_stock = $storehouseO->stock_other_units;
                $updateHistoricalInventoryOrigin->after_stock = $stockFinal;
                $updateHistoricalInventoryOrigin->fraction_after_stock = $storehouseO->stock_other_units - $quantityProduct;
                $updateHistoricalInventoryOrigin->unit_price = $valueUnitPrice;
                $updateHistoricalInventoryOrigin->value_movement = $valueMovement;
                $updateHistoricalInventoryOrigin->value_inventory = $storehouseO->total - $valueMovement;
                $updateHistoricalInventoryOrigin->save();

                $storehouseO->stock = $stockFinal;
                $storehouseO->stock_other_units = $storehouseO->stock_other_units - $quantityProduct;
                if ($storehouseO->stock == 0 && $storehouseO->stock_other_units == 0) $storehouseO->total = 0;
                else $storehouseO->total = $storehouseO->total - $valueMovement;
                $storehouseO->save();
            }

            DB::commit();

            $transferUpdate = transfer_employee_employee::find($transfer->id);
            $transferUpdate->id_transfer_ee = 'TCLE-' . $transfer->id;
            $transferUpdate->total = $total;
            $transferUpdate->save();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (Exception $th) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
