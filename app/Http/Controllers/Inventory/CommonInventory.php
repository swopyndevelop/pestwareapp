<?php


namespace App\Http\Controllers\Inventory;


use App\entry;
use App\transfer_center_center;
use App\transfer_center_employee;
use App\transfer_employee_center;
use App\transfer_employee_employee;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CommonInventory
{
    public static function getFolioEntry()
    {
        $entry = entry::where('companie', Auth::user()->companie)->latest()->first();
        if ($entry) {
            if ($entry->id_entry == 'E-1') return 'E-2';
            else {
                $folio = explode('-', $entry->id_entry);
                return 'E-'.++$folio[1];
            }
        } else return 'E-1';
    }

    public static function getFolioTransferSS()
    {
        $transfer = transfer_center_center::where('companie', Auth::user()->companie)->latest()->first();
        if ($transfer) {
            if ($transfer->id_transfer_cc == 'TCC-1') return 'TCC-2';
            else {
                $folio = explode('-', $transfer->id_transfer_cc);
                return 'TCC-'.++$folio[1];
            }
        } else return 'TCC-1';
    }

    public static function getFolioTransferSE()
    {
        $transfer = \DB::table('transfer_center_employees as tce')
            ->join('users as u', 'tce.id_user', 'u.id')
            ->where('u.companie', Auth::user()->companie)
            ->latest('tce.created_at')->first();
        if ($transfer) {
            if ($transfer->id_transfer_ce == 'TCE-1') return 'TCE-2';
            else {
                $folio = explode('-', $transfer->id_transfer_ce);
                return 'TCE-'.++$folio[1];
            }
        } else return 'TCE-1';
    }

    public static function getFolioTransferEE()
    {
        $transfer = transfer_employee_employee::where('companie', Auth::user()->companie)->latest()->first();
        if ($transfer) {
            if ($transfer->id_transfer_ee == 'TEE-1') return 'TEE-2';
            else {
                $folio = explode('-', $transfer->id_transfer_ee);
                if (Str::contains('V', $transfer->id_transfer_ee)) return 'TEE-'.++$folio[2];
                else return 'TEE-'.++$folio[1];
            }
        } else return 'TEE-1';
    }

    public static function getFolioTransferEC()
    {
        $transfer = transfer_employee_center::where('companie', Auth::user()->companie)->latest()->first();
        if ($transfer) {
            if ($transfer->id_transfer_ec == 'TEC-1') return 'TEC-2';
            else {
                $folio = explode('-', $transfer->id_transfer_ec);
                if (Str::contains('V', $transfer->id_transfer_ec)) return 'TEC-'.++$folio[2];
                else return 'TEC-'.++$folio[1];
            }
        } else return 'TEC-1';
    }
}