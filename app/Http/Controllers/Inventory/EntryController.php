<?php

namespace App\Http\Controllers\Inventory;

use App\article_order;
use App\companie;
use App\employee;
use App\HistoricalInventory;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Product_Tax;
use App\profile_job_center;
use App\purchase_order;
use App\Tax;
use App\treeJobCenter;
use Carbon\Carbon;
use http\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Storage;
use App\entry;
use App\product;
use App\storehouse;
use App\entry_product;
use App\facture_entry;


class EntryController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function index($jobcenter = 0)
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $idJobCenter = 0;
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        $search = \Request::get('search');
        $companie = DB::table('users')->select('companie')->where('id',auth()->user()->id)->first();
        $entry = DB::table('entrys as e')
            ->join('users as u','e.id_user','u.id')
            ->join('profile_job_centers as pjc', 'e.id_job_center', 'pjc.id')
            ->select('e.id','e.id_entry','e.origin','e.id_job_center','e.id_user','e.total','e.created_at as date',
            'u.name', 'pjc.name as destinity', 'e.id_purchase')
            ->where('e.id_job_center', $profileJobCenter)
            ->where('e.origin', 'like', '%' . $search . '%')
            ->where('e.visible', 1)
            ->orderBy('e.created_at', 'desc')
            ->paginate(10);
        
        foreach ($entry as $ent) {
            $entry->map(function($ent){
                $unidades = DB::table('entry_products')->select(DB::raw('SUM(units) as tot_unit'))->groupBy('id_entry')->where('id_entry', $ent->id)->first();
                if (!$unidades) dd($ent->id);
                $ent->tot_unit = $unidades->tot_unit;
                $ent->purchase = 0;
                if ($ent->id_purchase != 0) {
                    $purchase = purchase_order::find($ent->id_purchase);
                    $ent->purchase = $purchase;
                }
            });
        }
        $factura = DB::table('facture_entrys as fe')->join('entrys as e','fe.id_entry','e.id')
            ->select('fe.id','fe.id_entry', 'fe.file_route')->get();
        //Listado de Productos y Cantidades para agregar en el modal
        $product = DB::table('products as p')
            ->join('product_types as pt','pt.id','p.id_type_product')
            ->join('product_presentations as pp', 'pp.id', 'p.id_presentation')
            ->join('product_units as pu', 'pu.id', 'p.id_unit')
            ->where('p.profile_job_center_id', $profileJobCenter)
            ->select('p.id', 'p.name as producto','p.description','p.characteristics','p.suggested_use',
                'p.active_ingredient','p.id as id_product', 'pt.id as type_id', 'pt.name as tipo',
                'pp.id as pre_id','pp.name as presentation','pu.id as id_unit','pu.name as unit','p.quantity',
                'p.base_price')->get();

        $productsEntry = DB::table('entry_products as e')->join('products as p','e.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->where('p.profile_job_center_id', $profileJobCenter)
            ->select('e.id as e_p','e.id_entry','e.id_product','e.units','e.price','p.name','p.quantity','p.base_price', 'pu.name as unit')->get();

        $symbol_country = CommonCompany::getSymbolByCountry();

        return view('vendor.adminlte.entrys.index')
            ->with([
                'entry' => $entry, 'factura' =>$factura, 'product' => $product, 'productsEntry' => $productsEntry, 'symbol_country' => $symbol_country,
                'jobCenters' => $jobCenters, 'profileJobCenterSelect' => $profileJobCenterSelect, 'treeJobCenterMain' => $treeJobCenterMain,
                'jobCenterSession' => $jobCenterSession
                ]);
    }

    public function createEntry()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $products = DB::table('products')->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)->orderBy('name','asc')->get();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $company = companie::find(Auth::user()->companie);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $productsEntry = DB::table('entry_products as ep')
            ->join('products as p', 'ep.id_product', 'p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('p.name', 'ep.units', 'p.id', 'ep.price', 'p.base_price', 'p.quantity', 'pu.name as unit')
            ->where('p.profile_job_center_id', $jobCenterSession->id_profile_job_center)
            ->get();

        $purchasesOrder = DB::table('purchase_orders as ps')->where('ps.companie', Auth::user()->companie)
            ->join('providers as p', 'ps.id_provider', 'p.id')
            ->where('p.profile_job_center_id', $jobCenterSession->id_profile_job_center)
            ->where('ps.has_entry', '!=', '1')
            ->select('ps.*', 'p.company as provider')
            ->get();

        // Modal create product
        $types = DB::table('product_types')->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)->orderBy('name','asc')->get();
        $presentation = DB::table('product_presentations')->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)->orderBy('name','asc')->get();
        $unit = DB::table('product_units')->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)->orderBy('name','asc')->get();
        $taxes = Tax::where('profile_job_center_id',$jobCenterSession->id_profile_job_center)->orderBy('name','asc')->get();

        return view('vendor.adminlte.entrys._create')->with(['products' => $products, 'jobCenters' => $jobCenters, 'company' => $company,
            'productsEntry' => $productsEntry, 'types' => $types, 'presentation' => $presentation, 'unit' => $unit, 'taxes' => $taxes, 'purchasesOrder' => $purchasesOrder]);
    }

    public function add_entry(Request $request)
    {

        try {
            
            DB::beginTransaction();

            $purchaseOrderId = $request->get('purchaseOrderId');

            // Valida si la entrada es por orden de compra o normal.
            if ($purchaseOrderId != 0) {
                $purchase = purchase_order::find($purchaseOrderId);
                $entry = new entry();
                $entry->id_entry = CommonInventory::getFolioEntry();
                $entry->id_user = Auth::id();
                $entry->origin = $request->get('origin');
                $entry->id_job_center = $request->get('selectJobCenters');
                $entry->companie = Auth::user()->companie;
                $entry->total = $purchase->total;
                $entry->id_purchase = $purchaseOrderId;
                $entry->save();

                $destiny = profile_job_center::find($request->get('selectJobCenters'));
                //$products = article_order::where('id_purchase_order', $purchaseOrderId)->get();

                $products = $request->get('dataProducts');
                $partial = false;
                $totalGlobalWithTaxes = 0;

                foreach ($products as $product) {

                    $productId = $product['productId'];
                    $articleProductId = $product['id'];
                    $quantity = $product['quantity'];

                    $articleProduct = article_order::find($articleProductId);
                    $dataProduct = product::find($productId);
                    if ($articleProduct->quantity != $quantity) {
                        $partial = true;
                        // Update a Article Product
                        if ($articleProduct->quantity > $quantity) {
                            $totalQuantity = $articleProduct->quantity_partial + $quantity;
                            $articleProduct->quantity_partial = $totalQuantity;
                            if ($totalQuantity == $articleProduct->quantity) $articleProduct->is_partial = 0;
                            else $articleProduct->is_partial = 1;
                            $articleProduct->save();
                        }
                    }
                    else {
                        $articleProduct->quantity_partial = $articleProduct->quantity;
                        $articleProduct->save();
                    }

                    $total = $articleProduct->unit_price * $quantity;
                    $priceWithTax = $articleProduct->total / $articleProduct->quantity;
                    $priceWithOutTax = $total / $articleProduct->quantity;
                    $totalWithTax = $priceWithTax * $quantity;
                    $totalWithOutTax = $priceWithOutTax * $quantity;
                    $totalGlobalWithTaxes += $totalWithTax;

                    $entryProduct = new entry_product();
                    $entryProduct->id_entry = $entry->id;
                    $entryProduct->id_product = $productId;
                    $entryProduct->units = $quantity;
                    $entryProduct->price = $priceWithTax;
                    $entryProduct->save();

                    /*//update price base to product
                    $productFind = DB::table('products')->select('base_price')->where('id', $product->id_product)->first();
                    if ($priceFacture > $productFind->base_price) {
                        $productUpdate = product::find($product->id_product);
                        $productUpdate->base_price = $priceFacture;
                        $productUpdate->save();
                    }*/

                    //variables
                    $quantityProduct = DB::table('products')->select('quantity')->where('id', $productId)->first();
                    $quantityProduct = $quantityProduct->quantity * $quantity;
                    $fractionQuantity = explode('.', $quantityProduct);

                    //update storehouse destinity
                    $isStorehouse = DB::table('storehouses')->where('id_job_center', $request->selectJobCenters)
                        ->where('id_product', $productId)
                        ->first();

                    // History Inventory Before
                    $historicalInventory = new HistoricalInventory();
                    $historicalInventory->folio = $entry->id_entry;
                    $historicalInventory->date = Carbon::now()->toDateString();
                    $historicalInventory->hour = Carbon::now()->toTimeString();
                    $historicalInventory->id_user = Auth::id();
                    $historicalInventory->id_type_movement_inventory = 1;
                    $historicalInventory->id_product = $productId;
                    $historicalInventory->source = $request->get('origin');
                    $historicalInventory->destiny = $destiny->name;
                    $historicalInventory->quantity = $quantity;
                    $historicalInventory->fraction_quantity = $quantityProduct;
                    $historicalInventory->before_stock = $quantity;
                    $historicalInventory->after_stock = $quantity;
                    $historicalInventory->fraction_before_stock = $fractionQuantity[0];
                    $historicalInventory->fraction_after_stock = $fractionQuantity[0];
                    $historicalInventory->unit_price = $priceWithOutTax / $dataProduct->quantity;
                    $historicalInventory->value_movement = $totalWithOutTax;
                    $historicalInventory->value_inventory = $totalWithOutTax;
                    $historicalInventory->is_destiny = 1;
                    $historicalInventory->id_profile_job_center = $request->get('selectJobCenters');
                    $historicalInventory->id_company = Auth::user()->companie;
                    $historicalInventory->save();

                    if (empty($isStorehouse)) {
                        $storehouse = new storehouse();
                        $storehouse->id_store = "A-0001";
                        $storehouse->id_job_center = $request->selectJobCenters;
                        $storehouse->id_product = $productId;
                        $storehouse->stock = $quantity;
                        $storehouse->stock_other_units = $quantityProduct;
                        $storehouse->total = $totalWithOutTax;
                        $storehouse->companie = Auth::user()->companie;
                        $storehouse->save();
                        $storehouseId = storehouse::find($storehouse->id);
                        $storehouseId->id_store = "A-" . $storehouse->id;
                        $storehouseId->save();

                        $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                        $updateHistoricalInventory->before_stock = 0;
                        $updateHistoricalInventory->fraction_before_stock = 0;
                        $updateHistoricalInventory->save();
                    }
                    else{
                        $storehouse = storehouse::find($isStorehouse->id);
                        // History Inventory Before
                        $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                        $updateHistoricalInventory->before_stock = $storehouse->stock;
                        $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                        $updateHistoricalInventory->after_stock = $storehouse->stock + $quantity;
                        $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + $fractionQuantity[0];
                        $updateHistoricalInventory->value_movement = $totalWithOutTax;
                        $updateHistoricalInventory->value_inventory = $storehouse->total + $totalWithOutTax;
                        $updateHistoricalInventory->save();

                        $storehouse->stock = $storehouse->stock + $quantity;
                        $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                        $storehouse->total = $storehouse->total + $totalWithOutTax;
                        $storehouse->save();
                    }

                }

                $articleOrders = article_order::where('id_purchase_order', $purchaseOrderId)->get();
                $isComplete = true;
                foreach ($articleOrders as $articleOrder){
                    if ($articleOrder->quantity != $articleOrder->quantity_partial) $isComplete = false;
                }

                $purchase = purchase_order::find($purchaseOrderId);
                $purchase->has_entry = $isComplete ? 1 : 2;
                $purchase->save();

                $entryUpd = entry::find($entry->id);
                $entryUpd->total = $totalGlobalWithTaxes;
                $entryUpd->save();

            }
            else {
                $products = $request->get('selectProducts');

                $entry = new entry();
                $entry->id_entry = CommonInventory::getFolioEntry();
                $entry->id_user = Auth::id();
                $entry->origin = $request->get('origin');
                $entry->id_job_center = $request->get('selectJobCenters');
                $entry->companie = Auth::user()->companie;
                $entry->save();

                $destiny = profile_job_center::find($request->get('selectJobCenters'));

                $total = 0;

                foreach ($products as $key => $product) {

                    /*if (!$request->exists('units')[$key] || !$request->exists('priceFacture')[$key]) {
                        DB::rollBack();
                        return response()->json([
                            'code' => 500,
                            'message' => 'Debes ingresar cantidad y precio factura'
                        ]);
                    }*/

                    $quantity = $request->get('quantity')[$key];
                    $fractionQuantity = explode(' ', $quantity);
                    $units = $request->get('units')[$key];
                    $price = $request->get('priceBase')[$key];
                    $priceFacture = $request->get('priceFacture')[$key];

                   /* if ($units == null || $priceFacture == null) {
                        DB::rollBack();
                        return response()->json([
                            'code' => 500,
                            'message' => 'Debes ingresar cantidad y precio factura'
                        ]);
                    }*/


                    $entryProduct = new entry_product();
                    $entryProduct->id_entry = $entry->id;
                    $entryProduct->id_product = $product;
                    $entryProduct->units = $units;
                    $entryProduct->price = $priceFacture;
                    $entryProduct->save();

                    //update price base to product
                    $productFind = DB::table('products')->select('base_price')->where('id', $product)->first();
                    if ($priceFacture > $productFind->base_price) {
                        $productUpdate = product::find($product);
                        $productUpdate->base_price = $priceFacture;
                        $productUpdate->save();
                    }

                    //variables
                    $totalStore = $priceFacture * $units;
                    $total += $totalStore;
                    $productEntry = DB::table('products')->select('quantity')->where('id', $product)->first();
                    $quantityProduct = $productEntry->quantity * $units;

                    //update storehouse destinity
                    $isStorehouse = DB::table('storehouses')->where('id_job_center', $request->selectJobCenters)
                        ->where('id_product', $product)
                        ->first();

                    // History Inventory Before
                    $historicalInventory = new HistoricalInventory();
                    $historicalInventory->folio = $entry->id_entry;
                    $historicalInventory->date = Carbon::now()->toDateString();
                    $historicalInventory->hour = Carbon::now()->toTimeString();
                    $historicalInventory->id_user = Auth::id();
                    $historicalInventory->id_type_movement_inventory = 1;
                    $historicalInventory->id_product = $product;
                    $historicalInventory->source = $request->get('origin');
                    $historicalInventory->destiny = $destiny->name;
                    $historicalInventory->quantity = $units;
                    $historicalInventory->fraction_quantity = $fractionQuantity[0] * $units;
                    $historicalInventory->before_stock = $units;
                    $historicalInventory->after_stock = $units;
                    $historicalInventory->fraction_before_stock = $fractionQuantity[0] * $units;
                    $historicalInventory->fraction_after_stock = $fractionQuantity[0] * $units;
                    $historicalInventory->unit_price = $priceFacture / $productEntry->quantity;
                    $historicalInventory->value_movement = $priceFacture * $units;
                    $historicalInventory->value_inventory = $priceFacture * $units;
                    $historicalInventory->is_destiny = 1;
                    $historicalInventory->id_profile_job_center = $request->get('selectJobCenters');
                    $historicalInventory->id_company = Auth::user()->companie;
                    $historicalInventory->save();

                    if (empty($isStorehouse)) {
                        $storehouse = new storehouse();
                        $storehouse->id_store = "A-0001";
                        $storehouse->id_job_center = $request->selectJobCenters;
                        $storehouse->id_product = $product;
                        $storehouse->stock = $units;
                        $storehouse->stock_other_units = $quantityProduct;
                        $storehouse->total = $totalStore;
                        $storehouse->companie = Auth::user()->companie;
                        $storehouse->save();
                        $storehouseId = storehouse::find($storehouse->id);
                        $storehouseId->id_store = "A-" . $storehouse->id;
                        $storehouseId->save();

                        $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                        $updateHistoricalInventory->before_stock = 0;
                        $updateHistoricalInventory->fraction_before_stock = 0;
                        $updateHistoricalInventory->save();
                    }else{
                        $storehouse = storehouse::find($isStorehouse->id);

                        // History Inventory Before
                        $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                        $updateHistoricalInventory->before_stock = $storehouse->stock;
                        $updateHistoricalInventory->fraction_before_stock = $storehouse->stock_other_units;
                        $updateHistoricalInventory->after_stock = $storehouse->stock + $units;
                        $updateHistoricalInventory->fraction_after_stock = $storehouse->stock_other_units + ($fractionQuantity[0] * $units);
                        $updateHistoricalInventory->unit_price = $priceFacture / $productEntry->quantity;
                        $updateHistoricalInventory->value_movement = $priceFacture * $units;
                        $updateHistoricalInventory->value_inventory = $storehouse->total + $total;
                        $updateHistoricalInventory->save();

                        $storehouse->stock = $storehouse->stock + $units;
                        $storehouse->stock_other_units = $storehouse->stock_other_units + $quantityProduct;
                        $storehouse->total = $storehouse->total + $totalStore;
                        $storehouse->save();
                    }

                }

                $entry = entry::find($entry->id);
                $entry->total = $total;
                $entry->save();
            }

            if ($request->facture != null){
                //Guardar documentos de la factura
                foreach ($request->facture as $f) {
                    $filename = $f->store('entrys', 's3');
                    facture_entry::create([
                        'id_entry' => $entry->id,
                        'file_route' => $filename //Storage::disk('s3')->url($filename)
                    ]);
                }
            }

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $e->getLine() //'Algo salió mal, Intentalo de Nuevo'
            ]);
        }

    }

    public function addComprobant(Request $request){

        $id = $request->get('idEntryAddComprobant');
        $comprobant = $request->file('comprobantInput');
        /*$facture_entry = facture_entry::where('id_entry', $id)->first();
        if ($facture_entry){
            Storage::disk('s3')->delete($facture_entry->file_route);
            $facture_entry->id_entry = $id;
            $filename = $comprobant->store('entrys', 's3');
            $facture_entry->file_route = $filename;
            $facture_entry->save();
        } */
            $facture_entry = new facture_entry();
            $facture_entry->id_entry = $id;
            $filename = $comprobant->store('entrys','s3');
            $facture_entry->file_route = $filename;
            $facture_entry->save();

        return redirect()->route('index_entry')->with('success', 'Factura Pdf, Se guardo correctamente.');
    }

    public function View_Detail($id)
    {
        $entry = DB::table('entrys as e')->join('users as u','e.id_user','u.id')
        ->select('e.id','e.id_entry','e.origin','e.id_job_center','e.id_user','e.total','e.created_at as date',
        'u.name')->where('id',$id)->first();
        $productsEntry = DB::table('entry_products as e')->join('products as p','e.id_product','p.id')
            ->select('e.id as e_p','e.id_entry','e.id_product','e.units','e.price','p.name','p.quantity','p.base_price')
            ->where('e.id_entry',$id)->get();
        $factura = DB::table('facture_entrys as fe')->join('entrys as e','p.id_entry','e.id')
            ->select('fe.id','fe.id_entry', 'fe.file_route')->where('fe.id_entry',$id)->get();
        return view('')->with(['entry' => $entry, '`productsEntry' => $productsEntry, 'factura' => $factura]);
    }

    public function getData(Request $request)
    {
        $product = DB::table('products as p')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->join('storehouses as s', 'p.id', 's.id_product')
            ->select('p.base_price', 'p.quantity', 'pu.name as unit', 's.stock', 's.stock_other_units')
            ->where('p.id', $request->get('id'))
            ->where($request->campo, $request->get('origin'))
            ->where('s.id_product', $request->get('id'))
            ->first();

        return response()->json($product);
    }

    public function getProducts()
    {
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $products = DB::table('products')->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)->orderBy('name','asc')->get();

        return response()->json($products);
    }

    public function getProductsForPurchaseOrder()
    {
        try {
            $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
            $products = DB::table('products')
                ->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)
                //->where('sale_price', '<>', null)
                ->orderBy('name','asc')
                ->get();

            return \Response::json([
                'code' => 200,
                'products' => $products
            ]);
        } catch (\Exception $exception) {
            return \Response::json([
                'code' => 500,
                'message' => 'Error al cargar los productos, intenta de nuevo.'
            ]);
        }

    }

    public function getDataProductEntry(Request $request)
    {
        $product = DB::table('products as p')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('p.base_price', 'p.quantity', 'pu.name as unit')
            ->where('p.id', $request->get('id'))
            ->first();

        return response()->json($product);
    }

    public function getDataPurchase(Request $request)
    {
        $purchaseOrder = purchase_order::where('purchase_orders.id', $request->get('id_purchase'))
            ->join('providers as p', 'purchase_orders.id_provider', 'p.id')
            ->first();

        $articlesOrder = DB::table('article_orders as ao')
            ->join('products as p', 'ao.id_product', 'p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->where('id_purchase_order', $request->get('id_purchase'))
            ->select('ao.*', 'p.name as product', 'pu.name as unit', 'p.quantity as quantityProduct', 'p.id as id_product')
            ->get();

        if ($purchaseOrder) {
            return Response::json([
                'code' => 200,
                'purchase' => $purchaseOrder,
                'products' => $articlesOrder
            ]);
        }

        return Response::json([
            'code' => 500,
            'message' => 'No se encontró información para la orden de compra seleccionada.'
        ]);

    }

    public function getDataProductForPurchaseOrder(Request $request)
    {
        try {
            $product = DB::table('products as p')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('p.base_price', 'p.quantity', 'pu.name as unit', 'p.id', 'p.sale_price')
                ->where('p.id', $request->get('id'))
                ->first();
            $lastPurchase = article_order::where('id_product', $request->get('id'))
                ->latest('id')
                ->first();

            $lastPrice = $product->base_price;
            if ($lastPurchase) $lastPrice = $lastPurchase->unit_price;

            $taxes = Product_Tax::where('id_product', $product->id)
                ->join('taxes as tx', 'product_tax.id_tax', 'tx.id')
                ->orderBy('tx.value', 'desc')
                ->get();
            $taxesLabel = "Sin impuestos";
            $taxesValues = 0;
            if ($taxes->count() > 0) {
                $taxesLabel = "";
                $taxesValues = $taxes;
                foreach ($taxes as $tax) {
                    $taxesLabel = $taxesLabel . $tax->name . ", ";
                }
            }

            return \Response::json([
                'code' => 200,
                'product' => $product,
                'taxes' => $taxesLabel,
                'taxesValues' => $taxesValues,
                'lastPrice' => $lastPrice
            ]);
        } catch (\Exception $exception) {
            return \Response::json([
                'code' => 500,
                'message' => 'Error al cargar los productos, intenta de nuevo.'
            ]);
        }
    }

    public function download($id)
    {
       $facture = facture_entry::find($id);
       //$ruta = storage_path().'/app/'.$facture->file_route;
       //return response()->download($ruta);
       //return Storage::disk('s3')->download($facture->file_route); <- Esta opción es para descargar directo
        return Storage::disk('s3')->response($facture->file_route); // Con preview similar a stream dom pdf
    }

    public function deleteEntry(Request $request){
        try {
            $idEntry = SecurityController::decodeId($request->get('idEntry'));
            $entry = entry::find($idEntry);
            $entry->visible = 0;
            $entry->save();
            return \Response::json([
                'code' => 200,
                'message' => 'Entrada Eliminada Correctamente'
            ]);
        } catch (\Exception $exception) {
            return \Response::json([
                'code' => 500,
                'message' => 'Error al cargar los productos, intenta de nuevo.'
            ]);
        }
    }
}
