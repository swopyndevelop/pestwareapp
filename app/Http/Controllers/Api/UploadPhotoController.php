<?php

namespace App\Http\Controllers\Api;

use App\Common\CommonImage;
use App\condition_picture;
use App\Http\Controllers\Api\Dao\PlaceInspectionDao;
use App\inspection_picture;
use App\Notifications\LogsNotification;
use App\place_condition;
use App\place_inspection;
use App\plague_control;
use App\plague_control_picture;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Log;
use Response;
use Storage;

class UploadPhotoController extends Controller
{
    private $FOLDER_PHOTO_INSPECTION = "services/place_inspections/";
    private $FOLDER_PHOTO_CONDITIONS = "services/place_conditions/";
    private $FOLDER_PHOTO_CASH = "services/cash/";
    private $FOLDER_PHOTO_FIRMS = "services/firms/";
    private $FOLDER_PHOTO_CONTROL = "services/plague_controls/";
    private $FOLDER_PHOTO_EXPENSE = "services/expenses/";
    private $FOLDER_PHOTO_STATIONS = "services/stations/";

    private $PHOTO_INSPECTION = "place_inspections";
    private $PHOTO_CONDITIONS = "place_conditions";
    private $PHOTO_CASH = "cash";
    private $PHOTO_FIRMS = "firms";
    private $PHOTO_CONTROL = "plague_controls";
    private $PHOTO_EXPENSE = "expenses";
    private $PHOTO_STATIONS = "stations";

    public function uploadPhotosInspectionTest(Request $request)
    {

        try {
            $photosUploadSuccess = new Collection();



            $inspectionId = $request->get('inspection_id');
            $photos = json_decode($request->get('photos'));

            Log::error('LOGAPI:' . $inspectionId);

            foreach ($photos as $photo)
            {
                $photo = json_decode($photo);
                Log::error('LOGAPI:' . $photo->encoded_image);
                $fileImage = Image::make($photo->encoded_image)
                    ->orientate()
                    ->resize(500, null, function ($constraint) { $constraint->aspectRatio(); } )
                    ->encode('jpg',50);
                $name = $photo->name;
                $saveImage = Storage::disk('s3')->put('/' . $this->FOLDER_PHOTO_INSPECTION . $name . ".JPG", $fileImage);
                if ($saveImage) {
                    $route = $this->FOLDER_PHOTO_INSPECTION . $name . '.JPG';
                    PlaceInspectionDao::saveInspectionPicture($inspectionId, $route);
                    $photosUploadSuccess->push($route);
                }
            }

            $responseMessage = $photosUploadSuccess->count() . ' de ' . count($photos) . ' fotos subidas';

            return Response::json([
                'code' => 201,
                'message' => $responseMessage
            ]);

        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification($exception->getMessage(), $exception->getLine(), 3));
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function uploadPhotoInspection(Request $request, $id, $name)
    {
        try {

            $image = $request->get('image');
            $folder = $this->FOLDER_PHOTO_INSPECTION;
            $route = $folder . $name . "JPG";

            $decodedImage = base64_decode($image);

            $fileImage = Image::make($decodedImage)
                ->orientate()
                ->resize(500, null, function ($constraint) { $constraint->aspectRatio(); } )
                ->encode('jpg',60);

            $saveImage = Storage::disk('s3')->put('/' . $folder . $name . ".JPG", $fileImage);

            if($saveImage !== false) {
                $codeResponse = PlaceInspectionDao::saveInspectionPicture($id, $route);
                if ($codeResponse == 200) {
                    return Response::json([
                        'code' => 200,
                        'message' => 'Foto guardada.'
                    ]);
                } else {
                    return Response::json([
                        'code' => 500,
                        'message' => 'Error al guardar los datos de la foto.'
                    ]);
                }
            }else{
                return Response::json([
                    'code' => 500,
                    'message' => 'Error al guardar la foto.'
                ]);
            }

        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification("Error exception", $exception->getMessage(), 3));
            return Response::json([
                'code' => 500,
                'message' => $exception->getMessage()//'Algo salio mal.'
            ]);
        }
    }

    public function getAllPhotosBySectionAndOrderId($orderId, $sectionName)
    {
        try {
            $photos = null;
            switch ($sectionName) {
                case $this->PHOTO_INSPECTION:
                    // Place inspection
                    $inspection = place_inspection::where('id_service_order', $orderId)->first();
                    $photos = inspection_picture::where('place_inspection_id', $inspection->id)->get();
                    break;
                case $this->PHOTO_CONDITIONS:
                    // Place condition
                    $condition = place_condition::where('id_service_order', $orderId)->first();
                    $photos = condition_picture::where('place_condition_id', $condition->id)->get();
                    break;
                case $this->PHOTO_CASH:
                    // Payment box

                    break;
                case $this->PHOTO_FIRMS:
                    // Firm

                    break;
                case $this->PHOTO_CONTROL:
                    // Plague control
                    $control= plague_control::where('id_service_order', $orderId)->first();
                    $photos = plague_control_picture::where('plague_control_id', $control->id)->get();
                    break;
                case $this->PHOTO_EXPENSE:

                    break;
                case $this->PHOTO_STATIONS:
                    // Monitoring stations
            }
            foreach ($photos as $photo) {
                $photos->map(function ($photo) {
                    $photo->url_s3 = CommonImage::getTemporaryUrl($photo->file_route, 5);
                });
            }
            return Response::json($photos);
        } catch (Exception $exception) {
            return Response::json([]);
        }
    }
}
