<?php

namespace App\Http\Controllers\Api;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MonitoringApiController extends Controller
{
    public function getConditions($idCompany) {
        $conditions = DB::table('monitoring_conditions')->where('id_company', $idCompany)->get();
        return response()->json($conditions);
    }

    public function getMonitoringTree($idMonitoring) {
        $tree = DB::table('monitoring_trees as mt')
            ->join('type_stations as ts', 'mt.id_type_station', 'ts.id')
            ->where('mt.id_monitoring', $idMonitoring)
            ->select('mt.*', 'ts.id_type_area')
            ->get();
        return response()->json($tree);
    }
}
