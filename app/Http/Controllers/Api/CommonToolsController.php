<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\UploadEvents;
use App\User;
use DB;
use Exception;
use Response;
use Vinkla\Hashids\Facades\Hashids;

class CommonToolsController extends Controller
{
    public function encodeId($id) {
        try {
            $users = DB::table('users')->paginate(10);

            return $users;
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta más tarde.'
            ]);
        }
    }

    public function eventsMassive() {
        try {
            UploadEvents::dispatch();
            return Response::json([
                'code' => 200,
                'message' => 'Ok.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param $id
     * Encode id with Hashid
     */
    private function encodeHashId($id)
    {
        return Hashids::encode($id);
    }
}
