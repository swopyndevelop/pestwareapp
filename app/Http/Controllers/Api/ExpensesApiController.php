<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\Dao\ExpenseDao;
use App\Http\Controllers\Api\Dao\ResponseCodes;

class ExpensesApiController extends Controller
{
    public function getExpensesByUser($userId)
    {
        $expenses = DB::table('expenses as ex')
            ->join('users as u', 'ex.id_user', 'u.id')
            ->join('concepts as c', 'ex.id_concept', 'c.id')
            ->join('vouchers as v', 'ex.id_voucher', 'v.id')
            ->join('payment_methods as pm', 'ex.id_payment_method', 'pm.id')
            ->select('ex.id', 'id_expense', 'ex.expense_name as name', 'description_expense as description', 'ex.date_expense as date', 
                'c.name as concept', 'ex.total', 'ex.credit_days', 'v.name as voucher', 'ex.created_at', 'ex.account_status',
                'pm.name as typePayment','ex.status')
            ->where('id_user', $userId)
            ->orderBy('ex.created_at', 'desc')
            ->get();

        foreach ($expenses as $expense) {
            $expenses->map(function($expense){
                $expense->date = Carbon::parse($expense->date)->format('M d');
                $expense->created_at = Carbon::parse($expense->created_at)->format('h:i a');
                $expense->amount = "$" . $expense->total;
                $expense->color = $expense->status == 2 ? "#176536" : "#DC2213";
                $expense->updated = 1;
            });
        }

        return response()->json($expenses);
    }

    public function packageJsonExpense(Request $request) {

        try {

            // Get all request json and convert to object
            $expense = json_decode($request->get('json'));
            $eventId = $request->get('id');

            $isFile = $expense->isFile;
            $name = $expense->name;
            $expenseName = $expense->expense_name;
            $description = $expense->description;
            $idConcept = $expense->id_concept;
            $idMethodWay = $expense->id_method_way;
            $idMethod = $expense->id_method;
            $article = $expense->article;
            $mount = $expense->mount;
            $daysCredit = $expense->days_credit;
            $idUser = $expense->id_user;
            $idVoucher = $expense->id_voucher;
            $companie = $expense->companie;
            $file = $expense->file;
            $photo = $expense->photos_expenses;

            // Created new expense
            $save = ExpenseDao::saveExpense(
                $isFile, $name, $expenseName, $description, $idConcept, $idMethodWay, $idMethod, $article,
                $mount, $daysCredit, $idUser, $idVoucher, $companie, $file
            );

            if ($save['code']) {
                return response()->json([
                    'code' => ResponseCodes::$SUCCESS,
                    'photo' => $photo,
                    'idExpense' => $save['id'] 
                ]);
            }

            return response()->json([
                'code' => ResponseCodes::$ERROR,
                'photo' => 'No fue posible guardar el gasto.',
                'idExpense' => ""
            ]);

        } catch (\Exception $ex) {
            return response()->json([
                'code' => $ex->getCode(),
                'photo' => $ex->getMessage(),
                'idExpense' => ""
            ]);
        }
    }
}
