<?php

namespace App\Http\Controllers\Api;

use App\Common\CommonCarbon;
use App\companie;
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\plague_type_quotation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\service_order;
use App\employee;
use App\customer_branche;
use App\User;
use App\event;
use App\Http\Controllers\Api\Dao\ResponseCodes;
use Carbon\Carbon;
use Vinkla\Hashids\Facades\Hashids;

class ServicesApiController extends Controller
{
    #region Funciones JSON

    private $ALL_STATUS = 1;
    private $FINISHED_STATUS = 2;
    private $STARTED_STATUS = 3;
    private $CANCELED_STATUS = 4;

    private $ALL_TYPE = 5;
    private $SERVICE_TYPE = 6;
    private $REINFORCEMENT_TYPE = 7;
    private $WARRANTY_TYPE = 8;
    private $TRACING_TYPE = 9;

    private $ALL_STATUS_PAYMENT = 10;
    private $PAYMENT_STATUS_PAYMENT = 11;
    private $DEBT_STATUS_PAYMENT = 12;
    private $CREDIT_STATUS_PAYMENT = 13;

    public function getAllServicesByEmployee($idEmployee, $serviceStatus, $serviceType, $paymentStatus, $startDate, $endDate)
    {
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id');

        if ($paymentStatus == $this->PAYMENT_STATUS_PAYMENT) {
            $events->join('cashes as c', 'c.id_service_order', 'so.id');
            $events->where('c.payment', 2);
        }

        if ($paymentStatus == $this->DEBT_STATUS_PAYMENT) {
            $events->join('cashes as c', 'c.id_service_order', 'so.id');
            $events->where('c.payment', 1);
        }

        if ($paymentStatus == $this->CREDIT_STATUS_PAYMENT) {
            $events->join('cashes as c', 'c.id_service_order', 'so.id');
            $events->where('c.id_payment_way', 1);
        }

        $events->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
            'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
            'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
            'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality', 'qo.id_price_list', 'qo.construction_measure',
            'qo.price_reinforcement as pr', 'so.customer_branch_id', 'cu.id as customer_id', 'qo.id as quotation_id', 'so.companie as companyId', 'so.is_shared',
            'so.is_main', 'so.inspection', 'em.name as technician')
            ->where('ev.id_employee', $idEmployee);

        if ($serviceStatus == $this->ALL_STATUS) $events->where('ev.id_status', '!=', null);
        if ($serviceStatus == $this->FINISHED_STATUS) $events->where('ev.id_status', 4);
        if ($serviceStatus == $this->STARTED_STATUS) $events->where('ev.id_status', 2);
        if ($serviceStatus == $this->CANCELED_STATUS) $events->where('ev.id_status', 3);

        if ($serviceType == $this->SERVICE_TYPE) $events->where('ev.service_type', 1);
        if ($serviceType == $this->REINFORCEMENT_TYPE) $events->where('ev.service_type', 2);
        if ($serviceType == $this->WARRANTY_TYPE) $events->where('ev.service_type', 3);
        if ($serviceType == $this->TRACING_TYPE) $events->where('ev.service_type', 4);

        $events->whereDate('ev.initial_date', '>=', $startDate);
        $events->whereDate('ev.initial_date', '<=', $endDate);
        $events->orderBy('ev.initial_date', 'asc');
        $events->orderBy('ev.initial_hour', 'asc');

        $data = $events->get();

        $count = $data->count();
        if ($count > 25) return response()->json([]);

        $response = $this->mapServicesToJson($data);
        return $response;
    }

    public function getAllServicesByDate($date, $jobCenter, $userId=0)
    {
        $timezone = CommonCarbon::getTimeZoneByJobCenter($jobCenter);

        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality', 'qo.id_price_list', 'qo.construction_measure',
                'qo.price_reinforcement as pr', 'so.customer_branch_id', 'cu.id as customer_id', 'qo.id as quotation_id', 'so.companie as companyId', 'so.is_shared',
                'so.is_main', 'so.inspection', 'em.name as technician')
            ->where('ev.id_job_center', $jobCenter)
            ->where('ev.id_status', '!=', 3)
            ->whereDate('ev.initial_date', '=', Carbon::parse($date)->timezone($timezone)->toDateString());


        if ($userId != 0) {
            $rolesUser = DB::table('role_user as ru')
                ->join('roles as r', 'ru.role_id', 'r.id')
                ->where('user_id', $userId)
                ->select('r.*')
                ->first();
            $permissionCalendar = DB::table('permission_role')
                ->where('role_id', $rolesUser->id)
                ->where('permission_id', 2)
                ->first();
            $technician = employee::where('employee_id', $userId)->first();
            if (!$permissionCalendar || $rolesUser->id != 2) $events->where('ev.id_employee', $technician->id);
        }

        $events = $events->orderBy('ev.initial_date', 'asc')->orderBy('ev.initial_hour', 'asc')->get();

        return $this->mapServicesToJson($events);
    }

    public function getTodayServicesByEmployee($id)
    {
        $timezone = CommonCarbon::getTimeZoneByEmployee($id);
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality', 'qo.id_price_list', 'qo.construction_measure',
                'qo.price_reinforcement as pr', 'so.customer_branch_id', 'cu.id as customer_id', 'qo.id as quotation_id', 'so.companie as companyId', 'so.is_shared',
                'so.is_main', 'so.inspection', 'em.name as technician')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->whereDate('ev.initial_date', '=', Carbon :: today ($timezone) -> toDateString ())
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapServicesToJson($events);
        return $response;
    }

    public function getTomorrowServicesByEmployee($id)
    {
        $timezone = CommonCarbon::getTimeZoneByEmployee($id);
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality', 'qo.id_price_list', 'qo.construction_measure',
                'qo.price_reinforcement as pr', 'so.customer_branch_id', 'cu.id as customer_id', 'qo.id as quotation_id', 'so.companie as companyId', 'so.is_shared',
                'so.is_main', 'so.inspection', 'em.name as technician')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->whereDate('ev.initial_date', '=', Carbon :: tomorrow ($timezone) -> toDateString ())
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapServicesToJson($events);
        return $response;
    }

    public function getYesterdayServicesByEmployee($id)
    {
        $timezone = CommonCarbon::getTimeZoneByEmployee($id);
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality', 'qo.id_price_list', 'qo.construction_measure',
                'qo.price_reinforcement as pr', 'so.customer_branch_id', 'cu.id as customer_id', 'qo.id as quotation_id', 'so.companie as companyId', 'so.is_shared',
                'so.is_main', 'so.inspection', 'em.name as technician')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->whereDate('ev.initial_date', '=', Carbon :: yesterday ($timezone) -> toDateString ())
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapServicesToJson($events);
        return $response;
    }

    public function getCanceledServicesByEmployee($id, $startDate, $endDate)
    {
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality', 'qo.id_price_list', 'qo.construction_measure',
                'qo.price_reinforcement as pr', 'so.customer_branch_id', 'cu.id as customer_id', 'qo.id as quotation_id', 'so.companie as companyId', 'so.is_shared',
                'so.is_main', 'so.inspection', 'em.name as technician')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', 3)
            ->where('ev.service_type', 1)
            ->whereDate('ev.initial_date', '>=', $startDate)
            ->whereDate('ev.initial_date', '<=', $endDate)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapServicesToJson($events);
        return $response;
    }

    public function getFinishedServicesByEmployee($id, $startDate, $endDate)
    {
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality', 'qo.id_price_list', 'qo.construction_measure',
                'qo.price_reinforcement as pr', 'so.customer_branch_id', 'cu.id as customer_id', 'qo.id as quotation_id', 'so.companie as companyId', 'so.is_shared',
                'so.is_main', 'so.inspection', 'em.name as technician')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', 4)
            ->where('ev.service_type', 1)
            ->whereDate('ev.initial_date', '>=', $startDate)
            ->whereDate('ev.initial_date', '<=', $endDate)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapServicesToJson($events);
        return $response;
    }

    public function getStartedServicesByEmployee($id)
    {
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality', 'qo.id_price_list', 'qo.construction_measure',
                'qo.price_reinforcement as pr', 'so.customer_branch_id', 'cu.id as customer_id', 'qo.id as quotation_id', 'so.companie as companyId', 'so.is_shared',
                'so.is_main', 'so.inspection', 'em.name as technician')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', 2)
            ->where('ev.service_type', 1)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapServicesToJson($events);
        return $response;
    }


    public function mapServicesToJson($events)
    {
        foreach ($events as $event) {
            $events->map(function($event){
                $fecha = $event->initial_date;
                $hora = $event->initial_hour;
                $horaFinal = $event->final_hour;

                // Plagues of quote
                $plaguesQuote = DB::table('plague_type_quotation as pq')
                    ->join('plague_types as pt', 'pq.plague_type_id', 'pt.id')
                    ->where('quotation_id', $event->quotation_id)
                    ->select('pt.name')
                    ->get();

                $plaguesText = "";
                foreach ($plaguesQuote as $plague) {
                    $plaguesText .= $plague->name . ", ";
                }
                $event->plagues_quote = trim($plaguesText, ', ');

                //Check monitorings
                $monitorings = DB::table('monitorings')
                    ->where('id_customer', $event->customer_id)
                    ->first();
                if ($monitorings) $event->monitoring = $monitorings->id;
                else $event->monitoring = 0;

                if ($event->customer_branch_id != null) {
                    $brancheId = $event->customer_branch_id;
                    $monitorings = DB::table('monitorings')
                        ->where('id_customer', $event->customer_id)
                        ->where('id_customer_branch', $brancheId)
                        ->first();
                    if ($monitorings) $event->monitoring = $monitorings->id;
                }

                if($event->reinforcement == 1) $event->type = "Refuerzo";
                else if($event->warranty == 1) $event->type = "Garantía";
                else if($event->tracing == 1) $event->type = "Seguimiento";
                else $event->type = "";

                if($event->reinforcement == 0) {
                    $priceTwo = $this->getTwoPrice($event->id_price_list, $event->construction_measure);
                    if ($priceTwo != null && $priceTwo->price_two > 0) {
                        $priceReinforcement = $priceTwo->price_two;
                        if ($event->pr != null) $priceReinforcement = $event->pr + $priceReinforcement;
                        $event->price_reinforcement = $priceReinforcement;
                    }else $event->price_reinforcement = 0;
                }else $event->price_reinforcement = 0;

                if ($event->customer_branch_id != null) {
                    $customerBranch = customer_branche::find($event->customer_branch_id);
                    $event->address_number = $customerBranch->address_number;
                    $event->street = $customerBranch->address;
                    $event->colony = $customerBranch->colony;
                    $event->municipality = $customerBranch->municipality;
                    $event->state = $customerBranch->state;
                    $event->empresa = $customerBranch->name;
                    $event->cellphone = $customerBranch->phone;
                }

                $event->address = $event->street . ' ' . $event->address_number . ', ' . $event->colony;
                if ($event->state == null || $event->municipality == null) $event->aditionalAddress = "Desconocido";
                else $event->aditionalAddress = $event->state . ", " . $event->municipality;
                $event->date = \Carbon\Carbon::parse($fecha)->format('M d');
                $event->hour = \Carbon\Carbon::parse($hora)->format('h:i a');
                $event->final_hour = \Carbon\Carbon::parse($horaFinal)->format('h:i a');

                $company = companie::find($event->companyId);
                $country = Country::find($company->id_code_country);
                $symbol = $country->symbol_country;
                $event->totalC = $symbol . $event->total;
                if ($company->id == 643) $event->totalC = "";

                $event->id_order_encode = Hashids::encode($event->idSo);

                if ($event->status == 1) {
                    $event->color = "#0288d1";
                    $event->etiqueta = "";
                }elseif ($event->status == 2) {
                    $event->color = "#f9a825";
                    $event->etiqueta = "Comenzado";
                }elseif ($event->status == 3) {
                    $event->color = "#DC2213";
                    $event->etiqueta = "Cancelado";
                }elseif ($event->status == 4) {
                    $event->color = "#176536";
                    $event->etiqueta = "Finalizado";
                }

                if ($event->title == null) {
                    $event->title = "";
                }

                if ($event->cellphone == null) {
                    $event->cellphone = "";
                }

                if ($event->colony == null) {
                    $event->colony = "";
                }

                if ($event->street == null) {
                    $event->street = "";
                }

                if ($event->observations == null) {
                    $event->observations = "";
                }

                if ($event->address == null) {
                    $event->address = "";
                }

                if ($event->establishment_name == null) {
                    $event->establishment_name = "";
                }

                if ($event->address_number == null) {
                    $event->address_number = "";
                }

                if ($event->email == null) {
                    $event->email = 0;
                }

                $pregnancy = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',1)->first();
                $baby = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',2)->first();
                $children = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',3)->first();
                $adult = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',4)->first();

                $respiratory = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',1)->first();
                $inmunologic = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',2)->first();
                $renal = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',3)->first();
                $cardiac = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',4)->first();
                $does_not = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',5)->first();
                $other = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id','specification')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',6)->first();

                $dog = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',1)->first();
                $cat = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',2)->first();
                $bird = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',3)->first();
                $fish = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',4)->first();
                $other_mascot = DB::table('mascot_service_order')->select('mascot_id','specification')->where('service_order_id',$event->idSo)->where('mascot_id',5)->first();

                $cash = DB::table('cashes')->where('id_event', $event->id)->select('payment')->first();
                $event->etiqueta_payment = "";
                $event->etiqueta_color = "#000000";
                if (!empty($cash)) {
                    $event->payment = $cash->payment;
                    if ($event->payment == 1) {
                        $event->etiqueta_payment = "Adeudo";
                        $event->etiqueta_color = "#DC2213";
                    }else if ($event->payment == 2) {
                        $event->etiqueta_payment = "Pagado";
                        $event->etiqueta_color = "#176536";
                    }
                }else{
                    $event->payment = 0;
                }

                $conditions = "";
                $inhabitants = "";
                $mascots = "";

                //conditions
                if (!empty($pregnancy)) {
                    $conditions = "Embarazo";
                }

                if (!empty($baby)) {
                    $conditions = $conditions . " Bebés";
                }

                if (!empty($children)) {
                    $conditions = $conditions . " Niños";
                }

                if (!empty($adult)) {
                    $conditions = $conditions . " Adulto Mayor";
                }

                //inhabitants
                if (!empty($respiratory)) {
                    $inhabitants = "Respiratorias";
                }

                if (!empty($inmunologic)) {
                    $inhabitants = $inhabitants . " Inmunológicas";
                }

                if (!empty($renal)) {
                    $inhabitants = $inhabitants . " Renales";
                }

                if (!empty($cardiac)) {
                    $inhabitants = $inhabitants . " Cardiacas";
                }

                if (!empty($does_not)) {
                    $inhabitants = $inhabitants . " No aplica";
                }

                if (!empty($other)) {
                    $inhabitants = $inhabitants . " Otros";
                }

                //mascots
                if (!empty($dog)) {
                    $mascots = "Perros";
                }

                if (!empty($cat)) {
                    $mascots = $mascots . " Gatos";
                }

                if (!empty($bird)) {
                    $mascots = $mascots . " Aves";
                }

                if (!empty($fish)) {
                    $mascots = $mascots . " Peces";
                }

                if (!empty($other_mascot)) {
                    $mascots = $mascots . " Otros";
                }

                $event->conditions = $conditions;
                $event->inhabitants = $inhabitants;
                $event->mascots = $mascots;
            });
        }

        return response()->json($events);
    }

    private function getTwoPrice($idPriceList, $area) {
        return DB::table('prices')
            ->where('price_list_id', $idPriceList)
            ->where('area', '<=', $area)
            ->orderBy('area', 'desc')
            ->first();
    }

    public function createReinforcement($idOrder, $date, $initialHour, $finalHour, $user, $company)
    {

        //TODO: check available hour event.

        try {
            DB::beginTransaction();

            // Validation available hour
            $available = DB::table('events')
                ->where('initial_date', $date)
                ->where('initial_hour', $initialHour)
                ->where('id_employee', $user)
                ->first();

            if ($available) {
                return response()->json([
                    'status' => 'conflict',
                    'message' => 'Ya se encuentra un servicio agendado'
                ]);
            }

            $order = service_order::find($idOrder);
            $order->reinforcement = 1;
            $order->save();

            $quotation = DB::table('quotations')->where('id', $order->id_quotation)->first();
            $eventOld = event::where('id_service_order', $idOrder)->first();
            $priceTwo = DB::table('prices')
                ->where('price_list_id', $quotation->id_price_list)
                ->where('area', '<=', $quotation->construction_measure)
                ->orderBy('area', 'desc')
                ->first();

            $price = $priceTwo->price_two;
            $reinforcement = new service_order;
            $reinforcement->id_service_order = 'R-' . $order->id_quotation;
            $reinforcement->id_quotation = $order->id_quotation;
            $reinforcement->id_payment_method = $order->id_payment_method;
            $reinforcement->id_payment_way = $order->id_payment_way;
            $reinforcement->bussiness_name = $order->bussiness_name;
            $reinforcement->address = $order->address;
            $reinforcement->email = $order->email;
            $reinforcement->observations = $order->observations;
            if ($quotation->price_reinforcement == null) {
                $reinforcement->total = $price;
            } else {
                $reinforcement->total = $quotation->price_reinforcement + $price;
            }
            $reinforcement->user_id = $user;
            $reinforcement->id_job_center = $order->id_job_center;
            $reinforcement->companie = $company;
            $reinforcement->id_status = 10;
            $reinforcement->reinforcement = 1;
            $reinforcement->save();

            $event = new event;
            $event->title = 'Refuerzo';
            $event->id_service_order = $reinforcement->id;
            $event->id_employee = $eventOld->id_employee;
            $event->id_job_center = $reinforcement->id_job_center;
            $event->companie = $company;
            $event->save();

            // Schedule reinforcement into calendar event.

            $orderService = DB::table('service_orders as so')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('customers as c', 'q.id_customer', 'c.id')
                ->where('so.id', $reinforcement->id)
                ->select('so.id_quotation', 'so.id as id_order', 'so.address', 'q.id_customer', 'c.name as nameCustomer', 'so.id_service_order',
                    'so.warranty', 'so.reinforcement', 'so.tracing')
                ->first();

            $evento = event::where('id_service_order', $reinforcement->id)->first();
            $evento['title'] = 'Refuerzo de ' . $orderService->nameCustomer;
            $evento['id_employee'] = $user;
            $evento['initial_hour'] = $initialHour;
            $evento['final_hour'] = $finalHour;
            $evento['initial_date'] = $date;
            $evento['final_date'] = $date;
            $evento['id_service_order'] = $orderService->id_order;
            $evento['id_status'] = 1;
            $evento['service_type'] = 2;
            $evento->save();

            $order = service_order::find($orderService->id_order);
            $order->id_status = 4;
            $saved = $order->save();

            //send notification app technician
            if ($saved) {
                $employee = employee::find($user);
                $user = User::find($employee->employee_id);
                $message = "Fecha: " . $date . " (" . $initialHour . ")  " . $orderService->id_service_order;

                $user->sendFCM('Nuevo Refuerzo', $message);
            }

            DB::commit();

            return response()->json([
                'status' => 'ok'
            ]);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 'fail',
                'message' => $exception->getMessage()
            ]);
        }
    }

}