<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Dao\InspectionDao;
use App\Http\Controllers\Controller;

class InspectionsApiController extends Controller
{
    public function getCustomersInspections($technicianId) {
        return InspectionDao::getCustomersOfInspectionByTechnician($technicianId);
    }

    public function getInspectionsByCustomer($customerId) {
        return InspectionDao::getInspectionsByCustomer($customerId);
    }

    public function getInspectionById($inspectionId) {
        return InspectionDao::getInspectionById($inspectionId);
    }
}
