<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Dao\ExpressQuoteDao;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ExpressQuoteApiController extends Controller
{
    public function store(Request $request)
    {
        // Validate data
        try {
            $isSchedule = $request->get('isSchedule');
            $data = [
                'companyId' => $request->get('companyId'),
                'jobCenterId' => $request->get('jobCenterId'),
                'customerName' => $request->get('customerName'),
                'cellphone' => $request->get('cellphone'),
                'establishmentId' => $request->get('establishmentId'),
                'colony' => $request->get('colony'),
                'address' => $request->get('address'),
                'addressNumber' => $request->get('addressNumber'),
                'sourceOriginId' => $request->get('sourceOriginId'),
                'total' => $request->get('total'),
                'userId' => $request->get('userId'),
                'employeeId' => $request->get('employeeId'),
                'plagues' => $request->get('plagues'),
                'initialDate' => $request->get('initialDate'),
                'initialHour' => $request->get('initialHour'),
                'finalHour' => $request->get('finalHour')
            ];
            $createdQuote = ExpressQuoteDao::createExpressQuote($data, $isSchedule);
            if ($createdQuote['status']) {
                return Response::json([
                    'code' => 201,
                    'message' => $createdQuote['message']
                ]);
            } else {
                return Response::json([
                    'code' => 500,
                    'message' => $createdQuote['message']
                ]);
            }
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Solicitud incorrecta.'
            ]);
        }

    }
}
