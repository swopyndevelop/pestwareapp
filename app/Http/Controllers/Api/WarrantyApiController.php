<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class WarrantyApiController extends Controller
{
    #region Funciones JSON

	public function getAllWarrantyByEmployee($id)
    {
    	$events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                    'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->where('ev.service_type', 3)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapWarrantyToJson($events);
        return $response;
    }

    public function getTodayWarrantyByEmployee($id)
    {
    	$events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                    'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->where('ev.service_type', 3)
            ->whereDate('ev.initial_date', '=', Carbon :: today () -> toDateString ())
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapWarrantyToJson($events);
        return $response;
    }

    public function getTomorrowWarrantyByEmployee($id)
    {
    	$events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                    'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->where('ev.service_type', 3)
            ->whereDate('ev.initial_date', '=', Carbon :: tomorrow () -> toDateString ())
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapWarrantyToJson($events);
        return $response;
    }

    public function getCanceledWarrantyByEmployee($id)
    {
    	$events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email',
                    'so.warranty', 'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', 3)
            ->where('ev.service_type', 3)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapWarrantyToJson($events);
        return $response;
    }

    public function getFinishedWarrantyByEmployee($id)
    {
    	$events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email', 'so.warranty',
                    'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', 4)
            ->where('ev.service_type', 3)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapWarrantyToJson($events);
        return $response;
    }

    public function getStartedWarrantyByEmployee($id)
    {
    	$events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'pl.key as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email', 'so.warranty',
                    'so.reinforcement', 'so.tracing', 'ev.service_type', 'cd.state', 'cu.municipality')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', 2)
            ->where('ev.service_type', 3)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        $response = $this->mapWarrantyToJson($events);
        return $response;
    }


    public function mapWarrantyToJson($events)
    {
    	foreach ($events as $event) {
            $events->map(function($event){
                $fecha = $event->initial_date;
                $hora = $event->initial_hour;
                $horaFinal = $event->final_hour;

                if($event->reinforcement == 1) $event->type = "Refuerzo";
                else if($event->warranty == 1) $event->type = "Garantía";
                else if($event->tracing == 1) $event->type = "Seguimiento";
                else $event->type = "";

                $event->address = $event->street . " " . $event->address_number . ", " . $event->colony;
                if ($event->state == null || $event->municipality == null) $event->aditionalAddress = "Desconocido";
                else $event->aditionalAddress = $event->state . ", " . $event->municipality;
                $event->date = \Carbon\Carbon::parse($fecha)->format('M d');
                $event->hour = \Carbon\Carbon::parse($hora)->format('h:i a');
                $event->final_hour = \Carbon\Carbon::parse($horaFinal)->format('h:i a');
                $event->totalC = "$" . $event->total;

                if ($event->status == 1) {
                    $event->color = "#0288d1";
                    $event->etiqueta = "";
                }elseif ($event->status == 2) {
                    $event->color = "#f9a825";
                    $event->etiqueta = "Comenzado";
                }elseif ($event->status == 3) {
                    $event->color = "#DC2213";
                    $event->etiqueta = "Cancelado";
                }elseif ($event->status == 4) {
                    $event->color = "#176536";
                    $event->etiqueta = "Finalizado";
                }

                if ($event->title == null) {
                     $event->title = "";
                } 

                if ($event->cellphone == null) {
                     $event->cellphone = "";
                } 

                if ($event->colony == null) {
                     $event->colony = "";
                } 

                if ($event->street == null) {
                     $event->street = "";
                } 

                if ($event->observations == null) {
                     $event->observations = "";
                } 

                if ($event->address == null) {
                     $event->address = "";
                } 

                if ($event->establishment_name == null) {
                     $event->establishment_name = "";
                } 

                if ($event->address_number == null) {
                     $event->address_number = "";
                }

                if ($event->email == null) {
                     $event->email = 0;
                } 

                $pregnancy = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',1)->first();
                $baby = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',2)->first();
                $children = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',3)->first();
                $adult = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',4)->first();

                $respiratory = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',1)->first();
                $inmunologic = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',2)->first();
                $renal = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',3)->first();
                $cardiac = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',4)->first();
                $does_not = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',5)->first();
                $other = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id','specification')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',6)->first();

                $dog = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',1)->first();
                $cat = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',2)->first();
                $bird = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',3)->first();
                $fish = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',4)->first();
                $other_mascot = DB::table('mascot_service_order')->select('mascot_id','specification')->where('service_order_id',$event->idSo)->where('mascot_id',5)->first();

                $cash = DB::table('cashes')->where('id_event', $event->id)->select('payment')->first();
                $event->etiqueta_payment = "";
                $event->etiqueta_color = "#000000";
                if (!empty($cash)) {
                    $event->payment = $cash->payment;
                    if ($event->payment == 1) {
                        $event->etiqueta_payment = "Adeudo";
                        $event->etiqueta_color = "#DC2213";
                    }else if ($event->payment == 2) {
                        $event->etiqueta_payment = "Pagado";
                        $event->etiqueta_color = "#176536";
                    }
                }else{
                    $event->payment = 0;
                }

                $conditions = "";
                $inhabitants = "";
                $mascots = "";

                //conditions
                if (!empty($pregnancy)) {
                    $conditions = "Embarazo";
                }

                if (!empty($baby)) {
                    $conditions = $conditions . " Bebés";
                }

                if (!empty($children)) {
                    $conditions = $conditions . " Niños";
                }

                if (!empty($adult)) {
                    $conditions = $conditions . " Adulto Mayor";
                }

                //inhabitants
                if (!empty($respiratory)) {
                    $inhabitants = "Respiratorias";
                }

                if (!empty($inmunologic)) {
                    $inhabitants = $inhabitants . " Inmunológicas";
                }

                if (!empty($renal)) {
                    $inhabitants = $inhabitants . " Renales";
                }

                if (!empty($cardiac)) {
                    $inhabitants = $inhabitants . " Cardiacas";
                }

                if (!empty($does_not)) {
                    $inhabitants = $inhabitants . " No aplica";
                }

                if (!empty($other)) {
                    $inhabitants = $inhabitants . " Otros";
                }

                //mascots
                if (!empty($dog)) {
                    $mascots = "Perros";
                }

                if (!empty($cat)) {
                    $mascots = $mascots . " Gatos";
                }

                if (!empty($bird)) {
                    $mascots = $mascots . " Aves";
                }

                if (!empty($fish)) {
                    $mascots = $mascots . " Peces";
                }

                if (!empty($other_mascot)) {
                    $mascots = $mascots . " Otros";
                }

                $event->conditions = $conditions;
                $event->inhabitants = $inhabitants;
                $event->mascots = $mascots;
            });
		}
		
		return response()->json($events);
    }
}
