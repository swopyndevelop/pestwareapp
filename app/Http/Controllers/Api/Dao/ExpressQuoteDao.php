<?php


namespace App\Http\Controllers\Api\Dao;


use App\address_job_center;
use App\customer;
use App\customer_data;
use App\discount;
use App\discount_quotation;
use App\employee;
use App\event;
use App\extra;
use App\Http\Controllers\Quotations\CommonQuotation;
use App\payment_method;
use App\payment_way;
use App\plague_type_quotation;
use App\PriceList;
use App\profile_job_center;
use App\quotation;
use App\service_order;
use App\User;
use DB;
use Exception;

class ExpressQuoteDao
{
    /**
     * @param $data
     * @param $isSchedule
     * @return array
     */
    public static function createExpressQuote($data, $isSchedule)
    {
        try {
            DB::beginTransaction();

            $companyId = $data['companyId'];
            $jobCenterId = $data['jobCenterId'];
            $customerName = $data['customerName'];
            $cellphone = $data['cellphone'];
            $establishmentId = $data['establishmentId'];
            $colony = $data['colony'];
            $address = $data['address'];
            $addressNumber = $data['addressNumber'];
            $sourceOriginId = $data['sourceOriginId'];
            $total = $data['total'];
            $employeeId = $data['userId'];
            $technicianId = $data['employeeId'];
            $plagues = $data['plagues'];
            $initialDate = $data['initialDate'];
            $initialHour = $data['initialHour'];
            $finalHour = $data['finalHour'];

            $employee = employee::find($employeeId);
            $userId = $employee->employee_id;
            $profileJobCenter = profile_job_center::find($jobCenterId);
            $addressJobCenter = address_job_center::where('profile_job_centers_id',
                $profileJobCenter->profile_job_centers_id)->first();
            $municipality = $addressJobCenter->municipality;
            $state = $addressJobCenter->state;

            $customer = customer::where('id_profile_job_center', $jobCenterId)
                ->where('cellphone', $cellphone)
                ->first();

            if (!$customer) {
                $customer = new customer();
                $customer->name = $customerName;
                $customer->establishment_name = $customerName;
                $customer->cellphone = $cellphone;
                $customer->cellphone_main = $cellphone;
                $customer->establishment_id = $establishmentId;
                $customer->colony = $colony;
                $customer->municipality = $municipality;
                $customer->source_origin_id = $sourceOriginId;
                $customer->id_profile_job_center = $jobCenterId;
                $customer->companie = $companyId;
                $customer->save();

                $customerDatas = new customer_data();
                $customerDatas->customer_id = $customer->id;
                $customerDatas->address = $address;
                $customerDatas->address_number = $addressNumber;
                $customerDatas->state = $state;
                $customerDatas->phone_number = $cellphone;
                $customerDatas->save();
            }

            $idPriceListFind = PriceList::where('establishment_id', $establishmentId)
                ->where('profile_job_center_id', $jobCenterId)
                ->where('status', 1)
                ->first();

            $idExtra = extra::where('profile_job_center_id', $jobCenterId)
                ->where('description', 'No borrar. Este extra es propio del sistema, si se borra el sistema dejará de funcionar.')
                ->first();

            if (!$idPriceListFind) {
                DB::rollBack();
                return [
                    'status' => false,
                    'message' => "No existe ninguna lista de precio para el servicio seleccionado."
                ];
            }

            $quote = new quotation();
            $quote->id_customer = $customer->id;
            $quote->construction_measure = 100;
            $quote->id_plague_jer = 1;
            $quote->establishment_id = $establishmentId;
            $quote->id_status = 10;
            $quote->total = $total;
            $quote->price = $total;
            $quote->user_id = $userId;
            $quote->id_job_center = $jobCenterId;
            $quote->companie = $companyId;
            $quote->id_extra = $idExtra->id;
            $quote->id_price_list = $idPriceListFind->id;
            $quote->id_quotation = CommonQuotation::getFolioQuoteWithCompany($companyId);
            $quote->save();

            if ($plagues != "") {
                $arrayPlagues = explode(",", $plagues);
                for ($i = 0; $i < count($arrayPlagues); $i++) {
                    $plague = new plague_type_quotation();
                    $plague->quotation_id = $quote->id;
                    $plague->plague_type_id = $arrayPlagues[$i];
                    $plague->save();
                }
            }

            //Asignar descuento a cotización.
            $discountId = discount::where('description', 'No borrar. Este descuento es propio del sistema, si se borra el sistema dejará de funcionar.')
                ->where('profile_job_center_id', $jobCenterId)
                ->first();
            $discount = new discount_quotation();
            $discount->quotation_id = $quote->id;
            $discount->discount_id = $discountId->id;
            $discount->save();

            $message = "Cotización creada correctamente.";

            if ($isSchedule) {
                $order = self::createServiceOrder($quote);
                self::createEvent($customer, $technicianId, $order, $initialDate, $initialHour, $finalHour, $quote);
                $message = "Orden de servicio programada.";
            }

            DB::commit();

            return [
                'status' => true,
                'message' => $message
            ];

        } catch (Exception $exception) {
            DB::rollBack();
            return [
                'status' => false,
                'message' => "Algo salio mal, intenta de nuevo."
            ];
        }
    }

    /**
     * Create a new service order
     * @param $quotation
     * @return service_order
     */
    private static function createServiceOrder($quotation) {
        $folio = explode('-', $quotation->id_quotation);
        $folio = $folio[1];

        $paymentWay = payment_way::where('profile_job_center_id', $quotation->id_job_center)->first();
        $paymentMethod = payment_method::where('profile_job_center_id', $quotation->id_job_center)->first();

        $service = new service_order;
        $service->id_service_order = 'OS-' . $folio;
        $service->id_quotation = $quotation->id;
        $service->user_id = $quotation->user_id;
        $service->id_payment_method = $paymentMethod->id;
        $service->id_payment_way = $paymentWay->id;
        $service->id_status = 4;
        $service->id_job_center = $quotation->id_job_center;
        $service->companie = $quotation->companie;
        $service->email = "";
        $service->observations = "";
        $service->total = $quotation->total;
        $service->warranty = 0;
        $service->reinforcement = 0;
        $service->tracing = 0;
        $service->whatsapp = 0;
        $service->confirmed = 0;
        $service->reminder = 0;
        $service->save();

        return $service;
    }

    /**
     * Create a new event
     * @param $customer
     * @param $employeeId
     * @param $order
     * @param $date
     * @param $initialHour
     * @param $finalHour
     * @param $quotation
     */
    public static function createEvent($customer, $employeeId, $order, $date, $initialHour, $finalHour, $quotation) {
        $event = new event();
        $event->title = "Fumigación de " . $customer->name;
        $event->id_employee = $employeeId;
        $event->id_service_order = $order->id;
        $event->initial_hour = $initialHour;
        $event->final_hour = $finalHour;
        $event->initial_date = $date;
        $event->final_date = $date;
        $event->start_event = "00:00";
        $event->final_event = "00:00";
        $event->id_job_center = $quotation->id_job_center;
        $event->companie = $quotation->companie;
        $event->id_status = 1;
        $event->service_type = 1;
        $event->save();

        //change status to schedule quote
        $quote = quotation::find($quotation->id);
        $quote->id_status = 4;
        $quote->save();

        //send notification app technician
        $employee = employee::find($employeeId);
        $message = "Fecha: " . $date . " (" . $initialHour . ")  " . $order->id_service_order;
        $user = User::find($employee->employee_id);
        $user->sendFCM('Nuevo Servicio', $message);
    }
}