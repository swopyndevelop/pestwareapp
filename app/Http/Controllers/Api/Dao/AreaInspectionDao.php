<?php


namespace App\Http\Controllers\Api\Dao;

use App\Area;
use App\Area_tree;
use App\AreaInspectionPhotos;
use App\AreaInspectionPlagues;
use App\AreaInspections;
use App\custom_quote;
use App\customer;
use App\event;
use App\Http\Controllers\Api\CommonMonitoring;
use App\Monitoring;
use App\Notifications\LogsNotification;
use App\payment_method;
use App\payment_way;
use App\quotation;
use App\service_order;
use App\StationInspection;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AreaInspectionDao
{

    public static function saveInspectionArea($request)
    {
        try {

            DB::beginTransaction();

            // Get data request.
            $idAreaNode = $request->get('id_area_node');
            $idTechnician = $request->get('id_technician');
            $comments = $request->get('comments');
            $date = Carbon::now()->toDateString();
            $hour = Carbon::now()->toTimeString();
            $plagues = $request->get('plagues');
            $grades = $request->get('grades');
            $idOrder = 0;
            if ($request->exists('id_order')) {
                $idOrder = $request->get('id_order');
            }

            //If check exists service order
            //$node = Area_tree::find($idAreaNode);
            //$areaCustomer = Area::find($node->id_area);
            /*$order = DB::table('events as ev')
                ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->select('so.id', 'q.id_customer')
                ->where('q.id_customer', $areaCustomer->id_customer)
                ->where('ev.initial_date', Carbon::now()->toDateString())
                ->first();*/

            $inspection = AreaInspections::where('id_area_node', $idAreaNode)
                ->where('id_technician', $idTechnician)
                ->where('date_inspection', Carbon::now()->toDateString())
                ->where('id_service_order', $idOrder)
                ->first();

            if (!$inspection) {
                // Create at new area inspection.
                $inspection = new AreaInspections();
                $inspection->id_area_node = $idAreaNode;
                $inspection->id_technician = $idTechnician;
            }

            $inspection->id_service_order = $idOrder;
            $inspection->comments = $comments;
            $inspection->date_inspection = $date;
            $inspection->hour_inspection = $hour;
            $inspection->save();

            // Create at plagues by inspection.
            $arrayPlagues = explode(",", $plagues);
            $arrayGrades = explode(",", $grades);
            for ($i = 0; $i < count($arrayPlagues); $i++) {
                $plague = new AreaInspectionPlagues();
                $plague->id_area_inspection = $inspection->id;
                $plague->id_plague = $arrayPlagues[$i];
                $plague->id_infestation_degree = $arrayGrades[$i];
                $plague->save();
            }

            DB::commit();
            return [
                'status' => 201,
                'inspection_id' => $inspection->id
            ];

        } catch (Exception $exception) {
            DB::rollBack();
            $error = "API-AREA " . $exception->getMessage();
            Log::debug($error);
            return false;
        }
    }

    public static function validateQrArea($nodeId, $employeeId)
    {
        try {
            $areaTree = Area_tree::find($nodeId);
            if ($areaTree) {
                $area = Area::find($areaTree->id_area);
                $customer = customer::find($area->id_customer);
                $perimeter = Area_tree::where('id_node', $areaTree->parent)->where('id_area', $area->id)->first();
                $zone = Area_tree::where('id_node', $perimeter->parent)->where('id_area', $area->id)->first();
                if ($area->is_inspection == 1) {
                    return [
                        'status' => true,
                        'customer' => $customer->name,
                        'area' => $areaTree->text,
                        'zone' => $zone->text,
                        'perimeter' => $perimeter->text,
                        'parent' => $areaTree->parent,
                        'is_inspection' => true,
                        'message' => 'Código valido.'
                    ];
                }
                // Create service order.
                $data = self::isScheduleServiceNow($customer->id);
                if ($data != false) {
                    // Make service order.
                    $quotation = quotation::find($data);
                    $order = self::createServiceOrder($quotation, $nodeId);
                    if ($order != false) {
                        self::createEvent(
                            $employeeId,
                            $order,
                            Carbon::now()->toDateString(),
                            Carbon::now()->toTimeString(),
                            Carbon::now()->addMinutes(30)->toTimeString(),
                            $quotation,
                            $areaTree->text
                        );
                        return [
                            'status' => true,
                            'customer' => $customer->name,
                            'area' => $areaTree->text,
                            'zone' => $zone->text,
                            'perimeter' => $perimeter->text,
                            'parent' => $areaTree->parent,
                            'is_inspection' => false,
                            'message' => 'Orden generada correctamente.'
                        ];
                    } else {
                        User::first()->notify(new LogsNotification("Log Area", "NO SE CREO OS", 3));
                        return false;
                    }
                }
                User::first()->notify(new LogsNotification("Log Area", "NO SE ENCONTRO OS", 3));
                return false;

            }
            User::first()->notify(new LogsNotification("Log Area", "NO SE ECONTRO AREA TREE", 3));
            return false;
        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification("Log Area", $exception->getMessage(), 3));
            return false;
        }
    }

    public static function saveAreaOnlyPicture($nodeId, $fileRoute, $technicianId) {

        try {

            $inspection = AreaInspections::where('id_area_node', $nodeId)
                ->where('id_technician', $technicianId)
                ->where('date_inspection', Carbon::now()->toDateString())
                ->first();

            if (!$inspection) {
                $inspection = new AreaInspections();
                $inspection->id_area_node = $nodeId;
                $inspection->id_technician = $technicianId;
                $inspection->date_inspection = Carbon::now()->toDateString();
                $inspection->save();
            }

            $photo= new AreaInspectionPhotos();
            $photo->id_area_inspection = $inspection->id;
            $photo->photo = $fileRoute;
            $photo->save();

            return ResponseCodes::$SUCCESS;

        } catch (\Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }

    private static function isScheduleServiceNow($customerId)
    {
        $orderNow = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->where('ev.initial_date', Carbon::now()->toDateString())
            ->where('c.id', $customerId)
            ->select('q.id as quotationId')
            ->first();
        if ($orderNow) return $orderNow->quotationId;
        return false;
    }

    /**
     * Create a new service order
     * @param $quotation
     * @param $areaId
     * @return service_order|false
     */
    private static function createServiceOrder($quotation, $areaId) {
        try {
            $time = Carbon::now()->timestamp;
            $folio = explode('-', $quotation->id_quotation);
            $folio = $folio[1];
            $folioOrder = 'OS-' . $folio . '-A-' . $areaId . '-' . $time;

            $paymentWay = payment_way::where('profile_job_center_id', $quotation->id_job_center)->first();
            $paymentMethod = payment_method::where('profile_job_center_id', $quotation->id_job_center)->first();

            $isExistOrder = service_order::where('id_service_order', $folioOrder)->first();
            if ($isExistOrder) {
                User::first()->notify(new LogsNotification("Log Area", "YA EXISTE OS", 3));
                return false;
            }

            $service = new service_order;
            $service->id_service_order = $folioOrder;
            $service->id_quotation = $quotation->id;
            $service->user_id = $quotation->user_id;
            $service->id_payment_method = $paymentMethod->id;
            $service->id_payment_way = $paymentWay->id;
            $service->id_status = 4;
            $service->id_job_center = $quotation->id_job_center;
            $service->companie = $quotation->companie;
            $service->email = "";
            $service->observations = "";
            $service->total = $quotation->total;
            $service->warranty = 0;
            $service->reinforcement = 0;
            $service->tracing = 0;
            $service->whatsapp = 0;
            $service->confirmed = 0;
            $service->reminder = 0;
            $service->area_node_id = $areaId;
            $service->save();

            return $service;
        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification("Log Area", $exception->getMessage(), 3));
            return false;
        }
    }

    /**
     * Create a new event
     * @param $employeeId
     * @param $order
     * @param $date
     * @param $initialHour
     * @param $finalHour
     * @param $quotation
     * @param $text
     */
    public static function createEvent($employeeId, $order, $date, $initialHour, $finalHour, $quotation, $text) {
        $event = new event();
        $event->title = "Fumigación de " . $text;
        $event->id_employee = $employeeId;
        $event->id_service_order = $order->id;
        $event->initial_hour = $initialHour;
        $event->final_hour = $finalHour;
        $event->initial_date = $date;
        $event->final_date = $date;
        $event->start_event = "00:00";
        $event->final_event = "00:00";
        $event->id_job_center = $quotation->id_job_center;
        $event->companie = $quotation->companie;
        $event->id_status = 1;
        $event->service_type = 1;
        $event->save();

        //send notification app technician
        $message = "Fecha: " . $date . " (" . $initialHour . ")  " . $order->id_service_order;
        $user = User::find($order->user_id);
        $user->sendFCM('Nuevo Servicio', $message);
    }
}