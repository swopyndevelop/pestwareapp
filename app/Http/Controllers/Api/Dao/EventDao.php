<?php


namespace App\Http\Controllers\Api\Dao;


use App\Attendance;
use App\canceled_service_order;
use App\Common\CommonCarbon;
use App\event;
use App\Http\Controllers\Services\AppServicesController;
use App\Jobs\SendDocsEmailCustomer;
use App\Jobs\UpdateMipCustomer;
use App\MailAccount;
use App\Notifications\LogsNotification;
use App\quotation;
use App\service_order;
use App\User;
use Carbon\Carbon;
use DB;
use Exception;

class EventDao
{

    public static function startService($eventId, $hour) {
        try {

            $event = event::find($eventId);
            $event->id_status = 2;
            $event->start_event = $hour;
            $event->save();

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }

    public static function canceledService($eventId, $reason, $commentary, $date) {
        try {

            $event = event::find($eventId);
            $event->id_status = 3;
            $event->save();

            $canceled = new canceled_service_order;
            $canceled->id_service_order = $event->id_service_order;
            $canceled->reason = $reason;
            $canceled->commentary = $commentary;
            $canceled->date = $date;
            $canceled->save();

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }

    public static function finishedService($eventId, $hour, $customerEmail, $inputEmail, $orderId) {
        try {

            $event = event::find($eventId);
            $event->id_status = 4;
            $event->final_event = $hour;
            $event->save();

            // Adding date expired certificate.
            $priceList = DB::table('service_orders as so')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('customers as c', 'q.id_customer','c.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->select('pl.days_expiration_certificate as dec_pl','c.days_expiration_certificate as dec_c','so.id')
                ->where('so.id', $event->id_service_order)
                ->first();

            if ($priceList) {
                if ($priceList->dec_pl != 0 || $priceList->dec_c != 0) {
                    $dateExpiration = Carbon::now()->addDays($priceList->dec_pl)->toDateString();
                    if ($priceList->dec_c != 0) $dateExpiration = Carbon::now()->addDays($priceList->dec_c)->toDateString();
                    $order = service_order::find($event->id_service_order);
                    $order->date_expiration_certificate = $dateExpiration;
                    $order->save();
                }
            }

            // Send pdf certificate to customer
            if ($customerEmail != "0" && $inputEmail != "0") {
                AppServicesController::mail($customerEmail, $inputEmail, $orderId);
            }
            // Create and save report pdf
            UpdateMipCustomer::dispatch($orderId);

            // Send documents pdf for emails to customer
            $order = service_order::find($orderId);
            $quote = quotation::find($order->id_quotation);
            SendDocsEmailCustomer::dispatch($quote->id_customer, $orderId);

            // Register attendance
            if ($event->companie == 220 || $event->companie == 503 || $event->companie == 37) {
                $timezone = CommonCarbon::getTimeZoneByJobCenter($event->id_job_center);
                $now = Carbon::now()->timezone($timezone);
                $events = event::where('id_employee', $event->id_employee)
                    ->where('initial_date', $now->toDateString())
                    ->orderBy('initial_hour')
                    ->get();
                $eventFirst = $events->first();
                $eventLast = $events->last();

                $attendanceEmployeeNow = Attendance::where('id_employee', $event->id_employee)
                    ->where('date', $now->toDateString())
                    ->first();

                if (!$attendanceEmployeeNow) $attendanceEmployeeNow = new Attendance();

                if ($eventFirst->id == $eventId) {
                    $attendanceEmployeeNow->id_employee = $event->id_employee;
                    $attendanceEmployeeNow->id_company = $event->companie;
                    $attendanceEmployeeNow->date = $now->toDateString();
                    $attendanceEmployeeNow->start_hour = $now->toTimeString();
                    $attendanceEmployeeNow->save();
                } elseif ($eventLast->id == $eventId) {
                    $attendanceEmployeeNow->id_employee = $event->id_employee;
                    $attendanceEmployeeNow->id_company = $event->companie;
                    $attendanceEmployeeNow->end_hour = $now->toTimeString();
                    $attendanceEmployeeNow->save();
                }
            }

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            User::first()->notify(new LogsNotification("LOG", $e->getMessage(), 3));
            return ResponseCodes::$ERROR;
        }
    }
}