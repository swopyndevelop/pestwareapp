<?php


namespace App\Http\Controllers\Api\Dao;


use App\expense;
use App\article_expense;
use App\archive_expense;
use App\Http\Controllers\Accounting\CommonAccounting;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\Dao\ResponseCodes;

class ExpenseDao
{
    public static function saveExpense($isFile, $name, $expenseName, $description, $idConcept, $idMethodWay, $idMethod, $article,
        $mount, $daysCredit, $idUser, $idVoucher, $companie, $file) {

        try {

            DB::beginTransaction();
                $employee = DB::table('employees')->where('id', $idUser)->select('profile_job_center_id', 'employee_id')->first();
    
                $expense = new expense;
                $expense->id_expense = CommonAccounting::getFolioExpenseByApi($companie);
                $expense->expense_name = $expenseName;
                $expense->id_concept = $idConcept;
                $expense->date_expense = Carbon::now()->toDateString();
                $expense->id_user = $employee->employee_id;
                $expense->description_expense = $description;
                $expense->total = $mount;
                $expense->status = 1;
                $expense->id_payment_way = $idMethodWay;
                $expense->credit_days = $daysCredit;
                $expense->id_payment_method = $idMethod;
                $expense->account_status = 1;
                $expense->id_voucher = $idVoucher;
                $expense->id_jobcenter = $employee->profile_job_center_id;
                $expense->companie = $companie;
                $expense->save();
    
                //Articulos
                $art_exp = new article_expense;
                $art_exp->id_expense = $expense->id;
                $art_exp->concept = $article;
                $art_exp->total = $mount;
                $art_exp->companie = $companie;
                $art_exp->save();
    
            DB::commit();
            return [
                'code' => true,
                'id' => $expense->id
            ];
            
        } catch (Exception $e) {
            DB::rollBack();
            return [
                'code' => false,
                'id' => ""
            ];
        }
    }

    public static function saveExpensePicture($id, $fileRoute) {
        
        try {

            $expense = expense::find($id);

            archive_expense::create(['id_expense' => $id,
                'file_route' => $fileRoute, 'type' => 1, 'companie' => $expense->companie]);

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }
}