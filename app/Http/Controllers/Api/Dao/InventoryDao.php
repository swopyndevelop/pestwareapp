<?php


namespace App\Http\Controllers\Api\Dao;


use Illuminate\Support\Facades\DB;

class InventoryDao
{
    public static function getDataInventory($employeeId) {

        try {

            $storehouses = DB::table('storehouses')
                ->where('id_employee', $employeeId)
                ->first();

            $units = DB::table('storehouses')
                ->select(DB::raw('SUM(stock) as total_units'))
                ->groupBy('id_employee')
                ->where('id_employee', $employeeId)
                ->first();

            $total = DB::table('storehouses')
                ->select(DB::raw('SUM(total) as total'))
                ->groupBy('id_employee')
                ->where('id_employee', $employeeId)
                ->first();

            $date = DB::table('storehouses')
                ->select('updated_at as date')
                ->where('id_employee', $employeeId)
                ->orderby('updated_at','DESC')
                ->take(1)->first();

            $storehousesDetail = DB::table('storehouses as s')
                ->join('products as p', 'p.id', 's.id_product', 'p.')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->where('s.id_employee', $employeeId)
                ->select('s.id', 's.stock', 's.stock_other_units', 's.total', 'p.name as product', 'pu.name as unit')
                ->get();

            return response()->json($storehousesDetail);

        }catch (\Exception $exception) {

            return response()->json([]);

        }



    }
}