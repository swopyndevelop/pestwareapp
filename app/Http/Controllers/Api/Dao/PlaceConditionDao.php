<?php


namespace App\Http\Controllers\Api\Dao;


use App\condition_picture;
use App\HistoricalCustomer;
use App\place_condition;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PlaceConditionDao
{
    public static function savePlaceCondition($serviceId, $restrictedAccess, $commentary, $indications, $cleanings) {

        try {
            $arrayCleanings = explode(",", $cleanings);
            DB::beginTransaction();

            $place_condition = place_condition::where('id_service_order', $serviceId)->first();

            if ($place_condition) {
                $place_condition->id_service_order = $serviceId;
                $place_condition->restricted_access = $restrictedAccess;
                $place_condition->commentary = $commentary;
                $place_condition->indications = $indications;
                $place_condition->save();
            } else {
                $place_condition = new place_condition;
                $place_condition->id_service_order = $serviceId;
                $place_condition->restricted_access = $restrictedAccess;
                $place_condition->commentary = $commentary;
                $place_condition->indications = $indications;
                $place_condition->save();
            }

            $place_condition->order_cleanings()->sync($arrayCleanings);

            if (strlen($commentary) > 0) {
                $serviceData = DB::table('events as ev')
                    ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                    ->join('employees as e', 'ev.id_employee', 'e.id')
                    ->join('quotations as q', 'so.id_quotation', 'q.id')
                    ->join('customers as c', 'q.id_customer', 'c.id')
                    ->select('c.id as customer_id', 'e.employee_id as user_id', 'so.id', 'so.id_service_order')
                    ->where('so.id', $serviceId)
                    ->first();

                if ($serviceData) {
                    $historicalCustomer = new HistoricalCustomer();
                    $historicalCustomer->id_customer = $serviceData->customer_id;
                    $historicalCustomer->id_user = $serviceData->user_id;
                    $historicalCustomer->title = 'Condiciones del Lugar (Orden De Servicio): ' . $serviceData->id_service_order;
                    $historicalCustomer->commentary = $commentary;
                    $historicalCustomer->date = Carbon::parse($place_condition->created_at)->toDateString();
                    $historicalCustomer->hour = Carbon::parse($place_condition->created_at)->toTimeString();
                    $historicalCustomer->save();
                }
            }

            DB::commit();
            return ['code' => ResponseCodes::$SUCCESS, 'id' => $place_condition->id];

        }catch (Exception $e) {
            DB::rollBack();
            return ['code' => ResponseCodes::$ERROR, 'message' => $e->getMessage()];
        }
    }

    public static function saveConditionPicture($orderId, $fileRoute) {

        try {

            $isPlaceCondition = place_condition::where('id_service_order', $orderId)->first();

            if ($isPlaceCondition) {
                $condition_picture = new condition_picture;
                $condition_picture->place_condition_id = $isPlaceCondition->id;
                $condition_picture->file_route = $fileRoute;
                $condition_picture->save();
            } else {
                $placeCondition = new place_condition();
                $placeCondition->id_service_order = $orderId;
                $placeCondition->save();

                $condition_picture = new condition_picture;
                $condition_picture->place_condition_id = $placeCondition->id;
                $condition_picture->file_route = $fileRoute;
                $condition_picture->save();
            }

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }
}