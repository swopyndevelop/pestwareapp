<?php


namespace App\Http\Controllers\Api\Dao;

use Carbon\Carbon;
use DB;

class InspectionDao
{
    public static function getCustomersOfInspectionByTechnician($technicianId) {
        try {
            $inspections = DB::table('station_inspections as si')
                ->join('monitorings as m', 'si.id_monitoring', 'm.id')
                ->join('customers as c', 'm.id_customer', 'c.id')
                ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
                ->where('si.id_technician', $technicianId)
                ->select('si.id_monitoring', 'si.id_inspection', 'c.name as customer_name', 'c.cellphone as phone', 'c.colony',
                    'c.municipality', 'cd.address', 'cd.state', 'cd.address_number', 'c.id as id_customer')
                ->groupBy(['c.name'])
                ->get();
            foreach ($inspections as $inspection) {
                $inspections->map(function($inspection){
                    $lastInspection = DB::table('station_inspections')
                        ->where('id_monitoring', $inspection->id_monitoring)
                        ->latest('date')
                        ->first();
                    $countInspections = DB::table('station_inspections')
                        ->where('id_monitoring', $inspection->id_monitoring)
                        ->count();
                    $inspection->date = Carbon::parse($lastInspection->date)->format('M d');
                    $inspection->hour = Carbon::parse($lastInspection->hour)->format('h:i a');
                    $inspection->id_inspection = $lastInspection->id_inspection;
                    $inspection->address_customer = $inspection->address . " " . $inspection->address_number . ", " . $inspection->colony . ". " . $inspection->state;
                    $inspection->count_inspections = $countInspections;
                });
            }
            return response()->json($inspections);

        } catch (\Exception $exception) {
            return response()->json([]);
        }
    }

    public static function getInspectionsByCustomer($customerId) {
        try {
            $inspections = DB::table('station_inspections as si')
                ->join('monitorings as m', 'si.id_monitoring', 'm.id')
                ->join('customers as c', 'm.id_customer', 'c.id')
                ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
                ->where('m.id_customer', $customerId)
                ->select('si.id', 'm.id_monitoring', 'si.id_inspection', 'c.name as customer_name',
                    'c.id as id_customer', 'si.date', 'si.hour', 'm.id as id_monitoring_inc')
                ->get();
            foreach ($inspections as $inspection) {
                $inspections->map(function($inspection){
                    $totals = DB::table('monitoring_trees as mt')
                        ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
                        ->join('type_areas as ta', 'mn.id_type_area', 'ta.id')
                        ->where('mt.id_monitoring', $inspection->id_monitoring_inc)
                        ->where('ta.type', 'station')
                        ->groupBy(['mn.name'])
                        ->select(DB::raw('count(*) as count'), 'mn.name')
                        ->get();
                    $stations = "";
                    foreach ($totals as $i => $station) {
                        $stations = $stations . $station->count . " " . $station->name . "\n";
                    }
                    $inspection->date = Carbon::parse($inspection->date)->format('M d');
                    $inspection->hour = Carbon::parse($inspection->hour)->format('h:i a');
                    $inspection->detail = $stations;
                });
            }
            return response()->json($inspections);
        } catch (\Exception $exception) {
            return response()->json([]);
        }
    }

    public static function getInspectionById($inspectionId) {
        try {
            $inspection = DB::table('station_inspections')
                ->where('id', $inspectionId)
                ->first();
            $stationsInspection = self::getStationsMonitoringByInspection($inspectionId);
            return response()->json([
                'id_inspection' => $inspection->id_inspection,
                'date' => Carbon::parse($inspection->date)->format('M d'),
                'hour' => Carbon::parse($inspection->hour)->format('h:i a'),
                'stations' => $stationsInspection
            ]);
        } catch (\Exception $exception) {
            return response()->json([]);
        }
    }

    private static function getStationsMonitoringByInspection($inspectionId) {
        $stations = DB::table('check_monitoring_responses as cmr')
            ->join('monitoring_trees as mt', 'cmr.id_station', 'mt.id')
            ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
            ->where('cmr.id_inspection', $inspectionId)
            ->select('mt.id as id_node_tree', 'cmr.*', 'mt.*', 'mn.*')
            ->get();
        $stations->map(function($station){
            $perimeter = DB::table('monitoring_trees')
                ->where('id_monitoring', $station->id_monitoring)
                ->where('id_node', $station->parent)
                ->first();
            $perimeterText = $perimeter->text;
            $zone = DB::table('monitoring_trees')
                ->where('id_monitoring', $station->id_monitoring)
                ->where('id_node', $perimeter->parent)
                ->first();
            $zoneText = $zone->text;
            $station->perimeter = $perimeterText;
            $station->zone = $zoneText;
            $conditions = DB::table('station_conditions as sc')
                ->join('monitoring_conditions as mc', 'sc.id_condition', 'mc.id')
                ->where('sc.id_node', $station->id_node_tree)
                ->select('mc.name')
                ->get();
            $conditionsText = "";
            foreach ($conditions as $condition) {
                $conditionsText = $conditionsText . $condition->name . ", ";
            }
            $station->conditions = $conditionsText;
            if ($station->quantity == null) $station->quantity = 0;
        });
        return $stations;
    }
}