<?php


namespace App\Http\Controllers\Api\Dao;


class ResponseCodes
{
    public static $SUCCESS = 200;
    public static $ERROR = 500;
}