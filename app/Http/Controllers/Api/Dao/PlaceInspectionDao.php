<?php


namespace App\Http\Controllers\Api\Dao;


use App\HistoricalCustomer;
use App\inspection_picture;
use App\Notifications\LogsNotification;
use App\place_inspection;
use App\place_inspection_plague_type;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PlaceInspectionDao
{
    public static function savePlaceInspection($serviceId, $nestingAreas, $commentary, $plagues, $grades) {

        try {
            $arrayPlagues = explode(",", $plagues);
            $arrayGradesInfestation = explode(",", $grades);
            DB::beginTransaction();

            $place_inspection = place_inspection::where('id_service_order', $serviceId)->first();

            if ($place_inspection) {
                $place_inspection->id_service_order = $serviceId;
                $place_inspection->nesting_areas = $nestingAreas;
                $place_inspection->commentary = $commentary;
                $place_inspection->save();
            } else {
                $place_inspection = new place_inspection;
                $place_inspection->id_service_order = $serviceId;
                $place_inspection->nesting_areas = $nestingAreas;
                $place_inspection->commentary = $commentary;
                $place_inspection->save();
            }

            for ($i = 0; $i < count($arrayPlagues); $i++) {
                $place_inspection_plague_type = new place_inspection_plague_type;
                $place_inspection_plague_type->place_inspection_id = $place_inspection->id;
                $place_inspection_plague_type->plague_type_id = $arrayPlagues[$i];
                $place_inspection_plague_type->id_infestation_degree = $arrayGradesInfestation[$i];
                $place_inspection_plague_type->save();
            }

            if (strlen($commentary) > 0) {
                $serviceData = DB::table('events as ev')
                    ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                    ->join('employees as e', 'ev.id_employee', 'e.id')
                    ->join('quotations as q', 'so.id_quotation', 'q.id')
                    ->join('customers as c', 'q.id_customer', 'c.id')
                    ->select('c.id as customer_id', 'e.employee_id as user_id', 'so.id', 'so.id_service_order')
                    ->where('so.id', $serviceId)
                    ->first();

                if ($serviceData) {
                    $historicalCustomer = new HistoricalCustomer();
                    $historicalCustomer->id_customer = $serviceData->customer_id;
                    $historicalCustomer->id_user = $serviceData->user_id;
                    $historicalCustomer->title = 'Inspección del Lugar (Orden De Servicio): ' . $serviceData->id_service_order;
                    $historicalCustomer->commentary = $place_inspection->commentary;
                    $historicalCustomer->date = Carbon::parse($place_inspection->created_at)->toDateString();
                    $historicalCustomer->hour = Carbon::parse($place_inspection->created_at)->toTimeString();
                    $historicalCustomer->save();
                }
            }

            DB::commit();
            return ['code' => ResponseCodes::$SUCCESS, 'id' => $place_inspection->id];

        } catch (Exception $e) {
            User::first()->notify(new LogsNotification($e->getMessage(), $e->getLine(), 3));
            DB::rollBack();
            return ['code' => ResponseCodes::$ERROR, 'message' => $e->getMessage()];
        }
    }

    public static function saveInspectionPicture($orderId, $fileRoute) {

        try {

            $isPlaceInspection = place_inspection::where('id_service_order', $orderId)->first();

            if ($isPlaceInspection) {
                $inspection_picture = new inspection_picture;
                $inspection_picture->place_inspection_id = $isPlaceInspection->id;
                $inspection_picture->file_route = $fileRoute;
                $inspection_picture->save();
            } else {
                $placeInspection = new place_inspection();
                $placeInspection->id_service_order = $orderId;
                $placeInspection->nesting_areas = "na";
                $placeInspection->commentary = "";
                $placeInspection->save();

                $inspection_picture = new inspection_picture;
                $inspection_picture->place_inspection_id = $placeInspection->id;
                $inspection_picture->file_route = $fileRoute;
                $inspection_picture->save();
            }

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }
}