<?php


namespace App\Http\Controllers\Api\Dao;

use App\HistoricalCustomer;
use App\HistoricalInventory;
use App\Notifications\LogsNotification;
use App\plague_control;
use App\plague_control_application_method;
use App\plague_control_picture;
use App\plague_control_product;
use App\quotation;
use App\service_order;
use App\storehouse;
use App\transfer_ee_product;
use App\transfer_employee_employee;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\Dao\ResponseCodes;

class PlagueControlDao
{
    public static function savePlagueControl($serviceId, $controlAreas, $commentary, $transferId, $companyId,
                                             $employeeId, $applicationMethods, $products, $dose, $quantity) {

        try {

            $arrayApplicationMethods = explode(",", $applicationMethods);
            $arrayProducts = explode(",", $products);
            $arrayDose = explode(",", $dose);
            $arrayQuantity = explode(",", $quantity);

            DB::beginTransaction();
            $idQuotation = DB::table('service_orders')
                ->select('id_quotation')
                ->where('id', $serviceId)
                ->first()->id_quotation;
            $quote = quotation::find($idQuotation);
            $folio = explode('-', $quote->id_quotation);

            $place_control = plague_control::where('id_service_order', $serviceId)->first();

            if ($place_control) {
                $place_control->id_service_order = $serviceId;
                $place_control->control_areas = $controlAreas;
                $place_control->commentary = $commentary;
                $place_control->save();
            } else {
                $place_control = new plague_control;
                $place_control->id_service_order = $serviceId;
                $place_control->control_areas = $controlAreas;
                $place_control->commentary = $commentary;
                $place_control->save();
            }

            if (strlen($commentary) > 0) {
                $serviceData = DB::table('events as ev')
                    ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                    ->join('employees as e', 'ev.id_employee', 'e.id')
                    ->join('quotations as q', 'so.id_quotation', 'q.id')
                    ->join('customers as c', 'q.id_customer', 'c.id')
                    ->select('c.id as customer_id', 'e.employee_id as user_id', 'so.id', 'so.id_service_order')
                    ->where('so.id', $serviceId)
                    ->first();

                if ($serviceData) {
                    $historicalCustomer = new HistoricalCustomer();
                    $historicalCustomer->id_customer = $serviceData->customer_id;
                    $historicalCustomer->id_user = $serviceData->user_id;
                    $historicalCustomer->title = 'Control de Plagas (Orden De Servicio): ' . $serviceData->id_service_order;
                    $historicalCustomer->commentary = $commentary;
                    $historicalCustomer->date = Carbon::parse($place_control->created_at)->toDateString();
                    $historicalCustomer->hour = Carbon::parse($place_control->created_at)->toTimeString();
                    $historicalCustomer->save();
                }
            }

            if ($transferId == "null"){
                //Create transfer employee to client
                $orderTechnician = service_order::find($serviceId);
                $storehouseClient = DB::table('employees')->where('id_company', $companyId)->where('status', 200)->first(); //status => 200 : Es para identificar el almacen de tipo cliente.
                $user = DB::table('employees')->where('id', $employeeId)->first();
                $transfer = new transfer_employee_employee();
                $transfer->id_transfer_ee = $orderTechnician->is_shared == 1 && $orderTechnician->is_main == 0 ? 'TAC-' . $folio[1] . '-' . $employeeId : 'TAC-' . $folio[1];
                $transfer->id_user = $user->employee_id;
                $transfer->id_employee_origin = $employeeId;
                $transfer->id_employee_destiny = $storehouseClient->id;
                $transfer->companie = $companyId;
                $transfer->save();
            }

            for ($i = 0; $i < count($arrayApplicationMethods); $i++) {
                $plague_control_application_methods = new plague_control_application_method;
                $plague_control_application_methods->plague_control_id = $place_control->id;
                $plague_control_application_methods->id_application_method = $arrayApplicationMethods[$i];
                $plague_control_application_methods->save();
            }

            for ($i = 0; $i < count($arrayProducts); $i++) {
                $products = new plague_control_product;
                $products->plague_control_id = $place_control->id;
                $products->id_product = $arrayProducts[$i];
                $products->dose = $arrayDose[$i];
                $products->quantity = $arrayQuantity[$i];
                $products->save();

                // Update storehouse
                $productData = DB::table('products')->where('id', $arrayProducts[$i])->first();
                $priceByQuantity = $productData->base_price / $productData->quantity;
                $storehouse = DB::table('storehouses')->where('id_employee', $employeeId)
                    ->where('id_product', $arrayProducts[$i])
                    ->first();

                /*// Validate Quantity Inventory <= stock // TODO: Implement
                if ($arrayQuantity[$i] > $storehouse->stock_other_units) {
                    return ['status' => ResponseCodes::$ERROR, 'id' => $place_control->id, 'id_transfer' => $transfer->id];
                }*/

                $price = $storehouse->total / $storehouse->stock_other_units;
                $totalStore = $price * $arrayQuantity[$i];

                // Calculate stock units
                $stockFinal = $storehouse->stock_other_units - $arrayQuantity[$i];
                $stockFinal = $stockFinal / $productData->quantity;
                $stockFinal = ceil($stockFinal);
                $stockFinal = intval($stockFinal);

                $stock = storehouse::find($storehouse->id);
                $stock->stock = $stockFinal;
                $stock->stock_other_units = $stock->stock_other_units - $arrayQuantity[$i];
                if ($stock->stock == 0 && $stock->stock_other_units == 0) $stock->total = 0;
                else $stock->total = $stock->total - $totalStore;
                $stock->save();

                // History Inventory Before Destiny
                $historicalInventory = new HistoricalInventory();
                $historicalInventory->folio = $transfer->id_transfer_ee . '-D';
                $historicalInventory->date = Carbon::now()->toDateString();
                $historicalInventory->hour = Carbon::now()->toTimeString();
                $historicalInventory->id_user = $user->employee_id;
                $historicalInventory->id_type_movement_inventory = 11;
                $historicalInventory->id_product = $arrayProducts[$i];
                $historicalInventory->source = $user->name;
                $historicalInventory->destiny = $storehouseClient->name;
                $historicalInventory->quantity = ceil($arrayQuantity[$i] / $productData->quantity);
                $historicalInventory->fraction_quantity = $arrayQuantity[$i];
                $historicalInventory->before_stock = ceil($arrayQuantity[$i] / $productData->quantity);
                $historicalInventory->after_stock = ceil($arrayQuantity[$i] / $productData->quantity);
                $historicalInventory->fraction_before_stock = $arrayQuantity[$i];
                $historicalInventory->fraction_after_stock = $arrayQuantity[$i];
                $historicalInventory->unit_price = $price;
                $historicalInventory->value_movement = $totalStore;
                $historicalInventory->value_inventory = $totalStore;
                $historicalInventory->is_destiny = 1;
                $historicalInventory->id_profile_job_center = $productData->profile_job_center_id;
                $historicalInventory->id_company = $productData->id_companie;
                $historicalInventory->save();

                // Update storehouse customer
                $storehouseCustomer = DB::table('storehouses')->where('id_employee', $storehouseClient->id)->where('id_product', $arrayProducts[$i])->first();
                if (empty($storehouseCustomer)) {
                    $storehouseCust = new storehouse();
                    $storehouseCust->id_store = "A-0001";
                    $storehouseCust->id_employee = $storehouseClient->id;
                    $storehouseCust->id_product = $arrayProducts[$i];
                    $storehouseCust->stock = $arrayQuantity[$i];
                    $storehouseCust->stock_other_units = $arrayQuantity[$i];
                    $storehouseCust->total = $totalStore;
                    $storehouseCust->companie = $companyId;
                    $storehouseCust->save();
                    $storehouseId = storehouse::find($storehouseCust->id);
                    $storehouseId->id_store = "A-" . $storehouseCust->id;
                    $storehouseId->save();

                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = 0;
                    $updateHistoricalInventory->fraction_before_stock = 0;
                    $updateHistoricalInventory->save();
                }else{
                    $storehouseCust = storehouse::find($storehouseCustomer->id);
                    // Calculate stock units
                    $stockFinal = $storehouseCust->stock_other_units + $arrayQuantity[$i];
                    $stockFinal = $stockFinal / $productData->quantity;
                    $stockFinal = ceil($stockFinal);
                    $stockFinal = intval($stockFinal);
                    //$storehouseCust->stock = $storehouse->stock + $request->units;

                    // update Historical Inventory destiny
                    $updateHistoricalInventory = HistoricalInventory::find($historicalInventory->id);
                    $updateHistoricalInventory->before_stock = $storehouseCust->stock;
                    $updateHistoricalInventory->after_stock = $stockFinal;
                    $updateHistoricalInventory->fraction_before_stock = $storehouseCust->stock_other_units;
                    $updateHistoricalInventory->fraction_after_stock = $storehouseCust->stock_other_units + $arrayQuantity[$i];
                    $updateHistoricalInventory->unit_price = $price;
                    $updateHistoricalInventory->value_movement = $totalStore;
                    $updateHistoricalInventory->value_inventory = $storehouseCust->total + $totalStore;
                    $updateHistoricalInventory->save();

                    $storehouseCust->stock = $stockFinal;
                    $storehouseCust->stock_other_units = $storehouseCust->stock_other_units + $arrayQuantity[$i];
                    if ($storehouseCust->stock == 0 && $storehouseCust->stock_other_units == 0) $storehouseCust->total = 0;
                    else $storehouseCust->total = $storehouseCust->total + $totalStore;
                    $storehouseCust->save();
                }

                // History Inventory Before Origin
                $historicalInventoryOrigin = new HistoricalInventory();
                $historicalInventoryOrigin->folio = $transfer->id_transfer_ee . '-O';
                $historicalInventoryOrigin->date = Carbon::now()->toDateString();
                $historicalInventoryOrigin->hour = Carbon::now()->toTimeString();
                $historicalInventoryOrigin->id_user = $user->employee_id;
                $historicalInventoryOrigin->id_type_movement_inventory = 11;
                $historicalInventoryOrigin->id_product = $arrayProducts[$i];
                $historicalInventoryOrigin->source = $user->name ;
                $historicalInventoryOrigin->destiny = $storehouseClient->name;
                $historicalInventoryOrigin->quantity = ceil($arrayQuantity[$i] / $productData->quantity);
                $historicalInventoryOrigin->fraction_quantity = $arrayQuantity[$i];
                $historicalInventoryOrigin->before_stock = ceil(($stock->stock_other_units + $arrayQuantity[$i]) / $productData->quantity);
                $historicalInventoryOrigin->after_stock = ceil($stock->stock_other_units / $productData->quantity);
                $historicalInventoryOrigin->fraction_before_stock = $stock->stock_other_units + $arrayQuantity[$i];
                $historicalInventoryOrigin->fraction_after_stock = $stock->stock_other_units;
                $historicalInventoryOrigin->unit_price = $price;
                $historicalInventoryOrigin->value_movement = $totalStore;
                $historicalInventoryOrigin->value_inventory = $stock->stock_other_units * $price;
                $historicalInventoryOrigin->is_destiny = 0;
                $historicalInventoryOrigin->id_profile_job_center = $productData->profile_job_center_id;
                $historicalInventoryOrigin->id_company = $productData->id_companie;
                $historicalInventoryOrigin->save();

                //Transfer products
                $transferCCProduct = new transfer_ee_product();
                if ($transferId == "null") $transferCCProduct->id_transfer_ee = $transfer->id;
                else $transferCCProduct->id_transfer_ee = $transferId;
                $transferCCProduct->id_product = $arrayProducts[$i];
                $transferCCProduct->units = $arrayQuantity[$i];
                $transferCCProduct->is_units = 0;
                $transferCCProduct->save();

                //Update total transfer
                if ($transferId == "null") $entryTransfer = transfer_employee_employee::find($transfer->id);
                else $entryTransfer = transfer_employee_employee::find($transferId);
                $entryTransfer->total = $entryTransfer->total + $totalStore;
                $entryTransfer->save();
            }
            DB::commit();
            if ($transferId == "null")
                return ['status' => ResponseCodes::$SUCCESS, 'id' => $place_control->id, 'id_transfer' => $transfer->id];
            else return ['status' => ResponseCodes::$SUCCESS, 'id' => $place_control->id, 'id_transfer' => $transferId];

        } catch (Exception $e) {
            User::first()->notify(new LogsNotification($e->getMessage(), $e->getLine(), 3));
            DB::rollBack();
            return ResponseCodes::$ERROR;
        }
    }

    public static function saveControlPlaguePicture($orderId, $fileRoute) {

        try {

            $isPlagueControl = plague_control::where('id_service_order', $orderId)->first();

            if ($isPlagueControl) {
                $control_picture = new plague_control_picture;
                $control_picture->plague_control_id = $isPlagueControl->id;
                $control_picture->file_route = $fileRoute;
                $control_picture->save();
            } else {
                $plagueControl = new plague_control();
                $plagueControl->id_service_order = $orderId;
                $plagueControl->save();

                $control_picture = new plague_control_picture;
                $control_picture->plague_control_id = $plagueControl->id;
                $control_picture->file_route = $fileRoute;
                $control_picture->save();
            }

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }
}