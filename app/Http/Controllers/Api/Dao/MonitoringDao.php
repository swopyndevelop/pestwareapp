<?php


namespace App\Http\Controllers\Api\Dao;

use App\CheckListImage;
use App\CheckListMonitoringOption;
use App\CheckListMonitoringResponse;
use App\customer;
use App\event;
use App\Http\Controllers\Api\CommonMonitoring;
use App\Monitoring;
use App\Notifications\LogsNotification;
use App\PlagueResponseInspection;
use App\quotation;
use App\service_order;
use App\ShareServiceOrder;
use App\StationCondition;
use App\StationInspection;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Log;

class MonitoringDao
{
    public static function saveStationInspection($stations, $serviceOrderId)
    {
        try {
            DB::beginTransaction();

            $inspectionId = 0;
            foreach ($stations as $i => $station) {

                if ($i == 0) {
                    $monitoring = Monitoring::find($station->id_monitoring);
                    $isInspection = StationInspection::where('id_service_order', $serviceOrderId)->first();
                    if ($isInspection) {
                        $inspectionId = $isInspection->id;
                    } else {
                        // Create at new inspection
                        $inspection = new StationInspection;
                        $inspection->id_inspection = CommonMonitoring::getFolioInspection($station->id_monitoring, $monitoring->id_customer);
                        $inspection->id_monitoring = $station->id_monitoring;
                        $inspection->id_service_order = $serviceOrderId;
                        $inspection->id_technician = $station->id_technician;
                        $inspection->date = $station->date;
                        $inspection->hour = $station->hour;
                        $inspection->comments = "Inspección - Monitoreo de Estaciones.";
                        $inspection->save();
                        $inspectionId = $inspection->id;
                    }
                }

                $isOnline = $station->isOnline;
                if ($isOnline == "false")
                {
                    // Stations inspected
                    $option = CheckListMonitoringOption::where('option', $station->activity)->first();
                    $stationResponse = new CheckListMonitoringResponse;
                    $stationResponse->id_inspection = $inspectionId;
                    $stationResponse->id_station = $station->id_node;
                    $stationResponse->id_check_monitoring_option = $option->id;
                    $stationResponse->value = $station->activity;
                    $stationResponse->comments = $station->comments;
                    $stationResponse->actions = $station->actions;
                    $stationResponse->hour = $station->hour;
                    $stationResponse->save();

                    $arrayConditions = explode(",", $station->conditions);

                    for ($i = 0; $i < count($arrayConditions); $i++) {
                        $conditions = new StationCondition;
                        $conditions->id_node = $station->id_node;
                        $conditions->id_condition = $arrayConditions[$i];
                        $conditions->id_inspection = $inspectionId;
                        $conditions->save();
                    }

                    if ($station->plagues != "") {
                        $arrayPlagues = explode(",", $station->plagues);
                        $arrayQuantity = explode(",", $station->quantity);
                        for ($i = 0; $i < count($arrayPlagues); $i++) {
                            $plague = new PlagueResponseInspection;
                            $plague->id_station_response = $stationResponse->id;
                            $plague->id_plague = $arrayPlagues[$i];
                            $plague->quantity = $arrayQuantity[$i];
                            $plague->save();
                        }
                    }
                }

            }

            DB::commit();
            return ['code' => ResponseCodes::$SUCCESS, 'id' => $inspectionId];
        } catch (Exception $e) {
            User::first()->notify(new LogsNotification($e->getLine(), $e->getMessage(), 3));
            DB::rollBack();
            $error = "LINE: " . $e->getLine() . " MESSAGE: " .$e->getMessage();
            Log::error($error);
            return ['code' => ResponseCodes::$ERROR, 'message' => $e->getMessage()];
        }

    }

    public static function saveStationPicture($idInspection, $idStation, $fileRoute) {

        try {

            $images = new CheckListImage;
            $images->id_inspection = $idInspection;
            $images->id_station = $idStation;
            $images->urlImage = $fileRoute;
            $images->save();

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }

    public static function saveStationOnlyPicture($idOrder, $idStation, $fileRoute) {

        try {
            $order = service_order::find($idOrder);
            if ($order->is_shared == 1) {
                if ($order->is_main == 0) {
                    $sharesServiceOrder = ShareServiceOrder::where('id_service_order', $idOrder)->first();
                    $idOrder = $sharesServiceOrder->id_service_order_main;
                }
            }
            $isInspection = StationInspection::where('id_service_order', $idOrder)->first();

            if ($isInspection) {
                $images = new CheckListImage;
                $images->id_inspection = $isInspection->id;
                $images->id_station = $idStation;
                $images->urlImage = $fileRoute;
                $images->save();
            } else {
                $order = service_order::find($idOrder);
                $event = event::where('id_service_order', $idOrder)->first();
                $quote = quotation::find($order->id_quotation);
                $customer = customer::find($quote->id_customer);
                $monitoring = Monitoring::where('id_customer', $customer->id)->first();

                $inspection = new StationInspection();
                $inspection->id_inspection = CommonMonitoring::getFolioInspection($monitoring->id, $customer->id);
                $inspection->id_monitoring = $monitoring->id;
                $inspection->id_service_order = $idOrder;
                $inspection->id_technician = $event->id_employee;
                $inspection->date = Carbon::now()->toDateString();
                $inspection->hour = Carbon::now()->toTimeString();
                $inspection->comments = "";
                $inspection->save();

                $images = new CheckListImage;
                $images->id_inspection = $inspection->id;
                $images->id_station = $idStation;
                $images->urlImage = $fileRoute;
                $images->save();
            }

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }

    public static function saveStationInspectionOnly($station)
    {
        try {
            DB::beginTransaction();
            $orderId = $station->orderId;
            $order = service_order::find($orderId);
            if ($order->is_shared == 1) {
                if ($order->is_main == 0) {
                    $sharesServiceOrder = ShareServiceOrder::where('id_service_order', $orderId)->first();
                    $orderId = $sharesServiceOrder->id_service_order_main;
                }
            }
            $monitoring = Monitoring::find($station->id_monitoring);
            $isInspection = StationInspection::where('id_service_order', $orderId)->first();
            if ($isInspection) $inspectionId = $isInspection->id;
            else {
                // Create at new inspection
                $inspection = new StationInspection;
                $inspection->id_inspection = CommonMonitoring::getFolioInspection($station->id_monitoring, $monitoring->id_customer);
                $inspection->id_monitoring = $station->id_monitoring;
                $inspection->id_service_order = $orderId;
                $inspection->id_technician = $station->id_technician;
                $inspection->date = $station->date;
                $inspection->hour = $station->hour;
                $inspection->comments = "Inspección - Monitoreo de Estaciones.";
                $inspection->save();
                $inspectionId = $inspection->id;
            }

            // Stations inspected
            $option = CheckListMonitoringOption::where('option', $station->activity)->first();
            $stationResponse = new CheckListMonitoringResponse;
            $stationResponse->id_inspection = $inspectionId;
            $stationResponse->id_station = $station->id_node;
            $stationResponse->id_check_monitoring_option = $option->id;
            $stationResponse->value = $station->activity;
            $stationResponse->comments = $station->comments;
            $stationResponse->actions = $station->actions;
            $stationResponse->hour = $station->hour;
            $stationResponse->save();

            $arrayConditions = explode(",", $station->conditions);

            for ($i = 0; $i < count($arrayConditions); $i++) {
                $conditions = new StationCondition;
                $conditions->id_node = $station->id_node;
                $conditions->id_condition = $arrayConditions[$i];
                $conditions->id_inspection = $inspectionId;
                $conditions->save();
            }

            if ($station->plagues != "") {
                $arrayPlagues = explode(",", $station->plagues);
                $arrayQuantity = explode(",", $station->quantity);
                for ($i = 0; $i < count($arrayPlagues); $i++) {
                    $plague = new PlagueResponseInspection;
                    $plague->id_station_response = $stationResponse->id;
                    $plague->id_plague = $arrayPlagues[$i];
                    $plague->quantity = $arrayQuantity[$i];
                    $plague->save();
                }
            }
            DB::commit();
            return response()->json([
                'code' => 201,
                'message' => 'Inspección Guardada Correctamente.'
            ]);
        } catch (Exception $exception) {
            DB::rollBack();
            User::first()->notify(new LogsNotification($exception->getLine(), $exception->getMessage(), 3));
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}