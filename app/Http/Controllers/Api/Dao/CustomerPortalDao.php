<?php


namespace App\Http\Controllers\Api\Dao;

use App\companie;
use App\CustomerDocumentation;
use App\Http\Controllers\ReportsPDF\ServiceCertificate;
use App\Http\Controllers\ReportsPDF\ServiceOrder;
use App\Http\Controllers\ReportsPDF\StationInspectionTable;
use App\Http\Controllers\ReportsPDF\VisitSchedule;
use App\Notifications\LogsNotification;
use App\StationInspection;
use App\User;
use Carbon\Carbon;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PDF;

class CustomerPortalDao
{
    public static function createAndSaveReportPDF($id) {

        // Build service certificate and get data.
        $dataCertificate = ServiceCertificate::build($id);
        $order = $dataCertificate['order'];

        // Validate if exist directory
        $now = Carbon::now();
        $currentYear = $now->year;
        $customerId = $order->customer_id;
        $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $customerId;
        $storagePathService = $storagePathMip . '/' . $currentYear . '/services' . '/';
        $storagePathOrder = $storagePathMip . '/' . $currentYear . '/orders' . '/';
        $storagePathInspection = $storagePathMip . '/' . $currentYear . '/inspections' . '/';

        if (!file_exists($storagePathMip)) Storage::makeDirectory('customer_portal/mip/' . $customerId);

        if (!file_exists($storagePathMip . '/' . $currentYear . '/services')) {
            Storage::makeDirectory('customer_portal/mip/' . $customerId . '/' . $currentYear . '/services');
        }
        if (!file_exists($storagePathMip . '/' . $currentYear . '/orders')) {
            Storage::makeDirectory('customer_portal/mip/' . $customerId . '/' . $currentYear . '/orders');
        }
        if (!file_exists($storagePathMip . '/' . $currentYear . '/inspections')) {
            Storage::makeDirectory('customer_portal/mip/' . $customerId . '/' . $currentYear . '/inspections');
        }
        if (!file_exists($storagePathMip . '/' . $currentYear . '/products')) {
            Storage::makeDirectory('customer_portal/mip/' . $customerId . '/' . $currentYear . '/products');
        }

        // Generate pdf and save report Station Inspection Table
        $isInspection = StationInspection::where('id_service_order', $id)->first();
        if ($isInspection) {
            $dataStationInspection = StationInspectionTable::build($id, $order->compania);
            PDF::loadView('vendor.adminlte.stationmonitoring._stationMonitoringPdf', $dataStationInspection)
                ->setPaper('A4')
                ->save($storagePathInspection. $dataStationInspection['inspection']->id_inspection . '.pdf');
        }

        // Generate pdf and save report Service Certificate
        if ($order->compania == 269) {
            PDF::loadView('vendor.adminlte.register.INFORM.informCustom', $dataCertificate)
                ->setPaper('A4')
                ->save($storagePathService . $order->id_service_order . '.pdf');
        }
        else {
            PDF::loadView('vendor.adminlte.register.INFORM.inform', $dataCertificate)
                ->setPaper('A4')
                ->save($storagePathService . $order->id_service_order . '.pdf');
        }

        // Generate pdf and save report Service Order
        $dataServiceOrder = ServiceOrder::build($id);
        PDF::loadView('vendor.adminlte.register.INFORM.orderService', $dataServiceOrder)
            ->setPaper('A4')
            ->save($storagePathOrder. $dataServiceOrder['order']->id_service_order . '.pdf');

        // Product documents
        $infos = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('products as p','pc.id_product','p.id')
            ->join('aditional_infos as a','p.id','a.product_id')
            ->select('a.file_route')
            ->where('pl.id_service_order', $id)
            ->groupby('a.file_route')
            ->get();

        foreach ($infos as $info) {
            $product = $info->file_route;
            $productMip = 'customer_portal/mip/' . $customerId . '/' . $currentYear . '/' . $info->file_route;
            if (Storage::exists($productMip)) Storage::delete($productMip);
            Storage::copy($product, $productMip);
        }

        // Folders mip
        if ($order->pdf_mip != null) {
            $pdfMip = 'customer_portal/mip/' . $customerId . '/' . $currentYear . '/' . $order->pdf_mip;
            if (Storage::exists($pdfMip)) Storage::delete($pdfMip);
            Storage::copy($order->pdf_mip, $pdfMip);
        }

        return ResponseCodes::$SUCCESS;
    }

    public static function getFolderMIP($customerId, $year)
    {
        try {
            $customer = DB::table('customers as c')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('profile_job_centers as pc', 'c.id_profile_job_center', 'pc.id')
                ->select('c.id', 'cd.address', 'c.name', 'c.cellphone', 'cd.address_number', 'c.colony',
                    'c.municipality', 'cd.state', 'cd.email', 'c.companie', 'pc.sanitary_license')
                ->where('c.id', $customerId)
                ->first();

            $companie = companie::find($customer->companie);
            $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $customerId;
            $storagePathCalendar = $storagePathMip . '/' . $year . '/';

            self::makeCovers($customer, $companie, $year);

            //TODO: Modificar Parametro
            $calendarData = VisitSchedule::build($customerId, $year);
            PDF::loadView('vendor.adminlte.register.PDF.calendarClientsPdf', $calendarData)
                ->setPaper('A4')
                ->save($storagePathCalendar . "CALENDARIO-" . $customerId . '.pdf');

            $licenseSanitaryUrl = $customer->sanitary_license ? env('URL_STORAGE_FTP') . $customer->sanitary_license : $customer->sanitary_license;
            if ($licenseSanitaryUrl != null) {
                copy($licenseSanitaryUrl, $storagePathCalendar . 'LICENSE-SANITARY-'. $customerId . '.pdf');
                $customerDoc = CustomerDocumentation::where('id_customer', $customerId)
                    ->where('section_name', 'Licencia Sanitaria')->first();
                //User::first()->notify(new LogsNotification('LOG CARPETA MIP', 'LINE 131', 3));
                $customerDoc->url_document = 'customer_portal/mip/'.  $customerId . '/' . $year . '/LICENSE-SANITARY-'. $customerId . '.pdf';
                $customerDoc->save();
                //User::first()->notify(new LogsNotification('LOG CARPETA MIP', 'LINE 134', 3));
            }
            $calendar = 'customer_portal/mip/'.  $customerId . '/' . $year . '/' . 'CALENDARIO-'. $customerId . '.pdf';
            $customerDoc = CustomerDocumentation::where('id_customer', $customerId)
                ->where('section_name', 'Calendario de Visitas')->first();
            $customerDoc->url_document = $calendar;
            $customerDoc->save();

            $sections = CustomerDocumentation::where('id_customer', $customerId)->get();
            $documentsForMip = [];

            foreach ($sections as $key => $section) {
                $slug = Str::slug($section->section_name);
                $pathCover = 'customer_portal/mip/'.  $customerId . '/' . $year . '/' . $slug . '-' . $customerId . '.pdf';
                $cover = [$pathCover];
                $document = [$section->url_document];
                $documentsForMip = array_merge($documentsForMip, $cover);
                if ($key > 0) {
                    if ($section->section_name == 'Certificados de Fumigación' ||
                        $section->section_name == 'Ordenes de Servicio' ||
                        $section->section_name == 'Información de Plaguicidas' ||
                        $section->section_name == 'Reporte de Inspecciones' ||
                        $section->section_name == 'Reporte Gráfico de Inspecciones') {
                        $document = Storage::files('customer_portal/mip/' . $customerId . '/' . $year . $section->url_document);
                        $documentsForMip = array_merge($documentsForMip, $document);
                    } elseif ($section->section_name == 'Licencia Sanitaria') {
                        if ($licenseSanitaryUrl != null) $documentsForMip = array_merge($documentsForMip, $document);
                    } else $documentsForMip = array_merge($documentsForMip, $document);
                }
            }

            if ($customerId == 22499) {
                User::first()->notify(new LogsNotification('sections', json_encode($sections), 3));
                User::first()->notify(new LogsNotification('documentsForMip', json_encode($documentsForMip) , 3));
                User::first()->notify(new LogsNotification('documentsForMip', json_encode(count($documentsForMip)) , 3));
            }

            $merger = new Merger(new TcpdiDriver);
            foreach ($documentsForMip as $document) {
                $merger->addFile(storage_path() . '/app/' . $document);
            }
            $mipDoc = $merger->merge();

            $name = 'CARPETA MIP - ' . 'AÑO ' .$year . '.pdf';
            header("Content-type:application/pdf");
            header("Content-Disposition:attachment;filename=$name");
            echo $mipDoc;
            exit;
        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification($exception->getMessage(), $exception->getLine(), 3));
            return Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }

    }

    private static function makeCovers($customer, $companie, $year)
    {
        $customerId = $customer->id;
        $sections = CustomerDocumentation::where('id_customer', $customerId)->get();
        $imagen = DB::table('companies')
            ->select('pdf_logo','pdf_sello','phone','licence','facebook')
            ->where('id', $customer->companie)
            ->first();
        $pdf_logo = env('URL_STORAGE_FTP') . $imagen->pdf_logo;
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha = Carbon::now();
        $mes = $meses[($fecha->format('n')) - 1];
        $inputs= $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
        $elaborado = DB::table('companies as c')
            ->join('contacts as co','c.contact_id','co.id')
            ->select('co.name as responsable')
            ->where('c.id', $customer->companie)->first();
        foreach ($sections as $key => $section) {
            $isHeader = $key == 0;
            $nameCover = Str::slug($section->section_name);
            PDF::loadView('vendor.adminlte.customerportal.cover', [
                'pdf_logo' => $pdf_logo,
                'fecha' =>$inputs,
                'elaborado' => $elaborado,
                'cliente' => $customer,
                'title' => $section->section_name,
                'subtitle' => $section->subtitle,
                'description' => $section->description,
                'isHeader' => $isHeader,
                'cover_color' => $companie->cover_mip_color
            ])->save(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/' . $nameCover . '-' . $customerId . '.pdf');

        }
    }

    public static function getYearsMIPbyCustomer($customerId) {
        $customer = DB::table('customers')->where('id', $customerId)->first();
        $years = Storage::directories('customer_portal/mip/' . $customer->id);
        $mips = new Collection();

        foreach ($years as $year) {
            $separate = explode("/", $year);
            $mips->push([
                'year' => $separate[3],
                'orders' => 20,
                'services' => 10
            ]);
        }

        return response()->json($mips);
    }
}