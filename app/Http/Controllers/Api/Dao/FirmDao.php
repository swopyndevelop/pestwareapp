<?php


namespace App\Http\Controllers\Api\Dao;


use App\Http\Controllers\Api\Dao\CustomerPortalDao;
use App\Http\Controllers\Services\AppServicesController;
use App\Jobs\UpdateMipCustomer;
use App\Notifications\LogsNotification;
use App\service_firms;
use App\User;
use Illuminate\Database\Eloquent\Model;

class FirmDao
{
    public static function saveFirmsPicture($id, $fileRoute, $otherName) {

        try {

            $firm = service_firms::where('id_service_order', $id)->first();
            if (!$firm) $firm = new service_firms();
            $firm->file_route = $fileRoute;
            $firm->id_service_order = $id;
            $firm->other_name = $otherName;
            $firm->save();

            return ResponseCodes::$SUCCESS;

        }catch (\Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }

    public static function saveFirmOtherName($idOrder, $otherName) {
        try {

            $firm = service_firms::where('id_service_order', $idOrder)->first();
            if (!$firm) {
                $firm = new service_firms();
                $firm->file_route = "url_photo_firm";
            }
            $firm->id_service_order = $idOrder;
            $firm->other_name = $otherName;
            $firm->save();

            User::first()->notify(new LogsNotification($otherName, $otherName, 3));

            return ResponseCodes::$SUCCESS;

        }catch (\Exception $e) {
            User::first()->notify(new LogsNotification($e->getMessage(), $e->getLine(), 3));
            return ResponseCodes::$ERROR;
        }
    }
}