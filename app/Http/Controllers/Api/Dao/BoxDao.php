<?php


namespace App\Http\Controllers\Api\Dao;


use App\cash;
use App\cash_picture;
use App\event;
use App\HistoricalCustomer;
use App\payment_method;
use App\payment_way;
use App\service_order;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BoxDao
{
    public static function saveBox($serviceId, $eventId, $companyId, $paymentMethodId, $paymentWayId, $amount,
                                   $commentary, $payment) {

        try {

            DB::beginTransaction();
            /*$ocpc = cash::where('id_service_order', $serviceId);
            if (!empty($ocpc)) {
                //delete payment and save new
                $deletedRows = cash::where('id_service_order', $serviceId)->delete();
            }*/

            $cash = cash::where('id_service_order', $serviceId)->first();
            $order = service_order::find($serviceId);

            if ($paymentMethodId == 0 && $order != null) {
                $paymentMethod = payment_method::where('profile_job_center_id', $order->id_job_center)->first();
                $paymentMethodId = $paymentMethod->id;
            }
            if ($paymentWayId == 0 && $order != null) {
                $paymentWay = payment_way::where('profile_job_center_id', $order->id_job_center)->first();
                $paymentWayId = $paymentWay->id;
            }

            if ($cash) {
                $cash->id_service_order = $serviceId;
                $cash->id_event = $eventId;
                $cash->companie = $companyId;
                $cash->id_payment_method = $paymentMethodId;
                $cash->id_payment_way = $paymentWayId;
                $cash->amount_received = $amount;
                $cash->commentary = $commentary;
                $cash->payment = $payment;
                $cash->save();
            } else {
                $cash = new cash;
                $cash->id_service_order = $serviceId;
                $cash->id_event = $eventId;
                $cash->companie = $companyId;
                $cash->id_payment_method = $paymentMethodId;
                $cash->id_payment_way = $paymentWayId;
                $cash->amount_received = $amount;
                $cash->commentary = $commentary;
                $cash->payment = $payment;
                $cash->save();
            }

            if (strlen($commentary) > 0) {
                $serviceData = DB::table('events as ev')
                    ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                    ->join('employees as e', 'ev.id_employee', 'e.id')
                    ->join('quotations as q', 'so.id_quotation', 'q.id')
                    ->join('customers as c', 'q.id_customer', 'c.id')
                    ->select('c.id as customer_id', 'e.employee_id as user_id', 'so.id', 'so.id_service_order')
                    ->where('so.id', $serviceId)
                    ->first();

                if ($serviceData) {
                    $historicalCustomer = new HistoricalCustomer();
                    $historicalCustomer->id_customer = $serviceData->customer_id;
                    $historicalCustomer->id_user = $serviceData->user_id;
                    $historicalCustomer->title = 'Pago (Orden De Servicio): ' . $serviceData->id_service_order;
                    $historicalCustomer->commentary = $commentary;
                    $historicalCustomer->date = Carbon::parse($cash->created_at)->toDateString();
                    $historicalCustomer->hour = Carbon::parse($cash->created_at)->toTimeString();
                    $historicalCustomer->save();
                }
            }

            DB::commit();
            return ['code' => ResponseCodes::$SUCCESS, 'id' => $cash->id];

        } catch (Exception $e) {
            DB::rollBack();
            return ['code' => ResponseCodes::$ERROR, 'message' => $e->getMessage()];
        }
    }

    public static function saveCashPicture($orderId, $fileRoute) {

        try {

            $isCash = cash::where('id_service_order', $orderId)->first();

            if ($isCash) {
                $cash_picture = new cash_picture;
                $cash_picture->cash_id = $isCash->id;
                $cash_picture->file_route = $fileRoute;
                $cash_picture->save();
            } else {
                $event = event::where('id_service_order', $orderId)->first();
                $order = service_order::find($orderId);
                $cash = new cash();
                $cash->id_service_order = $orderId;
                $cash->id_event = $event->id;
                $cash->id_payment_method = $order->id_payment_method;
                $cash->id_payment_way = $order->id_payment_way;
                $cash->companie = $order->companie;
                $cash->payment = 1;
                $cash->amount_received = $order->total;
                $cash->save();

                $cash_picture = new cash_picture;
                $cash_picture->cash_id = $cash->id;
                $cash_picture->file_route = $fileRoute;
                $cash_picture->save();
            }

            return ResponseCodes::$SUCCESS;

        } catch (Exception $e) {
            return ResponseCodes::$ERROR;
        }
    }
}