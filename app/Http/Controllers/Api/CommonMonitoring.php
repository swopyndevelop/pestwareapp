<?php


namespace App\Http\Controllers\Api;


use App\StationInspection;
use Log;

class CommonMonitoring
{
    public static function getFolioInspection($monitoringId, $customerId)
    {
        $inspection = StationInspection::where('id_monitoring', $monitoringId)
            ->latest()->first();

        if ($inspection) {
            $folio = explode('-', $inspection->id_inspection);
            return 'I-' . $customerId . '-' . ++$folio[2];
        } else return 'I-' . $customerId . '-1';
    }
}