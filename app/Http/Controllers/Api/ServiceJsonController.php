<?php

namespace App\Http\Controllers\Api;

use App\app_service_log;
use App\companie;
use App\employee;
use App\event;
use App\Http\Controllers\Api\Dao\BoxDao;
use App\Http\Controllers\Api\Dao\EventDao;
use App\Http\Controllers\Api\Dao\FirmDao;
use App\Http\Controllers\Api\Dao\ExpenseDao;
use App\Http\Controllers\Api\Dao\MonitoringDao;
use App\Http\Controllers\Api\Dao\PlaceConditionDao;
use App\Http\Controllers\Api\Dao\PlaceInspectionDao;
use App\Http\Controllers\Api\Dao\PlagueControlDao;
use App\Http\Controllers\Api\Dao\ResponseCodes;
use App\Mail\AppServiceLogMail;
use App\MailAccount;
use App\Notifications\LogsNotification;
use App\service_order;
use App\ShareServiceOrder;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ServiceJsonController extends Controller
{
    /**
     * main function
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function packageJsonService(Request $request) {

        try {
            DB::beginTransaction();
                // Log messages
                $logMessages = new Collection();
                $logPhotosArea = new Collection();
                $logPhotosStations = new Collection();

                // Get all request json and convert to object
                $service = json_decode($request->get('json'));
                $eventId = $request->get('id');

                $event = event::find($eventId);

                // Save json service in logs app.
                $json = $request->get('json');
                $error = "BACKUP JSON SERVICE";
                $this->appServiceLog($event, $json, $error, false);

                if ($event->id_status == 4) {
                    return response()->json([
                        'code' => 4,
                        'message' => 'El servicio ya se encuentra finalizado.',
                    ]);
                }

                $hour = $service->start_event;

                $placeInspection = $service->place_inspection;
                $serviceOrderId = $placeInspection->id_service_order;
                $nestingAreas = $placeInspection->nesting_areas;
                $commentary = $placeInspection->commentary;
                $plagues = $placeInspection->plagues;
                $grades = $placeInspection->grades_infestation;

                $placeCondition = $service->place_condition;
                $indications = $placeCondition->indications;
                $cleanings = $placeCondition->cleanings;
                $restricted = $placeCondition->restricted_access;
                $commentaryCondition = $placeCondition->commentary;

                $plagueControl = $service->control_plague;
                $transferId = "null";

                $paymentBox = $service->payment_box;
                $paymentMethodId = $paymentBox->id_payment_method;
                $paymentWayId = $paymentBox->id_payment_way;
                $amount = $paymentBox->amount_received;
                $commentaryPayment = $paymentBox->commentary;
                $payment = $paymentBox->payment;
                $companyId = $paymentBox->id_company;

                $stations = $service->monitoring;

                $finalHour = $service->finish_event;
                $customerEmail = $service->customer_email;
                $inputEmail = $service->input_email;
                $otherNameFirm = $service->other_name;

                // Start service
                $startService = EventDao::startService($eventId, $hour);

                // Place inspection
                $savePlaceInspection = PlaceInspectionDao::savePlaceInspection(
                    $serviceOrderId, $nestingAreas, $commentary, $plagues, $grades
                );
                if ($savePlaceInspection['code'] == ResponseCodes::$SUCCESS) {
                    $logMessages->push([
                        'name' => 'place_inspection',
                        'id' => $savePlaceInspection['id']
                    ]);
                }

                // Place condition
                $savePlaceCondition = PlaceConditionDao::savePlaceCondition(
                    $serviceOrderId, $restricted, $commentaryCondition, $indications, $cleanings
                );
                if ($savePlaceCondition['code'] == ResponseCodes::$SUCCESS) {
                    $logMessages->push([
                        'name' => 'place_condition',
                        'id' => $savePlaceCondition['id']
                    ]);
                }

                // Plague control
                foreach ($plagueControl as $area) {
                    $employeeId = $area->id_employee;
                    $companyId = $area->id_company;
                    $controlAreas = $area->control_areas;
                    $applicationMethods = $area->application_methods;
                    $products = $area->products;
                    $dose = $area->dose;
                    $quantity = $area->quantity;
                    $commentaryPlague = $area->commentary;

                    $pl = PlagueControlDao::savePlagueControl(
                        $serviceOrderId, $controlAreas, $commentaryPlague, $transferId, $companyId, $employeeId,
                        $applicationMethods, $products, $dose, $quantity
                    );

                    if ($pl['status'] == ResponseCodes::$ERROR) {
                        DB::rollBack();
                        return response()->json([
                            'code' => 500,
                            'message' => 'Revisa la existencia de tus productos.'
                        ]);
                    }

                    $transferId = $pl['id_transfer'];
                    $logPhotosArea->push(['area' => $area->control_areas,'id' => $pl['id']]);
                }

                // Service payment
                $saveBox = BoxDao::saveBox(
                    $serviceOrderId, $eventId, $companyId, $paymentMethodId, $paymentWayId, $amount, $commentaryPayment, $payment
                );
                if ($saveBox['code'] == ResponseCodes::$SUCCESS) {
                    $logMessages->push([
                        'name' => 'payment_box',
                        'id' => $saveBox['id']
                    ]);
                }

                // Service firm
                //FirmDao::saveFirmOtherName($serviceOrderId, $otherNameFirm);

                $logMessages->push([
                    'name' => 'firm',
                    'id' => $serviceOrderId
                ]);

                // Monitoring
                if ($stations != "null") {
                    $order = service_order::find($serviceOrderId);
                    if ($order->is_shared == 1) {
                        if ($order->is_main == 0) {
                            $sharesServiceOrder = ShareServiceOrder::where('id_service_order', $serviceOrderId)->first();
                            $serviceOrderId = $sharesServiceOrder->id_service_order_main;
                        }
                    }
                    $saveMonitoring = MonitoringDao::saveStationInspection($stations, $serviceOrderId);
                    if ($saveMonitoring['code'] == ResponseCodes::$SUCCESS) {
                        $logMessages->push([
                            'name' => 'station',
                            'id' => $saveMonitoring['id']
                        ]);
                    }
                }

                // Finished service
                EventDao::finishedService($eventId, $finalHour, $customerEmail, $inputEmail, $serviceOrderId);

            DB::commit();

            return response()->json([
                'code' => ResponseCodes::$SUCCESS,
                'message' => 'Servicio actualizado.'
            ]);

        }catch (Exception $ex) {
            DB::rollBack();
            $eventId = $request->get('id');
            $dataEvent = event::find($eventId);
            $service = $request->get('json');
            $error = $ex->getMessage() . " - " . $ex->getLine();
            $this->appServiceLog($dataEvent, $service, $error, true);
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta más tarde.'
            ]);
        }
    }

    public static function packageJsonPhotos($folder, $id, $route, $idStation, $otherName) {

        $FOLDER_PHOTO_INSPECTION = "services/place_inspections/";
        $FOLDER_PHOTO_CONDITIONS = "services/place_conditions/";
        $FOLDER_PHOTO_CASH = "services/cash/";
        $FOLDER_PHOTO_FIRMS = "services/firms/";
        $FOLDER_PHOTO_CONTROL = "services/plague_controls/";
        $FOLDER_PHOTO_EXPENSE = "services/expenses/";
        $FOLDER_PHOTO_STATIONS = "services/stations/";

        $response = 500;

        switch ($folder) {
            case $FOLDER_PHOTO_INSPECTION:
                // Place inspection
                $response = PlaceInspectionDao::saveInspectionPicture($id, $route);
                break;
            case $FOLDER_PHOTO_CONDITIONS:
                // Place condition
                $response = PlaceConditionDao::saveConditionPicture($id, $route);
                break;
            case $FOLDER_PHOTO_CASH:
                // Payment box
                $response = BoxDao::saveCashPicture($id, $route);
                break;
            case $FOLDER_PHOTO_FIRMS:
                // Firm
                $response = FirmDao::saveFirmsPicture($id, $route, $otherName);
                break;
            case $FOLDER_PHOTO_CONTROL:
                // Plague control
                $response = PlagueControlDao::saveControlPlaguePicture($id, $route);
                break;
            case $FOLDER_PHOTO_EXPENSE:
                $response = ExpenseDao::saveExpensePicture($id, $route);
                break;
            case $FOLDER_PHOTO_STATIONS:
                $response = MonitoringDao::saveStationPicture($id, $idStation, $route);
        }

        return $response;

    }

    public function packageJsonCanceledService(Request $request) {

        try {

            // Get all request json and convert to object
            $service = json_decode($request->get('json'));
            $eventId = $request->get('id');

            $reason = $service->motive;
            $comments = $service->comments;
            $date = $service->date;

            $canceled = EventDao::canceledService($eventId, $reason, $comments, $date);
            if ($canceled == 200) return ['status' => 'ok'];
            else return ['status' => 'Error al guardar los datos.'];

        } catch (Exception $e) {
            return ['status' => 'Error al guardar los datos.'];
        }
    }

    private function appServiceLog($dataEvent, $json, $error_message, $isNotification) {

        $log = new app_service_log;
        $log->employee_id = $dataEvent->id_employee;
        $log->company_id = $dataEvent->companie;
        $log->event_id = $dataEvent->id;
        $log->json = $json;
        $log->error_message = $error_message;
        $log->save();

        if ($isNotification == true) User::first()->notify(new LogsNotification($error_message, $dataEvent, 3));

    }

    public function saveStationInspection(Request $request) {
        return MonitoringDao::saveStationInspectionOnly($request);
    }
}
