<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Dao\CustomerPortalDao;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CustomerPortalApiController extends Controller
{
    public function getFolderMIP($customerId, $year) {
        CustomerPortalDao::getFolderMIP($customerId, $year);
    }

    public function getYearsMIP($customerId) {
        return CustomerPortalDao::getYearsMIPbyCustomer($customerId);
    }

    public function downloadMipLicense($customerId, $year) {
        if ($customerId == 6640) {
            return response()->download(storage_path() .'/app/customer_portal/mip/' . $customerId . '/' . $year . '/LICENSE-SANITARY-6640.pdf');
        }
        if ($customerId == 13172) {
            return response()->download(storage_path() .'/app/customer_portal/mip/' . $customerId . '/' . $year . '/LICENSE-SANITARY-13172.pdf');
        }
    }

    public function downloadMipAnalisis($customerId, $year) {

        if ($customerId == 6640) {
            $merger = new Merger(new TcpdiDriver);
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/AnalisisDeRiesgoGenerales.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/AnalisisDeRiesgoInsectosVoladores.pdf');
            $mipDoc = $merger->merge();

            $name = 'CARPETA MIP - Analisis De Riesgo' . 'AÑO ' . $year . '.pdf';
            header("Content-type:application/pdf");
            header("Content-Disposition:attachment;filename=$name");
            echo $mipDoc;
            exit;
        }

    }

    public function downloadInsurancePolicy($customerId, $year) {
        if ($customerId == 6640) {
            return response()->download(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/PolizaDeSeguro.pdf');
        }
    }

    public function downloadMipPlanWork($customerId, $year) {
        if ($customerId == 6640) {
            $merger = new Merger(new TcpdiDriver);
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/PlanDeManejoIntegradoDePlagasYAreasDeAtencion.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/PlanDeManejoYRecolecciondeEnvasesVacios.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/PREREQUISITOS.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/CALENDARIO-DE -ROTACION-DE-QUIMICOS-2023-2024.pdf');
            $mipDoc = $merger->merge();

            $name = 'CARPETA MIP - Plan de Trabajo' . 'AÑO ' . $year . '.pdf';
            header("Content-type:application/pdf");
            header("Content-Disposition:attachment;filename=$name");
            echo $mipDoc;
            exit;
        }

        if ($customerId == 13172) {
            return response()->download(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/plan-manejo.pdf');
        }
    }

    public function downloadMipProcess($customerId, $year) {

        if ($customerId == 6640) {
            $merger = new Merger(new TcpdiDriver);
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/InsectosVoladores.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/ProcedimientoInsectosRastreros.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/TrampasMecanicasMulticaptura.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/DLqWab2RxROoQxHApAboWaidvgs1hsVWzCHQEjNP.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/WYZag5HZUfndiMBB1wF68OumZez3ANmCPBlpbXsj.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/ZXCdILGvVRcKxiLl5jnx6D2zGZpNvI0Cqj0gOgml.pdf');
            $mipDoc = $merger->merge();

            $name = 'CARPETA MIP - Procedimientos Interior y Exterior' . 'AÑO ' . $year . '.pdf';
            header("Content-type:application/pdf");
            header("Content-Disposition:attachment;filename=$name");
            echo $mipDoc;
            exit;
        }

        if ($customerId == 13172) {
            return response()->download(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/estaciones-cebaderas.pdf');
        }

    }

    public function downloadMipNormas($customerId, $year) {
        if ($customerId == 6640) {
            return response()->download(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/NormasMexicanas.pdf');
        }
        if ($customerId == 13172) {
            return response()->download(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/normas.pdf');
        }
    }

    public function downloadMipCapacition($customerId, $year) {
        if ($customerId == 6640) {
            $merger = new Merger(new TcpdiDriver);
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/ConstaciaCursosControlPlagas.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/DC-3AlejandroCancholaSantana.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/DC3-AngelIvanSandoval.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/DC-3SERGIOALBERTOVILLAREALAMEZCUA.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/DC-3LUISALBERTOGONZALEZSANCHEZ.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/DC3-DANIELGONZALOPEÑAONTIVEROS.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/DC-3BRAVORASCONJOSEANDRES.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/qq3bYkFtYhVxXf7QNRMsXoContfjGy1e6q5XXKI2.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/CsdthiPGt588bJFmWENniuO8PzAm68wufFIz0gwI.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/f61Buxta0bDgG6guoBElLGzekzaaB8yGUsuBM4md.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/0u8yC7wFG37QBjuAIggWJgeC0Sw7imd9mIGor6Bp.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/rYSyEZKTRTNCIVWWmRgeai0tZEGkvCLnjv32E2t0.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/TZMlIyz1RKHJdPNcjOvlL0bz6w4rFIkXsRuLpPH1.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/WjGuMKsHAdZTm7lcVqJAsKQch3tmGYvK9bEB36Dk.pdf');
            $merger->addFile(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/attachments/91HWRj7YkDvMLijj747uRN0wqIhfpPMKC0JsstkU.pdf');
            $mipDoc = $merger->merge();

            $name = 'CARPETA MIP - Capacitacion' . 'AÑO ' . $year . '.pdf';
            header("Content-type:application/pdf");
            header("Content-Disposition:attachment;filename=$name");
            echo $mipDoc;
            exit;
        }

    }

    public function downloadMipProducts($customerId, $year) {
        $orders = Storage::files('customer_portal/mip/' . $customerId . '/' . $year . '/products/');
        $merger = new Merger(new TcpdiDriver);
        foreach ($orders as $order) {
            $merger->addFile(storage_path() . '/app/' . $order);
        }
        $mipDoc = $merger->merge();

        $name = 'CARPETA MIP - Productos' . 'AÑO ' . $year . '.pdf';
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=$name");
        echo $mipDoc;
        exit;
    }

    public function downloadMipCalendar($customerId, $year) {
        if ($customerId == 6640) {
            return response()->download(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/CALENDARIO-6640.pdf');
        }
        if ($customerId == 13172) {
            return response()->download(storage_path() . '/app/customer_portal/mip/' . $customerId . '/' . $year . '/CALENDARIO-13172.pdf');
        }
    }

    public function downloadMipCertificates($customerId, $year) {
        $orders = Storage::files('customer_portal/mip/' . $customerId . '/' . $year . '/services/');
        $merger = new Merger(new TcpdiDriver);
        foreach ($orders as $order) {
            $merger->addFile(storage_path() . '/app/' . $order);
        }
        $mipDoc = $merger->merge();

        $name = 'CARPETA MIP - Certificados de Servicio' . 'AÑO ' . $year . '.pdf';
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=$name");
        echo $mipDoc;
        exit;
    }

    public function downloadMipOrders($customerId, $year) {
        $orders = Storage::files('customer_portal/mip/' . $customerId . '/' . $year . '/orders/');
        $merger = new Merger(new TcpdiDriver);
        foreach ($orders as $order) {
            $merger->addFile(storage_path() . '/app/' . $order);
        }
        $mipDoc = $merger->merge();

        $name = 'CARPETA MIP - Ordenes de Servicio' . 'AÑO ' . $year . '.pdf';
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=$name");
        echo $mipDoc;
        exit;
    }

    public function downloadMipInspections($customerId, $year) {
        $orders = Storage::files('customer_portal/mip/' . $customerId . '/' . $year . '/inspections/');
        $merger = new Merger(new TcpdiDriver);
        foreach ($orders as $order) {
            $merger->addFile(storage_path() . '/app/' . $order);
        }
        $mipDoc = $merger->merge();

        $name = 'CARPETA MIP - Inspecciones' . 'AÑO ' . $year . '.pdf';
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=$name");
        echo $mipDoc;
        exit;
    }

}
