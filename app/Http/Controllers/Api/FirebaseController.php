<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FirebaseController extends Controller
{
    public function postToken(Request $request)
    {
        $idUser = $request->input('id_user');
        $user = User::find($idUser);
        if ($request->has('device_token')) {
            $user->device_token = $request->input('device_token');
            $user->save();
        }
    }

    public function sendNotification(Request $request)
    {   
        fcm()
            ->to(['evG-B-KZ5KY:APA91bGlxmxOJiaDSPjAlhpTzBeHNhA5GYBFLO0xXxOaNuUztfX9ZRurWGE4N1yU0orik1aNsY2zzaSDFgAev90SaYJZMDtuPxN2eBZ5uIxyRoimw6e36Q2dwKpjEEHg9E3DtGNDc-Fq'])
            ->notification([
                'title' => $request->title,
                'body' => $request->message,
            ])
            ->send();
    }
}
