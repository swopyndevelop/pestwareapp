<?php

namespace App\Http\Controllers\Api;

use App\contact;
use App\Mail\NotificationMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Response;

class ServiceApiMailController extends Controller
{
    public function notification()
    {
        try {
            $emails = contact::all();

            $subject = "Actualización de PestWare App - Play Store";
            $message = "Esperando te encuentres muy bien te notificamos por este medio
             y por parte del equipo de soporte que se ha lanzado una nueva versión de la aplicación móvil en la Play Store
             y por lo tanto es necesario que actualices tu aplicación lo antes posible para evitar mal funcionamiento de 
             la misma.";

            foreach ($emails as $email) {
                $result = filter_var($email->email, FILTER_VALIDATE_EMAIL);
                config(['mail.from.name' => 'Pestware App']);
                if($result != false){
                    Mail::to($email->email)->send(new NotificationMail($subject, $message));
                }
            }
            return Response::json('Sends Mails', 200);
        }catch (\Exception $exception) {
            return Response::json($exception);
        }
    }
}
