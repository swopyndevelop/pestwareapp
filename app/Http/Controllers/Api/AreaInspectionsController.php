<?php

namespace App\Http\Controllers\Api;

use App\Area_tree;
use App\Http\Controllers\Api\Dao\AreaInspectionDao;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Response;

class AreaInspectionsController extends Controller
{
    public function store(Request $request)
    {
        $data = AreaInspectionDao::saveInspectionArea($request);
        if ($data != false) {
            return Response::json([
                'code' => 201,
                'message' => 'Inspección Guardada.',
                'customer' =>  "",
                'area' => $data['inspection_id'],
                'zone' => "",
                'perimeter' => ""
            ]);
        }
        return Response::json([
            'code' => 500,
            'message' => 'Algo salio mal, intenta de nuevo.',
            'customer' =>  "",
            'area' => "",
            'zone' => "",
            'perimeter' => ""
        ]);
    }

    public function validateQrArea($nodeId, $employeeId)
    {
        $data = AreaInspectionDao::validateQrArea($nodeId, $employeeId);
        if ($data != false) {
            return Response::json([
                'code' => 200,
                'message' => $data['message'],
                'customer' =>  $data['customer'],
                'area' => $data['area'],
                'zone' => $data['zone'],
                'perimeter' => $data['perimeter'],
                'parent' => $data['parent'],
                'is_inspection' => $data['is_inspection']
            ]);
        }
        return Response::json([
            'code' => 404,
            'message' => 'Algo salio mal, intenta de nuevo.',
            'customer' =>  "",
            'area' => "",
            'zone' => "",
            'perimeter' => "",
            'parent' => ""
        ]);
    }

    public function getCustomersWithAreasByJobCenterId($jobCenterId) {
        $customers = DB::table('areas as a')
            ->join('customers as c', 'a.id_customer', 'c.id')
            ->where('c.id_profile_job_center', $jobCenterId)
            ->select('c.id', 'a.id as id_area', 'a.id_area as folio_area', 'c.name as customer_name')
            ->get();

        return Response::json($customers);
    }

    public function getTreeAreasById($id) {
        $areas = Area_tree::where('id_area', $id)->get();
        $parents = Area_tree::where('id_area', $id)->get();

        $areas->map(function($area) use ($parents) {
            $orders = $parents->filter(function($parent) use ($area) {
                return $parent->id_node == $area->parent;
            });

            $areas = $area;
            $areas['parent_text'] = $orders->first() != null ? $orders->first()->text : "";

            return $areas;
        });

        return Response::json($areas);
    }

    public function getOrdersByCustomerNow($customerId) {
        $orders = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->where('c.id', $customerId)
            ->whereDate('ev.initial_date', Carbon::now()->toDateString())
            ->where('ev.id_status', '<>', '4')
            ->select('so.id', 'so.id_service_order', 'ev.title')
            ->get();
        return Response::json($orders);
    }
}
