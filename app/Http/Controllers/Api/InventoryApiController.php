<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Dao\InventoryDao;
use App\Http\Controllers\Controller;

class InventoryApiController extends Controller
{
    public function getInventoryByEmployee($employeeId) {

        return InventoryDao::getDataInventory($employeeId);
    }
}
