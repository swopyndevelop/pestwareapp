<?php

namespace App\Http\Controllers\Attendance;

use App\Attendance;
use App\CodesAttendance;
use App\Common\CommonCarbon;
use App\employee;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    public function index(Request $request)
    {
        // Filters
        $idEmployee = $request->get('employee_id');
        $initialDateCreated = $request->get('initialDate');
        $finalDateCreated = $request->get('finalDate');

        $attendances = DB::table('attendances as at')
            ->join('employees as emp', 'at.id_employee', 'emp.id')
            ->where('at.id_company', Auth::user()->companie)
            ->select('at.*', 'emp.name as employee_name');

        if ($initialDateCreated != null && $finalDateCreated != null) {
            $attendances->whereDate('at.date', '>=', $initialDateCreated)
                ->whereDate('at.date', '<=', $finalDateCreated);
        }
        if ($idEmployee != null && $idEmployee != 0) $attendances->where('at.id_employee', $idEmployee);


        $attendances = $attendances->orderBy('at.created_at', 'desc')->paginate(10);

        $employees = employee::where('id_company', Auth::user()->companie)
            ->where('status', '<>', 200)
            ->get();

        return view('vendor.adminlte.attendance.index')->with([
            'attendances' => $attendances,
            'employees' => $employees
        ]);
    }

    public function generateCode()
    {
        $now = Carbon::now();
        $random = rand(1000, 9999);
        $code = new CodesAttendance();
        $code->code = $random;
        $code->date = $now->toDateString();
        $code->hour = $now->toTimeString();
        $code->is_active = 1;
        $code->save();

        return response()->json([
            'code' => 200,
            'message' => 'Nuevo Código de Asistencia',
            'data' => $random
        ]);
    }

    public function store(Request $request)
    {
        try {
            $employee = employee::find($request->get('employee_id'));
            $timezone = CommonCarbon::getTimeZoneByJobCenter($employee->profile_job_center_id);
            $now = Carbon::now()->timezone($timezone);

            $type = $request->get('type');
            $code = $request->get('code');

            // Validate code
            /*$isCode = CodesAttendance::where('code', $code)
                ->where('date', $now->toDateString())
                ->where('is_active', 1)
                ->first();
            if (!$isCode) {
                return response()->json([
                    'code' => 500,
                    'message' => 'El código es invalido o ya se uso.'
                ]);
            }*/

            $attendanceEmployeeNow = Attendance::where('id_employee', $employee->id)
                ->where('date', $now->toDateString())
                ->first();

            if (!$attendanceEmployeeNow) $attendanceEmployeeNow = new Attendance();

            if ($type == 'Entrada') {
                $attendanceEmployeeNow->id_employee = $employee->id;
                $attendanceEmployeeNow->id_company = $employee->id_company;
                $attendanceEmployeeNow->date = $now->toDateString();
                $attendanceEmployeeNow->start_hour = $now->toTimeString();
                $attendanceEmployeeNow->save();
            } elseif ($type == 'Salida') {
                $attendanceEmployeeNow->id_employee = $employee->id;
                $attendanceEmployeeNow->id_company = $employee->id_company;
                $attendanceEmployeeNow->end_hour = $now->toTimeString();
                $attendanceEmployeeNow->save();
            }

            // Destroy or inactive code.
            //$isCode->is_active = 0;
            //$isCode->save();

            return response()->json([
                'code' => 200,
                'message' => 'Asistencia Registrada'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al registrar la asistencia'
            ]);
        }
    }
}
