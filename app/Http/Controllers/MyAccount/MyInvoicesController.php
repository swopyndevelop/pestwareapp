<?php

namespace App\Http\Controllers\MyAccount;

use App\companie;
use App\CompanyBillingData;
use App\CompanyInvoices;
use App\customer;
use App\Http\Controllers\Security\SecurityController;
use App\Invoice;
use App\Library\FacturamaLibrary;
use Auth;
use DB;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;

class MyInvoicesController extends Controller
{
    /**
     * @return Factory|Application|View
     */
    public function index()
    {
        $companyId = Auth::user()->companie;
        $companyDataBilling = CompanyBillingData::where('id_company', $companyId)->first();
        $invoices = DB::table('company_invoices as iv')
            ->join('statuses as st', 'iv.id_status', 'st.id')
            ->select('iv.*', 'st.name as status')
            ->where('iv.id_company', $companyId)
            ->orderBy('iv.created_at', 'desc')
            ->paginate(10);

        return view('vendor.adminlte.myaccount.invoices.index')->with([
            'invoices' => $invoices,
            'companyDataBilling' => $companyDataBilling
        ]);
    }

    /**
     * Download invoice by id facturama and type document. (html, Xml, pdf)
     */
    public function downloadInvoiceById($id, $typeDocument) {
        $id = SecurityController::decodeId($id);
        $invoice = CompanyInvoices::find($id);
        $company = companie::find($invoice->id_company);
        $docResponse = FacturamaLibrary::downloadInvoice($invoice->id_invoice, $typeDocument);
        $extension = $typeDocument == 'pdf' ? '.pdf' : '.xml';

        $decoded = base64_decode(end($docResponse));
        $file = $invoice->folio_invoice . ' - ' . $company->name . '-' . $invoice->date . $extension;
        //store file temporarily
        file_put_contents($file, $decoded);

        //download file and delete it
        return response()->download($file)->deleteFileAfterSend(true);
    }

    public function storeDataBilling(Request $request)
    {
        try {
            // Get request data.
            $companyId = Auth::user()->companie;
            $billing = $request->get('billing');
            $rfc = $request->get('rfc');
            $cfdiType = $request->get('cfdiType');
            $email = $request->get('email');
            $address = $request->get('address');
            $exteriorNumber = $request->get('exteriorNumber');
            $interiorNumber = $request->get('interiorNumber');
            $postalCode = $request->get('postalCode');
            $colony = $request->get('colony');
            $municipality = $request->get('municipality');
            $state = $request->get('state');

            //Try store data in db.
            $companyDataBilling = CompanyBillingData::where('id_company', $companyId)->first();
            if (!$companyDataBilling) $companyDataBilling = new CompanyBillingData();

            $companyDataBilling->reason_social = $billing;
            $companyDataBilling->rfc = $rfc;
            $companyDataBilling->email = $email;
            $companyDataBilling->cfdi_type = $cfdiType;
            $companyDataBilling->billing_street = $address;
            $companyDataBilling->billing_exterior_number = $exteriorNumber;
            $companyDataBilling->billing_interior_number = $interiorNumber;
            $companyDataBilling->billing_postal_code = $postalCode;
            $companyDataBilling->billing_colony = $colony;
            $companyDataBilling->billing_municipality = $municipality;
            $companyDataBilling->billing_state = $state;
            $companyDataBilling->id_company = $companyId;
            $companyDataBilling->save();

            return Response::json([
                'code' => 201,
                'message' => 'Datos de Facturación Guardados'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
