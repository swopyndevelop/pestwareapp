<?php

namespace App\Http\Controllers\AreaMonitoring;

use App\Area;
use App\Area_tree;
use App\AreaInspectionPhotos;
use App\Common\CommonImage;
use App\companie;
use App\customer;
use App\employee;
use App\Http\Controllers\ReportsPDF\AreaInspectionTable;
use App\Http\Controllers\Security\SecurityController;
use App\Http\Controllers\Controller;
use App\JobCenterSession;
use App\Mail\AreaInspectionMail;
use App\profile_job_center;
use App\quotation;
use App\service_order;
use App\treeJobCenter;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use PDF;
use Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Business\CommonCompany;

class InspectionsAreasController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function show($jobcenter = 0)
    {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $idJobCenter = 0;
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }
        // Filters Inspections
        $customers = customer::where('id_profile_job_center', $profileJobCenter)->get();
        $addressCustomers = DB::table('customers as c')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->select('c.id', 'cd.address', 'cd.address_number', 'c.colony','cd.state','c.municipality')
            ->where('c.id_profile_job_center', $profileJobCenter)
            ->get();

        return view('vendor.adminlte.areamonitoring.inspections')->with([
            'jobCenters' => $jobCenters,
            'profileJobCenterSelect' => $profileJobCenterSelect,
            'treeJobCenterMain' => $treeJobCenterMain,
            'jobCenterSession' => $jobCenterSession,
            'customers' => $customers,
            'addressCustomers' => $addressCustomers
        ]);
    }

    public function getInspections(Request $request) {
        try {
            $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
            $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);

            // Filters
            $initialDateInspection = $request->get('initialDateInspection');
            $finalDateInspection = $request->get('finalDateInspection');
            $idServiceOrderInspection = $request->get('idServiceOrderInspection');
            $customersFilter = $request->get('customersFilter');
            $addressFilter = $request->get('addressFilter');

            $orders = DB::table('service_orders as so')
                ->join('area_inspections as ai', 'ai.id_service_order', 'so.id')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('customers as cu', 'q.id_customer', 'cu.id')
                ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
                ->join('areas as a', 'a.id_customer', 'cu.id')
                ->select('so.id_service_order', 'ai.date_inspection', 'ai.hour_inspection', 'cu.name as customer',
                    'cu.establishment_name', 'cu.colony', 'cu.municipality', 'cd.address', 'cd.address_number', 'cd.state',
                    'so.id', 'a.id_area', 'cu.id_profile_job_center', 'so.id as id_order','cd.email','cu.cellphone','cu.cellphone_main')
                ->where('cu.id_profile_job_center', $profileJobCenter->id)
                ->orderBy('ai.date_inspection', 'desc');

            if ($idServiceOrderInspection != 'null' ) $orders->where('so.id_service_order','like','%'. $idServiceOrderInspection . '%');
            if ($initialDateInspection != null && $finalDateInspection != null) {
                $orders->whereDate('ai.date_inspection', '>=', $initialDateInspection)
                    ->whereDate('ai.date_inspection', '<=', $finalDateInspection);
            } else {
                $orders->whereDate('ai.date_inspection', '>=', Carbon::now()->toDateString())
                    ->whereDate('ai.date_inspection', '<=', Carbon::now()->toDateString());
            }
            if ($customersFilter != 0) $orders->where('cu.id', $customersFilter);
            if ($addressFilter != 0) $orders->where('cu.id', $addressFilter);

           $ordersFilter = $orders->groupBy(['so.id_service_order'])->get();

            foreach ($ordersFilter as $order) {
                $ordersFilter->map(function ($order) {
                    $inspections = DB::table('area_inspections as ai')
                        ->where('ai.id_service_order', $order->id)
                        ->get();
                    $order->id_encoded = SecurityController::encodeId($order->id);
                    //$order->inspections = $this->mapInspections($inspections);
                    $order->count_areas = $inspections->count();
                    $company = DB::table('companies')->where('id', Auth::user()->companie)->first();
                    $countryCode = DB::table('countries')->where('id', $company->id_code_country)->first();
                    $order->urlWhatsappEmailAreaInspection = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" .
                        "%0d%0dMonitoreo de Áreas: https://pestwareapp.com/area/inspections/pdf/" . $order->id_encoded;
                });
            }

            return Response::json([
                'code' => 200,
                'inspections' => $ordersFilter,
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDetailInspectionByOrderId($orderId) {
        try {
            $inspections = DB::table('area_inspections as ai')
                ->where('ai.id_service_order', $orderId)
                ->get();
            $order = service_order::find($orderId);
            $quote = quotation::find($order->id_quotation);
            $customer = customer::find($quote->id_customer);
            $area = Area::where('id_customer', $customer->id)->first();
            $detail = $this->mapInspections($inspections, $area->id);
            return Response::json([
                'code' => 200,
                'inspections' => $detail
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function areasInspectionPdf($id)
    {
        $id = SecurityController::decodeId($id);
        $dataPdfInspection = AreaInspectionTable::build($id);
        $pdf = PDF::loadView('vendor.adminlte.areamonitoring._areaInspectionPdf',$dataPdfInspection)
            ->setPaper('A4');
        return $pdf->stream();
    }

    public function mailInspectionArea(Request $request)
    {
        try {
            $id = SecurityController::decodeId($request->get('id'));
            // Build service inspection and get data.
            $idCompany = service_order::where('id', $id)->first()->companie;
            $employee = employee::join('profile_job_centers as pjc','profile_job_center_id','pjc.id')
                ->where('companie', Auth::user()->companie)
                ->first();
            $profileJobCenter = profile_job_center::find($employee->profile_job_center_id);
            $dataPdfInspection = AreaInspectionTable::build($id);
            $company = CommonCompany::getCompanyById($idCompany);
            $banner = DB::table('personal_mails')->where('id_company',$company->id)->first();
            $country_code = DB::table('countries')->where('id', $company->id_code_country)->first();
            $whatsapp = 'https://api.whatsapp.com/send?phone='. $country_code->code_country .$profileJobCenter->whatsapp_personal;
            $emails = $request->get('emails');

            $pdf = PDF::loadView('vendor.adminlte.areamonitoring._areaInspectionPdf', $dataPdfInspection);
            config(['mail.from.name' => $profileJobCenter->name]);

            foreach ($emails as $email) {
                $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
                if($result != false){
                    Mail::to($email)->send(new AreaInspectionMail($pdf->output(), $banner, $whatsapp, $dataPdfInspection));
                }
            }
            return \response()->json([
                'code' => 201,
                'message' => 'Área Inspección Enviada'
            ]);
        }
        catch (\Exception $e){
            return \response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intentalo de nuevo'
            ]);
        }
    }

    private function mapInspections($inspections, $areaId) {
        foreach ($inspections as $inspection) {
            $inspections->map(function ($inspection) use ($areaId) {
                $area = Area_tree::find($inspection->id_area_node);
                $perimeter = Area_tree::where('id_node', $area->parent)->where('id_area', $areaId)->first();
                $zone = Area_tree::where('id_node', $perimeter->parent)->where('id_area', $areaId)->first();
                $plagues = DB::table('area_inspections_plagues as aip')
                    ->join('plague_types as pt', 'aip.id_plague', 'pt.id')
                    ->join('infestation_degrees as id', 'aip.id_infestation_degree', 'id.id')
                    ->select('pt.name as plague', 'id.name as grade')
                    ->where('id_area_inspection', $inspection->id)
                    ->get();
                $photos = AreaInspectionPhotos::where('id_area_inspection', $inspection->id)->get();
                foreach ($photos as $image) {
                    $photos->map(function ($image) {
                        $image->url_s3 = CommonImage::getTemporaryUrl($image->photo, 5);
                    });
                }
                $technician = employee::find($inspection->id_technician);
                $inspection->area = $area->text;
                $inspection->zone = $zone->text;
                $inspection->perimeter = $perimeter->text;
                $inspection->plagues = $plagues;
                $inspection->technician = $technician->name;
                $inspection->photos = $photos;
            });
        }
        return $inspections;
    }

}
