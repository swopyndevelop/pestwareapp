<?php

namespace App\Http\Controllers\AreaMonitoring;

use App\companie;
use App\customer;
use App\employee;
use App\event;
use App\Http\Controllers\ReportsPDF\AreaReportInspection;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\profile_job_center;
use App\treeJobCenter;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PDF;

class ReportsInspectionsAreasController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function show($jobCenter = 0)
    {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobCenter != 0) {
            $profileJobCenter = $jobCenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        $customers = DB::table('customers as c')
            ->join('areas as a', 'a.id_customer', 'c.id')
            ->where('c.id_profile_job_center', $profileJobCenter)
            ->select('c.*')
            ->paginate(10);


        //dd($customers);
        // Filters Inspections
        $areas = [];

        return view('vendor.adminlte.reportsAreaMonitoring.index')->with([
            'jobCenters' => $jobCenters,
            'profileJobCenterSelect' => $profileJobCenterSelect,
            'treeJobCenterMain' => $treeJobCenterMain,
            'jobCenterSession' => $jobCenterSession,
            'customers' => $customers
        ]);
    }

    /**
     * Get reports for MIP folder by customer id.
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function getReportByAreaId(Request $request)
    {

        try {
            $idArea = SecurityController::decodeId($request->get('idArea'));
            $data = AreaReportInspection::build($idArea);

            $pdf = PDF::loadView('vendor.adminlte.reportsAreaMonitoring._templateReport', $data)->setPaper('A4');
            //return $pdf->stream('Reporte de Servicio' . 'Range' . '.pdf');
            $year= 2023;

            return response()->stream(
                function () use ($pdf) {
                    echo $pdf->output();
                },
                200,
                [
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename=Certificados_AÑO_' . $year . '.pdf',
                ]
            );

        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }


}
