<?php

namespace App\Http\Controllers\AreaMonitoring;

use App\Area;
use App\Area_tree;
use App\companie;
use App\customer;
use App\CustomQr;
use App\employee;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Jobs\DownloadAreaQr;
use App\Monitoring;
use App\MonitoringNode;
use App\MonitoringTree;
use App\profile_job_center;
use App\treeJobCenter;
use App\TypeArea;
use App\TypeStation;
use Carbon\Carbon;
use Entrust;
use Exception;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use PDF;

class AreaMonitoringController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function index($jobcenter = 0) {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $idJobCenter = 0;
        $jobCenters = \Illuminate\Support\Facades\DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        $search = \Request::get('search');
        $customers = customer::where('id_profile_job_center', $profileJobCenter)->get();
        $areas = DB::table('areas as a')
            ->join('users as u', 'a.id_user', 'u.id')
            ->join('customers as c', 'a.id_customer', 'c.id')
            ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
            ->select('a.id', 'a.id_area', 'a.created_at', 'u.name as user', 'c.name as customer', 'c.id_profile_job_center',
                'c.establishment_name', 'c.colony', 'c.municipality', 'cd.address', 'cd.address_number', 'cd.state', 'a.is_inspection', 'c.id as customer_id',
                'a.status', 'a.date_updated_file', 'a.date_updated_tree', 'a.no_parts', 'a.amount')
            ->where('c.id_profile_job_center', $profileJobCenter)
            ->where('c.name', 'like', '%' . $search . '%')
            ->paginate(10);

        $customersCollection = new Collection();
        foreach ($customers as $customer) {
            $area = DB::table('areas')->where('id_customer', $customer->id)->first();
            $quotationSchedule = DB::table('quotations')->where('id_customer', $customer->id)->where('id_status', 4)->first();
            if (!$area && $quotationSchedule) $customersCollection->push($customer);
        }

        foreach ($areas as $area) {
            $areas->map(function($area){
                $totals = DB::table('areas_tree as at')
                    ->where('at.id_area', $area->id)
                    ->where('at.type_node', 'Área')
                    ->groupBy(['at.type_node'])
                    ->select(DB::raw('count(*) as count'), 'at.type_node')
                    ->get();
                $lastInspection = DB::table('area_inspections as ai')
                    ->join('areas_tree as at','ai.id_area_node','at.id')
                    ->where('at.id_area', $area->id)
                    ->select('ai.*','at.id_area')
                    ->latest()
                    ->first();
                $area->nodes = $totals;
                $area->lastInspection = $lastInspection;
                if ($area->status == 1 && $area->no_parts == null) {
                    $file = Storage::disk('s3')->exists('/area_monitoring/qrs/'. $area->id_area . '.pdf');
                    if ($file) {
                        $monitoring = Area::find($area->id);
                        $monitoring->status = '2';
                        $monitoring->save();
                        $area->status = 2;
                    }
                }
                if ($area->status == 2) {
                    $area->url_pdf ='area_qrs/'. $area->id_area . '.pdf';
                    $updatedAt = Carbon::parse($area->date_updated_tree);
                    $dateUpdatedFile = Carbon::parse($area->date_updated_file);
                    $diff = false;
                    if($updatedAt > $dateUpdatedFile) $diff = true;
                    $area->diffInSeconds = $diff;
                }
                else $area->url_pdf = null;
            });
        }

        return view('vendor.adminlte.areamonitoring.index')->with(['customers' => $customersCollection, 'jobCenterSession' => $jobCenterSession,
            'areas' => $areas, 'jobCenters' => $jobCenters, 'profileJobCenterSelect' => $profileJobCenterSelect, 'treeJobCenterMain' => $treeJobCenterMain]);
    }

    public function saveTree(Request $request) {
        try {
            $data = json_decode($request->get('tree'));
            if (!$this->validTreeStructure($data)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'El formato debe ser: ZONA > PERÍMETRO > ÁREA(S).'
                ]);
            }
            $customerId = $request->get('customerId');
            $monitoringId = $this->addMonitoring($customerId, $request->get('isInspection'));
            $this->addNodes($monitoringId, $data);
            return response()->json([
                'code' => 201,
                'message' => 'Datos guardados.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => $exception->getLine()
            ]);
        }
    }

    public function printCardQr($id)
    {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $id = SecurityController::decodeId($id);
        $totalAreas = DB::table('areas_tree as at')
            ->where('at.id_area', $id)
            ->where('at.type_node', 'Área')
            ->get();
        if ($totalAreas->count() > 0) {
            $area = Area::find($id);
            $companyId = Auth::user()->companie;
            Storage::makeDirectory('area_qrs/' . $area->id_area);
            $parts = $totalAreas->chunk(30);
            foreach ($parts as $i => $part) {
                //Segundo plano
                DownloadAreaQr::dispatch($id, $companyId, $i);
            }
            $area->status = '1';
            $area->date_updated_file = Carbon::now()->toDateTimeString();
            $area->no_parts = $parts->count();
            $area->save();

            return redirect()->route('index_area_station_monitoring')
                ->with('success', 'Tu reporte se esta generando, lo podrás descargar una vez terminado el proceso.');
        } else {
            return redirect()->route('index_area_station_monitoring')
                ->with('warning', 'Debes crear por lo menos una área.');
        }
    }

    public function download($id)
    {
        $id = SecurityController::decodeId($id);
        $area =  Area::find($id);
        $route = 'area_monitoring/qrs/' . $area->id_area . '.pdf';
        return Storage::disk('s3')->response($route);
    }

    private function validTreeStructure($data)
    {
        // Validate min structure:
        if (count($data ) >= 4) {
            $isZone = false;
            $isPerimeter = false;
            $isArea = false;
            foreach ($data as $node) {
                if ($node->parent != '#') {
                    if ($node->type_node == 'Zona' && $node->parent == '1') $isZone = true;
                    if ($node->type_node == 'Perímetro' && $node->parent == 'j1_2') $isPerimeter = true;
                    if ($node->type_node == 'Área' && $node->parent == 'j1_3') $isArea = true;
                }
            }
            if (!$isZone || !$isPerimeter || !$isArea) return false;
            else return true;
        } else return false;
    }

    private function addMonitoring($customerId, $isInspection, $amount) {
        $area = new Area;
        $area->id_area = 'A-' . $customerId;
        $area->id_customer = $customerId;
        $area->is_inspection = $isInspection == "true" ? 1: 0;
        $area->id_user = Auth::user()->id;
        $area->date_updated_tree = Carbon::now()->toDateTimeString();
        $area->amount = $amount;
        $area->save();
        return $area->id;
    }

    private function addNodes($areaId, $data)
    {
        foreach ($data as $i => $node) {
            $this->addNode($node, $areaId, $i);
        }
    }

    private function addNode($node, $areaId, $i) {
        $type = 'main';
        if ($i != 0) $type = $node->type_node;

        $update = new Area_tree;
        $update->id_area = $areaId;
        $update->id_company = Auth::user()->companie;
        $update->id_node = $node->id;
        $update->type_node = $type;
        $update->parent = $node->parent;
        $update->text = trim($node->text);
        $update->save();

    }

    public function getTypeAreaByNodeId($monitoringId) {
        return DB::table('areas_tree as at')
            ->where('at.id_area', $monitoringId)
            ->select('at.id', 'at.type_node', 'at.text')
            ->get();
    }

    public function read($id){
        $tree = DB::table('areas_tree')
            ->select('id_node', 'id', 'parent', 'text')
            ->where('id_area', $id)
            ->where('visible', 1)
            ->where('id_company', Auth::user()->companie)
            ->get();
        $args = array();
        foreach ($tree as $key => $dat) {
            $row = array();
            $row['id']= $dat->id_node;
            $row['parent'] = $dat->parent;
            $row['text']= $dat->text;
            $row['a_attr']['id_test'] = $dat->id;
            array_push($args, $row);
        }
        return Response::json($args);
    }

    public function editTree(Request $request) {
        try {
            $data = json_decode($request->get('tree'));
            $monitoringId = $request->get('monitoringId');
            $isInspection = $request->get('isInspection');
            $oldTree = DB::table('areas_tree')
                ->where('id_area', $monitoringId)
                ->where('parent', '<>', '#')
                ->get();

            $area = Area::find($monitoringId);
            $area->is_inspection = $isInspection == "true" ? 1: 0;
            $area->save();

            $this->updateTree($oldTree, $data, $monitoringId);

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados.'
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => $exception->getLine()
            ]);
        }
    }

    private function updateTree($oldTree, $newTree, $monitoringId)
    {
        foreach ($newTree as $newNode)
        {
            $isContained = false;
            foreach ($oldTree as $oldNode)
            {
                if ($newNode->parent != "#") {
                    if ($oldNode->text == trim($newNode->text)) {
                        DB::table('areas_tree')
                            ->where('id', $oldNode->id)
                            ->where('id_company', Auth::user()->companie)
                            ->update([
                                'id_node' => $newNode->id,
                                'parent' => $newNode->parent,
                                'text' => $newNode->text
                            ]);
                        $isContained = true;
                    }
                }
            }
            if (!$isContained && $newNode->parent != "#") $this->addNode($newNode, $monitoringId, 1);
        }
    }

    public function deleteTree($id) {
        try {
            $area = Area::find($id);
            $area->delete();
            return redirect()->route('index_area_station_monitoring')->with('success', 'Área Eliminada.');
        } catch (Exception $exception) {
            return redirect()->route('index_area_station_monitoring')->with('error', 'Algo salio mal, intenta de nuevo.');
        }
    }

    public function createZone(Request $request){
        try {
            $customerId = $request->get('customerId');
            $zone = $request->get('nameZone');
            $isInspection = $request->get('isInspection');
            $amount = $request->get('amount');

            $customer = customer::find($customerId);
            $area = Area::where('id_customer', $customerId)->first();
            $typeStation = TypeStation::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('id_type_area', 2)->first();
            $typeStationArea = TypeStation::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('id_type_area', 1)->first();

            if (!$area) $areaId = $this->addMonitoring($customerId, $isInspection, $amount);
            else $areaId = $area->id;

            $areaTreeParent = Area_tree::where('id_area', $areaId)
                ->where('parent', '#')->first();

            if (!$areaTreeParent) {
                $areaTree = new Area_tree();
                $areaTree->id_area = $areaId;
                $areaTree->id_company = Auth::user()->companie;
                $areaTree->id_node = 1;
                $areaTree->type_node = 'main';
                $areaTree->parent = '#';
                $areaTree->id_type_area = $typeStationArea->id;
                $areaTree->text = $customer->name;
                $areaTree->save();
            }

            $lastNode = Area_tree::where('id_area', $areaId)
                ->where('parent','<>', '#')
                ->latest('id')
                ->first();

            if ($lastNode) {
                $idNode = explode('_', $lastNode->id_node);

                $areaZone = new Area_tree();
                $areaZone->id_area = $areaId;
                $areaZone->id_company = Auth::user()->companie;
                $areaZone->id_node = 'j1_' . ++$idNode[1];
                $areaZone->type_node = 'Zona';
                $areaZone->parent = '1';
                $areaZone->text = $zone;
                $areaZone->id_type_area = $typeStation->id;
                $areaZone->save();
            } else {
                $monitoringZone = new Area_tree();
                $monitoringZone->id_area = $areaId;
                $monitoringZone->id_company = Auth::user()->companie;
                $monitoringZone->id_node = 'j1_2';
                $monitoringZone->type_node = 'Zona';
                $monitoringZone->parent = '1';
                $monitoringZone->text = $zone;
                $monitoringZone->id_type_area = $typeStation->id;
                $monitoringZone->save();
            }

            $zones = Area_tree::where('id_area', $areaId)
                ->where('parent','1')
                ->get();

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados.',
                'zones' => $zones,
                'areaId' => $areaId
            ]);

        } catch (Exception $exception){
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intenta de Nuevo.'
            ]);
        }

    }

    public function createPerimeter(Request $request) {
        try {
            $customerId = $request->get('customerId');
            $perimeter = $request->get('namePerimeter');
            $idNodeZone = $request->get('idNodeZone');

            $area = Area::where('id_customer', $customerId)->first();
            $customer = customer::find($customerId);
            $typeStation = TypeStation::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('id_type_area', 3)->first();

            $lastNode = Area_tree::where('id_area', $area->id)
                ->where('parent','<>','#')
                ->latest('id')
                ->first();
            $lastZone = Area_tree::where('id_area', $area->id)
                ->where('parent','1')
                ->latest()
                ->first();

            if ($lastNode) {
                $idNode = explode('_', $lastNode->id_node);

                $areaZone = new Area_tree();
                $areaZone->id_area = $area->id;
                $areaZone->id_company = Auth::user()->companie;
                $areaZone->id_node = 'j1_' . ++$idNode[1];
                $areaZone->type_node = 'Perímetro';
                $areaZone->parent = $idNodeZone;
                $areaZone->text = $perimeter;
                $areaZone->id_type_area = $typeStation->id;
                $areaZone->save();
            } else {
                $idNode = explode('_', $lastZone->id_node);
                $areaZone = new Area_tree();
                $areaZone->id_area = $area->id;
                $areaZone->id_company = Auth::user()->companie;
                $areaZone->id_node = 'j1_' . ++$idNode[1];
                $areaZone->type_node = 'Perímetro';
                $areaZone->parent = $idNodeZone;
                $areaZone->text = $perimeter;
                $areaZone->id_type_area = $typeStation->id;
                $areaZone->save();
            }

            $perimeters = Area_tree::where('id_area', $area->id)
                ->join('type_stations as ts', 'areas_tree.id_type_area', 'ts.id')
                ->where('ts.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('areas_tree.id', 'areas_tree.id_node', 'areas_tree.text')
                ->get();

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados.',
                'perimeters' => $perimeters,
                'areaId' => $area->id
            ]);

        } catch (Exception $exception){
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intenta de Nuevo.'
            ]);
        }

    }

    public function createStation(Request $request){
        try {
            $customerId = $request->get('customerId');
            $perimeterSelect = $request->get('perimeterSelect');
            $nameArea = $request->get('nameArea');
            $initialRange = $request->get('initialRange');
            $finalRange = $request->get('finalRange');
            $isOnlyArea = $request->get('isOnlyArea');

            $area = Area::where('id_customer', $customerId)->first();
            $customer = customer::find($customerId);
            $idTypeArea = TypeStation::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('id_type_area', 7)->first();

            // Validations
            $areas = Area_tree::where('id_area', $area->id)->count();
            /*if ($finalRange > 1000) {
                return response()->json([
                    'code' => 1000,
                    'message' => 'Para crear más de 1000 áreas, requiere autorización.'
                ]);
            }*/
            if ($areas > 3000) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Has superado el límite de áreas, requiere autorización.'
                ]);
            }

            $totalAreas = ($finalRange - $initialRange) + 1;
            if ($isOnlyArea == "true") $totalAreas = 1;

            $arrayStations = array_fill(0, $totalAreas, '');
            $folio = $initialRange;

            foreach ($arrayStations as $station) {
                $lastStation = Area_tree::where('id_area', $area->id)
                    ->where('parent','<>','1')
                    ->where('parent','<>','#')
                    ->latest('id')
                    ->first();
                $idNode = explode('_', $lastStation->id_node);

                $monitoringZone = new Area_tree();
                $monitoringZone->id_area = $area->id;
                $monitoringZone->id_company = Auth::user()->companie;
                $monitoringZone->id_node = 'j1_' . ++$idNode[1];
                $monitoringZone->type_node = 'Área';
                $monitoringZone->parent = $perimeterSelect;
                $monitoringZone->text = $isOnlyArea == 'true' ? $nameArea : $nameArea . ' #' . $folio++;
                $monitoringZone->id_type_area = $idTypeArea->id;
                $monitoringZone->save();
            }

            $stationAreas = Area_tree::where('id_area', $area->id)
                ->join('type_stations as ts', 'areas_tree.id_type_area', 'ts.id')
                ->where('ts.id_type_area', 7)
                ->select('areas_tree.*')
                ->get();
            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados.',
                'areaId' => $area->id,
                'stationAreas' => $stationAreas,
                'deleteData' => $deleteData,
                'master' => $master
            ]);

        } catch (Exception $exception){
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intenta de Nuevo.'
            ]);
        }

    }

    public function deleteZoneById($id) {
        try {
            $zone = Area_tree::find($id);
            $perimeters = Area_tree::where('parent', $zone->id_node)->where('id_area', $zone->id_area)->get();
            $zone->delete();
            foreach ($perimeters as $perimeter) {
                $stations = Area_tree::where('parent', $perimeter->id_node)->where('id_area', $zone->id_area);
                $perimeter->delete();
                $stations->delete();
            }
            $zones = Area_tree::where('id_area', $zone->id_area)
                ->where('parent','1')
                ->get();
            return Response::json([
                'code' => 200,
                'message' => 'Zona eliminada correctamente.',
                'zones' => $zones
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function deletePerimeterById($id) {
        try {
            $perimeter = Area_tree::find($id);
            $idArea = $perimeter->id_area;
            $stations = Area_tree::where('parent', $perimeter->id_node)->where('id_area', $perimeter->id_area);
            $perimeter->delete();
            $stations->delete();
            $perimeters = Area_tree::where('id_area', $idArea)
                ->join('type_stations as ts', 'areas_tree.id_type_area', 'ts.id')
                ->where('ts.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('areas_tree.id', 'areas_tree.id_node', 'areas_tree.text')
                ->get();
            return Response::json([
                'code' => 200,
                'message' => 'Perímetro eliminado correctamente.',
                'perimeters' => $perimeters,
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function updateZoneById(Request $request) {
        try {
            $id = $request->get('id');
            $name = $request->get('name');
            $zone = Area_tree::find($id);
            $zone->text = $name;
            $zone->save();
            $zones = Area_tree::where('id_area', $zone->id_area)
                ->where('parent','1')
                ->get();
            return Response::json([
                'code' => 200,
                'message' => 'Zona actualizada correctamente.',
                'zones' => $zones
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function updatePerimeterById(Request $request) {
        try {
            $id = $request->get('id');
            $name = $request->get('name');
            $perimeter = Area_tree::find($id);
            $perimeter->text = $name;
            $perimeter->save();
            $perimeters = Area_tree::where('id_area', $perimeter->id_area)
                ->join('type_stations as ts', 'areas_tree.id_type_area', 'ts.id')
                ->where('ts.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('areas_tree.id', 'areas_tree.id_node', 'areas_tree.text')
                ->get();
            return Response::json([
                'code' => 200,
                'message' => 'Perímetro actualizado correctamente.',
                'perimeters' => $perimeters
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getAllTreeByCustomer($id) {
        try {
            $customerId = $id;
            $area = Area::where('id_customer', $customerId)->first();
            $zones = Area_tree::where('id_area', $area->id)
                ->where('parent','1')
                ->get();
            $perimeters = Area_tree::where('id_area', $area->id)
                ->join('type_stations as ts', 'areas_tree.id_type_area', 'ts.id')
                ->where('ts.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('areas_tree.id', 'areas_tree.id_node', 'areas_tree.text')
                ->get();

            $stationAreas = Area_tree::where('id_area', $area->id)
                ->join('type_stations as ts', 'areas_tree.id_type_area', 'ts.id')
                ->where('ts.id_type_area', 7)
                ->select('areas_tree.*')
                ->get();

            $lastInspection = DB::table('area_inspections as ai')
                ->join('areas_tree as at','ai.id_area_node','at.id')
                ->where('at.id_area', $area->id)
                ->select('ai.*','at.id_area')
                ->latest()
                ->first();

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');

            return Response::json([
                'code' => 200,
                'message' => 'Datos Actualizados.',
                'zones' => $zones,
                'perimeters' => $perimeters,
                'stationAreas' => $stationAreas,
                'areaId' => $area->id,
                'lastInspection' => $lastInspection,
                'deleteData' => $deleteData,
                'master' => $master
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function updateDateMonitoring($areaId, Request $request) {
        try {
            $isInspection = $request->get('isInspection');
            $monitoring = Area::find($areaId);
            $monitoring->date_updated_tree = Carbon::now()->toDateTimeString();
            $monitoring->is_inspection = $isInspection == "true" ? 1: 0;
            $monitoring->amount = $request->get('amount');
            $monitoring->save();
            return Response::json([
                'code' => 200,
                'message' => 'Datos actualizados.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal.'
            ]);
        }
    }

    public function getPerimetersArea(Request $request) {
        try {
            $customerId = $request->get('customerId');
            $area = Area::where('id_customer', $customerId)->first();
            $perimeters = Area_tree::where('id_area', $area->id)
                ->join('type_stations as ts', 'areas_tree.id_type_area', 'ts.id')
                ->where('ts.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->get();

            return Response::json([
                'code' => 200,
                'perimeters' => $perimeters
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function relocateAreasByPerimeter(Request $request) {
        try {
            $idArea = $request->get('idArea');
            $idNodeParent = $request->get('idNodeStation');
            $nameArea = $request->get('nameArea');
            $area = Area_tree::find($idArea);
            $area->parent = $idNodeParent;
            $area->text = $nameArea;
            $area->save();

            return Response::json([
                'code' => 200,
                'message' => 'Estación reubicada correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function cancelAreasByPerimeter(Request $request) {
        try {
            $idArea = $request->get('idArea');
            $area = Area_tree::find($idArea);
            if ($area->visible == 1) $area->visible = 0;
            else $area->visible = 1;
            $area->save();

            return Response::json([
                'code' => 200,
                'message' => 'Se guardo el cambio correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function deleteAreasByPerimeterSingular(Request $request) {
        try {
            $idArea = $request->get('idArea');
            $area = Area_tree::find($idArea);
            $area->delete();

            return Response::json([
                'code' => 200,
                'message' => 'Estación (Área) eliminada correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

}
