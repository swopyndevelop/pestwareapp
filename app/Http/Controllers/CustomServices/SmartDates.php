<?php


namespace App\Http\Controllers\CustomServices;


use Carbon\Carbon;

class SmartDates
{
    public static function getFirstOfMonth($date, $weekDay) {
        $date = Carbon::parse($date);
        return $date->firstOfMonth($weekDay);
    }

    public static function getLastOfMonth($date, $weekDay) {
        $date = Carbon::parse($date);
        return $date->lastOfMonth($weekDay);
    }

    public static function getSecondOfMonth($date, $weekDay) {
        $first = self::getFirstOfMonth($date, $weekDay);
        return $first->addWeek(1);
    }

    public static function getThirdOfMonth($date, $weekDay) {
        $second = self::getSecondOfMonth($date, $weekDay);
        return $second->addWeek(1);
    }

}