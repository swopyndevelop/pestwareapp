<?php


namespace App\Http\Controllers\CustomServices;


use App\event;
use App\service_order;

class CommonCustomServices
{
    /**
     * Create a new service order
     * @param $completeFolio
     * @param $quotationId
     * @param $paymentMethod
     * @param $paymentWay
     * @param $quotation
     * @param $branche
     * @param $concept
     * @return service_order
     */
    public static function createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay, $quotation,
                                        $branche, $concept, $observations) {
        $service = new service_order;
        $service->id_service_order = $completeFolio;
        $service->id_quotation = $quotationId;
        $service->user_id = auth()->user()->id;
        $service->id_payment_method = $paymentMethod;
        $service->id_payment_way = $paymentWay;
        $service->id_status = 4;
        $service->id_job_center = $quotation->id_job_center;
        $service->companie = auth()->user()->companie;
        $service->bussiness_name = $branche->name;
        $service->address = $branche->address;
        $service->email = "";
        $service->observations = $observations;
        $service->total = $concept->unit_price;
        $service->subtotal = $concept->unit_price;
        $service->warranty = 0;
        $service->reinforcement = 0;
        $service->tracing = 0;
        $service->inspection = 0;
        $service->fake = 0;
        $service->whatsapp = 0;
        $service->confirmed = 0;
        $service->reminder = 0;
        $service->customer_branch_id = $branche->id;
        $service->save();

        return $service;
    }

    public static function createServiceOrderInspection($completeFolio, $quotationId, $paymentMethod, $paymentWay, $quotation,
                                              $customer, $observations) {
        $service = new service_order;
        $service->id_service_order = $completeFolio;
        $service->id_quotation = $quotationId;
        $service->user_id = auth()->user()->id;
        $service->id_payment_method = $paymentMethod;
        $service->id_payment_way = $paymentWay;
        $service->id_status = 10;
        $service->id_job_center = $quotation->id_job_center;
        $service->companie = auth()->user()->companie;
        $service->bussiness_name = $customer->name;
        $service->address = $customer->address;
        $service->email = "";
        $service->observations = $observations;
        $service->total = 0;
        $service->subtotal = 0;
        $service->warranty = 0;
        $service->reinforcement = 0;
        $service->tracing = 0;
        $service->inspection = 1;
        $service->fake = 1;
        $service->whatsapp = 0;
        $service->confirmed = 0;
        $service->reminder = 0;
        $service->save();

        return $service;
    }

    /**
     * Create a new event
     * @param $customer
     * @param $branche
     * @param $employeeId
     * @param $order
     * @param $date
     * @param $initialHour
     * @param $finalHour
     * @param $quotation
     */
    public static function createEvent($customer, $branche, $employeeId, $order, $date, $initialHour, $finalHour, $quotation) {
        $event = new event;
        $event->title = $customer->name . " - " . $branche->name;
        $event->id_employee = $employeeId;
        $event->id_service_order = $order->id;
        $event->initial_hour = $initialHour;
        $event->final_hour = $finalHour;
        $event->initial_date = $date;
        $event->final_date = $date;
        $event->start_event = "00:00";
        $event->final_event = "00:00";
        $event->id_job_center = $quotation->id_job_center;
        $event->companie = auth()->user()->companie;
        $event->id_status = 1;
        $event->service_type = 1;
        $event->save();
    }
}