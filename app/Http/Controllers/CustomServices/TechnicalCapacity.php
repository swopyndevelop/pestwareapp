<?php


namespace App\Http\Controllers\CustomServices;


use App\custom_quote;
use App\customer;
use App\customer_branche;
use App\employee;
use App\event;
use App\Http\Controllers\Services\CommonCalendar;
use App\quotation;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Collection;

class TechnicalCapacity
{

    private $jobCenterId;
    private $quotationId;
    private $customerId;
    private $branchOffices;
    private $branchConcepts;
    private $servicesByTechnician;
    private $lastBranch;
    private $smartRoutes;
    private $maxServices = 6;
    private $daysOfWeek;
    private $technicians;
    private $initialHour;
    private $finalHour;
    private $date;
    private $folio = 1;
    private $quotation;

    /**
     * TechnicalCapacity constructor.
     * @param $quotationId
     */
    public function __construct($quotationId)
    {
        $this->quotationId = $quotationId;
        $this->smartRoutes = new SmartRoutes();
        $this->jobCenterId = quotation::find($quotationId)->id_job_center;
        $this->customerId = quotation::find($quotationId)->id_customer;
        $this->quotation = quotation::find($quotationId);
        $this->lastBranch = "Av Eugenio Garza Sada 260-A, Pocitos, 20997 Pocitos, Ags.";
        $this->branchOffices = custom_quote::where('id_quotation', $quotationId)->get();
    }

    public function searchActiveTechniciansByJobCenter()
    {
        $technicians = new Collection();
        $employees = employee::where('profile_job_center_id', $this->jobCenterId)
            ->where('status', 1)
            ->get();

        foreach ($employees as $employee) {
            $user = User::find($employee->employee_id);
            if ($user->can('Realizar Servicios (App)')) $technicians->push($employee);
        }

        return $technicians;
    }

    private function getTechniciansDataByEmployeesId($employees) {
        $technicians = new Collection();
        foreach ($employees as $id) $technicians->push(employee::find($id));
        return $technicians;
    }

    private function appliesDayOfWeek($date) {
        $date = Carbon::parse($date);
        foreach ($this->daysOfWeek as $i => $item) if ($i == $date->dayOfWeek && $item == 'true') {
            return $this->appliesDayOfWeek($date->addDay());
        }
        return $date;
    }

    // if count technicians >= quantity concept => 'Capacity maximum'
    // if count technicians < quantity concepts && (12 * count technicians) >= quantity concepts
    // if count technicians < quantity concepts && (12 * count technicians) < quantity concepts

    private function calculateTechnicalCapacity($period)
    {
        $technicalCapacity = [];
        $totalTechnicians = $this->searchActiveTechniciansByJobCenter()->count();
        $totalServices = custom_quote::where('id_quotation', $this->quotationId)
            ->where('term_month', '>=', ++$period)
            ->select(DB::raw('SUM(quantity * frecuency_month) as quantity'))
            ->first()->quantity;

        if ($totalTechnicians >= $totalServices) {
            $technicalCapacity = ['capacity' => 'maximum', 'totalTechnicians' => $totalTechnicians];
        }
        elseif ($totalTechnicians < $totalServices && ($this->maxServices * $totalTechnicians) >= $totalServices) {
            $technicalCapacity = ['capacity' => 'median', 'totalTechnicians' => $totalTechnicians];
        }
        elseif ($totalTechnicians < $totalServices && ($this->maxServices * $totalTechnicians) < $totalServices) {
            $technicalCapacity = ['capacity' => 'low', 'totalTechnicians' => $totalTechnicians];
        }

        return $technicalCapacity;
    }

    public function scheduleServicesOriginal($request)
    {
        Carbon::setLocale('es');
        setlocale(LC_ALL, 'es_ES');

        // Set max services by technician.
        $startHour = Carbon::parse($request['initialHour']);
        $endHour = Carbon::parse($request['finalHour']);
        $diff = $startHour->diffInMinutes($endHour);
        $this->maxServices = $diff / $request['duration'];
        $this->daysOfWeek = $request['week'];

        $periods = custom_quote::where('id_quotation', $this->quotationId)->max('term_month');
        $timesMonth = custom_quote::where('id_quotation', $this->quotationId)->max('frecuency_month');

        $arrayTermMonth = array_fill(0, $periods, '');
        $arrayTimesMonth = array_fill(0, $timesMonth, '');
        //$arrayTermMonth = array_reverse($arrayTermMonth);

        $data = new Collection();
        $hour = Carbon::parse($request['initialHour']);
        $date = Carbon::parse($request['initialDate']);
        $periodDate = Carbon::parse($request['initialDate']);
        //$technicians = $this->searchActiveTechniciansByJobCenter();
        $technicians = $this->getTechniciansDataByEmployeesId($request['technicians']);

        foreach ($arrayTermMonth as $key => $period)
        {
            $periodServices = new Collection();
            $technicalCapacity = $this->calculateTechnicalCapacity($key);

            $dateForMedian = $date->copy();
            $dateForLow = $date->copy();
            $t = 0;

            foreach ($arrayTimesMonth as $time) {
                ++$t;
                $branchOffices = custom_quote::where('id_quotation', $this->quotationId)
                    ->where('term_month', '>=', ++$key)
                    ->where('frecuency_month', '>=', $t)
                    ->get();

                if ($technicalCapacity['capacity'] == 'maximum') {
                    $technicians = $this->searchActiveTechniciansByJobCenter();
                    $k = 0;
                    foreach ($branchOffices as $office) {
                        $totalServices = $office->quantity;
                        $arrayServices = array_fill(0, $totalServices, '');
                        foreach ($arrayServices as $j => $service) {
                            $periodServices->push([
                                'folio' => 'OS',
                                'branch' => $office->concept,
                                'date' => Carbon::parse($request['initialDate'])->toDateString(),
                                'hour' => $hour->toTimeString() . ' - ' . $hour->copy()->addMinutes($request['duration'])->toTimeString(),
                                'technician' => $technicians->get($k++)->name
                            ]);
                        }
                    }
                }
                if ($technicalCapacity['capacity'] == 'median') {

                    foreach ($branchOffices as $office) {
                        $totalServices = $office->quantity;
                        $arrayServices = array_fill(0, $totalServices, '');
                        foreach ($arrayServices as $i => $service) {
                            if ($technicians->isEmpty()) {
                                $hour->addMinutes($request['duration']);
                                $technicians = $this->getTechniciansDataByEmployeesId($request['technicians']);
                            }
                            $empName = $technicians->pop();
                            if ($this->appliesDayOfWeek($dateForMedian->toDateString())) $dateForMedian->addDays(1);
                            $periodServices->push([
                                'folio' => 'OS',
                                'branch' => $office->concept,
                                'date' => $dateForMedian->toDateString(),
                                'hour' => $hour->toTimeString() . ' - ' . $hour->copy()->addMinutes($request['duration'])->toTimeString(),
                                'technician' => $empName->name
                            ]);
                        }
                    }
                    $frecuency = 15;
                    $dateForMedian->addDays($frecuency);
                }
                if ($technicalCapacity['capacity'] == 'low') {
                    if ($key == 1 && $t == 0) dd($technicalCapacity['capacity']);
                    $techniciansReset = 0;
                    foreach ($branchOffices as $office) {
                        $totalServices = $office->quantity;
                        $arrayServices = array_fill(0, $totalServices, '');
                        foreach ($arrayServices as $i => $service) {
                            if ($technicians->isEmpty()) {
                                $hour->addMinutes($request['duration']);
                                ++$techniciansReset;
                                $technicians = $this->getTechniciansDataByEmployeesId($request['technicians']);
                            }
                            if ($techniciansReset == $this->maxServices) {
                                $hour = Carbon::parse($request['initialHour']);
                                $date->addDay(1);
                                $techniciansReset = 0;
                            }
                            $empName = $technicians->pop();
                            $periodServices->push([
                                'folio' => 'OS',
                                'branch' => $office->concept,
                                'date' => $dateForLow->toDateString(),
                                'hour' => $hour->toTimeString() . ' - ' . $hour->copy()->addMinutes($request['duration'])->toTimeString(),
                                'technician' => $empName->name
                            ]);
                        }
                    }
                }

            }

            $data->push([
                'name' => $date->formatLocalized('%B') . ' ' . $date->year,
                'periods' => $periodServices,
            ]);
            $date = $periodDate->addMonth(1); // reset initial date by period
            $hour = Carbon::parse($request['initialHour']); // reset initial hour by period
            $technicians = $this->getTechniciansDataByEmployeesId($request['technicians']); // reset technicians by period
        }
        return $data;
    }

    public function scheduleServices($request)
    {
        Carbon::setLocale('es');
        setlocale(LC_ALL, 'es_ES');

        // Set max services by technician.
        $this->date = Carbon::parse($request['initialDate']);
        $startHour = Carbon::parse($request['initialHour']);
        $endHour = Carbon::parse($request['finalHour']);
        $duration = $request['duration'];
        $save = $request['save'];
        $paymentMethod = $request['paymentMethod'];
        $paymentWay = $request['paymentWay'];
        $this->initialHour = $startHour;
        $this->finalHour = $startHour->copy()->addMinutes($duration);
        $diff = $startHour->diffInMinutes($endHour);
        $this->maxServices = $diff / $duration;
        $this->daysOfWeek = $request['week'];
        $this->getTechnicians($request['technicians']);
        $this->setServicesByTechnician($this->technicians);

        $periods = custom_quote::where('id_quotation', $this->quotationId)->max('term_month');
        $timesMonth = custom_quote::where('id_quotation', $this->quotationId)->max('frecuency_month');

        $arrayTermMonth = array_fill(0, $periods, '');
        $arrayTimesMonth = array_fill(0, $timesMonth, '');

        $data = new Collection();
        $p = 1;
        $t = 1;

        foreach ($arrayTermMonth as $period)
        {
            $periodServices = new Collection();

            foreach ($arrayTimesMonth as $time) {
                $timesByMonth = new Collection();
                $branchOffices = custom_quote::where('id_quotation', $this->quotationId)
                    ->where('term_month', '>=', $p)
                    ->where('frecuency_month', '>=', $t)
                    ->get();

                $date = $this->date;

                foreach ($branchOffices as $office) {
                    $totalServices = $office->quantity;
                    $arrayServices = array_fill(0, $totalServices, '');

                    // Get branches by concept
                    $this->branchConcepts = customer_branche::where('customer_id', $this->customerId)
                        ->where('type', $office->concept)
                        ->get();

                    // increment date after times by month
                    $frecuency = intval(30 / $office->frecuency_month) * ($t - 1);
                    if ($t > 1) $date =  $this->date->copy()->addDays($frecuency);

                    foreach ($arrayServices as $j => $service) {
                        // schedule section TODO: Extract section to function
                        if ($this->technicians->count() == 0) {
                            $this->resetTechnicians($request['technicians']);
                            $this->initialHour = $this->initialHour->copy()->addMinutes($duration);
                            $this->finalHour = $this->initialHour->copy()->addMinutes($duration);
                        }

                        // Validate not max end hour schedule
                        if ($this->initialHour > $endHour || $this->finalHour > $endHour) {
                            $date->addDay();
                            //$this->resetTechnicians($request['technicians']);
                            $this->initialHour = $startHour;
                            $this->finalHour = $startHour->copy()->addMinutes($duration);
                        }

                        // test week days
                        $date = $this->appliesDayOfWeek($date);

                        $available = $this->getAvailability($date->toDateString(), $this->initialHour->toTimeString(),
                            $this->finalHour->toTimeString(), $this->technicians, $request['duration'], $endHour);

                        if ($available['available'] == 1) {
                            $this->technicians->pull($available['key']);
                            $timesByMonth->push([
                                'folio' => 'OS-' . $office->id_quotation . '-' . $this->folio,
                                'branch' => $office->concept . ' | ' . $available['branch'],
                                'address' => $available['address'],
                                'date' => $date->toDateString(),
                                'hour' => $this->initialHour->toTimeString() . ' - ' . $this->finalHour->toTimeString(),
                                'technician' => $available['technician'],
                                'sort' => $available['technician'] . ' ' . $this->folio,
                                'color' => $available['color'],
                                'branchShort' => $available['branch']
                            ]);
                            // Saved in database
                            if ($save == 'true') {
                                $completeFolio = 'OS-' . $office->id_quotation . '-' . $this->folio;
                                $branch = customer_branche::find($available['branchId']);
                                $customer = customer::find($this->customerId);
                                $order = CommonCustomServices::createServiceOrder($completeFolio, $this->quotationId, $paymentMethod, $paymentWay,
                                    $this->quotation, $branch, $office, '');
                                if ($order) CommonCustomServices::createEvent($customer, $branch, $available['technicianId'], $order, $date->toDateString(), $this->initialHour->toTimeString(), $this->finalHour->toTimeString(), $this->quotation);
                            }

                        } elseif ($available['available'] == 0) {
                            $this->technicians->pull($available['key']);
                            $timesByMonth->push([
                                'folio' => 'OS-' . $office->id_quotation . ' - ' . $this->folio,
                                'branch' => $office->concept . ' | ' . $available['branch'],
                                'address' => $available['address'],
                                'date' => $date->toDateString(),
                                'hour' => $available['hour'],
                                'technician' => $available['technician'],
                                'sort' => $available['technician'] . ' ' . $this->folio,
                                'color' => $available['color'],
                                'branchShort' => $available['branch']
                            ]);
                            // Saved in database
                            if ($save == 'true') {
                                $completeFolio = 'OS-' . $office->id_quotation . '-' . $this->folio;
                                $branch = customer_branche::find($available['branchId']);
                                $customer = customer::find($this->customerId);
                                $order = CommonCustomServices::createServiceOrder($completeFolio, $this->quotationId, $paymentMethod, $paymentWay,
                                    $this->quotation, $branch, $office, '');
                                if ($order) CommonCustomServices::createEvent($customer, $branch, $available['technicianId'], $order, $date->toDateString(), $available['hour'], $available['hour'], $this->quotation);
                            }

                        }
                        //if ($j == 1 && $p == 1) dd($this->technicians->count());
                       /* if ($this->technicians->count() >= $totalServices) {
                            $this->initialHour = $startHour;
                            $this->finalHour = $startHour->copy()->addMinutes($duration);
                        } else {
                            $this->initialHour = $this->initialHour->copy()->addMinutes($duration);
                            $this->finalHour = $this->initialHour->copy()->addMinutes($duration);
                        }*/
                        $this->folio++;
                        // end section
                    }// end services
                }
                $periodServices->push([
                    'times' => $timesByMonth->groupBy('technician')
                ]);
                ++$t;
                $this->resetTechnicians($request['technicians']);
                $this->initialHour = $startHour;
                $this->finalHour = $startHour->copy()->addMinutes($duration);
            }

            $data->push([
                'name' => $this->date->formatLocalized('%B') . ' ' . $this->date->year,
                'periods' => $periodServices,
            ]);
            // reset
            ++$p;
            $t = 1;
            $this->date->addMonth();
        }
        //dd($this->servicesByTechnician);
        return $data;
    }

    private function getAvailability($initialDate, $initialHour, $finalHour, $capacity, $duration, $endHour) {

        $data = false;
        $keyRoute = false;
        $technicians = CommonCalendar::getTechniciansAvailableIntelligent($initialDate, $initialHour, $finalHour, $this->jobCenterId);

        foreach ($capacity as $key => $item) {
            foreach ($technicians['available'] as $technician) {
                if ($technician->id == $item->id) {

                    // Calculate the best route
                    $servicesByTechnician = $this->servicesByTechnician->get($technician->id);
                    $lastBranch = $servicesByTechnician->last();
                    if ($lastBranch == null) $lastBranch = $this->lastBranch;
                    else $lastBranch = $lastBranch['address'];

                    $bestBranch = $this->smartRoutes->getTheNearestBranch($this->branchConcepts, $lastBranch);
                    $keyRoute = $this->branchConcepts->search(function($item) use ($bestBranch) {
                        return $item->id == $bestBranch['key'];
                    });
                    // End calculate the best route

                    $data = [
                        'technician' => $item->name,
                        'technicianId' => $item->id,
                        'available' => 1,
                        'key' => $key,
                        'id' => $item->id,
                        'branch' => $bestBranch['branch'],
                        'branchId' => $bestBranch['key'],
                        'address' => $bestBranch['address'],
                        'color' => $item->color
                    ];
                    $servicesByTechnician = $this->servicesByTechnician->get($technician->id);
                    $servicesByTechnician->push($data);

                }
            }
        }
        if (!$data) {
            foreach ($technicians['unavailable'] as $technician) {
                $events = event::where('id_employee', $technician->id)
                    ->where('initial_date', $initialDate)
                    ->where('id_status', 1)
                    ->get();
                foreach ($events as $event) {
                    $eventFinalHour = Carbon::parse($event->final_hour)->addMinutes($duration);
                    if ($event->final_hour < $endHour || $eventFinalHour < $endHour) {
                        // get key
                        $key = 0;
                        foreach ($capacity as $k => $value) if ($value->id == $technician->id) $key = $k;

                        // Calculate the best route
                        $servicesByTechnician = $this->servicesByTechnician->get($technician->id);
                        $lastBranch = $servicesByTechnician->last();
                        if ($lastBranch == null) $lastBranch = $this->lastBranch;
                        else $lastBranch = $lastBranch['address'];

                        $bestBranch = $this->smartRoutes->getTheNearestBranch($this->branchConcepts, $lastBranch);
                        $keyRoute = $this->branchConcepts->search(function($item) use ($bestBranch) {
                            return $item->id == $bestBranch['key'];
                        });
                        // End calculate the best route

                        $data = [
                            'technician' => $technician->name,
                            'technicianId' => $item->id,
                            'available' => 0,
                            'key' => $key,
                            'hour' => $event->final_hour . ' - ' . $eventFinalHour->toTimeString(),
                            'id' => $technician->id,
                            'branch' => $bestBranch['branch'],
                            'branchId' => $bestBranch['key'],
                            'address' => $bestBranch['address'],
                            'color' => $technician->color
                        ];
                        $servicesByTechnician = $this->servicesByTechnician->get($technician->id);
                        $servicesByTechnician->push($data);
                    }
                }
            }
        }
        $this->branchConcepts->pull($keyRoute);
        return $data;
    }

    private function resetTechnicians($request) {
        $this->getTechnicians($request);
    }

    private function getTechnicians($request) {
        if ($request[0] == 0) $this->technicians = $this->searchActiveTechniciansByJobCenter();
        else $this->technicians = $this->getTechniciansDataByEmployeesId($request);
    }

    private function setServicesByTechnician($technicians) {
        $this->servicesByTechnician = collect();
        foreach ($technicians as $technician) $this->servicesByTechnician->put($technician->id, new Collection());
    }

}












