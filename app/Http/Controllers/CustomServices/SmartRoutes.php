<?php


namespace App\Http\Controllers\CustomServices;

use Illuminate\Database\Eloquent\Collection;

class SmartRoutes
{
    private $urlBase = 'https://maps.googleapis.com/maps/api/';

    private function getCoordinates($address) {
        $geo = file_get_contents($this->urlBase.'geocode/json?address='
            .urlencode($address).'&sensor=false&key='.env('API_KEY_GOOGLE'));

        $geo = json_decode($geo, true);

        if ($geo['status'] == 'OK') {
            $latitude = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
            return [
                'latitude' => $latitude,
                'longitude' => $longitude,
                'place_id' => $geo['results'][0]['place_id']
            ];
        }

        return false;
    }

    private function getDistance($data) {

        $placeIdOrigin = $data['placeIdOrigin'];
        $placeIdDestination = $data['placeIdDestination'];

        $url = file_get_contents($this->urlBase.'distancematrix/json?origins=place_id:'.
            urlencode($placeIdOrigin)."&destinations=place_id:". urlencode($placeIdDestination).
            "&key=".env('API_KEY_GOOGLE'));

        $distance = json_decode($url, true);

        if ($distance['status'] == 'OK') {
            if ($distance['rows'][0]['elements'][0]['status'] == 'OK') {
                $distance = $distance['rows'][0]['elements'][0]['distance']['text'];
                $distance = explode(' ', $distance);
                return ['status' => 'OK', 'data' => $distance[0]];
            }
            return ['status' => $distance['status'], 'data' => null];
        }

        return ['status' => $distance['status'], 'data' => null];
    }

    private function getAddressFormat($branch) {
        return $branch->address . " " . $branch->address_number . ', ' . $branch->colony . ", "
            . $branch->municipality . ", " . $branch->state . ", México";
    }

    public function getTheNearestBranch($branches, $lastBranch)
    {
        $distanceMin = 0;
        $branchMin = 10;
        $branchMinKey = 0;
        $addressMin = '';
        $key = 0;
        $coordinatesMin = '';
        $coordinatesOffice = $this->getCoordinates($lastBranch);

        foreach ($branches as $branch) {
            $address = $this->getAddressFormat($branch);
            $coordinates = $this->getCoordinates($address);

            $data = [
                'placeIdOrigin' => $coordinatesOffice['place_id'],
                'placeIdDestination' => $coordinates['place_id']
            ];

            $distance = $this->getDistance($data);
            if ($distance['data'] == null) dd($coordinates);

            if ($key == 0) {
                $distanceMin = $distance;
                $branchMin = $branch->name;
                $branchMinKey = $branch->id;
                $addressMin = $this->getAddressFormat($branch);
                $coordinatesMin = $coordinates;
            }
            elseif ($distance < $distanceMin) {
                $distanceMin = $distance;
                $branchMin = $branch->name;
                $branchMinKey = $branch->id;
                $addressMin = $this->getAddressFormat($branch);
                $coordinatesMin = $coordinates;
            }
            $key++;
        }

        return [
            'distance' => $distanceMin,
            'branch' => $branchMin,
            'key' => $branchMinKey,
            'address' => $addressMin,
            'coordinates' => $coordinatesMin
        ];
    }

    public function getTheBestRoute($branches, $office)
    {
        $route = new Collection();
        $branchesNew = new Collection();
        $bestBranch = $this->getTheNearestBranch($branches, $office);

        $key = $branches->search(function($item) use ($bestBranch) {
            return $item->id == $bestBranch['key'];
        });
        $branches->pull($key);
        $route->push($bestBranch);

        foreach ($branches as $i => $branch) {
            $branchesNew->push($branch);
            $bestBranch = $this->getTheNearestBranch($branches, $route->last()['address']);
            $key = $branches->search(function($item) use ($bestBranch) {
                return $item->id == $bestBranch['key'];
            });
            $branches->pull($key);
            $route->push($bestBranch);
        }

        return $route;
    }
}