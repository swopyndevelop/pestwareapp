<?php


namespace App\Http\Controllers\CustomServices;


use App\custom_quote;
use App\customer_branche;
use Exception;

class ScheduleIntelligent
{
    public static function getConceptsByIdQuot($quotationId)
    {
        try {
            $concepts = custom_quote::find($quotationId);
            return response()->json([
                'code' => 200,
                'data' => $concepts
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'data' => ''
            ]);
        }

    }

    public static function getBranchesByConcept($concept, $customerId, $quotationId)
    {
        return customer_branche::where('customer_id', $customerId)
            ->where('id_quotation', $quotationId)
            ->where('type', $concept)
            ->get();
    }

    public static function getConceptById($conceptId) {
        return custom_quote::find($conceptId);
    }
}