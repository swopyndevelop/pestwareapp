<?php


namespace App\Http\Controllers\CustomServices;


use App\custom_quote;
use App\customer;
use App\customer_branche;
use App\employee;
use App\Http\Controllers\Customers\CustomerBranchesController;
use App\payment_method;
use App\payment_way;
use App\quotation;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Protection;

class ScheduleExcel
{

    private $message = "Servicios importados correctamente.";
    private $title = "Datos Importados";
    private $type = "success";

    public static function createFormatExcel($data) {

        try {

            $quotationId = $data['id_quotation'];
            $customerId = $data['customerId'];
            $employeeId = $data['technician'];
            $paymentMethod = $data['paymentMethod'];
            $paymentWay = $data['paymentWay'];
            $paymentMethod = payment_method::find($paymentMethod)->name;
            $paymentWay = payment_way::find($paymentWay)->name;
            $employee = employee::find($employeeId)->name;
            $date = Carbon::now()->toDateString();
            $initialHour = Carbon::now()->toTimeString();
            $finalHour = Carbon::parse($initialHour)->addHour()->toTimeString();

            $customQuotations = custom_quote::where('id_quotation', $quotationId)->get();
            $quotation = quotation::find($quotationId);
            $customer = customer::find($customerId);
            $folioOrder = "OS-" . $quotationId;
            $folioId = 0;

            $dataArray = array();
            $keySchedule = 1;
            $fileName = 'Programacion C-' . $quotationId;

            // validations
            $countBranches = customer_branche::where('customer_id', $customerId)
                ->where('id_quotation', $quotationId)
                ->get()->count();

            if ($countBranches != CustomerBranchesController::getCountBranchesCustom($quotationId)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'El número de sucursales no coincide con la cantidad de conceptos registrados.'
                ]);
            }

            foreach ($customQuotations as $concept) {

                $arrayTermMonth = array_fill(0, $concept->term_month, '');
                $arrayFrecuencyMonth = array_fill(0, $concept->frecuency_month, '');
                $arrayQuantity = array_fill(0, $concept->quantity, '');

                $branches = customer_branche::where('customer_id', $customerId)
                    ->where('id_quotation', $concept->id_quotation)
                    ->where('type', $concept->concept)
                    ->get();

                /** @noinspection PhpUnusedLocalVariableInspection */
                foreach ($arrayTermMonth as $month) {
                    /** @noinspection PhpUnusedLocalVariableInspection */
                    foreach ($arrayFrecuencyMonth as $frecuency) {
                        foreach ($arrayQuantity as $key => $item) {
                            $branche = $branches->get($key);
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            // Todo: Create row excel
                            ++$keySchedule;
                            array_push($dataArray, array(
                                'numero_servicio' => $completeFolio,
                                'sucursal' => $branche->name,
                                'tipo' => $concept->concept,
                                'fecha' => $date,
                                'hora_inicial' => $initialHour,
                                'hora_final' => $finalHour,
                                'tecnico' => $employee,
                                'metodo_pago' => $paymentMethod,
                                'forma_pago' => $paymentWay
                            ));
                        }
                    }
                }
            }

            Excel::create('Formato Programacion Servicios', function($excel) use ($dataArray, $fileName, $keySchedule) {

                $excel->sheet($fileName, function($sheet) use ($dataArray, $keySchedule) {
                    $sheet->getProtection()->setPassword('OHdT4$uKs%W@');
                    $sheet->getProtection()->setSheet(true);
                    $rangeProtection = 'B2:B' . $keySchedule;
                    $rangeProtectionTwo = 'D2:I' . $keySchedule;
                    $sheet->getStyle($rangeProtection)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                    $sheet->getStyle($rangeProtectionTwo)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                    $sheet->fromArray($dataArray);
                });
            })->export('xlsx');

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    public function importFormatExcel($path, $data) {

        if ($path != null) {
            $pathComplete = 'storage/app/' . $path;
            try {
                Excel::load($pathComplete, function($reader) use ($data) {

                    // Getting all results
                    $rows = $reader->get();
                    $this->message = count($rows);
                    if (count($rows) == 0) {
                        $title = "Formato vacío";
                        $message = "El formato debe incluir servicios.";
                        $type = "warning";
                    } else if ($this->validateFormat($rows)) { // Validate format document

                        $quotation = quotation::find($data['quotationId']);
                        $customer = customer::find($quotation->id_customer);

                        foreach ($rows as $row) {
                            if ($row->numero_servicio != null) {
                                $paymentMethod = payment_method::where('name', $row->metodo_pago)->where('companie', Auth::user()->companie)->first();
                                $paymentWay = payment_way::where('name', $row->forma_pago)->first();
                                $branche = customer_branche::where('name', $row->sucursal)->first();
                                $concept = custom_quote::where('id_quotation', $quotation->id)->where('concept', $row->tipo)->first();
                                $employee = employee::where('name', $row->tecnico)->where('id_company', Auth::user()->companie)->first();
                                $order = CommonCustomServices::createServiceOrder(
                                    $row->numero_servicio,
                                    $data['quotationId'],
                                    $paymentMethod,
                                    $paymentWay,
                                    $quotation,
                                    $branche,
                                    $concept,
                                    ''
                                );
                                if ($order) CommonCustomServices::createEvent(
                                    $customer,
                                    $branche,
                                    $employee->id,
                                    $order->id,
                                    $row->fecha,
                                    $row->hora_inicial,
                                    $row->hora_final,
                                    $quotation
                                );
                            }
                        }
                    }else{
                        return [
                            'type' => "warning",
                            'title' => "Formato incorrecto",
                            'message' => 'Revise el formato e intente de nuevo.'
                        ];
                    }

                });
            }catch (\Exception $exception) {
                return [
                    'type' => "error",
                    'title' => "Error al importa",
                    'message' => $exception->getMessage() //'No ha sido posible importar los datos, intente de nuevo.'
                ];
            }
        }else {
            return [
                'type' => "error",
                'title' => "Error de archivo",
                'message' => 'No ha sido posible cargar el archivo.'
            ];
        }

    }

    public static function uploadFileInStorage($file, $quotationId) {
        return Storage::putFileAs(
            'public/excel/orders', $file, $quotationId . '.xlsx'
        );
    }

    public function validateFormat($columns) {
        if (count($columns) > 0)
            /*if ($columns[0]->clave && $columns[0]->nombre && $columns[0]->tipo && $columns[0]->telefono &&
                $columns[0]->correo && $columns[0]->responsable && $columns[0]->calle && $columns[0]->calle &&
                $columns[0]->numero && $columns[0]->colonia && $columns[0]->municipio && $columns[0]->estado &&
                $columns[0]->codigo_postal)*/
                return true;
        return false;
    }
}