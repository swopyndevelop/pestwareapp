<?php

namespace App\Http\Controllers\CustomServices;

use App\custom_quote;
use App\customer;
use App\employee;
use App\Http\Controllers\Controller;
use App\quotation;
use App\service_order;
use App\ShareServiceOrder;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomServicesController extends Controller
{

    private $monthly = 1;
    private $bimonthly = 2;
    private $quarterly = 3;
    private $quarter = 4;
    private $biannual = 5;
    private $allDays = 6;

    // Schedules with intelligent.

    public function getSmartProposal(Request $request)
    {
        $data = $request->only(['quotationId', 'initialDate', 'initialHour', 'finalHour', 'duration', 'technicians', 'week', 'save', 'paymentMethod', 'paymentWay']);
        $quotationId = $data['quotationId'];
        $proposal = new TechnicalCapacity($quotationId);
        return response()->json($proposal->scheduleServices($data));
    }

    public function getAllConceptsByIdQuot($quotationId) {
        return ScheduleIntelligent::getConceptsByIdQuot($quotationId);
    }

    public function getBranchesByConcept(Request $request) {
        $concept = $request->get('concept');
        $customerId = $request->get('customerId');
        return ScheduleIntelligent::getBranchesByConcept($concept, $customerId);
    }

    public function scheduleServicesByConcept(Request $request)
    {
        $save = "false";
        $data = new Collection;
        try {
            DB::beginTransaction();

            $conceptId = $request->get('conceptId');
            $customerId = $request->get('customerId');
            $weekDay = $request->get('weekDay');
            $option = $request->get('optionDate');
            $optionHour = $request->get('optionHour');
            $optionDuration = $request->get('optionsDuration');
            $date = Carbon::parse($request->get('date'));
            $hour = $request->get('hour');
            $initialHour = Carbon::parse($hour);
            if ($optionDuration == 30) $finalHour = Carbon::parse($hour)->addMinutes($optionDuration);
            else $finalHour = Carbon::parse($hour)->addHour($optionDuration);
            $save = $request->get('save');

            $paymentMethod = $request->get('paymentMethod');
            $paymentWay = $request->get('paymentWay');
            $employeeId = $request->get('technician');
            $techniciansAuxiliaries = $request->get('techniciansAuxiliaries');
            $observations = $request->get('commentsTechnicians');

            if ($techniciansAuxiliaries != null) {
                if (in_array($employeeId, $techniciansAuxiliaries)) {
                    return Response::json([
                        'code' => 500,
                        'message' => 'El técnico principal no puede ser auxiliar.'
                    ]);
                }
            }

            $concept = ScheduleIntelligent::getConceptById($conceptId);
            $quotationId = $concept->id_quotation;
            $branches = ScheduleIntelligent::getBranchesByConcept($concept->concept, $customerId, $quotationId);
            $arrayTermMonth = array_fill(0, $concept->term_month, '');
            $arrayFrecuencyMonth = array_fill(0, $concept->frecuency_month, '');

            $quotation = quotation::find($quotationId);
            $customer = customer::find($customerId);
            $lastOrder = service_order::where('id_quotation', $quotationId)->count();

            $folio = explode('-', $quotation->id_quotation);

            $folioOrder = "OS-" . $folio[1];
            $folioId = 0;
            if ($lastOrder > 0) $folioId = $lastOrder;

            $isMonthly = true;
            $arrayBim = [];
            if ($concept->id_type_service == $this->bimonthly) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 2);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->quarterly) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 3);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->quarter) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 4);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->biannual) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 6);
                $isMonthly = false;
            }

            foreach ($arrayTermMonth as $key => $item) {
                if (!$isMonthly) $result = in_array($key, $arrayBim);
                foreach ($arrayFrecuencyMonth as $i => $value) {
                    // for month
                    $i += 1;
                    foreach ($branches as $branch) {
                        // insert db
                        if ($isMonthly) {
                            if ($save == 'true') {
                                $completeFolio = $folioOrder . "-" . ++$folioId;
                                $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                    $quotation, $branch, $concept, $observations);
                                if ($order) {
                                    if ($techniciansAuxiliaries != null){
                                        $serviceOrderMain = service_order::find($order->id);
                                        $serviceOrderMain->is_shared = 1;
                                        $serviceOrderMain->is_main= 1;
                                        $serviceOrderMain->save();
                                        foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                            $serviceOrder = service_order::find($order->id);
                                            $new = $serviceOrder->replicate();
                                            $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                            $new->id_status = 4;
                                            $new->is_main = 0;
                                            $new->is_shared = 1;
                                            $new->save();

                                            CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $date, $initialHour->toTimeString(), $finalHour->toTimeString(), $quotation);

                                            $shareServiceOrder = new ShareServiceOrder();
                                            $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                            $shareServiceOrder->id_service_order = $new->id;
                                            $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                            $shareServiceOrder->save();
                                        }
                                    }
                                    CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $date, $initialHour->toTimeString(), $finalHour->toTimeString(), $quotation);
                                    $this->updateStatusQuotation($quotationId);
                                }
                            } else {
                                $data->push([
                                    'folio' => $folioOrder . "-" . ++$folioId,
                                    'name' => $branch->name,
                                    'date' => $date->toDateString(),
                                    'hour' => $initialHour->toTimeString() . ' - ' . $finalHour->toTimeString(),
                                    'month' => $date->month
                                ]);
                            }
                        } else {
                            if ($result) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $date, $initialHour->toTimeString(), $finalHour->toTimeString(), $quotation);
                                        $this->updateStatusQuotation($quotationId);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $date->toDateString(),
                                        'hour' => $initialHour->toTimeString() . ' - ' . $finalHour->toTimeString(),
                                        'month' => $date->month
                                    ]);
                                }
                            }
                        }
                        // Hours
                        if ($optionHour == 2) {
                            $initialHour = $initialHour->addMinutes(30);
                            $finalHour = $finalHour->addMinutes(30);
                        } elseif ($optionHour == 3) {
                            $initialHour = $initialHour->addHour(1);
                            $finalHour = $finalHour->addHour(1);
                        } elseif ($optionHour == 4) {
                            $initialHour = $initialHour->addHour(2);
                            $finalHour = $finalHour->addHour(2);
                        } elseif ($optionHour == 5) {
                            $initialHour = $initialHour->addHour(3);
                            $finalHour = $finalHour->addHour(3);
                        } elseif ($optionHour == 6) {
                            $initialHour = $initialHour->addHour(4);
                            $finalHour = $finalHour->addHour(4);
                        }

                    }
                    // Dates
                    if ($option == 1) $date = $date->addMonth($i);
                    elseif ($option == 2) $date = SmartDates::getFirstOfMonth($date->addMonth($i), $weekDay);
                    elseif ($option == 3) $date = SmartDates::getSecondOfMonth($date->addMonth($i), $weekDay);
                    elseif ($option == 4) $date = SmartDates::getThirdOfMonth($date->addMonth($i), $weekDay);
                    elseif ($option == 5) $date = SmartDates::getLastOfMonth($date->addMonth($i), $weekDay);
                    // Hours reset
                    $initialHour = Carbon::parse($hour);
                    if ($optionDuration == 30) $finalHour = Carbon::parse($hour)->addMinutes($optionDuration);
                    else $finalHour = Carbon::parse($hour)->addHour($optionDuration);
                }
            }

            DB::commit();

            if ($save == 'true') return response()->json(['code' => 201, 'message' => 'Servicios Programados.']);
            return response()->json($data);
        } catch (Exception $exception) {
            if ($save == 'true') {
                DB::rollBack();
                return response()->json(['code' => 500, 'message' => $exception->getMessage()]);
            }
            return response()->json($data);
        }
    }

    public function scheduleTwoServicesByConcept(Request $request) {

        $save = "false";
        $data = new Collection;

        try {
            DB::beginTransaction();

            $conceptId = $request->get('conceptId');
            $customerId = $request->get('customerId');
            $firstDay = Carbon::parse($request->get('firstDay'));
            $secondDay = Carbon::parse($request->get('secondDay'));
            $hourFirstDay = $request->get('hourFirstDay');
            $hourSecondDay = $request->get('hourSecondDay');
            $weekFirstDay = $request->get('firstWeekDay');
            $weekSecondDay = $request->get('secondWeekDay');
            $option = $request->get('optionDate');
            $optionHour = $request->get('optionHour');
            $optionDuration = $request->get('optionsDuration');
            $firstInitialHour = Carbon::parse($hourFirstDay);
            $secondInitialHour = Carbon::parse($hourSecondDay);
            $employeeId = $request->get('technician');
            $techniciansAuxiliaries = $request->get('techniciansAuxiliaries');
            $observations = $request->get('commentsTechnicians');

            if ($techniciansAuxiliaries != null) {
                if (in_array($employeeId, $techniciansAuxiliaries)) {
                    return Response::json([
                        'code' => 500,
                        'message' => 'El técnico principal no puede ser auxiliar.'
                    ]);
                }
            }

            if ($optionDuration == 30) {
                $firstFinalHour = Carbon::parse($hourFirstDay)->addMinutes($optionDuration);
                $secondFinalHour = Carbon::parse($hourSecondDay)->addMinutes($optionDuration);
            }
            else {
                $firstFinalHour = Carbon::parse($hourFirstDay)->addHour($optionDuration);
                $secondFinalHour = Carbon::parse($hourSecondDay)->addHour($optionDuration);
            }

            $save = $request->get('save');

            $paymentMethod = $request->get('paymentMethod');
            $paymentWay = $request->get('paymentWay');

            $concept = ScheduleIntelligent::getConceptById($conceptId);
            $quotationId = $concept->id_quotation;
            $branches = ScheduleIntelligent::getBranchesByConcept($concept->concept, $customerId, $quotationId);
            $arrayTermMonth = array_fill(0, $concept->term_month, '');
            $arrayFrecuencyMonth = array_fill(0, $concept->frecuency_month, '');

            $quotation = quotation::find($quotationId);
            $customer = customer::find($customerId);
            $lastOrder = service_order::where('id_quotation', $quotationId)->count();

            $folio = explode('-', $quotation->id_quotation);
            $folioOrder = "OS-" . $folio[1];
            $folioId = 0;
            $key = 0;
            if ($lastOrder > 0) $folioId = $lastOrder;

            $isMonthly = true;
            $arrayBim = [];
            if ($concept->id_type_service == $this->bimonthly) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 2);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->quarterly) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 3);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->quarter) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 4);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->biannual) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 6);
                $isMonthly = false;
            }

            foreach ($arrayTermMonth as $k => $item) {
                if (!$isMonthly) $result = in_array($k, $arrayBim);
                //$key += 1;
                $key += 1;
                foreach ($arrayFrecuencyMonth as $i => $value) {

                    // for month
                    foreach ($branches as $branch) {
                        if ($i == 0) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);

                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                        $this->updateStatusQuotation($quotationId);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $firstDay->toDateString(),
                                        'hour' => $firstInitialHour->toTimeString() . ' - ' . $firstFinalHour->toTimeString(),
                                        'month' => $firstDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                            $this->updateStatusQuotation($quotationId);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $firstDay->toDateString(),
                                            'hour' => $firstInitialHour->toTimeString() . ' - ' . $firstFinalHour->toTimeString(),
                                            'month' => $firstDay->month
                                        ]);
                                    }
                                }
                            }

                            // Hours
                            if ($optionHour == 2) {
                                $firstInitialHour = $firstInitialHour->addMinutes(30);
                                $firstFinalHour = $firstFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $firstInitialHour = $firstInitialHour->addHour(1);
                                $firstFinalHour = $firstFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $firstInitialHour = $firstInitialHour->addHour(2);
                                $firstFinalHour = $firstFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $firstInitialHour = $firstInitialHour->addHour(3);
                                $firstFinalHour = $firstFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $firstInitialHour = $firstInitialHour->addHour(4);
                                $firstFinalHour = $firstFinalHour->addHour(4);
                            }
                        } elseif ($i == 1) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $secondDay->toDateString(),
                                        'hour' => $secondInitialHour->toTimeString() . ' - ' . $secondFinalHour->toTimeString(),
                                        'month' => $secondDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $secondDay->toDateString(),
                                            'hour' => $secondInitialHour->toTimeString() . ' - ' . $secondFinalHour->toTimeString(),
                                            'month' => $secondDay->month
                                        ]);
                                    }
                                }
                            }
                            // Hours
                            if ($optionHour == 2) {
                                $secondInitialHour = $secondInitialHour->addMinutes(30);
                                $secondFinalHour = $secondFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $secondInitialHour = $secondInitialHour->addHour(1);
                                $secondFinalHour = $secondFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $secondInitialHour = $secondInitialHour->addHour(2);
                                $secondFinalHour = $secondFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $secondInitialHour = $secondInitialHour->addHour(3);
                                $secondFinalHour = $secondFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $secondInitialHour = $secondInitialHour->addHour(4);
                                $secondFinalHour = $secondFinalHour->addHour(4);
                            }
                        }
                    }
                    // Hours reset
                    $firstInitialHour = Carbon::parse($hourFirstDay);
                    $secondInitialHour = Carbon::parse($hourSecondDay);
                    if ($optionDuration == 30) {
                        $firstFinalHour = Carbon::parse($hourFirstDay)->addMinutes($optionDuration);
                        $secondFinalHour = Carbon::parse($hourSecondDay)->addMinutes($optionDuration);
                    }
                    else {
                        $firstFinalHour = Carbon::parse($hourFirstDay)->addHour($optionDuration);
                        $secondFinalHour = Carbon::parse($hourSecondDay)->addHour($optionDuration);
                    }
                }
                // Dates
                if ($option == 6) {
                    $firstDay = $firstDay->addMonth();
                    $secondDay = $secondDay->addMonth();
                } elseif ($option == 7) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekFirstDay);
                } elseif ($option == 8) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekFirstDay);
                } elseif ($option == 9) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getLastOfMonth($secondDay->addMonth(), $weekFirstDay);
                } elseif ($option == 10) {
                    $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekFirstDay);
                } elseif ($option == 11) {
                    $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getLastOfMonth($secondDay->addMonth(), $weekFirstDay);
                } elseif ($option == 12) {
                    $firstDay = SmartDates::getThirdOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getLastOfMonth($secondDay->addMonth(), $weekFirstDay);
                } elseif ($option == 13) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekSecondDay);
                } elseif ($option == 14) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekSecondDay);
                } elseif ($option == 15) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getLastOfMonth($secondDay->addMonth(), $weekSecondDay);
                } elseif ($option == 16) {
                    $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekSecondDay);
                } elseif ($option == 17) {
                    $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getLastOfMonth($secondDay->addMonth(), $weekFirstDay);
                } elseif ($option == 18) {
                    $firstDay = SmartDates::getThirdOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getLastOfMonth($secondDay->addMonth(), $weekSecondDay);
                }
            }

            DB::commit();
            return response()->json($data);
        } catch (Exception $exception) {
            DB::rollBack();
        }
    }

    /**
     * NOT USE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function scheduleThirdServicesByConcept(Request $request) {
        $conceptId = $request->get('conceptId');
        $customerId = $request->get('customerId');
        $firstDay = Carbon::parse($request->get('firstDay'));
        $secondDay = Carbon::parse($request->get('secondDay'));
        $thirdDay = Carbon::parse($request->get('thirdDay'));
        $hourFirstDayThird = Carbon::parse($request->get('hourFirstDayThird'));
        $hourSecondDayThird = Carbon::parse($request->get('hourSecondDayThird'));
        $hourThirdDayThird = Carbon::parse($request->get('hourThirdDayThird'));
        $weekFirstDay = $request->get('firstWeekDay');
        $weekSecondDay = $request->get('secondWeekDay');
        $weekThirdDay = $request->get('thirdWeekDay');
        $option = $request->get('optionDate');

        $concept = ScheduleIntelligent::getConceptById($conceptId);
        $branches = ScheduleIntelligent::getBranchesByConcept($concept->concept, $customerId, $concept->id_quotation);
        $arrayTermMonth = array_fill(0, $concept->term_month, '');
        $arrayFrecuencyMonth = array_fill(0, $concept->frecuency_month, '');

        $data = new Collection;

        $isMonthly = true;
        $arrayBim = [];
        if ($concept->id_type_service == $this->bimonthly) {
            $arrayBim = $this->getMonthsBim($concept->term_month, 2);
            $isMonthly = false;
        }
        if ($concept->id_type_service == $this->quarterly) {
            $arrayBim = $this->getMonthsBim($concept->term_month, 3);
            $isMonthly = false;
        }
        if ($concept->id_type_service == $this->quarter) {
            $arrayBim = $this->getMonthsBim($concept->term_month, 4);
            $isMonthly = false;
        }
        if ($concept->id_type_service == $this->biannual) {
            $arrayBim = $this->getMonthsBim($concept->term_month, 6);
            $isMonthly = false;
        }

        foreach ($arrayTermMonth as $key => $item) {
            if (!$isMonthly) $result = in_array($key, $arrayBim);
            $key += 1;
            foreach ($arrayFrecuencyMonth as $i => $value) {
                // for month
                foreach ($branches as $branch) {
                    if ($i == 0) {
                        $data->push([
                            'name' => $branch->name,
                            'date' => $firstDay->toDateString(),
                            'month' => $firstDay->month
                        ]);
                    } else if ($i == 1) {
                        $data->push([
                            'name' => $branch->name,
                            'date' => $secondDay->toDateString(),
                            'month' => $secondDay->month
                        ]);
                    } else if ($i == 2) {
                        $data->push([
                            'name' => $branch->name,
                            'date' => $thirdDay->toDateString(),
                            'month' => $thirdDay->month
                        ]);
                    }
                }
            }
            if ($option == 19) {
                $firstDay = $firstDay->addMonth($key);
                $secondDay = $secondDay->addMonth($key);
                $thirdDay = $thirdDay->addMonth($key);
            } elseif ($option == 20) {
                $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth($key), $weekFirstDay);
                $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth($key), $weekFirstDay);
                $thirdDay = SmartDates::getThirdOfMonth($thirdDay->addMonth($key), $weekFirstDay);
            } elseif ($option == 21) {
                $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth($key), $weekFirstDay);
                $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth($key), $weekFirstDay);
                $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth($key), $weekFirstDay);
            } elseif ($option == 22) {
                $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth($key), $weekFirstDay);
                $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth($key), $weekFirstDay);
                $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth($key), $weekFirstDay);
            } elseif ($option == 23) {
                $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth($key), $weekFirstDay);
                $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth($key), $weekFirstDay);
                $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth($key), $weekFirstDay);
            } elseif ($option == 24) {
                $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth($key), $weekFirstDay);
                $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth($key), $weekSecondDay);
                $thirdDay = SmartDates::getThirdOfMonth($thirdDay->addMonth($key), $weekThirdDay);
            } elseif ($option == 25) {
                $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth($key), $weekFirstDay);
                $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth($key), $weekSecondDay);
                $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth($key), $weekThirdDay);
            } elseif ($option == 26) {
                $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth($key), $weekFirstDay);
                $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth($key), $weekSecondDay);
                $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth($key), $weekThirdDay);
            } elseif ($option == 27) {
                $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth($key), $weekFirstDay);
                $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth($key), $weekSecondDay);
                $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth($key), $weekThirdDay);
            }
        }
        return response()->json($data);
    }

    private function updateStatusQuotation($quotationId) {
        $quotation = quotation::find($quotationId);
        $quotation->id_status = 4;
        $quotation->save();
    }

    // test third
    public function scheduleServicesByConceptTest(Request $request) {

        $save = "false";
        $data = new Collection;

        try {
            DB::beginTransaction();

            $conceptId = $request->get('conceptId');
            $customerId = $request->get('customerId');

            $firstDay = Carbon::parse($request->get('firstDay'));
            $secondDay = Carbon::parse($request->get('secondDay'));
            $thirdDay = Carbon::parse($request->get('thirdDay'));

            $hourFirstDayThird = Carbon::parse($request->get('hourFirstDayThird'));
            $hourSecondDayThird = Carbon::parse($request->get('hourSecondDayThird'));
            $hourThirdDayThird = Carbon::parse($request->get('hourThirdDayThird'));
            $employeeId = $request->get('technician');
            $techniciansAuxiliaries = $request->get('techniciansAuxiliaries');
            $observations = $request->get('commentsTechnicians');

            if ($techniciansAuxiliaries != null) {
                if (in_array($employeeId, $techniciansAuxiliaries)) {
                    return Response::json([
                        'code' => 500,
                        'message' => 'El técnico principal no puede ser auxiliar.'
                    ]);
                }
            }

            $weekFirstDay = $request->get('firstWeekDay');
            $weekSecondDay = $request->get('secondWeekDay');
            $weekThirdDay = $request->get('thirdWeekDay');

            $option = $request->get('optionDate');
            $optionHour = $request->get('optionHour');
            $optionDuration = $request->get('optionsDuration');

            $firstInitialHour = Carbon::parse($hourFirstDayThird);
            $secondInitialHour = Carbon::parse($hourSecondDayThird);
            $thirdInitialHour = Carbon::parse($hourThirdDayThird);

            if ($optionDuration == 30) {
                $firstFinalHour = Carbon::parse($hourFirstDayThird)->addMinutes($optionDuration);
                $secondFinalHour = Carbon::parse($hourSecondDayThird)->addMinutes($optionDuration);
                $thirdFinalHour = Carbon::parse($hourThirdDayThird)->addMinutes($optionDuration);
            }
            else {
                $firstFinalHour = Carbon::parse($hourFirstDayThird)->addHour($optionDuration);
                $secondFinalHour = Carbon::parse($hourSecondDayThird)->addHour($optionDuration);
                $thirdFinalHour = Carbon::parse($hourThirdDayThird)->addHour($optionDuration);
            }

            $save = $request->get('save');

            $paymentMethod = $request->get('paymentMethod');
            $paymentWay = $request->get('paymentWay');

            $concept = ScheduleIntelligent::getConceptById($conceptId);
            $quotationId = $concept->id_quotation;
            $branches = ScheduleIntelligent::getBranchesByConcept($concept->concept, $customerId, $quotationId);
            $arrayTermMonth = array_fill(0, $concept->term_month, '');
            $arrayFrecuencyMonth = array_fill(0, $concept->frecuency_month, '');

            $quotation = quotation::find($quotationId);
            $customer = customer::find($customerId);
            $lastOrder = service_order::where('id_quotation', $quotationId)->count();

            $folio = explode('-', $quotation->id_quotation);
            $folioOrder = "OS-" . $folio[1];
            $folioId = 0;
            $key = 0;
            if ($lastOrder > 0) $folioId = $lastOrder;

            $isMonthly = true;
            $arrayBim = [];
            if ($concept->id_type_service == $this->bimonthly) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 2);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->quarterly) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 3);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->quarter) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 4);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->biannual) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 6);
                $isMonthly = false;
            }

            foreach ($arrayTermMonth as $k => $item) {
                if (!$isMonthly) $result = in_array($k, $arrayBim);
                //$key += 1;
                $key += 1;
                foreach ($arrayFrecuencyMonth as $i => $value) {

                    // for month
                    foreach ($branches as $branch) {
                        if ($i == 0) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                        $this->updateStatusQuotation($quotationId);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $firstDay->toDateString(),
                                        'hour' => $firstInitialHour->toTimeString() . ' - ' . $firstFinalHour->toTimeString(),
                                        'month' => $firstDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                            $this->updateStatusQuotation($quotationId);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $firstDay->toDateString(),
                                            'hour' => $firstInitialHour->toTimeString() . ' - ' . $firstFinalHour->toTimeString(),
                                            'month' => $firstDay->month
                                        ]);
                                    }
                                }
                            }
                            // Hours
                            if ($optionHour == 2) {
                                $firstInitialHour = $firstInitialHour->addMinutes(30);
                                $firstFinalHour = $firstFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $firstInitialHour = $firstInitialHour->addHour(1);
                                $firstFinalHour = $firstFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $firstInitialHour = $firstInitialHour->addHour(2);
                                $firstFinalHour = $firstFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $firstInitialHour = $firstInitialHour->addHour(3);
                                $firstFinalHour = $firstFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $firstInitialHour = $firstInitialHour->addHour(4);
                                $firstFinalHour = $firstFinalHour->addHour(4);
                            }
                        } elseif ($i == 1) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                    }

                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $secondDay->toDateString(),
                                        'hour' => $secondInitialHour->toTimeString() . ' - ' . $secondFinalHour->toTimeString(),
                                        'month' => $secondDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $secondDay->toDateString(),
                                            'hour' => $secondInitialHour->toTimeString() . ' - ' . $secondFinalHour->toTimeString(),
                                            'month' => $secondDay->month
                                        ]);
                                    }
                                }
                            }
                            // Hours
                            if ($optionHour == 2) {
                                $secondInitialHour = $secondInitialHour->addMinutes(30);
                                $secondFinalHour = $secondFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $secondInitialHour = $secondInitialHour->addHour(1);
                                $secondFinalHour = $secondFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $secondInitialHour = $secondInitialHour->addHour(2);
                                $secondFinalHour = $secondFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $secondInitialHour = $secondInitialHour->addHour(3);
                                $secondFinalHour = $secondFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $secondInitialHour = $secondInitialHour->addHour(4);
                                $secondFinalHour = $secondFinalHour->addHour(4);
                            }
                        } elseif ($i == 2) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $thirdDay->toDateString(), $thirdInitialHour->toTimeString(), $thirdFinalHour->toTimeString(), $quotation);
                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $thirdDay->toDateString(), $thirdInitialHour->toTimeString(), $thirdFinalHour->toTimeString(), $quotation);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $thirdDay->toDateString(),
                                        'hour' => $thirdInitialHour->toTimeString() . ' - ' . $thirdFinalHour->toTimeString(),
                                        'month' => $thirdDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $thirdDay->toDateString(), $thirdInitialHour->toTimeString(), $thirdFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $thirdDay->toDateString(), $thirdInitialHour->toTimeString(), $thirdFinalHour->toTimeString(), $quotation);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $thirdDay->toDateString(),
                                            'hour' => $thirdInitialHour->toTimeString() . ' - ' . $thirdFinalHour->toTimeString(),
                                            'month' => $thirdDay->month
                                        ]);
                                    }
                                }
                            }
                            // Hours
                            if ($optionHour == 2) {
                                $thirdInitialHour = $thirdInitialHour->addMinutes(30);
                                $thirdFinalHour = $thirdFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $thirdInitialHour = $thirdInitialHour->addHour(1);
                                $thirdFinalHour = $thirdFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $thirdInitialHour = $thirdInitialHour->addHour(2);
                                $thirdFinalHour = $thirdFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $thirdInitialHour = $thirdInitialHour->addHour(3);
                                $thirdFinalHour = $thirdFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $thirdInitialHour = $thirdInitialHour->addHour(4);
                                $thirdFinalHour = $thirdFinalHour->addHour(4);
                            }
                        }
                    }
                    // Hours reset
                    $firstInitialHour = Carbon::parse($hourFirstDayThird);
                    $secondInitialHour = Carbon::parse($hourSecondDayThird);
                    $thirdInitialHour = Carbon::parse($hourThirdDayThird);
                    if ($optionDuration == 30) {
                        $firstFinalHour = Carbon::parse($hourFirstDayThird)->addMinutes($optionDuration);
                        $secondFinalHour = Carbon::parse($hourSecondDayThird)->addMinutes($optionDuration);
                        $thirdFinalHour = Carbon::parse($hourThirdDayThird)->addMinutes($optionDuration);
                    }
                    else {
                        $firstFinalHour = Carbon::parse($hourFirstDayThird)->addHour($optionDuration);
                        $secondFinalHour = Carbon::parse($hourSecondDayThird)->addHour($optionDuration);
                        $thirdFinalHour = Carbon::parse($hourSecondDayThird)->addHour($optionDuration);
                    }
                }
                // Dates
                if ($option == 19) {
                    $firstDay = $firstDay->addMonth();
                    $secondDay = $secondDay->addMonth();
                    $thirdDay = $thirdDay->addMonth();
                } elseif ($option == 20) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekFirstDay);
                    $thirdDay = SmartDates::getThirdOfMonth($thirdDay->addMonth(), $weekFirstDay);
                } elseif ($option == 21) {
                    $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekFirstDay);
                    $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth(), $weekFirstDay);
                } elseif ($option == 22) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekFirstDay);
                    $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth(), $weekFirstDay);
                } elseif ($option == 23) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekFirstDay);
                    $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth(), $weekFirstDay);
                } elseif ($option == 24) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getThirdOfMonth($thirdDay->addMonth(), $weekThirdDay);
                } elseif ($option == 25) {
                    $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth(), $weekThirdDay);
                } elseif ($option == 26) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth(), $weekThirdDay);
                } elseif ($option == 27) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth(), $weekThirdDay);
                }
            }

            DB::commit();
            return response()->json($data);
        } catch (Exception $exception) {
            DB::rollBack();
        }
    }

    // test fourth
    public function scheduleFourthServicesByConcept(Request $request) {

        $save = "false";
        $data = new Collection;

        try {
            DB::beginTransaction();

            $conceptId = $request->get('conceptId');
            $customerId = $request->get('customerId');

            $firstDay = Carbon::parse($request->get('firstDay'));
            $secondDay = Carbon::parse($request->get('secondDay'));
            $thirdDay = Carbon::parse($request->get('thirdDay'));
            $fourthDay = Carbon::parse($request->get('fourthDay'));

            $hourFirstDayFourth = Carbon::parse($request->get('hourFirstDayFourth'));
            $hourSecondDayFourth = Carbon::parse($request->get('hourSecondDayFourth'));
            $hourThirdDayFourth = Carbon::parse($request->get('hourThirdDayFourth'));
            $hourFourthDayFourth = Carbon::parse($request->get('hourFourthDayFourth'));
            $employeeId = $request->get('technician');
            $techniciansAuxiliaries = $request->get('techniciansAuxiliaries');
            $observations = $request->get('commentsTechnicians');

            if ($techniciansAuxiliaries != null) {
                if (in_array($employeeId, $techniciansAuxiliaries)) {
                    return Response::json([
                        'code' => 500,
                        'message' => 'El técnico principal no puede ser auxiliar.'
                    ]);
                }
            }

            $weekFirstDay = $request->get('firstWeekDay');
            $weekSecondDay = $request->get('secondWeekDay');
            $weekThirdDay = $request->get('thirdWeekDay');
            $weekFourthDay = $request->get('fourthWeekDay');

            $option = $request->get('optionDate');
            $optionHour = $request->get('optionHour');
            $optionDuration = $request->get('optionsDuration');

            $firstInitialHour = Carbon::parse($hourFirstDayFourth);
            $secondInitialHour = Carbon::parse($hourSecondDayFourth);
            $thirdInitialHour = Carbon::parse($hourThirdDayFourth);
            $fourthInitialHour = Carbon::parse($hourFourthDayFourth);

            if ($optionDuration == 30) {
                $firstFinalHour = Carbon::parse($hourFirstDayFourth)->addMinutes($optionDuration);
                $secondFinalHour = Carbon::parse($hourSecondDayFourth)->addMinutes($optionDuration);
                $thirdFinalHour = Carbon::parse($hourThirdDayFourth)->addMinutes($optionDuration);
                $fourthFinalHour = Carbon::parse($hourFourthDayFourth)->addMinutes($optionDuration);
            }
            else {
                $firstFinalHour = Carbon::parse($hourFirstDayFourth)->addHour($optionDuration);
                $secondFinalHour = Carbon::parse($hourSecondDayFourth)->addHour($optionDuration);
                $thirdFinalHour = Carbon::parse($hourThirdDayFourth)->addHour($optionDuration);
                $fourthFinalHour = Carbon::parse($hourFourthDayFourth)->addHour($optionDuration);
            }

            $save = $request->get('save');

            $paymentMethod = $request->get('paymentMethod');
            $paymentWay = $request->get('paymentWay');

            $concept = ScheduleIntelligent::getConceptById($conceptId);
            $quotationId = $concept->id_quotation;
            $branches = ScheduleIntelligent::getBranchesByConcept($concept->concept, $customerId, $quotationId);
            $arrayTermMonth = array_fill(0, $concept->term_month, '');
            $arrayFrecuencyMonth = array_fill(0, $concept->frecuency_month, '');

            $quotation = quotation::find($quotationId);
            $customer = customer::find($customerId);
            $lastOrder = service_order::where('id_quotation', $quotationId)->count();

            $folio = explode('-', $quotation->id_quotation);
            $folioOrder = "OS-" . $folio[1];
            $folioId = 0;
            $key = 0;
            if ($lastOrder > 0) $folioId = $lastOrder;

            $isMonthly = true;
            $arrayBim = [];
            if ($concept->id_type_service == $this->bimonthly) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 2);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->quarterly) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 3);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->quarter) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 4);
                $isMonthly = false;
            }
            if ($concept->id_type_service == $this->biannual) {
                $arrayBim = $this->getMonthsBim($concept->term_month, 6);
                $isMonthly = false;
            }

            foreach ($arrayTermMonth as $k => $item) {
                if (!$isMonthly) $result = in_array($k, $arrayBim);
                //$key += 1;
                $key += 1;
                foreach ($arrayFrecuencyMonth as $i => $value) {

                    // for month
                    foreach ($branches as $branch) {
                        if ($i == 0) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                        $this->updateStatusQuotation($quotationId);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $firstDay->toDateString(),
                                        'hour' => $firstInitialHour->toTimeString() . ' - ' . $firstFinalHour->toTimeString(),
                                        'month' => $firstDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $firstDay->toDateString(), $firstInitialHour->toTimeString(), $firstFinalHour->toTimeString(), $quotation);
                                            $this->updateStatusQuotation($quotationId);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $firstDay->toDateString(),
                                            'hour' => $firstInitialHour->toTimeString() . ' - ' . $firstFinalHour->toTimeString(),
                                            'month' => $firstDay->month
                                        ]);
                                    }
                                }
                            }
                            // Hours
                            if ($optionHour == 2) {
                                $firstInitialHour = $firstInitialHour->addMinutes(30);
                                $firstFinalHour = $firstFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $firstInitialHour = $firstInitialHour->addHour(1);
                                $firstFinalHour = $firstFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $firstInitialHour = $firstInitialHour->addHour(2);
                                $firstFinalHour = $firstFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $firstInitialHour = $firstInitialHour->addHour(3);
                                $firstFinalHour = $firstFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $firstInitialHour = $firstInitialHour->addHour(4);
                                $firstFinalHour = $firstFinalHour->addHour(4);
                            }
                        } elseif ($i == 1) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $secondDay->toDateString(),
                                        'hour' => $secondInitialHour->toTimeString() . ' - ' . $secondFinalHour->toTimeString(),
                                        'month' => $secondDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $secondDay->toDateString(), $secondInitialHour->toTimeString(), $secondFinalHour->toTimeString(), $quotation);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $secondDay->toDateString(),
                                            'hour' => $secondInitialHour->toTimeString() . ' - ' . $secondFinalHour->toTimeString(),
                                            'month' => $secondDay->month
                                        ]);
                                    }
                                }
                            }

                            // Hours
                            if ($optionHour == 2) {
                                $secondInitialHour = $secondInitialHour->addMinutes(30);
                                $secondFinalHour = $secondFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $secondInitialHour = $secondInitialHour->addHour(1);
                                $secondFinalHour = $secondFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $secondInitialHour = $secondInitialHour->addHour(2);
                                $secondFinalHour = $secondFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $secondInitialHour = $secondInitialHour->addHour(3);
                                $secondFinalHour = $secondFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $secondInitialHour = $secondInitialHour->addHour(4);
                                $secondFinalHour = $secondFinalHour->addHour(4);
                            }
                        } elseif ($i == 2) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $thirdDay->toDateString(), $thirdInitialHour->toTimeString(), $thirdFinalHour->toTimeString(), $quotation);
                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $thirdDay->toDateString(), $thirdInitialHour->toTimeString(), $thirdFinalHour->toTimeString(), $quotation);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $thirdDay->toDateString(),
                                        'hour' => $thirdInitialHour->toTimeString() . ' - ' . $thirdFinalHour->toTimeString(),
                                        'month' => $thirdDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $thirdDay->toDateString(), $thirdInitialHour->toTimeString(), $thirdFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $thirdDay->toDateString(), $thirdInitialHour->toTimeString(), $thirdFinalHour->toTimeString(), $quotation);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $thirdDay->toDateString(),
                                            'hour' => $thirdInitialHour->toTimeString() . ' - ' . $thirdFinalHour->toTimeString(),
                                            'month' => $thirdDay->month
                                        ]);
                                    }
                                }
                            }
                            // Hours
                            if ($optionHour == 2) {
                                $thirdInitialHour = $thirdInitialHour->addMinutes(30);
                                $thirdFinalHour = $thirdFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $thirdInitialHour = $thirdInitialHour->addHour(1);
                                $thirdFinalHour = $thirdFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $thirdInitialHour = $thirdInitialHour->addHour(2);
                                $thirdFinalHour = $thirdFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $thirdInitialHour = $thirdInitialHour->addHour(3);
                                $thirdFinalHour = $thirdFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $thirdInitialHour = $thirdInitialHour->addHour(4);
                                $thirdFinalHour = $thirdFinalHour->addHour(4);
                            }
                        } elseif ($i == 3) {
                            // insert db
                            if ($isMonthly) {
                                if ($save == 'true') {
                                    $completeFolio = $folioOrder . "-" . ++$folioId;
                                    $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                        $quotation, $branch, $concept, $observations);
                                    if ($order) {
                                        if ($techniciansAuxiliaries != null){
                                            $serviceOrderMain = service_order::find($order->id);
                                            $serviceOrderMain->is_shared = 1;
                                            $serviceOrderMain->is_main= 1;
                                            $serviceOrderMain->save();
                                            foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                $serviceOrder = service_order::find($order->id);
                                                $new = $serviceOrder->replicate();
                                                $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                $new->id_status = 4;
                                                $new->is_main = 0;
                                                $new->is_shared = 1;
                                                $new->save();

                                                CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $fourthDay->toDateString(), $fourthInitialHour->toTimeString(), $fourthFinalHour->toTimeString(), $quotation);
                                                $shareServiceOrder = new ShareServiceOrder();
                                                $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                $shareServiceOrder->id_service_order = $new->id;
                                                $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                $shareServiceOrder->save();
                                            }
                                        }
                                        CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $fourthDay->toDateString(), $fourthInitialHour->toTimeString(), $fourthFinalHour->toTimeString(), $quotation);
                                    }
                                } else {
                                    $data->push([
                                        'folio' => $folioOrder . "-" . ++$folioId,
                                        'name' => $branch->name,
                                        'date' => $fourthDay->toDateString(),
                                        'hour' => $fourthInitialHour->toTimeString() . ' - ' . $fourthFinalHour->toTimeString(),
                                        'month' => $fourthDay->month
                                    ]);
                                }
                            } else {
                                if ($result) {
                                    if ($save == 'true') {
                                        $completeFolio = $folioOrder . "-" . ++$folioId;
                                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                            $quotation, $branch, $concept, $observations);
                                        if ($order) {
                                            if ($techniciansAuxiliaries != null){
                                                $serviceOrderMain = service_order::find($order->id);
                                                $serviceOrderMain->is_shared = 1;
                                                $serviceOrderMain->is_main= 1;
                                                $serviceOrderMain->save();
                                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                                    $serviceOrder = service_order::find($order->id);
                                                    $new = $serviceOrder->replicate();
                                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                                    $new->id_status = 4;
                                                    $new->is_main = 0;
                                                    $new->is_shared = 1;
                                                    $new->save();

                                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $fourthDay->toDateString(), $fourthInitialHour->toTimeString(), $fourthFinalHour->toTimeString(), $quotation);
                                                    $shareServiceOrder = new ShareServiceOrder();
                                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                                    $shareServiceOrder->id_service_order = $new->id;
                                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                                    $shareServiceOrder->save();
                                                }
                                            }
                                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $fourthDay->toDateString(), $fourthInitialHour->toTimeString(), $fourthFinalHour->toTimeString(), $quotation);
                                        }
                                    } else {
                                        $data->push([
                                            'folio' => $folioOrder . "-" . ++$folioId,
                                            'name' => $branch->name,
                                            'date' => $fourthDay->toDateString(),
                                            'hour' => $fourthInitialHour->toTimeString() . ' - ' . $fourthFinalHour->toTimeString(),
                                            'month' => $fourthDay->month
                                        ]);
                                    }
                                }
                            }
                            // Hours
                            if ($optionHour == 2) {
                                $fourthInitialHour = $fourthInitialHour->addMinutes(30);
                                $fourthFinalHour = $fourthFinalHour->addMinutes(30);
                            } elseif ($optionHour == 3) {
                                $fourthInitialHour = $fourthInitialHour->addHour(1);
                                $fourthFinalHour = $fourthFinalHour->addHour(1);
                            } elseif ($optionHour == 4) {
                                $fourthInitialHour = $fourthInitialHour->addHour(2);
                                $fourthFinalHour = $fourthFinalHour->addHour(2);
                            } elseif ($optionHour == 5) {
                                $fourthInitialHour = $fourthInitialHour->addHour(3);
                                $fourthFinalHour = $fourthFinalHour->addHour(3);
                            } elseif ($optionHour == 6) {
                                $fourthInitialHour = $fourthInitialHour->addHour(4);
                                $fourthFinalHour = $fourthFinalHour->addHour(4);
                            }
                        }
                    }
                    // Hours reset
                    $firstInitialHour = Carbon::parse($hourFirstDayFourth);
                    $secondInitialHour = Carbon::parse($hourSecondDayFourth);
                    $thirdInitialHour = Carbon::parse($hourThirdDayFourth);
                    $fourthInitialHour = Carbon::parse($hourFourthDayFourth);
                    if ($optionDuration == 30) {
                        $firstFinalHour = Carbon::parse($hourFirstDayFourth)->addMinutes($optionDuration);
                        $secondFinalHour = Carbon::parse($hourSecondDayFourth)->addMinutes($optionDuration);
                        $thirdFinalHour = Carbon::parse($hourThirdDayFourth)->addMinutes($optionDuration);
                        $fourthFinalHour = Carbon::parse($hourFourthDayFourth)->addMinutes($optionDuration);
                    }
                    else {
                        $firstFinalHour = Carbon::parse($hourFirstDayFourth)->addHour($optionDuration);
                        $secondFinalHour = Carbon::parse($hourSecondDayFourth)->addHour($optionDuration);
                        $thirdFinalHour = Carbon::parse($hourThirdDayFourth)->addHour($optionDuration);
                        $fourthFinalHour = Carbon::parse($hourFourthDayFourth)->addHour($optionDuration);
                    }
                }
                // Dates
                if ($option == 28) {
                    $firstDay = $firstDay->addMonth();
                    $secondDay = $secondDay->addMonth();
                    $thirdDay = $thirdDay->addMonth();
                    $fourthDay = $fourthDay->addMonth();
                } elseif ($option == 29) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getThirdOfMonth($thirdDay->addMonth(), $weekThirdDay);
                    $fourthDay = SmartDates::getLastOfMonth($fourthDay->addMonth(), $weekFourthDay);
                } elseif ($option == 30) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getFirstOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getFirstOfMonth($thirdDay->addMonth(), $weekThirdDay);
                    $fourthDay = SmartDates::getFirstOfMonth($fourthDay->addMonth(), $weekFourthDay);
                } elseif ($option == 31) {
                    $firstDay = SmartDates::getSecondOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getSecondOfMonth($thirdDay->addMonth(), $weekThirdDay);
                    $fourthDay = SmartDates::getSecondOfMonth($fourthDay->addMonth(), $weekFourthDay);
                } elseif ($option == 32) {
                    $firstDay = SmartDates::getThirdOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getThirdOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getThirdOfMonth($thirdDay->addMonth(), $weekThirdDay);
                    $fourthDay = SmartDates::getThirdOfMonth($fourthDay->addMonth(), $weekFourthDay);
                } elseif ($option == 33) {
                    $firstDay = SmartDates::getLastOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getLastOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getLastOfMonth($thirdDay->addMonth(), $weekThirdDay);
                    $fourthDay = SmartDates::getLastOfMonth($fourthDay->addMonth(), $weekFourthDay);
                } elseif ($option == 34) {
                    $firstDay = SmartDates::getFirstOfMonth($firstDay->addMonth(), $weekFirstDay);
                    $secondDay = SmartDates::getSecondOfMonth($secondDay->addMonth(), $weekSecondDay);
                    $thirdDay = SmartDates::getThirdOfMonth($thirdDay->addMonth(), $weekThirdDay);
                    $fourthDay = SmartDates::getLastOfMonth($fourthDay->addMonth(), $weekFourthDay);
                }
            }

            DB::commit();
            return response()->json($data);
        } catch (Exception $exception) {
            DB::rollBack();
        }
    }

    public function scheduleDailyServices(Request $request)
    {
        $scheduleDays = new Collection();
        $save = $request->get('save');
        try {
            DB::beginTransaction();
            $conceptId = $request->get('conceptId');
            $customerId = $request->get('customerId');
            $date = Carbon::parse($request->get('date'));
            $checkboxMonday = $request->get('checkboxMonday');
            $checkboxTuesday = $request->get('checkboxTuesday');
            $checkboxWednesday = $request->get('checkboxWednesday');
            $checkboxThursday = $request->get('checkboxThursday');
            $checkboxFriday = $request->get('checkboxFriday');
            $checkboxSaturday = $request->get('checkboxSaturday');
            $checkboxSunday = $request->get('checkboxSunday');
            $initialHourMonday = $request->get('initialHourMonday');
            $finalHourMonday = $request->get('finalHourMonday');

            $initialHourTuesday = $request->get('initialHourTuesday');
            $finalHourTuesday = $request->get('finalHourTuesday');

            $initialHourWednesday = $request->get('initialHourWednesday');
            $finalHourWednesday = $request->get('finalHourWednesday');

            $initialHourThursday = $request->get('initialHourThursday');
            $finalHourThursday = $request->get('finalHourThursday');

            $initialHourFriday = $request->get('initialHourFriday');
            $finalHourFriday = $request->get('finalHourFriday');

            $initialHourSaturday = $request->get('initialHourSaturday');
            $finalHourSaturday = $request->get('finalHourSaturday');

            $initialHourSunday = $request->get('initialHourSunday');
            $finalHourSunday = $request->get('finalHourSunday');

            $paymentMethod = $request->get('paymentMethod');
            $paymentWay = $request->get('paymentWay');
            $employeeId = $request->get('technician');
            $techniciansAuxiliaries = $request->get('techniciansAuxiliaries');
            $observations = $request->get('commentsTechnicians');

            if ($techniciansAuxiliaries != null) {
                if (in_array($employeeId, $techniciansAuxiliaries)) {
                    return Response::json([
                        'code' => 500,
                        'message' => 'El técnico principal no puede ser auxiliar.'
                    ]);
                }
            }

            $concept = ScheduleIntelligent::getConceptById($conceptId);
            $quotationId = $concept->id_quotation;
            $branches = ScheduleIntelligent::getBranchesByConcept($concept->concept, $customerId, $quotationId);
            $arrayTermMonth = array_fill(0, $concept->term_month, '');
            $arrayFrecuencyMonth = array_fill(0, $concept->frecuency_month, '');

            $quotation = quotation::find($quotationId);
            $customer = customer::find($customerId);
            $lastOrder = service_order::where('id_quotation', $quotationId)->count();

            $folio = explode('-', $quotation->id_quotation);

            $folioOrder = "OS-" . $folio[1];
            $folioId = 0;
            if ($lastOrder > 0) $folioId = $lastOrder;
            $endDate = $date->copy()->addMonths($concept->term_month);
            $period = CarbonPeriod::between($date->toDateString(), $endDate->toDateString());

            foreach ($period as $day) {
                foreach ($branches as $branch) {
                    if ($day->isMonday() && $checkboxMonday == 'true') {
                        if ($save == 'true') {
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                $quotation, $branch, $concept, $observations);
                            if ($order) {
                                if ($techniciansAuxiliaries != null){
                                    $serviceOrderMain = service_order::find($order->id);
                                    $serviceOrderMain->is_shared = 1;
                                    $serviceOrderMain->is_main= 1;
                                    $serviceOrderMain->save();
                                    foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                        $serviceOrder = service_order::find($order->id);
                                        $new = $serviceOrder->replicate();
                                        $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                        $new->id_status = 4;
                                        $new->is_main = 0;
                                        $new->is_shared = 1;
                                        $new->save();

                                        CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $day->toDateString(), $initialHourMonday, $finalHourMonday, $quotation);
                                        $shareServiceOrder = new ShareServiceOrder();
                                        $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                        $shareServiceOrder->id_service_order = $new->id;
                                        $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                        $shareServiceOrder->save();
                                    }
                                }
                                CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $day->toDateString(), $initialHourMonday, $finalHourMonday, $quotation);
                                $this->updateStatusQuotation($quotationId);
                            }
                        } else {
                            $scheduleDays->push([
                                'folio' => $folioOrder . "-" . ++$folioId,
                                'name' => $branch->name,
                                'date' => $day->toDateString(),
                                'hour' => $initialHourMonday . ' - ' . $finalHourMonday,
                                'month' => $day->month
                            ]);
                        }
                    }
                    if ($day->isTuesday() && $checkboxTuesday == 'true') {
                        if ($save == 'true') {
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                $quotation, $branch, $concept, $observations);
                            if ($order) {
                                if ($techniciansAuxiliaries != null){
                                    $serviceOrderMain = service_order::find($order->id);
                                    $serviceOrderMain->is_shared = 1;
                                    $serviceOrderMain->is_main= 1;
                                    $serviceOrderMain->save();
                                    foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                        $serviceOrder = service_order::find($order->id);
                                        $new = $serviceOrder->replicate();
                                        $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                        $new->id_status = 4;
                                        $new->is_main = 0;
                                        $new->is_shared = 1;
                                        $new->save();

                                        CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $day->toDateString(), $initialHourTuesday, $finalHourTuesday, $quotation);
                                        $shareServiceOrder = new ShareServiceOrder();
                                        $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                        $shareServiceOrder->id_service_order = $new->id;
                                        $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                        $shareServiceOrder->save();
                                    }
                                }
                                CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $day->toDateString(), $initialHourTuesday, $finalHourTuesday, $quotation);
                                $this->updateStatusQuotation($quotationId);
                            }
                        } else {
                            $scheduleDays->push([
                                'folio' => $folioOrder . "-" . ++$folioId,
                                'name' => $branch->name,
                                'date' => $day->toDateString(),
                                'hour' => $initialHourTuesday . ' - ' . $finalHourTuesday,
                                'month' => $day->month
                            ]);
                        }
                    }
                    if ($day->isWednesday() && $checkboxWednesday == 'true') {
                        if ($save == 'true') {
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                $quotation, $branch, $concept, $observations);
                            if ($order) {
                                if ($techniciansAuxiliaries != null){
                                    $serviceOrderMain = service_order::find($order->id);
                                    $serviceOrderMain->is_shared = 1;
                                    $serviceOrderMain->is_main= 1;
                                    $serviceOrderMain->save();
                                    foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                        $serviceOrder = service_order::find($order->id);
                                        $new = $serviceOrder->replicate();
                                        $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                        $new->id_status = 4;
                                        $new->is_main = 0;
                                        $new->is_shared = 1;
                                        $new->save();

                                        CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $day->toDateString(), $initialHourWednesday, $finalHourWednesday, $quotation);
                                        $shareServiceOrder = new ShareServiceOrder();
                                        $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                        $shareServiceOrder->id_service_order = $new->id;
                                        $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                        $shareServiceOrder->save();
                                    }
                                }
                                CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $day->toDateString(), $initialHourWednesday, $finalHourWednesday, $quotation);
                                $this->updateStatusQuotation($quotationId);
                            }
                        } else {
                            $scheduleDays->push([
                                'folio' => $folioOrder . "-" . ++$folioId,
                                'name' => $branch->name,
                                'date' => $day->toDateString(),
                                'hour' => $initialHourWednesday . ' - ' . $finalHourWednesday,
                                'month' => $day->month
                            ]);
                        }
                    }
                    if ($day->isThursday() && $checkboxThursday == 'true') {
                        if ($save == 'true') {
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                $quotation, $branch, $concept, $observations);
                            if ($order) {
                                if ($techniciansAuxiliaries != null){
                                    $serviceOrderMain = service_order::find($order->id);
                                    $serviceOrderMain->is_shared = 1;
                                    $serviceOrderMain->is_main= 1;
                                    $serviceOrderMain->save();
                                    foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                        $serviceOrder = service_order::find($order->id);
                                        $new = $serviceOrder->replicate();
                                        $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                        $new->id_status = 4;
                                        $new->is_main = 0;
                                        $new->is_shared = 1;
                                        $new->save();

                                        CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $day->toDateString(), $initialHourThursday, $finalHourThursday, $quotation);
                                        $shareServiceOrder = new ShareServiceOrder();
                                        $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                        $shareServiceOrder->id_service_order = $new->id;
                                        $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                        $shareServiceOrder->save();
                                    }
                                }
                                CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $day->toDateString(), $initialHourThursday, $finalHourThursday, $quotation);
                                $this->updateStatusQuotation($quotationId);
                            }
                        } else {
                            $scheduleDays->push([
                                'folio' => $folioOrder . "-" . ++$folioId,
                                'name' => $branch->name,
                                'date' => $day->toDateString(),
                                'hour' => $initialHourThursday . ' - ' . $finalHourThursday,
                                'month' => $day->month
                            ]);
                        }
                    }
                    if ($day->isFriday() && $checkboxFriday == 'true') {
                        if ($save == 'true') {
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                $quotation, $branch, $concept, $observations);
                            if ($order) {
                                if ($techniciansAuxiliaries != null){
                                    $serviceOrderMain = service_order::find($order->id);
                                    $serviceOrderMain->is_shared = 1;
                                    $serviceOrderMain->is_main= 1;
                                    $serviceOrderMain->save();
                                    foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                        $serviceOrder = service_order::find($order->id);
                                        $new = $serviceOrder->replicate();
                                        $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                        $new->id_status = 4;
                                        $new->is_main = 0;
                                        $new->is_shared = 1;
                                        $new->save();

                                        CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $day->toDateString(), $initialHourFriday, $finalHourFriday, $quotation);
                                        $shareServiceOrder = new ShareServiceOrder();
                                        $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                        $shareServiceOrder->id_service_order = $new->id;
                                        $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                        $shareServiceOrder->save();
                                    }
                                }
                                CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $day->toDateString(), $initialHourFriday, $finalHourFriday, $quotation);
                                $this->updateStatusQuotation($quotationId);
                            }
                        } else {
                            $scheduleDays->push([
                                'folio' => $folioOrder . "-" . ++$folioId,
                                'name' => $branch->name,
                                'date' => $day->toDateString(),
                                'hour' => $initialHourFriday . ' - ' . $finalHourFriday,
                                'month' => $day->month
                            ]);
                        }
                    }
                    if ($day->isSaturday() && $checkboxSaturday == 'true') {
                        if ($save == 'true') {
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                $quotation, $branch, $concept, $observations);
                            if ($order) {
                                if ($techniciansAuxiliaries != null){
                                    $serviceOrderMain = service_order::find($order->id);
                                    $serviceOrderMain->is_shared = 1;
                                    $serviceOrderMain->is_main= 1;
                                    $serviceOrderMain->save();
                                    foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                        $serviceOrder = service_order::find($order->id);
                                        $new = $serviceOrder->replicate();
                                        $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                        $new->id_status = 4;
                                        $new->is_main = 0;
                                        $new->is_shared = 1;
                                        $new->save();

                                        CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $day->toDateString(), $initialHourSaturday, $finalHourSaturday, $quotation);
                                        $shareServiceOrder = new ShareServiceOrder();
                                        $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                        $shareServiceOrder->id_service_order = $new->id;
                                        $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                        $shareServiceOrder->save();
                                    }
                                }
                                CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $day->toDateString(), $initialHourSaturday, $finalHourSaturday, $quotation);
                                $this->updateStatusQuotation($quotationId);
                            }
                        } else {
                            $scheduleDays->push([
                                'folio' => $folioOrder . "-" . ++$folioId,
                                'name' => $branch->name,
                                'date' => $day->toDateString(),
                                'hour' => $initialHourSaturday . ' - ' . $finalHourSaturday,
                                'month' => $day->month
                            ]);
                        }
                    }
                    if ($day->isSunday() && $checkboxSunday == 'true') {
                        if ($save == 'true') {
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                $quotation, $branch, $concept, $observations);
                            if ($order) {
                                if ($techniciansAuxiliaries != null){
                                    $serviceOrderMain = service_order::find($order->id);
                                    $serviceOrderMain->is_shared = 1;
                                    $serviceOrderMain->is_main= 1;
                                    $serviceOrderMain->save();
                                    foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                        $serviceOrder = service_order::find($order->id);
                                        $new = $serviceOrder->replicate();
                                        $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                        $new->id_status = 4;
                                        $new->is_main = 0;
                                        $new->is_shared = 1;
                                        $new->save();

                                        CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $day->toDateString(), $initialHourSunday, $finalHourSunday, $quotation);
                                        $shareServiceOrder = new ShareServiceOrder();
                                        $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                        $shareServiceOrder->id_service_order = $new->id;
                                        $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                        $shareServiceOrder->save();
                                    }
                                }
                                CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $day->toDateString(), $initialHourSunday, $finalHourSunday, $quotation);
                                $this->updateStatusQuotation($quotationId);
                            }
                        } else {
                            $scheduleDays->push([
                                'folio' => $folioOrder . "-" . ++$folioId,
                                'name' => $branch->name,
                                'date' => $day->toDateString(),
                                'hour' => $initialHourSunday . ' - ' . $finalHourSunday,
                                'month' => $day->month
                            ]);
                        }
                    }
                }
            }
            DB::commit();
            if ($save == 'true') return response()->json(['code' => 201, 'message' => 'Servicios Programados.']);
            return response()->json($scheduleDays);
        } catch (Exception $exception) {
            if ($save == 'true') {
                DB::rollBack();
                return response()->json(['code' => 500, 'message' => $exception->getMessage()]);
            }
            return response()->json($scheduleDays);
        }
    }

    public function scheduleCustomServices(Request $request)
    {
        $scheduleDays = new Collection();
        $save = $request->get('save');
        try {
            DB::beginTransaction();
            $conceptId = $request->get('conceptId');
            $customerId = $request->get('customerId');
            $dates = $request->get('date');
            $durationService = $request->get('durationServiceCustom');
            $paymentMethod = $request->get('paymentMethod');
            $paymentWay = $request->get('paymentWay');
            $employeeId = $request->get('technician');
            $techniciansAuxiliaries = $request->get('techniciansAuxiliaries');
            $observations = $request->get('commentsTechnicians');
            $arrayDates = explode(",", $dates);
            $totalOrders = count($arrayDates);

            if ($techniciansAuxiliaries != null) {
                if (in_array($employeeId, $techniciansAuxiliaries)) {
                    return Response::json([
                        'code' => 500,
                        'message' => 'El técnico principal no puede ser auxiliar.'
                    ]);
                }
            }

            $concept = ScheduleIntelligent::getConceptById($conceptId);
            $quotationId = $concept->id_quotation;
            $branches = ScheduleIntelligent::getBranchesByConcept($concept->concept, $customerId, $quotationId);

            $quotation = quotation::find($quotationId);
            $customer = customer::find($customerId);
            $lastOrder = service_order::where('id_quotation', $quotationId)->count();
            $cost = $quotation->total / $totalOrders;

            $folio = explode('-', $quotation->id_quotation);

            $folioOrder = "OS-" . $folio[1];
            $folioId = 0;
            if ($lastOrder > 0) $folioId = $lastOrder;

            foreach ($arrayDates as $date) {
                $dateCarbon = Carbon::parse($date);
                $initialHourCustom = $dateCarbon->toTimeString();
                $finalHourCustom = $dateCarbon->addMinutes($durationService)->toTimeString();
                $date = $dateCarbon->toDateString();
                foreach ($branches as $branch) {
                    if ($save == 'true') {
                        $completeFolio = $folioOrder . "-" . ++$folioId;
                        $concept->unit_price = $cost;
                        $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                            $quotation, $branch, $concept, $observations);
                        if ($order) {
                            if ($techniciansAuxiliaries != null){
                                $serviceOrderMain = service_order::find($order->id);
                                $serviceOrderMain->is_shared = 1;
                                $serviceOrderMain->is_main= 1;
                                $serviceOrderMain->save();
                                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                                    $serviceOrder = service_order::find($order->id);
                                    $new = $serviceOrder->replicate();
                                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                                    $new->id_status = 4;
                                    $new->is_main = 0;
                                    $new->is_shared = 1;
                                    $new->save();

                                    CommonCustomServices::createEvent($customer, $branch, $techniciansAuxiliary, $new, $date, $initialHourCustom, $finalHourCustom, $quotation);
                                    $shareServiceOrder = new ShareServiceOrder();
                                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                                    $shareServiceOrder->id_service_order = $new->id;
                                    $shareServiceOrder->id_employee = $techniciansAuxiliary;
                                    $shareServiceOrder->save();
                                }
                            }
                            CommonCustomServices::createEvent($customer, $branch, $employeeId, $order, $date, $initialHourCustom, $finalHourCustom, $quotation);
                            $this->updateStatusQuotation($quotationId);
                        }
                    } else {
                        $scheduleDays->push([
                            'folio' => $folioOrder . "-" . ++$folioId,
                            'name' => $branch->name,
                            'date' => $date,
                            'hour' => $initialHourCustom . ' - ' . $finalHourCustom,
                            'month' => Carbon::parse($date)->month
                        ]);
                    }
                }
            }

            DB::commit();
            if ($save == 'true') return response()->json(['code' => 201, 'message' => 'Servicios Programados.']);
            return response()->json($scheduleDays);
        } catch (Exception $exception) {
            if ($save == 'true') {
                DB::rollBack();
                return response()->json(['code' => 500, 'message' => $exception->getMessage()]);
            }
            return response()->json($scheduleDays);
        }
    }


    // Schedules with excel.
    public function exportTemplateScheduleServices(Request $request) {
        $data = $request->all();
        $export = ScheduleExcel::createFormatExcel($data);
        if ($export) {
            return redirect()->back()->with(['success' => 'Plantilla Descargada', 'message' => 'Complete la plantilla.']);
        } else {
            return redirect()->back()->with(['error' => 'Error al generar plantilla', 'message' => 'Algo salio mal, intente de nuevo.']);
        }
    }

    public function importFromExcel(Request $request) {
        try {
            // Get requests
            $data = $request->all();
            $document = request('fileImportSchedules');
            $quotationId = $request->get('quotationId');

            // Upload file document
            $path = ScheduleExcel::uploadFileInStorage($document, $quotationId);
            $schedule = new ScheduleExcel();
            $response = $schedule->importFormatExcel($path, $data);

            return redirect()->back()->with([$response['type'] => $response['title'], 'message' => $response['message']]);
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'Error al Importar', 'message' => $exception->getMessage()]);
        }
    }

    private function getMonthsBim($termMonth, $number) {
        $divider = $termMonth / $number;
        $arrayTermMonthDivider = array_fill(0, $divider, '');
        $arrayBim = array();
        foreach ($arrayTermMonthDivider as $k => $it) {
            array_push($arrayBim, ($k * $number));
        }
        return $arrayBim;
    }
}
