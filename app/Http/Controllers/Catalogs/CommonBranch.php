<?php

namespace App\Http\Controllers\Catalogs;

use App\employee;
use App\JobCenterSession;
use App\profile_job_center;
use App\treeJobCenter;
use Illuminate\Support\Facades\Auth;

class CommonBranch
{
    public static function getBranchByJobCenter($jobcenter){
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        $jobCenters = profile_job_center::where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = profile_job_center::where('profile_job_centers_id', $employee->profile_job_centers_id)->get();
        $profileJobCenter = $profileJobCenter->id;
        $idJobCenter = 0;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        return [$jobCenterSession, $jobCenters, $profileJobCenter];
    }
}