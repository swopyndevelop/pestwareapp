<?php

namespace App\Http\Controllers\Catalogs;

use App\cash;
use App\DescriptionCustomQuote;
use App\employee;
use App\expense;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\PlagueCategory;
use App\profile_job_center;
use App\purchase_order;
use App\service_order;
use App\Tax;
use App\treeJobCenter;
use App\TypeStation;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use App\extra;
use App\price;
use App\concept;
use App\voucher;
use App\discount;
use App\Indication;
use App\payment_way;
use App\plague_type;
use App\product_type;
use App\product_unit;
use App\source_origin;
use App\order_cleaning;
use App\payment_method;
use App\infestation_degree;
use App\establishment_type;
use App\application_method;
use App\product_presentation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function index(){
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        return view('adminlte::catalogs.index');
    }

#region store

    //Module Quotation CRUD
    public function storeSourceOrigin(Request $request){

        try {
            $source = new source_origin;
            $source->name = $request->name;
            $source->companie = Auth::user()->companie;
            $source->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $source->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego la fuente de origen ' . $source->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    public function storeTypeService(Request $request){

        try {
            $establishment = new establishment_type;
            $establishment->name = $request->name;
            $establishment->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $establishment->id_company = Auth::user()->companie;
            $establishment->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el tipo de servicio ' . $establishment->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeDiscount(Request $request){

        try {
            $discount = new discount;
            $discount->title = $request->name;
            $discount->description = $request->description;
            $discount->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $discount->id_company = Auth::user()->companie;
            $discount->percentage = $request->percentage;
            $discount->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el descuento ' . $discount->title
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeExtra(Request $request){

        try {
            $extra = new extra;
            $extra->name = $request->name;
            $extra->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $extra->description = $request->description;
            $extra->amount = $request->amount;
            $extra->id_company = Auth::user()->companie;
            $extra->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el extra ' . $extra->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeIndication(Request $request){

        try {
            $indication = new Indication;
            $indication->name = $request->name;
            $indication->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $indication->key = $request->key;
            $indication->description = $request->description;
            $indication->company_id = Auth::user()->companie;
            $indication->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego las indicaciones ' . $indication->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeCustomDescriptions(Request $request){

        try {
            $customDescription = new DescriptionCustomQuote();
            $customDescription->name = $request->get('name');
            $customDescription->description = $request->get('description');
            $customDescription->id_company = Auth::user()->companie;
            $customDescription->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $customDescription->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego la descripción ' . $customDescription->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    //Module TypeStations Store
    public function storeStation(Request $request){

        try {
            $station = new TypeStation;
            if (strlen($request->get('name')) > 45)  {
                return response()->json([
                    'code' => 500,
                    'message' => 'No debe exceder los 45 caracteres'
                ]);
            }
            $station->name = $request->get('name');
            $station->key = $request->get('key');
            $station->id_type_area = $request->get('typeArea');
            $station->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $station->id_company = Auth::user()->companie;
            $station->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego las tipo de estaciones ' . $station->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    public function storeMethodApplication(Request $request){

        try {
            $applicationMethod = new application_method;
            $applicationMethod->name = $request->name;
            $applicationMethod->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $applicationMethod->id_company = Auth::user()->companie;
            $applicationMethod->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el método ' . $applicationMethod->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    public function storeGradeInfestation(Request $request){

        try {
            $infestationDegree = new infestation_degree;
            $infestationDegree->name = $request->name;
            $infestationDegree->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $infestationDegree->companie = Auth::user()->companie;
            $infestationDegree->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el grado ' . $infestationDegree->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeClenaing(Request $request){

        try {
            $orderCleaning = new order_cleaning;
            $orderCleaning->name = $request->name;
            $orderCleaning->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $orderCleaning->companie = Auth::user()->companie;
            $orderCleaning->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego ' . $orderCleaning->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storePlague(Request $request){

        try {
            $isMonitoring = 0;
            $isMonitoringCapture = $request->get('isMonitoringCapture');
            $isMonitoringLight = $request->get('isMonitoringLight');
            if ($isMonitoringCapture == "true" && $isMonitoringLight == "true") $isMonitoring = 3;
            elseif ($isMonitoringCapture == "true" && $isMonitoringLight != "true") $isMonitoring = 1;
            elseif ($isMonitoringLight == "true" && $isMonitoringCapture != "true") $isMonitoring = 2;

            $plague = new plague_type;
            $plague->name = $request->get('name');
            $plague->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $plague->id_categorie = $request->get('category');
            $plague->is_monitoring = $isMonitoring;
            $plague->id_company = Auth::user()->companie;
            $plague->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego la plaga ' . $plague->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storePlagueCategory(Request $request){

        try {

            $category = new PlagueCategory;
            $category->name = $request->get('name');
            $category->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $category->id_company = Auth::user()->companie;
            $category->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego la categoría ' . $category->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    public function storePresentation(Request $request){

        try {
            $presentation = new product_presentation;
            $presentation->name = $request->name;
            $presentation->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $presentation->id_company = Auth::user()->companie;
            $presentation->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego la presentación ' . $presentation->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    public function storeType(Request $request){

        try {
            $type = new product_type;
            $type->name = $request->name;
            $type->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $type->id_company = Auth::user()->companie;
            $type->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el tipo ' . $type->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeUnits(Request $request){

        try {
            $unit = new product_unit;
            $unit->name = $request->name;
            $unit->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $unit->id_company = Auth::user()->companie;
            $unit->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego la unidad ' . $unit->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storePaymentMethod(Request $request){

        try {
            $paymentMethod = new payment_method;
            $paymentMethod->name = $request->name;
            $paymentMethod->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $paymentMethod->companie = Auth::user()->companie;
            $paymentMethod->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el método ' . $paymentMethod->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    public function storePaymentWay(Request $request){

        try {
            $paymentWay = new payment_way;
            $paymentWay->name = $request->get('name');
            $paymentWay->credit_days = $request->get('creditDays');
            $paymentWay->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $paymentWay->id_company = Auth::user()->companie;
            $paymentWay->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego la forma ' . $paymentWay->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeComprobant(Request $request){

        try {
            $voucher = new voucher;
            $voucher->name = $request->name;
            $voucher->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $voucher->companie = Auth::user()->companie;
            $voucher->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el comprobante ' . $voucher->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeConcepts(Request $request){

        try {
            $concept = new concept;
            $concept->name = $request->name;
            $concept->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $concept->type = $request->categorie;
            $concept->id_company = Auth::user()->companie;
            $concept->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el concepto ' . $concept->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
        
    }

    public function storeTaxes(Request $request){

        try {
            $tax = new Tax;
            $tax->name = $request->get('name');
            $tax->value = $request->get('value');
            $tax->profile_job_center_id = $request->get('selectJobCenterCatalogsListId');
            $tax->id_company = Auth::user()->companie;
            $tax->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se agrego el impuesto ' . $tax->name
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    //Updates
    public function updateSourceOrigin(Request $request){

        $source = source_origin::find($request->id);
        $source->name = $request->name;
        $source->save();

        return redirect()->route('list_sourceorigin')->with('success', 'Datos Actualizados.');

    }

    public function updateTypeService(Request $request){

        $establishment = establishment_type::find($request->id);
        $establishment->name = $request->name;
        $establishment->save();

        return redirect()->route('list_typeservice')->with('success', 'Datos Actualizados.');

    }

    public function updateDiscount(Request $request){

        $discount = discount::find($request->id);
        $discount->title = $request->name;
        $discount->description = $request->description;
        $discount->percentage = $request->percentage;
        $discount->save();

        return redirect()->route('list_discount')->with('success', 'Datos Actualizados.');

    }

    public function updateExtra(Request $request){

        $extra = extra::find($request->id);
        $extra->name = $request->name;
        $extra->description = $request->description;
        $extra->amount = $request->amount;
        $extra->save();

        return redirect()->route('list_extra')->with('success', 'Datos Actualizados.');

    }

    public function updateIndication(Request $request){

        $indication = Indication::find($request->id);
        $indication->name = $request->name;
        $indication->key = $request->key;
        $indication->description = $request->description;
        $indication->save();

        return redirect()->route('list_indications')->with('success', 'Datos Actualizados.');

    }

    public function updateCustomDescription(Request $request){

        $customDescription = DescriptionCustomQuote::find($request->get('id'));
        $customDescription->name = $request->get('name');
        $customDescription->description = $request->get('description');
        $customDescription->save();

        return redirect()->route('list_custom_descriptions')->with('success', 'Datos Actualizados.');

    }

    public function updateStation(Request $request){

        $station = TypeStation::find($request->get('id'));
        if (strlen($request->get('name')) > 45)  {
            return redirect()->route('list_type_stations')->with('error', 'No debe exceder los 45 caracteres.');
        }
        $station->name = $request->get('name');
        $station->key = $request->get('key');
        $station->id_type_area = $request->get('typeArea');
        $station->save();

        return redirect()->route('list_type_stations')->with('success', 'Datos Actualizados.');

    }

    public function updateMethodApplication(Request $request){

        $applicationMethod = application_method::find($request->id);
        $applicationMethod->name = $request->name;
        $applicationMethod->save();

        return redirect()->route('list_methodapp')->with('success', 'Datos Actualizados.');

    }

    public function updateGradeInfestation(Request $request){

        $infestationDegree = infestation_degree::find($request->id);
        $infestationDegree->name = $request->name;
        $infestationDegree->save();

        return redirect()->route('list_grade')->with('success', 'Datos Actualizados.');

    }

    public function updateClenaing(Request $request){

        $orderCleaning = order_cleaning::find($request->id);
        $orderCleaning->name = $request->name;
        $orderCleaning->save();

        return redirect()->route('list_cleaning')->with('success', 'Datos Actualizados.');

    }

    public function updatePlague(Request $request){

        $isMonitoring = 0;
        $isMonitoringCapture = $request->get('isMonitoringCapture');
        $isMonitoringLight = $request->get('isMonitoringUV');
        if ($isMonitoringCapture == "on" && $isMonitoringLight == "on") $isMonitoring = 3;
        elseif ($isMonitoringCapture == "on" && $isMonitoringLight != "on") $isMonitoring = 1;
        elseif ($isMonitoringLight == "on" && $isMonitoringCapture != "on") $isMonitoring = 2;

        $plague = plague_type::find($request->get('id'));
        $plague->name = $request->get('name');
        $plague->id_categorie = $request->get('plagueCategory');
        $plague->is_monitoring = $isMonitoring;
        $plague->save();

        return redirect()->route('list_plague')->with('success', 'Datos Actualizados.');

    }

    public function updatePlagueCategory(Request $request){

        $plague = PlagueCategory::find($request->get('id'));
        $plague->name = $request->get('name');
        $plague->id_company = Auth::user()->companie;
        $plague->save();

        return redirect()->route('list_plague_categories')->with('success', 'Datos Actualizados.');

    }

    public function updatePresentation(Request $request){

        $presentation = product_presentation::find($request->id);
        $presentation->name = $request->name;
        $presentation->save();

        return redirect()->route('list_presentation')->with('success', 'Datos Actualizados.');

    }

    public function updateType(Request $request){

        $type = product_type::find($request->id);
        $type->name = $request->name;
        $type->save();

        return redirect()->route('list_type')->with('success', 'Datos Actualizados.');

    }

    public function updateUnits(Request $request){

        $unit = product_unit::find($request->id);
        $unit->name = $request->name;
        $unit->save();

        return redirect()->route('list_units')->with('success', 'Datos Actualizados.');

    }

    public function updatePaymentMethod(Request $request){

        $paymentMethod = payment_method::find($request->id);
        $paymentMethod->name = $request->name;
        $paymentMethod->save();

        return redirect()->route('list_paymentmethod')->with('success', 'Datos Actualizados.');

    }

    public function updatePaymentWay(Request $request){

        $paymentWay = payment_way::find($request->id);
        $paymentWay->name = $request->get('name');
        $paymentWay->credit_days = $request->get('creditDays');
        $paymentWay->save();

        return redirect()->route('list_paymentway')->with('success', 'Datos Actualizados.');

    }

    public function updateComprobant(Request $request){

        $voucher = voucher::find($request->id);
        $voucher->name = $request->name;
        $voucher->save();

        return redirect()->route('list_comprobant')->with('success', 'Datos Actualizados.');

    }

    public function updateConcepts(Request $request){

        $concept = concept::find($request->id);
        $concept->name = $request->name;
        $concept->type = $request->type;
        $concept->save();

        return redirect()->route('list_concepts')->with('success', 'Datos Actualizados.');

    }

    public function updateTaxes(Request $request){

        $tax = Tax::find($request->id);
        $tax->name = $request->get('name');
        $tax->value = $request->get('value');
        $tax->save();

        return redirect()->route('list_taxes')->with('success', 'Datos Actualizados.');

    }

    public function updatePrices(Request $request){

        $price = price::find($request->id);
        $price->MTTO_biodegradable = $request->MTTO_biodegradable;
        $price->MTTO_organic = $request->MTTO_organic;
        $price->MTTO_service_plus_garden = $request->MTTO_service_plus_garden;
        $price->MTTO_garden = $request->MTTO_garden;
        $price->CA_1_application = $request->CA_1_application;
        $price->CA_reinforcement = $request->CA_reinforcement;
        $price->CH_1_application = $request->CH_1_application;
        $price->CH_reinforcement = $request->CH_reinforcement;
        $price->TER_treatment = $request->TER_treatment;
        $price->RAT_amount_traps = $request->RAT_amount_traps;
        $price->RAT_bait_stations = $request->RAT_bait_stations;
        $price->RAT_glue_traps = $request->RAT_glue_traps;
        $price->save();

        return redirect()->route('list_prices')->with('success', 'Lista de Precios Actualizada Correctamente.');
        
    }

    //Delete
    public function deleteSourceOrigin($id, Request $request){
        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])){
                $source = source_origin::find($id);
                $source->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_sourceorigin'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteTypeService($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $establishment = establishment_type::find($id);
                $establishment->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_typeservice'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteDiscount($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])){
                $discount = discount::find($id);
                $discount->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_discount'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteExtra($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])){
                $extra = extra::find($id);
                $extra->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_extra'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteIndication($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $indication = Indication::find($id);
                $indication->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_indications'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteCustomDescription($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $customDescription = DescriptionCustomQuote::find($id);
                $customDescription->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_custom_descriptions'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteStation($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $station = TypeStation::find($id);
                $station->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_type_stations'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteMethodApplication($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $applicationMethod = application_method::find($id);
                $applicationMethod->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_methodapp'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteGradeInfestation($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $infestationDegree = infestation_degree::find($id);
                $infestationDegree->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_grade'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteClenaing($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $orderCleaning = order_cleaning::find($id);
                $orderCleaning->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_cleaning'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deletePlague($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $plague = plague_type::find($id);
                $plague->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_plague'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deletePlagueCategory($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $category = PlagueCategory::find($id);
                $category->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_plague_categories'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deletePresentation($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $presentation = product_presentation::find($id);
                $presentation->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_presentation'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteType($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $type = product_type::find($id);
                $type->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_type'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteUnits($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $unit = product_unit::find($id);
                $unit->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_units'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deletePaymentMethod($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $paymentMethod = payment_method::find($id);
                $paymentMethod->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_paymentmethod'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deletePaymentWay($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $paymentWay = payment_way::find($id);
                $paymentWay->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_paymentway'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteComprobant($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $voucher = voucher::find($id);
                $voucher->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_comprobant'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteConcepts($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $concept = concept::find($id);
                $concept->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_concepts'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function deleteTaxes($id, Request $request){

        try{
            $passwordManager = $request->get('passwordManagerCatalogs');
            $emailManager = Auth::user()->email;

            if (Auth::attempt(['email' => $emailManager, 'password' => $passwordManager])) {
                $tax = Tax::find($id);
                $tax->delete();
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos eliminados correctamente.',
                    'url' => 'list_taxes'
                ]);
            } else {
                return response()->json([
                    'code' => 300,
                    'message' => 'Credenciales incorrectas',
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

#endregion functions store

#region list

    //Module Quotation CRUD
    public function listSourceOrigin($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $source = source_origin::where('companie', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($source as $sou) {
            $source->map(function($sou){
                $sou->type = 1;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $source, 'title' => "Fuente de Origen", 'type' => 1,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_sourceorigin']);

    }

    public function listTypeService($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $establishment = establishment_type::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($establishment as $est) {
            $establishment->map(function($est){
                $est->type = 2;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $establishment, 'title' => "Tipos de Servicio", 'type' => 2,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_typeservice']);
        
    }

    public function listDiscount($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $discount = discount::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('description', '<>', 'No borrar. Este descuento es propio del sistema, si se borra el sistema dejará de funcionar.')
            ->where('title', 'like', '%' . $search . '%')
            ->orderBy('title')
            ->paginate(10);
        foreach ($discount as $dis) {
            $discount->map(function($dis){
                $dis->type = 3;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $discount, 'title' => "Descuentos", 'type' => 3,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_discount']);

    }

    public function listExtra($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $extra = extra::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('description', '<>', 'No borrar. Este extra es propio del sistema, si se borra el sistema dejará de funcionar.')
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($extra as $ext) {
            $extra->map(function($ext){
                $ext->type = 4;
            });
        }
        $symbol_country = CommonCompany::getSymbolByCountry();
        return view('vendor.adminlte.catalogs._list')
            ->with(['data' => $extra, 'title' => "Extras", 'type' => 4, 'symbol_country' => $symbol_country,
                'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_extra']);
    }

    //Module Services CRUD
    public function listMethodApplication($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $applicationMethod = application_method::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($applicationMethod as $apm) {
            $applicationMethod->map(function($apm){
                $apm->type = 5;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $applicationMethod, 'title' => "Métodos de Aplicación", 'type' => 5,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_methodapp']);

    }

    public function listGradeInfestation($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $infestationDegree = infestation_degree::where('companie', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->paginate(10);
        foreach ($infestationDegree as $ifd) {
            $infestationDegree->map(function($ifd){
                $ifd->type = 6;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $infestationDegree, 'title' => "Grado de Infestación", 'type' => 6,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_grade']);
        
    }

    public function listClenaing($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $orderCleaning = order_cleaning::where('companie', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($orderCleaning as $oc) {
            $orderCleaning->map(function($oc){
                $oc->type = 7;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $orderCleaning, 'title' => "Orden y Limpieza", 'type' => 7,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_cleaning']);
        
    }

    public function listPlague($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $plagues = \DB::table('plague_types as pt')
            ->join('plague_categories as pc', 'pt.id_categorie', 'pc.id')
            ->where('pt.id_company', Auth::user()->companie)
            ->where('pt.profile_job_center_id', $dataJobCenter[2])
            ->where('pt.name', 'like', '%' . $search . '%')
            ->select('pt.*', 'pc.name as category')
            ->orderBy('name')
            ->paginate(10);

        $categories = PlagueCategory::where('profile_job_center_id', $dataJobCenter[2])->get();
        foreach ($plagues as $plg) {
            $plagues->map(function($plg){
                $plg->type = 8;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $plagues, 'title' => "Plagas", 'type' => 8, 'categories' => $categories,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_plague']);
    }

    public function listPlagueCategories($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $categories = \DB::table('plague_categories as pc')
            ->where('pc.id_company', Auth::user()->companie)
            ->where('pc.profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($categories as $category) {
            $categories->map(function($category){
                $category->type = 18;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $categories, 'title' => "Categorías de Plagas", 'type' => 18,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_plague_categories']);
    }

    //Module TypeStations
    public function listStations($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $typeStations = TypeStation::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->whereNotIn('id_type_area', [1, 2, 3, 7])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($typeStations as $typeStation) {
            $typeStations->map(function($typeStation){
                $typeStation->type = 17;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $typeStations, 'title' => "Tipo de Estaciones", 'type' => 17,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_type_stations']);
    }

    //Module Products CRUD
    public function listPresentation($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $presentation = product_presentation::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($presentation as $pre) {
            $presentation->map(function($pre){
                $pre->type = 9;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $presentation, 'title' => "Presentaciones", 'type' => 9,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_presentation']);

    }

    public function listType($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $type = product_type::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($type as $ty) {
            $type->map(function($ty){
                $ty->type = 10;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $type, 'title' => "Tipos", 'type' => 10,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_type']);
        
    }

    public function listUnits($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $unit = product_unit::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($unit as $uni) {
            $unit->map(function($uni){
                $uni->type = 11;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $unit, 'title' => "Unidades", 'type' => 11,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_units']);
        
    }

    //Module Accounting CRUD
    public function listPaymentMethod($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $paymentMethod = payment_method::where('companie', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($paymentMethod as $pm) {
            $paymentMethod->map(function($pm){
                $pm->type = 12;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $paymentMethod, 'title' => "Métodos de Pago", 'type' => 12,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_paymentmethod']);

    }

    public function listPaymentWay($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $paymentWay = payment_way::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($paymentWay as $pw) {
            $paymentWay->map(function($pw){
                $pw->type = 13;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $paymentWay, 'title' => "Forma de Pago", 'type' => 13,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_paymentway']);
        
    }

    public function listComprobant($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $voucher = voucher::where('companie', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($voucher as $vou) {
            $voucher->map(function($vou){
                $vou->type = 14;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $voucher, 'title' => "Comprobantes", 'type' => 14,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_comprobant']);
        
    }

    public function listConcepts($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $concept = concept::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->select('id', 'name', 'type as tipo')
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($concept as $con) {
            $concept->map(function($con){
                $con->type = 15;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $concept, 'title' => "Conceptos", 'type' => 15,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_concepts']);
        
    }

    public function listIndications($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $indications = Indication::where('company_id', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($indications as $indication) {
            $indications->map(function($indication){
                $indication->type = 16;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $indications, 'title' => "Indicaciones", 'type' => 16,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_indications']);

    }

    public function listCustomDescriptions($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $customDescriptions = DescriptionCustomQuote::where('id_company', Auth::user()->companie)
            ->where('profile_job_center_id', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($customDescriptions as $customDescription) {
            $customDescriptions->map(function($customDescription){
                $customDescription->type = 20;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $customDescriptions, 'title' => "Descripciones Personalizadas", 'type' => 20,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_custom_descriptions']);

    }

    public function listPrices(){

        $prices = price::where('id_company', Auth::user()->companie)
            ->orderBy('name')
            ->paginate(10);
        return view('vendor.adminlte.catalogs._listPrices')->with(['prices' => $prices]);

    }

    public function listTaxes($jobcenter = 0, Request $request){
        $search = $request->get('search');
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $columns = ['id','name','value'];
        $tax = Tax::where('profile_job_center_id', $dataJobCenter[2])
            ->select($columns)
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name')
            ->paginate(10);
        foreach ($tax as $con) {
            $tax->map(function($con){
                $con->type = 19;
            });
        }
        return view('vendor.adminlte.catalogs._list')->with(['data' => $tax, 'title' => "Impuestos", 'type' => 19,
            'jobCenterSession' => $dataJobCenter[0], 'jobCenters' => $dataJobCenter[1], 'indexRoute' => 'list_taxes']);

    }

    public function updatePaymentWayMatchChange($companyId, $oldPayment, $newPayment) {
        /*try {
            cash::where('id_payment_way', $oldPayment)
                ->where('companie', $companyId)
                ->update(['id_payment_way' => $newPayment]);
            expense::where('id_payment_way', $oldPayment)
                ->where('companie', $companyId)
                ->update(['id_payment_way' => $newPayment]);
            purchase_order::where('id_payment_way', $oldPayment)
                ->where('companie', $companyId)
                ->update(['id_payment_way' => $newPayment]);
            return \Response::json([
                'code' => 200,
                'message' => 'Updated orders success'
            ]);
        } catch (Exception $exception) {
            return \Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }*/
    }

#endregion list

}
