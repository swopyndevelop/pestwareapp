<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class ManagementController extends Controller
{
    public function index(){
        $data = 'Company';
        return view('vendor.adminlte.management.index')->with(
            [
                'data' => $data,
            ]);
    }

    public function importBanner(Request $request){
        $fileBannerOne = $request->file('fileBannerOne');
        $fileBannerTwo = $request->file('fileBannerTwo');
        $fileBannerThree = $request->file('fileBannerThree');

        if ($fileBannerOne != null) {
            Storage::disk('s3')->putFileAs('/banners/', $fileBannerOne, 'BannerOne' . ".jpg");
        }
        if ($fileBannerTwo != null) {
            Storage::disk('s3')->putFileAs('/banners/', $fileBannerTwo, 'BannerTwo' . ".jpg");
        }
        if ($fileBannerThree != null) {
            Storage::disk('s3')->putFileAs('/banners/', $fileBannerThree, 'BannerThree' . ".jpg");
        }

        return redirect()->route('management_general')->with('success', 'Guardado Correctamente');
    }
}
