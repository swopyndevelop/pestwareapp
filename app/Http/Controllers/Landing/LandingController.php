<?php

namespace App\Http\Controllers\Landing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LandingController extends Controller
{
    //
    public function index(){
        $data = 'Api';
        return view('welcome')->with($data);
    }
}
