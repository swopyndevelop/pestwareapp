<?php


namespace App\Http\Controllers\Blog;


use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;

class BlogController extends Controller
{

    public function index(){

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://www.googleapis.com',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
        $collectionItem = new Collection();

        $response = $client->request('GET', '/blogger/v3/blogs/2163376118199082900/posts?key=AIzaSyCkDqK1Hg3glq4_7uyVp-eLOGGXkdgg-Tk');
        $posts = json_decode( $response->getBody()->getContents(), true );
        $varpost= empty($posts['items']);

        $author = '';
        if ($varpost != true) {

            $postSingular = $posts['items'];

            foreach ($postSingular as $singular) {
                $author = $singular['author']['displayName'];
            }

            if ($author == null) $author = 'Desconocido';
            // Article
            usort($postSingular, function ($a, $b) {
                return strcmp($a["updated"], $b["updated"]);
            });
        }

        return view('vendor.adminlte.blog.blog')->with([
            'varpost' => $varpost,
            'posts' => $posts,
            'author' => $author
            ]);

    }


}
