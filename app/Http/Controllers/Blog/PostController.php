<?php


namespace App\Http\Controllers\Blog;


use GuzzleHttp\Client;

class PostController
{

    public function index($id)
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://www.googleapis.com',

        ]);

        $response = $client->request('GET', "/blogger/v3/blogs/2163376118199082900/posts/{$id}?key=AIzaSyCkDqK1Hg3glq4_7uyVp-eLOGGXkdgg-Tk");
        $post = json_decode( $response->getBody()->getContents(), true ) ;

        return view('vendor.adminlte.blog.posts', compact('post'));

    }

}
