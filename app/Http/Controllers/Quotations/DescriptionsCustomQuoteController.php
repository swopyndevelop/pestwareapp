<?php

namespace App\Http\Controllers\Quotations;

use App\DescriptionCustomQuote;
use Auth;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class DescriptionsCustomQuoteController extends Controller
{
    public function getDescriptionById($id) {
        try {
            $description = DescriptionCustomQuote::find($id);
            return Response::json([
                'code' => 200,
                'description' => $description->description
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function saveDescription(Request $request) {
        try {
            $description = new DescriptionCustomQuote();
            $description->name = $request->get('name');
            $description->description = $request->get('description');
            $description->profile_job_center_id = $request->get('jobCenterSelect');
            $description->id_company = Auth::user()->companie;
            $description->save();
            $descriptions = DescriptionCustomQuote::where('profile_job_center_id', $description->profile_job_center_id)->get();
            return Response::json([
                'code' => 201,
                'id' => $description->id,
                'message' => 'Se agrego la nueva descripción a tu historial.',
                'descriptions' => $descriptions
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
