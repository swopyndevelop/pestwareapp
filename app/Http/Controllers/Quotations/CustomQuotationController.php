<?php

namespace App\Http\Controllers\Quotations;

use App\Common\CommonFormats;
use App\companie;
use App\Country;
use App\customer_branche;
use App\establishment_type;
use App\extra;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Customers\CommonCustomer;
use App\Http\Controllers\ReportsPDF\QuotationPdf;
use App\Http\Controllers\Security\SecurityController;
use App\plague_type;
use App\PriceList;
use App\profile_job_center;
use App\service_order;
use App\source_origin;
use App\User;
use Exception;
use iio\libmergepdf\Merger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\custom_quote;
use App\customer;
use App\customer_data;
use App\quotation;
use App\discount_quotation;
use App\Mail\MailGenerate;
use App\Mail\QuotatioMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Response;

/**
 * Class CustomQuotationController
 * @package App\Http\Controllers
 * @author Alberto Martínez
 * @version 01/03/2019
 */
class CustomQuotationController extends Controller
{

    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    private $monthly = 1;
    private $bimonthly = 2;
    private $quarterly = 3;
    private $quarter = 4;
    private $biannual = 5;
    private $allDays = 6;

    public function getConceptsByQuote($id) {
        try {
            $quote = quotation::find($id);
            $customer = DB::table('customers as c')
                ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
                ->select('c.*', 'cd.email')
                ->where('c.id', $quote->id_customer)
                ->first();
            $concepts = DB::table('custom_quotes as cq')
                ->join('type_concepts as tc', 'cq.id_type_concepts', 'tc.id')
                ->join('type_services as ts', 'cq.id_type_service', 'ts.id')
                ->select('cq.concept', 'cq.quantity', 'cq.frecuency_month', 'cq.term_month',
                    'cq.unit_price', 'cq.id_type_concepts', 'tc.name as type_concepts', 'cq.id_type_service', 'ts.name as type_service')
                ->where('cq.id_quotation', $id)
                ->get();
            $plaguesQuote = DB::table('plague_type_quotation')->where('quotation_id', $quote->id)->get();
            $plagues = plague_type::where('profile_job_center_id', $quote->id_job_center)->get();
            $sources = source_origin::where('profile_job_center_id', $quote->id_job_center)->get();
            $establishments = establishment_type::where('profile_job_center_id', $quote->id_job_center)->get();
            return Response::json([
                'code' => 200,
                'sources' => $sources,
                'establishments' => $establishments,
                'plaguesQuote' => $plaguesQuote,
                'plagues' => $plagues,
                'customer' => $customer,
                'concepts' => $concepts,
                'quote' => $quote,
                'description' => $quote->description
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal.'
            ]);
        }
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     * @description Guarda una nueva cotización personalizada.
     */
    public function store(Request $request)
    {
        
        try {

            DB::beginTransaction();

            //Validate plan free
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                return response()->json([
                    'code' => 500,
                    'message' => "Algo salio mal, intenta de nuevo."
                ]);
            } else {
                $idUser = Auth::id();
                $jobCenterEmployee = $request->get('jobCenter');
                $customerIdFind = $request->get('customerIdFind');

                if ($customerIdFind != 0) {
                    $client = DB::table('customers as c')
                        ->where('id', $customerIdFind)
                        ->where('id_profile_job_center', $jobCenterEmployee)
                        ->first();
                } else {
                    $client = DB::table('customers as c')
                        ->where('cellphone', $request->get('cellphone'))
                        ->where('cellphone', '!=', 'null')
                        ->where('id_profile_job_center', $jobCenterEmployee)
                        ->first();
                }

                // Valida si el cliente intenta usar el teléfono para más de un cliente.
                if (!empty($client)) {
                    if ($client->name != $request->get('name') && $request->get('isConfirmedCustomer') == "false") {
                        return response()->json([
                            'code' => 304,
                            'message' => "El número " . $client->cellphone . " pertenece al cliente " . $client->name . ", si continuas guardando la cotización remplazarás los datos del cliente por los de esta cotización."
                        ]);
                    }
                }

                if (empty($client)){
                    //Agregar Cliente -> Tabla customers
                    $customer = new customer;
                    $customer->name = $request->get('name');
                    $customer->establishment_name = $request->get('establishmentName');
                    $customer->cellphone = $request->get('cellphone');
                    $customer->cellphone_main = $request->get('cellphoneMain');
                    $customer->establishment_id = $request->get('establishment');
                    $customer->colony = $request->get('colony');
                    $customer->municipality = $request->get('municipality');
                    $customer->source_origin_id = $request->get('sourceOrigin');
                    $customer->id_profile_job_center = $jobCenterEmployee;
                    $customer->companie = Auth::user()->companie;
                    $customer->save();

                    $customerId = $customer->id;

                    //Agregar los datos conocidos en la tabla customer_datas
                    $customer_data = new customer_data;
                    $customer_data->customer_id = $customer->id;
                    $customer_data->reference_address = $request->get('reference_address');
                    $customer_data->email = $request->get('email');
                    $customer_data->phone_number = $request->get('cellphone');
                    $customer_data->save();
                }else{
                    //Actualizar Cliente -> Tabla customers
                    $customer = customer::find($client->id);
                    $customer->name = $request->get('name');
                    $customer->establishment_name = $request->get('establishmentName');
                    $customer->cellphone = $request->get('cellphone');
                    $customer->establishment_id = $request->get('establishment');
                    $customer->cellphone_main = $request->get('cellphoneMain');
                    $customer->colony = $request->get('colony');
                    $customer->municipality = $request->get('municipality');
                    $customer->source_origin_id = $request->get('sourceOrigin');
                    $customer->id_profile_job_center = $jobCenterEmployee;
                    $customer->save();

                    $customerId = $client->id;

                    //Actualizar los datos conocidos en la tabla customer_datas
                    $customer_data = customer_data::where('customer_id', $client->id)->first();
                    $customer_data->reference_address = $request->get('reference_address');
                    $customer_data->email = $request->get('email');
                    $customer_data->phone_number = $request->get('cellphone');
                    $customer_data->save();
                }

                $idPriceListFind = PriceList::where('establishment_id', $request->get('establishment'))
                    ->where('profile_job_center_id', $jobCenterEmployee)
                    ->where('status', 1)
                    ->first();

                $idExtra = extra::where('profile_job_center_id', $jobCenterEmployee)
                    ->where('description', 'No borrar. Este extra es propio del sistema, si se borra el sistema dejará de funcionar.')
                    ->first();

                if (!$idPriceListFind) {
                    DB::rollBack();
                    return response()->json([
                        'code' => 500,
                        'message' => "No existe ninguna lista de precio para el servicio seleccionado."
                    ]);
                }

                //Crear la nueva Cotizacion
                $quotation = new quotation;
                $quotation->id_customer = $customerId;
                $quotation->establishment_id = $request->get('establishment');
                $quotation->id_status = 10;
                $quotation->total = $request->get('total');
                $quotation->price = $request->get('price');
                $quotation->price_reinforcement = 0;
                $quotation->construction_measure = 100;
                $quotation->description = $request->get('description');
                $quotation->user_id = $idUser;
                $quotation->id_job_center = $jobCenterEmployee;
                $quotation->id_extra = $idExtra->id;
                $quotation->companie = Auth::user()->companie;
                $quotation->id_plague_jer = 1;
                $quotation->messenger = $request->get('messengerCustom');
                $idPriceList = $idPriceListFind->id;
                $quotation->id_quotation = CommonQuotation::getFolioQuote();
                $quotation->pesticide_product = $request->get('pesticide');
                $quotation->id_price_list = $idPriceList;
                $quotation->save();

                //Guardar plagas asociados al id de la cotización.
                $contador = count($request->get('plague'));
                $plagues = [];

                for ($i = 0; $i < $contador; $i++) {
                    $separador = explode(",", $request->get('plague')[$i]);
                    $idPlague = $separador[0];

                    array_push($plagues, $idPlague);
                }

                $quotation->plague_type()->sync($plagues);

                //Asignar descuento y cotización
                $discountId = DB::table('discounts')
                    ->where('description', 'No borrar. Este descuento es propio del sistema, si se borra el sistema dejará de funcionar.')
                    ->where('profile_job_center_id', $jobCenterEmployee)
                    ->first();
                $discount = new discount_quotation;
                $discount->quotation_id = $quotation->id;
                $discount->discount_id = $discountId->id;
                $discount->save();

                //Crear las cotizaciones personalizadas
                $json = $request->input('arrayConcepts');
                $array = json_decode($json);
                $branchId = 0;

                foreach($array as $obj){

                    $concept = $obj->concept;
                    $quantity = $obj->quantity;
                    $frequency = $obj->frequency;
                    $contract = $obj->contract;
                    $price = $obj->price;
                    $typeId = $obj->typeId;
                    $typeServiceId = $obj->typeServiceId;

                    if ($obj->typeId == 2) {
                        $frequency = 1;
                        $subtotal = $obj->quantity * $obj->price * $obj->contract;
                    } else {
                        if ($typeServiceId != $this->allDays) $subtotal = $obj->quantity * $obj->frequency * $obj->contract * $obj->price;
                        if ($typeServiceId == $this->bimonthly) $subtotal = $subtotal / 2;
                        if ($typeServiceId == $this->quarterly) $subtotal = $subtotal / 3;
                        if ($typeServiceId == $this->quarter) $subtotal = $subtotal / 4;
                        if ($typeServiceId == $this->biannual) $subtotal = $subtotal / 6;
                        if ($typeServiceId == $this->allDays) $subtotal = $obj->quantity * $obj->contract * $obj->price;
                    }

                    $customQuote = new custom_quote;
                    $customQuote->id_quotation = $quotation->id;
                    $customQuote->concept = $concept;
                    $customQuote->quantity = $quantity;
                    $customQuote->frecuency_month = $frequency;
                    $customQuote->term_month = $contract;
                    $customQuote->unit_price = $price;
                    $customQuote->subtotal = $subtotal;
                    $customQuote->id_type_concepts = $typeId;
                    $customQuote->id_type_service = $typeServiceId;
                    $customQuote->save();

                    if ($obj->typeId != 2) {
                        $arrayBranches = array_fill(0, $obj->quantity, '');
                        foreach ($arrayBranches as $item) {
                            $branche = new customer_branche;
                            $branche->branch_id = ++$branchId;
                            $branche->customer_id = $customerId;
                            $branche->id_quotation = $quotation->id;
                            $branche->name = $obj->concept;
                            $branche->type = $obj->concept;
                            $branche->phone = $request->get('cellphone');
                            $branche->manager = $customer->establishment_name;
                            $branche->email = $customer_data->email;
                            $branche->address = $customer_data->address != null ? $customer_data->address : "";
                            $branche->address_number = $customer_data->address_number != null ? $customer_data->address_number : "";
                            $branche->colony = $customer->colony != null ? $customer->colony : "";
                            $branche->municipality = $customer->municipality != null ? $customer->municipality : "";
                            $branche->state = $customer_data->state != null ? $customer_data->state : "";
                            $branche->postal_code = "";
                            $branche->save();
                        }
                    }
                }

                // Create account user
                if (Auth::user()->id_plan == $this->PLAN_BUSINESS) {
                    $priceListIsCustomerPortal = PriceList::find($quotation->id_price_list);
                    if ($priceListIsCustomerPortal->customer_portal == 1) {
                        $customerUser = customer::find($quotation->id_customer);
                        $nameUser = $customerUser->name;
                        $email = $request->get('email');
                        if ($customerUser->user_id == null) {
                            //Create new user
                            $this->createUser($nameUser, $email, $quotation->id_customer, $customerUser->cellphone);
                        }
                    }
                }

                // Inspection
                $inspectionQuote = $request->get('inspection');
                if ($inspectionQuote != 0) {
                    $inspection = service_order::where('id_quotation', $inspectionQuote)->first();
                    if ($inspection) {
                        $inspection->id_quotation = $quotation->id;
                        $inspection->fake = 0;
                        $inspection->save();
                        $quotationFake = quotation::find($inspectionQuote);
                        $quotation->id_quotation = $quotationFake->id_quotation;
                        $quotation->save();
                        DB::table('quotations')->where('id', $inspectionQuote)->delete();
                    }
                }

                DB::commit();

                return response()->json([
                    'code' => 201,
                    'message' => 'Datos Guardados Correctamente'
                ]);
            }

        } catch (Exception $th) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => "Algo salio mal, intenta de nuevo."
            ]);
        }

    }

    /**
     * @param $customer
     * @param $email
     * @param $customerId
     * @param $cellphone
     */
    public function createUser($customer, $email, $customerId, $cellphone)
    {
        $password = "PestWareApp2021@";
        $passwordEncrypt = bcrypt($password);

        $user = User::create([
            'name' => $customer,
            'email' => $email,
            'cellphone' => $cellphone,
            'companie' => Auth::user()->companie,
            'password' => $passwordEncrypt
        ]);

        if ($user) {
            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => 3
            ]);

            $customer = customer::find($customerId);
            $customer->user_id = $user->id;
            $customer->save();
            // Add default documents for portal MIP
            CommonCustomer::createDefaultDocuments($customerId);
        }

    }

    /**
     * @param Request $request
     * @description Actualiza una cotización personalizada.
     */
    public function update(Request $request){

        try {
            $idQuotation = $request->get('id_quotation');
            $customerId = $request->get('customerId');
            $idUser = Auth::id();

            //Actualizar Cliente -> Tabla customers
            $customer = customer::find($customerId);
            $customer->establishment_id = $request->get('establishment');
            $customer->establishment_name = $request->get('establishmentName');
            $customer->cellphone_main = $request->get('cellphoneMain');
            $customer->colony = $request->get('colony');
            $customer->municipality = $request->get('municipality');
            $customer->source_origin_id = $request->get('sourceOrigin');
            $customer->save();

            //Actualizar los datos conocidos en la tabla customer_datas
            $customer_data = customer_data::where('customer_id', $customerId)->first();
            $customer_data->email = $request->get('email');
            $customer_data->save();

            $idPriceListFind = PriceList::where('establishment_id', $request->get('establishment'))
                ->where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('status', 1)
                ->first();

            if (!$idPriceListFind) {
                DB::rollBack();
                return response()->json([
                    'code' => 500,
                    'message' => "No existe ninguna lista de precio para el servicio seleccionado."
                ]);
            }

            //Actualizar la Cotizacion
            $quotation = quotation::find($idQuotation);
            $beforeTotal = $quotation->total;
            $quotation->total = $request->get('total');
            $quotation->price = $request->get('price');
            $quotation->description = $request->get('description');
            $quotation->user_id = $idUser;
            $quotation->establishment_id = $request->get('establishment');
            $quotation->total = $request->get('total');
            $quotation->price = $request->get('price');
            $quotation->messenger = $request->get('messengerCustom');
            $quotation->pesticide_product = $request->get('pesticide');
            $quotation->id_price_list = $idPriceListFind->id;
            $quotation->description = $request->get('description');
            $quotation->save();

            //Actualiza precio en orden de servicio.
            $serviceOrder = DB::table('service_orders')
                ->where('id_quotation', $quotation->id)
                ->where('total', $beforeTotal)
                ->first();
            if ($serviceOrder) {
                $updateOrder = service_order::find($serviceOrder->id);
                $updateOrder->total = $quotation->total;
                $updateOrder->save();
            }

            //Guardar plagas asociados al id de la cotización.
            $contador = count($request->get('plague'));
            $plagues = [];

            for ($i = 0; $i < $contador; $i++) {
                $separador = explode(",", $request->get('plague')[$i]);
                $idPlague = $separador[0];

                array_push($plagues, $idPlague);
            }
            $quotation->plague_type()->sync($plagues);

            //Actualizar las cotizaciones personalizadas
            DB::table('custom_quotes')->where('id_quotation', $idQuotation)->delete();
            DB::table('customer_branches')->where('customer_id', $quotation->id_customer)->where('id_quotation', $idQuotation)->delete();

            $json = $request->input('arrayConcepts');
            $array = json_decode($json);
            $branchId = 0;

            foreach($array as $obj){

                $concept = $obj->concept;
                $quantity = $obj->quantity;
                $frequency = $obj->frequency;
                $contract = $obj->contract;
                $price = $obj->price;
                $typeId = $obj->typeId;
                $typeServiceId = $obj->typeServiceId;

                if ($obj->typeId == 2) {
                    $frequency = 1;
                    $subtotal = $obj->quantity * $obj->price * $obj->contract;
                } else {
                    if ($typeServiceId != $this->allDays) $subtotal = $obj->quantity * $obj->frequency * $obj->contract * $obj->price;
                    if ($typeServiceId == $this->bimonthly) $subtotal = $subtotal / 2;
                    if ($typeServiceId == $this->quarterly) $subtotal = $subtotal / 3;
                    if ($typeServiceId == $this->quarter) $subtotal = $subtotal / 4;
                    if ($typeServiceId == $this->biannual) $subtotal = $subtotal / 6;
                    if ($typeServiceId == $this->allDays) $subtotal = $obj->quantity * $obj->contract * $obj->price;
                }

                $customQuote = new custom_quote;
                $customQuote->id_quotation = $idQuotation;
                $customQuote->concept = $concept;
                $customQuote->quantity = $quantity;
                $customQuote->frecuency_month = $frequency;
                $customQuote->term_month = $contract;
                $customQuote->unit_price = $price;
                $customQuote->subtotal = $subtotal;
                $customQuote->id_type_concepts = $typeId;
                $customQuote->id_type_service = $typeServiceId;
                $customQuote->save();

                if ($obj->typeId != 2) {
                    $arrayBranches = array_fill(0, $obj->quantity, '');
                    foreach ($arrayBranches as $item) {
                        $branche = new customer_branche;
                        $branche->branch_id = ++$branchId;
                        $branche->customer_id = $quotation->id_customer;
                        $branche->id_quotation = $quotation->id;
                        $branche->name = $obj->concept;
                        $branche->type = $obj->concept;
                        $branche->phone = $request->get('cellphone');
                        $branche->manager = $customer->establishment_name;
                        $branche->email = $customer_data->email;
                        $branche->address = $customer_data->address != null ? $customer_data->address : "";
                        $branche->address_number = $customer_data->address_number != null ? $customer_data->address_number : "";
                        $branche->colony = $customer->colony != null ? $customer->colony : "";
                        $branche->municipality = $customer->municipality != null ? $customer->municipality : "";
                        $branche->state = $customer_data->state != null ? $customer_data->state : "";
                        $branche->postal_code = "";
                        $branche->save();
                    }
                }
            }
            return Response::json([
                'code' => 200,
                'message' => 'Cotización actualizada.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal. Intenta de nuevo.'
            ]);
        }

    }

    public function delete($id, $idCustom){
        $custom_quote = custom_quote::findOrFail($idCustom);
        $quote = quotation::findOrFail($id);

        $price = DB::table('quotations')->select('price')->where('id', $id)->first()->price;
        $priceCustom = DB::table('custom_quotes')->select('subtotal')->where('id', $idCustom)->first()->subtotal;
        $idDiscount = DB::table('discount_quotation')->select('discount_id')->where('quotation_id', $id)->first()->discount_id;
        $percentage = DB::table('discounts')->select('percentage')->where('id', $idDiscount)->first()->percentage;
        $discount = $percentage/100;
        $subtotal = $price - $priceCustom;
        $quote->price = $subtotal; //actualiza el precio
        $discountP = $discount * $subtotal;
        $subtotal = $subtotal - $discountP;
        $quote->total = $subtotal;
        $quote->save();

        $custom_quote->delete();
        return redirect()->route('edit_quotation', $id);
    }

    /**
     * @param $id de la cotización
     * @return mixed
     * @author Yomira Martínez
     * Genera el PDF de la cotización.
     */

     public function PDFCustom($id)
     {
         if (strlen($id) > 4) $id = SecurityController::decodeId($id);
         $quotationId = $id;
         $quotation = CommonQuotation::getQuotationDataById($quotationId);
         $quotationPdf = QuotationPdf::build($quotationId,$quotation);

         if ($quotationPdf['imagen']->id == 333) {
             if ($quotationPdf['pricesList']->pdf != null) {
                 $ruta = storage_path().'/app/'.$quotationPdf['pricesList']->pdf;
                 PDF::loadView('vendor.adminlte.register.PDF.Customized.pdfCustom', $quotationPdf)
                     ->save(storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf');
                 $pdf1 = storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf';
                 $documentos = [$ruta,$pdf1];
                 $combinador = new Merger;

                 foreach ($documentos as $documento) {
                     $combinador->addFile($documento);
                 }

                 $salida = $combinador->merge();
                 $nombre = 'Cotización'.' '.$quotation->id_quotation.'.pdf';

                 header("Content-type:application/pdf");
                 header("Content-Disposition:attachment;filename=$nombre");
                 echo $salida;
                 exit;
             }
             else {
                 $pdf = PDF::loadView('vendor.adminlte.register.PDF.Customized.pdfCustom', $quotationPdf);
                 return $pdf->stream();
             }
         }
         else {
             if ($quotationPdf['pricesList']->pdf != null) {
                 $ruta = storage_path().'/app/'.$quotationPdf['pricesList']->pdf;
                 PDF::loadView('vendor.adminlte.register.PDF.Customized.customPDFV2', $quotationPdf)
                     ->save(storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf');
                 $pdf1 = storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf';
                 $documentos = [$ruta,$pdf1];
                 $combinador = new Merger;

                 foreach ($documentos as $documento) {
                     $combinador->addFile($documento);
                 }

                 $salida = $combinador->merge();
                 $nombre = 'Cotización'.' '.$quotation->id_quotation.'.pdf';

                 header("Content-type:application/pdf");
                 header("Content-Disposition:attachment;filename=$nombre");
                 echo $salida;
                 exit;
             }
             else {
                 $pdf = PDF::loadView('vendor.adminlte.register.PDF.Customized.customPDFV2', $quotationPdf);
                 return $pdf->stream();
             }
         }
    }
    public function customMessage($id)
    {
        // TODO: Delete Function
    }
    public function customGenerate(Request $request)
    {
        // TODO: Delete Function
    }

    public function convertToMoney($quantity) {
         return Response::json([
             'quantity' => CommonFormats::convertToMoney($quantity),
             'symbol' => CommonCompany::getSymbolByCountry()
         ]);
    }
}
