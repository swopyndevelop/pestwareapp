<?php

namespace App\Http\Controllers\Quotations;

use App\Common\CommonLabel;
use App\customer;
use App\customer_data;
use App\DescriptionCustomQuote;
use App\discount;
use App\discount_quotation;
use App\employee;
use App\establishment_type;
use App\event;
use App\extra;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Catalogs\CommonBranch;
use App\Http\Controllers\CustomServices\CommonCustomServices;
use App\Http\Controllers\Security\SecurityController;
use App\Http\Controllers\Services\CommonCalendar;
use App\JobCenterSession;
use App\plague_type;
use App\PriceList;
use App\profile_job_center;
use App\quotation;
use App\service_order;
use App\source_origin;
use App\treeJobCenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InspectionController extends Controller
{
    public function index()
    {
        $inspections = event::join('service_orders as so', 'events.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation','q.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'c.id','cd.customer_id')
            ->join('employees as em','events.id_employee','em.id')
            ->join('users as u','q.user_id','u.id')
            ->where('so.inspection', 1)
            ->where('so.companie', Auth::user()->companie)
            ->select('so.id','so.id_service_order as folio', 'events.id_status as status', 'events.initial_date',
                'events.initial_hour', 'u.name as agent', 'c.name as customer_name', 'c.cellphone', 'c.colony', 'c.municipality',
                'c.establishment_name', 'c.cellphone_main', 'em.name as technician')
            ->orderBy('so.created_at', 'desc')
            ->paginate(10);

        $date = Carbon::now()->toDateString();
        $folio = CommonQuotation::getFolioQuote();
        $folioExplode = explode('-', $folio);
        $folioInspection = 'I-' . $folioExplode[1];

        $jobcenter = 0;
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $sourceOrigin = source_origin::where('profile_job_center_id', $dataJobCenter[2])->orderBy('name', 'asc')->get();
        $commonLabels = new CommonLabel();
        $labels = $commonLabels->getLabelsByJobCenter($dataJobCenter[2]);

        return view('vendor.adminlte.register.indexInspection')->with([
            'inspections' => $inspections,
            'date' => $date,
            'folioInspection' => $folioInspection,
            'sources' => $sourceOrigin,
            'labels' => $labels
        ]);
    }

    public function store(Request $request){
        try {
            DB::beginTransaction();
            $jobcenter = 0;
            $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
            $cellphone = $request->get('cellphone');

            $customerIdFind = $request->get('customerIdFind');
            if($customerIdFind != 0){
                $client = DB::table('customers as c')
                    ->where('id', $customerIdFind)
                    ->where('companie', Auth::user()->companie)
                    ->first();
            } else {
                $client = DB::table('customers as c')
                    ->where('cellphone', $cellphone)
                    ->where('companie', Auth::user()->companie)
                    ->first();
            }

            // Valida si el cliente intenta usar el teléfono para más de un cliente.
            if (!empty($client)) {
                if ($client->name != $request->get('name') && $request->get('isConfirmedCustomer') == "false") {
                    return response()->json([
                        'code' => 304,
                        'message' => "El número " . $client->cellphone . " pertenece al cliente " . $client->name . ", si continuas guardando la cotización remplazarás los datos del cliente por los de esta cotización."
                    ]);
                }
            }

            if (empty($client)){
                //Agregar Cliente
                $customer = new customer;
                $customer->name = $request->get('name');
                $customer->colony = $request->get('colony');
                $customer->establishment_name = $request->get('establishmentName');
                if ($request->get('cellphone') == 0){
                    $customer->cellphone = null;
                }
                else{
                    $customer->cellphone = $request->get('cellphone');
                }
                $customer->cellphone_main = $request->get('cellphoneMain');
                $customer->establishment_id = 6687;
                $customer->municipality = $request->get('municipality');
                $customer->source_origin_id = $request->get('sourceOrigin');
                $customer->id_profile_job_center = $dataJobCenter[2];
                $customer->companie = Auth::user()->companie;
                $customer->save();

                //Agregar los datos conocidos en la tabla customer_datas
                $customer_data = new customer_data;
                $customer_data->customer_id = $customer->id;
                $customer_data->email = $request->get('email');
                $customer_data->phone_number = $request->get('cellphone');
                $customer_data->save();
            }else{
                // Actualizar Cliente
                $customer = customer::find($client->id);
                $customer->name = $request->get('name');
                $customer->establishment_name = $request->get('establishmentName');
                if ($request->get('cellphone') == 0){
                    $customer->cellphone = null;
                }
                else{
                    $customer->cellphone = $request->get('cellphone');
                }
                $customer->cellphone_main = $request->get('cellphoneMain');
                $customer->establishment_id = 6687;
                $customer->colony = $request->get('colony');
                $customer->municipality = $request->get('municipality');
                $customer->source_origin_id = $request->get('sourceOrigin');
                $customer->id_profile_job_center = $dataJobCenter[2];
                $customer->save();

                //Actualizar los datos conocidos en la tabla customer_datas
                $customer_data = customer_data::where('customer_id', $client->id)->first();
                $customer_data->email = $request->get('email');
                $customer_data->phone_number = $request->get('cellphone');
                $customer_data->save();
            }

            //Crear la nueva Cotización
            $quotation = new quotation;
            $quotation->id_customer = $customer->id;
            $quotation->construction_measure = 0;
            $quotation->garden_measure = 0;
            $quotation->id_plague_jer = 1;
            $quotation->establishment_id = 6687;
            $quotation->id_status = 10;
            $quotation->total = 0;
            $quotation->price = 0;
            $quotation->price_reinforcement = 0;
            $quotation->user_id = Auth::user()->id;
            $quotation->id_job_center = $dataJobCenter[2];
            $quotation->companie = Auth::user()->companie;
            $quotation->id_extra = 2279;
            $quotation->id_price_list = 5959;
            $quotation->id_quotation = CommonQuotation::getFolioQuote();
            $quotation->save();

            //Asignar descuento y cotización
            $discount = new discount_quotation;
            $discount->quotation_id = $quotation->id;
            $discount->discount_id = 1963;
            $discount->save();

            // Crear Inspección
            $folioExplode = explode('-', $quotation->id_quotation);
            $folioInspection = 'I-' . $folioExplode[1];
            $order = CommonCustomServices::createServiceOrderInspection(
                $folioInspection,
                $quotation->id,
                1676,
                199,
                $quotation,
                $customer,
                ''
            );

            $event = new event;
            $event->title = 'Inspección';
            $event->id_service_order = $order->id;
            $event->id_employee = 672;
            $event->id_job_center = $dataJobCenter[2];
            $event->id_status = 10;
            $event->companie = Auth::user()->companie;
            $event->save();

            DB::commit();
            return \Response::json([
                'code' => 201,
                'message' => 'Inspección Guardada'
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return \Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }

    }
}
