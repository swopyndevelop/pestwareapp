<?php


namespace App\Http\Controllers\Quotations;


use App\customer;
use App\customer_data;
use App\discount_quotation;
use App\Mail\QuotatioMail;
use App\service_order;
use PDF;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Users\CommonUser;
use App\plague_type_quotation;
use App\quotation;
use iio\libmergepdf\Merger;
use Illuminate\Support\Facades\DB;

class CommonQuotation
{
    public static function getQuotationById($quotationId)
    {
        return quotation::find($quotationId);
    }

    public static function getFolioQuote()
    {
        $quote = quotation::where('companie', auth()->user()->companie)->latest()->first();
        if ($quote) {
            if ($quote->id_quotation == 'C-1') {
                return 'C-2';
            } else {
                $folio = explode('-', $quote->id_quotation);
                return 'C-'.++$folio[1];
            }
        } else return 'C-1';
    }

    public static function getFolioQuoteWithCompany($companyId)
    {
        $quote = quotation::where('companie', $companyId)->latest()->first();
        if ($quote) {
            if ($quote->id_quotation == 'C-1') {
                return 'C-2';
            } else {
                $folio = explode('-', $quote->id_quotation);
                return 'C-'.++$folio[1];
            }
        } else return 'C-1';
    }

    public static function getFolioTracing($quotationId, $lastFolio)
    {
        $tracing = service_order::where('companie', auth()->user()->companie)
            ->where('tracing', 1)
            ->where('id_quotation', $quotationId)
            ->latest()->first();

        if ($tracing) {
            $folio = explode('-', $tracing->id_service_order);
            return 'S-' . $lastFolio . '-' . ++$folio[2];
        } else return 'S-' . $lastFolio . '-1';
    }

    public static function getFolioOrderArea($quotationId, $lastFolio)
    {
        $order = service_order::where('companie', auth()->user()->companie)
            ->where('tracing', 1)
            ->where('id_quotation', $quotationId)
            ->latest()->first();

        if ($order) {
            $folio = explode('-', $order->id_service_order);
            return 'OS-' . $lastFolio . '-' . ++$folio[2];
        } else return 'OS-' . $lastFolio . '-1';
    }

    public static function getQuotationDataById($quotationId)
    {
         return DB::table('quotations as q')
            ->join('customers as c','c.id','q.id_customer')
            ->join('establishment_types as e','e.id','c.establishment_id')
            ->join('customer_datas as cd','cd.customer_id','c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->select('q.id', 'q.created_at as date', 'q.total', 'q.construction_measure', 'q.garden_measure',
                'q.id_status', 'c.name', 'c.establishment_name', 'c.cellphone', 'e.id as e_id', 'e.name as e_name',
                'cd.address', 'cd.reference_address', 'cd.email', 'cd.phone_number', 'c.municipality',
                'q.id_quotation', 'q.id_plague_jer as id_plague_jer', 'q.price', 'q.user_id', 'q.id_extra', 'q.id_job_center',
                'q.companie', 'q.id_price_list', 'q.description','q.pesticide_product','cd.billing')
            ->where('q.id', $quotationId)->first();
    }

    public static function getDiscountByQuot($quotationId)
    {
        return discount_quotation::where('quotation_id', $quotationId)
            ->join('discounts', 'discount_quotation.discount_id', 'discounts.id')
            ->select('discounts.id', 'discounts.percentage')
            ->first();
    }

    public static function getPlaguesByQuot($quotationId)
    {
        return plague_type_quotation::where('quotation_id', $quotationId)->get();
    }

    public static function getCustomerById($customerId)
    {
        return customer::find($customerId)->first();
    }

    public static function getCustomerDataById($customerId)
    {
        return customer_data::where('customer_id', $customerId)->first();
    }

    public static function createQuotPDF($quotationId)
    {
        //TODO: Delete Function
    }
}