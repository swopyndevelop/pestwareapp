<?php


namespace App\Http\Controllers\Quotations;

use App\companie;
use App\Country;
use App\establishment_type;
use App\extra;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\ReportsPDF\QuotationPdf;
use App\Http\Controllers\Users\CommonUser;
use App\Mail\MailGenerate;
use App\Mail\QuotatioMail;
use App\profile_job_center;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use iio\libmergepdf\Merger;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendMailQuotation
{
    private $quotationId;

    /**
     * SendMailQuotation constructor.
     * @param $quotationId
     */
    public function __construct($quotationId)
    {
        $this->quotationId = $quotationId;
    }

    public function sendMailQuotById($emails)
    {
        $quotationId = $this->quotationId;
        $quotation = CommonQuotation::getQuotationDataById($quotationId);
        if ($quotation != null) {
            // Variables PDF
            $quotationPdf = QuotationPdf::build($quotationId, $quotation);

            if($quotationPdf['imagen']->id == 333){
                if ($quotationPdf['pricesList']->pdf == null) {
                    // Quotation Custom withOut Cover
                    $pdf = PDF::loadView('vendor.adminlte.register.PDF.Customized.pdfCustom', $quotationPdf);
                    config(['mail.from.name' => $quotationPdf['imagen']->name]);

                    foreach ($emails as $email) {
                        $result = filter_var($email, FILTER_VALIDATE_EMAIL);
                        if ($result != false) {
                            config(['mail.from.name' => $quotationPdf['imagen']->name]);
                            Mail::to($email)->send(new QuotatioMail($email, $quotationId, $pdf->output(), $quotationPdf['imagen']->id, 2,
                                $quotationPdf['jobCenterProfile']->whatsapp_personal, $quotationPdf['jobCenterProfile']->messenger_personal, $quotationPdf['jobCenterProfile']->id));
                        }
                    }

                }

                else if ($quotationPdf['pricesList']->pdf != null) {
                    // Quotation List Price with Cover
                    PDF::loadView('vendor.adminlte.register.PDF.Customized.pdfCustom', $quotationPdf)
                        ->save(storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf');

                    $pdf1 = storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf';

                    $documentos = [$quotationPdf['ruta'],$pdf1];
                    $combinador = new Merger;
                    foreach ($documentos as $documento) {
                        $combinador->addFile($documento);
                    }
                    $salida = $combinador->merge();

                    foreach ($emails as $email) {
                        $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
                        if($result != false){
                            config(['mail.from.name' => $quotationPdf['imagen']->name]);
                            Mail::to($email)->send(new QuotatioMail($email, $quotationId, $salida, $quotationPdf['imagen']->id, 2,
                                $quotationPdf['jobCenterProfile']->whatsapp_personal, $quotationPdf['jobCenterProfile']->messenger_personal, $quotationPdf['jobCenterProfile']->id));
                        }
                    }
                }

            }

            else {
                if ($quotationPdf['pricesList']->pdf == null) {
                    // Quotation with Cover
                    if ($quotation->description != null) {
                        // Quotation PriceList with and without Cover
                        $pdf = PDF::loadView('vendor.adminlte.register.PDF.Customized.customPDFV2', $quotationPdf);
                        config(['mail.from.name' => $quotationPdf['imagen']->name]);

                        foreach ($emails as $email) {
                            $result = filter_var($email, FILTER_VALIDATE_EMAIL);
                            if ($result != false) {
                                Mail::to($email)->send(new QuotatioMail($email, $quotationId, $pdf->output(), $quotationPdf['imagen']->id, 1,
                                    $quotationPdf['jobCenterProfile']->whatsapp_personal, $quotationPdf['jobCenterProfile']->messenger_personal, $quotationPdf['jobCenterProfile']->id));
                            }
                        }
                    } else {
                        // Quotation Custom withOut Cover
                        $pdf = PDF::loadView('vendor.adminlte.register.PDF.Customized.pdf', $quotationPdf);
                        config(['mail.from.name' => $quotationPdf['imagen']->name]);

                        foreach ($emails as $email) {
                            $result = filter_var($email, FILTER_VALIDATE_EMAIL);
                            if ($result != false) {
                                config(['mail.from.name' => $quotationPdf['imagen']->name]);
                                Mail::to($email)->send(new QuotatioMail($email, $quotationId, $pdf->output(), $quotationPdf['imagen']->id, 2,
                                    $quotationPdf['jobCenterProfile']->whatsapp_personal, $quotationPdf['jobCenterProfile']->messenger_personal, $quotationPdf['jobCenterProfile']->id));
                            }
                        }
                    }
                }

                else if ($quotation->description == null) {
                    // Quotation List Price with Cover
                    PDF::loadView('vendor.adminlte.register.PDF.Customized.pdf', $quotationPdf)
                        ->save(storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf');

                    $pdf1 = storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf';

                    $documentos = [$quotationPdf['ruta'],$pdf1];
                    $combinador = new Merger;
                    foreach ($documentos as $documento) {
                        $combinador->addFile($documento);
                    }
                    $salida = $combinador->merge();

                    foreach ($emails as $email) {
                        $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
                        if($result != false){
                            config(['mail.from.name' => $quotationPdf['imagen']->name]);
                            Mail::to($email)->send(new QuotatioMail($email, $quotationId, $salida, $quotationPdf['imagen']->id, 2,
                                $quotationPdf['jobCenterProfile']->whatsapp_personal, $quotationPdf['jobCenterProfile']->messenger_personal, $quotationPdf['jobCenterProfile']->id));
                        }
                    }

                }

                else{
                    // Quotation Custom with Cover
                    PDF::loadView('vendor.adminlte.register.PDF.Customized.customPDFV2', $quotationPdf)
                        ->save(storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf');

                    $pdf1 = storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf';

                    $documentos = [$quotationPdf['ruta'],$pdf1];
                    $combinador = new Merger;
                    foreach ($documentos as $documento) {
                        $combinador->addFile($documento);
                    }
                    $salida = $combinador->merge();

                    config(['mail.from.name' => $quotationPdf['imagen']->name]);

                    foreach ($emails as $email) {
                        $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
                        if($result != false){
                            Mail::to($email)->send(new QuotatioMail($email, $quotationId, $salida, $quotationPdf['imagen']->id, 1,
                                $quotationPdf['jobCenterProfile']->whatsapp_personal, $quotationPdf['jobCenterProfile']->messenger_personal, $quotationPdf['jobCenterProfile']->id));
                        }
                    }

                }
            }

        }
    }
}