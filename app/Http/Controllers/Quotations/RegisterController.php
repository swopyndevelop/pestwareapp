<?php

namespace App\Http\Controllers\Quotations;

use App\canceled_quotation;
use App\Common\CommonLabel;
use App\companie;
use App\Country;
use App\customer;
use App\customer_data;
use App\DescriptionCustomQuote;
use App\discount;
use App\discount_quotation;
use App\employee;
use App\establishment_type;
use App\event;
use App\extra;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ReportsPDF\QuotationPdf;
use App\Http\Controllers\Security\SecurityController;
use App\Http\Controllers\Users\CommonUser;
use App\JobCenterSession;
use App\Jobs\UpdateMipCustomer;
use App\Mail\QuotatioMail;
use App\plague_type;
use App\plague_type_quotation;
use App\PriceList;
use App\profile_job_center;
use App\quotation;
use App\rejected_quotation;
use App\service_order;
use App\source_origin;
use App\tracing;
use App\treeJobCenter;
use Carbon\Carbon;
use iio\libmergepdf\Exception;
use iio\libmergepdf\Merger;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Mail;
use PDF;
use Validator;


/**
 * @author Alberto Martínez || Yomira Martínez
 * @version 13/01/2019
 * Controller Register, CRUD para módulo de registro.
 */
class RegisterController extends Controller
{
    #region CRUD

    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    // Crear variable de sesion para guardar idJobcenter seleccionado.
    /*public function __construct(){

        if (!\Session::has('jobCenterQuotation')) \Session::put('jobCenterQuotation', 0);

    }*/

    protected $rules =
        [
            'name' => 'required',
            'establishment' => 'required',
            'plague' => 'required',
            'price' => 'required'
        ];

    protected $rulesPrice =
        [
            'plague' => 'required'
        ];

    public function fetchCellphone(Request $request){
        if ($request->get('query')){
            $query = $request->get('query');
            $jobCenterSelect = $request->get('jobCenterSelect');
            $data = DB::table('customers')
                ->where('cellphone', 'like', '%' . $query . '%')
                ->where('id_profile_job_center', $jobCenterSelect)->get();
            $output = '<ul class="dropdown-menu" id="listClients" style="display:block; position:absolute;">';
            foreach ($data as $row){
                $output .= '<li class="cellphone" id="getdataClient"><a href="#">' . $row->cellphone . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }

    public function fetchCustomerName(Request $request){
        if ($request->get('query')){
            $query = $request->get('query');
            $jobCenterSelect = $request->get('jobCenterSelect');
            $data = DB::table('customers')
                ->where('name', 'LIKE', "%$query%")
                ->where('id_profile_job_center', $jobCenterSelect)->get();
            $output = '<ul class="dropdown-menu" id="listClientsName" style="display:block; position:absolute;">';
            foreach ($data as $row){
                $output .= '<li class="cellphone" id="getdataClientName"><a href="#">' . $row->name . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }

    public function loadClient(Request $request){
        $cellphone = $request->get('cellphone');
        $jobCenterSelect = $request->get('jobCenterSelect');
        $client = DB::table('customers as c')
            ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
            ->join('source_origins as so','c.source_origin_id', 'so.id')
            ->where('c.cellphone', $cellphone)
            ->where('id_profile_job_center', $jobCenterSelect)
            ->select('c.id', 'c.name', 'c.establishment_name', 'c.cellphone', 'c.cellphone_main', 'c.colony', 'c.municipality',
                'cd.email','so.id as id_source_origin', 'so.name as name_source_origin')
            ->first();
        return response()->json($client);
    }

    public function loadClientByQuotation(Request $request){
        $idQuotation = $request->get('idQuotation');
        $quotation = quotation::find($idQuotation);
        $client = DB::table('customers as c')
            ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
            ->join('source_origins as so','c.source_origin_id', 'so.id')
            ->where('c.id', $quotation->id_customer)
            ->select('c.id', 'c.name', 'c.establishment_name', 'c.cellphone', 'c.cellphone_main', 'c.colony', 'c.municipality',
                'cd.email','so.id as id_source_origin', 'so.name as name_source_origin')
            ->first();
        return response()->json($client);
    }

    public function loadClientName(Request $request){
        $customerName = $request->get('customerName');
        $jobCenterSelect = $request->get('jobCenterSelect');
        $client = DB::table('customers as c')
            ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
            ->join('source_origins as so','c.source_origin_id', 'so.id')
            ->where('c.name', $customerName)
            ->where('id_profile_job_center', $jobCenterSelect)
            ->select('c.id', 'c.name', 'c.establishment_name', 'c.cellphone', 'c.cellphone_main', 'c.colony', 'c.municipality',
                'cd.email','so.id as id_source_origin', 'so.name as name_source_origin')
            ->first();
        return response()->json($client);
    }

    public function index(Request $request, $jobcenter = 0)
    {
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        //Crea y guarda por primera vez las configuraciones del tema en una variable de sesion.
        $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
        if (!\Session::has('theme')) \Session::put('theme', $companie->id_theme);
        if (!\Session::has('logo')) \Session::put('logo', $companie->logo);
        if (!\Session::has('mini_logo')) \Session::put('mini_logo', $companie->mini_logo);
        if (!\Session::has('pdf_logo')) \Session::put('pdf_logo', $companie->pdf_logo);
        if (!\Session::has('pdf_sello')) \Session::put('pdf_sello', $companie->pdf_sello);
        if (!\Session::has('sidebar-collapse')) \Session::put('sidebar-collapse', false);

        $idUser = Auth::id();
        $jobCenterEmployee = 0;
        $jobCenterStatus = 0;
        $idJobCenter = 0;
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        // Filters
        $keyPriceLists = PriceList::where('profile_job_center_id', $profileJobCenter)->get();
        $employees = DB::table('employees')
            ->where('profile_job_center_id', $profileJobCenter)
            ->where('status', '<>', 200)
            ->get();
        $customers = customer::where('id_profile_job_center', $profileJobCenter)->get();
        $address = DB::table('customers as c')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->select('c.id', 'cd.address', 'cd.address_number', 'c.colony')
            ->where('c.id_profile_job_center', $profileJobCenter)
            ->get();

        $date = Carbon::now()->format('d-m-Y'); //Fecha
        $dateTracing = Carbon::now()->format('Y-m-d'); //Fecha
        $tomorrow = new Carbon('tomorrow');
        $plague = plague_type::where('profile_job_center_id', $profileJobCenter)->orderBy('name', 'asc')->get(); //Arreglo para el select de Tipo de Plaga
        $discounts = discount::where('profile_job_center_id', $profileJobCenter)->orderBy('title','asc')->get(); //Arreglo para el select de Descuentos
        $extras = extra::where('profile_job_center_id', $profileJobCenter)->orderBy('name', 'asc')->get(); //Arreglo para el select de Extras
        $sourceOrigin = source_origin::where('profile_job_center_id', $profileJobCenter)->orderBy('name', 'asc')->get(); //Arreglo para el select de Fuente de origen
        $descriptionsForCustomQuotes = DescriptionCustomQuote::where('profile_job_center_id', $profileJobCenter)->get();
        $establishment = establishment_type::where('profile_job_center_id', $profileJobCenter)->orderBy('name', 'asc')->get(); //Arreglo para select de Tipo de Servicio
        $quotation_id = DB::table('quotations')->select('id')->orderBy('id', 'DESC')->first();//Obtenie el ultimo ID; de la tabla de cotizaciones
        $inspections = event::join('service_orders as so', 'events.id_service_order', 'so.id')
            ->where('so.fake', 1)
            ->where('so.id_job_center', $profileJobCenter)
            ->where('events.id_status', 4)
            ->select('so.*')
            ->get();

        $id_q = CommonQuotation::getFolioQuote();
        $commonLabels = new CommonLabel();
        $labels = $commonLabels->getLabelsByJobCenter($profileJobCenter);

        if (!$request->initialDateQuotation && $request->search == null) {
            $companie = DB::table('users')->select('companie')->where('id',auth()->user()->id)->first();
            //Consulta para llenar la tabla
            $quotations = DB::table('quotations as q')
                ->join('customers as c', 'c.id', 'q.id_customer')
                ->join('establishment_types as e', 'e.id', 'c.establishment_id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('statuses as st', 'q.id_status', 'st.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('users as u','q.user_id','u.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->select('q.id', 'q.id_quotation', 'q.created_at as date', 'q.construction_measure', 'q.garden_measure',
                    'q.id_status', 'q.messenger', 'st.name as status', 'pt.plague_key', 'c.name', 'c.establishment_name',
                    'c.cellphone', 'c.cellphone_main', 'q.id_plague_jer', 'q.establishment_id', 'q.description_whatsapp',
                    'e.id as e_id', 'e.name as e_name', 'cd.address', 'cd.reference_address', 'cd.email', 'cd.phone_number',
                    'c.municipality', 'q.total', 'q.price', 'd.percentage', 'ex.amount', 'q.id_extra','u.name as agente',
                    'q.whatsapp','q.companie', 'pl.key', 'c.colony', 'q.id_price_list', 'q.description','q.approved',
                    'q.email as send_email', 'pl.name as name_list_price')
                ->where('q.companie',$companie->companie)
                ->where('id_job_center', $profileJobCenter)
                ->orderby('q.id', 'DESC')
                ->paginate(10);

            foreach ($quotations as $q) {
                $quotations->map(function($q){
                    if ($q->id_status == 3) {
                        $motive = DB::table('rejected_quotations')->where('id_quotation', $q->id)->first();
                        $q->motiveRejected = $motive->reason . ", " . $motive->commentary;
                    }
                    $description = PriceList::find($q->id_price_list)->description;
                    $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
                    $countryCode = DB::table('countries')
                        ->where('id', $companie->id_code_country)->first();
                    if($q->description == null){
                        $q->urlWhatsapp = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" . $description . "%0d%0dCotización: https://pestwareapp.com/quotation/" . SecurityController::encodeId($q->id) . "/PDF";
                        $q->urlWhatsappShow = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . $q->cellphone . "&text=";
                    }else{
                        $q->urlWhatsapp = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" . $description . "%0d%0dCotización: https://pestwareapp.com/quotation/" . SecurityController::encodeId($q->id) . "/custom/PDF";
                        $q->urlWhatsappShow = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . $q->cellphone . "&text=";
                    }

                    // Validate quotation type
                    $cont = DB::table('custom_quotes')->where('id_quotation', $q->id)->count();
                    if ($cont > 0) $q->isCustomQuotation = 1;
                    else $q->isCustomQuotation = 0;

                    $serviceOrders = DB::table('service_orders')->where('id_quotation',$q->id)->get();
                    $q->serviceOrdersCount = $serviceOrders->count();

                    // Inspection
                    $inspection = service_order::where('id_quotation', $q->id)->where('fake', 1)->first();
                    if ($inspection) $q->fake = 1;
                    else $q->fake = 0;
                });
            }

        }else{

            //Validate plan free
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                return redirect()->route('index_register');
            }
            // Filters
            $idQuot = $request->get('search');
            $initialDateCreated = $request->get('initialDateQuotation');
            $finalDateCreated = $request->get('finalDateQuotation');
            $agentName = $request->get('agentName');
            $customerName = $request->get('customerName');
            $customerColony = $request->get('customerColony');
            $key = $request->get('key');
            $payment = $request->get('payment');
            $status = $request->get('status');

            $companie = DB::table('users')->select('companie')->where('id',auth()->user()->id)->first();
            //Consulta para llenar la tabla
            $quotation = DB::table('quotations as q')
                ->join('customers as c', 'c.id', 'q.id_customer')
                ->join('establishment_types as e', 'e.id', 'c.establishment_id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('statuses as st', 'q.id_status', 'st.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('users as u','q.user_id','u.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->select('q.id', 'q.id_quotation', 'q.created_at as date', 'q.construction_measure', 'q.garden_measure',
                    'q.id_status', 'q.messenger', 'st.name as status', 'pt.plague_key', 'c.name', 'c.establishment_name',
                    'c.cellphone', 'c.cellphone_main','q.id_plague_jer', 'q.establishment_id', 'q.description_whatsapp',
                    'e.id as e_id', 'e.name as e_name', 'cd.address', 'cd.reference_address', 'cd.email',
                    'cd.phone_number', 'c.municipality', 'q.total', 'q.price', 'd.percentage', 'ex.amount', 'q.id_extra',
                    'u.name as agente', 'q.whatsapp', 'pl.key', 'c.colony', 'q.id_price_list', 'q.description','q.approved',
                    'q.email as send_email', 'pl.name as name_list_price')
                ->where('id_job_center', $profileJobCenter);

            if ($idQuot != null) $quotation->where('q.id_quotation', 'like', '%'. $idQuot .'%');
            else {
                if ($initialDateCreated != "null" && $finalDateCreated != "null") {
                    if ($status == 1) {
                        $quotation = $quotation->join('tracings as t', 'q.id', 't.id_quotation')
                            ->whereDate('t.tracing_date', '>=', $initialDateCreated)
                            ->whereDate('t.tracing_date', '<=', $finalDateCreated);

                    } else {
                        $quotation->whereDate('q.created_at', '>=', $initialDateCreated)
                            ->whereDate('q.created_at', '<=', $finalDateCreated);
                    }
                }
                if ($agentName != 0) $quotation->where('q.user_id', $agentName);
                if ($customerName != 0) $quotation->where('c.id', $customerName);
                if ($customerColony != 0) $quotation->where('c.id', $customerColony);
                if ($key != 0) $quotation->where('pl.id', $key);
                if ($payment != 0) {
                    $separador = explode(",", $payment);
                    $id = $separador[0];
                    $type = $separador[1];
                    if ($type == 1) $quotation->where('d.id', $payment);
                    elseif ($type == 2) $quotation->where('ex.id', $payment);
                }
                if ($status != 0) {
                    if ($status == 14) {
                        $quotation->where('q.approved', 1)
                            ->whereNotIn('q.id_status', [4]);
                    }
                    else {
                        if ($status == 10) $quotation->whereNotIn('q.id_status', [2,3,4]);
                        else $quotation->where('q.id_status', $status);
                    }
                }
            }

            $quotations = $quotation->orderby('q.id', 'DESC')->paginate(10)->appends([
                'initialDateQuotation' => $request->initialDateQuotation,
                'finalDateQuotation' => $request->finalDateQuotation,
                'customerName' => $request->customerName,
                'customerColony' => $request->customerColony,
                'key' => $request->key,
                'payment' => $request->payment,
                'status' => $request->status,
            ]);

            foreach ($quotations as $q) {
                $quotations->map(function($q){
                    if ($q->id_status == 2) {
                        $motive = DB::table('canceled_quotations')->where('id_quotation', $q->id)->first();
                        $q->motive = $motive->reason . ", " . $motive->commentary;
                    }
                    if ($q->id_status == 3) {
                        $motive = DB::table('rejected_quotations')->where('id_quotation', $q->id)->first();
                        $q->motiveRejected = $motive->reason . ", " . $motive->commentary;
                    }
                    $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
                    $countryCode = DB::table('countries')
                        ->where('id', $companie->id_code_country)->first();

                    $description = PriceList::find($q->id_price_list)->description;

                    if($q->description == null){
                        $q->urlWhatsapp = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" . $description . "%0d%0dCotización: https://pestwareapp.com/quotation/" . SecurityController::encodeId($q->id) . "/PDF";
                        $q->urlWhatsappShow = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . $q->cellphone . "&text=";
                    }else{
                        $q->urlWhatsapp = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" . $description . "%0d%0dCotización: https://pestwareapp.com/quotation/" . SecurityController::encodeId($q->id)  . "/custom/PDF";
                        $q->urlWhatsappShow = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . $q->cellphone . "&text=";
                    }

                    // Validate quotation type
                    $cont = DB::table('custom_quotes')->where('id_quotation', $q->id)->count();
                    if ($cont > 0) $q->isCustomQuotation = 1;
                    else $q->isCustomQuotation = 0;

                    $serviceOrders = DB::table('service_orders')->where('id_quotation',$q->id)->get();
                    $q->serviceOrdersCount = $serviceOrders->count();

                    // Inspection
                    $inspection = service_order::where('id_quotation', $q->id)->where('fake', 1)->first();
                    if ($inspection) $q->fake = 1;
                    else $q->fake = 0;

                });
            }
        }

        //Consulta para obtener los eventos del calendario
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->select('ev.id','ev.title','ev.initial_hour','ev.final_hour','ev.initial_date',
                'ev.final_date', 'em.color', 'ev.id_status')
            ->where('ev.id_job_center', $profileJobCenter)
            ->where('ev.id_status', '!=', 3)
            ->get();

        $quotationsTracing = DB::table('quotations as q')->join('tracings as t', 'q.id', 't.id_quotation')->groupBy('q.id')->get();
        $array = [];
        foreach ($quotationsTracing as $qt){
            $tracing = DB::table('tracings')->orderBy('created_at', 'DESC')->where('id_quotation', $qt->id_quotation)->first();
            if ($dateTracing == $tracing->tracing_date){
                $color = 'green';
            }elseif ($tracing->tracing_date == $tomorrow->format('Y-m-d')){
                $color = 'yellow';
            }elseif($dateTracing > $tracing->tracing_date){
                $color = 'red';
            }else{
                $color = 'normal';
            }
            $tracingsQuotations = ['id_quotation' => $qt->id_quotation, 'tracing_hour' => $tracing->tracing_hour, 'tracing_date' => $tracing->tracing_date, 'color' => $color];
            array_push($array, $tracingsQuotations);
        }
        $tracings = collect($array);

        //Numero de cotizaciones que hay
        $initMonthDate = Carbon::now()->startOfMonth()->toDateString();
        $nowDate = Carbon::now()->toDateString();
        $initialDateCreated = $request->get('initialDateQuotation');
        $finalDateCreated = $request->get('finalDateQuotation');

        if ($initialDateCreated != null && $finalDateCreated != null) {
            $initMonthDate = $initialDateCreated;
            $nowDate = $finalDateCreated;
        }

        $totalQuotations = DB::table('quotations as q')
            ->where('q.id_job_center', $profileJobCenter)
            ->whereDate('q.created_at', '>=', $initMonthDate)
            ->whereDate('q.created_at', '<=', $nowDate)
            ->count();

        //Numero de programados que hay
        $totalProgrammed = DB::table('quotations as q')
            ->where('id_job_center', $profileJobCenter)
            ->where('q.companie',$companie->companie)
            ->whereDate('q.created_at', '>=', $initMonthDate)
            ->whereDate('q.created_at', '<=', $nowDate)
            ->where('id_status', 4)->count();

        if ($totalQuotations != 0) {
            $percentage = number_format(($totalProgrammed * 100) / $totalQuotations, 2);
        } else $percentage = 0.0;

        if (count($quotations) == 3) $indexLastQuotation = 2;
        elseif (count($quotations) == 0) $indexLastQuotation = null;
        else $indexLastQuotation = 0;

        $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
        $countryCode = DB::table('countries')
            ->where('id', $companie->id_code_country)
            ->first();

        $symbol_country = CommonCompany::getSymbolByCountry();
        $timeZone = profile_job_center::find($jobCenterSession->id_profile_job_center)->timezone;

        return view('vendor.adminlte.register.index')->with(
            [
                'date' => $date,
                'plagues' => $plague,
                'establishments' => $establishment,
                'employees' => $employees,
                'jobCenterSession' => $jobCenterSession,
                'id_q' => $id_q,
                'quotation' => $quotations,
                'discounts' => $discounts,
                'extras' => $extras,
                'sources' => $sourceOrigin,
                'tracing' => $tracings,
                'jobCenters' => $jobCenters,
                'jobCenterEmployee' => $jobCenterEmployee,
                'totalQuotations' => $totalQuotations,
                'jobCenterStatus' => $jobCenterStatus,
                'totalProgrammed' => $totalProgrammed,
                'events' => $events,
                'jobCenterSelect' => $profileJobCenter,
                'keyPriceLists' => $keyPriceLists,
                'customers' => $customers,
                'address' => $address,
                'percentage' => $percentage,
                'indexLastQuot' => $indexLastQuotation,
                'countryCode' => $countryCode->code_country,
                'symbol_country' => $symbol_country,
                'descriptionsForCustomQuotes' => $descriptionsForCustomQuotes,
                'labels' => $labels,
                'timezone' => $timeZone,
                'inspections' => $inspections
        ]);
    }

    public function store(Request $request)
    {

        try {

            DB::beginTransaction();
            $jobCenterEmployee = $request->get('jobCenter');
            //Validate plan free
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                $defaultDiscount = discount::where('description', 'No borrar. Este descuento es propio del sistema, si se borra el sistema dejará de funcionar.')
                    ->where('profile_job_center_id', $jobCenterEmployee)
                    ->first();
                $defaultExtra = extra::where('description', 'No borrar. Este extra es propio del sistema, si se borra el sistema dejará de funcionar.')
                    ->where('profile_job_center_id', $jobCenterEmployee)
                    ->first();
                if ($request->get('discount') != $defaultDiscount->id || $request->get('extra') != $defaultExtra->id) {
                    return \response()->json([
                        'code' => 500,
                        'message' => 'Algo salio mal, intente de nuevo.'
                    ]);
                }
            }

            $validator = Validator::make(Input::all(), $this->rules);
            if ($validator->fails()) {
                return \response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal, intente de nuevo.'
                ]);
            } else {

                if (empty($request->cellphone)) {
                    $cellphone = 'no';
                }else{
                    $cellphone = $request->cellphone;
                }

                $customerIdFind = $request->get('customerIdFind');
                if($customerIdFind != 0){
                    $client = DB::table('customers as c')
                        ->where('id', $customerIdFind)
                        ->where('companie', Auth::user()->companie)
                        ->first();
                } else {
                    $client = DB::table('customers as c')
                        ->where('cellphone', $cellphone)
                        ->where('companie', Auth::user()->companie)
                        ->first();
                }

                // Valida si el cliente intenta usar el teléfono para más de un cliente.
                if (!empty($client)) {
                    if ($client->name != $request->get('name') && $request->get('isConfirmedCustomer') == "false") {
                        return response()->json([
                            'code' => 304,
                            'message' => "El número " . $client->cellphone . " pertenece al cliente " . $client->name . ", si continuas guardando la cotización remplazarás los datos del cliente por los de esta cotización."
                        ]);
                    }
                }

                $idUser = Auth::id();

                if (empty($client)){
                    //Agregar Cliente -> Tabla customers
                    $customer = new customer;
                    $customer->name = $request->get('name');
                    $customer->colony = $request->get('address');
                    $customer->establishment_name = $request->get('establishmentName');
                    if ($request->get('cellphone') == 0){
                        $customer->cellphone = null;
                    }
                    else{
                        $customer->cellphone = $request->get('cellphone');
                    }
                    $customer->cellphone_main = $request->get('cellphoneMain');
                    $customer->establishment_id = $request->get('establishment');
                    $customer->municipality = $request->get('municipality');
                    $customer->source_origin_id = $request->get('sourceOrigin');
                    $customer->id_profile_job_center = $jobCenterEmployee;
                    $customer->companie = Auth::user()->companie;
                    $customer->save();

                    //Agregar los datos conocidos en la tabla customer_datas
                    $customer_data = new customer_data;
                    $customer_data->customer_id = $customer->id;
                    $customer_data->reference_address = $request->get('reference_address');
                    $customer_data->email = $request->get('email');
                    $customer_data->phone_number = $request->get('cellphone');
                    $customer_data->save();
                }else{
                    //Actualizar Cliente -> Tabla customers
                    $customer = customer::find($client->id);
                    $customer->name = $request->get('name');
                    $customer->establishment_name = $request->get('establishmentName');
                    if ($request->get('cellphone') == 0){
                        $customer->cellphone = null;
                    }
                    else{
                        $customer->cellphone = $request->get('cellphone');
                    }
                    $customer->cellphone_main = $request->get('cellphoneMain');
                    $customer->establishment_id = $request->get('establishment');
                    $customer->colony = $request->get('address');
                    $customer->municipality = $request->get('municipality');
                    $customer->source_origin_id = $request->get('sourceOrigin');
                    $customer->id_profile_job_center = $jobCenterEmployee;
                    $customer->save();

                    //Actualizar los datos conocidos en la tabla customer_datas
                    $customer_data = customer_data::where('customer_id', $client->id)->first();
                    $customer_data->reference_address = $request->get('reference_address');
                    $customer_data->email = $request->get('email');
                    $customer_data->phone_number = $request->get('cellphone');
                    $customer_data->save();
                }

                //Crear la nueva Cotizacion
                $quotation = new quotation;
                $quotation->id_customer = $customer->id;
                $quotation->construction_measure = $request->get('construction_measure') ;
                $quotation->garden_measure = $request->get('garden_measure');
                $quotation->id_plague_jer = $request->get('id_plague_jer');
                $quotation->establishment_id = $request->get('establishment');
                $quotation->id_status = 10; //Falta definir cuales son los tipos de estados de la cotización
                $quotation->total = $request->get('total');
                $quotation->price = $request->get('price');
                $quotation->price_reinforcement = $request->get('priceReinforcement');
                $quotation->user_id = $idUser;
                $quotation->id_job_center = $jobCenterEmployee;
                $quotation->companie = Auth::user()->companie;
                $quotation->messenger = $request->get('messenger');
                $quotation->id_extra = $request->get('extra');
                $quotation->id_price_list = $request->get('id_price_list');
                $quotation->pesticide_product = $request->get('pesticide');
                $quotation->description_whatsapp = $request->get('description_whatsapp');
                $quotation->id_quotation = CommonQuotation::getFolioQuote();
                $quotation->save();

                //Guardar plagas asociados al id de la cotización.
                $contador = count($request->plague);
                $plagues = [];

                for ($i = 0; $i < $contador; $i++) {
                    $separador = explode(",", $request->plague[$i]);
                    $idPlague = $separador[0];

                    array_push($plagues, $idPlague);
                }

                $quotation->plague_type()->sync($plagues);//Guardar plagas asociados al id de la cotización.
                $contador = count($request->plague);
                $plagues = [];

                for ($i = 0; $i < $contador; $i++) {
                    $separador = explode(",", $request->plague[$i]);
                    $idPlague = $separador[0];

                    array_push($plagues, $idPlague);
                }

                $quotation->plague_type()->sync($plagues);

                //Asignar descuento y cotización
                $discount = new discount_quotation;
                $discount->quotation_id = $quotation->id;
                $discount->discount_id = $request->discount;
                $discount->save();

                // Inspection
                $inspectionQuote = $request->get('inspection');
                if ($inspectionQuote != 0) {
                    $inspection = service_order::where('id_quotation', $inspectionQuote)->first();
                    if ($inspection) {
                        $inspection->id_quotation = $quotation->id;
                        $inspection->fake = 0;
                        $inspection->save();
                        $quotationFake = quotation::find($inspectionQuote);
                        $quotation->id_quotation = $quotationFake->id_quotation;
                        $quotation->save();
                        DB::table('quotations')->where('id', $inspectionQuote)->delete();
                    }
                }

                DB::commit();

                return \response()->json([
                    'code' => 201,
                    'message' => 'Cotización Guardada.',
                    'id' => $quotation->id,
                    'idEncrypt' => SecurityController::encodeId($quotation->id)
                ]);
            }
        }
        catch (Exception $e){
            DB::rollBack();
            return \response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

    public function massiveQuote()
    {
        $customers1 = customer::where('id_profile_job_center', 480)->get();
        $customers3 = customer::where('id_profile_job_center', 481)->get();
        $customers4 = customer::where('id_profile_job_center', 482)->get();
        $customers5 = customer::where('id_profile_job_center', 484)->get();
        $customers6 = customer::where('id_profile_job_center', 485)->get();
        $customers7 = customer::where('id_profile_job_center', 486)->get();
        $customers9 = customer::where('id_profile_job_center', 487)->get();

        $priceList1 = 15434;
        $priceList3 = 15435;
        $priceList4 = 15436;
        $priceList5 = 15437;
        $priceList6 = 15438;
        $priceList7 = 15439;
        $priceList9 = 15440;

        foreach ($customers9 as $customer)
        {
            $extra = extra::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('name', 'Sin Extra')->first();

            $discount = discount::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('title', 'Sin Descuento')->first();

            $plague = plague_type::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('name', 'Rastreros, Roedores y Voladores')->first();

            $quotation = new quotation;
            $quotation->id_customer = $customer->id;
            $quotation->construction_measure = 1;
            $quotation->id_plague_jer = 1;
            $quotation->establishment_id = $customer->establishment_id;
            $quotation->id_status = 10;
            $quotation->total = 0;
            $quotation->price = 0;
            $quotation->price_reinforcement = 0;
            $quotation->user_id = 11448;
            $quotation->id_job_center = $customer->id_profile_job_center;
            $quotation->companie = 640;
            $quotation->id_extra = $extra->id;
            $quotation->id_price_list = $priceList9;
            $quotation->id_quotation = CommonQuotation::getFolioQuote();
            $quotation->save();

            //Plague
            $plagueQuote = new plague_type_quotation();
            $plagueQuote->quotation_id = $quotation->id;
            $plagueQuote->plague_type_id = $plague->id;
            $plagueQuote->save();

            //Discount
            $discountQuote = new discount_quotation();
            $discountQuote->quotation_id = $quotation->id;
            $discountQuote->discount_id = $discount->id;
            $discountQuote->save();

            sleep(2);

        }

        dd('ok');
    }

    public function editQuotation($idQuotation)
    {
        $idQuotation = SecurityController::decodeId($idQuotation);

        $quotation = quotation::find($idQuotation);
        $validate = 1;

        // Middleware to deny access to data from another company
        if ($quotation->companie != auth()->user()->companie) {
            SecurityController::abort();
        }

        $quot = DB::table('quotations as q')
            ->join('customers as c', 'c.id', 'q.id_customer')
            ->join('establishment_types as e', 'e.id', 'c.establishment_id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('source_origins as so', 'c.source_origin_id', 'so.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            ->join('profile_job_centers as pjc', 'q.id_job_center', 'pjc.id')
            ->select('q.id', 'q.id_quotation', 'q.created_at as date', 'c.name', 'c.id as id_customer', 'cd.phone_number', 'e.id as e_id', 'e.name as e_name',
                'q.construction_measure', 'q.garden_measure', 'q.description' ,'q.id_status', 'q.id_plague_jer', 'c.establishment_name', 'c.cellphone', 'c.cellphone_main',
                'cd.address', 'cd.reference_address', 'cd.email', 'c.colony', 'c.municipality', 'q.total', 'q.price', 'q.messenger', 'qd.discount_id', 'q.id_extra',
                'd.title as discount_name', 'd.percentage', 'so.name as source_name', 'so.id as source_id', 'ex.name as extra_name', 'ex.amount',
                'pjc.name as job_center_name', 'pjc.id as id_job_center','q.pesticide_product')
            ->where('q.id', $idQuotation)
            ->first();

        $customQuotation = DB::table('custom_quotes')->where('id_quotation', $quot->id)->get();
        $cont = DB::table('custom_quotes')->where('id_quotation', $quot->id)->count();
        $arrayCustomQuotations = ['quotation' => $quot, 'customQuotations' => $customQuotation];
        $quot = collect($arrayCustomQuotations);
        $suma = 0;

        if ($cont > 0)
        {
            $validate = 0;
            foreach ($customQuotation as $cusQ){
                $suma = $suma + $cusQ->subtotal;
            }
        }

        $pricesList = DB::table('price_lists')
            ->where('establishment_id', $quot['quotation']->e_id)
            ->where('status', 1)
            ->get();

        $plagues = new Collection();
        foreach ($pricesList as $priceList) {
            $plaguesList = DB::table('plagues_list as pl')
                ->join('plague_types as pt', 'pl.plague_id', 'pt.id')
                ->where('pl.price_list_id', $priceList->id)
                ->select('pt.id', 'pt.name', 'pt.plague_key')
                ->get();
            foreach ($plaguesList as $plagueList) {
                if ($plagues->search($plagueList) == false) $plagues->push($plagueList);
            }
        }

        $establishment = establishment_type::where('profile_job_center_id', $quot['quotation']->id_job_center)->orderBy('name','asc')->get();
        $discount = discount::where('profile_job_center_id', $quot['quotation']->id_job_center)->orderBy('title','asc')->get();
        $extras = extra::where('profile_job_center_id', $quot['quotation']->id_job_center)->orderBy('name','asc')->get(); //Arreglo para el select de Extras
        $sources = source_origin::where('profile_job_center_id', $quot['quotation']->id_job_center)->orderBy('name','asc')->get();
        $sucursales = profile_job_center::where('companie', Auth::user()->companie)->get();
        $quotationPlague = $quotation->plague_type;
        $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
        $countryCode = DB::table('countries')
            ->where('id', $companie->id_code_country)->first();
        $symbol_country = CommonCompany::getSymbolByCountry();
        $descriptionsForCustomQuotes = DescriptionCustomQuote::where('profile_job_center_id', $quot['quotation']->id_job_center)->get(); // TODO: Add Migration Profile Job Center
        //return response()->json(['quotation' => $quot, 'plagues' => $plague, 'quotationPlagues' => $quotationPlague, 'establishments' => $establishment]);
        return view('vendor.adminlte.register._editquotation')
            ->with([
                'quotation' => $quot, 'suma' => $suma, 'plagues' => $plagues, 'quotationPlagues' => $quotationPlague, 'establishments' => $establishment, 'discounts' => $discount,
                'validate' => $validate, 'sources' => $sources, 'extras' => $extras, 'sucursales' => $sucursales, 'countryCode' => $countryCode,
                'symbol_country' => $symbol_country, 'descriptionsForCustomQuotes' => $descriptionsForCustomQuotes
            ]);
    }

    public function update(Request $request)
    {
        $q = $request->get('id');
        $idUser = Auth::id();
        $idPriceListRequest = $request->get('id_price_list');

        $customer_query = DB::table('quotations')->select('id_customer')->where('id', $q)->first();
        //Editar los datos del cliente1
        $customer = customer::find($customer_query->id_customer);
        $customer->name = $request->get('name');
        $customer->colony = $request->get('address');
        $customer->establishment_name = $request->get('establishmentName');
        $customer->cellphone = $request->get('cellphone');
        $customer->cellphone_main = $request->get('cellphoneMain');
        $customer->establishment_id = $request->get('establishment');
        $customer->municipality = $request->get('municipality');
        $customer->source_origin_id = $request->get('sourceOrigin');
        $customer->id_profile_job_center = $request->get('sucursal');
        $customer->save();

        $customerData_query = DB::table('customer_datas')->select('id', 'customer_id')->where('customer_id', $customer_query->id_customer)->first();

        //Editar los datos del cliente2
        $customer_data = customer_data::find($customerData_query->id);
        $customer_data->reference_address = $request->get('reference_address');
        $customer_data->email = $request->get('email');
        $customer_data->phone_number = $request->get('cellphone');
        $customer_data->save();

        $beforeTotal = 0;
        $quotationBefore = quotation::find($q);
        $beforeTotal = $quotationBefore->total;

        //Actualizar la Cotizacion
        $quotation = quotation::find($q);
        $quotation->construction_measure = $request->get('construction_measure');
        $quotation->garden_measure = $request->get('garden_measure');
        $quotation->id_plague_jer = $request->get('id_plague_jer');
        $quotation->establishment_id = $request->get('establishment');
        if ($request->get('validate') == 1) {
            $quotation->total = $request->get('total');
            $quotation->price = $request->get('price');
            $quotation->price_reinforcement = $request->get('priceReinforcement');
        }
        $quotation->user_id = $idUser;
        $quotation->messenger = $request->get('messenger');
        $quotation->id_extra = $request->get('extra');
        if ($idPriceListRequest) $quotation->id_price_list = $request->get('id_price_list');
        $quotation->id_job_center = $request->get('sucursal');
        $quotation->pesticide_product = $request->get('pesticide');
        $quotation->description_whatsapp = $request->get('description_whatsapp');
        $quotation->save();

        //Actualiza precio en orden de servicio.
        $serviceOrder = DB::table('service_orders')
            ->where('id_quotation', $q)
            ->where('total', $beforeTotal)
            ->first();
        if ($serviceOrder) {
            $updateOrder = service_order::find($serviceOrder->id);
            $updateOrder->total = $quotation->total;
            $updateOrder->save();
        }


        //Guardar plagas asociados al id de la cotización.
        $contador = count($request->get('plague'));
        $plagues = [];

        for ($i = 0; $i < $contador; $i++) {
            $separador = explode(",", $request->plague[$i]);
            $idPlague = $separador[0];

            array_push($plagues, $idPlague);
        }
        $quotation->plague_type()->sync($plagues);

        if ($request->get('validate') == 1) {
            $quotation->discount()->sync([$request->get('discount')]);
        }

        return response()->json(['id' => $quotation->id]);
    }

    public function delete($q)
    {
        try {
            // Middleware to deny access to data from another company
            $quote = quotation::find($q);
            $company = Auth::user()->companie;
            if ($quote->companie != $company || empty($quote)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal. Usuario bloqueado.'
                ]);
            }

            DB::table('quotations')->select('id')->where('id', $q)->delete();
            return response()->json([
                'code' => 201,
                'message' => 'Se eliminó la cotización.'
            ]);
        }
        catch (\Exception $exception) {
        return response()->json(
            [
                'code' => 500,
                'message' => 'Algo salio mal intentalo de nuevo']
            );
        }
    }

    public function approvedQuotation(Request $request) {
        try {
            $quotation = quotation::find(SecurityController::decodeId($request->get('idQuotation')));
            $quotation->approved = 1;
            $quotation->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se aprobo la cotización.'
            ]);
        }
        catch (\Exception $exception) {
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo']
            );
        }
    }

    public function getPriceQuotation(Request $request){

        $validator = Validator::make(Input::all(), $this->rulesPrice);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        else {

            try {

                $price = 0;
                $discountFinal = 0;
                $priceReinforcement = 0;
                $discountSpeech = "";
                $extraDescription = "";
                $message = "No existe lista de precios para el tipo de servicio seleccionado.";
                $establishment = $request->establishment;
                $constructionMeasure = $request->construction_measure;
                $jobCenterSelect = $request->get('jobCenterSelect');

                $herarCurrent = 100;
                $idPriceCurrent = 0;

                for ($i=0; $i < $request->contador; $i++) {
                    $separador = explode(",", $request->plague[$i]);
                    $campo = $separador[1];
                    $idPlague = $separador[0];

                    $hierarchy = $pricesList = DB::table('plagues_list')
                        ->join('price_lists','plagues_list.price_list_id','price_lists.id')
                        ->where('plagues_list.plague_id',$idPlague)
                        ->where('price_lists.profile_job_center_id', $jobCenterSelect)
                        ->where('price_lists.status', 1)
                        ->where('establishment_id', $establishment)
                        ->min('hierarchy');

                    $pricesList = DB::table('plagues_list')
                        ->join('price_lists','plagues_list.price_list_id','price_lists.id')
                        ->where('plagues_list.plague_id',$idPlague)
                        ->where('price_lists.profile_job_center_id', $jobCenterSelect)
                        ->where('price_lists.status', 1)
                        ->where('establishment_id', $establishment)
                        ->where('hierarchy', $hierarchy)
                        ->first();

                    if ($hierarchy < $herarCurrent) {
                        $idPriceCurrent = $pricesList->id;
                        $herarCurrent = $hierarchy;
                    }

                }


                $idPriceList = $idPriceCurrent;

                // Individual Price
                if ($pricesList->individual_price != 0) {
                   $price = $pricesList->value_individual_price * $constructionMeasure;
                }

                // Price List
                else {
                    $priceOne = DB::table('prices')
                        ->where('price_list_id', $idPriceList)
                        ->where('area', '<=', $constructionMeasure)
                        ->orderBy('area', 'desc')
                        ->first();

                    if($priceOne == null) $message = "No existe precio para las medidas de construccion: " . $constructionMeasure . " m², Revisar lista de precios.";

                    $price = $priceOne->price_one;
                }

                $description = $pricesList->description;

                // validacion si existe descuento.
                $discount = DB::table('discounts')->select('percentage')->where('id', $request->discount)->first();
                $d1 = $discount->percentage/100;
                $total = $price*$d1;
                $total = $price-$total;
                $discountFinal = $total;
                $discountSpeech = number_format($discount->percentage,0);

                // validacion si existe extra.
                $extra = DB::table('extras')->select('amount', 'description')->where('id', $request->extra)->first();
                if($extra->amount != 0){
                    $total = $total + $extra->amount;
                    $priceReinforcement = $extra->amount;
                    $extraDescription = $extra->description;
                }else{
                    $extraDescription = "";
                }

                $total = round($total);

                return response()->json(['response' => 200, 'price' => $price, 'id_plague' => 1, 'speech' => $description, 'total' => $total,
                    'extra' => $extraDescription, 'discount' => $discountSpeech, 'id_price_list' => $idPriceList,
                    'priceRefuerzo' => $priceReinforcement, 'discountFinal' => $discountFinal]);

            }catch (\Exception $exception) {
                return response()->json(['response' => 500, 'error' => 'Internal Error', 'message' => $message]);
            }


        }
    }

    public function getPlaguesByService($serviceId)
    {
        try {
            $pricesList = DB::table('price_lists')
                ->where('establishment_id', $serviceId)
                ->where('status', 1)
                ->get();

            if ($pricesList->count() == 0) {
                return Response::json([
                    'code' => 500,
                    'message' => 'No se encontrarón plagas para el tipo de servicio seleccionado.'
                ]);
            }

            $plagues = new Collection();
            foreach ($pricesList as $priceList) {
                $plaguesList = DB::table('plagues_list as pl')
                    ->join('plague_types as pt', 'pl.plague_id', 'pt.id')
                    ->where('pl.price_list_id', $priceList->id)
                    ->select('pt.id', 'pt.name')
                    ->get();
                foreach ($plaguesList as $plagueList) {
                    if ($plagues->search($plagueList) == false) $plagues->push($plagueList);
                }
            }
            return Response::json([
                'code' => 200,
                'plagues' => $plagues
            ]);
        }catch (\Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function LastQuotation(){
        $user = Auth::id();
        $last = DB::table('quotations')->select('id')->where('user_id',$user)->orderBy('id','DESC')->first();
        $last_id = $last->id;
        return Response::json($last_id);
    }

    public function PDF($id)
    {
        if (strlen($id) > 4) $id = SecurityController::decodeId($id);
        $quotationId = $id;
        $quotation = CommonQuotation::getQuotationDataById($quotationId);

        if ($quotation != null) {

            // Variables PDF
            $quotationPdf = QuotationPdf::build($quotationId,$quotation);

            if ($quotationPdf['imagen']->id == 333) {
                if ($quotationPdf['pricesList']->pdf != null) {

                    $pdf = PDF::loadView('vendor.adminlte.register.PDF.Customized.pdfCustom', $quotationPdf)
                        ->save(storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf');
                    $pdf1 = storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf';

                    $documentos = [$quotationPdf['ruta'],$pdf1];
                    $combinador = new Merger;

                    foreach ($documentos as $documento) {
                        $combinador->addFile($documento);
                    }

                    $salida = $combinador->merge();
                    $nombre = 'Cotización'.' '.$quotation->id_quotation.'.pdf';

                    header("Content-type:application/pdf");
                    header("Content-Disposition:attachment;filename=$nombre");
                    echo $salida;
                    exit;

                }
                else{
                    $pdf3 = PDF::loadView('vendor.adminlte.register.PDF.Customized.pdfCustom',$quotationPdf);
                    return $pdf3->stream();
                }
            }

            if ($quotationPdf['pricesList']->pdf != null) {

                $pdf = PDF::loadView('vendor.adminlte.register.PDF.Customized.pdf',$quotationPdf)
                    ->save(storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf');
                $pdf1 = storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf';

                $documentos = [$quotationPdf['ruta'],$pdf1];
                $combinador = new Merger;

                foreach ($documentos as $documento) {
                    $combinador->addFile($documento);
                }

                $salida = $combinador->merge();
                $nombre = 'Cotización'.' '.$quotation->id_quotation.'.pdf';

                header("Content-type:application/pdf");
                header("Content-Disposition:attachment;filename=$nombre");
                echo $salida;
                exit;

            }
            else{
                $pdf3 = PDF::loadView('vendor.adminlte.register.PDF.Customized.pdf',$quotationPdf);
                return $pdf3->stream();
            }
        }
    }

    public function viewMessage($id)
    {
        $quotation = DB::table('quotations as q')->join('customers as c','c.id','q.id_customer')
            ->join('establishment_types as e','e.id','c.establishment_id')
            ->join('customer_datas as cd','cd.customer_id','c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->select('q.id','q.created_at as date','q.total','q.construction_measure','q.garden_measure','q.id_status','c.name','c.establishment_name',
                'c.cellphone','e.id as e_id','e.name as e_name','user_id',
                'cd.address','cd.reference_address','cd.email', 'cd.phone_number', 'c.municipality','q.id_quotation','q.id_plague_jer','q.price','q.id_extra',
                'q.companie')
            ->where('q.id',$id)->first();

        return view('vendor.adminlte.register._modalSendEmailQuotation')->with(['quotation' => $quotation]);
    }

    public function Message(Request $request){

        $id = $request->id;
        $quotation = DB::table('quotations as q')->join('customers as c','c.id','q.id_customer')
            ->join('establishment_types as e','e.id','c.establishment_id')
            ->join('customer_datas as cd','cd.customer_id','c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->select('q.id','q.created_at as date','q.total','q.construction_measure','q.garden_measure','q.id_status','c.name','c.establishment_name',
                'c.cellphone','e.id as e_id','e.name as e_name','user_id',
                'cd.address','cd.reference_address','cd.email', 'cd.phone_number', 'c.municipality','q.id_quotation','q.id_plague_jer','q.price','q.id_extra',
                'q.companie', 'q.id_price_list')
            ->where('q.id',$id)->first();
        $user = DB::table('users')->select('name')->where('id',$quotation->user_id)->first();
        $discount = DB::table('discount_quotation as qd')->join('discounts as d','d.id','qd.discount_id')
            ->select('d.percentage')->where('qd.quotation_id',$id)->first();
        $imagen = DB::table('companies')->select('pdf_logo','pdf_sello','phone','licence','facebook')->where('id',$quotation->companie)->first();
        $d = 0;
        $subtotal = 0;
        $area = 0;
        if($discount != null){//Obtener el total a pagar
            $d1 = $discount->percentage/100;
            $d = $quotation->price*$d1;
            $pre = $quotation->price - $d;
            $subtotal = $quotation->total;
        }else {
            $subtotal = $quotation->total;
        }
        if($quotation->garden_measure == null){//Obtener el area total a fumigar
            $area = $quotation->construction_measure;
        }else{
            $area = $quotation->construction_measure+$quotation->garden_measure;
        }
        //Obtener el extra
        $extra = DB::table('extras')->select('id','description','amount')->where('id',$quotation->id_extra)->first();
        if($extra->amount == 0){
            $e = 0;
        }else{
            $e = $extra->amount;
        }
        $plague = DB::table('plague_type_quotation as qpt')->join('plague_types as pt','pt.id','qpt.plague_type_id')
            ->select('pt.name','pt.id as plague')->where('qpt.quotation_id',$id)->get();
        $plague2 = collect($plague)->toArray();//Generar array con todas las plagas seleccionadas
        //$priority = $plague->where('plague',$quotation->id_plague_jer)->first();
        //$speech = DB::table('service_informations')->select('description_pdf')->where('plague_id',$priority->plague)
        //->where('establishment_id',$quotation->e_id)->first();

        $pricesList = DB::table('price_lists')
            ->where('id', $quotation->id_price_list)
            ->first();

        $indications = DB::table('indications')
            ->where('id',$pricesList->indications_id)
            ->first();

        $c = DB::table('users')->select('companie')->where('id',auth()->user()->id)->first();
        $companie = $c->companie;
        $banner = DB::table('personal_mails')->where('id_company',$companie)->first();
        $ruta = storage_path().'/app/'.$pricesList->pdf;
        $pdf = PDF::loadView('vendor.adminlte.register.PDF.Customized.pdf',['imagen' => $imagen, 'quotation' => $quotation,
            'user' => $user,'plague2' => $plague2, 'plagues' => $plague,'area' => $area, 'indications' => $indications,
            'extra' => $extra, 'e' => $e, 'd' => $d, 'subtotal' => $subtotal, 'pricesList' => $pricesList, 'pre' => $pre])->save(storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf');
        $pdf1 = storage_path().'/app/pdf/'.'Cotizacion'.' '.$quotation->id_quotation.'.pdf';

        $documentos = [$ruta,$pdf1];
        $combinador = new Merger;
        foreach ($documentos as $documento) {
            $combinador->addFile($documento);
        }
        $salida = $combinador->merge();
        $comp = companie::find(Auth::user()->companie);
        if ($request->email != 'null') {
            config(['mail.from.name' => $comp->name]);
            $user = $request->email;
            $result = filter_var( $user, FILTER_VALIDATE_EMAIL );
            if($result != false){
                Mail::to($user)->send(new QuotatioMail($user,$id,$salida,$companie));
            }
            $user = $quotation->email;
            config(['mail.from.name' => $comp->name]);
            $result = filter_var( $user, FILTER_VALIDATE_EMAIL );
            if($result != false){
                Mail::to($user)->send(new QuotatioMail($user,$id,$salida,$companie));
            }
        }else{
            $user = $quotation->email;
            config(['mail.from.name' => $comp->name]);
            $result = filter_var( $user, FILTER_VALIDATE_EMAIL );
            if($result != false){
                Mail::to($user)->send(new QuotatioMail($user,$id,$salida,$companie));
            }
        }

        return redirect()->route('index_register');
    }

    public function MailGenerate(Request $request){
        try {
            //Validate plan free
            DB::beginTransaction();
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                DB::commit();
                return response()->json([
                    'code' => 500,
                    'message' => 'Error al enviar'
                ]);
            }
            else {
                $quotationId = $request->get('quotationId');
                $emails = $request->get('emails');
                $quot = new SendMailQuotation($quotationId);
                $quot->sendMailQuotById($emails);

                // Update Status Send Email
                $quotation = quotation::find($quotationId);
                $quotation->email = 1;
                $quotation->save();

                DB::commit();
                return response()->json([
                    'code' => 200,
                    'message' => 'Cotización enviada'
                ]);
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, Intentalo de nuevo.'
            ]);
        }

    }

    public function DenyView($id){
        $quotation = quotation::find($id);

        $quot = DB::table('quotations as q')
            ->join('customers as c', 'c.id', 'q.id_customer')
            ->join('establishment_types as e', 'e.id', 'c.establishment_id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->select('q.id', 'q.id_quotation', 'q.created_at as date', 'c.name', 'c.id as id_customer', 'cd.phone_number', 'e.id as e_id', 'e.name as e_name',
                'q.construction_measure', 'q.garden_measure', 'q.id_status', 'q.id_plague_jer', 'c.establishment_name', 'c.cellphone',
                'cd.address', 'cd.reference_address', 'cd.email', 'c.colony', 'c.municipality', 'q.total', 'q.price', 'qd.discount_id', 'd.title as discount_name', 'd.percentage')
            ->where('q.id', $id)
            ->first();

        return view('vendor.adminlte.register._deny')->with(['quotation' => $quot]);
    }

    public function Deny_Quotation(Request $request){
        try {
            //Validate plan free
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal, intente de nuevo.'
                ]);
            } else {
                $id_Quotation = $request->id_quotation;
                $deny = new rejected_quotation;
                $deny->id_quotation = $id_Quotation;
                $deny->reason = $request->reason;
                $deny->commentary = $request->comments;
                $deny->date = Carbon::now()->toDateString();
                $deny->save();

                //Actualizar Status
                $quotation = quotation::find($id_Quotation);
                $quotation->id_status = 3;
                $quotation->save();

                // Actualizar Comentarios Historicos
                $historicalCustomer = new \App\HistoricalCustomer();
                $historicalCustomer->id_customer = $quotation->id_customer;
                $historicalCustomer->id_user = $quotation->user_id;
                $historicalCustomer->title = 'Rechazo Cotización: ' . $quotation->id_quotation;
                $historicalCustomer->commentary = 'Razón: ' . $deny->reason . ' ' . $deny->commentary;
                $historicalCustomer->date = Carbon::now()->toDateString();
                $historicalCustomer->hour = Carbon::now()->toTimeString();
                $historicalCustomer->save();

                return response()->json([
                    'code' => 201,
                    'message' => 'Se rechazo la cotización.'
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function CancelView($id){
        $quotation = quotation::find($id);

        $quot = DB::table('quotations as q')
            ->join('customers as c', 'c.id', 'q.id_customer')
            ->join('establishment_types as e', 'e.id', 'c.establishment_id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->select('q.id', 'q.id_quotation', 'q.created_at as date', 'c.name', 'c.id as id_customer', 'cd.phone_number', 'e.id as e_id', 'e.name as e_name',
                'q.construction_measure', 'q.garden_measure', 'q.id_status', 'q.id_plague_jer', 'c.establishment_name', 'c.cellphone',
                'cd.address', 'cd.reference_address', 'cd.email', 'c.colony', 'c.municipality', 'q.total', 'q.price', 'qd.discount_id', 'd.title as discount_name', 'd.percentage')
            ->where('q.id', $id)
            ->first();

        return view('vendor.adminlte.register._cancel')->with(['quotation' => $quot]);
    }

    public function Cancel_Quotation(Request $request){
        $id_Quotation = $request->id_quotation;

        $deny = new canceled_quotation;
        $deny->id_quotation = $id_Quotation;
        $deny->reason = $request->reason;
        $deny->commentary = $request->comments;
        $deny->date = Carbon::now()->toDateString();
        $deny->save();

        //Actualizar Status
        $quotation = quotation::find($id_Quotation);
        $quotation->id_status = 2;
        $quotation->save();
    }

    public function tracingView($id){
        try {
            //Validate plan free
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Error al obtener los seguimientos.'
                ]);
            } else {
                $tracings = DB::table('tracings')->where('id_quotation', $id)->get();
                return response()->json([
                    'data' => $tracings,
                    'message' => 'Correcto.'
                ]);
            }
        } catch (\Exception $exception) {
            return \response()->json([
                'code' => 500,
                'message' => 'Error al obtener los seguimientos.'
            ]);
        }
    }

    public function tracingQuotation(Request $request){
        try {

        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al obtener los seguimientos.'
            ]);
        } else {
            $idQuotation = $request->input('id_quotation');
            $tracing = new tracing;
            $tracing->id_quotation = $idQuotation;
            $tracing->tracing_date = $request->input('dateTracing');
            $tracing->tracing_hour = $request->input('hourTracing');
            $tracing->commentary = $request->input('commentsTracing');
            $tracing->save();

            //Actualizar Status
            $quotation = quotation::find($idQuotation);
            $quotation->id_status = 1;
            $quotation->save();

            // Actualizar Comentarios Historicos
            $historicalCustomer = new \App\HistoricalCustomer();
            $historicalCustomer->id_customer = $quotation->id_customer;
            $historicalCustomer->id_user = $quotation->user_id;
            $historicalCustomer->title = 'Seguimiento Cotización: ' . $quotation->id_quotation;
            $historicalCustomer->commentary = $tracing->commentary;
            $historicalCustomer->date = Carbon::now()->toDateString();
            $historicalCustomer->hour = Carbon::now()->toTimeString();
            $historicalCustomer->save();
        }
            return response()->json([
                'code' => 201,
                'message' => 'Seguimiento de la cotización.'
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function Recover_Quotation($id){
        $sta = DB::table('quotations')->where('id', '=', $id)->update(['id_status' => 10]);
        return redirect()->route('index_register');
    }

    public function whatsapp(Request $request){

        $order = quotation::find($request->id);
        $order->whatsapp = 1;
        $order->save();

        return response()->json();

    }

    #endregion QUOTATION
}
