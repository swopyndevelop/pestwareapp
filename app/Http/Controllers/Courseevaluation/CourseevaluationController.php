<?php

namespace App\Http\Controllers\Courseevaluation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AnswerEvaluation;
use App\QuestionEvaluation;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use DB;
use App\Tracking;
use App\CourseProgress;
use App\Employee;


class CourseevaluationController extends Controller
{
	protected $rules = 
	[
		'Answer' => 'required',
	];

	public function intro($course){
		$tmp = DB::table('question_evaluations')->where('trainings_id', $course)->count();
		return view('adminlte::courseevaluation.intro')->with(['course' => $course, 'pass' => $tmp]);
	}

	public function index($course){

		/*$tmp = DB::table('employees as e')->join('job_title_profile_training_plan as jtp','e.job_title_profile_id','jtp.job_title_profile_id')->join('training_training_plan as ttp','ttp.training_plan_id','jtp.id')->join('question_evaluations as questions','questions.trainings_id','ttp.id')->where('ttp.training_id',$course);*/
		$tmp = DB::table('question_evaluations')->where('trainings_id',$course);
		$question = $tmp->paginate(1);
		$option = DB::table('option_evaluations')->where('question_evaluation_id', $question[0]->id)->inRandomorder()->get();
		$ans = DB::table('answer_evaluations')->where('question_evaluations_id', $question[0]->id)->where('user_id',auth()->user()->id)->orderBy('id', 'asc')->get();
		$time = DB::table('answer_evaluations')->select('question_evaluations_id')->where('user_id', auth()->user()->id)->groupBy('question_evaluations_id')->get();
		return view('adminlte::courseevaluation.index')->with(['question' => $question,'option' => $option, 'answers' => $ans, 'time' => $time, 'course'=> $course]);
	}

	public function save(Request $request){
		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} 
		else {
			$quiz = QuestionEvaluation::where('id','=',$request->question_id)->first();
			$ans = DB::table('answer_evaluations')->where('question_evaluations_id','=',$request->question_id)->where('user_id',auth()->user()->id)->delete();
			$temp;
			if ($quiz->type == 'multioption' || $quiz->type == 'order' ) {
				foreach ($request->Answer as $d) {
					$temp = new AnswerEvaluation;
					$temp->question_evaluations_id = $request->question_id;
					$temp->user_id = auth()->user()->id;
					$temp->option_evaluations_id = $d;
					$temp -> save();
				}
			} else if ($quiz->type == 'only'){
				$temp = new AnswerEvaluation;
				$temp->question_evaluations_id = $request->question_id;
				$temp->user_id = auth()->user()->id;
				$temp->option_evaluations_id = $request->Answer;
				$temp -> save();
			}
			$porcentage = CourseProgress::where('user_id',auth()->user()->id)->where('training_id',$request->CourseId)->first();
			if(count($porcentage)<=0)
			{
				$porcentage = new CourseProgress;
				$porcentage ->user_id = auth()->user()->id;
				$porcentage -> training_id = $request->CourseId;
			}
			$cont = $this->Score($request->CourseId);
			$porcentage -> course_grades = $cont;
			$porcentage -> save();
			return response()->json($cont);
		}
	}


	public function result($user, $curso){
		$temp = DB::table('users')->where('id', $user)->first();
		$question = DB::table('question_evaluations')->where('trainings_id', $curso)->get();
		$arr;
		$ansUser;
		$cont = 0;
		$tot = DB::table('trackings')->where('user_id', $user)->first();
		$var = 0;
		if ($tot->course_level != null) {
			$var = $tot->course_level;
		}
		foreach ($question as $q) {
			$aux = 'CAST(validation as SIGNED)';
			$option = DB::table('option_evaluations')->where('question_evaluation_id', $q->id)->orderByRaw($aux,'asc')->get();
			$arr[] = $option;
			$cont += $this->grade($q, $option);
			$ansTmp = DB::table('answer_evaluations')->where('user_id', $user)->where('question_evaluations_id',$q->id)->orderBy('id','asc')->get();
			$ansUser[] = $ansTmp;
		}
		return view('adminlte::courseevaluation.resultsAdmin')->with(['question' => $question, 'option' => $arr, 'answers' => $ansUser, 'count' => $cont, 'tot' => $var, 'usuario' => $temp]);
	}

	public function resultsUser($course){
		$question = DB::table('question_evaluations')->where('trainings_id', $course)->get();
		$cont = 0;
		foreach ($question as $q) {
			$option = DB::table('option_evaluations')->where('question_evaluation_id', $q->id)->get();
			$cont += $this->grade($q, $option);
		}
		$result = CourseProgress::where('user_id',auth()->user()->id)->where('training_id',$course)->first();
		//$this->globalGrades();
		return view('adminlte::courseevaluation.resultsUser')->with(['TrainingUser' => $result,'count' => $cont ]);
	}

	public function globalGrades()
	{
		$usr = DB::table('employees')->where('employee_id', auth()->user()->id)->first();
		$trainingJobs = DB::table('job_title_profile_training_plan as jtp')
		->join('training_training_plan as ttp','ttp.training_plan_id','jtp.training_plan_id')
		->join('trainings','ttp.training_id','trainings.id')->where('job_title_profile_id',$usr->job_title_profile_id)->select('trainings.*','jtp.training_plan_id as TrainingPlan')->get();
		$trainingsUser = DB::table('training_plans as tp')->join('training_plan_user as tpu','tp.id','tpu.training_plan_id')->join('training_training_plan as ttp','tp.id','ttp.training_plan_id')->join('trainings','ttp.training_id','trainings.id')->where('user_id',auth()->user()->id)->select('trainings.*','ttp.training_plan_id as TrainingPlan')->get();

		$training = $trainingJobs->merge($trainingsUser)->unique();

		$course = DB::table('course_progresses')->where('user_id', auth()->user()->id)->where('progress', 100)->where('course_grades', '>=', 80)->count();

		$db = Employee::where('employee_id',auth()->user()->id)->first();
		$db -> course_percentage = ($course * 100) / count($training);
		$db->save();
	}

	public function grade($q, $option){
		$cont = 0;
		switch ($q->type) {
			case TipoRespuesta::Numeric:
			$cont += count($option);
			break;
			default:
			foreach ($option as $o) {
				if ($o->validation == TipoRespuesta::Correcto) 
					$cont++;
				}
				break;
			}
			return $cont;
		}


		public function Score($course){
			$question = DB::table('question_evaluations')->where('trainings_id', $course)->get();
			$count = 0;
			$total = 0;
			foreach ($question as $q) {
				$aux = 'CAST(validation as SIGNED)';
				$option = DB::table('option_evaluations')->where('question_evaluation_id', $q->id)->orderByRaw($aux,'asc')->get();
				$type = $q->type;
				switch ($type) 
				{
					case TipoRespuesta::Numeric :
					$answer = DB::table('answer_evaluations')->where('question_evaluations_id', $q->id)->where('user_id', auth()->user()->id)->orderByRaw('id','asc')->get();
					foreach ($option as $o) {
						$total++;
						for ($i=0; $i < count($answer) ; $i++) { 
							if ($o->id == $answer[$i]->option_evaluations_id && $o->validation == $i+1)
								$count++;
						}
					}
					break;

					case TipoRespuesta::Only:
					$answer = DB::table('answer_evaluations')->where('question_evaluations_id', $q->id)->where('user_id', auth()->user()->id)->first();
					foreach ($option as $o) { 
						if($o->validation == 'si')
							$total++;
						if (isset($answer) && $answer->option_evaluations_id == $o->id && $o->validation == TipoRespuesta::Correcto){
							$count++;
						}
					}
					break;
					
					default:
					$answer = DB::table('answer_evaluations')->where('question_evaluations_id', $q->id)->where('user_id', auth()->user()->id)->get();
					foreach ($option as $o) {
						if($o->validation == 'si')
							$total++;
						foreach ($answer as $a) { 
							if ($a->option_evaluations_id == $o->id && $o->validation == TipoRespuesta::Correcto){
								$count++;
							}
						}
					}

					break;
				}
			}
			DB::table('course_progresses')
			->where('user_id', auth()->user()->id)
			->where('training_id', $course)
			->update(['correct' => $count]);
			return (($count*100)/$total);
		}


		public function CompanyEval(){
			//Consultas gráfica vacantes
			$solicitantes = DB::table('trackings')->whereNotNull('job_title_profile_id')->count();
			$totalsoli = DB::table('trackings')->count();
			$porcentajesoli = ($solicitantes * 100) / $totalsoli;

			$examenpsy = DB::table('trackings')->whereNotNull('psychometric')->count();
			$porcentajepsy = ($examenpsy * 100) / $totalsoli;

			$contratados = DB::table('trackings')->whereNotNull('contract_download')->count();
			$porcentajecontra = ($contratados * 100) / $totalsoli;

			$work_capacities = DB::table('work_capacities')->select(DB::raw('SUM(quantity) as total'))->first();
			
			$employeestotal = DB::table('employees')->where('id_company', 1)->count();
			$progress_total = DB::table('employees')->select(DB::raw('AVG(course_percentage) as promedio'))->first();

			//return view('adminlte::courseevaluation.CompanyEvaluation')->with(['work_capacities' => $work_capacities, 'employees' => $employees]);

		$raw;
		$column;
		$search = "jobcenters";
		$title = $search;
		if ($search == 'jobcenters') {
			$raw = DB::table('tree_job_centers')->where('id',1)->get();
			$region;
			
			$region1 = DB::table('tree_job_centers')->where('tree_job_centers.parent', 1 )->get();
			$regiones = array();
			$zonas = array();
			$determinantes = array();
			$sucursal = array();
			$empleado = array();

			foreach ($region1 as $cont => $region ) {
				$sumzonas=0;

				$zona1 = DB::table('tree_job_centers')->where('parent', $region->id)->get();
				
				foreach ($zona1 as $key => $zona) {
					$det1 = DB::table('tree_job_centers')->where('parent',$zona->id)->get();
					$sumdet = 0;
					
					foreach ($det1 as $contDet => $det) {
						
						$suc1 = DB::table('tree_job_centers')->where('parent', $det->id)->get();
						
						$sum = 0;

						foreach ($suc1 as $contsuc => $suc) {

							$temp = DB::table('employees')->join('profile_job_centers as jobTreeCenter','jobTreeCenter.id','employees.profile_job_center_id')->where('jobTreeCenter.profile_job_centers_id', $suc->id_inc)->select(DB::raw('AVG(course_percentage) as promedio'))->first();
							
							$promedio = 0;
							if($temp->promedio != null)
								$promedio = $temp->promedio;
							array_push($sucursal, ['text' => $suc->text , 'id' => $suc->id, 'parent_id' => $det->id, 'parent' => $det->text, 'promedio' => $promedio]);
							$emp1 = DB::table('employees')->join('profile_job_centers as jobTreeCenter','jobTreeCenter.id','employees.profile_job_center_id')->where('jobTreeCenter.profile_job_centers_id', $suc->id_inc)->get();
							
							foreach ($emp1 as $contemp => $emp) {
								$empleado1 = DB::table('employees')->where('employee_id',$emp->employee_id)->first();	
								
								array_push($empleado, ['text' => $empleado1->name , 'id' => $empleado1->employee_id, 'parent_id' => $suc->id, 'promedio' => $empleado1->course_percentage]);

							}

						}
						

						for ($i=0; $i < count($sucursal); $i++) { 
							if($sucursal[$i]['parent_id'] == $det->id)
								$sum += (float) $sucursal[$i]['promedio'];
						}
						if(count($suc1)>0)
						array_push($determinantes, ['text' => $det->text , 'id' => $det->id, 'parent_id' => $zona->id, 'parent' => $zona->text, 'promedio' => ($sum / count($suc1))]);
					else
						array_push($determinantes, ['text' => $det->text , 'id' => $det->id, 'parent_id' => $zona->id, 'parent' => $zona->text, 'promedio' => 0]);
					}
					for ($i=0; $i < count($determinantes); $i++) { 
						if($determinantes[$i]['parent_id'] == $zona->id)
							$sumdet += (float) $determinantes[$i]['promedio'];
					}
					if(count($det1)>0){
						array_push($zonas, ['text' => $zona->text , 'id' => $zona->id, 'parent_id' => $region->id, 'parent' => $region->text, 'promedio' => ($sumdet / count($det1))]);
					}
					else{
						array_push($zonas, ['text' => $zona->text , 'id' => $zona->id, 'parent_id' => $region->id, 'parent' => $region->text, 'promedio' => 0]);	
					}
				}
				for ($i=0; $i < count($zonas); $i++) { 
					if($zonas[$i]['parent_id'] == $region->id)
						$sumzonas += (float) $zonas[$i]['promedio'];
				}
				array_push($regiones, ['text' => $region->text , 'id' => $region->id, 'parent' => 'Circle K', 'promedio' => ($sumzonas / count($zona1))]);
			}
			$column = $regiones;
						
						return view('adminlte::courseevaluation.CompanyEvaluation', ['porcentajecontra'=>$porcentajecontra,'porcentajepsy'=>$porcentajepsy,'porcentajesoli'=>$porcentajesoli,'column' => $column,'zonas' => $zonas, 'determinantes' => $determinantes, 'sucursal' => $sucursal,'empleado' => $empleado, 'title' => $search, 'work_capacities' => $work_capacities, 'employeestotal' => $employeestotal, 'progress_total' => $progress_total ]);
		}
		elseif ($search == 'jobtitles'){
			$raw = DB::table('tree_job_profiles')->get();
			$column = DB::table('employees')->join('job_title_profiles as jobTitle','jobTitle.id','employees.job_title_profile_id')->select('jobTitle.job_title_profiles_id as jobCenterID', DB::raw('AVG(course_percentage) as promedio'))->groupBy('job_title_profiles_id')->get();
		}
		
		//return view('adminlte::employees.generalsearch', ['raw' => $raw, 'column' => $column, 'title' => $title]);
	}



			





		






		// public function CompanyEval(){
		// 	$user = DB::table('employees')->select('job_title_profile_id', 'profile_job_center_id')->where('id_company', 1)->get();
		// 	$puestos = DB::table('job_title_profiles')->get();
		// 	$sucursales = DB::table('profile_job_centers')->get();
		// 	$person = DB::table('trackings')->whereNotNull('lms')->count();
		// 	$totPerson = DB::table('trackings')->count();
		// 	$puesto = array();
		// 	foreach ($sucursales as $suc) {
		// 		foreach ($puestos as $puest) {
		// 			$cont = 0 ;
		// 			foreach ($user as $usr) {
		// 				if ($puest->id == $usr->job_title_profile_id && $suc->id == $usr->profile_job_center_id) {
		// 					$cont++;
		// 				}
		// 			}
		// 			array_push($puesto, [$puest->id,$cont,$suc->id]);
		// 		}
		// 	}
		// 	$aux = DB::table('profile_job_centers as pjc')->join('work_capacities as wc','pjc.id', 'wc.profile_job_centers_id')->join('job_title_profiles as jtp','jobtitle_capacity','jtp.id')->select('pjc.id as jobCenter','job_title_name as jobTitle', 'jtp.id as jobTitleId', 'quantity')->get();
		// 	$sucursal = DB::table('tree_job_centers')->where('parent','like','j%')->get();
		// 	$tempo = DB::table('work_capacities')->get();
		// 	return view('adminlte::courseevaluation.CompanyEvaluation')->with(['total' => $puesto, 'puestos' => $puestos, 'sucursales' => $sucursales, 'sucursal' => $sucursal, 'temporal' => $tempo, 'person' => $person, 'totPerson' => $totPerson, 'auxiliares' => $aux]);
		// }
	}
	abstract class TipoRespuesta{
		const Multi = 'multioption';
		const Only = 'only';
		const Numeric = 'order';
		const Correcto = 'si';
	}