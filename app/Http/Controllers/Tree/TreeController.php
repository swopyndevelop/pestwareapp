<?php

namespace App\Http\Controllers\Tree;

use App\employee;
use App\Http\Controllers\Instance\CatalogsJobCenterController;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Notifications\LogsNotification;
use App\profile_job_center;
use App\User;
use Hashids\Hashids;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\treeJobCenter;

class TreeController extends Controller
{
    public function create(){
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        return view('vendor.adminlte.tree.treejobcenter')->with(['treeJobCenterMain' => $treeJobCenterMain]);
    }
    
    public function read(){
        $tree = DB::table('tree_job_centers')->select('id_inc', 'id', 'company_id', 'parent','text')->where('company_id', Auth::user()->companie)->get();
        $args = array();
        foreach ($tree as $key => $dat) {
            $row = array();
            $row['id']= $dat->id;
            $row['parent'] = $dat->parent;
            $row['text'] = $dat->text;
            $row['a_attr']['id_test'] = $dat->id_inc;
            $row['a_attr']['id_hash'] = SecurityController::encodeId($dat->id_inc);
            array_push($args, $row);
        }
        return Response::json($args);
    }
    public function write(Request $request){

        try {
            DB::beginTransaction();
            $tmp = TreeJobCenter::where('company_id', Auth::user()->companie)->get();
            $companie = DB::table('companies')->where('id_company', Auth::user()->companie)->first();
            if( $request->ajax()){
                $data = $request->json()->all();
                if (count($data) > $companie->no_branch_office) {
                    return \response()->json([
                        'code' => 500,
                        'message' => 'Debes contactar a soporte para adquirir una nueva sucursal. (Sucursal extra $499MXN/29USD)'
                    ]);
                } else {
                    $this->delete($data, $tmp);
                    $this->update($data, $tmp);
                    $this->add($data);
                }
            }
            DB::commit();
            return \response()->json([
                'code' => 201,
                'message' => 'Sucursal Guardada Correctamente.'
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            User::first()->notify(new LogsNotification($exception->getMessage(), $exception->getLine(), 3));
            return \response()->json([
                'code' => 500,
                'message' => $exception->getMessage() //'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    public function update($data, $tmp)
    {
        foreach ($tmp as $t) {
            foreach ($data as $datas) {
                if(isset($datas['a_attr']['id_test'])){
                    if($t->id_inc == $datas['a_attr']['id_test']){
                        
                        DB::table('tree_job_centers')->where('id_inc', $t->id_inc)->where('company_id', Auth::user()->companie)
                            ->update(['id' => $datas['id'], 'parent' => $datas['parent'], 'text' => $datas['text']]);

                        //update job center
                        $jobtitle = profile_job_center::where('profile_job_centers_id', $t->id_inc)->first();
                        if (!empty($jobtitle)) {
                            $jobtitle->name = $datas['text'];
                            $jobtitle->save();
                        }
                    }
                }
            }
        }
    }

    public function delete($data, $tmp)
    {
        foreach ($tmp as $tmp) {
            $is_Contained = false;
            foreach ($data as $d) {
                if(isset($d['a_attr']['id_test'])){
                    if ( ($tmp->id_inc == $d['a_attr']['id_test']))
                        $is_Contained = true;
                }
            }
            if(!$is_Contained){
                $del = TreeJobCenter::where('id_inc', '=', $tmp->id_inc)->delete();
                $job = profile_job_center::where('profile_job_centers_id', $tmp->id_inc)->delete();
            }
        }   
    }

    public function add($data)
    {
        foreach ($data as $var) {
            if (!isset($var['a_attr']['id_test'])) {
                $update = new TreeJobCenter;
                $update->id = $var['id'];
                $update->parent = $var['parent'];
                $update->company_id = Auth::user()->companie;
                $update->text = $var['text'];
                $update->save();

                //create job title profile
                $jobtitle = new profile_job_center;
                $jobtitle->name = $var['text'];
                $jobtitle->profile_job_centers_id = $update->id;
                $jobtitle->companie = Auth::user()->companie;
                $jobtitle->save();

                //create replicate data parent
                $userId = Auth::user()->id;
                CatalogsJobCenterController::UpdateCatalogs($jobtitle->id, $userId);
            }
        }
    }
}
