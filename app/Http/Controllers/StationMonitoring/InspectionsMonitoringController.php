<?php

namespace App\Http\Controllers\StationMonitoring;

use App\address_job_center;
use App\Common\CommonImage;
use App\companie;
use App\customer;
use App\employee;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\ReportsPDF\ServiceCertificate;
use App\Http\Controllers\ReportsPDF\ServiceOrder;
use App\Http\Controllers\ReportsPDF\StationInspectionTable;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Mail\InspectionMail;
use App\Mail\PurchaseOrderMail;
use App\Mail\ServiceMail;
use App\Monitoring;
use App\profile_job_center;
use App\quotation;
use App\service_order;
use App\StationInspection;
use App\treeJobCenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class InspectionsMonitoringController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function show($jobcenter = 0, Request $request)
    {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $idJobCenter = 0;
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        // Filters Inspections
        $customers = customer::where('id_profile_job_center', $profileJobCenter)->get();
        $addressCustomers = DB::table('customers as c')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->select('c.id', 'cd.address', 'cd.address_number', 'c.colony','cd.state','c.municipality')
            ->where('c.id_profile_job_center', $profileJobCenter)
            ->get();

        $search = \Request::get('search');
        $inspections = DB::table('station_inspections as si')
            ->join('monitorings as mo', 'si.id_monitoring', 'mo.id')
            ->join('employees as em', 'si.id_technician', 'em.id')
            ->join('service_orders as so', 'si.id_service_order', 'so.id')
            ->join('customers as cu', 'mo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
            ->select('si.id_inspection', 'mo.id_monitoring', 'si.date', 'si.hour', 'em.name as technician',
                'cu.name as customer', 'cu.establishment_name', 'cu.colony', 'cu.municipality', 'cd.address',
                'cd.address_number', 'cd.state', 'mo.id as id_monitoring_inc', 'si.id_service_order', 'si.id',
                'so.id_service_order as order', 'cu.id_profile_job_center','cd.email','cu.cellphone','cu.cellphone_main',
                'cu.id as id_customer')
            ->where('cu.id_profile_job_center', $profileJobCenter)
            ->where('cu.name', 'like', '%' . $search . '%');

        $initialDateInspection = $request->get('initialDateInspection');
        if (!$request->exists('initialDateInspection')) $initialDateInspection = "null";
        $finalDateInspection = $request->get('finalDateInspection');
        if (!$request->exists('finalDateInspection')) $finalDateInspection = "null";
        $idMonitoringInspection = $request->get('idMonitoringInspection');
        $customersFilter = $request->get('customersFilter');
        $addressFilter = $request->get('addressFilter');

        if ($idMonitoringInspection != 'null' ) $inspections->where('mo.id_monitoring','like','%'. $idMonitoringInspection . '%');
        if ($initialDateInspection != "null" && $finalDateInspection != "null") {
            $inspections
                ->whereDate('si.created_at', '>=', $initialDateInspection)
                ->whereDate('si.created_at', '<=', $finalDateInspection);
        }
        if ($customersFilter != 0) $inspections->where('cu.id', $customersFilter);
        if ($addressFilter != 0) $inspections->where('cu.id', $addressFilter);

        $inspectionsFilter = $inspections->orderBy('si.id')->paginate(10)->appends([
            'initialDateInspection' => $initialDateInspection,
            'finalDateInspection' => $finalDateInspection,
            'idMonitoringInspection' => $idMonitoringInspection,
            'customersFilter' => $customersFilter,
            'addressFilter' => $addressFilter
        ]);

        foreach ($inspectionsFilter as $inspection) {
            $inspectionsFilter->map(function($inspection){
                $totals = DB::table('monitoring_trees as mt')
                    ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
                    ->join('type_areas as ta', 'mn.id_type_area', 'ta.id')
                    ->where('mt.id_monitoring', $inspection->id_monitoring_inc)
                    ->where('ta.type', 'station')
                    ->groupBy(['mn.name'])
                    ->select(DB::raw('count(*) as count'), 'mn.name')
                    ->get();
                $images = DB::table('check_list_images')->where('id_inspection', $inspection->id)->get();
                foreach ($images as $image) {
                    $images->map(function ($image) {
                        $image->url_s3 = CommonImage::getTemporaryUrl($image->urlImage, 5);
                    });
                }
                $inspection->nodes = $totals;
                $inspection->stations = self::getStationsMonitoringByInspection($inspection->id);
                $inspection->photos = $images;
                $company = DB::table('companies')->where('id', Auth::user()->companie)->first();
                $countryCode = DB::table('countries')->where('id', $company->id_code_country)->first();
                $inspection->urlWhatsappEmailInspection = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" . "%0d%0dMonitoreo de Estaciones: https://pestwareapp.com/station/monitoring/pdf/" . SecurityController::encodeId($inspection->id_service_order);
            });
        }

        return view('vendor.adminlte.stationmonitoring.inspections')->with([
            'inspections' => $inspectionsFilter,
            'jobCenters' => $jobCenters,
            'profileJobCenterSelect' => $profileJobCenterSelect,
            'treeJobCenterMain' => $treeJobCenterMain,
            'jobCenterSession' => $jobCenterSession,
            'customers' => $customers,
            'addressCustomers' => $addressCustomers
        ]);
    }

    public function stationMonitoringPdf($id)
    {
        $id = SecurityController::decodeId($id);
        //Validate plan free
        //if (Auth::user()->id_plan == $this->PLAN_FREE) SecurityController::abort();

        $isInspection = StationInspection::where('id_service_order', $id)->first();
        if ($isInspection) {
            $idCompany = service_order::where('id', $id)->first()->companie;
            $dataStationInspection = StationInspectionTable::build($id, $idCompany);
            $pdf = PDF::loadView('vendor.adminlte.stationmonitoring._stationMonitoringPdf', $dataStationInspection)
                ->setPaper('A4');
            return $pdf->stream();
        } else return redirect()->route('index_inspections_monitoring');
    }

    public function mailInspection(Request $request)
    {
        try {
        $id = SecurityController::decodeId($request->get('id'));
        // Build service inspection and get data.
        $idCompany = service_order::where('id', $id)->first()->companie;
        $employee = employee::join('profile_job_centers as pjc','profile_job_center_id','pjc.id')
            ->where('companie', Auth::user()->companie)
            ->first();
        $profileJobCenter = profile_job_center::find($employee->profile_job_center_id);
        $dataStationInspection = StationInspectionTable::build($id, $idCompany);
        $company = CommonCompany::getCompanyById($idCompany);
        $banner = DB::table('personal_mails')->where('id_company',$company->id)->first();
        $country_code = DB::table('countries')->where('id', $company->id_code_country)->first();
        $whatsapp = 'https://api.whatsapp.com/send?phone='. $country_code->code_country .$profileJobCenter->whatsapp_personal;
        $emails = $request->get('emails');

        $pdf = PDF::loadView('vendor.adminlte.stationmonitoring._stationMonitoringPdf', $dataStationInspection);
        config(['mail.from.name' => $profileJobCenter->name]);

        foreach ($emails as $email) {
            $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
            if($result != false){
                Mail::to($email)->send(new InspectionMail($pdf->output(), $banner, $whatsapp, $dataStationInspection));
            }
        }
        return \response()->json([
            'code' => 201,
            'message' => 'Inspección Enviada'
        ]);
        }
        catch (\Exception $e){
            return \response()->json([
            'code' => 500,
            'message' => $e->getMessage() //'Algo salió mal, intentalo de nuevo'
            ]);
        }
    }

    private function getStationsMonitoringByInspection($inspectionId) {
         $stations = DB::table('check_monitoring_responses as cmr')
            ->join('monitoring_trees as mt', 'cmr.id_station', 'mt.id')
            ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
            ->where('cmr.id_inspection', $inspectionId)
             ->select('mt.id as id_node_tree', 'cmr.*', 'mt.*', 'mn.*', 'cmr.id as id_response_station')
            ->get();
        $stations->map(function($station) use ($inspectionId) {
            $perimeter = DB::table('monitoring_trees')
                ->where('id_monitoring', $station->id_monitoring)
                ->where('id_node', $station->parent)
                ->first();
            $perimeterText = $perimeter->text;
            $zone = DB::table('monitoring_trees')
                ->where('id_monitoring', $station->id_monitoring)
                ->where('id_node', $perimeter->parent)
                ->first();
            $zoneText = $zone->text;
            $station->perimeter = $perimeterText;
            $station->zone = $zoneText;
            $conditions = DB::table('station_conditions as sc')
                ->join('monitoring_conditions as mc', 'sc.id_condition', 'mc.id')
                ->where('sc.id_node', $station->id_node_tree)
                ->where('sc.id_inspection', $inspectionId)
                ->select('mc.name')
                ->get();
            $station->conditions = $conditions;
            $plagues = DB::table('plagues_response_inspection as pri')
                ->join('plague_types as pt', 'pri.id_plague', 'pt.id')
                ->where('pri.id_station_response', $station->id_response_station)
                ->select('pt.name as plague', 'pri.quantity as quantity_plague')
                ->get();
            $station->plagues = $plagues;
            $station->plagues_count = $plagues->count();
        });
        return $stations;
    }
}
