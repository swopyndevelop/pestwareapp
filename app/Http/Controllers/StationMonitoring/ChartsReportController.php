<?php

namespace App\Http\Controllers\StationMonitoring;

use App\customer;
use App\Http\Controllers\Business\CommonCompany;
use App\profile_job_center;
use Auth;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ChartsReportController extends Controller
{
    public function reportStationBaits($customerId, $nameImage, $period)
    {
        $companyId = customer::find($customerId)->companie;
        $data = $this->getCommonData($customerId, $companyId);

        $pdf = PDF::loadView('vendor.adminlte.register.INFORM.reportDetailPlagueMonitoringStationInstance', [
            'addressProfile' => $data['addressJobCenter'],
            'image' => $data['image'],
            'qrcode' => $data['qrcode'],
            'symbol_country' => $data['symbolCountry'],
            'customer' => $data['customer'],
            'period' => $period,
            'nameImage' => $nameImage
        ])->setPaper('A4');
        return $pdf->stream('Reporte Gráfico.pdf');
    }

    public function reportAllStations($customerId, $nameImage, $period)
    {
        if ($customerId == 0) {
            $customers = DB::table('monitorings as m')
                ->join('customers as c', 'm.id_customer', 'c.id')
                ->where('c.id_profile_job_center', CommonCompany::getProfileJobCenterId())
                ->select('c.id', 'c.name', 'c.establishment_name')
                ->get();
            $customerId = $customers[0]->id;
        }

        $companyId = customer::find($customerId)->companie;

        $data = $this->getCommonData($customerId, $companyId);

        PDF::loadView('vendor.adminlte.register.INFORM.reportDetailPlagueMonitoringStationInstance', [
            'addressProfile' => $data['addressJobCenter'],
            'image' => $data['image'],
            'qrcode' => $data['qrcode'],
            'symbol_country' => $data['symbolCountry'],
            'customer' => $data['customer'],
            'period' => $period,
            'nameImage' => $nameImage,
            'title' => 'Estación de Cebadero',
            'station' => 'Baits',
            'jobCenterProfile' => $data['jobCenterProfile']
        ])->save(storage_path() . '/app/charts/Baits-' . $nameImage . '.pdf');

        PDF::loadView('vendor.adminlte.register.INFORM.reportDetailPlagueMonitoringStationInstance', [
            'addressProfile' => $data['addressJobCenter'],
            'image' => $data['image'],
            'qrcode' => $data['qrcode'],
            'symbol_country' => $data['symbolCountry'],
            'customer' => $data['customer'],
            'period' => $period,
            'nameImage' => $nameImage,
            'title' => 'Trampa de Captura',
            'station' => 'Capture',
            'jobCenterProfile' => $data['jobCenterProfile']
        ])->save(storage_path() . '/app/charts/Capture-' . $nameImage . '.pdf');

        PDF::loadView('vendor.adminlte.register.INFORM.reportDetailPlagueMonitoringStationInstance', [
            'addressProfile' => $data['addressJobCenter'],
            'image' => $data['image'],
            'qrcode' => $data['qrcode'],
            'symbol_country' => $data['symbolCountry'],
            'customer' => $data['customer'],
            'period' => $period,
            'nameImage' => $nameImage,
            'title' => 'Trampa de Luz UV',
            'station' => 'Light',
            'jobCenterProfile' => $data['jobCenterProfile']
        ])->save(storage_path() . '/app/charts/Light-' . $nameImage . '.pdf');

        PDF::loadView('vendor.adminlte.register.INFORM.reportDetailPlagueMonitoringStationInstance', [
            'addressProfile' => $data['addressJobCenter'],
            'image' => $data['image'],
            'qrcode' => $data['qrcode'],
            'symbol_country' => $data['symbolCountry'],
            'customer' => $data['customer'],
            'period' => $period,
            'nameImage' => $nameImage,
            'title' => 'Control de Insectos',
            'station' => 'Insects',
            'jobCenterProfile' => $data['jobCenterProfile']
        ])->save(storage_path() . '/app/charts/Insects-' . $nameImage . '.pdf');

        $pathBaits = 'charts/Baits-' . $nameImage . '.pdf';
        $pathCapture = 'charts/Capture-' . $nameImage . '.pdf';
        $pathLight = 'charts/Light-' . $nameImage . '.pdf';
        $pathInsects = 'charts/Insects-' . $nameImage . '.pdf';

        $reports = [$pathBaits];
        $Capture = [$pathCapture];
        $Light = [$pathLight];
        $Insects = [$pathInsects];

        $reports = array_merge($reports, $Capture);
        $reports = array_merge($reports, $Light);
        $reports = array_merge($reports, $Insects);

        $merger = new Merger(new TcpdiDriver);
        foreach ($reports as $report) {
            $merger->addFile(storage_path() . '/app/' . $report);
        }
        $reportFinal = $merger->merge();

        $name = "Reporte Gráfico - " . $data['customer']->name . ".pdf";

        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=$name");
        echo $reportFinal;
        exit;

    }

    private function getCommonData($customerId, $companyId)
    {
        $symbolCountry = CommonCompany::getSymbolByCountryWithCompanyId($companyId);
        $image = DB::table('companies')
            ->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook', 'warnings_service',
                'contract_service', 'pdf_sanitary_license', 'rfc','bussines_name', 'health_manager','email')
            ->where('id', $companyId)
            ->first();
        $profileJobCenter = DB::table('profile_job_centers')->where('companie', $companyId)->first();
        $sanitaryLicenseQR = env('URL_STORAGE_FTP') . $profileJobCenter->sanitary_license;
        $jobCenterProfile = profile_job_center::where('id', $profileJobCenter->id)->first();
        $addressProfile = DB::table('address_job_centers')->where('profile_job_centers_id', $profileJobCenter->profile_job_centers_id)->first();
        $customer = DB::table('customers as c')
            ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
            ->where('c.id', $customerId)
            ->select('c.*', 'cd.address', 'cd.state', 'cd.email', 'cd.address_number')
            ->first();
        if ($profileJobCenter->sanitary_license != null)
            $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitaryLicenseQR));
        else $qrcode = null;
        return [
            'image' => $image,
            'jobCenterProfile' => $jobCenterProfile,
            'addressJobCenter' => $addressProfile,
            'customer' => $customer,
            'symbolCountry' => $symbolCountry,
            'qrcode' => $qrcode
        ];
    }
}
