<?php

namespace App\Http\Controllers\StationMonitoring;

use App\companie;
use App\customer;
use App\CustomQr;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ReportsPDF\MonitoringQr;
use App\Http\Controllers\Security\SecurityController;
use App\Jobs\DownloadMonitoringQr;
use App\Monitoring;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CustomQrController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function storeCustomQr(Request $request)
    {
        try {
            $customQr = new CustomQr;
            $customQr->id_monitoring = 1;
            $customQr->title = $request->get('title');
            $customQr->subtitle = $request->get('subtitle');
            $customQr->description = $request->get('description');
            $customQr->save();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);
        }
        catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal intente de nuevo'
            ]);
        }
    }

    public function printQr($id)
    {
        $id = SecurityController::decodeId($id);
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $companyId = Auth::user()->companie;
        $totalsStations = DB::table('monitoring_trees as mt')
            ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
            ->join('type_areas as ta', 'mn.id_type_area', 'ta.id')
            ->where('mt.id_monitoring', $id)
            ->where('ta.type', 'station')
            ->select('mn.name', 'mt.id as id_node', 'mt.parent', 'mt.text', 'mt.id', 'mt.id_monitoring')
            ->get();

        if ($totalsStations->count() > 20) {
            $monitoring = Monitoring::find($id);
            Storage::makeDirectory('monitoring_qrs/' . $monitoring->id_monitoring);
            $parts = $totalsStations->chunk(45);
            foreach ($parts as $i => $part) {
                //Segundo plano
                DownloadMonitoringQr::dispatch($id, $companyId, $i);
            }
            $monitoring->status = '1';
            $monitoring->date_updated_file = Carbon::now()->toDateTimeString();
            $monitoring->no_parts = $parts->count();
            $monitoring->save();

            return redirect()->route('index_station_monitoring')
                ->with('success', 'Tu reporte se esta generando, lo podrás descargar una vez terminado el proceso.');
        }

        else {

            $monitoringData = MonitoringQr::build($id, $companyId, -1);
            $storagePath = storage_path() . '/app/monitoring_qrs/';

            if ($companyId == 646){
                PDF::loadView('vendor.adminlte.stationmonitoring._printQr3', ['rows'=> $monitoringData['rows'],
                    'customer' => $monitoringData['customer'], 'pdfLogo' => $monitoringData['pdfLogo']])->setPaper('letter')
                    ->save($storagePath. $monitoringData['folio'] . '.pdf');
            }
            else {
                if (count($monitoringData['stations']) == 1){
                  PDF::loadView('vendor.adminlte.stationmonitoring._printQr1', ['rows'=> $monitoringData['rows'],
                      'customer' => $monitoringData['customer'], 'pdfLogo' => $monitoringData['pdfLogo']])->setPaper('letter')
                      ->save($storagePath. $monitoringData['folio'] . '.pdf');

              }
              else if(count($monitoringData['stations']) == 2){
                  PDF::loadView('vendor.adminlte.stationmonitoring._printQr2', ['rows'=> $monitoringData['rows'],
                      'customer' => $monitoringData['customer'], 'pdfLogo' => $monitoringData['pdfLogo']])->setPaper('letter')
                      ->save($storagePath. $monitoringData['folio'] . '.pdf');
              }
              else {
                  PDF::loadView('vendor.adminlte.stationmonitoring._printQr', ['rows'=> $monitoringData['rows'],
                      'customer' => $monitoringData['customer'], 'pdfLogo' => $monitoringData['pdfLogo']])->setPaper('letter')
                      ->save($storagePath. $monitoringData['folio'] . '.pdf');
              }
            }

            $monitoring = Monitoring::find($id);
            $monitoring->status = '2';
            $monitoring->date_updated_file = Carbon::now()->toDateTimeString();
            $monitoring->save();

            try {
                $reportFinal = Storage::get('monitoring_qrs/' . $monitoringData['folio'] . '.pdf');
                Storage::disk('s3')->put('/station_monitoring/qrs/' . $monitoringData['folio'] . '.pdf', $reportFinal);
                Storage::delete('monitoring_qrs/' . $monitoringData['folio'] . '.pdf');
                return redirect()->route('index_station_monitoring')
                    ->with('success', 'Tu reporte se ha generado correctamente.');
            } catch (Exception $exception) {
                return redirect()->route('index_station_monitoring')
                    ->with('warning', 'Algo salio mal, intenta más tarde.');
            }
        }
    }
}
