<?php

namespace App\Http\Controllers\StationMonitoring;

use App\Area_tree;
use App\custom_quote;
use App\customer;
use App\customer_branche;
use App\employee;
use App\Http\Controllers\Api\CommonMonitoring;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Monitoring;
use App\MonitoringNode;
use App\MonitoringTree;
use App\product;
use App\profile_job_center;
use App\StationInspection;
use App\storehouse;
use App\treeJobCenter;
use App\TypeArea;
use App\TypeStation;
use Carbon\Carbon;
use DB;
use Entrust;
use Exception;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class StationMonitoringController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function show($jobcenter = 0) {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $idJobCenter = 0;
        $jobCenters = \Illuminate\Support\Facades\DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        $search = \Request::get('search');
        // Checar variable customers que se carguen bien los datos
        $customers = customer::where('id_profile_job_center', $profileJobCenter)->get();
        $monitorings = DB::table('monitorings as m')
            ->join('users as u', 'm.id_user', 'u.id')
            ->join('customers as c', 'm.id_customer', 'c.id')
            ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
            ->where('c.id_profile_job_center', $profileJobCenter)
            ->select('m.id', 'm.id_monitoring', 'm.id_customer_branch','m.created_at', 'u.name as user', 'c.name as customer',
                'c.establishment_name', 'c.colony', 'c.municipality', 'cd.address', 'cd.address_number', 'cd.state',
                'm.status', 'm.date_updated_file', 'm.date_updated_tree', 'c.id as customer_id', 'm.no_parts')
            ->where('c.name', 'like', '%' . $search . '%')
            ->orderBy('c.name')
            ->paginate(10);

        $customersCollection = new Collection();
        foreach ($customers as $customer) {
            $monitoring = DB::table('monitorings')
                ->where('id_customer', $customer->id)
                ->first();
            $quotationSchedule = DB::table('quotations')->where('id_customer', $customer->id)->where('id_status', '<>',10)->first();
            if ($monitoring && $quotationSchedule) {
                if ($monitoring->id_customer_branch != 0 || $monitoring->id_customer_branch != null) {
                    $customerBranches = customer_branche::where('customer_id', $customer->id)->get();
                    $monitoringBranches = DB::table('monitorings')
                        ->where('id_customer', $customer->id)
                        ->get();
                    if ($customerBranches->count() != $monitoringBranches->count()) $customersCollection->push($customer);
                }
            } else {
                if (!$monitoring && $quotationSchedule) $customersCollection->push($customer);
            }

        }

        foreach ($monitorings as $m) {
            $monitorings->map(function($m){
                $totals = DB::table('monitoring_trees as mt')
                    ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
                    ->join('type_areas as ta', 'mn.id_type_area', 'ta.id')
                    ->where('mt.id_monitoring', $m->id)
                    ->where('ta.type', 'station')
                    ->groupBy(['mn.name'])
                    ->select(DB::raw('count(*) as count'), 'mn.name')
                    ->get();
                $lastInspection = DB::table('station_inspections')
                    ->where('id_monitoring', $m->id)
                    ->latest()
                    ->first();
                $m->CustomerBranchId = 0;
                if($m->id_customer_branch != 0 || $m->id_customer_branch != null) {
                    $customerBranches = customer_branche::find($m->id_customer_branch);
                    $m->CustomerBranchName = $customerBranches->name;
                    $m->CustomerBranchId = $customerBranches->id;
                    $m->customerBranch = $customerBranches;
                }
                $m->nodes = $totals;
                $m->lastInspection = $lastInspection;
                if ($m->status == 1 && $m->no_parts == null) {
                    $file = Storage::disk('s3')->exists('/station_monitoring/qrs/'. $m->id_monitoring . '.pdf');
                    if ($file) {
                        $monitoring = Monitoring::find($m->id);
                        $monitoring->status = '2';
                        $monitoring->save();
                        $m->status = 2;
                    }
                }
                if ($m->status == 2) {
                    $m->url_pdf ='monitoring_qrs/'. $m->id_monitoring . '.pdf';
                    $updatedAt = Carbon::parse($m->date_updated_tree);
                    $dateUpdatedFile = Carbon::parse($m->date_updated_file);
                    $diff = false;
                    if($updatedAt > $dateUpdatedFile) $diff = true;
                    $m->diffInSeconds = $diff;
                }
                else $m->url_pdf = null;
            });
        }

        return view('vendor.adminlte.stationmonitoring.index')
            ->with(['customers' => $customersCollection, 'monitorings' => $monitorings, 'jobCenters' => $jobCenters, 'profileJobCenterSelect' => $profileJobCenterSelect,
            'treeJobCenterMain' => $treeJobCenterMain, 'jobCenterSession' => $jobCenterSession]);
    }

    public function download($id)
    {
        $id = SecurityController::decodeId($id);
        $monitoring =  Monitoring::find($id);
        $route = 'station_monitoring/qrs/' . $monitoring->id_monitoring . '.pdf';
        return Storage::disk('s3')->response($route);
    }

    public function saveTree(Request $request) {
        try {
            $data = json_decode($request->get('tree'));
            if (!$this->validTreeStructure($data)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'El formato debe ser: ZONA > PERÍMETRO > TRAMPA(S).'
                ]);
            }
            $customerId = $request->get('customerId');
            //$monitoringId = $this->addMonitoring($customerId);
            //$this->addNodes($monitoringId, $data);
            return response()->json([
                'code' => 201,
                'message' => 'Datos guardados.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intenta de Nuevo.'
            ]);
        }
    }

    public function editTree(Request $request) {
        try {
            $data = json_decode($request->get('tree'));
            $monitoringId = $request->get('monitoringId');
            $oldTree = DB::table('monitoring_trees')
                ->where('id_monitoring', $monitoringId)
                ->where('parent', '<>', '#')
                ->get();

            $this->updateTree($oldTree, $data, $monitoringId);

            $monitoring = Monitoring::find($monitoringId);
            $monitoring->date_updated_tree = Carbon::now()->toDateTimeString();
            $monitoring->save();

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados.'
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

    private function addMonitoring($customerId, $customerBranchId) {
        $monitoring = new Monitoring;
        $monitoring->id_monitoring = $customerBranchId == 0 ? 'M-' . $customerId : 'M-' . $customerId . '-' . $customerBranchId;
        $monitoring->id_customer = $customerId;
        $monitoring->id_customer_branch = $customerBranchId;
        $monitoring->id_user = Auth::user()->id;
        $monitoring->date_updated_tree = Carbon::now()->toDateTimeString();
        $monitoring->save();
        return $monitoring->id;
    }

    private function addNodes($monitoringId, $data)
    {
        foreach ($data as $i => $node) {
            $this->addNode($node, $monitoringId, $i);
        }
    }

    private function addNode($node, $monitoringId, $i) {
        $update = new MonitoringTree;
        $update->id_monitoring = $monitoringId;
        $update->id_company = Auth::user()->companie;
        $update->id_node = $node->id;
        $update->parent = $node->parent;
        $update->text = trim($node->text);
        $update->save();

        $type = 'Área';
        if ($i != 0) $type = $node->type_node;
        $area = TypeArea::where('name', $type)->first();

        $nodes = new MonitoringNode;
        $nodes->id_monitoring_tree = $update->id;
        $nodes->id_type_area = $area->id;
        $nodes->name = $type;
        $nodes->save();
    }

    private function updateTree($oldTree, $newTree, $monitoringId)
    {
        foreach ($newTree as $newNode)
        {
            $isContained = false;
            foreach ($oldTree as $oldNode)
            {
                if ($newNode->parent != "#") {
                    if ($oldNode->text == trim($newNode->text)) {
                        DB::table('monitoring_trees')
                            ->where('id', $oldNode->id)
                            ->where('id_company', Auth::user()->companie)
                            ->update([
                                'id_node' => $newNode->id,
                                'parent' => $newNode->parent,
                                'text' => $newNode->text
                            ]);
                        $isContained = true;
                    }
                }
            }
            if (!$isContained && $newNode->parent != "#") $this->addNode($newNode, $monitoringId, 1);
        }
    }

    private function validTreeStructure($data)
    {
        // Validate min structure:
        if (count($data ) >= 4) {
            $isZone = false;
            $isPerimeter = false;
            $isStation = false;
            foreach ($data as $node) {
                if ($node->parent != '#') {
                    if ($node->type_node == 'Zona' && $node->parent == '1') $isZone = true;
                    if ($node->type_node == 'Perímetro' && $node->parent == 'j1_2') $isPerimeter = true;
                    if ($node->type_node == 'Cebadero' || $node->type_node == 'Trampa de Luz' ||
                        $node->type_node == 'Trampa de Captura' && $node->parent == 'j1_3') $isStation = true;
                }
            }
            if (!$isZone || !$isPerimeter || !$isStation) return false;
            else return true;
        } else return false;
    }

    public function getTypeAreaByNodeId($monitoringId) {
        return DB::table('monitoring_trees as mt')
            ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
            ->where('mt.id_monitoring', $monitoringId)
            ->select('mt.id', 'mn.id_type_area', 'mt.text')
            ->get();
    }

    public function read($id){
        $tree = DB::table('monitoring_trees')
            ->select('id_node', 'id', 'parent', 'text')
            ->where('id_monitoring', $id)
            ->where('id_company', Auth::user()->companie)
            ->where('visible', 1)
            ->get();
        $args = array();
        foreach ($tree as $key => $dat) {
            $row = array();
            $row['id']= $dat->id_node;
            $row['parent'] = $dat->parent;
            $row['text']= $dat->text;
            $row['a_attr']['id_test'] = $dat->id;
            array_push($args, $row);
        }
        return Response::json($args);
    }

    public function deleteMonitoring($id) {
        try {
            $monitoring = Monitoring::find($id);
            $monitoring->delete();
            return redirect()->route('index_station_monitoring')->with('success', 'Monitoreo Eliminado.');
        } catch (Exception $exception) {
            return redirect()->route('index_station_monitoring')->with('error', 'Algo salio mal, intenta de nuevo.');
        }
    }

    public function reporting() {
        return view('vendor.adminlte.monitoring.stations');
    }

    public function reportingAreas() {
        return view('vendor.adminlte.monitoring.areas');
    }

    public function getCustomersWithMonitoring() {
        $customers = customer::where('id_profile_job_center', CommonCompany::getProfileJobCenterId())->get();
        return Response::json($customers);
    }

    public function uploadImageChart(Request $request) {
        $chartBaitsLine = $request->get('chart_baits_line');
        $chartBaitsConditions = $request->get('chart_baits_conditions');
        $chartBaitsConsume = $request->get('chart_baits_consume');

        $chartCaptureLine = $request->get('chart_capture_line');
        $chartCaptureConditions = $request->get('chart_capture_conditions');

        $chartLightLine = $request->get('chart_light_line');
        $chartLightConditions = $request->get('chart_light_conditions');

        $chartInsectsLine = $request->get('chart_insects_line');
        $chartInsectsConditions = $request->get('chart_insects_conditions');

        $timeName = time();

        $this->uploadChartsBaits($chartBaitsLine, $chartBaitsConditions, $chartBaitsConsume, $timeName);
        $this->uploadChartsCapture($chartCaptureLine, $chartCaptureConditions, $timeName);
        $this->uploadChartsLight($chartLightLine, $chartLightConditions, $timeName);
        $this->uploadChartsInsects($chartInsectsLine, $chartInsectsConditions, $timeName);

        return $timeName;
    }

    private function uploadChartsBaits($chartBaitsLine, $chartBaitsConditions, $chartBaitsConsume, $timeName) {
        $base64StrBaitsLine = substr($chartBaitsLine, strpos($chartBaitsLine, ",")+1);
        $base64StrBaitsConditions = substr($chartBaitsConditions, strpos($chartBaitsConditions, ",")+1);
        $base64StrBaitsConsume = substr($chartBaitsConsume, strpos($chartBaitsConsume, ",")+1);

        $decodedImageLine = base64_decode($base64StrBaitsLine);
        $decodedImageConditions = base64_decode($base64StrBaitsConditions);
        $decodedImageConsume = base64_decode($base64StrBaitsConsume);

        $nameLine = 'BaitsLine-'. $timeName . '.png';
        $nameConditions = 'BaitsConditions-'. $timeName . '.png';
        $nameConsume = 'BaitsConsume-'. $timeName . '.png';

        file_put_contents(public_path('/img/charts_report/'.$nameLine), $decodedImageLine);
        file_put_contents(public_path('/img/charts_report/'.$nameConditions), $decodedImageConditions);
        file_put_contents(public_path('/img/charts_report/'.$nameConsume), $decodedImageConsume);
    }

    private function uploadChartsCapture($chartCaptureLine, $chartCaptureConditions, $timeName) {
        $base64StrCaptureLine = substr($chartCaptureLine, strpos($chartCaptureLine, ",")+1);
        $base64StrCaptureConditions = substr($chartCaptureConditions, strpos($chartCaptureConditions, ",")+1);

        $decodedImageLine = base64_decode($base64StrCaptureLine);
        $decodedImageConditions = base64_decode($base64StrCaptureConditions);

        $nameLine = 'CaptureLine-'. $timeName . '.png';
        $nameConditions = 'CaptureConditions-'. $timeName . '.png';

        file_put_contents(public_path('/img/charts_report/'.$nameLine), $decodedImageLine);
        file_put_contents(public_path('/img/charts_report/'.$nameConditions), $decodedImageConditions);
    }

    private function uploadChartsLight($chartLightLine, $chartLightConditions, $timeName) {
        $base64StrLightLine = substr($chartLightLine, strpos($chartLightLine, ",")+1);
        $base64StrLightConditions = substr($chartLightConditions, strpos($chartLightConditions, ",")+1);

        $decodedImageLine = base64_decode($base64StrLightLine);
        $decodedImageConditions = base64_decode($base64StrLightConditions);

        $nameLine = 'LightLine-'. $timeName . '.png';
        $nameConditions = 'LightConditions-'. $timeName . '.png';

        file_put_contents(public_path('/img/charts_report/'.$nameLine), $decodedImageLine);
        file_put_contents(public_path('/img/charts_report/'.$nameConditions), $decodedImageConditions);
    }

    private function uploadChartsInsects($chartInsectsLine, $chartInsectsConditions, $timeName) {
        $base64StrInsectsLine = substr($chartInsectsLine, strpos($chartInsectsLine, ",")+1);
        $base64StrInsectsConditions = substr($chartInsectsConditions, strpos($chartInsectsConditions, ",")+1);

        $decodedImageLine = base64_decode($base64StrInsectsLine);
        $decodedImageConditions = base64_decode($base64StrInsectsConditions);

        $nameLine = 'InsectsLine-'. $timeName . '.png';
        $nameConditions = 'InsectsConditions-'. $timeName . '.png';

        file_put_contents(public_path('/img/charts_report/'.$nameLine), $decodedImageLine);
        file_put_contents(public_path('/img/charts_report/'.$nameConditions), $decodedImageConditions);
    }

    public function createZone(Request $request){
        try {
            $customerId = $request->get('customerId');
            $zone = $request->get('nameZone');
            $customerBranchId = $request->get('selectCustomerBranchId');
            if ($customerBranchId == null) $customerBranchId = 0;

            $customer = customer::find($customerId);
            $monitoring = Monitoring::where('id_customer', $customerId)
                ->where('id_customer_branch', $customerBranchId)
                ->first();
            $typeStation = TypeStation::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('id_type_area', 2)->first();
            $typeStationArea = TypeStation::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('id_type_area', 1)->first();

            if (!$monitoring) $monitoringId = $this->addMonitoring($customerId,$customerBranchId);
            else $monitoringId = $monitoring->id;

            $monitoringTreeParent = MonitoringTree::where('id_monitoring', $monitoringId)
                ->where('parent', '#')->first();

            if (!$monitoringTreeParent) {
                $monitoringTree = new MonitoringTree;
                $monitoringTree->id_monitoring = $monitoringId;
                $monitoringTree->id_company = Auth::user()->companie;
                $monitoringTree->id_node = 1;
                $monitoringTree->parent = '#';
                $monitoringTree->id_type_station = $typeStationArea->id;
                $monitoringTree->text = $customer->name;
                $monitoringTree->save();

                $nodes = new MonitoringNode;
                $nodes->id_monitoring_tree = $monitoringTree->id;
                $nodes->id_type_area = 1;
                $nodes->name = 'Área';
                $nodes->save();
            }

            $lastNode = MonitoringTree::where('id_monitoring', $monitoringId)
                ->where('parent','<>', '#')
                ->latest('id')
                ->first();

            if ($lastNode) {
                $idNode = explode('_', $lastNode->id_node);

                $monitoringZone = new MonitoringTree;
                $monitoringZone->id_monitoring = $monitoringId;
                $monitoringZone->id_company = Auth::user()->companie;
                $monitoringZone->id_node = 'j1_' . ++$idNode[1];
                $monitoringZone->parent = '1';
                $monitoringZone->text = $zone;
                $monitoringZone->id_type_station = $typeStation->id;
                $monitoringZone->save();
            } else {
                $monitoringZone = new MonitoringTree;
                $monitoringZone->id_monitoring = $monitoringId;
                $monitoringZone->id_company = Auth::user()->companie;
                $monitoringZone->id_node = 'j1_2';
                $monitoringZone->parent = '1';
                $monitoringZone->text = $zone;
                $monitoringZone->id_type_station = $typeStation->id;
                $monitoringZone->save();
            }

            $nodes = new MonitoringNode;
            $nodes->id_monitoring_tree = $monitoringZone->id;
            $nodes->id_type_area = 2;
            $nodes->name = 'Zona';
            $nodes->save();

            $zones = MonitoringTree::where('id_monitoring', $monitoringId)
                ->where('parent','1')
                ->get();

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados.',
                'zones' => $zones,
                'monitoringId' => $monitoringId
            ]);

        } catch (Exception $exception){
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intenta de Nuevo.'
            ]);
        }

    }

    public function createPerimeter(Request $request) {
        try {
            $customerId = $request->get('customerId');
            $perimeter = $request->get('namePerimeter');
            $idNodeZone = $request->get('idNodeZone');
            $customerBranchId = $request->get('selectCustomerBranchId');
            if ($customerBranchId == null) $customerBranchId = 0;

            $monitoring = Monitoring::where('id_customer', $customerId)
                ->where('id_customer_branch', $customerBranchId)
                ->first();
            $customer = customer::find($customerId);
            $typeStation = TypeStation::where('profile_job_center_id', $customer->id_profile_job_center)
                ->where('id_type_area', 3)
                ->first();
            $lastNode = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->where('parent','<>','#')
                ->latest('id')
                ->first();
            $lastZone = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->where('parent','1')
                ->latest()
                ->first();

            if ($lastNode) {
                $idNode = explode('_', $lastNode->id_node);

                $monitoringZone = new MonitoringTree;
                $monitoringZone->id_monitoring = $monitoring->id;
                $monitoringZone->id_company = Auth::user()->companie;
                $monitoringZone->id_node = 'j1_' . ++$idNode[1];
                $monitoringZone->parent = $idNodeZone;
                $monitoringZone->text = $perimeter;
                $monitoringZone->id_type_station = $typeStation->id;
                $monitoringZone->save();
            } else {
                $idNode = explode('_', $lastZone->id_node);
                $monitoringZone = new MonitoringTree;
                $monitoringZone->id_monitoring = $monitoring->id;
                $monitoringZone->id_company = Auth::user()->companie;
                $monitoringZone->id_node = 'j1_' . ++$idNode[1];
                $monitoringZone->parent = $idNodeZone;
                $monitoringZone->text = $perimeter;
                $monitoringZone->id_type_station = $typeStation->id;
                $monitoringZone->save();
            }

            $nodes = new MonitoringNode;
            $nodes->id_monitoring_tree = $monitoringZone->id;
            $nodes->id_type_area = 3;
            $nodes->name = 'Perímetro';
            $nodes->save();

            $perimeters = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('monitoring_trees.id', 'monitoring_trees.id_node', 'monitoring_trees.text')
                ->get();

            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $typeStations = TypeStation::where('profile_job_center_id', $employee->profile_job_center_id)
                ->whereNotIn('id_type_area', [1, 2, 3, 7])
                ->get();

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados.',
                'perimeters' => $perimeters,
                'typeStations' => $typeStations,
                'monitoringId' => $monitoring->id
            ]);

        } catch (Exception $exception){
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intenta de Nuevo.'
            ]);
        }

    }

    public function createStation(Request $request){
        try {
            DB::beginTransaction();

            $customerId = $request->get('customerId');
            $perimeterSelect = $request->get('perimeterSelect');
            $typeStationsSelect = $request->get('typeStationsSelect');
            $initialRange = $request->get('initialRange');
            $finalRange = $request->get('finalRange');
            $loan = $request->get('loan');
            $customerBranchId = $request->get('selectCustomerBranchId');
            if ($customerBranchId == null) $customerBranchId = 0;

            $monitoring = Monitoring::where('id_customer', $customerId)
                ->where('id_customer_branch', $customerBranchId)
                ->first();

            // Validations
            $stations = MonitoringTree::where('id_monitoring', $monitoring->id)->count();
            if ($finalRange > 500) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Para crear más de 500 estaciones, requiere autorización.'
                ]);
            }
            if ($stations > 1000) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Has superado el límite de estaciones, requiere autorización.'
                ]);
            }

            $totalStations = ($finalRange - $initialRange) + 1;
            $arrayStations = array_fill(0, $totalStations, '');
            $folio = $initialRange;

            foreach ($arrayStations as $station) {
                $lastStation = MonitoringTree::where('id_monitoring', $monitoring->id)
                    ->where('parent','<>','1')
                    ->where('parent','<>','#')
                    ->latest('id')
                    ->first();

                //$nameStation = explode('#', $lastStation->text);
                $typeStationsSelectName = TypeStation::find($typeStationsSelect);

                //$typeStation = $lastStation->id_type_station;
                //if ($typeStation == $typeStationsSelect) $key = $nameStation[1];
                $idNode = explode('_', $lastStation->id_node);

                $monitoringZone = new MonitoringTree;
                $monitoringZone->id_monitoring = $monitoring->id;
                $monitoringZone->id_company = Auth::user()->companie;
                $monitoringZone->id_node = 'j1_' . ++$idNode[1];
                $monitoringZone->parent = $perimeterSelect;
                $monitoringZone->text = $typeStationsSelectName->name . ' #' . $folio++;
                $monitoringZone->id_type_station = $typeStationsSelect;
                if ($loan == "false") $monitoringZone->loan = 0;
                $monitoringZone->save();

                $nodes = new MonitoringNode;
                $nodes->id_monitoring_tree = $monitoringZone->id;
                $nodes->id_type_area = $typeStationsSelectName->id_type_area;
                $nodes->name = $typeStationsSelectName->name;
                $nodes->save();
            }

            $perimeters = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 3)
                ->get();

            $stationTypes = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->join('type_stations as ts', 'monitoring_trees.id_type_station', 'ts.id')
                ->whereNotIn('ts.id_type_area', [1, 2, 3, 7])
                ->select(DB::raw('count(*) as count'), 'ts.name', 'ts.id')
                ->groupBy('ts.name')
                ->get();

            $perimetersWithTypeStation = new Collection();

            foreach ($stationTypes as $stationType) {
                foreach ($perimeters as $p) {
                    $st = MonitoringTree::where('id_monitoring', $monitoring->id)
                        ->join('type_stations as ts', 'monitoring_trees.id_type_station', 'ts.id')
                        ->whereNotIn('ts.id_type_area', [1, 2, 3, 7])
                        ->where('ts.id', $stationType->id)
                        ->where('monitoring_trees.parent', $p->id_node)
                        ->where('monitoring_trees.visible',1)
                        ->count();
                    $zoneCb = MonitoringTree::where('id_node', $p->parent)
                        ->where('id_monitoring', $monitoring->id)
                        ->first();
                    if ($st > 0) {
                        $perimetersWithTypeStation->push([
                            'id' => $stationType->id,
                            'text' => $p->text,
                            'zone' => $zoneCb->text,
                            'quantity' => $st,
                            'type_station' => $stationType->name,
                            'id_type_station' => $stationType->id,
                            'parent' => $p->id_node,
                        ]);
                    }
                }
            }

            $areas = MonitoringTree::where('id_monitoring',$monitoring->id)->get();
            $parents = MonitoringTree::where('id_monitoring',$monitoring->id)->get();

            $areas->map(function($area) use ($parents) {
                $orders = $parents->filter(function($parent) use ($area) {
                    return $parent->id_node == $area->parent;
                });
                $areas = $area;
                $areas['parent_text'] = $orders->first() != null ? $orders->first()->text : "";
                $station = TypeStation::find($area->id_type_station);
                $areas['is_station'] = 0;
                if ($station->id_type_area == 4 || $station->id_type_area == 5 || $station->id_type_area == 6) {
                    $areas['is_station'] = $station->id_type_area;
                }
            });

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados.',
                'stations' => $perimetersWithTypeStation,
                'monitoringId' => $monitoring->id,
                'stationTrees' => $areas,
                'deleteData' => $deleteData,
                'master' => $master
            ]);

        } catch (Exception $exception){
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intenta de Nuevo.'
            ]);
        }

    }

    public function deleteZoneById($id) {
        try {
            $zone = MonitoringTree::find($id);
            $perimeters = MonitoringTree::where('parent', $zone->id_node)->where('id_monitoring', $zone->id_monitoring)->get();
            $zone->delete();
            foreach ($perimeters as $perimeter) {
                $stations = MonitoringTree::where('parent', $perimeter->id_node)->where('id_monitoring', $zone->id_monitoring);
                $perimeter->delete();
                $stations->delete();
            }
            $zones = MonitoringTree::where('id_monitoring', $zone->id_monitoring)
                ->where('parent','1')
                ->get();
            return Response::json([
                'code' => 200,
                'message' => 'Zona eliminada correctamente.',
                'zones' => $zones
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function deletePerimeterById($id) {
        try {
            $perimeter = MonitoringTree::find($id);
            $stations = MonitoringTree::where('parent', $perimeter->id_node)->where('id_monitoring', $perimeter->id_monitoring);
            $perimeter->delete();
            $stations->delete();
            $perimeters = MonitoringTree::where('id_monitoring', $perimeter->id_monitoring)
                ->join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('monitoring_trees.id', 'monitoring_trees.id_node', 'monitoring_trees.text')
                ->get();
            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $typeStations = TypeStation::where('profile_job_center_id', $employee->profile_job_center_id)
                ->whereNotIn('id_type_area', [1, 2, 3, 7])
                ->get();
            return Response::json([
                'code' => 200,
                'message' => 'Perímetro eliminado correctamente.',
                'perimeters' => $perimeters,
                'typeStations' => $typeStations
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function updateZoneById(Request $request) {
        try {
            $id = $request->get('id');
            $name = $request->get('name');
            $zone = MonitoringTree::find($id);
            $zone->text = $name;
            $zone->save();
            $zones = MonitoringTree::where('id_monitoring', $zone->id_monitoring)
                ->where('parent','1')
                ->get();
            return Response::json([
                'code' => 200,
                'message' => 'Zona actualizada correctamente.',
                'zones' => $zones
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function updatePerimeterById(Request $request) {
        try {
            $id = $request->get('id');
            $name = $request->get('name');
            $perimeter = MonitoringTree::find($id);
            $perimeter->text = $name;
            $perimeter->save();
            $perimeters = MonitoringTree::where('id_monitoring', $perimeter->id_monitoring)
                ->join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('monitoring_trees.id', 'monitoring_trees.id_node', 'monitoring_trees.text')
                ->get();
            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $typeStations = TypeStation::where('profile_job_center_id', $employee->profile_job_center_id)
                ->whereNotIn('id_type_area', [1, 2, 3, 7])
                ->get();
            return Response::json([
                'code' => 200,
                'message' => 'Perímetro actualizado correctamente.',
                'perimeters' => $perimeters,
                'typeStations' => $typeStations
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getPerimetersMonitoring(Request $request) {
        try {
            $customerId = $request->get('customerId');
            $monitoring = Monitoring::where('id_customer', $customerId)->first();
            $perimetersSt = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 3)
                ->get();

            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $typeStations = TypeStation::where('profile_job_center_id', $employee->profile_job_center_id)
                ->whereNotIn('id_type_area', [1, 2, 3, 7])
                ->get();

            return Response::json([
                'code' => 200,
                'perimeters' => $perimetersSt,
                'typeStations' => $typeStations
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function relocateStationsByPerimeter(Request $request) {
        try {
            $idStation = $request->get('idStation');
            $idNodeParent = $request->get('idNodeStation');
            $idNumberStation = $request->get('numberStation');
            $typeStation = $request->get('typeStation');

            $station = MonitoringTree::find($idStation);
            $type = TypeStation::find($typeStation);

            $station->parent = $idNodeParent;
            $station->text = $type->name . ' #' . $idNumberStation;
            $station->id_type_station = $type->id;
            $station->save();

            $monitoringNode = MonitoringNode::where('id_monitoring_tree', $idStation)->first();
            $monitoringNode->id_type_area = $type->id_type_area;
            $monitoringNode->name = $type->name;
            $monitoringNode->save();

            return Response::json([
                'code' => 200,
                'message' => 'Estación reubicada correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function cancelStationsByPerimeter(Request $request) {
        try {
            $idStation = $request->get('idStation');
            $station = MonitoringTree::find($idStation);
            if ($station->visible == 1) $station->visible = 0;
            else $station->visible = 1;
            $station->save();

            return Response::json([
                'code' => 200,
                'message' => 'Estaciones eliminadas correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function loanStationsByPerimeter(Request $request) {
        try {
            $idStation = $request->get('idStation');
            $station = MonitoringTree::find($idStation);
            if ($station->loan == 1) $station->loan = 0;
            else $station->loan = 1;
            $station->save();

            return Response::json([
                'code' => 200,
                'message' => 'Estacione cambiada correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function deleteStationsByPerimeterSingular(Request $request) {
        try {
            $idStation = $request->get('idStation');
            $station = MonitoringTree::find($idStation);
            $station->delete();

            return Response::json([
                'code' => 200,
                'message' => 'Estación eliminada correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function deleteStationsByPerimeter(Request $request) {
        try {
            $idCustomer = $request->get('id_customer');
            $idTypeStation = $request->get('id_type_station');
            $parent = $request->get('parent');
            $monitoring = Monitoring::where('id_customer', $idCustomer)->first();
            $stations = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->where('id_type_station', $idTypeStation)
                ->where('parent', $parent);
            $stations->delete();
            return Response::json([
                'code' => 200,
                'message' => 'Estaciones eliminadas correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function updateStationsByPerimeter(Request $request) {
        try {
            $idCustomer = $request->get('id_customer');
            $idTypeStation = $request->get('id_type_station');
            $parent = $request->get('parent');
            $initialRange = $request->get('initialRange');
            $finalRange = $request->get('finalRange');

            $monitoring = Monitoring::where('id_customer', $idCustomer)->first();

            // Validations
            $stations = MonitoringTree::where('id_monitoring', $monitoring->id)->count();
            if ($finalRange > 500) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Para crear más de 500 estaciones, requiere autorización.'
                ]);
            }
            if ($stations > 1000) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Has superado el límite de estaciones, requiere autorización.'
                ]);
            }

            // Delete stations
            $stations = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->where('id_type_station', $idTypeStation)
                ->where('parent', $parent);
            $stations->delete();

            // Create stations
            $totalStations = ($finalRange - $initialRange) + 1;
            $arrayStations = array_fill(0, $totalStations, '');
            $folio = $initialRange;

            foreach ($arrayStations as $station) {
                $lastStation = MonitoringTree::where('id_monitoring', $monitoring->id)
                    ->where('parent','<>','1')
                    ->where('parent','<>','#')
                    ->latest('id')
                    ->first();

                //$nameStation = explode('#', $lastStation->text);
                $typeStationsSelectName = TypeStation::find($idTypeStation);

                //$typeStation = $lastStation->id_type_station;
                //if ($typeStation == $idTypeStation) $key = $nameStation[1];
                $idNode = explode('_', $lastStation->id_node);

                $monitoringZone = new MonitoringTree;
                $monitoringZone->id_monitoring = $monitoring->id;
                $monitoringZone->id_company = Auth::user()->companie;
                $monitoringZone->id_node = 'j1_' . ++$idNode[1];
                $monitoringZone->parent = $parent;
                $monitoringZone->text = $typeStationsSelectName->name . ' #' . $folio++;
                $monitoringZone->id_type_station = $idTypeStation;
                $monitoringZone->save();

                $nodes = new MonitoringNode;
                $nodes->id_monitoring_tree = $monitoringZone->id;
                $nodes->id_type_area = $typeStationsSelectName->id_type_area;
                $nodes->name = $typeStationsSelectName->name;
                $nodes->save();
            }

            return Response::json([
                'code' => 200,
                'message' => 'Estaciones actualizadas correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getAllTreeByCustomer($id, Request $request) {
        try {
            $customerId = $id;
            $monitoringId = $request->get('monitoringId');
            $monitoring = Monitoring::where('id', $monitoringId)->first();
            $zones = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->where('parent','1')
                ->get();
            $perimeters = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('monitoring_trees.id', 'monitoring_trees.id_node', 'monitoring_trees.text')
                ->get();
            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $typeStations = TypeStation::where('profile_job_center_id', $employee->profile_job_center_id)
                ->whereNotIn('id_type_area', [1, 2, 3, 7])
                ->get();

            $perimetersSt = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 3)
                ->get();
            $stationTypes = MonitoringTree::where('id_monitoring', $monitoring->id)
                ->join('type_stations as ts', 'monitoring_trees.id_type_station', 'ts.id')
                ->whereNotIn('ts.id_type_area', [1, 2, 3, 7])
                ->where('monitoring_trees.visible', 1)
                ->select(DB::raw('count(*) as count'), 'ts.name', 'ts.id')
                ->groupBy('ts.name')
                ->get();
            $perimetersWithTypeStation = new Collection();
            foreach ($stationTypes as $stationType) {
                foreach ($perimetersSt as $p) {
                    $st = MonitoringTree::where('id_monitoring', $monitoring->id)
                        ->join('type_stations as ts', 'monitoring_trees.id_type_station', 'ts.id')
                        ->whereNotIn('ts.id_type_area', [1, 2, 3, 7])
                        ->where('ts.id', $stationType->id)
                        ->where('monitoring_trees.parent', $p->id_node)
                        ->where('monitoring_trees.visible',1)
                        ->count();
                    $zoneCb = MonitoringTree::where('id_node', $p->parent)
                        ->where('id_monitoring', $monitoring->id)
                        ->first();
                    if ($st > 0) {
                        $perimetersWithTypeStation->push([
                            'id' => $stationType->id,
                            'text' => $p->text,
                            'zone' => $zoneCb->text,
                            'quantity' => $st,
                            'type_station' => $stationType->name,
                            'id_type_station' => $stationType->id,
                            'parent' => $p->id_node,
                        ]);
                    }
                }
            }

            $lastInspection = DB::table('station_inspections')
                ->where('id_monitoring', $monitoring->id)
                ->latest()
                ->first();

            $areas = MonitoringTree::where('id_monitoring',$monitoring->id)->get();
            $parents = MonitoringTree::where('id_monitoring',$monitoring->id)->get();

            $areas->map(function($area) use ($parents) {
                $orders = $parents->filter(function($parent) use ($area) {
                    return $parent->id_node == $area->parent;
                });
                $areas = $area;
                $areas['parent_text'] = $orders->first() != null ? $orders->first()->text : "";
                $station = TypeStation::find($area->id_type_station);
                $areas['is_station'] = 0;
                if ($station->id_type_area == 4 || $station->id_type_area == 5 || $station->id_type_area == 6) {
                    $areas['is_station'] = $station->id_type_area;
                }
            });

            $deleteData = Entrust::can('Eliminar Datos');
            $master = Entrust::hasRole('Cuenta Maestra');

            return Response::json([
                'code' => 200,
                'message' => 'Datos Actualizados.',
                'zones' => $zones,
                'perimeters' => $perimeters,
                'typeStations' => $typeStations,
                'stations' => $perimetersWithTypeStation,
                'stationTrees' => $areas,
                'perimetersTrees' => $perimetersSt,
                'monitoringId' => $monitoring->id,
                'lastInspection' => $lastInspection,
                'deleteData' => $deleteData,
                'master' => $master
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getAllCustomerBranchByCustomer($id){
        try {
            $customerBranches = customer_branche::where('customer_id', $id)->get();
            $isCustomerBranchMonitoring = false;
            $collectionBranches = new Collection();
            foreach ($customerBranches as $customerBranch) {
                $concept = custom_quote::where('id_quotation', $customerBranch->id_quotation)
                    ->where('concept', $customerBranch->type)
                    ->where('id_type_concepts', 3)
                    ->first();
                if ($concept) {
                    $monitoring = Monitoring::where('id_customer_branch', $customerBranch->id)->first();
                    if (!$monitoring) $collectionBranches->push($customerBranch);
                }
            }
            if ($collectionBranches->count() != $customerBranches->count()) $isCustomerBranchMonitoring = true;
            return Response::json([
                'code' => 200,
                'customerBranches' => $collectionBranches,
                'isCustomerBranchMonitoring' => $isCustomerBranchMonitoring,
                'message' => 'Sucursales Cliente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function createBaseStations(Request $request) {
        try {
            DB::beginTransaction();
            $companyId = $request->get('company_id');
            $jobCenterId = $request->get('job_center_id');

            $typeStationArea = new TypeStation();
            $typeStationArea->key = "AR";
            $typeStationArea->name = "Área";
            $typeStationArea->id_type_area = 1;
            $typeStationArea->profile_job_center_id = $jobCenterId;
            $typeStationArea->id_company = $companyId;
            $typeStationArea->save();

            $typeStationZone = new TypeStation();
            $typeStationZone->key = "ZN";
            $typeStationZone->name = "Zona";
            $typeStationZone->id_type_area = 2;
            $typeStationZone->profile_job_center_id = $jobCenterId;
            $typeStationZone->id_company = $companyId;
            $typeStationZone->save();

            $typeStationPerimeter = new TypeStation();
            $typeStationPerimeter->key = "PR";
            $typeStationPerimeter->name = "Perímetro";
            $typeStationPerimeter->id_type_area = 3;
            $typeStationPerimeter->profile_job_center_id = $jobCenterId;
            $typeStationPerimeter->id_company = $companyId;
            $typeStationPerimeter->save();

            $typeStationCeb = new TypeStation();
            $typeStationCeb->key = "CEB";
            $typeStationCeb->name = "Cebadero";
            $typeStationCeb->id_type_area = 4;
            $typeStationCeb->profile_job_center_id = $jobCenterId;
            $typeStationCeb->id_company = $companyId;
            $typeStationCeb->save();

            $typeStationTc = new TypeStation();
            $typeStationTc->key = "TC";
            $typeStationTc->name = "Trampa de Captura";
            $typeStationTc->id_type_area = 5;
            $typeStationTc->profile_job_center_id = $jobCenterId;
            $typeStationTc->id_company = $companyId;
            $typeStationTc->save();

            $typeStationTcl = new TypeStation();
            $typeStationTcl->key = "TL";
            $typeStationTcl->name = "Trampa de Luz";
            $typeStationTcl->id_type_area = 5;
            $typeStationTcl->profile_job_center_id = $jobCenterId;
            $typeStationTcl->id_company = $companyId;
            $typeStationTcl->save();

            MonitoringTree::where('id_company', $companyId)
                ->where('parent', '#')
                ->update(['id_type_station' => $typeStationArea->id]);

            MonitoringTree::where('id_company', $companyId)
                ->where('parent', '1')
                ->update(['id_type_station' => $typeStationZone->id]);

            $trees = MonitoringTree::join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 3)
                ->where('parent','<>','1')
                ->where('parent','<>','#')
                ->select('monitoring_trees.*')
                ->get();

            foreach ($trees as $tree) {
                $node = MonitoringTree::find($tree->id);
                $node->id_type_station = $typeStationPerimeter->id;
                $node->save();
            }

            $trees = MonitoringTree::join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 4)
                ->select('monitoring_trees.*')
                ->get();

            foreach ($trees as $tree) {
                $node = MonitoringTree::find($tree->id);
                $node->id_type_station = $typeStationCeb->id;
                $node->save();
            }

            $trees = MonitoringTree::join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 5)
                ->select('monitoring_trees.*')
                ->get();

            foreach ($trees as $tree) {
                $node = MonitoringTree::find($tree->id);
                $node->id_type_station = $typeStationTc->id;
                $node->save();
            }

            MonitoringTree::join('monitoring_nodes as mn', 'monitoring_trees.id', 'mn.id_monitoring_tree')
                ->where('mn.id_type_area', 6)
                ->select('monitoring_trees.*')
                ->get();

            foreach ($trees as $tree) {
                $node = MonitoringTree::find($tree->id);
                $node->id_type_station = $typeStationTcl->id;
                $node->save();
            }

            DB::commit();

            return Response::json([
                'code' => 200,
                'message' => 'Datos Actualizados'
            ]);
        } catch (Exception $exception) {
            DB::rollBack();
            return Response::json([
                'code' => 500,
                'message' => $exception->getMessage() . $exception->getLine()
            ]);
        }
    }

    public function updateStorehouse() {
        $storehouses = storehouse::where('companie', 244)->get();
        foreach ($storehouses as $storehouse) {
            $product = product::find($storehouse->id_product);
            if ($product->profile_job_center_id == 268) {
                $sh = storehouse::find($storehouse->id);
                $sh->stock = 0;
                $sh->stock_other_units = 0;
                $sh->total = 0;
                $sh->save();
            }
        }
    }

    public function updateDateMonitoring($monitoringId) {
        try {
            $monitoring = Monitoring::find($monitoringId);
            $monitoring->date_updated_tree = Carbon::now()->toDateTimeString();
            $monitoring->save();
            return Response::json([
                'code' => 200,
                'message' => 'Datos actualizados.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal.'
            ]);
        }
    }

    public function updateBranchIdMonitoring($monitoringId, Request $request) {
        try {
            $monitoring = Monitoring::find($monitoringId);
            $monitoring->id_customer_branch = $request->get('selectCustomerBranchId');
            $monitoring->save();
            return Response::json([
                'code' => 200,
                'message' => 'Datos guardados.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal.'
            ]);
        }
    }

}
