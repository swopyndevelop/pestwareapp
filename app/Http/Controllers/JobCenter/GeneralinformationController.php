<?php

namespace App\Http\Controllers\JobCenter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GeneralInformationJobCenter;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Validator;
use Image;

class GeneralinformationController extends Controller
{
	protected $rules =
	[
		'HorarioInicial' => 'Required',
		'HorarioFinal' => 'Required',
		'DiasLaboralesInicio' => 'Required',
		'DiasLaboralesFinal' => 'Required',
		'Fondo' => 'Required',
/*		'ArchivoFondo' => 'Required',
*/		'Equilibrio' => 'Required',
	// 'ContratoRenta' => 'Required',
		'VigenciaRenta' => 'Required',
	// 'Hacienda' => 'Required',
		'VigenciaHacienda' => 'Required',
	// 'PermisoComercial' => 'Required',
	//'VigenciaPermiso' => 'Required',
	// 'Fumigacion' => 'Required',
		'VigenciaFumigacion' => 'Required',
	// 'Extintor' => 'Required',
		'VigenciaExtintor' => 'Required',
	// 'Salubridad' => 'Required',
		'VigenciaSalubridad' => 'Required',
		'Identificador' => 'Required'
	];

	public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} 
		else {
			$gralinfoTemp = DB::table('general_information_job_centers')->where('profile_job_centers_id', $request->Identificador)->first();
			if (count($gralinfoTemp) <= 0) {
				$info = New GeneralInformationJobCenter;
				$info->profile_job_centers_id=$request->Identificador;
			}
			else{
				$id = $gralinfoTemp->id;
				$info = GeneralInformationJobCenter::find($id);
			}

			// images config
			// $ArchivoFondo = Input::file('ArchivoFondo');
			// $Afondo = Image::make($ArchivoFondo)->fit(200,80)->encode('png');


			// $imgimage = Input::file('Imagen');
			// $imagen = Image::make($imgimage)->fit(300)->encode('png');
			/*$file = Input::file('ArchivoFondo');
			$img = Image::make($file)->fit(600, 800, function ($constraint) { $constraint->upsize(); })->encode('jpeg');*/
			$info->hour_init = $request->HorarioInicial;
			$info->hour_final = $request->HorarioFinal;
			$info->start_workdays = $request->DiasLaboralesInicio;
			$info->final_workdays = $request->DiasLaboralesFinal;
			$info->small_box = $request->Fondo;
			/*$info->file_small_box = $img;*/
			$info->breakeven = $request->Equilibrio;
			// $info->rent_contract = $request->ContratoRenta;
			$info->validity_rent_contract = $request->VigenciaRenta;
			// $info->hacienda = $request->Hacienda;
			$info->validity_hacienda = $request->VigenciaHacienda;
			// $info->commercial_permission = $request->PermisoComercial;
			$info->validity_commercial_permission = $request->VigenciaPermiso;
			// $info->fumigation = $request->Fumigacion;
			$info->validity_fumigation = $request->VigenciaFumigacion;
			// $info->extinguisher = $request->Extintor;
			$info->validity_extinguisher = $request->VigenciaExtintor;
			//$info->salubrity = $request->Salubridad;
			$info->validity_salubrity = $request->VigenciaSalubridad;

			$info->save();
			return response()->json($info);
		}
	}

	
}



