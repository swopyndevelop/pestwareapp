<?php

namespace App\Http\Controllers\JobCenter;

use App\Common\CommonLabel;
use App\Common\Constants;
use App\companie;
use App\CustomLabel;
use App\Http\Controllers\Security\SecurityController;
use App\Library\FacturamaLibrary;
use App\Plan;
use App\profile_job_center;
use App\ProfileJobCenterBilling;
use App\treeJobCenter;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\address_job_center;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DataJobCenterController extends Controller
{
    public function index($jobcenter_id) {
        $jobcenter_id = SecurityController::decodeId($jobcenter_id);
        $address = address_job_center::where('profile_job_centers_id', $jobcenter_id)->first();
        $profileJobCenter = profile_job_center::where('profile_job_centers_id', $jobcenter_id)->first();
        $email = DB::table('personal_mails')->where('profile_job_center_id', $profileJobCenter->id)->first();
        $company = companie::find(Auth::user()->companie);
        $commonLabels = new CommonLabel();
        $labels = $commonLabels->getLabelsByJobCenter($profileJobCenter->id);
        $pdfOrImage = $profileJobCenter->sanitary_license;
        if ($pdfOrImage != null) {
            $pdfOrImageExplode = explode(".", $pdfOrImage);
            $pdfOrImageExplode = $pdfOrImageExplode[1];
        }
        else $pdfOrImageExplode = null;
        $isAddress = false;
        if ($address) $isAddress = true;
        $profileJobCenterBilling = null;
        $billing = ProfileJobCenterBilling::where('id_profile_job_center', $profileJobCenter->id)->first();
        if ($billing) $profileJobCenterBilling = $billing;
        $treeJobCenter = treeJobCenter::where('id_inc', $profileJobCenter->profile_job_centers_id)->first();
        $plansBilling = Plan::where('type', Constants::$PLAN_TYPE_BILLING)->get();
        $plansBilling->map(function($plan) {
            $plansBilling = $plan;
            $plansBilling['price_folio'] = $plan->price / $plan->quantity;
            return $plansBilling;
        });

        $commonLabels = new CommonLabel();
        $labels = $commonLabels->getLabelsByJobCenter($profileJobCenter->id);

        return view('vendor.adminlte.tree._modalIndex')
            ->with([
                'profileJobCenter' => $profileJobCenter,
                'address' => $address,
                'jobCenterId' => $jobcenter_id,
                'isAddress' => $isAddress,
                'pdfOrImageExplode' => $pdfOrImageExplode,
                'email' => $email,
                'labels' => $labels,
                'company' => $company,
                'profileJobCenterBilling' => $profileJobCenterBilling,
                'tree' => $treeJobCenter,
                'plansBilling' => $plansBilling,
                'labels' => $labels
            ]);
    }

    public function updateCompany(Request $request)
    {
        try {
            $jobCenterId = $request->get('jobCenterId');
            $profileJobCenter = profile_job_center::where('profile_job_centers_id', $jobCenterId)->first();
            $profileJobCenter->email_personal = $request->get('email_personal');
            $profileJobCenter->business_name = $request->get('business_name');
            $profileJobCenter->rfc = $request->get('rfc');
            $profileJobCenter->license = $request->get('license');
            $profileJobCenter->facebook_personal = $request->get('facebook_personal');
            $profileJobCenter->web_page = $request->get('web_page');
            $profileJobCenter->messenger_personal = $request->get('messenger_personal');
            $profileJobCenter->cellphone = $request->get('cellphoneGeneral');
            $profileJobCenter->whatsapp_personal = $request->get('whatsapp_personal');
            $profileJobCenter->health_manager = $request->get('health_manager');
            $profileJobCenter->warning_service = $request->get('warning_service');
            $profileJobCenter->contract_service = $request->get('contract_service');
            $profileJobCenter->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se guardo correctamente la sucursal.'
            ]);
        }
        catch (Exception $exception) {
            return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function addSanitaryLicense(Request $request)
    {
        try{
            $jobCenterId = $request->get('jobCenterIdSanitaryLicense');

            $filename = $request->file('file')->store('pdf_licencias_sanitarias', 'ftp');

            $sanitaryLincense = profile_job_center::where('profile_job_centers_id', $jobCenterId)->first();
            $sanitaryLincense->sanitary_license = $filename;
            $sanitaryLincense->save();

        } catch (Exception $exception) {

        }
    }

    public function updateAddress(Request $request) {
        try {
            $address = address_job_center::where('profile_job_centers_id', $request->get('jobCenterId'))->first();
            if (!$address) $address = new address_job_center;
            $address->street = $request->get('street');
            $address->zip_code = $request->get('zip_code');
            $address->num_ext = $request->get('num_ext');
            $address->municipality = $request->get('municipality');
            $address->state = $request->get('state');
            $address->location = $request->get('locationColony');
            $address->address_job_centers_id = $request->get('jobCenterId');
            $address->profile_job_centers_id = $request->get('jobCenterId');
            $address->save();
            return response()->json([
                'code' => 201,
                'message' => 'Domicilio actualizado correctamente.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => $exception->getMessage() //'Error al actualizar el domicilio.'
            ]);
        }
    }

    public function updateTax(Request $request) {
        try {
            $jobCenterId = $request->get('jobCenterId');
            $profileJobCenter = profile_job_center::where('profile_job_centers_id', $jobCenterId)->first();
            $profileJobCenter->iva = $request->get('ivaProfileJobCenter');
            $profileJobCenter->save();
            return response()->json([
                'code' => 201,
                'message' => 'Impuesto actualizado correctamente.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al actualizar el Impuesto.'
            ]);
        }
    }

    public function updateSsid(Request $request) {
        try {
            $jobCenterId = $request->get('jobCenterId');
            $profileJobCenter = profile_job_center::where('profile_job_centers_id', $jobCenterId)->first();
            $profileJobCenter->ssid = $request->get('ssidProfileJobCenter');
            $profileJobCenter->save();
            return response()->json([
                'code' => 201,
                'message' => 'Datos actualizados correctamente.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al actualizar el Ssid.'
            ]);
        }
    }

    public function updateLabel(Request $request) {
        try {
            $jobCenterId = $request->get('jobCenterId');
            $street = $request->get('street');
            $colony = $request->get('colony');
            $state = $request->get('state');
            $iva = $request->get('iva');
            $streetId = $request->get('streetId');
            $colonyId = $request->get('colonyId');
            $stateId = $request->get('stateId');
            $ivaId = $request->get('ivaId');
            $rfcCountry = $request->get('rfcCountryProfileJobCenter');

            $profileJobCenter = profile_job_center::where('profile_job_centers_id',$jobCenterId)->first();
            $customLabelStreet = CustomLabel::where('id_profile_job_center', $profileJobCenter->id)
                ->where('id_label', $streetId)
                ->first();
            $customLabelColony = CustomLabel::where('id_profile_job_center', $profileJobCenter->id)
                ->where('id_label', $colonyId)
                ->first();
            $customLabelState = CustomLabel::where('id_profile_job_center', $profileJobCenter->id)
                ->where('id_label', $stateId)
                ->first();
            $customLabelIva = CustomLabel::where('id_profile_job_center', $profileJobCenter->id)
                ->where('id_label', $ivaId)
                ->first();

            if (!$customLabelStreet) $customLabelStreet = new CustomLabel;
            if (!$customLabelColony) $customLabelColony = new CustomLabel;
            if (!$customLabelState) $customLabelState = new CustomLabel;
            if (!$customLabelIva) $customLabelIva = new CustomLabel;

            $customLabelStreet->label_spanish = $street;
            $customLabelStreet->id_label = $streetId;
            $customLabelStreet->id_profile_job_center = $profileJobCenter->id;
            $customLabelStreet->save();
            $customLabelColony->label_spanish = $colony;
            $customLabelColony->id_label = $colonyId;
            $customLabelColony->id_profile_job_center = $profileJobCenter->id;
            $customLabelColony->save();
            $customLabelState->label_spanish = $state;
            $customLabelState->id_label = $stateId;
            $customLabelState->id_profile_job_center = $profileJobCenter->id;
            $customLabelState->save();
            $customLabelIva->label_spanish = $iva;
            $customLabelIva->id_label = $ivaId;
            $customLabelIva->id_profile_job_center = $profileJobCenter->id;
            $customLabelIva->save();

            // Rfc Country
            $profileJobCenter->rfc_country = $rfcCountry;
            $profileJobCenter->save();

            return response()->json([
                'code' => 201,
                'message' => 'Etiquetas actualizadas correctamente.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al actualizar el Impuesto.'
            ]);
        }
    }

    public  function updateBilling(Request $request) {
        try {
            $dataRequest = $request->all();

            //Validate postal code in SAT.
            $postalCodes = FacturamaLibrary::getCatalogsPostalCodesByKeyword($dataRequest['postalCodeBilling']);
            if (count($postalCodes) == 0)
            return response()->json([
                'code' => 500,
                'message' => 'El código postal ingresado no se encuentrá dentro de los cátalogos del SAT.'
            ]);

            $profileJobCenter = profile_job_center::where('profile_job_centers_id',$dataRequest['jobCenterId'])->first();
            $billing = ProfileJobCenterBilling::where('id_profile_job_center', $profileJobCenter->id)->first();
            if (!$billing) $billing = new ProfileJobCenterBilling();
            $billing->id_profile_job_center = $profileJobCenter->id;
            $billing->rfc = Str::upper($dataRequest['rfcBilling']);
            $billing->iva = $dataRequest['ivaBilling'];
            $billing->email = $dataRequest['emailBilling'];
            $billing->password = bcrypt($dataRequest['passwordBilling']);
            $billing->address = $dataRequest['addressBilling'];
            $billing->address_number = $dataRequest['addressNumberBilling'];
            $billing->colony = $dataRequest['colonyBilling'];
            $billing->municipality = $dataRequest['municipalityBilling'];
            $billing->state = $dataRequest['stateBilling'];
            $billing->code_postal = $dataRequest['postalCodeBilling'];
            $billing->fiscal_regime = $dataRequest['regimenFiscal'];
            $billing->text_mail = $dataRequest['textMail'];
            if (strlen($billing->file_certificate) == 0) $billing->file_certificate = 1;
            if (strlen($billing->file_key) == 0) $billing->file_key = 1;
            $billing->save();
            return response()->json([
                'code' => 201,
                'message' => 'Datos del Emisor guardados correctamente.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => $exception->getMessage()//'Error al guardar los datos.'
            ]);
        }
    }

    public function addFileCertificate(Request $request){
        try{
            $jobCenterId = $request->get('jobCenterIdCertificate');
            $profileJobCenter = profile_job_center::where('profile_job_centers_id', $jobCenterId)->first();
            $fileCertificate = $request->file('file');
            if ($fileCertificate->getClientOriginalExtension() != 'cer') {
                return response()->json([
                    'code' => 301,
                    'message' => 'El archivo Certificado debe ser .cer',
                ]);
            }
            $billing = ProfileJobCenterBilling::where('id_profile_job_center', $profileJobCenter->id)->first();
            if (Storage::disk('s3')->exists($billing->file_certificate)) {
                Storage::disk('s3')->delete($billing->file_certificate);
            }
            $filename = $fileCertificate->store('billing/certificates', 's3');
            $billing->file_certificate = $filename;
            $billing->save();
            return response()->json([
                'code' => 201,
                'message' => 'Se guardo correctamente el certificado.'
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al cargar el certificado.'
            ]);
        }
    }

    public function addFileKey(Request $request)
    {
        try {
            $jobCenterId = $request->get('jobCenterIdKey');
            $profileJobCenter = profile_job_center::where('profile_job_centers_id', $jobCenterId)->first();
            $fileKey = $request->file('file');
            if ($fileKey->getClientOriginalExtension() != 'key'){
                return response()->json([
                    'code' => 301,
                    'message' => 'El archivo Llave privada debe ser .key',
                ]);
            }
            $billing = ProfileJobCenterBilling::where('id_profile_job_center', $profileJobCenter->id)->first();
            if (Storage::disk('s3')->exists($billing->file_key)) {
                Storage::disk('s3')->delete($billing->file_key);
            }
            $filename = $fileKey->store('billing/keys', 's3');
            $billing->file_key = $filename;
            $billing->save();
            return response()->json([
                'code' => 201,
                'message' => 'Se guardo correctamente la Llave privada.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al cargar la llave privada.'
            ]);
        }
    }
}
