<?php

namespace App\Http\Controllers\JobCenter;

use App\address_job_center;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\profile_job_center;
use App\GeneralInformationJobCenter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Validator;

class ProfilejobcenterController extends Controller
{
	protected $rules =
	[
	'NombreJobCenter' => 'Required',
	'CodigoJobCenter' => 'Required',
	//'Imagen'=>'Required',
	'TipoPuesto' => 'Required'
	];

	public function createAddress($jobcenter_id, $jobcenter_name) {
	    $address = address_job_center::where('profile_job_centers_id', $jobcenter_id)->first();
	    $isAddress = false;
	    if ($address) $isAddress = true;
	    return view('vendor.adminlte.tree._modalAddressJobCenter')->with(['jobCenterName' => $jobcenter_name,
            'address' => $address, 'jobCenterId' => $jobcenter_id, 'isAddress' => $isAddress]);
    }

    public function storeAddress(Request $request) {
        try {
            $address = address_job_center::where('profile_job_centers_id', $request->get('jobCenterId'))->first();
            if (!$address) $address = new address_job_center;
            $address->street = $request->get('street');
            $address->zip_code = $request->get('zip_code');
            $address->num_ext = $request->get('num_ext');
            $address->municipality = $request->get('municipality');
            $address->state = $request->get('state');
            $address->location = $request->get('location');
            $address->address_job_centers_id = $request->get('jobCenterId');
            $address->profile_job_centers_id = $request->get('jobCenterId');
            $address->save();
            return response()->json([
                'code' => 201,
                'message' => 'Domicilio actualizado correctamente.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al actualizar el domicilio.'
            ]);
        }
    }

	public function create($jobcenter_id,$jobcenter_name){
		$tmp = DB::table('profile_job_centers')->where('profile_job_centers_id', $jobcenter_id)->first();
		if (count($tmp) <= 0){
			$jobcenter                         = New profile_job_center;
			$jobcenter->profile_job_centers_id = $jobcenter_id;
		}
		else{
			$jobcenter = profile_job_center::find($tmp->id);
		}
		$jobcenter->name = $jobcenter_name;
		$jobaddress = DB::table('address_job_centers')->where('profile_job_centers_id', $jobcenter_id)->first();
		$work = DB::table('work_capacities')->where('profile_job_centers_id', $jobcenter_id)->get();
		$general = DB::table('general_information_job_centers')->where('profile_job_centers_id', $jobcenter_id)->first();
		$goals = DB::table('goal_job_centers')->where('profile_job_centers_id', $jobcenter_id)->first();
		$jobtitles = DB::table('tree_job_profiles')->join('job_title_profiles as jtp', 'id_inc', 'jtp.job_title_profiles_id')->where('company_id','4')->get();
		$jobcenter->save();
		 return view('vendor.adminlte.jobcenter.create')->with(['jobcenter' => $jobcenter, 'jobaddress' => $jobaddress, 'work' => $work, 'general' => $general, 'goals' => $goals, 'jobtitles' => $jobtitles]);
		//return view('adminlte::jobcenter._ubication');
	}

	//treejobcentersummary
	public function create2(){
        $jobcenters = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('prof.id', 'prof.name', 'gen.validity_rent_contract', 'gen.validity_hacienda', 'gen.validity_commercial_permission', 'gen.validity_fumigation', 'gen.validity_extinguisher', 'gen.validity_salubrity')->get();
        $rent = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_rent_contract')->get();
        $hacienda = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_hacienda')->get();
        $commercial = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_commercial_permission')->get();
        $fumigation = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_fumigation')->get();
        $extinguisher = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_extinguisher')->get();
        $salubrity = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_salubrity')->get();


        return view('vendor.adminlte.tree.treejobcentersummary')->with(['jobcenters'=> $jobcenters, 'rent' => $rent, 'hacienda' => $hacienda, 'commercial' => $commercial, 'fumigation' => $fumigation, 'extinguisher' => $extinguisher, 'salubrity' => $salubrity]);

    }

	public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		}
		else {
			$jobcenter = DB::table('profile_job_centers')->where('profile_job_centers_id', $request->jobcenter_id)->first();
			if (count($jobcenter) <= 0){
				$profile_job_centers = New profile_job_center;

			}else{
				$profile_job_centers = profile_job_center::find($jobcenter->id);
			}
				$profile_job_centers->profile_job_centers_id = $request->jobcenter_id;
				$profile_job_centers->name                   = $request->NombreJobCenter;
				$profile_job_centers->code                   = $request->CodigoJobCenter;
				//$profile_job_centers->image                = $request->Imagen;
				$profile_job_centers->type                   = $request->TipoPuesto;
				$profile_job_centers->save();

			 return response()->json($profile_job_centers );


		}
	}


}
