<?php

namespace App\Http\Controllers\JobCenter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GoalJobCenter;
use App\ProfileJobCenter;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Validator;

class GoalJobCenterController extends Controller
{
	protected $rules =
	[
	'txt_campo1' => 'Required',
	'txt_campo2' => 'Required',
	'txt_campo3' => 'Required',
	'txt_campo4' => 'Required',
	'txt_campo5' => 'Required',
	'txt_campo6' => 'Required',
	'txt_campo7' => 'Required',
	'txt_campo8' => 'Required',
	'txt_campo9' => 'Required',
	'txt_campo10' => 'Required',
	'txt_campo11' => 'Required',
	'txt_campo12' => 'Required',
	'Identificador' => 'Required'
	];

	public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} 
		else {
			$jobCenter=ProfileJobCenter::where('profile_job_centers_id',$request->Identificador)->first();
			$goalTemp = DB::table('goal_job_centers')->where('profile_job_centers_id', $jobCenter->id)->first();
			if (count($goalTemp) <= 0) {
				$goal = New GoalJobCenter;
				$goal->profile_job_centers_id=$jobCenter->id;
			}
			else{
				$id = $goalTemp->id;
				$goal = GoalJobCenter::find($id);
			}
			
			$goal->value_january  = $request->txt_campo1;
			$goal->value_february  = $request->txt_campo2;
			$goal->value_march  = $request->txt_campo3;
			$goal->value_april  = $request->txt_campo4;
			$goal->value_may  = $request->txt_campo5;
			$goal->value_june  = $request->txt_campo6;
			$goal->value_july  = $request->txt_campo7;
			$goal->value_august  = $request->txt_campo8;
			$goal->value_september  = $request->txt_campo9;
			$goal->value_october  = $request->txt_campo10;
			$goal->value_november  = $request->txt_campo11;
			$goal->value_december  = $request->txt_campo12;
			$goal->value_spTotal  = $request->spTotal;
			$goal->save();
			return response()->json($goal);
		}
	}
}