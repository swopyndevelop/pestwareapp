<?php

namespace App\Http\Controllers\JobCenter;

use Illuminate\Http\Request;
use App\WorkCapacity;
use App\ProfileJobCenter;
use App\JobTitleProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Validator;
use Image;
use PDF;

class WorkcapacityController extends Controller
{
	protected $rules =
	[
		'Puesto' => 'Required',
		'cantidadPuesto'=> 'Required',
		'Identificador' => 'Required'
	];

	public function add(Request $request){
		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {  
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} 
		else {
			$jobTitle=JobTitleProfile::find($request->Puesto);
			$workcapacities = DB::table('work_capacities')->where('profile_job_centers_id', $request->Identificador)->get();
			$editPass = false;
			foreach ($workcapacities as $wc) {
				if($wc->jobtitle_capacity == $jobTitle->job_title_profiles_id){
					$work_capacities = WorkCapacity::find($wc->id);
					$editPass = true;
				}
			}
			if (!$editPass) {
				$work_capacities = new WorkCapacity;
			}
			$work_capacities->profile_job_centers_id = $request->Identificador;
			$work_capacities->work_capacities_id = $request->Identificador + $jobTitle->job_title_profiles_id;
			$work_capacities->jobtitle_capacity = $jobTitle->job_title_profiles_id;
			$work_capacities->quantity = $request->cantidadPuesto;
			$work_capacities->save();
			$this->totPersonal($request->Identificador);

			return response()->json($work_capacities);
		}
	}

	public function delete(Request $request){
		$tmp = DB::table('work_capacities')->where('id', $request->Id)->delete();
        return response()->json($tmp);
	}
	// public function update(Request $request){
	// 	$validator = Validator::make(Input::all(), $this->rules);
	// 	if ($validator->fails()) {  
	// 		return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
	// 	} 
	// 	else { 
	// 		$work_capacities->jobtitle_capacity=$request->Puesto;
	// 		$work_capacities->quantity=$request->cantidadPuesto;
	// 		$work_capacities->profile_job_centers_id=$request->Identificador;

	// 		$work_capacities->save();

	// 		return response()->json($work_capacities);
	// 	}
	// }
	public function totPersonal($jobcenter_id){
		$sumPersonal = DB::table('work_capacities')->where('profile_job_centers_id',$jobcenter_id)->sum('quantity');
		$tmp = ProfileJobCenter::where('profile_job_centers_id','=',$jobcenter_id)->first();
		$tmp->total = $sumPersonal;
		$tmp->save();
	}
}