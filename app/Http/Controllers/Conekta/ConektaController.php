<?php

namespace App\Http\Controllers\Conekta;

use App\employee;
use App\profile_job_center;
use App\treeJobCenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\ConektaLibrary;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\companie;
use App\company_payment;


class ConektaController extends Controller {

    private $planEmpresarial = 3;
    private $planEmprendedor = 2;
    private $costExtraUserMXN = 99;
    private $costExtraUserUSD = 9;
    private $costExtraBranchOfficeMXN = 499;
    private $costExtraBranchOfficeUSD = 29;
    private $planPriceEmprendedorUSD = 29;
    private $planPriceEmpresarialUSD = 49;

    public function validateData(Request $request) {
        $data = $request->all();
        if(isset($data['conektaTokenId']) && isset($data['planExtras']) && isset($data['paymentData'])) {

            $plan = DB::table('plans')->where('id', Auth::user()->id_plan)->first();
            if($plan == null) return response()->json(['result' => false, 'msg' => 'No fue posible encontrar el tipo de plan']);

            $company = Companie::find(Auth::user()->companie);

            $conektaPlanId = $plan->id_conekta;

            // check if pay in USD
            $currency = 'MXN';
            if($company->id_code_country != 2) {
                $conektaPlanId = $conektaPlanId.'-USD';
                $currency = 'USD';
            }

            $userExtras = $this->getExtraUsers($company, $plan);

            // Calcula costo si tiene usuario y sucursales extras
            if($data['planExtras'] > 0) {
                if($currency == 'USD') {
                    $extraCost = $userExtras * $this->costExtraUserUSD;
                } else $extraCost = $userExtras * $this->costExtraUserMXN;

                if(($branchOfficeExtras = $company->no_branch_office - 1) > 0) {
                    if($currency == 'USD') $extraCost += $branchOfficeExtras * $this->costExtraBranchOfficeUSD;
                    else $extraCost += $branchOfficeExtras * $this->costExtraBranchOfficeMXN;
                }

                $planPrice = $plan->price;
                if ($plan->id == $this->planEmpresarial) {
                    if($currency == 'USD') $planPrice = $this->planPriceEmpresarialUSD;
                }
                if ($plan->id == $this->planEmprendedor) {
                    if($currency == 'USD') $planPrice = $this->planPriceEmprendedorUSD;
                }

                $extraCost += $planPrice;
                $iva = $extraCost * 0.16;
                $extraCost += $iva;

                if($userExtras > 0) {
                    $conektaPlanId = $conektaPlanId.'-'.$userExtras.'U';
                }
                if($branchOfficeExtras > 0) {
                    $conektaPlanId = $conektaPlanId.'-'.$branchOfficeExtras.'S';
                }

                // revisar si ya existe el plan en conekta
                $planConekta = ConektaLibrary::getPlan($conektaPlanId);
                if($planConekta == false) { // sino crear plan
                    if($currency == 'USD') $amount = $extraCost * 100; // en centavos
                    else $amount = $extraCost * 100; // en centavos

                    $newPlanConekta = ConektaLibrary::createPlan($conektaPlanId, $amount, $currency);
                    if($newPlanConekta) {
                        $conektaPlanId = $newPlanConekta;
                    } else {
                        return response()->json(['result' => false, 'msg' => 'Ocurrió un problema']);
                    }
                }
            }

            // crear cliente
            $customer = ConektaLibrary::createClient($company, $conektaPlanId, $data['conektaTokenId']);
            if($customer) {
                // guardar cliente y fecha de suscipción en db
                $company->id_conekta = $customer->id;
                $company->conekta_subscription_payment_date = date("Y-m-d", $customer->created_at);
                $company->save();
            } else {
                return response()->json(['result' => false, 'msg' => 'Ocurrió un problema']);
            }

            // guardar tarjeta en db
            $companyPayment = new company_payment();
            $companyPayment->id_company = $company->id;
            $companyPayment->card_name = strtoupper($data['paymentData']['card_name']);
            $companyPayment->card_number = $data['paymentData']['card_number'];
            $companyPayment->card_month = $data['paymentData']['card_month'];
            $companyPayment->card_year = $data['paymentData']['card_year'];
            $companyPayment->save();

            return response()->json(['result' => true]);
        } else {
            return response()->json(['result' => false, 'msg' => 'Ocurrió un problema al obtener los datos']);
        }
    }

    public function index() {
        $plan = DB::table('plans')->where('id', Auth::user()->id_plan)->first();
        $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
        $countryCode = DB::table('countries')
            ->where('id', $companie->id_code_country)
            ->first();
        $dateUpdate=$countryCode->updated_at;
        $dateUpdateModified = Carbon::parse($dateUpdate)->toDateString();
        $planExtras = [];
        $costExtraUser = $this->costExtraUserMXN;
        $costExtraBranch = $this->costExtraBranchOfficeMXN;

        if($companie->id_code_country != 2) {
            $costExtraUser = $this->costExtraUserUSD;
            $costExtraBranch = $this->costExtraBranchOfficeUSD;
        }

        $userExtras = $this->getExtraUsers($companie, $plan);
        $planExtras[] = "Con $userExtras usuario(s) extra / $" . $userExtras * $costExtraUser;

        if($plan->id != 1) {
            if(($branchOfficeExtras =  $companie->no_branch_office - 1) > 0) {
                $planExtras[] = "Con $branchOfficeExtras sucursal(es) extra / $" . $branchOfficeExtras * $costExtraBranch;
            }
        }

        // check if the company has a payment source
        $companyPayment = DB::table('company_payment')->where('id_company', Auth::user()->companie)->first();

        $subscriptionPaymentDate = null;
        if($companie->conekta_subscription_payment_date != null) {
            $companie->conekta_subscription_payment_date;
            $subscriptionPaymentDate = 'Fecha de creación de suscripción: '.$companie->conekta_subscription_payment_date;
        }

        // Calcula costo si tiene usuario y sucursales extras
        // check if pay in USD
        $currency = 'MXN';
        if($companie->id_code_country != 2) $currency = 'USD';

        if($currency == 'USD') {
            $extraCost = $userExtras * $this->costExtraUserUSD;
        } else $extraCost = $userExtras * $this->costExtraUserMXN;

        if(($branchOfficeExtras = $companie->no_branch_office - 1) > 0) {
            if($currency == 'USD') $extraCost += $branchOfficeExtras * $this->costExtraBranchOfficeUSD;
            else $extraCost += $branchOfficeExtras * $this->costExtraBranchOfficeMXN;
        }

        $planPrice = $plan->price;
        if ($plan->id == $this->planEmpresarial) {
            if($currency == 'USD') $planPrice = $this->planPriceEmpresarialUSD;
        }
        if ($plan->id == $this->planEmprendedor) {
            if($currency == 'USD') $planPrice = $this->planPriceEmprendedorUSD;
        }

        $extraCost += $planPrice;
        $iva = $extraCost * 0.16;
        $planExtras[] = "Impuestos $" . $iva;
        $extraCost += $iva;

        return view('adminlte::billing.index')->with([
            'dateUpdateModified' => $dateUpdateModified,
            'createdAt' => Carbon::parse($companie->created_at)->day,
            'total' => $extraCost,
            'currency' => $currency,
            'planName' => $plan->name,
            'planPrice' => $planPrice,
            'planExtras' => $planExtras,
            'companyPayment' => $companyPayment,
            'subscriptionPaymentDate' => $subscriptionPaymentDate
        ]);
    }

    private function getExtraUsers($companie, $plan) {
        $defaultUsers = 3;
        $extraUsers = 0;
        if ($plan->id == $this->planEmpresarial) $defaultUsers = 6;
        $treeJobCenterMain = treeJobCenter::where('parent', '#')
            ->where('company_id', Auth::user()->companie)->first();
        $profileJobCenterMain = profile_job_center::where('profile_job_centers_id', $treeJobCenterMain->id_inc)->first();

        if ($companie->no_branch_office > 1) {
            $jobCenters = profile_job_center::where('companie', Auth::user()->companie)
                ->where('profile_job_centers_id', '<>', $treeJobCenterMain->id_inc)
                ->get();
            foreach ($jobCenters as $jobCenter) {
                $employees = employee::where('profile_job_center_id', $jobCenter->id)->count();
                if ($employees > $defaultUsers) $extraUsers += $employees - $defaultUsers;
            }
            $employees = employee::where('profile_job_center_id', $profileJobCenterMain->id)->count();
            if ($employees > $defaultUsers) $extraUsers += $employees - $defaultUsers;
        } else {
            $employees = employee::where('profile_job_center_id', $profileJobCenterMain->id)->count();
            if ($employees > $defaultUsers) $extraUsers += $employees - $defaultUsers;
        }
        return $extraUsers;
    }

    public function webhook(Request $request) {
        switch($request->type) {
            case 'order.paid':
                $amount = $request->data['object']['amount'];
                $customerId = $request->data['object']['customer_info']['customer_id'];
                $paymentMethod = $request->data['object']['charges']['data']['payment_method']['object'];
                $paymentStatus = $request->data['object']['payment_status'];
                break;
            case 'subscription.paid':
                $planId = $request->data['object']['plan_id'];
                $customerId = $request->data['object']['customer_id'];
                break;
            case 'subscription.payment_failed':
                $planId = $request->data['object']['plan_id'];
                $customerId = $request->data['object']['customer_id'];
                break;
            case 'webhook_ping':
                return response('webhook ping', 200);
                break;
        }

        logger('WEBHOOK LOG', [
            'type' => $request->type,
            'data' => print_r($request->data, true)
        ]);

        return response('webhook done', 200);
    }
}
