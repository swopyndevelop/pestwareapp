<?php

namespace App\Http\Controllers\Treejobprofile;

use App\Http\Controllers\Security\SecurityController;
use App\job_title_profile;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\treeJobProfile;

class TreejobprofileController extends Controller
{

    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function create(){
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        return view('vendor.adminlte.treejobprofile.treejobprofile');
    }

    public function read(){
        $tree = DB::table('tree_job_profiles')->select('id_inc', 'id', 'parent', 'text')->where('company_id', Auth::user()->companie)->get();
        $args = array();
        foreach ($tree as $key => $dat) {
            $row = array();
            $row['id']= $dat->id;
            $row['parent'] = $dat->parent;
            $row['text']= $dat->text;
            $row['a_attr']['id_test'] = $dat->id_inc;
            array_push($args, $row);
        }
        return Response::json($args);
    }

    public function write(Request $request){
        $tmp = DB::table('tree_job_profiles')->where('company_id', Auth::user()->companie)->get();
        if( $request->ajax()){
            $data = $request->json()->all();
            $this->delete($data, $tmp);
            $this->update($data, $tmp);
            $this->add($data);
        }
    }

    public function update($data, $tmp)
    {
        foreach ($tmp as $t) {
            foreach ($data as $datas) {
                if(isset($datas['a_attr']['id_test'])){
                    if($t->id_inc == $datas['a_attr']['id_test']){
                        DB::table('tree_job_profiles')->where('id_inc', $t->id_inc)->where('company_id', Auth::user()->companie)
                            ->update(['id' => $datas['id'], 'parent' => $datas['parent'], 'text' => $datas['text']]);

                        //update job title profile
                        $jobtitle = job_title_profile::where('job_title_profiles_id', $t->id_inc)->first();
                        $jobtitle->job_title_name = $datas['text'];
                        $jobtitle->save();

                        //update role
                        $role = Role::where('display_name', $t->id_inc)->where('id_company', Auth::user()->companie)->first();
                        if (!empty($role)){
                            $role->name = $datas['text'];
                            $role->id_company = Auth::user()->companie;
                            $role->display_name = $t->id_inc;
                            $role->save();
                        }
                    }
                }
            }
        }
    }

    public function delete($data, $tmp)
    {
        foreach ($tmp as $tmp) {
            $is_Contained = false;
            foreach ($data as $d) {
                if(isset($d['a_attr']['id_test'])){
                    if ( ($tmp->id_inc == $d['a_attr']['id_test']))
                        $is_Contained = true;
                }
            }
            if(!$is_Contained){
                $del = treeJobProfile::where('id_inc', '=', $tmp->id_inc)->delete();
                $rol = Role::where('display_name', $tmp->id_inc)->where('id_company', Auth::user()->companie)->delete();
                $job = job_title_profile::where('job_title_profiles_id', $tmp->id_inc)->delete();
            }
        }   
    }

    public function add($data)
    {
        foreach ($data as $var) {
            if (!isset($var['a_attr']['id_test'])) {
                $update = new treeJobProfile;
                $update->id = $var['id'];
                $update->parent = $var['parent'];
                $update->company_id = Auth::user()->companie;
                $update->text = $var['text'];
                $update->save();

                //create job title profile
                $jobtitle = new job_title_profile;
                $jobtitle->job_title_name = $var['text'];
                $jobtitle->job_title_profiles_id = $update->id;
                $jobtitle->companie = Auth::user()->companie;
                $jobtitle->save();

                //create role
                $role = new Role;
                $role->name = $var['text'];
                $role->id_company = Auth::user()->companie;
                $role->display_name = $update->id;
                $role->save();
            }
        }
    }

}
