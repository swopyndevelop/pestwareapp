<?php

namespace App\Http\Controllers\Theme;

use App\employee;
use App\Notifications;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Response;
use Session;

class ThemeController extends Controller
{
    public function changeSidebarCollapse(Request $request) {
        $sidebarCollapse = $request->get('sidebarCollapse');
        if ($sidebarCollapse == "true") Session::put('sidebar-collapse', false);
        else Session::put('sidebar-collapse', true);
    }

    public function updateintroJs(Request $request){
        try {
            $user = User::find(Auth::user()->id);
            $user->introjs = $request->get('introjs');
            $user->save();
            return response()->json('ok');

        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateintroUpgrade(Request $request){
        try {
            $user = User::find(Auth::user()->id);
            $introUpgrade = $request->get('introUpgrade');
            if ($introUpgrade == true)
            {
                $introUpgrade = 1;
                $user->intro_upgrade = $introUpgrade;
                $user->save();
                return response()->json('ok');
            }

        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getNotifications() {
        try {
            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $notifications = Notifications::where('id_profile_job_center', $employee->profile_job_center_id)
                ->orderBy('created_at', 'desc')
                ->get();
            $unread = Notifications::where('id_profile_job_center', $employee->profile_job_center_id)
                ->where('is_read', 0)
                ->get();
            $notifications->map(function ($item) {
                $item['title'] = Str::limit($item->title, 15);
                $item['message'] = Str::limit($item->message, 35);
                return $item;
            });
            $count = $unread->count();
            return Response::json([
                'code' => 200,
                'notifications' => $notifications,
                'count' => $count
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function markAllReadNotifications() {
        try {
            Notifications::where('id_company', Auth::user()->companie)
                ->where('is_read', 0)
                ->update(['is_read' => 1]);
            return Response::json([
                'code' => 200,
                'message' => 'Notificaciones marcadas como leídas.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function markReadNotification($notificationId) {
        try {
            Notifications::where('id', $notificationId)
                ->update(['is_read' => 1]);
            return Response::json([
                'code' => 200,
                'message' => 'Notificación marcadas como leída.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getNotificationById($notificationId) {
        try {
            $notification = Notifications::find($notificationId);
            return Response::json([
                'code' => 200,
                'data' => $notification
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
