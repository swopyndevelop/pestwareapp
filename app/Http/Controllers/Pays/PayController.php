<?php

namespace App\Http\Controllers\Pays;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use DB;
use App\discount;
use Carbon\Carbon;
use Image;
use DateTime;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PayController extends Controller
{
    /**
     * [adminDiscount description]
     * @return [type] [description]
     */
    public function adminDiscount()
    {
        $search = \Request::get('search');
        $discount = DB::table('discounts')->select('id','title','description','percentage')->where('title','LIKE','%'.$search.'%')->orderBy('title','ASC')->paginate(10);
        return view('adminlte::discount.admin_discount')->with(['discount' => $discount]);
    }

    /**
     * [create_discount description]
     * @return [type] [description]
     */
    public function create_discount()
    {
        return view('adminlte::discount.creatediscount');
    }

    /**
     * [add_discount description]
     * @param Request $request [description]
     */
    public function add_discount(Request $request)
    {
        $discount  = new Discount;
        $discount->title        = $request->Nombre;
        $discount->description  = $request->Descripcion;
        $discount->percentage     = $request->Cantidad;
        $discount->save();
           
        return redirect()->route('discount_admin');
    }

    /**
     * [edit_discount description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit_discount($id)
    {
        $discount = Discount::find($id);
        return view('adminlte::discount.editdiscount')->with(['discount' => $discount]);

    }

    /**
     * [update_discount description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    
    /**
     * [update_discount description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update_discount($id, Request $request)
    {
        $discount  = Discount::find($id);
        $discount->title        = $request->Nombre;
        $discount->description  = $request->Descripcion;
        $discount->percentage     = $request->Cantidad;
        $discount->save();
           
        return redirect()->route('discount_admin');
        
    }

    /**
     * [delete_discount description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete_discount($id)
    {
        $discount = Discount::find($id);
        $discount->delete();
        return redirect()->route('discount_admin');
    }

}