<?php

namespace App\Http\Controllers\Pays;

use App\JobCenterSession;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\extra;
use DB;
use Illuminate\Support\Facades\Auth;

class ExtraController extends Controller
{
	/**
	 * [adminExtra description]
	 * @return [type] [description]
	 */
    public function adminExtra()
    {
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        $search = \Request::get('search');
        $extra = DB::table('extras')
            ->select('id','name','description','amount')
            ->where('name','LIKE','%'.$search.'%')
            ->where('profile_job_center_id', $jobCenterSession)
            ->orderBy('name','ASC')
            ->paginate(10);
        return view('adminlte::discount.admin_extra')->with(['extras' => $extra]);
    }

    /**
     * [create_discount description]
     * @return [type] [description]
     */
    public function create_extra()
    {
        return view('adminlte::discount.admin_extra');
    }

    /**
     * [add_discount description]
     * @param Request $request [description]
     */
    public function add_extra(Request $request)
    {
        $extra  = new extra;
        $extra->name        = $request->Nombre;
        $extra->description  = $request->Descripcion;
        $extra->amount     = $request->Cantidad;
        $extra->save();
           
        return redirect()->route('extra_admin');
    }

    /**
     * [edit_discount description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit_extra($id)
    {
        $extra = extra::find($id);
        return view('adminlte::discount.editextra')->with(['extra' => $extra]);

    }
    
    /**
     * [update_discount description]
     * @param  [type]  $id      [description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update_extra($id, Request $request)
    {
        $extra  = extra::find($id);
        $extra->name        = $request->Nombre;
        $extra->description  = $request->Descripcion;
        $extra->amount     = $request->Cantidad;
        $extra->save();
           
        return redirect()->route('extra_admin');
        
    }

    /**
     * [delete_discount description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete_extra($id)
    {
        $extra = extra::find($id);
        $extra->delete();
        return redirect()->route('extra_admin');
    }
}
