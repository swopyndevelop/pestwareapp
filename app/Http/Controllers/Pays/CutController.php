<?php

namespace App\Http\Controllers\Pays;

use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\profile_job_center;
use App\treeJobCenter;
use iio\libmergepdf\Merger;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\cash_cut;
use App\cut_service;
use Carbon\Carbon;
use PDF;
use NumeroALetras\NumeroALetras;
use Jenssegers\Date\Date;
use App\deposit_cut;
use Storage;
use Image;
use App\event;
use App\expense;
use App\employee;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;


class CutController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function index()
    {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $date = Carbon::now()->toDateString();
        $cuts = DB::table('cash_cuts')
            ->select('date','id')
            ->where('date',$date)
            ->where('user_id',auth()->user()->id)
            ->orderBy('created_at', 'ASC')
            ->first();

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        $employee =DB::table('employees')->select('id','employee_id')->where('employee_id',auth()->user()->id)->first();

            $order = DB::table('events as e')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('payment_methods as pm','so.id_payment_method','pm.id')
                ->join('payment_ways as pw','so.id_payment_way','pw.id')
                //->join('cashes as ch','so.id','ch.id_service_order')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->select('so.id as order','so.id_service_order','e.initial_hour','users.name','e.id_status',
                    'e.id_employee','e.x_cut','c.name as cliente','c.establishment_name as empresa',
                    'c.cellphone','cd.address','c.municipality','so.total',
                    'cd.address_number','cd.state','cd.email','c.colony'
                    ,'pl.key','pw.name as pago_realizado','pm.name as metodo')
                ->where('e.id_employee',$employee->id)
                ->where('e.x_cut',null)
                ->where('e.id_status',4)
                //->where('e.id_status','!=',null)
                //->where('e.id_status','!=',1)
                //->where('e.id_status','!=',2)
                ->get();

            $cashes = DB::table('cashes as c')
                ->join('service_orders as so','c.id_service_order','so.id')
                ->select('c.id_service_order as ser','c.payment')
                ->get();

            $product = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('products as p','pc.id_product','p.id')
            ->select('pl.id_service_order','p.name','pc.dose','pc.quantity','p.id as id_product')
            ->where('so.id_job_center', $jobCenterSession)
            ->get();

        $symbol_country = CommonCompany::getSymbolByCountry();

        return view('adminlte::cuts.index')
            ->with(['order' => $order, 'product' =>$product, 'date' => $date, 'cuts' => $cuts, 'employee' => $employee,
                'cashes' => $cashes, 'symbol_country' => $symbol_country
            ]);
    }

    public function arqueo($user)
    {

        try {

            DB::beginTransaction();

            if ($user == 0) {
                $user = auth()->user()->id;
            } else {
                $employee = employee::find($user);
                $user = $employee->employee_id;
            }

            $company = DB::table('users')->where('id', $user)->first()->companie;
            $today = Carbon::now()->toDateString();
            //Numero de servicios
            $service = DB::table('events as e')
            ->join('service_orders as so','e.id_service_order','so.id')
            ->join('employees as em','e.id_employee','em.id')
            ->join('users','em.employee_id','users.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('establishment_types as et','q.establishment_id','et.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('payment_methods as pm','so.id_payment_method','pm.id')
            ->join('payment_ways as pw','so.id_payment_way','pw.id')
            //->join('cashes as ch','so.id','ch.id_service_order')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->select('so.id as order','so.id_service_order','e.initial_hour','users.name','e.id_status',
                'e.id_employee','e.x_cut','c.name as cliente','c.establishment_name as empresa',
                'c.cellphone','cd.address','c.municipality','so.total',
                'cd.address_number','cd.state','cd.email','c.colony'
                ,'pl.key','pw.name as pago_realizado','pm.name as metodo')
            ->where('em.employee_id', $user)
            ->where('e.x_cut',null)
            ->where('e.id_status',4)
            //->where('e.id_status','!=',null)
            //->where('e.id_status','!=',1)
            //->where('e.id_status','!=',2)
            ->select(DB::raw('count(*) as service'))->first();

            if ($service->service == 0) {
                return response()->json([
                    'status' => 'empty'
                ]);
            }

            //Total Efectivo
            $total_efec = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('e.x_cut', null)
                ->where('pm.name','Efectivo');

            $totalCash = $total_efec->where('ch.payment',2)->select(DB::raw('SUM(so.total) as total'))->first()->total;
            $subCash = $total_efec->where('ch.payment',2)->select(DB::raw('SUM(ch.amount_received) as total'))->first()->total;

            $total_credit = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('e.x_cut', null)
                ->where('ch.id_payment_way',1)
                //->where('pm.name','Tarjeta de Crédito')
                ->select(DB::raw('SUM(so.total) as total_credito'))->first();

            $total_deb = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('e.x_cut', null)
                ->where('pm.name','Tarjeta de Débito');

            $totalDebit = $total_deb->select(DB::raw('SUM(so.total) as total'))->first()->total;
            $subDebit = $total_deb->where('ch.payment',2)->select(DB::raw('SUM(so.total) as total'))->first()->total;

            $total_tar_cred = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('e.x_cut', null)
                ->where('pm.name','Tarjeta de Crédito');

            $totalCredit = $total_tar_cred->select(DB::raw('SUM(so.total) as total'))->first()->total;
            $subCredit = $total_tar_cred->where('ch.payment',2)->select(DB::raw('SUM(so.total) as total'))->first()->total;

            $total_transf = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('e.x_cut', null)
                ->where('pm.name','Transferencia');

            $totalTransfer = $total_transf->select(DB::raw('SUM(so.total) as total'))->first()->total;
            $subTransfer = $total_transf->where('ch.payment',2)->select(DB::raw('SUM(so.total) as total'))->first()->total;

            $total_depos = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('e.x_cut', null)
                ->where('pm.name','Deposito');

            $totalDeposit = $total_depos->select(DB::raw('SUM(so.total) as total'))->first()->total;
            $subDeposit = $total_depos->where('ch.payment',2)->select(DB::raw('SUM(so.total) as total'))->first()->total;

            $total_cheq = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('pm.name','Cheque')
                ->where('e.id_status',4)
                ->where('e.x_cut', null);

            $totalCheque = $total_cheq->select(DB::raw('SUM(so.total) as total'))->first()->total;
            $subCheque = $total_cheq->where('ch.payment',2)->select(DB::raw('SUM(so.total) as total'))->first()->total;

            $total_query = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('e.x_cut', null);

            $total = $total_query->select(DB::raw('SUM(so.total) as total'))->first()->total;
            $subTotal = $total_query->where('ch.payment',2)->select(DB::raw('SUM(so.total) as total'))->first()->total;

            //Num de servicios programados
            $program = DB::table('events as e')
                ->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('payment_methods as pm','so.id_payment_method','pm.id')
                ->join('payment_ways as pw','so.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.x_cut',null)
                ->where('e.id_status',4)
                //->where('e.id_status','!=',null)
                //->where('e.id_status','!=',1)
                //->where('e.id_status','!=',2)
                ->select(DB::raw('count(*) as service_p'))->first();

            //Num de servicios realizados
            $realized = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('e.x_cut',null)
                ->select(DB::raw('count(*) as service_r'))->first();

            //Total de gastos en efectivo
            $expensesEfect = DB::table('expenses as e')
                ->join('payment_methods as pm', 'e.id_payment_method', 'pm.id')
                ->where('id_user', $user)
                ->where('cut', 0)
                ->where('pm.name', "Efectivo")
                ->select(DB::raw('SUM(e.total) as total'))->first();

            //Total de otras formas
            $total_other = $totalDebit + $totalCheque + $totalTransfer +
                $totalDeposit + $totalCredit;
            if($total_other == null){
                $total_other = 0;
            }

            //Total de aduedos
            $tot_ad = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','ch.id_payment_method','pm.id')
                ->join('payment_ways as pw','ch.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('em.employee_id', $user)
                ->where('e.id_status',4)
                ->where('ch.payment',1)
                ->where('e.x_cut',null)
                ->select(DB::raw('SUM(so.total) as adeudo'))->first();

            $tot_cred = $tot_ad->adeudo;
            if($tot_cred == null){
                $tot_cred = 0;
            }


            $center = DB::table('employees')->select('profile_job_center_id')->where('employee_id', $user)->first();

            //Guardar corte
            $cut = new cash_cut;
            $cut->name = 'Corte-'.' '.Carbon::now()->format('Y-m-d');
            $cut->user_id = $user;
            $cut->total_efec = $totalCash;
            $cut->sub_efec = $subCash;
            $cut->total_transf = $totalTransfer;
            $cut->sub_transf = $subTransfer;
            $cut->total_cred = $totalCredit;
            $cut->sub_cred = $subCredit;
            $cut->total_deb = $totalDebit;
            $cut->sub_deb = $subDebit;
            $cut->total_cheq = $totalCheque;
            $cut->sub_cheq = $subCheque;
            $cut->total_depos = $totalDeposit;
            $cut->sub_depos = $subDeposit;
            $cut->total = $total;
            $cut->subtotal = $subTotal;
            $cut->total_arqueo = 0;
            $cut->desfase = 0;
            $cut->arqueo = 1;
            $cut->tot_credit = $tot_cred;
            $cut->total_adeudo = $tot_ad->adeudo;
            $cut->tot_other = $total_other;
            $cut->total_gastos = $expensesEfect->total;
            $cut->depositado = 0;
            $cut->delivered = 0;
            $cut->sucursal_id = $center->profile_job_center_id;
            $cut->date = $today;
            $cut->time = Carbon::now()->toTimeString();
            $cut->programs = $program->service_p;
            $cut->realize = $realized->service_r;
            $cut->companie = $company;
            $cut->save();

            $cutUpdate = cash_cut::find($cut->id);
            $cutUpdate->name = "Corte-" . $cut->id . " " . $cut->date . "-" . $cut->time;
            $cutUpdate->save();

            $order = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('users','em.employee_id','users.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
               // ->join('cashes as ch','so.id','ch.id_service_order')
                ->join('payment_methods as pm','so.id_payment_method','pm.id')
                ->join('payment_ways as pw','so.id_payment_way','pw.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->select('e.id as event','so.id as order','so.id_service_order','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                    'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality',
                    'q.id_plague_jer','so.total','pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                    'c.colony','pm.name as metodo','pw.name as pago_realizado','e.x_cut','pl.key')
                    ->where('em.employee_id', $user)
                    ->where('e.x_cut',null)
                    ->where('e.id_status', 4)
                    //->where('e.id_status','!=',null)
                    //->where('e.id_status','!=',1)
                    //->where('e.id_status','!=',2)
                ->orderBy('so.id','DESC')->get()->toArray();


            foreach ($order as $value){
                $ass = event::find($value->event);
                $ass->x_cut = 1;
                $ass->date_cut = Carbon::now()->toDateString();
                $ass->id_cut = $cut->id;
                $ass->save();
            }

            $expenses = DB::table('expenses as e')
                ->where('id_user', $user)
                ->where('cut', 0)
                ->get();

            foreach ($expenses as $expense) {
                $exp = expense::find($expense->id);
                $exp->cut = 1;
                $exp->date_cut = Carbon::now()->toDateString();
                $exp->id_cut = $cut->id;
                $exp->save();
            }

            DB::commit();

            return response()->json([
                'status' => 'ok'
            ]);


        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 'fail',
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    public function Cuts_Tec()
    {
        $cortes = DB::table('cash_cuts')->select('id','name','total_efec','total','total_arqueo',
                'desfase','date','depositado','time','programs','realize','tot_other','tot_credit','user_id')->where('user_id',auth()->user()->id)->where('arqueo',1)
                ->orderBy('date','DESC')->get();

        return view('adminlte::cuts.cuts_tech')->with(['cortes' => $cortes]);
    }

    public function report($id)
    {
        $id = SecurityController::decodeId($id);
        $cash_id = cash_cut::find($id);
        // Middleware to deny access to data from another company
        if ($cash_id->companie != auth()->user()->companie) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        $cortes = DB::table('cash_cuts')->where('id',$id)->first();
        $tecnico = DB::table('users')->select('name','companie')->where('id',$cortes->user_id)->first();
        $employee = DB::table('employees')->where('employee_id', $cortes->user_id)->first();
        $date = Carbon::parse($cortes->date)->toDateString();

        $order = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
            ->join('users','em.employee_id','users.id')
            ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
            ->join('service_orders as so','e.id_service_order','so.id')
            ->join('users as u','so.user_id','u.id')
            ->join('statuses as st','so.id_status','st.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('establishment_types as et','q.establishment_id','et.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            ->join('cashes as cash','e.id','cash.id_event')
            ->join('payment_methods as pm','cash.id_payment_method','pm.id')
            ->join('payment_ways as pw','cash.id_payment_way','pw.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->select('so.id as order','so.id_service_order','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality',
                'q.id_plague_jer','so.total','pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                'c.colony','pm.name as metodo','pw.name as pago_realizado','pl.key')
            ->where('em.employee_id', $cortes->user_id)
            ->where('e.companie', $tecnico->companie)
            ->where('e.date_cut',$date)
            ->where('e.id_cut', $id)
            ->orderBy('e.initial_date','ASC')
            ->orderBy('e.initial_hour','ASC')
            ->get();

        $cashes = DB::table('cashes as c')
        ->join('service_orders as so','c.id_service_order','so.id')
        ->select('c.id_service_order as ser','c.payment', 'c.amount_received')
        ->get();

        $product = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('products as p','pc.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('pl.id_service_order','p.name','pc.dose','pc.quantity','p.id as id_product', 'pu.name as unit')
            ->where('so.id_job_center', $jobCenterSession)
            ->get();

        $expenses = DB::table('expenses as e')
            ->join('payment_methods as pm', 'e.id_payment_method', 'pm.id')
            ->select('e.expense_name', 'e.total', 'pm.name as method')
            ->where('id_user', $cortes->user_id)
            ->where('date_cut',$date)
            ->where('id_cut', $id)
            ->where('cut', 1)
            ->get();

        $expenseTotal = 0;

        foreach ($expenses as $expense) {
            $expenseTotal += $expense->total;
        }

        $effectiveDiff = $cortes->total_efec - $cortes->sub_efec;
        $effectiveTotal = $cortes->sub_efec - $expenseTotal;

        $sum = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('events as e','so.id','e.id_service_order')
            ->join('employees as em','e.id_employee','em.id')
            ->join('users','em.employee_id','users.id')
            ->join('products as p','pc.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->join('storehouses as s', 's.id_product', 'p.id')
            ->where('e.date_cut',$date)
            ->where('s.id_employee', $employee->id)
            ->where('so.companie', $tecnico->companie)
            ->select(DB::raw('SUM(pc.quantity) as sumx'),'pl.id_service_order','p.id as id_product','p.name','em.id',
                'users.id as usuario', 'pu.name as unit', 's.stock', 's.stock_other_units')
            ->groupBy('p.id','em.id')
            ->get();

        $imagen = DB::table('companies')->select('pdf_logo','pdf_sello','phone','licence','facebook')->where('id',$tecnico->companie)->first();
        $symbol_country = CommonCompany::getSymbolByCountry();

        PDF::loadView('vendor.adminlte.cuts.report_cut',['cortes' => $cortes, 'tecnico' => $tecnico,
            'date' => $date, 'order' => $order, 'product' => $product, 'sum' => $sum, 'imagen' => $imagen,
            'cashes' => $cashes, 'expenses' => $expenses, 'expenseTotal' => $expenseTotal,'symbol_country' => $symbol_country])
            ->setPaper('A4','landscape')
            ->save(storage_path().'/app/pdf_cuts/'. $cortes->name.'.pdf');

        PDF::loadView('vendor.adminlte.cuts.report_cut_total',['cortes' => $cortes, 'tecnico' => $tecnico,
            'date' => $date, 'order' => $order, 'product' => $product, 'sum' => $sum, 'imagen' => $imagen,
            'cashes' => $cashes, 'expenses' => $expenses, 'expenseTotal' => $expenseTotal, 'effectiveTotal' => $effectiveTotal,
            'effectiveDiff' => $effectiveDiff, 'symbol_country' => $symbol_country])
            ->setPaper('A4','landscape')
            ->save(storage_path().'/app/pdf_cuts_total/'. $cortes->name.'.pdf');

        $pdfServices = storage_path().'/app/pdf_cuts/'. $cortes->name.'.pdf';
        $pdfTotal = storage_path().'/app/pdf_cuts_total/'. $cortes->name.'.pdf';

        $combine = new Merger();
        $docs = [$pdfServices, $pdfTotal];

        foreach ($docs as $doc) {
            $combine->addFile($doc);
        }
        $pdfFinal = $combine->merge();

        $namePdf = $cortes->name.'.pdf';
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=$namePdf");
        echo $pdfFinal;
        exit;

    }
    //Desfase
    public function desfase($id)
    {
        $cortes = DB::table('cash_cuts')->select('id','name','total_efec','total_transf','total','total_arqueo',
                'desfase','date','user_id','total_cred','total_deb','total_cheq','total_depos')->where('id',$id)->first();
        $tecnico = DB::table('users')->select('name','email')->where('id',$cortes->user_id)->first();
        $moneda = NumeroALetras::convertir($cortes->desfase, 'pesos');
        Date::setLocale('es');
        $fecha = $cortes->date;
        $fecha_pago = Carbon::parse($fecha)->addDays(3);
        $fecha1 = Date::parse($fecha)->format('l j F Y');
        $fecha_pago1 = Date::parse($fecha_pago)->format('l j F Y');
        $pdf = PDF::loadView('vendor.adminlte.cuts._pagare',['cortes' => $cortes, 'tecnico' => $tecnico,
            'fecha1' => $fecha1, 'fecha_pago1' => $fecha_pago1, 'moneda' => $moneda])->setPaper('A5','landscape');
        return $pdf->download('Pagare CD-'.$cortes->id.'.pdf');
    }

    public function deposito($id)
    {
        $cortes = DB::table('cash_cuts')
            ->select('id','name','total_efec','total_transf','total','total_arqueo',
            'desfase','date','user_id','total_cred','total_deb','total_cheq','total_depos')
            ->where('id',$id)
            ->first();
        return view('adminlte::cuts._deposito')->with(['cortes' => $cortes]);
    }

    public function add_deposito(Request $request)
    {
        $deposito = deposit_cut::where('id_cash_cut', $request->corte)->first();
        if (!$deposito) $deposito = new deposit_cut;
        $deposito->id_cash_cut = $request->corte;
        $deposito->date = $request->dateDep;
        $deposito->hour = $request->hourDep;
        $deposito->total = $request->totalDep;
        $deposito->user_id = auth()->user()->id;
        //Imagen
        $this->validate($request, ['imagen' => 'required',]);
        $file = Input::file('imagen');
        $img = Image::make($file)->fit(600, 800, function ($constraint) { $constraint->upsize(); })->encode('jpeg');
        $deposito->image = $img;
        $deposito->save();
        //Editar Registro
        $cut = cash_cut::find($request->corte);
        $cut->depositado = 1;
        $cut->save();
        $id = auth()->user()->id;

        return redirect()->route('cuts_admin');
    }

    public function Cuts_Consum($id)
    {
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        $cortes = DB::table('cash_cuts')->select('id','name','total_efec','total','total_arqueo',
                'desfase','date','depositado','time','programs','realize','tot_other','tot_credit','user_id')->where('id',$id)->first();
        $sum = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('events as e','so.id','e.id_service_order')
            ->join('employees as em','e.id_employee','em.id')
            ->join('users','em.employee_id','users.id')
            ->join('products as p','pc.id_product','p.id')
            ->where('e.id_cut', $cortes->id)
            ->where('so.id_job_center', $jobCenterSession)
            ->select(DB::raw('SUM(pc.quantity) as sumx'),'pl.id_service_order','p.id as id_product','p.name','em.id','users.id as usuario')->groupBy('p.id','em.id')->get();
        return view('adminlte::cuts._menu3')->with(['cortes' => $cortes,'sum' => $sum]);

    }

    public function Cuts_Consum2($id)
    {
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        $cortes = DB::table('cash_cuts')->select('id','name','total_efec','total','total_arqueo',
                'desfase','date','depositado','time','programs','realize','tot_other','tot_credit','user_id')->where('id',$id)->first();
        $tecnico = DB::table('users')->select('name','companie')->where('id',$cortes->user_id)->first();
        $date = Carbon::parse($cortes->date)->toDateString();
        $sum = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('events as e','so.id','e.id_service_order')
            ->join('employees as em','e.id_employee','em.id')
            ->join('users','em.employee_id','users.id')
            ->join('products as p','pc.id_product','p.id')
            ->where('e.id_cut',$cortes->id)
            ->where('so.id_job_center', $jobCenterSession)
            ->select(DB::raw('SUM(pc.quantity) as sumx'),'pl.id_service_order','p.id as id_product','p.name','em.id','users.id as usuario')->groupBy('p.id','em.id')->get();
        return view('adminlte::cuts._adeudo')->with(['cortes' => $cortes,'sum' => $sum]);
    }

    public function adminCuts($jobcenter = 0, Request $request)
    {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();

        $idJobCenter = 0;
        $jobCenters = \Illuminate\Support\Facades\DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        $search = $request->get('search');
        $cortes = DB::table('cash_cuts as c')
            ->join('users as u','u.id','c.user_id')
            ->select('c.id','u.name as tecnico','total_efec','total','total_arqueo', 'desfase','date','depositado','delivered',
                'programs','realize','tot_other','tot_credit', 'arqueo','time','c.user_id','c.sucursal_id')
            ->where('arqueo',1)
            ->where('c.sucursal_id', $profileJobCenter)
            ->where('u.name', 'like', '%' . $search . '%')
            ->orderBy('c.created_at','DESC')
            ->paginate(10);

        $cortes_sn = DB::table('cash_cuts as c')
            ->join('users as u','u.id','c.user_id')
            ->join('employees as e', 'u.id', 'e.employee_id')
            ->select('c.id','u.name as tecnico','total_efec','total','total_arqueo', 'desfase','date','depositado',
                'delivered','programs','realize','tot_other','tot_credit', 'arqueo','time')
        ->where('e.profile_job_center_id', $profileJobCenter)
        ->where('arqueo',0)->get();
        $sum = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('events as e','so.id','e.id_service_order')
            ->join('employees as em','e.id_employee','em.id')
            ->join('users','em.employee_id','users.id')
            ->join('products as p','pc.id_product','p.id')
            ->where('so.id_job_center', $profileJobCenter)
            ->select(DB::raw('SUM(pc.quantity) as sumx'),'pl.id_service_order','p.id as id_product','p.name','em.id','users.id as usuario')
            ->groupBy('p.id','em.id')->get();

        $symbol_country = CommonCompany::getSymbolByCountry();

        return view('adminlte::cuts.cuts_admin')
            ->with(['cortes' => $cortes, 'cortes_sn' => $cortes_sn, 'sum' => $sum, 'symbol_country' => $symbol_country, 'jobCenterSession' => $jobCenterSession,
                'jobCenters' => $jobCenters, 'profileJobCenterSelect' => $profileJobCenterSelect, 'treeJobCenterMain' => $treeJobCenterMain]);
    }

    public function Dep_detail($id)
    {
        $id = SecurityController::decodeId($id);
        $dep = DB::table('deposit_cuts as dc')
            ->join('users as u','dc.user_id','u.id')
            ->select('date','hour','id_cash_cut','image','total','u.name')->where('id_cash_cut',$id)->first();

        return view('adminlte::cuts._detail_dep')->with(['dep' => $dep]);
    }

    public function Quit($id)
    {
        $adeudo = cash_cut::find($id);
        $adeudo->delivered = 1;
        $adeudo->save();
        $cortes = DB::table('cash_cuts')->select('id','name','total_efec','total_transf','total','total_arqueo',
                'desfase','date','user_id','total_cred','total_deb','total_cheq','total_depos')->where('id',$id)->first();
        $tecnico = DB::table('users')->select('name','email')->where('id',$cortes->user_id)->first();
        $moneda = NumeroALetras::convertir($cortes->total_arqueo, 'pesos');
        Date::setLocale('es');
        $fecha = Carbon::now();
        $fecha_pago = Carbon::parse($fecha)->addDays(3);
        $fecha1 = Date::parse($fecha)->format('l j F Y');
        $fecha_pago1 = Date::parse($fecha_pago)->format('l j F Y');
        $pdf = PDF::loadView('vendor.adminlte.cuts._pagare',['cortes' => $cortes, 'tecnico' => $tecnico,
            'fecha1' => $fecha1, 'fecha_pago1' => $fecha_pago1, 'moneda' => $moneda])->setPaper('A5','landscape');
        return $pdf->download('Entrega CD-'.$cortes->id.'.pdf');
    }

    public function View($id)
    {
        $id = SecurityController::decodeId($id);
        $cash_id = cash_cut::find($id);
        // Middleware to deny access to data from another company
        if ($cash_id->companie != auth()->user()->companie) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        $cortes = DB::table('cash_cuts')->select('id','name','total_efec','total_transf','total','total_arqueo',
                'desfase','date','user_id','total_cred','total_deb','total_cheq','total_depos','depositado','time')->where('id',$id)->first();
        $tecnico = DB::table('users')->select('name','companie')->where('id',$cortes->user_id)->first();
        $date = Carbon::parse($cortes->date)->toDateString();
        $order = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
            ->join('users','em.employee_id','users.id')
            ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
            ->join('service_orders as so','e.id_service_order','so.id')
            ->join('users as u','so.user_id','u.id')
            ->join('statuses as st','so.id_status','st.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('establishment_types as et','q.establishment_id','et.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            //->join('cashes as ch','so.id','ch.id_service_order')
            ->join('payment_methods as pm','so.id_payment_method','pm.id')
            ->join('payment_ways as pw','so.id_payment_way','pw.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->select('so.id as order','so.id_service_order','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality',
                'q.id_plague_jer','so.total','pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                'c.colony','pm.name as metodo','pw.name as pago_realizado','pl.key')
            ->where('em.employee_id', $cortes->user_id)
            ->where('e.companie', $tecnico->companie)
            ->where('e.date_cut',$date)
            ->where('e.id_cut', $cortes->id)
            ->orderBy('e.initial_date','ASC')->get();

            $cashes = DB::table('cashes as c')
            ->join('service_orders as so','c.id_service_order','so.id')
            ->select('c.id_service_order as ser','c.payment')
            ->get();

            $product = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('products as p','pc.id_product','p.id')
            ->select('pl.id_service_order','p.name','pc.dose','pc.quantity','p.id as id_product')
            ->where('so.id_job_center', $jobCenterSession)
            ->get();

        $symbol_country = CommonCompany::getSymbolByCountry();

        return view('adminlte::cuts._view')
            ->with(['cortes' => $cortes, 'tecnico' => $tecnico, 'date' => $date, 'order' => $order, 'product' => $product,
                'cashes' => $cashes, 'symbol_country' => $symbol_country]);
    }

    public function sn_arqueo($id)
    {
        $x = $id;
        return view('adminlte::cuts._snarqueo')->with(['x' => $x]);
    }
    public function cut_store(Request $request)
    {
        $cut = DB::table('cash_cuts')->where('id',$request->corter)->first();
        //Marcar las cantidades introducidas en el arqueo
        $mil = $request->milx*1000;
        $quinientos = $request->quinientosx*500;
        $doscientos = $request->doscientosx*200;
        $cien = $request->cienx*100;
        $cincuenta = $request->cincuentax*50;
        $veinte = $request->veintex*20;
        $diez = $request->diezx*10;
        $cinco = $request->cincox*5;
        $dos = $request->dosx*2;
        $uno = $request->unox*1;
        $cincuenta_centavos = $request->cincuenta_centavosx*.50;
        $veinte_centavos = $request->veinte_centavosx*.20;
        $diez_centavos = $request->diez_centavosx*.10;
        $sum_arq = $mil+$quinientos+$doscientos+$cien+$cincuenta+$veinte+$diez+$cinco+$dos+$uno+$cincuenta_centavos+$veinte_centavos+$diez_centavos;
        $desf = $cut->total_efec-$sum_arq;

        //Guardar corte
        $cut = cash_cut::find($request->corter);
        $cut->total_arqueo = $sum_arq;
        $cut->desfase = $desf;
        $cut->arqueo = 1;
        $cut->time = Carbon::now()->toTimeString();
        $cut->save();

        return response()->json();
    }
}
