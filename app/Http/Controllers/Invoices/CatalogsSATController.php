<?php

namespace App\Http\Controllers\Invoices;

use App\BillingMode;
use App\Common\CommonNotifications;
use App\Common\Constants;
use App\companie;
use App\CustomerBankAccount;
use App\Http\Controllers\Security\SecurityController;
use App\Library\FacturamaLibrary;
use App\Plan;
use App\profile_job_center;
use App\ProfileJobCenterBilling;
use App\UnitSat;
use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Response;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;

class CatalogsSATController extends Controller
{
    public function getCatalogsFiscalRegimens(Request $request) {
        $jobCenterId = $request->get('jobCenterId');
        $profileJobCenter = profile_job_center::where('profile_job_centers_id', $jobCenterId)->first();
        $billing = ProfileJobCenterBilling::where('id_profile_job_center', $profileJobCenter->id)->first();
        $fiscalRegime = null;
        if ($billing) $fiscalRegime = $billing->fiscal_regime;
        $regimens = FacturamaLibrary::getCatalogsFiscalRegimens();
        return Response::json([
            'regimens' => $regimens,
            'fiscalRegime' => $fiscalRegime
        ]);
    }

    public function getCatalogsPaymentForms() {
        $paymentForms = FacturamaLibrary::getCatalogsPaymentForms();
        return Response::json([
            'paymentForms' => $paymentForms,
        ]);
    }

    public function getCatalogsPaymentMethods() {
        $paymentMethods = FacturamaLibrary::getCatalogsPaymentMethods();
        return Response::json([
            'paymentMethods' => $paymentMethods,
        ]);
    }

    public function getCatalogsTypeCfdi() {
        $typeCfdi = FacturamaLibrary::getCatalogsCfdiUses();
        return Response::json([
            'typeCfdi' => $typeCfdi
        ]);
    }

    public function getBillingModes() {
        $billingModes = BillingMode::all();
        return Response::json([
            'billingModes' => $billingModes
        ]);
    }

    public function getBankAccounts($customerId) {
        $customerId = SecurityController::decodeId($customerId);
        $bankAccounts = CustomerBankAccount::where('id_customer', $customerId)->get();
        return Response::json([
            'bankAccounts' => $bankAccounts
        ]);
    }

    public function saveBankAccount(Request $request) {
        try {
            $customerId = $request->get('customerId');
            $bank = $request->get('bank');
            $account = $request->get('account');
            $customerId = SecurityController::decodeId($customerId);

            $bankAccount = new CustomerBankAccount();
            $bankAccount->id_customer = $customerId;
            $bankAccount->bank = $bank;
            $bankAccount->account = $account;
            $bankAccount->is_default = 1;
            $bankAccount->save();

            $bankAccounts = CustomerBankAccount::where('id_customer', $customerId)->get();

            foreach ($bankAccounts as $account) {
                if ($account->id != $bankAccount->id) {
                    $account->is_default = 0;
                    $account->save();
                }
            }

            return Response::json([
                'bankAccounts' => $bankAccounts
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getProductsOfServicesByKeyword($keyword) {
        $keys = FacturamaLibrary::getCatalogsProductsOrServicesByKeyword($keyword);
        $output = '<ul class="dropdown-menu" style="display:block; position:absolute;">';
        foreach ($keys as $row) {
            $output .= '<li id="getdataProductKey" data-id="'. $row->Value .'"><a href="#">' . $row->Value . ' - ' . $row->Name . '</a></li>';
        }
        $output .= '</ul>';
        echo $output;
    }

    public function getUnitsByKeyword($keyword) {
        $units = UnitSat::where('name', 'like', '%' . $keyword . '%')->get();
        $output = '<ul class="dropdown-menu" style="display:block; position:absolute;">';
        foreach ($units as $row) {
            $output .= '<li id="getdataProductUnit" data-id="'. $row->id .'"><a href="#">' . $row->key . ' - ' . $row->name . '</a></li>';
        }
        $output .= '</ul>';
        echo $output;
    }

    public function getJobCentersFolios() {
        try {
            $jobCentersWithFolios = DB::table('profile_job_centers_billing as pjcb')
                ->join('profile_job_centers as pjc', 'pjcb.id_profile_job_center', 'pjc.id')
                ->where('companie', Auth::user()->companie)
                ->select('pjc.id', 'pjc.name', 'pjcb.folios', 'pjcb.folios_consumed')
                ->get();
            return Response::json([
                'code' => 200,
                'jobCentersFolios' => $jobCentersWithFolios
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    public function getJobCentersFoliosById($id) {
        try {
            $jobCentersWithFolios = DB::table('profile_job_centers_billing as pjcb')
                ->join('profile_job_centers as pjc', 'pjcb.id_profile_job_center', 'pjc.id')
                ->where('pjc.id', $id)
                ->select('pjc.id', 'pjc.name', 'pjcb.folios')
                ->first();
            return Response::json([
                'code' => 200,
                'data' => $jobCentersWithFolios
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    public function transferFolios(Request $request) {
        try {
            $origin = $request->get('origin');
            $source = $request->get('source');
            $quantity = $request->get('quantity');

            $jobCenterOrigin = ProfileJobCenterBilling::where('id_profile_job_center', $origin)->first();
            if ($quantity > $jobCenterOrigin->folios) {
                return Response::json([
                    'code' => 500,
                    'message' => 'La cantidad de folios es mayor a la existencia actual. Se ha detectado un comportamiento indebido en el sistema. Es posible que la cuenta sea suspendida.'
                ]);
            }
            $jobCenterOrigin->folios = $jobCenterOrigin->folios - $quantity;
            $jobCenterOrigin->save();

            $jobCenterSource = ProfileJobCenterBilling::where('id_profile_job_center', $source)->first();
            $jobCenterSource->folios = $jobCenterSource->folios + $quantity;
            $jobCenterSource->save();

            return Response::json([
                'code' => 200,
                'message' => 'Folios transferidos correctamente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    public function updateIsFoliosConsumed(Request $request) {
        try {
            $isFoliosConsumed = $request->get('isFoliosConsumed');

            $company = companie::find(Auth::user()->companie);
            $company->is_folios_consumed = $isFoliosConsumed;
            $company->save();

            return Response::json([
                'code' => 200,
                'message' => 'Configuración Aplicada.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    public function updateIsBuyFolios(Request $request) {
        try {
            $isBuyFolios = $request->get('isBuyFolios');

            $company = companie::find(Auth::user()->companie);
            $company->is_buy_folios = $isBuyFolios;
            $company->save();

            return Response::json([
                'code' => 200,
                'message' => 'Configuración Aplicada.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function purchaseFolios(Request $request) {
        try {

            $package = $request->get('package');
            $plan = Plan::find($package);
            $company = companie::find(Auth::user()->companie);
            $price = $plan->price / 0.01; // pennies

            // Try charge in stripe.
            $stripe = new StripeClient(env('STRIPE_SECRET'));

            $stripe->charges->create([
                'customer' => $company->id_stripe_customer,
                'amount' => $price,
                'currency' => 'mxn',
                'description' => $plan->name,
            ]);

            // Update folios job center main.
            $jobCenterMain = CommonInvoice::getJobCenterIdMain($company->id);
            $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterMain->id)->first();
            $jobCenterBilling->folios = $jobCenterBilling + $plan->quantity;
            $jobCenterBilling->save();

            return Response::json([
                'code' => 200,
                'message' => 'Folios comprados correctamente.'
            ]);
        } catch (ApiErrorException $e) {
           return Response::json([
               'code' => 500,
               'message' => $e->getMessage()
           ]);
        }
    }
}
