<?php

namespace App\Http\Controllers\Invoices;

use App\Common\CommonCarbon;
use App\Common\Constants;
use App\companie;
use App\CompanyBillingData;
use App\CompanyInvoices;
use App\customer;
use App\customer_data;
use App\CustomerBankAccount;
use App\Http\Controllers\Security\SecurityController;
use App\Invoice;
use App\InvoiceOrder;
use App\Jobs\SendDocsEmailCustomer;
use App\Library\FacturamaLibrary;
use App\Mail\InvoiceMail;
use App\Plan;
use App\PriceList;
use App\profile_job_center;
use App\ProfileJobCenterBilling;
use App\quotation;
use App\service_order;
use App\UnitSat;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Response;
use Storage;

class InvoiceController extends Controller
{

    /**
     * The Digital Seal Certificates (CSD) are elements that allow the creation of CFDIs.
     * Each RFC with which you want to create CFDIs must have its own CSD.
     * @param Request $request
     * @return JsonResponse|void
     * @throws FileNotFoundException
     */
    public function loadDocumentsJobCenter(Request $request)
    {
        $idProfileJobCenter = $request->get('jobCenterId');
        $passwordForKey = $request->get('passwordBilling');
        $rfc = Str::upper($request->get('rfcBilling'));
        $profileJobCenter = profile_job_center::where('profile_job_centers_id', $idProfileJobCenter)->first();
        $profileJobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $profileJobCenter->id)
            ->first();
        if ($profileJobCenterBilling) {
            // Validation Data.
            if (strlen($rfc) == 0) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Debes ingresar primero un RFC valido.'
                ]);
            }
            if (strlen($passwordForKey) == 0) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Debes ingresar primero una Contraseña de la llave privada valida.'
                ]);
            }
            if (strlen($profileJobCenterBilling->file_certificate) == 0) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Debes ingresar primero un Certificado valido.'
                ]);
            }
            if (strlen($profileJobCenterBilling->file_key) == 0) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Debes ingresar primero una LLave Privada valida.'
                ]);
            }
            if (!Storage::disk('s3')->exists($profileJobCenterBilling->file_certificate)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Debes ingresar primero un Certificado valido.'
                ]);
            }
            if (!Storage::disk('s3')->exists($profileJobCenterBilling->file_key)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Debes ingresar primero una LLave Privada valida.'
                ]);
            }

            $profileJobCenterBilling->rfc = $rfc;

            $certificate = Storage::disk('s3')->get($profileJobCenterBilling->file_certificate);
            $certificateEncode = base64_encode($certificate);
            $key = Storage::disk('s3')->get($profileJobCenterBilling->file_key);
            $keyEncode = base64_encode($key);

            $data = [
                'Rfc' => $rfc,
                'Certificate' => $certificateEncode,
                'PrivateKey' => $keyEncode,
                'PrivateKeyPassword' => $passwordForKey
            ];

            // Upload and validate data with SAT by Facturama API.
            $response = FacturamaLibrary::loadDocuments($data);
            if (str_contains($response['message'], 'Ya existe un CSD asociado a este RFC.') || $response['code'] != 500) {
                $profileJobCenterBilling->status_connection_sat = 1;
                $company = companie::find(Auth::user()->companie);
                $company->is_active_billing = 1;
                $company->save();
            }
            $profileJobCenterBilling->save();
            return response()->json($response);

        }
    }

    public function deleteCSDbyRFC(Request $request) {
        $idProfileJobCenter = $request->get('jobCenterId');
        $profileJobCenter = profile_job_center::where('profile_job_centers_id', $idProfileJobCenter)->first();
        $profileJobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $profileJobCenter->id)->first();

        if ($profileJobCenter == null) {
            return response()->json([
                'code' => 500,
                'message' => 'Error al encontrar el centro de trabajo.'
            ]);
        }

        if ($profileJobCenterBilling == null) {
            return response()->json([
                'code' => 500,
                'message' => 'Aún no se ha registrado los datos del emisor.'
            ]);
        }

        if (strlen($profileJobCenterBilling->rfc) == 0) {
            return response()->json([
                'code' => 500,
                'message' => 'Debes ingresar primero un RFC valido.'
            ]);
        }

        $response = FacturamaLibrary::deleteCSDbyRFC($profileJobCenterBilling->rfc);
        if ($response['code'] != 500) {
            $profileJobCenterBilling->status_connection_sat = 0;
            $profileJobCenterBilling->save();
            $company = companie::find(Auth::user()->companie);
            $company->is_active_billing = 0;
            $company->save();
        }

        return Response::json($response);

    }

    /**
     * Test catalogs of Facturama with use Postman.
     * @return void
     */
    public function testCatalogsFacturama() {
        //$response = FacturamaLibrary::getCatalogsPaymentForms();
        //return response()->json($response);
        // Send emails to customer
        $order = service_order::find(22107);
        $quote = quotation::find($order->id_quotation);
        SendDocsEmailCustomer::dispatch($quote->id_customer, 22107);
    }

    /**
     * Create a new invoice for each order.
     *
     * @param $customerId
     * @param $orderId
     * @return array
     */
    public static function createInvoiceForEachOrder($customerId, $orderId) {

        $customer = customer::find($customerId);
        $order = service_order::find($orderId);
        $company = companie::find($customer->companie);
        $invoiceOrder = InvoiceOrder::where('id_service_order', $orderId)->first();
        $customerData = customer_data::where('customer_id', $customerId)->first();
        $jobCenter = profile_job_center::find($customer->id_profile_job_center);
        $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $customer->id_profile_job_center)->first();
        $bankAccount = CustomerBankAccount::where('id_customer', $customerId)->where('is_default', 1)->first();

        // Validations
        if ($jobCenterBilling != null) {
            if ($jobCenterBilling->status_connection_sat == 0) {
                return [
                    'code' => 500,
                    'message' => 'No se ha establecido conexión con el SAT.'
                ];
            }
        } else {
            return [
                'code' => 500,
                'message' => 'La facturación no esta configurada.'
            ];
        }

        if (!CommonInvoice::isFoliosAvailable($company->id, $jobCenter->id, 1)) {
            return [
                'code' => 500,
                'message' => 'No cuentas con folios disponibles para facturar.'
            ];
        }

        $urlLogo = 'https://storage.pestwareapp.com/' . $company->pdf_logo;
        $quote = quotation::find($order->id_quotation);
        $priceList = PriceList::find($quote->id_price_list);
        $totalIva = ($quote->price * $jobCenterBilling->iva) / 100;
        $rate = $jobCenterBilling->iva / 100;


        $params = [
            "Issuer" => [
                "FiscalRegime" => $jobCenterBilling->fiscal_regime, // 601
                "Rfc" => $jobCenterBilling->rfc, // SWO170518NG8
                "Name" => $jobCenter->business_name,
                "Address" => [
                    "Street" => $jobCenterBilling->address,
                    "ExteriorNumber" => $jobCenterBilling->address_number,
                    "InteriorNumber" => null,
                    "Neighborhood" => $jobCenterBilling->colony,
                    "ZipCode" => $jobCenterBilling->code_postal,
                    "Municipality" => $jobCenterBilling->municipality,
                    "State" => $jobCenterBilling->state,
                    "Country" => "México"
                ]
            ],
            "Receiver" => [
                "Name" => $customerData->billing, // Alberto Martínez Rangel
                "FiscalRegime" => $customerData->fiscal_regime,
                "TaxZipCode" => $customerData->billing_postal_code,
                "CfdiUse" => $customerData->cfdi_type, //G03
                "Rfc" => $customerData->rfc, // MARA9512306H2
                "Address" => [
                    "Street" => $customerData->billing_street,
                    "ExteriorNumber" => $customerData->billing_exterior_number,
                    "InteriorNumber" => $customerData->billing_interior_number,
                    "Neighborhood" => $customerData->billing_colony,
                    "ZipCode" => $customerData->billing_postal_code,
                    "Municipality" => $customerData->billing_municipality,
                    "State" => $customerData->billing_state,
                    "Country" => "México"
                ]
            ],
            "CfdiType" => "I",
            "NameId" => "1",
            "Folio" => CommonInvoice::getFolioNumberInvoice($customer->companie), // 100
            "ExpeditionPlace" => $jobCenterBilling->code_postal, // 20120
            "LogoUrl" => $urlLogo,
            "PaymentForm" => $customerData->payment_way, // 01
            "PaymentMethod" => $customerData->payment_method, // PUE
            "Date" => Carbon::now()->toDateTimeString(),
            "Items" => [
                [
                    "Quantity" => "1",
                    "ProductCode" => $priceList->service_code_billing, // 84111506
                    "UnitCode" => $priceList->unit_code_billing, // E48
                    "Unit" => "Unidad de servicio",
                    "Description" => $priceList->description_billing, // API folios adicionales
                    "IdentificationNumber" => "21",
                    "UnitPrice" => $quote->price, //TODO: REST IVA
                    "Subtotal" => $quote->price,
                    "Discount" => "0",
                    "DiscountVal" => "0",
                    "TaxObject" => "02",
                    "Taxes" => [
                        [
                            "Name" => "IVA",
                            "Rate" => $rate, // 0.16
                            "Total" => $totalIva, // base * rate = total
                            "Base" => $quote->price,
                            "IsRetention" => "false"
                        ],
                    ],
                    "Total" => $order->total
                ]
            ],
            "Observations" => $priceList->description_billing . ' - ' . $order->id_service_order,
            "PaymentBankName" => $bankAccount != null ? $bankAccount->bank : "",
            "PaymentAccountNumber" => $bankAccount != null ? $bankAccount->account : "",
            //"OrderNumber" => $order->id_service_order
        ];

        $response = FacturamaLibrary::createInvoice($params);

        if ($response['code'] == 201) {
            $complement = $response['data']->Complement;
            $taxStamp = $complement->TaxStamp;
            // Create Invoice in database.
            $invoice = new Invoice();
            $invoice->folio_invoice = CommonInvoice::getFolioInvoice($customer->companie);
            $invoice->id_invoice = $response['data']->Id;
            $invoice->uuid = $taxStamp->Uuid;
            $invoice->id_customer = $customerId;
            $invoice->description = $priceList->description_billing;
            $invoice->amount = $response['data']->Total;
            $invoice->id_status = Constants::$STATUS_INVOICE_VALID;
            $invoice->date = Carbon::now()->toDateString();
            $invoice->id_company = $customer->companie;
            $invoice->save();

            $invoiceOrder = new InvoiceOrder();
            $invoiceOrder->id_invoice = $invoice->id;
            $invoiceOrder->id_service_order = $orderId;
            $invoiceOrder->save();

            $order->invoiced = 1;
            $order->save();

            CommonInvoice::addFolioConsumed($company->id, $jobCenter->id);

            return [
                'code' => 201,
                'id' => $invoice->id_invoice,
                'message' => 'Factura creada correctamente.'
            ];

        } else {
            return [
                'code' => 500,
                'message' => $response['message']
            ];
        }
    }

    /**
     * Create at new invoice for config amount.
     * @param $customerId
     * @param $folios
     * @param $orderIds
     * @return array
     */
    public static function createInvoiceByAmount($customerId, $folios, $orderIds) {

        try {
            $customer = customer::find($customerId);
            $company = companie::find($customer->companie);
            $customerData = customer_data::where('customer_id', $customerId)->first();
            $jobCenter = profile_job_center::find($customer->id_profile_job_center);
            $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $customer->id_profile_job_center)->first();
            $bankAccount = CustomerBankAccount::where('id_customer', $customerId)->where('is_default', 1)->first();

            // Validations
            if ($jobCenterBilling != null) {
                if ($jobCenterBilling->status_connection_sat == 0) {
                    return [
                        'code' => 500,
                        'message' => 'No se ha establecido conexión con el SAT.'
                    ];
                }
            } else {
                return [
                    'code' => 500,
                    'message' => 'La facturación no esta configurada.'
                ];
            }

            $urlLogo = 'https://storage.pestwareapp.com/' . $company->pdf_logo;
            $totalIva = ($customerData->amount_billing * $jobCenterBilling->iva) / 100;
            $rate = $jobCenterBilling->iva / 100;
            $total = $customerData->amount_billing + $totalIva;
            $unitCode = UnitSat::find($customerData->unit_code_billing);

            $params = [
                "Issuer" => [
                    "FiscalRegime" => $jobCenterBilling->fiscal_regime, // 601
                    "Rfc" => $jobCenterBilling->rfc, // SWO170518NG8
                    "Name" => $jobCenter->business_name,
                    "Address" => [
                        "Street" => $jobCenterBilling->address,
                        "ExteriorNumber" => $jobCenterBilling->address_number,
                        "InteriorNumber" => null,
                        "Neighborhood" => $jobCenterBilling->colony,
                        "ZipCode" => $jobCenterBilling->code_postal,
                        "Municipality" => $jobCenterBilling->municipality,
                        "State" => $jobCenterBilling->state,
                        "Country" => "México"
                    ]
                ],
                "Receiver" => [
                    "Name" => $customerData->billing, // Alberto Martínez Rangel
                    "FiscalRegime" => $customerData->fiscal_regime,
                    "TaxZipCode" => $customerData->billing_postal_code,
                    "CfdiUse" => $customerData->cfdi_type, //G03
                    "Rfc" => $customerData->rfc, // MARA9512306H2
                    "Address" => [
                        "Street" => $customerData->billing_street,
                        "ExteriorNumber" => $customerData->billing_exterior_number,
                        "InteriorNumber" => $customerData->billing_interior_number,
                        "Neighborhood" => $customerData->billing_colony,
                        "ZipCode" => $customerData->billing_postal_code,
                        "Municipality" => $customerData->billing_municipality,
                        "State" => $customerData->billing_state,
                        "Country" => "México"
                    ]
                ],
                "CfdiType" => "I",
                "NameId" => "1",
                "Folio" => CommonInvoice::getFolioNumberInvoice($customer->companie), // 100
                "ExpeditionPlace" => $jobCenterBilling->code_postal, // 20120
                "LogoUrl" => $urlLogo,
                "PaymentForm" => $customerData->payment_way, // 01
                "PaymentMethod" => $customerData->payment_method, // PUE
                "Date" => Carbon::now()->toDateTimeString(),
                "Items" => [
                    [
                        "Quantity" => "1",
                        "ProductCode" => $customerData->service_code_billing, // 84111506
                        "UnitCode" => $unitCode->key, // E48
                        "Unit" => "Unidad de servicio",
                        "Description" => $customerData->description_billing, // API folios adicionales
                        "IdentificationNumber" => "21",
                        "UnitPrice" => $customerData->amount_billing,
                        "Subtotal" => $customerData->amount_billing,
                        "Discount" => "0",
                        "DiscountVal" => "0",
                        "TaxObject" => "02",
                        "Taxes" => [
                            [
                                "Name" => "IVA",
                                "Rate" => $rate, // 0.16
                                "Total" => $totalIva, // base * rate = total
                                "Base" => $customerData->amount_billing,
                                "IsRetention" => "false"
                            ],
                        ],
                        "Total" => $total
                    ]
                ],
                "Observations" => $customerData->description_billing . ' - ' . $folios,
                "PaymentBankName" => $bankAccount != null ? $bankAccount->bank : "",
                "PaymentAccountNumber" => $bankAccount != null ? $bankAccount->account : "",
                //"OrderNumber" => $order->id_service_order
            ];

            $response = FacturamaLibrary::createInvoice($params);

            if ($response['code'] == 201) {
                $complement = $response['data']->Complement;
                $taxStamp = $complement->TaxStamp;
                // Create Invoice in database.
                $invoice = new Invoice();
                $invoice->folio_invoice = CommonInvoice::getFolioInvoice($customer->companie);
                $invoice->id_invoice = $response['data']->Id;
                $invoice->uuid = $taxStamp->Uuid;
                $invoice->id_customer = $customerId;
                $invoice->description = $customerData->description_billing;
                $invoice->amount = $response['data']->Total;
                $invoice->id_status = Constants::$STATUS_INVOICE_VALID;
                $invoice->date = Carbon::now()->toDateString();
                $invoice->id_company = $customer->companie;
                $invoice->save();

                foreach ($orderIds as $orderId) {
                    $invoiceOrder = new InvoiceOrder();
                    $invoiceOrder->id_invoice = $invoice->id;
                    $invoiceOrder->id_service_order = $orderId->id;
                    $invoiceOrder->save();

                    $order = service_order::find($orderId->id);
                    $order->invoiced = 1;
                    $order->save();
                }

                // Update counter no invoices.
                $customerData->no_invoices = --$customerData->no_invoices;
                $customerData->last_invoiced_day = Carbon::now()->toDateString();
                $customerData->save();

                CommonInvoice::addFolioConsumed($company->id, $jobCenter->id);
                CommonInvoice::removeFolioReserved($company->id, $jobCenter->id);

                return [
                    'code' => 201,
                    'message' => 'Factura creada correctamente.'
                ];

            } else {
                return [
                    'code' => 500,
                    'message' => $response['message']
                ];
            }
        } catch (Exception $exception) {
            return [
                'code' => 500,
                'message' => $exception->getMessage()
            ];
        }

    }

    /**
     * Create at new invoice for config month.
     * @param $customerId
     * @param $subtotal
     * @param $foliosText
     * @param $orderIds
     * @param $items
     * @return array
     */
    public static function createInvoiceByMonth($customerId, $subtotal, $foliosText, $orderIds, $items) {
        try {
            $customer = customer::find($customerId);
            $company = companie::find($customer->companie);
            $customerData = customer_data::where('customer_id', $customerId)->first();
            $jobCenter = profile_job_center::find($customer->id_profile_job_center);
            $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $customer->id_profile_job_center)->first();
            $bankAccount = CustomerBankAccount::where('id_customer', $customerId)->where('is_default', 1)->first();

            // Validations
            if ($jobCenterBilling != null) {
                if ($jobCenterBilling->status_connection_sat == 0) {
                    return [
                        'code' => 500,
                        'message' => 'No se ha establecido conexión con el SAT.'
                    ];
                }
            } else {
                return [
                    'code' => 500,
                    'message' => 'La facturación no esta configurada.'
                ];
            }

            $urlLogo = 'https://storage.pestwareapp.com/' . $company->pdf_logo;

            $params = [
                "Issuer" => [
                    "FiscalRegime" => $jobCenterBilling->fiscal_regime, // 601
                    "Rfc" => $jobCenterBilling->rfc, // SWO170518NG8
                    "Name" => $jobCenter->business_name,
                    "Address" => [
                        "Street" => $jobCenterBilling->address,
                        "ExteriorNumber" => $jobCenterBilling->address_number,
                        "InteriorNumber" => null,
                        "Neighborhood" => $jobCenterBilling->colony,
                        "ZipCode" => $jobCenterBilling->code_postal,
                        "Municipality" => $jobCenterBilling->municipality,
                        "State" => $jobCenterBilling->state,
                        "Country" => "México"
                    ]
                ],
                "Receiver" => [
                    "Name" => $customerData->billing, // Alberto Martínez Rangel
                    "FiscalRegime" => $customerData->fiscal_regime,
                    "TaxZipCode" => $customerData->billing_postal_code,
                    "CfdiUse" => $customerData->cfdi_type, //G03
                    "Rfc" => $customerData->rfc, // MARA9512306H2
                    "Address" => [
                        "Street" => $customerData->billing_street,
                        "ExteriorNumber" => $customerData->billing_exterior_number,
                        "InteriorNumber" => $customerData->billing_interior_number,
                        "Neighborhood" => $customerData->billing_colony,
                        "ZipCode" => $customerData->billing_postal_code,
                        "Municipality" => $customerData->billing_municipality,
                        "State" => $customerData->billing_state,
                        "Country" => "México"
                    ]
                ],
                "CfdiType" => "I",
                "NameId" => "1",
                "Folio" => CommonInvoice::getFolioNumberInvoice($customer->companie), // 100
                "ExpeditionPlace" => $jobCenterBilling->code_postal, // 20120
                "LogoUrl" => $urlLogo,
                "PaymentForm" => $customerData->payment_way, // 01
                "PaymentMethod" => $customerData->payment_method, // PUE
                "Date" => Carbon::now()->toDateTimeString(),
                "Items" => $items,
                "Observations" => $customerData->description_billing . ' - ' . $foliosText,
                "PaymentBankName" => $bankAccount != null ? $bankAccount->bank : "",
                "PaymentAccountNumber" => $bankAccount != null ? $bankAccount->account : "",
                //"OrderNumber" => $order->id_service_order
            ];

            $response = FacturamaLibrary::createInvoice($params);

            if ($response['code'] == 201) {
                $complement = $response['data']->Complement;
                $taxStamp = $complement->TaxStamp;
                // Create Invoice in database.
                $invoice = new Invoice();
                $invoice->folio_invoice = CommonInvoice::getFolioInvoice($customer->companie);
                $invoice->id_invoice = $response['data']->Id;
                $invoice->uuid = $taxStamp->Uuid;
                $invoice->id_customer = $customerId;
                $invoice->description = $customerData->description_billing;
                $invoice->amount = $response['data']->Total;
                $invoice->id_status = Constants::$STATUS_INVOICE_VALID;
                $invoice->date = Carbon::now()->toDateString();
                $invoice->id_company = $customer->companie;
                $invoice->save();

                foreach ($orderIds as $orderId) {
                    $invoiceOrder = new InvoiceOrder();
                    $invoiceOrder->id_invoice = $invoice->id;
                    $invoiceOrder->id_service_order = $orderId->id;
                    $invoiceOrder->save();

                    $order = service_order::find($orderId->id);
                    $order->invoiced = 1;
                    $order->save();
                }

                CommonInvoice::addFolioConsumed($company->id, $jobCenter->id);
                CommonInvoice::removeFolioReserved($company->id, $jobCenter->id);

                return [
                    'code' => 201,
                    'message' => 'Factura creada correctamente.'
                ];

            } else {
                return [
                    'code' => 500,
                    'message' => $response['message']
                ];
            }
        } catch (Exception $exception) {
            return [
                'code' => 500,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Create a new invoice for company.
     *
     * @param $data
     * @return array
     */
    public static function createInvoiceForCompany($data) {

        try {
            if (empty($data['customer'])) {
                return [
                    'code' => 500,
                    'message' => 'Not found company'
                ];
            }
            $company = companie::where('id_stripe_customer', $data['customer'])->first();
            if ($company == null) {
                return [
                    'code' => 500,
                    'message' => 'Not found company'
                ];
            }
            $companyId = $company->id;
            $companyBillingData = CompanyBillingData::where('id_company', $companyId)->first();

            $urlLogo = 'https://storage.pestwareapp.com/pdf_logos/xcHahMkX79rum8z3wdRpbaQQ5T6rQzXUwUyZU2dt.jpeg';
            $rate = 16 / 100;

            $amount = $data['amount'] / 100;
            $amount = $amount / 1.16;
            $totalIva = $amount * $rate;
            $total = $amount + $totalIva;
            $amount = bcdiv($amount, 1, 2);
            $totalIva = bcdiv($totalIva, 1, 2);
            $itemInvoice = [
                "Quantity" => "1",
                "ProductCode" => "81112502",
                "UnitCode" => "E48",
                "Unit" => "Unidad de servicio",
                "Description" => $data['description'],
                "IdentificationNumber" => "21",
                "UnitPrice" => $amount,
                "Subtotal" => $amount,
                "Discount" => "0",
                "DiscountVal" => "0",
                "TaxObject" => "02",
                "Taxes" => [
                    [
                        "Name" => "IVA",
                        "Rate" => $rate, // 0.16
                        "Total" => $totalIva, // base * rate = total
                        "Base" => $amount,
                        "IsRetention" => "false"
                    ],
                ],
                "Total" => $total
            ];

            // Si la compañia es de México y tiene registrado sus datos de facturación, de lo contrario
            // el resto siempre se facturara como publico en general.
            if ($company->id_code_country == 2 && $companyBillingData) {
                $receiver = [
                    "Name" => $companyBillingData->reason_social, // Alberto Martínez Rangel
                    "CfdiUse" => $companyBillingData->cfdi_type,
                    "Rfc" => $companyBillingData->rfc, // MARA9512306H2
                    "Address" => [
                        "Street" => $companyBillingData->billing_street,
                        "ExteriorNumber" => $companyBillingData->billing_exterior_number,
                        "InteriorNumber" => $companyBillingData->billing_interior_number,
                        "Neighborhood" => $companyBillingData->billing_colony,
                        "ZipCode" => $companyBillingData->billing_postal_code,
                        "Municipality" => $companyBillingData->billing_municipality,
                        "State" => $companyBillingData->billing_state,
                        "Country" => "México"
                    ]
                ];
            } else {
                $receiver = [
                    "Name" => "Público en general", // Alberto Martínez Rangel
                    "CfdiUse" => "P01",
                    "Rfc" => $company->id_code_country == 2 ? "XAXX010101000": "XEXX010101000" // MARA9512306H2
                ];
            }

            $params = [
                "Issuer" => [
                    "FiscalRegime" => 612,
                    "Rfc" => "EARS8503314T1",
                    "Name" => "Saúl Esparza Romo",
                    "Address" => [
                        "Street" => "Av. Eugenio Garza Sada",
                        "ExteriorNumber" => "260",
                        "InteriorNumber" => null,
                        "Neighborhood" => "Pocitos",
                        "ZipCode" => "20997",
                        "Municipality" => "Aguascalientes",
                        "State" => "Aguascalientes",
                        "Country" => "México"
                    ]
                ],
                "Receiver" => $receiver,
                "CfdiType" => "I",
                "NameId" => "1",
                "Folio" => CommonInvoice::getFolioNumberInvoiceCompany($companyId), // 100
                "ExpeditionPlace" => "20997",
                "LogoUrl" => $urlLogo,
                "PaymentForm" => "04",
                "PaymentMethod" => "PUE",
                "Date" => Carbon::now()->toDateTimeString(),
                "Items" => [
                    $itemInvoice
                ],
                //"PaymentBankName" => $bankAccount != null ? $bankAccount->bank : "",
                //"PaymentAccountNumber" => $bankAccount != null ? $bankAccount->account : "",
                //"OrderNumber" => $order->id_service_order
            ];

            $response = FacturamaLibrary::createInvoice($params);

            if ($response['code'] == 201) {
                $complement = $response['data']->Complement;
                $taxStamp = $complement->TaxStamp;
                // Create Invoice in database.
                $invoice = new CompanyInvoices();
                $invoice->folio_invoice = CommonInvoice::getFolioInvoiceCompany($companyId);
                $invoice->id_invoice = $response['data']->Id;
                $invoice->uuid = $taxStamp->Uuid;
                $invoice->id_company = $companyId;
                $invoice->description = $data['description'];
                $invoice->amount = $response['data']->Total;
                $invoice->id_status = Constants::$STATUS_INVOICE_VALID;
                $invoice->date = Carbon::now()->toDateString();
                $invoice->save();

                // Send invoice email to customer instance.
                if ($companyBillingData) {
                    self::sendInvoiceForMail($invoice->id, $companyBillingData->email);
                }

                return [
                    'code' => 201,
                    'id' => $invoice->id_invoice,
                    'message' => 'Factura creada correctamente. CLIENTE: ' . $company->name . ' INVOICE ID: ' . $invoice->id_invoice
                ];

            } else {
                return [
                    'code' => 500,
                    'message' => $response['message'] . ' CLIENTE: ' . $company->name
                ];
            }
        }catch (Exception $exception) {
            return [
                'code' => 500,
                'message' => $exception->getMessage()
            ];
        }

    }

    /**
     * Create a new invoice for each order.
     *
     * @param Request $request
     * @return array
     */
    public function createInvoiceByOrderId(Request $request) {

        // Get request data.
        $orderId = $request->get('orderId');
        $isIva = $request->get('isIva');
        $billing = $request->get('billing');
        $rfc = $request->get('rfc');
        $cfdiType = $request->get('cfdiType');
        $email = $request->get('email');
        $address = $request->get('address');
        $exteriorNumber = $request->get('exteriorNumber');
        $interiorNumber = $request->get('interiorNumber');
        $postalCode = $request->get('postalCode');
        $colony = $request->get('colony');
        $municipality = $request->get('municipality');
        $state = $request->get('state');
        $paymentMethod = $request->get('paymentMethod');
        $paymentWay = $request->get('paymentWay');
        $bankAccountSelect = $request->get('bankAccountSelect');

        $order = service_order::find($orderId);
        $quote = quotation::find($order->id_quotation);
        $customer = customer::find($quote->id_customer);
        $customerId = $customer->id;

        $customerDataUpd = customer_data::where('customer_id', $customerId)->first();
        $customerDataUpd->email = $email;
        $customerDataUpd->billing = $billing;
        $customerDataUpd->rfc = $rfc;
        $customerDataUpd->email_billing = $email;
        $customerDataUpd->cfdi_type = $cfdiType;
        $customerDataUpd->billing_street = $address;
        $customerDataUpd->billing_interior_number = $interiorNumber;
        $customerDataUpd->billing_exterior_number = $exteriorNumber;
        $customerDataUpd->billing_postal_code = $postalCode;
        $customerDataUpd->billing_colony = $colony;
        $customerDataUpd->billing_municipality = $municipality;
        $customerDataUpd->billing_state = $state;
        $customerDataUpd->payment_method = $paymentMethod;
        $customerDataUpd->payment_way = $paymentWay;
        $customerDataUpd->save();

        if ($bankAccountSelect != null) {
            $bankAccount = CustomerBankAccount::find($bankAccountSelect);
            $bankAccount->is_default = 1;
            $bankAccount->save();
            $bankAccounts = CustomerBankAccount::where('id_customer', $customerId)->get();
            foreach ($bankAccounts as $account) {
                if ($account->id != $bankAccount->id) {
                    $account->is_default = 0;
                    $account->save();
                }
            }
        }

        $company = companie::find($customer->companie);
        $customerData = customer_data::where('customer_id', $customerId)->first();
        $jobCenter = profile_job_center::find($customer->id_profile_job_center);
        $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $customer->id_profile_job_center)->first();
        $bankAccount = CustomerBankAccount::where('id_customer', $customerId)->where('is_default', 1)->first();

        $urlLogo = 'https://storage.pestwareapp.com/' . $company->pdf_logo;
        $quote = quotation::find($order->id_quotation);
        $priceList = PriceList::find($quote->id_price_list);

        if ($isIva == "false") {
            $totalIva = ($order->subtotal * $jobCenterBilling->iva) / 100;
            $subtotal = $order->subtotal;
            $total = $order->subtotal + $totalIva;
        }
        else {
            $iva = ($jobCenterBilling->iva / 100) + 1;
            $lastTotal = bcdiv($order->total / $iva, 1, 2);
            $totalIva = bcdiv(($lastTotal * $jobCenterBilling->iva) / 100, 1, 2);
            $subtotal = $lastTotal;
            $total = $lastTotal + $totalIva;
        }

        $rate = $jobCenterBilling->iva / 100;

        // Validations
        if ($jobCenterBilling != null) {
            if ($jobCenterBilling->status_connection_sat == 0) {
                return [
                    'code' => 500,
                    'message' => 'No se ha establecido conexión con el SAT.'
                ];
            }
        } else {
            return [
                'code' => 500,
                'message' => 'La facturación no esta configurada.'
            ];
        }

        if (!CommonInvoice::isFoliosAvailable($company->id, $jobCenter->id, 1)) {
            return [
                'code' => 404,
                'message' => 'No cuentas con folios disponibles para facturar.'
            ];
        }

        if ($priceList->unit_code_billing == null) {
            $idEncode = SecurityController::encodeId($priceList->id);
            $urlConfigList = env('URL_BASE') . 'catalogos/edit/listprices/' . $idEncode;
            return [
                'code' => 500,
                'message' => 'La facturación no esta configurada en la lista de precio: ' . $priceList->name . '<br><a href="' . $urlConfigList . '" target="_blank">Configurar Lista</a>'
            ];
        }

        if ($customerData->billing == null || $customerData->cfdi_type == null || $customerData->rfc == null ||
            $customerData->payment_way == null || $customerData->payment_method == null) {
            return [
                'code' => 500,
                'message' => 'Los datos de facturación están incompletos para el cliente: ' . $customer->name
            ];
        }

        $timezone = CommonCarbon::getTimeZoneByJobCenter($customer->id_profile_job_center);

        $params = [
            "Issuer" => [
                "FiscalRegime" => $jobCenterBilling->fiscal_regime, // 601
                "Rfc" => $jobCenterBilling->rfc, // SWO170518NG8
                "Name" => $jobCenter->business_name,
                "Address" => [
                    "Street" => $jobCenterBilling->address,
                    "ExteriorNumber" => $jobCenterBilling->address_number,
                    "InteriorNumber" => null,
                    "Neighborhood" => $jobCenterBilling->colony,
                    "ZipCode" => $jobCenterBilling->code_postal,
                    "Municipality" => $jobCenterBilling->municipality,
                    "State" => $jobCenterBilling->state,
                    "Country" => "México"
                ]
            ],
            "Receiver" => [
                "Name" => $customerData->billing, // Alberto Martínez Rangel
                "FiscalRegime" => $customerData->fiscal_regime,
                "TaxZipCode" => $customerData->billing_postal_code,
                "CfdiUse" => $customerData->cfdi_type, //G03
                "Rfc" => $customerData->rfc, // MARA9512306H2
                "Address" => [
                    "Street" => $customerData->billing_street,
                    "ExteriorNumber" => $customerData->billing_exterior_number,
                    "InteriorNumber" => $customerData->billing_interior_number,
                    "Neighborhood" => $customerData->billing_colony,
                    "ZipCode" => $customerData->billing_postal_code,
                    "Municipality" => $customerData->billing_municipality,
                    "State" => $customerData->billing_state,
                    "Country" => "México"
                ]
            ],
            "CfdiType" => "I",
            "NameId" => "1",
            "Folio" => CommonInvoice::getFolioNumberInvoice($customer->companie), // 100
            "ExpeditionPlace" => $jobCenterBilling->code_postal, // 20120
            "LogoUrl" => $urlLogo,
            "PaymentForm" => $customerData->payment_way, // 01
            "PaymentMethod" => $customerData->payment_method, // PUE
            "Date" => Carbon::now()->timezone($timezone)->toDateTimeString(),
            "Items" => [
                [
                    "Quantity" => "1",
                    "ProductCode" => $priceList->service_code_billing, // 84111506
                    "UnitCode" => $priceList->unit_code_billing, // E48
                    "Unit" => "Unidad de servicio",
                    "Description" => $priceList->description_billing, // API folios adicionales
                    "IdentificationNumber" => $order->id_service_order,
                    "UnitPrice" => $subtotal, //TODO: REST IVA
                    "Subtotal" => $subtotal,
                    "Discount" => "0",
                    "DiscountVal" => "0",
                    "TaxObject" => "02",
                    "Taxes" => [
                        [
                            "Name" => "IVA",
                            "Rate" => $rate, // 0.16
                            "Total" => $totalIva, // base * rate = total
                            "Base" => $subtotal,
                            "IsRetention" => "false"
                        ],
                    ],
                    "Total" => $total
                ]
            ],
            "Observations" => $priceList->description_billing . ' - ' . $order->id_service_order,
            "PaymentBankName" => $bankAccount != null ? $bankAccount->bank : null,
            "PaymentAccountNumber" => $bankAccount != null ? $bankAccount->account : null,
            "OrderNumber" => $order->id_service_order
        ];

        $response = FacturamaLibrary::createInvoice($params);

        if ($response['code'] == 201) {
            $complement = $response['data']->Complement;
            $taxStamp = $complement->TaxStamp;
            // Create Invoice in database.
            $invoice = new Invoice();
            $invoice->folio_invoice = CommonInvoice::getFolioInvoice($customer->companie);
            $invoice->id_invoice = $response['data']->Id;
            $invoice->uuid = $taxStamp->Uuid;
            $invoice->id_customer = $customerId;
            $invoice->description = $priceList->description_billing;
            $invoice->amount = $response['data']->Total;
            $invoice->id_status = Constants::$STATUS_INVOICE_VALID;
            $invoice->date = Carbon::now()->toDateString();
            $invoice->id_company = $customer->companie;
            $invoice->save();

            $invoiceOrder = new InvoiceOrder();
            $invoiceOrder->id_invoice = $invoice->id;
            $invoiceOrder->id_service_order = $orderId;
            $invoiceOrder->save();

            $order->invoiced = 1;
            $order->save();

            CommonInvoice::addFolioConsumed($company->id, $jobCenter->id);

            return [
                'code' => 201,
                'id' => $invoice->id_invoice,
                'message' => 'Factura creada correctamente.'
            ];

        } else {
            return [
                'code' => 500,
                'message' => $response['message']
            ];
        }
    }

    /**
     * CFDI cancellation.
     * https://apisandbox.facturama.mx/guias/api-multi/cfdi/cancelacion
     * @return JsonResponse
     */
    public function cancelInvoice($invoiceId) {
        $response = FacturamaLibrary::cancelInvoice($invoiceId);
        return Response::json($response);
    }

    /**
     * Download invoice to pdf or xml.
     * @param $invoiceId
     */
    public function downloadInvoice($invoiceId) {
        FacturamaLibrary::downloadInvoice($invoiceId, 'pdf');
    }

    public function calculateAmountsInvoiceByOrder(Request $request) {
        try {
            $orderId = $request->get('orderId');
            $isIva = $request->get('isIva');
            $order = service_order::find($orderId);
            $ivaPercentage = CommonInvoice::getIVA();

            if ($isIva == "false") {
                $ivaDiv = $ivaPercentage / 100;
                $totalIva = bcdiv($order->subtotal * $ivaDiv, 1, 2);
                $subtotal = $order->subtotal;
                $total = $order->subtotal + $totalIva;
            }
            else {
                $iva = ($ivaPercentage / 100) + 1;
                $lastTotal = bcdiv($order->total / $iva, 1, 2);
                $totalIva = bcdiv(($lastTotal * $ivaPercentage) / 100, 1, 2);
                $subtotal = $lastTotal;
                $total = $lastTotal + $totalIva;
            }

            return Response::json([
                'code' => 200,
                'subtotal' => $subtotal,
                'iva' => $totalIva,
                'total' => round($total)
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal'
            ]);
        }
    }

    public function getDataCustomerInvoiceByOrder($customerId) {
        try {
            $customerId = SecurityController::decodeId($customerId);
            $customerDatas = customer_data::where('customer_id', $customerId)->first();
            return Response::json([
                'code' => 200,
                'customer' => $customerDatas
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'customer' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    private static function sendInvoiceForMail($invoiceId, $email) {

        $invoice = CompanyInvoices::find($invoiceId);

        // Download invoice pdf
        $pdfInvoice = FacturamaLibrary::downloadInvoice($invoice->id_invoice, 'pdf');
        $xmlInvoice = FacturamaLibrary::downloadInvoice($invoice->id_invoice, 'Xml');

        $banner = DB::table('personal_mails')->where('id_company', 40)->first();

        $whatsapp = 'https://api.whatsapp.com/send?phone=524494139091';
        $greeting = 'Esperando te encuentres muy bien te adjuntamos la factura correspondiente al pago realizado en PestWare App.';
        $billingText = 'Gracias por usar PestWare App. Autorizamos con éxito su tarjeta de crédito para sus cargos de uso, ¡así que está todo listo!';

        config(['mail.from.name' => 'PestWare App']);

        $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
        if($result != false) {
            Mail::to($email)->send(new InvoiceMail($pdfInvoice, $xmlInvoice, $banner, $whatsapp, $invoice, $billingText, $greeting, 'soporte@pestwareapp.com', true));
        }
    }

}
