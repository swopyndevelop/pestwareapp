<?php

namespace App\Http\Controllers\Invoices;

use App\cash;
use App\Common\Constants;
use App\companie;
use App\customer;
use App\customer_data;
use App\CustomerBankAccount;
use App\employee;
use App\event;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Security\SecurityController;
use App\Invoice;
use App\InvoiceOrder;
use App\Library\FacturamaLibrary;
use App\Mail\InvoiceMail;
use App\Notifications;
use App\payment_method;
use App\payment_way;
use App\Plan;
use App\ProductBilling;
use App\profile_job_center;
use App\ProfileJobCenterBilling;
use App\service_order;
use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;

class BillingController extends Controller
{
    /**
     * Show all invoices.
     * @return Factory|Application|View
     */
    public function index($orderId = 0) {
        $invoices = DB::table('invoices as iv')
            ->join('customers as c', 'iv.id_customer', 'c.id')
            ->join('customer_datas as cd', 'c.id', 'cd.customer_id')
            ->join('statuses as st', 'iv.id_status', 'st.id')
            ->where('iv.id_company', Auth::user()->companie)
            ->select('iv.*', 'st.name as status', 'c.name as customer_name', 'c.establishment_name',
                'cd.billing_colony as colony', 'cd.billing_municipality as municipality', 'cd.billing_street as address',
                'cd.billing_exterior_number as address_number', 'cd.billing_interior_number as address_number_int',
                'cd.billing_state as state', 'cd.email_billing', 'phone_number');

        if (strlen($orderId) > 1) {
            $orderId = SecurityController::decodeId($orderId);
            $invoices->join('invoice_orders as io', 'iv.id', 'io.id_invoice')
                ->where('io.id_service_order', $orderId);
        }

        $invoices =  $invoices->orderBy('iv.created_at', 'desc')->paginate(10);

        $codeCountry = CommonCompany::getCodeByCountry();
        $employee = employee::where('employee_id', Auth::user()->id)->first();
        $paymentWays = payment_way::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        $paymentMethods = payment_method::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        $products = ProductBilling::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        $customers = customer::where('id_profile_job_center', $employee->profile_job_center_id)->get();

        $plansBilling = Plan::where('type', Constants::$PLAN_TYPE_BILLING)->get();
        $plansBilling->map(function($plan) {
            $plansBilling = $plan;
            $plansBilling['price_folio'] = $plan->price / $plan->quantity;
            return $plansBilling;
        });

        return view('vendor.adminlte.invoices.index')->with([
            'invoices' => $invoices,
            'codeCountry' => $codeCountry,
            'paymentWays' => $paymentWays,
            'paymentMethods' => $paymentMethods,
            'products' => $products,
            'customers' => $customers,
            'plansBilling' => $plansBilling
        ]);
    }

    /**
     * Download invoice by id facturama and type document. (html, Xml, pdf)
     */
    public function downloadInvoiceById($id, $typeDocument) {
        $id = SecurityController::decodeId($id);
        $invoice = Invoice::find($id);
        $customer = customer::find($invoice->id_customer);
        $docResponse = FacturamaLibrary::downloadInvoice($invoice->id_invoice, $typeDocument);
        $extension = $typeDocument == 'pdf' ? '.pdf' : '.xml';

        $decoded = base64_decode(end($docResponse));
        $file = $invoice->folio_invoice . ' - ' . $customer->name . '-' . $invoice->date . $extension;
        //store file temporarily
        file_put_contents($file, $decoded);

        //download file and delete it
        return response()->download($file)->deleteFileAfterSend(true);
    }

    /**
     * CFDI cancellation.
     * https://apisandbox.facturama.mx/guias/api-multi/cfdi/cancelacion
     *
     * @param $invoiceId
     * @return JsonResponse
     */
    public function cancelInvoiceById($invoiceId) {
        try {
            $invoiceId = SecurityController::decodeId($invoiceId);
            $invoice = Invoice::find($invoiceId);
            if (!$invoice) {
                return Response::json([
                    'code' => 500,
                    'message' => 'No se encontró ninguna factura.'
                ]);
            }
            $response = FacturamaLibrary::cancelInvoice($invoice->id_invoice);
            if ($response['code'] == 500) {
                return Response::json([
                    'code' => 500,
                    'message' => $response['message']
                ]);
            }
            $invoice->id_status = Constants::$STATUS_INVOICE_CANCELED;
            $invoice->save();
            return Response::json([
                'code' => 500,
                'message' => $response['data']->Message
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function sendInvoiceForMail(Request $request) {
        $invoiceId = $request->get('invoiceId');
        $invoiceId = SecurityController::decodeId($invoiceId);
        $emails = $request->get('emails');

        $invoice = Invoice::join('customers as c', 'invoices.id_customer', 'c.id')
            ->where('invoices.id', $invoiceId)
            ->select('invoices.*', 'c.name as customer_name', 'c.id as customer_id')
            ->first();
        $company = companie::find(Auth::user()->companie);

        // Download invoice pdf
        $pdfInvoice = FacturamaLibrary::downloadInvoice($invoice->id_invoice, 'pdf');
        $xmlInvoice = FacturamaLibrary::downloadInvoice($invoice->id_invoice, 'Xml');

        $banner = DB::table('personal_mails')->where('id_company', $company->id)->first();
        $employee = employee::join('profile_job_centers as pjc','profile_job_center_id','pjc.id')
            ->where('companie', Auth::user()->companie)
            ->first();
        $profileJobCenter = profile_job_center::find($employee->profile_job_center_id);
        $country_code = DB::table('countries')->where('id', $company->id_code_country)->first();
        $whatsapp = 'https://api.whatsapp.com/send?phone='. $country_code->code_country .$profileJobCenter->whatsapp_personal;
        $billing = ProfileJobCenterBilling::where('id_profile_job_center', $profileJobCenter->id)->first();

        $invoiceOrders = InvoiceOrder::where('id_invoice', $invoiceId)->get();
        $greeting = 'Esperando te encuentres muy bien te adjuntamos la factura correspondiente del servicio realizado ';
        if ($invoiceOrders->count() == 1) {
            $order = service_order::find($invoiceOrders[0]->id_service_order);
            $event = event::where('id_service_order', $order->id)->first();
            $greeting .= 'el día ' . $event->initial_date . ' a las ' . $event->initial_hour . ' con folio ' . $order->id_service_order;
        } else {
            $customer = customer_data::where('customer_id', $invoice->customer_id)->first();
            if ($customer->billing_mode == Constants::$BILLING_MODE_INVOICE_BY_AMOUNT || $customer->billing_mode == Constants::$BILLING_MODE_INVOICE_MONTH) {
                $foliosText = "";
                foreach ($invoiceOrders as $invoiceOrder) {
                    $order = service_order::find($invoiceOrder->id_service_order);
                    $foliosText .= $order->id_service_order . ", ";
                }
                $folios = trim($foliosText, ', ');
                if ($customer->billing_mode == Constants::$BILLING_MODE_INVOICE_BY_AMOUNT) {
                    $greeting .= 'al corte del día ' . $customer->last_invoiced_day . ' con folios ' . $folios;
                }
                if ($customer->billing_mode == Constants::$BILLING_MODE_INVOICE_MONTH) {
                    $greeting .= 'al corte del fin de mes; con folios ' . $folios;
                }
            }
        }


        config(['mail.from.name' => $profileJobCenter->name]);

        foreach ($emails as $email) {
            $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
            if($result != false) {
                Mail::to($email)->send(new InvoiceMail($pdfInvoice, $xmlInvoice, $banner, $whatsapp, $invoice, $billing->text_mail, $greeting, $billing->email, false));
            }
        }

        return Response::json([
            'code' => 200,
            'message' => 'Factura enviada correctamente.'
        ]);
    }

    public function paymentInvoice(Request $request)
    {
        try {
            $invoiceId = $request->get('invoiceId');
            $invoiceId = SecurityController::decodeId($invoiceId);
            $amount = $request->get('amount');
            $paymentForm = $request->get('paymentForm');
            $paymentWay = $request->get('paymentWay');
            $dateHour = $request->get('dateHour');
            $paymentFormSat = $request->get('paymentFormSat');
            $isComplement = $request->get('isComplement');
            $partiality = $request->get('partiality');
            $payerAccount = $request->get('payerAccount');
            $beneficiaryAccount = $request->get('BeneficiaryAccount');
            $foreignAccountNamePayer = $request->get('ForeignAccountNamePayer');
            $operationNumber = $request->get('OperationNumber');
            $rfcIssuerPayerAccount = $request->get('RfcIssuerPayerAccount');
            $rfcReceiverBeneficiaryAccount = $request->get('RfcReceiverBeneficiaryAccount');

            if ($isComplement == "true") {
                $invoice = Invoice::find($invoiceId);
                $customer = customer::find($invoice->id_customer);
                $company = companie::find($customer->companie);
                $customerData = customer_data::where('customer_id', $customer->id)->first();
                $jobCenter = profile_job_center::find($customer->id_profile_job_center);
                $urlLogo = 'https://storage.pestwareapp.com/' . $company->pdf_logo;
                $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $customer->id_profile_job_center)->first();

                $params = [
                    "Issuer" => [
                        "FiscalRegime" => $jobCenterBilling->fiscal_regime, // 601
                        "Rfc" => $jobCenterBilling->rfc, // SWO170518NG8
                        "Name" => $jobCenter->business_name,
                    ],
                    "Receiver" => [
                        "Name" => $customerData->billing, // Alberto Martínez Rangel
                        "CfdiUse" => "P01",
                        "Rfc" => $customerData->rfc // MARA9512306H2
                    ],
                    "CfdiType" => "P",
                    "NameId" => "14",
                    "Folio" => CommonInvoice::getFolioNumberByInvoiceId($invoiceId),
                    "ExpeditionPlace" => $jobCenterBilling->code_postal, // 20120
                    "LogoUrl" => $urlLogo,
                    "Complemento" => [
                        "Payments" => [
                            [
                                "Date" => Carbon::parse($dateHour)->toDateTimeString(),
                                "PaymentForm" => $paymentFormSat,
                                "Currency" => "MXN",
                                "Amount" => $invoice->amount, //1200
                                "BeneficiaryAccount" => $beneficiaryAccount, //1010101011
                                "ForeignAccountNamePayer" => $foreignAccountNamePayer,
                                "PayerAccount" => $payerAccount, //4152313131
                                "OperationNumber" => $operationNumber,
                                "RfcIssuerPayerAccount" => $rfcIssuerPayerAccount, //SWO170518NG8
                                "RfcReceiverBeneficiaryAccount" => $rfcReceiverBeneficiaryAccount, //SWO170518NG8
                                "RelatedDocuments" => [
                                    [
                                        "Uuid" => $invoice->uuid,
                                        "Folio" => CommonInvoice::getFolioNumberByInvoiceId($invoiceId),
                                        "Currency" => "MXN",
                                        "PaymentMethod" => "PPD",
                                        "PartialityNumber" => $partiality,
                                        "PreviousBalanceAmount" => $invoice->amount,
                                        "AmountPaid" => $amount
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];

                $response = FacturamaLibrary::complementInvoice($params);

                if ($response['code'] == 201) {
                    $complement = $response['data']->Complement;
                    $taxStamp = $complement->TaxStamp;
                    // Create Invoice in database.
                    $invoice = new Invoice();
                    $invoice->folio_invoice = CommonInvoice::getFolioComplement($invoiceId);
                    $invoice->id_invoice = $response['data']->Id;
                    $invoice->uuid = $taxStamp->Uuid;
                    $invoice->id_customer = $customer->id;
                    $invoice->description = $customerData->description_billing;
                    $invoice->amount = $amount;
                    $invoice->id_status = Constants::$STATUS_INVOICE_VALID_PAID;
                    $invoice->date = Carbon::now()->toDateString();
                    $invoice->id_company = $customer->companie;
                    $invoice->invoice_complement = $invoiceId;
                    $invoice->save();
                } else {
                    return [
                        'code' => 500,
                        'message' => $response['message']
                    ];
                }
            }

            // Update status invoice original.
            $invoice = Invoice::find($invoiceId);
            $invoice->id_status = Constants::$STATUS_INVOICE_VALID_PAID;
            $invoice->save();

            // Update orders payment.
            $invoiceOrders = InvoiceOrder::where('id_invoice', $invoiceId)->get();
            foreach ($invoiceOrders as $order) {
                $cashe = cash::where('id_service_order', $order->id_service_order)->first();
                if (!$cashe) {
                    $event = event::where('id_service_order', $order->id_service_order)->first();
                    $cashe = new cash();
                    $cashe->id_service_order = $order->id_service_order;
                    $cashe->id_event = $event->id;
                    $cashe->companie = Auth::user()->companie;
                }
                $cashe->id_payment_method = $paymentForm;
                $cashe->id_payment_way = $paymentWay;
                $cashe->amount_received = $amount;
                $cashe->payment = 2;
                $cashe->updated_at = Carbon::parse($dateHour)->toDateTimeString();
                $cashe->save();
            }
            return [
                'code' => 201,
                'message' => 'Factura Pagada Correctamente.'
            ];
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => $exception->getMessage()//'Algo salio mal, intenta de nuevo'
            ]);
        }
    }
}
