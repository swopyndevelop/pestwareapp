<?php

namespace App\Http\Controllers\Invoices;

use App\companie;
use App\CompanyInvoices;
use App\employee;
use App\Invoice;
use App\profile_job_center;
use App\ProfileJobCenterBilling;
use Auth;
use DB;

class CommonInvoice
{
    public static function getFolioInvoice($companyId)
    {
        $invoice = Invoice::where('id_company', $companyId)
            ->latest()->first();

        if ($invoice) {
            $folio = explode('-', $invoice->folio_invoice);
            return 'FA-' . ++$folio[1];
        } else return 'FA-' . '1000';
    }

    public static function getFolioNumberInvoice($companyId)
    {
        $invoice = Invoice::where('id_company', $companyId)
            ->latest()->first();

        if ($invoice) {
            $folio = explode('-', $invoice->folio_invoice);
            return ++$folio[1];
        } else return '1000';
    }

    public static function getFolioNumberByInvoiceId($invoiceId)
    {
        $invoice = Invoice::find($invoiceId);

        $folio = explode('-', $invoice->folio_invoice);
        return $folio[1];
    }

    public static function getFolioComplement($invoiceId)
    {
        $invoice = Invoice::find($invoiceId);
        $complement = Invoice::where('invoice_complement', $invoiceId)
            ->latest()->first();
        $lastFolio = explode('-', $invoice->folio_invoice);

        if ($complement) {
            $folio = explode('-', $complement->folio_invoice);
            return 'CP-' . $lastFolio[1] . '-' . ++$folio[2];
        } else return 'CP-' . $lastFolio[1] . '-1';
    }

    public static function addFolioConsumed($companyId, $jobCenterId)
    {
        $company = companie::find($companyId);
        $jobCenterMain = DB::table('profile_job_centers as pjc')
            ->join('tree_job_centers as tjc', 'pjc.profile_job_centers_id', 'tjc.id_inc')
            ->where('pjc.companie', $companyId)
            ->where('tjc.parent', '#')
            ->select('pjc.id')
            ->first();
        $billingJobCenterMain = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterMain->id)->first();
        $billingJobCenter = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterId)->first();
        $billingJobCenter->folios_consumed = $billingJobCenter->folios_consumed + 1;
        if ($company->is_folios_consumed == 0) $billingJobCenter->folios = $billingJobCenter->folios - 1;
        else $billingJobCenterMain->folios = $billingJobCenterMain->folios - 1;
        $billingJobCenter->save();
        $billingJobCenterMain->save();
    }

    public static function removeFolioReserved($companyId, $jobCenterId)
    {
        $company = companie::find($companyId);
        $jobCenterMain = DB::table('profile_job_centers as pjc')
            ->join('tree_job_centers as tjc', 'pjc.profile_job_centers_id', 'tjc.id_inc')
            ->where('pjc.companie', $companyId)
            ->where('tjc.parent', '#')
            ->select('pjc.id')
            ->first();

        $billingJobCenterMain = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterMain->id)->first();
        $billingJobCenter = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterId)->first();

        if ($company->is_folios_consumed == 0) $billingJobCenter->folios_reserved = $billingJobCenter->folios_reserved - 1;
        else $billingJobCenterMain->folios_reserved = $billingJobCenterMain->folios_reserved - 1;
        $billingJobCenter->save();
        $billingJobCenterMain->save();
    }

    public static function isFoliosAvailable($companyId, $jobCenterId, $quantity)
    {
        $company = companie::find($companyId);
        $jobCenterMain = DB::table('profile_job_centers as pjc')
            ->join('tree_job_centers as tjc', 'pjc.profile_job_centers_id', 'tjc.id_inc')
            ->where('pjc.companie', $companyId)
            ->where('tjc.parent', '#')
            ->select('pjc.id')
            ->first();
        $billingJobCenterMain = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterMain->id)->first();
        $billingJobCenter = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterId)->first();

        if ($company->is_folios_consumed == 1) {
            if ($billingJobCenterMain->folios >= $quantity) return true;
            else return false;
        } else {
            if ($billingJobCenter->folios >= $quantity) return true;
            else return false;
        }
    }

    public static function isFoliosAvailableMainInstance($companyId, $quantity)
    {
        $jobCenterMain = DB::table('profile_job_centers as pjc')
            ->join('tree_job_centers as tjc', 'pjc.profile_job_centers_id', 'tjc.id_inc')
            ->where('pjc.companie', $companyId)
            ->where('tjc.parent', '#')
            ->select('pjc.id')
            ->first();
        $billingJobCenterMain = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterMain->id)->first();

        if ($billingJobCenterMain->folios >= $quantity) return true;
        else return false;
    }

    public static function getJobCenterIdMain($companyId)
    {
        return DB::table('profile_job_centers as pjc')
            ->join('tree_job_centers as tjc', 'pjc.profile_job_centers_id', 'tjc.id_inc')
            ->where('pjc.companie', $companyId)
            ->where('tjc.parent', '#')
            ->select('pjc.id')
            ->first();
    }

    public static function getIVA()
    {
        $employee = employee::where('employee_id', Auth::user()->id)->first();
        return profile_job_center::find($employee->profile_job_center_id)->iva;
    }

    public static function getFolioInvoiceCompany($companyId)
    {
        $invoice = CompanyInvoices::where('id_company', $companyId)
            ->latest()->first();

        if ($invoice) {
            $folio = explode('-', $invoice->folio_invoice);
            return 'FA-' . ++$folio[1];
        } else return 'FA-' . '1000';
    }

    public static function getFolioNumberInvoiceCompany($companyId)
    {
        $invoice = CompanyInvoices::where('id_company', $companyId)
            ->latest()->first();

        if ($invoice) {
            $folio = explode('-', $invoice->folio_invoice);
            return ++$folio[1];
        } else return '1000';
    }
}