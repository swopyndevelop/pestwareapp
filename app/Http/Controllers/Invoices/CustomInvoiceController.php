<?php

namespace App\Http\Controllers\Invoices;

use App\Common\Constants;
use App\companie;
use App\customer;
use App\customer_data;
use App\employee;
use App\establishment_type;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Library\FacturamaLibrary;
use App\ProductBilling;
use App\profile_job_center;
use App\ProfileJobCenterBilling;
use App\source_origin;
use App\UnitSat;
use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class CustomInvoiceController extends Controller
{
    /**
     * Create at new product service billing.
     * @param Request $request
     * @return JsonResponse
     */
    public function createProductService(Request $request)
    {
        try {
            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $isSave = $request->get('isSave');
            $id = $request->get('id');

            if ($isSave == "true") $product = new ProductBilling();
            else $product = ProductBilling::find($id);

            $product->name = $request->get('name');
            $product->identification_number = $request->get('identification_number');
            $product->description = $request->get('description');
            $product->unit_price = $request->get('unit_price');
            $product->property_account = $request->get('property_account');
            $product->unit_code_billing_id = $request->get('unit_code_billing_id');
            $product->product_code_billing_id = $request->get('product_code_billing_id');
            $product->unit_code_billing = $request->get('unit_code_billing');
            $product->product_code_billing = $request->get('product_code_billing');
            $product->iva = $request->get('iva');
            $product->ieps = $request->get('ieps');
            $product->iva_ret = $request->get('iva_ret');
            $product->isr = $request->get('isr');
            $product->profile_job_center_id = $employee->profile_job_center_id;
            $product->id_company = Auth::user()->companie;
            $product->save();

            $products = ProductBilling::where('profile_job_center_id', $employee->profile_job_center_id)->get();
            return Response::json([
                'code' => 201,
                'message' => 'Producto - Servicio agregado correctamente.',
                'products' => $products
            ]);

        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function createCustomerInvoice(Request $request)
    {
        try {
            DB::beginTransaction();
            // Get request data.
            $name = $request->get('name');
            $billing = $request->get('billing');
            $rfc = $request->get('rfc');
            $cfdiType = $request->get('cfdiType');
            $regimeFiscal = $request->get('regimeFiscal');
            $email = $request->get('email');
            $phone = $request->get('phone');
            $address = $request->get('address');
            $exteriorNumber = $request->get('exteriorNumber');
            $interiorNumber = $request->get('interiorNumber');
            $postalCode = $request->get('postalCode');
            $colony = $request->get('colony');
            $municipality = $request->get('municipality');
            $state = $request->get('state');

            // Validations.
            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $establishment = establishment_type::where('profile_job_center_id', $employee->profile_job_center_id)->first();
            $sourceOrigin = source_origin::where('profile_job_center_id', $employee->profile_job_center_id)->first();
            $customer = customer::where('cellphone', $phone)
                ->where('id_profile_job_center', $employee->profile_job_center_id)
                ->where('cellphone', '<>', null)
                ->first();
            if ($customer) {
                DB::rollBack();
                return response()->json([
                    'code' => 500,
                    'message' => 'El número de teléfono esta asociado a otro cliente.'
                ]);
            }

            // Try to create customer main.
            $customerMain = new customer();
            $customerMain->name = $name;
            $customerMain->cellphone = str_replace(' ', '', $phone);
            $customerMain->establishment_id = $establishment->id;
            $customerMain->source_origin_id = $sourceOrigin->id;
            $customerMain->id_profile_job_center = $employee->profile_job_center_id;
            $customerMain->companie = Auth::user()->companie;
            $customerMain->save();

            $customerData = new customer_data();
            $customerData->customer_id = $customerMain->id;
            $customerData->email = $email;
            $customerData->phone_number = str_replace(' ', '', $phone);
            $customerData->billing = $billing;
            $customerData->rfc = $rfc;
            $customerData->email_billing = $email;
            $customerData->cfdi_type = $cfdiType;
            $customerData->fiscal_regime = $regimeFiscal;
            $customerData->billing_street = $address;
            $customerData->billing_interior_number = $interiorNumber;
            $customerData->billing_exterior_number = $exteriorNumber;
            $customerData->billing_postal_code = $postalCode;
            $customerData->billing_colony = $colony;
            $customerData->billing_municipality = $municipality;
            $customerData->billing_state = $state;
            $customerData->save();

            $customers = customer::where('id_profile_job_center', $employee->profile_job_center_id)->get();

            DB::commit();

            return Response::json([
                'code' => 201,
                'message' => 'Cliente creado correctamente.',
                'customers' => $customers
            ]);
        } catch (Exception $exception) {
            DB::rollBack();
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    /**
     * Get detail product billing by id.
     * @param $productId
     * @return JsonResponse
     */
    public function getProductById($productId)
    {
        $product = ProductBilling::find($productId);
        return Response::json($product);
    }

    /**
     * Create at new invoice custom.
     * @param Request $request
     * @return array|JsonResponse
     */
    public function createCustomInvoice(Request $request)
    {
        try {
            // Get requests.
            $customerId = $request->get('customerId');
            $typeInvoice = $request->get('typeInvoice');
            $paymentFormSat = $request->get('paymentFormSat');
            $paymentMethodSat = $request->get('paymentMethodSat');
            $dateIssue = $request->get('dateIssue');
            $comments = $request->get('comments');
            $bankName = $request->get('bankName');
            $accountNumber = $request->get('accountNumber');
            $orderNumber = $request->get('orderNumber');
            $currency = $request->get('currency');
            $paymentConditions = $request->get('conditionsPayment');
            $products = $request->get('items');

            $date = Carbon::now()->toDateTimeString();
            if ($dateIssue == 1) $date = Carbon::now()->subDay()->toDateTimeString();
            if ($dateIssue == 2) $date = Carbon::now()->subDays(2)->toDateTimeString();

            $cfdiType = "I";
            if ($typeInvoice == 2) $cfdiType = "E";
            if ($typeInvoice == 14) $cfdiType = "P";

            if ($customerId != 0) {
                $customerData = customer_data::where('customer_id', $customerId)->first();
            }

            $company = companie::find(Auth::user()->companie);
            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $jobCenter = profile_job_center::find($employee->profile_job_center_id);
            $urlLogo = 'https://storage.pestwareapp.com/' . $company->pdf_logo;
            $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $employee->profile_job_center_id)->first();

            // Validations
            if ($jobCenterBilling != null) {
                if ($jobCenterBilling->status_connection_sat == 0) {
                    return [
                        'code' => 500,
                        'message' => 'No se ha establecido conexión con el SAT.'
                    ];
                }
            } else {
                return [
                    'code' => 500,
                    'message' => 'La facturación no esta configurada.'
                ];
            }

            if (!CommonInvoice::isFoliosAvailable($company->id, $jobCenter->id, 1)) {
                return [
                    'code' => 404,
                    'message' => 'No cuentas con folios disponibles para facturar.'
                ];
            }

            // Items
            $items = array();
            $description = "";
            foreach ($products as $product) {
                $productBilling = ProductBilling::find($product['productId']);
                $description = $description . $productBilling->description . ', ';
                $unit = UnitSat::find($productBilling->unit_code_billing_id);
                $total = $product['total'];
                $subtotal = $product['subtotal'];
                if ($product['totDiscount'] > 0) $subtotal = $subtotal - $product['totDiscount'];
                // Taxes
                $taxes = array();
                if ($product['iva'] > 0) {
                    $base = $product['subtotal'];
                    if ($product['ieps'] > 0) $base = $base + bcdiv($product['ieps'], 1, 2);
                    if ($product['totDiscount'] > 0) $base = $base - $product['totDiscount'];
                    $tax = [
                        "Name" => "IVA",
                        "Rate" => $productBilling->iva, // 0.16
                        "Total" => bcdiv($product['iva'], '1', 2), // base * rate = total
                        "Base" => $base,
                        "IsFederalTax" => "true",
                        "IsRetention" => "false"
                    ];
                    array_push($taxes, $tax);
                }
                if ($productBilling->iva == 0) {
                    if (Str::length($productBilling->iva) == 1) {
                        $tax = [
                            "Name" => "IVA Exento",
                            "Rate" => "0", // 0.16
                            "Total" => "0", // base * rate = total
                            "Base" => $subtotal,
                            "IsFederalTax" => "true",
                            "IsRetention" => "false"
                        ];
                        array_push($taxes, $tax);
                    }
                }
                if ($product['iva_ret'] > 0) {
                    $tax = [
                        "Name" => "IVA RET",
                        "Rate" => $productBilling->iva_ret, // 0.16
                        "Total" => bcdiv($product['iva_ret'], 1, 2), // base * rate = total
                        "Base" => $subtotal,
                        "IsFederalTax" => "true",
                        "IsRetention" => "true"
                    ];
                    array_push($taxes, $tax);
                }
                if ($product['isr'] > 0) {
                    $tax = [
                        "Name" => "ISR",
                        "Rate" => $productBilling->isr, // 0.16
                        "Total" => bcdiv($product['isr'], 1, 2), // base * rate = total
                        "Base" => $subtotal,
                        "IsFederalTax" => "true",
                        "IsRetention" => "true"
                    ];
                    array_push($taxes, $tax);
                }
                if ($product['ieps'] > 0) {
                    $tax = [
                        "Name" => "IEPS",
                        "Rate" => $productBilling->ieps, // 0.16
                        "Total" => bcdiv($product['ieps'], 1, 2), // base * rate = total
                        "Base" => $subtotal,
                        "IsFederalTax" => "true",
                        "IsQuota" => "false", //TODO: Validate input quota
                        "IsRetention" => "false"
                    ];
                    array_push($taxes, $tax);
                }
                if (empty($taxes)) $taxes = null;
                $item = [
                    "Quantity" => $product['quantityProduct'],
                    "ProductCode" => $productBilling->product_code_billing_id, // 84111506
                    "UnitCode" => $unit->key, // E48
                    "Unit" => $unit->name,
                    "CuentaPredial" => $productBilling->property_account,
                    "Description" => $productBilling->description, // API folios adicionales
                    "IdentificationNumber" => $productBilling->identification_number,
                    "UnitPrice" => $productBilling->unit_price,
                    "Subtotal" => $product['subtotal'],
                    "Discount" => $product['totDiscount'],
                    "DiscountVal" => $product['discount'],
                    "TaxObject" => "02",
                    "Taxes" => $taxes,
                    "Total" => $total
                ];
                array_push($items, $item);
            }

            $params = [
                "Issuer" => [
                    "FiscalRegime" => $jobCenterBilling->fiscal_regime, // 601
                    "Rfc" => $jobCenterBilling->rfc, // SWO170518NG8
                    "Name" => $jobCenter->business_name,
                    "Address" => [
                        "Street" => $jobCenterBilling->address,
                        "ExteriorNumber" => $jobCenterBilling->address_number,
                        "InteriorNumber" => null,
                        "Neighborhood" => $jobCenterBilling->colony,
                        "ZipCode" => $jobCenterBilling->code_postal,
                        "Municipality" => $jobCenterBilling->municipality,
                        "State" => $jobCenterBilling->state,
                        "Country" => "México"
                    ]
                ],
                "Receiver" => [
                    "Name" => $customerId != 0 ? $customerData->billing : "Público en general", // Alberto Martínez Rangel
                    "FiscalRegime" => $customerId != 0 ? $customerData->fiscal_regime : "601",
                    "CfdiUse" => $customerId != 0 ? $customerData->cfdi_type : "P01", //G03
                    "Rfc" => $customerId != 0 ? $customerData->rfc : "XAXX010101000", // MARA9512306H2
                    "TaxZipCode" => $customerId != 0 ? $customerData->billing_postal_code : "20200", // 20200
                    "Address" => [
                        "Street" => $customerId != 0 ? $customerData->billing_street : "",
                        "ExteriorNumber" => $customerId != 0 ? $customerData->billing_exterior_number : "",
                        "InteriorNumber" => $customerId != 0 ? $customerData->billing_interior_number : "",
                        "Neighborhood" => $customerId != 0 ? $customerData->billing_colony : "",
                        "ZipCode" => $customerId != 0 ? $customerData->billing_postal_code : "",
                        "Municipality" => $customerId != 0 ? $customerData->billing_municipality : "",
                        "State" => $customerId != 0 ? $customerData->billing_state : "",
                        "Country" => "México"
                    ]
                ],
                "CfdiType" => $cfdiType, // I, E o P (Factura, Nota de Crédito, Complmento de Pago)
                "Currency" => $currency,
                "NameId" => $typeInvoice, // 1, 2 o 14 (Factura, Nota de Crédito, Complmento de Pago)
                "Folio" => CommonInvoice::getFolioNumberInvoice($company->id), // 100
                "ExpeditionPlace" => $jobCenterBilling->code_postal, // 20120
                "LogoUrl" => $urlLogo,
                "PaymentForm" => $paymentFormSat, // 01
                "PaymentMethod" => $paymentMethodSat, // PUE
                "Date" => $date,
                "Items" => $items,
                "Observations" => $comments,
                "PaymentBankName" => $bankName,
                "PaymentAccountNumber" => $accountNumber,
                "PaymentConditions" => $paymentConditions,
                "OrderNumber" => $orderNumber
            ];

            $response = FacturamaLibrary::createInvoice($params);

            if ($response['code'] == 201) {
                $complement = $response['data']->Complement;
                $taxStamp = $complement->TaxStamp;
                // Create Invoice in database.
                $invoice = new Invoice();
                $invoice->folio_invoice = CommonInvoice::getFolioInvoice($company->id);
                $invoice->id_invoice = $response['data']->Id;
                $invoice->uuid = $taxStamp->Uuid;
                $invoice->id_customer = $customerId != 0 ? $customerId : 7000;
                $invoice->description = $description;
                $invoice->amount = $response['data']->Total;
                $invoice->id_status = Constants::$STATUS_INVOICE_VALID;
                $invoice->date = Carbon::now()->toDateString();
                $invoice->id_company = $company->id;
                $invoice->save();

                CommonInvoice::addFolioConsumed($company->id, $jobCenter->id);

                return [
                    'code' => 201,
                    'message' => 'Factura creada correctamente.'
                ];

            } else {
                return [
                    'code' => 500,
                    'message' => $response['message']
                ];
            }
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
