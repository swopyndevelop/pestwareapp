<?php

namespace App\Http\Controllers\Reinforcements;

use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReinforcementsController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;
    //
    public function getReinforcements() {
        try {
            //Validate plan free
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                return response()->json(['response' => 500, 'message' => 'Algo salio mal, intente de nuevo.']);
            } else {
                $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
                $orders = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                    ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                    ->join('service_orders as so','e.id_service_order','so.id')
                    ->join('payment_methods as pm','so.id_payment_method','pm.id')
                    ->join('payment_ways as pw','so.id_payment_way','pw.id')
                    ->join('users as u','so.user_id','u.id')
                    ->join('statuses as st','so.id_status','st.id')
                    ->join('quotations as q','so.id_quotation','q.id')
                    ->join('establishment_types as et','q.establishment_id','et.id')
                    ->join('customers as c','q.id_customer','c.id')
                    ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                    ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                    ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                    ->join('discounts as d', 'qd.discount_id', 'd.id')
                    ->join('extras as ex', 'q.id_extra', 'ex.id')
                    ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                    ->select('so.id as order','so.id_service_order','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                        'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality',
                        'q.id_plague_jer','so.total','pt.plague_key','u.name as agente','cd.address_number','cd.state','cd.email','e.id_status',
                        'c.colony', 'so.warranty', 'so.reinforcement', 'so.tracing', 'q.establishment_id', 'so.whatsapp', 'q.id as id_quotation', 'so.confirmed', 'so.reminder', 'so.id_status as status_order', 'so.companie',
                        'e.id as id_event', 'pl.key','pl.id as lista','q.construction_measure', 'q.id_price_list', 'q.price_reinforcement', 'q.id_status as status_quotation', 'pl.reinforcement_days')
                    ->where('so.id_job_center', $jobCenterSession)
                    ->where('so.reinforcement', '<>', 1)
                    ->where('so.warranty', '<>', 1)
                    ->where('so.tracing', '<>', 1)
                    ->where('e.id_status', 4)
                    ->where('q.id_status', '<>', 2)
                    ->orderBy('e.initial_date','ASC')->get();

                $reinforcements = $this->getReinforcementsFilter($orders);
                return response()->json($reinforcements);
            }
        }catch (\Exception $exception) {
            return response()->json(['response' => 500, 'message' => 'Algo salio mal, intente de nuevo.']);
        }

    }

    public function getReinforcementsFilter($orders) {
        $reinforcements = new Collection();
        foreach ($orders as $order) {
            $priceTwo = $this->getTwoPrice($order->id_price_list, $order->construction_measure);
            if ($priceTwo != null && $priceTwo->price_two > 0) {
                $priceReinforcement = $priceTwo->price_two;
                if ($priceTwo != null) {
                    $orders->map(function($order) use ($priceReinforcement){
                        if ($order->price_reinforcement == null) {
                            $order->total = $priceReinforcement;
                        }else{
                            $order->total = $order->price_reinforcement + $priceReinforcement;
                        }
                        $diffInDays = $this->calculateDays($order->initial_date, $order->reinforcement_days);
                        $order->diffInDays = $diffInDays['days'];
                        if ($diffInDays['days'] < 0) $order->colorDays = "#b71c1c";
                        else $order->colorDays = "#0277bd";
                        $order->dateReinforcement = $diffInDays['dateReinforcement'];
                        if ($order->empresa == null) $order->empresa = "";
                        if ($order->status_quotation == 2) {
                            $motive = DB::table('canceled_quotations')->where('id_quotation', $order->id_quotation)->first();
                            $order->motive = $motive->reason . ", " . $motive->commentary;
                            $order->status = "Cancelado";
                            $order->color = "#b71c1c";
                        }else {
                            $order->status = "No Programado";
                            $order->color = "#b71c1c";
                            $order->motive = "";
                        }
                        $order->id_order_encoded = SecurityController::encodeId($order->order);
                    });
                    $reinforcements->push($order);
                }
            }
        }
        return $reinforcements; // Todo: add paginate(10)
    }

    private function getTwoPrice($idPriceList, $area) {
        return DB::table('prices')
            ->where('price_list_id', $idPriceList)
            ->where('area', '<=', $area)
            ->orderBy('area', 'desc')
            ->first();
    }

    private function calculateDays($dateService, $reinforcementDays) {
        $dateService = Carbon::parse($dateService);
        $now = Carbon::now();
        $days = $reinforcementDays - ($dateService->diffInDays($now));
        $dateReinforcement = $dateService->addDays($reinforcementDays);
        return [
            'days' => $days,
            'dateReinforcement' => $dateReinforcement->format('d-m-Y')
        ];
    }
}
