<?php

namespace App\Http\Controllers\Products;

use App\companie;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Catalogs\CommonBranch;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Library\FacturamaLibrary;
use App\Product_Tax;
use App\Tax;
use App\UnitSat;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\product;
use App\aditional_info;
use Maatwebsite\Excel\Facades\Excel;
use Storage;
use Carbon\Carbon;


class ProductController extends Controller
{
    public function index($jobcenter = 0)
    {
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $search = \Request::get('search');
        $types = DB::table('product_types')->where('profile_job_center_id', $dataJobCenter[2])->orderBy('name','asc')->get();
        $presentation = DB::table('product_presentations')->where('profile_job_center_id', $dataJobCenter[2])->orderBy('name','asc')->get();
        $unit = DB::table('product_units')->where('profile_job_center_id', $dataJobCenter[2])->orderBy('name','asc')->get();
        $taxes = Tax::where('profile_job_center_id',$dataJobCenter[2])->orderBy('name','asc')->get();
        $company = companie::find(Auth::user()->companie);
        $products = DB::table('products as p')
            ->join('product_types as pt','pt.id','p.id_type_product')
            ->join('product_presentations as pp', 'pp.id', 'p.id_presentation')
            ->join('product_units as pu', 'pu.id', 'p.id_unit')
            ->where('p.profile_job_center_id', $dataJobCenter[2])
            ->where('p.name', 'like', '%' . $search . '%')
            ->select('p.id', 'p.name as producto','p.description','p.characteristics','p.suggested_use',
                'p.active_ingredient','p.id as id_product', 'pt.id as type_id', 'pt.name as tipo',
                'pp.id as pre_id','pp.name as presentation','pu.id as id_unit','pu.name as unit','p.quantity',
                'p.base_price','p.created_at as fecha', 'p.image_product','p.sale_price', 'p.is_available')
            ->paginate(10);

        foreach ($products as $product) {
            $products->map(function ($product) {
                $info = DB::table('aditional_infos as a')
                    ->join('products as p','p.id','a.product_id')
                    ->where('a.product_id', $product->id)
                    ->select('a.id','a.product_id','a.file_route','a.original_name','a.updated_at')
                    ->get();
                $product->productsFile = $info;
                if($info->count() > 0) $product->productsFileDate = $info[0]->updated_at;
                else $product->productsFileDate = '';
                $taxProduct = Product_Tax::where('id_product', $product->id)
                    ->join('taxes as t', 'product_tax.id_tax','t.id')
                    ->select('t.name','t.value')
                    ->orderBy('id_product','asc')->get();
                $product->taxes = $taxProduct;
            });
        }

        $symbol_country = CommonCompany::getSymbolByCountry();

        if (\Request::get('export') == 'true'){
            $data = new Collection();
            $products = product::where('profile_job_center_id', \Request::get('idJobCenter'))
                ->where('is_available', 1)
                ->select('id','name','base_price','sale_price')
                ->orderBy('name')
                ->get();

            foreach ($products as $product) {
                if ($product->sale_price == null) $product->sale_price = 0;
                $products_tax = Product_Tax::join('taxes as t', 'product_tax.id_tax', 't.id')
                    ->where('product_tax.id_product', $product->id)
                    ->select('t.name','t.value')
                    ->get();
                $taxes = new Collection();
                $total =  $product->sale_price;
                foreach ($products_tax as $product_tax) {
                    $priceTax = $total * ($product_tax->value / 100);
                    $total += $priceTax;
                    $taxes = $taxes->push($product_tax->name, $product_tax->value);
                }
                $product->taxes = $taxes;
                $product->total_tax = $total;
            }
            $data = $products;
            try {
                $fileName = 'Listas-Precio-Productos';
                $sheetName = 'Productos';
                Excel::create($fileName, function($excel) use ($data, $sheetName) {
                    $excel->sheet($sheetName, function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->export('xls');

            }catch (\Exception $exception) {
                return redirect()->route('index_product')->with('warning', 'Algo salió mal intente de nuevo.');
            }
        }

        return view('vendor.adminlte.products.index')
            ->with([
                'types' => $types,
                'presentation' => $presentation,
                'unit' => $unit,
                'products' => $products,
                'taxes' => $taxes,
                'symbol_country' => $symbol_country,
                'jobCenterSession' => $dataJobCenter[0],
                'jobCenters' => $dataJobCenter[1],
                'company' => $company
            ]);
    }

    public function save(Request $request)
    {
        try {
            $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
            $product = new product();
            $product->id_type_product = $request->get('tipo');
            $product->name = $request->get('nombre');
            $product->description = $request->get('description');
            $product->characteristics = $request->get('adjetive');
            $product->suggested_use = $request->get('uso');
            $product->id_presentation = $request->get('presentacion');
            $product->id_unit = $request->get('unidad');
            $product->quantity = $request->get('cantidad');
            $product->base_price = $request->get('precio');
            $product->sale_price = $request->get('sale_price');
            $product->product_code_billing = $request->get('productCodeSatId');
            $product->unit_code_billing = $request->get('productUnitSatId');
            $is_available = 0;
            if ($request->get('is_available')) $is_available = 1;
            $product->active_ingredient = $request->get('ingredient');
            $product->is_available = $is_available;
            $product->active_ingredient = $request->get('ingredient');
            $product->register = $request->get('register');
            $product->profile_job_center_id = $jobCenterSession->id_profile_job_center;
            $product->id_companie = Auth::user()->companie;
            $product->save();
            $taxes = $request->get('taxes');
            foreach ($taxes as $tax) {
                $productTax = new Product_Tax;
                $productTax->id_product = $product->id;
                $productTax->id_tax = $tax;
                $productTax->save();
            }

            if($request->information) {
                foreach ($request->information as $i) {
                    $original = $i->getClientOriginalName();
                    $filename = $i->store('products');
                    aditional_info::create(['product_id' => $product->id,
                        'file_route' => $filename, 'original_name' => $original]);
                }
            }

            if($request->image) {
                $filename = $request->file('image')->store(
                    'products-image', 'ftp'
                );
                $prodImage = product::find($product->id);
                $prodImage->image_product = $filename;
                $prodImage->save();
            }

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (Exception $th) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, Intenta de nuevo'
            ]);
        }

    }

    public function download($id)
    {
       $info = aditional_info::find($id);
       $ruta = storage_path().'/app/'.$info->file_route;
       return response()->download($ruta, $info->original_name);
    }

    public function edit($id)
    {
        $id = SecurityController::decodeId($id);
        $product_id = product::find($id);
        // Middleware to deny access to data from another company
        if ($product_id->id_companie != auth()->user()->companie) {
            SecurityController::abort();
        }
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $types = DB::table('product_types')->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)->get();
        $presentation = DB::table('product_presentations')->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)->get();
        $unit = DB::table('product_units')->where('profile_job_center_id', $jobCenterSession->id_profile_job_center)->get();
        $taxes = Tax::where('profile_job_center_id',$jobCenterSession->id_profile_job_center)->orderBy('name','asc')->get();
        $taxProducts = Product_Tax::where('id_product', $id)->orderBy('id_product','asc')->get();
        $productUnitSat = UnitSat::find($product_id->unit_code_billing);
        if ($product_id->product_code_billing == null || $product_id->product_code_billing == 0) $productCodeSat = null;
        else {
            $keyword = $product_id->product_code_billing != null ? $product_id->product_code_billing : '0000';
            $productCodeSat = FacturamaLibrary::getCatalogsProductsOrServicesByKeyword($keyword);
            $productCodeSat = count($productCodeSat) > 0 ? $productCodeSat[0] : null;
        }

        $product = DB::table('products as p')
            ->join('product_types as pt','pt.id','p.id_type_product')
            ->join('product_presentations as pp', 'pp.id', 'p.id_presentation')
            ->join('product_units as pu', 'pu.id', 'p.id_unit')
            ->select('p.name as producto','p.description','p.characteristics','p.suggested_use',
                'p.active_ingredient', 'p.register','p.id as id_product', 'pt.id as type_id', 'pt.name as tipo',
                'pp.id as pre_id','pp.name as presentation','pu.id as id_unit','pu.name as unit','p.quantity',
                'p.base_price','p.id_type_product','p.id_presentation','p.id_unit as unidad','p.sale_price',
                'p.is_available')
            ->where('p.id',$id)->first();
        $company = companie::find(Auth::user()->companie);
        return view('vendor.adminlte.products._editproduct')
            ->with([
                'types' => $types,
                'presentation' => $presentation,
                'unit' => $unit,
                'product' => $product,
                'taxes' => $taxes,
                'taxProducts' => $taxProducts,
                'productUnitSat' => $productUnitSat,
                'productCodeSat' => $productCodeSat,
                'company' => $company
            ]);
    }

    public function update(Request $request)
    {
        $id = $request->get('id_product');
        $product = product::find($id);
        $product->id_type_product = $request->get('tipo');
        $product->name = $request->get('nombre');
        $product->description = $request->get('description');
        $product->characteristics = $request->get('adjetive');
        $product->suggested_use = $request->get('uso');
        $product->id_presentation = $request->get('presentacion');
        $product->id_unit = $request->get('unidad');
        $product->quantity = $request->get('cantidad');
        $product->base_price = $request->get('precio');
        $product->sale_price = $request->get('sale_price');
        $product->product_code_billing = $request->get('productCodeSatId');
        $product->unit_code_billing = $request->get('productUnitSatId');
        $is_available = 0;
        if ($request->get('is_available')) $is_available = 1;
        $product->is_available = $is_available;
        $product->active_ingredient = $request->get('ingredient');
        $product->register = $request->get('register');
        $product->save();
        $taxes = $request->get('taxes');
        if ($taxes != null){
            Product_Tax::where('id_product', $id)->delete();
            foreach ($taxes as $tax) {
                $productTax = new Product_Tax;
                $productTax->id_product = $product->id;
                $productTax->id_tax = $tax;
                $productTax->save();
            }
        }

        // Checar si el file es diferente de null para actualizar los documentos
        if($request->information != null){
            $doc_r = DB::table('aditional_infos')->select('file_route')->where('product_id',$id)->get();
            foreach($doc_r as $d) {
                //$ruta = storage_path().'/app/'.$d->file_route;
                Storage::delete($d->file_route);
            }
            $doc = DB::table('aditional_infos')->select('id','file_route','original_name')->where('product_id',$id)->delete();
            foreach($request->information as $i) {
                $original = $i->getClientOriginalName();
                $filename = $i->store('products');
                aditional_info::create(['product_id' => $product->id,
                'file_route' => $filename, 'original_name' => $original]);
            }
        }

        if($request->image != null){
            $filename = $request->file('image')->store(
                'products-image', 'ftp'
            );
            $prodImage = product::find($product->id);
            $prodImage->image_product = $filename;
            $prodImage->save();
        }

        return redirect()->route('index_product');
    }
    public function view_delete($id)
    {
        $id = SecurityController::decodeId($id);
        $product = $id;

        $product_id = product::find($id);

        // Middleware to deny access to data from another company
        if ($product_id->id_companie != auth()->user()->companie) {
            SecurityController::abort();
        }

        return view('vendor.adminlte.products._deleteproduct')->with(['product'=>$product]);
    }
    public function delete(Request $request)
    {
        $id = $request->product;

        $product_id = product::find($id);

        // Middleware to deny access to data from another company
        if ($product_id->id_companie != auth()->user()->companie) {
            SecurityController::abort();
        }

        $doc_r = DB::table('aditional_infos')->select('file_route')->where('product_id',$id)->get();
            foreach($doc_r as $d){
                //$ruta = storage_path().'/app/'.$d->file_route;
                Storage::delete($d->file_route);
            }
        DB::table('aditional_infos')->select('id','file_route')->where('product_id',$id)->delete();
        DB::table('products')->where('id',$id)->delete();

        return redirect()->route('index_product');
    }

    public function listPrice($selectJobCenter){
        try {
            $products = product::where('profile_job_center_id', $selectJobCenter)
                ->where('is_available', 1)
                ->select('id','name','base_price','sale_price')
                ->orderBy('name')
                ->get();

            foreach ($products as $product) {
                if ($product->sale_price == null) $product->sale_price = 0;
                $products_tax = Product_Tax::join('taxes as t', 'product_tax.id_tax', 't.id')
                    ->where('product_tax.id_product', $product->id)
                    ->select('t.name','t.value')
                    ->get();
                $taxes = new Collection();
                $total =  $product->sale_price;
                foreach ($products_tax as $product_tax) {
                    $priceTax = $total * ($product_tax->value / 100);
                    $total += $priceTax;
                    $taxes = $taxes->push($product_tax->name, $product_tax->value);
                }
                $product->taxes = $taxes;
                $product->total_tax = $total;
            }
            $data = new Collection();
            $data = $products;
            $symbol_country = CommonCompany::getSymbolByCountry();
            return response()->json([
                'code' => 201,
                'products' => $data,
                'symbol_country' => $symbol_country,
                'message' => 'Datos cargados correctamente'
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, Intenta de nuevo'
            ]);
        }
    }
}
