<?php

namespace App\Http\Controllers\Services;

use App\Area;
use App\AreaInspections;
use App\cash;
use App\Common\CommonImage;
use App\Common\CommonLabel;
use App\Common\Constants;
use App\companie;
use App\Country;
use App\custom_quote;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Security\SecurityController;
use App\InvoiceOrder;
use App\JobCenterSession;
use App\Library\FacturamaLibrary;
use App\Notifications\LogsNotification;
use App\personal_mail;
use App\Plan;
use App\PriceList;
use App\profile_job_center;
use App\service_firms;
use App\service_order;
use App\Http\Controllers\Controller;
use App\ShareServiceOrder;
use App\source_origin;
use App\StationInspection;
use App\treeJobCenter;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\event;
use App\customer;
use App\customer_data;
use App\quotation;
use App\User;
use App\employee;
use App\customer_data_habit_condition;
use App\customer_data_inhabitant_type;
use App\customer_data_mascot;
use App\habit_condition_service_order;
use App\inhabitant_type_service_order;
use App\inhabitant_type;
use App\mascot;
use App\mascot_service_order;
use App\customer_branche;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\Types\Integer;
use Session;
use Entrust;
use Vinkla\Hashids\Facades\Hashids;
use Storage;


/**
 * @author Alberto Martínez || Yomira Martínez
 * @version 21/04/2019
 * Controller Calendar, CRUD para eventos del Calendario.
 */
class CalendarController extends Controller
{
    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    //Crear variable de sesion para guardar idJobcenter seleccionado.
    /*public function __construct(){

        if (!Session::has('jobCenterCalendar')) Session::put('jobCenterCalendar', 0);

    }*/

    public function index(Request $request, $id = 0, $idCustomerFilter = 0, $idEmployee = 0, $jobcenter = 0) {

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        $jobCenterEmployee = 0;
        $jobCenterStatus = 0;
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get();
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();
        $paymentMethods = DB::table('payment_methods')->select('id', 'name')->where('companie', auth()->user()->companie)->get();

        $profileJobCenterSelect = $profileJobCenter;
        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        $plansBilling = Plan::where('type', Constants::$PLAN_TYPE_BILLING)->get();
        $plansBilling->map(function($plan) {
            $plansBilling = $plan;
            $plansBilling['price_folio'] = $plan->price / $plan->quantity;
            return $plansBilling;
        });

        // Filters
        $keyPriceLists = PriceList::where('profile_job_center_id', $profileJobCenter)->get();
        $customers = customer::where('id_profile_job_center', $profileJobCenter)->get();
        $employeesFilter = employee::where('profile_job_center_id', $profileJobCenter)->get();
        $address = DB::table('customers as c')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->select('c.id', 'cd.address', 'cd.address_number', 'c.colony')
            ->where('c.id_profile_job_center', $profileJobCenter)
            ->get();
        $paymentTypes = DB::table('payment_ways')
            ->where('profile_job_center_id', $profileJobCenter)
            ->select('id', 'name')
            ->get();
        $sources = source_origin::where('profile_job_center_id', $profileJobCenter)->get();

        $employeesTec = DB::table('employees')
            ->where('profile_job_center_id', $profileJobCenter)
            ->get();

        $collectionTechnicians = collect();
        foreach ($employeesTec as $tec) {
            $user = User::find($tec->employee_id);
            if ($user->can('Realizar Servicios (App)')) {
                $collectionTechnicians->push($tec);
            }
        }

        $technicians = $collectionTechnicians;

        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd','c.id','customer_id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->select('ev.id','ev.title','ev.initial_hour','ev.final_hour','ev.initial_date',
                'ev.final_date','em.name as employee','so.id_service_order as order','so.total',
                'c.name as client','c.cellphone','cd.address','c.colony','c.municipality','cd.address_number',
                'cd.state','pt.plague_key','em.color', 'ev.id_status')
            ->where('so.id_job_center', $profileJobCenter)
            ->where('ev.id_status', '!=', 3);

        if ($idCustomerFilter != 0) $events = $events->where('c.id', $idCustomerFilter)->get();
        else if ($idEmployee != 0) $events = $events->where('ev.id_employee', $idEmployee)->get();
        else $events = $events->get();

        $eventsTechnicians = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->select('ev.id','ev.title','ev.initial_hour','ev.final_hour','ev.initial_date',
                'ev.final_date', 'em.color', 'ev.id_status')
            ->where('ev.id_employee', $idEmployee)
            ->where('ev.id_status', '!=', 3)
            ->get();

        //Consulta de ordenes no programadas
        $ordersNotSchedule = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
            ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
            ->join('service_orders as so','e.id_service_order','so.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->select('so.id as order','so.id_service_order','c.name as cliente', 'so.id_job_center', 'so.id_status')
            ->where('so.id_job_center', $profileJobCenter)
            ->where('so.id_status',10)
            ->orderBy('so.id','DESC')
            ->get();

        //Llenado de tabla
        if (!$request->initialDateCreated && $request->search == null) {

            $forPage = 10;
            if ($request->exists('forPage')) $forPage = $request->get('forPage');

            //Consulta para llenar la tabla
            $orders = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('payment_methods as pm','so.id_payment_method','pm.id')
                ->join('payment_ways as pw','so.id_payment_way','pw.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('source_origins as sor','c.source_origin_id','sor.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->select('so.id as order','so.id_service_order','so.created_at as date','e.initial_hour','e.initial_date','em.name', 'so.id_job_center',
                    'c.name as cliente','c.establishment_name as empresa','c.cellphone', 'c.cellphone_main','cd.address','c.municipality', 'q.price_reinforcement',
                    'q.id_plague_jer','so.total','pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                    'c.colony', 'so.warranty', 'so.reinforcement', 'so.tracing', 'q.establishment_id', 'so.whatsapp', 'q.id as id_quotation', 'so.confirmed', 'so.reminder',
                     'so.id_status as status_order', 'so.companie', 'e.id as id_event', 'pl.key','pl.id as lista','q.construction_measure',
                    'so.customer_branch_id', 'so.tracing_comments', 'so.evidence_tracings','so.is_shared','so.is_main', 'so.subtotal', 'so.invoiced', 'c.id as customerId',
                    'so.inspection', 'sor.name as source_name' , 'so.invoiced_status')
                ->where('so.id_job_center', $profileJobCenter)
                ->where('so.id_service_order', 'like', '%'. $request->get('search') .'%')
                ->orderBy('so.id','DESC')
                ->paginate($forPage);
/*
            $priceTwo = DB::table('prices')
            ->join('price_lists as p','prices.price_list_id','p.id')
            ->where('p.profile_job_center_id', $profileJobCenter)
            ->orderBy('area', 'desc')
            ->get();*/

            foreach ($orders as $o) {
                $orders->map(function($o){
                    $o->customerIdEncrypt = SecurityController::encodeId($o->customerId);
                    $isInspection = DB::table('station_inspections')->where('id_service_order', $o->order)->first();
                    if ($isInspection) $o->isInspection = 1;
                    else $o->isInspection = 0;
                    if ($o->customer_branch_id != null) {
                        $customerBranch = customer_branche::find($o->customer_branch_id);
                        $o->address_number = $customerBranch->address_number;
                        $o->address = $customerBranch->address;
                        $o->colony = $customerBranch->colony;
                        $o->municipality = $customerBranch->municipality;
                        $o->state = $customerBranch->state;
                        $o->empresa = $customerBranch->name;
                        $o->cellphone = $customerBranch->phone;
                    }
                    if ($o->id_status == 1) {
                        $plagues = DB::table('plague_type_quotation as pq')
                            ->join('plague_types as pt', 'pq.plague_type_id', 'pt.id')
                            ->where('pq.quotation_id', $o->id_quotation)->get();

                        $plaguesText = "";
                        foreach ($plagues as $plague) {
                            $plaguesText = $plaguesText . $plague->name . ", ";
                        }
                        $pricesList = DB::table('price_lists')->where('id', $o->lista)->first();
                        $indications = DB::table('indications')->where('id', $pricesList->indications_id)->first()->description;
                        $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
                        $countryCode = DB::table('countries')->where('id', $companie->id_code_country)->first();

                        // Custom reminder.
                        $reminderText = personal_mail::where('profile_job_center_id', $o->id_job_center)->first()->reminder_whatsapp;
                        $replaced = str_replace('[customer_name]', $o->cliente, $reminderText);
                        $replaced = str_replace('[company_name]', $companie->name, $replaced);
                        $replaced = str_replace('[service_date]', Carbon::parse($o->initial_date)->format('d/m/Y'), $replaced);
                        $replaced = str_replace('[service_hour]', date('h:i A', strtotime($o->initial_hour)), $replaced);
                        $replaced = str_replace('[service_plagues]', $plaguesText, $replaced);
                        $replaced = str_replace('[service_amount]', $o->total, $replaced);
                        $replaced = str_replace('[service_folio]', $o->id_service_order, $replaced);
                        $replaced = str_replace('[service_user]', $o->agente, $replaced);

                        $o->urlWhatsappIndications = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=Hola " . $o->cliente . ", " . $indications;
                        $o->urlWhatsappReminder = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" . $replaced;

                    }
                    if ($o->id_status == 3) {
                        $motive = DB::table('canceled_service_orders')->where('id_service_order', $o->order)->first();
                        $o->motive = 'Otro, Sin comentario';
                        if($motive) $o->motive = $motive->reason . ", " . $motive->commentary;
                    }
                    $o->cashes = null;
                    if ($o->id_status != 3) {
                        $cashes = DB::table('cashes')->where('id_event', $o->id_event)->first();
                        $conditions = DB::table('place_conditions')->where('id_service_order', $o->order)->first();
                        $rating = DB::table('ratings_service_orders')->where('id_service_order', $o->order)->first();

                        if(!empty($cashes)){
                            $o->cashes = $cashes;
                            if($cashes->payment == 1)
                                $o->adeudo = 1;
                            else{
                                $o->adeudo = 2;
                            }
                            if ($cashes->id_payment_way == 1) {
                                $o->credito = 1;
                            }else{
                                $o->credito = 2;
                            }
                        }else{
                            $o->adeudo = null;
                            $o->credito = null;
                            $o->cashes = null;
                        }
                    }
                    if ($o->id_status == 4) {

                        if ($conditions->indications == 2) {
                            $o->indications = 2;
                            $o->commentary_indications = $conditions->commentary;
                        }else{
                            $o->indications = 1;
                            $o->commentary_indications = "";
                        }
                        if (!empty($rating)) {
                            $o->rating = $rating->rating;
                            $o->rating_comments = $rating->comments;
                        }else{
                            $o->rating = 0;
                            $o->rating_comments = "";
                        }
                    }
                    $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
                    $countryCode = DB::table('countries')
                        ->where('id', $companie->id_code_country)->first();
                    $o->urlWhatsappShow = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . $o->cellphone . "&text=";
                    $idOrderEncrypt = Hashids::encode($o->order);
                    $o->urlWhatsappEmail = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country;
                    $o->urlWhatsappEmailService = "\n%0d%0dCertificado de Servicio: https://pestwareapp.com/order/" . $idOrderEncrypt . "/PDF";
                    $o->urlWhatsappEmailOrder = "\n%0d%0dOrden de Servicio: https://pestwareapp.com/order/" . $idOrderEncrypt . "/PDF/service";
                    if ($isInspection) {
                        $o->urlWhatsappEmailInspection = "\n%0d%0dMonitoreo de Estaciones: https://pestwareapp.com/station/monitoring/pdf/" . $idOrderEncrypt;
                    }
                    if($o->is_main == 1) {
                        $shareServiceOrder = ShareServiceOrder::where('id_service_order_main',$o->order)->first();
                        $employees = employee::where('id', $shareServiceOrder->id_employee)->select('name')->get();
                        $o->technicianAuxiliares = $employees;
                    }
                    $o->isAreaInspection = 0;

                    $isAreaInspection = AreaInspections::where('id_service_order', $o->order)->first();
                    if ($isAreaInspection) {
                        $o->isAreaInspection = 1;
                        $o->isAreaInspectionPdf = "\n%0d%0dMonitoreo de Áreas: https://pestwareapp.com/area/inspections/pdf/" . $idOrderEncrypt;
                    }
                    // Invoice
                    $invoice = InvoiceOrder::where('id_service_order', $o->order)->first();
                    if ($invoice) $o->invoice = "Facturado";
                    else $o->invoice = "";
                });
            }
        }
        else{
            //Validate plan free
            if (Auth::user()->id_plan == $this->PLAN_FREE) {
                return redirect()->route('calendario');
            }

            // Filters
            $idOS = $request->get('search');
            $initialDateCreated = $request->get('initialDateCreated');
            $finalDateCreated = $request->get('finalDateCreated');
            $initialDateEvent = $request->get('initialDateEvent');
            $finalDateEvent = $request->get('finalDateEvent');
            $idEmployee = $request->get('idEmployee');
            $customerName = $request->get('customerName');
            $customerPhone = $request->get('customerPhone');
            $customerAddress = $request->get('customerAddress');
            $key = $request->get('key');
            $source = $request->get('idSource');
            $payment = $request->get('payment');
            $status = $request->get('status');

            $forPage = 10;
            if ($request->exists('forPage')) $forPage = $request->get('forPage');

            // Consulta para llenar la tabla
            $order = Event::join('employees as em','events.id_employee','em.id')
                ->join('profile_job_centers as pjc','events.id_job_center','pjc.id')
                ->join('service_orders as so','events.id_service_order','so.id')
                ->join('payment_methods as pm','so.id_payment_method','pm.id')
                ->join('payment_ways as pw','so.id_payment_way','pw.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('source_origins as sor','c.source_origin_id','sor.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->where('so.id_job_center', $profileJobCenter);


            if ($request->get('export') != 'true') {

                $order->select('so.id as order','so.id_service_order','so.created_at as date','events.initial_hour','events.initial_date','em.name',
                    'c.name as cliente','c.establishment_name as empresa','c.cellphone', 'c.cellphone_main','cd.address','c.municipality','c.source_origin_id' ,'q.price_reinforcement',
                    'q.id_plague_jer','so.total','pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','events.id_status',
                    'c.colony', 'so.warranty', 'so.reinforcement', 'so.tracing', 'q.establishment_id', 'so.whatsapp', 'q.id as id_quotation', 'so.confirmed', 'so.reminder',
                    'so.id_status as status_order', 'so.companie', 'events.id as id_event', 'pl.key','pl.id as lista','q.construction_measure', 'events.service_type',
                    'so.customer_branch_id', 'so.tracing_comments', 'so.evidence_tracings','so.is_shared','so.is_main', 'so.subtotal', 'so.invoiced', 'c.id as customerId',
                    'so.id_job_center', 'sor.name as source_name', 'so.invoiced_status');
            }
            else {
                $order->select('so.id as orden','so.id_service_order','so.created_at as date','u.name as agente','events.initial_hour','events.initial_date',
                    'em.name', 'c.name as cliente','c.establishment_name as empresa','c.cellphone', 'c.cellphone_main','cd.address','cd.address_number','c.colony','c.municipality',
                    'cd.state','et.name as Service','pl.key','events.id_status','so.total', 'so.tracing_comments','pl.id as lista','so.customer_branch_id','q.id as id_quotation',
                    'events.start_event as hora_inicio', 'events.final_event as hora_fin');
            }

            if ($idOS != null) $order->where('so.id_service_order', 'like', '%'. $idOS .'%');
            else {
                if ($initialDateCreated != "null" && $finalDateCreated != "null") {
                    $order->whereDate('so.created_at', '>=', $initialDateCreated)
                        ->whereDate('so.created_at', '<=', $finalDateCreated);
                }
                if ($initialDateEvent != "null" && $finalDateEvent != "null") {
                    $order->whereDate('events.initial_date', '>=', $initialDateEvent)
                        ->whereDate('events.initial_date', '<=', $finalDateEvent);
                }

                if ($idEmployee != 0) $order->where('events.id_employee', $idEmployee);
                if ($customerName != 0) $order->where('c.id', $customerName);
                if ($customerPhone != 0) $order->where('c.id', $customerPhone);
                if ($customerAddress != 0) $order->where('c.id', $customerAddress);
                if ($key != 0) $order->where('pl.id', $key);
                if ($source != 0) $order->where('c.source_origin_id', $source);
                if ($payment != 0) {
                    if ($payment == 3) $order->join('cashes as ch', 'ch.id_service_order', 'so.id')->where('ch.id_payment_way', 1);
                    else if ($payment == 4) $order->join('place_conditions as pc', 'pc.id_service_order', 'so.id')->where('indications', 2);
                    else $order->join('cashes as ch', 'ch.id_service_order', 'so.id')->where('ch.payment', $payment);
                }
                if ($status > 0 && $status < 6) $order->where('events.id_status', $status);
                if ($status == 6) $order->where('events.service_type', 1);
                if ($status == 7) $order->where('events.service_type', 3);
                if ($status == 8) $order->where('events.service_type', 2);
                if ($status == 9) $order->where('events.service_type', 4);
                if ($status == 10) $order->where('so.invoiced', 1);
            }

            if ($request->get('export') != 'true') {
                $orders = $order->orderBy('so.id','DESC')->paginate($forPage)->appends([
                    'initialDateCreated' => $request->initialDateCreated,
                    'finalDateCreated' => $request->finalDateCreated,
                    'initialDateEvent' => $request->initialDateEvent,
                    'finalDateEvent' => $request->finalDateEvent,
                    'idEmployee' => $request->idEmployee,
                    'customerName' => $request->customerName,
                    'customerPhone' => $request->customerPhone,
                    'customerAddress' => $request->customerAddress,
                    'key' => $request->key,
                    'payment' => $request->payment,
                    'status' => $request->status,
                ]);
            }
            else {
                $orders = $order->get();
            }

            if ($orders->count() > 80) {
                return redirect()->route('calendario')->with('warning', 'Demasiados registros, Intenta con un periodo menor');
            }

            /*$priceTwo = DB::table('prices')
                ->join('price_lists as p','prices.price_list_id','p.id')
                ->where('p.profile_job_center_id', $profileJobCenter)
                ->orderBy('area', 'desc')
                ->get();*/

            if($request->get('export') != 'true'){
                foreach ($orders as $o) {
                    $orders->map(function($o){
                        $o->customerIdEncrypt = SecurityController::encodeId($o->customerId);
                        $isInspection = DB::table('station_inspections')->where('id_service_order', $o->order)->first();
                        if ($isInspection) $o->isInspection = 1;
                        else $o->isInspection = 0;
                        if ($o->customer_branch_id != null) {
                            $customerBranch = customer_branche::find($o->customer_branch_id);
                            $o->address_number = $customerBranch->address_number;
                            $o->address = $customerBranch->address;
                            $o->colony = $customerBranch->colony;
                            $o->municipality = $customerBranch->municipality;
                            $o->state = $customerBranch->state;
                            $o->empresa = $customerBranch->name;
                            $o->cellphone = $customerBranch->phone;
                        }
                        if ($o->id_status == 1) {
                            $plagues = DB::table('plague_type_quotation as pq')
                                ->join('plague_types as pt', 'pq.plague_type_id', 'pt.id')
                                ->where('pq.quotation_id', $o->id_quotation)->get();

                            $plaguesText = "";
                            foreach ($plagues as $plague) {
                                $plaguesText = $plaguesText . $plague->name . ", ";
                            }
                            $pricesList = DB::table('price_lists')->where('id', $o->lista)->first();
                            $indications = DB::table('indications')->where('id',$pricesList->indications_id)->first()->description;
                            $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
                            $countryCode = DB::table('countries')->where('id', $companie->id_code_country)->first();

                            // Custom reminder.
                            $reminderText = personal_mail::where('profile_job_center_id', $o->id_job_center)->first()->reminder_whatsapp;
                            $replaced = str_replace('[customer_name]', $o->ciente, $reminderText);
                            $replaced = str_replace('[company_name]', $companie->name, $replaced);
                            $replaced = str_replace('[service_date]', Carbon::parse($o->initial_date)->format('d/m/Y'), $replaced);
                            $replaced = str_replace('[service_hour]', date('h:i A', strtotime($o->initial_hour)), $replaced);
                            $replaced = str_replace('[service_plagues]', $plaguesText, $replaced);
                            $replaced = str_replace('[service_amount]', $o->total, $replaced);
                            $replaced = str_replace('[service_folio]', $o->id_service_order, $replaced);
                            $replaced = str_replace('[service_user]', $o->agente, $replaced);

                            $o->urlWhatsappIndications = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=Hola " . $o->cliente . ", " . $indications;
                            $o->urlWhatsappReminder = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" . $replaced;
                        }
                        if ($o->id_status == 3) {
                            $motive = DB::table('canceled_service_orders')->where('id_service_order', $o->order)->first();
                            $o->motive = 'Otro, Sin comentario';
                            if ($motive) $o->motive = $motive->reason . ", " . $motive->commentary;
                        }
                        $o->cashes = null;
                        if ($o->id_status != 3) {
                            $cashes = DB::table('cashes')->where('id_event', $o->id_event)->first();
                            $conditions = DB::table('place_conditions')->where('id_service_order', $o->order)->first();
                            $rating = DB::table('ratings_service_orders')->where('id_service_order', $o->order)->first();

                            if(!empty($cashes)){
                                $o->cashes = $cashes;
                                if($cashes->payment == 1)
                                    $o->adeudo = 1;
                                else{
                                    $o->adeudo = 2;
                                }
                                if ($cashes->id_payment_way == 1) {
                                    $o->credito = 1;
                                }else{
                                    $o->credito = 2;
                                }
                            }else{
                                $o->adeudo = null;
                                $o->credito = null;
                                $o->cashes = null;
                            }
                        }
                        if ($o->id_status == 4) {
                            $conditions = DB::table('place_conditions')->where('id_service_order', $o->order)->first();
                            $rating = DB::table('ratings_service_orders')->where('id_service_order', $o->order)->first();
                            if ($conditions) {
                                if ($conditions->indications == 2) {
                                    $o->indications = 2;
                                    $o->commentary_indications = $conditions->commentary;
                                }else{
                                    $o->indications = 1;
                                    $o->commentary_indications = "";
                                }
                            } else {
                                $o->indications = 1;
                                $o->commentary_indications = "";
                            }

                            if (!empty($rating)) {
                                $o->rating = $rating->rating;
                                $o->rating_comments = $rating->comments;
                            }else{
                                $o->rating = 0;
                                $o->rating_comments = "";
                            }
                        }
                        $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
                        $countryCode = DB::table('countries')
                            ->where('id', $companie->id_code_country)->first();
                        $o->urlWhatsappShow = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . $o->cellphone . "&text=";
                        $idOrderEncrypt = Hashids::encode($o->order);
                        $o->urlWhatsappEmail = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country;
                        $o->urlWhatsappEmailService = "\n%0d%0dCertificado de Servicio: https://pestwareapp.com/order/" . $idOrderEncrypt . "/PDF";
                        $o->urlWhatsappEmailOrder = "\n%0d%0dOrden de Servicio: https://pestwareapp.com/order/" . $idOrderEncrypt . "/PDF/service";
                        if ($isInspection) {
                            $o->urlWhatsappEmailInspection = "\n%0d%0dMonitoreo de Estaciones: https://pestwareapp.com/station/monitoring/pdf/" . $idOrderEncrypt;
                        }
                        if($o->is_main == 1) {
                            $shareServiceOrder = ShareServiceOrder::where('id_service_order_main',$o->order)->first();
                            $employees = employee::where('id', $shareServiceOrder->id_employee)->select('name')->get();
                            $o->technicianAuxiliares = $employees;
                        }
                        $o->isAreaInspection = 0;
                        $isAreaInspection = AreaInspections::where('id_service_order', $o->order)->first();
                        if ($isAreaInspection) {
                            $o->isAreaInspection = 1;
                            $o->isAreaInspectionPdf = "\n%0d%0dMonitoreo de Áreas: https://pestwareapp.com/area/inspections/pdf/" . $idOrderEncrypt;
                        }
                        // Invoice
                        $invoice = InvoiceOrder::where('id_service_order', $o->order)->first();
                        if ($invoice) $o->invoice = "Facturado";
                        else $o->invoice = "";
                    });
                }
            }
            else {

                foreach ($orders as $o) {
                    $orders->map(function($o) use ($request){
                        $o->empresa = $o->empresa == null ? "N/A": $o->empresa;
                        $o->cellphone = $o->cellphone == null ? "N/A": $o->cellphpne;
                        $o->cellphone_main = $o->cellphone_main == null ? "N/A": $o->cellphone_main;
                        $o->address = $o->address == null ? "N/A": $o->address;
                        $o->address_number = $o->address_number == null ? "N/A": $o->address_number;
                        $o->colony = $o->colony == null ? "N/A": $o->colony;
                        $o->municipality = $o->municipality == null ? "N/A": $o->municipality;
                        $o->state = $o->state == null ? "N/A": $o->state;
                        $o->customer_branch_id = $o->customer_branch_id == null ? "": $o->customer_branch_id;
                        $o->tracing_comments = $o->tracing_comments == null ? "N/A": $o->tracing_comments;
                        $customQuote = custom_quote::where('id_quotation', $o->id_quotation)->first();
                        $o->FrecuencyMonth = $customQuote == null ? "Único": $customQuote->frecuency_month;
                        $isInspection = DB::table('station_inspections')->where('id_service_order', $o->order)->first();
                        $conditions = DB::table('place_conditions')->where('id_service_order', $o->order)->first();
                        if ($isInspection) $o->isInspection = 1;
                        else $o->isInspection = 0;
                        if ($o->customer_branch_id != null) {
                            $customerBranch = customer_branche::find($o->customer_branch_id);
                            $o->address_number = $customerBranch->address_number;
                            $o->address = $customerBranch->address;
                            $o->colony = $customerBranch->colony;
                            $o->municipality = $customerBranch->municipality;
                            $o->state = $customerBranch->state;
                            $o->empresa = $customerBranch->name;
                            $o->cellphone = $customerBranch->phone == null ? "": $customerBranch->phone;
                        }
                        if ($o->id_status == 1) {
                            $plagues = DB::table('plague_type_quotation as pq')
                                ->join('plague_types as pt', 'pq.plague_type_id', 'pt.id')
                                ->where('pq.quotation_id', $o->id_quotation)->get();

                            $plaguesText = "";
                            foreach ($plagues as $plague) {
                                $plaguesText = $plaguesText . $plague->name . ", ";
                            }
                            $pricesList = DB::table('price_lists')->where('id', $o->lista)->first();
                            $indications = DB::table('indications')->where('id',$pricesList->indications_id)->first()->description;
                            $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
                            $countryCode = DB::table('countries')->where('id', $companie->id_code_country)->first();

                            if ($request->get('export') != 'true') {
                                $o->urlWhatsappIndications = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=Hola " . $o->cliente . ", " . $indications;
                                $o->urlWhatsappReminder = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=Hola " . $o->cliente . ", Te escribo de " .$companie->name . ", te recordamos que el día " . Carbon::parse($o->initial_date)->format('d/m/Y') . " a las " . date('h:i A', strtotime($o->initial_hour)) . " tenemos programado tu servicio de fumigación contra: " . $plaguesText . " el cual tiene un costo de $" . $o->total . ". " . $companie->reminder_whatsapp . ". " . $o->id_service_order . "%0dAgente: " . $o->agente;
                            }
                        }
                        if ($o->id_status == 3) {
                            $motive = DB::table('canceled_service_orders')->where('id_service_order', $o->order)->first();
                            $o->motive = 'Otro, Sin comentario';
                            if ($motive) $o->motive = $motive->reason . ", " . $motive->commentary;
                        }
                        $o->cashes = null;
                        if ($o->id_status != 3) {
                            $cashes = DB::table('cashes')->where('id_event', $o->id_event)->first();
                            $rating = DB::table('ratings_service_orders')->where('id_service_order', $o->order)->first();

                            if(!empty($cashes)){
                                $o->cashes = $cashes;
                                if($cashes->payment == 1)
                                    $o->adeudo = 1;
                                else{
                                    $o->adeudo = 2;
                                }
                                if ($cashes->id_payment_way == 1) {
                                    $o->credito = 1;
                                }else{
                                    $o->credito = 2;
                                }
                            }else{
                                if ($request->get('export') == 'true')  {
                                    $o->adeudo = $o->adeudo == 2 ? 'Pagado': "N/A";
                                    $o->credito = $o->credito == 1 ? 'Crédito': "N/A";
                                    $o->cashes = "N/A";
                                }
                                $o->adeudo = null;
                                $o->credito = null;
                                $o->cashes = null;
                            }
                        }
                        if ($o->id_status == 4) {
                            if ($conditions){
                                if ($conditions->indications == 2) {
                                    $o->indications = 2;
                                    $o->commentary_indications = $conditions->commentary;
                                }else{
                                    $o->indications = 1;
                                    $o->commentary_indications = "N/A";
                                }
                            }
                            if (!empty($rating)) {
                                $o->rating = $rating->rating;
                                $o->rating_comments = $rating->comments;
                            }else{
                                $o->rating = 0;
                                $o->rating_comments = "N/A";
                            }
                        }
                        if($request->get('export') != 'true'){
                            $companie = DB::table('companies')->where('id', Auth::user()->companie)->first();
                            $countryCode = DB::table('countries')->where('id', $companie->id_code_country)->first();
                            $o->urlWhatsappShow = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . $o->cellphone . "&text=";
                        }
                        if($o->id_status == 1) $o->id_status = 'Programado';
                        if($o->id_status == 2) $o->id_status = 'Comenzado';
                        if($o->id_status == 3) $o->id_status = 'Cancelado';
                        if($o->id_status == 4) $o->id_status = 'Finalizado';

                    });
                }
            }

            if ($request->get('export') == 'true') {
                try {
                    $fileName = 'Ordenes-Servicio';
                    $sheetName = 'Ordenes de Servicio';
                    Excel::create($fileName, function($excel) use ($orders, $sheetName) {
                        $excel->sheet($sheetName, function($sheet) use ($orders) {
                            $sheet->fromArray($orders);
                        });
                    })->export('xls');

                }catch (\Exception $exception) {
                    return redirect()->route('calendario')->with('warning', 'Algo salió mal intente de nuevo.');
                }
            }
        }

        $now = Carbon::today()->toDateString();
        $symbol_country = CommonCompany::getSymbolByCountry();

        //dd($orders);

        return view('vendor.adminlte.register.calendar')
            ->with([
                'jobCenters' => $jobCenters, 'jobCenterEmployee' => $jobCenterEmployee, 'technicians' => $technicians, 'events' => $events,
                'idOrder' => $id, 'order'=> $orders, 'eventsTechnicians' => $eventsTechnicians, 'treeJobCenterMain' => $treeJobCenterMain,
                'jobCenterStatus' => $jobCenterStatus, 'ordersNotSchedule' => $ordersNotSchedule, 'profileJobCenterSelect' => $profileJobCenterSelect,
                'jobCenterSelect' => $profileJobCenter, 'paymentMethods' => $paymentMethods, 'sources' => $sources,
                'paymentTypes' => $paymentTypes, 'keyPriceLists' => $keyPriceLists, 'customers' => $customers, 'jobCenterSession' => $jobCenterSession,
                'employeesFilter' => $employeesFilter, 'now' => $now, 'address' => $address, 'symbol_country' => $symbol_country,
                'idCustomerFilter' => $idCustomerFilter, 'idEmployeeFilter' => $idEmployee, 'plansBilling' => $plansBilling
        ]);
    }

    /**
     * Get all technicians available by range hour event
     * @param Request $request
     * @return JsonResponse
     */
    public function getTechniciansAvailable(Request $request) {
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $initialDate = $request->get('initialDate');
        $finalDate = $request->get('finalDate');
        $initialHour = $request->get('initialHour');
        $finalHour = $request->get('finalHour');
        $employee = employee::where('employee_id', Auth::user()->id)->first();

        if ($request->exists('id_event')) {
            $employeesAvailable = CommonCalendar::getTechniciansAvailableForUpdate(
                $initialDate, $initialHour, $finalHour, $request->get('id_event'), $jobCenterSession->id_profile_job_center);
        } else $employeesAvailable = CommonCalendar::getTechniciansAvailable($initialDate, $initialHour, $finalHour, $jobCenterSession->id_profile_job_center);

        return response()->json($employeesAvailable);
    }

    public function getInfoEvent($id)
    {
        try {
            $id = SecurityController::decodeId($id);
            $idOrder = DB::table('events as ev')
                ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                ->join('quotations as qo', 'so.id_quotation', 'qo.id')
                ->join('users as u','so.user_id','u.id')
                ->join('customers as cu', 'qo.id_customer', 'cu.id')
                ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
                ->join('employees as em', 'ev.id_employee', 'em.id')
                ->join('price_lists as pl', 'qo.id_price_list', 'pl.id')
                ->select('cu.name', 'cu.establishment_name','so.id_service_order', 'ev.initial_date','qo.companie', 'u.name as agente',
                    'ev.initial_hour', 'ev.final_hour','so.total', 'cd.address as street', 'cd.address_number', 'so.warranty', 'so.reinforcement',
                    'cu.colony', 'em.name as technical', 'qo.id as id_quotation', 'cu.cellphone', 'cu.cellphone_main', 'qo.construction_measure',
                    'cu.municipality', 'cd.state', 'so.id as id_order', 'ev.id_status as status', 'so.tracing', 'qo.id_job_center',
                    'ev.id as id_event', 'so.customer_branch_id', 'cd.email','pl.id as lista','so.total','so.confirmed', 'qo.price_reinforcement',
                    'so.is_shared','so.is_main', 'ev.latitude', 'ev.longitude', 'ev.start_event', 'so.inspection')
                ->where('ev.id', $id)
                ->first();

            $isInspection = DB::table('station_inspections')->where('id_service_order', $idOrder->id_order)->first();
            $rating = DB::table('ratings_service_orders')->where('id_service_order', $idOrder->id_order)->first();
            $conditions = DB::table('place_conditions')->where('id_service_order', $idOrder->id_order)->first();
            $indications = null;
            $pricesList = DB::table('price_lists')->where('id', $idOrder->lista)->first();
            $indicationsWhatsapp = DB::table('indications')->where('id', $pricesList->indications_id)->first()->description;
            $cash = cash::where('id_service_order',  $idOrder->id_order)->first();
            $areaInspection = AreaInspections::where('id_service_order', $idOrder->id_order)->first();

            if ($isInspection) $isInspection = 1;
            else $isInspection = 0;
            if ($rating) $rating = 1;
            else $rating = 0;
            if ($conditions) $indications = $conditions->indications;
            else $indications;
            if ($pricesList) $indicationsWhatsapp;
            else $indicationsWhatsapp = null;
            if ($cash) {
                $id_payment_method = $cash->id_payment_method;
                $id_payment_way = $cash->id_payment_way;
                $amount_received = $cash->amount_received;
                $commentary = $cash->commentary;
                $payment = $cash->payment;
                $pay_day = Carbon::parse($cash->updated_at)->toDateTimeString() ;
                $update_cash = 1;
            }
            else {
                $id_payment_method = null;
                $id_payment_way = null;
                $amount_received = null;
                $commentary = null;
                $payment = null;
                $pay_day = null;
                $update_cash = 0;
            }

            // Encoded id
            $idOrder->id_order = Hashids::encode($idOrder->id_order);
            $urlWhatsappEmailInspection = ' ';
            if ($isInspection) $urlWhatsappEmailInspection = "\n" . "%0d%0dMonitoreo de Estaciones: https://pestwareapp.com/station/monitoring/pdf/" . $idOrder->id_order;
            $isAreaInspection = 0;
            $isAreaInspectionPdf = ' ';
            if ($areaInspection) {
                $isAreaInspection = 1;
                $isAreaInspectionPdf = "\n%0d%0dMonitoreo de Áreas: https://pestwareapp.com/area/inspections/pdf/" . $idOrder->id_order;
            }


            $customer = $idOrder->name;
            if ($idOrder->establishment_name) $customer = $idOrder->name . ' - ' . $idOrder->establishment_name;
            //Ajuste de datos si es cotización personalizada
            if ($idOrder->customer_branch_id != null) {
                $customerBranch = customer_branche::find($idOrder->customer_branch_id);
                $idOrder->name = $idOrder->name . " " . $customerBranch->name;
                $idOrder->street = $customerBranch->address;
                $idOrder->address_number = $customerBranch->address_number;
                $idOrder->colony = $customerBranch->colony;
                $idOrder->municipality = $customerBranch->municipality;
                $idOrder->state = $customerBranch->state;
                $idOrder->cellphone = $customerBranch->phone;
                $customer = $customerBranch->name;
                if ($customerBranch->manager) $customer = $idOrder->name . ' - ' . $customerBranch->manager;
            }

            $plagues = DB::table('plague_type_quotation as pq')
                ->join('plague_types as pt', 'pq.plague_type_id', 'pt.id')
                ->where('pq.quotation_id', $idOrder->id_quotation)->get();

            $plaguesText = "";
            foreach ($plagues as $plague) {
                $plaguesText = $plaguesText . $plague->name . ", ";
            }
            $idPlan = Auth::user()->id_plan;
            $code_country = CommonCompany::getCodeByCountry();
            $company = companie::find($idOrder->companie);
            $urlWhatsappEmail = "https://api.whatsapp.com/send?phone=" . $code_country;
            $urlWhatsappEmailService = "\n%0d%0dCertificado de Servicio: https://pestwareapp.com/order/" . $idOrder->id_order . "/PDF";
            $urlWhatsappEmailOrder = "\n%0d%0dOrden de Servicio: https://pestwareapp.com/order/" . $idOrder->id_order . "/PDF/service";
            $urlWhatsappIndications = "https://api.whatsapp.com/send?phone=" . $code_country . "{&text=Hola " . $idOrder->name . ", " . $indicationsWhatsapp;

            // Custom reminder.
            $reminderText = personal_mail::where('profile_job_center_id', $idOrder->id_job_center)->first()->reminder_whatsapp;
            $replaced = str_replace('[customer_name]', $idOrder->name, $reminderText);
            $replaced = str_replace('[company_name]', $company->name, $replaced);
            $replaced = str_replace('[service_date]', Carbon::parse($idOrder->initial_date)->format('d/m/Y'), $replaced);
            $replaced = str_replace('[service_hour]', date('h:i A', strtotime($idOrder->initial_hour)), $replaced);
            $replaced = str_replace('[service_plagues]', $plaguesText, $replaced);
            $replaced = str_replace('[service_amount]', $idOrder->total, $replaced);
            $replaced = str_replace('[service_folio]', $idOrder->id_service_order, $replaced);
            $replaced = str_replace('[service_user]', $idOrder->agente, $replaced);

            $urlWhatsappReminder = "https://api.whatsapp.com/send?phone=" . $code_country . "{&text=".$replaced;

            $showPrice = Entrust::can('No Mostrar Precio (Agenda)');

            $collection = collect($idOrder);
            $collection->put('plagues', $plaguesText);
            $collection->put('initial_date', date('d-m-Y', strtotime($idOrder->initial_date)));
            $collection->put('initial_hour', date('h:i A', strtotime($idOrder->initial_hour)));
            $collection->put('final_hour', date('h:i A', strtotime($idOrder->final_hour)));
            $collection->put('address', $idOrder->street . " #" . $idOrder->address_number . ", " . $idOrder->colony . ". " . $idOrder->municipality . ", " . $idOrder->state);
            $collection->put('id_plan', $idPlan);
            $collection->put('urlWhatsappEmail', $urlWhatsappEmail);
            $collection->put('urlWhatsappEmailService', $urlWhatsappEmailService);
            $collection->put('urlWhatsappEmailOrder', $urlWhatsappEmailOrder);
            $collection->put('isInspection', $isInspection);
            $collection->put('urlWhatsappEmailInspection', $urlWhatsappEmailInspection);
            $collection->put('isAreaInspectionPdf', $isAreaInspectionPdf);
            $collection->put('rating', $rating);
            $collection->put('id_order_decrypt', SecurityController::decodeId($idOrder->id_order));
            $collection->put('id_event_encrypt', SecurityController::encodeId($idOrder->id_event));
            $collection->put('indications', $indications);
            $collection->put('indicationsWhatsapp', $urlWhatsappIndications);
            $collection->put('urlWhatsappReminder', $urlWhatsappReminder);
            $collection->put('id_payment_method', $id_payment_method);
            $collection->put('id_payment_way', $id_payment_way);
            $collection->put('amount_received', $amount_received);
            $collection->put('commentary', $commentary);
            $collection->put('payment', $payment);
            $collection->put('pay_day', $pay_day);
            $collection->put('update_cash', $update_cash);
            $collection->put('customer', $customer);
            $collection->put('isAreaInspection', $isAreaInspection);
            $collection->put('showPrice', $showPrice);

            return response()->json([
                'code' => 200,
                'data' => $collection
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'code' => 500,
                'data' => 'Algo salio mal, intenta de nuevo'
            ]);
        }


    }


    /**
     * @param Request $request
     * @return JsonResponse
     *
     * Save new event calendar
     */
    public function store(Request $request)
    {
        try {
            //Validate Plan Diamant
            /*if (Auth::user()->id_plan != $this->PLAN_BUSINESS) {
                return \Response::json([
                    'code' => 500,
                    'message' => 'Algo salio mal, intenta de nuevo.'
                ]);
            }*/
            DB::beginTransaction();

            $idOrder = $request->get('idOrderService');
            $techniciansAuxiliaries = $request->get('techniciansAuxiliariesEvent');
            $orderService = DB::table('service_orders as so')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('customers as c', 'q.id_customer', 'c.id')
                ->where('so.id', $idOrder)
                ->select('so.id_quotation', 'so.id as id_order', 'so.address', 'q.id_customer', 'c.name as nameCustomer', 'so.id_service_order',
                    'so.warranty', 'so.reinforcement', 'so.tracing')
                ->first();

            if ($techniciansAuxiliaries != null){

                if (in_array($request->get('techniciansEvent'), $techniciansAuxiliaries)) {
                    return \Response::json([
                        'code' => 500,
                        'message' => 'El técnico principal no puede ser auxiliar.'
                    ]);
                }

                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                    $serviceOrder = service_order::find($idOrder);
                    $new = $serviceOrder->replicate();
                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                    $new->save();

                    $event = new event();
                    $event->title = $request->title;
                    $event->id_employee = $techniciansAuxiliary;
                    $event->initial_hour = $request->get('hourStart');
                    $event->final_hour = $request->get('hourEnd');
                    $event->initial_date = $request->get('dateStart');
                    $event->final_date = $request->get('dateEnd');
                    $event->id_service_order = $new->id;
                    $event->id_status = 1;
                    $event->id_job_center = $serviceOrder->id_job_center;
                    $event->companie = $serviceOrder->companie;
                    if ($new->warranty == 0 && $new->reinforcement == 0 && $new->tracing == 0)
                        $event->service_type = 1;
                    elseif($new->warranty == 1) $event->service_type = 3;
                    elseif($new->reinforcement == 1) $event->service_type = 2;
                    elseif($new->tracing == 1) $event->service_type = 4;
                    $event->save();

                    $order = service_order::find($new->id);
                    $order->id_status = 4;
                    $order->is_shared = 1;
                    $saved = $order->save();
                    //send notification app technician
                    if ($saved) {
                        $employee = employee::find($techniciansAuxiliary);
                        $user = User::find($employee->employee_id);
                        $message = "Fecha: " . $request->get('dateStart') . " (" . $request->get('hourStart') . ")  " . $new->id_service_order;

                        if ($new->warranty == 0 && $new->reinforcement == 0 && $new->tracing == 0) $user->sendFCM('Nuevo Servicio', $message);
                        elseif($new->warranty == 1) $user->sendFCM('Nuevo Garantia', $message);
                        elseif($new->reinforcement == 1) $user->sendFCM('Nuevo Refuerzo', $message);
                        elseif($new->tracing == 1) $user->sendFCM('Nuevo Seguimiento', $message);
                    }

                    $employeeId = employee::find($techniciansAuxiliary)->id;
                    $shareServiceOrder = new ShareServiceOrder();
                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                    $shareServiceOrder->id_service_order = $new->id;
                    $shareServiceOrder->id_employee = $employeeId;
                    $shareServiceOrder->save();

                }
            }

            $evento = event::where('id_service_order', $idOrder)->first();
            $evento['title'] = $request->title;
            $evento['id_employee'] = $request->get('techniciansEvent');
            $evento['initial_hour'] = $request->get('hourStart');
            $evento['final_hour'] = $request->get('hourEnd');
            $evento['initial_date'] = $request->get('dateStart');
            $evento['final_date'] = $request->get('dateEnd');
            $evento['id_service_order'] = $orderService->id_order;
            $evento['id_status'] = 1;
            if ($orderService->warranty == 0 && $orderService->reinforcement == 0 && $orderService->tracing == 0)
                $evento['service_type'] = 1;
            elseif($orderService->warranty == 1) $evento['service_type'] = 3;
            elseif($orderService->reinforcement == 1) $evento['service_type'] = 2;
            elseif($orderService->tracing == 1) $evento['service_type'] = 4;
            $evento->save();

            $order = service_order::find($orderService->id_order);
            $order->id_status = 4;
            $order->is_shared = $techniciansAuxiliaries != null ? 1 : 0;
            $order->is_main = $techniciansAuxiliaries != null ? 1 : 0;
            $saved = $order->save();

            //send notification app technician
            if ($saved) {
                $employee = employee::find($request->get('techniciansEvent'));
                $user = User::find($employee->employee_id);
                $message = "Fecha: " . $request->get('dateStart') . " (" . $request->get('hourStart') . ")  " . $orderService->id_service_order;

                if ($orderService->warranty == 0 && $orderService->reinforcement == 0 && $orderService->tracing == 0)
                    $user->sendFCM('Nuevo Servicio', $message);
                elseif($orderService->warranty == 1) $user->sendFCM('Nuevo Garantia', $message);
                elseif($orderService->reinforcement == 1) $user->sendFCM('Nuevo Refuerzo', $message);
                elseif($orderService->tracing == 1) $user->sendFCM('Nuevo Seguimiento', $message);
            }

            DB::commit();

            return \Response::json([
                'code' => 200,
                'message' => 'Servicio actualizado.'
            ]);
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo'
            ]);
        }
    }

    public function edit($id){

        $id = SecurityController::decodeId($id);

        $order_sr = DB::table('events as e')->join('service_orders as so','e.id_service_order','so.id')
                    ->join('payment_methods as pm','so.id_payment_method','pm.id')
                    ->join('payment_ways as pw','so.id_payment_way','pw.id')
                    ->join('quotations as q', 'so.id_quotation','q.id')
                    ->join('customers as c', 'q.id_customer','c.id')
                    ->join('customer_datas as cd', 'c.id','customer_id')
                    ->join('discount_quotation as dq','q.id','dq.quotation_id')
                    ->select('so.id as order','so.id_service_order','so.observations','so.bussiness_name as empresa','so.email as e_mail','so.total as t', 'so.subtotal',
                    'pm.id as metodo','pm.name as met_name','pw.id as way','pw.name as way_name', 'q.id_job_center',
                    'q.id as quotation', 'q.construction_measure', 'q.garden_measure','q.id_plague_jer',
                    'q.total','q.price','c.name as cliente','c.id as customer','c.cellphone','c.colony','c.municipality',
                    'cd.address','cd.address_number','cd.state','cd.reference_address','cd.email','cd.billing',
                    'cd.rfc','cd.tax_residence', 'cd.contact_two_name', 'cd.contact_two_phone','dq.discount_id','e.id as event', 'so.customer_branch_id',
                    'cd.email_billing', 'cd.billing_mode', 'cd.cfdi_type', 'cd.payment_method as payment_method_sat', 'cd.payment_way as payment_way_sat')
            ->where('so.id',$id)
            ->first();

        //dd($order_sr);

        if ($order_sr->customer_branch_id != null) {
            $customerBranch = customer_branche::find($order_sr->customer_branch_id);
            $order_sr->address_number = $customerBranch->address_number;
            $order_sr->address = $customerBranch->address;
            $order_sr->colony = $customerBranch->colony;
            $order_sr->municipality = $customerBranch->municipality;
            $order_sr->state = $customerBranch->state;
            $order_sr->empresa = $customerBranch->name;
            $order_sr->cellphone = $customerBranch->phone;
        }

        $order_service_id = service_order::find($id);
        // Middleware to deny access to data from another company
        if ($order_service_id->companie != auth()->user()->companie) {
            SecurityController::abort();
        }

        $pregnancy = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$id)->where('habit_condition_id',1)->first();
        $baby = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$id)->where('habit_condition_id',2)->first();
        $children = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$id)->where('habit_condition_id',3)->first();
        $adult = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$id)->where('habit_condition_id',4)->first();

        $respiratory = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$id)->where('inhabitant_type_id',1)->first();
        $inmunologic = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$id)->where('inhabitant_type_id',2)->first();
        $renal = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$id)->where('inhabitant_type_id',3)->first();
        $cardiac = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$id)->where('inhabitant_type_id',4)->first();
        $does_not = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$id)->where('inhabitant_type_id',5)->first();
        $other = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id','specification')->where('service_order_id',$id)->where('inhabitant_type_id',6)->first();

        $dog = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$id)->where('mascot_id',1)->first();
        $cat = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$id)->where('mascot_id',2)->first();
        $bird = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$id)->where('mascot_id',3)->first();
        $fish = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$id)->where('mascot_id',4)->first();
        $other_mascot = DB::table('mascot_service_order')->select('mascot_id','specification')->where('service_order_id',$id)->where('mascot_id',5)->first();

        $payment_method = DB::table('payment_methods')->where('profile_job_center_id', $order_sr->id_job_center)->select('id','name')->get();
        $payment_way = DB::table('payment_ways')->where('profile_job_center_id', $order_sr->id_job_center)->select('id','name')->get();

        if($order_sr->garden_measure == null){//Obtener el area total a fumigar
            $area = $order_sr->construction_measure;
        }else{
            $area = $order_sr->construction_measure+$order_sr->garden_measure;
        }
        //Plagas
        $plague = DB::table('plague_type_quotation as qpt')->join('plague_types as pt','pt.id','qpt.plague_type_id')
            ->select('pt.name','pt.id as plague')->where('qpt.quotation_id',$order_sr->quotation)->get();
        $plague2 = collect($plague)->toArray();//Generar array con todas las plagas seleccionadas
        //Descuentos
        $d = DB::table('discounts')->select('percentage')->where('id',$order_sr->discount_id)->first();
        $d1 = $d->percentage/100;
        $discount = $order_sr->price*$d1;

        //Calculo del iva
        $profile_job_center = profile_job_center::where('id', $order_sr->id_job_center)->first();
        $iva_country = $profile_job_center->iva / 100;
        $iva = $order_sr->subtotal * $iva_country;
        $symbol_country = CommonCompany::getSymbolByCountry();
        $commonLabels = new CommonLabel();
        $labels = $commonLabels->getLabelsByJobCenter($order_sr->id_job_center);

        //Catalogs SAT
        $typesCfdi = FacturamaLibrary::getCatalogsCfdiUses();
        $paymentMethodsSat = FacturamaLibrary::getCatalogsPaymentMethods();
        $paymentFormsSat = FacturamaLibrary::getCatalogsPaymentForms();

        return view('vendor.adminlte.register._editschedule')->with(['order_sr' => $order_sr, 'pregnancy' => $pregnancy,
            'baby' => $baby, 'children' => $children, 'adult' => $adult, 'respiratory' => $respiratory, 'renal' => $renal,
            'cardiac' => $cardiac, 'does_not' => $does_not, 'other' => $other, 'dog' => $dog, 'cat' => $cat,
            'bird' => $bird, 'fish' => $fish, 'other_mascot' => $other_mascot, 'area' => $area, 'plague' => $plague,
            'plague2' => $plague2, 'd' => $d, 'd1' => $d1, 'discount' => $discount, 'iva' => $iva, 'inmunologic' => $inmunologic,
            'payment_method' => $payment_method, 'payment_way' => $payment_way, 'symbol_country' => $symbol_country,
            'profile_job_center' => $profile_job_center, 'labels' => $labels, 'typesCfdi' => $typesCfdi, 'paymentMethodsSat' => $paymentMethodsSat, 'paymentFormsSat' => $paymentFormsSat]);
    }

    public function update(Request $request){

        //Orden de servicio

        $order = service_order::find($request->o_id);
        $order->id_payment_method = $request->pay_mC;
        $order->id_payment_way = $request->pay_fC;
        $order->bussiness_name = $request->establishmentNameC;
        $order->address = $request->address_rfcC;
        $order->email = $request->e_mailC;
        $order->observations = $request->commentsC;
        $order->total = $request->totalC;
        $order->save();

        //Customer
        if ($order->customer_branch_id != null) {
            $customerBranch = customer_branche::find($order->customer_branch_id);
            $customerBranch->address_number = $request->numberC;
            $customerBranch->address = $request->addressC;
            $customerBranch->colony = $request->colonyC;
            $customerBranch->municipality = $request->municipalityC;
            $customerBranch->state = $request->stateC;
            $customerBranch->manager = $request->customerNameC;
            $customerBranch->phone = $request->cellphoneC;
            $customerBranch->save();
        } else {
            $customer = customer::find($request->customer_idC);
            $customer->name = $request->customerNameC;
            $customer->cellphone = $request->cellphoneC;
            $customer->colony = $request->colonyC;
            $customer->municipality = $request->municipalityC;
            $customer->save();
            //Customer data
            $customer_data = DB::table('customer_datas')->select('id')->where('customer_id',$request->customer_idC)->first();
            $datas = customer_data::find($customer_data->id);
            $datas->address = $request->addressC;
            $datas->reference_address = $request->referenceC;
            $datas->contact_two_name = $request->contactC;
            $datas->contact_two_phone = $request->cellphone_twoC;
            $datas->address_number = $request->numberC;
            $datas->state = $request->stateC;
            $datas->payment_method = $request->get('methodPayCustomerMainNew');
            $datas->payment_way = $request->get('wayPayCustomerMainNew');
            $datas->cfdi_type = $request->get('typeCfdiCustomerMainNew');
            if($request->habilitarC == 1){
                $datas->rfc = $request->rfcC;
                $datas->billing = $request->socialC;
                $datas->tax_residence = $request->address_rfcC;
                $datas->email_billing = $request->get('e_mailC');
            }
            $datas->save();
        }

        //Habit
        //Embarazo
        if ($request->pregnancyC != null) {
            $p = DB::table('habit_condition_service_order')->select('service_order_id as so','habit_condition_id as hc')->where('service_order_id',$request->o_id)
                ->where('habit_condition_id',1)->first();
            if($p == null){
              $h = new habit_condition_service_order;
              $h->service_order_id = $request->o_id;
              $h->habit_condition_id = $request->pregnancyC;
              $h->save();
            }
        }else{
            $p1 = DB::table('habit_condition_service_order')->select('service_order_id as so','habit_condition_id as hc', 'id')->where('service_order_id',$request->o_id)
            ->where('habit_condition_id',1)->first();
            if($p1 != null){
                $p = habit_condition_service_order::find($p1->id);
                $p->delete();
            }
        }
        //Bebes
        if ($request->babyC != null) {
            $p2 = DB::table('habit_condition_service_order')->select('service_order_id as so','habit_condition_id as hc')->where('service_order_id',$request->o_id)
                ->where('habit_condition_id',2)->first();
            if($p2 == null){
                $h2 = new habit_condition_service_order;
                $h2->service_order_id = $request->o_id;
                $h2->habit_condition_id = $request->babyC;
                $h2->save();
            }
        }else{
            $p3 = DB::table('habit_condition_service_order')->select('service_order_id as so','habit_condition_id as hc', 'id')->where('service_order_id',$request->o_id)
            ->where('habit_condition_id',2)->first();
            if($p3 != null){
                $p = habit_condition_service_order::find($p3->id);
                $p->delete();
            }
        }
        //Niños
        if ($request->childrenC != null) {
            $p4 = DB::table('habit_condition_service_order')->select('service_order_id as so','habit_condition_id as hc')->where('service_order_id',$request->o_id)
            ->where('habit_condition_id',3)->first();
            if($p4 == null){
                $h3 = new habit_condition_service_order;
                $h3->service_order_id = $request->o_id;
                $h3->habit_condition_id = $request->childrenC;
                $h3->save();
            }
        }else{
            $p5 = DB::table('habit_condition_service_order')->select('service_order_id as so','habit_condition_id as hc', 'id')->where('service_order_id',$request->o_id)
            ->where('habit_condition_id',3)->first();
            if($p5 != null){
                $p = habit_condition_service_order::find($p5->id);
                $p->delete();
            }
        }
        //Adulto Mayor
        if ($request->adultC != null) {
            $p6 = DB::table('habit_condition_service_order')->select('service_order_id as so','habit_condition_id as hc')->where('service_order_id',$request->o_id)
            ->where('habit_condition_id',4)->first();
            if($p6 == null){
                $h4 = new habit_condition_service_order;
                $h4->service_order_id = $request->o_id;
                $h4->habit_condition_id = $request->adultC;
                $h4->save();
            }
        }else{
            $p7 = DB::table('habit_condition_service_order')->select('service_order_id as so','habit_condition_id as hc', 'id')->where('service_order_id',$request->o_id)
            ->where('habit_condition_id',4)->first();
            if($p7 != null){
                $p = habit_condition_service_order::find($p7->id);
                $p->delete();
            }
        }

        //Enfermedades Respiratorias
        if ($request->respiratoryC != null) {
            $a = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',1)->first();
            if ($a == null) {
                $inh = new inhabitant_type_service_order;
                $inh->service_order_id = $request->o_id;
                $inh->inhabitant_type_id = $request->respiratoryC;
                $inh->save();
            }
        }else{
            $a2 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it', 'id')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',1)->first();
            if($a2 != null){
                $a = inhabitant_type_service_order::find($a2->id);
                $a->delete();
            }
        }
        //Enfermedades Inmunologicas
        if ($request->inmunologicC != null) {
            $a3 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',2)->first();
            if ($a3 == null) {
                $inh2 = new inhabitant_type_service_order;
                $inh2->service_order_id = $request->o_id;
                $inh2->inhabitant_type_id = $request->inmunologicC;
                $inh2->save();
            }
        }else{
            $a4 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it', 'id')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',2)->first();
            if($a4 != null){
                $a = inhabitant_type_service_order::find($a4->id);
                $a->delete();
            }
        }
        //Enfermedades Renales
        if ($request->renalC != null) {
            $a5 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',3)->first();
            if ($a5 == null) {
                $inh3 = new inhabitant_type_service_order;
                $inh3->service_order_id = $request->o_id;
                $inh3->inhabitant_type_id = $request->renalC;
                $inh3->save();
            }
        }else{
            $a6 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it', 'id')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',3)->first();
            if($a6 != null){
                $a = inhabitant_type_service_order::find($a6->id);
                $a->delete();
            }
        }
        //Enfermedades Cardiacas
        if ($request->cardiacC != null) {
            $a7 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',4)->first();
            if ($a7 == null) {
                $inh4 = new inhabitant_type_service_order;
                $inh4->service_order_id = $request->o_id;
                $inh4->inhabitant_type_id = $request->cardiacC;
                $inh4->save();
            }
        }else{
            $a8 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it', 'id')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',4)->first();
            if($a8 != null){
                $a = inhabitant_type_service_order::find($a8->id);
                $a->delete();
            }
        }
        //No Aplica
        if ($request->does_notC != null) {
            $a9 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',5)->first();
            if ($a9 == null) {
                $inh5 = new inhabitant_type_service_order;
                $inh5->service_order_id = $request->o_id;
                $inh5->inhabitant_type_id = $request->does_notC;
                $inh5->save();
            }
        }else{
            $a10 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it', 'id')->where('service_order_id',$request->o_id)
                ->where('inhabitant_type_id',5)->first();
            if($a10 != null){
                $a = inhabitant_type_service_order::find($a10->id);
                $a->delete();
            }
        }
        //Otro
        if($request->otherC != null){
            $a11 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it')->where('service_order_id',$request->o_id)
            ->where('inhabitant_type_id',6)->first();
        if ($a11 == null) {
            $inh6 = new inhabitant_type_service_order;
            $inh6->service_order_id = $request->o_id;
            $inh6->inhabitant_type_id = $request->otherC;
            $inh6->specification = $request->specificC;
            $inh6->save();
        }
        }else{
            $a12 = DB::table('inhabitant_type_service_order')->select('service_order_id as so','inhabitant_type_id as it', 'id')->where('service_order_id',$request->o_id)
            ->where('inhabitant_type_id',6)->first();
            if($a12 != null){
                $a = inhabitant_type_service_order::find($a12->id);
                $a->delete();
            }
        }

        //Perro
        if ($request->dogC != null) {
            $b = DB::table('mascot_service_order')->select('service_order_id','mascot_id')->where('service_order_id',$request->o_id)
                ->where('mascot_id',1)->first();
            if($b == null){
                $m = new mascot_service_order;
                $m->service_order_id = $request->o_id;
                $m->mascot_id = $request->dogC;
                $m->save();
            }
        }else{
            $b1 = DB::table('mascot_service_order')->select('service_order_id','mascot_id', 'id')->where('service_order_id',$request->o_id)
            ->where('mascot_id',1)->first();
            if($b1 != null){
                $b = mascot_service_order::find($b1->id);
                $b->delete();
            }
        }
        //Gato
        if ($request->catC != null) {
            $b2 = DB::table('mascot_service_order')->select('service_order_id','mascot_id')->where('service_order_id',$request->o_id)
                ->where('mascot_id',2)->first();
            if($b2 == null){
                $m2 = new mascot_service_order;
                $m2->service_order_id = $request->o_id;
                $m2->mascot_id = $request->catC;
                $m2->save();
            }
        }else{
            $b3 = DB::table('mascot_service_order')->select('service_order_id','mascot_id', 'id')->where('service_order_id',$request->o_id)
                ->where('mascot_id',2)->first();
            if($b3 != null){
                $b = mascot_service_order::find($b3->id);
                $b->delete();
            }
        }
        //Ave
        if ($request->birdC != null) {
            $b4 = DB::table('mascot_service_order')->select('service_order_id','mascot_id')->where('service_order_id',$request->o_id)
                ->where('mascot_id',3)->first();
            if($b4 == null){
                $m3 = new mascot_service_order;
                $m3->service_order_id = $request->o_id;
                $m3->mascot_id = $request->birdC;
                $m3->save();
            }
        }else{
            $b5 = DB::table('mascot_service_order')->select('service_order_id','mascot_id', 'id')->where('service_order_id',$request->o_id)
                ->where('mascot_id',3)->first();
                if($b5 != null){
                    $b = mascot_service_order::find($b5->id);
                    $b->delete();
                }
        }
        //Pez
        if ($request->fishC != null) {
            $b6 = DB::table('mascot_service_order')->select('service_order_id','mascot_id')->where('service_order_id',$request->o_id)
                ->where('mascot_id',4)->first();
            if($b6 == null){
                $m4 = new mascot_service_order;
                $m4->service_order_id = $request->o_id;
                $m4->mascot_id = $request->fishC;
                $m4->save();
            }
        }else{
            $b7 = DB::table('mascot_service_order')->select('service_order_id','mascot_id', 'id')->where('service_order_id',$request->o_id)
                ->where('mascot_id',4)->first();
                if($b7 != null){
                    $b = mascot_service_order::find($b7->id);
                    $b->delete();
                }
        }
        //Otro
        if($request->other_mascotC != null){
            $b8 = DB::table('mascot_service_order')->select('service_order_id','mascot_id')->where('service_order_id',$request->o_id)
                ->where('mascot_id',5)->first();
            if($b8 == null){
                $m5 = new mascot_service_order;
                $m5->service_order_id = $request->o_id;
                $m5->mascot_id = $request->other_mascotC;
                $m5->specification = $request->mascotC;
                $m5->save();
            }
        }else{
            $b9 = DB::table('mascot_service_order')->select('service_order_id','mascot_id', 'id')->where('service_order_id',$request->o_id)
            ->where('mascot_id',5)->first();
            if($b9 != null){
                $b = mascot_service_order::find($b9->id);
                $b->delete();
            }
        }

        return response()->json();

    }

    public function edit_event($id)
    {
        $id = SecurityController::decodeId($id);
        $event = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->select('ev.id as event','ev.id_employee','ev.id_service_order',
                    'ev.initial_hour','ev.final_hour','ev.initial_date',
                    'ev.final_date','ev.id_job_center','em.name', 'ev.title', 'so.is_main', 'so.is_shared')
            ->where('ev.id',$id)->first();

            /*$technicians = DB::table('employees')
            ->select('id','name')
            ->where('profile_job_center_id', $event->id_job_center)
            ->get();*/
            $employeesTec = DB::table('employees')
            ->where('profile_job_center_id', $event->id_job_center)
            ->get();

        $collectionTechnicians = collect();

        foreach ($employeesTec as $tec) {
            $user = User::find($tec->employee_id);
            if ($user->can('Realizar Servicios (App)')) {
                $collectionTechnicians->push($tec);
            }
        }

        $techniciansAuxiliaries = new Collection();
        $shareOrders = ShareServiceOrder::where('id_service_order_main', $event->id_service_order)->get();
        foreach ($shareOrders as $shareOrder) {
            $employee = employee::find($shareOrder->id_employee);
            $techniciansAuxiliaries->push($employee);
        }

        $technicians = $collectionTechnicians;
            // Arreglo para select de technicians
        return view('vendor.adminlte.register._editcalendar')
            ->with([
                'event' => $event,
                'technicians' => $technicians,
                'techniciansAuxiliaries' => $techniciansAuxiliaries,
                'techniciansAuxiliariesCount' => $techniciansAuxiliaries->count()
            ]);
    }

    public function update_event(Request $request)
    {
        $id_event = $request->id_event;
        $event = event::find($id_event);
        $event->id_employee = $request->techniciansEventE;
        $event->initial_hour = $request->hourStartE;
        $event->final_hour = $request->hourEndE;
        $event->initial_date = $request->dateStartE;
        $event->final_date = $request->dateEndE;
        $event->id_status = 1;
        $event->save();

        //send notification app technician
        if ($event) {
            $employee = employee::find($request->get('techniciansEventE'));
            $user = User::find($employee->employee_id);
            $message = "Fecha: " . $request->get('dateStartE') . " (" . $request->get('hourStartE') . ") Se actualizo.";
            $user->sendFCM('Nuevo Servicio', $message);
        }

        return redirect()->route('calendario');
    }

    public function updateEventWithDrop(Request $request)
    {
        try {
           /* //Validate Plan Diamant
            if (Auth::user()->id_plan != $this->PLAN_BUSINESS) {
                return \Response::json([
                    'code' => 500,
                    'message' => 'Algo salio mal, intenta de nuevo.'
                ]);
            }*/
            $eventId = $request->get('idEvent');
            $techniciansAuxiliaries = $request->get('techniciansAuxiliaries');
            if (strlen($eventId) > 6) $eventId = SecurityController::decodeId($eventId);
            $event = event::find($eventId);
            $order = service_order::find($event->id_service_order);
            //if (Auth::user()->companie == 338) User::first()->notify(new LogsNotification("1", "", 3));

            if ($order->is_shared == 1 && $order->is_main == 1) {
                $shareOrders = ShareServiceOrder::where('id_service_order_main', $order->id)->get();
                foreach ($shareOrders as $shareOrder) service_order::where('id', $shareOrder->id_service_order)->delete();
                ShareServiceOrder::where('id_service_order_main', $event->id_service_order)->delete();
            }
            if ($order->is_shared == 1 && $order->is_main == 0) {
                $sharedOrder = ShareServiceOrder::where('id_service_order', $order->id)->first();
                if ($sharedOrder) {
                    $sharedOrder->id_employee = $request->get('techniciansEvent');
                    $sharedOrder->save();
                }
            }
            //if (Auth::user()->companie == 338) User::first()->notify(new LogsNotification("2", "", 3));
            if ($techniciansAuxiliaries != null){

                if (in_array($request->get('techniciansEvent'), $techniciansAuxiliaries)) {
                    return \Response::json([
                        'code' => 500,
                        'message' => 'El técnico principal no puede ser auxiliar.'
                    ]);
                }

                foreach ($techniciansAuxiliaries as $techniciansAuxiliary){
                    $serviceOrder = service_order::find($event->id_service_order);
                    $new = $serviceOrder->replicate();
                    $new->id_service_order = $serviceOrder->id_service_order . '-' . $techniciansAuxiliary;
                    $new->save();

                    $eventSecondary = new event();
                    $eventSecondary->title = $request->get('title');
                    $eventSecondary->id_employee = $techniciansAuxiliary;
                    $eventSecondary->initial_hour = $request->get('hourStart');
                    $eventSecondary->final_hour = $request->get('hourEnd');
                    $eventSecondary->initial_date = $request->get('dateStart');
                    $eventSecondary->final_date = $request->get('dateEnd');
                    $eventSecondary->id_service_order = $new->id;
                    $eventSecondary->id_status = 1;
                    $eventSecondary->id_job_center = $serviceOrder->id_job_center;
                    $eventSecondary->companie = $serviceOrder->companie;
                    $eventSecondary->save();

                    $order = service_order::find($new->id);
                    $order->id_status = 4;
                    $order->is_shared = 1;
                    $order->is_main = 0;
                    $saved = $order->save();

                    //send notification app technician
                    if ($saved) {
                        $employee = employee::find($techniciansAuxiliary);
                        $user = User::find($employee->employee_id);
                        $message = "Fecha: " . $request->get('dateStart') . " (" . $request->get('hourStart') . ") Se actualizo.";
                        $user->sendFCM('Nuevo Servicio', $message);
                    }

                    $employeeId = employee::find($techniciansAuxiliary)->id;
                    $shareServiceOrder = ShareServiceOrder::where('id_service_order_main', $serviceOrder->id)
                        ->where('id_employee',$employeeId)->first();
                    if (!$shareServiceOrder) $shareServiceOrder = new ShareServiceOrder();
                    $shareServiceOrder->id_service_order_main = $serviceOrder->id;
                    $shareServiceOrder->id_service_order = $new->id;
                    $shareServiceOrder->id_employee = $employeeId;
                    $shareServiceOrder->save();

                }
            }
            //if (Auth::user()->companie == 338) User::first()->notify(new LogsNotification("3", "", 3));

            $event->title = $request->get('title');
            $event->id_employee = $request->get('techniciansEvent');
            $event->initial_hour = $request->get('hourStart');
            $event->final_hour = $request->get('hourEnd');
            $event->initial_date = $request->get('dateStart');
            $event->final_date = $request->get('dateEnd');
            $event->id_status = 1;
            $event->save();
            //if (Auth::user()->companie == 338) User::first()->notify(new LogsNotification("4", "", 3));

            $orderMain = service_order::find($event->id_service_order);
            $orderMain->id_status = 4;
            if ($orderMain->is_main == 1 || $orderMain->is_main == 0 && $orderMain->is_shared == 0) {
                $orderMain->is_shared = $techniciansAuxiliaries != null ? 1 : 0;
                $orderMain->is_main = $techniciansAuxiliaries != null ? 1 : 0;
            }

            $orderMain->save();

            //send notification app technician
            if ($event) {
                $employee = employee::find($request->get('techniciansEvent'));
                $user = User::find($employee->employee_id);
                $message = "Fecha: " . $request->get('dateStart') . " (" . $request->get('hourStart') . ") Se actualizo.";
                $user->sendFCM('Nuevo Servicio', $message);
                //if (Auth::user()->companie == 338) User::first()->notify(new LogsNotification("5", "", 3));
            }
            return \Response::json([
                'code' => 200,
                'message' => 'Servicio actualizado.'
            ]);
        }catch (\Exception $exception) {
            return \Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

#region Funciones JSON

    /**
     * @param Integer $id
     * @return JsonResponse
     */
    public function getServicesByIdEmployee($id, $filter, $idStatus=0)
    {
        $events;

        if ($filter == 1) {

            $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'qo.id_plague_jer as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email', 'so.warranty', 'so.reinforcement', 'so.tracing')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->whereDate('ev.initial_date', '=', Carbon :: today () -> toDateString ())
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        }elseif ($filter == 2) {

            $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'qo.id_plague_jer as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email', 'so.warranty', 'so.reinforcement', 'so.tracing')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->whereDate('ev.initial_date', '=', Carbon :: tomorrow () -> toDateString ())
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        }elseif ($filter == 3) {

            $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'qo.id_plague_jer as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email', 'so.warranty', 'so.reinforcement', 'so.tracing')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        }elseif ($filter == 4 || $filter == 5) {

            $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'qo.id_customer', 'cd.customer_id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->select('ev.id', 'ev.title', 'ev.id_employee', 'ev.initial_hour', 'ev.final_hour', 'ev.initial_date', 'cu.name as customer', 'cu.cellphone',
                     'cu.colony', 'cu.establishment_name', 'so.total', 'cd.address as street', 'cd.address_number', 'qo.id_plague_jer as plague', 'em.color',
                      'so.id_service_order', 'so.id as idSo', 'so.observations', 'ev.id_status as status', 'u.name as user_schedule_service', 'cd.email', 'so.warranty', 'so.reinforcement', 'so.tracing')
            ->where('ev.id_employee', $id)
            ->where('ev.id_status', '!=', null)
            ->where('ev.id_status', $idStatus)
            ->orderBy('ev.initial_date', 'asc')
            ->orderBy('ev.initial_hour', 'asc')
            ->get();

        }

        foreach ($events as $event) {
            $events->map(function($event){
                $fecha = $event->initial_date;
                $hora = $event->initial_hour;
                $horaFinal = $event->final_hour;

                if($event->reinforcement == 1) $event->type = "Refuerzo";
                else if($event->warranty == 1) $event->type = "Garantía";
                else if($event->tracing == 1) $event->type = "Seguimiento";
                else $event->type = "";

                if ($event->plague === 1 || $event->plague === 2 || $event->plague === 3 || $event->plague === 4 || $event->plague === 5 || $event->plague === 6 || $event->plague === 7 || $event->plague === 8 ) {
                    $event->plague = 'MTTO';
                }elseif ($event->plague === 10 || $event->plague === 11 || $event->plague === 12 || $event->plague === 13) {
                    $event->plague = 'CH';
                }elseif ($event->plague === 9) {
                    $event->plague = 'CA';
                }elseif ($event->plague === 14) {
                    $event->plague = 'TER';
                }else{
                    $event->plague = $event->plague;
                }
                $event->address = $event->street . ' ' . $event->address_number . ', ' . $event->colony;
                $event->date = Carbon::parse($fecha)->format('M d');
                $event->hour = Carbon::parse($hora)->format('h:i a');
                $event->final_hour = Carbon::parse($horaFinal)->format('h:i a');
                $event->totalC = "$" . $event->total;

                if ($event->status == 1) {
                    $event->color = "#0288d1";
                    $event->etiqueta = "";
                }elseif ($event->status == 2) {
                    $event->color = "#f9a825";
                    $event->etiqueta = "Comenzado";
                }elseif ($event->status == 3) {
                    $event->color = "#DC2213";
                    $event->etiqueta = "Cancelado";
                }elseif ($event->status == 4) {
                    $event->color = "#176536";
                    $event->etiqueta = "Finalizado";
                }

                if ($event->title == null) {
                     $event->title = "";
                }

                if ($event->cellphone == null) {
                     $event->cellphone = "";
                }

                if ($event->colony == null) {
                     $event->colony = "";
                }

                if ($event->street == null) {
                     $event->street = "";
                }

                if ($event->observations == null) {
                     $event->observations = "";
                }

                if ($event->address == null) {
                     $event->address = "";
                }

                if ($event->establishment_name == null) {
                     $event->establishment_name = "";
                }

                if ($event->address_number == null) {
                     $event->address_number = "";
                }

                if ($event->email == null) {
                     $event->email = 0;
                 }

                $pregnancy = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',1)->first();
                $baby = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',2)->first();
                $children = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',3)->first();
                $adult = DB::table('habit_condition_service_order')->select('habit_condition_id')->where('service_order_id',$event->idSo)->where('habit_condition_id',4)->first();

                $respiratory = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',1)->first();
                $inmunologic = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',2)->first();
                $renal = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',3)->first();
                $cardiac = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',4)->first();
                $does_not = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',5)->first();
                $other = DB::table('inhabitant_type_service_order')->select('inhabitant_type_id','specification')->where('service_order_id',$event->idSo)->where('inhabitant_type_id',6)->first();

                $dog = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',1)->first();
                $cat = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',2)->first();
                $bird = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',3)->first();
                $fish = DB::table('mascot_service_order')->select('mascot_id')->where('service_order_id',$event->idSo)->where('mascot_id',4)->first();
                $other_mascot = DB::table('mascot_service_order')->select('mascot_id','specification')->where('service_order_id',$event->idSo)->where('mascot_id',5)->first();

                $cash = DB::table('cashes')->where('id_service_order', $event->idSo)->select('payment')->first();
                $event->etiqueta_payment = "";
                $event->etiqueta_color = "#000000";
                if (!empty($cash)) {
                    $event->payment = $cash->payment;
                    if ($event->payment == 1) {
                        $event->etiqueta_payment = "Adeudo";
                        $event->etiqueta_color = "#DC2213";
                    }else if ($event->payment == 2) {
                        $event->etiqueta_payment = "Pagado";
                        $event->etiqueta_color = "#176536";
                    }
                }else{
                    $event->payment = 0;
                }

                $conditions = "";
                $inhabitants = "";
                $mascots = "";

                //conditions
                if (!empty($pregnancy)) {
                    $conditions = "Embarazo";
                }

                if (!empty($baby)) {
                    $conditions = $conditions . " Bebés";
                }

                if (!empty($children)) {
                    $conditions = $conditions . " Niños";
                }

                if (!empty($adult)) {
                    $conditions = $conditions . " Adulto Mayor";
                }

                //inhabitants
                if (!empty($respiratory)) {
                    $inhabitants = "Respiratorias";
                }

                if (!empty($inmunologic)) {
                    $inhabitants = $inhabitants . " Inmunológicas";
                }

                if (!empty($renal)) {
                    $inhabitants = $inhabitants . " Renales";
                }

                if (!empty($cardiac)) {
                    $inhabitants = $inhabitants . " Cardiacas";
                }

                if (!empty($does_not)) {
                    $inhabitants = $inhabitants . " No aplica";
                }

                if (!empty($other)) {
                    $inhabitants = $inhabitants . " Otros";
                }

                //mascots
                if (!empty($dog)) {
                    $mascots = "Perros";
                }

                if (!empty($cat)) {
                    $mascots = $mascots . " Gatos";
                }

                if (!empty($bird)) {
                    $mascots = $mascots . " Aves";
                }

                if (!empty($fish)) {
                    $mascots = $mascots . " Peces";
                }

                if (!empty($other_mascot)) {
                    $mascots = $mascots . " Otros";
                }

                $event->conditions = $conditions;
                $event->inhabitants = $inhabitants;
                $event->mascots = $mascots;


            });
        }

        return response()->json($events);
    }

    public function indexSignature($id) {

        $id = SecurityController::decodeId($id);
        $event = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->select('so.id_service_order', 'ev.initial_date', 'ev.initial_hour', 'ev.final_hour', 'em.name')
            ->where('ev.id_service_order', $id)
            ->first();
        $serviceFirm = service_firms::where('id_service_order', $id)->first();
        $imageSignature = '';
        if ($serviceFirm) $imageSignature = CommonImage::getTemporaryUrl($serviceFirm->file_route, 5);
        else {
            $serviceFirm = new service_firms();
            $serviceFirm->id_service_order = $id;
            $serviceFirm->other_name = '';
            $serviceFirm->save();
        }

        return view('vendor.adminlte.auth.signature_service')
            ->with([
                'event' => $event,
                'serviceFirm' => $serviceFirm,
                'imageSignature' => $imageSignature
            ]);
    }

    public function signatureMip(Request $request){
        try {
            $idServiceOrder = SecurityController::decodeId($request->get('id'));
            $serviceOrder = service_order::find($idServiceOrder);
            $serviceFirm = service_firms::where('id_service_order', $serviceOrder->id)->first();

            // Guardar la imagen en el almacenamiento en S3
            if ($request->get('updateCanvas') == 1) {
                $image = $request->file('file');
                $folder = 'services/firms/';
                $name = $request->get('id');
                $serviceFirm->file_route = $folder . $name . '.jpg';
                Storage::disk('s3')->put($folder . $name . '.jpg', file_get_contents($image));
            }
            $serviceFirm->other_name = $request->get('name');
            $serviceFirm->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se guardo la firma.'
            ]);

        }
        catch (Exception $exception) {
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

#endregion
}
