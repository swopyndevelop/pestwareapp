<?php

namespace App\Http\Controllers\Services;

use App\address_job_center;
use App\AreaInspections;
use App\canceled_service_order;
use App\cash;
use App\cash_picture;
use App\Common\CommonImage;
use App\Common\CommonLabel;
use App\Common\Constants;
use App\companie;
use App\custom_quote;
use App\customer;
use App\customer_branche;
use App\customer_data;
use App\employee;
use App\event;
use App\habit_condition_service_order;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Customers\CommonCustomer;
use App\Http\Controllers\Customers\CustomerBranchesController;
use App\Http\Controllers\CustomServices\TechnicalCapacity;
use App\Http\Controllers\Quotations\CommonQuotation;
use App\Http\Controllers\ReportsPDF\AreaInspectionTable;
use App\Http\Controllers\ReportsPDF\ServiceCertificate;
use App\Http\Controllers\ReportsPDF\ServiceOrder;
use App\Http\Controllers\ReportsPDF\ServiceOrderTemplate;
use App\Http\Controllers\ReportsPDF\StationInspectionTable;
use App\Http\Controllers\Security\SecurityController;
use App\inhabitant_type_service_order;
use App\Invoice;
use App\InvoiceOrder;
use App\Library\FacturamaLibrary;
use App\Mail\ServiceMail;
use App\mascot_service_order;
use App\payment_method;
use App\payment_way;
use App\PriceList;
use App\profile_job_center;
use App\quotation;
use App\ratings_service_orders;
use App\service_order;
use App\StationInspection;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mail;
use PDF;
use PhpParser\Node\Stmt\If_;
use Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Storage;

/**
 * @author Alberto Martínez || Yomira Martínez
 * @version 22/03/2019
 * Controller Service, CRUD para módulo de programar servicio.
 */
class ServiceController extends Controller
{

    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Retorna la vista para programar servicio.
     */
    public function orderView($id)
    {
        $id = SecurityController::decodeId($id);
        $quotation_id = quotation::find($id);

        // Middleware to deny access to data from another company
        if ($quotation_id->companie != auth()->user()->companie) {
            SecurityController::abort();
        }

        $q = DB::table('quotations as q')
            ->join('customers as c', 'c.id', 'q.id_customer')
            ->join('establishment_types as e', 'e.id', 'c.establishment_id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('source_origins as so', 'c.source_origin_id', 'so.id')
            ->join('profile_job_centers as jc', 'jc.id', 'q.id_job_center')
            ->select('q.id', 'q.id_quotation', 'q.created_at as date', 'c.name', 'c.id as id_customer', 'cd.phone_number', 'e.id as e_id', 'e.name as e_name',
                'q.construction_measure', 'q.garden_measure', 'q.description', 'q.id_status', 'q.id_plague_jer', 'c.establishment_name', 'c.cellphone',
                'cd.address', 'cd.reference_address', 'cd.email', 'c.colony', 'c.municipality', 'q.total', 'q.price', 'q.messenger', 'qd.discount_id', 'd.title as discount_name', 'd.percentage', 'so.name as source_name', 'so.id as source_id',
                'cd.state', 'cd.contact_two_name', 'cd.contact_two_phone', 'cd.billing', 'cd.rfc','jc.iva','jc.rfc_country','q.id_job_center',
                'cd.tax_residence', 'cd.address_number', 'cd.email_billing', 'cd.billing_mode', 'cd.cfdi_type', 'cd.payment_method as payment_method_sat', 'cd.payment_way as payment_way_sat')
            ->where('q.id', $id)
            ->first();

        //Obtener espacio total a fumigar
        if ($q->garden_measure == null) {//Obtener el area total a fumigar
            $area = $q->construction_measure;
        } else {
            $area = $q->construction_measure + $q->garden_measure;
        }
        //Plagas
        $plague = DB::table('plague_type_quotation as qpt')
            ->join('plague_types as pt', 'pt.id', 'qpt.plague_type_id')
            ->select('pt.name', 'pt.id as plague')
            ->where('qpt.quotation_id', $id)->get();
        $plague2 = collect($plague)->toArray();//Generar array con todas las plagas seleccionadas
        //Descuentos
        $d = DB::table('discounts')->select('percentage')->where('id', $q->discount_id)->first();
        $d1 = $d->percentage / 100;
        $discount = $q->price * $d1;
        $iva_profile_job_center = $q->iva / 100;

        //Calculo del iva
        $iva = $q->total * $iva_profile_job_center;

        //Metodos de Pago
        $method = DB::table('payment_methods')->select('id', 'name')->where('profile_job_center_id', $q->id_job_center)->get();
        //Forma de Pago
        $way = DB::table('payment_ways')->where('profile_job_center_id', $q->id_job_center)->select('id', 'name')->get();
        $profile_job_center = profile_job_center::find($q->id_job_center);
        $address_job_center = address_job_center::where('address_job_centers_id', $profile_job_center->profile_job_centers_id)->first();
        $symbol_country = CommonCompany::getSymbolByCountry();
        $commonLabels = new CommonLabel();
        $labels = $commonLabels->getLabelsByJobCenter($q->id_job_center);

        //Catalogs SAT
        $typesCfdi = FacturamaLibrary::getCatalogsCfdiUses();
        $paymentMethodsSat = FacturamaLibrary::getCatalogsPaymentMethods();
        $paymentFormsSat = FacturamaLibrary::getCatalogsPaymentForms();

        return view('vendor.adminlte.register._schedule')->with([
            'q' => $q,
            'discount' => $discount,
            'address_job_center' => $address_job_center,
            'plague2' => $plague2,
            'area' => $area,
            'iva' => $iva,
            'method' => $method,
            'way' => $way,
            'labels' => $labels,
            'plagues' => $plague,
            'symbol_country' => $symbol_country,
            'typesCfdi' => $typesCfdi,
            'paymentMethodsSat' => $paymentMethodsSat,
            'paymentFormsSat' => $paymentFormsSat]);
    }

    public function orderViewCustom($id) {

        //$id = SecurityController::decodeId($id);
        $test = new TechnicalCapacity($id);
        $quotation = quotation::find($id);
        $customQuotations = custom_quote::join('type_services as ts', 'custom_quotes.id_type_service', 'ts.id')
            ->where('id_quotation', $id)
            ->where('id_type_concepts', '<>', 2)
            ->select('custom_quotes.*', 'ts.name as type_service')
            ->get();
        $paymentMethods = payment_method::where('profile_job_center_id', $quotation->id_job_center)->get();
        $paymentWays = payment_way::where('profile_job_center_id', $quotation->id_job_center)->get();
        $employees = $test->searchActiveTechniciansByJobCenter();
        $commonLabels = new CommonLabel();
        $labels = $commonLabels->getLabelsByJobCenter($quotation->id_job_center);

        // Middleware to deny access to data from another company
        //if ($quotation->companie != auth()->user()->companie) {
            //SecurityController::abort();
        //}

        if ($quotation && $customQuotations) {
            $totalServices = 0;
            foreach ($customQuotations as $concept) {
                $totalServices += ($concept->quantity * $concept->frecuency_month) * $concept->term_month;
            }
            $customerBranches = customer_branche::where('customer_id', $quotation->id_customer)->where('id_quotation', $quotation->id)->get();

            $serviceOrders = DB::table('service_orders as so')
                ->join('events as e', 'e.id_service_order', 'so.id')
                ->join('employees as emp', 'e.id_employee', 'emp.id')
                ->join('customer_branches as cb', 'so.customer_branch_id', 'cb.id')
                ->select('so.*', 'cb.name as branche', 'e.initial_date', 'e.initial_hour', 'e.final_hour',
                    'emp.name as employee', 'e.final_date', 'e.id_employee', 'e.id as event_id')
                ->where('so.id_quotation', $id)
                ->get();
            $countOrders = $serviceOrders->count();

            return view('vendor.adminlte.register._modalScheduleCustomQuotation')
                ->with(['customerId' => $quotation->id_customer, 'branches' => $customerBranches, 'labels' => $labels,
                    'services' => $serviceOrders, 'totalServices' => $totalServices, 'quotationId' => $id,
                    'paymentMethods' => $paymentMethods, 'employees' => $employees, 'countOrders' => $countOrders,
                    'paymentWays' => $paymentWays, 'countBranches' => CustomerBranchesController::getCountBranchesCustom($id),
                    'concepts' => $customQuotations, 'quotationKey' => $quotation->id_quotation]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Agregar todos los datos de la orden de servicio
     */
    public function orderStore(Request $request)
    {
        try {

            DB::beginTransaction();
            $quot = quotation::find($request->q_id);
            $profileJobCenter = profile_job_center::find($quot->id_job_center);
            $folio = explode('-', $quot->id_quotation);
            $folio = $folio[1];
            $total = $quot->total;
            //Query id del cliente ---> Completar datos de la tabla customers
            $customer = customer::find($request->customer_id);
            $customer->name = $request->customerName;
            $customer->establishment_name = $request->establishmentName;
            $customer->cellphone = $request->cellphone;
            $customer->colony = $request->colony;
            $customer->municipality = $request->municipality;
            $customer->save();
            //Customer_data table
            $customer_data = DB::table('customer_datas')->select('id')->where('customer_id', $request->customer_id)->first();
            $datas = customer_data::find($customer_data->id);
            $datas->address = $request->address;
            $datas->contact_two_name = $request->contact;
            $datas->contact_two_phone = $request->cellphone_two;
            $datas->address_number = $request->number;
            $datas->state = $request->state;
            $datas->payment_method = $request->get('methodPayCustomerMainNew');
            $datas->payment_way = $request->get('wayPayCustomerMainNew');
            $datas->cfdi_type = $request->get('typeCfdiCustomerMainNew');
            $datas->email_billing = $request->get('e_mail');
            if ($request->habilitar == 1) {
                $datas->rfc = $request->rfc;
                $datas->billing = $request->social;
                $datas->tax_residence = $request->address_rfc;
                // Calculate iva
                $iva = $total * ($profileJobCenter->iva / 100);
                $total = $total + $iva;
            }
            $datas->save();
            //Añadir Orden de servicio
            $service = new service_order;
            $service->id_service_order = 'OS-' . $folio;
            $service->id_quotation = $request->q_id;
            $service->id_payment_method = $request->pay_m;
            $service->id_payment_way = $request->pay_f;
            $service->bussiness_name = $request->establishmentName;
            $service->address = $request->address_rfc;
            $service->email = $request->e_mail;
            $service->observations = $request->comments;
            $service->total = $total;
            $service->subtotal = $quot->total;
            $service->user_id = Auth::id();
            $quotationJobCenter = quotation::find($request->q_id);
            $service->id_job_center = $quotationJobCenter->id_job_center;
            $service->companie = Auth::user()->companie;
            $service->id_status = 10;
            $service->save();

            //Cambiar estatus de Cotizacion
            $quotation = quotation::find($request->q_id);
            $quotation->id_status = 4;
            $quotation->save();
            //Habit Conditions, Inhabitant, Mascot Sevices
            //Embarazo
            if ($request->pregnancy != null) {
                $h = new habit_condition_service_order;
                $h->service_order_id = $service->id;
                $h->habit_condition_id = $request->pregnancy;
                $h->save();
            }
            //Bebes
            if ($request->baby != null) {
                $h2 = new habit_condition_service_order;
                $h2->service_order_id = $service->id;
                $h2->habit_condition_id = $request->baby;
                $h2->save();
            }
            //Niños
            if ($request->children != null) {
                $h3 = new habit_condition_service_order;
                $h3->service_order_id = $service->id;
                $h3->habit_condition_id = $request->children;
                $h3->save();
            }
            //Adulto Mayor
            if ($request->adult != null) {
                $h4 = new habit_condition_service_order;
                $h4->service_order_id = $service->id;
                $h4->habit_condition_id = $request->adult;
                $h4->save();
            }
            //Enfermedades Respiratorias
            if ($request->respiratory != null) {
                $inh = new inhabitant_type_service_order;
                $inh->service_order_id = $service->id;
                $inh->inhabitant_type_id = $request->respiratory;
                $inh->save();
            }
            //Enfermedades Inmunologicas
            if ($request->inmunologic != null) {
                $inh2 = new inhabitant_type_service_order;
                $inh2->service_order_id = $service->id;
                $inh2->inhabitant_type_id = $request->inmunologic;
                $inh2->save();
            }
            //Enfermedades Renales
            if ($request->renal != null) {
                $inh3 = new inhabitant_type_service_order;
                $inh3->service_order_id = $service->id;
                $inh3->inhabitant_type_id = $request->renal;
                $inh3->save();
            }
            //Enfermedades Cardiacas
            if ($request->cardiac != null) {
                $inh4 = new inhabitant_type_service_order;
                $inh4->service_order_id = $service->id;
                $inh4->inhabitant_type_id = $request->cardiac;
                $inh4->save();
            }
            //No Aplica
            if ($request->does_not != null) {
                $inh5 = new inhabitant_type_service_order;
                $inh5->service_order_id = $service->id;
                $inh5->inhabitant_type_id = $request->does_not;
                $inh5->save();
            }
            //Otro
            if ($request->other != null) {
                $inh6 = new inhabitant_type_service_order;
                $inh6->service_order_id = $service->id;
                $inh6->inhabitant_type_id = $request->other;
                $inh6->specification = $request->specific;
                $inh6->save();
            }
            //Perro
            if ($request->dog != null) {
                $m = new mascot_service_order;
                $m->service_order_id = $service->id;
                $m->mascot_id = $request->dog;
                $m->save();
            }
            //Gato
            if ($request->cat != null) {
                $m2 = new mascot_service_order;
                $m2->service_order_id = $service->id;
                $m2->mascot_id = $request->cat;
                $m2->save();
            }
            //Ave
            if ($request->bird != null) {
                $m3 = new mascot_service_order;
                $m3->service_order_id = $service->id;
                $m3->mascot_id = $request->bird;
                $m3->save();
            }
            //Pez
            if ($request->fish != null) {
                $m4 = new mascot_service_order;
                $m4->service_order_id = $service->id;
                $m4->mascot_id = $request->fish;
                $m4->save();
            }
            //Otro
            if ($request->other_mascot != null) {
                $m5 = new mascot_service_order;
                $m5->service_order_id = $service->id;
                $m5->mascot_id = $request->other_mascot;
                $m5->specification = $request->mascot;
                $m5->save();
            }

            // Create account user
            if (Auth::user()->id_plan == $this->PLAN_BUSINESS) {
                $priceListIsCustomerPortal = PriceList::find($quotation->id_price_list);
                if ($priceListIsCustomerPortal->customer_portal == 1) {
                    $customerUser = customer::find($request->customer_id);
                    $nameUser = $customerUser->name;
                    $email = $request->get('email');
                    if ($customerUser->user_id == null) {
                        //Create new user
                        $this->createUser($nameUser, $email, $request->customer_id, $customerUser->cellphone);
                    }
                }
            }

            $idEmployee = DB::table('employees')->where('employee_id', Auth::id())->first()->id;

            $event = new event;
            $event->title = 'Fumigación';
            $event->id_service_order = $service->id;
            $event->id_employee = $idEmployee;
            $event->id_job_center = $service->id_job_center;
            $event->companie = Auth::user()->companie;
            $event->save();

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Error al Guardar, Intente de Nuevo'
            ]);
        }
    }

    /**
     * @param $customer
     * @param $email
     * @param $customerId
     * @param $cellphone
     */
    public function createUser($customer, $email, $customerId, $cellphone)
    {
        // generate random 10 char password from below chars
        //$random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        //$password = substr($random, 0, 10);
        $password = "PestWareApp2021@";
        $passwordEncrypt = bcrypt($password);

        $user = User::create([
            'name' => $customer,
            'email' => $email,
            'cellphone' => $cellphone,
            'companie' => Auth::user()->companie,
            'password' => $passwordEncrypt
        ]);

        if ($user) {
            $rol = DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => 3 //Clientes del portal
            ]);

            $cust = customer::find($customerId);
            $cust->user_id = $user->id;
            $cust->save();

            // Add default documents for portal MIP
            CommonCustomer::createDefaultDocuments($customerId);
        }

    }

    public function deleteview($id)
    {
        $id = SecurityController::decodeId($id);
        $order = service_order::find($id);

        // Middleware to deny access to data from another company
        if ($order->companie != Auth::user()->companie) {
            SecurityController::abort();
        }

        return view('vendor.adminlte.register._cancelschedule')->with(['order' => $order]);
    }

    public function orderRecover($id)
    {

        $idEvent = DB::table('events')->where('id_service_order', $id)->first()->id;

        $order = service_order::find($id);
        $order->id_status = 4;
        $order->save();

        $event = event::find($idEvent);
        $event->id_status = 1;
        $event->save();

        return redirect()->route('calendario');
    }

    public function delete(Request $request)
    {
        try {

        $idOrder = $request->id_order;
        $idEvent = DB::table('events')->where('id_service_order', $idOrder)->first()->id;

        $deny = new canceled_service_order;
        $deny->id_service_order = $idOrder;
        $deny->reason = $request->reason_o;
        $deny->commentary = $request->comments_o;
        $deny->date = Carbon::now()->toDateString();
        $deny->save();

        //Cambiar status
        $order = service_order::find($idOrder);
        $order->id_status = 2;
        $order->save();

        $event = event::find($idEvent);
        $event->id_status = 3;
        $event->save();

        $serviceOrder = service_order::where('id', $idOrder)->first();
        $quotation = quotation::find($serviceOrder->id_quotation);

        $historicalCustomer = new \App\HistoricalCustomer();
        $historicalCustomer->id_customer = $quotation->id_customer;
        $historicalCustomer->id_user = Auth::user()->id;
        $historicalCustomer->title = 'Cancelación (Orden De Servicio): ' . $serviceOrder->id_service_order;
        $historicalCustomer->commentary = 'Razon: ' . $deny->reason . ' ' . $deny->commentary;
        $historicalCustomer->date = Carbon::now()->toDateString();
        $historicalCustomer->hour = Carbon::now()->toTimeString();
        $historicalCustomer->save();

            return \Response::json([
                'code' => 200,
                'message' => 'Servicio Cancelado.'
            ]);
        }
        catch (\Exception $ex) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo'
            ]);
        }

    }

    public function PDF($id)
    {
        if (strlen($id) > 5) $id = SecurityController::decodeId($id);

        // Build service certificate and get data.
        $dataCertificate = ServiceCertificate::build($id);
        $order = $dataCertificate['order'];
        $company = $order->compania;
        $year = substr($dataCertificate['order']->initial_date, 0,4);

        if ($company == 269)$pdf = PDF::loadView('vendor.adminlte.register.INFORM.informCustom', $dataCertificate)->setPaper('A4');
        else $pdf = PDF::loadView('vendor.adminlte.register.INFORM.inform', $dataCertificate)->setPaper('A4');

        $routeServices = 'app/customer_portal/mip/' . $dataCertificate['order']->customer_id . '/' . $year . '/services/';
        $folderLocal = storage_path($routeServices);
        if (!file_exists($folderLocal)) mkdir($folderLocal, 0755, true);
        $routeDestiny = storage_path($routeServices . $dataCertificate['order']->id_service_order . '.pdf'); // Ruta completa de destino en la carpeta local
        file_put_contents($routeDestiny, $pdf->output());

        return $pdf->stream('Certificado de Servicio' . ' ' . $order->id_service_order . '.pdf');
    }

    public function orderServicePDF($id)
    {
        if (strlen($id) > 5) $id = SecurityController::decodeId($id);

        $dataServiceOrder = ServiceOrder::build($id);
        $order = $dataServiceOrder['order'];
        $year = substr($dataServiceOrder['order']->initial_date, 0,4);
        $pdf = PDF::loadView('vendor.adminlte.register.INFORM.orderService', $dataServiceOrder)->setPaper('A4');

        $routeServices = 'app/customer_portal/mip/' . $dataServiceOrder['order']->customer_id . '/' . $year . '/orders/';
        $folderLocal = storage_path($routeServices);
        if (!file_exists($folderLocal)) mkdir($folderLocal, 0755, true);
        $routeDestiny = storage_path($routeServices . $dataServiceOrder['order']->id_service_order . '.pdf'); // Ruta completa de destino en la carpeta local
        file_put_contents($routeDestiny, $pdf->output());

        return $pdf->stream('Orden de Servicio' . ' ' . $order->id_service_order . '.pdf');
    }

    public function mail(Request $request)
    {
        try {
            $id = $request->get('id');
            // Files to send
            $fileOrder = $request->get('fileOrder');
            $fileCertificate = $request->get('fileCertificate');
            $fileStation = $request->get('fileStation');
            $fileArea = $request->get('fileArea');
            $pdfInvoice = null;
            $xmlInvoice = null;

            // Build service certificate and get data.
            $dataCertificate = ServiceCertificate::build($id);
            $order = $dataCertificate['order'];
            $company = $order->compania;
            $jobCenterProfile = profile_job_center::where('id', $order->id_job_center)->first();

            // Certificate Service
            $pdf = null;
            if ($fileCertificate == "true") {
                if ($company == 269) {
                    $pdf1 = PDF::loadView('vendor.adminlte.register.INFORM.informCustom', $dataCertificate)->setPaper('A4');
                } else {
                    $pdf1 = PDF::loadView('vendor.adminlte.register.INFORM.inform', $dataCertificate)->setPaper('A4');
                }
                $pdf = $pdf1->output();
            }

            // Build service order and get data
            $pdfOrder = null;
            if ($fileOrder == "true") {
                $dataServiceOrder = ServiceOrder::build($id);
                $pdfOrderView = PDF::loadView('vendor.adminlte.register.INFORM.orderService', $dataServiceOrder)->setPaper('A4');
                $pdfOrder = $pdfOrderView->output();
            }

            // Generate pdf report Monitoring.
            $pdfMonitoring = null;
            if ($fileStation == "true") {
                $isInspection = StationInspection::where('id_service_order', $id)->first();
                // Generate pdf report Station Inspection Table.
                if ($isInspection) {
                    $dataStationInspection = StationInspectionTable::build($id, $order->compania);
                    $pdfInspection = PDF::loadView(
                        'vendor.adminlte.stationmonitoring._stationMonitoringPdf', $dataStationInspection
                    )->setPaper('A4');
                    $pdfMonitoring = $pdfInspection->output();
                }
            }

            // Generate Inspection Areas
            $pdfArea = null;
            if ($fileArea == "true") {
                $isAreaInspection = AreaInspections::where('id_service_order', $id)->first();
                if ($isAreaInspection) {
                    $dataAreaInspection = AreaInspectionTable::build($id);
                    $pdfAreaInspection = PDF::loadView('vendor.adminlte.areamonitoring._areaInspectionPdf', $dataAreaInspection)
                        ->setPaper('A4');
                    $pdfArea = $pdfAreaInspection->output();
                }
            }

            $comp = companie::find($company);
            $email = $request->get('email');
            $emailInput = $request->get('emailInput');

            if ($email != '0') {
                $user = $email;
                config(['mail.from.name' => $comp->name]);
                $result = filter_var($user, FILTER_VALIDATE_EMAIL);
                if ($result != false) {
                    Mail::to($user)->send(new ServiceMail($user, $id, $pdf, $pdfOrder, $pdfMonitoring, $pdfArea, $pdfInvoice, $xmlInvoice, $company, $jobCenterProfile->id));
                }
            }

            if ($emailInput != '0') {
                $user = $emailInput;
                config(['mail.from.name' => $comp->name]);
                $result = filter_var($user, FILTER_VALIDATE_EMAIL);
                if ($result != false) {
                    Mail::to($user)->send(new ServiceMail($user, $id, $pdf, $pdfOrder, $pdfMonitoring, $pdfArea,$pdfInvoice, $xmlInvoice, $company, $jobCenterProfile->id));
                }
            }

            return \Response::json([
                'code' => 200,
                'message' => 'Se enviaron correctamente los Documentos.'
            ]);
        }
        catch (\Exception $ex) {
            return response()->json([
                'code' => 500,
                'data' => $ex->getMessage(), //'Algo salio mal, intenta de nuevo'
                'bug' => $ex->getLine()
            ]);
        }
    }

    public function orderServiceTemplate($id){
        if (strlen($id) > 5) $id = SecurityController::decodeId($id);
        $dataServiceOrder = ServiceOrderTemplate::build($id);
        $order = $dataServiceOrder['order'];

        //Validate Company Custom Order Service
        $company = Auth::user()->companie;
        if ($company == 269) {
            $pdf = PDF::loadView('vendor.adminlte.register.INFORM.orderServiceTemplateCustom', $dataServiceOrder)->setPaper('A4');
        } else {
            $pdf = PDF::loadView('vendor.adminlte.register.INFORM.orderServiceTemplate', $dataServiceOrder)->setPaper('A4');
        }

        return $pdf->stream('Servicio' . ' ' . $order->id_service_order . '.pdf');
    }

    public function orderServiceTemplateOS($id){
        if (strlen($id) > 5) $id = SecurityController::decodeId($id);
        $dataServiceOrder = ServiceOrderTemplate::build($id);
        $order = $dataServiceOrder['order'];
        $pdf = PDF::loadView('vendor.adminlte.register.INFORM.orderServiceTemplateOS', $dataServiceOrder)->setPaper('A4');
        return $pdf->stream('Servicio' . ' ' . $order->id_service_order . '.pdf');
    }

    public function createWarranty($idOrder)
    {
        try {
            $order = service_order::find($idOrder);
            $order->warranty = 1;
            $order->save();

            $quote = quotation::find($order->id_quotation);

            $folio = explode('-', $quote->id_quotation);
            $folio = $folio[1];

            $eventOld = event::where('id_service_order', $idOrder)->first();

            $warranty = new service_order;
            $warranty->id_service_order = 'G-' . $folio;
            $warranty->id_quotation = $order->id_quotation;
            $warranty->id_payment_method = $order->id_payment_method;
            $warranty->id_payment_way = $order->id_payment_way;
            $warranty->bussiness_name = $order->bussiness_name;
            $warranty->address = $order->address;
            $warranty->email = $order->email;
            $warranty->observations = $order->observations;
            $warranty->total = 0;
            $warranty->user_id = Auth::id();
            $warranty->id_job_center = $order->id_job_center;
            $warranty->companie = Auth::user()->companie;
            $warranty->id_status = 10;
            $warranty->warranty = 1;
            $warranty->save();

            $event = new event;
            $event->title = 'Garantía';
            $event->id_service_order = $warranty->id;
            $event->id_employee = $eventOld->id_employee;
            $event->id_job_center = $warranty->id_job_center;
            $event->companie = Auth::user()->companie;
            $event->save();

            return \Response::json([
                'code' => 200,
                'message' => 'Servicio actualizado.'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'code' => 500,
                'data' => 'Algo salio mal, intenta de nuevo'
            ]);
        }

    }

    public function createReinforcement($idOrder)
    {
        try {
            $order = service_order::find($idOrder);
            $order->reinforcement = 1;
            $order->save();

            $quote = quotation::find($order->id_quotation);

            $folio = explode('-', $quote->id_quotation);
            $folio = $folio[1];

            $quotation = DB::table('quotations')->where('id', $order->id_quotation)->first();
            $eventOld = event::where('id_service_order', $idOrder)->first();
            $priceTwo = DB::table('prices')
                ->where('price_list_id', $quotation->id_price_list)
                ->where('area', '<=', $quotation->construction_measure)
                ->orderBy('area', 'desc')
                ->first();

            $price = $priceTwo->price_two;
            $reinforcement = new service_order;
            $reinforcement->id_service_order = 'R-' . $folio;
            $reinforcement->id_quotation = $order->id_quotation;
            $reinforcement->id_payment_method = $order->id_payment_method;
            $reinforcement->id_payment_way = $order->id_payment_way;
            $reinforcement->bussiness_name = $order->bussiness_name;
            $reinforcement->address = $order->address;
            $reinforcement->email = $order->email;
            $reinforcement->observations = $order->observations;
            if ($quotation->price_reinforcement == null) {
                $reinforcement->total = $price;
            } else {
                $reinforcement->total = $quotation->price_reinforcement + $price;
            }
            $reinforcement->user_id = Auth::id();
            $reinforcement->id_job_center = $order->id_job_center;
            $reinforcement->companie = Auth::user()->companie;
            $reinforcement->id_status = 10;
            $reinforcement->reinforcement = 1;
            $reinforcement->save();

            $event = new event;
            $event->title = 'Refuerzo';
            $event->id_service_order = $reinforcement->id;
            $event->id_employee = $eventOld->id_employee;
            $event->id_job_center = $reinforcement->id_job_center;
            $event->companie = Auth::user()->companie;
            $event->save();

            return \Response::json([
                'code' => 200,
                'message' => 'Servicio actualizado.'
            ]);
        }
         catch (\Exception $ex) {
            return response()->json([
                'code' => 500,
                'data' => 'Algo salio mal, intenta de nuevo'
            ]);
        }
    }

    public function createTracing($idOrder)
    {
        try {
        $order = service_order::find($idOrder);
        $order->tracing = $order->tracing++;
        $order->save();

        $quote = quotation::find($order->id_quotation);

        $folio = explode('-', $quote->id_quotation);
        $lastFolio = $folio[1];

        $folio = CommonQuotation::getFolioTracing($order->id_quotation, $lastFolio);

        $eventOld = event::where('id_service_order', $idOrder)->first();

        $tracing = new service_order;
        $tracing->id_service_order = $folio;
        $tracing->id_quotation = $order->id_quotation;
        $tracing->id_payment_method = $order->id_payment_method;
        $tracing->id_payment_way = $order->id_payment_way;
        $tracing->bussiness_name = $order->bussiness_name;
        $tracing->address = $order->address;
        $tracing->email = $order->email;
        $tracing->observations = $order->observations;
        $tracing->total = 0;
        $tracing->user_id = Auth::id();
        $tracing->id_job_center = $order->id_job_center;
        $tracing->companie = Auth::user()->companie;
        $tracing->id_status = 10;
        $tracing->tracing = 1;
        $tracing->save();

        $event = new event;
        $event->title = 'Seguimiento';
        $event->id_service_order = $tracing->id;
        $event->id_employee = $eventOld->id_employee;
        $event->id_job_center = $tracing->id_job_center;
        $event->companie = Auth::user()->companie;
        $event->save();

        return \Response::json([
            'code' => 200,
            'message' => 'Servicio actualizado.'
        ]);
            }
        catch (\Exception $ex) {
            return response()->json([
                'code' => 500,
                'data' => 'Algo salio mal, intenta de nuevo'
            ]);
        }
    }

    public function ratingService(Request $request)
    {
        try {
            $event = event::where('id_service_order', $request->idOrder)->first();
            $rating = new ratings_service_orders;
            $rating->user_id = Auth::id();
            $rating->id_service_order = $request->idOrder;
            $rating->id_event = $event->id;
            $rating->rating = $request->raiting;
            $rating->comments = $request->comments;
            $rating->save();

            $serviceOrder = service_order::where('id', $request->get('idOrder'))->first();
            $quotation = quotation::find($serviceOrder->id_quotation);
            $historicalCustomer = new \App\HistoricalCustomer();
            $historicalCustomer->id_customer = $quotation->id_customer;
            $historicalCustomer->id_user = $rating->user_id;
            $historicalCustomer->title = 'Evaluación (Orden De Servicio): ' . $serviceOrder->id_service_order;
            $historicalCustomer->commentary = 'Calificación: ' . $rating->rating . 'estrallas. ' . $rating->comments;
            $historicalCustomer->date = Carbon::now()->toDateString();
            $historicalCustomer->hour = Carbon::now()->toTimeString();
            $historicalCustomer->save();

            return \Response::json([
                'code' => 200,
                'message' => 'Servicio actualizado.'
            ]);
        }
        catch (\Exception $ex) {
            return response()->json([
            'code' => 500,
            'data' =>  'Algo salio mal, intenta de nuevo'
            ]);
        }
    }

    public function orderViewModal($idOrder)
    {
        //Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            return redirect()->route('calendario');
        }

        $idOrder = SecurityController::decodeId($idOrder);

        //Extraer todos los datos del evento correspondiente
        $order = DB::table('events as e')->join('employees as em', 'e.id_employee', 'em.id')
            ->join('profile_job_centers as pjc', 'e.id_job_center', 'pjc.id')
            ->join('service_orders as so', 'e.id_service_order', 'so.id')
            ->join('payment_methods as pm', 'so.id_payment_method', 'pm.id')
            ->join('payment_ways as pw', 'so.id_payment_way', 'pw.id')
            ->join('users as u', 'so.user_id', 'u.id')
            ->join('statuses as st', 'so.id_status', 'st.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('establishment_types as et', 'q.establishment_id', 'et.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            ->select('so.id as order', 'so.id_service_order', 'so.created_at as date', 'so.observations', 'e.initial_hour', 'e.initial_date', 'em.name as nombre', 'e.start_event', 'e.final_event',
                'c.name as cliente', 'c.establishment_name as empresa', 'c.cellphone', 'cd.address', 'c.municipality',
                'q.id_plague_jer', 'q.total', 'pt.plague_key', 'u.name as agente', 'st.name as status', 'cd.address_number', 'cd.state', 'cd.email', 'e.id_status',
                'e.id as event', 'c.colony', 'q.id as quotation', 'e.final_hour', 'e.final_date', 'so.address as a_rfc',
                'so.email as e_rfc', 'so.bussiness_name', 'so.observations', 'cd.billing', 'd.id as discount',
                'd.percentage', 'ex.id as extra', 'ex.amount', 'q.construction_measure', 'q.garden_measure', 'q.price',
                'q.establishment_id as e_id', 'pm.id as pm_id', 'pw.id as pw_id', 'pm.name as metodo', 'pw.name as tipo',
                'cd.email', 'so.companie as compania', 'et.name as establecimiento', 'so.total as total_service')
            ->where('so.id', $idOrder)->first();
        $imagen = DB::table('companies')->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook')->where('id', $order->compania)->first();


        //Grado de Infestacion de Plagas
        $plagasGrade = DB::table('place_inspection_plague_type as pt')
            ->join('place_inspections as pi', 'pt.place_inspection_id', 'pi.id')
            ->join('plague_types as pl', 'pt.plague_type_id', 'pl.id')
            ->join('infestation_degrees as ie', 'pt.id_infestation_degree', 'ie.id')
            ->select('pl.name as plaga', 'ie.name as infestacion')->where('pi.id_service_order', $idOrder)->get();

        //Habit Conditions
        $h = DB::table('habit_condition_service_order as hc')
            ->join('habit_conditions as  h', 'hc.habit_condition_id', 'h.id')
            ->select('h.id', 'h.condition', 'hc.service_order_id', 'hc.habit_condition_id')
            ->where('hc.service_order_id', $idOrder)->get();
        $habit = collect($h)->toArray();

        //Inhabit Conditions
        $inh = DB::table('inhabitant_type_service_order as it')
            ->join('inhabitant_types as  ih', 'it.inhabitant_type_id', 'ih.id')
            ->select('ih.id', 'ih.name', 'it.service_order_id', 'it.inhabitant_type_id',
                'it.specification')
            ->where('it.service_order_id', $idOrder)->get();
        $inhabit = collect($inh)->toArray();

        $m = DB::table('mascot_service_order as ms')
            ->join('mascots as  m', 'ms.mascot_id', 'm.id')
            ->select('m.id', 'm.name', 'ms.service_order_id', 'ms.mascot_id',
                'ms.specification')
            ->where('ms.service_order_id', $idOrder)->get();
        $mascot = collect($m)->toArray();

        $idPlaceInspection = DB::table('place_inspections')->where('id_service_order', $idOrder)->first();
        $images = DB::table('inspection_pictures')->where('place_inspection_id', $idPlaceInspection->id)->select('id', 'file_route')->get();
        $inspection = DB::table('place_inspections')->where('id_service_order', $idOrder)->first();

        foreach ($images as $image) {
            $images->map(function ($image) {
                $image->url_s3 = CommonImage::getTemporaryUrl($image->file_route, 5);
            });
        }

        $idPlaceCondition = DB::table('place_conditions')->where('id_service_order', $idOrder)->first();
        $imagesCon = DB::table('condition_pictures')->where('place_condition_id', $idPlaceCondition->id)->select('id', 'file_route')->get();
        $condition = DB::table('place_conditions')->where('id_service_order', $idOrder)->first();
        $order_cleanings = DB::table('order_cleaning_place_condition as co')
            ->join('place_conditions as pc', 'co.place_condition_id', 'pc.id')
            ->join('order_cleanings as o', 'co.order_cleaning_id', 'o.id')
            ->select('co.place_condition_id', 'o.name as cleaning')->where('pc.id_service_order', $idOrder)->get();

        foreach ($imagesCon as $image) {
            $imagesCon->map(function ($image) {
                $image->url_s3 = CommonImage::getTemporaryUrl($image->file_route, 5);
            });
        }

        $idPlaceControl = DB::table('plague_controls')->where('id_service_order', $idOrder)->first();

        $plague_controls = DB::table('plague_controls')->where('id_service_order', $idOrder)->select('control_areas', 'commentary', 'id')->get();

        foreach ($plague_controls as $pg) {
            $plague_controls->map(function ($pg) {
                $methods_application = DB::table('plague_controls_application_methods as pcam')
                    ->join('application_methods as am', 'pcam.id_application_method', 'am.id')
                    ->select('am.name as method')
                    ->where('plague_control_id', $pg->id)->get();
                $methods = "";
                foreach ($methods_application as $ma) {
                    $methods = $methods . $ma->method . ", ";
                }
                $pg->methods = $methods;
                $plague_controls_products = DB::table('plague_controls_products as pcp')
                    ->join('products as p', 'pcp.id_product', 'p.id')
                    ->where('pcp.plague_control_id', $pg->id)
                    ->select('p.name as product', 'pcp.dose', 'pcp.quantity')
                    ->get();
                $pg->plague_controls = $plague_controls_products;
                $imagesCtrlPlague = DB::table('plague_control_pictures')->where('plague_control_id', $pg->id)->select('id', 'file_route')->get();
                foreach ($imagesCtrlPlague as $image) {
                    $imagesCtrlPlague->map(function ($image) {
                        $image->url_s3 = CommonImage::getTemporaryUrl($image->file_route, 5);
                    });
                }
                $pg->imagesCtrlPlague = $imagesCtrlPlague;
            });
        }
        $plague = DB::table('plague_type_quotation as qpt')->join('plague_types as pt', 'pt.id', 'qpt.plague_type_id')
            ->select('pt.name', 'pt.id as plague')->where('qpt.quotation_id', $order->quotation)->get();
        $plague2 = collect($plague)->toArray();

        $cashe = DB::table('cashes as c')
            ->join('payment_methods as pm', 'c.id_payment_method', 'pm.id')
            ->join('payment_ways as pw', 'c.id_payment_way', 'pw.id')
            ->select('pm.name as payment_method', 'pw.name as payment_type', 'c.amount_received', 'c.commentary', 'c.payment', 'c.id')
            ->where('c.id_service_order', $idOrder)
            ->first();

        $isInspections = DB::table('station_inspections')->where('id_service_order', $idOrder)->first();
        $imagesMonitoring = null;
        if($isInspections)
        {
            $imagesMonitoring = DB::table('check_list_images')->where('id_inspection', $isInspections->id)->get();
            foreach ($imagesMonitoring as $image) {
                $imagesMonitoring->map(function ($image) {
                    $image->url_s3 = CommonImage::getTemporaryUrl($image->urlImage, 5);
                });
            }
        }

        $imagesCashe = DB::table('cash_pictures')->where('cash_id', $cashe->id)->get();
        foreach ($imagesCashe as $image) {
            $imagesCashe->map(function ($image) {
                $image->url_s3 = CommonImage::getTemporaryUrl($image->file_route, 5);
            });
        }
        $firm = DB::table('service_firms')->where('id_service_order', $idOrder)->first();
        $firmUrl = "";
        if ($firm) {
            $firmUrl = CommonImage::getTemporaryUrl($firm->file_route, 5);
        }
        $symbol_country = CommonCompany::getSymbolByCountry();

        return view('vendor.adminlte.register._modalServiceOrder')->with([
            'order' => $order, 'plagasGrade' => $plagasGrade, 'habits' => $habit, 'inhabits' => $inhabit, 'mascots' => $mascot, 'imagesInspection' => $images,
            'imagesCondition' => $imagesCon, 'inspection' => $inspection, 'condition' => $condition, 'order_cleanings' => $order_cleanings,
            'plague_controls' => $plague_controls, 'cashe' => $cashe, 'firm' => $firm, 'imagesCashe' => $imagesCashe, 'plague2' => $plague2,
            'imagen' => $imagen, 'symbol_country' => $symbol_country, 'imagesMonitoring' => $imagesMonitoring, 'isInspections' => $isInspections, 'firmUrl' => $firmUrl
        ]);
    }

    public function whatsapp(Request $request)
    {

        $order = service_order::find($request->id);
        $order->whatsapp = 1;
        $order->save();

        return response()->json();

    }

    public function reminder(Request $request)
    {

        $order = service_order::find($request->id);
        $order->reminder = 1;
        $order->save();

        return response()->json();

    }

    public function confirmed(Request $request)
    {

        $order = service_order::find($request->id);
        $order->confirmed = $request->confirmed;
        $order->save();

        return response()->json();

    }

    public function cash_index()
    {
        // TODO: Delete Function
    }

    public function getPaymentMethods() {
        $employee = employee::where('employee_id', Auth::user()->id)->first();
        $paymentMethods = payment_method::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        $paymentWay = payment_way::where('profile_job_center_id', $employee->profile_job_center_id)->get();
        return response()->json([
            'code' => 200,
            'paymentMethods' => $paymentMethods,
            'paymentWay' => $paymentWay
        ]);
    }

    public function cash_add(Request $request)
    {

        try {

            if ($request->exists('ids')) {
                $payments = $request->get('payments');

                foreach ($payments as $payment) {

                    $paymentMethodId = $payment['paymentMethod'];
                    $paymentTypeId = $payment['paymentType'];
                    $total = $payment['total'];
                    $isPayment = $payment['isPayment'];
                    $payDay = $payment['paymentDate'];
                    $id = $payment['id'];

                    $event = event::where('id_service_order', $id)->first();
                    $cashService = cash::where('id_event', $event->id)->first();

                    if ($cashService) {
                        $cashService->id_payment_method = $paymentMethodId;
                        $cashService->id_payment_way = $paymentTypeId;
                        $cashService->amount_received = $total;
                        $cashService->payment = $isPayment;
                        $cashService->updated_at = $payDay;
                        $cashService->save();
                    } else {
                        $cash = new cash();
                        $cash->id_event = $event->id;
                        $cash->id_service_order = $id;
                        $cash->id_payment_method = $paymentMethodId;
                        $cash->id_payment_way = $paymentTypeId;
                        $cash->amount_received = $total;
                        $cash->companie = Auth::user()->companie;
                        $cash->payment = $isPayment;
                        $cash->save();
                    }

                    //Invoice
                    if ($isPayment == 2) {
                        $invoiceOrder = InvoiceOrder::where('id_service_order', $id)->first();
                        if ($invoiceOrder) {
                            $invoice = Invoice::find($invoiceOrder->id_invoice);
                            $invoice->id_status = Constants::$STATUS_INVOICE_VALID_PAID;
                            $invoice->save();
                        }
                    }

                }
                return response()->json([
                    'code' => 201,
                    'message' => 'Datos Guardados Correctamente'
                ]);
            } else {
                $idOrder = $request->get('idOrderCasheModal');
                $idEvent = $request->get('idEventCasheModal');
                $total = $request->get('total');
                $comments = $request->get('commentary');
                $paymentMethodId = $request->get('paymentMethod');
                $paymentTypeId = $request->get('paymentType');
                $payment = $request->get('payment');
                $payment = $payment == null ? 2 : 1;
                $payDay = $request->get('payDay');

                $cashService = cash::where('id_event', $idEvent)->first();
                $serviceOrder = service_order::where('id', $idOrder)->first();
                $quotation = quotation::find($serviceOrder->id_quotation);
                $event = event::where('id', $idEvent)->first();
                $employee = employee::find($event->id_employee);
                $user = User::find($employee->employee_id);

                if ($cashService) {
                    $cashService->id_payment_method = $paymentMethodId;
                    $cashService->id_payment_way = $paymentTypeId;
                    $cashService->amount_received = $total;
                    $cashService->commentary = $comments;
                    $cashService->payment = $payment;
                    $cashService->updated_at = $payDay;
                    $cashService->save();

                    $historicalCustomer = new \App\HistoricalCustomer();
                    $historicalCustomer->id_customer = $quotation->id_customer;
                    $historicalCustomer->id_user = $user->id;
                    $historicalCustomer->title = 'Pago (Orden De Servicio): ' . $serviceOrder->id_service_order;
                    $historicalCustomer->commentary = 'Monto Recibido: $' . $cashService->amount_received . ' ' . $cashService->commentary;
                    $historicalCustomer->date = Carbon::now()->toDateString();
                    $historicalCustomer->hour = Carbon::now()->toTimeString();
                    $historicalCustomer->save();

                } else {
                    $cash = new cash();
                    $cash->id_event = $idEvent;
                    $cash->id_service_order = $idOrder;
                    $cash->id_payment_method = $paymentMethodId;
                    $cash->id_payment_way = $paymentTypeId;
                    $cash->amount_received = $total;
                    $cash->companie = Auth::user()->companie;
                    $cash->payment = $payment;
                    $cash->commentary = $comments;
                    $cash->save();

                    $historicalCustomer = new \App\HistoricalCustomer();
                    $historicalCustomer->id_customer = $quotation->id_customer;
                    $historicalCustomer->id_user = $user->id;
                    $historicalCustomer->title = 'Pago (Orden De Servicio): ' . $serviceOrder->id_service_order;
                    $historicalCustomer->commentary = 'Monto Recibido: $' . $cash->amount_received . ' ' . $cash->commentary;
                    $historicalCustomer->date = Carbon::now()->toDateString();
                    $historicalCustomer->hour = Carbon::now()->toTimeString();
                    $historicalCustomer->save();
                }

                //Invoice
                if ($payment == 2) {
                    $invoiceOrder = InvoiceOrder::where('id_service_order', $idOrder)->first();
                    if ($invoiceOrder) {
                        $invoice = Invoice::find($invoiceOrder->id_invoice);
                        $invoice->id_status = Constants::$STATUS_INVOICE_VALID_PAID;
                        $invoice->save();
                    }
                }

                return response()->json([
                    'code' => 201,
                    'message' => 'Datos Guardados Correctamente'
                ]);
            }

        }
        catch (\Exception $e){
            return response()->json([
                'code' => 500,
                'message' => 'Error al Guardar, Intente de nuevo.'
            ]);
        }
    }

    public function download($id)
    {
        $info = cash_picture::find($id);
        $ruta = storage_path() . '/app/' . $info->file_route;
        return response()->download($ruta);
    }

    public function getSelectedPayments(Request $request) {
        try {
            $ids = $request->get('ids');
            $idProfileJobCenter = CommonCompany::getProfileJobCenterId();
            $orders = DB::table('service_orders as so')
                ->join('payment_methods as pm', 'so.id_payment_method', 'pm.id')
                ->join('payment_ways as pw', 'so.id_payment_way', 'pw.id')
                ->select('so.id', 'so.id_service_order', 'so.id_payment_method as ipm', 'so.id_payment_way as ipw',
                    'so.total')
                ->whereIn('so.id', $ids)
                ->get();

            $paymentMethods = payment_method::where('profile_job_center_id', $idProfileJobCenter)->get();
            $paymentWay = payment_way::where('profile_job_center_id', $idProfileJobCenter)->get();

            if ($orders->count() == 0) {
                return Response::json([
                    'code' => 500,
                    'message' => 'No se encontró información.'
                ]);
            }
            return Response::json([
                'orders' => $orders,
                'paymentMethods' => $paymentMethods,
                'paymentWay' => $paymentWay
            ]);

        } catch (\Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    public function getSelectedInvoices(Request $request) {
        try {
            $ids = $request->get('ids');
            $orders = DB::table('service_orders as so')
                ->select('so.id', 'so.id_service_order',  'so.invoiced_status')
                ->whereIn('so.id', $ids)
                ->get();

            if ($orders->count() == 0) {
                return Response::json([
                    'code' => 500,
                    'message' => 'No se encontró información.'
                ]);
            }
            return Response::json([
                'orders' => $orders,
            ]);

        } catch (\Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }

    }

    public function invoicesAdd(Request $request)
    {

        try {

            $isServiceOrders = $request->exists('ids');
            $invoices = $request->get('invoices');

            foreach ($invoices as $invoice) {

                $id = $invoice['id'];
                $isInvoice = $invoice['isInvoice'];

                $serviceOrder = service_order::where('id', $id)->first();
                if ($isInvoice == 1) $serviceOrder->invoiced_status = $isInvoice;
                else $serviceOrder->invoiced_status = 0;
                $serviceOrder->save();

            }

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        }
        catch (\Exception $e){
            return response()->json([
                'code' => 500,
                'message' => 'Error al Guardar, Intente de nuevo.'
            ]);
        }
    }

}
