<?php

namespace App\Http\Controllers\Services;

use App\archive_expense;
use App\article_expense;
use App\canceled_service_order;
use App\cash;
use App\cash_picture;
use App\Common\CommonCarbon;
use App\condition_picture;
use App\employee;
use App\establishment_type;
use App\event;
use App\expense;
use App\Http\Controllers\Accounting\CommonAccounting;
use App\Http\Controllers\Api\Dao\AreaInspectionDao;
use App\Http\Controllers\Api\Dao\MonitoringDao;
use App\Http\Controllers\Api\ServiceJsonController;
use App\Http\Controllers\Controller;
use App\inspection_picture;
use App\Jobs\SendCertificateEmail;
use App\Notifications\LogsNotification;
use App\order_cleaning_place_condition;
use App\place_condition;
use App\place_inspection;
use App\place_inspection_plague_type;
use App\plague_control;
use App\plague_control_application_method;
use App\plague_control_picture;
use App\plague_control_product;
use App\service_firms;
use App\source_origin;
use App\storehouse;
use App\transfer_ee_product;
use App\transfer_employee_employee;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Response;
use Storage;

class AppServicesController extends Controller
{

    public function downloadApp()
    {
        $ruta = storage_path().'/app/appAndroid/app-biofin-v1.apk';
        return response()->download($ruta);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getTypesOfService($id)
    {
        $types = establishment_type::where('profile_job_center_id', $id)->orderBy('name')->get();
        return response()->json($types);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getSourcesOrigin($id)
    {
        $sources = source_origin::where('profile_job_center_id', $id)->orderBy('name')->get();
        return response()->json($sources);
    }

    /**
     * @param $jobCenterId
     * @return JsonResponse
     */
    public function getTechniciansByJobCenter($jobCenterId)
    {
        $technicians = employee::where('profile_job_center_id', $jobCenterId)
            ->where('status', 1)
            ->get();
        return response()->json($technicians);
    }

    /**
     * @return JsonResponse
     */
    public function getPaymentMethods($id)
    {
        $paymentMethods = DB::table('payment_methods')
            ->select('id', 'name')
            ->where('profile_job_center_id', $id)
            ->get();

        return response()->json($paymentMethods);
    }

    /**
     * @param $idCompany
     * @return JsonResponse
     */
    public function getPaymentTypes($idCompany)
    {
        $paymentTypes = DB::table('payment_ways')
            ->select('id', 'name')
            ->where('profile_job_center_id', $idCompany)
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($paymentTypes);
    }

    /**
     * @return JsonResponse
     */
    public function getApplicationMethods($id)
    {
        $applicationMethods = DB::table('application_methods')
            ->select('id', 'name')
            ->where('profile_job_center_id', $id)
            ->get();

        return response()->json($applicationMethods);
    }

    public function getVouchers($id)
    {
        $vouchers = DB::table('vouchers')
            ->select('id', 'name')
            ->where('profile_job_center_id', $id)
            ->get();

        return response()->json($vouchers);
    }

    public function getConceptsForExpense($id)
    {
        $concepts = DB::table('concepts')
            ->select('id', 'name')
            ->where('type', 2)
            ->where('profile_job_center_id', $id)
            ->get();

        return response()->json($concepts);
    }

    /**
     * @return JsonResponse
     */
    public function getOrderCleanings($idCompany)
    {
        $orderCleanings = DB::table('order_cleanings')
            ->where('profile_job_center_id', $idCompany)
            ->select('id', 'name')
            ->get();

        return response()->json($orderCleanings);
    }

    /**
     * @param $idCompany
     * @param $type
     * @return JsonResponse
     */
    public function getPlagues($idCompany, $type)
    {
        $plagues = DB::table('plague_types')
            ->where('profile_job_center_id', $idCompany)
            ->orderBy('name');

        if ($type == 0) { // all
            $plagues->select('id', 'name');
        } elseif ($type == 1) { // capture
            $plagues->where('is_monitoring', 1)
                ->orWhere('is_monitoring', 3)
                ->select('id', 'name');
        } elseif ($type == 2) { // UV
            $plagues->where('is_monitoring', 2)
                ->orWhere('is_monitoring', 3)
                ->select('id', 'name');
        }

        return response()->json($plagues->get());
    }

    /**
     * @return JsonResponse
     */
    public function getGradesInfestation($idCompany)
    {
        $gradesInfestation = DB::table('infestation_degrees')
            ->where('profile_job_center_id', $idCompany)
            ->select('id', 'name')
            ->get();

        return response()->json($gradesInfestation);
    }

    /**
     * @return JsonResponse
     */
    public function getProducts($id, $idEmployee)
    {
        $products = DB::table('storehouses as s')
            ->join('products as p', 's.id_product', 'p.id')
            ->select('p.id', 'p.name')
            ->where('s.id_employee', $idEmployee)
            ->where('s.stock_other_units', '>', 0)
            ->where('p.profile_job_center_id', $id)
            ->get();

        return response()->json($products);
    }

    /**
     * [getProductById description]
     * @param Request $request
     * @return array [type]            [description]
     */
    public function getProductById(Request $request)
    {
        $product = DB::table('products')
            ->where('name', $request->name)
            ->select('id', 'active_ingredient')
            ->first();
        return ['active' => $product->active_ingredient];
    }

    //section POST -->
    
    public function startServiceWithEvent(Request $request, $id)
    {
        try {
            $event = event::find($id);
            if ($event->id_status != 2) {
                $timezone = CommonCarbon::getTimeZoneByJobCenter($event->id_job_center);
                $date = Carbon::now()->timezone($timezone);
                $date = $date->format('H:i');
                $event->start_event = $date;
                $event->id_status = $request->get('status');
                $event->latitude = $request->get('latitude');
                $event->longitude = $request->get('longitude');
                $event->save();
            }

            return ['status' => 'ok'];
            
        } catch (Exception $e) {
            return ['status' => 'Error al actualizar los datos.'];
        }
    }

    public function checkInManual(Request $request, $id)
    {
        try {
            $eventId = $id;

            $event = event::find($eventId);
            if ($event->id_status == 4) {
                return Response::json([
                    'code' => 200,
                    'message' => 'El servicio ya se encuentra finalizado.'
                ]);
            }

            $timezone = CommonCarbon::getTimeZoneByJobCenter($event->id_job_center);
            $date = Carbon::now()->timezone($timezone);
            $date = $date->format('H:i');
            $event->start_event = $date;
            $event->id_status = $request->get('status');
            $event->latitude = $request->get('latitude');
            $event->longitude = $request->get('longitude');
            $event->save();

            return Response::json([
                'code' => 200,
                'message' => 'Check in ok!'
            ]);
        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification("ERROR CHECK IN", $exception->getMessage(), 3));
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function saveCancelService(Request $request)
    {
        try {

            $cancelService = new canceled_service_order;
            $cancelService->id_service_order = $request->id_service_order;
            $cancelService->reason = $request->motive;
            $cancelService->commentary = $request->comments;
            $cancelService->save();

            return ['status' => 'ok'];

        } catch (Exception $e) {
            return ['status' => 'Error al guardar los datos.'];
        }
        
    }

    public function saveInspectionPicture(Request $request)
    {

        $isInspection = DB::table('place_inspections')->where('id_service_order', $request->id_service_order)->first();

        if (empty($isInspection)) {
            try {
            DB::beginTransaction();
                $place_inspection = new place_inspection;
                $place_inspection->id_service_order = $request->id_service_order;
                $place_inspection->save();

                $inspection_picture = new inspection_picture;
                $inspection_picture->place_inspection_id = $place_inspection->id;
                $inspection_picture->file_route = $request->file_route;
                $inspection_picture->save();
            DB::commit();
                return ['status' => 'ok'];
            
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }
        }else{
            try {

            $inspection_picture = new inspection_picture;
            $inspection_picture->place_inspection_id = $isInspection->id;
            $inspection_picture->file_route = $request->file_route;
            $inspection_picture->save();

            return ['status' => 'ok'];
            
            } catch (Exception $e) {
                return ['status' => 'Error al guardar los datos.'];
            }
        }
        
    }

    public function savePlaceInspection(Request $request)
    {
        $isInspection = DB::table('place_inspections')->where('id_service_order', $request->id_service_order)->first();
        $arrayPlagues = explode(",", $request->plagues);
        $arrayGradesInfestation = explode(",", $request->grades_infestation);

        if (empty($isInspection)) {

            try {
                DB::beginTransaction();
                    $place_inspection = new place_inspection;
                    $place_inspection->id_service_order = $request->id_service_order;
                    $place_inspection->nesting_areas = $request->nesting_areas;
                    $place_inspection->commentary = $request->commentary;
                    $place_inspection->save();

                    for ($i = 0; $i < count($arrayPlagues); $i++) {
                        $place_inspection_plague_type = new place_inspection_plague_type;
                        $place_inspection_plague_type->place_inspection_id = $place_inspection->id;
                        $place_inspection_plague_type->plague_type_id = $arrayPlagues[$i];
                        $place_inspection_plague_type->id_infestation_degree = $arrayGradesInfestation[$i];
                        $place_inspection_plague_type->save();

                    }
                DB::commit();
                return ['status' => 'ok'];
                
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }
            
        }

        else{

            try {

                DB::beginTransaction();
                    $place_inspection = place_inspection::find($isInspection->id);
                    $place_inspection->nesting_areas = $request->nesting_areas;
                    $place_inspection->commentary = $request->commentary;
                    $place_inspection->save();

                    $pipt = place_inspection_plague_type::where('place_inspection_id', $isInspection->id);
                    if (!empty($pipt)) {
                        //delete plagues and save new
                        $deletedRows = place_inspection_plague_type::where('place_inspection_id', $isInspection->id)->delete();
                    }

                    for ($i = 0; $i < count($arrayPlagues); $i++) {

                        $place_inspection_plague_type = new place_inspection_plague_type;
                        $place_inspection_plague_type->place_inspection_id = $place_inspection->id;
                        $place_inspection_plague_type->plague_type_id = $arrayPlagues[$i];
                        $place_inspection_plague_type->id_infestation_degree = $arrayGradesInfestation[$i];
                        $place_inspection_plague_type->save();

                    }


                DB::commit();
                return ['status' => 'ok'];
                
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }

        }

    }

    public function saveConditionPicture(Request $request)
    {

        $isCondition = DB::table('place_conditions')->where('id_service_order', $request->id_service_order)->first();

        if (empty($isCondition)) {
            try {

                DB::beginTransaction();
                    $place_condition = new place_condition;
                    $place_condition->id_service_order = $request->id_service_order;
                    $place_condition->save();

                    $condition_picture = new condition_picture;
                    $condition_picture->place_condition_id = $place_condition->id;
                    $condition_picture->file_route = $request->file_route;
                    $condition_picture->save();
                DB::commit();
                return ['status' => 'ok'];
            
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }
        }else{
            try {

                $condition_picture = new condition_picture;
                $condition_picture->place_condition_id = $isCondition->id;
                $condition_picture->file_route = $request->file_route;
                $condition_picture->save();

                return ['status' => 'ok'];
            
            } catch (Exception $e) {
                return ['status' => 'Error al guardar los datos.'];
            }
        }
        
    }

    public function saveControlPlaguePicture(Request $request)
    {

        $id = $request->id;
        $idService = $request->id_service_order;
        $idTransfer = $request->id_transfer;
        $idQuotation = DB::table('service_orders')->select('id_quotation')->where('id', $idService)->first()->id_quotation;
        $idCompany = $request->id_company;
        $idEmployee = $request->id_employee;

        if ($id == "null") { //se crea por primera vez una área.
           try {

               DB::beginTransaction();
                    $place_control = new plague_control;
                    $place_control->id_service_order = $request->id_service_order;
                    $place_control->save();

                    $control_picture = new plague_control_picture;
                    $control_picture->plague_control_id = $place_control->id;
                    $control_picture->file_route = $request->file_route;
                    $control_picture->save();

                    if ($idTransfer == "null") {
                        //Create transfer employee to client
                        $storehouseClient = DB::table('employees')->where('id_company', $idCompany)->where('status', 200)->first(); //status => 200 : Es para identificar el almacen de tipo cliente.
                        $user = DB::table('employees')->where('id', $idEmployee)->first();
                        $transfer = new transfer_employee_employee();
                        $transfer->id_transfer_ee = 'TAC-' . $idQuotation;
                        $transfer->id_user = $user->employee_id;
                        $transfer->id_employee_origin = $idEmployee;
                        $transfer->id_employee_destiny = $storehouseClient->id;
                        $transfer->companie = $idCompany;
                        $transfer->save();
                    }
               DB::commit();
                    if ($idTransfer == "null") return ['status' => 'ok', 'id' => $place_control->id, 'id_transfer' => $transfer->id];
                    else return ['status' => 'ok', 'id' => $place_control->id, 'id_transfer' => $idTransfer];
            
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            } 
        }else{ //ya se creo la primera área. (esta puede ser la segunda o más...)
            try {

            $control_picture = new plague_control_picture;
            $control_picture->plague_control_id = $id;
            $control_picture->file_route = $request->file_route;
            $control_picture->save();

            return ['status' => 'ok', 'id' => $id, 'id_transfer' => $idTransfer];
            
            } catch (Exception $e) {
                return ['status' => 'Error al guardar los datos.'];
            }
        }
        
    }

    public function saveFirmsPicture(Request $request)
    {
        try {

            DB::beginTransaction();
                $firm = new service_firms;
                $firm->id_service_order = $request->id_service_order;
                $firm->file_route = $request->file_route;
                $firm->save();

                $event = event::find($request->id_event);
                $event->id_status = $request->status;
                $date = Carbon::now();
                $date = $date->format('H:i');
                $event->final_event = $date;
                $event->save();
            DB::commit();

            return ['status' => 'ok'];
            
        } catch (Exception $e) {
            DB::rollBack();
            return ['status' => 'Error al guardar los datos.'];
        }
        
    }

    public function saveCashPicture(Request $request)
    {
        $isCash = DB::table('cashes')->where('id_service_order', $request->id_service_order)->first();

        if (empty($isCash)) {
            try {

                DB::beginTransaction();
                    $cash = new cash;
                    $cash->id_service_order = $request->id_service_order;
                    $cash->id_payment_method = 1;
                    $cash->id_payment_way = 1;
                    $cash->save();

                    $cash_picture = new cash_picture;
                    $cash_picture->cash_id = $cash->id;
                    $cash_picture->file_route = $request->file_route;
                    $cash_picture->save();
                DB::commit();

                return ['status' => 'ok'];
                
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }
        }else{
            try {
                $cash_picture = new cash_picture;
                $cash_picture->cash_id = $isCash->id;
                $cash_picture->file_route = $request->file_route;
                $cash_picture->save();

                return ['status' => 'ok'];
            } catch (Exception $e) {
                return ['status' => 'Error al guardar los datos.'];
            }
            
        }
    }

    public function savePlaceCondition(Request $request)
    {
        $isCondition = DB::table('place_conditions')->where('id_service_order', $request->id_service_order)->first();
        $arrayCleanings = explode(",", $request->cleanings);

        if (empty($isCondition)) {
            try {

                DB::beginTransaction();
                    $place_condition = new place_condition;
                    $place_condition->id_service_order = $request->id_service_order;
                    $place_condition->restricted_access = $request->restricted_access;
                    $place_condition->commentary = $request->commentary;
                    if ($request->indications == "Si") {
                        $place_condition->indications = 1;
                    }elseif ($request->indications == "No") {
                        $place_condition->indications = 2;
                    }else{
                        $place_condition->indications = 3;
                    }
                    $place_condition->save();

                    $place_condition->order_cleanings()->sync($arrayCleanings);
                DB::commit();
                return ['status' => 'ok'];
            
            }catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }
        }else{
            try {

                DB::beginTransaction();
                    $ocpc = order_cleaning_place_condition::where('place_condition_id', $isCondition->id);
                    if (!empty($ocpc)) {
                        //delete cleanings and save new
                        $deletedRows = order_cleaning_place_condition::where('place_condition_id', $isCondition->id)->delete();
                    }

                    $place_condition = place_condition::find($isCondition->id);
                    $place_condition->restricted_access = $request->restricted_access;
                    $place_condition->commentary = $request->commentary;
                    if ($request->indications == "Si") {
                        $place_condition->indications = 1;
                    }elseif ($request->indications == "No") {
                        $place_condition->indications = 2;
                    }else{
                        $place_condition->indications = 3;
                    }
                    $place_condition->save();

                    $place_condition->order_cleanings()->sync($arrayCleanings);
                DB::commit();
                return ['status' => 'ok'];
            
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }
        }//end else

    }

    public function savePlagueControl(Request $request)
    {
        $id = $request->id;
        $idService = $request->id_service_order;
        $idQuotation = DB::table('service_orders')->select('id_quotation')->where('id', $idService)->first()->id_quotation;
        $idTransfer = $request->id_transfer;
        $idEmployee = $request->get('id_employee');
        $idCompany = $request->get('id_company');
        $isNewArea = $request->get('is_new_area');
        $isControl = DB::table('plague_controls')->where('id', $id)->first();
        $arrayApplicationMethods = explode(",", $request->application_methods);
        $arrayProducts = explode(",", $request->products);
        $arrayDose = explode(",", $request->dose);
        $arrayQuantity = explode(",", $request->quantity);

        if (empty($isControl) || $isNewArea) {

            if (empty($isControl)) {

                try {

                    DB::beginTransaction();
                        $place_control = new plague_control;
                        $place_control->id_service_order = $request->id_service_order;
                        $place_control->control_areas = $request->control_areas;
                        $place_control->commentary = $request->commentary;
                        $place_control->save();

                        if ($idTransfer == "null"){
                            //Create transfer employee to client
                            $storehouseClient = DB::table('employees')->where('id_company', $idCompany)->where('status', 200)->first(); //status => 200 : Es para identificar el almacen de tipo cliente.
                            $user = DB::table('employees')->where('id', $idEmployee)->first();
                            $transfer = new transfer_employee_employee();
                            $transfer->id_transfer_ee = 'TAC-' . $idQuotation;
                            $transfer->id_user = $user->employee_id;
                            $transfer->id_employee_origin = $idEmployee;
                            $transfer->id_employee_destiny = $storehouseClient->id;
                            $transfer->companie = $idCompany;
                            $transfer->save();
                        }

                        for ($i = 0; $i < count($arrayApplicationMethods); $i++) {
                            $plague_control_application_methods = new plague_control_application_method;
                            $plague_control_application_methods->plague_control_id = $place_control->id;
                            $plague_control_application_methods->id_application_method = $arrayApplicationMethods[$i];
                            $plague_control_application_methods->save();
                        }

                        for ($i = 0; $i < count($arrayProducts); $i++) {
                            $products = new plague_control_product;
                            $products->plague_control_id = $place_control->id;
                            $products->id_product = $arrayProducts[$i];
                            $products->dose = $arrayDose[$i];
                            $products->quantity = $arrayQuantity[$i];
                            $products->save();

                            // Update storehouse
                            $productData = DB::table('products')->where('id', $arrayProducts[$i])->first();
                            $priceByQuantity = $productData->base_price / $productData->quantity;
                            $totalStore = $priceByQuantity * $arrayQuantity[$i];
                            $storehouse = DB::table('storehouses')->where('id_employee', $idEmployee)->where('id_product', $arrayProducts[$i])->first();
                            $stock = storehouse::find($storehouse->id);
                            $stock->stock_other_units = $stock->stock_other_units - $arrayQuantity[$i];
                            $stock->total = $stock->total - $totalStore;
                            $stock->save();

                            //Transfer products
                            $transferCCProduct = new transfer_ee_product();
                            if ($idTransfer == "null") $transferCCProduct->id_transfer_ee = $transfer->id;
                            else $transferCCProduct->id_transfer_ee = $idTransfer;
                            $transferCCProduct->id_product = $arrayProducts[$i];
                            $transferCCProduct->units = $arrayQuantity[$i];
                            $transferCCProduct->is_units = 0;
                            $transferCCProduct->save();

                            //Update total transfer
                            if ($idTransfer == "null") $entryTransfer = transfer_employee_employee::find($transfer->id);
                            else $entryTransfer = transfer_employee_employee::find($idTransfer);
                            $entryTransfer->total = $entryTransfer->total + $totalStore;
                            $entryTransfer->save();
                        }
                    DB::commit();
                    if ($idTransfer == "null")
                        return ['status' => 'ok', 'id' => "null", 'id_transfer' => $transfer->id];
                    else return ['status' => 'ok', 'id' => "null", 'id_transfer' => $idTransfer];

                } catch (Exception $e) {
                    DB::rollBack();
                    return ['status' => 'Error al guardar los datos.'];
                }

            }else {
                    
                try {

                    DB::beginTransaction();
                        $place_control = plague_control::find($isControl->id);
                        $place_control->control_areas = $request->control_areas;
                        $place_control->commentary = $request->commentary;
                        $place_control->save();

                        for ($i = 0; $i < count($arrayApplicationMethods); $i++) {
                            $plague_control_application_methods = new plague_control_application_method;
                            $plague_control_application_methods->plague_control_id = $isControl->id;
                            $plague_control_application_methods->id_application_method = $arrayApplicationMethods[$i];
                            $plague_control_application_methods->save();
                        }

                        for ($i = 0; $i < count($arrayProducts); $i++) {
                            $products = new plague_control_product;
                            $products->plague_control_id = $isControl->id;
                            $products->id_product = $arrayProducts[$i];
                            $products->dose = $arrayDose[$i];
                            $products->quantity = $arrayQuantity[$i];
                            $products->save();

                            // Update storehouse
                            $productData = DB::table('products')->where('id', $arrayProducts[$i])->first();
                            $priceByQuantity = $productData->base_price / $productData->quantity;
                            $totalStore = $priceByQuantity * $arrayQuantity[$i];
                            $storehouse = DB::table('storehouses')->where('id_employee', $idEmployee)->where('id_product', $arrayProducts[$i])->first();
                            $stock = storehouse::find($storehouse->id);
                            $stock->stock_other_units = $stock->stock_other_units - $arrayQuantity[$i];
                            $stock->total = $stock->total - $totalStore;
                            $stock->save();

                            //Transfer products
                            $transferCCProduct = new transfer_ee_product();
                            $transferCCProduct->id_transfer_ee = $idTransfer;
                            $transferCCProduct->id_product = $arrayProducts[$i];
                            $transferCCProduct->units = $arrayQuantity[$i];
                            $transferCCProduct->is_units = 0;
                            $transferCCProduct->save();

                            //Update total transfer
                            $entryTransfer = transfer_employee_employee::find($idTransfer);
                            $entryTransfer->total = $entryTransfer->total + $totalStore;
                            $entryTransfer->save();
                        }
                    DB::commit();
                    return ['status' => 'ok', 'id' => "null", 'id_transfer' => $idTransfer]; //TODO: REVISAR EL RETURN PARA QUE NO SE CILE CON ID REGRESADO (MISMO EDITA)
                    
                } catch (Exception $e) {
                    DB::rollBack();
                    return ['status' => 'Error al guardar los datos.'];
                }
            }
        }

    }

    public function saveBox(Request $request)
    {

        try {

            DB::beginTransaction();
                $ocpc = cash::where('id_service_order', $request->id_service_order);
                if (!empty($ocpc)) {
                    //delete payment and save new
                    $deletedRows = cash::where('id_service_order', $request->id_service_order)->delete();
                }

                $cash = new cash;
                $cash->id_service_order = $request->id_service_order;
                $cash->id_event = $request->id_event;
                $cash->companie = $request->id_company;
                $cash->id_payment_method = $request->id_payment_method;
                $cash->id_payment_way = $request->id_payment_way;
                $cash->amount_received = $request->amount_received;
                $cash->commentary = $request->commentary;
                $cash->payment = $request->payment;
                $cash->save();
            DB::commit();
            return ['status' => 'ok'];
            
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }

    }

    public function saveExpense(Request $request)
    {

        try {

            DB::beginTransaction();
                $employee = DB::table('employees')->where('id', $request->id_user)->select('profile_job_center_id', 'employee_id')->first();
    
                $expense = new expense;
                $expense->id_expense = CommonAccounting::getFolioExpenseByApi($request->companie);
                $expense->expense_name = $request->expense_name;
                $expense->id_concept = $request->id_concept;
                $expense->date_expense = Carbon::now()->toDateString();
                $expense->id_user = $employee->employee_id;
                $expense->description_expense = $request->description;
                $expense->total = $request->mount;
                $expense->status = 1;
                $expense->id_payment_way = $request->id_method_way;
                $expense->credit_days = $request->days_credit;
                $expense->id_payment_method = $request->id_method;
                $expense->account_status = 1;
                $expense->id_voucher = $request->id_voucher;
                $expense->id_jobcenter = $employee->profile_job_center_id;
                $expense->companie = $request->companie;
                $expense->save();
    
                //Articulos
                $art_exp = new article_expense;
                $art_exp->id_expense = $expense->id;
                $art_exp->concept = $request->article;
                $art_exp->total = $request->mount;
                $art_exp->companie = $request->companie;
                $art_exp->save();
    
                //Guardar comprobante
                if ($request->isFile == "1") {
                    $name = $expense->id;
                    $target_path = storage_path('app/expenses/');
                    
                    $partes_ruta = pathinfo(basename( $_FILES['pdf']['name']));
                    $extension = $partes_ruta['extension'];
                    $target_path = $target_path . $name . "." . $extension;
                    $filename = "expenses/" . $name . "." . $extension;
                    
                    move_uploaded_file($_FILES['pdf']['tmp_name'], $target_path);
                    
                    archive_expense::create(['id_expense' => $expense->id,
                        'file_route' => $filename, 'type' => 1, 'companie' => $request->companie]);
                }
            DB::commit();
    
                return ['status' => 'ok'];
            
            } catch (Exception $e) {
                DB::rollBack();
                return ['status' => 'Error al guardar los datos.'];
            }

    }

    public function getPicturesPlaceInspection($idServiceOrder)
    {
        $idPlaceInspection = DB::table('place_inspections')->where('id_service_order', $idServiceOrder)->first();

        $images = DB::table('inspection_pictures')->where('place_inspection_id', $idPlaceInspection->id)->select('id', 'file_route')->get();

        return response()->json($images);
    }

    public function getPicturesPlaceCondition($idServiceOrder)
    {
        $idPlaceCondition = DB::table('place_conditions')->where('id_service_order', $idServiceOrder)->first();

        $images = DB::table('condition_pictures')->where('place_condition_id', $idPlaceCondition->id)->select('id', 'file_route')->get();

        return response()->json($images);
    }

    public function getPicturesPlagueControl($idServiceOrder)
    {
        $idPlaceControl = DB::table('plague_controls')->where('id_service_order', $idServiceOrder)->first();

        $images = DB::table('plague_control_pictures')->where('plague_control_id', $idPlaceControl->id)->select('id', 'file_route')->get();

        return response()->json($images);
    }

    public function getPicturesPlagueControlById($id)
    {
        $images = DB::table('plague_control_pictures')->where('plague_control_id', $id)->select('id', 'file_route')->get();

        return response()->json($images);
    }

    public function getPicturesCash($idServiceOrder)
    {
        $idCash = DB::table('cashes')->where('id_service_order', $idServiceOrder)->first();

        $images = DB::table('cash_pictures')->where('cash_id', $idCash->id)->select('id', 'file_route')->get();

        return response()->json($images);
    }

    // Show service information.
    
    public function getPlaceInspection($id)
    {
        $place_inspection = DB::table('place_inspections as pi')
            ->where('id_service_order', $id)
            ->select('pi.nesting_areas', 'pi.commentary')
            ->first();

        $plagues_inspection = DB::table('place_inspections as pi')
            ->where('id_service_order', $id)
            ->join('place_inspection_plague_type as pipt', 'pi.id', 'pipt.place_inspection_id')
            ->join('plague_types as pt', 'pipt.plague_type_id', 'pt.id')
            ->join('infestation_degrees as ie','pipt.id_infestation_degree','ie.id')
            ->select('pt.name', 'ie.name as infestacion')
            ->get();

        $plagues = "";
        foreach ($plagues_inspection as $plague) {
            $plagues = $plagues . $plague->name . ": " . $plague->infestacion . ", ";
        }

        $place_conditions = DB::table('place_conditions as pc')
            ->where('id_service_order', $id)
            ->select('pc.indications', 'pc.commentary', 'pc.restricted_access')
            ->first();

        $order_cleanings = DB::table('place_conditions as pc')
            ->where('id_service_order', $id)
            ->join('order_cleaning_place_condition as ocpc', 'pc.id', 'ocpc.place_condition_id')
            ->join('order_cleanings as oc', 'ocpc.order_cleaning_id', 'oc.id')
            ->select('oc.name')
            ->get();

        $orderCleanings = "";
        foreach ($order_cleanings as $order) {
            $orderCleanings = $orderCleanings . $order->name . ", ";
        }

        $collection = collect();
        $collection->put('nesting_areas', $place_inspection->nesting_areas);
        if ($place_inspection->commentary == null) {
            $collection->put('commentary', "");
        }else{
            $collection->put('commentary', $place_inspection->commentary);
        }
        $collection->put('plagues', $plagues);

        if ($place_conditions->indications == "1") {
            $collection->put('indications', "Si");
        }elseif ($place_conditions->indications == "1") {
            $collection->put('indications', "No");
        }else{
            $collection->put('indications', "Una Parte");
        }

        if ($place_conditions->restricted_access == null) {
            $collection->put('restricted_access', "");
        }else{
            $collection->put('restricted_access', $place_conditions->restricted_access);
        }

        if ($place_conditions->commentary == null) {
            $collection->put('commentary_conditions', "");
        }else{
            $collection->put('commentary_conditions', $place_conditions->commentary);
        }
        $collection->put('cleanings', $orderCleanings);

        return response()->json($collection);
    }


    public function getControlPlagues($id)
    {
        $plague_control = DB::table('plague_controls as pc')
            ->where('id_service_order', $id)
            ->join('application_methods as am', 'pc.id_application_method', 'am.id')
            ->join('products as pr', 'pc.id_product', 'pr.id')
            ->select('pc.control_areas', 'pc.dose', 'pc.quantity', 'pc.commentary', 'am.name as application_method', 'pr.name as product_name', 'pr.active_ingredient')
            ->first();

        $collection = collect($plague_control);

        if ($plague_control->commentary == null) {
            $collection->put('commentary', "");
        }else{
            $collection->put('commentary', $plague_control->commentary);
        }

        return response()->json($collection);   
    }


    public function getBoxPayment($id)
    {
        $box = DB::table('cashes as ca')
          ->where('id_service_order', $id)
          ->join('payment_methods as pm', 'ca.id_payment_method', 'pm.id')
          ->join('payment_ways as pw', 'ca.id_payment_way', 'pw.id')
          ->select('pm.name as payment_method', 'pw.name as payment_way', 'ca.amount_received', 'ca.commentary', 'ca.payment')
          ->first();

        $collection = collect($box);

        if ($box->commentary == null) {
            $collection->put('commentary', "");
        }else{
            $collection->put('commentary', $box->commentary);
        }

        if ($box->amount_received == null) {
            $collection->put('amount_received', "");
        }else{
            $collection->put('amount_received', $box->amount_received);
        }

        if ($box->payment == "2") {
            $collection->put('payment', "El cliente si pago."); 
        }else{
            $collection->put('payment', "El cliente no pago.");
        }

        return response()->json($collection);  
    }

    public function getSignPicture($id)
    {
        $sign = DB::table('service_firms')->where('id_service_order', $id)->select('file_route')->first();
        return response()->json($sign);
    }

    public function getInitialHour($id)
    {
        $initialHour = DB::table('events')->where('id', $id)->select('start_event')->first();

        return response()->json($initialHour);
    }

    public function getFinalHour($id)
    {
        $finalHour = DB::table('events')->where('id', $id)->select('final_event', 'final_date')->first();

        return response()->json($finalHour);
    }

    public function getAreasControlById($id)
    {

        $plague_controls = DB::table('plague_controls')->where('id_service_order', $id)->select('control_areas', 'commentary', 'id')->get();
            
        foreach ($plague_controls as $pg) {
            $plague_controls->map(function($pg){
                if ($pg->commentary == null) {
                    $pg->commentary = "";
                }
                $methods_application = DB::table('plague_controls_application_methods as pcam')
                    ->join('application_methods as am', 'pcam.id_application_method', 'am.id')
                    ->select('am.name as method')
                    ->where('plague_control_id', $pg->id)->get();
                $methods = "";
                foreach ($methods_application as $ma) {
                    $methods = $methods . $ma->method . ", ";
                }
                $pg->methods = $methods;
                $plague_controls_products = DB::table('plague_controls_products as pcp')
                    ->join('products as p', 'pcp.id_product', 'p.id')
                    ->where('pcp.plague_control_id', $pg->id)
                    ->select('p.name as product', 'pcp.dose', 'pcp.quantity')
                    ->get();
                $pg->plague_controls = $plague_controls_products;
            });
        }

        return response()->json($plague_controls);
    }

    public function uploadPhoto(Request $request)
    {
        try {
            $name = $request->get('name');
            $image = $request->get('image');
            $folder = $request->get('folder');
            $route = $request->get('route');
            $id = $request->get('id');
            $otherName = $request->get('other_name', '');
            $idStation = $request->get('id_station', 1);

            $decodedImage = base64_decode($image);

            $fileImage = Image::make($decodedImage)
                ->orientate()
                ->resize(500, null, function ($constraint) { $constraint->aspectRatio(); } )
                ->encode('jpg',60);

            if ($folder == "services/expenses/") {
                $saveImage = Storage::disk('s3')->put('/expenses/' . $name . ".JPG", $fileImage);
                $route = "expenses/" . $name . ".JPG";
            }else {
                if ($folder == "profile/photos/" || $folder == "profile/firms/") {
                    $saveImage = Storage::disk('s3p')->put('/' . $folder . $name . ".JPG", $fileImage);
                } else {
                    $saveImage = Storage::disk('s3')->put('/' . $folder . $name . ".JPG", $fileImage);
                }
            }

            if($saveImage !== false) {
                if ($route == 1) {
                    return Response::json([
                        'code' => 200,
                        'message' => 'Foto guardada.'
                    ]);
                }
                $codeResponse = ServiceJsonController::packageJsonPhotos($folder, $id, $route, $idStation, $otherName);
                if ($codeResponse == 200) {
                    return Response::json([
                        'code' => 200,
                        'message' => 'Foto guardada.'
                    ]);
                } else {
                    User::first()->notify(new LogsNotification('Data Photo', 'Error al guardar los datos de la foto.', 3));
                    return Response::json([
                        'code' => 500,
                        'message' => 'Error al guardar los datos de la foto.'
                    ]);
                }
            }else {
                User::first()->notify(new LogsNotification('File Photo', 'Error al guardar la foto.', 3));
                return Response::json([
                    'code' => 500,
                    'message' => 'Error al guardar la foto.'
                ]);
            }

        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification('Catch Photo', 'Algo salio mal.', 3));
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal.'
            ]);
        }
    }

    public function uploadPhotoStation()
    {
        $objString = file_get_contents('php://input');
        if (isset($objString)) {
            $request = json_decode($objString);
            $name = $request->name;
            $image = $request->image;
            $folder = $request->folder;
            $route = $request->route;
            $id = $request->id;
            $idStation = $request->id_station;

            $fileImage = Image::make($request->image)
                ->orientate()
                ->resize(500, null, function ($constraint) { $constraint->aspectRatio(); } )
                ->encode('jpg',50);

            $response = array();
            $decodedImage = base64_decode("$image");

            $saveImage =Storage::disk('s3')->put('/' . $folder . $name . ".JPG", $fileImage);

            if($saveImage !== false){
                MonitoringDao::saveStationOnlyPicture($id, $idStation, $route);
                $response['success'] = 1;
                $response['message'] = "Foto guardada.";
            }else{
                $response['success'] = 0;
                $response['message'] = "Error al guardar la foto.";
            }

            echo json_encode($response);
        }
    }

    public function uploadPhotoArea(Request $request)
    {
        try {
            $name = $request->get('name');
            $image = $request->get('image');
            $folder = $request->get('folder');
            $route = $request->get('route');
            $nodeId = $request->get('nodeId');
            $technicianId = $request->get('technicianId');

            $decodedImage = base64_decode($image);

            $fileImage = Image::make($decodedImage)
                ->orientate()
                ->resize(500, null, function ($constraint) { $constraint->aspectRatio(); } )
                ->encode('jpg',50);

            $saveImage =Storage::disk('s3')->put('/' . $folder . $name . ".JPG", $fileImage);

            if($saveImage !== false){
                $codeResponse = AreaInspectionDao::saveAreaOnlyPicture($nodeId, $route, $technicianId);
                if ($codeResponse == 200) {
                    return Response::json([
                        'code' => 200,
                        'message' => 'Foto guardada.'
                    ]);
                } else {
                    return Response::json([
                        'code' => 500,
                        'message' => 'Error al guardar los datos de la foto.'
                    ]);
                }
            }else{
                return Response::json([
                    'code' => 500,
                    'message' => 'Error al guardar la foto.'
                ]);
            }
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal.'
            ]);
        }
    }

    public function saveDataPhotoProfile($id)
    {
        $user = User::find($id);
        $user->profile_photo = "profile/photos/" . $id . ".JPG";
        $user->save();
    }

    public function saveDataPhotoFirmProfile($id)
    {
        $employee = Employee::find($id);
        $employee->file_route_firm = "profile/firms/" . $id . ".JPG";
        $employee->save();
    }

    public function validateVersionApp($code) {
        $currentVersion = env('CURRENT_VERSION_APP');
        if ($currentVersion != $code) {
            return Response::json([
                'code' => 500,
                'message' => 'La app no esta actualizada.'
            ]);
        }
        return Response::json([
            'code' => 200,
            'message' => 'La app se encuentra actualizada.'
        ]);
    }

    public static function mail($email, $emailInput, $id)
    {
        SendCertificateEmail::dispatch($id, $email, $emailInput);
        return ['status' => 'ok'];
    }

}
