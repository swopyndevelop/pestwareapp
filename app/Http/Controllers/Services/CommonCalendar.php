<?php


namespace App\Http\Controllers\Services;


use App\event;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommonCalendar
{
    public static function getTechniciansAvailable($initialDate, $initialHour, $finalHour, $profileJobCenterId)
    {
        $employeesAvailable = collect();

        $employeesTec = DB::table('employees')
            ->where('profile_job_center_id', $profileJobCenterId)
            ->where('status', '<>', 200)
            ->where('status', 1)
            ->get();

        /*foreach ($employeesTec as $employee) {

            $user = User::find($employee->employee_id);
            if ($user->can('Realizar Servicios (App)')) {
                $events = event::where('id_employee', $employee->id)
                    ->where('initial_date',$initialDate)
                    ->where('id_status', 1)
                    ->get();

                $availableInitial = false;
                $availableFinal = false;

                foreach ($events as $event) {
                    $initial = new Carbon($initialHour);
                    $final = new Carbon($finalHour);
                    $init = $event->initial_date . " " . $event->initial_hour;
                    $fin = $event->final_date . " " . $event->final_hour;
                    $availableInitial = $initial->between(Carbon::parse($init)->addMinutes(1), Carbon::parse($fin)->subMinutes(1));
                    $availableFinal = $final->between(Carbon::parse($init)->addMinutes(1), Carbon::parse($fin)->subMinutes(1));
                    $initial = new Carbon($initialHour);
                    $final = new Carbon($finalHour);
                    if ($initial->toTimeString() == $event->initial_hour && $final->toTimeString() == $event->final_hour) {
                        $availableInitial = true;
                        $availableFinal = true;
                    }
                }

                if ($availableInitial == false && $availableFinal == false) $employeesAvailable->push($employee);
            }

        }*/
        return $employeesTec;
    }

    public static function getTechniciansAvailableIntelligent($initialDate, $initialHour, $finalHour, $profileJobCenterId)
    {
        $employeesAvailable = collect();
        $unavailableEmployees = collect();

        $employeesTec = DB::table('employees')
            ->where('profile_job_center_id', $profileJobCenterId)
            ->where('status', '<>', 200)
            ->where('status', 1)
            ->get();

        foreach ($employeesTec as $employee) {

            $user = User::find($employee->employee_id);
            if ($user->can('Realizar Servicios (App)')) {
                $events = event::where('id_employee', $employee->id)
                    ->where('initial_date',$initialDate)
                    ->where('id_status', 1)
                    ->get();

                $availableInitial = false;
                $availableFinal = false;

                foreach ($events as $event) {
                    $initial = new Carbon($initialHour);
                    $final = new Carbon($finalHour);
                    $init = $event->initial_date . " " . $event->initial_hour;
                    $fin = $event->final_date . " " . $event->final_hour;
                    $availableInitial = $initial->between(Carbon::parse($init)->addMinutes(1), Carbon::parse($fin)->subMinutes(1));
                    $availableFinal = $final->between(Carbon::parse($init)->addMinutes(1), Carbon::parse($fin)->subMinutes(1));
                    $initial = new Carbon($initialHour);
                    $final = new Carbon($finalHour);
                    if ($initial->toTimeString() == $event->initial_hour && $final->toTimeString() == $event->final_hour) {
                        $availableInitial = true;
                        $availableFinal = true;
                    }
                }

                if ($availableInitial == false && $availableFinal == false) $employeesAvailable->push($employee);
                else $unavailableEmployees->push($employee);

            }

        }
        return [
            'available' => $employeesAvailable,
            'unavailable' => $unavailableEmployees
        ];
    }

    public static function getTechniciansAvailableForUpdate($initialDate, $initialHour, $finalHour, $eventId, $profileJobCenterId)
    {
        $employeesAvailable = collect();

        $employeesTec = DB::table('employees')
            ->where('profile_job_center_id', $profileJobCenterId )
            ->where('status', '<>', 200)
            ->where('status', 1)
            ->get();

       /* foreach ($employeesTec as $employee) {

            $user = User::find($employee->employee_id);
            if ($user->can('Realizar Servicios (App)')) {
                $events = event::where('id_employee', $employee->id)
                    ->where('initial_date',$initialDate)
                    ->where('id_status', 1)
                    ->get();

                $availableInitial = false;
                $availableFinal = false;

                foreach ($events as $event) {
                    $initial = new Carbon($initialHour);
                    $final = new Carbon($finalHour);
                    $init = $event->initial_date . " " . $event->initial_hour;
                    $fin = $event->final_date . " " . $event->final_hour;
                    $availableInitial = $initial->between(Carbon::parse($init)->addMinutes(1), Carbon::parse($fin)->subMinutes(1));
                    $availableFinal = $final->between(Carbon::parse($init)->addMinutes(1), Carbon::parse($fin)->subMinutes(1));
                    $initial = new Carbon($initialHour);
                    $final = new Carbon($finalHour);
                    if ($initial->toTimeString() == $event->initial_hour && $final->toTimeString() == $event->final_hour && $event->id != $eventId) {
                        $availableInitial = true;
                        $availableFinal = true;
                    }
                }

                if ($availableInitial == false && $availableFinal == false) $employeesAvailable->push($employee);
            }

        }*/
        return $employeesTec;
    }
}