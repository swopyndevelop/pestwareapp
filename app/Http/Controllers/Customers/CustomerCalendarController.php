<?php

namespace App\Http\Controllers\Customers;

use App\address_job_center;
use App\companie;
use App\customer;
use App\employee;
use App\Http\Controllers\ReportsPDF\VisitSchedule;
use App\Http\Controllers\Security\SecurityController;
use App\profile_job_center;
use App\service_firms;
use App\service_order;
use Carbon\Carbon;
use DB;
use App\Http\Controllers\Controller;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CustomerCalendarController extends Controller
{
    public function index($customerId, $date)
    {
        $customerId = SecurityController::decodeId($customerId);
        $calendarData = VisitSchedule::build($customerId, $date);
        $pdf = PDF::loadView('vendor.adminlte.register.PDF.calendarClientsPdf', $calendarData);
        return $pdf->stream();
    }
}
