<?php

namespace App\Http\Controllers\Customers;

use App\customer;
use App\HistoricalCustomer;
use Auth;
use Carbon\Carbon;
use Exception;
use App\Http\Controllers\Security\SecurityController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HistoricalCustomerController extends Controller
{

    public function index($id){
        $id = SecurityController::decodeId($id);
        $customer = customer::find($id);
        $historicalCustomers = HistoricalCustomer::where('id_customer', $customer->id)
            ->join('users as u','historical_customers.id_user','u.id')
            ->orderBy('date','desc')
            ->orderBy('hour','desc')
            ->select('historical_customers.*','u.name as user_name')
            ->get();

        return view('vendor.adminlte.customers.historicalcustomer.index')
            ->with([
                'historicalCustomers' => $historicalCustomers,
                'customer' => $customer
            ]);
    }

    public function addCommentary(Request $request){

        try {
            $idCustomer = $request->get('idCustomer');
            $idUser = Auth::user()->id;
            $titleHistoricalCustomer = $request->get('titleHistoricalCustomer');
            $commentaryHistoricalCustomer = $request->get('commentaryHistoricalCustomer');
            $date = Carbon::now()->toDateString();
            $hour = Carbon::now()->toTimeString();

            $historicalCustomer = new HistoricalCustomer();
            $historicalCustomer->id_customer = $idCustomer;
            $historicalCustomer->id_user = $idUser;
            $historicalCustomer->title = $titleHistoricalCustomer;
            $historicalCustomer->commentary = $commentaryHistoricalCustomer;
            $historicalCustomer->date = $date;
            $historicalCustomer->hour = $hour;
            $historicalCustomer->save();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }

    }

}
