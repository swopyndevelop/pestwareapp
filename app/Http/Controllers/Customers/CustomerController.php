<?php

namespace App\Http\Controllers\Customers;

use App\Area;
use App\AreaInspectionPhotos;
use App\AreaInspections;
use App\BillingMode;
use App\cash_picture;
use App\CheckListImage;
use App\Common\CommonLabel;
use App\Common\Constants;
use App\companie;
use App\condition_picture;
use App\Console\Commands\DbDump;
use App\custom_quote;
use App\customer;
use App\customer_branche;
use App\customer_data;
use App\CustomerDocumentation;
use App\employee;
use App\establishment_type;
use App\event;
use App\HistoricalCustomer;
use App\Http\Controllers\Catalogs\CommonBranch;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\inspection_picture;
use App\JobCenterSession;
use App\Library\FacturamaLibrary;
use App\Monitoring;
use App\Notifications\LogsNotification;
use App\plague_control;
use App\plague_control_picture;
use App\PriceList;
use App\profile_job_center;
use App\quotation;
use App\service_firms;
use App\service_order;
use App\source_origin;
use App\StationInspection;
use App\treeJobCenter;
use App\UnitSat;
use App\User;
use Carbon\Carbon;
use Entrust;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class CustomerController extends Controller
{
    public function view($jobcenter = 0, Request $request) {
        $dataJobCenter = CommonBranch::getBranchByJobCenter($jobcenter);
        $search = $request->get('search');
        $filterPaginate = $request->get('forPage');
        $customers = customer::where('id_profile_job_center', $dataJobCenter[2])
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy('name');
        $countCustomers = $customers->count();
        $customers = $customers->paginate($filterPaginate);
        $sources = source_origin::where('profile_job_center_id', $dataJobCenter[2])->orderBy('name','asc')->get();
        $establishments = establishment_type::where('profile_job_center_id', $dataJobCenter[2])->orderBy('name','asc')->get();
        $commonLabels = new CommonLabel();
        $labels = $commonLabels->getLabelsByJobCenter($dataJobCenter[2]);

        $customersMain = customer::where('is_main', 1)
            ->where('id_profile_job_center', $dataJobCenter[2])
            ->orderBy('name')
            ->get();

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach ($customers as $customer) {
            $customers->map(function($customer) {
                $quotations = DB::table('quotations as q')->where('id_customer', $customer->id)
                    ->where('id_status', 4)->get();
                $servicesOrder = DB::table('quotations as q')->where('id_customer', $customer->id)
                    ->join('service_orders as os','os.id_quotation','q.id')->get();
                $customerData = DB::table('customer_datas')->where('customer_id',$customer->id)->first();
                if (!$customerData) dd($customer);
                $customer->quotationsCount = $quotations->count();
                $customer->servicesOrderCount = $servicesOrder->count();
                $customer->address = $customerData->address;
                $customer->addressNumber = $customerData->address_number;
                $customer->state = $customerData->state;
                $customer->email = $customerData->email;
                $customer->billing = $customerData->billing;
                $customer->rfc = $customerData->rfc;
                $customer->tax_residence = $customerData->tax_residence;
                $customer->expense_type = $customerData->expense_type;
                $customer->contact_two_name = $customerData->contact_two_name;
                $customer->contact_two_phone = $customerData->contact_two_phone;
                $customer->email_billing = $customerData->email_billing;
                $customer->cfdi_type = $customerData->cfdi_type;
                $customer->payment_method = $customerData->payment_method;
                $customer->payment_way = $customerData->payment_way;
                $customer->billing_mode = $customerData->billing_mode;
                $unitCodeBillingData = UnitSat::find($customerData->unit_code_billing);
                $unitCodeBilling = $customerData->unit_code_billing == null || $customerData->unit_code_billing == '' ? 'E48 - Unidad de Servicio': $unitCodeBillingData->key . ' - ' . $unitCodeBillingData->name;
                $customer->unit_code_billing = $customerData->unit_code_billing;
                $customer->unit_code_billing_name = $unitCodeBilling;
                $customer->description_billing = $customerData->description_billing;
                $customer->amount_billing = $customerData->amount_billing;
                $customer->date_billing = $customerData->date_billing;
                $customer->no_invoices = $customerData->no_invoices;
                $customer->service_code_billing = $customerData->service_code_billing;
                $customer->service_code_billing_name = $customerData->service_code_billing_name;
                $customer->billing_street = $customerData->billing_street;
                $customer->billing_interior_number = $customerData->billing_interior_number;
                $customer->billing_exterior_number = $customerData->billing_exterior_number;
                $customer->billing_postal_code = $customerData->billing_postal_code;
                $customer->billing_colony = $customerData->billing_colony;
                $customer->billing_municipality = $customerData->billing_municipality;
                $customer->billing_state = $customerData->billing_state;
                $customer->fiscal_regime = $customerData->fiscal_regime;
            });
        }
        $profileJobCenter = profile_job_center::find($dataJobCenter[2]);

        //Catalogs SAT
        $typesCfdi = FacturamaLibrary::getCatalogsCfdiUses();
        $paymentForms = FacturamaLibrary::getCatalogsPaymentForms();
        $paymentMethods = FacturamaLibrary::getCatalogsPaymentMethods();
        $modesBilling = BillingMode::all();
        $company = companie::find(Auth::user()->companie);

        return view('adminlte::customers.index')->with([
            'customers' => $customers,
            'customersMain' => $customersMain,
            'countCustomers' => $countCustomers,
            'jobCenters' => $dataJobCenter[1],
            'jobCenterSession' => $dataJobCenter[0],
            'sources' => $sources,
            'establishments' => $establishments,
            'profileJobCenter' => $profileJobCenter,
            'labels' => $labels,
            'typesCfdi' => $typesCfdi,
            'paymentForms' => $paymentForms,
            'paymentMethods' => $paymentMethods,
            'modesBilling' => $modesBilling,
            'company' => $company
            ]);
    }

    public function changePassword(Request $request){
        try {
            $idCustomer = $request->get('idCustomer');
            $customer = customer::find($idCustomer);
            $user = User::find($customer->user_id);
            $user->password = bcrypt('PestWareApp2021@');
            $user->save();

            return response()->json([
                'code' => 201,
                'message' => 'Se reseteo correctamente la contraseña.'
            ]);
        }
        catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function CustomerUser(Request $request){
        try {
            $idCustomer = $request->get('idCustomer');
            $customer = customer::find($idCustomer);

            if ($customer->cellphone == null){
                return response()->json([
                    'code' => 500,
                    'message' => 'El cliente no cuenta con número (Teléfono).'
                ]);
            }

            $user = new User;
            $user->name = $customer->name;
            $user->cellphone = $customer->cellphone;
            $user->password = bcrypt('PestWareApp2021@');
            $user->confirmed = 1;
            $user->companie = Auth::user()->companie;
            $user->introjs = 0;
            $user->id_plan = 0;
            $user->save();

            $customer->user_id = $user->id;
            $customer->save();

            // Add default documents for portal MIP
            CommonCustomer::createDefaultDocuments($idCustomer);

            return response()->json([
                'code' => 201,
                'title' => 'Cuenta Agregada al Portal',
                'message' => 'Se creó correctamente.'
            ]);
        }
        catch (Exception $exception) {
            return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function editCustomerBranch(Request $request){
        try {
            $idCustomerBranch = $request->get('idCustomerBranch');
            $customerBranch = customer_branche::find($idCustomerBranch);
            $customerBranch->name = $request->get('nameCustomerBranch');
            $customerBranch->manager = $request->get('managerCustomerBranch');
            $customerBranch->phone = $request->get('phoneCustomerBranch');
            $customerBranch->email = $request->get('emailCustomerBranch');
            $customerBranch->address = $request->get('streetCustomerBranch');
            $customerBranch->address_number = $request->get('numberCustomerBranch');
            $customerBranch->colony = $request->get('colonyCustomerBranch');
            $customerBranch->municipality = $request->get('municipalityCustomerBranch');
            $customerBranch->state = $request->get('stateCustomerBranch');
            $customerBranch->postal_code = $request->get('codePostalCustomerBranch');
            $customerBranch->save();

            $events = Event::join('service_orders as so','events.id_service_order','so.id')
                ->where('so.customer_branch_id', $customerBranch->id)
                ->select('events.title','so.id_service_order','events.id')
                ->get();

            $customer = customer::find($customerBranch->customer_id);
            foreach ($events as $event){
                $event = event::find($event->id);
                $title = $customer->name . ' - ' . $customerBranch->name;
                $event->title = $title;
                $event->save();
            }

            return response()->json([
                'code' => 201,
                'message' => 'Se guardo correctamente la sucursal.'
            ]);
        }
        catch (Exception $exception) {
            return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function createCustomerMain(Request $request){
        try {
            DB::beginTransaction();

            $employee = employee::where('employee_id', Auth::user()->id)->first();
            $customer = customer::where('cellphone', $request->get('phoneCustomerBranch'))
                ->where('id_profile_job_center', $employee->profile_job_center_id)
                ->where('cellphone', '<>', null)
                ->first();
            if ($customer) {
                return response()->json([
                    'code' => 500,
                    'message' => 'El Número esta asociado a otro cliente.'
                ]);
            }

            $address = $request->get('address');
            $exteriorNumber = $request->get('exteriorNumber');
            $interiorNumber = $request->get('interiorNumber');
            $postalCode = $request->get('postalCode');
            $colony = $request->get('colony');
            $municipality = $request->get('municipality');
            $state = $request->get('state');

            $customerMain = new customer();
            $customerMain->name = $request->get('nameCustomerMain');
            $customerMain->establishment_name = $request->get('contactCustomerMain');
            $customerMain->colony = $request->get('colonyCustomerMain');
            $customerMain->municipality = $request->get('municipalityCustomerMain');
            $showPrice = $request->get('showPriceOrderService');
            $showPriceValue = 0;
            if ($showPrice == "true") $showPriceValue = 1;
            $customerMain->show_price = $showPriceValue;
            $customerMain->days_expiration_certificate = $request->get('daysExpirationCertificateCustomer');
            $customerMain->cellphone = str_replace(' ', '', $request->get('phoneCustomerBranch'));
            if ($customerMain->user_id != null) {
                $user = User::find($customerMain->user_id);
                $user->cellphone = str_replace(' ', '', $request->get('phoneCustomerBranch'));
                $user->save();
            }
            $customerMain->cellphone_main = str_replace(' ', '', $request->get('phoneMainCustomerBranch'));
            $customerMain->establishment_id = $request->get('establishment');
            $customerMain->source_origin_id = $request->get('sourceOrigin');
            $customerMain->id_profile_job_center = $employee->profile_job_center_id;
            $customerMain->companie = Auth::user()->companie;
            $isMainCheckValue = 1;
            $isMainCheck = $request->get('isMainCheck');
            if ($isMainCheck == "true") $isMainCheckValue = 0;
            $customerMain->is_main = $isMainCheckValue;
            if ($isMainCheckValue == 0) $customerMain->customer_main_id = $request->get('customerMainSelect');
            $customerMain->save();

            $customerData = new customer_data();
            $customerData->customer_id = $customerMain->id;
            $customerData->email = $request->get('emailCustomerMain');
            $customerData->phone_number = str_replace(' ', '', $customerMain->cellphone);
            $customerData->address = $request->get('addressCustomerMain');
            $customerData->address_number = $request->get('addressNumberCustomerMain');
            $customerData->state = $request->get('stateCustomerMain');
            $customerData->billing = $request->get('billingCustomerMain');
            $customerData->rfc = $request->get('rfcCustomerMain');
            $customerData->tax_residence = $request->get('taxResidenceCustomerMain');
            $customerData->contact_two_name = $request->get('contactTwoCustomerMain');
            $customerData->contact_two_phone = $request->get('contactTwoPhoneCustomerMain');
            $customerData->email_billing = $request->get('emailBilling');
            if ($request->get('modeBilling') == null) $customerData->billing_mode = 0;
            else $customerData->billing_mode = $request->get('modeBilling');
            $customerData->cfdi_type = $request->get('typeCfdiCustomerMainNew');
            $customerData->payment_method = $request->get('methodPayCustomerMainNew');
            $customerData->payment_way = $request->get('wayPayCustomerMainNew');
            if ($request->get('modeBilling') == Constants::$BILLING_MODE_INVOICE_BY_AMOUNT) {
                $customerData->service_code_billing = $request->get('codeServiceSat');
                $customerData->service_code_billing_name = $request->get('codeServiceNameSat');
                $customerData->unit_code_billing = $request->get('unitServiceSat');
                $customerData->description_billing = $request->get('descriptionBilling');
                $customerData->amount_billing = $request->get('amountByInvoice');
                $customerData->no_invoices = $request->get('quantityInvoices');
                $customerData->date_billing = $request->get('dateInvoice');
            }
            $customerData->billing_street = $address;
            $customerData->billing_interior_number = $interiorNumber;
            $customerData->billing_exterior_number = $exteriorNumber;
            $customerData->billing_postal_code = $postalCode;
            $customerData->billing_colony = $colony;
            $customerData->billing_municipality = $municipality;
            $customerData->billing_state = $state;
            $customerData->fiscal_regime = $request->get('regimeFiscalCustomerMainNew');
            $customerData->save();

            $portalChecked = $request->get('portalCustomerMain');
            if ($portalChecked == "true")
            {
                $user = new User();
                $user->name = $customerMain->name;
                $user->cellphone = $customerMain->cellphone;
                $user->password = bcrypt('PestWareApp2021@');
                $user->confirmed = 1;
                $user->companie = Auth::user()->companie;
                $user->introjs = 0;
                $user->id_plan = 0;
                $user->save();

                //Update Customer User
                $customerMain->user_id = $user->id;
                $customerMain->save();
            }

            DB::commit();
            return response()->json([
                'code' => 201,
                'message' => 'Se guardo correctamente el Cliente.'
            ]);
        }
        catch (Exception $exception) {
            DB::rollBack();
            return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function editCustomerMain(Request $request){
        try {
            DB::beginTransaction();

            $address = $request->get('address');
            $exteriorNumber = $request->get('exteriorNumber');
            $interiorNumber = $request->get('interiorNumber');
            $postalCode = $request->get('postalCode');
            $colony = $request->get('colony');
            $municipality = $request->get('municipality');
            $state = $request->get('state');

            $idCustomerMain = SecurityController::decodeId($request->get('idCustomerClient'));
            $customerMain = customer::find($idCustomerMain);
            $customerData = customer_data::where('customer_id',$idCustomerMain)->first();
            $customerMain->name = $request->get('nameCustomerMain');
            $customerMain->establishment_name = $request->get('contactCustomerMain');
            $customerMain->colony = $request->get('colonyCustomerMain');
            $customerMain->municipality = $request->get('municipalityCustomerMain');
            $showPrice = $request->get('showPriceOrderService');
            $showPriceValue = 0;
            if ($showPrice == "true") $showPriceValue = 1;
            $customerMain->show_price = $showPriceValue;
            $isMainCheckValue = 1;
            $isMainCheck = $request->get('isMainCheck');
            if ($isMainCheck == "true") $isMainCheckValue = 0;
            $customerMain->is_main = $isMainCheckValue;
            if ($isMainCheckValue == 0) $customerMain->customer_main_id = $request->get('customerMainSelect');
            $customerMain->days_expiration_certificate = $request->get('daysExpirationCertificateCustomer');
            $customerData->email = $request->get('emailCustomerMain');
            $customerData->address = $request->get('addressCustomerMain');
            $customerData->address_number = $request->get('addressNumberCustomerMain');
            $customerData->state = $request->get('stateCustomerMain');
            $customerData->billing = $request->get('billingCustomerMain');
            $customerData->rfc = $request->get('rfcCustomerMain');
            $customerData->tax_residence = $request->get('taxResidenceCustomerMain');
            $customerData->contact_two_name = $request->get('contactTwoCustomerMain');
            $customerData->contact_two_phone = $request->get('contactTwoPhoneCustomerMain');
            $customerData->payment_method = $request->get('methodPayCustomerMainNew');
            $customerData->payment_way = $request->get('wayPayCustomerMainNew');
            $customerData->cfdi_type = $request->get('typeCfdiCustomerMainNew');
            $customerData->billing_mode = $request->get('modeBilling');
            $customerData->email_billing = $request->get('emailBillingCustomerMain');
            $customerData->billing_street = $address;
            $customerData->billing_interior_number = $interiorNumber;
            $customerData->billing_exterior_number = $exteriorNumber;
            $customerData->billing_postal_code = $postalCode;
            $customerData->billing_colony = $colony;
            $customerData->billing_municipality = $municipality;
            $customerData->billing_state = $state;
            $customerData->fiscal_regime = $request->get('regimeFiscalCustomerMainEdit');
            if ($request->get('modeBilling') == Constants::$BILLING_MODE_INVOICE_BY_AMOUNT) {
                /*$dateInvoice = Carbon::parse($request->get('dateInvoice'));
                $isValidDateInvoice = Carbon::now()->subDay()->gt($dateInvoice);
                if ($isValidDateInvoice) {
                    return response()->json([
                        'code' => 500,
                        'message' => 'La Fecha de Facturación no puede ser de días pasados.'
                    ]);
                }*/
                $customerData->service_code_billing = $request->get('codeServiceSat');
                $customerData->service_code_billing_name = $request->get('codeServiceNameSat');
                $customerData->unit_code_billing = $request->get('unitServiceSat');
                $customerData->description_billing = $request->get('descriptionBilling');
                $customerData->amount_billing = $request->get('amountByInvoice');
                $customerData->no_invoices = $request->get('quantityInvoices');
                $customerData->date_billing = $request->get('dateInvoice');
            }
            $customer = customer::where('cellphone', $request->get('phoneCustomerBranch'))
                ->where('id_profile_job_center', $customerMain->id_profile_job_center)
                ->where('id', '!=', $customerMain->id)
                ->where('cellphone', '<>', null)
                ->first();
            if ($customer) {
                return response()->json([
                    'code' => 500,
                    'message' => 'El Número esta asociado a otro cliente.'
                ]);
            }
            $customerMain->cellphone = $request->get('phoneCustomerBranch');
            if ($customerMain->user_id != null) {
               if (strlen($request->get('phoneCustomerBranch')) > 0) {
                   $user = User::find($customerMain->user_id);
                   $user->cellphone = $request->get('phoneCustomerBranch');
                   $user->save();
               }
               else {
                   return response()->json([
                       'code' => 500,
                       'message' => 'El teléfono no puede ir vacío, cliente ya tiene cuenta portal.'
                   ]);
               }
            }
            $customerMain->cellphone_main = $request->get('phoneMainCustomerBranch');
            $customerMain->save();
            $customerData->save();

            // Update Event Title Customer Branch
            $customerBranch = customer_branche::where('customer_id', $idCustomerMain)->first();
            if ($customerBranch) {
                $events = Event::join('service_orders as so','events.id_service_order','so.id')
                    ->where('so.customer_branch_id', $customerBranch->id)
                    ->select('events.title','so.id_service_order','events.id')
                    ->get();
                $customer = customer::find($customerBranch->customer_id);
                foreach ($events as $event){
                    $event = event::find($event->id);
                    $title = $customer->name . ' - ' . $customerBranch->name;
                    $event->title = $title;
                    $event->save();
                }
            }

            DB::commit();
            return response()->json([
                'code' => 201,
                'message' => 'Se guardo correctamente el Cliente.'
            ]);
        }
        catch (Exception $exception) {
            DB::rollBack();
            return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function customerDelete(Request $request){
        try {
            $idCustomer = SecurityController::decodeId($request->get('idCustomer'));
            $customer = customer::find($idCustomer);
            // Middleware to deny access to data from another company
            $company = Auth::user()->companie;
            if ($customer->companie != $company || empty($customer)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal. Usuario bloqueado.'
                ]);
            }

            // Delete Portal
            $user = User::find($customer->user_id);
            if ($user) {
                $user->delete();
            }
            DB::table('customers')->select('id')->where('id', $idCustomer)->delete();

            return response()->json([
                'code' => 201,
                'message' => 'Se eliminó el cliente.'
            ]);
        }
        catch (Exception $exception) {
            return response()->json(
                [
                'code' => 500,
                'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function customerBranchDelete(Request $request){
        try {
            DB::beginTransaction();
            $idBranchCustomer = SecurityController::decodeId($request->get('idBranchCustomer'));
            $customerBranch = customer_branche::find($idBranchCustomer);
            $quotation = quotation::find($customerBranch->id_quotation);
            $customer = customer::find($customerBranch->customer_id);

            // Middleware to deny access to data from another company
            $company = Auth::user()->companie;
            if ($customer->companie != $company || empty($customer)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal. Usuario bloqueado.'
                ]);
            }

            service_order::where('customer_branch_id',$idBranchCustomer)->delete();
            $quotation->id_status = 10;
            $quotation->save();
            //DB::table('customer_branches')->select('id')->where('id', $idBranchCustomer)->delete();
            DB::commit();
            return response()->json([
                'code' => 201,
                'message' => 'Se eliminó la sucursal.'
            ]);
        }
        catch (Exception $exception) {
            DB::rollBack();
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo'
                ]
            );
        }
    }

    public function getServicesOrdersByCustomerId($customerId){
        try {
            $customerId = SecurityController::decodeId($customerId);
            $customer = customer::find($customerId);
            $deletePermissionCan = Entrust::can('Eliminar Datos');
            $deletePermissionHasRole = Entrust::hasRole('Cuenta Maestra');
            if ($customerId == 8737) {
                $customerBranches = DB::table('customer_branches')
                    ->where('customer_id',$customerId)
                    ->whereDate('created_at', '>=', '2022-09-01')
                    ->get();
            }
            else $customerBranches = DB::table('customer_branches')->where('customer_id',$customerId)->get();
            if ($customerBranches){
                foreach ($customerBranches as $customerBranch) {
                    $customerBranches->map(function ($customerBranch){
                        $concept = custom_quote::where('id_quotation', $customerBranch->id_quotation)
                            ->where('concept', $customerBranch->type)
                            ->where('id_type_concepts', 3)
                            ->first();
                        if ($concept) $customerBranch->isBranch = 1;
                        else {
                            $customerBranch->isBranch = 0;
                            $customerBranch->id_quotation_folio = 'C-#';
                            if ($customerBranch->id_quotation != null){
                                $quotation = quotation::find($customerBranch->id_quotation);
                                if ($quotation) $customerBranch->id_quotation_folio = $quotation->id_quotation;
                                else $customerBranch->id_quotation_folio = 'C-#';
                            }
                        }
                        $customerBranch->idEncrypt = SecurityController::encodeId($customerBranch->id);
                    });
                }
            }

            return response()->json([
                'code' => 200,
                'customerBranches' => $customerBranches,
                'deletePermissionCan' => $deletePermissionCan,
                'deletePermissionHasRole' => $deletePermissionHasRole,
                'customer' => $customer
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getDataCustomerBranch($idCustomerBranch){
        try {
            $customerBranch = customer_branche::find($idCustomerBranch);

            return response()->json([
                'code' => 200,
                'customerBranch' => $customerBranch,
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function getServicesByBranchId($branchId) {
        try {
            $branchId = SecurityController::decodeId($branchId);
            $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
            $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
            $services = DB::table('events as ev')
                ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                ->join('employees as emp', 'ev.id_employee', 'emp.id')
                ->where('so.customer_branch_id', $branchId)
                ->select('so.id_service_order', 'ev.initial_date', 'ev.final_date', 'ev.initial_hour',
                    'ev.final_hour', 'emp.name as technician', 'emp.id as id_technician', 'ev.id', 'ev.id_status')
                ->where('ev.id_status','<>',4)
                ->get();
            $technicians = employee::where('profile_job_center_id', $profileJobCenter->id)->get();
            $deletePermissionCan = Entrust::can('Eliminar Datos');
            $deletePermissionHasRole = Entrust::hasRole('Cuenta Maestra');

            return response()->json([
                'code' => 200,
                'services' => $services,
                'technicians' => $technicians,
                'deletePermissionCan' => $deletePermissionCan,
                'deletePermissionHasRole' => $deletePermissionHasRole
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function updateServicesByBranch(Request $request) {
        try {
            $events = json_decode($request->get('events'));
            $customerBranchId = SecurityController::decodeId($request->get('customerBranchId'));

            $eventQuotation = event::find($events[0]->id);
            if ($eventQuotation == null) {
                return response()->json([
                    'code' => 500,
                    'message' => 'No se puede editar debido a que no hay ninguna OS.'
                ]);
            }
            $serviceOrderQuotation = service_order::find($eventQuotation->id_service_order);
            $quotation = quotation::find($serviceOrderQuotation->id_quotation);

            foreach ($events as $event) {

                $technicianId = $event->technicianId;
                $initialHour = $event->timeInitialId;
                $finalHour = $event->timeFinalId;
                $initialDate = $event->initialDateId;
                $finalDate = $event->finalDateId;
                $id = $event->id;


                $eventSave = event::find($id);
                if (Entrust::can('Eliminar Datos') || Entrust::hasRole('Cuenta Maestra')) {
                    $deleteOrder = $event->deleteOrderId;
                    if ($deleteOrder == 'true') {
                        service_order::find($eventSave->id_service_order)->delete();
                    }
                }

                $eventSave->id_employee = $technicianId;
                $eventSave->initial_hour = $initialHour;
                $eventSave->final_hour = $finalHour;
                $eventSave->initial_date = $initialDate;
                $eventSave->final_date = $finalDate;
                $eventSave->save();

            }

            $serviceOrder = service_order::where('customer_branch_id',$customerBranchId)->get();
            if ($serviceOrder->count() == 0) {
                service_order::where('customer_branch_id',$customerBranchId)->delete();
                DB::table('customer_branches')->select('id')->where('id', $customerBranchId)->delete();
                $quotation->id_status = 10;
                $quotation->save();
            }

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    //Temp
    public function matchCustomersData()
    {
        try {
            DB::beginTransaction();
            // BIOFIN FUMIGACIONES
            customer::where('companie', 37)
                ->update(['id_profile_job_center' => 73]);
            $quotations = quotation::where('id_job_center', 255)->get();
            foreach ($quotations as $quotation) customer::find($quotation->id_customer)->update(['id_profile_job_center' => 255]);

            // EXPERT FUMIGACIONES
            customer::where('companie', 232)
                ->update(['id_profile_job_center' => 252]);

            // FUMIPLAG
            customer::where('companie', 191)
                ->update(['id_profile_job_center' => 212]);

            // SAR CONTROL DE PLAGAS
            customer::where('companie', 216)
                ->update(['id_profile_job_center' => 237]);

            // MB CONTROL DE PLAGAS
            customer::where('companie', 231)
                ->update(['id_profile_job_center' => 251]);
            $quotations = quotation::where('id_job_center', 257)->get();
            foreach ($quotations as $quotation) customer::find($quotation->id_customer)->update(['id_profile_job_center' => 257]);

            // ECO FUMIGACIONES
            customer::where('companie', 244)
                ->update(['id_profile_job_center' => 268]);
            DB::commit();

            return \Response::json([
                'code' => 200,
                'message' => 'Customers updated successfully.'
            ]);

        } catch (Exception $exception) {
            DB::rollBack();
            return \Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

    //Temp
    public function matchHistoricalCustomersData(Request $request)
    {
        try {
            DB::beginTransaction();

            $company = $request->get('companyId');
            $customers = customer::where('companie', $company)->get();

            foreach ($customers as $customer) {

                // Cotizaciones Seguimientos
                $tracings = quotation::join('tracings','tracings.id_quotation','=', 'quotations.id')
                    ->select('tracings.tracing_date','tracings.tracing_hour','tracings.commentary','quotations.id_quotation','quotations.user_id')
                    ->where('quotations.id_customer', $customer->id)
                    ->get();

                foreach ($tracings as $tracing) {
                    if (strlen($tracing->commentary) > 0) {
                        $historicalCustomer = new HistoricalCustomer();
                        $historicalCustomer->id_customer = $customer->id;
                        $historicalCustomer->id_user = $tracing->user_id;
                        $historicalCustomer->title = 'Seguimiento Cotización: ' . $tracing->id_quotation;
                        $historicalCustomer->commentary = $tracing->commentary;
                        $historicalCustomer->date = Carbon::parse($tracing->tracing_date)->toDateString();
                        $historicalCustomer->hour = Carbon::parse($tracing->tracing_hour)->toTimeString();
                        $historicalCustomer->save();
                    }
                }

                // Cotizaciones Rechazadas
                $rejected_quotations = quotation::join('rejected_quotations','rejected_quotations.id_quotation','=','quotations.id')
                    ->select('rejected_quotations.commentary', 'rejected_quotations.id_quotation','rejected_quotations.created_at','quotations.user_id','rejected_quotations.reason')
                    ->where('quotations.id_customer', $customer->id)
                    ->get();

                foreach ($rejected_quotations as $rejected_quotation) {
                    $historicalCustomer = new HistoricalCustomer();
                    $historicalCustomer->id_customer = $customer->id;
                    $historicalCustomer->id_user = $rejected_quotation->user_id;
                    $historicalCustomer->title = 'Rechazo Cotización: ' . $rejected_quotation->id_quotation;
                    $historicalCustomer->commentary = 'Razón: ' . $rejected_quotation->reason . ' ' . $rejected_quotation->commentary;
                    $historicalCustomer->date = Carbon::parse($rejected_quotation->created_at)->toDateString();
                    $historicalCustomer->hour = Carbon::parse($rejected_quotation->created_at)->toTimeString();
                    $historicalCustomer->save();
                }

                // Inspección del Lugar
                $place_inspections = quotation::join('service_orders as so','so.id_quotation','quotations.id')
                    ->join('place_inspections as pi','pi.id_service_order','so.id')
                    ->join('events as e','e.id_service_order','so.id')
                    ->join('employees as em', 'e.id_employee','em.id')
                    ->select('so.id_service_order','pi.commentary', 'pi.created_at','em.employee_id as user_id')
                    ->where('quotations.id_customer', $customer->id)
                    ->get();

                foreach ($place_inspections as $place_inspection) {
                    if (strlen($place_inspection->commentary) > 0) {
                        $historicalCustomer = new HistoricalCustomer();
                        $historicalCustomer->id_customer = $customer->id;
                        $historicalCustomer->id_user = $place_inspection->user_id;
                        $historicalCustomer->title = 'Inspección del Lugar (Orden De Servicio): ' . $place_inspection->id_service_order;
                        $historicalCustomer->commentary = $place_inspection->commentary;
                        $historicalCustomer->date = Carbon::parse($place_inspection->created_at)->toDateString();
                        $historicalCustomer->hour = Carbon::parse($place_inspection->created_at)->toTimeString();
                        $historicalCustomer->save();
                    }
                }

                // Condiciones del Lugar
                $place_conditions = quotation::join('service_orders as so','so.id_quotation','quotations.id')
                    ->join('place_conditions as pc','pc.id_service_order','so.id')
                    ->join('events as e','e.id_service_order','so.id')
                    ->join('employees as em', 'e.id_employee','em.id')
                    ->select('so.id_service_order','pc.commentary', 'pc.created_at','em.employee_id as user_id')
                    ->where('quotations.id_customer', $customer->id)
                    ->get();

                foreach ($place_conditions as $place_condition) {
                    if (strlen($place_condition->commentary) > 0) {
                        $historicalCustomer = new HistoricalCustomer();
                        $historicalCustomer->id_customer = $customer->id;
                        $historicalCustomer->id_user = $place_condition->user_id;
                        $historicalCustomer->title = 'Condiciones del Lugar (Orden De Servicio): ' . $place_condition->id_service_order;
                        $historicalCustomer->commentary = $place_condition->commentary;
                        $historicalCustomer->date = Carbon::parse($place_condition->created_at)->toDateString();
                        $historicalCustomer->hour = Carbon::parse($place_condition->created_at)->toTimeString();
                        $historicalCustomer->save();
                    }
                }

                // Control de Plagas
                $plague_controls = quotation::join('service_orders as so','so.id_quotation','quotations.id')
                    ->join('plague_controls as pc','pc.id_service_order','so.id')
                    ->join('events as e','e.id_service_order','so.id')
                    ->join('employees as em', 'e.id_employee','em.id')
                    ->select('so.id_service_order','pc.commentary', 'pc.created_at','em.employee_id as user_id')
                    ->where('quotations.id_customer', $customer->id)
                    ->get();

                foreach ($plague_controls as $plague_control) {
                    if (strlen($plague_control->commentary) > 0) {
                        $historicalCustomer = new HistoricalCustomer();
                        $historicalCustomer->id_customer = $customer->id;
                        $historicalCustomer->id_user = $plague_control->user_id;
                        $historicalCustomer->title = 'Control de Plagas (Orden De Servicio): ' . $plague_control->id_service_order;
                        $historicalCustomer->commentary = $plague_control->commentary;
                        $historicalCustomer->date = Carbon::parse($plague_control->created_at)->toDateString();
                        $historicalCustomer->hour = Carbon::parse($plague_control->created_at)->toTimeString();
                        $historicalCustomer->save();
                    }
                }

                // Pagos
                $cashes = quotation::join('service_orders as so','so.id_quotation','quotations.id')
                    ->join('cashes as c','c.id_service_order','so.id')
                    ->join('events as e','e.id_service_order','so.id')
                    ->join('employees as em', 'e.id_employee','em.id')
                    ->where('quotations.id_customer', $customer->id)
                    ->select('so.id_service_order','c.commentary','c.updated_at','em.employee_id as user_id','c.amount_received')
                    ->get();

                foreach ($cashes as $cash) {
                    $historicalCustomer = new HistoricalCustomer();
                    $historicalCustomer->id_customer = $customer->id;
                    $historicalCustomer->id_user = $cash->user_id;
                    $historicalCustomer->title = 'Pago (Orden De Servicio): ' . $cash->id_service_order;
                    $historicalCustomer->commentary = 'Monto Recibido: $' . $cash->amount_received . ' ' . $cash->commentary;
                    $historicalCustomer->date = Carbon::parse($cash->updated_at)->toDateString();
                    $historicalCustomer->hour = Carbon::parse($cash->updated_at)->toTimeString();
                    $historicalCustomer->save();
                }

                // Evaluar
                $ratings_service_orders = quotation::join('service_orders as so','so.id_quotation','quotations.id')
                    ->join('ratings_service_orders as rso','rso.id_service_order','so.id')
                    ->where('quotations.id_customer', $customer->id)
                    ->select('so.id_service_order','rso.comments','rso.created_at','rso.user_id')
                    ->get();

                foreach ($ratings_service_orders as $ratings_service_order) {
                    $historicalCustomer = new HistoricalCustomer();
                    $historicalCustomer->id_customer = $customer->id;
                    $historicalCustomer->id_user = $ratings_service_order->user_id;
                    $historicalCustomer->title = 'Evaluación (Orden De Servicio): ' . $ratings_service_order->id_service_order;
                    $historicalCustomer->commentary = 'Calificación: ' . $ratings_service_order->rating . 'estrallas. ' . $ratings_service_order->comments;
                    $historicalCustomer->date = Carbon::parse($ratings_service_order->created_at)->toDateString();
                    $historicalCustomer->hour = Carbon::parse($ratings_service_order->created_at)->toTimeString();
                    $historicalCustomer->save();
                }

                // Cancelar Servicio
                $canceled_service_orders = quotation::join('service_orders as so','so.id_quotation','quotations.id')
                    ->join('canceled_service_orders as cso','cso.id_service_order','so.id')
                    ->where('quotations.id_customer', $customer->id)
                    ->select('so.id_service_order', 'cso.commentary','cso.created_at','quotations.user_id','cso.reason')
                    ->get();

                foreach ($canceled_service_orders as $canceled_service_order) {
                    $historicalCustomer = new HistoricalCustomer();
                    $historicalCustomer->id_customer = $customer->id;
                    $historicalCustomer->id_user = $canceled_service_order->user_id;
                    $historicalCustomer->title = 'Cancelación (Orden De Servicio): ' . $canceled_service_order->id_service_order;
                    $historicalCustomer->commentary = 'Razon: ' . $canceled_service_order->reason . ' ' . $canceled_service_order->comments;
                    $historicalCustomer->date = Carbon::parse($canceled_service_order->created_at)->toDateString();
                    $historicalCustomer->hour = Carbon::parse($canceled_service_order->created_at)->toTimeString();
                    $historicalCustomer->save();
                }

                $service_orders = quotation::join('service_orders as so','so.id_quotation','quotations.id')
                    ->where('quotations.id_customer', $customer->id)
                    ->where('so.tracing_comments', '<>', null)
                    ->select('so.id_service_order','so.tracing_comments','so.updated_at')
                    ->get();

                foreach ($service_orders as $service_order) {
                    if (strlen($service_order->tracing_comments) > 0) {
                        $historicalCustomer = new HistoricalCustomer();
                        $historicalCustomer->id_customer = $customer->id;
                        $historicalCustomer->id_user = $customer->user_id;
                        $historicalCustomer->title = 'Seguimiento de Cliente (Portal): ' . $service_order->id_service_order;
                        $historicalCustomer->commentary = $service_order->tracing_comments;
                        $historicalCustomer->date = Carbon::parse($service_order->updated_at)->toDateString();
                        $historicalCustomer->hour = Carbon::parse($service_order->updated_at)->toTimeString();
                        $historicalCustomer->save();
                    }
                }

                $monitoring = Monitoring::where('id_customer', $customer->id)->first();
                if ($monitoring) {
                    $inspections = StationInspection::where('id_monitoring', $monitoring->id)->get();
                    foreach ($inspections as $inspection) {
                        $historicalCustomer = new HistoricalCustomer();
                        $historicalCustomer->id_customer = $customer->id;
                        $historicalCustomer->id_user = $monitoring->id_user;
                        $historicalCustomer->title = 'Monitoreo de Estaciones: ' . $inspection->id_inspection;
                        $historicalCustomer->commentary = 'Inspección Realizada de Equipos.';
                        $historicalCustomer->date = Carbon::parse($inspection->updated_at)->toDateString();
                        $historicalCustomer->hour = Carbon::parse($inspection->updated_at)->toTimeString();
                        $historicalCustomer->save();
                    }
                }

            }

            DB::commit();

            return \Response::json([
                'code' => 200,
                'message' => 'Customers updated successfully.'
            ]);

        } catch (Exception $exception) {
            DB::rollBack();
            User::first()->notify(new LogsNotification($exception->getMessage(), $exception->getLine(), 3));
            return \Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function matchCustomerDocumentation()
    {
        try {
            DB::beginTransaction();
            $customers = customer::where('user_id', '<>', null)->get();
            foreach ($customers as $customer) {
                CommonCustomer::createDefaultDocuments($customer->id);
            }
            DB::commit();
            return \Response::json([
                'code' => 200,
                'message' => 'Customers updated successfully.'
            ]);
        } catch (Exception $exception) {
            DB::rollBack();
            return \Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function matchUrlPhotos(){

        $inspectionsPictures = inspection_picture::all();
        foreach ($inspectionsPictures as $inspectionsPicture) {
            $urlSeparate = substr($inspectionsPicture->file_route,-44);
            $ip = inspection_picture::find($inspectionsPicture->id);
            $ip->file_route = $urlSeparate;
            $ip->save();
        }

       $conditionsPictures = condition_picture::all();
        foreach ($conditionsPictures as $conditionsPicture) {
            $urlSeparate = substr($conditionsPicture->file_route,-43);
            $ip = condition_picture::find($conditionsPicture->id);
            $ip->file_route = $urlSeparate;
            $ip->save();
        }

        $plagueControlsPictures = plague_control_picture::all();
        foreach ($plagueControlsPictures as $plagueControlsPicture) {
            $urlSeparate = substr($plagueControlsPicture->file_route,-42);
            $ip = plague_control_picture::find($plagueControlsPicture->id);
            $ip->file_route = $urlSeparate;
            $ip->save();
        }

        $cashPictures = cash_picture::all();
        foreach ($cashPictures as $cashPicture) {
            $urlSeparate = substr($cashPicture->file_route,-31);
            $ip = cash_picture::find($cashPicture->id);
            $ip->file_route = $urlSeparate;
            $ip->save();
        }

        $servicesFirmsPictures = service_firms::all();
        foreach ($servicesFirmsPictures as $servicesFirmsPicture) {
            $urlSeparate = substr($servicesFirmsPicture->file_route,-32);
            $ip = service_firms::find($servicesFirmsPicture->id);
            $ip->file_route = $urlSeparate;
            $ip->save();
        }

        $checkListImages = CheckListImage::all();
        foreach ($checkListImages as $checkListImage) {
            $dir = substr(strrchr($checkListImage->urlImage, "/"), 1);
            $ip = CheckListImage::find($checkListImage->id);
            $ip->urlImage = 'services/stations/' . $dir;
            $ip->save();
        }

        $areaInspectionPhotos = AreaInspectionPhotos::all();
        foreach ($areaInspectionPhotos as $areaInspectionPhoto) {
            $dir = substr(strrchr($areaInspectionPhoto->photo, "/"), 1);
            $ip = AreaInspectionPhotos::find($areaInspectionPhoto->id);
            $ip->photo = 'services/inspections_areas/' . $dir;
            $ip->save();
        }

        $employeesFirms = employee::all();
        foreach ($employeesFirms as $employeesFirm) {
            if ($employeesFirm->file_route_firm != null) {
                $dir = substr(strrchr($employeesFirm->file_route_firm, "/"), 1);
                $ip = employee::find($employeesFirm->id);
                $ip->file_route_firm = 'profile/firms/' . $dir;
                $ip->save();
            }
        }

        $usersPhotos = User::all();
        foreach ($usersPhotos as $usersPhoto) {
            if ($usersPhoto->profile_photo != null) {
                $dir = substr(strrchr($usersPhoto->profile_photo, "/"), 1);
                $ip = User::find($usersPhoto->id);
                $ip->profile_photo = 'profile/photos/' . $dir;
                $ip->save();
            }
        }
    }

    public function sendNotificationUpdateApp() {
        $users = User::where('device_token', '<>', null)->get();
        foreach ($users as $user) {
            fcm()
                ->to([$user->device_token])
                ->notification([
                    'title' => "Nueva versión disponible.",
                    'body' => "Te notificamos que ya se encuentra disponible la nueva versión PestWare App, por lo que te recomendamos actualizar tu aplicación.",
                    'sound' => 'default',
                ])
                ->send();
        }
    }

    public function legendPriceList() {
        $legends = PriceList::all();
        foreach ($legends as $legend) {
            $legend->legend = "*Cotización válida por 30 días naturales \n*Cotización única e intransferible. \n*Sujeta a restricciones. \n*No aplica ninguna otra promoción o descuento. \n*Precio más IVA.";
            $legend->save();
        }
        dd('ok');
    }

}
