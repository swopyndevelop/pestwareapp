<?php


namespace App\Http\Controllers\Customers;


use App\CustomerDocumentation;
use Illuminate\Database\Eloquent\Model;

class CommonCustomer
{
    public static function createDefaultDocuments($customerId)
    {
        $defaultDocuments = [
            [
                'section_name' => "PLAN MAESTRO",
                'subtitle' => "Manejo Integral de Plagas",
                'description' => "Investigación, procesos, formatos y documentación necesaria para garantizar servicios eficaces y de calidad.",
                'url' => 'default_doc'
            ],
            [
                'section_name' => "Licencia Sanitaria",
                'subtitle' => "",
                'description' => "Anexo de licencia sanitaria",
                'url' => 'default_doc'
            ],
            [
                'section_name' => "Calendario de Visitas",
                'subtitle' => "",
                'description' => "Servicios Programados Del Año",
                'url' => 'default_doc'
            ],
            [
                'section_name' => "Certificados de Fumigación",
                'subtitle' => "",
                'description' => "Anexo de todos los certificados y constancias",
                'url' => '/services'
            ],
            [
                'section_name' => "Ordenes de Servicio",
                'subtitle' => "",
                'description' => "Anexo de todas las ordenes de servicio",
                'url' => '/orders'
            ],
            [
                'section_name' => "Información de Plaguicidas",
                'subtitle' => "Hojas de Seguridad y Fichas Técnicas de Plaguicidas.",
                'description' => "Anexo de todos los documentos de plaguicidas.",
                'url' => '/products'
            ],
            [
                'section_name' => "Reporte de Inspecciones",
                'subtitle' => "",
                'description' => "Adjuntos de Monitoreo de Estaciones.",
                'url' => '/inspections'
            ],
            [
                'section_name' => "Reporte Gráfico de Inspecciones",
                'subtitle' => "",
                'description' => "Adjuntos Gráficos de Monitoreo de Estaciones.",
                'url' => '/inspections'
            ]
        ];

        foreach ($defaultDocuments as $key => $defaultDocument) {
            $document = new CustomerDocumentation();
            $document->id_customer = $customerId;
            $document->section_name = $defaultDocument['section_name'];
            $document->subtitle = $defaultDocument['subtitle'];
            $document->description = $defaultDocument['description'];
            $document->url_document = $defaultDocument['url'];
            $document->position = ++$key;
            $document->save();
        }
    }
}