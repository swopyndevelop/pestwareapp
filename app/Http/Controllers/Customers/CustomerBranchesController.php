<?php

namespace App\Http\Controllers\Customers;

use App\custom_quote;
use App\customer;
use App\customer_branche;
use App\event;
use App\Http\Controllers\CustomServices\CommonCustomServices;
use App\Http\Controllers\Security\SecurityController;
use App\quotation;
use App\service_order;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Protection;

/**
 * Class CustomerBranchesController
 * @package App\Http\Controllers\Customers
 */
class CustomerBranchesController extends Controller
{

    private $message = "Sucursales importadas correctamente.";
    private $title = "Datos Importados";
    private $type = "success";

    /**
     * Export format with preview branches
     * @param $quotationId
     * @return RedirectResponse
     */
    public function exportFormat($quotationId) {
        try {
            $quote = quotation::find($quotationId);
            $folio = explode('-', $quote->id_quotation);
            $fileName = 'Sucursales C-' . $folio[1];
            $concepts = custom_quote::where('id_quotation', $quotationId)->get();
            $data = array();
            $keyBranche = 0;

            foreach ($concepts as $concept) {
                $arrayQuantity = array_fill(0, $concept->quantity, '');
                foreach ($arrayQuantity as $key => $item) {
                    array_push($data, array(
                        'clave' => ++$keyBranche,
                        'nombre' => 'Suc. Ejemplo',
                        'tipo' => $concept->concept,
                        'telefono' => '',
                        'correo' => 'correo@ejemplo.com',
                        'responsable' => '',
                        'calle' => '',
                        'numero' => '',
                        'colonia' => '',
                        'municipio' => '',
                        'estado' => '',
                        'codigo_postal' => ''
                    ));
                }
            }
            Excel::create('Formato Sucursales', function($excel) use ($data, $fileName, $keyBranche) {

                $excel->sheet($fileName, function($sheet) use ($data, $keyBranche) {
                    $sheet->getProtection()->setPassword('OHdT4$uKs%W@');
                    $sheet->getProtection()->setSheet(true);
                    $rangeProtection = 'A2:B' . ++$keyBranche;
                    $rangeProtectionTwo = 'D2:L' . $keyBranche;
                    $sheet->getStyle($rangeProtection)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                    $sheet->getStyle($rangeProtectionTwo)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                    $sheet->fromArray($data);
                });
            })->export('xlsx');
            return redirect()->back()->with(['success' => 'Plantilla Descargada', 'message' => 'Complete la plantilla.']);
        }catch (\Exception $exception) {
            return redirect()->back()->with(['error' => 'Error al Descargar', 'message' => 'Intente de nuevo.']);
        }
    }

    public function importFromExcel($customerId) {
        try {
            // Get requests
            $id = $customerId;
            $document = request('fileImport');

            // Upload file document
            $path = $this->uploadFileInStorage($document, $id);
            $this->import($path, $id);

            return redirect()->back()->with([$this->type => $this->title, 'message' => $this->message]);
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'Error al Importar', 'message' => $this->message]);
        }
    }

    private function uploadFileInStorage($file, $customerId) {
        return Storage::putFileAs(
            'public/branches', $file, $customerId . '.xlsx'
        );
    }

    public function import($path, $id) {

        if ($path != null) {
            $pathComplete = 'storage/app/' . $path;
            try {
                Excel::load($pathComplete, function($reader) use ($id) {

                    // Getting all results
                    $rows = $reader->get();
                    $this->message = count($rows);
                    if (count($rows) == 0) {
                        $this->title = "Formato vacío";
                        $this->message = "El formato debe incluir sucursales.";
                        $this->type = "warning";
                        //return redirect()->back()->with(['warning' => 'El archivo esta vacío.', 'message' => 'El formato debe incluir sucursales.']);
                    } else if ($this->validateFormat($rows)) { // Validate format document
                        customer_branche::where('customer_id', $id)->delete();
                        foreach ($rows as $row) {
                            if ($row->clave != null) {
                                $branch = new customer_branche;
                                $branch->customer_id = $id;
                                $branch->branch_id = $row->clave;
                                $branch->name = $row->nombre;
                                $branch->type = $row->tipo;
                                $branch->manager = $row->responsable;
                                $branch->phone = $row->telefono;
                                $branch->email = $row->correo;
                                $branch->address_number = $row->numero;
                                $branch->address = $row->calle;
                                $branch->colony = $row->colonia;
                                $branch->municipality = $row->municipio;
                                $branch->state = $row->estado;
                                $branch->postal_code = $row->codigo_postal;
                                $branch->save();
                            }
                        }
                    }else{
                        $this->title = "Formato incorrecto";
                        $this->message = "Revise el formato e intente de nuevo.";
                        $this->type = "warning";
                    }

                });
            }catch (\Exception $exception) {
                $this->title = "Error al importar";
                $this->message = "No ha sido posible importar los datos, intente de nuevo.";
                $this->type = "error";
            }
        }else {
            $this->title = "Error de archivo";
            $this->message = "No ha sido posible cargar el archivo.";
            $this->message = "error";
        }

    }

    private function validateFormat($columns) {
        if (count($columns) > 0)
            if ($columns[0]->clave && $columns[0]->nombre && $columns[0]->tipo &&
                $columns[0]->calle && $columns[0]->calle && $columns[0]->numero &&
                $columns[0]->colonia && $columns[0]->municipio && $columns[0]->estado &&
                $columns[0]->codigo_postal)
                return true;
        return false;
    }

    // saved new customer branches
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $json = $data['concepts'];
            $array = json_decode($json);

            foreach($array as $obj){
                $update = customer_branche::where('branch_id', $obj->key)
                    ->where('id_quotation', $request->get('quotationId'))
                    ->where('customer_id', $data['customerId'])
                    ->first();
                if ($update) $branch = customer_branche::find($update->id);
                else $branch = new customer_branche;
                $branch->branch_id = $obj->key;
                $branch->customer_id = $data['customerId'];
                $branch->name = $obj->name;
                $branch->manager = $obj->manager;
                $branch->phone = $obj->phone;
                $branch->email = $obj->email;
                $branch->address_number = $obj->address_number;
                $branch->address = $obj->address;
                $branch->colony = $obj->colony;
                $branch->municipality = $obj->municipality;
                $branch->state = $obj->state;
                $branch->postal_code = $obj->postal_code;
                $branch->save();
            }

            return response()->json([
                'code' => 201,
                'message' => 'Sucursales guardadas correctamente.'
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    /**
     * Create all service orders
     * @param Request $request
     * @return JsonResponse
     */
    public function createServiceOrders(Request $request) {

        try {

            DB::beginTransaction();

            $data = $request->all();
            $quotationId = $data['id_quotation'];
            $customerId = $data['customerId'];
            $employeeId = $data['technician'];
            $paymentMethod = $data['paymentMethod'];
            $paymentWay = $data['paymentWay'];
            $date = $data['date'];
            $initialHour = $data['hour'];
            $finalHour = Carbon::parse($initialHour)->addHour()->toTimeString();

            $customQuotations = custom_quote::where('id_quotation', $quotationId)->get();
            $quotation = quotation::find($quotationId);
            $customer = customer::find($customerId);
            $folioOrder = "OS-" . $quotationId;
            $folioId = 0;

            // validations
            $countBranches = customer_branche::where('customer_id', $customerId)->get()->count();

            if ($countBranches != CustomerBranchesController::getCountBranchesCustom($quotationId)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'El número de sucursales no coincide con la cantidad de conceptos registrados.'
                ]);
            }

            foreach ($customQuotations as $concept) {

                $arrayTermMonth = array_fill(0, $concept->term_month, '');
                $arrayFrecuencyMonth = array_fill(0, $concept->frecuency_month, '');
                $arrayQuantity = array_fill(0, $concept->quantity, '');

                $branches = customer_branche::where('customer_id', $customerId)
                    ->where('type', $concept->concept)
                    ->get();

                /** @noinspection PhpUnusedLocalVariableInspection */
                foreach ($arrayTermMonth as $month) {
                    /** @noinspection PhpUnusedLocalVariableInspection */
                    foreach ($arrayFrecuencyMonth as $frecuency) {
                        foreach ($arrayQuantity as $key => $item) {
                            $branche = $branches->get($key);
                            $completeFolio = $folioOrder . "-" . ++$folioId;
                            $order = CommonCustomServices::createServiceOrder($completeFolio, $quotationId, $paymentMethod, $paymentWay,
                                $quotation, $branche, $concept, '');
                            if ($order) CommonCustomServices::createEvent($customer, $branche, $employeeId, $order, $date, $initialHour,
                                $finalHour, $quotation);
                        }
                    }
                }
            }

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Ordenes de servicio Guardadas.'
            ]);
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function updateServiceOrder(Request $request) {
        try {
            $data = $request->all();
            $event = event::find($data['id_event']);
            $event->initial_date = $data['dateStart'];
            $event->final_date = $data['dateEnd'];
            $event->initial_hour = $data['hourStart'];
            $event->final_hour = $data['hourEnd'];
            $event->id_employee = $data['techniciansEvent'];
            $event->save();
            $message = $event->title . " actualizado.";
            return redirect()->back()->with(['success' => 'Datos Actualizados', 'message' => $message]);
        } catch (Exception $exception) {
            $message = "No ha sido posible actualizar el servicio.";
            return redirect()->back()->with(['error' => 'Error al Actualizar', 'message' => $message]);
        }
    }

    public function deleteServiceOrders(Request $request) {
        try {
            $quotationId = $request->get('quotationId');
            $quote = quotation::find($quotationId);
            $company = Auth::user()->companie;

            // Middleware to deny access to data from another company
            if ($quote->companie != $company || empty($quote)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal. Usuario bloqueado.'
                ]);
            }

            service_order::where('id_quotation', $quotationId)
                ->where('companie', $company)->delete();

            return response()->json([
                'code' => 201,
                'message' => 'Ordenes de servicio Eliminadas.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal.'
            ]);
        }
    }

    public function scheduleCustomQuotation(Request $request) {
        try {
            $data = $request->all();
            $quotation = quotation::find($data['quotationId']);
            $quotation->id_status = 4;
            $quotation->save();
            return response()->json([
                'code' => 201,
                'message' => 'Ordenes de servicio Programadas.'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public static function getCountBranchesCustom ($quotationId) {
        return intval(custom_quote::where('id_quotation', $quotationId)
            ->where('id_type_concepts', '<>', 2)
            ->select(DB::raw('SUM(quantity) as total'))
            ->first()->total);
    }
}
