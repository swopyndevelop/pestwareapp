<?php

namespace App\Http\Controllers\Customers;

use App\companie;
use App\customer;
use App\CustomerDocumentation;
use App\event;
use App\Http\Controllers\Security\SecurityController;
use App\Jobs\DownloadMipDocuments;
use App\service_order;
use Carbon\Carbon;
use Exception;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Response;
use PDF;

class CustomerDocumentationController extends Controller
{
    /**
     * Get all documents for MIP folder by customer id.
     * @param $customerId
     * @return JsonResponse
     */
    public function getAllDocumentsByCustomerId($customerId)
    {
        try {
            $customerId = SecurityController::decodeId($customerId);
            $customer = customer::find($customerId);
            $documents = CustomerDocumentation::where('id_customer', $customerId)
                ->orderBy('position')->get();
            $company = companie::find($customer->companie);
            return Response::json([
                'code' => 201,
                'documents' => $documents,
                'company' => $company
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal. Intenta de nuevo.'
            ]);
        }
    }

    /**
     * Add a new document to the client's mip folder.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $customerId = $request->get('id_customer');
            $customerId = SecurityController::decodeId($customerId);
            $sectionName = $request->get('section_name');
            $subtitle = $request->get('subtitle');
            $description = $request->get('description');
            $position = $request->get('position');
            $file = $request->file('documentFile');

            if (!$file->getSize()) {
                return Response::json([
                    'code' => 500,
                    'message' => 'El archivo es demasiado grande.'
                ]);
            }

            if ($position == 0) {
                $latest = CustomerDocumentation::where('id_customer', $customerId)->latest('id')->first();
                $position = ++$latest->position;
            }

            $document = new CustomerDocumentation();
            $document->id_customer = $customerId;
            $document->section_name = $sectionName;
            $document->subtitle = $subtitle;
            $document->description = $description;
            $document->url_document = "url";
            $document->position = $position;
            $document->save();
            $this->validateAndCreateDirectoryForDocument($customerId, $file, $document->id);
            return Response::json([
                'code' => 201,
                'message' => 'Se agrego el documento a la carpeta del cliente.'
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    /**
     * Validates if the client's mip folder exists and saves the new document.
     * @param $customerId
     * @param $file
     * @param $id
     */
    private function validateAndCreateDirectoryForDocument($customerId, $file, $id)
    {
        // Validate if exist directory
        $now = Carbon::now();
        $currentYear = $now->year;
        $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $customerId;
        $storagePathNewDocuments = $storagePathMip . '/' . $currentYear . '/attachments/';

        if (!file_exists($storagePathMip)) Storage::makeDirectory('customer_portal/mip/' . $customerId);
        if (!file_exists($storagePathNewDocuments)) {
            Storage::makeDirectory('customer_portal/mip/' . $customerId . '/' . $currentYear . '/attachments/');
        }

        // Save document in directory.
        $filename = $file->store('customer_portal/mip/' . $customerId . '/' . $currentYear . '/attachments');
        $docFile = CustomerDocumentation::find($id);
        $docFile->url_document = $filename;
        $docFile->save();
    }

    /**
     * Get all documents for MIP folder by customer id.
     * @return JsonResponse
     */
    public function getSeparateDocumentsByCustomerId(Request $request)
    {

        try {
            $customerId = SecurityController::decodeId($request->get('customerId'));
            $customer = customer::find($customerId);
            $document = $request->get('fileDocument');
            $initialDate = $request->get('initial_date');
            $finalDate = $request->get('final_date');
            $year = \Carbon\Carbon::parse($initialDate)->year;
            $data = ['Certificados de Servicio'];
            if ($document == 1) $document = '/services/';
            if ($document == 2) {
                $document = '/orders/';
                $data = ['Ordenes de Servicio'];
            }

            /*
            //DownloadMipDocuments::dispatch($customer, $year, $document, $data)->onConnection('database');
            //DownloadMipDocuments::dispatch();
            return Response::json([
                'code' => 200,
                'message' => 'Generado Correctamente'
            ]);*/


            if ($customer->is_main === 0) {
                $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $customer->id . '/' . $year . $document;

                if (file_exists($storagePathMip)) {
                    $nameCover = 'Servicios';
                    self::makeCovers($customer, $year, $data, $nameCover);

                    $filesWithoutExtension = collect(Storage::files('customer_portal/mip/' . $customer->id . '/' . $year . $document))
                        ->map(function ($file) {
                            return pathinfo($file, PATHINFO_FILENAME);
                        })
                        ->toArray();

                    $serviceOrders = Event::join('service_orders as so', 'events.id_service_order', 'so.id')
                        ->where('so.id_job_center', $customer->id_profile_job_center)
                        ->whereIn('so.id_service_order', $filesWithoutExtension)
                        ->whereBetween('events.initial_date', [$initialDate, $finalDate])
                        ->select('events.initial_date', 'so.id_service_order')
                        ->get();

                    $serviceOrdersSorted = $serviceOrders->sortBy('initial_date');

                    $servicesOrdersByDateInitial = $serviceOrdersSorted->map(function ($arrayService) use ($customer, $year) {
                        return 'customer_portal/mip/' . $customer->id . '/' . $year . '/services/' . $arrayService->id_service_order . '.pdf';
                    })->toArray();

                    $merger = new Merger(new TcpdiDriver);
                    $routeCover = 'customer_portal/mip/' . $customer->id . '/' . $year . '/' . $nameCover . '-' . $customer->id . '.pdf';
                    $merger->addFile(storage_path() . '/app/' . $routeCover);
                    foreach ($servicesOrdersByDateInitial as $order) {
                        $merger->addFile(storage_path() . '/app/' . $order);
                    }
                    $mipDoc = $merger->merge();

                    return response()->stream(
                        function () use ($mipDoc) {
                            echo $mipDoc;
                        },
                        200,
                        [
                            'Content-Type' => 'application/pdf',
                            'Content-Disposition' => 'attachment; filename=Certificados_AÑO_' . $year . '.pdf',
                        ]
                    );

                }

                else {
                    return Response::json([
                        'code' => 500,
                        'message' => 'No Hay archivos'
                    ]);
                }
            }

            if ($customer->is_main === 1) {

                $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $customer->id . '/' . $year . $document;
                $merger = new Merger(new TcpdiDriver);

                if (file_exists($storagePathMip)) {
                    $nameCover = 'Servicios';
                    self::makeCovers($customer, $year, $data, $nameCover);

                    $filesWithoutExtension = collect(Storage::files('customer_portal/mip/' . $customer->id . '/' . $year . $document))
                        ->map(function ($file) {
                            return pathinfo($file, PATHINFO_FILENAME);
                        })
                        ->toArray();

                    $serviceOrders = Event::join('service_orders as so', 'events.id_service_order', 'so.id')
                        ->where('so.id_job_center', $customer->id_profile_job_center)
                        ->whereIn('so.id_service_order', $filesWithoutExtension)
                        ->whereBetween('events.initial_date', [$initialDate, $finalDate])
                        ->select('events.initial_date', 'so.id_service_order')
                        ->get();

                    $serviceOrdersSorted = $serviceOrders->sortBy('initial_date');

                    $merger = new Merger(new TcpdiDriver);
                    $routeCover = 'customer_portal/mip/' . $customer->id . '/' . $year . '/' . $nameCover . '-' . $customer->id . '.pdf';
                    $merger->addFile(storage_path() . '/app/' . $routeCover);
                    foreach ($serviceOrdersSorted as $order) {
                        $merger->addFile(storage_path() . '/app/' . $order);
                    }
                }

                $customersBranches = customer::where('is_main', 0)
                    ->where('customer_main_id', $customer->id)
                    ->get();

                if (!$customersBranches->isEmpty()) {
                    foreach ($customersBranches as $customersBranch) {
                        $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $customersBranch->id . '/' . $year . $document;
                        if (file_exists($storagePathMip)) {
                            $nameCover = 'Servicios';
                            self::makeCovers($customersBranch, $year, $data, $nameCover);
                            $orders = Storage::files('customer_portal/mip/' . $customersBranch->id . '/' . $year . $document);
                            $routeCover = 'customer_portal/mip/' . $customersBranch->id . '/' . $year . '/' . $nameCover . '-' . $customersBranch->id . '.pdf';
                            $merger->addFile(storage_path() . '/app/' . $routeCover);
                            foreach ($orders as $order) {
                                $merger->addFile(storage_path() . '/app/' . $order);
                            }
                        }

                    }
                }

                if (!empty($merger->merge())) {
                    $mipDoc = $merger->merge();

                    return response()->stream(
                        function () use ($mipDoc) {
                            echo $mipDoc;
                        },
                        200,
                        [
                            'Content-Type' => 'application/pdf',
                            'Content-Disposition' => 'attachment; filename=Certificados_AÑO_' . $year . '.pdf',
                        ]
                    );

                }
                else {
                    return Response::json([
                        'code' => 500,
                        'message' => 'No hay Archivos'
                    ]);
                }

            }

        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => $exception->getMessage()
            ]);
        }
    }

    private static function makeCovers($customer, $year, $data, $cover)
    {

        $company = companie::find($customer->companie);
        $pdf_logo = env('URL_STORAGE_FTP') . $company->pdf_logo;
        PDF::loadView('vendor.adminlte.customers.templateCoverMip',
            [
                'customer' => $customer,
                'company' => $company,
                'pdf_logo' => $pdf_logo,
                'data' => $data
            ]
        )->save(storage_path() . '/app/customer_portal/mip/' . $customer->id . '/' . $year . '/' . $cover . '-' . $customer->id . '.pdf');

    }


}
