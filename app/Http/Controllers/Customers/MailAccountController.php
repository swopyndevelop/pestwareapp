<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\MailAccount;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

class MailAccountController extends Controller
{
    /**
     * Get all mail accounts by customer id.
     * @param $customerId
     * @return JsonResponse
     */
    public function getMailAccountsByCustomerId($customerId) {
        try {
            $customerId = SecurityController::decodeId($customerId);
            $accounts = MailAccount::where('id_customer', $customerId)->get();
            return Response::json([
                'code' => 200,
                'accounts' => $accounts
            ]);
        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    /**
     * Add new mail account in customer.
     * @param Request $request
     * @return JsonResponse
     */
    public function addNewMailAccount(Request $request) {
        try {
            // Get data request.
            $customerId = SecurityController::decodeId($request->get('customerId'));
            $document = $request->get('document');
            $name = $request->get('name');
            $job = $request->get('job');
            $email = $request->get('email');

            // Create at new mail account.
            $mailAccount = new MailAccount();
            $mailAccount->id_customer = $customerId;
            $mailAccount->document = $document;
            $mailAccount->name = $name;
            $mailAccount->job = $job;
            $mailAccount->email = $email;
            $mailAccount->save();

            return Response::json([
                'code' => 201,
                'message' => 'Cuenta agregada correctamente.'
            ]);

        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    /**
     * Update mail account by id.
     * @param Request $request
     * @return JsonResponse
     */
    public function updateMailAccount(Request $request) {
        try {
            // Get data request.
            $id = $request->get('id');
            $document = $request->get('document');
            $name = $request->get('name');
            $job = $request->get('job');
            $email = $request->get('email');

            // Update mail account.
            $mailAccount = MailAccount::find($id);
            $mailAccount->document = $document;
            $mailAccount->name = $name;
            $mailAccount->job = $job;
            $mailAccount->email = $email;
            $mailAccount->save();

            return Response::json([
                'code' => 201,
                'message' => 'Cuenta actualizada correctamente.'
            ]);

        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    /**
     * Delete mail account by id.
     * @param $id
     * @return JsonResponse
     */
    public function deleteMailAccount($id) {
        try {

            // Delete mail account.
            $mailAccount = MailAccount::find($id);
            $mailAccount->delete();

            return Response::json([
                'code' => 201,
                'message' => 'Cuenta eliminada correctamente.'
            ]);

        } catch (Exception $exception) {
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }
}
