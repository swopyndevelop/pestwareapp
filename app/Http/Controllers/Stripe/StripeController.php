<?php

namespace App\Http\Controllers\Stripe;

use App\companie;
use App\employee;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Invoices\InvoiceController;
use App\Notifications\LogsNotification;
use App\Plan;
use App\profile_job_center;
use App\ReportsSlack;
use App\treeJobCenter;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use Stripe\Checkout\Session;
use Stripe\Exception\ApiErrorException;
use Stripe\Stripe;
use Stripe\Webhook;

class StripeController extends Controller
{

    /**
     * StripeController constructor.
     */
    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    private $planEmpresarial = 3;

    public function createCheckoutSession(Request $request)
    {
        try {
            $users = $request->get('users');
            $centers = $request->get('centers');
            $plan = $request->get('plan');

            $company = companie::find(Auth::user()->companie);

            $planIdStripe = Plan::find($plan)->id_price_stripe;
            $userIdStripe = Plan::find(5)->id_price_stripe;
            $centerIdStripe = Plan::find($company->id_plan_center)->id_price_stripe;

            $country = $company->id_code_country;
            if ($country != 2) { // 2 => México
                $planIdStripe = Plan::find(7)->id_price_stripe; // 2 => Emprendedor - USD
                if ($plan == $this->planEmpresarial) {
                    $planIdStripe = Plan::find(8)->id_price_stripe; // 8 => Empresarial -USD
                }
                $userIdStripe = Plan::find(9)->id_price_stripe; // 9 => Usuario Extra - USD
            }

            if (!$planIdStripe) {
                return Response::json([
                    'error' => [
                        'message' => 'El plan no existe.'
                    ]
                ], 400);
            }

            $data = [];
            $dataPlan = [
                'price' => $planIdStripe,
                'quantity' => 1,
            ];
            array_push($data, $dataPlan);
            if ($users > 0) {
                $dataUser = [
                    'price' => $userIdStripe,
                    'quantity' => $users,
                ];
                array_push($data, $dataUser);
            }
            if ($centers > 0) {
                $dataCenter = [
                    'price' => $centerIdStripe,
                    'quantity' => $centers,
                ];
                array_push($data, $dataCenter);
            }

            $checkoutSession = Session::create([
                'success_url' => env('URL_BASE').'stripe/success/session/{CHECKOUT_SESSION_ID}',
                'cancel_url' => env('URL_BASE').'stripe/canceled/session/{CHECKOUT_SESSION_ID}',
                'payment_method_types' => ['card'],
                'mode' => 'subscription',
                'subscription_data' => [
                    'default_tax_rates' => ['txr_1Itg6GHVGkiXew9dHpFbY5hM'],
                ],
                'line_items' => $data,
                'customer_email' => Auth::user()->email,
                'metadata' => ["company" => "123"],
                'client_reference_id' => "123"
            ]);
            return Response::json([
                'sessionId' => $checkoutSession['id']
            ]);
        } catch (ApiErrorException $e) {
            return Response::json([
                'error' => [
                    'message' => $e->getError()->message
                ]
            ], 400);
        }
    }

    public function successCheckoutSession($session_id)
    {
        try {
            $checkoutSession = Session::retrieve($session_id);
            $company = companie::find(\Auth::user()->companie);
            $company->id_stripe_customer = $checkoutSession->customer;
            $company->save();
            return redirect()->route('purchase_plans')->with('success', 'Transacción realizada correctamente.');
        } catch (ApiErrorException $e) {
            return redirect()->route('purchase_plans')->with('error', 'Algo salio mal.');
        }
    }

    public function canceledCheckoutSession($session_id)
    {
        try {
            $checkoutSession = Session::retrieve($session_id);
            $company = companie::find(\Auth::user()->companie);
            $company->id_stripe_customer = $checkoutSession->customer;
            $company->save();
            return redirect()->route('purchase_plans')->with('warning', 'Transacción cancelada por el usuario.');
        } catch (ApiErrorException $e) {
            return redirect()->route('purchase_plans')->with('error', 'Algo salio mal.');
        }
    }

    public function index()
    {
        return view('vendor.adminlte.billing.indexV2');
    }

    public function customerPortal()
    {
        try {

            $stripe_customer_id = companie::find(\Auth::user()->companie)->id_stripe_customer;
            $return_url = env('URL_BASE').'plans/purchase';

            $session = \Stripe\BillingPortal\Session::create([
                'customer' => $stripe_customer_id,
                'return_url' => $return_url,
            ]);

            return Response::json([
                'url' => $session->url
            ]);

        } catch (ApiErrorException $e) {
            return Response::json([
                'error' => [
                    'message' => $e->getError()->message
                ]
            ], 400);
        }
    }

    public function getPricePlan(Request $request)
    {
        $users = $request->get('users');
        $centers = $request->get('centers');
        $plan = $request->get('plan');

        $company = companie::find(Auth::user()->companie);
        $currency = 'MXN';

        $currentPlan = Plan::find($plan);
        $planPrice = Plan::find($plan)->price;
        $userPrice = Plan::find(5)->price;
        $centerPrice = Plan::find($company->id_plan_center)->price;

        $country = $company->id_code_country;
        if ($country != 2) { // 2 => México
            $currency = 'USD';
            $planPrice = Plan::find(7)->price; // 2 => Emprendedor - USD
            if ($currentPlan->id == $this->planEmpresarial) {
                $planPrice = Plan::find(8)->price; // 8 => Empresarial -USD
            }
            $userPrice = Plan::find(9)->price; // 9 => Usuario Extra - USD
        }

        if ($users < $this->getExtraUsers($company, $currentPlan)) {
            return Response::json([
                'code' => 500,
                'message' => 'No puedes restar usuarios de los que tienes actualmente contratados.'
            ]);
        }

        if ($users >= 0 && $centers >= 0 && $planPrice != null && $userPrice > 0 && $centerPrice > 0) {
            $totalUsers = $userPrice * $users;
            $totalCenters = $centerPrice * $centers;
            $price = $planPrice + $totalUsers + $totalCenters;
            $iva = $price * 0.16;
            $total = $price + $iva;
            return Response::json([
                'code' => 200,
                'users' => $totalUsers,
                'centers' => $totalCenters,
                'currency' => $currency,
                'iva' => number_format((float)$iva, 2, '.', ''),
                'price' => number_format((float)$price, 2, '.', ''),
                'total' => number_format((float)$total, 2, '.', '')
            ]);
        }

        return Response::json([
            'code' => 500,
            'message' => 'Algo salio mal, intenta de nuevo.'
        ]);
    }

    public function getCurrentExtrasPlan()
    {
        $company = companie::find(Auth::user()->companie);
        $plan = Plan::find(Auth::user()->id_plan);
        $users = $this->getExtraUsers($company, $plan);
        return Response::json([
            'users' => $users,
            'centers' => $company->no_branch_office - 1,
            'plan' => $plan->id
        ]);
    }

    public function webhook(Request $request)
    {
        $webhookSecret = env('STRIPE_WEBHOOK_SECRET');
        if ($webhookSecret) {
            try {
                $event = Webhook::constructEvent(
                    $request->getContent(),
                    $request->header('stripe-signature'),
                    $webhookSecret
                );
            } catch (\Exception $e) {
                error_log($e->getMessage());
                return Response::json(['error' => $e->getMessage()], 403);
            }
        } else {
            $event = $request->all();
        }
        $type = $event['type'];
        $object = $event['data']['object'];

        switch ($type) {
            case 'checkout.session.completed':
                // Payment is successful and the subscription is created.
                // You should provision the subscription and save the customer ID to your database.
                \Log::info('checkout.session.completed');
                break;
            case 'invoice.paid':
                // Continue to provision the subscription as payments continue to be made.
                // Store the status in your database and check when a user accesses your service.
                // This approach helps you avoid hitting rate limits.
                \Log::info('invoice.paid');
                error_log('Some message here.');
                break;
            case 'charge.succeeded':
                $reportSlack = new ReportsSlack();
                $title = 'NEW CHARGE STRIPE SUCCEEDED: ' . Carbon::now()->toDateTimeString();
                $charge = $event->data->object;
                $status = $charge->status;
                $amount = $charge->amount;
                $customer = $charge->customer;
                $payment_method_details = $charge->payment_method_details;
                $card = $payment_method_details->card;
                $description = $charge->description;
                $body = 'STATUS: ' . $status . ' AMOUNT: ' . $amount . ' DESCRIPTION: ' . $description . ' CARD: ' . $card->brand . ', ' . $card->funding . ', ' . $card->last4;
                $reportSlack->notify(new LogsNotification($title, $body, 1));
                $data = [
                    'status' => $status,
                    'amount' => $amount, //57884
                    'customer' => $customer, //'cus_Jh6UpmmHhaVSUo',
                    'card' => $card,
                    'description' => $description,
                ];
                $response = InvoiceController::createInvoiceForCompany($data);
                $title = 'LOG FACTURA SAT: ' . Carbon::now()->toDateTimeString();
                $reportSlack->notify(new LogsNotification($title, $response['message'], 1));
                break;
            case 'invoice.payment_failed':
                // The payment failed or the customer does not have a valid payment method.
                // The subscription becomes past_due. Notify your customer and send them to the
                // customer portal to update their payment information.
                $employee = DB::table('users as u')
                    ->join('employees as emp', 'emp.employee_id', 'u.id')
                    ->where('u.email', $object['customer_email'])
                    ->first();
                if ($employee) {
                    $company = companie::find($employee->id_company);
                    if ($company->id_stripe_customer == null) {
                        $company->id_stripe_customer = $object['customer'];
                        $company->save();
                    }
                    \Log::info('invoice.payment_failed:'.$company->id);
                }
                break;
            // ... handle other event types
            default:
                // Unhandled event type
        }

        return Response::json(['status' => 'success'], 200);
    }

    private function getExtraUsers($companie, $plan)
    {
        $defaultUsers = 3;
        $extraUsers = 0;
        if ($plan->id == $this->planEmpresarial) $defaultUsers = 6;
        $treeJobCenterMain = treeJobCenter::where('parent', '#')
            ->where('company_id', Auth::user()->companie)->first();
        $profileJobCenterMain = profile_job_center::where('profile_job_centers_id', $treeJobCenterMain->id_inc)->first();

        if ($companie->no_branch_office > 1) {
            $jobCenters = profile_job_center::where('companie', Auth::user()->companie)
                ->where('profile_job_centers_id', '<>', $treeJobCenterMain->id_inc)
                ->get();
            foreach ($jobCenters as $jobCenter) {
                $employees = employee::where('profile_job_center_id', $jobCenter->id)->whereNotIn('status', [200, 300, 500])->count();
                if ($employees > $defaultUsers) $extraUsers += $employees - $defaultUsers;
            }
            $employees = employee::where('profile_job_center_id', $profileJobCenterMain->id)->whereNotIn('status', [200, 300, 500])->count();
            if ($employees > $defaultUsers) $extraUsers += $employees - $defaultUsers;
        } else {
            $employees = employee::where('profile_job_center_id', $profileJobCenterMain->id)->whereNotIn('status', [200, 300, 500])->count();
            if ($employees > $defaultUsers) $extraUsers += $employees - $defaultUsers;
        }
        return $extraUsers;
    }
}
