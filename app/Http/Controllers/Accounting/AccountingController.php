<?php

namespace App\Http\Controllers\Accounting;

use App\address_job_center;
use App\employee;
use App\entry;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Security\SecurityController;
use App\JobCenterSession;
use App\Mail\PurchaseOrderMail;
use App\Mail\QuotatioMail;
use App\product;
use App\Product_Tax;
use App\profile_job_center;
use App\treeJobCenter;
use App\User;
use Doctrine\DBAL\Driver\AbstractDB2Driver;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Response;
use Storage;
use Validator;
use Carbon\Carbon;
use App\provider;
use App\purchase_order;
use App\archive_order;
use App\article_order;
use App\expense;
use App\archive_expense;
use App\article_expense;
use function foo\func;
use PDF;

class AccountingController extends Controller
{
	#region CRUD

    private $PLAN_FREE = 1;
    private $PLAN_ENTREPRENEUR = 2;
    private $PLAN_BUSINESS = 3;

    public function fetchCompany(Request $request){
        if ($request->get('query')){
            $query = $request->get('query');
            $profileJobCenter = $request->get('jobCenter');
            $data = DB::table('providers')->where('company', 'LIKE', "%$query%")->where('profile_job_center_id', $profileJobCenter)->get();
            $output = '<ul class="dropdown-menu" id="listCompany" style="display:block; position:absolute;">';
            foreach ($data as $row){
                $output .= '<li class="cellphone" id="getdataCompany"><a href="#">' . $row->company . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }

    public function loadCompany(Request $request){
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        $provider = DB::table('providers')
            ->where('company', $request->get('company'))
            ->where('profile_job_center_id', $jobCenterSession)->first();
        return response()->json($provider);
    }

    public function index(Request $request, $jobcenter = 0)
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }

        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first();
        $employee = employee::where('employee_id', Auth::user()->id)
            ->join('profile_job_centers as pjc', 'employees.profile_job_center_id', 'pjc.id')
            ->first();
        $profileJobCenter = profile_job_center::find($jobCenterSession->id_profile_job_center);
        $profileJobCenterMain = profile_job_center::find($employee->profile_job_center_id);
        $treeJobCenterMain = treeJobCenter::where('id_inc', $profileJobCenterMain->profile_job_centers_id)->first();
        $profileJobCenterRfc = profile_job_center::find($employee->profile_job_center_id)->rfc_country;

        $idJobCenter = 0;
        $jobCenters = DB::table('profile_job_centers')->where('companie', Auth::user()->companie)->get(); //Arreglo para select de job centers
        if ($treeJobCenterMain->parent != '#') $jobCenters = DB::table('profile_job_centers')->where('profile_job_centers_id', $employee->profile_job_centers_id)->get();

        $profileJobCenter = $profileJobCenter->id;
        if ($jobcenter != 0) {
            $profileJobCenter = $jobcenter;
            $jobCenterSession->id_profile_job_center = $profileJobCenter;
            $jobCenterSession->save();
        }

        $search = \Request::get('search');
    	$concepts = DB::table('concepts')->where('profile_job_center_id', $profileJobCenter)->get();
    	$vouchers = DB::table('vouchers')->where('profile_job_center_id', $profileJobCenter)->get();
    	$paymentMethods = DB::table('payment_methods')->where('profile_job_center_id', $profileJobCenter)->get();
    	$paymentWays = DB::table('payment_ways')->where('profile_job_center_id', $profileJobCenter)->orderBy('name', 'asc')->get();
        $employees = DB::table('employees')->where('profile_job_center_id', $profileJobCenter)->where('status', '<>', 200)->get();
        $providers = DB::table('providers')->where('profile_job_center_id', $profileJobCenter)->get();
        $purchaseId = DB::table('purchase_orders')->select('id')->where('id_jobcenter', $profileJobCenter)->orderBy('id', 'DESC')->first();
        $todayDate = Carbon::now()->subMonth(2)->toDateString();
        $tomorrowDate = Carbon::now()->addDay(2)->toDateString();

        $purchaseOrder = DB::table('purchase_orders as po')
            ->join('providers as p', 'po.id_provider', 'p.id')
            ->join('users as u', 'po.id_user', 'u.id')
            ->join('concepts as c', 'po.id_concept', 'c.id')
            ->join('vouchers as v', 'po.id_voucher', 'v.id')
            ->join('payment_methods as pm', 'po.id_payment_method', 'pm.id')
            ->join('payment_ways as pw','po.id_payment_way','pw.id')
            ->select('po.id', 'id_purchase_order as folio', 'po.date_order as date', 'u.name as solicitante', 'p.company as proveedor', 'p.contact_name',
                'p.contact_cellphone', 'description_order as description', 'c.name as concepto', 'po.total', 'po.credit_days', 'v.name as voucher', 'po.created_at',
                'po.account_status', 'po.id_payment_way', 'pm.name as typePayment', 'p.contact_address', 'p.contact_email', 'p.bank', 'p.account_holder', 'p.account_number',
                'p.clabe', 'p.rfc', 'po.status', 'po.has_entry', 'pw.name as name_payment_way')
            ->where('id_jobcenter', $profileJobCenter);

        if ($profileJobCenter == 268) {
            $purchaseOrder->whereDate('po.created_at', '>=', $todayDate)
            ->whereDate('po.created_at', '<=', $tomorrowDate);
        }

        $articlesPurchase = DB::table('article_orders as ao')->where('companie', Auth::user()->companie)
            ->join('products as p', 'ao.id_product', 'p.id')
            ->select('ao.*', 'p.name as product')
            ->get();
        $articlesExpense = DB::table('article_expenses')->where('companie', Auth::user()->companie)->orderBy('concept','asc')->get();
        $archivesPurchase = DB::table('archive_expenses')->where('companie', Auth::user()->companie)->get();

        foreach ($archivesPurchase as $ach){
            $archivesPurchase->map(function ($ach){
                $arrayFile = explode(".", $ach->file_route);
                $ach->extension = $arrayFile[1];
            });
        }

        $expense = DB::table('expenses as ex')
            ->join('users as u', 'ex.id_user', 'u.id')
            ->join('concepts as c', 'ex.id_concept', 'c.id')
            ->join('vouchers as v', 'ex.id_voucher', 'v.id')
            ->join('payment_methods as pm', 'ex.id_payment_method', 'pm.id')
            ->join('payment_ways as pw','ex.id_payment_way','pw.id')
            ->select('ex.id', 'id_expense as folio', 'ex.date_expense as date', 'u.name as solicitante', 'description_expense as description',
                'c.name as concepto', 'ex.total', 'ex.credit_days', 'v.name as voucher', 'ex.created_at', 'ex.account_status', 'ex.id_payment_way',
                'pm.name as typePayment', 'ex.status','pw.name as name_payment_way')
            ->where('id_jobcenter', $profileJobCenter);

        if ($search) {
            $expense->where('ex.id_expense', 'like', '%' . $search . '%');
            $purchaseOrder->where('po.id_purchase_order', 'like', '%' . $search . '%');
        }

        if ($request->initialDateExpense) {

            // Filters
            $initialDate = $request->initialDateExpense;
            $finalDate = $request->finalDateExpense;
            $idEmployee = $request->idEmployee;
            $idProvider = $request->idProvider;
            $description = $request->description;
            $type = $request->type;
            $idConcept = $request->idConcept;
            $total = $request->total;
            $statusPayment = $request->statusPayment;
            $infoPayment = $request->infoPayment;
            $statusAccount = $request->statusAccount;
            $voucher = $request->voucher;

            if ($initialDate != "null" && $finalDate != "null") {
                if ($type == 0) {
                    $expense->whereDate('ex.date_expense', '>=', $initialDate)
                        ->whereDate('ex.date_expense', '<=', $finalDate);
                    $purchaseOrder->whereDate('po.date_order', '>=', $initialDate)
                        ->whereDate('po.date_order', '<=', $finalDate);
                }
                elseif ($type == 1) {
                    $expense->whereDate('ex.date_expense', '>=', $initialDate)
                    ->whereDate('ex.date_expense', '<=', $finalDate);
                }elseif ($type == 2) {
                    $purchaseOrder->whereDate('po.date_order', '>=', $initialDate)
                        ->whereDate('po.date_order', '<=', $finalDate);
                }

            }
            if ($idEmployee != 0) {
                if ($type == 0) {
                    $expense->where('ex.id_user', $idEmployee);
                    $purchaseOrder->where('po.id_user', $idEmployee);
                }
                elseif ($type == 1) $expense->where('ex.id_user', $idEmployee);
                elseif ($type == 2) $purchaseOrder->where('po.id_user', $idEmployee);
            }
            if ($description != null) {
                if ($type == 0) {
                    $expense->where('ex.description_expense', 'like', '%' . $description . '%');
                    $purchaseOrder->where('po.description_order', 'like', '%' . $description . '%');
                }
                elseif ($type == 1) $expense->where('ex.description_expense', 'like', '%' . $description . '%');
                elseif ($type == 2) $purchaseOrder->where('po.description_order', 'like', '%' . $description . '%');
            }
            if ($idConcept != 0) {
                if ($type == 0) {
                    $expense->where('ex.id_concept', $idConcept);
                    $purchaseOrder->where('po.id_concept', $idConcept);
                }
                elseif ($type == 1) $expense->where('ex.id_concept', $idConcept);
                elseif ($type == 2) $purchaseOrder->where('po.id_concept', $idConcept);
            }
            if ($total != null) {
                if ($type == 0) {
                    $expense->where('ex.total', $total);
                    $purchaseOrder->where('po.total', $total);
                }
                elseif ($type == 1) $expense->where('ex.total', $total);
                elseif ($type == 2) $purchaseOrder->where('po.total', $total);
            }
            if ($statusPayment != 0) {
                if ($type == 0) {
                    $expense->where('ex.status', $statusPayment);
                    $purchaseOrder->where('po.status', $statusPayment);
                }
                elseif ($type == 1) $expense->where('ex.status', $statusPayment);
                elseif ($type == 2) $purchaseOrder->where('po.status', $statusPayment);
            }
            if ($infoPayment != 0) {
                if ($type == 0) {
                    $expense->where('ex.id_payment_way', $infoPayment);
                    $purchaseOrder->where('po.id_payment_way', $infoPayment);
                }
                elseif ($type == 1) $expense->where('ex.id_payment_way', $infoPayment);
                elseif ($type == 2) $purchaseOrder->where('po.id_payment_way', $infoPayment);
            }
            if ($statusAccount != 0) {
                if ($type == 0) {
                    $expense->where('ex.account_status', $statusAccount);
                    $purchaseOrder->where('po.account_status', $statusAccount);
                }
                elseif ($type == 1) $expense->where('ex.account_status', $statusAccount);
                elseif ($type == 2) $purchaseOrder->where('po.account_status', $statusAccount);
            }
            if ($voucher != 0) {
                if ($type == 0) {
                    $expense->where('ex.id_voucher', $voucher);
                    $purchaseOrder->where('po.id_voucher', $voucher);
                }
                elseif ($type == 1) $expense->where('ex.id_voucher', $voucher);
                elseif ($type == 2) $purchaseOrder->where('po.id_voucher', $voucher);
            }
            if ($idProvider != 0) $purchaseOrder->where('po.id_provider', $idProvider);

        }

        $expenses = $expense->get();
        $purchaseOrders = $purchaseOrder->get();

        foreach ($purchaseOrders as $po){
            $purchaseOrders->map(function ($po){
                $po->type = 1;
                $imagesMonitoring = DB::table('archive_orders')->where('id_purchase_order', $po->id)->get();
                $po->photosMonitoring = $imagesMonitoring;
                foreach ($imagesMonitoring as $imageMonitoring){
                    $imagesMonitoring->map(function ($imageMonitoring){
                        $imagesMonitoringExt = explode('.', $imageMonitoring->file_route);
                        //$imagesMonitoringExt[1]; //TODO Manuel?
                        $imageMonitoring->fileExtension = $imagesMonitoringExt[1];
                    });
                }
                if ($po->has_entry != 0) {
                    $entries = entry::where('id_purchase', $po->id)->get();
                    $po->entries = $entries;
                }
            });
        }
        foreach ($expenses as $ex){
            $expenses->map(function ($ex){
                $ex->type = 2;
                $imagesMonitoring = DB::table('archive_expenses')->where('id_expense', $ex->id)->get();
                if ($ex->status == 2){
                    $filePayment = DB::table('archive_expenses')->where('type', 3)->where('id_expense', $ex->id)->first();
                    if (!empty($filePayment)) $ex->refund = 1;
                    else $ex->refund = 0;
                }
                $ex->photosMonitoring = $imagesMonitoring;
                foreach ($imagesMonitoring as $imageMonitoring){
                    $imagesMonitoring->map(function ($imageMonitoring){
                        $imagesMonitoringExt = explode('.', $imageMonitoring->file_route);
                        $imagesMonitoringExt[1];
                        $imageMonitoring->fileExtension = $imagesMonitoringExt[1];
                    });
                }
            });
        }

        $accountings = $purchaseOrders->merge($expenses);
        $accountings = $accountings->sortByDesc(function($result) {
            return $result->created_at;
        });

        if ($request->initialDateExpense && $request->type != 0) {
            $type = $request->type;
            if ($type == 1) $accountings = $expenses;
            if ($type == 2) $accountings = $purchaseOrders;
        }


        /* TODO: Ajustar Paginador
        $perPage = 3;
        $currentPage = app('request')->get('page') ?: 1; // or $request->get('page') if available
        $users = User::all();
        $paginatorAccounting = new Paginator($accountings, $accountings->count(), $perPage,$currentPage);
        $paginatorAccounting->withPath(route('index_accounting'));*/

        $symbol_country = CommonCompany::getSymbolByCountry();
        $code_country = CommonCompany::getCodeByCountry();

    	return view('vendor.adminlte.accounting.index')->with(['jobCenters' => $jobCenters, 'paymentMethods' =>
            $paymentMethods, 'paymentWays' => $paymentWays, 'concepts' => $concepts, 'vouchers' => $vouchers,
            'purchaseId' => $purchaseId, 'accountings' => $accountings, 'articlesPurchase' => $articlesPurchase, 'jobCenterSession' => $jobCenterSession,
            'archivesPurchase' => $archivesPurchase, 'article_expenses' => $articlesExpense,'profileJobCenterRfc' => $profileJobCenterRfc,
            'employees' => $employees, 'providers' => $providers, 'symbol_country' => $symbol_country, 'code_country' => $code_country
    	]);
	}

	public function storePurchase(Request $request)
	{
        try {
            DB::beginTransaction();
            if (empty($request->company)) {
                $company = 'no';
            }else{
                $company = $request->get('company');
            }

            $providers = DB::table('providers')
                ->where('company', $company)
                ->where('profile_job_center_id', $request->get('jobCenter'))
                ->first();
            $idUser = Auth::id();
            if(empty($providers)){
                $provider = new provider;
                $provider->company = $request->company;
                $provider->contact_name = $request->contactName;
                $provider->contact_address = $request->contactAddress;
                $provider->contact_cellphone = $request->contactCellphone;
                $provider->contact_email = $request->contactEmail;
                $provider->bank = $request->bank;
                $provider->account_holder = $request->accountHolder;
                $provider->account_number = $request->accountNumber;
                $provider->clabe = $request->clabe;
                $provider->rfc = $request->rfc;
                $provider->companie = Auth::user()->companie;
                $provider->profile_job_center_id = $request->get('jobCenter');
                $provider->save();
            }else{
                $provider = provider::find($providers->id);
                $provider->company = $request->company;
                $provider->contact_name = $request->contactName;
                $provider->contact_address = $request->contactAddress;
                $provider->contact_cellphone = $request->contactCellphone;
                $provider->contact_email = $request->contactEmail;
                $provider->bank = $request->bank;
                $provider->account_holder = $request->accountHolder;
                $provider->account_number = $request->accountNumber;
                $provider->clabe = $request->clabe;
                $provider->rfc = $request->rfc;
                $provider->companie = Auth::user()->companie;
                $provider->profile_job_center_id = $request->get('jobCenter');
                $provider->save();
            }

            //Nueva Orden de Compra
            $purchase = new purchase_order;
            $purchase->id_purchase_order = CommonAccounting::getFolioPurchaseOrder();
            $purchase->id_provider = $provider->id;
            $purchase->id_concept = $request->concept;
            $purchase->date_order = Carbon::now()->toDateString();
            $purchase->id_user = $idUser;
            $purchase->description_order = $request->descriptionOrder;
            $purchase->total = $request->total;
            $purchase->status = 1;
            $purchase->id_payment_way = $request->paymentWay;
            $purchase->credit_days = $request->creditDays;
            $purchase->id_payment_method = $request->paymentMethod;
            $purchase->account_status = 1;
            $purchase->id_voucher = $request->voucher;
            $purchase->id_jobcenter = $request->jobCenter;
            $purchase->companie = Auth::user()->companie;
            $purchase->save();

            //Agregar Articulos
            $json = $request->input('arrayConcepts');
            $array = json_decode($json);

            foreach($array as $obj){
                $concept_order = new article_order;
                $concept_order->id_purchase_order = $purchase->id;
                $concept_order->concept = "NA";
                $concept_order->id_product = $obj->productId;
                $concept_order->quantity = $obj->quantity;
                $concept_order->unit_price = $obj->lastPrice;
                $concept_order->subtotal = $obj->subtotal;
                $concept_order->total = $obj->total;
                $concept_order->companie = Auth::user()->companie;
                $concept_order->save();
                // Update Product
                $product = product::find($obj->productId);
                $product->base_price = $obj->lastPrice;
                $product->save();
            }
            DB::commit();
            return Response::json([
                'code' => 201,
                'message' => 'Orden de compra creada correctamente.'
            ]);

        } catch (Exception $exception) {
            DB::rollBack();
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
	}

	//Gastos
	public function storeExpense(Request $request)
	{

        try {

            DB::beginTransaction();
            $idUser = Auth::id();

		    $expense = new expense;
		    $expense->id_expense = CommonAccounting::getFolioExpense();
		    $expense->expense_name = $request->expense_name;
		    $expense->id_concept = $request->conceptExpense;
		    $expense->date_expense = Carbon::now()->toDateString();
		    $expense->id_user = $idUser;
		    $expense->description_expense = $request->description_expense;
		    $expense->total = $request->total;
            $expense->status = 1;
            $expense->id_payment_way = $request->typePayment;
            $expense->credit_days = $request->daysExpense;
            $expense->id_payment_method = $request->typeMethod;
            $expense->account_status = 1;
            $expense->id_voucher = $request->voucherExpense;
            $expense->id_jobcenter = $request->jobCenter;
            $expense->companie = Auth::user()->companie;
            $expense->save();

            //Articulos
            $art_exp = new article_expense;
            $art_exp->id_expense = $expense->id;
            $art_exp->concept = $request->article;
            $art_exp->total = $request->total;
            $art_exp->companie = Auth::user()->companie;
            $art_exp->save();

            if ($request->comprobantArchive) {
                //Archivos
                //Guardar documentos de la factura
                foreach ($request->comprobantArchive as $f) {
                    $filename = $f->store('expenses', 's3');
                    archive_expense::create([
                        'id_expense' => $expense->id,
                        'file_route' => $filename,
                        'type' => 1,
                        'companie' => Auth::user()->companie
                    ]);
                }
            }

            DB::commit();

            return response()->json([
                'code' => 201,
                'message' => 'Datos Guardados Correctamente'
            ]);

        } catch (\Exception $th) {
            DB::rollBack();
            return response()->json([
                'code' => 500,
                'message' => 'Algo salio mal.'
            ]);

        }

        return redirect()->route('index_accounting');
	}

	public function accountStatus(Request $request){

        $id = $request->get('id');
        $type = $request->get('type');
        $status = $request->get('status');

        if ($type == "1"){
            $purchase = purchase_order::find($id);
            $purchase->account_status = $status;
            $purchase->save();
        }else{
            $expense = expense::find($id);
            $expense->account_status = $status;
            $expense->save();
        }

        return redirect()->route('index_accounting');

    }

    public function download($id, $type)
    {
        if ($type == 2) $facture = archive_expense::find($id);
        else $facture = archive_order::find($id);
        //$ruta = storage_path().'/app/'.$facture->file_route;
        //return response()->download($ruta);

        // Con $s3 guarda una url temporal tokenizada que genera s3 para hacerla publica temporalmente ej. 2 minutos.
        //$s3 = Storage::disk('s3')->temporaryUrl($facture->file_route, Carbon::now()->addDays(2));

        return Storage::disk('s3')->response($facture->file_route);
    }

    public function editPurchase($id){

        try {
            $id = SecurityController::decodeId($id);
            $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
            $concepts = DB::table('concepts')
                ->where('profile_job_center_id', $jobCenterSession)
                ->where('type', 1)
                ->get();
            $vouchers = DB::table('vouchers')->where('profile_job_center_id', $jobCenterSession)->get();
            $paymentMethods = DB::table('payment_methods')->where('profile_job_center_id', $jobCenterSession)->get();
            $paymentWays = DB::table('payment_ways')->where('profile_job_center_id', $jobCenterSession)->orderBy('name', 'asc')->get();

            $purchaseOrder = DB::table('purchase_orders as po')
                ->join('providers as p', 'po.id_provider', 'p.id')
                ->join('users as u', 'po.id_user', 'u.id')
                ->join('concepts as c', 'po.id_concept', 'c.id')
                ->join('vouchers as v', 'po.id_voucher', 'v.id')
                ->join('payment_methods as pm', 'po.id_payment_method', 'pm.id')
                ->select('po.id', 'id_purchase_order as folio', 'po.date_order as date', 'u.name as solicitante', 'p.company as proveedor', 'p.contact_name',
                    'p.contact_cellphone', 'description_order as description', 'c.name as concepto', 'po.total', 'po.credit_days', 'v.name as voucher', 'po.created_at',
                    'po.account_status', 'po.id_payment_way', 'pm.name as typePayment', 'p.contact_address', 'p.contact_email', 'p.bank', 'p.account_holder', 'p.account_number',
                    'p.clabe', 'p.rfc', 'c.id as id_concept', 'v.id as id_voucher', 'pm.id as id_payment_method', 'po.id_payment_way')
                ->where('po.id', $id)
                ->first();

            $articlesPurchase = DB::table('article_orders as ao')
                ->where('ao.id_purchase_order', $id)
                ->join('products as p', 'ao.id_product', 'p.id')
                ->join('product_units as pu', 'p.id_unit', 'pu.id')
                ->select('ao.*', 'p.name as product', 'p.quantity as quantityProduct', 'pu.name as unit')
                ->get();

            foreach ($articlesPurchase as $item) {
                $articlesPurchase->map(function ($item){
                    $taxes = Product_Tax::where('id_product', $item->id_product)
                        ->join('taxes as tx', 'product_tax.id_tax', 'tx.id')
                        ->orderBy('tx.value', 'desc')
                        ->get();
                    $taxesLabel = "Sin impuestos";
                    $taxesValues = 0;
                    if ($taxes->count() > 0) {
                        $taxesLabel = "";
                        $taxesValues = $taxes;
                        foreach ($taxes as $tax) {
                            $taxesLabel = $taxesLabel . $tax->name . ", ";
                        }
                    }
                    $item->taxes = $taxesLabel;
                    $item->taxesValues = $taxesValues;
                });
            }



            $subtotal = 0;
            foreach ($articlesPurchase as $item) $subtotal += $item->subtotal;

            $purchase_order_id = purchase_order::find($id);
            // Middleware to deny access to data from another company
            if ($purchase_order_id->companie != auth()->user()->companie) {
                SecurityController::abort();
            }

            $symbol_country = CommonCompany::getSymbolByCountry();

            return Response::json([
                'code' => 200,
                'paymentMethods' => $paymentMethods,
                'paymentWays' => $paymentWays,
                'concepts' => $concepts,
                'vouchers' => $vouchers,
                'articlesPurchase' => $articlesPurchase,
                'purchaseOrder' => $purchaseOrder,
                'symbol_country' => $symbol_country
            ]);
        } catch (Exception $exception) {
            return \Response::json([
                'code' => 500,
                'message' => 'Error al cargar la compra, intenta de nuevo.'
            ]);
        }

    }

    public function updatePurchase(Request $request)
    {
        try {
            DB::beginTransaction();
            $purchaseOrderId = SecurityController::decodeId($request->get('purchase_id'));
            if (empty($request->company)) {
                $company = 'no';
            }else{
                $company = $request->company;
            }
            $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
            $providers = DB::table('providers')
                ->where('company', $company)
                ->where('profile_job_center_id', $jobCenterSession)
                ->first();
            $idUser = Auth::id();
            if(empty($providers)){
                $provider = new provider;
                $provider->company = $request->company;
                $provider->contact_name = $request->contactName;
                $provider->contact_address = $request->contactAddress;
                $provider->contact_cellphone = $request->contactCellphone;
                $provider->contact_email = $request->contactEmail;
                $provider->bank = $request->bank;
                $provider->account_holder = $request->accountHolder;
                $provider->account_number = $request->accountNumber;
                $provider->clabe = $request->clabe;
                $provider->rfc = $request->rfc;
                $provider->profile_job_center_id = $jobCenterSession;
                $provider->save();
            }else{
                $provider = provider::find($providers->id);
                $provider->company = $request->company;
                $provider->contact_name = $request->contactName;
                $provider->contact_address = $request->contactAddress;
                $provider->contact_cellphone = $request->contactCellphone;
                $provider->contact_email = $request->contactEmail;
                $provider->bank = $request->bank;
                $provider->account_holder = $request->accountHolder;
                $provider->account_number = $request->accountNumber;
                $provider->clabe = $request->clabe;
                $provider->rfc = $request->rfc;
                $provider->save();
            }

            //Actualizar Orden de Compra
            $purchase = purchase_order::find($purchaseOrderId);
            $purchase->id_provider = $provider->id;
            $purchase->id_concept = $request->concept;
            $purchase->date_order = Carbon::now()->toDateString();
            $purchase->id_user = $idUser;
            $purchase->description_order = $request->descriptionOrder;
            $purchase->total = $request->total;
            $purchase->id_payment_way = $request->paymentWay;
            $purchase->credit_days = $request->creditDays;
            $purchase->id_payment_method = $request->paymentMethod;
            $purchase->id_voucher = $request->voucher;
            $purchase->save();

            //Agregar Articulos
            //Actualizar las cotizaciones personalizadas
            DB::table('article_orders')->where('id_purchase_order', $purchaseOrderId)->delete();

            $json = $request->input('arrayConcepts');
            $array = json_decode($json);

            foreach($array as $obj){
                $concept_order = new article_order;
                $concept_order->id_purchase_order = $purchaseOrderId;
                $concept_order->concept = "NA";
                $concept_order->id_product = $obj->productId;
                $concept_order->quantity = $obj->quantity;
                $concept_order->unit_price = $obj->lastPrice;
                $concept_order->subtotal = $obj->subtotal;
                $concept_order->total = $obj->total;
                $concept_order->companie = Auth::user()->companie;
                $concept_order->save();
                // Update Product
                $product = product::find($obj->productId);
                $product->base_price = $obj->lastPrice;
                $product->save();
            }
            DB::commit();
            return Response::json([
                'code' => 200,
                'message' => 'Orden de compra actualizada correctamente.'
            ]);

        } catch (Exception $exception) {
            DB::rollBack();
            return Response::json([
                'code' => 500,
                'message' => 'Algo salio mal, intenta de nuevo.'
            ]);
        }
    }

    public function deletePurchaseOrder(Request $request)
    {
        try {
            // Middleware to deny access to data from another company
            $idPurchaseOrder = SecurityController::decodeId($request->get('idPurchaseOrder'));
            $purchaseOrderCompany = purchase_order::find($idPurchaseOrder)->companie;
            $company = Auth::user()->companie;
            if ($purchaseOrderCompany != $company || empty($purchaseOrderCompany)) {
                return response()->json([
                    'code' => 500,
                    'message' => 'Algo salio mal. Usuario bloqueado.'
                ]);
            }

            DB::table('purchase_orders')->select('id')->where('id', $idPurchaseOrder)->delete();
            return response()->json([
                'code' => 201,
                'message' => 'Se eliminó la Orden de Compra.'
            ]);
        }
        catch (\Exception $exception) {
            return response()->json(
                [
                    'code' => 500,
                    'message' => 'Algo salio mal intentalo de nuevo']
            );
        }
    }

    public function viewPdfPurchaseOrder($id){
        try {
            $purchaseOrderId = SecurityController::decodeId($id);
            $purchaseOrder = purchase_order::join('providers as p','purchase_orders.id_provider', 'p.id')
                ->join('users as u', 'purchase_orders.id_user', 'u.id')
                ->join('concepts as c', 'purchase_orders.id_concept', 'c.id')
                ->join('vouchers as v', 'purchase_orders.id_voucher', 'v.id')
                ->join('payment_methods as pm', 'purchase_orders.id_payment_method', 'pm.id')
                ->join('payment_ways as pw', 'purchase_orders.id_payment_way', 'pw.id')
                ->select('purchase_orders.id', 'id_purchase_order as folio', 'purchase_orders.date_order as date', 'u.name as solicitante', 'p.company as proveedor', 'p.contact_name',
                    'p.contact_cellphone', 'description_order as description', 'c.name as concepto', 'purchase_orders.total', 'purchase_orders.credit_days', 'v.name as voucher', 'purchase_orders.created_at',
                    'purchase_orders.account_status', 'purchase_orders.id_payment_way', 'pw.name as pay_way', 'pm.name as typePayment', 'p.contact_address', 'p.contact_email', 'p.bank', 'p.account_holder', 'p.account_number',
                    'p.clabe', 'p.rfc', 'c.id as id_concept', 'v.id as id_voucher', 'v.name as type_voucher','pm.id as id_payment_method', 'pm.name as payment_method', 'purchase_orders.id_payment_way',
                    'purchase_orders.id_jobcenter','purchase_orders.companie')
                ->where('purchase_orders.id', $purchaseOrderId)
                ->first();

            $company = CommonCompany::getCompanyById($purchaseOrder->companie);
            $symbol_country = CommonCompany::getSymbolByCountryPublic($company->id);
            $jobCenterProfile = profile_job_center::where('id', $purchaseOrder->id_jobcenter)->first();
            $addressProfile = address_job_center::where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

            $articlesPurchase = DB::table('article_orders as ao')
                ->where('ao.id_purchase_order', $purchaseOrder->id)
                ->join('products as p', 'ao.id_product', 'p.id')
                ->select('ao.*', 'p.name as product')
                ->get();

            $subtotal = 0;
            foreach ($articlesPurchase as $item) $subtotal += $item->subtotal;

            $pdfSendProvider = PDF::loadView('vendor.adminlte.accounting._sendPurchasePdfEmail',
                ['company' => $company, 'symbol_country' => $symbol_country, 'jobCenterProfile' => $jobCenterProfile, 'purchaseOrder' => $purchaseOrder,
                    'addressProfile' => $addressProfile, 'articlesPurchase' => $articlesPurchase, 'subtotal' => $subtotal])
                ->setPaper('A4');
            return $pdfSendProvider->stream();
        }
        catch (Exception $e){
            return redirect()->route('index_accounting')->with('error', 'Algo salió mal.');
        }
    }

    public function sendEmailPurchaseOrder(Request $request){
        try {
            $idPurchaseOrderEmail = $request->get('idPurchaseOrderEmail');
            $employee = employee::join('profile_job_centers as pjc','profile_job_center_id','pjc.id')
                ->where('companie', Auth::user()->companie)
                ->first();
            $profileJobCenter = profile_job_center::find($employee->profile_job_center_id);

            $purchaseOrder = purchase_order::join('providers as p','purchase_orders.id_provider', 'p.id')
                ->join('users as u', 'purchase_orders.id_user', 'u.id')
                ->join('concepts as c', 'purchase_orders.id_concept', 'c.id')
                ->join('vouchers as v', 'purchase_orders.id_voucher', 'v.id')
                ->join('payment_methods as pm', 'purchase_orders.id_payment_method', 'pm.id')
                ->join('payment_ways as pw', 'purchase_orders.id_payment_way', 'pw.id')
                ->select('purchase_orders.id', 'id_purchase_order as folio', 'purchase_orders.date_order as date', 'u.name as solicitante', 'p.company as proveedor', 'p.contact_name',
                    'p.contact_cellphone', 'description_order as description', 'c.name as concepto', 'purchase_orders.total', 'purchase_orders.credit_days', 'v.name as voucher', 'purchase_orders.created_at',
                    'purchase_orders.account_status', 'purchase_orders.id_payment_way', 'pw.name as pay_way', 'pm.name as typePayment', 'p.contact_address', 'p.contact_email', 'p.bank', 'p.account_holder', 'p.account_number',
                    'p.clabe', 'p.rfc', 'c.id as id_concept', 'v.id as id_voucher', 'v.name as type_voucher','pm.id as id_payment_method', 'pm.name as payment_method', 'purchase_orders.id_payment_way',
                    'purchase_orders.id_jobcenter', 'purchase_orders.companie')
                ->where('purchase_orders.id', $idPurchaseOrderEmail)
                ->first();

            $company = CommonCompany::getCompanyById($purchaseOrder->companie);
            $symbol_country = CommonCompany::getSymbolByCountryPublic($company->id);
            $jobCenterProfile = profile_job_center::where('id', $purchaseOrder->id_jobcenter)->first();
            $addressProfile = address_job_center::where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

            $articlesPurchase = DB::table('article_orders as ao')
                ->where('ao.id_purchase_order', $purchaseOrder->id)
                ->join('products as p', 'ao.id_product', 'p.id')
                ->select('ao.*', 'p.name as product')
                ->get();

            $subtotal = 0;
            foreach ($articlesPurchase as $item) $subtotal += $item->subtotal;
            
            $banner = DB::table('personal_mails')->where('id_company',$company->id)->first();
            $country_code = DB::table('countries')->where('id', $company->id_code_country)->first();
            $whatsapp = 'https://api.whatsapp.com/send?phone='. $country_code->code_country .$profileJobCenter->whatsapp_personal;

            $emails = $request->get('emails');
            $pdfSendProvider = PDF::loadView('vendor.adminlte.accounting._sendPurchasePdfEmail',
                ['company' => $company, 'symbol_country' => $symbol_country, 'jobCenterProfile' => $jobCenterProfile, 'purchaseOrder' => $purchaseOrder, 'addressProfile' => $addressProfile,
                    'articlesPurchase' => $articlesPurchase, 'subtotal' => $subtotal]);
            config(['mail.from.name' => $jobCenterProfile->name]);

            foreach ($emails as $email) {
                $result = filter_var( $email, FILTER_VALIDATE_EMAIL );
                if($result != false){
                    Mail::to($email)->send(new PurchaseOrderMail($pdfSendProvider->output(), $banner, $whatsapp, $purchaseOrder));
                }
            }
            return \response()->json([
                'code' => 201,
                'message' => 'Orden De Compra Enviada'
            ]);
        }
        catch (\Exception $e){
            return \response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intentalo de nuevo'
            ]);
        }
    }

    public function whatsappPdfPurchaseOrder($id){
        try {
            $purchaseOrderId = SecurityController::decodeId($id);
            $purchaseOrder = purchase_order::join('providers as p','purchase_orders.id_provider', 'p.id')
                ->join('users as u', 'purchase_orders.id_user', 'u.id')
                ->join('concepts as c', 'purchase_orders.id_concept', 'c.id')
                ->join('vouchers as v', 'purchase_orders.id_voucher', 'v.id')
                ->join('payment_methods as pm', 'purchase_orders.id_payment_method', 'pm.id')
                ->join('payment_ways as pw', 'purchase_orders.id_payment_way', 'pw.id')
                ->select('purchase_orders.id', 'id_purchase_order as folio', 'purchase_orders.date_order as date', 'u.name as solicitante', 'p.company as proveedor', 'p.contact_name',
                    'p.contact_cellphone', 'description_order as description', 'c.name as concepto', 'purchase_orders.total', 'purchase_orders.credit_days', 'v.name as voucher', 'purchase_orders.created_at',
                    'purchase_orders.account_status', 'purchase_orders.id_payment_way', 'pw.name as pay_way', 'pm.name as typePayment', 'p.contact_address', 'p.contact_email', 'p.bank', 'p.account_holder', 'p.account_number',
                    'p.clabe', 'p.rfc', 'c.id as id_concept', 'v.id as id_voucher', 'v.name as type_voucher','pm.id as id_payment_method', 'pm.name as payment_method', 'purchase_orders.id_payment_way',
                    'purchase_orders.id_jobcenter','purchase_orders.companie','purchase_orders.updated_at')
                ->where('purchase_orders.id', $purchaseOrderId)
                ->first();

            $company = CommonCompany::getCompanyById($purchaseOrder->companie);
            $symbol_country = CommonCompany::getSymbolByCountryPublic($company->id);
            $jobCenterProfile = profile_job_center::where('id', $purchaseOrder->id_jobcenter)->first();
            $addressProfile = address_job_center::where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();
            $articlesPurchase = DB::table('article_orders as ao')
                ->where('ao.id_purchase_order', $purchaseOrder->id)
                ->join('products as p', 'ao.id_product', 'p.id')
                ->select('ao.*', 'p.name as product')
                ->get();

            $subtotal = 0;
            foreach ($articlesPurchase as $item) $subtotal += $item->subtotal;

            $pdfSendProvider = PDF::loadView('vendor.adminlte.accounting._sendPurchasePdfEmail',
                ['company' => $company, 'symbol_country' => $symbol_country, 'jobCenterProfile' => $jobCenterProfile, 'purchaseOrder' => $purchaseOrder, 'addressProfile' => $addressProfile,
                    'articlesPurchase' => $articlesPurchase, 'subtotal' => $subtotal])
                ->setPaper('A4');
            return $pdfSendProvider->stream();
        }
        catch (Exception $e){
            return \response()->json([
                'code' => 500,
                'message' => 'Algo salio mal, intente de nuevo.'
            ]);
        }
    }

    public function editExpense($id){

        $id = SecurityController::decodeId($id);
        $jobCenterSession = JobCenterSession::where('id_user', Auth::user()->id)->first()->id_profile_job_center;
        $concepts = DB::table('concepts')->where('profile_job_center_id', $jobCenterSession)->get();
        $vouchers = DB::table('vouchers')->where('profile_job_center_id', $jobCenterSession)->get();
        $paymentMethods = DB::table('payment_methods')->where('profile_job_center_id', $jobCenterSession)->get();
        $paymentWays = DB::table('payment_ways')->where('profile_job_center_id', $jobCenterSession)->orderBy('name', 'asc')->get();
        $archivesExpense = DB::table('archive_expenses')->where('id_expense', $id)->get();
        $articlesExpense = DB::table('article_expenses')->where('id_expense', $id)->first();

        $expense = DB::table('expenses as ex')
            ->join('users as u', 'ex.id_user', 'u.id')
            ->join('concepts as c', 'ex.id_concept', 'c.id')
            ->join('vouchers as v', 'ex.id_voucher', 'v.id')
            ->join('payment_methods as pm', 'ex.id_payment_method', 'pm.id')
            ->select('ex.id', 'id_expense as folio', 'ex.expense_name', 'ex.date_expense as date', 'u.name as solicitante', 'description_expense as description',
                'c.name as concepto', 'ex.total', 'ex.credit_days', 'v.name as voucher', 'ex.created_at', 'ex.account_status', 'ex.id_payment_way',
                'pm.name as typePayment', 'ex.id_concept', 'ex.id_payment_method', 'ex.id_voucher')
            ->where('ex.id', $id)
            ->first();

        $expense_id = expense::find($id);
        // Middleware to deny access to data from another company
        if ($expense_id->companie != auth()->user()->companie) {
            SecurityController::abort();
        }

        foreach ($archivesExpense as $ach){
            $archivesExpense->map(function ($ach){
                $arrayFile = explode(".", $ach->file_route);
                $ach->extension = $arrayFile[1];
            });
        }

        return view('vendor.adminlte.accounting._modalEditExpense')->with(['paymentMethods' =>
            $paymentMethods, 'paymentWays' => $paymentWays, 'concepts' => $concepts, 'vouchers' => $vouchers,
            'articleExpense' => $articlesExpense, 'expense' => $expense, 'archivesExpense' => $archivesExpense
        ]);
    }

    public function updateExpense(Request $request){
        $idUser = Auth::id();

        $expense = expense::find($request->id);
        $expense->expense_name = $request->expense_name;
        $expense->id_concept = $request->conceptExpense;
        $expense->id_user = $idUser;
        $expense->description_expense = $request->description_expense;
        $expense->total = $request->total;
        $expense->id_payment_way = $request->typePayment;
        $expense->credit_days = $request->daysExpense;
        $expense->id_payment_method = $request->typeMethod;
        $expense->date_expense = $request->dateExpense;
        $expense->id_voucher = $request->voucherExpense;
        $expense->save();

        //Articulos
        $art_exp = article_expense::find($request->idArticle);
        $art_exp->id_expense = $expense->id;
        $art_exp->concept = $request->article;
        $art_exp->total = $request->total;
        $art_exp->save();

        //Archivos
        //Guardar documentos de la factura
        if ($request->comprobant != null){
            $files = archive_expense::where('id_expense', $request->id)->get();
            foreach ($files as $file) {
                Storage::disk('s3')->delete($file->file_route);
            }
            DB::table('archive_expenses')->where('id_expense', $request->id)->delete();
            foreach ($request->comprobant as $f) {
                $filename = $f->store('expenses', 's3');
                archive_expense::create(['id_expense' => $expense->id,
                    'file_route' => $filename, 'type' => 1, 'companie' => Auth::user()->companie]);
            }
        }

        return redirect()->route('index_accounting')->with('success','Guardando Correctamente');
    }

    public function accountPayment(Request $request){
        $id = $request->get("id");
        $type = $request->get("type");

        if ($type == 1){
            //order purchase
            $orderPurchase = purchase_order::find($id);
            $orderPurchase->status = 2;
            $orderPurchase->save();

            //save files
            foreach ($request->comprobant as $f) {
                $filename = $f->store('purchases', 's3');
                archive_order::create(['id_purchase_order' => $request->id,
                    'file_route' => $filename, 'type' => 2, 'companie' => Auth::user()->companie]);
            }
        }else{
            //expense
            $expense = expense::find($id);
            $expense->status = 2;
            $expense->save();

            if ($request->comprobant) {
                //save files
                foreach ($request->comprobant as $f) {
                    $filename = $f->store('expenses', 's3');
                    archive_expense::create(['id_expense' => $request->id,
                        'file_route' => $filename, 'type' => 2, 'companie' => Auth::user()->companie]);
                }
            }
        }

        return redirect()->route('index_accounting');
    }

    public function accountRefund(Request $request){
        $id = $request->get("id");

        //save files
        foreach ($request->comprobant as $f) {
            $filename = $f->store('expenses', 's3');
            archive_expense::create(['id_expense' => $request->id,
                'file_route' => $filename, 'type' => 3, 'companie' => Auth::user()->companie]);
        }

        return redirect()->route('index_accounting');
    }

    public function Expenses()
    {
        //Validate plan free
        if (\Illuminate\Support\Facades\Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }
        
        $expense = DB::table('expenses as ex')
            ->join('users as u', 'ex.id_user', 'u.id')
            ->join('concepts as c', 'ex.id_concept', 'c.id')
            ->join('vouchers as v', 'ex.id_voucher', 'v.id')
            ->join('payment_methods as pm', 'ex.id_payment_method', 'pm.id')
            ->select('ex.id', 'id_expense as folio', 'ex.expense_name', 'ex.date_expense as date', 'u.name as solicitante', 'description_expense as description',
                'c.name as concepto', 'ex.total', 'ex.credit_days', 'v.name as voucher', 'ex.created_at', 'ex.account_status', 'ex.id_payment_way',
                'pm.name as typePayment', 'ex.id_concept', 'ex.id_payment_method', 'ex.id_voucher','ex.id_user','ex.status')
            ->where('id_user', auth()->user()->id)
            ->get();
        foreach ($expense as $ex){
            $expense->map(function ($ex){
            $ex->type = 2;
            $imagesMonitoring = DB::table('archive_expenses')->where('id_expense', $ex->id)->get();
            if ($ex->status == 2){
                $filePayment = DB::table('archive_expenses')->where('type', 3)->where('id_expense', $ex->id)->first();
                    if (!empty($filePayment)) $ex->refund = 1;
                    else $ex->refund = 0;
            }
            $ex->photosMonitoring = $imagesMonitoring;
            });
        }
        $articlesExpense = DB::table('article_expenses')->where('companie', Auth::user()->companie)->get();
        $archivesPurchase = DB::table('archive_expenses')->where('companie', Auth::user()->companie)->get();
        foreach ($archivesPurchase as $ach){
            $archivesPurchase->map(function ($ach){
                $arrayFile = explode(".", $ach->file_route);
                $ach->extension = $arrayFile[1];
            });
        }

        $symbol_country = CommonCompany::getSymbolByCountry();

        return view('vendor.adminlte.accounting._myexpense')
            ->with(['expense' => $expense, 'articlesExpense' => $articlesExpense, 'archivesPurchase' => $archivesPurchase, 'symbol_country' => $symbol_country]);
    }
}
