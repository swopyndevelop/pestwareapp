<?php


namespace App\Http\Controllers\Accounting;


use App\expense;
use App\purchase_order;
use Illuminate\Support\Facades\Auth;

class CommonAccounting
{
    public static function getFolioExpense()
    {
        $expense = expense::where('companie', Auth::user()->companie)->latest()->first();
        if ($expense) {
            if ($expense->id_expense == 'OG-1') {
                return 'OG-2';
            } else {
                $folio = explode('-', $expense->id_expense);
                return 'OG-'.++$folio[1];
            }
        } else return 'OG-1';
    }

    public static function getFolioExpenseByApi($idCompany)
    {
        $expense = expense::where('companie', $idCompany)->latest()->first();
        if ($expense) {
            if ($expense->id_expense == 'OG-1') {
                return 'OG-2';
            } else {
                $folio = explode('-', $expense->id_expense);
                return 'OG-'.++$folio[1];
            }
        } else return 'OG-1';
    }

    public static function getFolioPurchaseOrder()
    {
        $purchase = purchase_order::where('companie', Auth::user()->companie)->latest()->first();
        if ($purchase) {
            if ($purchase->id_purchase_order == 'OC-1') {
                return 'OC-2';
            } else {
                $folio = explode('-', $purchase->id_purchase_order);
                return 'OC-'.++$folio[1];
            }
        } else return 'OC-1';
    }
}