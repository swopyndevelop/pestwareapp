<?php

namespace App\Http\Middleware;

use App\Log_request;
use App\Notifications\LogsNotification;
use App\User;
use Auth;
use Closure;
use Illuminate\Http\Request;

class LogRoute
{

    private $start;
    protected $end;

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->start = microtime(true);
        return $next($request);
    }

    public function terminate($request)
    {
        $this->end = microtime(true);

        $this->log($request);
    }

    protected function log($request)
    {
        $duration = $this->end - $this->start;
        $auth = 'Unauthenticated';
        if (Auth::check()) $auth = Auth::user()->id;

        $log = new Log_request();
        $log->url = $request->fullUrl();
        $log->method = $request->getMethod();
        $log->ip = $request->getClientIp();
        $log->duration = $duration . "ms";
        $log->user = $auth;
        $log->save();

        if ($log->method === 'DELETE') {
            $message = 'LOG: ' . $log->id;
            User::first()->notify(new LogsNotification('NEW LOG REQUEST - DELETE', $message, 2));
        }

    }

}
