<?php

namespace App\Http\Middleware;

use App\LogRequestApi;
use App\Notifications\LogsNotification;
use App\User;
use Closure;
use Illuminate\Http\Request;

class LogRouteApi
{

    private $start;
    protected $end;

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->start = microtime(true);
        return $next($request);
    }

    public function terminate($request)
    {
        $this->end = microtime(true);

        $this->log($request);
    }

    protected function log($request)
    {
        $duration = $this->end - $this->start;

        $log = new LogRequestApi();
        $log->url = $request->fullUrl();
        $log->endpoint = $request->getRequestUri();
        $log->method = $request->getMethod();
        $log->ip = $request->getClientIp();
        $log->duration = $duration . "ms";
        $log->cookie = json_encode($request->cookie());
        $log->save();

        if ($log->method === 'DELETE') {
            $message = 'LOG: ' . $log->id;
            User::first()->notify(new LogsNotification('NEW LOG REQUEST API - DELETE', $message, 2));
        }

    }
}
