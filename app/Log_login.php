<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Log_login
 *
 * @package App
 * @author Manuel Mendoza
 * @version 03/18/2021
 * @property int $id
 * @property string|null $city
 * @property string|null $country_code
 * @property string|null $country_name
 * @property string|null $ip
 * @property string|null $ip_request
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $region_code
 * @property string|null $region_name
 * @property string|null $time_zone
 * @property string|null $zip_code
 * @property string|null $request_duration
 * @property string|null $request_url
 * @property string|null $request_method
 * @property string|null $status_code
 * @property string|null $message
 * @property string|null $user
 * @property string|null $password
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */

class Log_login extends Model
{
    protected $table = "log_logins";

    protected $fillable = [
        'id',
        'city',
        'country_code',
        'country_name',
        'ip',
        'ip_request',
        'latitude',
        'longitude',
        'region_code',
        'region_name',
        'time_zone',
        'zip_code',
        'request_duration',
        'request_url',
        'request_method',
        'status_code',
        'message',
        'user',
        'password'
    ];
}
