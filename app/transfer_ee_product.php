<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\transfer_ee_product
 *
 * @property int $id
 * @property int $id_transfer_ee
 * @property int $id_product
 * @property string $units
 * @property int|null $is_units
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|transfer_ee_product newModelQuery()
 * @method static Builder|transfer_ee_product newQuery()
 * @method static Builder|transfer_ee_product query()
 * @method static Builder|transfer_ee_product whereCreatedAt($value)
 * @method static Builder|transfer_ee_product whereId($value)
 * @method static Builder|transfer_ee_product whereIdProduct($value)
 * @method static Builder|transfer_ee_product whereIdTransferEe($value)
 * @method static Builder|transfer_ee_product whereIsUnits($value)
 * @method static Builder|transfer_ee_product whereUnits($value)
 * @method static Builder|transfer_ee_product whereUpdatedAt($value)
 * @mixin Eloquent
 */
class transfer_ee_product extends Model
{
    /**
     * @var string
     */
    protected $table = "transfer_ee_products";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_transfer_ee',
        'id_product',
        'units'
    ];
}
