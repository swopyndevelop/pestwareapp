<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_machine
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_machines_id
 * @property int $job_title_profiles_id
 * @property string|null $machine_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_machine newModelQuery()
 * @method static Builder|job_title_machine newQuery()
 * @method static Builder|job_title_machine query()
 * @method static Builder|job_title_machine whereCreatedAt($value)
 * @method static Builder|job_title_machine whereId($value)
 * @method static Builder|job_title_machine whereJobTitleMachinesId($value)
 * @method static Builder|job_title_machine whereJobTitleProfilesId($value)
 * @method static Builder|job_title_machine whereMachineName($value)
 * @method static Builder|job_title_machine whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_machine extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_machines";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_machines_id',
        'job_title_profiles_id',
        'machine_name'
    ];
}
