<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class cash_picture
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/06/2019
 * @property int $id
 * @property int $cash_id
 * @property string $file_route
 * @property int|null $route
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read cash $cash
 * @method static Builder|cash_picture newModelQuery()
 * @method static Builder|cash_picture newQuery()
 * @method static Builder|cash_picture query()
 * @method static Builder|cash_picture whereCashId($value)
 * @method static Builder|cash_picture whereCreatedAt($value)
 * @method static Builder|cash_picture whereFileRoute($value)
 * @method static Builder|cash_picture whereId($value)
 * @method static Builder|cash_picture whereRoute($value)
 * @method static Builder|cash_picture whereUpdatedAt($value)
 * @mixin Eloquent
 */
class cash_picture extends Model
{
    /**
     * @var string
     */
    protected $table = "cash_pictures";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'cash_id',
        'file_route',
        'route'
    ];

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación uno a muchos con tabla place_inspections
    */
    public function cash()
    {
        return $this->belongsTo(cash::class);
    }
}
