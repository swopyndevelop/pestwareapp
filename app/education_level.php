<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class education_level
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property string|null $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|education_level newModelQuery()
 * @method static Builder|education_level newQuery()
 * @method static Builder|education_level query()
 * @method static Builder|education_level whereCreatedAt($value)
 * @method static Builder|education_level whereId($value)
 * @method static Builder|education_level whereName($value)
 * @method static Builder|education_level whereUpdatedAt($value)
 * @mixin Eloquent
 */
class education_level extends Model
{
    /**
     * @var string
     */
    protected $table = "education_levels";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];
}
