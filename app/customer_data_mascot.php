<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class customer_data_mascot
 *
 * @package App
 * @author Olga Rodríguez
 * @version 12/01/2019
 * @property int $id
 * @property int $customer_data_id
 * @property int $mascot_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|customer_data_mascot newModelQuery()
 * @method static Builder|customer_data_mascot newQuery()
 * @method static Builder|customer_data_mascot query()
 * @method static Builder|customer_data_mascot whereCreatedAt($value)
 * @method static Builder|customer_data_mascot whereCustomerDataId($value)
 * @method static Builder|customer_data_mascot whereId($value)
 * @method static Builder|customer_data_mascot whereMascotId($value)
 * @method static Builder|customer_data_mascot whereUpdatedAt($value)
 * @mixin Eloquent
 */
class customer_data_mascot extends Model
{
    /**
     * @var string
     */
    protected $table = "customer_data_mascot";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'customer_data_id',
        'mascot_id'
    ];
}
