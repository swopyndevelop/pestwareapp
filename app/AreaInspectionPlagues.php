<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AreaInspectionPlagues
 * @package App
 * @property int $id
 * @property int $id_area_inspection
 * @property int $id_plague
 * @property int $id_infestation_degree
 * @mixin Eloquent
 */
class AreaInspectionPlagues extends Model
{
    /**
     * @var string
     */
    protected $table = "area_inspections_plagues";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_area_inspection',
        'id_plague',
        'id_infestation_degree'
    ];
}
