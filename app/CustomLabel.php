<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomLabel
 * @package App
 * @property int $id
 * @property int $id_label
 * @property string|null $label_spanish
 * @property string|null $label_english
 * @property int $id_profile_job_center
 * @mixin Eloquent
 */

class CustomLabel extends Model
{
    /**
     * @var string
     */
    protected $table = "custom_labels";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_label',
        'label_spanish',
        'label_english',
        'id_profile_job_center'
    ];
}
