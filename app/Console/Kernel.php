<?php

namespace App\Console;

use App\Comment;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SendNotifications::class,
        Commands\Tracking::class,
        Commands\TestNotificationCommand::class,
        Commands\DatabaseBackUp::class,
        Commands\DbDump::class,
        Commands\GenerateDatabaseDump::class,
        Commands\GenerateInvoices::class,
        Commands\CalculateFoliosInvoice::class,
        Commands\UploadEvents::class,
        Commands\ExpirationCertificates::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $filePath = storage_path('logs/fcm_send.log');

        $schedule->command('fcm:send')
            ->everyMinute();
            //->appendOutputTo($filePath);

        $schedule->command('tracking:gps')->everyMinute();

        //$schedule->command('database:backup')->daily();
        $schedule->command('database:dump')->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
