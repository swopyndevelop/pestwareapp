<?php

namespace App\Console\Commands;

use App\Common\CommonNotifications;
use App\companie;
use App\customer;
use App\customer_data;
use App\Http\Controllers\Invoices\InvoiceController;
use App\Jobs\GenerateAutomaticInvoices;
use App\Notifications;
use App\Notifications\LogsNotification;
use App\profile_job_center;
use App\ProfileJobCenterBilling;
use App\ReportsSlack;
use App\UnitSat;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class GenerateInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate all schedule invoices of customers.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->info('************************************************************************************');
        $this->info('START---Generate all schedule invoices of customers.');
        GenerateAutomaticInvoices::dispatch();
        $this->info('************************************************************************************');
        $this->info('RUNNING IN THE BACKGROUND...');
    }
}
