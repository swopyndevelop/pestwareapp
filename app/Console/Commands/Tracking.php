<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Tracking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tracking:gps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data gps technicians of Sino Track';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('getting gps data...');
        $count = 0;
        while ($count < 59) {
            $this->getDataGps();
            $count += 10;
            sleep(10);
        }

    }

    private function getDataGps() {
        $now = Carbon::now();
        $technicians = DB::table('employees')->where('gps', '!=', null)->get();
        foreach ($technicians as $technician) {
            $client = new Client();
            try {
                $response = $client->post('http://www.sinotrackpro.com/APP/AppJson.asp', [
                    'form_params' => [
                        'Cmd' => 'Proc_GetLastPosition',
                        'Data' => "N'".$technician->gps."'",
                    ]
                ]);
                $response = json_decode($response->getBody()->getContents());
                if (property_exists($response, 'm_arrRecord')) {
                    $data = $response->m_arrRecord[0];
                    DB::table('tracking')->insert([
                        'id_employee' => $technician->id,
                        'date' => Carbon::now()->toDateString(),
                        'hour' => Carbon::now()->toTimeString(),
                        'strTEID' => $data[0],
                        'nTime' => $data[1],
                        'dbLon' => $data[2],
                        'dbLat' => $data[3],
                        'nDirection' => $data[4],
                        'nSpeed' => $data[5],
                        'nGSMSignal' => $data[6],
                        'nGPSSignal' => $data[7],
                        'nFuel' => $data[8],
                        'nMileage' => $data[9],
                        'nTemp' => $data[10],
                        'nCarState' => $data[11],
                        'nTEState' => $data[12],
                        'nAlarmState' => $data[13],
                        'strOther' => $data[14],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ]);
                    $log = 'Technician: ' . $technician->name . ' | GPS: ' . $technician->gps . ' | Time: ' . $now->toDateTimeString() . ' | Latitude: ' . $data[3] . ' | Longitude: ' . $data[2];
                    $this->info($log);
                }
            } catch (GuzzleException $e) {
                $this->info($e->getMessage());
            }
        }
    }
}
