<?php

namespace App\Console\Commands;

use App\Common\CommonNotifications;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class ExpirationCertificates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expiration:certificates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command create notification for expiration certificates.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->info('************************************************************************************');
        $this->info('START---create notification for expiration certificates.');
        $now = Carbon::now()->toDateString();
        $certificates = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as cu', 'q.id_customer', 'cu.id')
            ->whereDate('so.date_expiration_certificate', $now)
            ->select('so.date_expiration_certificate', 'so.id_job_center', 'so.companie', 'cu.name as customer',
                'so.id_service_order as folio', 'ev.start_event', 'ev.initial_date');
        foreach ($certificates->get() as $certificate)
        {
            $message = 'Ha expirado el certificado del servicio ' . $certificate->folio . ' realizado el día ' . $certificate->initial_date . ' a las ' . $certificate->start_event;
            CommonNotifications::create(
                'Vencimiento de Certificado - ' . $certificate->customer,
                $message,
                $certificate->id_job_center,
                $certificate->companie
            );
            $this->info('SEND NOTIFICATION ' . $certificate->folio);
        }
        $this->info('************************************************************************************');
        $this->info('TOTAL DE NOTIFICACIONES ENVIADAS: ' . $certificates->count());
        $this->info('END...');

        $quotes = DB::table('quotations as q')
            ->join('custom_quotes as cq', 'cq.id_quotation', 'q.id')
            ->where('cq.id_type_service', '<>', 7)
            ->get();

        foreach ($quotes as $quote)
        {
            $events = DB::table('events as ev')
                ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('customers as cu', 'q.id_customer', 'cu.id')
                ->where('so.id_quotation', $quote->id)
                ->where('ev.id_status', 1)
                ->select('so.id_job_center', 'so.companie', 'cu.name as customer',
                    'so.id_service_order as folio', 'ev.initial_date', 'q.id_quotation')
                ->orderBy('ev.initial_date');

            if ($events->count() == 2) {
                $lastEvent = $events->get()->last();
                $message = 'El día ' . $lastEvent->initial_date . ' es el último servicio agendado de la siguiente programación: COTIZACION: ' . $lastEvent->id_quotation . ' | ' . $quote->concept;
                CommonNotifications::create(
                    'Renovación de Servicio - ' . $lastEvent->customer,
                    $message,
                    $lastEvent->id_job_center,
                    $lastEvent->companie
                );
            }
        }
    }
}
