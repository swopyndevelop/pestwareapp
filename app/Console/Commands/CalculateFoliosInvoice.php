<?php

namespace App\Console\Commands;

use App\Common\CommonNotifications;
use App\Common\Constants;
use App\companie;
use App\customer;
use App\Http\Controllers\Invoices\CommonInvoice;
use App\Jobs\CalculateFoliosInvoiceJob;
use App\ProfileJobCenterBilling;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;

class CalculateFoliosInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:calculateFolios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate the number of folios required to execute the automatic daily billing.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('************************************************************************************');
        $this->info('START---Calculate the number of folios required to execute the automatic daily billing.');
        CalculateFoliosInvoiceJob::dispatch();
        $this->info('************************************************************************************');
        $this->info('RUNNING IN THE BACKGROUND...');
    }
}
