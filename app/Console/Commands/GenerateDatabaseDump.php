<?php

namespace App\Console\Commands;

use App\BackupNotification;
use App\Mail\DevOpsBackupMail;
use App\Notifications\LogsNotification;
use Exception;
use Ifsnop\Mysqldump\Mysqldump;
use Illuminate\Console\Command;
use Mail;

class GenerateDatabaseDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a dump of the MySQL database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        $backupNotification = new BackupNotification();

        $backupNotification->notify(new LogsNotification("RUNNING BACKUP...", "Database dump building.", 1));

        $file = 'db_dump' . date('_Y-m-d') . '.sql';
        $dumpFile = storage_path('app/backup/'.$file);

        $dumpSettings = array(
            'compress' => MySQLDump::GZIP,
        );

        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        $database = env('DB_DATABASE');
        $dsn = "mysql:host=" . $host . ";dbname=" . $database;

        $dump = new MySQLDump(
            $dsn,
            $username,
            $password,
            $dumpSettings
        );
        $dump->start($dumpFile);

        $backupNotification->notify(new LogsNotification("BACKUP COMPLETED", "Database dump created successfully.", 1));

        //Notify by email
        $emails = ['pestwareapp@gmail.com', 'alberto.martinez@swopyn.com'];
        foreach ($emails as $email) {
            $result = filter_var($email, FILTER_VALIDATE_EMAIL);
            config(['mail.from.name' => 'DevOps PestWare App']);
            if($result != false){
                $data['path_backup'] = $file;
                Mail::to($email)->send(new DevOpsBackupMail($data));
            }
        }
    }
}
