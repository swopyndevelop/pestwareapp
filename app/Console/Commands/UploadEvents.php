<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UploadEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:massive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('************************************************************************************');
        $this->info('START---UPLOAD EVENTS MASSIVE.');
        \App\Jobs\UploadEvents::dispatch();
        $this->info('************************************************************************************');
        $this->info('RUNNING IN THE BACKGROUND...');
    }
}
