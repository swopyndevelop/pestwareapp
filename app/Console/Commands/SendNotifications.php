<?php

namespace App\Console\Commands;

use App\employee;
use App\event;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fcm:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send message with FCM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('*************************************************************************************************************************************************************');
        $this->info('EVENTS NOTIFICATION NEXT 45 MINUTES');
        $now = Carbon::now();
        $this->info('LOG DATETIME: ' . $now);
        $headers = ['id', 'title', 'id_employee', 'id_service_order', 'initial_hour', 'final_hour', 'initial_date',
            'final_date', 'start_event', 'final_event', 'id_job_center', 'companie', 'id_status', 'x_cut', 'date_cut',
            'service_type', 'created_at', 'updated_at'];

        $eventsNextHour = $this->getEventsNextHour($now->copy());
        $this->table($headers, $eventsNextHour);
        foreach ($eventsNextHour as $event) {
            $employee = employee::find($event->id_employee);
            $user = User::find($employee->employee_id);
            $message = $this->getDataNotification($event->id, " en 45 minutos. ");
            $user->sendFCM(' Recordatorio', $message);
            $this->info('Recordatorio enviado al usuario con ID ' . $event->id_employee);
        }

        $eventsNext15Min = $this->getEventsNext15Min($now->copy());
        $this->info('EVENTS NOTIFICATION NEXT 15 MINUTES');
        $this->table($headers, $eventsNext15Min);
        foreach ($eventsNext15Min as $event) {
            $employee = employee::find($event->id_employee);
            $user = User::find($employee->employee_id);
            $message = $this->getDataNotification($event->id, " en 15 minutos. ");
            $user->sendFCM(' Recordatorio',  $message);
            $this->info('Recordatorio enviado al usuario con ID ' . $event->id_employee);
        }

        $this->info('*************************************************************************************************************************************************************');
    }

    private function getEventsNextHour($now) {

        return $events = event::where('id_status', 1)
            ->where('initial_date', $now->addMinutes(45)->toDateString())
            ->where('initial_hour', '>=' , $now->copy()->subMinutes(1)->toTimeString())
            ->where('initial_hour', '<', $now->copy()->addMinutes(1)->toTimeString())
            ->get();
    }

    private function getEventsNext15Min($now) {

        return $events = event::where('id_status', 1)
            ->where('initial_date', $now->addMinutes(15)->toDateString())
            ->where('initial_hour', '>=' , $now->copy()->subMinutes(1)->toTimeString())
            ->where('initial_hour', '<', $now->copy()->addMinutes(1)->toTimeString())
            ->get();
    }

    private function getDataNotification($id, $timeNotification) {
        $data = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->select('cu.name', 'so.id_service_order', 'ev.initial_date', 'ev.initial_hour', 'so.total', 'cd.address as street',
                'cd.address_number', 'cu.colony', 'em.name as technical', 'qo.id as id_quotation', 'cu.cellphone', 'cu.municipality',
                'cd.state', 'so.id as id_order')
            ->where('ev.id', $id)
            ->first();
        return $notification = "Tienes un " . $data->id_service_order . $timeNotification . $data->name .
            " " . date('h:i A', strtotime($data->initial_hour)) . " " . $data->street . " #" .
            $data->address_number . ", " . $data->colony . ". " . $data->municipality . ", " . $data->state;
    }
}
