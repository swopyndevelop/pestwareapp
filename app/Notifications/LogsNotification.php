<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LogsNotification extends Notification
{
    use Queueable;

    private $message;
    private $title;
    private $type;

    private $SUCCESS = 1;
    private $WARNING = 2;
    private $ERROR = 3;

    /**
     * Create a new notification instance.
     *
     * @param $message
     * @param $title
     * @param $type
     */
    public function __construct($title, $message, $type)
    {
        $this->message = $message;
        $this->title = $title;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $message = $this->message;
        $title = '*' . $this->title . '*';

        if ($this->type == $this->SUCCESS) {
            return (new SlackMessage)
                ->success()
                ->content($title)
                ->attachment(function ($attachment) use ($message, $title) {
                    $attachment->content($message)->markdown(['title', 'text']);
                });
        } elseif ($this->type == $this->WARNING) {
            return (new SlackMessage)
                ->warning()
                ->content($title)
                ->attachment(function ($attachment) use ($message, $title) {
                    $attachment->content($message)->markdown(['title', 'text']);
                });
        } else {
            return (new SlackMessage)
                ->error()
                ->content($title)
                ->attachment(function ($attachment) use ($message, $title) {
                    $attachment->content($message)->markdown(['title', 'text']);
                });
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
