<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class command
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $commands_id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|command newModelQuery()
 * @method static Builder|command newQuery()
 * @method static Builder|command query()
 * @method static Builder|command whereCommandsId($value)
 * @method static Builder|command whereCreatedAt($value)
 * @method static Builder|command whereId($value)
 * @method static Builder|command whereName($value)
 * @method static Builder|command whereUpdatedAt($value)
 * @mixin Eloquent
 */
class command extends Model
{
    /**
     * @var string
     */
    protected $table = "commands";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'commands_id',
        'name'
    ];
}
