<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class service_order
 *
 * @package App
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @property int $id
 * @property string $id_service_order
 * @property int $id_quotation
 * @property int|null $user_id
 * @property int $id_payment_method
 * @property int $id_payment_way
 * @property int|null $id_status
 * @property int|null $id_job_center
 * @property int $companie
 * @property string|null $bussiness_name
 * @property string|null $address
 * @property string|null $email
 * @property string|null $observations
 * @property float $total
 * @property double $subtotal
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $warranty
 * @property int $reinforcement
 * @property int $tracing
 * @property boolean $inspection
 * @property boolean $fake
 * @property string|null $evidence_tracings
 * @property int|null $whatsapp
 * @property int|null $confirmed
 * @property int|null $reminder
 * @property int|null $customer_branch_id
 * @property int|null $area_node_id
 * @property string|null $visible
 * @property boolean $is_shared
 * @property boolean $is_main
 * @property-read Collection|habit_condition[] $habit_condition
 * @property-read int|null $habit_condition_count
 * @property-read Collection|inhabitant_type[] $inhabitant_type
 * @property-read int|null $inhabitant_type_count
 * @property-read Collection|mascot[] $mascot
 * @property-read int|null $mascot_count
 * @property string|null date_expiration_certificate
 * @property boolean $invoiced
 * @property boolean $invoiced_status
 * @method static Builder|service_order newModelQuery()
 * @method static Builder|service_order newQuery()
 * @method static Builder|service_order query()
 * @method static Builder|service_order whereAddress($value)
 * @method static Builder|service_order whereBussinessName($value)
 * @method static Builder|service_order whereCompanie($value)
 * @method static Builder|service_order whereConfirmed($value)
 * @method static Builder|service_order whereCreatedAt($value)
 * @method static Builder|service_order whereCustomerBranchId($value)
 * @method static Builder|service_order whereEmail($value)
 * @method static Builder|service_order whereId($value)
 * @method static Builder|service_order whereIdJobCenter($value)
 * @method static Builder|service_order whereIdPaymentMethod($value)
 * @method static Builder|service_order whereIdPaymentWay($value)
 * @method static Builder|service_order whereIdQuotation($value)
 * @method static Builder|service_order whereIdServiceOrder($value)
 * @method static Builder|service_order whereIdStatus($value)
 * @method static Builder|service_order whereObservations($value)
 * @method static Builder|service_order whereReinforcement($value)
 * @method static Builder|service_order whereReminder($value)
 * @method static Builder|service_order whereTotal($value)
 * @method static Builder|service_order whereTracing($value)
 * @method static Builder|service_order whereUpdatedAt($value)
 * @method static Builder|service_order whereUserId($value)
 * @method static Builder|service_order whereWarranty($value)
 * @method static Builder|service_order whereWhatsapp($value)
 * @mixin Eloquent
 */

class service_order extends Model
{
    /**
     * @var string
     */
    protected $table = "service_orders";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_service_order',
        'id_quotation',
        'user_id',
        'id_payment_method',
        'id_payment_way',
        'id_status',
        'id_job_center,',
        'bussiness_name',
        'address',
        'email',
        'observations',
        'total',
        'subtotal',
        'companie',
        'evidence_tracings',
        'visible',
        'date_expiration_certificate',
        'is_shared',
        'is_main',
        'area_node_id',
        'invoiced',
        'invoiced_status'
    ];

    /**
    * @author Olga Rodríguez
    * @version 10/04/2019
    *Función para relación muchos a muchos con tabla habit_conditions
    */
    public function habit_condition()
    {
        return $this->belongsToMany(habit_condition::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 10/04/2019
    *Función para relación muchos a muchos con tabla inhabitant_types
    */
    public function inhabitant_type()
    {
        return $this->belongsToMany(inhabitant_type::class);
    }

    /**
    * @author Olga Rodríguez
    * @version 10/04/2019
    *Función para relación muchos a muchos con tabla mascots
    */
    public function mascot()
    {
        return $this->belongsToMany(mascot::class);
    }
}
