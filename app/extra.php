<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class extra
 *
 * @package App
 * @author Olga Rodríguez
 * @version 29/03/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property string $description
 * @property float $amount
 * @property int $id_company
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|extra newModelQuery()
 * @method static Builder|extra newQuery()
 * @method static Builder|extra query()
 * @method static Builder|extra whereAmount($value)
 * @method static Builder|extra whereCreatedAt($value)
 * @method static Builder|extra whereDescription($value)
 * @method static Builder|extra whereId($value)
 * @method static Builder|extra whereIdCompany($value)
 * @method static Builder|extra whereName($value)
 * @method static Builder|extra whereUpdatedAt($value)
 * @mixin Eloquent
 */
class extra extends Model
{
    /**
     * @var string
     */
    protected $table = "extras";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'amount',
        'profile_job_center_id',
        'visible'
    ];
}
