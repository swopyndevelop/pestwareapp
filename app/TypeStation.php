<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeStation
 * @package App
 * @property int $id
 * @property string $key
 * @property string $name
 * @property int $id_company
 * @property int $id_type_area
 * @property int $profile_job_center_id
 * @mixin Eloquent
 * @property string $visible
 * @mixin Eloquent
 */

class TypeStation extends Model
{
    protected $table = 'type_stations';

    protected $fillable = ['key','name', 'profile_job_center_id','id_company','visible', 'id_type_area'];
}
