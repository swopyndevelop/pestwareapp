<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\transfer_employee_center
 *
 * @property int $id
 * @property string $id_transfer_ec
 * @property int $id_user
 * @property int $id_employee_origin
 * @property int $id_job_center_destiny
 * @property float $total
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|transfer_employee_center newModelQuery()
 * @method static Builder|transfer_employee_center newQuery()
 * @method static Builder|transfer_employee_center query()
 * @method static Builder|transfer_employee_center whereCompanie($value)
 * @method static Builder|transfer_employee_center whereCreatedAt($value)
 * @method static Builder|transfer_employee_center whereId($value)
 * @method static Builder|transfer_employee_center whereIdEmployeeOrigin($value)
 * @method static Builder|transfer_employee_center whereIdJobCenterDestiny($value)
 * @method static Builder|transfer_employee_center whereIdTransferEc($value)
 * @method static Builder|transfer_employee_center whereIdUser($value)
 * @method static Builder|transfer_employee_center whereTotal($value)
 * @method static Builder|transfer_employee_center whereUpdatedAt($value)
 * @mixin Eloquent
 */
class transfer_employee_center extends Model
{
    /**
     * @var string
     */
    protected $table = "transfer_employee_centers";

    /**
     * @var array
     */

    protected $fillable = [
        'id',
        'id_transfer_ec',
        'id_user',
        'id_employee_origin',
        'id_job_center_destiny',
        'total',
        'created_at'
    ];
}
