<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_contract
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_contracts_id
 * @property int $job_title_profiles_id
 * @property int $contract_types_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_contract newModelQuery()
 * @method static Builder|job_title_contract newQuery()
 * @method static Builder|job_title_contract query()
 * @method static Builder|job_title_contract whereContractTypesId($value)
 * @method static Builder|job_title_contract whereCreatedAt($value)
 * @method static Builder|job_title_contract whereId($value)
 * @method static Builder|job_title_contract whereJobTitleContractsId($value)
 * @method static Builder|job_title_contract whereJobTitleProfilesId($value)
 * @method static Builder|job_title_contract whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_contract extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_contracts";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_contracts_id',
        'job_title_profiles_id',
        'contract_types_id'
    ];
}
