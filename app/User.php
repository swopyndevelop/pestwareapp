<?php namespace App;

use Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Acoustep\EntrustGui\Contracts\HashMethodInterface;
use Hash;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property int|null $user_id
 * @property string|null $email
 * @property string|null $cellphone
 * @property string $password
 * @property string|null $remember_token
 * @property int $companie
 * @property int $introjs
 * @property boolean $intro_upgrade
 * @property int $id_plan
 * @property int $confirmed
 * @property string|null $confirmation_code
 * @property string|null $profile_photo
 * @property string|null $device_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|assignment_plan[] $assignment_plans
 * @property-read int|null $assignment_plans_count
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereCellphone($value)
 * @method static Builder|User whereCompanie($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeviceToken($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereProfilePhoto($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HashMethodInterface
{
  use Authenticatable, CanResetPassword, EntrustUserTrait, Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id','email', 'password','companie', 'profile_photo', 'device_token', 'cellphone', 'introjs', 'intro_upgrade','id_plan', 'confirmation_code', 'confirmed'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $hashable = ['password'];

    protected $rulesets = [

        'creating' => [
            'password'   => 'required',
        ],

        'updating' => [
            'password'   => '',
        ],
    ];

    public function entrustPasswordHash() 
    {
        $this->password = Hash::make($this->password);
        $this->save();
    }

    /**
    * @author Olga Rodríguez
    * @version 10/07/2019
    *Función para relación muchos a muchos con tabla assignment_plans
    */
    public function assignment_plans()
    {
        return $this->belongsToMany(assignment_plan::class);
    }

    public function sendFCM($title, $message)
    {
        if (!$this->device_token)
            return;

        return fcm()
            ->to([$this->device_token])
            ->notification([
                'title' => $title,
                'body' => $message,
                'sound' => 'default',
            ])
            ->send();
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return "https://hooks.slack.com/services/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";
    }

}
