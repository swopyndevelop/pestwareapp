<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class product_presentation
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $id_company
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|product_presentation newModelQuery()
 * @method static Builder|product_presentation newQuery()
 * @method static Builder|product_presentation query()
 * @method static Builder|product_presentation whereCreatedAt($value)
 * @method static Builder|product_presentation whereId($value)
 * @method static Builder|product_presentation whereIdCompany($value)
 * @method static Builder|product_presentation whereName($value)
 * @method static Builder|product_presentation whereUpdatedAt($value)
 * @mixin Eloquent
 */
class product_presentation extends Model
{
    /**
     * @var string
     */
    protected $table = "product_presentations";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'profile_job_center_id',
        'visible'
    ];
}
