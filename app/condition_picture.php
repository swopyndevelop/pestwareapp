<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class condition_picture
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/06/2019
 * @property int $id
 * @property int $place_condition_id
 * @property string $file_route
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read place_condition $place_condition
 * @method static Builder|condition_picture newModelQuery()
 * @method static Builder|condition_picture newQuery()
 * @method static Builder|condition_picture query()
 * @method static Builder|condition_picture whereCreatedAt($value)
 * @method static Builder|condition_picture whereFileRoute($value)
 * @method static Builder|condition_picture whereId($value)
 * @method static Builder|condition_picture wherePlaceConditionId($value)
 * @method static Builder|condition_picture whereUpdatedAt($value)
 * @mixin Eloquent
 */
class condition_picture extends Model
{
    /**
     * @var string
     */
    protected $table = "condition_pictures";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'place_condition_id',
        'file_route',
    ];

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    * Función para relación uno a muchos con tabla place_conditions
    */
    public function place_condition()
    {
        return $this->belongsTo(place_condition::class);
    }
}
