<?php

namespace App\Jobs;

use App\companie;
use App\customer;
use PDF;
use Illuminate\Support\Facades\Storage;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class DownloadMipDocuments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $customer;
    protected $year;
    protected $document;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($customer, $year, $document, $data)
    {
        //
        $this->customer = $customer;
        $this->year = $year;
        $this->document = $document;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {

            if ($this->customer->is_main === 0) {
                $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $this->customer->id . '/' . $this->year . $this->document;

                if (file_exists($storagePathMip)) {

                    $nameCover = 'Servicios';
                    self::makeCovers($this->customer, $this->year, $this->data, $nameCover);

                    $orders = Storage::files('customer_portal/mip/' . $this->customer->id . '/' . $this->year . $this->document);

                    $merger = new Merger(new TcpdiDriver);
                    $routeCover = 'customer_portal/mip/' . $this->customer->id . '/' . $this->year . '/' . $nameCover . '-' . $this->customer->id . '.pdf';
                    $merger->addFile(storage_path() . '/app/' . $routeCover);
                    foreach ($orders as $order) {
                        $merger->addFile(storage_path() . '/app/' . $order);
                    }
                    $mipDoc = $merger->merge();

                    $nameFile = 'Services-' . $this->year . '.pdf'; // El nombre que deseas darle al archivo

                    $folderLocal = public_path('/mip/' . $this->customer->id . '/');
                    if (!file_exists($folderLocal)) {
                        mkdir($folderLocal, 0755, true);
                    }

                    $routeDestiny = public_path('/mip/' . $this->customer->id . '/' . $nameFile); // Ruta completa de destino en la carpeta local

                    // Guarda el archivo en la carpeta local
                    file_put_contents($routeDestiny, $mipDoc);

                    Log::info('Generado Correctamente');
                }

            }

            if ($this->customer->is_main === 1) {

                $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $this->customer->id . '/' . $this->year . $this->document;
                $merger = new Merger(new TcpdiDriver);

                if (file_exists($storagePathMip)) {
                    $nameCover = 'Servicios';
                    self::makeCovers($this->customer, $this->year, $this->document, $nameCover);

                    $orders = Storage::files('customer_portal/mip/' . $this->customer->id . '/' . $this->year . $this->document);
                    $merger = new Merger(new TcpdiDriver);
                    $routeCover = 'customer_portal/mip/' . $this->customer->id . '/' . $this->year . '/' . $nameCover . '-' . $this->customer->id . '.pdf';
                    $merger->addFile(storage_path() . '/app/' . $routeCover);
                    foreach ($orders as $order) {
                        $merger->addFile(storage_path() . '/app/' . $order);
                    }
                }

                $customersBranches = customer::where('is_main', 0)
                    ->where('customer_main_id', $this->customer->id)
                    ->get();

                if (!$customersBranches->isEmpty()) {
                    foreach ($customersBranches as $customersBranch) {
                        $storagePathMip = storage_path() . '/app/customer_portal/mip/' . $customersBranch->id . '/' . $this->year . $this->document;
                        if (file_exists($storagePathMip)) {
                            $nameCover = 'Servicios';
                            self::makeCovers($customersBranch, $this->year, $this->document, $nameCover);
                            $orders = Storage::files('customer_portal/mip/' . $customersBranch->id . '/' . $this->year . $this->document);
                            $routeCover = 'customer_portal/mip/' . $customersBranch->id . '/' . $this->year . '/' . $nameCover . '-' . $customersBranch->id . '.pdf';
                            $merger->addFile(storage_path() . '/app/' . $routeCover);
                            foreach ($orders as $order) {
                                $merger->addFile(storage_path() . '/app/' . $order);
                            }
                        }

                    }
                }

                if (!empty($merger->merge())) {
                    $mipDoc = $merger->merge();

                    $nameFile = 'Services-' . $this->year . '.pdf'; // El nombre que deseas darle al archivo

                    $folderLocal = public_path('/mip/' . $this->customer->id . '/');
                    if (!file_exists($folderLocal)) {
                        mkdir($folderLocal, 0755, true);
                    }

                    $routeDestiny = public_path('/mip/' . $this->customer->id . '/' . $nameFile); // Ruta completa de destino en la carpeta local

                    // Guarda el archivo en la carpeta local
                    file_put_contents($routeDestiny, $mipDoc);

                    Log::info('Generado Correctamente Principal');

                }

            }

        } catch (\Exception $e) {
            // En caso de error, se registra el error en el registro
            Log::error('Error en el trabajo de cola: ' . $e->getMessage());
        }


    }

    private static function makeCovers($customer, $year, $data, $cover)
    {

        $company = companie::find($customer->companie);
        $pdf_logo = env('URL_STORAGE_FTP') . $company->pdf_logo;
        PDF::loadView('vendor.adminlte.customers.templateCoverMip',
            [
                'customer' => $customer,
                'company' => $company,
                'pdf_logo' => $pdf_logo,
                'data' => $data
            ]
        )->save(storage_path() . '/app/customer_portal/mip/' . $customer->id . '/' . $year . '/' . $cover . '-' . $customer->id . '.pdf');

    }

    public function failed(\Exception $exception)
    {
        // Esta función se ejecuta cuando el trabajo de cola falla
        Log::error('El trabajo de cola falló: ' . $exception->getMessage());
    }

}
