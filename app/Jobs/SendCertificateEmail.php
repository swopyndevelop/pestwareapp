<?php

namespace App\Jobs;

use App\AreaInspections;
use App\Common\Constants;
use App\companie;
use App\customer;
use App\customer_data;
use App\Http\Controllers\Invoices\InvoiceController;
use App\Http\Controllers\ReportsPDF\AreaInspectionTable;
use App\Http\Controllers\ReportsPDF\ServiceCertificate;
use App\Http\Controllers\ReportsPDF\ServiceOrder;
use App\Http\Controllers\ReportsPDF\StationInspectionTable;
use App\Invoice;
use App\Library\FacturamaLibrary;
use App\Mail\ServiceMail;
use App\Notifications;
use App\Notifications\LogsNotification;
use App\profile_job_center;
use App\quotation;
use App\service_order;
use App\StationInspection;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use PDF;

class SendCertificateEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $email;
    protected $emailInput;
    public $tries = 3;
    public $timeout = 900;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $email, $emailInput)
    {
        $this->id = $id;
        $this->email = $email;
        $this->emailInput = $emailInput;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Build service certificate and get data.
        $dataCertificate = ServiceCertificate::build($this->id);
        $order = $dataCertificate['order'];
        $companie = $order->compania;
        $jobCenterProfile = profile_job_center::where('id', $order->id_job_center)->first();
        $serviceOrder = service_order::find($this->id);
        $quote = quotation::find($serviceOrder->id_quotation);
        $customer = customer::find($quote->id_customer);
        $customerDatas = customer_data::where('customer_id', $customer->id)->first();
        $companieObject = companie::findOrFail($companie);

        // Generate pdf to report Service Certificate
        if ($companie == 269) {
            $pdf1 = PDF::loadView('vendor.adminlte.register.INFORM.informCustom', $dataCertificate)->setPaper('A4');
        } else {
            $pdf1 = PDF::loadView('vendor.adminlte.register.INFORM.inform', $dataCertificate)->setPaper('A4');
        }
        $pdf = $pdf1->output();

        // Build service order and get data
        $dataServiceOrder = ServiceOrder::build($this->id);
        $pdfOrderView = PDF::loadView('vendor.adminlte.register.INFORM.orderService', $dataServiceOrder)->setPaper('A4');
        $pdfOrder = $pdfOrderView->output();

        // Generate pdf report Station Inspection Table
        $isInspection = StationInspection::where('id_service_order', $this->id)->first();
        $pdfMonitoring = null;
        if ($isInspection) {
            $dataStationInspection = StationInspectionTable::build($this->id, $companie);
            $pdfInspection = PDF::loadView('vendor.adminlte.stationmonitoring._stationMonitoringPdf', $dataStationInspection)
                ->setPaper('A4');
            $pdfMonitoring = $pdfInspection->output();
        }

        // Generate Inspection Areas
        $pdfArea = null;
        $isAreaInspection = AreaInspections::where('id_service_order', $this->id)->first();
        if ($isAreaInspection) {
            $dataAreaInspection = AreaInspectionTable::build($this->id);
            $pdfAreaInspection = PDF::loadView('vendor.adminlte.areamonitoring._areaInspectionPdf', $dataAreaInspection)
                ->setPaper('A4');
            $pdfArea = $pdfAreaInspection->output();
        }

        // Generate Invoice.
        $pdfInvoice = null;
        $xmlInvoice = null;
        if ($customerDatas->billing_mode == Constants::$BILLING_MODE_INVOICE_BY_EACH_SERVICE) {
            $response = InvoiceController::createInvoiceForEachOrder($customer->id, $this->id);

            if ($response['code'] == 201) {
                // Download invoice pdf
                $pdfInvoice = FacturamaLibrary::downloadInvoice($response['id'], 'pdf');
                $xmlInvoice = FacturamaLibrary::downloadInvoice($response['id'], 'Xml');
            } else {
                $notification = new Notifications();
                $notification->title = 'Error al generar Factura';
                $notification->message = 'Cliente: ' . $customer->name . ' - ' . $response['message'];
                $notification->id_profile_job_center = $customer->id_profile_job_center;
                $notification->id_company = $customer->companie;
                $notification->save();
            }
        }

        // Validations for remove certificate in send email.
        if ($companie == 504) $pdf = null;


        if ($this->email != '0') {
            $user = $this->email;
            config(['mail.from.name' => $companieObject->name]);
            $result = filter_var( $user, FILTER_VALIDATE_EMAIL );
            if($result != false){
                Mail::to($user)->send(new ServiceMail(
                    $user,
                    $this->id,
                    $pdf,
                    $pdfOrder,
                    $pdfMonitoring,
                    $pdfArea,
                    $pdfInvoice,
                    $xmlInvoice,
                    $companie,
                    $jobCenterProfile->id
                ));
            }
        }

        if ($this->emailInput != '0') {
            $user = $this->emailInput;
            config(['mail.from.name' => $companieObject->name]);
            $result = filter_var( $user, FILTER_VALIDATE_EMAIL );
            if($result != false){
                Mail::to($user)->send(new ServiceMail(
                    $user,
                    $this->id,
                    $pdf,
                    $pdfOrder,
                    $pdfMonitoring,
                    $pdfArea,
                    $pdfInvoice,
                    $xmlInvoice,
                    $companie,
                    $jobCenterProfile->id
                ));
            }
        }
    }
}
