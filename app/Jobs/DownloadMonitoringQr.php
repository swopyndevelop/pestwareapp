<?php

namespace App\Jobs;

use App\Http\Controllers\ReportsPDF\MonitoringQr;
use App\Monitoring;
use App\Notifications\LogsNotification;
use App\User;
use Exception;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use PDF;

class DownloadMonitoringQr implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $companyId;
    protected $partIndex;
    public $tries = 3;
    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $companyId, $partIndex)
    {
        $this->id = $id;
        $this->companyId = $companyId;
        $this->partIndex = $partIndex;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $monitoringData = MonitoringQr::build($this->id, $this->companyId, $this->partIndex);
            $storagePath = storage_path() . '/app/monitoring_qrs/' . $monitoringData['folio'] . '/';

            if ($this->companyId == 646){
                PDF::loadView('vendor.adminlte.stationmonitoring._printQr3', ['rows'=> $monitoringData['rows'],
                    'customer' => $monitoringData['customer'], 'pdfLogo' => $monitoringData['pdfLogo']])->setPaper('letter')
                    ->save($storagePath. $monitoringData['folio'] . '-' . $this->partIndex . '.pdf');
            }

            else {
                if (count($monitoringData['stations']) == 1){
                    PDF::loadView('vendor.adminlte.stationmonitoring._printQr1', ['rows'=> $monitoringData['rows'],
                        'customer' => $monitoringData['customer'], 'pdfLogo' => $monitoringData['pdfLogo']])->setPaper('letter')
                        ->save($storagePath. $monitoringData['folio'] . '-' . $this->partIndex . '.pdf');
                }
                else if(count($monitoringData['stations']) == 2){
                    PDF::loadView('vendor.adminlte.stationmonitoring._printQr2', ['rows'=> $monitoringData['rows'],
                        'customer' => $monitoringData['customer'], 'pdfLogo' => $monitoringData['pdfLogo']])->setPaper('letter')
                        ->save($storagePath. $monitoringData['folio'] . '-' . $this->partIndex . '.pdf');
                }
                else {
                    PDF::loadView('vendor.adminlte.stationmonitoring._printQr', ['rows'=> $monitoringData['rows'],
                        'customer' => $monitoringData['customer'], 'pdfLogo' => $monitoringData['pdfLogo']])->setPaper('letter')
                        ->save($storagePath. $monitoringData['folio'] . '-' . $this->partIndex . '.pdf');
                }
            }

            // Merged all parts and save final pdf in AWS S3.
            $lastPart = --$monitoringData['no_parts'];
            if ($lastPart == $this->partIndex) {
                $directory = 'monitoring_qrs/' . $monitoringData['folio'] . '/';
                $pdfs = Storage::allFiles($directory);
                $merger = new Merger(new TcpdiDriver);

                foreach ($pdfs as $pdf) $merger->addFile(storage_path() . '/app/' . $pdf);
                $reportFinal = $merger->merge();
                $name = $monitoringData['folio'] . ".pdf";
                Storage::disk('s3')->put('/station_monitoring/qrs/' . $name, $reportFinal);
                Storage::deleteDirectory('monitoring_qrs/' . $monitoringData['folio']);
                $monitoring = Monitoring::find($this->id);
                $monitoring->no_parts = null;
                $monitoring->save();
            }
        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification($exception->getLine(), $exception->getMessage(), 3));
        }
    }
}
