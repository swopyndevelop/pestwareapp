<?php

namespace App\Jobs;

use App\Common\CommonNotifications;
use App\Common\Constants;
use App\companie;
use App\customer;
use App\Http\Controllers\Invoices\CommonInvoice;
use App\Plan;
use App\ProfileJobCenterBilling;
use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;

class CalculateFoliosInvoiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $timeout = 1800;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $companies = companie::where('is_active_billing', 1)->get();
        $jobCenters = ProfileJobCenterBilling::all();
        foreach ($companies as $company) {

            $totalGlobalInvoices = 0;
            $totalGlobalInvoicesIncomplete = 0;
            $companyId = $company->id;
            $jobCenterMain = CommonInvoice::getJobCenterIdMain($companyId);
            $jobCenterMainId = $jobCenterMain->id;
            $totalRequiredFoliosByJobCenter = new Collection();

            foreach ($jobCenters as $jobCenter) {
                $billingJobCenter = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenter->id_profile_job_center)->first();
                $totalInvoices = 0;
                $totalInvoices += $this->calculateInvoicesForAmount($jobCenter->id_profile_job_center);
                $totalInvoices += $this->calculateInvoicesForMonth($jobCenter->id_profile_job_center);
                $totalGlobalInvoices += $totalInvoices;
                if ($company->is_folios_consumed == 0 && $totalInvoices > 0) {
                    $totalGlobalInvoicesIncomplete += $totalInvoices - $billingJobCenter->folios;
                }
                $totalRequiredFoliosByJobCenter->push([
                    'jobCenterId' => $jobCenter->id_profile_job_center,
                    'foliosRequired' => $totalInvoices
                ]);
            }
            // Validate type consume folios.
            if ($company->is_folios_consumed == 0) $totalGlobalInvoices = $totalGlobalInvoicesIncomplete;
            if (!CommonInvoice::isFoliosAvailableMainInstance($companyId, $totalGlobalInvoices)) {
                // 1- Validate purchase folios in stripe automatic.
                if ($company->is_buy_folios == 1) {
                    // 2- Try purchase folios in stripe.
                    $isPurchase = $this->calculateAndPurchasePackageFolios($totalGlobalInvoices, $jobCenterMainId, $companyId);
                    if ($isPurchase) {
                        if ($company->is_folios_consumed == 0) {
                            foreach ($totalRequiredFoliosByJobCenter as $item) {
                                $centerBilling = ProfileJobCenterBilling::find($item->jobCenterId)->first();
                                $centerBilling->folios_reserved = $item->foliosRequired;
                                $centerBilling->night_billing_approved = 1;
                                $centerBilling->save();
                                // subtract folios from job center main.
                                $centerBillingMain = ProfileJobCenterBilling::find($jobCenterMainId)->first();
                                $centerBillingMain->folios = $centerBillingMain->folios - $item->foliosRequired;
                                $centerBillingMain->save();
                            }
                        } else {
                            $centerBilling = ProfileJobCenterBilling::find($jobCenterMainId)->first();
                            $centerBilling->folios_reserved = $totalGlobalInvoices;
                            $centerBilling->folios = $centerBilling->folios - $totalGlobalInvoices;
                            $centerBilling->night_billing_approved = 1;
                            $centerBilling->save();
                        }
                    }
                } else {
                    CommonNotifications::create(
                        'Folios Insuficientes',
                        'Se requieren de ' . $totalGlobalInvoices . ' folios para la facturación automática del día ' . Carbon::now()->addDay()->toDateString() . ' y no tienes configurado la compra automática. Debes comprar folios manualmente de lo contrario tus facturas no se llevarán a cabo.',
                        $jobCenterMainId,
                        $companyId
                    );
                }
            }
        }
    }

    private function calculateInvoicesForAmount($jobCenterId)
    {
        $dayNow = Carbon::now()->addDay()->day;
        $customers = customer::join('customer_datas as cd', 'cd.customer_id', 'customers.id')
            ->where('cd.billing_mode', 3)
            ->where('cd.no_invoices', '>=', 1)
            ->where('customers.id_profile_job_center', $jobCenterId)
            ->select('customers.id', 'cd.date_billing', 'customers.id_profile_job_center',
                'customers.companie', 'customers.name', 'cd.last_invoiced_day')
            ->get();

        $collectionCustomers = new Collection();
        foreach ($customers as $customer) {
            if (Carbon::parse($customer->date_billing)->day == $dayNow) $collectionCustomers->push($customer);
        }
        return $collectionCustomers->count();
    }

    private function calculateInvoicesForMonth($jobCenterId)
    {
        $totalInvoices = 0;
        $customersMonth = customer::join('customer_datas as cd', 'cd.customer_id', 'customers.id')
            ->where('cd.billing_mode', 4)
            ->where('customers.id_profile_job_center', $jobCenterId)
            ->select('customers.id', 'cd.date_billing', 'customers.id_profile_job_center',
                'customers.companie', 'customers.name')
            ->get();
        foreach ($customersMonth as $customer) {
            $orders = $this->getOrders($customer);
            if ($orders[0]->count > 0) {
                // Generate unique invoice.
                $totalInvoices++;
            }
        }
        return $totalInvoices;
    }

    private function getOrders($customer) {
        $lastDayOfMonth = Carbon::now()->endOfMonth()->toDateString();
        if ($lastDayOfMonth == Carbon::now()->addDay()->toDateString()) {
            $from = Carbon::now()->startOfMonth()->toDateString();
            $to = Carbon::now()->toDateString();
            $orders = DB::table('events as ev')
                ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('customers as c', 'q.id_customer', 'c.id')
                ->where('c.id', $customer->id)
                ->where('ev.id_status', 4)
                ->where('so.invoiced', 0)
                ->whereBetween('ev.initial_date', [$from, $to])
                ->select(DB::raw('SUM(so.subtotal) as total'), DB::raw('count(*) as count'))
                ->get();
            $otherOrders = DB::table('events as ev')
                ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('customers as c', 'q.id_customer', 'c.id')
                ->where('c.id', $customer->id)
                ->where('ev.id_status', 1)
                ->where('so.invoiced', 0)
                ->whereDate('ev.initial_date', $to)
                ->select(DB::raw('SUM(so.subtotal) as total'), DB::raw('count(*) as count'))
                ->get();
            return $orders->merge($otherOrders);
        } else return 0;
    }

    private function calculateAndPurchasePackageFolios($quantity, $jobCenterId, $companyId) {
        // Stripe
        $package = "";
        $price = 0;
        $companie = companie::find($companyId);

        // Calculate aproximate package.
        $idealPackage = Plan::where('type', Constants::$PLAN_TYPE_BILLING)
            ->where('quantity', '>=', $quantity)
            ->orderBy('quantity', 'asc')
            ->first();

        if ($idealPackage) {
            $price = $idealPackage->price / 0.01; // pennies
            $package = $idealPackage->description;
        } else {
            CommonNotifications::create(
                'Error al comprar folios',
                'Actualmente no disponemos de algún paquete de folios para cubrir la cantidad de ' . $quantity,
                $jobCenterId,
                $companyId
            );
            return false;
        }

        // Try charge in stripe.
        $stripe = new StripeClient(env('STRIPE_SECRET'));
        try {
            $stripe->charges->create([
                'customer' => $companie->id_stripe_customer,
                'amount' => $price,
                'currency' => 'mxn',
                'description' => $package,
            ]);
            CommonNotifications::create(
                'Nueva Compra de Folios Facturación',
                'Se compro: ' . $package,
                $jobCenterId,
                $companyId
            );
            // Update folios job center main.
            $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterId)->first();
            $jobCenterBilling->folios = $jobCenterBilling->folios + $idealPackage->quantity;
            $jobCenterBilling->night_billing_approved = 1;
            $jobCenterBilling->save();
            return true;
        } catch (ApiErrorException $e) {
            CommonNotifications::create(
                'Error al comprar folios',
                'Se requiere de ' . $quantity . ' folios para realizar la facturación automática de esta noche. Se ha intentado comprar: ' . $package . ' Sin embargo tu compra no ha sido exitosa. Resuelve la compra de folios lo antes posible sino tus facturas no serán timbradas. Error: ' . $e->getMessage(),
                $jobCenterId,
                $companyId
            );
            return false;
        }
    }
}
