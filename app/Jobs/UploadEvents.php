<?php

namespace App\Jobs;

use App\event;
use App\Notifications\LogsNotification;
use App\quotation;
use App\service_order;
use App\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class UploadEvents implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $timeout = 1800;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pathComplete = 'storage/app/public/prices_lists/sanabit.xls';

        if (Storage::exists($pathComplete)) {
            User::first()->notify(new LogsNotification('LOG', 'si existe', 1));
        } else User::first()->notify(new LogsNotification('LOG', 'no existe', 1));

        try {
            Excel::load($pathComplete, function($reader) {

                // Getting all results
                $rows = $reader->get();
                User::first()->notify(new LogsNotification('LOG', '49', 1));

                $folioOrder = "OS-1200-";
                $folioId = 0;

                DB::beginTransaction();

                foreach ($rows as $row) {
                    User::first()->notify(new LogsNotification('LOG', '57', 1));
                    if ($row->id != null) {
                        $eventId = $row->id;
                        $date = $row->date;

                        $event = event::find($eventId);
                        $order = service_order::find($event->id_service_order);
                        $quotation = quotation::find($order->id_quotation);

                        User::first()->notify(new LogsNotification('LOG', '66', 1));

                        $completeFolio = $folioOrder . "-" . ++$folioId;

                        // Create a new service order.
                        $service = new service_order;
                        $service->id_service_order = $completeFolio;
                        $service->id_quotation = $quotation->id;
                        $service->user_id = $order->user_id;
                        $service->id_payment_method = $order->id_payment_method;
                        $service->id_payment_way = $order->id_payment_way;
                        $service->id_status = 10;
                        $service->id_job_center = $order->id_job_center;
                        $service->companie = $order->companie;
                        $service->bussiness_name = $order->bussiness_name;
                        $service->address = $order->address;
                        $service->email = $order->email;
                        $service->observations = $order->observations;
                        $service->total = $order->total;
                        $service->subtotal = $order->subtotal;
                        $service->warranty = $order->warranty;
                        $service->reinforcement = $order->reinforcement;
                        $service->tracing = $order->tracing;
                        $service->inspection = $order->inspection;
                        $service->fake = $order->fake;
                        $service->whatsapp = $order->whatsapp;
                        $service->confirmed = $order->confirmed;
                        $service->reminder = $order->reminder;
                        $service->save();

                        // Create a new event.
                        $new_event = new event;
                        $new_event->title = $event->title;
                        $new_event->id_employee = $event->id_employee;
                        $new_event->id_service_order = $service->id;
                        $new_event->initial_hour = $event->initial_hour;
                        $new_event->final_hour = $event->final_hour;
                        $new_event->initial_date = $date;
                        $new_event->final_date = $date;
                        $new_event->start_event = "00:00";
                        $new_event->final_event = "00:00";
                        $new_event->id_job_center = $event->id_job_center;
                        $new_event->companie = $event->companie;
                        $new_event->id_status = 1;
                        $new_event->service_type = $event->service_type;
                        $new_event->save();
                    }
                }

                DB::commit();
                User::first()->notify(new LogsNotification('SUCCESS', 'FINISHED IMPORT', 1));

            });
            User::first()->notify(new LogsNotification('SUCCESS', 'FINISHED IMPORT', 1));
        }catch (Exception $exception) {
            User::first()->notify(new LogsNotification('ERROR EVENTS MASSIVE', $exception->getLine(), 3));
            DB::rollBack();
        }
    }
}
