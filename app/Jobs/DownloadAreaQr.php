<?php

namespace App\Jobs;

use App\Area;
use App\Http\Controllers\ReportsPDF\MonitoringAreaQr;
use App\Notifications\LogsNotification;
use App\User;
use Exception;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use PDF;

class DownloadAreaQr implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $companyId;
    protected $partIndex;
    public $tries = 3;
    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $companyId, $partIndex)
    {
        $this->id = $id;
        $this->companyId = $companyId;
        $this->partIndex = $partIndex;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $areaData = MonitoringAreaQr::build($this->id, $this->companyId, $this->partIndex);
            $storagePath = storage_path() . '/app/area_qrs/' . $areaData['folio'] . '/';

            if(count($areaData['sheetCard']) == 1) {
                PDF::loadView('vendor.adminlte.areamonitoring._printCardQr1', $areaData)
                    ->setPaper('letter')
                    ->save($storagePath. $areaData['folio'] . '-' . $this->partIndex . '.pdf');
            } else if (count($areaData['sheetCard']) == 2) {
                PDF::loadView('vendor.adminlte.areamonitoring._printCardQr2', $areaData)
                    ->setPaper('letter')
                    ->save($storagePath. $areaData['folio'] . '-' . $this->partIndex . '.pdf');
            } else {
                PDF::loadView('vendor.adminlte.areamonitoring._printCardQr', $areaData)
                    ->setPaper('letter')
                    ->save($storagePath. $areaData['folio'] . '-' . $this->partIndex . '.pdf');
            }

            // Merged all parts and save final pdf in AWS S3.
            $lastPart = --$areaData['no_parts'];
            if ($lastPart == $this->partIndex) {
                $directory = 'area_qrs/' . $areaData['folio'] . '/';
                $pdfs = Storage::allFiles($directory);
                $merger = new Merger(new TcpdiDriver);

                foreach ($pdfs as $pdf) $merger->addFile(storage_path() . '/app/' . $pdf);
                $reportFinal = $merger->merge();
                $name = $areaData['folio'] . ".pdf";
                Storage::disk('s3')->put('/area_monitoring/qrs/' . $name, $reportFinal);
                Storage::deleteDirectory('area_qrs/' . $areaData['folio']);
                $monitoring = Area::find($this->id);
                $monitoring->no_parts = null;
                $monitoring->save();
            }

        } catch (Exception $exception) {
            User::first()->notify(new LogsNotification($exception->getLine(), $exception->getMessage(), 3));
        }
    }
}
