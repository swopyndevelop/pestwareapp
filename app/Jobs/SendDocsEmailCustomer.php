<?php

namespace App\Jobs;

use App\AreaInspections;
use App\Common\Constants;
use App\companie;
use App\Country;
use App\customer;
use App\customer_data;
use App\Http\Controllers\Invoices\InvoiceController;
use App\Http\Controllers\ReportsPDF\AreaInspectionTable;
use App\Http\Controllers\ReportsPDF\ServiceCertificate;
use App\Http\Controllers\ReportsPDF\ServiceOrder;
use App\Http\Controllers\ReportsPDF\StationInspectionTable;
use App\Library\FacturamaLibrary;
use App\Mail\SendDocumentMail;
use App\MailAccount;
use App\Notifications;
use App\personal_mail;
use App\profile_job_center;
use App\service_order;
use App\StationInspection;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;
use PDF;

/**
 * runs in the background to send bulk documents at the close of a service by email.
 */
class SendDocsEmailCustomer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $customerId;
    protected $orderId;
    public $tries = 3;
    public $timeout = 60;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($customerId, $orderId)
    {
        $this->customerId = $customerId;
        $this->orderId = $orderId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Common data.
        $mailAccounts = MailAccount::where('id_customer', $this->customerId)->get();
        $customer = customer::find($this->customerId);
        $customerDatas = customer_data::where('customer_id', $this->customerId)->first();
        $order = service_order::find($this->orderId);
        $company = companie::find($customer->companie);
        $banner = personal_mail::where('profile_job_center_id', $customer->id_profile_job_center)->first();
        $country = Country::find($company->id_code_country);
        $urlWhatsApp = 'https://api.whatsapp.com/send?phone='. $country->code_country .$banner->image_whatsapp;
        $jobCenter = profile_job_center::find($customer->id_profile_job_center);

        $titleFileCertificate = 'Certificado de Servicio ';
        if ($company->id == 457 || $company->id == 504) $titleFileCertificate = 'Factura ';

        /**
         * For each email account of the client send the corresponding document.
         */
        foreach ($mailAccounts as $mailAccount) {

            $email = $mailAccount->email;
            $document = $mailAccount->document;
            $result = filter_var($email,FILTER_VALIDATE_EMAIL);

            switch ($document) {
                case Constants::$CERTIFICATE_PDF:
                    // Generate Service Certificate
                    $dataCertificate = ServiceCertificate::build($order->id);
                    if ($company->id == 269) {
                        $pdf1 = PDF::loadView('vendor.adminlte.register.INFORM.informCustom', $dataCertificate)
                            ->setPaper('A4');
                    } else {
                        $pdf1 = PDF::loadView('vendor.adminlte.register.INFORM.inform', $dataCertificate)
                            ->setPaper('A4');
                    }
                    $pdf = $pdf1->output();
                    $data = [
                        'subjectText' => $company->name . ' Certificado de Control de Plagas',
                        'banner' => $banner,
                        'urlWhatsApp' => $urlWhatsApp,
                        'messengerPersonal' => $jobCenter,
                        'customer' => $customer->name,
                        'accountName' => $mailAccount->name,
                        'nameFile' => $titleFileCertificate . $order->id_service_order . '.pdf',
                        'mime' => 'application/pdf',
                        'pdf' => base64_encode($pdf),
                        'xml' => null
                    ];
                    if($result != false) Mail::to($email)->send(new SendDocumentMail($data));
                    break;
                case Constants::$ORDER_SERVICE_PDF:
                    // Generate Service Order
                    $dataServiceOrder = ServiceOrder::build($order->id);
                    $pdfOrderView = PDF::loadView('vendor.adminlte.register.INFORM.orderService', $dataServiceOrder)
                        ->setPaper('A4');
                    $pdfOrder = $pdfOrderView->output();
                    $data = [
                        'subjectText' => $company->name . ' Orden de Servicio',
                        'banner' => $banner,
                        'urlWhatsApp' => $urlWhatsApp,
                        'messengerPersonal' => $jobCenter,
                        'customer' => $customer->name,
                        'accountName' => $mailAccount->name,
                        'nameFile' => 'Orden de Servicio ' . $order->id_service_order . '.pdf',
                        'mime' => 'application/pdf',
                        'pdf' => base64_encode($pdfOrder),
                        'xml' => null
                    ];
                    if($result != false) Mail::to($email)->send(new SendDocumentMail($data));
                    break;
                case Constants::$INSPECTION_STATIONS_PDF:
                    // Generate Inspection Monitoring Stations
                    $isInspection = StationInspection::where('id_service_order', $order->id)->first();
                    if ($isInspection) {
                        $dataStationInspection = StationInspectionTable::build($order->id, $company->id);
                        $pdfInspection = PDF::loadView('vendor.adminlte.stationmonitoring._stationMonitoringPdf', $dataStationInspection)
                            ->setPaper('A4');
                        $pdfMonitoring = $pdfInspection->output();
                        $data = [
                            'subjectText' => $company->name . ' Inspección de Servicio - Monitoreo de Estaciones',
                            'banner' => $banner,
                            'urlWhatsApp' => $urlWhatsApp,
                            'messengerPersonal' => $jobCenter,
                            'customer' => $customer->name,
                            'accountName' => $mailAccount->name,
                            'nameFile' => 'Inspección - Monitoreo de Estaciones ' . $isInspection->id_inspection . '.pdf',
                            'mime' => 'application/pdf',
                            'pdf' => base64_encode($pdfMonitoring),
                            'xml' => null
                        ];
                        if($result != false) Mail::to($email)->send(new SendDocumentMail($data));
                    }
                    break;
                case Constants::$INSPECTION_AREAS_PDF:
                    // Generate Inspection Monitoring Areas
                    $isInspection = AreaInspections::where('id_service_order', $order->id)->get();
                    if ($isInspection->count() > 0) {
                        $dataPdfInspection = AreaInspectionTable::build($order->id);
                        $pdf = PDF::loadView('vendor.adminlte.areamonitoring._areaInspectionPdf', $dataPdfInspection)->setPaper('A4');
                        $pdfMonitoringAreas = $pdf->output();
                        $data = [
                            'subjectText' => $company->name . ' Inspección de Servicio - Monitoreo de Áreas',
                            'banner' => $banner,
                            'urlWhatsApp' => $urlWhatsApp,
                            'messengerPersonal' => $jobCenter,
                            'customer' => $customer->name,
                            'accountName' => $mailAccount->name,
                            'nameFile' => 'Inspección - Monitoreo de Áreas ' . $order->id_service_order . '.pdf',
                            'mime' => 'application/pdf',
                            'pdf' => base64_encode($pdfMonitoringAreas),
                            'xml' => null
                        ];
                        if($result != false) Mail::to($email)->send(new SendDocumentMail($data));
                    }
                    break;
                case Constants::$INVOICE_PDF_XML:
                    // Generate Invoice.
                    $pdfInvoice = null;
                    $xmlInvoice = null;
                    if ($customerDatas->billing_mode == Constants::$BILLING_MODE_INVOICE_BY_EACH_SERVICE) {
                        $response = InvoiceController::createInvoiceForEachOrder($customer->id, $order->id);

                        if ($response['code'] == 201) {
                            // Download invoice pdf
                            $pdfInvoice = FacturamaLibrary::downloadInvoice($response['id'], 'pdf');
                            $xmlInvoice = FacturamaLibrary::downloadInvoice($response['id'], 'Xml');
                            $data = [
                                'subjectText' => $company->name . ' Factura de Servicio',
                                'banner' => $banner,
                                'urlWhatsApp' => $urlWhatsApp,
                                'messengerPersonal' => $jobCenter,
                                'customer' => $customer->name,
                                'accountName' => $mailAccount->name,
                                'nameFile' => 'Factura ' . $order->id_service_order,
                                'mime' => 'application/pdf',
                                'pdf' => $pdfInvoice,
                                'xml' => $xmlInvoice
                            ];
                            if($result != false) Mail::to($email)->send(new SendDocumentMail($data));
                        } else {
                            $notification = new Notifications();
                            $notification->title = 'Error al generar Factura';
                            $notification->message = 'Cliente: ' . $customer->name . ' - ' . $response['message'];
                            $notification->id_profile_job_center = $customer->id_profile_job_center;
                            $notification->id_company = $customer->companie;
                            $notification->save();
                        }
                    }
                    break;
            }
        }
    }
}
