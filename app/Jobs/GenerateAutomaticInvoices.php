<?php

namespace App\Jobs;

use App\Common\CommonNotifications;
use App\companie;
use App\customer;
use App\customer_data;
use App\Http\Controllers\Invoices\CommonInvoice;
use App\Http\Controllers\Invoices\InvoiceController;
use App\Notifications;
use App\Notifications\LogsNotification;
use App\profile_job_center;
use App\ProfileJobCenterBilling;
use App\ReportsSlack;
use App\UnitSat;
use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateAutomaticInvoices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $bodyLogs = '';
    protected $totalInvoices = 0;
    protected $successInvoices = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now = Carbon::now();
        $this->bodyLogs .= '************************************************************************************'.PHP_EOL;
        $this->bodyLogs .= 'GENERATE ALL SCHEDULE INVOICES OF CUSTOMERS'.PHP_EOL;
        $this->bodyLogs .= 'LOG DATETIME: ' . $now.PHP_EOL;

        $jobCenters = ProfileJobCenterBilling::all();
        foreach ($jobCenters as $jobCenter) {
            if ($jobCenter->night_billing_approved == 1) {
                $profileJobCenter = profile_job_center::find($jobCenter->id_profile_job_center);
                $companyId = $profileJobCenter->companie;
                $this->generateInvoicesByAmount($jobCenter->id_profile_job_center);
                $this->generateInvoicesByMonth($jobCenter->id_profile_job_center);
                CommonNotifications::create(
                    'Reporte de Facturación Automática',
                    'TOTAL FACTURAS REALIZADAS: '  . $this->successInvoices . '/' . $this->totalInvoices,
                    $jobCenter->id_profile_job_center,
                    $companyId
                );
                $this->totalInvoices = 0;
                $this->successInvoices = 0;
            } else {
                $profileJobCenter = profile_job_center::find($jobCenter->id_profile_job_center);
                $companyId = $profileJobCenter->companie;
                CommonNotifications::create(
                    'Folios Insuficientes',
                    'No fue posible realizar el proceso de facturación electrónica por motivos de folios insuficientes.',
                    $jobCenter->id_profile_job_center,
                    $companyId
                );
            }
        }

        // Returns the remaining reserved folios, and they are added to their folios.
        $this->returnLeftoverFolios();
    }

    private function generateInvoicesByAmount($jobCenterId)
    {
        $dayNow = Carbon::now()->day;
        $customers = customer::join('customer_datas as cd', 'cd.customer_id', 'customers.id')
            ->where('cd.billing_mode', 3)
            ->where('cd.no_invoices', '>=', 1)
            ->where('customers.id_profile_job_center', $jobCenterId)
            ->select('customers.id', 'cd.date_billing', 'customers.id_profile_job_center',
                'customers.companie', 'customers.name', 'cd.last_invoiced_day')
            ->get();

        $collectionCustomers = new Collection();
        foreach ($customers as $customer) {
            if (Carbon::parse($customer->date_billing)->day == $dayNow) $collectionCustomers->push($customer);
        }

        // Create invoices
        $logs = new Collection();
        $invoicesSuccess = new Collection();
        foreach ($collectionCustomers as $customer) {
            if ($customer->last_invoiced_day == null) $from = Carbon::parse($customer->date_billing)->subMonth()->toDateString();
            else $from = Carbon::parse($customer->last_invoiced_day)->addDay()->toDateString();
            $to = Carbon::now()->toDateString();
            $dataOrders = $this->getFoliosAndOrderIds($customer, $from, $to);
            $foliosText = $dataOrders['folios'];
            $orderIds = $dataOrders['orderIds'];
            $response = InvoiceController::createInvoiceByAmount($customer->id, $foliosText, $orderIds);
            $logs->push($response);
            if ($response['code'] == 201) $invoicesSuccess->push($response);
            else {
                $notification = new Notifications();
                $notification->title = 'Error al generar Factura';
                $notification->message = 'Cliente: ' . $customer->name . ' - ' . $response['message'];
                $notification->id_profile_job_center = $customer->id_profile_job_center;
                $notification->id_company = $customer->companie;
                $notification->save();
            }
        }
        $now = Carbon::now();
        $this->bodyLogs .= 'TOTAL INVOICES SUCCESS: ' . $invoicesSuccess->count() . '/' . $collectionCustomers->count().PHP_EOL;
        $this->bodyLogs .= 'LOG DATETIME: ' . $now.PHP_EOL;
        $this->bodyLogs .= 'LOGS RESPONSE: ' . $logs.PHP_EOL;
        $this->bodyLogs .= '************************************************************************************'.PHP_EOL.PHP_EOL.PHP_EOL;

        $this->totalInvoices += $collectionCustomers->count();
        $this->successInvoices += $invoicesSuccess->count();
    }

    private function generateInvoicesByMonth($jobCenterId)
    {
        $lastDayOfMonth = Carbon::now()->endOfMonth()->toDateString();
        $logs = new Collection();
        $invoicesSuccess = new Collection();
        if ($lastDayOfMonth == Carbon::now()->toDateString()) { //Carbon::now()->toDateString()
            $this->bodyLogs .= '************************************************************************************'.PHP_EOL;
            $this->bodyLogs .= 'GENERATE ALL SCHEDULE INVOICES OF END DAY OF MONTH'.PHP_EOL;
            $customersMonth = customer::join('customer_datas as cd', 'cd.customer_id', 'customers.id')
                ->where('cd.billing_mode', 4)
                ->where('customers.id_profile_job_center', $jobCenterId)
                ->select('customers.id', 'cd.date_billing', 'customers.id_profile_job_center',
                    'customers.companie', 'customers.name')
                ->get();

            $from = Carbon::now()->startOfMonth()->toDateString();
            $to = Carbon::now()->toDateString();
            foreach ($customersMonth as $customer) {
                $orders = $this->getOrders($customer);
                $foliosAndOrderIds = $this->getFoliosAndOrderIds($customer, $from, $to);
                $foliosText = $foliosAndOrderIds['folios'];
                $orderIds = $foliosAndOrderIds['orderIds'];
                $items = $foliosAndOrderIds['items'];
                if ($orders[0]->count > 0) {
                    $total = $orders[0]->total;
                    // Generate unique invoice.
                    $response = InvoiceController::createInvoiceByMonth($customer->id, $total, $foliosText, $orderIds, $items);
                    $logs->push($response);
                    if ($response['code'] == 201) $invoicesSuccess->push($response);
                    else {
                        $notification = new Notifications();
                        $notification->title = 'Error al generar Factura';
                        $notification->message = 'Cliente: ' . $customer->name . ' - ' . $response['message'];
                        $notification->id_profile_job_center = $customer->id_profile_job_center;
                        $notification->id_company = $customer->companie;
                        $notification->save();
                    }
                } else {
                    $this->bodyLogs .= 'NO HAY ORDENES DE SERVICIO PARA: ' . $customer->name;
                }
            }

            $now = Carbon::now();
            $this->bodyLogs .= 'TOTAL INVOICES BY MONTH SUCCESS: ' . $invoicesSuccess->count() . '/' . $customersMonth->count().PHP_EOL;
            $this->bodyLogs .= 'LOG DATETIME: ' . $now.PHP_EOL;
            $this->bodyLogs .= 'LOGS RESPONSE: ' . $logs.PHP_EOL;
            $this->bodyLogs .= '************************************************************************************'.PHP_EOL;

            $reportSlack = new ReportsSlack();
            $title = 'REPORT CRON JOB INVOICES: ' . Carbon::now()->toDateTimeString();
            $reportSlack->notify(new LogsNotification($title, $this->bodyLogs, 1));

            $this->totalInvoices += $customersMonth->count();
            $this->successInvoices += $invoicesSuccess->count();

        }
    }

    private function getOrders($customer) {
        $from = Carbon::now()->startOfMonth()->toDateString();
        $to = Carbon::now()->toDateString();
        $orders = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->where('c.id', $customer->id)
            ->where('ev.id_status', 4)
            ->where('so.invoiced', 0)
            ->whereBetween('ev.initial_date', [$from, $to])
            ->select(DB::raw('SUM(so.subtotal) as total'), DB::raw('count(*) as count'))
            ->get();
        $otherOrders = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->where('c.id', $customer->id)
            ->where('ev.id_status', 1)
            ->where('so.invoiced', 0)
            ->whereDate('ev.initial_date', $to)
            ->select(DB::raw('SUM(so.subtotal) as total'), DB::raw('count(*) as count'))
            ->get();
        return $orders->merge($otherOrders);
    }

    private function getFoliosAndOrderIds($customer, $from, $to) {
        $customerData = customer_data::where('customer_id', $customer->id)->first();
        $unitCode = UnitSat::find($customerData->unit_code_billing);
        $jobCenterBilling = ProfileJobCenterBilling::where('id_profile_job_center', $customer->id_profile_job_center)->first();
        $orders = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->where('c.id', $customer->id)
            ->where('ev.id_status', 4)
            ->where('so.invoiced', 0)
            ->whereBetween('ev.initial_date', [$from, $to])
            ->select('so.id_service_order as folio', 'so.id', 'so.subtotal')
            ->get();
        $otherOrders = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->where('c.id', $customer->id)
            ->where('ev.id_status', 1)
            ->where('so.invoiced', 0)
            ->whereDate('ev.initial_date', $to)
            ->select('so.id_service_order as folio', 'so.id', 'so.subtotal')
            ->get();
        $orders = $orders->merge($otherOrders);
        $foliosText = "";
        $items = array();
        foreach ($orders as $order) {
            $foliosText .= $order->folio . ", ";
            $totalIva = ($order->subtotal * $jobCenterBilling->iva) / 100;
            $rate = $jobCenterBilling->iva / 100;
            $total = $order->subtotal + $totalIva;
            $item = [
                "Quantity" => "1",
                "ProductCode" => $customerData->service_code_billing, // 84111506
                "UnitCode" => $unitCode->key, // E48
                "Unit" => "Unidad de servicio",
                "Description" => $order->folio . ' - ' . $customerData->description_billing, // API folios adicionales
                "IdentificationNumber" => "21",
                "UnitPrice" => $order->subtotal,
                "Subtotal" => $order->subtotal,
                "Discount" => "0",
                "DiscountVal" => "0",
                "TaxObject" => "02",
                "Taxes" => [
                    [
                        "Name" => "IVA",
                        "Rate" => $rate, // 0.16
                        "Total" => $totalIva, // base * rate = total
                        "Base" => $order->subtotal,
                        "IsRetention" => "false"
                    ],
                ],
                "Total" => $total
            ];
            array_push($items, $item);
        }
        return [
            'folios' => trim($foliosText, ', '),
            'orderIds' => $orders,
            'items' => $items
        ];
    }

    private function returnLeftoverFolios()
    {
        $companies = companie::where('is_active_billing', 1)->get();
        $jobCenters = ProfileJobCenterBilling::all();
        foreach ($companies as $company) {
            $companyId = $company->id;
            $jobCenterMain = CommonInvoice::getJobCenterIdMain($companyId);
            $jobCenterMainId = $jobCenterMain->id;

            foreach ($jobCenters as $jobCenter) {
                $billingJobCenter = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenter->id_profile_job_center)->first();
                if ($company->is_folios_consumed == 0) {
                    $billingJobCenter->folios = $billingJobCenter->folios + $billingJobCenter->folios_reserved;
                    $billingJobCenter->folios_reserved = 0;
                    $billingJobCenter->save();
                }
            }

            if ($company->is_folios_consumed == 1) {
                $billingJobCenter = ProfileJobCenterBilling::where('id_profile_job_center', $jobCenterMainId)->first();
                $billingJobCenter->folios = $billingJobCenter->folios + $billingJobCenter->folios_reserved;
                $billingJobCenter->folios_reserved = 0;
                $billingJobCenter->save();
            }
        }
    }
}
