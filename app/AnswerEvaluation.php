<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\AnswerEvaluation
 *
 * @property int $id
 * @property string|null $answer_evaluations_id
 * @property string|null $user_id
 * @property int|null $question_evaluations_id
 * @property string|null $option_evaluations_id
 * @property float|null $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|AnswerEvaluation newModelQuery()
 * @method static Builder|AnswerEvaluation newQuery()
 * @method static Builder|AnswerEvaluation query()
 * @method static Builder|AnswerEvaluation whereAnswerEvaluationsId($value)
 * @method static Builder|AnswerEvaluation whereCreatedAt($value)
 * @method static Builder|AnswerEvaluation whereId($value)
 * @method static Builder|AnswerEvaluation whereOptionEvaluationsId($value)
 * @method static Builder|AnswerEvaluation whereQuestionEvaluationsId($value)
 * @method static Builder|AnswerEvaluation whereUpdatedAt($value)
 * @method static Builder|AnswerEvaluation whereUserId($value)
 * @method static Builder|AnswerEvaluation whereValue($value)
 * @mixin Eloquent
 */
class AnswerEvaluation extends Model
{
    protected $table = "answer_evaluations";
    protected $primarykey = "answer_evaluations_id";
    protected $fillable = ['id', 'answer_evaluations_id', 'user_id','option_evaluations_id', 'value'];
}
