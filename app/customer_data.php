<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use setasign\Fpdi\PdfParser\StreamReader;

/**
 * Class customer_data
 *
 * @package App
 * @author Alberto Martínez
 * @version 03/01/2019
 * @property int $id
 * @property int $customer_id
 * @property string|null $address
 * @property string|null $address_number
 * @property string|null $state
 * @property string|null $reference_address
 * @property string|null $email
 * @property string|null $phone_number
 * @property int|null $pay_type_id
 * @property string|null $billing
 * @property string|null $rfc
 * @property string|null $tax_residence
 * @property string|null $expense_type
 * @property string|null $contact_two_name
 * @property string|null $contact_two_phone
 * @property string|null $email_billing
 * @property int $billing_mode
 * @property string|null $cfdi_type
 * @property string|null $fiscal_regime
 * @property string|null $payment_method
 * @property string|null $payment_way
 * @property string|null $service_code_billing
 * @property string|null $service_code_billing_name
 * @property string|null $unit_code_billing
 * @property string|null $description_billing
 * @property double|null $amount_billing
 * @property string|null $date_billing
 * @property int|null $no_invoices
 * @property string|null $last_invoiced_day
 * @property string|null $billing_street
 * @property string|null $billing_exterior_number
 * @property string|null $billing_interior_number
 * @property string|null $billing_colony
 * @property string|null $billing_postal_code
 * @property string|null $billing_municipality
 * @property string|null $billing_state
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|customer_data newModelQuery()
 * @method static Builder|customer_data newQuery()
 * @method static Builder|customer_data query()
 * @method static Builder|customer_data whereAddress($value)
 * @method static Builder|customer_data whereAddressNumber($value)
 * @method static Builder|customer_data whereBilling($value)
 * @method static Builder|customer_data whereContactTwoName($value)
 * @method static Builder|customer_data whereContactTwoPhone($value)
 * @method static Builder|customer_data whereCreatedAt($value)
 * @method static Builder|customer_data whereCustomerId($value)
 * @method static Builder|customer_data whereEmail($value)
 * @method static Builder|customer_data whereExpenseType($value)
 * @method static Builder|customer_data whereId($value)
 * @method static Builder|customer_data wherePayTypeId($value)
 * @method static Builder|customer_data wherePhoneNumber($value)
 * @method static Builder|customer_data whereReferenceAddress($value)
 * @method static Builder|customer_data whereRfc($value)
 * @method static Builder|customer_data whereState($value)
 * @method static Builder|customer_data whereTaxResidence($value)
 * @method static Builder|customer_data whereUpdatedAt($value)
 * @mixin Eloquent
 */
class customer_data extends Model
{
    /**
     * @var string
     */
    protected $table = "customer_datas";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'customer_id',
        'address',
        'address_number',
        'state',
        'reference_address',
        'email',
        'phone_number',
        'pay_type_id',
        'billing',
        'rfc',
        'tax_residence',
        'expense_type',
        'contact_two_name',
        'contact_two_phone',
        'email_billing',
        'billing_mode',
        'cfdi_type',
        'payment_method',
        'payment_way',
        'service_code_billing',
        'service_code_billing_name',
        'unit_code_billing',
        'description_billing',
        'amount_billing',
        'date_billing',
        'no_invoices',
        'last_invoiced_day',
        'inhabitant_type_id',
        'condition_type_id',
        'mascot_id',
        'observations',
        'billing_street',
        'billing_exterior_number',
        'billing_interior_number',
        'billing_colony',
        'billing_postal_code',
        'billing_municipality',
        'billing_state',
        'fiscal_regime'
    ];

    /**
    * @author Olga Rodríguez
    * @version 12/01/2019
    *Función para relación muchos a muchos con tabla habit_conditions
    */
}
