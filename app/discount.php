<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class discount
 *
 * @package App
 * @author Olga Rodríguez
 * @version 16/01/2019
 * @property int $id
 * @property string $title
 * @property int $profile_job_center_id
 * @property string $description
 * @property int $id_company
 * @property float $percentage
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|quotation[] $quotation
 * @property-read int|null $quotation_count
 * @method static Builder|discount newModelQuery()
 * @method static Builder|discount newQuery()
 * @method static Builder|discount query()
 * @method static Builder|discount whereCreatedAt($value)
 * @method static Builder|discount whereDescription($value)
 * @method static Builder|discount whereId($value)
 * @method static Builder|discount whereIdCompany($value)
 * @method static Builder|discount wherePercentage($value)
 * @method static Builder|discount whereTitle($value)
 * @method static Builder|discount whereUpdatedAt($value)
 * @mixin Eloquent
 */

class discount extends Model
{
     /**
     * @var string
     */
    protected $table = "discounts";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'description',
        'percentage',
        'profile_job_center_id',
        'id_company',
        'visible'
    ];

    /**
    * @author Olga Rodríguez
    * @version 12/01/2019
    *Función para relación muchos a muchos con tabla quotations.
    */
    public function quotation()
    {
        return $this->belongsToMany(quotation::class);
    }
}
