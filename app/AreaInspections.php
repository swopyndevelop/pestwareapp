<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AreaInspections
 * @package App
 * @property int $id
 * @property int $id_area_node
 * @property int $id_technician
 * @property int $id_service_order
 * @property string $date_inspection
 * @property string $hour_inspection
 * @property string $comments
 * @mixin Eloquent
 */
class AreaInspections extends Model
{
    /**
     * @var string
     */
    protected $table = "area_inspections";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_area_node',
        'id_technician',
        'id_service_order',
        'date_inspection',
        'hour_inspection',
        'comments'
    ];
}
