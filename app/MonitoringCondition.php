<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MonitoringCondition
 * @package App
 * @property int $id
 * @property int $id_company
 * @property string $name
 */
class MonitoringCondition extends Model
{
    /**
     * @var string
     */
    protected $table = "monitoring_conditions";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_company',
        'name'
    ];
}
