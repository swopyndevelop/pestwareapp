<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class plague_control_picture
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/06/2019
 * @property int $id
 * @property int $plague_control_id
 * @property string $file_route
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read plague_control $plague_control
 * @method static Builder|plague_control_picture newModelQuery()
 * @method static Builder|plague_control_picture newQuery()
 * @method static Builder|plague_control_picture query()
 * @method static Builder|plague_control_picture whereCreatedAt($value)
 * @method static Builder|plague_control_picture whereFileRoute($value)
 * @method static Builder|plague_control_picture whereId($value)
 * @method static Builder|plague_control_picture wherePlagueControlId($value)
 * @method static Builder|plague_control_picture whereUpdatedAt($value)
 * @mixin Eloquent
 */
class plague_control_picture extends Model
{
    /**
     * @var string
     */
    protected $table = "plague_control_pictures";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'plague_control_id',
        'file_route'
    ];

    /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación uno a muchos con tabla plague_controls
    */
    public function plague_control()
    {
        return $this->belongsTo(plague_control::class);
    }
}
