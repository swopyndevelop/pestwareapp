<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\transfer_center_employee
 *
 * @property int $id
 * @property string $id_transfer_ce
 * @property int $id_user
 * @property int $id_job_center_origin
 * @property int $id_employee_destiny
 * @property float $total
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|transfer_center_employee newModelQuery()
 * @method static Builder|transfer_center_employee newQuery()
 * @method static Builder|transfer_center_employee query()
 * @method static Builder|transfer_center_employee whereCreatedAt($value)
 * @method static Builder|transfer_center_employee whereId($value)
 * @method static Builder|transfer_center_employee whereIdEmployeeDestiny($value)
 * @method static Builder|transfer_center_employee whereIdJobCenterOrigin($value)
 * @method static Builder|transfer_center_employee whereIdTransferCe($value)
 * @method static Builder|transfer_center_employee whereIdUser($value)
 * @method static Builder|transfer_center_employee whereTotal($value)
 * @method static Builder|transfer_center_employee whereUpdatedAt($value)
 * @mixin Eloquent
 */
class transfer_center_employee extends Model
{
    /**
     * @var string
     */
    protected $table = "transfer_center_employees";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_transfer_ce',
        'id_user',
        'id_job_center_origin',
        'id_employee_destiny',
        'total',
        'created_at',
        'companie'
    ];
}
