<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class order_cleaning
 *
 * @package App
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $companie
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|place_condition[] $place_conditions
 * @property-read int|null $place_conditions_count
 * @method static Builder|order_cleaning newModelQuery()
 * @method static Builder|order_cleaning newQuery()
 * @method static Builder|order_cleaning query()
 * @method static Builder|order_cleaning whereCompanie($value)
 * @method static Builder|order_cleaning whereCreatedAt($value)
 * @method static Builder|order_cleaning whereId($value)
 * @method static Builder|order_cleaning whereName($value)
 * @method static Builder|order_cleaning whereUpdatedAt($value)
 * @mixin Eloquent
 */
class order_cleaning extends Model
{
    /**
     * @var string
     */
    protected $table = "order_cleanings";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'companie',
        'visible',
        'profile_job_center_id',
    ];

     /**
    * @author Olga Rodríguez
    * @version 06/06/2019
    *Función para relación muchos a muchos con tabla place_conditions
    */
    public function place_conditions()
    {
        return $this->belongsToMany(place_condition::class);
    }
}
