<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\transfer_employee_employee
 *
 * @property int $id
 * @property string $id_transfer_ee
 * @property int $id_user
 * @property int $id_employee_origin
 * @property int $id_employee_destiny
 * @property float $total
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|transfer_employee_employee newModelQuery()
 * @method static Builder|transfer_employee_employee newQuery()
 * @method static Builder|transfer_employee_employee query()
 * @method static Builder|transfer_employee_employee whereCompanie($value)
 * @method static Builder|transfer_employee_employee whereCreatedAt($value)
 * @method static Builder|transfer_employee_employee whereId($value)
 * @method static Builder|transfer_employee_employee whereIdEmployeeDestiny($value)
 * @method static Builder|transfer_employee_employee whereIdEmployeeOrigin($value)
 * @method static Builder|transfer_employee_employee whereIdTransferEe($value)
 * @method static Builder|transfer_employee_employee whereIdUser($value)
 * @method static Builder|transfer_employee_employee whereTotal($value)
 * @method static Builder|transfer_employee_employee whereUpdatedAt($value)
 * @mixin Eloquent
 */
class transfer_employee_employee extends Model
{
    /**
     * @var string
     */
    protected $table = "transfer_employee_employees";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_transfer_ee',
        'id_user',
        'id_job_employee_origin',
        'id_job_employee_destiny',
        'total',
        'created_at',
        'companie'
    ];
}
