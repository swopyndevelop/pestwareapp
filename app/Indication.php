<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Indication
 *
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property string $key
 * @property string $description
 * @property int $company_id
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Indication newModelQuery()
 * @method static Builder|Indication newQuery()
 * @method static Builder|Indication query()
 * @method static Builder|Indication whereCompanyId($value)
 * @method static Builder|Indication whereCreatedAt($value)
 * @method static Builder|Indication whereDescription($value)
 * @method static Builder|Indication whereId($value)
 * @method static Builder|Indication whereKey($value)
 * @method static Builder|Indication whereName($value)
 * @method static Builder|Indication whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Indication extends Model
{
    /**
     * @var string
     */
    protected $table = "indications";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'key',
        'description',
        'profile_job_center_id',
        'company_id',
        'visible'
    ];
}
