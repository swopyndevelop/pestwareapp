<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CheckListMonitoringOption
 * @package App
 * @property int $id
 * @property int $id_monitoring_checklist
 * @property string $option
 */

class CheckListMonitoringOption extends Model
{
    protected $table = 'check_monitoring_options';

    protected $fillable = ['id_monitoring_checklist','option'];
}
