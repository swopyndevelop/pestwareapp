<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\article_order
 *
 * @property int $id
 * @property int $id_purchase_order
 * @property string $concept
 * @property int $id_product
 * @property int $quantity
 * @property float $unit_price
 * @property float $subtotal
 * @property float $total
 * @property boolean $is_partial
 * @property int $quantity_partial
 * @property int $companie
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|article_order newModelQuery()
 * @method static Builder|article_order newQuery()
 * @method static Builder|article_order query()
 * @method static Builder|article_order whereCompanie($value)
 * @method static Builder|article_order whereConcept($value)
 * @method static Builder|article_order whereCreatedAt($value)
 * @method static Builder|article_order whereId($value)
 * @method static Builder|article_order whereIdPurchaseOrder($value)
 * @method static Builder|article_order whereQuantity($value)
 * @method static Builder|article_order whereSubtotal($value)
 * @method static Builder|article_order whereUnitPrice($value)
 * @method static Builder|article_order whereUpdatedAt($value)
 * @mixin Eloquent
 */
class article_order extends Model
{
    /**
     * @var string
     */
    protected $table = "article_orders";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_purchase_order',
        'concept',
        'id_product',
        'quantity',
        'unit_price',
        'subtotal',
        'total',
        'is_partial',
        'quantity_partial',
        'companie'
    ];

}
