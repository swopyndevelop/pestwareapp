<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class rejected_quotation
 *
 * @package App
 * @author Olga Rodríguez
 * @version 06/03/2019
 * @property int $id
 * @property int $id_quotation
 * @property string $reason
 * @property string|null $commentary
 * @property string $date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|rejected_quotation newModelQuery()
 * @method static Builder|rejected_quotation newQuery()
 * @method static Builder|rejected_quotation query()
 * @method static Builder|rejected_quotation whereCommentary($value)
 * @method static Builder|rejected_quotation whereCreatedAt($value)
 * @method static Builder|rejected_quotation whereDate($value)
 * @method static Builder|rejected_quotation whereId($value)
 * @method static Builder|rejected_quotation whereIdQuotation($value)
 * @method static Builder|rejected_quotation whereReason($value)
 * @method static Builder|rejected_quotation whereUpdatedAt($value)
 * @mixin Eloquent
 */
class rejected_quotation extends Model
{
    /**
     * @var string
     */
    protected $table = "rejected_quotations";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_quotation',
        'reason',
        'commentary',
        'date'
    ];
}
