<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class job_title_function
 *
 * @package App
 * @author Olga Rodríguez
 * @version 03/03/2019
 * @property int $id
 * @property int $job_title_functions_id
 * @property int $job_title_profiles_id
 * @property string|null $function_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|job_title_function newModelQuery()
 * @method static Builder|job_title_function newQuery()
 * @method static Builder|job_title_function query()
 * @method static Builder|job_title_function whereCreatedAt($value)
 * @method static Builder|job_title_function whereFunctionName($value)
 * @method static Builder|job_title_function whereId($value)
 * @method static Builder|job_title_function whereJobTitleFunctionsId($value)
 * @method static Builder|job_title_function whereJobTitleProfilesId($value)
 * @method static Builder|job_title_function whereUpdatedAt($value)
 * @mixin Eloquent
 */
class job_title_function extends Model
{
    /**
     * @var string
     */
    protected $table = "job_title_functions";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'job_title_functions_id',
        'job_title_profiles_id',
        'function_name'
    ];
}
