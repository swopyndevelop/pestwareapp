<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class source_origin
 *
 * @package App
 * @author Olga Rodríguez
 * @version 25/02/2019
 * @property int $id
 * @property string $name
 * @property int $profile_job_center_id
 * @property int $companie
 * @property string $visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|source_origin newModelQuery()
 * @method static Builder|source_origin newQuery()
 * @method static Builder|source_origin query()
 * @method static Builder|source_origin whereCompanie($value)
 * @method static Builder|source_origin whereCreatedAt($value)
 * @method static Builder|source_origin whereId($value)
 * @method static Builder|source_origin whereName($value)
 * @method static Builder|source_origin whereUpdatedAt($value)
 * @mixin Eloquent
 */

class source_origin extends Model
{
    /**
     * @var string
     */
    protected $table = "source_origins";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'companie',
        'visible',
        'profile_job_center_id',
    ];
}
