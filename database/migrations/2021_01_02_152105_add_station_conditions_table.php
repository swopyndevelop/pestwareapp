<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStationConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('station_conditions', function (Blueprint $table) {
            $table->integer('id_node')->unsigned()->after('id');
            $table->foreign('id_node')->references('id')->on('monitoring_trees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('station_conditions', function (Blueprint $table) {
            //
        });
    }
}
