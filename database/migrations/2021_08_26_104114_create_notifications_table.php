<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('message');
            $table->string('action_url')->nullable();
            $table->boolean('is_read')->default(0);
            $table->string('permission_read')->nullable();
            $table->integer('id_profile_job_center')->unsigned();
            $table->foreign('id_profile_job_center')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_company')->unsigned();
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
