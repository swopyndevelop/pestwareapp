<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileJobCentersBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_job_centers_billing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profile_job_center')->unsigned();
            $table->foreign('id_profile_job_center')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->string('rfc');
            $table->string('fiscal_regime');
            $table->string('iva');
            $table->string('email');
            $table->string('address');
            $table->string('address_number');
            $table->string('colony');
            $table->string('municipality');
            $table->string('state');
            $table->string('code_postal');
            $table->string('file_certificate');
            $table->string('file_key');
            $table->string('password');
            $table->boolean('status_connection_sat')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_job_centers_billing');
    }
}
