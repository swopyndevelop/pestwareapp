<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPartialToArticleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_orders', function (Blueprint $table) {
            $table->boolean('is_partial')->default(false)->after('total');
            $table->integer('quantity_partial')->after('is_partial')->nuallable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_orders', function (Blueprint $table) {
            //
        });
    }
}
