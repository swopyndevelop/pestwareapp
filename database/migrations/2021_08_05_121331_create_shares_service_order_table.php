<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharesServiceOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shares_service_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_service_order_main')->unsigned();
            $table->foreign('id_service_order_main')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_service_order')->unsigned();
            $table->foreign('id_service_order')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_employee')->unsigned();
            $table->foreign('id_employee')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shares_service_order');
    }
}
