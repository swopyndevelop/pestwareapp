<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategorieToPlagueTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plague_types', function (Blueprint $table) {
            $table->integer('id_categorie')->unsigned()->after('plague_key')->default(1);
            $table->integer('is_monitoring')->after('id_categorie')->default(0);
            $table->foreign('id_categorie')->references('id')->on('plague_categories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plague_types', function (Blueprint $table) {
            //
        });
    }
}
