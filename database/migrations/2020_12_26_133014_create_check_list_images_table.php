<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckListImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_list_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_check_monitoring_response')->unsigned();
            $table->foreign('id_check_monitoring_response')->references('id')->on('check_monitoring_responses')->onUpdate('cascade')->onDelete('cascade');
            $table->string('urlImage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_list_images');
    }
}
