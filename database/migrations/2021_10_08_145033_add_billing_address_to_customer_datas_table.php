<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillingAddressToCustomerDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_datas', function (Blueprint $table) {
            $table->string('billing_street')->nullable()->after('last_invoiced_day');
            $table->string('billing_exterior_number')->nullable()->after('billing_street');
            $table->string('billing_interior_number')->nullable()->after('billing_exterior_number');
            $table->string('billing_colony')->nullable()->after('billing_interior_number');
            $table->string('billing_postal_code')->nullable()->after('billing_colony');
            $table->string('billing_municipality')->nullable()->after('billing_postal_code');
            $table->string('billing_state')->nullable()->after('billing_municipality');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_datas', function (Blueprint $table) {
            //
        });
    }
}
