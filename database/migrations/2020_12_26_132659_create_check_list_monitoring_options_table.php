<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckListMonitoringOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_monitoring_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_monitoring_checklist')->unsigned();
            $table->foreign('id_monitoring_checklist')->references('id')->on('monitoring_checklists')->onUpdate('cascade')->onDelete('cascade');
            $table->string('option');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_list_monitoring_options');
    }
}
