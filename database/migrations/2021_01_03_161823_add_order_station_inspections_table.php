<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderStationInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('station_inspections', function (Blueprint $table) {
            $table->integer('id_service_order')->after('id_monitoring')->unsigned();
            $table->foreign('id_service_order')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('station_inspections', function (Blueprint $table) {
            //
        });
    }
}
