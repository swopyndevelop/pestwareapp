<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFoliosToProfJobBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_job_centers_billing', function (Blueprint $table) {
            $table->integer('folios')->default(0)->after('text_mail');
            $table->integer('folios_consumed')->default(0)->after('folios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_job_centers_billing', function (Blueprint $table) {
            //
        });
    }
}
