<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBranchToProfileJobCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_job_centers', function (Blueprint $table) {
            $table->string('email_personal')->after('name')->nullable();
            $table->string('business_name')->after('email_personal')->nullable();
            $table->string('rfc')->after('business_name');
            $table->string('license')->after('rfc')->nullabble();
            $table->string('messenger_personal')->after('license')->nullable();
            $table->string('whatsapp_personal')->after('messenger_personal')->nullable();
            $table->string('facebook_personal')->after('whatsapp_personal');
            $table->string('health_manager')->after('facebook_personal')->nullable();
            $table->string('sanitary_license')->after('health_manager')->nullable();
            $table->string('warning_service',5000)->after('sanitary_license')->nullable();
            $table->string('contract_service', 5000)->after('warning_service')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_job_centers', function (Blueprint $table) {
            //
        });
    }
}
