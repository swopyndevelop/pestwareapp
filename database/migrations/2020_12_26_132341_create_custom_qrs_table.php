<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomQrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_qrs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_monitoring')->unsigned();
            $table->foreign('id_monitoring')->references('id')->on('monitorings')->onUpdate('cascade')->onDelete('cascade');
            $table->string('title');
            $table->string('subtitle');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_qrs');
    }
}
