<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_station_response')->unsigned();
            $table->integer('id_condition')->unsigned();
            $table->timestamps();
            $table->foreign('id_station_response')->references('id')->on('check_monitoring_responses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_condition')->references('id')->on('monitoring_conditions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_conditions');
    }
}
