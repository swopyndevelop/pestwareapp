<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdInspectionToMonitoringConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('station_conditions', function (Blueprint $table) {
            $table->integer('id_inspection')->unsigned()->default(32)->after('id_condition');
            $table->foreign('id_inspection')->references('id')->on('station_inspections')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('station_conditions', function (Blueprint $table) {
            //
        });
    }
}
