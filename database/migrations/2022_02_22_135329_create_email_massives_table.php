<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailMassivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_massives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url_web')->nullable();
            $table->string('url_facebook')->nullable();
            $table->string('number_whatsapp')->nullable();
            $table->string('number_phone')->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->string('address')->nullable();
            $table->string('colony')->nullable();
            $table->string('municipality')->nullable();
            $table->string('state')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('license')->nullable();
            $table->string('date')->nullable();
            $table->string('health_officer')->nullable();
            $table->string('rfc')->nullable();
            $table->string('observations')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_massives');
    }
}
