<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_billing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('identification_number')->nullable();
            $table->text('description');
            $table->double('unit_price', 8, 2);
            $table->string('property_account')->nullable();
            $table->string('unit_code_billing_id');
            $table->string('product_code_billing_id');
            $table->string('unit_code_billing');
            $table->string('product_code_billing');
            $table->string('iva');
            $table->string('ieps');
            $table->string('iva_ret');
            $table->string('isr');
            $table->integer('profile_job_center_id')->unsigned();
            $table->foreign('profile_job_center_id')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_company')->unsigned();
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_billing');
    }
}
