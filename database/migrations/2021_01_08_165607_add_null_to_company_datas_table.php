<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullToCompanyDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_datas', function (Blueprint $table) {
            //
            $table->string('city')->nullable()->change();
            $table->string('country_code')->nullable()->change();
            $table->string('country_name')->nullable()->change();
            $table->string('ip')->nullable()->change();
            $table->string('latitude')->nullable()->change();
            $table->string('longitude')->nullable()->change();
            $table->string('region_code')->nullable()->change();
            $table->string('region_name')->nullable()->change();
            $table->string('time_zone')->nullable()->change();
            $table->string('zip_code')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_datas', function (Blueprint $table) {
            //
        });
    }
}
