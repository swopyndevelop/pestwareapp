<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('value');
            $table->integer('profile_job_center_id')->unsigned()->default(1);
            $table->foreign('profile_job_center_id')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_company')->unsigned()->default(1);
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->char('visible',1)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxes');
    }
}
