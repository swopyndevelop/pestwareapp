<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdTypeAreaToMonitoringTreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_stations', function (Blueprint $table) {
            $table->integer('id_type_area')->unsigned()->default(4)->after('name');
            $table->foreign('id_type_area')->references('id')->on('type_areas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_stations', function (Blueprint $table) {
            //
        });
    }
}
