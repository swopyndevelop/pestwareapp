<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillingOthersSatToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_datas', function (Blueprint $table) {
            $table->double('amount_billing', 8, 2)->after('description_billing')->nullable();
            $table->date('date_billing')->after('amount_billing')->nullable();
            $table->integer('no_invoices')->after('date_billing')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_datas', function (Blueprint $table) {
            //
        });
    }
}
