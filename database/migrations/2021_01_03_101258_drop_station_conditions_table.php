<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropStationConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('station_conditions', function (Blueprint $table) {
            $table->dropForeign('station_conditions_id_station_response_foreign');
            $table->dropColumn('id_station_response');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('station_conditions', function (Blueprint $table) {
            //
        });
    }
}
