<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyBillingDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_billing_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reason_social');
            $table->string('rfc');
            $table->string('email');
            $table->string('cfdi_type');
            $table->string('billing_street')->nullable();
            $table->string('billing_exterior_number')->nullable();
            $table->string('billing_interior_number')->nullable();
            $table->string('billing_colony')->nullable();
            $table->string('billing_postal_code')->nullable();
            $table->string('billing_municipality')->nullable();
            $table->string('billing_state')->nullable();
            $table->integer('id_company')->unsigned();
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_billing_data');
    }
}
