<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRequestsApiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_requests_api', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->nullable();
            $table->string('endpoint')->nullable();
            $table->string('method')->nullable();
            $table->string('ip')->nullable();
            $table->string('user')->nullable();
            $table->string('duration')->nullable();
            $table->string('cookie')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_requests_api');
    }
}
