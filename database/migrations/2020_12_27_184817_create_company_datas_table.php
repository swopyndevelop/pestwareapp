<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_company')->unsigned();
            $table->string('city');
            $table->string('country_code');
            $table->string('country_name');
            $table->ipAddress('ip');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('region_code');
            $table->string('region_name');
            $table->string('time_zone');
            $table->string('zip_code');
            $table->timestamps();
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_datas');
    }
}
