<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInspectionToResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('check_monitoring_responses', function (Blueprint $table) {
            $table->integer('id_inspection')->after('id')->unsigned();
            $table->foreign('id_inspection')->references('id')->on('station_inspections')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('check_monitoring_responses', function (Blueprint $table) {
            //
        });
    }
}
