<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_inspections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_inspection');
            $table->integer('id_monitoring')->unsigned();
            $table->integer('id_technician')->unsigned();
            $table->string('comments');
            $table->date('date');
            $table->time('hour');
            $table->timestamps();
            $table->foreign('id_monitoring')->references('id')->on('monitorings')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_technician')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_inspections');
    }
}
