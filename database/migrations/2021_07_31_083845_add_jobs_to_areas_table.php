<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobsToAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->char('status', 1)->default('0')->after('visible');
            $table->dateTime('date_updated_file')->nullable()->after('status');
            $table->integer('no_parts')->nullable()->after('date_updated_file');
            $table->dateTime('date_updated_tree')->nullable()->after('no_parts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas', function (Blueprint $table) {
            //
        });
    }
}
