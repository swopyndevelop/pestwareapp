<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricalInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->date('date');
            $table->time('hour');
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_type_movement_inventory')->unsigned();
            $table->foreign('id_type_movement_inventory')->references('id')->on('type_movements_inventories')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_product')->unsigned();
            $table->foreign('id_product')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->string('source');
            $table->string('destiny');
            $table->integer('quantity');
            $table->integer('fraction_quantity');
            $table->integer('before_stock');
            $table->integer('after_stock');
            $table->integer('fraction_before_stock');
            $table->integer('fraction_after_stock');
            $table->double('unit_price',20,2);
            $table->double('value_movement', 20,2);
            $table->double('value_inventory', 20,2);
            $table->integer('id_profile_job_center')->unsigned();
            $table->foreign('id_profile_job_center')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_company')->unsigned();
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_inventories');
    }
}
