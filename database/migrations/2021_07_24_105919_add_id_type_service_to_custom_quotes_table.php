<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdTypeServiceToCustomQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_quotes', function (Blueprint $table) {
            $table->integer('id_type_service')->unsigned()->default(1)->after('id_type_concepts');
            $table->foreign('id_type_service')->references('id')->on('type_services')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_quotes', function (Blueprint $table) {
            //
        });
    }
}
