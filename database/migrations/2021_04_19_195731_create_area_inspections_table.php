<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_inspections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_area_node')->unsigned();
            $table->integer('id_technician')->unsigned();
            $table->integer('id_service_order')->unsigned()->nullable();
            $table->date('date_inspection')->nullable();
            $table->time('hour_inspection')->nullable();
            $table->string('comments', 255)->nullable();
            $table->foreign('id_area_node')->references('id')->on('areas_tree')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_technician')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_inspections');
    }
}
