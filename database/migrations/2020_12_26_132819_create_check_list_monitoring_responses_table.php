<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckListMonitoringResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_monitoring_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_check_monitoring_option')->unsigned();
            $table->foreign('id_check_monitoring_option')->references('id')->on('check_monitoring_options')->onUpdate('cascade')->onDelete('cascade');
            $table->string('value');
            $table->string('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_list_monitoring_responses');
    }
}
