<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropMonitoringResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('check_monitoring_responses', function (Blueprint $table) {
            $table->dropForeign('check_monitoring_responses_id_node_foreign');
            $table->dropColumn('id_node');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('check_monitoring_responses', function (Blueprint $table) {
            //
        });
    }
}
