<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfileJobCenterIdToPersonalMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personal_mails', function (Blueprint $table) {
            //
            $table->integer('profile_job_center_id')->unsigned()->after('id_company')->default(1);
            $table->foreign('profile_job_center_id')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->string('reminder_whatsapp',5000)->after('image_messenger')->default('Recordatorio antes de las 24 horas.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personal_mails', function (Blueprint $table) {
            //
        });
    }
}
