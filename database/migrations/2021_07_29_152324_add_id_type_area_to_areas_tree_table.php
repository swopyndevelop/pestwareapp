<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdTypeAreaToAreasTreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas_tree', function (Blueprint $table) {
            $table->integer('id_type_area')->unsigned()->default(1)->after('text');
            $table->foreign('id_type_area')->references('id')->on('type_stations')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas_tree', function (Blueprint $table) {
            //
        });
    }
}
