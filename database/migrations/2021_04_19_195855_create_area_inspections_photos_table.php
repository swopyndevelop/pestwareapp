<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaInspectionsPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_inspections_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_area_inspection')->unsigned();
            $table->string('photo');
            $table->foreign('id_area_inspection')->references('id')->on('area_inspections')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_inspections_photos');
    }
}
