<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeConceptToCustomQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_quotes', function (Blueprint $table) {
            $table->integer('id_type_concepts')->unsigned()->after('subtotal')->default(3);
            $table->foreign('id_type_concepts')->references('id')->on('type_concepts')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_quotes', function (Blueprint $table) {
            //
        });
    }
}
