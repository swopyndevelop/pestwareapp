<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitoringNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoring_nodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_monitoring_tree')->unsigned();
            $table->foreign('id_monitoring_tree')->references('id')->on('monitoring_trees')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_type_area')->unsigned();
            $table->foreign('id_type_area')->references('id')->on('type_areas')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoring_nodes');
    }
}
