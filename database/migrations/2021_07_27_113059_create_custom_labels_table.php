<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_labels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_label')->unsigned();
            $table->foreign('id_label')->references('id')->on('labels')->onUpdate('cascade')->onDelete('cascade');
            $table->string('label_spanish')->nullable();
            $table->string('label_english')->nullable();
            $table->integer('id_profile_job_center')->unsigned();
            $table->foreign('id_profile_job_center')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_labels');
    }
}
