<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonitoringImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('check_list_images', function (Blueprint $table) {
            $table->integer('id_inspection')->after('id')->unsigned();
            $table->integer('id_station')->after('id_inspection')->unsigned();
            $table->foreign('id_inspection')->references('id')->on('station_inspections')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_station')->references('id')->on('monitoring_trees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('check_list_images', function (Blueprint $table) {
            //
        });
    }
}
