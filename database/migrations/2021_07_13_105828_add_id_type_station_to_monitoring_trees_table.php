<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdTypeStationToMonitoringTreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monitoring_trees', function (Blueprint $table) {
            $table->integer('id_type_station')->unsigned()->default(1);
            $table->foreign('id_type_station')->references('id')->on('type_stations')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monitoring_trees', function (Blueprint $table) {
            //
        });
    }
}
