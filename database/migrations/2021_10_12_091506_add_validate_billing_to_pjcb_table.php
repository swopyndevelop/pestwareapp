<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValidateBillingToPjcbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile_job_centers_billing', function (Blueprint $table) {
            $table->integer('folios_reserved')->default(0)->after('folios_consumed');
            $table->boolean('night_billing_approved')->default(0)->after('folios_reserved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_job_centers_billing', function (Blueprint $table) {
            //
        });
    }
}
