<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonitoringToCheckMonitoringResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('check_monitoring_responses', function (Blueprint $table) {
            $table->integer('id_node')->after('id')->unsigned();
            $table->foreign('id_node')->references('id')->on('monitoring_nodes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('check_monitoring_responses', function (Blueprint $table) {
            //
        });
    }
}
