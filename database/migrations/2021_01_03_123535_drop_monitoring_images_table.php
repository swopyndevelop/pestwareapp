<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropMonitoringImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('check_list_images', function (Blueprint $table) {
            $table->dropForeign('check_list_images_id_check_monitoring_response_foreign');
            $table->dropColumn('id_check_monitoring_response');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('check_list_images', function (Blueprint $table) {
            //
        });
    }
}
