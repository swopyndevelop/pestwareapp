<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaguesResponseInspectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plagues_response_inspection', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_station_response')->unsigned();
            $table->integer('id_plague')->unsigned();
            $table->integer('quantity')->default(0);
            $table->timestamps();
            $table->foreign('id_station_response')->references('id')->on('check_monitoring_responses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_plague')->references('id')->on('plague_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plagues_response_inspection');
    }
}
