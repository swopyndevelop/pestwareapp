<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateQuotationsTable
 * Migration for create table quotations.
 * @author Olga Rodríguez
 * @version 16/01/2019
 * @copyright Swopyn 2019
 */
class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_quotation')->unsigned();
            $table->integer('id_customer')->unsigned();
            $table->integer('construction_measure')->nullable();
            $table->integer('garden_measure')->nullable();
            $table->integer('id_plague_jer')->nullable();
            $table->integer('establishment_id')->unsigned()->nullable();
            $table->integer('spaces_to_fumigate')->nullable();
            $table->string('description', 4000)->nullable();
            $table->double('total', 8,2);
            $table->integer('price')->nullable();
            $table->timestamps();

            $table->foreign('id_customer')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('establishment_id')->references('id')->on('establishment_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
