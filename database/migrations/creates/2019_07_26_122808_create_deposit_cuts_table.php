<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositCutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_cuts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cash_cut')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('image', 250);
            $table->date('date');
            $table->time('hour');
            $table->double('total');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_cash_cut')->references('id')->on('cash_cuts')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_cuts');
    }
}
