<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCustomerDataMascotsTable
 * Migration for create table customer_data_mascot.
 * @author Olga Rodríguez
 * @version 12/01/2019
 * @copyright Swopyn 2019
 */
class CreateCustomerDataMascotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_data_mascot', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_data_id')->unsigned();
            $table->integer('mascot_id')->unsigned();
            $table->timestamps();

            $table->foreign('customer_data_id')->references('id')->on('customer_datas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('mascot_id')->references('id')->on('mascots')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_data_mascots');
    }
}
