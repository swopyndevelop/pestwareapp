<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddFieldsToServiceOrders
 * Migration for modify table service_orders.
 * @author Olga Rodríguez
 * @version 02/05/2019
 * @copyright Swopyn 2019
 */
class AddFieldsToServiceOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_orders', function (Blueprint $table) {
            $table->integer('id_status')->unsigned()->nullable()->after('id_payment_way');
            $table->integer('id_job_center')->unsigned()->nullable()->after('id_status');

            $table->foreign('id_status')->references('id')->on('statuses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_job_center')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_orders', function (Blueprint $table) {
            //
        });
    }
}
