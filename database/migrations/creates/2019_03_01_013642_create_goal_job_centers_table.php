<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateGoalJobCentersTable
 * Migration for create table goal_job_centers.
 * @author Olga Rodríguez
 * @version 01/03/2019
 * @copyright Swopyn 2019
 */
class CreateGoalJobCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goal_job_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('goal_job_centers_id', 191);
            $table->integer('profile_job_centers_id')->unsigned();
            $table->integer('value_january');
            $table->integer('value_february');
            $table->integer('value_march');
            $table->integer('value_april');
            $table->integer('value_may');
            $table->integer('value_june');
            $table->integer('value_july');
            $table->integer('value_august');
            $table->integer('value_september');
            $table->integer('value_october');
            $table->integer('value_november');
            $table->integer('value_december');
            $table->integer('value_spTotal');
            $table->timestamps();

            $table->foreign('profile_job_centers_id')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goal_job_centers');
    }
}
