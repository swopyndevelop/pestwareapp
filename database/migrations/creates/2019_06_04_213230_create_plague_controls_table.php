<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreatePlagueControlsTable
 * Migration for create table plague_controls.
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @copyright Swopyn 2019
 */
class CreatePlagueControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plague_controls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_service_order')->unsigned();
            $table->string('control_areas', 250);
            $table->integer('id_application_method')->unsigned();
            $table->integer('id_product')->unsigned();
            $table->string('dose');
            $table->double('quantity', 8,2);
            $table->string('commentary', 250);
            $table->timestamps();

            $table->foreign('id_service_order')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('id_application_method')->references('id')->on('application_methods')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('id_product')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plague_controls');
    }
}
