<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddFieldsToCustomerDatas
 * Migration for modify table customer_datas.
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @copyright Swopyn 2019
 */
class AddFieldsToCustomerDatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_datas', function (Blueprint $table) {
            $table->string('contact_two_name')->nullable()->after('expense_type');
            $table->string('contact_two_phone')->nullable()->after('contact_two_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_datas', function (Blueprint $table) {
            //
        });
    }
}
