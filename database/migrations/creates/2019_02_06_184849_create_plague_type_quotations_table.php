<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePlagueTypeQuotationsTable
 * Migration for create table plague_type_quotation.
 * @author Olga Rodríguez
 * @version 06/02/2019
 * @copyright Swopyn 2019
 */
class CreatePlagueTypeQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plague_type_quotation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotation_id')->unsigned();
            $table->integer('plague_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('quotation_id')->references('id')->on('quotations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('plague_type_id')->references('id')->on('plague_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plague_type_quotations');
    }
}
