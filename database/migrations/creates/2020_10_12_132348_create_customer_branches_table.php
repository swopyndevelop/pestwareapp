<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch_id')->nullable();
            $table->integer('customer_id')->unsigned();
            $table->string('name');
            $table->string('manager')->nullable();
            $table->string('phone', 10)->nullable();
            $table->string('email')->nullable();
            $table->string('address_number');
            $table->string('address');
            $table->string('colony');
            $table->string('municipality');
            $table->string('state');
            $table->string('postal_code');
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_branches');
    }
}
