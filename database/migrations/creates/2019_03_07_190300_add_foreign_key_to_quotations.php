<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddForeignKeyToQuotations
 * Migration for create table quotations.
 * @author Olga Rodríguez
 * @version 07/03/2019
 * @copyright Swopyn 2019
 */
class AddForeignKeyToQuotations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotations', function (Blueprint $table) {
            $table->integer('id_status')->unsigned()->nullable()->after('price');

            $table->foreign('id_status')->references('id')->on('statuses')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
