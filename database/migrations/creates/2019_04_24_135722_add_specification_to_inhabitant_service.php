<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddSpecificationToInhabitantService
 * Migration for modify table inhabitant_type_service_order.
 * @author Olga Rodríguez
 * @version 24/04/2019
 * @copyright Swopyn 2019
 */
class AddSpecificationToInhabitantService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inhabitant_type_service_order', function (Blueprint $table) {
            $table->string('specification', 250)->nullable()->after('inhabitant_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inhabitant_type_service_order', function (Blueprint $table) {
            //
        });
    }
}
