<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreatePlaceInspectionsTable
 * Migration for create table place_inspections.
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @copyright Swopyn 2019
 */
class CreatePlaceInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place_inspections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_service_order')->unsigned();
            $table->string('nesting_areas', 250);
            $table->string('commentary', 250);
            $table->timestamps();

            $table->foreign('id_service_order')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place_inspections');
    }
}
