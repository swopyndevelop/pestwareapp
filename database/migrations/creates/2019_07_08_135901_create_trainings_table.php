<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTrainingsTable
 * Migration for create table trainings.
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @copyright Swopyn 2019
 */
class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('duration');
            $table->string('description');
            $table->date('start');
            $table->date('end');
            $table->string('course');
            $table->string('job_title_name')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
