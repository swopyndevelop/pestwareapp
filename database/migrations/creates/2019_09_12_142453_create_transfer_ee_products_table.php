<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferEeProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_ee_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_transfer_ee')->unsigned();
            $table->integer('id_product')->unsigned();
            $table->string('units');
            $table->timestamps();
       

            $table->foreign('id_transfer_ee')->references('id')->on('transfer_employee_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_product')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_ee_products');
    }
}
