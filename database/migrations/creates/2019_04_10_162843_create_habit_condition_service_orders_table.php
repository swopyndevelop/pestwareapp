<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateHabitConditionServiceOrdersTable
 * Migration for create table habit_condition_service_order.
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @copyright Swopyn 2019
 */
class CreateHabitConditionServiceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habit_condition_service_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_order_id')->unsigned();
            $table->integer('habit_condition_id')->unsigned();
            $table->timestamps();

            $table->foreign('service_order_id')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('habit_condition_id')->references('id')->on('habit_conditions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habit_condition_service_orders');
    }
}
