<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCustomersTable
 * Migration for create table customers.
 * @author Alberto Martínez
 * @version 03/01/2019
 * @copyright Swopyn 2019
 */
class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 250);
            $table->string('establishment_name', 250)->nullable();
            $table->string('cellphone', 10);
            $table->integer('establishment_id')->unsigned();
            $table->string('colony', 250);
            $table->string('municipality', 250);
            $table->timestamps();

            $table->foreign('establishment_id')->references('id')->on('establishment_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
