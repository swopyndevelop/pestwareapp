<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateMascotServiceOrdersTable
 * Migration for create table mascot_service_order.
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @copyright Swopyn 2019
 */
class CreateMascotServiceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mascot_service_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_order_id')->unsigned();
            $table->integer('mascot_id')->unsigned();
            $table->timestamps();

            $table->foreign('service_order_id')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('mascot_id')->references('id')->on('mascots')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mascot_service_orders');
    }
}
