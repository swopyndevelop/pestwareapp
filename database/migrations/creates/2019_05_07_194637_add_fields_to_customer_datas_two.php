<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddFieldsToCustomerDatasTwo
 * Migration for modify table customer_datas.
 * @author Olga Rodríguez
 * @version 07/05/2019
 * @copyright Swopyn 2019
 */
class AddFieldsToCustomerDatasTwo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_datas', function (Blueprint $table) {
             $table->string('address_number')->nullable()->after('address');
            $table->string('state')->nullable()->after('address_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_datas', function (Blueprint $table) {
            //
        });
    }
}
