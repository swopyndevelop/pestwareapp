<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaceInspectionPlagueTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place_inspection_plague_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('place_inspection_id')->unsigned();
            $table->integer('plague_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('place_inspection_id')->references('id')->on('place_inspections')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('plague_type_id')->references('id')->on('plague_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place_inspection_plague_type');
    }
}
