<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateJobTitleFunctionsTable
 * Migration for create table job_title_functions.
 * @author Olga Rodríguez
 * @version 28/02/2019
 * @copyright Swopyn 2019
 */
class CreateJobTitleFunctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_title_functions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_title_functions_id')->unsigned();
            $table->integer('job_title_profiles_id')->unsigned();
            $table->string('function_name', 191)->nullable();
            $table->timestamps();

            $table->foreign('job_title_functions_id')->references('id_inc')->on('tree_job_profiles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('job_title_profiles_id')->references('id_inc')->on('tree_job_profiles')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_title_functions');
    }
}
