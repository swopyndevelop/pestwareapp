<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_adjustment');
            $table->integer('id_user')->unsigned();
            $table->integer('id_storehouse')->unsigned();
            $table->integer('id_company')->unsigned();
            $table->string('comments');
            $table->integer('units_before');
            $table->float('quantity_before');
            $table->double('total_before', 8,2);
            $table->integer('units_adjustment');
            $table->float('quantity_adjustment');
            $table->double('total_adjustment', 8,2);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_storehouse')->references('id')->on('storehouses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_adjustments');
    }
}
