<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactureEntrysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facture_entrys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_entry')->unsigned();
            $table->integer('file_route');
            $table->timestamps();
        

            $table->foreign('id_entry')->references('id')->on('entrys')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facture_entrys');
    }

}
