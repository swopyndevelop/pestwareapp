<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateGeneralInformationJobCentersTable
 * Migration for create table general_information_job_centers.
 * @author Olga Rodríguez
 * @version 28/02/2019
 * @copyright Swopyn 2019
 */
class CreateGeneralInformationJobCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_information_job_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('general_information_job_centers_id', 191)->nullable();
            $table->integer('profile_job_centers_id')->nullable();
            $table->time('hour_init')->nullable();
            $table->time('hour_final')->nullable();
            $table->string('start_workdays', 191)->nullable();
            $table->string('final_workdays', 191)->nullable();
            $table->float('small_box')->nullable();
            $table->binary('file_small_box')->nullable();
            $table->float('breakeven')->nullable();
            $table->binary('rent_contract')->nullable();
            $table->date('validity_rent_contract')->nullable();
            $table->binary('hacienda')->nullable();
            $table->date('validity_hacienda')->nullable();
            $table->binary('commercial_permission')->nullable();
            $table->date('validity_commercial_permission')->nullable();
            $table->binary('fumigation')->nullable();
            $table->date('validity_fumigation')->nullable();
            $table->binary('extinguisher')->nullable();
            $table->date('validity_extinguisher')->nullable();
            $table->binary('salubrity')->nullable();
            $table->date('validity_salubrity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_information_job_centers');
    }
}
