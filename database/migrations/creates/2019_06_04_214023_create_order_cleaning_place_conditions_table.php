<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreateOrderCleaningPlaceConditionsTable
 * Migration for create table order_cleaning_place_condition.
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @copyright Swopyn 2019
 */
class CreateOrderCleaningPlaceConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_cleaning_place_condition', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('place_condition_id')->unsigned();
            $table->integer('order_cleaning_id')->unsigned();
            $table->timestamps();

            $table->foreign('place_condition_id')->references('id')->on('place_conditions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('order_cleaning_id')->references('id')->on('order_cleanings')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_cleaning_place_conditions');
    }
}
