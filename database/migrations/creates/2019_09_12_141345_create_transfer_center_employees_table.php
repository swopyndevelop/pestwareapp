<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferCenterEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_center_employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_transfer_ce');
            $table->integer('id_user')->unsigned();
            $table->integer('id_job_center_origin')->unsigned();
            $table->integer('id_employee_destiny')->unsigned();
            $table->double('total');
            $table->timestamps();
        

            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_job_center_origin')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_employee_destiny')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_center_employees');
    }
}
