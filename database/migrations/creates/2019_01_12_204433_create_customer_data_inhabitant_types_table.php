<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCustomerDataInhabitantTypesTable
 * Migration for create table customer_data_inhabitant_type.
 * @author Olga Rodríguez
 * @version 12/01/2019
 * @copyright Swopyn 2019
 */
class CreateCustomerDataInhabitantTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_data_inhabitant_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_data_id')->unsigned();
            $table->integer('inhabitant_types_id')->unsigned();
            $table->timestamps();

            $table->foreign('customer_data_id')->references('id')->on('customer_datas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('inhabitant_types_id')->references('id')->on('inhabitant_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_data_inhabitant_types');
    }
}
