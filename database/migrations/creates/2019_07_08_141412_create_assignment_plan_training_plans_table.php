<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAssignmentPlanTrainingPlansTable
 * Migration for create table assignment_plan_training_plan.
 * @author Olga Rodríguez
 * @version 08/07/2019
 * @copyright Swopyn 2019
 */
class CreateAssignmentPlanTrainingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_plan_training_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assignment_plan_id')->unsigned();
            $table->integer('training_plan_id')->unsigned();
            $table->timestamps();

            $table->foreign('assignment_plan_id')->references('id')->on('assignment_plans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('training_plan_id')->references('id')->on('training_plans')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_plan_training_plans');
    }
}
