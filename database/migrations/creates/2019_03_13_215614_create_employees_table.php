<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEmployeesTable
 * Migration for create table employees.
 * @author Olga Rodríguez
 * @version 13/03/2019
 * @copyright Swopyn 2019
 */
class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('name');
            $table->integer('id_company')->unsigned();
            $table->integer('job_title_profile_id')->unsigned();
            $table->integer('profile_job_center_id')->unsigned();

            $table->timestamps();

            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('job_title_profile_id')->references('id')->on('job_title_profiles')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('profile_job_center_id')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
