<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreateConditionPicturesTable
 * Migration for create table condition_pictures.
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @copyright Swopyn 2019
 */
class CreateConditionPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condition_pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('place_condition_id')->unsigned();
            $table->string('file_route');
            $table->timestamps();

            $table->foreign('place_condition_id')->references('id')->on('place_conditions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condition_pictures');
    }
}
