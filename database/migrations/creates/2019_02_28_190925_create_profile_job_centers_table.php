<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateProfileJobCentersTable
 * Migration for create table profile_job_centers.
 * @author Olga Rodríguez
 * @version 28/02/2019
 * @copyright Swopyn 2019
 */
class CreateProfileJobCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_job_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_job_centers_id')->unsigned();
            $table->string('name', 191)->nullable();
            $table->string('code', 191)->nullable();
            $table->binary('image')->nullable();
            $table->string('type', 191)->nullable();
            $table->integer('total')->nullable();

            $table->timestamps();

            $table->foreign('profile_job_centers_id')->references('id_inc')->on('tree_job_centers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_job_centers');
    }
}
