<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddSpecificationToMascotService
 * Migration for modify table mascot_service_order.
 * @author Olga Rodríguez
 * @version 24/04/2019
 * @copyright Swopyn 2019
 */
class AddSpecificationToMascotService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mascot_service_order', function (Blueprint $table) {
            $table->string('specification', 250)->nullable()->after('mascot_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mascot_service_order', function (Blueprint $table) {
            //
        });
    }
}
