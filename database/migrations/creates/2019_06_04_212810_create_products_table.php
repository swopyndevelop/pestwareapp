<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreateProductsTable
 * Migration for create table products.
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @copyright Swopyn 2019
 */
class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_type_product')->unsigned();
            $table->string('name');
            $table->string('description', 250);
            $table->string('characteristics', 250);
            $table->string('suggested_use');
            $table->integer('id_presentation')->unsigned();
            $table->integer('id_unit')->unsigned();
            $table->double('quantity', 8,2);
            $table->string('active_ingredient')->nullable();
            $table->timestamps();

            $table->foreign('id_type_product')->references('id')->on('product_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_presentation')->references('id')->on('product_presentations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_unit')->references('id')->on('product_units')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
