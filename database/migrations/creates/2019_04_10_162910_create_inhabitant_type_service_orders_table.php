<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInhabitantTypeServiceOrdersTable
 * Migration for create table inhabitant_type_service_orders.
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @copyright Swopyn 2019
 */
class CreateInhabitantTypeServiceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inhabitant_type_service_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_order_id')->unsigned();
            $table->integer('inhabitant_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('service_order_id')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('inhabitant_type_id')->references('id')->on('inhabitant_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inhabitant_type_service_orders');
    }
}
