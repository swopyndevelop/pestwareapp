<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class AddForeignKeyToQuotationsThree
 * Migration for modify table quotations.
 * @author Olga Rodríguez
 * @version 01/04/2019
 * @copyright Swopyn 2019
 */
class AddForeignKeyToQuotationsThree extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotations', function (Blueprint $table) {
            $table->integer('id_extra')->unsigned()->nullable()->after('id_job_center');

            $table->foreign('id_extra')->references('id')->on('extras')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotations', function (Blueprint $table) {
            //
        });
    }
}
