<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAddressJobCentersTable
 * Migration for create table address_job_centers.
 * @author Olga Rodríguez
 * @version 01/03/2019
 * @copyright Swopyn 2019
 */
class CreateAddressJobCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_job_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('address_job_centers_id')->unsigned();
            $table->integer('profile_job_centers_id')->unsigned();
            $table->string('street', 191);
            $table->integer('num_ext');
            $table->string('num_int', 191)->nullable();
            $table->string('district', 191);
            $table->integer('zip_code');
            $table->string('location', 191);
            $table->string('municipality', 191);
            $table->string('state', 191);
            $table->string('country', 191);
            $table->string('ubication', 200);
            $table->string('phone', 11);
            $table->string('email', 191);
            $table->timestamps();

            $table->foreign('address_job_centers_id')->references('id_inc')->on('tree_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('profile_job_centers_id')->references('id_inc')->on('tree_job_centers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_job_centers');
    }
}
