<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTracingsTable
 * Migration for create table tracings.
 * @author Olga Rodríguez
 * @version 07/03/2019
 * @copyright Swopyn 2019
 */
class CreateTracingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_quotation')->unsigned();
            $table->date('tracing_date');
            $table->time('tracing_hour');
            $table->string('commentary', 500)->nullable();
            $table->timestamps();

            $table->foreign('id_quotation')->references('id')->on('quotations')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracings');
    }
}
