<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashCutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_cuts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 250);
            $table->integer('user_id')->unsigned();
            $table->double('total_efec', 8,2);
            $table->double('total_transf', 8,2);
            $table->double('total_cred', 8,2);
            $table->double('total_deb', 8,2);
            $table->double('total_cheq', 8,2);
            $table->double('total_depos', 8,2);
            $table->double('total', 8,2);
            $table->double('total_arqueo', 8,2);
            $table->double('desfase', 8,2);
            $table->integer('arqueo');
            $table->integer('depositado');
            $table->integer('sucursal_id')->unsigned();
            $table->date('date');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sucursal_id')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_cuts');
    }
}
