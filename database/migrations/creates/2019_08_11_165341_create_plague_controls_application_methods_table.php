<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePlagueControlsApplicationMethodsTable
 * Migration for create table plague_controls_application_methods.
 * @author Alberto Martínez
 * @version 11/08/2019
 * @copyright Swopyn 2019
 */
class CreatePlagueControlsApplicationMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plague_ctrl_application_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plague_control_id')->unsigned();
            $table->integer('id_application_method')->unsigned();
            $table->timestamps();

            $table->foreign('plague_control_id')->references('id')->on('plague_controls')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_application_method')->references('id')->on('application_methods')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plague_ctrl_application_methods');
    }
}
