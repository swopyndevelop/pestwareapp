<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class AddStatusToEvents
 * Migration for modify table events.
 * @author Olga Rodríguez
 * @version 06/06/2019
 * @copyright Swopyn 2019
 */
class AddStatusToEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->integer('id_status')->nullable()->after('id_job_center');

            $table->string('start_event')->nullable()->after('final_date')->default('00:00');
            $table->string('final_event')->nullable()->after('start_event')->default('00:00');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            //
        });
    }
}
