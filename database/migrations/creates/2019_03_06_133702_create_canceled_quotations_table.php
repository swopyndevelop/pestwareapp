<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCanceledQuotationsTable
 * Migration for create table canceled_quotations.
 * @author Olga Rodríguez
 * @version 06/03/2019
 * @copyright Swopyn 2019
 */
class CreateCanceledQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canceled_quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_quotation')->unsigned();
            $table->string('reason', 250);
            $table->string('commentary', 250);
            $table->dateTime('date');
            $table->timestamps();

            $table->foreign('id_quotation')->references('id')->on('quotations')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('canceled_quotations');
    }
}
