<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateQuotationCustomQuotesTable
 * Migration for create table quotation_custom_quote.
 * @author Olga Rodríguez
 * @version 16/01/2019
 * @copyright Swopyn 2019
 */
class CreateQuotationCustomQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_custom_quote', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotation_id')->unsigned();
            $table->integer('custom_quote_id')->unsigned();
            $table->timestamps();

            $table->foreign('quotation_id')->references('id')->on('quotations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('custom_quote_id')->references('id')->on('custom_quotes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_custom_quotes');
    }
}
