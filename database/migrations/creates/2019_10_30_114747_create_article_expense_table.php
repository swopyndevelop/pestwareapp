<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_expense')->unsigned();
            $table->string('concept');
            $table->double('total');
            $table->timestamps();   
            
            $table->foreign('id_expense')->references('id')->on('expenses')->onUpdate('cascade')->onDelete('cascade');

        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_expenses');
    }
}
