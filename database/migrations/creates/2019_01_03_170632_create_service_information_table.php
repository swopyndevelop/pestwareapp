<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateServiceInformationTable
 * Migration for create table service_information.
 * @author Alberto Martínez
 * @version 03/01/2019
 * @copyright Swopyn 2019
 */
class CreateServiceInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plague_id')->unsigned();
            $table->integer('establishment_id')->unsigned();
            $table->string('description', 2000)->nullable();
            $table->string('indications', 4000)->nullable();
            $table->timestamps();

            $table->foreign('plague_id')->references('id')->on('plague_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('establishment_id')->references('id')->on('establishment_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_information');
    }
}
