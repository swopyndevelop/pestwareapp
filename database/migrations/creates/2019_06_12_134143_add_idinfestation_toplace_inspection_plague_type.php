<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdinfestationToplaceInspectionPlagueType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('place_inspection_plague_type', function (Blueprint $table) {
            $table->integer('id_infestation_degree')->unsigned()->after('plague_type_id');

            $table->foreign('id_infestation_degree')->references('id')->on('infestation_degrees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('place_inspection_plague_type', function (Blueprint $table) {
            //
        });
    }
}
