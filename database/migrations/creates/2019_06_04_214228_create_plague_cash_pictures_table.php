<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreatePlagueCashPicturesTable
 * Migration for create table cash_pictures.
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @copyright Swopyn 2019
 */
class CreatePlagueCashPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_id')->unsigned();
            $table->string('file_route');
            $table->timestamps();

            $table->foreign('cash_id')->references('id')->on('cashes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plague_cash_pictures');
    }
}
