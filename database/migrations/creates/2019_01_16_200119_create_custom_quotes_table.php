<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCustomQuotesTable
 * Migration for create table custom_quotes.
 * @author Olga Rodríguez
 * @version 16/01/2019
 * @copyright Swopyn 2019
 */
class CreateCustomQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_quotation')->unsigned();
            $table->string('concept');
            $table->integer('quantity');
            $table->integer('frecuency_month');
            $table->integer('term_month');
            $table->double('unit_price', 10, 2);
            $table->double('subtotal', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_quotes');
    }
}
