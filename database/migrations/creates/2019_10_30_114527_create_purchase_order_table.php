<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_purchase_order');
            $table->integer('id_provider')->unsigned();
            $table->integer('id_concept')->unsigned();
            $table->date('date_order');
            $table->integer('id_user')->unsigned();
            $table->string('description_order');
            $table->double('total');
            $table->integer('status');
            $table->integer('id_payment_way')->unsigned();
            $table->integer('id_payment_method')->unsigned();
            $table->integer('account_status');
            $table->integer('id_voucher')->unsigned();
            $table->timestamps();    
            
            $table->foreign('id_provider')->references('id')->on('providers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_concept')->references('id')->on('concepts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_payment_way')->references('id')->on('payment_ways')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_payment_method')->references('id')->on('payment_methods')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_voucher')->references('id')->on('vouchers')->onUpdate('cascade')->onDelete('cascade');
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
