<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_purchase_order')->unsigned();
            $table->string('concept');
            $table->integer('quantity');
            $table->double('unit_price');
            $table->double('subtotal');
            $table->timestamps();   
            
            $table->foreign('id_purchase_order')->references('id')->on('purchase_orders')->onUpdate('cascade')->onDelete('cascade');

        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_orders');
    }
}
