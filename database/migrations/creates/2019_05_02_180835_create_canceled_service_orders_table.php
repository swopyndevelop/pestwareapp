<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCanceledServiceOrdersTable
 * Migration for create table canceled_service_orders.
 * @author Olga Rodríguez
 * @version 02/05/2019
 * @copyright Swopyn 2019
 */
class CreateCanceledServiceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canceled_service_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_service_order')->unsigned();
            $table->string('reason', 250);
            $table->string('commentary', 250);
            $table->dateTime('date');
            $table->timestamps();

            $table->foreign('id_service_order')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('canceled_service_orders');
    }
}
