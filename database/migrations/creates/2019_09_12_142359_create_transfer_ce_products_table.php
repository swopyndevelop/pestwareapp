<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferCeProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_ce_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_transfer_ce')->unsigned();
            $table->integer('id_product')->unsigned();
            $table->string('units');
            $table->timestamps();
        

            $table->foreign('id_transfer_ce')->references('id')->on('transfer_center_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_product')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_ce_products');
    }
}
