<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCompaniesTable
 * Migration for create table companies.
 * @author Olga Rodríguez
 * @version 13/03/2019
 * @copyright Swopyn 2019
 */
class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_company')->nullable();
            $table->string('name');
            $table->string('rfc', 13);
            $table->string('address');
            $table->binary('logo')->nullable();
            $table->binary('image')->nullable();
            $table->string('phone', 12);
            $table->integer('company_contract_id')->unsigned();
            $table->integer('contact_id')->unsigned();
            $table->string('bussines_name');
            $table->string('tax_regime', 8);
            $table->string('bank_account');
            $table->integer('pay_type_id')->unsigned();
            $table->integer('company_type');
            $table->integer('specialty');
            $table->integer('no_employees');
            $table->integer('no_branch_office');

            $table->timestamps();

            $table->foreign('company_contract_id')->references('id')->on('company_contracts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('contact_id')->references('id')->on('contacts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pay_type_id')->references('id')->on('pay_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
