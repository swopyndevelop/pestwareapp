<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEventsTable
 * Migration for create table events.
 * @author Olga Rodríguez
 * @version 18/04/2019
 * @copyright Swopyn 2019
 */
class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('id_employee')->unsigned();
            $table->integer('id_service_order')->unsigned();
            $table->time('initial_hour');
            $table->time('final_hour');
            $table->date('initial_date');
            $table->date('final_date');
            $table->integer('id_job_center')->unsigned();
            $table->timestamps();

            $table->foreign('id_employee')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('id_service_order')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('id_job_center')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
