<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreateCashesTable
 * Migration for create table cashes.
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @copyright Swopyn 2019
 */
class CreateCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_service_order')->unsigned();
            $table->integer('id_payment_method')->unsigned();
            $table->integer('id_payment_way')->unsigned();
            $table->double('amount_received', 8,2);
            $table->string('commentary', 250);
            $table->integer('payment');
            $table->timestamps();

            $table->foreign('id_service_order')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_payment_method')->references('id')->on('payment_methods')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_payment_way')->references('id')->on('payment_ways')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashes');
    }
}
