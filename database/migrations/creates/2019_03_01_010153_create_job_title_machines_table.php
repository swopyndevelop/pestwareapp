<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateJobTitleMachinesTable
 * Migration for create table job_title_machines.
 * @author Olga Rodríguez
 * @version 28/02/2019
 * @copyright Swopyn 2019
 */
class CreateJobTitleMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_title_machines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_title_machines_id')->unsigned();
            $table->integer('job_title_profiles_id')->unsigned();
            $table->string('machine_name', 191)->nullable();
            $table->timestamps();

            $table->foreign('job_title_machines_id')->references('id_inc')->on('tree_job_profiles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('job_title_profiles_id')->references('id_inc')->on('tree_job_profiles')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_title_machines');
    }
}
