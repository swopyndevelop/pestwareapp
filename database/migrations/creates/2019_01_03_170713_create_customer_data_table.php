<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCustomerDataTable
 * Migration for create table customer_data.
 * @author Alberto Martínez
 * @version 03/01/2019
 * @copyright Swopyn 2019
 */
class CreateCustomerDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->string('address', 250)->nullable();
            $table->string('reference_address', 250)->nullable();
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->integer('pay_type_id')->unsigned()->nullable();
            $table->string('billing')->nullable();
            $table->string('rfc')->nullable();
            $table->string('tax_residence')->nullable();
            $table->string('expense_type')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pay_type_id')->references('id')->on('pay_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_data');
    }
}
