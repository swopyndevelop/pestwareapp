<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreatePlaceConditionsTable
 * Migration for create table place_conditions.
 * @author Olga Rodríguez
 * @version 04/06/2019
 * @copyright Swopyn 2019
 */
class CreatePlaceConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place_conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_service_order')->unsigned();
            $table->integer('indications');
            $table->string('restricted_access', 250);
            $table->string('commentary', 250);
            $table->timestamps();

            $table->foreign('id_service_order')->references('id')->on('service_orders')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place_conditions');
    }
}
