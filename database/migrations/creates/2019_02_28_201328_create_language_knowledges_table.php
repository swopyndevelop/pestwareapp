<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateLanguageKnowledgesTable
 * Migration for create table language_knowledges.
 * @author Olga Rodríguez
 * @version 28/02/2019
 * @copyright Swopyn 2019
 */
class CreateLanguageKnowledgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_knowledge', function (Blueprint $table) {
            $table->increments('id');
            $table->string('language', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_knowledges');
    }
}
