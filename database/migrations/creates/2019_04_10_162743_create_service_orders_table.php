<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateServiceOrdersTable
 * Migration for create table service_orders.
 * @author Olga Rodríguez
 * @version 10/04/2019
 * @copyright Swopyn 2019
 */
class CreateServiceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_service_order');
            $table->integer('id_quotation')->unsigned();
            $table->integer('id_payment_method')->unsigned();
            $table->integer('id_payment_way')->unsigned();
            $table->string('bussiness_name')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->string('observations')->nullable();
            $table->double('total', 8,2);
            $table->timestamps();

            $table->foreign('id_quotation')->references('id')->on('quotations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_payment_method')->references('id')->on('payment_methods')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_payment_way')->references('id')->on('payment_ways')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_orders');
    }
}
