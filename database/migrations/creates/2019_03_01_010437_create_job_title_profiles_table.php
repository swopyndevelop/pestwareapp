<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateJobTitleProfilesTable
 * Migration for create table job_title_profiles.
 * @author Olga Rodríguez
 * @version 28/02/2019
 * @copyright Swopyn 2019
 */
class CreateJobTitleProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_title_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_title_profiles_id')->unsigned();
            $table->string('job_title_name', 191)->nullable();
            $table->string('job_title_objetive', 191)->nullable();
            $table->integer('min_salary')->nullable();
            $table->integer('max_salary')->nullable();
            $table->integer('function_type')->unsigned();
            $table->integer('min_age')->nullable();
            $table->integer('max_age')->nullable();
            $table->string('gender', 191)->nullable();
            $table->string('civil_status', 191)->nullable();
            $table->string('scholarship', 191)->nullable();
            $table->string('college_career', 191)->nullable();
            $table->integer('interview_type')->unsigned();
            $table->string('contract', 191)->nullable();
            $table->timestamps();

            $table->foreign('job_title_profiles_id')->references('id_inc')->on('tree_job_profiles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('function_type')->references('id')->on('commands')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('interview_type')->references('id')->on('commands')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_title_profiles');
    }
}
