<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreateInspectionPicturesTable
 * Migration for create table inspection_pictures.
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @copyright Swopyn 2019
 */
class CreateInspectionPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('place_inspection_id')->unsigned();
            $table->string('file_route');
            $table->timestamps();

            $table->foreign('place_inspection_id')->references('id')->on('place_inspections')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspection_pictures');
    }
}
