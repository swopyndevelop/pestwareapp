<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Class CreatePlagueControlPicturesTable
 * Migration for create table plague_control_pictures.
 * @author Olga Rodríguez
 * @version 05/06/2019
 * @copyright Swopyn 2019
 */
class CreatePlagueControlPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plague_control_pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plague_control_id')->unsigned();
            $table->string('file_route');
            $table->timestamps();

            $table->foreign('plague_control_id')->references('id')->on('plague_controls')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plague_control_pictures');
    }
}
