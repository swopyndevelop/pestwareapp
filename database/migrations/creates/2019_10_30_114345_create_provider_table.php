<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company');
            $table->string('contact_name');
            $table->string('contact_address');
            $table->string('contact_cellphone');
            $table->string('contact_email');
            $table->string('bank');
            $table->string('account_holder');
            $table->double('account_number');
            $table->string('clabe');
            $table->string('rfc');
            $table->timestamps();     
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
