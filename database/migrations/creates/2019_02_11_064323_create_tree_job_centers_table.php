<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTreeJobCentersTable
 * Migration for create table tree_job_centers.
 * @author Olga Rodríguez
 * @version 11/02/2019
 * @copyright Swopyn 2019
 */
class CreateTreeJobCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tree_job_centers', function (Blueprint $table) {
            $table->increments('id_inc');
            $table->string('id');
            $table->integer('company_id');
            $table->string('parent');
            $table->string('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tree_job_centers');
    }
}
