<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateWorkCapacitiesTable
 * Migration for create table work_capacities.
 * @author Olga Rodríguez
 * @version 01/03/2019
 * @copyright Swopyn 2019
 */
class CreateWorkCapacitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_capacities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('work_capacities_id')->nullable();
            $table->integer('profile_job_centers_id')->unsigned();
            $table->integer('jobtitle_capacity')->unsigned();
            $table->integer('quantity');
            $table->timestamps();

            $table->foreign('profile_job_centers_id')->references('id_inc')->on('tree_job_centers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('jobtitle_capacity')->references('id_inc')->on('tree_job_profiles')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_capacities');
    }
}
