<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchiveExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_expense')->unsigned();
            $table->string('file_route');
            $table->timestamps();   
            
            $table->foreign('id_expense')->references('id')->on('expenses')->onUpdate('cascade')->onDelete('cascade');

        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_expense');
    }
}
