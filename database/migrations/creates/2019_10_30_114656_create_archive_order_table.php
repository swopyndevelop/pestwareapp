<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchiveOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_purchase_order')->unsigned();
            $table->string('file_route');
            $table->timestamps();   
            
            $table->foreign('id_purchase_order')->references('id')->on('purchase_orders')->onUpdate('cascade')->onDelete('cascade');

        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_orders');
    }
}
