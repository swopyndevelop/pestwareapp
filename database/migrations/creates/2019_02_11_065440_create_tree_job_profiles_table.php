<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTreeJobProfilesTable
 * Migration for create table tree_job_profiles.
 * @author Olga Rodríguez
 * @version 11/02/2019
 * @copyright Swopyn 2019
 */
class CreateTreeJobProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tree_job_profiles', function (Blueprint $table) {
            $table->increments('id_inc');
            $table->string('id');
            $table->integer('company_id');
            $table->string('parent');
            $table->string('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tree_job_profiles');
    }
}
