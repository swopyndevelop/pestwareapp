<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 250);
            $table->string('key', 150);
            $table->integer('hierarchy')->unsigned();
            $table->string('description', 4000);
            $table->integer('establishment_id')->unsigned();
            $table->integer('indications_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('status')->unsigned();
            $table->timestamps();

            $table->foreign('establishment_id')->references('id')->on('establishment_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('indications_id')->references('id')->on('establishment_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_lists');
    }
}
