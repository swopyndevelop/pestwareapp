<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePlagueControlsProductsTable
 * Migration for create table plague_controls_products.
 * @author Alberto Martínez
 * @version 11/08/2019
 * @copyright Swopyn 2019
 */
class CreatePlagueControlsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plague_controls_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plague_control_id')->unsigned();
            $table->integer('id_product')->unsigned();
            $table->string('dose');
            $table->double('quantity', 8,2);
            $table->timestamps();

            $table->foreign('plague_control_id')->references('id')->on('plague_controls')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_product')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plague_controls_products');
    }
}
