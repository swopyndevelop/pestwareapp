<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaInspectionsPlaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_inspections_plagues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_area_inspection')->unsigned();
            $table->integer('id_plague')->unsigned();
            $table->integer('id_infestation_degree')->unsigned();
            $table->foreign('id_area_inspection')->references('id')->on('area_inspections')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_infestation_degree')->references('id')->on('infestation_degrees')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_inspections_plagues');
    }
}
