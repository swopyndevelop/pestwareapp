<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_employee')->unsigned();
            $table->foreign('id_employee')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');
            $table->date('date');
            $table->time('hour');
            $table->string('strTEID');
            $table->string('nTime');
            $table->string('dbLon');
            $table->string('dbLat');
            $table->string('nDirection');
            $table->string('nSpeed');
            $table->string('nGSMSignal');
            $table->string('nGPSSignal');
            $table->string('nFuel');
            $table->string('nMileage');
            $table->string('nTemp');
            $table->string('nCarState');
            $table->string('nTEState');
            $table->string('nAlarmState');
            $table->string('strOther');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking');
    }
}
