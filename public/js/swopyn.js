$("document").ready(function() {
  $("#GradoAcademico").change(function() {
    $("#Carreras").attr("disabled", "true");
    $.ajax({
      type: "POST",
      url: "../search_education",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Edu_level: $(this).val()
      },
      success: function(data) {
        if (data.errors) {
        } else {
          $("#Carreras").empty();
          for (var i = 0; i < data.length; i++) {
            $("#Carreras").append(
              "<option value='" + data[i].id + "'>" + data[i].name + "</option>"
              );
          }
          $("#Carreras").removeAttr("disabled");
        }
      }
    });
  });
  $("#GradoAcademicoProfile").change(function() {
    $("#CarreraProfile").attr("disabled", "true");
    $.ajax({
      type: "POST",
      url: "../../../search_education_profile",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Edu_level: $(this).val()
      },
      success: function(data) {
        if (data.errors) {
        } else {
          $("#CarreraProfile").empty();
          for (var i = 0; i < data.length; i++) {
            $("#CarreraProfile").append(
              "<option value='" + data[i].id + "'>" + data[i].name + "</option>"
              );
          }
          $("#CarreraProfile").removeAttr("disabled");
        }
      }
    });
  });
  $(".js-example-basic-single").select2();
  $(".tabs_application").removeAttr("data-toggle");
  $(".tabs_application")
  .eq(0)
  .attr("data-toggle", "tab");
  $("#fam_visible").click(function() {
    if ($("#fam_visible").val() == 1) {
      $("#fam_visible").val(0);
      $("#fam_visible").text("Ocultar Tabla de Familiares");
      $("#div_table_fam").removeClass("hidden");
    } else {
      $("#fam_visible").val(1);
      $("#fam_visible").text("Mostrar Tabla de Familiares");
      $("#div_table_fam").addClass("hidden");
    }
  });
  $("#schools_visible").click(function() {
    if ($("#schools_visible").val() == 1) {
      $("#schools_visible").val(0);
      $("#schools_visible").text("Ocultar Tabla de Estudios");
      $("#div_table_schools").removeClass("hidden");
    } else {
      $("#schools_visible").val(1);
      $("#schools_visible").text("Mostrar Tabla de Estudios");
      $("#div_table_schools").addClass("hidden");
    }
  });
  tabs_application_active();
  function tabs_application_active() {
    var cont = $("#job_tabs").val();
    var pass = $("#pass_tabs").val();
    for (var i = 0; i <= pass; i++) {
      var obj = $(".enlace_Pasos")[i];
      $(obj).attr("data-toggle", "tab");
      $(obj)
      .parent()
      .removeClass("disabled");
      if (i == pass) {
        $(obj).click();
      }
    }
    for (var i = 0; i <= cont; i++) {
      var obj = $(".tabs_application")[i];
      $(obj).attr("data-toggle", "tab");
      $(obj)
      .parent()
      .removeClass("disabled");
      if (i == cont) {
        $(obj).click();
      }
    }
  }
  // next-tab
  function tab_siguiente(clase, tab_activa) {
    var pas;
    $(clase).each(function(index) {
      if (tab_activa == $(this).attr("href")) {
        pas = index + 1;
        return true;
      }
    });
    var nextstep = $(clase).eq(pas);
    $(nextstep).attr("data-toggle", "tab");
    $(nextstep)
    .parent()
    .removeClass("disabled");
    $(nextstep).click();
  }
  $("#btnSavePsychometric").click(function() {
    $.ajax({
      type: "POST",
      url: "../../psychometric/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        user: $("#user").val(),
        question: $("#question").val(),
        answer1: $("#answer1").val(),
        answer2: $("#answer2").val()
      },
      success: function(data) {
        if (data.errors) {
          //alert('errors');
        } else {
          var ans = parseInt($("#question").val()) + 1;
          location.href = "../../psychometric/answer/" + ans;
        }
      }
    });
  });
  // //
  //COUNTDOWN REGRESIVE
  var finishTime;
  var timerLength = 1800;
  var timeoutID;
  if (localStorage.getItem("myTime")) {
    Update();
  }
  $("#btnStartTest").click(function() {
    //var dis = document.getElementById("displayReloj");
    // var fecha=new Date(timerLength);
    // var m=fecha.getMinutes();
    // var s=fecha.getSeconds();
    //     m = actualizarHora(m);
    // 	s = actualizarHora(s);
    // $('#displayReloj').html( m + ":" + s);
    //dis.innerHTML = "Time Left: " + timerLength;
    localStorage.setItem("myTime", new Date().getTime() + timerLength * 1000);
    if (timeoutID != undefined) {
      clearTimeout(timeoutID);
    }
    Update();
  });
  function Update() {
    finishTime = localStorage.getItem("myTime");
    var timeLeft = finishTime - new Date();
    fecha = new Date(timeLeft);
    m = fecha.getMinutes();
    s = fecha.getSeconds();
    m = actualizarHora(m);
    s = actualizarHora(s);
    $("#displayReloj").text(m + ":" + s);
    // $('#displayReloj').html( Math.max(timeLeft/1000,0));
    timeout = setTimeout(function() {
      Update();
      if (timeLeft <= 0) {
        clearTimeout(timeout);
        localStorage.removeItem("myTime");
        $.ajax({
          type: "POST",
          url: "../../psychometric/score",
          data: {
            _token: $("meta[name=csrf-token]").attr("content"),
            user: $("#user").val()
          },
          success: function(data) {
            if (data.errors) {
              //alert('errors');
            } else {
              location.href = "../../psychometric/thanks";
            }
          }
        });
      }
    }, 100);
  }
  function actualizarHora(i) {
    if (i < 10) {
      i = "0" + i;
    } // Añadir el cero en números menores de 10
    return i;
  }
  //END COUNTDOWN REGRESIVE
  //COUNTDOWN REGRESIVE
  var finishTime;
  var timerLength = 1800;
  var timeoutID;
  if (localStorage.getItem("myTime1")) {
    timeEvaluation();
  }
  $("#StartEvaluation").click(function() {
    localStorage.setItem("myTime1", new Date().getTime() + timerLength * 1000);
    if (timeoutID != undefined) {
      clearTimeout(timeoutID);
    }
    timeEvaluation();
  });
  $("#EndEvaluation").click(function() {
    window.location.href = "../training/study";
  });
  function timeEvaluation() {
    finishTime = localStorage.getItem("myTime1");
    var timeLeft = finishTime - new Date();
    fecha = new Date(timeLeft);
    m = fecha.getMinutes();
    s = fecha.getSeconds();
    m = actualizarHora(m);
    s = actualizarHora(s);
    $("#displayRelojEvaluation").text(m + ":" + s);
    // $('#displayReloj').html( Math.max(timeLeft/1000,0));
    timeout = setTimeout(function() {
      timeEvaluation();
      if (timeLeft <= 0) {
        clearTimeout(timeout);
        localStorage.removeItem("myTime1");
        location.href = "../courses/resultsUser";
      }
    }, 100);
  }
  function actualizarHora(i) {
    if (i < 10) {
      i = "0" + i;
    } // Añadir el cero en números menores de 10
    return i;
  }
  //END COUNTDOWN REGRESIVE
  //alerts
  function add() {
    swal("Datos agregados correctamente", "", "success");
  }
  function faltante() {
    swal({
      title: "¡Espera!",
      text: "Faltan datos por ingresar",
      imageUrl: "../img/icon-lte-09.png",
      imageWidth: 200,
      imageHeight: 200,
      timer: 2000,
      showCancelButton: false,
      showConfirmButton: false
    }).catch(swal.noop);
  }
  function faltante_jobcenter() {
    swal({
      title: "¡Espera!",
      text: "Faltan datos por ingresar",
      imageUrl: "../../../img/icon-lte-09.png",
      imageWidth: 200,
      imageHeight: 200,
      timer: 2000,
      showCancelButton: false,
      showConfirmButton: false
    }).catch(swal.noop);
  }
  function fDocuments() {
    swal({
      title: "¡Espera!",
      text: "Tienes que seleccionar por lo menos un documento",
      type: "warning",
      timer: 2000,
      showCancelButton: false,
      showConfirmButton: false
    }).catch(swal.noop);
  }
  function Cpuesto() {
    swal({
      title: "¡Espera!",
      text: "Es necesario que ingreses un puesto y la cantidad del mismo",
      type: "warning",
      timer: 2000,
      showCancelButton: false,
      showConfirmButton: false
    }).catch(swal.noop);
  }
  function showError(message) {
    swal({
      title: "¡Espera!",
      text: message,
      type: "warning",
      timer: 5000,
      showCancelButton: false,
      showConfirmButton: false
    }).catch(swal.noop);
  }
  function Redirect() {
    swal({
      title: "",
      html:
      "<h3>Felicidades haz concluido satisfactoriamente el proceso de Reclutamiento y Selección.</h3><p>Estamos muy contentos de que formes parte de este gran equipo, a continuación comenzará tu proceso de Capacitación</p>",
      type: "success",
      showCancelButton: false
    }).then(function() {
      location.href = "../home";
    });
  }
  //end alerts

  // Modal tracking.index
  $("#modal-default").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevel").empty();
          if (json.length <= 0) {
            $("#showResultLevel").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevel").append(Header);


            $("#showResultLevel").html(Header);
            $("#showResultLevel").append("</tbody></table>");
          }
        }
      }
    });
  });

  $("#modal-defaultAA").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevelAA").empty();
          if (json.length <= 0) {
            $("#showResultLevelAA").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevelAA").append(Header);


            $("#showResultLevelAA").html(Header);
            $("#showResultLevelAA").append("</tbody></table>");
          }
        }
      }
    });
  });

   $("#modal-defaultAAA").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevelAAA").empty();
          if (json.length <= 0) {
            $("#showResultLevelAAA").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevelAAA").append(Header);


            $("#showResultLevelAAA").html(Header);
            $("#showResultLevelAAA").append("</tbody></table>");
          }
        }
      }
    });
  });

  $("#modal-default2A").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevel2A").empty();
          if (json.length <= 0) {
            $("#showResultLevel2A").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevel2A").append(Header);


            $("#showResultLevel2A").html(Header);
            $("#showResultLevel2A").append("</tbody></table>");
          }
        }
      }
    });
  });

  $("#modal-default2AA").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevel2AA").empty();
          if (json.length <= 0) {
            $("#showResultLevel2AA").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevel2AA").append(Header);


            $("#showResultLevel2AA").html(Header);
            $("#showResultLevel2AA").append("</tbody></table>");
          }
        }
      }
    });
  });

   $("#modal-default2AAA").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevel2AAA").empty();
          if (json.length <= 0) {
            $("#showResultLevel2AAA").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevel2AAA").append(Header);


            $("#showResultLevel2AAA").html(Header);
            $("#showResultLevel2AAA").append("</tbody></table>");
          }
        }
      }
    });
  });
   $("#modal-default3A").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevel3A").empty();
          if (json.length <= 0) {
            $("#showResultLevel3A").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevel3A").append(Header);


            $("#showResultLevel3A").html(Header);
            $("#showResultLevel3A").append("</tbody></table>");
          }
        }
      }
    });
  });

  $("#modal-default3AA").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevel3AA").empty();
          if (json.length <= 0) {
            $("#showResultLevel3AA").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevel3AA").append(Header);


            $("#showResultLevel3AA").html(Header);
            $("#showResultLevel3AA").append("</tbody></table>");
          }
        }
      }
    });
  });

   $("#modal-default3AAA").on("shown.bs.modal", function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var level = button.data("whatever");
    $.ajax({
      type: "POST",
      url: "../tracking/level",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        result: level
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          $("#showResultLevel3AAA").empty();
          if (json.length <= 0) {
            $("#showResultLevel3AAA").html("<h3> Insuficiente Puntaje </h3>");
          } else {
            var Header = "<h6> Aciertos " + level + " de 48 </h6>";
            $("#showResultLevel3AAA").append(Header);


            $("#showResultLevel3AAA").html(Header);
            $("#showResultLevel3AAA").append("</tbody></table>");
          }
        }
      }
    });
  });
  // //END

  //Result Interview Modal
  $(".openResultInterview").click(function(){
    $('#Experiencia_score').text('');
    $('#Orden_score').text('');
    $('#Reglas_score').text('');
    $('#Conductas_score').text('');
    $('#Final_score').text('');
    $('#Evaluacion_Resultado').text('');
    var all = $(this);
    $.ajax({
      type: "POST",
      url: "../tracking/interview_result",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Applicant_id: all.attr('data-whatresult'),
      },
      success: function(data) {
        if (data.errors) {
          alert("error");
        } else {
          var json = JSON.parse(JSON.stringify(data));
          var tableHeader = '<table class="table"><thead><tr><th>Competencia</th><th class="text-center">Importancia</th><th class="text-center">Puntaje Máximo</th><th class="text-center">Puntaje Obtenido</th></tr></thead><tbody>';
          $('#modal-body').append(tableHeader);
          var tableBody='';
          var resultado = 0;
          var eva_res ='';
          for (var i=0;i<json.length;++i)
          {
            tableBody = tableBody + '<tr><td>' + json[i].name + '</td><td class="text-center">' + json[i].value + '</td><td class="text-center">'+ json[i].max + ' %</td><td class="text-center">' + json[i].grade.toFixed(2) + ' %</td></tr>' ;
            $('#showResultLevel').append(tableBody);
            resultado = resultado + json[i].grade;
          }
          if (resultado <= 25)
            eva_res = 'NO CONTRATABLE';
          else if (resultado <= 50)
            eva_res = 'POCO SATISFACTORIO';
          else if (resultado <= 75)
            eva_res = 'MEDIANAMENTE SATISFACTORIO';
          else eva_res = 'SATISFACTORIO';
          tableBody = tableBody + '<tr><td colspan="1"></td><td class="text-center">Resultado</td><td class="text-center">100 %</td><td class="text-center">' + resultado.toFixed(2) + ' %</td></tr><tr></tr><tr><td colspan="1"></td><td colspan="1" class="text-center">Evaluación del resultado</td><td class="text-center">' + eva_res + '</td></tr>';
          $('#modal-body').html(tableHeader+tableBody);
          $('#modal-body').append('</tbody></table>');
        }
      }
    });
  });
//END Result Interview Modal.

  // Modal tracking.index interview
  $("#finalizaContrato").click(function() {
    Redirect();
  });
  //
  //jobapplication.create
  // $('.nav-tabs > li a[title]').tooltip();
  // $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
  // 	var $target = $(e.target);
  // 	if ($target.parent().hasClass('disabled')) {
  // 		return false;
  // 	}
  // });
  // function nextTab(elem) {
  // 	$(elem).next().find('a[data-toggle="tab"]').click();
  // }
  // function prevTab(elem) {
  // 	$(elem).prev().find('a[data-toggle="tab"]').click();
  // }
  function radiovalidacion(caso) {
    var resultado = "";
    var radio = document.getElementsByName(caso);
    // Recorremos todos los valores del radio button para encontrar el
    // seleccionado
    for (var i = 0; i < radio.length; i++) {
      if (radio[i].checked) resultado = radio[i].value;
    }
    return resultado;
  }
  //
  //jobapplication.create
  $("#guarda_personaldata").click(function() {
    var hoy = new Date();
    var cumpleanos = new Date($("input[name=Nacimiento]").val());
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();
    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
      edad--;
    }
    $.ajax({
      type: "POST",
      url: "../personaldata/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        user: $("#user").val(),
        Apellidos: $("input[name=Apellidos]").val(),
        PrimerNombre: $("input[name=PrimerNombre]").val(),
        Genero: radiovalidacion("Genero"),
        Peso: $("input[name=Peso]").val(),
        Estatura: $("input[name=Estatura]").val(),
        Telefono_job: $("input[name=Telefono_job]").val(),
        Celular: $("input[name=Celular]").val(),
        Nacimiento: $("input[name=Nacimiento]").val(),
        LugarNacimiento: $("input[name=LugarNacimiento]").val(),
        Curp: $("input[name=Curp]").val(),
        Rfc: $("input[name=Rfc]").val(),
        Nss: $("input[name=Nss]").val(),
        Dependientes: $("select[name=Dependientes]").val(),
        ViveCon: $("select[name=ViveCon]").val(),
        EstadoCivil: $("select[id=EstadoCivil").val(),
        Edad: edad
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Apellidos) {
            $("#error_ultimonombre").removeClass("hidden");
            $("#error_ultimonombre").text(data.errors.Apellidos);
          } else {
            $("#error_ultimonombre").remove();
          }
          if (data.errors.PrimerNombre) {
            $("#error_primernombre").removeClass("hidden");
            $("#error_primernombre").text(data.errors.PrimerNombre);
          } else {
            $("#error_primernombre").remove();
          }
          if (data.errors.Genero) {
            $("#error_genero").removeClass("hidden");
            $("#error_genero").text(data.errors.Genero);
          } else {
            $("#error_genero").remove();
          }
          if (data.errors.Nacimiento) {
            $("#error_fechanacimiento").removeClass("hidden");
            $("#error_fechanacimiento").text(data.errors.Nacimiento);
          } else {
            $("#error_fechanacimiento").remove();
          }
          if (data.errors.LugarNacimiento) {
            $("#error_lugarnacimiento").removeClass("hidden");
            $("#error_lugarnacimiento").text(data.errors.LugarNacimiento);
          } else {
            $("#error_lugarnacimiento").remove();
          }
          if (data.errors.Peso) {
            $("#error_peso").removeClass("hidden");
            $("#error_peso").text(data.errors.Peso);
          } else {
            $("#error_peso").remove();
          }
          if (data.errors.Estatura) {
            $("#error_estatura").removeClass("hidden");
            $("#error_estatura").text(data.errors.Estatura);
          } else {
            $("#error_estatura").remove();
          }
          if (data.errors.Telefono_job) {
            $("#error_telefonojob").removeClass("hidden");
            $("#error_telefonojob").text(data.errors.Telefono_job);
          } else {
            $("#error_telefonojob").remove();
          }
          if (data.errors.Celular) {
            $("#error_celular").removeClass("hidden");
            $("#error_celular").text(data.errors.Celular);
          } else {
            $("#error_celular").remove();
          }
          if (data.errors.EstadoCivil) {
            $("#error_estadocivil").removeClass("hidden");
            $("#error_estadocivil").text(data.errors.EstadoCivil);
          } else {
            $("#error_estadocivil").remove();
          }
          if (data.errors.ViveCon) {
            $("#error_vivecon").removeClass("hidden");
            $("#error_vivecon").text(data.errors.ViveCon);
          } else {
            $("#error_vivecon").remove();
          }
          if (data.errors.Dependientes) {
            $("#error_dependientes").removeClass("hidden");
            $("#error_dependientes").text(data.errors.Dependientes);
          } else {
            $("#error_dependientes").remove();
          }
          if (data.errors.Curp) {
            $("#error_curp").removeClass("hidden");
            $("#error_curp").text(data.errors.Curp);
          } else {
            $("#error_curp").remove();
          }
          if (data.errors.Rfc) {
            $("#error_rfc").removeClass("hidden");
            $("#error_rfc").text(data.errors.Rfc);
          } else {
            $("#error_rfc").remove();
          }
          if (data.errors.Nss) {
            $("#error_nss").removeClass("hidden");
            $("#error_nss").text(data.errors.Nss);
          } else {
            $("#error_nss").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          tab_siguiente(".tabs_application", "#general_personal_datas");
        }
      }
    });
});
$("#guarda_personaladdress").click(function() {
  $.ajax({
    type: "POST",
    url: "../personaladdresses/add",
    data: {
      _token: $("meta[name=csrf-token]").attr("content"),
      user: $("#user").val(),
      Calle: $("input[name=Calle]").val(),
      NumeroExterior: $("input[name=NumeroExterior]").val(),
      NumeroInterior: $("input[name=NumeroInterior]").val(),
      Fraccionamiento: $("input[name=Fraccionamiento]").val(),
      CodigoPostal: $("input[name=CodigoPostal]").val(),
      Ciudad: $("input[name=Ciudad]").val(),
      Estado_personal: $("select[name=Estado_personal]").val()
    },
    success: function(data) {
      if (data.errors) {
        faltante();
        if (data.errors.Calle) {
          $("#error_calle").removeClass("hidden");
          $("#error_calle").text(data.errors.Calle);
        } else {
          $("#error_calle").remove();
        }
        if (data.errors.NumeroExterior) {
          $("#error_numeroExterior").removeClass("hidden");
          $("#error_numeroExterior").text(data.errors.NumeroExterior);
        } else {
          $("#error_numeroExterior").remove();
        }
        if (data.errors.NumeroInterior) {
          $("#error_numeroInterior").removeClass("hidden");
          $("#error_numeroInterior").text(data.errors.NumeroInterior);
        } else {
          $("#error_numeroInterior").remove();
        }
        if (data.errors.Fraccionamiento) {
          $("#error_fraccionamiento").removeClass("hidden");
          $("#error_fraccionamiento").text(data.errors.Fraccionamiento);
        } else {
          $("#error_fraccionamiento").remove();
        }
        if (data.errors.CodigoPostal) {
          $("#error_codigoPostal").removeClass("hidden");
          $("#error_codigoPostal").text(data.errors.CodigoPostal);
        } else {
          $("#error_codigoPostal").remove();
        }
        if (data.errors.Ciudad) {
          $("#error_ciudad").removeClass("hidden");
          $("#error_ciudad").text(data.errors.Ciudad);
        } else {
          $("#error_ciudad").remove();
        }
        if (data.errors.Estado_personal) {
          $("#error_estado").removeClass("hidden");
          $("#error_estado").text(data.errors.Estado_personal);
        } else {
          $("#error_estado").remove();
        }
      } else {
        showToast('success','Guardado', 'Guardando correctamente');
        $(".error").addClass("hidden");
        tab_siguiente(".tabs_application", "#personal_addresses");
      }
    }
  });
});
$("#guarda_familydata").click(function() {
  $.ajax({
    type: "POST",
    url: "../family_data/add",
    data: {
      _token: $("meta[name=csrf-token]").attr("content"),
      user: $("#user").val(),
      NombreFamiliar: $("input[name=NombreFamiliar]").val(),
      Ocupacion: $("input[name=Ocupacion]").val(),
      Parentesco: $("input[name=Parentesco").val(),
      ViveConUsted: radiovalidacion("ViveConUsted"),
      NumeroFamiliar: $("input[name=NumeroFamiliar]").val()
    },
    success: function(data) {
      if (data.errors) {
        faltante();
        if (data.errors.NombreFamiliar) {
          $("#error_nombrefamilia").removeClass("hidden");
          $("#error_nombrefamilia").text(data.errors.NombreFamiliar);
        } else {
          $("#error_nombrefamilia").remove();
        }
        if (data.errors.Parentesco) {
          $("#error_parentesco").removeClass("hidden");
          $("#error_parentesco").text(data.errors.Parentesco);
        } else {
          $("#error_parentesco").remove();
        }
        if (data.errors.Ocupacion) {
          $("#error_ocupacion").removeClass("hidden");
          $("#error_ocupacion").text(data.errors.Ocupacion);
        } else {
          $("#error_ocupacion").remove();
        }
        if (data.errors.ViveConUsted) {
          $("#error_viveconfamilia").removeClass("hidden");
          $("#error_viveconfamilia").text(data.errors.ViveConUsted);
        } else {
          $("#error_viveconfamilia").remove();
        }
        if (data.errors.NumeroFamiliar) {
          $("#error_numerofamilia").removeClass("hidden");
          $("#error_numerofamilia").text(data.errors.NumeroFamiliar);
        } else {
          $("#error_numerofamilia").remove();
        }
      } else {
        showToast('success','Guardado', 'Guardando correctamente');
        $("#div_table_fam").removeClass("hidden");
        var fila =
        '<tr id="Familiar' +
        data.id +
        '"><td>' +
        data.name +
        "</td><td>" +
        data.relationship +
        "</td><td>" +
        data.occupation +
        "</td><td>" +
        data.phone_number +
        "</td></tr>";
        $("#family_table").append(fila);
        $(".error").addClass("hidden");
        $("#NombreFamiliar").focus();
        $("#NombreFamiliar").val("");
        $("#Ocupacion").val("");
        $("#Parentesco").val("");
        $("#NumeroFamiliar").val("");
        $("#ViveConUsted").prop("checked", false);
      }
    }
  });
});
$("#next-familyinformation").click(function() {
  tab_siguiente(".tabs_application", "#family_datas");
});
$("#guarda_scholarshipdata").click(function() {
  $.ajax({
    type: "POST",
    url: "../scholarship_datas/add",
    data: {
      _token: $("meta[name=csrf-token]").attr("content"),
      user: $("#user").val(),
      Carreras: $("select[name=Carreras]").val(),
      AreaAcademica: $("input[name=AreaAcademica]").val(),
      GradoAcademico: $("select[name=GradoAcademico]").val(),
      TerminoEstudios: $("select[name=TerminoEstudios]").val(),
      NombreEscuela: $("input[name=NombreEscuela]").val(),
      DireccionEscuela: $("input[name=DireccionEscuela").val(),
      EscuelaPeriodoInicial: $("input[name=EscuelaPeriodoInicial").val(),
      EscuelaPeriodoFinal: $("input[name=EscuelaPeriodoFinal").val(),
      CertificadoGrado: $("input[name=CertificadoGrado").val()
    },
    success: function(data) {
      if (data.errors) {
        faltante();
        if (data.errors.GradoAcademico) {
          $("#error_gradoacademico").removeClass("hidden");
          $("#error_gradoacademico").text(data.errors.GradoAcademico);
        } else {
          $("#error_gradoacademico").remove();
        }
        if (data.errors.AreaAcademica) {
          $("#error_areaacademica").removeClass("hidden");
          $("#error_areaacademica").text(data.errors.AreaAcademica);
        } else {
          $("#error_areaacademica").remove();
        }
        if (data.errors.Carreras) {
          $("#error_carreras").removeClass("hidden");
          $("#error_carreras").text(data.errors.Carreras);
        } else {
          $("#error_carreras").remove();
        }
        if (data.errors.TerminoEstudios) {
          $("#error_terminoestudios").removeClass("hidden");
          $("#error_terminoestudios").text(data.errors.TerminoEstudios);
        } else {
          $("#error_terminoestudios").remove();
        }
        if (data.errors.NombreEscuela) {
          $("#error_nombreescuela").removeClass("hidden");
          $("#error_nombreescuela").text(data.errors.NombreEscuela);
        } else {
          $("#error_nombreescuela").remove();
        }
        if (data.errors.DireccionEscuela) {
          $("#error_direccionescuela").removeClass("hidden");
          $("#error_direccionescuela").text(data.errors.DireccionEscuela);
        } else {
          $("#error_direccionescuela").remove();
        }
        if (data.errors.EscuelaPeriodoInicial) {
          $("#error_escperiodoini").removeClass("hidden");
          $("#error_escperiodoini").text(data.errors.EscuelaPeriodoInicial);
        } else {
          $("#error_escperiodoini").remove();
        }
        if (data.errors.EscuelaPeriodoFinal) {
          $("#error_escperiodofin").removeClass("hidden");
          $("#error_escperiodofin").text(data.errors.EscuelaPeriodoFinal);
        } else {
          $("#error_escperiodofin").remove();
        }
        if (data.errors.CertificadoGrado) {
          $("#error_certificadogrado").removeClass("hidden");
          $("#error_certificadogrado").text(data.errors.CertificadoGrado);
        } else {
          $("#error_certificadogrado").remove();
        }
      } else {
        showToast('success','Guardado', 'Guardando correctamente');
        $("#div_table_schools").removeClass("hidden");
        var fila =
        '<tr id="School_' +
        data.id +
        '"><td>' +
        data.education_area +
        "</td><td>" +
        data.school_name +
        "</td><td>" +
        data.final_period +
        "</td><td>" +
        data.degree_certificate +
        "</td><td>" +
        data.finish_studies +
        "</td></tr>";
        $("#schools_table").append(fila);
        $(".error").addClass("hidden");
        $("#NombreGrado").focus();
        $("#NombreGrado").val("");
        $("#AreaAcademica").val("");
        $("#NombreEscuela").val("");
        $("#DireccionEscuela").val("");
        $("#EscuelaPeriodoInicial").val("");
        $("#EscuelaPeriodoFinal").val("");
        $("#CertificadoGrado").val("");
        $("#GradoAcademico").val($("#GradoAcademico").data("default-value"));
        $("#TerminoEstudios").val(
          $("#TerminoEstudios").data("default-value")
          );
      }
    }
  });
});
$("#next-scholarship").click(function() {
  tab_siguiente(".tabs_application", "#scholarship_datas");
});
  //start function to Show content about actual studies & scholarship data
  function showContent() {
    element = $("#actualstudies");
    check = $("#checkYES");
    if (check.checked) {
      element.style.display = "block";
    } else {
      element.style.display = "none";
    }
  }
  //end
  $("#guarda_generalknowledge").click(function() {
    $.ajax({
      type: "POST",
      url: "../general_knowledge/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Lenguaje) {
            $("#error_lenguaje").removeClass("hidden");
            $("#error_lenguaje").text(data.errors.Lenguaje);
          } else $("#error_lenguaje").remove();
          if (data.errors.languageread) {
            $("#error_nivel").removeClass("hidden");
            $("#error_nivel").text(data.errors.languageread);
          } else $("#error_nivel").remove();
          if (data.errors.languagewrite) {
            $("#error_languagewrite").removeClass("hidden");
            $("#error_languagewrite").text(data.errors.languagewrite);
          } else $("#error_languagewrite").remove();
          if (data.errors.languagespeak) {
            $("#error_languagespeak").removeClass("hidden");
            $("#error_languagespeak").text(data.errors.languagespeak);
          } else $("#error_languagespeak").remove();
          if (data.errors.languagelisten) {
            $("#error_languagelisten").removeClass("hidden");
            $("#error_languagelisten").text(data.errors.languagelisten);
          } else $("#error_languagelisten").remove();
          if (data.errors.Oficina) {
            $("#error_oficina").removeClass("hidden");
            $("#error_oficina").text(data.errors.Oficina);
          } else {
            $("#error_oficina").remove();
          }
          if (data.errors.Maquinas) {
            $("#error_maquinas").removeClass("hidden");
            $("#error_maquinas").text(data.errors.Maquinas);
          } else {
            $("#error_maquinas").remove();
          }
          if (data.errors.Software) {
            $("#error_software").removeClass("hidden");
            $("#error_software").text(data.errors.Software);
          } else {
            $("#error_software").remove();
          }
          if (data.errors.OtrosTrabajos) {
            $("#error_otrostrabajo").removeClass("hidden");
            $("#error_otrostrabajo").text(data.errors.OtrosTrabajos);
          } else {
            $("#error_otrostrabajo").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          tab_siguiente(".tabs_application", "#general_knowledge");
        }
      }
    });
  });
  //Idiomas en Conocimientos
  $("#add_language").click(function() {
    $.ajax({
      type: "POST",
      url: "../language_application/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Lenguaje: $("#languageknow").val(),
        languageread: $("#languageread").val(),
        languagewrite: $("#languagewrite").val(),
        languagespeak: $("#languagespeak").val(),
        languagelisten: $("#languagelisten").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
        } else {
          $(".error").addClass("hidden");
        }
      }
    });
  });

  var cont = 0;
  function agregar_language(level) {
    cont++;
    var fila =
    '<tr id="languageknow' +
    cont +
    '"><td>' +
    $("select[name=languageknow] option:selected")
    .select2()
    .text() +
    "</td><td>" +
    level +
    "</td></tr>";
    $("#tablaLanguage").append(fila);
  }
  //end idiomas en conocimientos
  //Oficina en Conocimientos
  $("#add_office").click(function() {
    $.ajax({
      type: "POST",
      url: "../office_application/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Office: $("#Officeknow").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Office) {
            $("#error_oficina").removeClass("hidden");
            $("#error_oficina").text(data.errors.Office);
          } else {
            $("#error_oficina").addClass("hidden");
          }
        } else {
          $(".error").addClass("hidden");
          $("#Officeknow").val("");
          $("#Officeknow").focus();
          var fila = "<tr><td class=text-center>" + data.office + "</td></tr>";
          $("#tablaOficina").append(fila);
        }
      }
    });
  });
  //end oficina en conocimientos
  //Maquinas en Conocimientos
  $("#add_Machines").click(function() {
    $.ajax({
      type: "POST",
      url: "../machines_application/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Machines: $("#Machinesknow").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Machines) {
            $("#error_maquinas").removeClass("hidden");
            $("#error_maquinas").text(data.errors.Machines);
          } else {
            $("#error_maquinas").addClass("hidden");
          }
        } else {
          $(".error").addClass("hidden");
          $("#Machinesknow").val("");
          $("#Machinesknow").focus();
          var fila = "<tr><td class=text-center>" + data.machines + "</td></tr>";
          $("#tablaMaquinaria").append(fila);
        }
      }
    });
  });
  //end Maquinas en conocimientos
  //Software en Conocimientos
  $("#add_soft").click(function() {
    $.ajax({
      type: "POST",
      url: "../software_application/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Software: $("#Softknow").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Software) {
            $("#error_software").removeClass("hidden");
            $("#error_software").text(data.errors.Software);
          } else {
            $("#error_software").remove();
          }
        } else {
          $(".error").addClass("hidden");
          $("#Softknow").val("");
          $("#Softknow").focus();
          var fila = "<tr><td class=text-center>" + data.software + "</td></tr>";
          $("#tablaSoftware").append(fila);
        }
      }
    });
  });
  //end Software en conocimientos
  //Otros en Conocimientos
  $("#add_others").click(function() {
    $.ajax({
      type: "POST",
      url: "../others_application/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Others: $("#Othersknow").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Others) {
            $("#error_otrostrabajo").removeClass("hidden");
            $("#error_otrostrabajo").text(data.errors.Others);
          } else {
            $("#error_otrostrabajo").remove();
          }
        } else {
          $(".error").addClass("hidden");
          $("#Othersknow").val("");
          $("#Othersknow").focus();
          var fila = "<tr><td class=text-center>" + data.others + "</td></tr>";
          $("#tablaOtros").append(fila);
        }
      }
    });
  });
  //end Otros en conocimientos
  $("#guarda_workhistory").click(function() {
    $.ajax({
      type: "POST",
      url: "../work_history/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        user: $("#user").val(),
        PeriodoInicial: $("input[name=PeriodoInicial]").val(),
        PeriodoFinal: $("input[name=PeriodoFinal]").val(),
        NombreCompañia: $("input[name=NombreCompañia]").val(),
        DireccionCompañia: $("input[name=DireccionCompañia]").val(),
        TelefonoCompañia: $("input[name=TelefonoCompañia]").val(),
        Puesto: $("input[name=Puesto]").val(),
        IngresoInicial: $("input[name=IngresoInicial]").val(),
        IngresoFinal: $("input[name=IngresoFinal]").val(),
        MotivoSeparacion: $("input[name=MotivoSeparacion]").val(),
        Experiencia: $("input[name=Experiencia]").val(),
        AreaExperiencia: $("input[name=AreaExperiencia]").val(),
        NombreJefe: $("input[name=NombreJefe]").val(),
        PuestoJefe: $("input[name=PuestoJefe]").val(),
        CorreoJefe: $("input[name=CorreoJefe]").val(),
        TelefonoJefe: $("input[name=TelefonoJefe]").val(),
        Referencias: radiovalidacion("Referencias"),
        Razones: $("input[name=Razones]").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.PeriodoInicial) {
            $("#error_perinicial").removeClass("hidden");
            $("#error_perinicial").text(data.errors.PeriodoInicial);
          } else $("#error_perinicial").remove();
          if (data.errors.PeriodoFinal) {
            $("#error_perfinal").removeClass("hidden");
            $("#error_perfinal").text(data.errors.PeriodoFinal);
          } else $("#error_perfinal").remove();
          if (data.errors.NombreCompañia) {
            $("#error_nombrecompañia").removeClass("hidden");
            $("#error_nombrecompañia").text(data.errors.NombreCompañia);
          } else {
            $("#error_nombrecompañia").remove();
          }
          if (data.errors.DireccionCompañia) {
            $("#error_direccioncompañia").removeClass("hidden");
            $("#error_direccioncompañia").text(data.errors.DireccionCompañia);
          } else {
            $("#error_direccioncompañia").remove();
          }
          if (data.errors.TelefonoCompañia) {
            $("#error_telcompañia").removeClass("hidden");
            $("#error_telcompañia").text(data.errors.TelefonoCompañia);
          } else {
            $("#error_telcompañia").remove();
          }
          if (data.errors.Puesto) {
            $("#error_puesto").removeClass("hidden");
            $("#error_puesto").text(data.errors.Puesto);
          } else {
            $("#error_puesto").remove();
          }
          if (data.errors.IngresoInicial) {
            $("#error_ingresoinicial").removeClass("hidden");
            $("#error_ingresoinicial").text(data.errors.IngresoInicial);
          } else {
            $("#error_ingresoinicial").remove();
          }
          if (data.errors.IngresoFinal) {
            $("#error_ingresofinal").removeClass("hidden");
            $("#error_ingresofinal").text(data.errors.IngresoFinal);
          } else {
            $("#error_ingresofinal").remove();
          }
          if (data.errors.MotivoSeparacion) {
            $("#error_motivoseparacion").removeClass("hidden");
            $("#error_motivoseparacion").text(data.errors.MotivoSeparacion);
          } else {
            $("#error_motivoseparacion").remove();
          }
          if (data.errors.Experiencia) {
            $("#error_experiencia").removeClass("hidden");
            $("#error_experiencia").text(data.errors.Experiencia);
          } else {
            $("#error_experiencia").remove();
          }
          if (data.errors.AreaExperiencia) {
            $("#error_areaexperiencia").removeClass("hidden");
            $("#error_areaexperiencia").text(data.errors.AreaExperiencia);
          } else {
            $("#error_areaexperiencia").remove();
          }
          if (data.errors.NombreJefe) {
            $("#error_nombrejefe").removeClass("hidden");
            $("#error_nombrejefe").text(data.errors.NombreJefe);
          } else {
            $("#error_nombrejefe").remove();
          }
          if (data.errors.PuestoJefe) {
            $("#error_puestojefe").removeClass("hidden");
            $("#error_puestojefe").text(data.errors.PuestoJefe);
          } else {
            $("#error_puestojefe").remove();
          }
          if (data.errors.CorreoJefe) {
            $("#error_correojefe").removeClass("hidden");
            $("#error_correojefe").text(data.errors.CorreoJefe);
          } else {
            $("#error_correojefe").remove();
          }
          if (data.errors.TelefonoJefe) {
            $("#error_teljefe").removeClass("hidden");
            $("#error_teljefe").text(data.errors.TelefonoJefe);
          } else {
            $("#error_teljefe").remove();
          }
          if (data.errors.Referencias) {
            $("#error_referencia").removeClass("hidden");
            $("#error_referencia").text(data.errors.Referencias);
          } else {
            $("#error_referencia").remove();
          }
          if (data.errors.Razones) {
            $("#error_razones").removeClass("hidden");
            $("#error_razones").text(data.errors.Razones);
          } else {
            $("#error_razones").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $("#div_table_jobs").removeClass("hidden");
          var fila =
          '<tr id="Jobs_' +
          data.id +
          '"><td>' +
          data.company_name +
          "</td><td>" +
          data.job_title +
          "</td><td>" +
          data.company_phone +
          "</td><td>" +
          data.boss_name +
          "</td><td>" +
          data.time_job +
          "</td></tr>";
          $("#jobs_table").append(fila);
          $(".error").addClass("hidden");
          $("#PeriodoInicial").focus();
          $("#PeriodoInicial").val("");
          $("#PeriodoFinal").val("");
          $("#NombreCompañia").val("");
          $("#DireccionCompañia").val("");
          $("#TelefonoCompañia").val("");
          $("#Puesto").val("");
          $("#IngresoInicial").val("");
          $("#IngresoFinal").val("");
          $("#MotivoSeparacion").val("");
          $("#Experiencia").val("");
          $("#AreaExperiencia").val("");
          $("#NombreJefe").val("");
          $("#PuestoJefe").val("");
          $("#CorreoJefe").val("");
          $("#TelefonoJefe").val("");
          $("#Referencias").val($("#Referencias").data("default-value"));
        }
      }
    });
});
$("#next-jobs").click(function() {
  tab_siguiente(".tabs_application", "#work_history");
});
$("#guarda_personalreferences").click(function() {
  var tiempo_ref =
  $("#TiempoReferencia").val() + " " + $("#timeReferences").val();
  $.ajax({
    type: "POST",
    url: "../personal_references/add",
    data: {
      _token: $("meta[name=csrf-token]").attr("content"),
      user: $("#user").val(),
      NombreReferencia: $("input[name = NombreReferencia]").val(),
      DireccionReferencia: $("input[name = DireccionReferencia]").val(),
      RelacionReferencia: $("select[name = RelacionReferencia]").val(),
      OcupacionReferencia: $("input[name = OcupacionReferencia]").val(),
      NumeroReferencia: $("input[name = NumeroReferencia]").val(),
      TiempoReferencia: tiempo_ref
    },
    success: function(data) {
      if (data.errors) {
        faltante();
        if (data.errors.NombreReferencia) {
          $("#error_NombreReferencia").removeClass("hidden");
          $("#error_NombreReferencia").text(data.errors.NombreReferencia);
        } else {
          $("#error_NombreReferencia").remove();
        }
        if (data.errors.DireccionReferencia) {
          $("#error_DireccionReferencia").removeClass("hidden");
          $("#error_DireccionReferencia").text(
            data.errors.DireccionReferencia
            );
        } else {
          $("#error_DireccionReferencia").remove();
        }
        if (data.errors.RelacionReferencia) {
          $("#error_RelacionReferencia").removeClass("hidden");
          $("#error_RelacionReferencia").text(data.errors.RelacionReferencia);
        } else {
          $("#error_RelacionReferencia").remove();
        }
        if (data.errors.OcupacionReferencia) {
          $("#error_OcupacionReferencia").removeClass("hidden");
          $("#error_OcupacionReferencia").text(
            data.errors.OcupacionReferencia
            );
        } else {
          $("#error_OcupacionReferencia").remove();
        }
        if (data.errors.NumeroReferencia) {
          $("#error_NumeroReferencia").removeClass("hidden");
          $("#error_NumeroReferencia").text(data.errors.NumeroReferencia);
        } else {
          $("#error_NumeroReferencia").remove();
        }
        if (data.errors.TiempoReferencia) {
          $("#error_TiempoReferencia").removeClass("hidden");
          $("#error_TiempoReferencia").text(data.errors.TiempoReferencia);
        } else {
          $("#error_TiempoReferencia").remove();
        }
      } else {
        showToast('success','Guardado', 'Guardando correctamente');
        $("#div_table_reference").removeClass("hidden");
        var fila =
        '<tr id="Referencia_' +
        data.id +
        '"><td>' +
        data.name +
        "</td><td>" +
        data.address +
        "</td><td>" +
        data.relationship +
        "</td><td>" +
        data.occupation +
        "</td><td>" +
        data.phone_number +
        "</td><td>" +
        data.meet_time +
        "</td></tr>";
        $("#reference_table").append(fila);
        $(".error").addClass("hidden");
        $("#NombreReferencia").val("");
        $("#DireccionReferencia").val("");
        $("#NumeroReferencia").val("");
        $("#OcupacionReferencia").val("");
        $("#TiempoReferencia").val("");
        $("#timeReferences").val("");
        $("#RelacionReferencia").val(
          $("#RelacionReferencia").data("default-value")
          );
      }
    }
  });
});
$("#next-personalreferences").click(function() {
  tab_siguiente(".tabs_application", "#personal_references");
});
$("#guarda_economicdata").click(function() {
  $.ajax({
    type: "POST",
    url: "../economic_datas/add",
    data: {
      _token: $("meta[name=csrf-token]").attr("content"),
      user: $("#user").val(),
      IngresoExtra: radiovalidacion("IngresoExtra"),
      MontoIngreso: $("input[name=MontoIngreso]").val(),
      ParejaTrabaja: radiovalidacion("ParejaTrabaja"),
      LugarPareja: $("input[name=LugarPareja]").val(),
      CasaPropia: radiovalidacion("CasaPropia"),
      RentaCasa: radiovalidacion("RentaCasa"),
      CarroPropio: radiovalidacion("CarroPropio"),
      Deudas: radiovalidacion("Deudas"),
      MontoDeudas: $("input[name=MontoDeudas]").val(),
      GastoMensual: $("input[name=GastoMensual]").val()
    },
    success: function(data) {
      if (data.errors) {
        faltante();
        if (data.errors.IngresoExtra) {
          $("#error_ingresoExtra").removeClass("hidden");
          $("#error_ingresoExtra").text(data.errors.IngresoExtra);
        } else {
          $("#error_ingresoExtra").remove();
        }
        if (data.errors.MontoIngreso) {
          $("#error_MontoIngreso").removeClass("hidden");
          $("#error_MontoIngreso").text(data.errors.MontoIngreso);
        } else {
          $("#error_MontoIngreso").remove();
        }
        if (data.errors.ParejaTrabaja) {
          $("#error_ParejaTrabaja").removeClass("hidden");
          $("#error_ParejaTrabaja").text(data.errors.ParejaTrabaja);
        } else {
          $("#error_ParejaTrabaja").remove();
        }
        if (data.errors.LugarPareja) {
          $("#error_LugarPareja").removeClass("hidden");
          $("#error_LugarPareja").text(data.errors.LugarPareja);
        } else {
          $("#error_LugarPareja").remove();
        }
        if (data.errors.CasaPropia) {
          $("#error_CasaPropia").removeClass("hidden");
          $("#error_CasaPropia").text(data.errors.CasaPropia);
        } else {
          $("#error_CasaPropia").remove();
        }
        if (data.errors.RentaCasa) {
          $("#error_RentaCasa").removeClass("hidden");
          $("#error_RentaCasa").text(data.errors.RentaCasa);
        } else {
          $("#error_RentaCasa").remove();
        }
        if (data.errors.CarroPropio) {
          $("#error_CarroPropio").removeClass("hidden");
          $("#error_CarroPropio").text(data.errors.CarroPropio);
        } else {
          $("#error_CarroPropio").remove();
        }
        if (data.errors.Deudas) {
          $("#error_Deudas").removeClass("hidden");
          $("#error_Deudas").text(data.errors.Deudas);
        } else {
          $("#error_Deudas").remove();
        }
        if (data.errors.MontoDeudas) {
          $("#error_MontoDeudas").removeClass("hidden");
          $("#error_MontoDeudas").text(data.errors.MontoDeudas);
        } else {
          $("#error_MontoDeudas").remove();
        }
        if (data.errors.GastoMensual) {
          $("#error_GastoMensual").removeClass("hidden");
          $("#error_GastoMensual").text(data.errors.GastoMensual);
        } else {
          $("#error_GastoMensual").remove();
        }
      } else {
        showToast('success','Guardado', 'Guardando correctamente');
        $(".error").addClass("hidden");
        tab_siguiente(".tabs_application", "#economic_datas");
      }
    }
  });
});
$("#guarda_actualstudies").click(function() {
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
  });
  $.ajax({
    type: "POST",
    url: "../actual_studies/add",
    data: {
      _token: $("meta[name=csrf-token]").attr("content"),
      user: $("#user").val(),
      NombreEscuela: $("input[name=Name_Escuela]").val(),
      DireccionEscuela: $("input[name=Direccion_Escuela]").val(),
      GradoActual: $("input[name=GradoActual]").val(),
      TerminoEstudios: $("select[name=TerminoEstudios]").val(),
      PeriodoInicialEscuela: $("input[name=PeriodoInicialEscuela]").val(),
      PeriodoFinalEscuela: $("input[name=PeriodoFinalEscuela]").val()
    },
    success: function(data) {
      if (data.errors) {
        faltante();
        if (data.errors.NombreEscuela) {
          $("#error_nombreEscuela").removeClass("hidden");
          $("#error_nombreEscuela").text(data.errors.NombreEscuela);
        } else {
          $("#error_nombreEscuela").remove();
        }
        if (data.errors.DireccionEscuela) {
          $("#error_direccionEscuela").removeClass("hidden");
          $("#error_direccionEscuela").text(data.errors.DireccionEscuela);
        } else {
          $("#error_direccionEscuela").remove();
        }
        if (data.errors.GradoActual) {
          $("#error_GradoActual").removeClass("hidden");
          $("#error_GradoActual").text(data.errors.GradoActual);
        } else {
          $("#error_GradoActual").remove();
        }
        if (data.errors.TerminoEstudios) {
          $("#error_terminoestudios").removeClass("hidden");
          $("#error_terminoestudios").text(data.errors.TerminoEstudios);
        } else {
          $("#error_terminoestudios").remove();
        }
        if (data.errors.PeriodoInicialEscuela) {
          $("#error_PeriodoInicialEscuela").removeClass("hidden");
          $("#error_PeriodoInicialEscuela").text(
            data.errors.PeriodoInicialEscuela
            );
        } else {
          $("#error_PeriodoInicialEscuela").remove();
        }
        if (data.errors.PeriodoFinalEscuela) {
          $("#error_PeriodoFinalEscuela").removeClass("hidden");
          $("#error_PeriodoFinalEscuela").text(
            data.errors.PeriodoFinalEscuela
            );
        } else {
          $("#error_PeriodoFinalEscuela").remove();
        }
      } else {
        showToast('success','Guardado', 'Guardando correctamente');
        $(".error").addClass("hidden");
      }
    }
  });
});
  //CHECAR EL ERROR
  $("#guarda_workgeneral").click(function() {
    $.ajax({
      type: "POST",
      url: "../work_general_datas/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        user: $("#user").val(),
        ConocimientoTrabajo: $("input[name=ConocimientoTrabajo]").val(),
        FamiliaEmpresa: $("select[name=FamiliaEmpresa]").val(),
        Seguro: $("select[name=Seguro]").val(),
        PrimerDia: $("input[name=PrimerDia]").val(),
        Puesto_jobapplication: $("select[name=Puesto_workgeneral]").val(),
        Jobcenter_workgeneral: $("select[name=Jobcenter_workgeneral]").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.ConocimientoTrabajo) {
            $("#error_conocimientoTrabajo").removeClass("hidden");
            $("#error_conocimientoTrabajo").text(
              data.errors.ConocimientoTrabajo
              );
          } else {
            $("#error_conocimientoTrabajo").remove();
          }
          if (data.errors.FamiliaEmpresa) {
            $("#error_familiaEmpresa").removeClass("hidden");
            $("#error_familiaEmpresa").text(data.errors.FamiliaEmpresa);
          } else {
            $("#error_familiaEmpresa").remove();
          }
          if (data.errors.Seguro) {
            $("#error_seguro").removeClass("hidden");
            $("#error_seguro").text(data.errors.Seguro);
          } else {
            $("#error_seguro").remove();
          }
          if (data.errors.Residencia) {
            $("#error_residencia").removeClass("hidden");
            $("#error_residencia").text(data.errors.Residencia);
          } else {
            $("#error_residencia").remove();
          }
          if (data.errors.PrimerDia) {
            $("#error_primerDia").removeClass("hidden");
            $("#error_primerDia").text(data.errors.PrimerDia);
          } else {
            $("#error_primerDia").remove();
          }
          if (data.errors.Puesto_jobapplication) {
            $("#error_puesto").removeClass("hidden");
            $("#error_puesto").text(data.errors.Puesto_jobapplication);
          } else {
            $("#error_puesto").remove();
          }
          if (data.errors.Jobcenter_workgeneral) {
            $("#error_jobcenter").removeClass("hidden");
            $("#error_jobcenter").text(data.errors.Jobcenter_workgeneral);
          } else {
            $("#error_jobcenter").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          //consulta porcentaje match
          $(".error").addClass("hidden");
          $("#Step2").attr("data-toggle", "tab");
          $("#Step2").click();
        }
      }
    });
  });
  //END JOB APLICATION
  //jobtitleprofile
  $("#guardar_perfil").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajax({
      type: "POST",
      url: "../../../job_title_profile/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Objetivo_Puesto: $("textarea[name=Objetivo_Puesto]").val(),
        SalarioMax: $("input[name=SalarioMax]").val(),
        SalarioMin: $("input[name=SalarioMin]").val(),
        Id_Profile: Id_Profile,
        Funcion_Puesto: $("select[name=Funcion_Puesto]").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.Centro_Trabajo) {
            $("#error_centrotrabajo").removeClass("hidden");
            $("#error_centrotrabajo").text(data.errors.Centro_Trabajo);
          } else {
            $("#error_centrotrabajo").remove();
          }
          if (data.errors.Objetivo_Puesto) {
            $("#error_objetivopuesto").removeClass("hidden");
            $("#error_objetivopuesto").text(data.errors.Objetivo_Puesto);
          } else {
            $("#error_objetivopuesto").remove();
          }
          if (data.errors.SalarioMax) {
            $("#error_salariomaximo").removeClass("hidden");
            $("#error_salariomaximo").text(data.errors.SalarioMax);
          } else {
            $("#error_salariomaximo").remove();
          }
          if (data.errors.SalarioMin) {
            $("#error_salariominimo").removeClass("hidden");
            $("#error_salariominimo").text(data.errors.SalarioMin);
          } else {
            $("#error_salariominimo").remove();
          }
          if (data.errors.Funcion_Puesto) {
            $("#error_funcionpuesto").removeClass("hidden");
            $("#error_funcionpuesto").text(data.errors.Funcion_Puesto);
          } else {
            $("#error_funcionpuesto").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          $('#tabResponsabilities').click();
        }
      }
    });
  });
  //Add Responsabilities
  $("#add_Responsabilidades").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajax({
      type: "POST",
      url: "../../../jobtitle_responsability/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Responsabilidad: $("input[name=inputResponsabilidades]").val(),
        Id_Profile: Id_Profile
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.Responsabilidad) {
            $("#error_responsabilidadespuesto").removeClass("hidden");
            $("#error_responsabilidadespuesto").text(
              data.errors.Responsabilidad
              );
          } else {
            $("#error_responsabilidadespuesto").removeClass("hidden");
            $("#error_responsabilidadespuesto").text(data.errors.Id_Profile);
          }
        } else {
          agregar_Responsabilidades();
          $(".error").addClass("hidden");
          $("#inputResponsabilidades").val("");
          $("#inputResponsabilidades").focus();
        }
      }
    });
  });
  var cont = 0;
  function agregar_Responsabilidades() {
    cont++;
    var fila =
    '<tr id="Responsabilidades_' +
    cont +
    '"><td>' +
    $("input[name=inputResponsabilidades]").val() +
    "</td></tr>";
    $("#tablaResponsabilidades").append(fila);
  }
  //Responsabilities end
  //Add Functions
  $("#add_function").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajax({
      type: "POST",
      url: " ../../../jobtitle_function/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Funcion: $("input[name=inputFunciones]").val(),
        Id_Profile: Id_Profile
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.Funcion) {
            $("#error_funcionespuesto").removeClass("hidden");
            $("#error_funcionespuesto").text(data.errors.Funcion);
          } else {
            $("#error_funcionespuesto").removeClass("hidden");
            $("#error_funcionespuesto").text(data.errors.Id_Profile);
          }
        } else {
          agregar_Funciones();
          $(".error").addClass("hidden");
          $("#inputFunciones").val("");
          $("#inputFunciones").focus();
        }
      }
    });
  });
  var cont = 0;
  function agregar_Funciones() {
    cont++;
    var fila =
    '<tr id="Funciones_' +
    cont +
    '"><td>' +
    $("input[name=inputFunciones]").val() +
    "</td></tr>";
    $("#tablaFunciones").append(fila);
  }
  //Functions end
  //Add Attitude
  $("#add_Actitudes").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajax({
      type: "POST",
      url: " ../../../jobtitle_attitude/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Actitud: $("input[name=inputActitudes]").val(),
        Id_Profile: Id_Profile
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.Actitud) {
            $("#error_actitudespuesto").removeClass("hidden");
            $("#error_actitudespuesto").text(data.errors.Actitud);
          } else {
            $("#error_actitudespuesto").removeClass("hidden");
            $("#error_actitudespuesto").text(data.errors.Id_Profile);
          }
        } else {
          agregar_Actitudes();
          $(".error").addClass("hidden");
          $("#inputActitudes").val("");
          $("#inputActitudes").focus();
        }
      }
    });
  });
  var cont = 0;
  function agregar_Actitudes() {
    cont++;
    var fila =
    '<tr id="Actitudes_' +
    cont +
    '"><td>' +
    $("input[name=inputActitudes]").val() +
    "</td></tr>";
    $("#tablaActitudes").append(fila);
  }
  //Attitudes end
  $("#step_responsabilities").click(function() {
    $("#Tabdemograficos").click();
  });
  //Demographic
  $("#EscolaridadPuesto").change(function(event) {
    if ($("#EscolaridadPuesto").prop("selectedIndex") >= 5) {
      $("#div_carrera").removeClass("hidden");
    } else {
      $("#div_carrera").addClass("hidden");
    }
  });
  $("#addscholarprofile").click(function() {
    $.ajax({
      type: "POST",
      url: "../../../job_title_profile_scholar/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        GradoAcademicoProfile: $("#GradoAcademicoProfile").val(),
        CarreraProfile: $("#CarreraProfile").val(),
        Id_Profile: $("#id_profile").val()
      },
      success: function(data) {
        if (data.errors) {
          if (data.errors.GradoAcademicoProfile) {
            $("#error_GradoAcademicoProfile").removeClass("hidden");
            $("#error_GradoAcademicoProfile").text(
              data.errors.GradoAcademicoProfile
              );
          } else {
            $("#error_GradoAcademicoProfile").remove();
          }
          if (data.errors.CarreraProfile) {
            $("#GradoAcademicoProfile").removeClass("hidden");
            $("#GradoAcademicoProfile").text(data.errors.CarreraProfile);
          } else {
            $("#GradoAcademicoProfile").remove();
          }
        } else {
          agregar_scholar_aca();
          $(".error").addClass("hidden");
          $("#GradoAcademicoProfile").val("");
          $("#CarreraProfile").val("");
        }
      }
    });
  });
  var cont = 0;
  function agregar_scholar_aca() {
    cont++;
    var fila =
    '<tr id="GradoAcademicoProfiles' +
    cont +
    '"><td>' +
    $("select[name=GradoAcademicoProfile] option:selected")
    .select2()
    .text() +
    "</td><td>" +
    $("select[name=CarreraProfile] option:selected")
    .select2()
    .text() +
    "</td></tr>";
    $("#tablaScholarProfile_aca").append(fila);
  }
  $(".deletescholarprofile").click(function() {
    $(this)
    .parent("td")
    .parent("tr")
    .remove();
    $.ajax({
      type: "POST",
      url: "../../../job_title_profile_scholar/delete",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Id: $(this).attr("data-info")
      },
      success: function() {}
    });
  });
  $("#add_Experiencia").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../../../job_title_experience/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Experiencia: $("input[name=inputExperiencia]").val(),
        AñosExperiencia:
        $("input[name=ExperienciaTiempo]").val() +
        " " +
        $("select[name=experience_time]").val(),
        Id_Profile: Id_Profile
      },
      success: function(data) {
        if (data.errors) {
          if (data.errors.Experiencia) {
            $("#error_experienciapuesto").removeClass("hidden");
            $("#error_experienciapuesto").text(data.errors.Experiencia);
          } else {
            $("#error_experienciapuesto").remove();
          }
        } else {
          agregar_experiencia();
          $(".error").addClass("hidden");
        }
      }
    });
  });
  var cont2 = 0;
  function agregar_experiencia() {
    cont2++;
    var años =
    $("input[name=ExperienciaTiempo]").val() +
    " " +
    $("select[name=experience_time]").val();
    var fila =
    '<tr id="Experiencia' +
    cont2 +
    '"><td>' +
    $("input[name=inputExperiencia]").val() +
    "</td><td class = text-center>" +
    años +
    "</td></tr>";
    $("#tablaExperienciaContenido").append(fila);
    $("#divExperiences").css("display","inline-block");
  }
  $("#guardar_demographic").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../../../job_title_profile_demographic/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Id_Profile: Id_Profile,
        minAge: $("input[name=minAge]").val(),
        maxAge: $("input[name=maxAge]").val(),
        GeneroPuesto: $("select[name=GeneroPuesto]").val(),
        EstadoCivilPuesto: $("select[name=EstadoCivilPuesto]").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.minAge) {
            $("#error_minage").removeClass("hidden");
            $("#error_minage").text(data.errors.minAge);
          } else {
            $("#error_minage").remove();
          }
          if (data.errors.maxAge) {
            $("#error_maxage").removeClass("hidden");
            $("#error_maxage").text(data.errors.maxAge);
          } else {
            $("#error_maxage").remove();
          }
          if (data.errors.GeneroPuesto) {
            $("#error_generopuesto").removeClass("hidden");
            $("#error_generopuesto").text(data.errors.GeneroPuesto);
          } else {
            $("#error_generopuesto").remove();
          }
          if (data.errors.EstadoCivilPuesto) {
            $("#error_estcivilpuesto").removeClass("hidden");
            $("#error_estcivilpuesto").text(data.errors.EstadoCivilPuesto);
          } else {
            $("#error_estcivilpuesto").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          $("#Tabconocimientos").click();
        }
      }
    });
  });
  //Demographic end
  //Add knowledges
  //Add lenguages
  $("#add_language_profile").click(function() {
    $.ajax({
      type: "POST",
      url: "../../../job_language/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Lenguaje: $("#languageknow_profile").val(),
        languageread: $("#languageread_profile").val(),
        languagewrite: $("#languagewrite_profile").val(),
        languagespeak: $("#languagespeak_profile").val(),
        languagelisten: $("#languagelisten_profile").val(),
        Id_Profile: $("#id_profile").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.languageknow_profile) {
            $("#error_languageknow_profile").removeClass("hidden");
            $("#error_languageknow_profile").text(data.errors.Lenguaje);
          } else {
            $("#error_languageknow_profile").addClass("hidden");
          }
          if (data.errors.languageread_profile) {
            $("#error_languageread_profile").removeClass("hidden");
            $("#error_languageread_profile").text(data.errors.languageread);
          } else {
            $("#error_languageread_profile").addClass("hidden");
          }
          if (data.errors.languagewrite_profile) {
            $("#error_languagewrite_profile").removeClass("hidden");
            $("#error_languagewrite_profile").text(data.errors.languagewrite);
          } else {
            $("#error_languagewrite_profile").addClass("hidden");
          }
          if (data.errors.languagespeak_profile) {
            $("#error_languagespeak_profile").removeClass("hidden");
            $("#error_languagespeak_profile").text(data.errors.languagespeak);
          } else {
            $("#error_languagespeak_profile").addClass("hidden");
          }
          if (data.errors.languagelisten_profile) {
            $("#error_languagelisten_profile").removeClass("hidden");
            $("#error_languagelisten_profile").text(data.errors.languagelisten);
          } else {
            $("#error_languagelisten_profile").addClass("hidden");
          }
        } else {
          $(".error").addClass("hidden");
          agregar_language_profile(data.level);
        }
      }
    });
  });
  var cont = 0;
  function agregar_language_profile(level) {
    cont++;
    var fila =
    '<tr class="col-md-offset-3 col-md-6 " id="languageknow_profile' +
    cont +
    '"><td>' +
    $("select[name=languageknow_profile] option:selected")
    .select2()
    .text() +
    "</td><td>" +
    level +
    "</td></tr>";
    $("#tablaLenguajes_profile").append(fila);
  }
  //anterior
  $("#add_lenguaje").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../../../job_language/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Lenguaje: $("input[name=LenguajePuesto]").val(),
        LenguajePorcentaje: $("input[name=LenguajePorcentaje]").val(),
        Id_Profile: Id_Profile
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.Lenguaje) {
            $("#error_idiomapuesto").removeClass("hidden");
            $("#error_idiomapuesto").text(data.errors.Lenguaje);
          } else {
            $("#error_idiomapuesto").remove();
          }
        } else {
          agregar_lenguaje();
          $(".error").addClass("hidden");
          $("#LenguajePuesto").val("");
          $("#LenguajePuesto").focus();
          $("#LenguajePorcentaje").val("");
        }
      }
    });
  });
  var cont = 0;
  function agregar_lenguaje() {
    cont++;
    var lenguaje = $("input[name=LenguajePuesto]").val();
    var porcentaje = $("input[name=LenguajePorcentaje]").val() + " %";
    var fila =
    '<tr class="col-md-offset-4 col-md-4" id="lenguaje' +
    cont +
    '"><td class="col-md-6">' +
    lenguaje +
    '</td><td class="col-md-6">' +
    porcentaje +
    "</td></tr>";
    $("#tablaLenguajes").append(fila);
  }
  $(".delete_language_profile").click(function() {
    var eliminar=$(this).parent("td").parent("tr");
    $.ajax({
      type: "POST",
      url: "../../../job_language/delete",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Id: $(this).attr("data-info")
      },
      success: function() {
        eliminar.remove();
      }
    });
  });
  //Add office_functions
  $("#add_Oficina").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../../../jobtitle_office/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Oficina: $("input[name=Oficina]").val(),
        Id_Profile: Id_Profile
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.Oficina) {
            $("#error_funciones").removeClass("hidden");
            $("#error_funciones").text(data.errors.Oficina);
          } else {
            $("#error_funciones").remove();
          }
        } else {
          agregar_Oficina();
          $(".error").addClass("hidden");
          $("#Oficina").val("");
          $("#Oficina").focus();
        }
      }
    });
  });
  var cont = 0;
  function agregar_Oficina() {
    cont++;
    var oficina = $("input[name=Oficina]").val();
    var fila = '<tr id="oficina_' + cont + '"><td>' + oficina + "</td></tr>";
    $("#tablaOficina").append(fila);
  }
  // //Add Machines
  $("#add_Maquinas").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../../../jobtitle_machine/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Maquinas: $("input[name=MaquinasPuesto]").val(),
        Id_Profile: Id_Profile
      },
      success: function(data) {
        if (data.errors) {
          if (data.errors.Maquinas) {
            $("#error_maquinaspuesto").removeClass("hidden");
            $("#error_maquinaspuesto").text(data.errors.Maquinas);
          } else {
            $("#error_maquinaspuesto").remove();
          }
        } else {
          agregar_Maquinas();
          $(".error").addClass("hidden");
        }
      }
    });
  });
  var cont = 0;
  function agregar_Maquinas() {
    cont++;
    var Maquinas = $("input[name=MaquinasPuesto]").val();
    var fila = '<tr id="Maquinas_' + cont + '"><td>' + Maquinas + "</td></tr>";
    $("#tablaMaquinas").append(fila);
  }
  // //Add Software
  $("#add_Software").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../../../jobtitle_software/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Software: $("input[name=SoftwarePuesto]").val(),
        Id_Profile: Id_Profile
      },
      success: function(data) {
        if (data.errors) {
          if (data.errors.Software) {
            $("#error_softwarePuesto").removeClass("hidden");
            $("#error_softwarePuesto").text(data.errors.Software);
          } else {
            $("#error_softwarePuesto").remove();
          }
        } else {
          agregar_Software();
          $(".error").addClass("hidden");
        }
      }
    });
  });
  var cont = 0;
  function agregar_Software() {
    cont++;
    var Software = $("input[name=SoftwarePuesto]").val();
    var fila = '<tr id="Software_' + cont + '"><td>' + Software + "</td></tr>";
    $("#tablaSoftware").append(fila);
  }
  $("#step_knowledges").click(function() {
    $("#Tabinterview").click();
  });
  //Knowledges end
  //Add interview
  $("#guardar_entrevista").click(function() {
    var Id_Profile = $("#id_profile").val();
    $.ajax({
      type: "POST",
      url: "../../../job_title_profile_interview/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Id_Profile: Id_Profile,
        EntrevistaPuesto: $("select[name=EntrevistaPuesto]").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.EntrevistaPuesto) {
            $("#error_entrevistapuesto").removeClass("hidden");
            $("#error_entrevistapuesto").text(data.errors.EntrevistaPuesto);
          } else {
            $("#error_entrevistapuesto").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          $("#Tabdocuments").click();

        }
      }
    });
  });
  //interview end
  //Add Documents
  $("#add_documento").click(function() {
    if ($("input[name=OtroDocumento]").val() == "") {
      faltante_jobcenter();
    } else agregar_documento();
  });
  var cont = 0;
  function agregar_documento() {
    cont++;
    var documento = $("input[name=OtroDocumento]").val();
    var fila =
    '<tr id="documento_' +
    cont +
    '"><td>' +
    documento +
    '</td><td class=text-center><input type="checkbox" class="verificadorDocumentos" checked value="' +
    documento +
    '"></td></tr>';
    $("#tablaDocumentos").prepend(fila);
  }
  $("#guardarDocumentos").click(function() {
    var documentos = [];
    $(".verificadorDocumentos:checked").each(function(i) {
      documentos[i] = $(this).val();
    });
    var Id_Profile = $("#id_profile").val();
    $.ajax({
      type: "POST",
      url: "../../../job_title_documents/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Id_Profile: Id_Profile,
        Documento: documentos
      },
      success: function(data) {
        if (data.errors) {
          fDocuments();
          // alert('Completa la información necesaria para guardar los documentos');
        } else {
          // alert("Documentos requeridos para el Puesto han sido guardados");
          showToast('success','Guardado', 'Guardando correctamente');
          $("#Tabcontract").click();
        }
      }
    });
  });
  var cont = 0;
  $('.verificadorDocumentos').change(function(){
    var documento=$(this).parent().parent().children('.textoDocumento').text();
    if($(this).prop('checked')){
      var fila = '<tr ' + cont + '"><td>' + documento + "</td></tr>";
      $("#tablaDocumentosmuestra").append(fila);
    }
    else{
      var docs = $("#tablaDocumentosmuestra").children('tr');
      docs.each(function(){
        if($(this).text() == documento)
        {
          $(this).remove();
        }
      });
    }
  });
  // Documents end
  $("#guardar_contrato_jobtitle").click(function() {
    var contratos = [];
    $("input[name=contratosjobs]:checked").each(function(i) {
      contratos[i] = $(this).val();
    });
    var Id_Profile = $("#id_profile").val();
    $.ajax({
      type: "POST",
      url: " ../../../job_title_profile_contract/add ",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Id_Profile: Id_Profile,
        Contrato: contratos
      },
      success: function(data) {
        if (data.errors) {
          if (data.errors.Contrato) {
            faltante_jobcenter();
            $("#error_contratopuesto").removeClass("hidden");
            $("#error_contratopuesto").text(data.errors.Contrato);
          } else {
            $("#error_contratopuesto").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          $("._contract").collapse('toggle');
        }
      }
    });
  });
  var cont = 0;
  $('.contratosjobs').change(function(){
    var contrato=$(this).parent().parent().children('.textoContrato').text();
    if($(this).prop('checked')){
      var fila = '<tr ' + cont + '"><td>' + contrato + "</td></tr>";
      $("#tablaContratosmuestra").append(fila);
    }
    else{
      var contract = $("#tablaContratosmuestra").children('tr');
      contract.each(function(){
        if($(this).text() == contrato)
        {
          $(this).remove();
        }
      });
    }
  });
  // End jobtitle Profile

  //--------------JOB CENTER--------------
  $("#guardar_perfil_jobcenter").click(function() {
    $.ajax({
      type: "POST",
      url: "../../add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        jobcenter_id: $("#jobcenter_id").val(),
        NombreJobCenter: $("input[name=NombreJobCenter]").val(),
        CodigoJobCenter: $("input[name=CodigoJobCenter]").val(),
        Imagen: $("input[name=Imagen]").val(),
        TipoPuesto: $("select[name=TipoPuesto]").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.NombreJobCenter) {
            $("#error_jobcenter").removeClass("hidden");
            $("#error_jobcenter").text(data.errors.NombreJobCenter);
          } else {
            $("#error_jobcenter").remove();
          }
          if (data.errors.CodigoJobCenter) {
            $("#error_codigojobcenter").removeClass("hidden");
            $("#error_codigojobcenter").text(data.errors.CodigoJobCenter);
          } else {
            $("#error_codigojobcenter").remove();
          }
          if (data.errors.Imagen) {
            $("#error_imagen").removeClass("hidden");
            $("#error_imagen").text(data.errors.Imagen);
          } else {
            $("#error_imagen").remove();
          }
          if (data.errors.TipoPuesto) {
            $("#error_tipopuesto").removeClass("hidden");
            $("#error_tipopuesto").text(data.errors.TipoPuesto);
          } else {
            $("#error_tipopuesto").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          $("#Tabdomicilio").click();
        }
      }
    });
  });
  $("#guardar_address").click(function() {
    var Id_JobCenter = $("#jobcenter_id").val();
    $.ajax({
      type: "POST",
      url: "../../../address_job_centers/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Calle: $("input[name=Calle]").val(),
        NumExt: $("input[name=NumExt]").val(),
        NumInt: $("input[name=NumInt]").val(),
        Colonia: $("input[name=Colonia]").val(),
        CodigoPostal: $("input[name=CodigoPostal]").val(),
        Localidad: $("input[name=Localidad]").val(),
        Municipio: $("input[name=Municipio]").val(),
        Estado: $("select[name=Estado]").val(),
        Pais: $("input[name=Pais]").val(),
        Telefono: $("input[name=Telefono]").val(),
        Ubicacion: $("input[name=Ubicacion]").val(),
        Correo: $("input[name=Correo]").val(),
        Identificador: Id_JobCenter
      },
      success: function(data) {
        if (data.errors) {
          faltante_jobcenter();
          if (data.errors.Calle) {
            $("#error_calle").removeClass("hidden");
            $("#error_calle").text(data.errors.Calle);
          } else {
            $("#error_calle").remove();
          }
          if (data.errors.NumExt) {
            $("#error_exterior").removeClass("hidden");
            $("#error_exterior").text(data.errors.NumExt);
          } else {
            $("#error_exterior").remove();
          }
          if (data.errors.NumInt) {
            $("#error_interior").removeClass("hidden");
            $("#error_interior").text(data.errors.NumInt);
          } else {
            $("#error_interior").remove();
          }
          if (data.errors.Colonia) {
            $("#error_colonia").removeClass("hidden");
            $("#error_colonia").text(data.errors.Colonia);
          } else {
            $("#error_colonia").remove();
          }
          if (data.errors.CodigoPostal) {
            $("#error_cp").removeClass("hidden");
            $("#error_cp").text(data.errors.CodigoPostal);
          } else {
            $("#error_cp").remove();
          }
          if (data.errors.Localidad) {
            $("#error_localidad").removeClass("hidden");
            $("#error_localidad").text(data.errors.Localidad);
          } else {
            $("#error_localidad").remove();
          }
          if (data.errors.Municipio) {
            $("#error_municipio").removeClass("hidden");
            $("#error_municipio").text(data.errors.Municipio);
          } else {
            $("#error_municipio").remove();
          }
          if (data.errors.Estado) {
            $("#error_estado").removeClass("hidden");
            $("#error_estado").text(data.errors.Estado);
          } else {
            $("#error_estado").remove();
          }
          if (data.errors.Pais) {
            $("#error_pais").removeClass("hidden");
            $("#error_pais").text(data.errors.Pais);
          } else {
            $("#error_pais").remove();
          }
          if (data.errors.Telefono) {
            $("#error_telefono").removeClass("hidden");
            $("#error_telefono").text(data.errors.Telefono);
          } else {
            $("#error_telefono").remove();
          }
          if (data.errors.Ubicacion) {
            $("#error_ubicacion").removeClass("hidden");
            $("#error_ubicacion").text(data.errors.Ubicacion);
          } else {
            $("#error_ubicacion").remove();
          }
          if (data.errors.Correo) {
            $("#error_correo").removeClass("hidden");
            $("#error_correo").text(data.errors.Correo);
          } else {
            $("#error_correo").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          $("#TabCapacidadLaboral").click();
        }
      }
    });
  });
  $("#add_puesto").click(function() {
    var Id_JobCenter = $("#jobcenter_id").val();
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../../../workcapacity_job_centers/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Puesto: $("select[name=puestoJobCenter]").val(),
        cantidadPuesto: $("input[name=cantidadPuesto]").val(),
        Identificador: Id_JobCenter
      },
      success: function(data) {
        if (data.errors) {
          Cpuesto();
          if (data.errors.Puesto) {
            $("#error_puestojobcenter").removeClass("hidden");
            $("#error_puestojobcenter").text(data.errors.Puesto);
          } else {
            $("#error_puestojobcenter").remove();
          }
          if (data.errors.cantidadPuesto) {
            $("#error_cantidad").removeClass("hidden");
            $("#error_cantidad").text(data.errors.cantidadPuesto);
          } else {
            $("#error_cantidad").remove();
          }
        } else {
          agregar_puesto();
          $(".error").addClass("hidden");
        }
      }
    });
  });
  $("#guardar_workcapacity").click(function() {
    $("#TabInformaciongeneral").click();
  });
  var cont = 0;
  function agregar_puesto() {
    cont++;
    var puesto = $("select[name=puestoJobCenter] option:selected").text();
    var can = $("input[name=cantidadPuesto]").val();
    elements = $('#tablaPuestos').children('tbody').children("tr");
    for (var i = 0; i < elements.length; i++) {
      if (puesto == $(elements[i]).find('#jobtitle_capacity').text()) {
       $(elements[i]).remove();
     }
   }
   $('#tablaPuestos').children('tbody').children("tr");
   var fila =
   '<tr id="puesto_' +
   cont +
   '" class="text-center" style="color: black"><td>' +
   puesto +
   "</td><td class=text-center>" +
   can +
   "</td></tr>";
   $("#tablaPuestos").append(fila);
 }
 $("#guardar_general_info").click(function() {
  var Id_JobCenter = $("#jobcenter_id").val();
  $.ajaxSetup({});
  $.ajax({
    type: "POST",
    url: "../../../general_information_job_centers/add",
    data: {
      _token: $("meta[name=csrf-token]").attr("content"),
      HorarioInicial: $("input[name=HorarioInicial]").val(),
      HorarioFinal: $("input[name=HorarioFinal]").val(),
      DiasLaboralesInicio: $("select[name=DiasLaboralesInicio]").val(),
      DiasLaboralesFinal: $("select[name=DiasLaboralesFinal]").val(),
      Fondo: $("input[name=Fondo]").val(),
      ArchivoFondo: $("input[name=ArchivoFondo]").val(),
      Equilibrio: $("input[name=Equilibrio]").val(),
      ContratoRenta: $("input[name=ContratoRenta]").val(),
      VigenciaRenta: $("input[name=VigenciaRenta]").val(),
      Hacienda: $("input[name=Hacienda]").val(),
      VigenciaHacienda: $("input[name=VigenciaHacienda]").val(),
      PermisoComercial: $("input[name=PermisoComercial]").val(),
      VigenciaPermiso: $("input[name=VigenciaPermiso]").val(),
      Fumigacion: $("input[name=Fumigacion]").val(),
      VigenciaFumigacion: $("input[name=VigenciaFumigacion]").val(),
      Extintor: $("input[name=Extintor]").val(),
      VigenciaExtintor: $("input[name=VigenciaExtintor]").val(),
      Salubridad: $("input[name=Salubridad]").val(),
      VigenciaSalubridad: $("input[name=VigenciaSalubridad]").val(),
      Identificador: Id_JobCenter
    },
    success: function(data) {
      if (data.errors) {
        faltante_jobcenter();
        if (data.errors.HorarioInicial) {
          $("#error_horarioinicial").removeClass("hidden");
          $("#error_horarioinicial").text(data.errors.HorarioInicial);
        } else {
          $("#error_horarioinicial").remove();
        }
        if (data.errors.HorarioFinal) {
          $("#error_horariofinal").removeClass("hidden");
          $("#error_horariofinal").text(data.errors.HorarioFinal);
        } else {
          $("#error_horariofinal").remove();
        }
        if (data.errors.DiasLaboralesInicio) {
          $("#error_inicioDias").removeClass("hidden");
          $("#error_inicioDias").text(data.errors.DiasLaboralesInicio);
        } else {
          $("#error_inicioDias").remove();
        }
        if (data.errors.DiasLaboralesFinal) {
          $("#error_finalDias").removeClass("hidden");
          $("#error_finalDias").text(data.errors.DiasLaboralesFinal);
        } else {
          $("#error_finalDias").remove();
        }
        if (data.errors.Fondo) {
          $("#error_fondofijo").removeClass("hidden");
          $("#error_fondofijo").text(data.errors.Fondo);
        } else {
          $("#error_fondofijo").remove();
        }
        if (data.errors.archivoFondo) {
          $("#error_archivofondo").removeClass("hidden");
          $("#error_archivofondo").text(data.errors.archivoFondo);
        } else {
          $("#error_archivofondo").remove();
        }
        if (data.errors.Equilibrio) {
          $("#error_equilibrio").removeClass("hidden");
          $("#error_equilibrio").text(data.errors.Equilibrio);
        } else {
          $("#error_equilibrio").remove();
        }
        if (data.errors.ContratoRenta) {
          $("#error_contratorenta").removeClass("hidden");
          $("#error_contratorenta").text(data.errors.ContratoRenta);
        } else {
          $("#error_contratorenta").remove();
        }
        if (data.errors.VigenciaRenta) {
          $("#error_vigenciarenta").removeClass("hidden");
          $("#error_vigenciarenta").text(data.errors.VigenciaRenta);
        } else {
          $("#error_vigenciarenta").remove();
        }
        if (data.errors.Hacienda) {
          $("#error_hacienda").removeClass("hidden");
          $("#error_hacienda").text(data.errors.Hacienda);
        } else {
          $("#error_hacienda").remove();
        }
        if (data.errors.VigenciaHacienda) {
          $("#error_vigenciahacienda").removeClass("hidden");
          $("#error_vigenciahacienda").text(data.errors.VigenciaHacienda);
        } else {
          $("#error_vigenciahacienda").remove();
        }
        if (data.errors.PermisoComercial) {
          $("#error_permisocomercial").removeClass("hidden");
          $("#error_permisocomercial").text(data.errors.PermisoComercial);
        } else {
          $("#error_permisocomercial").remove();
        }
        if (data.errors.VigenciaPermiso) {
          $("#error_vigenciapermiso").removeClass("hidden");
          $("#error_vigenciapermiso").text(data.errors.VigenciaPermiso);
        } else {
          $("#error_vigenciapermiso").remove();
        }
        if (data.errors.Fumigacion) {
          $("#error_fumigacion").removeClass("hidden");
          $("#error_fumigacion").text(data.errors.Fumigacion);
        } else {
          $("#error_fumigacion").remove();
        }
        if (data.errors.VigenciaFumigacion) {
          $("#error_vigenciafumigacion").removeClass("hidden");
          $("#error_vigenciafumigacion").text(data.errors.VigenciaFumigacion);
        } else {
          $("#error_vigenciafumigacion").remove();
        }
        if (data.errors.Extintor) {
          $("#error_extintor").removeClass("hidden");
          $("#error_extintor").text(data.errors.Extintor);
        } else {
          $("#error_extintor").remove();
        }
        if (data.errors.VigenciaExtintor) {
          $("#error_vigenciaextintor").removeClass("hidden");
          $("#error_vigenciaextintor").text(data.errors.VigenciaExtintor);
        } else {
          $("#error_vigenciaextintor").remove();
        }
        if (data.errors.Salubridad) {
          $("#error_salubridad").removeClass("hidden");
          $("#error_salubridad").text(data.errors.Salubridad);
        } else {
          $("#error_salubridad").remove();
        }
        if (data.errors.VigenciaSalubridad) {
          $("#error_vigenciasalubridad").removeClass("hidden");
          $("#error_vigenciasalubridad").text(data.errors.VigenciaSalubridad);
        } else {
          $("#error_vigenciasalubridad").remove();
        }
      } else {
        showToast('success','Guardado', 'Guardando correctamente');
        $(".error").addClass("hidden");
        $("#TabMetas").click();
      }
    }
  });
});
$(".sumas").change(function() {
  var numero1=Number(document.getElementById("numero1").value);
  var numero2=Number(document.getElementById("numero2").value);
  var numero3=Number(document.getElementById("numero3").value);
  var numero4=Number(document.getElementById("numero4").value);
  var numero5=Number(document.getElementById("numero5").value);
  var numero6=Number(document.getElementById("numero6").value);
  var numero7=Number(document.getElementById("numero7").value);
  var numero8=Number(document.getElementById("numero8").value);
  var numero9=Number(document.getElementById("numero9").value);
  var numero10=Number(document.getElementById("numero10").value);
  var numero11=Number(document.getElementById("numero11").value);
  var numero12=Number(document.getElementById("numero12").value);
  var spTotal=numero1+numero2+numero3+numero4+numero5+numero6+numero7+numero8+numero9+numero10+numero11+numero12;
  sumar(spTotal);
});
/* Sumar dos números. */
function sumar(valor) {
  var total = 0;
  valor = parseFloat(valor);
  total = document.getElementById("spTotal").innerHTML;
  total = total == null || total == undefined || total == "" ? 0 : total;
  total = parseFloat(valor);
  document.getElementById("spTotal").innerHTML = total;
}
$("#guardar_goals").click(function() {
  var Id_JobCenter = $("#jobcenter_id").val();
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
  });
  $.ajax({
    type: "POST",
    url: "../../../goal_job_centers/add",
    data: {
      _token: $("meta[name=csrf-token]").attr("content"),
      txt_campo1: $("input[id=numero1]").val(),
      txt_campo2: $("input[id=numero2]").val(),
      txt_campo3: $("input[id=numero3]").val(),
      txt_campo4: $("input[id=numero4]").val(),
      txt_campo5: $("input[id=numero5]").val(),
      txt_campo6: $("input[id=numero6]").val(),
      txt_campo7: $("input[id=numero7]").val(),
      txt_campo8: $("input[id=numero8]").val(),
      txt_campo9: $("input[id=numero9]").val(),
      txt_campo10: $("input[id=numero10]").val(),
      txt_campo11: $("input[id=numero11]").val(),
      txt_campo12: $("input[id=numero12]").val(),
      spTotal: $("#spTotal").text(),
      Identificador: Id_JobCenter
    },
    success: function(data) {
      if (data.errors) {
        faltante_jobcenter();
        if (data.errors.txt_campo1) {
          $("#error_enero").removeClass("hidden");
          $("#error_enero").text(data.errors.txt_campo1);
        } else {
          $("#error_enero").remove();
        }
        if (data.errors.txt_campo2) {
          $("#error_febrero").removeClass("hidden");
          $("#error_febrero").text(data.errors.txt_campo2);
        } else {
          $("#error_febrero").remove();
        }
        if (data.errors.txt_campo3) {
          $("#error_marzo").removeClass("hidden");
          $("#error_marzo").text(data.errors.txt_campo3);
        } else {
          $("#error_marzo").remove();
        }
        if (data.errors.txt_campo4) {
          $("#error_abril").removeClass("hidden");
          $("#error_abril").text(data.errors.txt_campo4);
        } else {
          $("#error_abril").remove();
        }
        if (data.errors.txt_campo5) {
          $("#error_mayo").removeClass("hidden");
          $("#error_mayo").text(data.errors.txt_campo5);
        } else {
          $("#error_mayo").remove();
        }
        if (data.errors.txt_campo6) {
          $("#error_junio").removeClass("hidden");
          $("#error_junio").text(data.errors.txt_campo6);
        } else {
          $("#error_junio").remove();
        }
        if (data.errors.txt_campo7) {
          $("#error_julio").removeClass("hidden");
          $("#error_julio").text(data.errors.txt_campo7);
        } else {
          $("#error_julio").remove();
        }
        if (data.errors.txt_campo8) {
          $("#error_agosto").removeClass("hidden");
          $("#error_agosto").text(data.errors.txt_campo8);
        } else {
          $("#error_agosto").remove();
        }
        if (data.errors.txt_campo9) {
          $("#error_septiembre").removeClass("hidden");
          $("#error_septiembre").text(data.errors.txt_campo9);
        } else {
          $("#error_septiembre").remove();
        }
        if (data.errors.txt_campo10) {
          $("#error_octubre").removeClass("hidden");
          $("#error_octubre").text(data.errors.txt_campo10);
        } else {
          $("#error_octubre").remove();
        }
        if (data.errors.txt_campo11) {
          $("#error_noviembre").removeClass("hidden");
          $("#error_noviembre").text(data.errors.txt_campo11);
        } else {
          $("#error_noviembre").remove();
        }
        if (data.errors.txt_campo12) {
          $("#error_diciembre").removeClass("hidden");
          $("#error_diciembre").text(data.errors.txt_campo12);
        } else {
          $("#error_diciembre").remove();
        }
      } else {
        showToast('success','Guardado', 'Guardando correctamente');
          window.location.href = "../../../tree_jobcenter/create"
        $(".error").addClass("hidden");
        $(".next-step").click(function(e) {
          var $active = $(".wizard .nav-tabs li.active");
        });
      }
    }
  });
});
  //END JOB CENTER
  //SCHEDULED INTERVIEW

  $(".openInterview").click(function(){
    var all = $(this);
    $('#dateInterview').val('');
    $('#timeInterview').val('');
    $('#interviewerAdd').val('');
    $('#JobcenterInterview').val('');
    $.ajax({
      type: "POST",
      url: "../scheduled_interview/search",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Applicant_id: all.attr('data-id'),
      },
      success: function(data) {
        if (data.errors) {
        } else {
          if(jQuery.isEmptyObject(data)){
            $("#Applicant_id").val(all.attr('data-id'));
          }
          else{
            $("#Applicant_id").val(data.applicant_id);
            $('#dateInterview').val(data.dateInterview);
            $('#timeInterview').val(data.timeInterview);
            $('#interviewerAdd').val(data.interviewerAdd);
            $('#JobcenterInterview').val(data.JobcenterInterview);
          }
          $("#interviewApplicant").val(all.attr('data-name'));
        }
      }
    });
  });

  $("#agenda_entrevista").click(function() {
    $.ajax({
      type: "POST",
      url: "../scheduled_interview/add",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Applicant_id: $("input[name=Applicant_id]").val(),
        Fecha_entrevista: $("input[name=dateInterview]").val(),
        Hora_entrevista: $("input[name=timeInterview]").val(),
        Entrevistador: $("select[name=interviewerAdd]").val(),
        Lugar_entrevista: $("select[name=JobcenterInterview]").val()
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Applicant_id) {
            $("#error_solicitante").removeClass("hidden");
            $("#error_solicitante").text(data.errors.Applicant_id);
          } else {
            $("#error_solicitante").remove();
          }
          if (data.errors.Fecha_entrevista) {
            $("#error_diaentrevista").removeClass("hidden");
            $("#error_diaentrevista").text(data.errors.Fecha_entrevista);
          } else {
            $("#error_diaentrevista").remove();
          }
          if (data.errors.Hora_entrevista) {
            $("#error_horaentrevista").removeClass("hidden");
            $("#error_horaentrevista").text(data.errors.Hora_entrevista);
          } else {
            $("#error_horaentrevista").remove();
          }
          if (data.errors.Entrevistador) {
            $("#error_entrevistador").removeClass("hidden");
            $("#error_entrevistador").text(data.errors.Entrevistador);
          } else {
            $("#error_entrevistador").remove();
          }
          if (data.errors.Lugar_entrevista) {
            $("#error_lugarentrevista").removeClass("hidden");
            $("#error_lugarentrevista").text(data.errors.Lugar_entrevista);
          } else {
            $("#error_lugarentrevista").remove();
          }
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".error").addClass("hidden");
          location.reload();
        }
      }
    });
  });
  //agenda end
  // Funciones para los arboles
  $("#createTree").click(function() {
    var ref = $("#jstree_demo").jstree(true),
    sel = ref.get_selected();
    if (!sel.length) {
      return false;
    }
    sel = sel[0];
    sel = ref.create_node(sel, { type: "file" });
    if (sel) {
      ref.edit(sel);
    }
  });
  function demo_rename() {
    var ref = $("#jstree_demo").jstree(true),
    sel = ref.get_selected();
    if (!sel.length) {
      return false;
    }
    sel = sel[0];
    ref.edit(sel);
  }
  function demo_delete() {
    var ref = $("#jstree_demo").jstree(true),
    sel = ref.get_selected();
    if (!sel.length) {
      return false;
    }
    ref.delete_node(sel);
  }
  $(function() {
    var to = false;
    $("#demo_q").keyup(function() {
      if (to) {
        clearTimeout(to);
      }
      to = setTimeout(function() {
        var v = $("#demo_q").val();
        $("#jstree_jobcenter")
        .jstree(true)
        .search(v);
      }, 250);
    });
    $("#jstree_jobcenter").jstree({
      core: {
        animation: 0,
        check_callback: true,
        force_text: true,
        themes: { stripes: true },
        data: {
          url: "../tree_jobcenter/read",
          data: function(node) {
            return { id: node.id };
          }
        }
      },
      types: {
        "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
        root: { icon: "glyphicon glyphicon-home", valid_children: ["default"] },
        default: {
          icon: "glyphicon glyphicon-home",
          valid_children: ["default", "file"]
        },
        file: { icon: "glyphicon glyphicon-home", valid_children: [] }
      },
      plugins: ["contextmenu", "dnd", "search", "state", "types", "wholerow"]
    });
  });

  $("#saveTree").click(function() {
    loaderPestWare('Guardando...');
    var tree = $("#jstree_jobcenter")
    .jstree(true)
    .get_json();
    // and this is for the flat format (with ID / PARENT)
    var tree_flat = $("#jstree_jobcenter")
    .jstree(true)
    .get_json(null, { flat: true });
    //var textToSave = JSON.stringify(tree_flat);
    $.ajaxSetup({
      headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
    });
    $.ajax({
      type: "POST",
      url: "../tree_jobcenter/write",
      data: JSON.stringify(tree_flat),
      contentType: "json",
      processData: false,
      success: function(data) {
        Swal.close();
        if (data.code == 500) showError(data.message);
        else {
          showToast('success','Centros de trabajo','Guardado Correctamente');
          $("#saveTree").attr("disabled", true);
          setTimeout(function () {
            location.reload();
          }, 2000);
        }
      }
    });
  });
  $(function() {
    var to = false;
    $("#demo_q").keyup(function() {
      if (to) {
        clearTimeout(to);
      }
      to = setTimeout(function() {
        var v = $("#demo_q").val();
        $("#jstree_jobprofile")
        .jstree(true)
        .search(v);
      }, 250);
    });
    $("#jstree_jobprofile").jstree({
      core: {
        animation: 0,
        check_callback: true,
        force_text: true,
        themes: { stripes: true },
        data: {
          url: "../tree_jobprofile/read",
          data: function(node) {
            return { id: node.id };
          }
        }
      },
      types: {
        "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
        root: { icon: "glyphicon glyphicon-user", valid_children: ["default"] },
        default: {
          icon: "glyphicon glyphicon-user",
          valid_children: ["default", "file"]
        },
        file: { icon: "glyphicon glyphicon-user", valid_children: [] }
      },
      plugins: ["contextmenu", "dnd", "search", "state", "types", "wholerow"]
    });
  });

  $("#jstree_kardex").jstree({
    core: {
        animation: 0
      },
      types: {
        "#": { max_children: 1, max_depth: 10, valid_children: ["root"] },
        root: { icon: "glyphicon glyphicon-home", valid_children: ["default"] },
        default: {
          icon: "glyphicon glyphicon-home",
          valid_children: ["default", "file"]
        },
        file: { icon: "glyphicon glyphicon-user", valid_children: [] }
      },
      plugins: [ "search", "state", "types", "wholerow"]
    });
  $("#saveTreejobprofile").click(function() {
    var tree = $("#jstree_jobprofile")
    .jstree(true)
    .get_json();
    // and this is for the flat format (with ID / PARENT)
    var tree_flat = $("#jstree_jobprofile")
    .jstree(true)
    .get_json(null, { flat: true });
    //var textToSave = JSON.stringify(tree_flat);
    $.ajaxSetup({
      headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
    });
    $.ajax({
      type: "POST",
      url: "../tree_jobprofile/write",
      data: JSON.stringify(tree_flat),
      contentType: "json",
      processData: false,
      success: function(data) {
        showToast('success','Guardado', 'Guardando correctamente');
        setTimeout(function(){location.reload()},2000);
      }
    });
  });
  $("#homeVideo").ready(function() {
    if ($("#showVideo").val() != 1) {
      $("#myModalVideo").modal("show", function() {
        $("#currentTime").html(
          $("#video_container")
          .find("video")
          .get(0)
          .load()
          );
      });
    }
  });
  $("#btnOpenVideo").click(function() {
    $("#btnOpenVideo").addClass("hide");
    $("#currentTime").html(
      $("#video_container")
      .find("video")
      .get(0)
      .play()
      );
    i = setInterval(function() {
      $("#currentTime").html(
        $("#video_container")
        .find("video")
        .get(0).currentTime
        );
      $("#totalTime").html(
        $("#video_container")
        .find("video")
        .get(0).duration
        );
      currentTime = $("#video_container")
      .find("video")
      .get(0).currentTime;
      totalTime = $("#video_container")
      .find("video")
      .get(0).duration;
      if (currentTime == totalTime) {
        $("#btnCloseVideo").removeClass("hide");
        clearInterval(i);
      }
    }, 500);
  });
  //Interview
  $(".save_interview").click(function(event) {
    var active = $( this ).attr('data-active');
    var pass;
    var last = false;
    $(".tabsInterviews").each(function( index ) {
      if (active === $(this).attr('href').replace("#","")) {
        pass = index + 1;
      }
    });
    var divActive = '#' + $(this).attr('data-active');
    if(divActive === $('.tabsInterviews:last').attr('href')){
      last = true;
    }
    var arrayAnswers = new Array;
    $(divActive).find('input:checked').each(function(index){
      arrayAnswers.push({'idQuestion': $(this).attr('id').replace("answer_","") , 'valor': $(this).val()});
    });
    $.ajax({
      type: "POST",
      url: "../save",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Answers : arrayAnswers,
        UserId : $('#usersInterview').val(),
        SectionId : $(divActive).find($(".sectionId")).val(),
        Validator: last,
      },
      success: function(data) {
        if (data.errors) {
          console.log(data.errors);
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $('.tabsInterviews').eq(pass).click();
        }
      }
    });
  });
  $("#save_order_organization").click(function(event) {
    var identificador = $("#operative_interview_id").val();
    var result =
    (parseInt(radiovalidacion("org_order_ask1")) +
      parseInt(radiovalidacion("org_order_ask2")) +
      parseInt(radiovalidacion("org_order_ask3")) +
      parseInt(radiovalidacion("org_order_ask4"))) *
    25 /
    12;
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../operative_interview_order_organization/addOrderOrganization",
      data: {
        Order_Organization_1: radiovalidacion("org_order_ask1"),
        Order_Organization_2: radiovalidacion("org_order_ask2"),
        Order_Organization_3: radiovalidacion("org_order_ask3"),
        Order_Organization_4: radiovalidacion("org_order_ask4"),
        Score: result,
        id: identificador
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Order_Organization_1) {
            $("#error_order_organization_ask_1").removeClass("hidden");
            $("#error_order_organization_ask_1").text(
              data.errors.Order_Organization_1
              );
          } else $("#error_order_organization_ask_1").remove();
          if (data.errors.Order_Organization_2) {
            $("#error_order_organization_ask_2").removeClass("hidden");
            $("#error_order_organization_ask_2").text(
              data.errors.Order_Organization_2
              );
          } else $("#error_order_organization_ask_2").remove();
          if (data.errors.Order_Organization_3) {
            $("#error_order_organization_ask_3").removeClass("hidden");
            $("#error_order_organization_ask_3").text(
              data.errors.Order_Organization_3
              );
          } else $("#error_order_organization_ask_3").remove();
          if (data.errors.Order_Organization_4) {
            $("#error_order_organization_ask_4").removeClass("hidden");
            $("#error_order_organization_ask_4").text(
              data.errors.Order_Organization_4
              );
          } else $("#error_order_organization_ask_4").remove();
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".errors_orders").remove();
          $("#tab_norms").click();
        }
      }
    });
  });
  $("#save_norms").click(function(event) {
    var identificador = $("#operative_interview_id").val();
    var result =
    (parseInt(radiovalidacion("norms_ask1")) +
      parseInt(radiovalidacion("norms_ask2")) +
      parseInt(radiovalidacion("norms_ask3"))) *
    20 /
    9;
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../operative_interview_norms/addNorms",
      data: {
        Norms_1: radiovalidacion("norms_ask1"),
        Norms_2: radiovalidacion("norms_ask2"),
        Norms_3: radiovalidacion("norms_ask3"),
        Score: result,
        id: identificador
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Norms_1) {
            $("#error_norms_ask_1").removeClass("hidden");
            $("#error_norms_ask_1").text(data.errors.Norms_1);
          } else $("#error_norms_ask_1").remove();
          if (data.errors.Norms_2) {
            $("#error_norms_ask_2").removeClass("hidden");
            $("#error_norms_ask_2").text(data.errors.Norms_2);
          } else $("#error_norms_ask_2").remove();
          if (data.errors.Norms_3) {
            $("#error_norms_ask_3").removeClass("hidden");
            $("#error_norms_ask_3").text(data.errors.Norms_3);
          } else $("#error_norms_ask_3").remove();
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".errors_norms").remove();
          $("#tab_behaviors").click();
        }
      }
    });
  });
  $("#save_behaviors").click(function(event) {
    var identificador = $("#operative_interview_id").val();
    var result =
    (parseInt(radiovalidacion("behaviors_ask1")) +
      parseInt(radiovalidacion("behaviors_ask2")) +
      parseInt(radiovalidacion("behaviors_ask3")) +
      parseInt(radiovalidacion("behaviors_ask4"))) *
    15 /
    12;
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      }
    });
    $.ajax({
      type: "POST",
      url: "../operative_interview_behaviors/addBehaviors",
      data: {
        Behaviors_1: radiovalidacion("behaviors_ask1"),
        Behaviors_2: radiovalidacion("behaviors_ask2"),
        Behaviors_3: radiovalidacion("behaviors_ask3"),
        Behaviors_4: radiovalidacion("behaviors_ask4"),
        Score: result,
        id: identificador
      },
      success: function(data) {
        if (data.errors) {
          faltante();
          if (data.errors.Behaviors_1) {
            $("#error_behaviors_ask_1").removeClass("hidden");
            $("#error_behaviors_ask_1").text(data.errors.Behaviors_1);
          } else $("#error_behaviors_ask_1").remove();
          if (data.errors.Behaviors_2) {
            $("#error_behaviors_ask_2").removeClass("hidden");
            $("#error_behaviors_ask_2").text(data.errors.Behaviors_2);
          } else $("#error_behaviors_ask_2").remove();
          if (data.errors.Behaviors_3) {
            $("#error_behaviors_ask_3").removeClass("hidden");
            $("#error_behaviors_ask_3").text(data.errors.Behaviors_3);
          } else $("#error_behaviors_ask_3").remove();
          if (data.errors.Behaviors_4) {
            $("#error_behaviors_ask_4").removeClass("hidden");
            $("#error_behaviors_ask_4").text(data.errors.Behaviors_4);
          } else $("#error_behaviors_ask_4").remove();
        } else {
          showToast('success','Guardado', 'Guardando correctamente');
          $(".errors_behaviors").remove();
        }
      }
    });
  });
  //contract
  $("#download_btn").click(function() {
    $.ajax({
      type: "POST",
      url: "../../contractdownload",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Respuesta: "Si"
      },
      success: function(data) {
        if (data.errors) {
          alerr(data.errors);
        } else {
          $(".upload_div").removeClass("hidden");
          $("#Contractfile").focus();
        }
      }
    });
  });
  if ($("#pass_contract").val() == 1) {
    $(".upload_div").removeClass("hidden");
    $("#Contractfile").focus();
  }
  $("#upload_btn").click(function() {
    $.ajax({
      type: "POST",
      url: "../contractupload",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        Respuesta: "Si",
        Contract: $("input[name=contract_file]").val()
      },
      success: function(data) {
        if (data.errors) {
          if (data.errors.Contract) {
            $("#error_contractfile").removeClass("hidden");
            $("#error_contractfile").text(data.errors.Contract);
          } else $("#error_contractfile").remove();
        } else {
          $(".error").addClass("hidden");
        }
      }
    });
  });
  //end contract
  //ANSWER EVALUATION
  $(".btn-save-encuesta").click(function() {
    var ans;
    var pass = $(".answer").attr("type");
    switch (pass) {
      case "radio":
      ans = radiovalidacion("answer");
      break;
      case "checkbox":
      ans = new Array();
      $("input:checkbox:checked").each(function(i) {
        ans[i] = $(this).val();
      });
      break;
      default:
      ans = new Array();
      $(".answer").each(function(i) {
        ans[i] = $(this)
        .parent()
        .attr("data-id");
      });
    }
    var page;
    if ($(this).attr("data-info") == "Next") {
      page = $("#next_page").val();
    } else if ($(this).attr("data-info") == "Previous") {
      page = $("#previous_page").val();
    } else {
      page = "../../courses/resultsUser/" + $('#courseId').val();
    }
    $.ajax({
      type: "POST",
      url: "../../courses/save",
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        question_id: $("#question_id").val(),
        Answer: ans,
        CourseId : $('#courseId').val()
      },
      success: function(data) {
        if (data.errors) alert("FALTA RESPUESTA");
        else {
          window.location.href = page;
        }
      }
    });
  });
  $("#closeResults").click(function() {
    window.location.href = "../../home";
  });
  // END ANSWER EVALUATION

  //CREATION EVALUATION
  //Read table default
  var optionsDetails = [];
  var variable = $("#questionId").val();
  if (variable != undefined && variable != " ") {
    $("#tableOptionsCreate")
    .children("tr")
    .each(function() {
      var temp = [];
      $(this)
      .children("td")
      .each(function() {
        if (
          $(this)
          .children("span")
          .text()
          ) {
          temp.push(
            $(this)
            .children("span")
            .text()
            );
      }
    });
      optionsDetails.push([temp[0], temp[1], temp[2]]);
    });
    removeOptionBtn();
  }
  $("#typeQuestion").change(function() {
    if ($(this).val() == "order") {
      $("#inputCorrect").attr("type", "number");
      $("#inputCorrect").css("width", "4em");
      $("#inputCorrect").addClass("form-control");
    } else {
      $("#inputCorrect").attr("type", "checkbox");
      $("#inputCorrect").removeClass("form-control");
      $("#inputCorrect").removeAttr("style");
    }
  });
  $("#add_answer").click(function() {
    var correct = "no";
    var option = $("#optionCreate").val();
    $("#optionCreate").val("");
    if ($("#inputCorrect").attr("type") == "number") {
      correct = $("#inputCorrect").val();
      $("#inputCorrect").val("");
    } else {
      if ($("#inputCorrect").prop("checked")) {
        correct = "si";
        $("#inputCorrect").prop("checked", false); // Unchecks it
      }
    }
    optionsDetails.push([correct, option, ""]);
    var row =
    "<tr class='optionRow'><td class='optionCheck text-center'><span>" +
    correct +
    "</span></td><td class='optionInput'><span>" +
    option +
    "</span></td> <td hidden='true'><span></span></td>" +
    "<td><button type='button' class='btn btn-danger removeOption'><i class='fa fa-minus' aria-hidden='true'></i></button></td></tr>";
    $("#tableOptionsCreate").append(row);
    removeOptionBtn();
  });
  function removeOptionBtn() {
    $(".removeOption").off("click");
    $(".removeOption").on("click", function() {
      var identificador = $(this)
      .parent()
      .parent()
      .children(".optionCheck")
      .children("span")
      .text();
      var idenText = $(this)
      .parent()
      .parent()
      .children(".optionInput")
      .children("span")
      .text();
      var tmp;
      optionsDetails.forEach(function(option, index) {
        option[0] == identificador && option[1] == idenText
        ? (tmp = index)
        : "";
      });
      optionsDetails.splice(tmp, 1);
      $("#tableOptionsCreate").empty();
      optionsDetails.forEach(function(option, index) {
        var row =
        "<tr class='optionRow'><td class='optionCheck text-center'>" +
        option[0] +
        "</td><td class='optionInput'>" +
        option[1] +
        "</td><td><button type='button' class='btn btn-danger removeOption'><i class='fa fa-minus' aria-hidden='true'></i></button></td></tr>";
        $("#tableOptionsCreate").append(row);
      });
      removeOptionBtn();
    });
  }
  $("#saveQuestion").click(function() {
    var urlTmp = $(this).attr("data-info");
    $.ajax({
      type: "POST",
      url: urlTmp,
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        QuestionId: $("#questionId").val(),
        CourseID: $("#courseId").val(),
        Question: $("#inputAsk").val(),
        Type: $("#typeQuestion").val(),
        Options: optionsDetails
      },
      success: function(data) {
        if (data.errors) {
          if (data.errors.Question) {
            missingText("Es necesario que registre una pregunta");
          }
        } else {
          showToast('success','Preguntas', 'Guardando correctamente');
          location.reload();
        }
      }
    });
  });

  //CREATION TASK EVALUATION
  //Read table default
  var optionsDetailsT = [];
  var variableT = $("#questionIdT").val();
  if (variableT != undefined && variableT != " ") {
    $("#tableOptionsCreateT")
    .children("tr")
    .each(function() {
      var temp = [];
      $(this)
      .children("td")
      .each(function() {
        if (
          $(this)
          .children("span")
          .text()
          ) {
          temp.push(
            $(this)
            .children("span")
            .text()
            );
      }
    });
      optionsDetailsT.push([temp[0], temp[1], temp[2]]);
    });
    removeOptionBtn();
  }
  $("#typeQuestionT").change(function() {
    if ($(this).val() == "order") {
      $("#inputCorrectT").attr("type", "number");
      $("#inputCorrectT").css("width", "4em");
      $("#inputCorrectT").addClass("form-control");
    } else {
      $("#inputCorrectT").attr("type", "checkbox");
      $("#inputCorrectT").removeClass("form-control");
      $("#inputCorrectT").removeAttr("style");
    }
  });
  $("#add_answerT").click(function() {
    var correctT = "no";
    var optionT = $("#optionCreateT").val();
    $("#optionCreateT").val("");
    if ($("#inputCorrectT").attr("type") == "number") {
      correctT = $("#inputCorrectT").val();
      $("#inputCorrectT").val("");
    } else {
      if ($("#inputCorrectT").prop("checked")) {
        correctT = "si";
        $("#inputCorrectT").prop("checked", false); // Unchecks it
      }
    }
    optionsDetailsT.push([correctT, optionT, ""]);
    var rowT =
    "<tr class='optionRowT'><td class='optionCheckT text-center'><span>" +
    correctT +
    "</span></td><td class='optionInputT'><span>" +
    optionT +
    "</span></td> <td hidden='true'><span></span></td>" +
    "<td><button type='button' class='btn btn-danger removeOptionT'><i class='fa fa-minus' aria-hidden='true'></i></button></td></tr>";
    $("#tableOptionsCreateT").append(rowT);
    removeOptionBtn();
  });
  function removeOptionBtn() {
    $(".removeOptionT").off("click");
    $(".removeOptionT").on("click", function() {
      var identificador = $(this)
      .parent()
      .parent()
      .children(".optionCheckT")
      .children("span")
      .text();
      var idenText = $(this)
      .parent()
      .parent()
      .children(".optionInputT")
      .children("span")
      .text();
      var tmp;
      optionsDetailsT.forEach(function(optionT, index) {
        optionT[0] == identificador && optionT[1] == idenText
        ? (tmp = index)
        : "";
      });
      optionsDetailsT.splice(tmp, 1);
      $("#tableOptionsCreateT").empty();
      optionsDetailsT.forEach(function(optionT, index) {
        var rowT =
        "<tr class='optionRowT'><td class='optionCheckT text-center'>" +
        optionT[0] +
        "</td><td class='optionInputT'>" +
        optionT[1] +
        "</td><td><button type='button' class='btn btn-danger removeOptionT'><i class='fa fa-minus' aria-hidden='true'></i></button></td></tr>";
        $("#tableOptionsCreateT").append(rowT);
      });
      removeOptionBtn();
    });
  }
  $("#saveQuestionT").click(function() {
    var urlTmpT = $(this).attr("data-info");
    $.ajax({
      type: "POST",
      url: urlTmpT,
      data: {
        _token: $("meta[name=csrf-token]").attr("content"),
        QuestionId: $("#questionIdT").val(),
        CourseID: $("#courseIdT").val(),
        Question: $("#inputAskT").val(),
        Type: $("#typeQuestionT").val(),
        Options: optionsDetailsT
      },
      success: function(data) {
        if (data.errors) {
          if (data.errors.Question) {
            alert("Es necesario que registre una pregunta");
          }
        } else {
          alert("Guardado");
          //window.location.href = '../../evaluation/courses/' + $('#courseId').val() ;
        }
      }
    });
  });

    //start CREATION TASK EVALUATION
    //Read table default
    var optionsDetailsT = [];
    var variableT = $("#questionIdT").val();
    if (variableT != undefined && variableT != " ") {
        $("#tableOptionsCreateT")
            .children("tr")
            .each(function() {
                var temp = [];
                $(this)
                    .children("td")
                    .each(function() {
                        if (
                            $(this)
                                .children("span")
                                .text()
                        ) {
                            temp.push(
                                $(this)
                                    .children("span")
                                    .text()
                            );
                        }
                    });
                optionsDetailsT.push([temp[0], temp[1], temp[2]]);
            });
        removeOptionBtn();
    }
    $("#typeQuestionT").change(function() {
        if ($(this).val() == "order") {
            $("#inputCorrectT").attr("type", "number");
            $("#inputCorrectT").css("width", "4em");
            $("#inputCorrectT").addClass("form-control");
        } else {
            $("#inputCorrectT").attr("type", "checkbox");
            $("#inputCorrectT").removeClass("form-control");
            $("#inputCorrectT").removeAttr("style");
        }
    });
    $("#add_answerT").click(function() {
        var correctT = "no";
        var optionT = $("#optionCreateT").val();
        $("#optionCreateT").val("");
        if ($("#inputCorrectT").attr("type") == "number") {
            correctT = $("#inputCorrectT").val();
            $("#inputCorrectT").val("");
        } else {
            if ($("#inputCorrectT").prop("checked")) {
                correctT = "si";
                $("#inputCorrectT").prop("checked", false); // Unchecks it
            }
        }
        optionsDetailsT.push([correctT, optionT, ""]);
        var rowT =
            "<tr class='optionRowT'><td class='optionCheckT text-center'><span>" +
            correctT +
            "</span></td><td class='optionInputT'><span>" +
            optionT +
            "</span></td> <td hidden='true'><span></span></td>" +
            "<td><button type='button' class='btn btn-danger removeOptionT'><i class='fa fa-minus' aria-hidden='true'></i></button></td></tr>";
        $("#tableOptionsCreateT").append(rowT);
        removeOptionBtn();
    });
    function removeOptionBtn() {
        $(".removeOptionT").off("click");
        $(".removeOptionT").on("click", function() {
            var identificador = $(this)
                .parent()
                .parent()
                .children(".optionCheckT")
                .children("span")
                .text();
            var idenText = $(this)
                .parent()
                .parent()
                .children(".optionInputT")
                .children("span")
                .text();
            var tmp;
            optionsDetailsT.forEach(function(optionT, index) {
                optionT[0] == identificador && optionT[1] == idenText
                    ? (tmp = index)
                    : "";
            });
            optionsDetailsT.splice(tmp, 1);
            $("#tableOptionsCreateT").empty();
            optionsDetailsT.forEach(function(optionT, index) {
                var rowT =
                    "<tr class='optionRowT'><td class='optionCheckT text-center'>" +
                    optionT[0] +
                    "</td><td class='optionInputT'>" +
                    optionT[1] +
                    "</td><td><button type='button' class='btn btn-danger removeOptionT'><i class='fa fa-minus' aria-hidden='true'></i></button></td></tr>";
                $("#tableOptionsCreateT").append(rowT);
            });
            removeOptionBtn();
        });
    }
    $("#saveQuestionT").click(function() {
        var urlTmpT = $(this).attr("data-info");
        $.ajax({
            type: "POST",
            url: urlTmpT,
            data: {
                _token: $("meta[name=csrf-token]").attr("content"),
                QuestionId: $("#questionIdT").val(),
                CourseID: $("#courseIdT").val(),
                Question: $("#inputAskT").val(),
                Type: $("#typeQuestionT").val(),
                Options: optionsDetailsT
            },
            success: function(data) {
                if (data.errors) {
                    if (data.errors.Question) {
                        alert("Es necesario que registre una pregunta");
                    }
                } else {
                    alert("Guardado");
                    //window.location.href = '../../evaluation/courses/' + $('#courseId').val() ;
                }
            }
        });
    });

    //end

      //start Answer Task Evaluation
      $(".btn-save-encuestaT").click(function() {
        var ansT;
        var passT = $(".answerT").attr("type");
        switch (passT) {
          case "radio":
          ansT = radiovalidacion("answerT");
          break;
          case "checkbox":
          ansT = new Array();
          $("input:checkbox:checked").each(function(i) {
            ansT[i] = $(this).val();
          });
          break;
          default:
          ansT = new Array();
          $(".answerT").each(function(i) {
            ansT[i] = $(this)
            .parent()
            .attr("data-id");
          });
        }
        var pageT;
        if ($(this).attr("data-info") == "NextT") {
          pageT = $("#next_pageT").val();
        } else if ($(this).attr("data-info") == "PreviousT") {
          pageT = $("#previous_pageT").val();
        } else {
          pageT = "../../tasks/resultsUserT/" + $('#courseId').val();
        }
        $.ajax({
          type: "POST",
          url: "../../tasks/saveResult",
          data: {
            _token: $("meta[name=csrf-token]").attr("content"),
            question_id: $("#question_id").val(),
            Answer: ansT,
            CourseId : $('#courseId').val()
          },
          success: function(data) {
            if (data.errors) alert("Falta Respuesta");
            else {
              window.location.href = pageT;
            }
          }
        });
      });
      $("#closeResults").click(function() {
        window.location.href = "../../home";
      });
    //end Answer Task Evaluation

  $('.search_employees').keyup(function(){
    _this = this;
    $.each($("#" + $(_this).attr('data-table') + " tbody tr"), function(){
      if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
       $(this).hide();
     else
       $(this).show();
   });
  });

});
function goBack() {
  window.history.back();
}

//alerts messages
    function missingText(textError) {
        swal({
            title: "¡Espera!",
            type: "error",
            text: textError,
            icon: "error",
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
        });
    }
    function loaderPestWare(message) {
  Swal.fire({
    html: `<div class="content-loader">
                    <div class="loader">
                        <span style="--i:1;"></span>
                        <span style="--i:2;"></span>
                        <span style="--i:3;"></span>
                        <span style="--i:4;"></span>
                        <span style="--i:5;"></span>
                        <span style="--i:6;"></span>
                        <span style="--i:7;"></span>
                        <span style="--i:8;"></span>
                        <span style="--i:9;"></span>
                        <span style="--i:10;"></span>
                        <span style="--i:11;"></span>
                        <span style="--i:12;"></span>
                        <span style="--i:13;"></span>
                        <span style="--i:14;"></span>
                        <span style="--i:15;"></span>
                        <span style="--i:16;"></span>
                        <span style="--i:17;"></span>
                        <span style="--i:18;"></span>
                        <span style="--i:19;"></span>
                        <span style="--i:20;"></span>
                        <div class="insect"></div>
                    </div>
                </div>
                <p class="text-center" style="color: #ffffff"><b>${message}</b></p>`,
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false
  });
  $(".swal2-modal").css('background-color', 'transparent');
  $('.swal2-container.swal2-shown').css('background', 'rgb(11 12 12 / 55%)');
}
    function showToast(type, title, message) {
      if (type === 'success') toastr.success(message, title)
    }

