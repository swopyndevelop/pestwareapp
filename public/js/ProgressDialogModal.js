/**
 * Genera una pantalla de cargando.
 * @param global Variable general paa el proyecto.
 * 
 * @version 1.0.
 * @author Abraham Esparza Moreno.
 */
(function (global) {

    // Declaramos el codigo como estricto.
    "use strict";
    
    /**
     * Declaramos nuestra propiedad dentro del global.
     */
    var ProgressDialogModal = function () {

        /**
         * Regresamos la instancia de una invocacion anterior.
         */
        if (ProgressDialogModal.prototype._singletonInstance) {
            return ProgressDialogModal.prototype._singletonInstance;
        }

        /**
         * Declaramos la instancia de singleton.
         */
        ProgressDialogModal.prototype._singletonInstance = this;

        /**
         * Declaramos una referenia a un modal.
         */
        this.Modal = null;

        /**
         * Constante que activa el loader al hacer click
         */
        this.ClassShow = ".displayLoader";

        /**
         * Inicializa el modal dentro del cotenedor y el listener de ejecución.
         * @param {*} container Contenedor donde se gaurdara el modal.
         */
        this.InitModal = function(container)
        {
            container.append('<div id="ProgressDialogModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog" ><div class="modal-dialog" style="width: 0px;height: 0px;position: absolute;top: 50%;left: 50%;margin-top: -25px;margin-left: -25px;padding: 0px;"><div class="modal-body"><span class="cssload-loader"><span class="cssload-loader-inner"></span></span></div></div></div>');
            this.Modal = $("#ProgressDialogModal");

            //Cargamos los linsteners
            $(this.ClassShow).on("click", function(){
                ProgressDialogModal.prototype._singletonInstance.ShowModal();
            });

            //Cachamos el evento de carga
            window.onbeforeunload = function() {
                ProgressDialogModal.prototype._singletonInstance.ShowModal();
            };
        }

        /**
         * Muestra el dialogo en la pantalla.
         */
        this.ShowModal = function()
        {
            $('#ProgressDialogModal.modal').show(); 
            this.Modal.modal('show');
        }

        /**
         * Esconde el modal dentro de la pantalla.
         */
        this.HideModal = function()
        {
            this.Modal.removeClass('in');
            this.Modal.removeClass('show');
            this.Modal.modal('hide');
            $('#ProgressDialogModal.modal.in').hide(); 
        }
    };

    // Registramos una clase dentro del registro global.
    global.ProgressDialogModal = new ProgressDialogModal();

 } (window));