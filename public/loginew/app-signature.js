document.addEventListener("DOMContentLoaded", function() {

    paper.setup(document.getElementById('myCanvas'));

    let path, touchCanvas = 0;

    function onMouseDown(event) {
        path = new paper.Path({
            strokeColor: 'black',
            strokeWidth: 2.2,
            strokeCap: 'round'
        });
        path.add(event.point);
    }

    function onMouseDrag(event) {
        // Añadir puntos al trazo mientras se arrastra el ratón
        path.add(event.point);
    }

    function onMouseUp() {
        // Dejar de añadir puntos al trazo
        path = null;
    }

    paper.view.onMouseDown = onMouseDown;
    paper.view.onMouseDrag = onMouseDrag;
    paper.view.onMouseUp = onMouseUp;

    // Agregar evento click al lienzo para validar clics
    paper.view.onClick = function(event) {
        touchCanvas = 1
    };

    function saveImage() {
        let name = document.getElementById('other_name_firm').value;
        // Obtener el data URL de la imagen en el lienzo de Paper.js
        let dataURL = paper.view.element.toDataURL({
            format: 'jpeg',
            quality: 1 // Ajusta la calidad de la imagen (entre 0 y 1)
        });

        let url = window.location.href;
        let partesURL = url.split("/");
        let id = partesURL[partesURL.length - 1];

        // Crear un Blob desde el data URL
        fetch(dataURL)
            .then(res => res.blob())
            .then(blob => {
                let formData = new FormData();
                formData.append('id', id);
                formData.append('name', name);
                formData.append('updateCanvas', touchCanvas);
                formData.append('file', blob, 'imagen_dibujada.jpg');

                $.ajax({
                    url: '/signature/service/mip',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        window.location.reload();
                        console.log('Imagen guardada con éxito.');
                    },
                    error: function (xhr, status, error) {
                        console.error('Error al guardar la imagen:', error);
                    }
                });
            });
    }

// Agregar el evento onMouseDrag al lienzo
    paper.view.onMouseDrag = onMouseDrag;

// Llamada a la función saveImage al hacer clic en el botón
    $('#sendSignatureCustomer').click(function () {
        saveImage();
    });

});